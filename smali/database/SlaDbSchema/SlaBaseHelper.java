public class database.SlaDbSchema.SlaBaseHelper extends android.database.sqlite.SQLiteOpenHelper {
	 /* .source "SlaBaseHelper.java" */
	 /* # static fields */
	 private static final java.lang.String DATABASE_NAME;
	 private static final java.lang.String TAG;
	 private static final Integer VERSION;
	 /* # direct methods */
	 static database.SlaDbSchema.SlaBaseHelper ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 /* const-class v0, Ldatabase/SlaDbSchema/SlaBaseHelper; */
		 (( java.lang.Class ) v0 ).getName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;
		 return;
	 } // .end method
	 public database.SlaDbSchema.SlaBaseHelper ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 19 */
		 int v0 = 0; // const/4 v0, 0x0
		 int v1 = 1; // const/4 v1, 0x1
		 final String v2 = "SlaBase.db"; // const-string v2, "SlaBase.db"
		 /* invoke-direct {p0, p1, v2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V */
		 /* .line 20 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void onCreate ( android.database.sqlite.SQLiteDatabase p0 ) {
		 /* .locals 4 */
		 /* .param p1, "db" # Landroid/database/sqlite/SQLiteDatabase; */
		 /* .line 24 */
		 final String v0 = "create table SlaUid(_id integer primary key autoincrement, uid,dayTraffic,monthTraffic,state,day,month)"; // const-string v0, "create table SlaUid(_id integer primary key autoincrement, uid,dayTraffic,monthTraffic,state,day,month)"
		 /* .line 32 */
		 /* .local v0, "createDb":Ljava/lang/String; */
		 v1 = database.SlaDbSchema.SlaBaseHelper.TAG;
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "create db cmd:"; // const-string v3, "create db cmd:"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Log .d ( v1,v2 );
		 /* .line 33 */
		 (( android.database.sqlite.SQLiteDatabase ) p1 ).execSQL ( v0 ); // invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
		 /* .line 34 */
		 return;
	 } // .end method
	 public void onUpgrade ( android.database.sqlite.SQLiteDatabase p0, Integer p1, Integer p2 ) {
		 /* .locals 0 */
		 /* .param p1, "db" # Landroid/database/sqlite/SQLiteDatabase; */
		 /* .param p2, "oldVersion" # I */
		 /* .param p3, "newVersion" # I */
		 /* .line 39 */
		 return;
	 } // .end method
