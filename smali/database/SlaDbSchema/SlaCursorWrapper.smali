.class public Ldatabase/SlaDbSchema/SlaCursorWrapper;
.super Landroid/database/CursorWrapper;
.source "SlaCursorWrapper.java"


# instance fields
.field private mDay:I

.field private mDayTraffic:J

.field private mMonth:I

.field private mMonthTraffic:J

.field private mState:Z


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .line 9
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 10
    return-void
.end method


# virtual methods
.method public getSLAApp()Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    .locals 10

    .line 19
    const-string/jumbo v0, "uid"

    invoke-virtual {p0, v0}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "uid":Ljava/lang/String;
    const-string v1, "dayTraffic"

    invoke-virtual {p0, v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getLong(I)J

    move-result-wide v1

    .line 21
    .local v1, "daytraffic":J
    const-string v3, "monthTraffic"

    invoke-virtual {p0, v3}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p0, v3}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getLong(I)J

    move-result-wide v3

    .line 22
    .local v3, "monthtraffic":J
    const-string/jumbo v5, "state"

    invoke-virtual {p0, v5}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v5}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getInt(I)I

    move-result v5

    .line 23
    .local v5, "state":I
    const-string v6, "day"

    invoke-virtual {p0, v6}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p0, v6}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getInt(I)I

    move-result v6

    .line 24
    .local v6, "day":I
    const-string v7, "month"

    invoke-virtual {p0, v7}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {p0, v7}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getInt(I)I

    move-result v7

    .line 26
    .local v7, "month":I
    new-instance v8, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;

    invoke-direct {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;-><init>()V

    .line 27
    .local v8, "slaApp":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;
    invoke-virtual {v8, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setUid(Ljava/lang/String;)V

    .line 28
    invoke-virtual {v8, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V

    .line 29
    invoke-virtual {v8, v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonth(I)V

    .line 30
    invoke-virtual {v8, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V

    .line 31
    invoke-virtual {v8, v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V

    .line 32
    if-eqz v5, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    :goto_0
    invoke-virtual {v8, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V

    .line 34
    return-object v8
.end method
