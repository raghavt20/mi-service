public class database.SlaDbSchema.SlaCursorWrapper extends android.database.CursorWrapper {
	 /* .source "SlaCursorWrapper.java" */
	 /* # instance fields */
	 private Integer mDay;
	 private Long mDayTraffic;
	 private Integer mMonth;
	 private Long mMonthTraffic;
	 private Boolean mState;
	 /* # direct methods */
	 public database.SlaDbSchema.SlaCursorWrapper ( ) {
		 /* .locals 0 */
		 /* .param p1, "cursor" # Landroid/database/Cursor; */
		 /* .line 9 */
		 /* invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V */
		 /* .line 10 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public com.xiaomi.NetworkBoost.slaservice.SLAApp getSLAApp ( ) {
		 /* .locals 10 */
		 /* .line 19 */
		 /* const-string/jumbo v0, "uid" */
		 v0 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getColumnIndex ( v0 ); // invoke-virtual {p0, v0}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I
		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getString ( v0 ); // invoke-virtual {p0, v0}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getString(I)Ljava/lang/String;
		 /* .line 20 */
		 /* .local v0, "uid":Ljava/lang/String; */
		 final String v1 = "dayTraffic"; // const-string v1, "dayTraffic"
		 v1 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getColumnIndex ( v1 ); // invoke-virtual {p0, v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I
		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getLong ( v1 ); // invoke-virtual {p0, v1}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getLong(I)J
		 /* move-result-wide v1 */
		 /* .line 21 */
		 /* .local v1, "daytraffic":J */
		 final String v3 = "monthTraffic"; // const-string v3, "monthTraffic"
		 v3 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getColumnIndex ( v3 ); // invoke-virtual {p0, v3}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I
		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getLong ( v3 ); // invoke-virtual {p0, v3}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getLong(I)J
		 /* move-result-wide v3 */
		 /* .line 22 */
		 /* .local v3, "monthtraffic":J */
		 /* const-string/jumbo v5, "state" */
		 v5 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getColumnIndex ( v5 ); // invoke-virtual {p0, v5}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I
		 v5 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getInt ( v5 ); // invoke-virtual {p0, v5}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getInt(I)I
		 /* .line 23 */
		 /* .local v5, "state":I */
		 final String v6 = "day"; // const-string v6, "day"
		 v6 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getColumnIndex ( v6 ); // invoke-virtual {p0, v6}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I
		 v6 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getInt ( v6 ); // invoke-virtual {p0, v6}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getInt(I)I
		 /* .line 24 */
		 /* .local v6, "day":I */
		 final String v7 = "month"; // const-string v7, "month"
		 v7 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getColumnIndex ( v7 ); // invoke-virtual {p0, v7}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getColumnIndex(Ljava/lang/String;)I
		 v7 = 		 (( database.SlaDbSchema.SlaCursorWrapper ) p0 ).getInt ( v7 ); // invoke-virtual {p0, v7}, Ldatabase/SlaDbSchema/SlaCursorWrapper;->getInt(I)I
		 /* .line 26 */
		 /* .local v7, "month":I */
		 /* new-instance v8, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
		 /* invoke-direct {v8}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;-><init>()V */
		 /* .line 27 */
		 /* .local v8, "slaApp":Lcom/xiaomi/NetworkBoost/slaservice/SLAApp; */
		 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v8 ).setUid ( v0 ); // invoke-virtual {v8, v0}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setUid(Ljava/lang/String;)V
		 /* .line 28 */
		 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v8 ).setDay ( v6 ); // invoke-virtual {v8, v6}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDay(I)V
		 /* .line 29 */
		 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v8 ).setMonth ( v7 ); // invoke-virtual {v8, v7}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonth(I)V
		 /* .line 30 */
		 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v8 ).setDayTraffic ( v1, v2 ); // invoke-virtual {v8, v1, v2}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setDayTraffic(J)V
		 /* .line 31 */
		 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v8 ).setMonthTraffic ( v3, v4 ); // invoke-virtual {v8, v3, v4}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setMonthTraffic(J)V
		 /* .line 32 */
		 if ( v5 != null) { // if-eqz v5, :cond_0
			 int v9 = 1; // const/4 v9, 0x1
		 } // :cond_0
		 int v9 = 0; // const/4 v9, 0x0
	 } // :goto_0
	 (( com.xiaomi.NetworkBoost.slaservice.SLAApp ) v8 ).setState ( v9 ); // invoke-virtual {v8, v9}, Lcom/xiaomi/NetworkBoost/slaservice/SLAApp;->setState(Z)V
	 /* .line 34 */
} // .end method
