public class miui.android.view.animation.CubicEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "CubicEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.CubicEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x40000000 # 2.0f */
		 /* mul-float/2addr p1, v0 */
		 /* .line 16 */
		 /* const/high16 v1, 0x3f800000 # 1.0f */
		 /* cmpg-float v1, p1, v1 */
		 /* const/high16 v2, 0x3f000000 # 0.5f */
		 /* if-gez v1, :cond_0 */
		 /* .line 17 */
		 /* mul-float/2addr v2, p1 */
		 /* mul-float/2addr v2, p1 */
		 /* mul-float/2addr v2, p1 */
		 /* .line 20 */
	 } // :cond_0
	 /* sub-float/2addr p1, v0 */
	 /* .line 21 */
	 /* mul-float v1, p1, p1 */
	 /* mul-float/2addr v1, p1 */
	 /* add-float/2addr v1, v0 */
	 /* mul-float/2addr v1, v2 */
} // .end method
