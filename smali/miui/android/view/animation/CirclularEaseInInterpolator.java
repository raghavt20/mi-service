public class miui.android.view.animation.CirclularEaseInInterpolator implements android.view.animation.Interpolator {
	 /* .source "CirclularEaseInInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.CirclularEaseInInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x3f800000 # 1.0f */
		 /* mul-float v1, p1, p1 */
		 /* sub-float/2addr v0, v1 */
		 /* float-to-double v0, v0 */
		 java.lang.Math .sqrt ( v0,v1 );
		 /* move-result-wide v0 */
		 /* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
		 /* sub-double/2addr v0, v2 */
		 /* double-to-float v0, v0 */
		 /* neg-float v0, v0 */
	 } // .end method
