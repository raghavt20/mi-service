public class miui.android.view.animation.SineEaseOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "SineEaseOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.SineEaseOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* float-to-double v0, p1 */
		 /* const-wide v2, 0x3ff921fb54442d18L # 1.5707963267948966 */
		 /* mul-double/2addr v0, v2 */
		 java.lang.Math .sin ( v0,v1 );
		 /* move-result-wide v0 */
		 /* double-to-float v0, v0 */
	 } // .end method
