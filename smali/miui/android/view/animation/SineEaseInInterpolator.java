public class miui.android.view.animation.SineEaseInInterpolator implements android.view.animation.Interpolator {
	 /* .source "SineEaseInInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.SineEaseInInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* float-to-double v0, p1 */
		 /* const-wide v2, 0x3ff921fb54442d18L # 1.5707963267948966 */
		 /* mul-double/2addr v0, v2 */
		 java.lang.Math .cos ( v0,v1 );
		 /* move-result-wide v0 */
		 /* double-to-float v0, v0 */
		 /* neg-float v0, v0 */
		 /* const/high16 v1, 0x3f800000 # 1.0f */
		 /* add-float/2addr v0, v1 */
	 } // .end method
