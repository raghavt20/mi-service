public class miui.android.view.animation.ElasticEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "ElasticEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private final Float mAmplitude;
	 private final Float mPeriod;
	 /* # direct methods */
	 public miui.android.view.animation.ElasticEaseInOutInterpolator ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, v0, v0}, Lmiui/android/view/animation/ElasticEaseInOutInterpolator;-><init>(FF)V */
		 /* .line 22 */
		 return;
	 } // .end method
	 public miui.android.view.animation.ElasticEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .param p1, "amplitude" # F */
		 /* .param p2, "period" # F */
		 /* .line 28 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 /* iput p1, p0, Lmiui/android/view/animation/ElasticEaseInOutInterpolator;->mAmplitude:F */
		 /* .line 30 */
		 /* iput p2, p0, Lmiui/android/view/animation/ElasticEaseInOutInterpolator;->mPeriod:F */
		 /* .line 31 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 11 */
		 /* .param p1, "t" # F */
		 /* .line 35 */
		 /* iget v0, p0, Lmiui/android/view/animation/ElasticEaseInOutInterpolator;->mPeriod:F */
		 /* .line 36 */
		 /* .local v0, "p":F */
		 /* iget v1, p0, Lmiui/android/view/animation/ElasticEaseInOutInterpolator;->mAmplitude:F */
		 /* .line 39 */
		 /* .local v1, "a":F */
		 int v2 = 0; // const/4 v2, 0x0
		 /* cmpl-float v3, p1, v2 */
		 /* if-nez v3, :cond_0 */
		 /* .line 40 */
		 /* .line 43 */
	 } // :cond_0
	 /* const/high16 v3, 0x3f000000 # 0.5f */
	 /* div-float/2addr p1, v3 */
	 /* .line 44 */
	 /* const/high16 v3, 0x40000000 # 2.0f */
	 /* cmpl-float v3, p1, v3 */
	 /* const/high16 v4, 0x3f800000 # 1.0f */
	 /* if-nez v3, :cond_1 */
	 /* .line 45 */
	 /* .line 47 */
} // :cond_1
/* cmpl-float v3, v0, v2 */
/* if-nez v3, :cond_2 */
/* .line 48 */
/* const v0, 0x3ee66667 # 0.45000002f */
/* .line 50 */
} // :cond_2
/* cmpl-float v2, v1, v2 */
/* const-wide v5, 0x401921fb54442d18L # 6.283185307179586 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* cmpg-float v2, v1, v4 */
/* if-gez v2, :cond_3 */
/* .line 54 */
} // :cond_3
/* float-to-double v2, v0 */
/* div-double/2addr v2, v5 */
/* div-float v7, v4, v1 */
/* float-to-double v7, v7 */
java.lang.Math .asin ( v7,v8 );
/* move-result-wide v7 */
/* mul-double/2addr v2, v7 */
/* double-to-float v2, v2 */
/* .local v2, "s":F */
/* .line 51 */
} // .end local v2 # "s":F
} // :cond_4
} // :goto_0
/* const/high16 v1, 0x3f800000 # 1.0f */
/* .line 52 */
/* const/high16 v2, 0x40800000 # 4.0f */
/* div-float v2, v0, v2 */
/* .line 56 */
/* .restart local v2 # "s":F */
} // :goto_1
/* cmpg-float v3, p1, v4 */
/* const-wide/high16 v7, 0x4000000000000000L # 2.0 */
/* if-gez v3, :cond_5 */
/* .line 57 */
/* sub-float/2addr p1, v4 */
/* .line 58 */
/* float-to-double v3, v1 */
/* const/high16 v9, 0x41200000 # 10.0f */
/* mul-float/2addr v9, p1 */
/* float-to-double v9, v9 */
java.lang.Math .pow ( v7,v8,v9,v10 );
/* move-result-wide v7 */
/* mul-double/2addr v3, v7 */
/* sub-float v7, p1, v2 */
/* float-to-double v7, v7 */
/* mul-double/2addr v7, v5 */
/* float-to-double v5, v0 */
/* div-double/2addr v7, v5 */
java.lang.Math .sin ( v7,v8 );
/* move-result-wide v5 */
/* mul-double/2addr v3, v5 */
/* double-to-float v3, v3 */
/* const/high16 v4, -0x41000000 # -0.5f */
/* mul-float/2addr v3, v4 */
/* .line 62 */
} // :cond_5
/* sub-float/2addr p1, v4 */
/* .line 63 */
/* float-to-double v3, v1 */
/* const/high16 v9, -0x3ee00000 # -10.0f */
/* mul-float/2addr v9, p1 */
/* float-to-double v9, v9 */
java.lang.Math .pow ( v7,v8,v9,v10 );
/* move-result-wide v7 */
/* mul-double/2addr v3, v7 */
/* sub-float v7, p1, v2 */
/* float-to-double v7, v7 */
/* mul-double/2addr v7, v5 */
/* float-to-double v5, v0 */
/* div-double/2addr v7, v5 */
java.lang.Math .sin ( v7,v8 );
/* move-result-wide v5 */
/* mul-double/2addr v3, v5 */
/* const-wide/high16 v5, 0x3fe0000000000000L # 0.5 */
/* mul-double/2addr v3, v5 */
/* const-wide/high16 v5, 0x3ff0000000000000L # 1.0 */
/* add-double/2addr v3, v5 */
/* double-to-float v3, v3 */
} // .end method
