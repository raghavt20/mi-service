public class miui.android.view.animation.BackEaseOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "BackEaseOutInterpolator.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private final Float mOvershot;
	 /* # direct methods */
	 public miui.android.view.animation.BackEaseOutInterpolator ( ) {
		 /* .locals 1 */
		 /* .line 20 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, v0}, Lmiui/android/view/animation/BackEaseOutInterpolator;-><init>(F)V */
		 /* .line 21 */
		 return;
	 } // .end method
	 public miui.android.view.animation.BackEaseOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .param p1, "overshot" # F */
		 /* .line 26 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 /* iput p1, p0, Lmiui/android/view/animation/BackEaseOutInterpolator;->mOvershot:F */
		 /* .line 28 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 32 */
		 /* iget v0, p0, Lmiui/android/view/animation/BackEaseOutInterpolator;->mOvershot:F */
		 int v1 = 0; // const/4 v1, 0x0
		 /* cmpl-float v1, v0, v1 */
		 /* if-nez v1, :cond_0 */
		 /* const v0, 0x3fd9cd60 */
		 /* .line 33 */
		 /* .local v0, "s":F */
	 } // :cond_0
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* sub-float/2addr p1, v1 */
	 /* .line 34 */
	 /* mul-float v2, p1, p1 */
	 /* add-float v3, v0, v1 */
	 /* mul-float/2addr v3, p1 */
	 /* add-float/2addr v3, v0 */
	 /* mul-float/2addr v2, v3 */
	 /* add-float/2addr v2, v1 */
} // .end method
