public class miui.android.view.animation.BounceEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "BounceEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.BounceEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x3f000000 # 0.5f */
		 /* cmpg-float v1, p1, v0 */
		 /* const/high16 v2, 0x40000000 # 2.0f */
		 /* if-gez v1, :cond_0 */
		 /* .line 16 */
		 /* new-instance v1, Lmiui/android/view/animation/BounceEaseInInterpolator; */
		 /* invoke-direct {v1}, Lmiui/android/view/animation/BounceEaseInInterpolator;-><init>()V */
		 /* mul-float/2addr v2, p1 */
		 v1 = 		 (( miui.android.view.animation.BounceEaseInInterpolator ) v1 ).getInterpolation ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/view/animation/BounceEaseInInterpolator;->getInterpolation(F)F
		 /* mul-float/2addr v1, v0 */
		 /* .line 18 */
	 } // :cond_0
	 /* new-instance v1, Lmiui/android/view/animation/BounceEaseOutInterpolator; */
	 /* invoke-direct {v1}, Lmiui/android/view/animation/BounceEaseOutInterpolator;-><init>()V */
	 /* mul-float/2addr v2, p1 */
	 /* const/high16 v3, 0x3f800000 # 1.0f */
	 /* sub-float/2addr v2, v3 */
	 v1 = 	 (( miui.android.view.animation.BounceEaseOutInterpolator ) v1 ).getInterpolation ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/view/animation/BounceEaseOutInterpolator;->getInterpolation(F)F
	 /* mul-float/2addr v1, v0 */
	 /* add-float/2addr v1, v0 */
} // .end method
