public class miui.android.view.animation.QuadraticEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "QuadraticEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.QuadraticEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x40000000 # 2.0f */
		 /* mul-float/2addr p1, v0 */
		 /* .line 16 */
		 /* const/high16 v1, 0x3f800000 # 1.0f */
		 /* cmpg-float v2, p1, v1 */
		 /* if-gez v2, :cond_0 */
		 /* .line 17 */
		 /* const/high16 v0, 0x3f000000 # 0.5f */
		 /* mul-float/2addr v0, p1 */
		 /* mul-float/2addr v0, p1 */
		 /* .line 19 */
	 } // :cond_0
	 /* sub-float/2addr p1, v1 */
	 /* .line 20 */
	 /* sub-float v0, p1, v0 */
	 /* mul-float/2addr v0, p1 */
	 /* sub-float/2addr v0, v1 */
	 /* const/high16 v1, -0x41000000 # -0.5f */
	 /* mul-float/2addr v0, v1 */
} // .end method
