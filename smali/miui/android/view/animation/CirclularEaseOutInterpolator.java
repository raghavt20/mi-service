public class miui.android.view.animation.CirclularEaseOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "CirclularEaseOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.CirclularEaseOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 2 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x3f800000 # 1.0f */
		 /* sub-float/2addr p1, v0 */
		 /* .line 16 */
		 /* mul-float v1, p1, p1 */
		 /* sub-float/2addr v0, v1 */
		 /* float-to-double v0, v0 */
		 java.lang.Math .sqrt ( v0,v1 );
		 /* move-result-wide v0 */
		 /* double-to-float v0, v0 */
	 } // .end method
