public class miui.android.view.animation.ExponentialEaseOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "ExponentialEaseOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.ExponentialEaseOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x3f800000 # 1.0f */
		 /* cmpl-float v1, p1, v0 */
		 /* if-nez v1, :cond_0 */
	 } // :cond_0
	 /* const/high16 v0, -0x3ee00000 # -10.0f */
	 /* mul-float/2addr v0, p1 */
	 /* float-to-double v0, v0 */
	 /* const-wide/high16 v2, 0x4000000000000000L # 2.0 */
	 java.lang.Math .pow ( v2,v3,v0,v1 );
	 /* move-result-wide v0 */
	 /* neg-double v0, v0 */
	 /* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
	 /* add-double/2addr v0, v2 */
	 /* double-to-float v0, v0 */
} // :goto_0
} // .end method
