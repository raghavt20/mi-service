public class miui.android.view.animation.ExponentialEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "ExponentialEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.ExponentialEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 5 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* cmpl-float v1, p1, v0 */
		 /* if-nez v1, :cond_0 */
		 /* .line 16 */
		 /* .line 18 */
	 } // :cond_0
	 /* const/high16 v0, 0x3f800000 # 1.0f */
	 /* cmpl-float v1, p1, v0 */
	 /* if-nez v1, :cond_1 */
	 /* .line 19 */
	 /* .line 22 */
} // :cond_1
/* const/high16 v1, 0x40000000 # 2.0f */
/* mul-float/2addr p1, v1 */
/* .line 23 */
/* cmpg-float v1, p1, v0 */
/* const/high16 v2, 0x3f000000 # 0.5f */
/* const-wide/high16 v3, 0x4000000000000000L # 2.0 */
/* if-gez v1, :cond_2 */
/* .line 24 */
/* const/high16 v1, 0x41200000 # 10.0f */
/* sub-float v0, p1, v0 */
/* mul-float/2addr v0, v1 */
/* float-to-double v0, v0 */
java.lang.Math .pow ( v3,v4,v0,v1 );
/* move-result-wide v0 */
/* double-to-float v0, v0 */
/* mul-float/2addr v0, v2 */
/* .line 27 */
} // :cond_2
/* sub-float/2addr p1, v0 */
/* .line 28 */
/* const/high16 v0, -0x3ee00000 # -10.0f */
/* mul-float/2addr v0, p1 */
/* float-to-double v0, v0 */
java.lang.Math .pow ( v3,v4,v0,v1 );
/* move-result-wide v0 */
/* neg-double v0, v0 */
/* add-double/2addr v0, v3 */
/* double-to-float v0, v0 */
/* mul-float/2addr v0, v2 */
} // .end method
