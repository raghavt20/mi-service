public class miui.android.view.animation.BackEaseInInterpolator implements android.view.animation.Interpolator {
	 /* .source "BackEaseInInterpolator.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private final Float mOvershot;
	 /* # direct methods */
	 public miui.android.view.animation.BackEaseInInterpolator ( ) {
		 /* .locals 1 */
		 /* .line 20 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, v0}, Lmiui/android/view/animation/BackEaseInInterpolator;-><init>(F)V */
		 /* .line 21 */
		 return;
	 } // .end method
	 public miui.android.view.animation.BackEaseInInterpolator ( ) {
		 /* .locals 0 */
		 /* .param p1, "overshot" # F */
		 /* .line 26 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 /* iput p1, p0, Lmiui/android/view/animation/BackEaseInInterpolator;->mOvershot:F */
		 /* .line 28 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 3 */
		 /* .param p1, "t" # F */
		 /* .line 32 */
		 /* iget v0, p0, Lmiui/android/view/animation/BackEaseInInterpolator;->mOvershot:F */
		 int v1 = 0; // const/4 v1, 0x0
		 /* cmpl-float v1, v0, v1 */
		 /* if-nez v1, :cond_0 */
		 /* const v0, 0x3fd9cd60 */
		 /* .line 33 */
		 /* .local v0, "s":F */
	 } // :cond_0
	 /* mul-float v1, p1, p1 */
	 /* const/high16 v2, 0x3f800000 # 1.0f */
	 /* add-float/2addr v2, v0 */
	 /* mul-float/2addr v2, p1 */
	 /* sub-float/2addr v2, v0 */
	 /* mul-float/2addr v1, v2 */
} // .end method
