public class miui.android.view.animation.QuarticEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "QuarticEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.QuarticEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 2 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const/high16 v0, 0x40000000 # 2.0f */
		 /* mul-float/2addr p1, v0 */
		 /* .line 16 */
		 /* const/high16 v1, 0x3f800000 # 1.0f */
		 /* cmpg-float v1, p1, v1 */
		 /* if-gez v1, :cond_0 */
		 /* .line 17 */
		 /* const/high16 v0, 0x3f000000 # 0.5f */
		 /* mul-float/2addr v0, p1 */
		 /* mul-float/2addr v0, p1 */
		 /* mul-float/2addr v0, p1 */
		 /* mul-float/2addr v0, p1 */
		 /* .line 19 */
	 } // :cond_0
	 /* sub-float/2addr p1, v0 */
	 /* .line 20 */
	 /* mul-float v1, p1, p1 */
	 /* mul-float/2addr v1, p1 */
	 /* mul-float/2addr v1, p1 */
	 /* sub-float/2addr v1, v0 */
	 /* const/high16 v0, -0x41000000 # -0.5f */
	 /* mul-float/2addr v1, v0 */
} // .end method
