public class miui.android.view.animation.SineEaseInOutInterpolator implements android.view.animation.Interpolator {
	 /* .source "SineEaseInOutInterpolator.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.view.animation.SineEaseInOutInterpolator ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Float getInterpolation ( Float p0 ) {
		 /* .locals 4 */
		 /* .param p1, "t" # F */
		 /* .line 15 */
		 /* const-wide v0, 0x400921fb54442d18L # Math.PI */
		 /* float-to-double v2, p1 */
		 /* mul-double/2addr v2, v0 */
		 java.lang.Math .cos ( v2,v3 );
		 /* move-result-wide v0 */
		 /* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
		 /* sub-double/2addr v0, v2 */
		 /* double-to-float v0, v0 */
		 /* const/high16 v1, -0x41000000 # -0.5f */
		 /* mul-float/2addr v0, v1 */
	 } // .end method
