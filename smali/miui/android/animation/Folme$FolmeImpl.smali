.class Lmiui/android/animation/Folme$FolmeImpl;
.super Ljava/lang/Object;
.source "Folme.java"

# interfaces
.implements Lmiui/android/animation/IFolme;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/Folme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FolmeImpl"
.end annotation


# instance fields
.field private mHover:Lmiui/android/animation/IHoverStyle;

.field private mState:Lmiui/android/animation/IStateStyle;

.field private mTargets:[Lmiui/android/animation/IAnimTarget;

.field private mTouch:Lmiui/android/animation/ITouchStyle;

.field private mVisible:Lmiui/android/animation/IVisibleStyle;


# direct methods
.method private varargs constructor <init>([Lmiui/android/animation/IAnimTarget;)V
    .locals 1
    .param p1, "target"    # [Lmiui/android/animation/IAnimTarget;

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTargets:[Lmiui/android/animation/IAnimTarget;

    .line 113
    const/4 v0, 0x0

    invoke-static {v0}, Lmiui/android/animation/Folme;->access$000(Z)V

    .line 114
    return-void
.end method

.method synthetic constructor <init>([Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/Folme$1;)V
    .locals 0
    .param p1, "x0"    # [Lmiui/android/animation/IAnimTarget;
    .param p2, "x1"    # Lmiui/android/animation/Folme$1;

    .line 99
    invoke-direct {p0, p1}, Lmiui/android/animation/Folme$FolmeImpl;-><init>([Lmiui/android/animation/IAnimTarget;)V

    return-void
.end method


# virtual methods
.method clean()V
    .locals 1

    .line 117
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTouch:Lmiui/android/animation/ITouchStyle;

    if-eqz v0, :cond_0

    .line 118
    invoke-interface {v0}, Lmiui/android/animation/ITouchStyle;->clean()V

    .line 120
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mVisible:Lmiui/android/animation/IVisibleStyle;

    if-eqz v0, :cond_1

    .line 121
    invoke-interface {v0}, Lmiui/android/animation/IVisibleStyle;->clean()V

    .line 123
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mState:Lmiui/android/animation/IStateStyle;

    if-eqz v0, :cond_2

    .line 124
    invoke-interface {v0}, Lmiui/android/animation/IStateStyle;->clean()V

    .line 126
    :cond_2
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mHover:Lmiui/android/animation/IHoverStyle;

    if-eqz v0, :cond_3

    .line 127
    invoke-interface {v0}, Lmiui/android/animation/IHoverStyle;->clean()V

    .line 129
    :cond_3
    return-void
.end method

.method end()V
    .locals 3

    .line 132
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTouch:Lmiui/android/animation/ITouchStyle;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 133
    new-array v2, v1, [Ljava/lang/Object;

    invoke-interface {v0, v2}, Lmiui/android/animation/ITouchStyle;->end([Ljava/lang/Object;)V

    .line 135
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mVisible:Lmiui/android/animation/IVisibleStyle;

    if-eqz v0, :cond_1

    .line 136
    new-array v2, v1, [Ljava/lang/Object;

    invoke-interface {v0, v2}, Lmiui/android/animation/IVisibleStyle;->end([Ljava/lang/Object;)V

    .line 138
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mState:Lmiui/android/animation/IStateStyle;

    if-eqz v0, :cond_2

    .line 139
    new-array v2, v1, [Ljava/lang/Object;

    invoke-interface {v0, v2}, Lmiui/android/animation/IStateStyle;->end([Ljava/lang/Object;)V

    .line 141
    :cond_2
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mHover:Lmiui/android/animation/IHoverStyle;

    if-eqz v0, :cond_3

    .line 142
    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lmiui/android/animation/IHoverStyle;->end([Ljava/lang/Object;)V

    .line 144
    :cond_3
    return-void
.end method

.method public hover()Lmiui/android/animation/IHoverStyle;
    .locals 2

    .line 148
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mHover:Lmiui/android/animation/IHoverStyle;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Lmiui/android/animation/controller/FolmeHover;

    iget-object v1, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTargets:[Lmiui/android/animation/IAnimTarget;

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeHover;-><init>([Lmiui/android/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mHover:Lmiui/android/animation/IHoverStyle;

    .line 151
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mHover:Lmiui/android/animation/IHoverStyle;

    return-object v0
.end method

.method public state()Lmiui/android/animation/IStateStyle;
    .locals 1

    .line 175
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mState:Lmiui/android/animation/IStateStyle;

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTargets:[Lmiui/android/animation/IAnimTarget;

    invoke-static {v0}, Lmiui/android/animation/controller/StateComposer;->composeStyle([Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/controller/IFolmeStateStyle;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mState:Lmiui/android/animation/IStateStyle;

    .line 178
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mState:Lmiui/android/animation/IStateStyle;

    return-object v0
.end method

.method public touch()Lmiui/android/animation/ITouchStyle;
    .locals 2

    .line 156
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTouch:Lmiui/android/animation/ITouchStyle;

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Lmiui/android/animation/controller/FolmeTouch;

    iget-object v1, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTargets:[Lmiui/android/animation/IAnimTarget;

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeTouch;-><init>([Lmiui/android/animation/IAnimTarget;)V

    .line 158
    .local v0, "touch":Lmiui/android/animation/controller/FolmeTouch;
    new-instance v1, Lmiui/android/animation/controller/FolmeFont;

    invoke-direct {v1}, Lmiui/android/animation/controller/FolmeFont;-><init>()V

    .line 159
    .local v1, "fontStyle":Lmiui/android/animation/controller/FolmeFont;
    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/FolmeTouch;->setFontStyle(Lmiui/android/animation/controller/FolmeFont;)V

    .line 160
    iput-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTouch:Lmiui/android/animation/ITouchStyle;

    .line 162
    .end local v0    # "touch":Lmiui/android/animation/controller/FolmeTouch;
    .end local v1    # "fontStyle":Lmiui/android/animation/controller/FolmeFont;
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTouch:Lmiui/android/animation/ITouchStyle;

    return-object v0
.end method

.method public visible()Lmiui/android/animation/IVisibleStyle;
    .locals 2

    .line 167
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mVisible:Lmiui/android/animation/IVisibleStyle;

    if-nez v0, :cond_0

    .line 168
    new-instance v0, Lmiui/android/animation/controller/FolmeVisible;

    iget-object v1, p0, Lmiui/android/animation/Folme$FolmeImpl;->mTargets:[Lmiui/android/animation/IAnimTarget;

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeVisible;-><init>([Lmiui/android/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mVisible:Lmiui/android/animation/IVisibleStyle;

    .line 170
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/Folme$FolmeImpl;->mVisible:Lmiui/android/animation/IVisibleStyle;

    return-object v0
.end method
