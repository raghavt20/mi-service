.class public interface abstract Lmiui/android/animation/IVisibleStyle;
.super Ljava/lang/Object;
.source "IVisibleStyle.java"

# interfaces
.implements Lmiui/android/animation/IStateContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/IVisibleStyle$VisibleType;
    }
.end annotation


# virtual methods
.method public varargs abstract hide([Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract setAlpha(F[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
.end method

.method public abstract setBound(IIII)Lmiui/android/animation/IVisibleStyle;
.end method

.method public abstract setFlags(J)Lmiui/android/animation/IVisibleStyle;
.end method

.method public abstract setHide()Lmiui/android/animation/IVisibleStyle;
.end method

.method public abstract setMove(II)Lmiui/android/animation/IVisibleStyle;
.end method

.method public varargs abstract setMove(II[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
.end method

.method public varargs abstract setScale(F[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
.end method

.method public abstract setShow()Lmiui/android/animation/IVisibleStyle;
.end method

.method public abstract setShowDelay(J)Lmiui/android/animation/IVisibleStyle;
.end method

.method public varargs abstract show([Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public abstract useAutoAlpha(Z)Lmiui/android/animation/IVisibleStyle;
.end method
