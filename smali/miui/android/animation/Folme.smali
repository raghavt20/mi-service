.class public Lmiui/android/animation/Folme;
.super Ljava/lang/Object;
.source "Folme.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/Folme$FolmeImpl;,
        Lmiui/android/animation/Folme$FontWeight;,
        Lmiui/android/animation/Folme$FontType;
    }
.end annotation


# static fields
.field private static final DELAY_TIME:J = 0x4e20L

.field private static final MSG_TARGET:I = 0x1

.field private static final sImplMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lmiui/android/animation/IAnimTarget;",
            "Lmiui/android/animation/Folme$FolmeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMainHandler:Landroid/os/Handler;

.field private static sTimeRatio:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 39
    new-instance v0, Lmiui/android/animation/Folme$1;

    invoke-direct {v0}, Lmiui/android/animation/Folme$1;-><init>()V

    invoke-static {v0}, Lmiui/android/animation/internal/ThreadPoolUtil;->post(Ljava/lang/Runnable;)V

    .line 47
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lmiui/android/animation/Folme;->sTimeRatio:Ljava/util/concurrent/atomic/AtomicReference;

    .line 89
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 371
    new-instance v0, Lmiui/android/animation/Folme$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/android/animation/Folme$2;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lmiui/android/animation/Folme;->sMainHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .line 36
    invoke-static {p0}, Lmiui/android/animation/Folme;->sendToTargetMessage(Z)V

    return-void
.end method

.method static synthetic access$200()V
    .locals 0

    .line 36
    invoke-static {}, Lmiui/android/animation/Folme;->clearTargets()V

    return-void
.end method

.method public static varargs clean([Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 244
    .local p0, "targetObjects":[Ljava/lang/Object;, "[TT;"
    invoke-static {p0}, Lmiui/android/animation/utils/CommonUtils;->isArrayEmpty([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/IAnimTarget;

    .line 246
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    invoke-static {v1}, Lmiui/android/animation/Folme;->cleanAnimTarget(Lmiui/android/animation/IAnimTarget;)V

    .line 247
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    goto :goto_0

    :cond_0
    goto :goto_2

    .line 249
    :cond_1
    array-length v0, p0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    aget-object v2, p0, v1

    .line 250
    .local v2, "targetObject":Ljava/lang/Object;, "TT;"
    invoke-static {v2}, Lmiui/android/animation/Folme;->doClean(Ljava/lang/Object;)V

    .line 249
    .end local v2    # "targetObject":Ljava/lang/Object;, "TT;"
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 253
    :cond_2
    :goto_2
    return-void
.end method

.method private static cleanAnimTarget(Lmiui/android/animation/IAnimTarget;)V
    .locals 2
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 281
    if-eqz p0, :cond_0

    .line 282
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/Folme$FolmeImpl;

    .line 283
    .local v0, "impl":Lmiui/android/animation/Folme$FolmeImpl;
    iget-object v1, p0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v1}, Lmiui/android/animation/internal/AnimManager;->clear()V

    .line 284
    if-eqz v0, :cond_0

    .line 285
    invoke-virtual {v0}, Lmiui/android/animation/Folme$FolmeImpl;->clean()V

    .line 288
    .end local v0    # "impl":Lmiui/android/animation/Folme$FolmeImpl;
    :cond_0
    return-void
.end method

.method private static clearTargetMessage()V
    .locals 3

    .line 398
    sget-object v0, Lmiui/android/animation/Folme;->sMainHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 399
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 401
    :cond_0
    return-void
.end method

.method private static clearTargets()V
    .locals 4

    .line 361
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/IAnimTarget;

    .line 362
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Lmiui/android/animation/IAnimTarget;->hasFlags(J)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    const/4 v3, 0x0

    new-array v3, v3, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 363
    :cond_0
    filled-new-array {v1}, [Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    invoke-static {v2}, Lmiui/android/animation/Folme;->clean([Ljava/lang/Object;)V

    .line 365
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    :cond_1
    goto :goto_0

    .line 366
    :cond_2
    return-void
.end method

.method private static doClean(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)V"
        }
    .end annotation

    .line 276
    .local p0, "targetObject":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 277
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    invoke-static {v0}, Lmiui/android/animation/Folme;->cleanAnimTarget(Lmiui/android/animation/IAnimTarget;)V

    .line 278
    return-void
.end method

.method public static varargs end([Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)V"
        }
    .end annotation

    .line 256
    .local p0, "targetObjects":[Ljava/lang/Object;, "[TT;"
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p0, v1

    .line 257
    .local v2, "targetObject":Ljava/lang/Object;, "TT;"
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v3

    .line 258
    .local v3, "target":Lmiui/android/animation/IAnimTarget;
    if-nez v3, :cond_0

    .line 259
    goto :goto_1

    .line 261
    :cond_0
    sget-object v4, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/android/animation/Folme$FolmeImpl;

    .line 262
    .local v4, "impl":Lmiui/android/animation/Folme$FolmeImpl;
    if-eqz v4, :cond_1

    .line 263
    invoke-virtual {v4}, Lmiui/android/animation/Folme$FolmeImpl;->end()V

    .line 256
    .end local v2    # "targetObject":Ljava/lang/Object;, "TT;"
    .end local v3    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v4    # "impl":Lmiui/android/animation/Folme$FolmeImpl;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    :cond_2
    return-void
.end method

.method private static fillTargetArrayAndGetImpl([Landroid/view/View;[Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/Folme$FolmeImpl;
    .locals 5
    .param p0, "views"    # [Landroid/view/View;
    .param p1, "targets"    # [Lmiui/android/animation/IAnimTarget;

    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "createImpl":Z
    const/4 v1, 0x0

    .line 230
    .local v1, "impl":Lmiui/android/animation/Folme$FolmeImpl;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    .line 231
    aget-object v3, p0, v2

    sget-object v4, Lmiui/android/animation/ViewTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    invoke-static {v3, v4}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v3

    aput-object v3, p1, v2

    .line 232
    sget-object v3, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    aget-object v4, p1, v2

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/android/animation/Folme$FolmeImpl;

    .line 233
    .local v3, "cur":Lmiui/android/animation/Folme$FolmeImpl;
    if-nez v1, :cond_0

    .line 234
    move-object v1, v3

    goto :goto_1

    .line 235
    :cond_0
    if-eq v1, v3, :cond_1

    .line 236
    const/4 v0, 0x1

    .line 230
    .end local v3    # "cur":Lmiui/android/animation/Folme$FolmeImpl;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 239
    .end local v2    # "i":I
    :cond_2
    if-eqz v0, :cond_3

    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    move-object v2, v1

    :goto_2
    return-object v2
.end method

.method public static getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lmiui/android/animation/ITargetCreator<",
            "TT;>;)",
            "Lmiui/android/animation/IAnimTarget;"
        }
    .end annotation

    .line 295
    .local p0, "targetObject":Ljava/lang/Object;, "TT;"
    .local p1, "creator":Lmiui/android/animation/ITargetCreator;, "Lmiui/android/animation/ITargetCreator<TT;>;"
    const/4 v0, 0x0

    if-nez p0, :cond_0

    .line 296
    return-object v0

    .line 298
    :cond_0
    instance-of v1, p0, Lmiui/android/animation/IAnimTarget;

    if-eqz v1, :cond_1

    .line 299
    move-object v0, p0

    check-cast v0, Lmiui/android/animation/IAnimTarget;

    return-object v0

    .line 301
    :cond_1
    sget-object v1, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/IAnimTarget;

    .line 302
    .local v2, "target":Lmiui/android/animation/IAnimTarget;
    invoke-virtual {v2}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v3

    .line 303
    .local v3, "obj":Ljava/lang/Object;
    if-eqz v3, :cond_2

    invoke-virtual {v3, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 304
    return-object v2

    .line 306
    .end local v2    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_2
    goto :goto_0

    .line 307
    :cond_3
    if-eqz p1, :cond_4

    .line 308
    invoke-interface {p1, p0}, Lmiui/android/animation/ITargetCreator;->createTarget(Ljava/lang/Object;)Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    .line 309
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    if-eqz v1, :cond_4

    .line 310
    invoke-static {v1}, Lmiui/android/animation/Folme;->useAt(Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/IFolme;

    .line 311
    return-object v1

    .line 314
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    :cond_4
    return-object v0
.end method

.method public static getTargetById(I)Lmiui/android/animation/IAnimTarget;
    .locals 3
    .param p0, "id"    # I

    .line 352
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/IAnimTarget;

    .line 353
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    iget v2, v1, Lmiui/android/animation/IAnimTarget;->id:I

    if-ne v2, p0, :cond_0

    .line 354
    return-object v1

    .line 356
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    :cond_0
    goto :goto_0

    .line 357
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getTargets()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/IAnimTarget;",
            ">;"
        }
    .end annotation

    .line 92
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static getTargets(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/IAnimTarget;",
            ">;)V"
        }
    .end annotation

    .line 339
    .local p0, "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;"
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/IAnimTarget;

    .line 340
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->isValid()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Lmiui/android/animation/IAnimTarget;->hasFlags(J)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    const/4 v3, 0x0

    new-array v3, v3, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 343
    :cond_0
    invoke-interface {p0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 341
    :cond_1
    :goto_1
    filled-new-array {v1}, [Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    invoke-static {v2}, Lmiui/android/animation/Folme;->clean([Ljava/lang/Object;)V

    .line 345
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    :goto_2
    goto :goto_0

    .line 346
    :cond_2
    return-void
.end method

.method public static getTimeRatio()F
    .locals 1

    .line 60
    sget-object v0, Lmiui/android/animation/Folme;->sTimeRatio:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public static getValueTarget(Ljava/lang/Object;)Lmiui/android/animation/ValueTarget;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lmiui/android/animation/ValueTarget;"
        }
    .end annotation

    .line 291
    .local p0, "targetObject":Ljava/lang/Object;, "TT;"
    sget-object v0, Lmiui/android/animation/ValueTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    invoke-static {p0, v0}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/ValueTarget;

    return-object v0
.end method

.method public static isInDraggingState(Landroid/view/View;)Z
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .line 332
    const v0, 0x100b0005

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static onListViewTouchEvent(Landroid/widget/AbsListView;Landroid/view/MotionEvent;)V
    .locals 1
    .param p0, "listView"    # Landroid/widget/AbsListView;
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 269
    invoke-static {p0}, Lmiui/android/animation/controller/FolmeTouch;->getListViewTouchListener(Landroid/widget/AbsListView;)Lmiui/android/animation/controller/ListViewTouchListener;

    move-result-object v0

    .line 270
    .local v0, "listener":Lmiui/android/animation/controller/ListViewTouchListener;
    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0, p0, p1}, Lmiui/android/animation/controller/ListViewTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 273
    :cond_0
    return-void
.end method

.method public static post(Ljava/lang/Object;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "task"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .line 64
    .local p0, "targetObject":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 65
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v0, p1}, Lmiui/android/animation/IAnimTarget;->post(Ljava/lang/Runnable;)V

    .line 68
    :cond_0
    return-void
.end method

.method private static sendToTargetMessage(Z)V
    .locals 4
    .param p0, "fromAuto"    # Z

    .line 384
    invoke-static {}, Lmiui/android/animation/Folme;->clearTargetMessage()V

    .line 385
    if-eqz p0, :cond_0

    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/IAnimTarget;

    .line 387
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exist target:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 388
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    goto :goto_0

    .line 390
    :cond_0
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 391
    sget-object v0, Lmiui/android/animation/Folme;->sMainHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 393
    :cond_1
    invoke-static {}, Lmiui/android/animation/Folme;->clearTargetMessage()V

    .line 395
    :goto_1
    return-void
.end method

.method public static setAnimPlayRatio(F)V
    .locals 2
    .param p0, "ratio"    # F

    .line 56
    sget-object v0, Lmiui/android/animation/Folme;->sTimeRatio:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method public static setDraggingState(Landroid/view/View;Z)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "isDragging"    # Z

    .line 321
    const v0, 0x100b0005

    if-eqz p1, :cond_0

    .line 322
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 324
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 326
    :goto_0
    return-void
.end method

.method public static useAt(Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/IFolme;
    .locals 5
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 195
    sget-object v0, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/Folme$FolmeImpl;

    .line 196
    .local v1, "mc":Lmiui/android/animation/Folme$FolmeImpl;
    if-nez v1, :cond_0

    .line 197
    new-instance v2, Lmiui/android/animation/Folme$FolmeImpl;

    filled-new-array {p0}, [Lmiui/android/animation/IAnimTarget;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lmiui/android/animation/Folme$FolmeImpl;-><init>([Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/Folme$1;)V

    move-object v1, v2

    .line 198
    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/Folme$FolmeImpl;

    .line 199
    .local v0, "prev":Lmiui/android/animation/Folme$FolmeImpl;
    if-eqz v0, :cond_0

    .line 200
    move-object v1, v0

    .line 203
    .end local v0    # "prev":Lmiui/android/animation/Folme$FolmeImpl;
    :cond_0
    return-object v1
.end method

.method public static varargs useAt([Landroid/view/View;)Lmiui/android/animation/IFolme;
    .locals 6
    .param p0, "views"    # [Landroid/view/View;

    .line 207
    array-length v0, p0

    if-eqz v0, :cond_3

    .line 210
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 211
    aget-object v0, p0, v1

    sget-object v1, Lmiui/android/animation/ViewTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    invoke-static {v0, v1}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-static {v0}, Lmiui/android/animation/Folme;->useAt(Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/IFolme;

    move-result-object v0

    return-object v0

    .line 213
    :cond_0
    array-length v0, p0

    new-array v0, v0, [Lmiui/android/animation/IAnimTarget;

    .line 214
    .local v0, "targets":[Lmiui/android/animation/IAnimTarget;
    invoke-static {p0, v0}, Lmiui/android/animation/Folme;->fillTargetArrayAndGetImpl([Landroid/view/View;[Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/Folme$FolmeImpl;

    move-result-object v2

    .line 215
    .local v2, "impl":Lmiui/android/animation/Folme$FolmeImpl;
    if-nez v2, :cond_2

    .line 216
    new-instance v3, Lmiui/android/animation/Folme$FolmeImpl;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lmiui/android/animation/Folme$FolmeImpl;-><init>([Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/Folme$1;)V

    move-object v2, v3

    .line 217
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v0, v1

    .line 218
    .local v4, "target":Lmiui/android/animation/IAnimTarget;
    sget-object v5, Lmiui/android/animation/Folme;->sImplMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/android/animation/Folme$FolmeImpl;

    .line 219
    .local v5, "prevImpl":Lmiui/android/animation/Folme$FolmeImpl;
    if-eqz v5, :cond_1

    .line 220
    invoke-virtual {v5}, Lmiui/android/animation/Folme$FolmeImpl;->clean()V

    .line 217
    .end local v4    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v5    # "prevImpl":Lmiui/android/animation/Folme$FolmeImpl;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 224
    :cond_2
    return-object v2

    .line 208
    .end local v0    # "targets":[Lmiui/android/animation/IAnimTarget;
    .end local v2    # "impl":Lmiui/android/animation/Folme$FolmeImpl;
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "useAt can not be applied to empty views array"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static useSystemAnimatorDurationScale(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 50
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "animator_duration_scale"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    .line 52
    .local v0, "scale":F
    sget-object v1, Lmiui/android/animation/Folme;->sTimeRatio:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public static varargs useValue([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 3
    .param p0, "targetObj"    # [Ljava/lang/Object;

    .line 184
    array-length v0, p0

    if-lez v0, :cond_0

    .line 185
    const/4 v0, 0x0

    aget-object v0, p0, v0

    sget-object v1, Lmiui/android/animation/ValueTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    invoke-static {v0, v1}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-static {v0}, Lmiui/android/animation/Folme;->useAt(Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/IFolme;

    move-result-object v0

    .local v0, "folme":Lmiui/android/animation/IFolme;
    goto :goto_0

    .line 187
    .end local v0    # "folme":Lmiui/android/animation/IFolme;
    :cond_0
    new-instance v0, Lmiui/android/animation/ValueTarget;

    invoke-direct {v0}, Lmiui/android/animation/ValueTarget;-><init>()V

    .line 188
    .local v0, "target":Lmiui/android/animation/ValueTarget;
    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/ValueTarget;->setFlags(J)V

    .line 189
    invoke-static {v0}, Lmiui/android/animation/Folme;->useAt(Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/IFolme;

    move-result-object v1

    move-object v0, v1

    .line 191
    .local v0, "folme":Lmiui/android/animation/IFolme;
    :goto_0
    invoke-interface {v0}, Lmiui/android/animation/IFolme;->state()Lmiui/android/animation/IStateStyle;

    move-result-object v1

    return-object v1
.end method

.method public static useVarFontAt(Landroid/widget/TextView;II)Lmiui/android/animation/IVarFontStyle;
    .locals 1
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "fontType"    # I
    .param p2, "initFontWeight"    # I

    .line 96
    new-instance v0, Lmiui/android/animation/controller/FolmeFont;

    invoke-direct {v0}, Lmiui/android/animation/controller/FolmeFont;-><init>()V

    invoke-virtual {v0, p0, p1, p2}, Lmiui/android/animation/controller/FolmeFont;->useAt(Landroid/widget/TextView;II)Lmiui/android/animation/IVarFontStyle;

    move-result-object v0

    return-object v0
.end method
