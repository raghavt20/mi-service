.class public final enum Lmiui/android/animation/IHoverStyle$HoverEffect;
.super Ljava/lang/Enum;
.source "IHoverStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/IHoverStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HoverEffect"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiui/android/animation/IHoverStyle$HoverEffect;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/android/animation/IHoverStyle$HoverEffect;

.field public static final enum FLOATED:Lmiui/android/animation/IHoverStyle$HoverEffect;

.field public static final enum FLOATED_WRAPPED:Lmiui/android/animation/IHoverStyle$HoverEffect;

.field public static final enum NORMAL:Lmiui/android/animation/IHoverStyle$HoverEffect;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 21
    new-instance v0, Lmiui/android/animation/IHoverStyle$HoverEffect;

    const-string v1, "NORMAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/IHoverStyle$HoverEffect;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/android/animation/IHoverStyle$HoverEffect;->NORMAL:Lmiui/android/animation/IHoverStyle$HoverEffect;

    new-instance v1, Lmiui/android/animation/IHoverStyle$HoverEffect;

    const-string v2, "FLOATED"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lmiui/android/animation/IHoverStyle$HoverEffect;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    new-instance v2, Lmiui/android/animation/IHoverStyle$HoverEffect;

    const-string v3, "FLOATED_WRAPPED"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lmiui/android/animation/IHoverStyle$HoverEffect;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED_WRAPPED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    .line 20
    filled-new-array {v0, v1, v2}, [Lmiui/android/animation/IHoverStyle$HoverEffect;

    move-result-object v0

    sput-object v0, Lmiui/android/animation/IHoverStyle$HoverEffect;->$VALUES:[Lmiui/android/animation/IHoverStyle$HoverEffect;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/android/animation/IHoverStyle$HoverEffect;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 20
    const-class v0, Lmiui/android/animation/IHoverStyle$HoverEffect;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/IHoverStyle$HoverEffect;

    return-object v0
.end method

.method public static values()[Lmiui/android/animation/IHoverStyle$HoverEffect;
    .locals 1

    .line 20
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverEffect;->$VALUES:[Lmiui/android/animation/IHoverStyle$HoverEffect;

    invoke-virtual {v0}, [Lmiui/android/animation/IHoverStyle$HoverEffect;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/IHoverStyle$HoverEffect;

    return-object v0
.end method
