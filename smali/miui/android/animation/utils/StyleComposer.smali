.class public Lmiui/android/animation/utils/StyleComposer;
.super Ljava/lang/Object;
.source "StyleComposer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/utils/StyleComposer$IInterceptor;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "StyleComposer"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs compose(Ljava/lang/Class;Lmiui/android/animation/utils/StyleComposer$IInterceptor;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "interceptor"    # Lmiui/android/animation/utils/StyleComposer$IInterceptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lmiui/android/animation/utils/StyleComposer$IInterceptor;",
            "[TT;)TT;"
        }
    .end annotation

    .line 31
    .local p0, "interfaceClz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "styles":[Ljava/lang/Object;, "[TT;"
    new-instance v0, Lmiui/android/animation/utils/StyleComposer$1;

    invoke-direct {v0, p1, p2, p0}, Lmiui/android/animation/utils/StyleComposer$1;-><init>(Lmiui/android/animation/utils/StyleComposer$IInterceptor;[Ljava/lang/Object;Ljava/lang/Class;)V

    .line 52
    .local v0, "handler":Ljava/lang/reflect/InvocationHandler;
    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    filled-new-array {p0}, [Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    .line 53
    .local v1, "proxy":Ljava/lang/Object;
    invoke-virtual {p0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .line 56
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method

.method public static varargs compose(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;[TT;)TT;"
        }
    .end annotation

    .line 26
    .local p0, "interfaceClz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p1, "styles":[Ljava/lang/Object;, "[TT;"
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lmiui/android/animation/utils/StyleComposer;->compose(Ljava/lang/Class;Lmiui/android/animation/utils/StyleComposer$IInterceptor;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
