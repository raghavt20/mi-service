public class miui.android.animation.utils.EaseManager$InterpolateEaseStyle extends miui.android.animation.utils.EaseManager$EaseStyle {
	 /* .source "EaseManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/utils/EaseManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "InterpolateEaseStyle" */
} // .end annotation
/* # instance fields */
public Long duration;
/* # direct methods */
public miui.android.animation.utils.EaseManager$InterpolateEaseStyle ( ) {
/* .locals 2 */
/* .param p1, "s" # I */
/* .param p2, "factors" # [F */
/* .line 211 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/utils/EaseManager$EaseStyle;-><init>(I[F)V */
/* .line 208 */
/* const-wide/16 v0, 0x12c */
/* iput-wide v0, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J */
/* .line 212 */
return;
} // .end method
/* # virtual methods */
public miui.android.animation.utils.EaseManager$InterpolateEaseStyle setDuration ( Long p0 ) {
/* .locals 0 */
/* .param p1, "d" # J */
/* .line 215 */
/* iput-wide p1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J */
/* .line 216 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 221 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "InterpolateEaseStyle{style="; // const-string v1, "InterpolateEaseStyle{style="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", duration="; // const-string v1, ", duration="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", factors="; // const-string v1, ", factors="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.factors;
/* .line 224 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 221 */
} // .end method
