public class miui.android.animation.utils.LinkNode {
	 /* .source "LinkNode.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Lmiui/android/animation/utils/LinkNode;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # instance fields */
public miui.android.animation.utils.LinkNode next;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "TT;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.utils.LinkNode ( ) {
/* .locals 0 */
/* .line 3 */
/* .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void addToTail ( miui.android.animation.utils.LinkNode p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)V" */
/* } */
} // .end annotation
/* .line 8 */
/* .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
/* .local p1, "node":Lmiui/android/animation/utils/LinkNode;, "TT;" */
/* move-object v0, p0 */
/* .line 10 */
/* .local v0, "head":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
} // :goto_0
/* if-ne v0, p1, :cond_0 */
/* .line 11 */
return;
/* .line 13 */
} // :cond_0
v1 = this.next;
/* if-nez v1, :cond_1 */
/* .line 14 */
/* nop */
/* .line 18 */
this.next = p1;
/* .line 19 */
return;
/* .line 16 */
} // :cond_1
v0 = this.next;
/* .line 17 */
} // .end method
public miui.android.animation.utils.LinkNode destroy ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TT;" */
/* } */
} // .end annotation
/* .line 30 */
/* .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
/* nop */
} // :cond_0
(( miui.android.animation.utils.LinkNode ) p0 ).remove ( ); // invoke-virtual {p0}, Lmiui/android/animation/utils/LinkNode;->remove()Lmiui/android/animation/utils/LinkNode;
/* .line 31 */
/* .local v0, "node":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
/* if-nez v0, :cond_0 */
/* .line 32 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public miui.android.animation.utils.LinkNode remove ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TT;" */
/* } */
} // .end annotation
/* .line 22 */
/* .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
v0 = this.next;
/* .line 23 */
/* .local v0, "node":Lmiui/android/animation/utils/LinkNode;, "TT;" */
int v1 = 0; // const/4 v1, 0x0
this.next = v1;
/* .line 24 */
} // .end method
public Integer size ( ) {
/* .locals 3 */
/* .line 36 */
/* .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 37 */
/* .local v0, "i":I */
/* move-object v1, p0 */
/* .line 38 */
/* .local v1, "head":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;" */
} // :goto_0
v2 = this.next;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 39 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 40 */
v1 = this.next;
/* .line 42 */
} // :cond_0
} // .end method
