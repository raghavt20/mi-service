public abstract class miui.android.animation.utils.EaseManager$EaseStyleDef {
	 /* .source "EaseManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/utils/EaseManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "EaseStyleDef" */
} // .end annotation
/* # static fields */
public static final Integer ACCELERATE;
public static final Integer ACCELERATE_DECELERATE;
public static final Integer ACCELERATE_INTERPOLATOR;
public static final Integer BOUNCE;
public static final Integer BOUNCE_EASE_IN;
public static final Integer BOUNCE_EASE_INOUT;
public static final Integer BOUNCE_EASE_OUT;
public static final Integer CUBIC_IN;
public static final Integer CUBIC_INOUT;
public static final Integer CUBIC_OUT;
public static final Integer DECELERATE;
public static final Integer DURATION;
public static final Integer EXPO_IN;
public static final Integer EXPO_INOUT;
public static final Integer EXPO_OUT;
public static final Integer FRICTION;
public static final Integer LINEAR;
public static final Integer QUAD_IN;
public static final Integer QUAD_INOUT;
public static final Integer QUAD_OUT;
public static final Integer QUART_IN;
public static final Integer QUART_INOUT;
public static final Integer QUART_OUT;
public static final Integer QUINT_IN;
public static final Integer QUINT_INOUT;
public static final Integer QUINT_OUT;
public static final Integer REBOUND;
public static final Integer SIN_IN;
public static final Integer SIN_INOUT;
public static final Integer SIN_OUT;
public static final Integer SPRING;
public static final Integer SPRING_PHY;
public static final Integer STOP;
