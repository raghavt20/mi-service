public class miui.android.animation.utils.VelocityMonitor {
	 /* .source "VelocityMonitor.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long MAX_DELTA;
private static final Integer MAX_RECORD_COUNT;
private static final Long MIN_DELTA;
private static final Long TIME_THRESHOLD;
/* # instance fields */
private java.util.LinkedList mHistory;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/LinkedList<", */
/* "Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private mVelocity;
/* # direct methods */
public miui.android.animation.utils.VelocityMonitor ( ) {
/* .locals 1 */
/* .line 8 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 20 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mHistory = v0;
return;
} // .end method
private void addAndUpdate ( miui.android.animation.utils.VelocityMonitor$MoveRecord p0 ) {
/* .locals 2 */
/* .param p1, "record" # Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 52 */
v0 = this.mHistory;
(( java.util.LinkedList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
/* .line 53 */
v0 = this.mHistory;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* const/16 v1, 0xa */
/* if-le v0, v1, :cond_0 */
/* .line 54 */
v0 = this.mHistory;
int v1 = 0; // const/4 v1, 0x0
(( java.util.LinkedList ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
/* .line 56 */
} // :cond_0
/* invoke-direct {p0}, Lmiui/android/animation/utils/VelocityMonitor;->updateVelocity()V */
/* .line 57 */
return;
} // .end method
private Float calVelocity ( Integer p0, miui.android.animation.utils.VelocityMonitor$MoveRecord p1, miui.android.animation.utils.VelocityMonitor$MoveRecord p2 ) {
/* .locals 29 */
/* .param p1, "idx" # I */
/* .param p2, "lastRecord" # Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .param p3, "lastRecord1" # Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 98 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p2 */
/* move-object/from16 v9, p3 */
v0 = this.values;
/* aget-wide v10, v0, p1 */
/* .line 99 */
/* .local v10, "lastValue":D */
/* iget-wide v12, v8, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;->timeStamp:J */
/* .line 100 */
/* .local v12, "lastTime":J */
v0 = this.values;
/* aget-wide v14, v0, p1 */
/* .line 101 */
/* .local v14, "lastValue1":D */
/* iget-wide v5, v9, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;->timeStamp:J */
/* .line 102 */
/* .local v5, "lastTime1":J */
/* sub-long v16, v12, v5 */
/* move-object/from16 v0, p0 */
/* move-wide v1, v10 */
/* move-wide v3, v14 */
/* move-wide/from16 v18, v5 */
} // .end local v5 # "lastTime1":J
/* .local v18, "lastTime1":J */
/* move-wide/from16 v5, v16 */
v0 = /* invoke-direct/range {v0 ..v6}, Lmiui/android/animation/utils/VelocityMonitor;->getVelocity(DDJ)F */
/* float-to-double v5, v0 */
/* .line 103 */
/* .local v5, "v1":D */
/* const v16, 0x7f7fffff # Float.MAX_VALUE */
/* .line 105 */
/* .local v16, "velocity":F */
int v0 = 0; // const/4 v0, 0x0
/* .line 106 */
/* .local v0, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
v1 = this.mHistory;
v1 = (( java.util.LinkedList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->size()I
/* add-int/lit8 v1, v1, -0x2 */
/* move v3, v1 */
/* .local v3, "i":I */
} // :goto_0
/* const-wide/16 v20, 0x64 */
/* const-wide/16 v22, 0x1e */
/* if-ltz v3, :cond_3 */
/* .line 107 */
v1 = this.mHistory;
(( java.util.LinkedList ) v1 ).get ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* move-object v4, v1 */
/* check-cast v4, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 108 */
} // .end local v0 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .local v4, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* iget-wide v0, v4, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;->timeStamp:J */
/* sub-long v24, v12, v0 */
/* .line 109 */
/* .local v24, "deltaT":J */
/* cmp-long v0, v24, v22 */
/* if-lez v0, :cond_2 */
/* cmp-long v0, v24, v20 */
/* if-gez v0, :cond_2 */
/* .line 110 */
v0 = this.values;
/* aget-wide v26, v0, p1 */
/* move-object/from16 v0, p0 */
/* move-wide v1, v10 */
/* move/from16 v17, v3 */
/* move-object/from16 v28, v4 */
} // .end local v3 # "i":I
} // .end local v4 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .local v17, "i":I */
/* .local v28, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* move-wide/from16 v3, v26 */
/* move-wide v7, v5 */
} // .end local v5 # "v1":D
/* .local v7, "v1":D */
/* move-wide/from16 v5, v24 */
v0 = /* invoke-direct/range {v0 ..v6}, Lmiui/android/animation/utils/VelocityMonitor;->getVelocity(DDJ)F */
/* .line 111 */
/* .local v0, "v2":F */
/* float-to-double v1, v0 */
/* mul-double v5, v7, v1 */
/* const-wide/16 v1, 0x0 */
/* cmpl-double v1, v5, v1 */
/* if-lez v1, :cond_1 */
/* .line 112 */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, v0, v1 */
/* if-lez v1, :cond_0 */
/* float-to-double v1, v0 */
java.lang.Math .max ( v7,v8,v1,v2 );
/* move-result-wide v1 */
} // :cond_0
/* float-to-double v1, v0 */
java.lang.Math .min ( v7,v8,v1,v2 );
/* move-result-wide v1 */
} // :goto_1
/* double-to-float v1, v1 */
/* move/from16 v16, v1 */
/* move-object/from16 v5, v28 */
} // .end local v16 # "velocity":F
/* .local v1, "velocity":F */
/* .line 114 */
} // .end local v1 # "velocity":F
/* .restart local v16 # "velocity":F */
} // :cond_1
/* move/from16 v16, v0 */
/* .line 116 */
/* move-object/from16 v5, v28 */
/* .line 109 */
} // .end local v0 # "v2":F
} // .end local v7 # "v1":D
} // .end local v17 # "i":I
} // .end local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .restart local v3 # "i":I */
/* .restart local v4 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .restart local v5 # "v1":D */
} // :cond_2
/* move/from16 v17, v3 */
/* move-object/from16 v28, v4 */
/* move-wide v7, v5 */
/* .line 106 */
} // .end local v3 # "i":I
} // .end local v4 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
} // .end local v5 # "v1":D
/* .restart local v7 # "v1":D */
/* .restart local v17 # "i":I */
/* .restart local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* add-int/lit8 v3, v17, -0x1 */
/* move-wide v5, v7 */
/* move-object/from16 v0, v28 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p2 */
} // .end local v17 # "i":I
/* .restart local v3 # "i":I */
} // .end local v7 # "v1":D
} // .end local v24 # "deltaT":J
} // .end local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .local v0, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .restart local v5 # "v1":D */
} // :cond_3
/* move/from16 v17, v3 */
/* move-wide v7, v5 */
} // .end local v3 # "i":I
} // .end local v5 # "v1":D
/* .restart local v7 # "v1":D */
/* .restart local v17 # "i":I */
/* move-object v5, v0 */
/* .line 119 */
} // .end local v0 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
} // .end local v17 # "i":I
/* .local v5, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
} // :goto_2
/* const v17, 0x7f7fffff # Float.MAX_VALUE */
/* cmpl-float v0, v16, v17 */
/* if-nez v0, :cond_5 */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 120 */
/* iget-wide v0, v5, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;->timeStamp:J */
/* sub-long v24, v12, v0 */
/* .line 121 */
/* .restart local v24 # "deltaT":J */
/* cmp-long v0, v24, v22 */
/* if-lez v0, :cond_4 */
/* cmp-long v0, v24, v20 */
/* if-gez v0, :cond_4 */
/* .line 122 */
v0 = this.values;
/* aget-wide v3, v0, p1 */
/* move-object/from16 v0, p0 */
/* move-wide v1, v10 */
/* move-object/from16 v28, v5 */
} // .end local v5 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .restart local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* move-wide/from16 v5, v24 */
v16 = /* invoke-direct/range {v0 ..v6}, Lmiui/android/animation/utils/VelocityMonitor;->getVelocity(DDJ)F */
/* .line 121 */
} // .end local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .restart local v5 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
} // :cond_4
/* move-object/from16 v28, v5 */
} // .end local v5 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .restart local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 119 */
} // .end local v24 # "deltaT":J
} // .end local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .restart local v5 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
} // :cond_5
/* move-object/from16 v28, v5 */
/* .line 125 */
} // .end local v5 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
/* .restart local v28 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
} // :goto_3
/* cmpl-float v0, v16, v17 */
/* if-nez v0, :cond_6 */
/* const/16 v16, 0x0 */
/* .line 126 */
} // :cond_6
} // .end method
private void clearVelocity ( ) {
/* .locals 2 */
/* .line 75 */
v0 = this.mVelocity;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 76 */
int v1 = 0; // const/4 v1, 0x0
java.util.Arrays .fill ( v0,v1 );
/* .line 78 */
} // :cond_0
return;
} // .end method
private miui.android.animation.utils.VelocityMonitor$MoveRecord getMoveRecord ( ) {
/* .locals 3 */
/* .line 46 */
/* new-instance v0, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;-><init>(Lmiui/android/animation/utils/VelocityMonitor$1;)V */
/* .line 47 */
/* .local v0, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;->timeStamp:J */
/* .line 48 */
} // .end method
private Float getVelocity ( Double p0, Double p1, Long p2 ) {
/* .locals 4 */
/* .param p1, "value1" # D */
/* .param p3, "value2" # D */
/* .param p5, "deltaT" # J */
/* .line 130 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p5, v0 */
/* if-nez v0, :cond_0 */
/* const-wide/16 v0, 0x0 */
} // :cond_0
/* sub-double v0, p1, p3 */
/* long-to-float v2, p5 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v2, v3 */
/* float-to-double v2, v2 */
/* div-double/2addr v0, v2 */
} // :goto_0
/* double-to-float v0, v0 */
} // .end method
private void updateVelocity ( ) {
/* .locals 6 */
/* .line 81 */
v0 = this.mHistory;
v0 = (( java.util.LinkedList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
/* .line 82 */
/* .local v0, "size":I */
int v1 = 2; // const/4 v1, 0x2
/* if-lt v0, v1, :cond_3 */
/* .line 83 */
v1 = this.mHistory;
(( java.util.LinkedList ) v1 ).getLast ( ); // invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;
/* check-cast v1, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 84 */
/* .local v1, "lastRecord":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
v2 = this.mHistory;
/* add-int/lit8 v3, v0, -0x2 */
(( java.util.LinkedList ) v2 ).get ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;
/* check-cast v2, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 86 */
/* .local v2, "lastRecord1":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
v3 = this.mVelocity;
if ( v3 != null) { // if-eqz v3, :cond_0
/* array-length v3, v3 */
v4 = this.values;
/* array-length v4, v4 */
/* if-ge v3, v4, :cond_1 */
/* .line 87 */
} // :cond_0
v3 = this.values;
/* array-length v3, v3 */
/* new-array v3, v3, [F */
this.mVelocity = v3;
/* .line 89 */
} // :cond_1
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = this.values;
/* array-length v4, v4 */
/* if-ge v3, v4, :cond_2 */
/* .line 90 */
v4 = this.mVelocity;
v5 = /* invoke-direct {p0, v3, v1, v2}, Lmiui/android/animation/utils/VelocityMonitor;->calVelocity(ILmiui/android/animation/utils/VelocityMonitor$MoveRecord;Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;)F */
/* aput v5, v4, v3 */
/* .line 89 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 92 */
} // .end local v1 # "lastRecord":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
} // .end local v2 # "lastRecord1":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
} // .end local v3 # "i":I
} // :cond_2
/* .line 93 */
} // :cond_3
/* invoke-direct {p0}, Lmiui/android/animation/utils/VelocityMonitor;->clearVelocity()V */
/* .line 95 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 70 */
v0 = this.mHistory;
(( java.util.LinkedList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
/* .line 71 */
/* invoke-direct {p0}, Lmiui/android/animation/utils/VelocityMonitor;->clearVelocity()V */
/* .line 72 */
return;
} // .end method
public Float getVelocity ( Integer p0 ) {
/* .locals 8 */
/* .param p1, "idx" # I */
/* .line 60 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 61 */
/* .local v0, "now":J */
v2 = this.mHistory;
v2 = (( java.util.LinkedList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->size()I
int v3 = 0; // const/4 v3, 0x0
/* if-lez v2, :cond_0 */
v2 = this.mHistory;
(( java.util.LinkedList ) v2 ).getLast ( ); // invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;
/* check-cast v2, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* iget-wide v4, v2, Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;->timeStamp:J */
/* sub-long v4, v0, v4 */
java.lang.Math .abs ( v4,v5 );
/* move-result-wide v4 */
/* const-wide/16 v6, 0x32 */
/* cmp-long v2, v4, v6 */
/* if-lez v2, :cond_0 */
/* .line 62 */
/* .line 63 */
} // :cond_0
v2 = this.mVelocity;
if ( v2 != null) { // if-eqz v2, :cond_1
/* array-length v4, v2 */
/* if-le v4, p1, :cond_1 */
/* .line 64 */
/* aget v2, v2, p1 */
/* .line 66 */
} // :cond_1
} // .end method
public void update ( Double...p0 ) {
/* .locals 1 */
/* .param p1, "value" # [D */
/* .line 37 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* array-length v0, p1 */
/* if-nez v0, :cond_0 */
/* .line 40 */
} // :cond_0
/* invoke-direct {p0}, Lmiui/android/animation/utils/VelocityMonitor;->getMoveRecord()Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 41 */
/* .local v0, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
this.values = p1;
/* .line 42 */
/* invoke-direct {p0, v0}, Lmiui/android/animation/utils/VelocityMonitor;->addAndUpdate(Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;)V */
/* .line 43 */
return;
/* .line 38 */
} // .end local v0 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
} // :cond_1
} // :goto_0
return;
} // .end method
public void update ( Float...p0 ) {
/* .locals 5 */
/* .param p1, "value" # [F */
/* .line 25 */
if ( p1 != null) { // if-eqz p1, :cond_2
/* array-length v0, p1 */
/* if-nez v0, :cond_0 */
/* .line 28 */
} // :cond_0
/* invoke-direct {p0}, Lmiui/android/animation/utils/VelocityMonitor;->getMoveRecord()Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* .line 29 */
/* .local v0, "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord; */
/* array-length v1, p1 */
/* new-array v1, v1, [D */
this.values = v1;
/* .line 30 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v1, v2, :cond_1 */
/* .line 31 */
v2 = this.values;
/* aget v3, p1, v1 */
/* float-to-double v3, v3 */
/* aput-wide v3, v2, v1 */
/* .line 30 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 33 */
} // .end local v1 # "i":I
} // :cond_1
/* invoke-direct {p0, v0}, Lmiui/android/animation/utils/VelocityMonitor;->addAndUpdate(Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;)V */
/* .line 34 */
return;
/* .line 26 */
} // .end local v0 # "record":Lmiui/android/animation/utils/VelocityMonitor$MoveRecord;
} // :cond_2
} // :goto_1
return;
} // .end method
