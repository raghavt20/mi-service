public class miui.android.animation.utils.ObjectPool {
	 /* .source "ObjectPool.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/utils/ObjectPool$Cache;, */
	 /* Lmiui/android/animation/utils/ObjectPool$IPoolObject; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long DELAY;
private static final Integer MAX_POOL_SIZE;
private static final java.util.concurrent.ConcurrentHashMap sCacheMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Lmiui/android/animation/utils/ObjectPool$Cache;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final android.os.Handler sMainHandler;
/* # direct methods */
static miui.android.animation.utils.ObjectPool ( ) {
/* .locals 2 */
/* .line 21 */
/* new-instance v0, Landroid/os/Handler; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 65 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
return;
} // .end method
private miui.android.animation.utils.ObjectPool ( ) {
/* .locals 0 */
/* .line 68 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static java.lang.Object access$000 ( java.lang.Class p0, java.lang.Object[] p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "x0" # Ljava/lang/Class; */
/* .param p1, "x1" # [Ljava/lang/Object; */
/* .line 13 */
miui.android.animation.utils.ObjectPool .createObject ( p0,p1 );
} // .end method
static android.os.Handler access$100 ( ) { //synthethic
/* .locals 1 */
/* .line 13 */
v0 = miui.android.animation.utils.ObjectPool.sMainHandler;
} // .end method
public static java.lang.Object acquire ( java.lang.Class p0, java.lang.Object...p1 ) {
/* .locals 2 */
/* .param p1, "args" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;[", */
/* "Ljava/lang/Object;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 71 */
/* .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
int v0 = 1; // const/4 v0, 0x1
miui.android.animation.utils.ObjectPool .getObjectCache ( p0,v0 );
/* .line 72 */
/* .local v0, "cache":Lmiui/android/animation/utils/ObjectPool$Cache; */
(( miui.android.animation.utils.ObjectPool$Cache ) v0 ).acquireObject ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lmiui/android/animation/utils/ObjectPool$Cache;->acquireObject(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
} // .end method
private static java.lang.Object createObject ( java.lang.Class p0, java.lang.Object...p1 ) {
/* .locals 6 */
/* .param p1, "args" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;[", */
/* "Ljava/lang/Object;", */
/* ")", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* .line 105 */
/* .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
try { // :try_start_0
(( java.lang.Class ) p0 ).getDeclaredConstructors ( ); // invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;
/* .line 106 */
/* .local v0, "ctrs":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<*>;" */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* aget-object v3, v0, v2 */
/* .line 107 */
/* .local v3, "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;" */
(( java.lang.reflect.Constructor ) v3 ).getParameterTypes ( ); // invoke-virtual {v3}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;
/* array-length v4, v4 */
/* array-length v5, p1 */
/* if-eq v4, v5, :cond_0 */
/* .line 108 */
/* nop */
/* .line 106 */
} // .end local v3 # "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
/* add-int/lit8 v2, v2, 0x1 */
/* .line 110 */
/* .restart local v3 # "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;" */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Constructor ) v3 ).setAccessible ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V
/* .line 111 */
(( java.lang.reflect.Constructor ) v3 ).newInstance ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 115 */
} // .end local v0 # "ctrs":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<*>;"
} // .end local v3 # "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
} // :cond_1
/* .line 113 */
/* :catch_0 */
/* move-exception v0 */
/* .line 114 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "ObjectPool.createObject failed, clz = "; // const-string v2, "ObjectPool.createObject failed, clz = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "miuix_anim"; // const-string v2, "miuix_anim"
android.util.Log .w ( v2,v1,v0 );
/* .line 116 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static miui.android.animation.utils.ObjectPool$Cache getObjectCache ( java.lang.Class p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "create" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;Z)", */
/* "Lmiui/android/animation/utils/ObjectPool$Cache;" */
/* } */
} // .end annotation
/* .line 94 */
/* .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
v0 = miui.android.animation.utils.ObjectPool.sCacheMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/android/animation/utils/ObjectPool$Cache; */
/* .line 95 */
/* .local v1, "cache":Lmiui/android/animation/utils/ObjectPool$Cache; */
/* if-nez v1, :cond_1 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 96 */
/* new-instance v2, Lmiui/android/animation/utils/ObjectPool$Cache; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Lmiui/android/animation/utils/ObjectPool$Cache;-><init>(Lmiui/android/animation/utils/ObjectPool$1;)V */
/* move-object v1, v2 */
/* .line 97 */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).putIfAbsent ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/utils/ObjectPool$Cache; */
/* .line 98 */
/* .local v0, "prev":Lmiui/android/animation/utils/ObjectPool$Cache; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v2, v0 */
} // :cond_0
/* move-object v2, v1 */
} // :goto_0
/* move-object v1, v2 */
/* .line 100 */
} // .end local v0 # "prev":Lmiui/android/animation/utils/ObjectPool$Cache;
} // :cond_1
} // .end method
public static void release ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Ljava/lang/Object; */
/* .line 76 */
/* if-nez p0, :cond_0 */
/* .line 77 */
return;
/* .line 79 */
} // :cond_0
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* .line 80 */
/* .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* instance-of v1, p0, Lmiui/android/animation/utils/ObjectPool$IPoolObject; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 81 */
/* move-object v1, p0 */
/* check-cast v1, Lmiui/android/animation/utils/ObjectPool$IPoolObject; */
/* .line 82 */
} // :cond_1
/* instance-of v1, p0, Ljava/util/Collection; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 83 */
/* move-object v1, p0 */
/* check-cast v1, Ljava/util/Collection; */
/* .line 84 */
} // :cond_2
/* instance-of v1, p0, Ljava/util/Map; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 85 */
/* move-object v1, p0 */
/* check-cast v1, Ljava/util/Map; */
/* .line 87 */
} // :cond_3
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
miui.android.animation.utils.ObjectPool .getObjectCache ( v0,v1 );
/* .line 88 */
/* .local v1, "cache":Lmiui/android/animation/utils/ObjectPool$Cache; */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 89 */
(( miui.android.animation.utils.ObjectPool$Cache ) v1 ).releaseObject ( p0 ); // invoke-virtual {v1, p0}, Lmiui/android/animation/utils/ObjectPool$Cache;->releaseObject(Ljava/lang/Object;)V
/* .line 91 */
} // :cond_4
return;
} // .end method
