.class public Lmiui/android/animation/utils/ObjectPool;
.super Ljava/lang/Object;
.source "ObjectPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/utils/ObjectPool$Cache;,
        Lmiui/android/animation/utils/ObjectPool$IPoolObject;
    }
.end annotation


# static fields
.field private static final DELAY:J = 0x1388L

.field private static final MAX_POOL_SIZE:I = 0xa

.field private static final sCacheMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Lmiui/android/animation/utils/ObjectPool$Cache;",
            ">;"
        }
    .end annotation
.end field

.field private static final sMainHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lmiui/android/animation/utils/ObjectPool;->sMainHandler:Landroid/os/Handler;

    .line 65
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiui/android/animation/utils/ObjectPool;->sCacheMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Ljava/lang/Class;
    .param p1, "x1"    # [Ljava/lang/Object;

    .line 13
    invoke-static {p0, p1}, Lmiui/android/animation/utils/ObjectPool;->createObject(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .line 13
    sget-object v0, Lmiui/android/animation/utils/ObjectPool;->sMainHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static varargs acquire(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .line 71
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lmiui/android/animation/utils/ObjectPool;->getObjectCache(Ljava/lang/Class;Z)Lmiui/android/animation/utils/ObjectPool$Cache;

    move-result-object v0

    .line 72
    .local v0, "cache":Lmiui/android/animation/utils/ObjectPool$Cache;
    invoke-virtual {v0, p0, p1}, Lmiui/android/animation/utils/ObjectPool$Cache;->acquireObject(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private static varargs createObject(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 105
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 106
    .local v0, "ctrs":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<*>;"
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 107
    .local v3, "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    invoke-virtual {v3}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v4

    array-length v4, v4

    array-length v5, p1

    if-eq v4, v5, :cond_0

    .line 108
    nop

    .line 106
    .end local v3    # "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 110
    .restart local v3    # "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 111
    invoke-virtual {v3, p1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 115
    .end local v0    # "ctrs":[Ljava/lang/reflect/Constructor;, "[Ljava/lang/reflect/Constructor<*>;"
    .end local v3    # "ctr":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :cond_1
    goto :goto_1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ObjectPool.createObject failed, clz = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "miuix_anim"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 116
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private static getObjectCache(Ljava/lang/Class;Z)Lmiui/android/animation/utils/ObjectPool$Cache;
    .locals 4
    .param p1, "create"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;Z)",
            "Lmiui/android/animation/utils/ObjectPool$Cache;"
        }
    .end annotation

    .line 94
    .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v0, Lmiui/android/animation/utils/ObjectPool;->sCacheMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/utils/ObjectPool$Cache;

    .line 95
    .local v1, "cache":Lmiui/android/animation/utils/ObjectPool$Cache;
    if-nez v1, :cond_1

    if-eqz p1, :cond_1

    .line 96
    new-instance v2, Lmiui/android/animation/utils/ObjectPool$Cache;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lmiui/android/animation/utils/ObjectPool$Cache;-><init>(Lmiui/android/animation/utils/ObjectPool$1;)V

    move-object v1, v2

    .line 97
    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/utils/ObjectPool$Cache;

    .line 98
    .local v0, "prev":Lmiui/android/animation/utils/ObjectPool$Cache;
    if-eqz v0, :cond_0

    move-object v2, v0

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    move-object v1, v2

    .line 100
    .end local v0    # "prev":Lmiui/android/animation/utils/ObjectPool$Cache;
    :cond_1
    return-object v1
.end method

.method public static release(Ljava/lang/Object;)V
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .line 76
    if-nez p0, :cond_0

    .line 77
    return-void

    .line 79
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 80
    .local v0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    instance-of v1, p0, Lmiui/android/animation/utils/ObjectPool$IPoolObject;

    if-eqz v1, :cond_1

    .line 81
    move-object v1, p0

    check-cast v1, Lmiui/android/animation/utils/ObjectPool$IPoolObject;

    invoke-interface {v1}, Lmiui/android/animation/utils/ObjectPool$IPoolObject;->clear()V

    goto :goto_0

    .line 82
    :cond_1
    instance-of v1, p0, Ljava/util/Collection;

    if-eqz v1, :cond_2

    .line 83
    move-object v1, p0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->clear()V

    goto :goto_0

    .line 84
    :cond_2
    instance-of v1, p0, Ljava/util/Map;

    if-eqz v1, :cond_3

    .line 85
    move-object v1, p0

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 87
    :cond_3
    :goto_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/android/animation/utils/ObjectPool;->getObjectCache(Ljava/lang/Class;Z)Lmiui/android/animation/utils/ObjectPool$Cache;

    move-result-object v1

    .line 88
    .local v1, "cache":Lmiui/android/animation/utils/ObjectPool$Cache;
    if-eqz v1, :cond_4

    .line 89
    invoke-virtual {v1, p0}, Lmiui/android/animation/utils/ObjectPool$Cache;->releaseObject(Ljava/lang/Object;)V

    .line 91
    :cond_4
    return-void
.end method
