.class public Lmiui/android/animation/utils/EaseManager;
.super Ljava/lang/Object;
.source "EaseManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/utils/EaseManager$SpringInterpolator;,
        Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;,
        Lmiui/android/animation/utils/EaseManager$EaseStyle;,
        Lmiui/android/animation/utils/EaseManager$EaseStyleDef;
    }
.end annotation


# static fields
.field public static final DEFAULT_DURATION:J = 0x12cL

.field static final sInterpolatorCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Integer;",
            "Landroid/animation/TimeInterpolator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 83
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiui/android/animation/utils/EaseManager;->sInterpolatorCache:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static varargs createTimeInterpolator(I[F)Landroid/animation/TimeInterpolator;
    .locals 2
    .param p0, "style"    # I
    .param p1, "factors"    # [F

    .line 86
    packed-switch p0, :pswitch_data_0

    .line 145
    const/4 v0, 0x0

    return-object v0

    .line 143
    :pswitch_0
    new-instance v0, Lmiui/android/view/animation/BounceEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/BounceEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 141
    :pswitch_1
    new-instance v0, Lmiui/android/view/animation/BounceEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/BounceEaseOutInterpolator;-><init>()V

    return-object v0

    .line 139
    :pswitch_2
    new-instance v0, Lmiui/android/view/animation/BounceEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/BounceEaseInInterpolator;-><init>()V

    return-object v0

    .line 137
    :pswitch_3
    new-instance v0, Landroid/view/animation/BounceInterpolator;

    invoke-direct {v0}, Landroid/view/animation/BounceInterpolator;-><init>()V

    return-object v0

    .line 133
    :pswitch_4
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    return-object v0

    .line 135
    :pswitch_5
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    return-object v0

    .line 131
    :pswitch_6
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    return-object v0

    .line 129
    :pswitch_7
    new-instance v0, Lmiui/android/view/animation/ExponentialEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/ExponentialEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 127
    :pswitch_8
    new-instance v0, Lmiui/android/view/animation/ExponentialEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/ExponentialEaseOutInterpolator;-><init>()V

    return-object v0

    .line 125
    :pswitch_9
    new-instance v0, Lmiui/android/view/animation/ExponentialEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/ExponentialEaseInInterpolator;-><init>()V

    return-object v0

    .line 123
    :pswitch_a
    new-instance v0, Lmiui/android/view/animation/SineEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/SineEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 121
    :pswitch_b
    new-instance v0, Lmiui/android/view/animation/SineEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/SineEaseOutInterpolator;-><init>()V

    return-object v0

    .line 119
    :pswitch_c
    new-instance v0, Lmiui/android/view/animation/SineEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/SineEaseInInterpolator;-><init>()V

    return-object v0

    .line 117
    :pswitch_d
    new-instance v0, Lmiui/android/view/animation/QuinticEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuinticEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 115
    :pswitch_e
    new-instance v0, Lmiui/android/view/animation/QuinticEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuinticEaseOutInterpolator;-><init>()V

    return-object v0

    .line 113
    :pswitch_f
    new-instance v0, Lmiui/android/view/animation/QuinticEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuinticEaseInInterpolator;-><init>()V

    return-object v0

    .line 111
    :pswitch_10
    new-instance v0, Lmiui/android/view/animation/QuarticEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuarticEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 109
    :pswitch_11
    new-instance v0, Lmiui/android/view/animation/QuadraticEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseOutInterpolator;-><init>()V

    return-object v0

    .line 107
    :pswitch_12
    new-instance v0, Lmiui/android/view/animation/QuarticEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuarticEaseInInterpolator;-><init>()V

    return-object v0

    .line 105
    :pswitch_13
    new-instance v0, Lmiui/android/view/animation/CubicEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/CubicEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 103
    :pswitch_14
    new-instance v0, Lmiui/android/view/animation/CubicEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/CubicEaseOutInterpolator;-><init>()V

    return-object v0

    .line 101
    :pswitch_15
    new-instance v0, Lmiui/android/view/animation/CubicEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/CubicEaseInInterpolator;-><init>()V

    return-object v0

    .line 99
    :pswitch_16
    new-instance v0, Lmiui/android/view/animation/QuadraticEaseInOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseInOutInterpolator;-><init>()V

    return-object v0

    .line 97
    :pswitch_17
    new-instance v0, Lmiui/android/view/animation/QuadraticEaseOutInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseOutInterpolator;-><init>()V

    return-object v0

    .line 95
    :pswitch_18
    new-instance v0, Lmiui/android/view/animation/QuadraticEaseInInterpolator;

    invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseInInterpolator;-><init>()V

    return-object v0

    .line 88
    :pswitch_19
    new-instance v0, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;

    invoke-direct {v0}, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;-><init>()V

    const/4 v1, 0x0

    aget v1, p1, v1

    .line 89
    invoke-virtual {v0, v1}, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;->setDamping(F)Lmiui/android/animation/utils/EaseManager$SpringInterpolator;

    move-result-object v0

    const/4 v1, 0x1

    aget v1, p1, v1

    .line 90
    invoke-virtual {v0, v1}, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;->setResponse(F)Lmiui/android/animation/utils/EaseManager$SpringInterpolator;

    move-result-object v0

    .line 88
    return-object v0

    .line 93
    :pswitch_1a
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1a
        :pswitch_19
        :pswitch_1a
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static varargs getInterpolator(I[F)Landroid/animation/TimeInterpolator;
    .locals 2
    .param p0, "styleDef"    # I
    .param p1, "values"    # [F

    .line 247
    invoke-static {p0, p1}, Lmiui/android/animation/utils/EaseManager;->getInterpolatorStyle(I[F)Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;

    move-result-object v0

    .line 248
    .local v0, "style":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
    invoke-static {v0}, Lmiui/android/animation/utils/EaseManager;->getInterpolator(Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;)Landroid/animation/TimeInterpolator;

    move-result-object v1

    return-object v1
.end method

.method public static getInterpolator(Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;)Landroid/animation/TimeInterpolator;
    .locals 4
    .param p0, "easeStyle"    # Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;

    .line 256
    if-eqz p0, :cond_1

    .line 257
    sget-object v0, Lmiui/android/animation/utils/EaseManager;->sInterpolatorCache:Ljava/util/concurrent/ConcurrentHashMap;

    iget v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/TimeInterpolator;

    .line 258
    .local v1, "interpolator":Landroid/animation/TimeInterpolator;
    if-nez v1, :cond_0

    .line 259
    iget v2, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I

    iget-object v3, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->factors:[F

    invoke-static {v2, v3}, Lmiui/android/animation/utils/EaseManager;->createTimeInterpolator(I[F)Landroid/animation/TimeInterpolator;

    move-result-object v1

    .line 260
    if-eqz v1, :cond_0

    .line 261
    iget v2, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    :cond_0
    return-object v1

    .line 266
    .end local v1    # "interpolator":Landroid/animation/TimeInterpolator;
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private static varargs getInterpolatorStyle(I[F)Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
    .locals 1
    .param p0, "styleDef"    # I
    .param p1, "values"    # [F

    .line 252
    new-instance v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;

    invoke-direct {v0, p0, p1}, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;-><init>(I[F)V

    return-object v0
.end method

.method public static varargs getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .locals 5
    .param p0, "styleDef"    # I
    .param p1, "values"    # [F

    .line 234
    const/4 v0, -0x1

    if-lt p0, v0, :cond_2

    .line 235
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    array-length v0, p1

    .line 236
    invoke-static {p1, v2, v0}, Ljava/util/Arrays;->copyOfRange([FII)[F

    move-result-object v0

    goto :goto_0

    :cond_0
    new-array v0, v1, [F

    .line 237
    .local v0, "factors":[F
    :goto_0
    invoke-static {p0, v0}, Lmiui/android/animation/utils/EaseManager;->getInterpolatorStyle(I[F)Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;

    move-result-object v2

    .line 238
    .local v2, "style":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
    array-length v3, p1

    if-lez v3, :cond_1

    .line 239
    aget v1, p1, v1

    float-to-int v1, v1

    int-to-long v3, v1

    invoke-virtual {v2, v3, v4}, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->setDuration(J)Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;

    .line 241
    :cond_1
    return-object v2

    .line 243
    .end local v0    # "factors":[F
    .end local v2    # "style":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
    :cond_2
    new-instance v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;

    invoke-direct {v0, p0, p1}, Lmiui/android/animation/utils/EaseManager$EaseStyle;-><init>(I[F)V

    return-object v0
.end method

.method public static isPhysicsStyle(I)Z
    .locals 1
    .param p0, "styleDef"    # I

    .line 230
    const/4 v0, -0x1

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
