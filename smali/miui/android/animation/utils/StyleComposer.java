public class miui.android.animation.utils.StyleComposer {
	 /* .source "StyleComposer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/utils/StyleComposer$IInterceptor; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # direct methods */
public miui.android.animation.utils.StyleComposer ( ) {
	 /* .locals 0 */
	 /* .line 16 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static java.lang.Object compose ( java.lang.Class p0, miui.android.animation.utils.StyleComposer$IInterceptor p1, java.lang.Object...p2 ) {
	 /* .locals 3 */
	 /* .param p1, "interceptor" # Lmiui/android/animation/utils/StyleComposer$IInterceptor; */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">(", */
	 /* "Ljava/lang/Class<", */
	 /* "TT;>;", */
	 /* "Lmiui/android/animation/utils/StyleComposer$IInterceptor;", */
	 /* "[TT;)TT;" */
	 /* } */
} // .end annotation
/* .line 31 */
/* .local p0, "interfaceClz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p2, "styles":[Ljava/lang/Object;, "[TT;" */
/* new-instance v0, Lmiui/android/animation/utils/StyleComposer$1; */
/* invoke-direct {v0, p1, p2, p0}, Lmiui/android/animation/utils/StyleComposer$1;-><init>(Lmiui/android/animation/utils/StyleComposer$IInterceptor;[Ljava/lang/Object;Ljava/lang/Class;)V */
/* .line 52 */
/* .local v0, "handler":Ljava/lang/reflect/InvocationHandler; */
(( java.lang.Class ) p0 ).getClassLoader ( ); // invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;
/* filled-new-array {p0}, [Ljava/lang/Class; */
java.lang.reflect.Proxy .newProxyInstance ( v1,v2,v0 );
/* .line 53 */
/* .local v1, "proxy":Ljava/lang/Object; */
v2 = (( java.lang.Class ) p0 ).isInstance ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 54 */
	 (( java.lang.Class ) p0 ).cast ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
	 /* .line 56 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
public static java.lang.Object compose ( java.lang.Class p0, java.lang.Object...p1 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;[TT;)TT;" */
/* } */
} // .end annotation
/* .line 26 */
/* .local p0, "interfaceClz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p1, "styles":[Ljava/lang/Object;, "[TT;" */
int v0 = 0; // const/4 v0, 0x0
miui.android.animation.utils.StyleComposer .compose ( p0,v0,p1 );
} // .end method
