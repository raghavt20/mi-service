.class public Lmiui/android/animation/utils/FieldManager;
.super Ljava/lang/Object;
.source "FieldManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/utils/FieldManager$FieldInfo;,
        Lmiui/android/animation/utils/FieldManager$MethodInfo;
    }
.end annotation


# static fields
.field static final GET:Ljava/lang/String; = "get"

.field static final SET:Ljava/lang/String; = "set"


# instance fields
.field mFieldMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmiui/android/animation/utils/FieldManager$FieldInfo;",
            ">;"
        }
    .end annotation
.end field

.field mMethodMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmiui/android/animation/utils/FieldManager$MethodInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/utils/FieldManager;->mMethodMap:Ljava/util/Map;

    .line 27
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/utils/FieldManager;->mFieldMap:Ljava/util/Map;

    return-void
.end method

.method static getField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/util/Map;)Lmiui/android/animation/utils/FieldManager$FieldInfo;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmiui/android/animation/utils/FieldManager$FieldInfo;",
            ">;)",
            "Lmiui/android/animation/utils/FieldManager$FieldInfo;"
        }
    .end annotation

    .line 138
    .local p2, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p3, "fieldMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lmiui/android/animation/utils/FieldManager$FieldInfo;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/utils/FieldManager$FieldInfo;

    .line 139
    .local v0, "info":Lmiui/android/animation/utils/FieldManager$FieldInfo;
    if-nez v0, :cond_0

    .line 140
    new-instance v1, Lmiui/android/animation/utils/FieldManager$FieldInfo;

    invoke-direct {v1}, Lmiui/android/animation/utils/FieldManager$FieldInfo;-><init>()V

    move-object v0, v1

    .line 141
    invoke-static {p0, p1, p2}, Lmiui/android/animation/utils/FieldManager;->getFieldByType(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Field;

    move-result-object v1

    iput-object v1, v0, Lmiui/android/animation/utils/FieldManager$FieldInfo;->field:Ljava/lang/reflect/Field;

    .line 142
    invoke-interface {p3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    :cond_0
    return-object v0
.end method

.method static getFieldByType(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/reflect/Field;
    .locals 3
    .param p0, "o"    # Ljava/lang/Object;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;)",
            "Ljava/lang/reflect/Field;"
        }
    .end annotation

    .line 148
    .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .line 150
    .local v0, "field":Ljava/lang/reflect/Field;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    move-object v0, v1

    .line 151
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    goto :goto_0

    .line 152
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v2

    .line 157
    goto :goto_0

    .line 155
    :catch_1
    move-exception v2

    .line 159
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    if-eq v1, p2, :cond_0

    .line 160
    const/4 v0, 0x0

    .line 162
    :cond_0
    return-object v0
.end method

.method static varargs getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 3
    .param p0, "o"    # Ljava/lang/Object;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class<",
            "*>;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    .line 122
    .local p2, "parameterClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .line 124
    .local v0, "method":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    move-object v0, v1

    .line 125
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    goto :goto_0

    .line 126
    :catch_0
    move-exception v1

    .line 128
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v2

    .line 131
    goto :goto_0

    .line 129
    :catch_1
    move-exception v2

    .line 133
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :goto_0
    return-object v0
.end method

.method static varargs getMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/util/Map;[Ljava/lang/Class;)Lmiui/android/animation/utils/FieldManager$MethodInfo;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmiui/android/animation/utils/FieldManager$MethodInfo;",
            ">;[",
            "Ljava/lang/Class<",
            "*>;)",
            "Lmiui/android/animation/utils/FieldManager$MethodInfo;"
        }
    .end annotation

    .line 112
    .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lmiui/android/animation/utils/FieldManager$MethodInfo;>;"
    .local p3, "parameterClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/utils/FieldManager$MethodInfo;

    .line 113
    .local v0, "info":Lmiui/android/animation/utils/FieldManager$MethodInfo;
    if-nez v0, :cond_0

    .line 114
    new-instance v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;

    invoke-direct {v1}, Lmiui/android/animation/utils/FieldManager$MethodInfo;-><init>()V

    move-object v0, v1

    .line 115
    invoke-static {p0, p1, p3}, Lmiui/android/animation/utils/FieldManager;->getMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, v0, Lmiui/android/animation/utils/FieldManager$MethodInfo;->method:Ljava/lang/reflect/Method;

    .line 116
    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    :cond_0
    return-object v0
.end method

.method static getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "propertyName"    # Ljava/lang/String;
    .param p1, "prefix"    # Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getValueByField(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;
    .locals 1
    .param p0, "o"    # Ljava/lang/Object;
    .param p1, "field"    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/Field;",
            ")TT;"
        }
    .end annotation

    .line 95
    :try_start_0
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 96
    :catch_0
    move-exception v0

    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method static varargs invokeMethod(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p0, "o"    # Ljava/lang/Object;
    .param p1, "method"    # Ljava/lang/reflect/Method;
    .param p2, "parameter"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/Method;",
            "[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .line 166
    if-eqz p1, :cond_0

    .line 168
    :try_start_0
    invoke-virtual {p1, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ValueProperty.invokeMethod failed, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 171
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 170
    const-string v2, "miuix_anim"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 174
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method static retToClz(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p0, "retValue"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 75
    .local p1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    instance-of v0, p0, Ljava/lang/Number;

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x0

    return-object v0

    .line 78
    :cond_0
    move-object v0, p0

    check-cast v0, Ljava/lang/Number;

    .line 79
    .local v0, "number":Ljava/lang/Number;
    const-class v1, Ljava/lang/Float;

    if-eq p1, v1, :cond_4

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne p1, v1, :cond_1

    goto :goto_1

    .line 81
    :cond_1
    const-class v1, Ljava/lang/Integer;

    if-eq p1, v1, :cond_3

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne p1, v1, :cond_2

    goto :goto_0

    .line 84
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPropertyValue, clz must be float or int instead of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 82
    :cond_3
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1

    .line 80
    :cond_4
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    return-object v1
.end method

.method static setValueByField(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    .locals 1
    .param p0, "o"    # Ljava/lang/Object;
    .param p1, "field"    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/Field;",
            "TT;)V"
        }
    .end annotation

    .line 104
    .local p2, "value":Ljava/lang/Object;, "TT;"
    :try_start_0
    invoke-virtual {p1, p0, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 108
    :goto_0
    return-void
.end method


# virtual methods
.method public declared-synchronized getField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .local p3, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    monitor-enter p0

    .line 30
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 31
    monitor-exit p0

    return-object v0

    .line 33
    :cond_0
    :try_start_0
    iget-object v1, p0, Lmiui/android/animation/utils/FieldManager;->mMethodMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;

    .line 34
    .local v1, "getter":Lmiui/android/animation/utils/FieldManager$MethodInfo;
    const/4 v2, 0x0

    if-nez v1, :cond_1

    .line 35
    const-string v3, "get"

    invoke-static {p2, v3}, Lmiui/android/animation/utils/FieldManager;->getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lmiui/android/animation/utils/FieldManager;->mMethodMap:Ljava/util/Map;

    new-array v5, v2, [Ljava/lang/Class;

    invoke-static {p1, v3, v4, v5}, Lmiui/android/animation/utils/FieldManager;->getMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/util/Map;[Ljava/lang/Class;)Lmiui/android/animation/utils/FieldManager$MethodInfo;

    move-result-object v3

    move-object v1, v3

    .line 37
    .end local p0    # "this":Lmiui/android/animation/utils/FieldManager;
    :cond_1
    iget-object v3, v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;->method:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_2

    .line 38
    iget-object v0, v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;->method:Ljava/lang/reflect/Method;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v0, v2}, Lmiui/android/animation/utils/FieldManager;->invokeMethod(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 39
    .local v0, "retValue":Ljava/lang/Object;
    invoke-static {v0, p3}, Lmiui/android/animation/utils/FieldManager;->retToClz(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    .line 41
    .end local v0    # "retValue":Ljava/lang/Object;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lmiui/android/animation/utils/FieldManager;->mFieldMap:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/utils/FieldManager$FieldInfo;

    .line 42
    .local v2, "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo;
    if-nez v2, :cond_3

    .line 43
    iget-object v3, p0, Lmiui/android/animation/utils/FieldManager;->mFieldMap:Ljava/util/Map;

    invoke-static {p1, p2, p3, v3}, Lmiui/android/animation/utils/FieldManager;->getField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/util/Map;)Lmiui/android/animation/utils/FieldManager$FieldInfo;

    move-result-object v3

    move-object v2, v3

    .line 45
    :cond_3
    iget-object v3, v2, Lmiui/android/animation/utils/FieldManager$FieldInfo;->field:Ljava/lang/reflect/Field;

    if-eqz v3, :cond_4

    .line 46
    iget-object v0, v2, Lmiui/android/animation/utils/FieldManager$FieldInfo;->field:Ljava/lang/reflect/Field;

    invoke-static {p1, v0}, Lmiui/android/animation/utils/FieldManager;->getValueByField(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 48
    :cond_4
    monitor-exit p0

    return-object v0

    .line 29
    .end local v1    # "getter":Lmiui/android/animation/utils/FieldManager$MethodInfo;
    .end local v2    # "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo;
    .end local p1    # "obj":Ljava/lang/Object;
    .end local p2    # "name":Ljava/lang/String;
    .end local p3    # "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;TT;)Z"
        }
    .end annotation

    .local p3, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "value":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    .line 52
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 53
    monitor-exit p0

    return v0

    .line 55
    :cond_0
    :try_start_0
    iget-object v1, p0, Lmiui/android/animation/utils/FieldManager;->mMethodMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;

    .line 56
    .local v1, "setter":Lmiui/android/animation/utils/FieldManager$MethodInfo;
    if-nez v1, :cond_1

    .line 57
    const-string/jumbo v2, "set"

    invoke-static {p2, v2}, Lmiui/android/animation/utils/FieldManager;->getMethodName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lmiui/android/animation/utils/FieldManager;->mMethodMap:Ljava/util/Map;

    filled-new-array {p3}, [Ljava/lang/Class;

    move-result-object v4

    invoke-static {p1, v2, v3, v4}, Lmiui/android/animation/utils/FieldManager;->getMethod(Ljava/lang/Object;Ljava/lang/String;Ljava/util/Map;[Ljava/lang/Class;)Lmiui/android/animation/utils/FieldManager$MethodInfo;

    move-result-object v2

    move-object v1, v2

    .line 59
    .end local p0    # "this":Lmiui/android/animation/utils/FieldManager;
    :cond_1
    iget-object v2, v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;->method:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 60
    iget-object v0, v1, Lmiui/android/animation/utils/FieldManager$MethodInfo;->method:Ljava/lang/reflect/Method;

    filled-new-array {p4}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1, v0, v2}, Lmiui/android/animation/utils/FieldManager;->invokeMethod(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    monitor-exit p0

    return v3

    .line 63
    :cond_2
    :try_start_1
    iget-object v2, p0, Lmiui/android/animation/utils/FieldManager;->mFieldMap:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/utils/FieldManager$FieldInfo;

    .line 64
    .local v2, "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo;
    if-nez v2, :cond_3

    .line 65
    iget-object v4, p0, Lmiui/android/animation/utils/FieldManager;->mFieldMap:Ljava/util/Map;

    invoke-static {p1, p2, p3, v4}, Lmiui/android/animation/utils/FieldManager;->getField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/util/Map;)Lmiui/android/animation/utils/FieldManager$FieldInfo;

    move-result-object v4

    move-object v2, v4

    .line 67
    :cond_3
    iget-object v4, v2, Lmiui/android/animation/utils/FieldManager$FieldInfo;->field:Ljava/lang/reflect/Field;

    if-eqz v4, :cond_4

    .line 68
    iget-object v0, v2, Lmiui/android/animation/utils/FieldManager$FieldInfo;->field:Ljava/lang/reflect/Field;

    invoke-static {p1, v0, p4}, Lmiui/android/animation/utils/FieldManager;->setValueByField(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 69
    monitor-exit p0

    return v3

    .line 71
    :cond_4
    monitor-exit p0

    return v0

    .line 51
    .end local v1    # "setter":Lmiui/android/animation/utils/FieldManager$MethodInfo;
    .end local v2    # "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo;
    .end local p1    # "obj":Ljava/lang/Object;
    .end local p2    # "name":Ljava/lang/String;
    .end local p3    # "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .end local p4    # "value":Ljava/lang/Object;, "TT;"
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
