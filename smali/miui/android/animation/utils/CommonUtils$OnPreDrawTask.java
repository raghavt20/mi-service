class miui.android.animation.utils.CommonUtils$OnPreDrawTask implements android.view.ViewTreeObserver$OnPreDrawListener {
	 /* .source "CommonUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/utils/CommonUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "OnPreDrawTask" */
} // .end annotation
/* # instance fields */
java.lang.Runnable mTask;
java.lang.ref.WeakReference mView;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
 miui.android.animation.utils.CommonUtils$OnPreDrawTask ( ) {
/* .locals 0 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 47 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 48 */
this.mTask = p1;
/* .line 49 */
return;
} // .end method
/* # virtual methods */
public Boolean onPreDraw ( ) {
/* .locals 2 */
/* .line 59 */
v0 = this.mView;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 60 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 61 */
v1 = this.mTask;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 62 */
/* .line 64 */
} // :cond_0
(( android.view.View ) v0 ).getViewTreeObserver ( ); // invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
(( android.view.ViewTreeObserver ) v1 ).removeOnPreDrawListener ( p0 ); // invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
/* .line 66 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
this.mTask = v1;
/* .line 67 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void start ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 52 */
(( android.view.View ) p1 ).getViewTreeObserver ( ); // invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;
/* .line 53 */
/* .local v0, "observer":Landroid/view/ViewTreeObserver; */
/* new-instance v1, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mView = v1;
/* .line 54 */
(( android.view.ViewTreeObserver ) v0 ).addOnPreDrawListener ( p0 ); // invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V
/* .line 55 */
return;
} // .end method
