class miui.android.animation.utils.ObjectPool$Cache {
	 /* .source "ObjectPool.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/utils/ObjectPool; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Cache" */
} // .end annotation
/* # instance fields */
final java.util.concurrent.ConcurrentHashMap mCacheRecord;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final java.util.concurrent.ConcurrentLinkedQueue pool;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentLinkedQueue<", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final java.lang.Runnable shrinkTask;
/* # direct methods */
private miui.android.animation.utils.ObjectPool$Cache ( ) {
/* .locals 1 */
/* .line 23 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 24 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V */
this.pool = v0;
/* .line 25 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mCacheRecord = v0;
/* .line 26 */
/* new-instance v0, Lmiui/android/animation/utils/ObjectPool$Cache$1; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/utils/ObjectPool$Cache$1;-><init>(Lmiui/android/animation/utils/ObjectPool$Cache;)V */
this.shrinkTask = v0;
return;
} // .end method
 miui.android.animation.utils.ObjectPool$Cache ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lmiui/android/animation/utils/ObjectPool$1; */
/* .line 23 */
/* invoke-direct {p0}, Lmiui/android/animation/utils/ObjectPool$Cache;-><init>()V */
return;
} // .end method
/* # virtual methods */
java.lang.Object acquireObject ( java.lang.Class p0, java.lang.Object...p1 ) {
/* .locals 2 */
/* .param p2, "args" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;[", */
/* "Ljava/lang/Object;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 34 */
/* .local p1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
v0 = this.pool;
(( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).poll ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;
/* .line 35 */
/* .local v0, "obj":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 36 */
v1 = this.mCacheRecord;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 37 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 38 */
miui.android.animation.utils.ObjectPool .access$000 ( p1,p2 );
/* .line 40 */
} // :cond_1
} // :goto_0
} // .end method
void releaseObject ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .line 44 */
v0 = this.mCacheRecord;
int v1 = 1; // const/4 v1, 0x1
java.lang.Boolean .valueOf ( v1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).putIfAbsent ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 45 */
return;
/* .line 47 */
} // :cond_0
v0 = this.pool;
(( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z
/* .line 48 */
miui.android.animation.utils.ObjectPool .access$100 ( );
v1 = this.shrinkTask;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 49 */
v0 = this.pool;
v0 = (( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I
/* const/16 v1, 0xa */
/* if-le v0, v1, :cond_1 */
/* .line 50 */
miui.android.animation.utils.ObjectPool .access$100 ( );
v1 = this.shrinkTask;
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 52 */
} // :cond_1
return;
} // .end method
void shrink ( ) {
/* .locals 2 */
/* .line 55 */
/* nop */
} // :goto_0
v0 = this.pool;
v0 = (( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I
/* const/16 v1, 0xa */
/* if-le v0, v1, :cond_1 */
/* .line 56 */
v0 = this.pool;
(( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).poll ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;
/* .line 57 */
/* .local v0, "obj":Ljava/lang/Object; */
/* if-nez v0, :cond_0 */
/* .line 58 */
/* .line 60 */
} // :cond_0
v1 = this.mCacheRecord;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 61 */
} // .end local v0 # "obj":Ljava/lang/Object;
/* .line 62 */
} // :cond_1
} // :goto_1
return;
} // .end method
