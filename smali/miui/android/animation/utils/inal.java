class inal implements java.lang.reflect.InvocationHandler {
	 /* .source "StyleComposer.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lmiui/android/animation/utils/StyleComposer;->compose(Ljava/lang/Class;Lmiui/android/animation/utils/StyleComposer$IInterceptor;[Ljava/lang/Object;)Ljava/lang/Object; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = null */
} // .end annotation
/* # instance fields */
final miui.android.animation.utils.StyleComposer$IInterceptor val$interceptor; //synthetic
final java.lang.Class val$interfaceClz; //synthetic
final java.lang.Object val$styles; //synthetic
/* # direct methods */
 inal ( ) {
/* .locals 0 */
/* .line 31 */
this.val$interceptor = p1;
this.val$styles = p2;
this.val$interfaceClz = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object invoke ( java.lang.Object p0, java.lang.reflect.Method p1, java.lang.Object[] p2 ) {
/* .locals 9 */
/* .param p1, "proxy" # Ljava/lang/Object; */
/* .param p2, "method" # Ljava/lang/reflect/Method; */
/* .param p3, "args" # [Ljava/lang/Object; */
/* .line 34 */
int v0 = 0; // const/4 v0, 0x0
/* .line 35 */
/* .local v0, "retValue":Ljava/lang/Object; */
v1 = this.val$interceptor;
	 v1 = if ( v1 != null) { // if-eqz v1, :cond_0
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 36 */
		 v1 = this.val$interceptor;
		 v2 = this.val$styles;
		 /* .line 38 */
	 } // :cond_0
	 v1 = this.val$styles;
	 /* array-length v2, v1 */
	 int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, v1, v3 */
/* .line 40 */
/* .local v4, "style":Ljava/lang/Object;, "TT;" */
try { // :try_start_0
	 (( java.lang.reflect.Method ) p2 ).invoke ( v4, p3 ); // invoke-virtual {p2, v4, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* move-object v0, v5 */
	 /* .line 43 */
	 /* .line 41 */
	 /* :catch_0 */
	 /* move-exception v5 */
	 /* .line 42 */
	 /* .local v5, "e":Ljava/lang/Exception; */
	 /* new-instance v6, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v7 = "failed to invoke "; // const-string v7, "failed to invoke "
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 final String v7 = " for "; // const-string v7, " for "
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.Exception ) v5 ).getCause ( ); // invoke-virtual {v5}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;
	 final String v8 = "StyleComposer"; // const-string v8, "StyleComposer"
	 android.util.Log .w ( v8,v6,v7 );
	 /* .line 38 */
} // .end local v4 # "style":Ljava/lang/Object;, "TT;"
} // .end local v5 # "e":Ljava/lang/Exception;
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 46 */
} // :cond_1
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = this.val$styles;
/* array-length v2, v1 */
/* add-int/lit8 v2, v2, -0x1 */
/* aget-object v1, v1, v2 */
/* if-ne v0, v1, :cond_2 */
/* .line 47 */
v1 = this.val$interfaceClz;
(( java.lang.Class ) v1 ).cast ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 49 */
} // :cond_2
} // .end method
