public class miui.android.animation.utils.LogUtils {
	 /* .source "LogUtils.java" */
	 /* # static fields */
	 private static final java.lang.String COMMA;
	 private static volatile Boolean sIsLogEnabled;
	 private static final android.os.Handler sLogHandler;
	 private static final java.util.Map sTag;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static final android.os.HandlerThread sThread;
/* # direct methods */
static miui.android.animation.utils.LogUtils ( ) {
/* .locals 2 */
/* .line 16 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "LogThread"; // const-string v1, "LogThread"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 19 */
/* new-instance v1, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
/* .line 22 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 23 */
/* new-instance v1, Lmiui/android/animation/utils/LogUtils$1; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v0}, Lmiui/android/animation/utils/LogUtils$1;-><init>(Landroid/os/Looper;)V */
/* .line 32 */
return;
} // .end method
private miui.android.animation.utils.LogUtils ( ) {
/* .locals 0 */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static java.util.Map access$000 ( ) { //synthethic
/* .locals 1 */
/* .line 14 */
v0 = miui.android.animation.utils.LogUtils.sTag;
} // .end method
public static void debug ( java.lang.String p0, java.lang.Object...p1 ) {
/* .locals 8 */
/* .param p0, "message" # Ljava/lang/String; */
/* .param p1, "objArray" # [Ljava/lang/Object; */
/* .line 64 */
/* sget-boolean v0, Lmiui/android/animation/utils/LogUtils;->sIsLogEnabled:Z */
/* if-nez v0, :cond_0 */
/* .line 65 */
return;
/* .line 67 */
} // :cond_0
/* array-length v0, p1 */
final String v1 = "miuix_anim"; // const-string v1, "miuix_anim"
/* if-lez v0, :cond_3 */
/* .line 68 */
/* new-instance v0, Ljava/lang/StringBuilder; */
final String v2 = ", "; // const-string v2, ", "
/* invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V */
/* .line 69 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
v3 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* .line 70 */
/* .local v3, "initLength":I */
/* array-length v4, p1 */
int v5 = 0; // const/4 v5, 0x0
} // :goto_0
/* if-ge v5, v4, :cond_2 */
/* aget-object v6, p1, v5 */
/* .line 71 */
/* .local v6, "obj":Ljava/lang/Object; */
v7 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-le v7, v3, :cond_1 */
/* .line 72 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 74 */
} // :cond_1
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 70 */
} // .end local v6 # "obj":Ljava/lang/Object;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 76 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .i ( v1,v2 );
/* .line 77 */
} // .end local v0 # "sb":Ljava/lang/StringBuilder;
} // .end local v3 # "initLength":I
/* .line 78 */
} // :cond_3
android.util.Log .i ( v1,p0 );
/* .line 80 */
} // :goto_1
return;
} // .end method
public static void getLogEnableInfo ( ) {
/* .locals 4 */
/* .line 48 */
final String v0 = "miuix_anim"; // const-string v0, "miuix_anim"
final String v1 = ""; // const-string v1, ""
/* .line 50 */
/* .local v1, "logLevel":Ljava/lang/String; */
try { // :try_start_0
final String v2 = "log.tag.folme.level"; // const-string v2, "log.tag.folme.level"
miui.android.animation.utils.CommonUtils .readProp ( v2 );
/* move-object v1, v2 */
/* .line 51 */
/* if-nez v1, :cond_0 */
final String v2 = ""; // const-string v2, ""
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // :cond_0
/* move-object v2, v1 */
} // :goto_0
/* move-object v1, v2 */
/* .line 54 */
/* .line 52 */
/* :catch_0 */
/* move-exception v2 */
/* .line 53 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "can not access property log.tag.folme.level, no log"; // const-string v3, "can not access property log.tag.folme.level, no log"
android.util.Log .i ( v0,v3,v2 );
/* .line 55 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "logLevel = "; // const-string v3, "logLevel = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .d ( v0,v2 );
/* .line 56 */
final String v0 = "D"; // const-string v0, "D"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
miui.android.animation.utils.LogUtils.sIsLogEnabled = (v0!= 0);
/* .line 57 */
return;
} // .end method
public static java.lang.String getStackTrace ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "length" # I */
/* .line 83 */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v0 ).getStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;
/* .line 84 */
/* .local v0, "traces":[Ljava/lang/StackTraceElement; */
/* array-length v1, v0 */
/* add-int/lit8 v2, p0, 0x4 */
v1 = java.lang.Math .min ( v1,v2 );
/* .line 85 */
/* .local v1, "count":I */
java.util.Arrays .asList ( v0 );
int v3 = 3; // const/4 v3, 0x3
java.util.Arrays .toString ( v2 );
} // .end method
public static Boolean isLogEnabled ( ) {
/* .locals 1 */
/* .line 60 */
/* sget-boolean v0, Lmiui/android/animation/utils/LogUtils;->sIsLogEnabled:Z */
} // .end method
public static void logThread ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p0, "tag" # Ljava/lang/String; */
/* .param p1, "log" # Ljava/lang/String; */
/* .line 34 */
v0 = miui.android.animation.utils.LogUtils.sLogHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 35 */
/* .local v0, "msg":Landroid/os/Message; */
this.obj = p1;
/* .line 36 */
v1 = (( java.lang.String ) p0 ).hashCode ( ); // invoke-virtual {p0}, Ljava/lang/String;->hashCode()I
/* iput v1, v0, Landroid/os/Message;->arg1:I */
/* .line 37 */
v1 = miui.android.animation.utils.LogUtils.sTag;
/* iget v2, v0, Landroid/os/Message;->arg1:I */
java.lang.Integer .valueOf ( v2 );
/* .line 38 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 39 */
return;
} // .end method
