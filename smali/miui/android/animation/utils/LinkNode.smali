.class public Lmiui/android/animation/utils/LinkNode;
.super Ljava/lang/Object;
.source "LinkNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lmiui/android/animation/utils/LinkNode;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public next:Lmiui/android/animation/utils/LinkNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addToTail(Lmiui/android/animation/utils/LinkNode;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 8
    .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    .local p1, "node":Lmiui/android/animation/utils/LinkNode;, "TT;"
    move-object v0, p0

    .line 10
    .local v0, "head":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    :goto_0
    if-ne v0, p1, :cond_0

    .line 11
    return-void

    .line 13
    :cond_0
    iget-object v1, v0, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    if-nez v1, :cond_1

    .line 14
    nop

    .line 18
    iput-object p1, v0, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    .line 19
    return-void

    .line 16
    :cond_1
    iget-object v0, v0, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    .line 17
    goto :goto_0
.end method

.method public destroy()Lmiui/android/animation/utils/LinkNode;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 30
    .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    nop

    :cond_0
    invoke-virtual {p0}, Lmiui/android/animation/utils/LinkNode;->remove()Lmiui/android/animation/utils/LinkNode;

    move-result-object v0

    .line 31
    .local v0, "node":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    if-nez v0, :cond_0

    .line 32
    const/4 v1, 0x0

    return-object v1
.end method

.method public remove()Lmiui/android/animation/utils/LinkNode;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 22
    .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    .line 23
    .local v0, "node":Lmiui/android/animation/utils/LinkNode;, "TT;"
    const/4 v1, 0x0

    iput-object v1, p0, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    .line 24
    return-object v0
.end method

.method public size()I
    .locals 3

    .line 36
    .local p0, "this":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    const/4 v0, 0x0

    .line 37
    .local v0, "i":I
    move-object v1, p0

    .line 38
    .local v1, "head":Lmiui/android/animation/utils/LinkNode;, "Lmiui/android/animation/utils/LinkNode<TT;>;"
    :goto_0
    iget-object v2, v1, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    if-eqz v2, :cond_0

    .line 39
    add-int/lit8 v0, v0, 0x1

    .line 40
    iget-object v1, v1, Lmiui/android/animation/utils/LinkNode;->next:Lmiui/android/animation/utils/LinkNode;

    goto :goto_0

    .line 42
    :cond_0
    return v0
.end method
