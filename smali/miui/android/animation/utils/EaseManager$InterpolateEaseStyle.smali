.class public Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
.super Lmiui/android/animation/utils/EaseManager$EaseStyle;
.source "EaseManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/utils/EaseManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InterpolateEaseStyle"
.end annotation


# instance fields
.field public duration:J


# direct methods
.method public varargs constructor <init>(I[F)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "factors"    # [F

    .line 211
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/utils/EaseManager$EaseStyle;-><init>(I[F)V

    .line 208
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J

    .line 212
    return-void
.end method


# virtual methods
.method public setDuration(J)Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
    .locals 0
    .param p1, "d"    # J

    .line 215
    iput-wide p1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J

    .line 216
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InterpolateEaseStyle{style="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", factors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->factors:[F

    .line 224
    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 221
    return-object v0
.end method
