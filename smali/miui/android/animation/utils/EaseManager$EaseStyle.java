public class miui.android.animation.utils.EaseManager$EaseStyle {
	 /* .source "EaseManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/utils/EaseManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "EaseStyle" */
} // .end annotation
/* # instance fields */
public volatile factors;
public final parameters;
public Boolean stopAtTarget;
public final Integer style;
/* # direct methods */
public miui.android.animation.utils.EaseManager$EaseStyle ( ) {
/* .locals 1 */
/* .param p1, "s" # I */
/* .param p2, "fa" # [F */
/* .line 156 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 154 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [D */
/* fill-array-data v0, :array_0 */
this.parameters = v0;
/* .line 157 */
/* iput p1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
/* .line 158 */
this.factors = p2;
/* .line 159 */
miui.android.animation.utils.EaseManager$EaseStyle .setParameters ( p0,v0 );
/* .line 160 */
return;
/* nop */
/* :array_0 */
/* .array-data 8 */
/* 0x0 */
/* 0x0 */
} // .end array-data
} // .end method
private static void setParameters ( miui.android.animation.utils.EaseManager$EaseStyle p0, Double[] p1 ) {
/* .locals 3 */
/* .param p0, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p1, "params" # [D */
/* .line 197 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
/* iget v0, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
miui.android.animation.styles.PropertyStyle .getPhyOperator ( v0 );
/* .line 198 */
/* .local v0, "operator":Lmiui/android/animation/physics/PhysicsOperator; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 199 */
v1 = this.factors;
/* .line 201 */
} // :cond_1
/* const-wide/16 v1, 0x0 */
java.util.Arrays .fill ( p1,v1,v2 );
/* .line 203 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 169 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_0 */
/* .line 170 */
/* .line 172 */
} // :cond_0
/* instance-of v1, p1, Lmiui/android/animation/utils/EaseManager$EaseStyle; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_1 */
/* .line 173 */
/* .line 175 */
} // :cond_1
/* move-object v1, p1 */
/* check-cast v1, Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .line 176 */
/* .local v1, "easeStyle":Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* iget v3, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
/* iget v4, v1, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
/* if-ne v3, v4, :cond_2 */
v3 = this.factors;
v4 = this.factors;
/* .line 177 */
v3 = java.util.Arrays .equals ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_2
} // :cond_2
/* move v0, v2 */
/* .line 176 */
} // :goto_0
} // .end method
public Integer hashCode ( ) {
/* .locals 3 */
/* .line 182 */
/* iget v0, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
java.lang.Integer .valueOf ( v0 );
/* filled-new-array {v0}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
/* .line 183 */
/* .local v0, "result":I */
/* mul-int/lit8 v1, v0, 0x1f */
v2 = this.factors;
v2 = java.util.Arrays .hashCode ( v2 );
/* add-int/2addr v1, v2 */
/* .line 184 */
} // .end local v0 # "result":I
/* .local v1, "result":I */
} // .end method
public void setFactors ( Float...p0 ) {
/* .locals 1 */
/* .param p1, "fa" # [F */
/* .line 163 */
this.factors = p1;
/* .line 164 */
v0 = this.parameters;
miui.android.animation.utils.EaseManager$EaseStyle .setParameters ( p0,v0 );
/* .line 165 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 189 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "EaseStyle{style="; // const-string v1, "EaseStyle{style="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", factors="; // const-string v1, ", factors="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.factors;
/* .line 191 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", parameters = "; // const-string v1, ", parameters = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.parameters;
/* .line 192 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 189 */
} // .end method
