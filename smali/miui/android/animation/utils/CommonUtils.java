public class miui.android.animation.utils.CommonUtils {
	 /* .source "CommonUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/utils/CommonUtils$OnPreDrawTask; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.Class BUILT_IN;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String TAG;
public static final Integer UNIT_SECOND;
public static final android.animation.ArgbEvaluator sArgbEvaluator;
private static Float sTouchSlop;
/* # direct methods */
static miui.android.animation.utils.CommonUtils ( ) {
/* .locals 12 */
/* .line 36 */
/* new-instance v0, Landroid/animation/ArgbEvaluator; */
/* invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V */
/* .line 189 */
/* const-class v1, Ljava/lang/String; */
v2 = java.lang.Integer.TYPE;
/* const-class v3, Ljava/lang/Integer; */
v4 = java.lang.Long.TYPE;
/* const-class v5, Ljava/lang/Long; */
v6 = java.lang.Short.TYPE;
/* const-class v7, Ljava/lang/Short; */
v8 = java.lang.Float.TYPE;
/* const-class v9, Ljava/lang/Float; */
v10 = java.lang.Double.TYPE;
/* const-class v11, Ljava/lang/Double; */
/* filled-new-array/range {v1 ..v11}, [Ljava/lang/Class; */
return;
} // .end method
private miui.android.animation.utils.CommonUtils ( ) {
/* .locals 0 */
/* .line 40 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static void addTo ( java.util.Collection p0, java.util.Collection p1 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/util/Collection<", */
/* "TT;>;", */
/* "Ljava/util/Collection<", */
/* "TT;>;)V" */
/* } */
} // .end annotation
/* .line 199 */
/* .local p0, "src":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;" */
/* .local p1, "dst":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 200 */
v2 = /* .local v1, "t":Ljava/lang/Object;, "TT;" */
/* if-nez v2, :cond_0 */
/* .line 201 */
/* .line 203 */
} // .end local v1 # "t":Ljava/lang/Object;, "TT;"
} // :cond_0
/* .line 204 */
} // :cond_1
return;
} // .end method
private static void closeQuietly ( java.io.Closeable p0 ) {
/* .locals 3 */
/* .param p0, "io" # Ljava/io/Closeable; */
/* .line 224 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 226 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 229 */
/* .line 227 */
/* :catch_0 */
/* move-exception v0 */
/* .line 228 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "close "; // const-string v2, "close "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = " failed"; // const-string v2, " failed"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "miuix_anim"; // const-string v2, "miuix_anim"
android.util.Log .w ( v2,v1,v0 );
/* .line 231 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
public static Double getDistance ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 6 */
/* .param p0, "x1" # F */
/* .param p1, "y1" # F */
/* .param p2, "x2" # F */
/* .param p3, "y2" # F */
/* .line 171 */
/* sub-float v0, p2, p0 */
/* float-to-double v0, v0 */
/* const-wide/high16 v2, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* sub-float v4, p3, p1 */
/* float-to-double v4, v4 */
java.lang.Math .pow ( v4,v5,v2,v3 );
/* move-result-wide v2 */
/* add-double/2addr v0, v2 */
java.lang.Math .sqrt ( v0,v1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public static java.lang.Object getLocal ( java.lang.ThreadLocal p0, java.lang.Class p1 ) {
/* .locals 2 */
/* .param p1, "clz" # Ljava/lang/Class; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/ThreadLocal<", */
/* "TT;>;", */
/* "Ljava/lang/Class;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 234 */
/* .local p0, "local":Ljava/lang/ThreadLocal;, "Ljava/lang/ThreadLocal<TT;>;" */
(( java.lang.ThreadLocal ) p0 ).get ( ); // invoke-virtual {p0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* .line 235 */
/* .local v0, "v":Ljava/lang/Object;, "TT;" */
/* if-nez v0, :cond_0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 236 */
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.ObjectPool .acquire ( p1,v1 );
/* .line 237 */
(( java.lang.ThreadLocal ) p0 ).set ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
/* .line 239 */
} // :cond_0
} // .end method
public static void getRect ( miui.android.animation.IAnimTarget p0, android.graphics.RectF p1 ) {
/* .locals 2 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p1, "rect" # Landroid/graphics/RectF; */
/* .line 175 */
v0 = miui.android.animation.property.ViewProperty.X;
v0 = (( miui.android.animation.IAnimTarget ) p0 ).getValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* iput v0, p1, Landroid/graphics/RectF;->left:F */
/* .line 176 */
v0 = miui.android.animation.property.ViewProperty.Y;
v0 = (( miui.android.animation.IAnimTarget ) p0 ).getValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* iput v0, p1, Landroid/graphics/RectF;->top:F */
/* .line 177 */
/* iget v0, p1, Landroid/graphics/RectF;->left:F */
v1 = miui.android.animation.property.ViewProperty.WIDTH;
v1 = (( miui.android.animation.IAnimTarget ) p0 ).getValue ( v1 ); // invoke-virtual {p0, v1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* add-float/2addr v0, v1 */
/* iput v0, p1, Landroid/graphics/RectF;->right:F */
/* .line 178 */
/* iget v0, p1, Landroid/graphics/RectF;->top:F */
v1 = miui.android.animation.property.ViewProperty.HEIGHT;
v1 = (( miui.android.animation.IAnimTarget ) p0 ).getValue ( v1 ); // invoke-virtual {p0, v1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* add-float/2addr v0, v1 */
/* iput v0, p1, Landroid/graphics/RectF;->bottom:F */
/* .line 179 */
return;
} // .end method
public static Float getSize ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1 ) {
/* .locals 2 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 182 */
int v0 = 0; // const/4 v0, 0x0
/* .line 183 */
/* .local v0, "sizeProp":Lmiui/android/animation/property/FloatProperty; */
v1 = miui.android.animation.property.ViewProperty.X;
/* if-ne p1, v1, :cond_0 */
v0 = miui.android.animation.property.ViewProperty.WIDTH;
/* .line 184 */
} // :cond_0
v1 = miui.android.animation.property.ViewProperty.Y;
/* if-ne p1, v1, :cond_1 */
v0 = miui.android.animation.property.ViewProperty.HEIGHT;
/* .line 185 */
} // :cond_1
v1 = miui.android.animation.property.ViewProperty.WIDTH;
/* if-eq p1, v1, :cond_2 */
v1 = miui.android.animation.property.ViewProperty.HEIGHT;
/* if-ne p1, v1, :cond_3 */
} // :cond_2
/* move-object v0, p1 */
/* .line 186 */
} // :cond_3
} // :goto_0
/* if-nez v0, :cond_4 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_4
v1 = (( miui.android.animation.IAnimTarget ) p0 ).getValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
} // :goto_1
} // .end method
public static Float getTouchSlop ( android.view.View p0 ) {
/* .locals 2 */
/* .param p0, "target" # Landroid/view/View; */
/* .line 164 */
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v0, v0, v1 */
/* if-nez v0, :cond_0 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 165 */
(( android.view.View ) p0 ).getContext ( ); // invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;
android.view.ViewConfiguration .get ( v0 );
v0 = (( android.view.ViewConfiguration ) v0 ).getScaledTouchSlop ( ); // invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I
/* int-to-float v0, v0 */
/* .line 167 */
} // :cond_0
} // .end method
public static Boolean hasFlags ( Long p0, Long p1 ) {
/* .locals 4 */
/* .param p0, "flags" # J */
/* .param p2, "mask" # J */
/* .line 125 */
/* and-long v0, p0, p2 */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean inArray ( java.lang.Object[] p0, java.lang.Object p1 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">([TT;TT;)Z" */
/* } */
} // .end annotation
/* .line 153 */
/* .local p0, "array":[Ljava/lang/Object;, "[TT;" */
/* .local p1, "element":Ljava/lang/Object;, "TT;" */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_1
if ( p0 != null) { // if-eqz p0, :cond_1
/* array-length v1, p0 */
/* if-lez v1, :cond_1 */
/* .line 154 */
/* array-length v1, p0 */
/* move v2, v0 */
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* aget-object v3, p0, v2 */
/* .line 155 */
/* .local v3, "item":Ljava/lang/Object;, "TT;" */
v4 = (( java.lang.Object ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 156 */
int v0 = 1; // const/4 v0, 0x1
/* .line 154 */
} // .end local v3 # "item":Ljava/lang/Object;, "TT;"
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 160 */
} // :cond_1
} // .end method
public static Boolean isArrayEmpty ( java.lang.Object[] p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">([TT;)Z" */
/* } */
} // .end annotation
/* .line 149 */
/* .local p0, "array":[Ljava/lang/Object;, "[TT;" */
if ( p0 != null) { // if-eqz p0, :cond_1
/* array-length v0, p0 */
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public static Boolean isBuiltInClass ( java.lang.Class p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Class<", */
/* "*>;)Z" */
/* } */
} // .end annotation
/* .line 195 */
/* .local p0, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
v0 = miui.android.animation.utils.CommonUtils.BUILT_IN;
v0 = miui.android.animation.utils.CommonUtils .inArray ( v0,p0 );
} // .end method
public static java.lang.StringBuilder mapToString ( java.util.Map p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<K:", */
/* "Ljava/lang/Object;", */
/* "V:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/util/Map<", */
/* "TK;TV;>;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/lang/StringBuilder;" */
/* } */
} // .end annotation
/* .line 98 */
/* .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 99 */
/* .local v0, "b":Ljava/lang/StringBuilder; */
/* const/16 v1, 0x7b */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 100 */
v1 = if ( p0 != null) { // if-eqz p0, :cond_1
/* if-lez v1, :cond_1 */
/* .line 101 */
v2 = } // :goto_0
/* const/16 v3, 0xa */
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 102 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;TV;>;" */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 103 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v4, 0x3d */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 104 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;TV;>;"
/* .line 105 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 107 */
} // :cond_1
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 108 */
} // .end method
public static java.lang.String mapsToString ( java.util.Map[] p0 ) {
/* .locals 5 */
/* .param p0, "maps" # [Ljava/util/Map; */
/* .line 87 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 88 */
/* .local v0, "b":Ljava/lang/StringBuilder; */
/* const/16 v1, 0x5b */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 89 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* const/16 v3, 0xa */
/* if-ge v1, v2, :cond_0 */
/* .line 90 */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 91 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v3, 0x2e */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* aget-object v3, p0, v1 */
final String v4 = " "; // const-string v4, " "
miui.android.animation.utils.CommonUtils .mapToString ( v3,v4 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;
/* .line 89 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 93 */
} // .end local v1 # "i":I
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v2, 0x5d */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 94 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static java.lang.Object mergeArray ( java.lang.Object[] p0, java.lang.Object...p1 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">([TT;[TT;)[TT;" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/SafeVarargs; */
} // .end annotation
/* .line 113 */
/* .local p0, "l":[Ljava/lang/Object;, "[TT;" */
/* .local p1, "r":[Ljava/lang/Object;, "[TT;" */
/* if-nez p0, :cond_0 */
/* .line 114 */
/* .line 115 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 116 */
/* .line 118 */
} // :cond_1
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v0 ).getComponentType ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;
/* array-length v1, p0 */
/* array-length v2, p1 */
/* add-int/2addr v1, v2 */
java.lang.reflect.Array .newInstance ( v0,v1 );
/* .line 119 */
/* .local v0, "newArray":Ljava/lang/Object; */
/* array-length v1, p0 */
int v2 = 0; // const/4 v2, 0x0
java.lang.System .arraycopy ( p0,v2,v0,v2,v1 );
/* .line 120 */
/* array-length v1, p0 */
/* array-length v3, p1 */
java.lang.System .arraycopy ( p1,v2,v0,v1,v3 );
/* .line 121 */
/* move-object v1, v0 */
/* check-cast v1, [Ljava/lang/Object; */
/* check-cast v1, [Ljava/lang/Object; */
} // .end method
public static java.lang.String readProp ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p0, "prop" # Ljava/lang/String; */
/* .line 207 */
int v0 = 0; // const/4 v0, 0x0
/* .line 208 */
/* .local v0, "ir":Ljava/io/InputStreamReader; */
int v1 = 0; // const/4 v1, 0x0
/* .line 210 */
/* .local v1, "input":Ljava/io/BufferedReader; */
try { // :try_start_0
java.lang.Runtime .getRuntime ( );
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getprop "; // const-string v4, "getprop "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.Runtime ) v2 ).exec ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
/* .line 211 */
/* .local v2, "process":Ljava/lang/Process; */
/* new-instance v3, Ljava/io/InputStreamReader; */
(( java.lang.Process ) v2 ).getInputStream ( ); // invoke-virtual {v2}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;
/* invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V */
/* move-object v0, v3 */
/* .line 212 */
/* new-instance v3, Ljava/io/BufferedReader; */
/* invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v1, v3 */
/* .line 213 */
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 217 */
miui.android.animation.utils.CommonUtils .closeQuietly ( v1 );
/* .line 218 */
miui.android.animation.utils.CommonUtils .closeQuietly ( v0 );
/* .line 213 */
/* .line 217 */
} // .end local v2 # "process":Ljava/lang/Process;
/* :catchall_0 */
/* move-exception v2 */
/* .line 214 */
/* :catch_0 */
/* move-exception v2 */
/* .line 215 */
/* .local v2, "e":Ljava/io/IOException; */
try { // :try_start_1
final String v3 = "miuix_anim"; // const-string v3, "miuix_anim"
final String v4 = "readProp failed"; // const-string v4, "readProp failed"
android.util.Log .i ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 217 */
/* nop */
} // .end local v2 # "e":Ljava/io/IOException;
miui.android.animation.utils.CommonUtils .closeQuietly ( v1 );
/* .line 218 */
miui.android.animation.utils.CommonUtils .closeQuietly ( v0 );
/* .line 219 */
/* nop */
/* .line 220 */
final String v2 = ""; // const-string v2, ""
/* .line 217 */
} // :goto_0
miui.android.animation.utils.CommonUtils .closeQuietly ( v1 );
/* .line 218 */
miui.android.animation.utils.CommonUtils .closeQuietly ( v0 );
/* .line 219 */
/* throw v2 */
} // .end method
public static void runOnPreDraw ( android.view.View p0, java.lang.Runnable p1 ) {
/* .locals 1 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 72 */
/* if-nez p0, :cond_0 */
/* .line 73 */
return;
/* .line 75 */
} // :cond_0
/* new-instance v0, Lmiui/android/animation/utils/CommonUtils$OnPreDrawTask; */
/* invoke-direct {v0, p1}, Lmiui/android/animation/utils/CommonUtils$OnPreDrawTask;-><init>(Ljava/lang/Runnable;)V */
(( miui.android.animation.utils.CommonUtils$OnPreDrawTask ) v0 ).start ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/utils/CommonUtils$OnPreDrawTask;->start(Landroid/view/View;)V
/* .line 76 */
return;
} // .end method
public static Float toFloatValue ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p0, "value" # Ljava/lang/Object; */
/* .line 139 */
/* instance-of v0, p0, Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 140 */
/* move-object v0, p0 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->floatValue()F
/* .line 141 */
} // :cond_0
/* instance-of v0, p0, Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 142 */
/* move-object v0, p0 */
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 144 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "toFloat failed, value is " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public static toIntArray ( Float[] p0 ) {
/* .locals 3 */
/* .param p0, "floatArray" # [F */
/* .line 79 */
/* array-length v0, p0 */
/* new-array v0, v0, [I */
/* .line 80 */
/* .local v0, "intArray":[I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_0 */
/* .line 81 */
/* aget v2, p0, v1 */
/* float-to-int v2, v2 */
/* aput v2, v0, v1 */
/* .line 80 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 83 */
} // .end local v1 # "i":I
} // :cond_0
} // .end method
public static Integer toIntValue ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p0, "value" # Ljava/lang/Object; */
/* .line 129 */
/* instance-of v0, p0, Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
/* move-object v0, p0 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 131 */
} // :cond_0
/* instance-of v0, p0, Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 132 */
/* move-object v0, p0 */
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->intValue()I
/* .line 134 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "toFloat failed, value is " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
