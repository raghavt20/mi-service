.class public Lmiui/android/animation/utils/EaseManager$EaseStyle;
.super Ljava/lang/Object;
.source "EaseManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/utils/EaseManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EaseStyle"
.end annotation


# instance fields
.field public volatile factors:[F

.field public final parameters:[D

.field public stopAtTarget:Z

.field public final style:I


# direct methods
.method public varargs constructor <init>(I[F)V
    .locals 1
    .param p1, "s"    # I
    .param p2, "fa"    # [F

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    const/4 v0, 0x2

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->parameters:[D

    .line 157
    iput p1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    .line 158
    iput-object p2, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    .line 159
    invoke-static {p0, v0}, Lmiui/android/animation/utils/EaseManager$EaseStyle;->setParameters(Lmiui/android/animation/utils/EaseManager$EaseStyle;[D)V

    .line 160
    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
    .end array-data
.end method

.method private static setParameters(Lmiui/android/animation/utils/EaseManager$EaseStyle;[D)V
    .locals 3
    .param p0, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p1, "params"    # [D

    .line 197
    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget v0, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    invoke-static {v0}, Lmiui/android/animation/styles/PropertyStyle;->getPhyOperator(I)Lmiui/android/animation/physics/PhysicsOperator;

    move-result-object v0

    .line 198
    .local v0, "operator":Lmiui/android/animation/physics/PhysicsOperator;
    :goto_0
    if-eqz v0, :cond_1

    .line 199
    iget-object v1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    invoke-interface {v0, v1, p1}, Lmiui/android/animation/physics/PhysicsOperator;->getParameters([F[D)V

    goto :goto_1

    .line 201
    :cond_1
    const-wide/16 v1, 0x0

    invoke-static {p1, v1, v2}, Ljava/util/Arrays;->fill([DD)V

    .line 203
    :goto_1
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 169
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    .line 170
    return v0

    .line 172
    :cond_0
    instance-of v1, p1, Lmiui/android/animation/utils/EaseManager$EaseStyle;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    .line 173
    return v2

    .line 175
    :cond_1
    move-object v1, p1

    check-cast v1, Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 176
    .local v1, "easeStyle":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    iget v3, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    iget v4, v1, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    iget-object v4, v1, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    .line 177
    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 176
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 182
    iget v0, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    .line 183
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([F)I

    move-result v2

    add-int/2addr v1, v2

    .line 184
    .end local v0    # "result":I
    .local v1, "result":I
    return v1
.end method

.method public varargs setFactors([F)V
    .locals 1
    .param p1, "fa"    # [F

    .line 163
    iput-object p1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    .line 164
    iget-object v0, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->parameters:[D

    invoke-static {p0, v0}, Lmiui/android/animation/utils/EaseManager$EaseStyle;->setParameters(Lmiui/android/animation/utils/EaseManager$EaseStyle;[D)V

    .line 165
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EaseStyle{style="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", factors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->factors:[F

    .line 191
    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parameters = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->parameters:[D

    .line 192
    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    return-object v0
.end method
