public class miui.android.animation.utils.EaseManager {
	 /* .source "EaseManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/utils/EaseManager$SpringInterpolator;, */
	 /* Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;, */
	 /* Lmiui/android/animation/utils/EaseManager$EaseStyle;, */
	 /* Lmiui/android/animation/utils/EaseManager$EaseStyleDef; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Long DEFAULT_DURATION;
static final java.util.concurrent.ConcurrentHashMap sInterpolatorCache;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/Integer;", */
/* "Landroid/animation/TimeInterpolator;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.utils.EaseManager ( ) {
/* .locals 1 */
/* .line 83 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
return;
} // .end method
public miui.android.animation.utils.EaseManager ( ) {
/* .locals 0 */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static android.animation.TimeInterpolator createTimeInterpolator ( Integer p0, Float...p1 ) {
/* .locals 2 */
/* .param p0, "style" # I */
/* .param p1, "factors" # [F */
/* .line 86 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 145 */
int v0 = 0; // const/4 v0, 0x0
/* .line 143 */
/* :pswitch_0 */
/* new-instance v0, Lmiui/android/view/animation/BounceEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/BounceEaseInOutInterpolator;-><init>()V */
/* .line 141 */
/* :pswitch_1 */
/* new-instance v0, Lmiui/android/view/animation/BounceEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/BounceEaseOutInterpolator;-><init>()V */
/* .line 139 */
/* :pswitch_2 */
/* new-instance v0, Lmiui/android/view/animation/BounceEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/BounceEaseInInterpolator;-><init>()V */
/* .line 137 */
/* :pswitch_3 */
/* new-instance v0, Landroid/view/animation/BounceInterpolator; */
/* invoke-direct {v0}, Landroid/view/animation/BounceInterpolator;-><init>()V */
/* .line 133 */
/* :pswitch_4 */
/* new-instance v0, Landroid/view/animation/AccelerateInterpolator; */
/* invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V */
/* .line 135 */
/* :pswitch_5 */
/* new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator; */
/* invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V */
/* .line 131 */
/* :pswitch_6 */
/* new-instance v0, Landroid/view/animation/DecelerateInterpolator; */
/* invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V */
/* .line 129 */
/* :pswitch_7 */
/* new-instance v0, Lmiui/android/view/animation/ExponentialEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/ExponentialEaseInOutInterpolator;-><init>()V */
/* .line 127 */
/* :pswitch_8 */
/* new-instance v0, Lmiui/android/view/animation/ExponentialEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/ExponentialEaseOutInterpolator;-><init>()V */
/* .line 125 */
/* :pswitch_9 */
/* new-instance v0, Lmiui/android/view/animation/ExponentialEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/ExponentialEaseInInterpolator;-><init>()V */
/* .line 123 */
/* :pswitch_a */
/* new-instance v0, Lmiui/android/view/animation/SineEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/SineEaseInOutInterpolator;-><init>()V */
/* .line 121 */
/* :pswitch_b */
/* new-instance v0, Lmiui/android/view/animation/SineEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/SineEaseOutInterpolator;-><init>()V */
/* .line 119 */
/* :pswitch_c */
/* new-instance v0, Lmiui/android/view/animation/SineEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/SineEaseInInterpolator;-><init>()V */
/* .line 117 */
/* :pswitch_d */
/* new-instance v0, Lmiui/android/view/animation/QuinticEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuinticEaseInOutInterpolator;-><init>()V */
/* .line 115 */
/* :pswitch_e */
/* new-instance v0, Lmiui/android/view/animation/QuinticEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuinticEaseOutInterpolator;-><init>()V */
/* .line 113 */
/* :pswitch_f */
/* new-instance v0, Lmiui/android/view/animation/QuinticEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuinticEaseInInterpolator;-><init>()V */
/* .line 111 */
/* :pswitch_10 */
/* new-instance v0, Lmiui/android/view/animation/QuarticEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuarticEaseInOutInterpolator;-><init>()V */
/* .line 109 */
/* :pswitch_11 */
/* new-instance v0, Lmiui/android/view/animation/QuadraticEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseOutInterpolator;-><init>()V */
/* .line 107 */
/* :pswitch_12 */
/* new-instance v0, Lmiui/android/view/animation/QuarticEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuarticEaseInInterpolator;-><init>()V */
/* .line 105 */
/* :pswitch_13 */
/* new-instance v0, Lmiui/android/view/animation/CubicEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/CubicEaseInOutInterpolator;-><init>()V */
/* .line 103 */
/* :pswitch_14 */
/* new-instance v0, Lmiui/android/view/animation/CubicEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/CubicEaseOutInterpolator;-><init>()V */
/* .line 101 */
/* :pswitch_15 */
/* new-instance v0, Lmiui/android/view/animation/CubicEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/CubicEaseInInterpolator;-><init>()V */
/* .line 99 */
/* :pswitch_16 */
/* new-instance v0, Lmiui/android/view/animation/QuadraticEaseInOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseInOutInterpolator;-><init>()V */
/* .line 97 */
/* :pswitch_17 */
/* new-instance v0, Lmiui/android/view/animation/QuadraticEaseOutInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseOutInterpolator;-><init>()V */
/* .line 95 */
/* :pswitch_18 */
/* new-instance v0, Lmiui/android/view/animation/QuadraticEaseInInterpolator; */
/* invoke-direct {v0}, Lmiui/android/view/animation/QuadraticEaseInInterpolator;-><init>()V */
/* .line 88 */
/* :pswitch_19 */
/* new-instance v0, Lmiui/android/animation/utils/EaseManager$SpringInterpolator; */
/* invoke-direct {v0}, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;-><init>()V */
int v1 = 0; // const/4 v1, 0x0
/* aget v1, p1, v1 */
/* .line 89 */
(( miui.android.animation.utils.EaseManager$SpringInterpolator ) v0 ).setDamping ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;->setDamping(F)Lmiui/android/animation/utils/EaseManager$SpringInterpolator;
int v1 = 1; // const/4 v1, 0x1
/* aget v1, p1, v1 */
/* .line 90 */
(( miui.android.animation.utils.EaseManager$SpringInterpolator ) v0 ).setResponse ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/utils/EaseManager$SpringInterpolator;->setResponse(F)Lmiui/android/animation/utils/EaseManager$SpringInterpolator;
/* .line 88 */
/* .line 93 */
/* :pswitch_1a */
/* new-instance v0, Landroid/view/animation/LinearInterpolator; */
/* invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_1a */
/* :pswitch_19 */
/* :pswitch_1a */
/* :pswitch_18 */
/* :pswitch_17 */
/* :pswitch_16 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static android.animation.TimeInterpolator getInterpolator ( Integer p0, Float...p1 ) {
/* .locals 2 */
/* .param p0, "styleDef" # I */
/* .param p1, "values" # [F */
/* .line 247 */
miui.android.animation.utils.EaseManager .getInterpolatorStyle ( p0,p1 );
/* .line 248 */
/* .local v0, "style":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle; */
miui.android.animation.utils.EaseManager .getInterpolator ( v0 );
} // .end method
public static android.animation.TimeInterpolator getInterpolator ( miui.android.animation.utils.EaseManager$InterpolateEaseStyle p0 ) {
/* .locals 4 */
/* .param p0, "easeStyle" # Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle; */
/* .line 256 */
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 257 */
v0 = miui.android.animation.utils.EaseManager.sInterpolatorCache;
/* iget v1, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Landroid/animation/TimeInterpolator; */
/* .line 258 */
/* .local v1, "interpolator":Landroid/animation/TimeInterpolator; */
/* if-nez v1, :cond_0 */
/* .line 259 */
/* iget v2, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I */
v3 = this.factors;
miui.android.animation.utils.EaseManager .createTimeInterpolator ( v2,v3 );
/* .line 260 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 261 */
/* iget v2, p0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->style:I */
java.lang.Integer .valueOf ( v2 );
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 264 */
} // :cond_0
/* .line 266 */
} // .end local v1 # "interpolator":Landroid/animation/TimeInterpolator;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static miui.android.animation.utils.EaseManager$InterpolateEaseStyle getInterpolatorStyle ( Integer p0, Float...p1 ) {
/* .locals 1 */
/* .param p0, "styleDef" # I */
/* .param p1, "values" # [F */
/* .line 252 */
/* new-instance v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle; */
/* invoke-direct {v0, p0, p1}, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;-><init>(I[F)V */
} // .end method
public static miui.android.animation.utils.EaseManager$EaseStyle getStyle ( Integer p0, Float...p1 ) {
/* .locals 5 */
/* .param p0, "styleDef" # I */
/* .param p1, "values" # [F */
/* .line 234 */
int v0 = -1; // const/4 v0, -0x1
/* if-lt p0, v0, :cond_2 */
/* .line 235 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-le v0, v2, :cond_0 */
/* array-length v0, p1 */
/* .line 236 */
java.util.Arrays .copyOfRange ( p1,v2,v0 );
} // :cond_0
/* new-array v0, v1, [F */
/* .line 237 */
/* .local v0, "factors":[F */
} // :goto_0
miui.android.animation.utils.EaseManager .getInterpolatorStyle ( p0,v0 );
/* .line 238 */
/* .local v2, "style":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle; */
/* array-length v3, p1 */
/* if-lez v3, :cond_1 */
/* .line 239 */
/* aget v1, p1, v1 */
/* float-to-int v1, v1 */
/* int-to-long v3, v1 */
(( miui.android.animation.utils.EaseManager$InterpolateEaseStyle ) v2 ).setDuration ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->setDuration(J)Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
/* .line 241 */
} // :cond_1
/* .line 243 */
} // .end local v0 # "factors":[F
} // .end local v2 # "style":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
} // :cond_2
/* new-instance v0, Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* invoke-direct {v0, p0, p1}, Lmiui/android/animation/utils/EaseManager$EaseStyle;-><init>(I[F)V */
} // .end method
public static Boolean isPhysicsStyle ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "styleDef" # I */
/* .line 230 */
int v0 = -1; // const/4 v0, -0x1
/* if-ge p0, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
