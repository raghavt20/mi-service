public class miui.android.animation.utils.FieldManager {
	 /* .source "FieldManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/utils/FieldManager$FieldInfo;, */
	 /* Lmiui/android/animation/utils/FieldManager$MethodInfo; */
	 /* } */
} // .end annotation
/* # static fields */
static final java.lang.String GET;
static final java.lang.String SET;
/* # instance fields */
java.util.Map mFieldMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lmiui/android/animation/utils/FieldManager$FieldInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.Map mMethodMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lmiui/android/animation/utils/FieldManager$MethodInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.utils.FieldManager ( ) {
/* .locals 1 */
/* .line 13 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 26 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mMethodMap = v0;
/* .line 27 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mFieldMap = v0;
return;
} // .end method
static miui.android.animation.utils.FieldManager$FieldInfo getField ( java.lang.Object p0, java.lang.String p1, java.lang.Class p2, java.util.Map p3 ) {
/* .locals 2 */
/* .param p0, "obj" # Ljava/lang/Object; */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "*>;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lmiui/android/animation/utils/FieldManager$FieldInfo;", */
/* ">;)", */
/* "Lmiui/android/animation/utils/FieldManager$FieldInfo;" */
/* } */
} // .end annotation
/* .line 138 */
/* .local p2, "clz":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* .local p3, "fieldMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lmiui/android/animation/utils/FieldManager$FieldInfo;>;" */
/* check-cast v0, Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* .line 139 */
/* .local v0, "info":Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* if-nez v0, :cond_0 */
/* .line 140 */
/* new-instance v1, Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* invoke-direct {v1}, Lmiui/android/animation/utils/FieldManager$FieldInfo;-><init>()V */
/* move-object v0, v1 */
/* .line 141 */
miui.android.animation.utils.FieldManager .getFieldByType ( p0,p1,p2 );
this.field = v1;
/* .line 142 */
/* .line 144 */
} // :cond_0
} // .end method
static java.lang.reflect.Field getFieldByType ( java.lang.Object p0, java.lang.String p1, java.lang.Class p2 ) {
/* .locals 3 */
/* .param p0, "o" # Ljava/lang/Object; */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Ljava/lang/reflect/Field;" */
/* } */
} // .end annotation
/* .line 148 */
/* .local p2, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 150 */
/* .local v0, "field":Ljava/lang/reflect/Field; */
try { // :try_start_0
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getDeclaredField ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* move-object v0, v1 */
/* .line 151 */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Field ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
/* :try_end_0 */
/* .catch Ljava/lang/NoSuchFieldException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 158 */
/* .line 152 */
/* :catch_0 */
/* move-exception v1 */
/* .line 154 */
/* .local v1, "e":Ljava/lang/NoSuchFieldException; */
try { // :try_start_1
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v2 ).getField ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
/* :try_end_1 */
/* .catch Ljava/lang/NoSuchFieldException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v0, v2 */
/* .line 157 */
/* .line 155 */
/* :catch_1 */
/* move-exception v2 */
/* .line 159 */
} // .end local v1 # "e":Ljava/lang/NoSuchFieldException;
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.reflect.Field ) v0 ).getType ( ); // invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;
/* if-eq v1, p2, :cond_0 */
/* .line 160 */
int v0 = 0; // const/4 v0, 0x0
/* .line 162 */
} // :cond_0
} // .end method
static java.lang.reflect.Method getMethod ( java.lang.Object p0, java.lang.String p1, java.lang.Class...p2 ) {
/* .locals 3 */
/* .param p0, "o" # Ljava/lang/Object; */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "[", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Ljava/lang/reflect/Method;" */
/* } */
} // .end annotation
/* .line 122 */
/* .local p2, "parameterClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
int v0 = 0; // const/4 v0, 0x0
/* .line 124 */
/* .local v0, "method":Ljava/lang/reflect/Method; */
try { // :try_start_0
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getDeclaredMethod ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* move-object v0, v1 */
/* .line 125 */
int v1 = 1; // const/4 v1, 0x1
(( java.lang.reflect.Method ) v0 ).setAccessible ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* :try_end_0 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 132 */
/* .line 126 */
/* :catch_0 */
/* move-exception v1 */
/* .line 128 */
/* .local v1, "e":Ljava/lang/NoSuchMethodException; */
try { // :try_start_1
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v2 ).getMethod ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* :try_end_1 */
/* .catch Ljava/lang/NoSuchMethodException; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object v0, v2 */
/* .line 131 */
/* .line 129 */
/* :catch_1 */
/* move-exception v2 */
/* .line 133 */
} // .end local v1 # "e":Ljava/lang/NoSuchMethodException;
} // :goto_0
} // .end method
static miui.android.animation.utils.FieldManager$MethodInfo getMethod ( java.lang.Object p0, java.lang.String p1, java.util.Map p2, java.lang.Class...p3 ) {
/* .locals 2 */
/* .param p0, "obj" # Ljava/lang/Object; */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lmiui/android/animation/utils/FieldManager$MethodInfo;", */
/* ">;[", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Lmiui/android/animation/utils/FieldManager$MethodInfo;" */
/* } */
} // .end annotation
/* .line 112 */
/* .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lmiui/android/animation/utils/FieldManager$MethodInfo;>;" */
/* .local p3, "parameterClass":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
/* check-cast v0, Lmiui/android/animation/utils/FieldManager$MethodInfo; */
/* .line 113 */
/* .local v0, "info":Lmiui/android/animation/utils/FieldManager$MethodInfo; */
/* if-nez v0, :cond_0 */
/* .line 114 */
/* new-instance v1, Lmiui/android/animation/utils/FieldManager$MethodInfo; */
/* invoke-direct {v1}, Lmiui/android/animation/utils/FieldManager$MethodInfo;-><init>()V */
/* move-object v0, v1 */
/* .line 115 */
miui.android.animation.utils.FieldManager .getMethod ( p0,p1,p3 );
this.method = v1;
/* .line 116 */
/* .line 118 */
} // :cond_0
} // .end method
static java.lang.String getMethodName ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "propertyName" # Ljava/lang/String; */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .line 90 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v1 = 0; // const/4 v1, 0x0
v1 = (( java.lang.String ) p0 ).charAt ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C
v1 = java.lang.Character .toUpperCase ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
int v1 = 1; // const/4 v1, 0x1
(( java.lang.String ) p0 ).substring ( v1 ); // invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
static java.lang.Object getValueByField ( java.lang.Object p0, java.lang.reflect.Field p1 ) {
/* .locals 1 */
/* .param p0, "o" # Ljava/lang/Object; */
/* .param p1, "field" # Ljava/lang/reflect/Field; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/reflect/Field;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 95 */
try { // :try_start_0
(( java.lang.reflect.Field ) p1 ).get ( p0 ); // invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 96 */
/* :catch_0 */
/* move-exception v0 */
/* .line 99 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
static java.lang.Object invokeMethod ( java.lang.Object p0, java.lang.reflect.Method p1, java.lang.Object...p2 ) {
/* .locals 3 */
/* .param p0, "o" # Ljava/lang/Object; */
/* .param p1, "method" # Ljava/lang/reflect/Method; */
/* .param p2, "parameter" # [Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/reflect/Method;", */
/* "[", */
/* "Ljava/lang/Object;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 166 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 168 */
try { // :try_start_0
(( java.lang.reflect.Method ) p1 ).invoke ( p0, p2 ); // invoke-virtual {p1, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 169 */
/* :catch_0 */
/* move-exception v0 */
/* .line 170 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "ValueProperty.invokeMethod failed, "; // const-string v2, "ValueProperty.invokeMethod failed, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 171 */
(( java.lang.reflect.Method ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 170 */
final String v2 = "miuix_anim"; // const-string v2, "miuix_anim"
android.util.Log .d ( v2,v1,v0 );
/* .line 174 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static java.lang.Object retToClz ( java.lang.Object p0, java.lang.Class p1 ) {
/* .locals 4 */
/* .param p0, "retValue" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .line 75 */
/* .local p1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* instance-of v0, p0, Ljava/lang/Number; */
/* if-nez v0, :cond_0 */
/* .line 76 */
int v0 = 0; // const/4 v0, 0x0
/* .line 78 */
} // :cond_0
/* move-object v0, p0 */
/* check-cast v0, Ljava/lang/Number; */
/* .line 79 */
/* .local v0, "number":Ljava/lang/Number; */
/* const-class v1, Ljava/lang/Float; */
/* if-eq p1, v1, :cond_4 */
v1 = java.lang.Float.TYPE;
/* if-ne p1, v1, :cond_1 */
/* .line 81 */
} // :cond_1
/* const-class v1, Ljava/lang/Integer; */
/* if-eq p1, v1, :cond_3 */
v1 = java.lang.Integer.TYPE;
/* if-ne p1, v1, :cond_2 */
/* .line 84 */
} // :cond_2
/* new-instance v1, Ljava/lang/IllegalArgumentException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "getPropertyValue, clz must be float or int instead of "; // const-string v3, "getPropertyValue, clz must be float or int instead of "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
/* .line 82 */
} // :cond_3
} // :goto_0
v1 = (( java.lang.Number ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Number;->intValue()I
java.lang.Integer .valueOf ( v1 );
/* .line 80 */
} // :cond_4
} // :goto_1
v1 = (( java.lang.Number ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F
java.lang.Float .valueOf ( v1 );
} // .end method
static void setValueByField ( java.lang.Object p0, java.lang.reflect.Field p1, java.lang.Object p2 ) {
/* .locals 1 */
/* .param p0, "o" # Ljava/lang/Object; */
/* .param p1, "field" # Ljava/lang/reflect/Field; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/reflect/Field;", */
/* "TT;)V" */
/* } */
} // .end annotation
/* .line 104 */
/* .local p2, "value":Ljava/lang/Object;, "TT;" */
try { // :try_start_0
(( java.lang.reflect.Field ) p1 ).set ( p0, p2 ); // invoke-virtual {p1, p0, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 107 */
/* .line 105 */
/* :catch_0 */
/* move-exception v0 */
/* .line 108 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public synchronized java.lang.Object getField ( java.lang.Object p0, java.lang.String p1, java.lang.Class p2 ) {
/* .locals 6 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .param p2, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .local p3, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* monitor-enter p0 */
/* .line 30 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 31 */
/* monitor-exit p0 */
/* .line 33 */
} // :cond_0
try { // :try_start_0
v1 = this.mMethodMap;
/* check-cast v1, Lmiui/android/animation/utils/FieldManager$MethodInfo; */
/* .line 34 */
/* .local v1, "getter":Lmiui/android/animation/utils/FieldManager$MethodInfo; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_1 */
/* .line 35 */
final String v3 = "get"; // const-string v3, "get"
miui.android.animation.utils.FieldManager .getMethodName ( p2,v3 );
v4 = this.mMethodMap;
/* new-array v5, v2, [Ljava/lang/Class; */
miui.android.animation.utils.FieldManager .getMethod ( p1,v3,v4,v5 );
/* move-object v1, v3 */
/* .line 37 */
} // .end local p0 # "this":Lmiui/android/animation/utils/FieldManager;
} // :cond_1
v3 = this.method;
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 38 */
v0 = this.method;
/* new-array v2, v2, [Ljava/lang/Object; */
miui.android.animation.utils.FieldManager .invokeMethod ( p1,v0,v2 );
/* .line 39 */
/* .local v0, "retValue":Ljava/lang/Object; */
miui.android.animation.utils.FieldManager .retToClz ( v0,p3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* .line 41 */
} // .end local v0 # "retValue":Ljava/lang/Object;
} // :cond_2
try { // :try_start_1
v2 = this.mFieldMap;
/* check-cast v2, Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* .line 42 */
/* .local v2, "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* if-nez v2, :cond_3 */
/* .line 43 */
v3 = this.mFieldMap;
miui.android.animation.utils.FieldManager .getField ( p1,p2,p3,v3 );
/* move-object v2, v3 */
/* .line 45 */
} // :cond_3
v3 = this.field;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 46 */
v0 = this.field;
miui.android.animation.utils.FieldManager .getValueByField ( p1,v0 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* monitor-exit p0 */
/* .line 48 */
} // :cond_4
/* monitor-exit p0 */
/* .line 29 */
} // .end local v1 # "getter":Lmiui/android/animation/utils/FieldManager$MethodInfo;
} // .end local v2 # "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo;
} // .end local p1 # "obj":Ljava/lang/Object;
} // .end local p2 # "name":Ljava/lang/String;
} // .end local p3 # "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public synchronized Boolean setField ( java.lang.Object p0, java.lang.String p1, java.lang.Class p2, java.lang.Object p3 ) {
/* .locals 5 */
/* .param p1, "obj" # Ljava/lang/Object; */
/* .param p2, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "TT;>;TT;)Z" */
/* } */
} // .end annotation
/* .local p3, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p4, "value":Ljava/lang/Object;, "TT;" */
/* monitor-enter p0 */
/* .line 52 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 53 */
/* monitor-exit p0 */
/* .line 55 */
} // :cond_0
try { // :try_start_0
v1 = this.mMethodMap;
/* check-cast v1, Lmiui/android/animation/utils/FieldManager$MethodInfo; */
/* .line 56 */
/* .local v1, "setter":Lmiui/android/animation/utils/FieldManager$MethodInfo; */
/* if-nez v1, :cond_1 */
/* .line 57 */
/* const-string/jumbo v2, "set" */
miui.android.animation.utils.FieldManager .getMethodName ( p2,v2 );
v3 = this.mMethodMap;
/* filled-new-array {p3}, [Ljava/lang/Class; */
miui.android.animation.utils.FieldManager .getMethod ( p1,v2,v3,v4 );
/* move-object v1, v2 */
/* .line 59 */
} // .end local p0 # "this":Lmiui/android/animation/utils/FieldManager;
} // :cond_1
v2 = this.method;
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 60 */
v0 = this.method;
/* filled-new-array {p4}, [Ljava/lang/Object; */
miui.android.animation.utils.FieldManager .invokeMethod ( p1,v0,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 61 */
/* monitor-exit p0 */
/* .line 63 */
} // :cond_2
try { // :try_start_1
v2 = this.mFieldMap;
/* check-cast v2, Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* .line 64 */
/* .local v2, "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo; */
/* if-nez v2, :cond_3 */
/* .line 65 */
v4 = this.mFieldMap;
miui.android.animation.utils.FieldManager .getField ( p1,p2,p3,v4 );
/* move-object v2, v4 */
/* .line 67 */
} // :cond_3
v4 = this.field;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 68 */
v0 = this.field;
miui.android.animation.utils.FieldManager .setValueByField ( p1,v0,p4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 69 */
/* monitor-exit p0 */
/* .line 71 */
} // :cond_4
/* monitor-exit p0 */
/* .line 51 */
} // .end local v1 # "setter":Lmiui/android/animation/utils/FieldManager$MethodInfo;
} // .end local v2 # "fieldInfo":Lmiui/android/animation/utils/FieldManager$FieldInfo;
} // .end local p1 # "obj":Ljava/lang/Object;
} // .end local p2 # "name":Ljava/lang/String;
} // .end local p3 # "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
} // .end local p4 # "value":Ljava/lang/Object;, "TT;"
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
