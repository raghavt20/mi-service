public abstract class miui.android.animation.utils.StyleComposer$IInterceptor {
	 /* .source "StyleComposer.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/utils/StyleComposer; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IInterceptor" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
/* # virtual methods */
public abstract java.lang.Object onMethod ( java.lang.reflect.Method p0, java.lang.Object[] p1, java.lang.Object...p2 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/reflect/Method;", */
/* "[", */
/* "Ljava/lang/Object;", */
/* "[TT;)", */
/* "Ljava/lang/Object;" */
/* } */
} // .end annotation
} // .end method
public abstract Boolean shouldIntercept ( java.lang.reflect.Method p0, java.lang.Object[] p1 ) {
} // .end method
