.class Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;
.super Ljava/lang/Object;
.source "FolmeTouch.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/controller/FolmeTouch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InnerListViewTouchListener"
.end annotation


# instance fields
.field private mConfigs:[Lmiui/android/animation/base/AnimConfig;

.field private mFolmeTouchRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiui/android/animation/controller/FolmeTouch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method varargs constructor <init>(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "folmeTouch"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p2, "configs"    # [Lmiui/android/animation/base/AnimConfig;

    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;->mFolmeTouchRef:Ljava/lang/ref/WeakReference;

    .line 364
    iput-object p2, p0, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;->mConfigs:[Lmiui/android/animation/base/AnimConfig;

    .line 365
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 369
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;->mFolmeTouchRef:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/FolmeTouch;

    .line 370
    .local v0, "folmeTouch":Lmiui/android/animation/controller/FolmeTouch;
    :goto_0
    if-eqz v0, :cond_2

    .line 371
    if-nez p2, :cond_1

    .line 372
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;->mConfigs:[Lmiui/android/animation/base/AnimConfig;

    invoke-static {v0, v1}, Lmiui/android/animation/controller/FolmeTouch;->access$600(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V

    goto :goto_1

    .line 374
    :cond_1
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;->mConfigs:[Lmiui/android/animation/base/AnimConfig;

    invoke-static {v0, p1, p2, v1}, Lmiui/android/animation/controller/FolmeTouch;->access$700(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 377
    :cond_2
    :goto_1
    const/4 v1, 0x0

    return v1
.end method
