class miui.android.animation.controller.FolmeTouch$InnerViewTouchListener implements android.view.View$OnTouchListener {
	 /* .source "FolmeTouch.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/controller/FolmeTouch; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "InnerViewTouchListener" */
} // .end annotation
/* # instance fields */
private java.util.WeakHashMap mTouchMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/WeakHashMap<", */
/* "Lmiui/android/animation/controller/FolmeTouch;", */
/* "[", */
/* "Lmiui/android/animation/base/AnimConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private miui.android.animation.controller.FolmeTouch$InnerViewTouchListener ( ) {
/* .locals 1 */
/* .line 392 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 393 */
/* new-instance v0, Ljava/util/WeakHashMap; */
/* invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V */
this.mTouchMap = v0;
return;
} // .end method
 miui.android.animation.controller.FolmeTouch$InnerViewTouchListener ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lmiui/android/animation/controller/FolmeTouch$1; */
/* .line 392 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;-><init>()V */
return;
} // .end method
/* # virtual methods */
void addTouch ( miui.android.animation.controller.FolmeTouch p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 1 */
/* .param p1, "folmeTouch" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p2, "configs" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 396 */
v0 = this.mTouchMap;
(( java.util.WeakHashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 397 */
return;
} // .end method
public Boolean onTouch ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 4 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 406 */
v0 = this.mTouchMap;
(( java.util.WeakHashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 407 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;>;" */
/* check-cast v2, Lmiui/android/animation/controller/FolmeTouch; */
/* .line 408 */
/* .local v2, "folmeTouch":Lmiui/android/animation/controller/FolmeTouch; */
/* check-cast v3, [Lmiui/android/animation/base/AnimConfig; */
/* .line 409 */
/* .local v3, "configs":[Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.controller.FolmeTouch .access$700 ( v2,p1,p2,v3 );
/* .line 410 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;>;"
} // .end local v2 # "folmeTouch":Lmiui/android/animation/controller/FolmeTouch;
} // .end local v3 # "configs":[Lmiui/android/animation/base/AnimConfig;
/* .line 411 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean removeTouch ( miui.android.animation.controller.FolmeTouch p0 ) {
/* .locals 1 */
/* .param p1, "folmeTouch" # Lmiui/android/animation/controller/FolmeTouch; */
/* .line 400 */
v0 = this.mTouchMap;
(( java.util.WeakHashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 401 */
v0 = this.mTouchMap;
v0 = (( java.util.WeakHashMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z
} // .end method
