public class miui.android.animation.controller.ListViewTouchListener implements android.view.View$OnTouchListener {
	 /* .source "ListViewTouchListener.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private Float mDownX;
	 private Float mDownY;
	 private java.util.WeakHashMap mListeners;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/WeakHashMap<", */
	 /* "Landroid/view/View;", */
	 /* "Landroid/view/View$OnTouchListener;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.graphics.Rect mRect;
private Integer mTouchSlop;
/* # direct methods */
 miui.android.animation.controller.ListViewTouchListener ( ) {
/* .locals 1 */
/* .param p1, "v" # Landroid/widget/AbsListView; */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 14 */
/* new-instance v0, Ljava/util/WeakHashMap; */
/* invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V */
this.mListeners = v0;
/* .line 16 */
/* new-instance v0, Landroid/graphics/Rect; */
/* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
this.mRect = v0;
/* .line 17 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownX:F */
/* iput v0, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownY:F */
/* .line 21 */
(( android.widget.AbsListView ) p1 ).getContext ( ); // invoke-virtual {p1}, Landroid/widget/AbsListView;->getContext()Landroid/content/Context;
android.view.ViewConfiguration .get ( v0 );
v0 = (( android.view.ViewConfiguration ) v0 ).getScaledTouchSlop ( ); // invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I
/* iput v0, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mTouchSlop:I */
/* .line 22 */
return;
} // .end method
private android.view.View getTouchedItemView ( android.widget.AbsListView p0, android.view.MotionEvent p1 ) {
/* .locals 8 */
/* .param p1, "listView" # Landroid/widget/AbsListView; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 58 */
v0 = (( android.view.MotionEvent ) p2 ).getX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F
/* float-to-int v0, v0 */
/* .line 59 */
/* .local v0, "x":I */
v1 = (( android.view.MotionEvent ) p2 ).getY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F
/* float-to-int v1, v1 */
/* .line 60 */
/* .local v1, "y":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = (( android.widget.AbsListView ) p1 ).getChildCount ( ); // invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I
/* .local v3, "n":I */
} // :goto_0
/* if-ge v2, v3, :cond_1 */
/* .line 61 */
(( android.widget.AbsListView ) p1 ).getChildAt ( v2 ); // invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;
/* .line 62 */
/* .local v4, "child":Landroid/view/View; */
v5 = this.mRect;
(( android.view.View ) v4 ).getLocalVisibleRect ( v5 ); // invoke-virtual {v4, v5}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z
/* .line 63 */
v5 = this.mRect;
v6 = (( android.view.View ) v4 ).getLeft ( ); // invoke-virtual {v4}, Landroid/view/View;->getLeft()I
v7 = (( android.view.View ) v4 ).getTop ( ); // invoke-virtual {v4}, Landroid/view/View;->getTop()I
(( android.graphics.Rect ) v5 ).offset ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->offset(II)V
/* .line 64 */
v5 = this.mRect;
v5 = (( android.graphics.Rect ) v5 ).contains ( v0, v1 ); // invoke-virtual {v5, v0, v1}, Landroid/graphics/Rect;->contains(II)Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 65 */
/* .line 60 */
} // .end local v4 # "child":Landroid/view/View;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 68 */
} // .end local v2 # "i":I
} // .end local v3 # "n":I
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
private void notifyItemListeners ( android.widget.AbsListView p0, android.view.MotionEvent p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "listView" # Landroid/widget/AbsListView; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "isScrolling" # Z */
/* .line 49 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/ListViewTouchListener;->getTouchedItemView(Landroid/widget/AbsListView;Landroid/view/MotionEvent;)Landroid/view/View; */
/* .line 50 */
/* .local v0, "touchedView":Landroid/view/View; */
v1 = this.mListeners;
(( java.util.WeakHashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 51 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Landroid/view/View$OnTouchListener;>;" */
/* check-cast v3, Landroid/view/View; */
/* .line 52 */
/* .local v3, "view":Landroid/view/View; */
/* if-nez p3, :cond_0 */
/* if-ne v3, v0, :cond_0 */
int v4 = 1; // const/4 v4, 0x1
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .line 53 */
/* .local v4, "isOnTouchView":Z */
} // :goto_1
/* check-cast v5, Landroid/view/View$OnTouchListener; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* move-object v6, p2 */
} // :cond_1
int v6 = 0; // const/4 v6, 0x0
} // :goto_2
/* .line 54 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/view/View;Landroid/view/View$OnTouchListener;>;"
} // .end local v3 # "view":Landroid/view/View;
} // .end local v4 # "isOnTouchView":Z
/* .line 55 */
} // :cond_2
return;
} // .end method
/* # virtual methods */
public Boolean onTouch ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 4 */
/* .param p1, "v" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 26 */
int v0 = 0; // const/4 v0, 0x0
/* .line 27 */
/* .local v0, "isScrolling":Z */
v1 = (( android.view.MotionEvent ) p2 ).getActionMasked ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I
int v2 = 0; // const/4 v2, 0x0
/* packed-switch v1, :pswitch_data_0 */
/* .line 37 */
/* :pswitch_0 */
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
/* iput v1, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownY:F */
/* iput v1, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownX:F */
/* .line 33 */
/* :pswitch_1 */
v1 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* iget v3, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownY:F */
/* sub-float/2addr v1, v3 */
/* iget v3, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mTouchSlop:I */
/* int-to-float v3, v3 */
/* cmpl-float v1, v1, v3 */
/* if-gtz v1, :cond_1 */
/* .line 34 */
v1 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* iget v3, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownX:F */
/* sub-float/2addr v1, v3 */
/* iget v3, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mTouchSlop:I */
/* int-to-float v3, v3 */
/* cmpl-float v1, v1, v3 */
/* if-lez v1, :cond_0 */
} // :cond_0
/* move v1, v2 */
} // :cond_1
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_1
/* move v0, v1 */
/* .line 35 */
/* .line 29 */
/* :pswitch_2 */
v1 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* iput v1, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownX:F */
/* .line 30 */
v1 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* iput v1, p0, Lmiui/android/animation/controller/ListViewTouchListener;->mDownY:F */
/* .line 31 */
/* nop */
/* .line 40 */
} // :goto_2
/* move-object v1, p1 */
/* check-cast v1, Landroid/widget/AbsListView; */
/* invoke-direct {p0, v1, p2, v0}, Lmiui/android/animation/controller/ListViewTouchListener;->notifyItemListeners(Landroid/widget/AbsListView;Landroid/view/MotionEvent;Z)V */
/* .line 41 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void putListener ( android.view.View p0, android.view.View$OnTouchListener p1 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "touchListener" # Landroid/view/View$OnTouchListener; */
/* .line 45 */
v0 = this.mListeners;
(( java.util.WeakHashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 46 */
return;
} // .end method
