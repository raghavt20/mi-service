.class public Lmiui/android/animation/controller/FolmeFont;
.super Lmiui/android/animation/controller/FolmeBase;
.source "FolmeFont.java"

# interfaces
.implements Lmiui/android/animation/IVarFontStyle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/controller/FolmeFont$FontType;
    }
.end annotation


# instance fields
.field private mDefaultTo:Lmiui/android/animation/base/AnimConfig;

.field private mInitValue:I

.field private mIsInitSet:Z

.field private mProperty:Lmiui/android/animation/font/FontWeightProperty;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 25
    const/4 v0, 0x0

    new-array v1, v0, [Lmiui/android/animation/IAnimTarget;

    invoke-direct {p0, v1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V

    .line 21
    new-instance v1, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mDefaultTo:Lmiui/android/animation/base/AnimConfig;

    .line 26
    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v2}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 27
    return-void

    nop

    :array_0
    .array-data 4
        0x43af0000    # 350.0f
        0x3f666666    # 0.9f
        0x3f5c28f6    # 0.86f
    .end array-data
.end method


# virtual methods
.method public clean()V
    .locals 1

    .line 41
    invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    .line 43
    iput-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/controller/FolmeFont;->mInitValue:I

    .line 45
    return-void
.end method

.method public varargs fromTo(II[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IVarFontStyle;
    .locals 4
    .param p1, "fromFontWeight"    # I
    .param p2, "toFontWeight"    # I
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 76
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->INIT:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 78
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->TARGET:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    int-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 79
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->INIT:Lmiui/android/animation/controller/FolmeFont$FontType;

    sget-object v2, Lmiui/android/animation/controller/FolmeFont$FontType;->TARGET:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1, v2, p3}, Lmiui/android/animation/controller/IFolmeStateStyle;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 81
    :cond_0
    return-object p0
.end method

.method public setTo(I)Lmiui/android/animation/IVarFontStyle;
    .locals 4
    .param p1, "toFontWeight"    # I

    .line 67
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->TARGET:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 69
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->TARGET:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 71
    :cond_0
    return-object p0
.end method

.method public varargs to(I[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IVarFontStyle;
    .locals 5
    .param p1, "toFontWeight"    # I
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 49
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_2

    .line 50
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeFont;->mIsInitSet:Z

    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeFont;->mIsInitSet:Z

    .line 52
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->INIT:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 54
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mDefaultTo:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-static {p2, v0}, Lmiui/android/animation/utils/CommonUtils;->mergeArray([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/base/AnimConfig;

    .line 55
    .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig;
    iget v1, p0, Lmiui/android/animation/controller/FolmeFont;->mInitValue:I

    if-ne v1, p1, :cond_1

    .line 56
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/controller/FolmeFont$FontType;->INIT:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v1, v2, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    goto :goto_0

    .line 58
    :cond_1
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/controller/FolmeFont$FontType;->TARGET:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    int-to-double v3, p1

    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 59
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/controller/FolmeFont$FontType;->TARGET:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v1, v2, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 62
    .end local v0    # "configArray":[Lmiui/android/animation/base/AnimConfig;
    :cond_2
    :goto_0
    return-object p0
.end method

.method public useAt(Landroid/widget/TextView;II)Lmiui/android/animation/IVarFontStyle;
    .locals 4
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "initFontType"    # I
    .param p3, "fromFontWeight"    # I

    .line 31
    new-instance v0, Lmiui/android/animation/controller/FolmeState;

    sget-object v1, Lmiui/android/animation/ViewTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    invoke-static {p1, v1}, Lmiui/android/animation/Folme;->getTarget(Ljava/lang/Object;Lmiui/android/animation/ITargetCreator;)Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeState;-><init>(Lmiui/android/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    .line 32
    new-instance v0, Lmiui/android/animation/font/FontWeightProperty;

    invoke-direct {v0, p1, p2}, Lmiui/android/animation/font/FontWeightProperty;-><init>(Landroid/widget/TextView;I)V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    .line 33
    iput p3, p0, Lmiui/android/animation/controller/FolmeFont;->mInitValue:I

    .line 34
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeFont;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/controller/FolmeFont$FontType;->INIT:Lmiui/android/animation/controller/FolmeFont$FontType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeFont;->mProperty:Lmiui/android/animation/font/FontWeightProperty;

    int-to-double v2, p3

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeFont;->mIsInitSet:Z

    .line 36
    return-object p0
.end method
