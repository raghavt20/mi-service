public class miui.android.animation.controller.FolmeHover extends miui.android.animation.controller.FolmeBase implements miui.android.animation.IHoverStyle {
	 /* .source "FolmeHover.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer CORNER_DIS;
private static final Float DEFAULT_CORNER;
private static final Float DEFAULT_SCALE;
private static final Integer SCALE_DIS;
private static java.util.WeakHashMap sHoverRecord;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/WeakHashMap<", */
/* "Landroid/view/View;", */
/* "Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.lang.String HoverMoveType;
private Boolean isSetAutoTranslation;
private java.lang.ref.WeakReference mChildView;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mClearTint;
private miui.android.animation.IHoverStyle$HoverEffect mCurrentEffect;
private miui.android.animation.listener.TransitionListener mDefListener;
private miui.android.animation.base.AnimConfig mEnterConfig;
private miui.android.animation.base.AnimConfig mExitConfig;
private java.lang.ref.WeakReference mHoverView;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mIsEnter;
private mLocation;
private miui.android.animation.base.AnimConfig mMoveConfig;
private java.lang.ref.WeakReference mParentView;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float mRadius;
private java.util.Map mScaleSetMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/IHoverStyle$HoverType;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mSetTint;
private Integer mTargetHeight;
private Integer mTargetWidth;
private Float mTranslateDist;
private java.util.Map mTranslationSetMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/IHoverStyle$HoverType;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.graphics.Rect mViewRect;
/* # direct methods */
static miui.android.animation.controller.FolmeHover ( ) {
/* .locals 1 */
/* .line 51 */
/* new-instance v0, Ljava/util/WeakHashMap; */
/* invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V */
return;
} // .end method
public miui.android.animation.controller.FolmeHover ( ) {
/* .locals 5 */
/* .param p1, "targets" # [Lmiui/android/animation/IAnimTarget; */
/* .line 92 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V */
/* .line 53 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslateDist:F */
/* .line 55 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
int v1 = 2; // const/4 v1, 0x2
/* new-array v2, v1, [F */
/* fill-array-data v2, :array_0 */
/* .line 56 */
int v3 = -2; // const/4 v3, -0x2
miui.android.animation.utils.EaseManager .getStyle ( v3,v2 );
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v2 ); // invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
this.mMoveConfig = v0;
/* .line 58 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.mEnterConfig = v0;
/* .line 59 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.mExitConfig = v0;
/* .line 61 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mScaleSetMap = v0;
/* .line 62 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mTranslationSetMap = v0;
/* .line 63 */
v0 = miui.android.animation.IHoverStyle$HoverEffect.NORMAL;
this.mCurrentEffect = v0;
/* .line 64 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
/* .line 67 */
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z */
/* .line 70 */
/* new-array v2, v1, [I */
this.mLocation = v2;
/* .line 72 */
int v2 = 0; // const/4 v2, 0x0
/* iput v2, p0, Lmiui/android/animation/controller/FolmeHover;->mRadius:F */
/* .line 73 */
/* iput v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I */
/* .line 74 */
/* iput v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I */
/* .line 80 */
final String v2 = "MOVE"; // const-string v2, "MOVE"
this.HoverMoveType = v2;
/* .line 82 */
/* new-instance v2, Lmiui/android/animation/controller/FolmeHover$1; */
/* invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeHover$1;-><init>(Lmiui/android/animation/controller/FolmeHover;)V */
this.mDefListener = v2;
/* .line 93 */
/* array-length v2, p1 */
/* if-lez v2, :cond_0 */
/* aget-object v0, p1, v0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->initDist(Lmiui/android/animation/IAnimTarget;)V */
/* .line 94 */
v0 = this.mCurrentEffect;
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->updateHoverState(Lmiui/android/animation/IHoverStyle$HoverEffect;)V */
/* .line 96 */
v0 = this.mEnterConfig;
/* new-array v2, v1, [F */
/* fill-array-data v2, :array_1 */
miui.android.animation.utils.EaseManager .getStyle ( v3,v2 );
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v2 ); // invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 97 */
v0 = this.mEnterConfig;
v2 = this.mDefListener;
/* filled-new-array {v2}, [Lmiui/android/animation/listener/TransitionListener; */
(( miui.android.animation.base.AnimConfig ) v0 ).addListeners ( v2 ); // invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
/* .line 99 */
v0 = this.mExitConfig;
/* new-array v2, v1, [F */
/* fill-array-data v2, :array_2 */
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Lmiui/android/animation/base/AnimConfig;->setEase(I[F)Lmiui/android/animation/base/AnimConfig;
v2 = miui.android.animation.property.ViewProperty.ALPHA;
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_3 */
/* .line 100 */
/* const-wide/16 v3, -0x2 */
(( miui.android.animation.base.AnimConfig ) v0 ).setSpecial ( v2, v3, v4, v1 ); // invoke-virtual {v0, v2, v3, v4, v1}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;
/* .line 102 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x3f666666 # 0.9f */
/* 0x3ecccccd # 0.4f */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x3f7d70a4 # 0.99f */
/* 0x3e19999a # 0.15f */
} // .end array-data
/* :array_2 */
/* .array-data 4 */
/* 0x3f7d70a4 # 0.99f */
/* 0x3e99999a # 0.3f */
} // .end array-data
/* :array_3 */
/* .array-data 4 */
/* 0x3f666666 # 0.9f */
/* 0x3e4ccccd # 0.2f */
} // .end array-data
} // .end method
static void access$100 ( miui.android.animation.controller.FolmeHover p0, android.view.View p1, android.view.MotionEvent p2, miui.android.animation.base.AnimConfig[] p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeHover; */
/* .param p1, "x1" # Landroid/view/View; */
/* .param p2, "x2" # Landroid/view/MotionEvent; */
/* .param p3, "x3" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 44 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
return;
} // .end method
private void actualTranslatDist ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 10 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 654 */
v0 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* .line 655 */
/* .local v0, "curx":F */
v1 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* .line 656 */
/* .local v1, "cury":F */
v2 = this.mLocation;
(( android.view.View ) p1 ).getLocationOnScreen ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V
/* .line 658 */
v2 = this.mLocation;
int v3 = 0; // const/4 v3, 0x0
/* aget v2, v2, v3 */
/* int-to-float v2, v2 */
v3 = (( android.view.View ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/view/View;->getWidth()I
/* int-to-float v3, v3 */
/* const/high16 v4, 0x3f000000 # 0.5f */
/* mul-float/2addr v3, v4 */
/* add-float/2addr v2, v3 */
/* .line 659 */
/* .local v2, "curViewCentreX":F */
v3 = this.mLocation;
int v5 = 1; // const/4 v5, 0x1
/* aget v3, v3, v5 */
/* int-to-float v3, v3 */
v5 = (( android.view.View ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/view/View;->getHeight()I
/* int-to-float v5, v5 */
/* mul-float/2addr v5, v4 */
/* add-float/2addr v3, v5 */
/* .line 660 */
/* .local v3, "curViewCentreY":F */
/* sub-float v4, v0, v2 */
v5 = (( android.view.View ) p1 ).getWidth ( ); // invoke-virtual {p1}, Landroid/view/View;->getWidth()I
/* int-to-float v5, v5 */
/* div-float/2addr v4, v5 */
/* .line 661 */
/* .local v4, "ox":F */
/* sub-float v5, v1, v3 */
v6 = (( android.view.View ) p1 ).getHeight ( ); // invoke-virtual {p1}, Landroid/view/View;->getHeight()I
/* int-to-float v6, v6 */
/* div-float/2addr v5, v6 */
/* .line 663 */
/* .local v5, "oy":F */
/* const/high16 v6, 0x3f800000 # 1.0f */
v7 = java.lang.Math .min ( v6,v4 );
/* const/high16 v8, -0x40800000 # -1.0f */
v4 = java.lang.Math .max ( v8,v7 );
/* .line 664 */
v7 = java.lang.Math .min ( v6,v5 );
v5 = java.lang.Math .max ( v8,v7 );
/* .line 666 */
/* iget v7, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslateDist:F */
/* const v8, 0x7f7fffff # Float.MAX_VALUE */
/* cmpl-float v9, v7, v8 */
/* if-nez v9, :cond_0 */
/* move v9, v6 */
} // :cond_0
/* move v9, v7 */
} // :goto_0
/* mul-float/2addr v4, v9 */
/* .line 667 */
/* cmpl-float v8, v7, v8 */
/* if-nez v8, :cond_1 */
} // :cond_1
/* move v6, v7 */
} // :goto_1
/* mul-float/2addr v5, v6 */
/* .line 669 */
v6 = this.mState;
v7 = this.HoverMoveType;
v7 = miui.android.animation.property.ViewProperty.TRANSLATION_X;
/* float-to-double v8, v4 */
/* .line 670 */
(( miui.android.animation.controller.AnimState ) v6 ).add ( v7, v8, v9 ); // invoke-virtual {v6, v7, v8, v9}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v7 = miui.android.animation.property.ViewProperty.TRANSLATION_Y;
/* float-to-double v8, v5 */
/* .line 671 */
(( miui.android.animation.controller.AnimState ) v6 ).add ( v7, v8, v9 ); // invoke-virtual {v6, v7, v8, v9}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 672 */
/* .local v6, "state":Lmiui/android/animation/controller/AnimState; */
v7 = this.mState;
v8 = this.mMoveConfig;
/* filled-new-array {v8}, [Lmiui/android/animation/base/AnimConfig; */
/* .line 673 */
return;
} // .end method
private static void addMagicPoint ( android.view.View p0, android.graphics.Point p1 ) {
/* .locals 4 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "magicPoint" # Landroid/graphics/Point; */
/* .line 789 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v0, v0, [Ljava/lang/Class; */
/* const-class v1, Landroid/graphics/Point; */
int v2 = 0; // const/4 v2, 0x0
/* aput-object v1, v0, v2 */
/* .line 792 */
/* .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v1 = "android.view.View"; // const-string v1, "android.view.View"
java.lang.Class .forName ( v1 );
/* .line 793 */
/* .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v2 = "addMagicPoint"; // const-string v2, "addMagicPoint"
(( java.lang.Class ) v1 ).getMethod ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 794 */
/* .local v2, "sGet":Ljava/lang/reflect/Method; */
/* filled-new-array {p1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v2 ).invoke ( p0, v3 ); // invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 797 */
/* nop */
} // .end local v0 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v1 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v2 # "sGet":Ljava/lang/reflect/Method;
/* .line 795 */
/* :catch_0 */
/* move-exception v0 */
/* .line 796 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "addMagicPoint failed , e:"; // const-string v2, "addMagicPoint failed , e:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 798 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void addMagicRect ( android.view.View p0, android.graphics.Rect p1 ) {
/* .locals 4 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "rect" # Landroid/graphics/Rect; */
/* .line 711 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v0, v0, [Ljava/lang/Class; */
/* const-class v1, Landroid/graphics/Rect; */
int v2 = 0; // const/4 v2, 0x0
/* aput-object v1, v0, v2 */
/* .line 714 */
/* .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v1 = "android.view.View"; // const-string v1, "android.view.View"
java.lang.Class .forName ( v1 );
/* .line 715 */
/* .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v2 = "addMagicRect"; // const-string v2, "addMagicRect"
(( java.lang.Class ) v1 ).getMethod ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 716 */
/* .local v2, "sGet":Ljava/lang/reflect/Method; */
/* filled-new-array {p1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v2 ).invoke ( p0, v3 ); // invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 719 */
/* nop */
} // .end local v0 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v1 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v2 # "sGet":Ljava/lang/reflect/Method;
/* .line 717 */
/* :catch_0 */
/* move-exception v0 */
/* .line 718 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "addMagicRect failed , e:"; // const-string v2, "addMagicRect failed , e:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 720 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void clearMagicPoint ( android.view.View p0 ) {
/* .locals 4 */
/* .param p0, "view" # Landroid/view/View; */
/* .line 802 */
try { // :try_start_0
final String v0 = "android.view.View"; // const-string v0, "android.view.View"
java.lang.Class .forName ( v0 );
/* .line 803 */
/* .local v0, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v1 = "clearMagicPoint"; // const-string v1, "clearMagicPoint"
int v2 = 0; // const/4 v2, 0x0
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 804 */
/* .local v1, "sGet":Ljava/lang/reflect/Method; */
/* new-array v2, v2, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p0, v2 ); // invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 807 */
/* nop */
} // .end local v0 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v1 # "sGet":Ljava/lang/reflect/Method;
/* .line 805 */
/* :catch_0 */
/* move-exception v0 */
/* .line 806 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "clearMagicPoint failed , e:"; // const-string v2, "clearMagicPoint failed , e:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 808 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void clearRound ( ) {
/* .locals 0 */
/* .line 258 */
return;
} // .end method
private void clearScale ( ) {
/* .locals 2 */
/* .line 232 */
v0 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v0 = /* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 233 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v1 = miui.android.animation.property.ViewProperty.SCALE_X;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 234 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v1 = miui.android.animation.property.ViewProperty.SCALE_Y;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 236 */
} // :cond_0
v0 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v0 = /* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 237 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.SCALE_X;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 238 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.SCALE_Y;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 240 */
} // :cond_1
v0 = this.mScaleSetMap;
/* .line 241 */
return;
} // .end method
private void clearTranslation ( ) {
/* .locals 2 */
/* .line 244 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
/* .line 245 */
v0 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v0 = /* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 246 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_X;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 247 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_Y;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 249 */
} // :cond_0
v0 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v0 = /* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 250 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_X;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 251 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_Y;
(( miui.android.animation.controller.AnimState ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 253 */
} // :cond_1
v0 = this.mTranslationSetMap;
/* .line 254 */
return;
} // .end method
private void doHandleHoverOf ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 288 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->handleViewHover(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 289 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->setHoverView(Landroid/view/View;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 290 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 291 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleViewHover for "; // const-string v1, "handleViewHover for "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 294 */
} // :cond_0
return;
} // .end method
private miui.android.animation.base.AnimConfig getEnterConfig ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 1 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 684 */
v0 = this.mEnterConfig;
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.utils.CommonUtils .mergeArray ( p1,v0 );
/* check-cast v0, [Lmiui/android/animation/base/AnimConfig; */
} // .end method
private miui.android.animation.base.AnimConfig getExitConfig ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 1 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 688 */
v0 = this.mExitConfig;
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.utils.CommonUtils .mergeArray ( p1,v0 );
/* check-cast v0, [Lmiui/android/animation/base/AnimConfig; */
} // .end method
private miui.android.animation.IHoverStyle$HoverType getType ( miui.android.animation.IHoverStyle$HoverType...p0 ) {
/* .locals 1 */
/* .param p1, "type" # [Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 593 */
/* array-length v0, p1 */
/* if-lez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, p1, v0 */
} // :cond_0
v0 = miui.android.animation.IHoverStyle$HoverType.ENTER;
} // :goto_0
} // .end method
private void handleMotionEvent ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 376 */
v0 = (( android.view.MotionEvent ) p2 ).getActionMasked ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* .line 384 */
/* :pswitch_1 */
/* invoke-direct {p0, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventExit(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 378 */
/* :pswitch_2 */
/* invoke-direct {p0, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventEnter(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 379 */
/* .line 381 */
/* :pswitch_3 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 382 */
/* nop */
/* .line 388 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x7 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private void handleViewHover ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 3 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 342 */
v0 = miui.android.animation.controller.FolmeHover.sHoverRecord;
(( java.util.WeakHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener; */
/* .line 343 */
/* .local v0, "hoverListener":Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener; */
/* if-nez v0, :cond_0 */
/* .line 344 */
/* new-instance v1, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v2}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;-><init>(Lmiui/android/animation/controller/FolmeHover$1;)V */
/* move-object v0, v1 */
/* .line 345 */
v1 = miui.android.animation.controller.FolmeHover.sHoverRecord;
(( java.util.WeakHashMap ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 347 */
} // :cond_0
(( android.view.View ) p1 ).setOnHoverListener ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V
/* .line 348 */
(( miui.android.animation.controller.FolmeHover$InnerViewHoverListener ) v0 ).addHover ( p0, p2 ); // invoke-virtual {v0, p0, p2}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;->addHover(Lmiui/android/animation/controller/FolmeHover;[Lmiui/android/animation/base/AnimConfig;)V
/* .line 349 */
return;
} // .end method
private void hoverExit ( android.view.MotionEvent p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 491 */
v0 = this.mParentView;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mHoverView;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
v1 = this.mLocation;
v0 = miui.android.animation.controller.FolmeHover .isOnHoverView ( v0,v1,p1 );
/* if-nez v0, :cond_0 */
/* .line 492 */
v0 = this.mParentView;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* filled-new-array {v0}, [Landroid/view/View; */
miui.android.animation.Folme .useAt ( v0 );
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Lmiui/android/animation/base/AnimConfig; */
/* .line 494 */
} // :cond_0
v0 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v0 = /* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 495 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_X;
/* .line 496 */
/* const-wide/16 v2, 0x0 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_Y;
/* .line 497 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 499 */
} // :cond_1
(( miui.android.animation.controller.FolmeHover ) p0 ).hoverExit ( p2 ); // invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->hoverExit([Lmiui/android/animation/base/AnimConfig;)V
/* .line 500 */
return;
} // .end method
private void initDist ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 13 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 623 */
/* instance-of v0, p1, Lmiui/android/animation/ViewTarget; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/ViewTarget; */
(( miui.android.animation.ViewTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 624 */
/* .local v0, "view":Landroid/view/View; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 625 */
v1 = miui.android.animation.property.ViewProperty.WIDTH;
v1 = (( miui.android.animation.IAnimTarget ) p1 ).getValue ( v1 ); // invoke-virtual {p1, v1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
v2 = miui.android.animation.property.ViewProperty.HEIGHT;
/* .line 626 */
v2 = (( miui.android.animation.IAnimTarget ) p1 ).getValue ( v2 ); // invoke-virtual {p1, v2}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* .line 625 */
v1 = java.lang.Math .max ( v1,v2 );
/* .line 627 */
/* .local v1, "maxSize":F */
/* const/high16 v2, 0x41400000 # 12.0f */
/* add-float/2addr v2, v1 */
/* div-float/2addr v2, v1 */
/* const v3, 0x3f933333 # 1.15f */
v2 = java.lang.Math .min ( v2,v3 );
/* .line 628 */
/* .local v2, "scaleValue":F */
/* const/16 v3, 0x14 */
/* .line 629 */
/* .local v3, "gravityEdge":I */
v4 = (( android.view.View ) v0 ).getWidth ( ); // invoke-virtual {v0}, Landroid/view/View;->getWidth()I
/* iput v4, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I */
/* .line 630 */
v4 = (( android.view.View ) v0 ).getHeight ( ); // invoke-virtual {v0}, Landroid/view/View;->getHeight()I
/* iput v4, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I */
/* .line 631 */
/* iget v5, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I */
/* mul-int/lit8 v6, v3, 0x2 */
/* sub-int/2addr v5, v6 */
/* .line 632 */
/* .local v5, "gravityW":I */
/* mul-int/lit8 v6, v3, 0x2 */
/* sub-int/2addr v4, v6 */
/* .line 634 */
/* .local v4, "gravityH":I */
/* int-to-float v6, v5 */
int v7 = 0; // const/4 v7, 0x0
/* const/high16 v8, 0x43b40000 # 360.0f */
v6 = /* invoke-direct {p0, v6, v7, v8}, Lmiui/android/animation/controller/FolmeHover;->perFromVal(FFF)F */
/* .line 635 */
/* .local v6, "wPer":F */
/* const/high16 v9, 0x3f800000 # 1.0f */
v10 = java.lang.Math .min ( v9,v6 );
v6 = java.lang.Math .max ( v7,v10 );
/* .line 636 */
/* const/high16 v10, 0x41700000 # 15.0f */
v11 = /* invoke-direct {p0, v6, v10, v7}, Lmiui/android/animation/controller/FolmeHover;->valFromPer(FFF)F */
/* .line 637 */
/* .local v11, "ox":F */
v11 = java.lang.Math .min ( v10,v11 );
/* .line 639 */
/* int-to-float v12, v4 */
v8 = /* invoke-direct {p0, v12, v7, v8}, Lmiui/android/animation/controller/FolmeHover;->perFromVal(FFF)F */
/* .line 640 */
/* .local v8, "hPer":F */
v12 = java.lang.Math .min ( v9,v8 );
v8 = java.lang.Math .max ( v7,v12 );
/* .line 641 */
v12 = /* invoke-direct {p0, v8, v10, v7}, Lmiui/android/animation/controller/FolmeHover;->valFromPer(FFF)F */
/* .line 642 */
/* .local v12, "oy":F */
v10 = java.lang.Math .min ( v10,v12 );
/* .line 643 */
} // .end local v12 # "oy":F
/* .local v10, "oy":F */
/* cmpl-float v9, v2, v9 */
/* if-nez v9, :cond_1 */
} // :cond_1
v7 = java.lang.Math .min ( v11,v10 );
} // :goto_1
/* iput v7, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslateDist:F */
/* .line 645 */
/* iget v7, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I */
/* iget v9, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I */
/* if-ne v7, v9, :cond_2 */
/* const/16 v12, 0x64 */
/* if-ge v7, v12, :cond_2 */
/* if-ge v9, v12, :cond_2 */
/* .line 646 */
/* int-to-float v7, v7 */
/* const/high16 v9, 0x3f000000 # 0.5f */
/* mul-float/2addr v7, v9 */
/* float-to-int v7, v7 */
/* int-to-float v7, v7 */
(( miui.android.animation.controller.FolmeHover ) p0 ).setCorner ( v7 ); // invoke-virtual {p0, v7}, Lmiui/android/animation/controller/FolmeHover;->setCorner(F)Lmiui/android/animation/IHoverStyle;
/* .line 648 */
} // :cond_2
/* const/high16 v7, 0x42100000 # 36.0f */
(( miui.android.animation.controller.FolmeHover ) p0 ).setCorner ( v7 ); // invoke-virtual {p0, v7}, Lmiui/android/animation/controller/FolmeHover;->setCorner(F)Lmiui/android/animation/IHoverStyle;
/* .line 651 */
} // .end local v1 # "maxSize":F
} // .end local v2 # "scaleValue":F
} // .end local v3 # "gravityEdge":I
} // .end local v4 # "gravityH":I
} // .end local v5 # "gravityW":I
} // .end local v6 # "wPer":F
} // .end local v8 # "hPer":F
} // .end local v10 # "oy":F
} // .end local v11 # "ox":F
} // :cond_3
} // :goto_2
return;
} // .end method
private static Boolean isMagicView ( android.view.View p0 ) {
/* .locals 4 */
/* .param p0, "view" # Landroid/view/View; */
/* .line 700 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
final String v1 = "android.view.View"; // const-string v1, "android.view.View"
java.lang.Class .forName ( v1 );
/* .line 701 */
/* .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
final String v2 = "isMagicView"; // const-string v2, "isMagicView"
/* new-array v3, v0, [Ljava/lang/Class; */
(( java.lang.Class ) v1 ).getMethod ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 702 */
/* .local v2, "sGet":Ljava/lang/reflect/Method; */
/* new-array v3, v0, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v2 ).invoke ( p0, v3 ); // invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 703 */
} // .end local v1 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v2 # "sGet":Ljava/lang/reflect/Method;
/* :catch_0 */
/* move-exception v1 */
/* .line 704 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isMagicView failed , e:"; // const-string v3, "isMagicView failed , e:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = ""; // const-string v3, ""
android.util.Log .e ( v3,v2 );
/* .line 706 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // .end method
static Boolean isOnHoverView ( android.view.View p0, Integer[] p1, android.view.MotionEvent p2 ) {
/* .locals 6 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "location" # [I */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 420 */
int v0 = 1; // const/4 v0, 0x1
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 421 */
(( android.view.View ) p0 ).getLocationOnScreen ( p1 ); // invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V
/* .line 422 */
v1 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* float-to-int v1, v1 */
/* .line 423 */
/* .local v1, "x":I */
v2 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* float-to-int v2, v2 */
/* .line 424 */
/* .local v2, "y":I */
int v3 = 0; // const/4 v3, 0x0
/* aget v4, p1, v3 */
/* if-lt v1, v4, :cond_0 */
/* aget v4, p1, v3 */
v5 = (( android.view.View ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/view/View;->getWidth()I
/* add-int/2addr v4, v5 */
/* if-gt v1, v4, :cond_0 */
/* aget v4, p1, v0 */
/* if-lt v2, v4, :cond_0 */
/* aget v4, p1, v0 */
/* .line 425 */
v5 = (( android.view.View ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/view/View;->getHeight()I
/* add-int/2addr v4, v5 */
/* if-gt v2, v4, :cond_0 */
} // :cond_0
/* move v0, v3 */
/* .line 424 */
} // :goto_0
/* .line 427 */
} // .end local v1 # "x":I
} // .end local v2 # "y":I
} // :cond_1
} // .end method
private Boolean isScaleSet ( miui.android.animation.IHoverStyle$HoverType p0 ) {
/* .locals 2 */
/* .param p1, "type" # Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 585 */
v0 = java.lang.Boolean.TRUE;
v1 = this.mScaleSetMap;
v0 = (( java.lang.Boolean ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
} // .end method
private Boolean isTranslationSet ( miui.android.animation.IHoverStyle$HoverType p0 ) {
/* .locals 2 */
/* .param p1, "type" # Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 589 */
v0 = java.lang.Boolean.TRUE;
v1 = this.mTranslationSetMap;
v0 = (( java.lang.Boolean ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
} // .end method
private void onEventEnter ( android.view.MotionEvent p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 391 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 392 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/Object; */
final String v1 = "onEventEnter, touchEnter"; // const-string v1, "onEventEnter, touchEnter"
miui.android.animation.utils.LogUtils .debug ( v1,v0 );
/* .line 394 */
} // :cond_0
(( miui.android.animation.controller.FolmeHover ) p0 ).hoverEnter ( p2 ); // invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->hoverEnter([Lmiui/android/animation/base/AnimConfig;)V
/* .line 395 */
return;
} // .end method
private void onEventExit ( android.view.MotionEvent p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 406 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 407 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 408 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/Object; */
final String v1 = "onEventExit, touchExit"; // const-string v1, "onEventExit, touchExit"
miui.android.animation.utils.LogUtils .debug ( v1,v0 );
/* .line 410 */
} // :cond_0
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->hoverExit(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 411 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->resetTouchStatus()V */
/* .line 413 */
} // :cond_1
return;
} // .end method
private void onEventMove ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 398 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 399 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v0 = /* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 400 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->actualTranslatDist(Landroid/view/View;Landroid/view/MotionEvent;)V */
/* .line 403 */
} // :cond_0
return;
} // .end method
private Float perFromVal ( Float p0, Float p1, Float p2 ) {
/* .locals 2 */
/* .param p1, "val" # F */
/* .param p2, "from" # F */
/* .param p3, "to" # F */
/* .line 615 */
/* sub-float v0, p1, p2 */
/* sub-float v1, p3, p2 */
/* div-float/2addr v0, v1 */
} // .end method
private void resetTouchStatus ( ) {
/* .locals 1 */
/* .line 416 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z */
/* .line 417 */
return;
} // .end method
private android.view.View resetView ( java.lang.ref.WeakReference p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;)", */
/* "Landroid/view/View;" */
/* } */
} // .end annotation
/* .line 676 */
/* .local p1, "viewHolder":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/view/View;>;" */
(( java.lang.ref.WeakReference ) p1 ).get ( ); // invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 677 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 678 */
int v1 = 0; // const/4 v1, 0x0
(( android.view.View ) v0 ).setOnHoverListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V
/* .line 680 */
} // :cond_0
} // .end method
private void setAutoRound ( ) {
/* .locals 0 */
/* .line 123 */
return;
} // .end method
private void setAutoScale ( ) {
/* .locals 4 */
/* .line 105 */
v0 = this.mScaleSetMap;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
/* .line 106 */
v0 = this.mScaleSetMap;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
/* .line 107 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.SCALE_X;
/* .line 108 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* .line 109 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 110 */
return;
} // .end method
private void setAutoTranslation ( ) {
/* .locals 4 */
/* .line 113 */
int v0 = 1; // const/4 v0, 0x1
/* .line 114 */
java.lang.Boolean .valueOf ( v0 );
/* .line 113 */
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
/* .line 114 */
v0 = this.mTranslationSetMap;
v2 = miui.android.animation.IHoverStyle$HoverType.ENTER;
/* .line 115 */
v0 = this.mTranslationSetMap;
v2 = miui.android.animation.IHoverStyle$HoverType.EXIT;
/* .line 116 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_X;
/* .line 117 */
/* const-wide/16 v2, 0x0 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.TRANSLATION_Y;
/* .line 118 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 119 */
return;
} // .end method
private Boolean setHoverView ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 332 */
v0 = this.mHoverView;
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 333 */
/* .local v0, "hoverView":Landroid/view/View; */
} // :goto_0
/* if-ne v0, p1, :cond_1 */
/* .line 334 */
int v1 = 0; // const/4 v1, 0x0
/* .line 336 */
} // :cond_1
/* new-instance v1, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mHoverView = v1;
/* .line 337 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
private static void setMagicView ( android.view.View p0, Boolean p1 ) {
/* .locals 6 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "isMagicView" # Z */
/* .line 724 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v1, v0, [Ljava/lang/Class; */
v2 = java.lang.Boolean.TYPE;
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* .line 727 */
/* .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v2 = "android.view.View"; // const-string v2, "android.view.View"
java.lang.Class .forName ( v2 );
/* .line 728 */
/* .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v4, "setMagicView" */
(( java.lang.Class ) v2 ).getMethod ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 729 */
/* .local v4, "sGet":Ljava/lang/reflect/Method; */
/* new-array v0, v0, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( p1 );
/* aput-object v5, v0, v3 */
(( java.lang.reflect.Method ) v4 ).invoke ( p0, v0 ); // invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 732 */
/* nop */
} // .end local v1 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v2 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v4 # "sGet":Ljava/lang/reflect/Method;
/* .line 730 */
/* :catch_0 */
/* move-exception v0 */
/* .line 731 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setMagicView failed , e:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 733 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void setPointerHide ( android.view.View p0, Boolean p1 ) {
/* .locals 6 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "isWrapped" # Z */
/* .line 750 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v1, v0, [Ljava/lang/Class; */
v2 = java.lang.Boolean.TYPE;
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* .line 753 */
/* .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v2 = "android.view.View"; // const-string v2, "android.view.View"
java.lang.Class .forName ( v2 );
/* .line 754 */
/* .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v4, "setPointerHide" */
(( java.lang.Class ) v2 ).getMethod ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 755 */
/* .local v4, "sGet":Ljava/lang/reflect/Method; */
/* new-array v0, v0, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( p1 );
/* aput-object v5, v0, v3 */
(( java.lang.reflect.Method ) v4 ).invoke ( p0, v0 ); // invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 758 */
/* nop */
} // .end local v1 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v2 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v4 # "sGet":Ljava/lang/reflect/Method;
/* .line 756 */
/* :catch_0 */
/* move-exception v0 */
/* .line 757 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setPointerHide failed , e:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 759 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void setPointerShape ( android.view.View p0, android.graphics.Bitmap p1 ) {
/* .locals 4 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "pointerShape" # Landroid/graphics/Bitmap; */
/* .line 763 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v0, v0, [Ljava/lang/Class; */
/* const-class v1, Landroid/graphics/Bitmap; */
int v2 = 0; // const/4 v2, 0x0
/* aput-object v1, v0, v2 */
/* .line 766 */
/* .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v1 = "android.view.View"; // const-string v1, "android.view.View"
java.lang.Class .forName ( v1 );
/* .line 767 */
/* .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v2, "setPointerShape" */
(( java.lang.Class ) v1 ).getMethod ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 768 */
/* .local v2, "sGet":Ljava/lang/reflect/Method; */
/* filled-new-array {p1}, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v2 ).invoke ( p0, v3 ); // invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 771 */
/* nop */
} // .end local v0 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v1 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v2 # "sGet":Ljava/lang/reflect/Method;
/* .line 769 */
/* :catch_0 */
/* move-exception v0 */
/* .line 770 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setPointerShape failed , e:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 772 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private static void setPointerShapeType ( android.view.View p0, Integer p1 ) {
/* .locals 6 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "pointerShapeType" # I */
/* .line 776 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v1, v0, [Ljava/lang/Class; */
v2 = java.lang.Integer.TYPE;
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* .line 779 */
/* .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v2 = "android.view.View"; // const-string v2, "android.view.View"
java.lang.Class .forName ( v2 );
/* .line 780 */
/* .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v4, "setPointerShapeType" */
(( java.lang.Class ) v2 ).getMethod ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 781 */
/* .local v4, "sGet":Ljava/lang/reflect/Method; */
/* new-array v0, v0, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
/* aput-object v5, v0, v3 */
(( java.lang.reflect.Method ) v4 ).invoke ( p0, v0 ); // invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 784 */
/* nop */
} // .end local v1 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v2 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v4 # "sGet":Ljava/lang/reflect/Method;
/* .line 782 */
/* :catch_0 */
/* move-exception v0 */
/* .line 783 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setPointerShapeType failed , e:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 785 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void setTintColor ( ) {
/* .locals 7 */
/* .line 126 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mSetTint:Z */
/* if-nez v0, :cond_3 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 129 */
} // :cond_0
/* const/16 v0, 0x14 */
int v1 = 0; // const/4 v1, 0x0
v0 = android.graphics.Color .argb ( v0,v1,v1,v1 );
/* .line 130 */
/* .local v0, "tintColor":I */
v1 = this.mState;
(( miui.android.animation.IAnimTarget ) v1 ).getTargetObject ( ); // invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 131 */
/* .local v1, "target":Ljava/lang/Object; */
/* instance-of v2, v1, Landroid/view/View; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 132 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/view/View; */
/* .line 133 */
/* .local v2, "view":Landroid/view/View; */
/* .line 134 */
/* .local v3, "colorRes":I */
(( android.view.View ) v2 ).getContext ( ); // invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;
/* const-string/jumbo v5, "uimode" */
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/app/UiModeManager; */
/* .line 135 */
/* .local v4, "mgr":Landroid/app/UiModeManager; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = (( android.app.UiModeManager ) v4 ).getNightMode ( ); // invoke-virtual {v4}, Landroid/app/UiModeManager;->getNightMode()I
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_1 */
/* .line 136 */
/* .line 138 */
} // :cond_1
(( android.view.View ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;
v0 = (( android.content.res.Resources ) v5 ).getColor ( v3 ); // invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I
/* .line 140 */
} // .end local v2 # "view":Landroid/view/View;
} // .end local v3 # "colorRes":I
} // .end local v4 # "mgr":Landroid/app/UiModeManager;
} // :cond_2
v2 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* .line 141 */
/* .local v2, "propFg":Lmiui/android/animation/property/FloatProperty; */
v3 = this.mState;
v4 = miui.android.animation.IHoverStyle$HoverType.ENTER;
/* int-to-double v4, v0 */
(( miui.android.animation.controller.AnimState ) v3 ).add ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 142 */
v3 = this.mState;
v4 = miui.android.animation.IHoverStyle$HoverType.EXIT;
/* const-wide/16 v4, 0x0 */
(( miui.android.animation.controller.AnimState ) v3 ).add ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 143 */
return;
/* .line 127 */
} // .end local v0 # "tintColor":I
} // .end local v1 # "target":Ljava/lang/Object;
} // .end local v2 # "propFg":Lmiui/android/animation/property/FloatProperty;
} // :cond_3
} // :goto_0
return;
} // .end method
private static void setWrapped ( android.view.View p0, Boolean p1 ) {
/* .locals 6 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "isWrapped" # Z */
/* .line 737 */
int v0 = 1; // const/4 v0, 0x1
try { // :try_start_0
/* new-array v1, v0, [Ljava/lang/Class; */
v2 = java.lang.Boolean.TYPE;
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
/* .line 740 */
/* .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;" */
final String v2 = "android.view.View"; // const-string v2, "android.view.View"
java.lang.Class .forName ( v2 );
/* .line 741 */
/* .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
/* const-string/jumbo v4, "setWrapped" */
(( java.lang.Class ) v2 ).getMethod ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 742 */
/* .local v4, "sGet":Ljava/lang/reflect/Method; */
/* new-array v0, v0, [Ljava/lang/Object; */
java.lang.Boolean .valueOf ( p1 );
/* aput-object v5, v0, v3 */
(( java.lang.reflect.Method ) v4 ).invoke ( p0, v0 ); // invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 745 */
/* nop */
} // .end local v1 # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
} // .end local v2 # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
} // .end local v4 # "sGet":Ljava/lang/reflect/Method;
/* .line 743 */
/* :catch_0 */
/* move-exception v0 */
/* .line 744 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setWrapped failed , e:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = ""; // const-string v2, ""
android.util.Log .e ( v2,v1 );
/* .line 746 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateHoverState ( miui.android.animation.IHoverStyle$HoverEffect p0 ) {
/* .locals 2 */
/* .param p1, "effect" # Lmiui/android/animation/IHoverStyle$HoverEffect; */
/* .line 297 */
v0 = miui.android.animation.controller.FolmeHover$2.$SwitchMap$miui$android$animation$IHoverStyle$HoverEffect;
v1 = (( miui.android.animation.IHoverStyle$HoverEffect ) p1 ).ordinal ( ); // invoke-virtual {p1}, Lmiui/android/animation/IHoverStyle$HoverEffect;->ordinal()I
/* aget v0, v0, v1 */
/* packed-switch v0, :pswitch_data_0 */
/* .line 320 */
/* :pswitch_0 */
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.NORMAL;
/* if-eq v0, v1, :cond_0 */
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED;
/* if-ne v0, v1, :cond_1 */
/* .line 321 */
} // :cond_0
(( miui.android.animation.controller.FolmeHover ) p0 ).clearTintColor ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeHover;->clearTintColor()Lmiui/android/animation/IHoverStyle;
/* .line 323 */
} // :cond_1
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoScale()V */
/* .line 324 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoTranslation()V */
/* .line 325 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoRound()V */
/* .line 326 */
this.mCurrentEffect = p1;
/* .line 311 */
/* :pswitch_1 */
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED_WRAPPED;
/* if-ne v0, v1, :cond_2 */
/* .line 312 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearRound()V */
/* .line 314 */
} // :cond_2
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V */
/* .line 315 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoScale()V */
/* .line 316 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoTranslation()V */
/* .line 317 */
this.mCurrentEffect = p1;
/* .line 318 */
/* .line 299 */
/* :pswitch_2 */
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED;
/* if-ne v0, v1, :cond_3 */
/* .line 300 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearScale()V */
/* .line 301 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearTranslation()V */
/* .line 302 */
} // :cond_3
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED_WRAPPED;
/* if-ne v0, v1, :cond_4 */
/* .line 303 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearScale()V */
/* .line 304 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearTranslation()V */
/* .line 305 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearRound()V */
/* .line 307 */
} // :cond_4
} // :goto_0
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V */
/* .line 308 */
this.mCurrentEffect = p1;
/* .line 309 */
/* nop */
/* .line 329 */
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Float valFromPer ( Float p0, Float p1, Float p2 ) {
/* .locals 1 */
/* .param p1, "per" # F */
/* .param p2, "from" # F */
/* .param p3, "to" # F */
/* .line 619 */
/* sub-float v0, p3, p2 */
/* mul-float/2addr v0, p1 */
/* add-float/2addr v0, p2 */
} // .end method
/* # virtual methods */
public void addMagicPoint ( android.graphics.Point p0 ) {
/* .locals 2 */
/* .param p1, "magicPoint" # Landroid/graphics/Point; */
/* .line 570 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 571 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 572 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .addMagicPoint ( v1,p1 );
/* .line 574 */
} // :cond_0
return;
} // .end method
public void clean ( ) {
/* .locals 2 */
/* .line 598 */
/* invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V */
/* .line 599 */
v0 = this.mScaleSetMap;
/* .line 600 */
v0 = this.mHoverView;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 601 */
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View; */
/* .line 602 */
this.mHoverView = v1;
/* .line 604 */
} // :cond_0
v0 = this.mChildView;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 605 */
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View; */
/* .line 606 */
this.mChildView = v1;
/* .line 608 */
} // :cond_1
v0 = this.mParentView;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 609 */
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View; */
/* .line 610 */
this.mParentView = v1;
/* .line 612 */
} // :cond_2
return;
} // .end method
public void clearMagicPoint ( ) {
/* .locals 2 */
/* .line 578 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 579 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 580 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .clearMagicPoint ( v1 );
/* .line 582 */
} // :cond_0
return;
} // .end method
public miui.android.animation.IHoverStyle clearTintColor ( ) {
/* .locals 3 */
/* .line 262 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z */
/* .line 263 */
v0 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* .line 264 */
/* .local v0, "propFg":Lmiui/android/animation/property/FloatProperty; */
v1 = this.mState;
v2 = miui.android.animation.IHoverStyle$HoverType.ENTER;
(( miui.android.animation.controller.AnimState ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 265 */
v1 = this.mState;
v2 = miui.android.animation.IHoverStyle$HoverType.EXIT;
(( miui.android.animation.controller.AnimState ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 266 */
} // .end method
public void handleHoverOf ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 0 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 284 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->doHandleHoverOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 285 */
return;
} // .end method
public void hoverEnter ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 9 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 450 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z */
/* .line 451 */
v1 = this.mCurrentEffect;
v2 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED_WRAPPED;
/* if-ne v1, v2, :cond_1 */
/* .line 452 */
v1 = this.mHoverView;
if ( v1 != null) { // if-eqz v1, :cond_0
(( java.lang.ref.WeakReference ) v1 ).get ( ); // invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v1, Landroid/view/View; */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 453 */
/* .local v1, "hoverView":Landroid/view/View; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 454 */
/* new-instance v2, Landroid/graphics/Rect; */
v3 = (( android.view.View ) v1 ).getLeft ( ); // invoke-virtual {v1}, Landroid/view/View;->getLeft()I
v4 = (( android.view.View ) v1 ).getTop ( ); // invoke-virtual {v1}, Landroid/view/View;->getTop()I
v5 = (( android.view.View ) v1 ).getRight ( ); // invoke-virtual {v1}, Landroid/view/View;->getRight()I
v6 = (( android.view.View ) v1 ).getBottom ( ); // invoke-virtual {v1}, Landroid/view/View;->getBottom()I
/* invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V */
this.mViewRect = v2;
/* .line 455 */
miui.android.animation.controller.FolmeHover .setMagicView ( v1,v0 );
/* .line 456 */
miui.android.animation.controller.FolmeHover .setWrapped ( v1,v0 );
/* .line 457 */
(( miui.android.animation.controller.FolmeHover ) p0 ).isHideHover ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeHover;->isHideHover()Z
/* .line 462 */
} // .end local v1 # "hoverView":Landroid/view/View;
} // :cond_1
/* iget v0, p0, Lmiui/android/animation/controller/FolmeHover;->mRadius:F */
(( miui.android.animation.controller.FolmeHover ) p0 ).setCorner ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->setCorner(F)Lmiui/android/animation/IHoverStyle;
/* .line 463 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V */
/* .line 464 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->getEnterConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig; */
/* .line 465 */
/* .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig; */
v1 = this.mState;
v2 = miui.android.animation.IHoverStyle$HoverType.ENTER;
/* .line 466 */
/* .local v1, "state":Lmiui/android/animation/controller/AnimState; */
v2 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v2 = /* invoke-direct {p0, v2}, Lmiui/android/animation/controller/FolmeHover;->isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 467 */
v2 = this.mState;
/* .line 468 */
/* .local v2, "target":Lmiui/android/animation/IAnimTarget; */
v3 = miui.android.animation.property.ViewProperty.WIDTH;
v3 = (( miui.android.animation.IAnimTarget ) v2 ).getValue ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
v4 = miui.android.animation.property.ViewProperty.HEIGHT;
/* .line 469 */
v4 = (( miui.android.animation.IAnimTarget ) v2 ).getValue ( v4 ); // invoke-virtual {v2, v4}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* .line 468 */
v3 = java.lang.Math .max ( v3,v4 );
/* .line 470 */
/* .local v3, "maxSize":F */
/* const/high16 v4, 0x41400000 # 12.0f */
/* add-float/2addr v4, v3 */
/* div-float/2addr v4, v3 */
/* const v5, 0x3f933333 # 1.15f */
v4 = java.lang.Math .min ( v4,v5 );
/* .line 471 */
/* .local v4, "scaleValue":F */
v5 = miui.android.animation.property.ViewProperty.SCALE_X;
/* float-to-double v6, v4 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v5, v6, v7 ); // invoke-virtual {v1, v5, v6, v7}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v6 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* float-to-double v7, v4 */
/* .line 472 */
(( miui.android.animation.controller.AnimState ) v5 ).add ( v6, v7, v8 ); // invoke-virtual {v5, v6, v7, v8}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 475 */
} // .end local v2 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v3 # "maxSize":F
} // .end local v4 # "scaleValue":F
} // :cond_2
v2 = this.mParentView;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 476 */
(( java.lang.ref.WeakReference ) v2 ).get ( ); // invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v2, Landroid/view/View; */
/* filled-new-array {v2}, [Landroid/view/View; */
miui.android.animation.Folme .useAt ( v2 );
/* .line 477 */
v3 = miui.android.animation.property.ViewProperty.SCALE_X;
/* .line 478 */
/* const/high16 v4, 0x3f800000 # 1.0f */
v3 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* .line 479 */
/* .line 480 */
/* .line 482 */
} // :cond_3
v2 = this.mState;
/* .line 483 */
return;
} // .end method
public void hoverExit ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 4 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 504 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->getExitConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig; */
/* .line 505 */
/* .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig; */
v1 = this.mState;
v2 = this.mState;
v3 = miui.android.animation.IHoverStyle$HoverType.EXIT;
/* .line 506 */
return;
} // .end method
public void hoverMove ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 0 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 487 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 488 */
return;
} // .end method
public void ignoreHoverOf ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 432 */
v0 = miui.android.animation.controller.FolmeHover.sHoverRecord;
(( java.util.WeakHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener; */
/* .line 433 */
/* .local v0, "hoverListener":Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( miui.android.animation.controller.FolmeHover$InnerViewHoverListener ) v0 ).removeHover ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;->removeHover(Lmiui/android/animation/controller/FolmeHover;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 434 */
v1 = miui.android.animation.controller.FolmeHover.sHoverRecord;
(( java.util.WeakHashMap ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 436 */
} // :cond_0
return;
} // .end method
public Boolean isHideHover ( ) {
/* .locals 2 */
/* .line 692 */
/* iget v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I */
/* const/16 v1, 0x64 */
/* if-ge v0, v1, :cond_1 */
/* iget v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I */
/* if-ge v0, v1, :cond_1 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED;
/* if-eq v0, v1, :cond_0 */
v0 = this.mCurrentEffect;
v1 = miui.android.animation.IHoverStyle$HoverEffect.FLOATED_WRAPPED;
/* if-ne v0, v1, :cond_1 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isMagicView ( ) {
/* .locals 2 */
/* .line 521 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 522 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 523 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
v1 = miui.android.animation.controller.FolmeHover .isMagicView ( v1 );
/* .line 525 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void onMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 445 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Lmiui/android/animation/base/AnimConfig; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1, p1, v0}, Lmiui/android/animation/controller/FolmeHover;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 446 */
return;
} // .end method
public void onMotionEventEx ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 0 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 440 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 441 */
return;
} // .end method
public miui.android.animation.IHoverStyle setAlpha ( Float p0, miui.android.animation.IHoverStyle$HoverType...p1 ) {
/* .locals 4 */
/* .param p1, "alpha" # F */
/* .param p2, "type" # [Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 167 */
v0 = this.mState;
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType; */
v1 = miui.android.animation.property.ViewProperty.ALPHA;
/* float-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 168 */
} // .end method
public miui.android.animation.IHoverStyle setBackgroundColor ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 4 */
/* .param p1, "a" # F */
/* .param p2, "r" # F */
/* .param p3, "g" # F */
/* .param p4, "b" # F */
/* .line 218 */
/* const/high16 v0, 0x437f0000 # 255.0f */
/* mul-float v1, p1, v0 */
/* float-to-int v1, v1 */
/* mul-float v2, p2, v0 */
/* float-to-int v2, v2 */
/* mul-float v3, p3, v0 */
/* float-to-int v3, v3 */
/* mul-float/2addr v0, p4 */
/* float-to-int v0, v0 */
v0 = android.graphics.Color .argb ( v1,v2,v3,v0 );
(( miui.android.animation.controller.FolmeHover ) p0 ).setBackgroundColor ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->setBackgroundColor(I)Lmiui/android/animation/IHoverStyle;
} // .end method
public miui.android.animation.IHoverStyle setBackgroundColor ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "color" # I */
/* .line 224 */
v0 = miui.android.animation.property.ViewPropertyExt.BACKGROUND;
/* .line 225 */
/* .local v0, "propBg":Lmiui/android/animation/property/FloatProperty; */
v1 = this.mState;
v2 = miui.android.animation.IHoverStyle$HoverType.ENTER;
/* int-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 226 */
v1 = this.mState;
v2 = miui.android.animation.IHoverStyle$HoverType.EXIT;
v2 = this.mState;
/* .line 227 */
/* const-wide/16 v3, 0x0 */
miui.android.animation.internal.AnimValueUtils .getValueOfTarget ( v2,v0,v3,v4 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* int-to-double v2, v2 */
/* .line 226 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 228 */
} // .end method
public miui.android.animation.IHoverStyle setCorner ( Float p0 ) {
/* .locals 4 */
/* .param p1, "radius" # F */
/* .line 194 */
/* iput p1, p0, Lmiui/android/animation/controller/FolmeHover;->mRadius:F */
/* .line 195 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 196 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 197 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
/* const v2, 0x100b0008 */
java.lang.Float .valueOf ( p1 );
(( android.view.View ) v1 ).setTag ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 199 */
} // :cond_0
} // .end method
public miui.android.animation.IHoverStyle setEffect ( miui.android.animation.IHoverStyle$HoverEffect p0 ) {
/* .locals 0 */
/* .param p1, "effect" # Lmiui/android/animation/IHoverStyle$HoverEffect; */
/* .line 278 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->updateHoverState(Lmiui/android/animation/IHoverStyle$HoverEffect;)V */
/* .line 279 */
} // .end method
public void setHoverEnter ( ) {
/* .locals 2 */
/* .line 510 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V */
/* .line 511 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
/* .line 512 */
return;
} // .end method
public void setHoverExit ( ) {
/* .locals 2 */
/* .line 516 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.EXIT;
/* .line 517 */
return;
} // .end method
public void setMagicView ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isMagicView" # Z */
/* .line 530 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 531 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 532 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .setMagicView ( v1,p1 );
/* .line 534 */
} // :cond_0
return;
} // .end method
public miui.android.animation.IHoverStyle setParentView ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "parent" # Landroid/view/View; */
/* .line 157 */
v0 = this.mParentView;
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 158 */
/* .local v0, "parentView":Landroid/view/View; */
} // :goto_0
/* if-ne p1, v0, :cond_1 */
/* .line 159 */
/* .line 161 */
} // :cond_1
/* new-instance v1, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mParentView = v1;
/* .line 162 */
} // .end method
public void setPointerHide ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isWrapped" # Z */
/* .line 546 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 547 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 548 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .setPointerHide ( v1,p1 );
/* .line 550 */
} // :cond_0
return;
} // .end method
public void setPointerShape ( android.graphics.Bitmap p0 ) {
/* .locals 2 */
/* .param p1, "pointerShape" # Landroid/graphics/Bitmap; */
/* .line 554 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 555 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 556 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .setPointerShape ( v1,p1 );
/* .line 558 */
} // :cond_0
return;
} // .end method
public void setPointerShapeType ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "pointerShapeType" # I */
/* .line 562 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 563 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 564 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .setPointerShapeType ( v1,p1 );
/* .line 566 */
} // :cond_0
return;
} // .end method
public miui.android.animation.IHoverStyle setScale ( Float p0, miui.android.animation.IHoverStyle$HoverType...p1 ) {
/* .locals 5 */
/* .param p1, "scale" # F */
/* .param p2, "type" # [Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 173 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 174 */
/* .local v0, "relType":Lmiui/android/animation/IHoverStyle$HoverType; */
v1 = this.mScaleSetMap;
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
/* .line 175 */
v1 = this.mState;
v2 = miui.android.animation.property.ViewProperty.SCALE_X;
/* float-to-double v3, p1 */
/* .line 176 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v2 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* float-to-double v3, p1 */
/* .line 177 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 178 */
} // .end method
public miui.android.animation.IHoverStyle setTint ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 4 */
/* .param p1, "a" # F */
/* .param p2, "r" # F */
/* .param p3, "g" # F */
/* .param p4, "b" # F */
/* .line 212 */
/* const/high16 v0, 0x437f0000 # 255.0f */
/* mul-float v1, p1, v0 */
/* float-to-int v1, v1 */
/* mul-float v2, p2, v0 */
/* float-to-int v2, v2 */
/* mul-float v3, p3, v0 */
/* float-to-int v3, v3 */
/* mul-float/2addr v0, p4 */
/* float-to-int v0, v0 */
v0 = android.graphics.Color .argb ( v1,v2,v3,v0 );
(( miui.android.animation.controller.FolmeHover ) p0 ).setTint ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->setTint(I)Lmiui/android/animation/IHoverStyle;
} // .end method
public miui.android.animation.IHoverStyle setTint ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "color" # I */
/* .line 204 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mSetTint:Z */
/* .line 205 */
/* if-nez p1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z */
/* .line 206 */
v0 = this.mState;
v1 = miui.android.animation.IHoverStyle$HoverType.ENTER;
v1 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* int-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 207 */
} // .end method
public miui.android.animation.IHoverStyle setTintMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 271 */
v0 = this.mEnterConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).setTintMode ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;
/* .line 272 */
v0 = this.mExitConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).setTintMode ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;
/* .line 273 */
} // .end method
public miui.android.animation.IHoverStyle setTranslate ( Float p0, miui.android.animation.IHoverStyle$HoverType...p1 ) {
/* .locals 5 */
/* .param p1, "translate" # F */
/* .param p2, "type" # [Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 183 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z */
/* .line 184 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType; */
/* .line 185 */
/* .local v0, "relType":Lmiui/android/animation/IHoverStyle$HoverType; */
v1 = this.mTranslationSetMap;
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
/* .line 186 */
v1 = this.mState;
v2 = miui.android.animation.property.ViewProperty.TRANSLATION_X;
/* float-to-double v3, p1 */
/* .line 187 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v2 = miui.android.animation.property.ViewProperty.TRANSLATION_Y;
/* float-to-double v3, p1 */
/* .line 188 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 189 */
} // .end method
public void setWrapped ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isWrapped" # Z */
/* .line 538 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 539 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 540 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
miui.android.animation.controller.FolmeHover .setWrapped ( v1,p1 );
/* .line 542 */
} // :cond_0
return;
} // .end method
