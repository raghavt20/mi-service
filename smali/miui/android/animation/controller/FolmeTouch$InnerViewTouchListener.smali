.class Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;
.super Ljava/lang/Object;
.source "FolmeTouch.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/controller/FolmeTouch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InnerViewTouchListener"
.end annotation


# instance fields
.field private mTouchMap:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Lmiui/android/animation/controller/FolmeTouch;",
            "[",
            "Lmiui/android/animation/base/AnimConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 393
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->mTouchMap:Ljava/util/WeakHashMap;

    return-void
.end method

.method synthetic constructor <init>(Lmiui/android/animation/controller/FolmeTouch$1;)V
    .locals 0
    .param p1, "x0"    # Lmiui/android/animation/controller/FolmeTouch$1;

    .line 392
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;-><init>()V

    return-void
.end method


# virtual methods
.method varargs addTouch(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "folmeTouch"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p2, "configs"    # [Lmiui/android/animation/base/AnimConfig;

    .line 396
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->mTouchMap:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 406
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->mTouchMap:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 407
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/controller/FolmeTouch;

    .line 408
    .local v2, "folmeTouch":Lmiui/android/animation/controller/FolmeTouch;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lmiui/android/animation/base/AnimConfig;

    .line 409
    .local v3, "configs":[Lmiui/android/animation/base/AnimConfig;
    invoke-static {v2, p1, p2, v3}, Lmiui/android/animation/controller/FolmeTouch;->access$700(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 410
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;>;"
    .end local v2    # "folmeTouch":Lmiui/android/animation/controller/FolmeTouch;
    .end local v3    # "configs":[Lmiui/android/animation/base/AnimConfig;
    goto :goto_0

    .line 411
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method removeTouch(Lmiui/android/animation/controller/FolmeTouch;)Z
    .locals 1
    .param p1, "folmeTouch"    # Lmiui/android/animation/controller/FolmeTouch;

    .line 400
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->mTouchMap:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->mTouchMap:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z

    move-result v0

    return v0
.end method
