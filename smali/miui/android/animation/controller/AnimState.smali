.class public Lmiui/android/animation/controller/AnimState;
.super Ljava/lang/Object;
.source "AnimState.java"


# static fields
.field public static final FLAG_IN_TOUCH:J = 0x4L

.field public static final FLAG_PARALLEL:J = 0x2L

.field public static final FLAG_QUEUE:J = 0x1L

.field private static final STEP:I = 0x64

.field private static final TAG:Ljava/lang/String; = "TAG_"

.field public static final VIEW_POS:I = 0xf42a4

.field public static final VIEW_SIZE:I = 0xf4240

.field private static final sId:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public flags:J

.field public final isTemporary:Z

.field private final mConfig:Lmiui/android/animation/base/AnimConfig;

.field private final mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mTag:Ljava/lang/Object;

.field tempIntValueProperty:Lmiui/android/animation/property/IntValueProperty;

.field tempValueProperty:Lmiui/android/animation/property/ValueProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiui/android/animation/controller/AnimState;->sId:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 86
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "t"    # Ljava/lang/Object;

    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Object;
    .param p2, "temporary"    # Z

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lmiui/android/animation/property/ValueProperty;

    const-string v1, ""

    invoke-direct {v0, v1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lmiui/android/animation/controller/AnimState;->tempValueProperty:Lmiui/android/animation/property/ValueProperty;

    .line 45
    new-instance v0, Lmiui/android/animation/property/IntValueProperty;

    invoke-direct {v0, v1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lmiui/android/animation/controller/AnimState;->tempIntValueProperty:Lmiui/android/animation/property/IntValueProperty;

    .line 54
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    .line 55
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    .line 94
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/AnimState;->setTag(Ljava/lang/Object;)V

    .line 95
    iput-boolean p2, p0, Lmiui/android/animation/controller/AnimState;->isTemporary:Z

    .line 96
    return-void
.end method

.method public static alignState(Lmiui/android/animation/controller/AnimState;Ljava/util/Collection;)V
    .locals 5
    .param p0, "state"    # Lmiui/android/animation/controller/AnimState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/android/animation/controller/AnimState;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 58
    .local p1, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/listener/UpdateInfo;

    .line 59
    .local v1, "update":Lmiui/android/animation/listener/UpdateInfo;
    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {p0, v2}, Lmiui/android/animation/controller/AnimState;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 60
    iget-boolean v2, v1, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z

    if-eqz v2, :cond_0

    .line 61
    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    iget-object v3, v1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v3, v3, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    double-to-int v3, v3

    int-to-double v3, v3

    invoke-virtual {p0, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    goto :goto_1

    .line 63
    :cond_0
    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    iget-object v3, v1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v3, v3, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    double-to-float v3, v3

    float-to-double v3, v3

    invoke-virtual {p0, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 66
    .end local v1    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :cond_1
    :goto_1
    goto :goto_0

    .line 67
    :cond_2
    const-class v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/ObjectPool;->acquire(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 68
    .local v0, "delList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 70
    .local v2, "key":Ljava/lang/Object;
    instance-of v3, v2, Lmiui/android/animation/property/FloatProperty;

    if-eqz v3, :cond_3

    .line 71
    move-object v3, v2

    check-cast v3, Lmiui/android/animation/property/FloatProperty;

    invoke-static {p1, v3}, Lmiui/android/animation/listener/UpdateInfo;->findBy(Ljava/util/Collection;Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;

    move-result-object v3

    .local v3, "update":Lmiui/android/animation/listener/UpdateInfo;
    goto :goto_3

    .line 73
    .end local v3    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :cond_3
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, v3}, Lmiui/android/animation/listener/UpdateInfo;->findByName(Ljava/util/Collection;Ljava/lang/String;)Lmiui/android/animation/listener/UpdateInfo;

    move-result-object v3

    .line 75
    .restart local v3    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :goto_3
    if-nez v3, :cond_4

    .line 76
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .end local v2    # "key":Ljava/lang/Object;
    .end local v3    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :cond_4
    goto :goto_2

    .line 79
    :cond_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 80
    .restart local v2    # "key":Ljava/lang/Object;
    invoke-virtual {p0, v2}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 81
    .end local v2    # "key":Ljava/lang/Object;
    goto :goto_4

    .line 82
    :cond_6
    invoke-static {v0}, Lmiui/android/animation/utils/ObjectPool;->release(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method private append(Lmiui/android/animation/controller/AnimState;)V
    .locals 2
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;

    .line 116
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    iget-object v1, p1, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V

    .line 117
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 118
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    iget-object v1, p1, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 119
    return-void
.end method

.method private getMapValue(Ljava/lang/Object;)Ljava/lang/Double;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    .line 234
    .local v0, "value":Ljava/lang/Double;
    if-nez v0, :cond_0

    instance-of v1, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v1, :cond_0

    .line 235
    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    move-object v2, p1

    check-cast v2, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/lang/Double;

    .line 237
    :cond_0
    return-object v0
.end method

.method private getProperValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D
    .locals 7
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "value"    # D

    .line 249
    invoke-virtual {p0, p2}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J

    move-result-wide v0

    .line 250
    .local v0, "flag":J
    const-wide/16 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lmiui/android/animation/utils/CommonUtils;->hasFlags(JJ)Z

    move-result v2

    .line 251
    .local v2, "isDelta":Z
    if-nez v2, :cond_1

    const-wide v3, 0x412e848000000000L    # 1000000.0

    cmpl-double v3, p3, v3

    if-eqz v3, :cond_1

    const-wide v3, 0x412e854800000000L    # 1000100.0

    cmpl-double v3, p3, v3

    if-eqz v3, :cond_1

    instance-of v3, p2, Lmiui/android/animation/property/ISpecificProperty;

    if-eqz v3, :cond_0

    goto :goto_0

    .line 261
    :cond_0
    return-wide p3

    .line 253
    :cond_1
    :goto_0
    invoke-static {p1, p2, p3, p4}, Lmiui/android/animation/internal/AnimValueUtils;->getValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v3

    .line 254
    .local v3, "curValue":D
    if-eqz v2, :cond_2

    invoke-static {p3, p4}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v5

    if-nez v5, :cond_2

    .line 255
    const-wide/16 v5, -0x2

    and-long/2addr v5, v0

    invoke-virtual {p0, p2, v5, v6}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 256
    add-double/2addr v3, p3

    .line 257
    invoke-direct {p0, p2, v3, v4}, Lmiui/android/animation/controller/AnimState;->setMapValue(Ljava/lang/Object;D)V

    .line 259
    :cond_2
    return-wide v3
.end method

.method private setMapValue(Ljava/lang/Object;D)V
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # D

    .line 241
    instance-of v0, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    move-object v1, p1

    check-cast v1, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    move-object v1, p1

    check-cast v1, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 244
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    :goto_0
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
    .locals 0
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "value"    # D

    .line 166
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/AnimState;->setMapValue(Ljava/lang/Object;D)V

    .line 167
    return-object p0
.end method

.method public varargs add(Ljava/lang/String;F[J)Lmiui/android/animation/controller/AnimState;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # F
    .param p3, "flags"    # [J

    .line 126
    array-length v0, p3

    if-lez v0, :cond_0

    .line 127
    const/4 v0, 0x0

    aget-wide v0, p3, v0

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 129
    :cond_0
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs add(Ljava/lang/String;I[J)Lmiui/android/animation/controller/AnimState;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I
    .param p3, "flags"    # [J

    .line 133
    array-length v0, p3

    const-wide/16 v1, 0x4

    if-lez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    aget-wide v3, p3, v0

    or-long v0, v3, v1

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    goto :goto_0

    .line 136
    :cond_0
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J

    move-result-wide v3

    or-long v0, v3, v1

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 138
    :goto_0
    int-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs add(Lmiui/android/animation/property/FloatProperty;F[J)Lmiui/android/animation/controller/AnimState;
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F
    .param p3, "flags"    # [J

    .line 150
    array-length v0, p3

    if-lez v0, :cond_0

    .line 151
    const/4 v0, 0x0

    aget-wide v0, p3, v0

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 153
    :cond_0
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs add(Lmiui/android/animation/property/FloatProperty;I[J)Lmiui/android/animation/controller/AnimState;
    .locals 5
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I
    .param p3, "flags"    # [J

    .line 157
    array-length v0, p3

    const-wide/16 v1, 0x4

    if-lez v0, :cond_0

    .line 158
    const/4 v0, 0x0

    aget-wide v3, p3, v0

    or-long v0, v3, v1

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    goto :goto_0

    .line 160
    :cond_0
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J

    move-result-wide v3

    or-long v0, v3, v1

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 162
    :goto_0
    int-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs add(Lmiui/android/animation/property/ViewProperty;F[J)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/ViewProperty;
    .param p2, "value"    # F
    .param p3, "flags"    # [J

    .line 142
    invoke-virtual {p0, p1, p2, p3}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/FloatProperty;F[J)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs add(Lmiui/android/animation/property/ViewProperty;I[J)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/ViewProperty;
    .param p2, "value"    # I
    .param p3, "flags"    # [J

    .line 146
    invoke-virtual {p0, p1, p2, p3}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/FloatProperty;I[J)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public clear()V
    .locals 1

    .line 103
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0}, Lmiui/android/animation/base/AnimConfig;->clear()V

    .line 104
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 105
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "property"    # Ljava/lang/Object;

    .line 179
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 180
    return v0

    .line 182
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    const/4 v0, 0x1

    return v0

    .line 185
    :cond_1
    instance-of v1, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v1, :cond_2

    .line 186
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    move-object v1, p1

    check-cast v1, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 188
    :cond_2
    return v0
.end method

.method public get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D
    .locals 3
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 225
    invoke-direct {p0, p2}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v0

    .line 226
    .local v0, "value":Ljava/lang/Double;
    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-direct {p0, p1, p2, v1, v2}, Lmiui/android/animation/controller/AnimState;->getProperValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v1

    return-wide v1

    .line 229
    :cond_0
    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    return-wide v1
.end method

.method public getConfig()Lmiui/android/animation/base/AnimConfig;
    .locals 1

    .line 274
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    return-object v0
.end method

.method public getConfigFlags(Ljava/lang/Object;)J
    .locals 4
    .param p1, "key"    # Ljava/lang/Object;

    .line 268
    instance-of v0, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v0}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    .line 269
    .local v0, "name":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v1, v0}, Lmiui/android/animation/base/AnimConfig;->getSpecialConfig(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v1

    .line 270
    .local v1, "config":Lmiui/android/animation/base/AnimSpecialConfig;
    if-eqz v1, :cond_1

    iget-wide v2, v1, Lmiui/android/animation/base/AnimSpecialConfig;->flags:J

    goto :goto_1

    :cond_1
    const-wide/16 v2, 0x0

    :goto_1
    return-wide v2
.end method

.method public getFloat(Ljava/lang/String;)F
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 220
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v0

    .line 221
    .local v0, "value":Ljava/lang/Double;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v1

    goto :goto_0

    :cond_0
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    :goto_0
    return v1
.end method

.method public getFloat(Lmiui/android/animation/property/FloatProperty;)F
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 215
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v0

    .line 216
    .local v0, "value":Ljava/lang/Double;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v1

    goto :goto_0

    :cond_0
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    :goto_0
    return v1
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 211
    new-instance v0, Lmiui/android/animation/property/IntValueProperty;

    invoke-direct {v0, p1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->getInt(Lmiui/android/animation/property/IIntValueProperty;)I

    move-result v0

    return v0
.end method

.method public getInt(Lmiui/android/animation/property/IIntValueProperty;)I
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/IIntValueProperty;

    .line 206
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v0

    .line 207
    .local v0, "value":Ljava/lang/Double;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    const v1, 0x7fffffff

    :goto_0
    return v1
.end method

.method public getProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
    .locals 5
    .param p1, "key"    # Ljava/lang/Object;

    .line 289
    instance-of v0, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v0, :cond_0

    .line 290
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/FloatProperty;

    return-object v0

    .line 293
    :cond_0
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    .line 294
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J

    move-result-wide v1

    const-wide/16 v3, 0x4

    invoke-static {v1, v2, v3, v4}, Lmiui/android/animation/utils/CommonUtils;->hasFlags(JJ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    new-instance v1, Lmiui/android/animation/property/IntValueProperty;

    invoke-direct {v1, v0}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V

    .local v1, "property":Lmiui/android/animation/property/FloatProperty;
    goto :goto_0

    .line 297
    .end local v1    # "property":Lmiui/android/animation/property/FloatProperty;
    :cond_1
    new-instance v1, Lmiui/android/animation/property/ValueProperty;

    invoke-direct {v1, v0}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V

    .line 299
    .restart local v1    # "property":Lmiui/android/animation/property/FloatProperty;
    :goto_0
    return-object v1
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .line 122
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public getTempProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
    .locals 5
    .param p1, "key"    # Ljava/lang/Object;

    .line 307
    instance-of v0, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v0, :cond_0

    .line 308
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/FloatProperty;

    return-object v0

    .line 310
    :cond_0
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    .line 311
    .local v0, "name":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J

    move-result-wide v1

    const-wide/16 v3, 0x4

    invoke-static {v1, v2, v3, v4}, Lmiui/android/animation/utils/CommonUtils;->hasFlags(JJ)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->tempIntValueProperty:Lmiui/android/animation/property/IntValueProperty;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->tempValueProperty:Lmiui/android/animation/property/ValueProperty;

    .line 313
    .local v1, "property":Lmiui/android/animation/property/ValueProperty;
    :goto_0
    move-object v2, p1

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Lmiui/android/animation/property/ValueProperty;->setName(Ljava/lang/String;)V

    .line 314
    return-object v1
.end method

.method public isEmpty()Z
    .locals 1

    .line 195
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 202
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;

    .line 278
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    instance-of v0, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    move-object v1, p1

    check-cast v1, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    :cond_0
    return-object p0
.end method

.method public set(Lmiui/android/animation/controller/AnimState;)V
    .locals 1
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;

    .line 108
    if-nez p1, :cond_0

    .line 109
    return-void

    .line 111
    :cond_0
    iget-object v0, p1, Lmiui/android/animation/controller/AnimState;->mTag:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->setTag(Ljava/lang/Object;)V

    .line 112
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->append(Lmiui/android/animation/controller/AnimState;)V

    .line 113
    return-void
.end method

.method public setConfigFlag(Ljava/lang/Object;J)V
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "flag"    # J

    .line 171
    instance-of v0, p1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v0}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    .line 172
    .local v0, "name":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v1

    iput-wide p2, v1, Lmiui/android/animation/base/AnimSpecialConfig;->flags:J

    .line 173
    return-void
.end method

.method public final setTag(Ljava/lang/Object;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;

    .line 99
    if-eqz p1, :cond_0

    move-object v0, p1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TAG_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/controller/AnimState;->sId:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lmiui/android/animation/controller/AnimState;->mTag:Ljava/lang/Object;

    .line 100
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 320
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nAnimState{mTag=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/controller/AnimState;->flags:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/AnimState;->mMap:Ljava/util/Map;

    .line 322
    const-string v2, "    "

    invoke-static {v1, v2}, Lmiui/android/animation/utils/CommonUtils;->mapToString(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 320
    return-object v0
.end method
