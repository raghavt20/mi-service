public class miui.android.animation.controller.FolmeFont extends miui.android.animation.controller.FolmeBase implements miui.android.animation.IVarFontStyle {
	 /* .source "FolmeFont.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/controller/FolmeFont$FontType; */
	 /* } */
} // .end annotation
/* # instance fields */
private miui.android.animation.base.AnimConfig mDefaultTo;
private Integer mInitValue;
private Boolean mIsInitSet;
private miui.android.animation.font.FontWeightProperty mProperty;
/* # direct methods */
public miui.android.animation.controller.FolmeFont ( ) {
	 /* .locals 3 */
	 /* .line 25 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* new-array v1, v0, [Lmiui/android/animation/IAnimTarget; */
	 /* invoke-direct {p0, v1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V */
	 /* .line 21 */
	 /* new-instance v1, Lmiui/android/animation/base/AnimConfig; */
	 /* invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
	 this.mDefaultTo = v1;
	 /* .line 26 */
	 int v2 = 3; // const/4 v2, 0x3
	 /* new-array v2, v2, [F */
	 /* fill-array-data v2, :array_0 */
	 miui.android.animation.utils.EaseManager .getStyle ( v0,v2 );
	 (( miui.android.animation.base.AnimConfig ) v1 ).setEase ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
	 /* .line 27 */
	 return;
	 /* nop */
	 /* :array_0 */
	 /* .array-data 4 */
	 /* 0x43af0000 # 350.0f */
	 /* 0x3f666666 # 0.9f */
	 /* 0x3f5c28f6 # 0.86f */
} // .end array-data
} // .end method
/* # virtual methods */
public void clean ( ) {
/* .locals 1 */
/* .line 41 */
/* invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V */
/* .line 42 */
int v0 = 0; // const/4 v0, 0x0
this.mState = v0;
/* .line 43 */
this.mProperty = v0;
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/controller/FolmeFont;->mInitValue:I */
/* .line 45 */
return;
} // .end method
public miui.android.animation.IVarFontStyle fromTo ( Integer p0, Integer p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 4 */
/* .param p1, "fromFontWeight" # I */
/* .param p2, "toFontWeight" # I */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 76 */
v0 = this.mState;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 77 */
	 v0 = this.mState;
	 v1 = miui.android.animation.controller.FolmeFont$FontType.INIT;
	 v1 = this.mProperty;
	 /* int-to-double v2, p1 */
	 (( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
	 /* .line 78 */
	 v0 = this.mState;
	 v1 = miui.android.animation.controller.FolmeFont$FontType.TARGET;
	 v1 = this.mProperty;
	 /* int-to-double v2, p2 */
	 (( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
	 /* .line 79 */
	 v0 = this.mState;
	 v1 = miui.android.animation.controller.FolmeFont$FontType.INIT;
	 v2 = miui.android.animation.controller.FolmeFont$FontType.TARGET;
	 /* .line 81 */
} // :cond_0
} // .end method
public miui.android.animation.IVarFontStyle setTo ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "toFontWeight" # I */
/* .line 67 */
v0 = this.mState;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 68 */
	 v0 = this.mState;
	 v1 = miui.android.animation.controller.FolmeFont$FontType.TARGET;
	 v1 = this.mProperty;
	 /* int-to-double v2, p1 */
	 (( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
	 /* .line 69 */
	 v0 = this.mState;
	 v1 = miui.android.animation.controller.FolmeFont$FontType.TARGET;
	 /* .line 71 */
} // :cond_0
} // .end method
public miui.android.animation.IVarFontStyle to ( Integer p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 5 */
/* .param p1, "toFontWeight" # I */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 49 */
v0 = this.mState;
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 50 */
	 /* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeFont;->mIsInitSet:Z */
	 /* if-nez v0, :cond_0 */
	 /* .line 51 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeFont;->mIsInitSet:Z */
	 /* .line 52 */
	 v0 = this.mState;
	 v1 = miui.android.animation.controller.FolmeFont$FontType.INIT;
	 /* .line 54 */
} // :cond_0
v0 = this.mDefaultTo;
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.utils.CommonUtils .mergeArray ( p2,v0 );
/* check-cast v0, [Lmiui/android/animation/base/AnimConfig; */
/* .line 55 */
/* .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig; */
/* iget v1, p0, Lmiui/android/animation/controller/FolmeFont;->mInitValue:I */
/* if-ne v1, p1, :cond_1 */
/* .line 56 */
v1 = this.mState;
v2 = miui.android.animation.controller.FolmeFont$FontType.INIT;
/* .line 58 */
} // :cond_1
v1 = this.mState;
v2 = miui.android.animation.controller.FolmeFont$FontType.TARGET;
v2 = this.mProperty;
/* int-to-double v3, p1 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 59 */
v1 = this.mState;
v2 = miui.android.animation.controller.FolmeFont$FontType.TARGET;
/* .line 62 */
} // .end local v0 # "configArray":[Lmiui/android/animation/base/AnimConfig;
} // :cond_2
} // :goto_0
} // .end method
public miui.android.animation.IVarFontStyle useAt ( android.widget.TextView p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "textView" # Landroid/widget/TextView; */
/* .param p2, "initFontType" # I */
/* .param p3, "fromFontWeight" # I */
/* .line 31 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeState; */
v1 = miui.android.animation.ViewTarget.sCreator;
miui.android.animation.Folme .getTarget ( p1,v1 );
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeState;-><init>(Lmiui/android/animation/IAnimTarget;)V */
this.mState = v0;
/* .line 32 */
/* new-instance v0, Lmiui/android/animation/font/FontWeightProperty; */
/* invoke-direct {v0, p1, p2}, Lmiui/android/animation/font/FontWeightProperty;-><init>(Landroid/widget/TextView;I)V */
this.mProperty = v0;
/* .line 33 */
/* iput p3, p0, Lmiui/android/animation/controller/FolmeFont;->mInitValue:I */
/* .line 34 */
v0 = this.mState;
v1 = miui.android.animation.controller.FolmeFont$FontType.INIT;
v1 = this.mProperty;
/* int-to-double v2, p3 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 35 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeFont;->mIsInitSet:Z */
/* .line 36 */
} // .end method
