.class Lmiui/android/animation/controller/StateHelper;
.super Ljava/lang/Object;
.source "StateHelper.java"


# static fields
.field static final DEFAULT_INT_PROPERTY:Lmiui/android/animation/property/IntValueProperty;

.field static final DEFAULT_PROPERTY:Lmiui/android/animation/property/ValueProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 19
    new-instance v0, Lmiui/android/animation/property/ValueProperty;

    const-string v1, "defaultProperty"

    invoke-direct {v0, v1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/android/animation/controller/StateHelper;->DEFAULT_PROPERTY:Lmiui/android/animation/property/ValueProperty;

    .line 20
    new-instance v0, Lmiui/android/animation/property/IntValueProperty;

    const-string v1, "defaultIntProperty"

    invoke-direct {v0, v1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/android/animation/controller/StateHelper;->DEFAULT_INT_PROPERTY:Lmiui/android/animation/property/IntValueProperty;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addConfigToLink(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p2, "obj"    # Ljava/lang/Object;

    .line 80
    instance-of v0, p2, Lmiui/android/animation/base/AnimConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 81
    move-object v0, p2

    check-cast v0, Lmiui/android/animation/base/AnimConfig;

    new-array v1, v1, [Z

    invoke-virtual {p1, v0, v1}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V

    .line 82
    const/4 v0, 0x1

    return v0

    .line 83
    :cond_0
    instance-of v0, p2, Lmiui/android/animation/base/AnimConfigLink;

    if-eqz v0, :cond_1

    .line 84
    move-object v0, p2

    check-cast v0, Lmiui/android/animation/base/AnimConfigLink;

    new-array v2, v1, [Z

    invoke-virtual {p1, v0, v2}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfigLink;[Z)V

    .line 86
    :cond_1
    return v1
.end method

.method private varargs addProperty(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/property/FloatProperty;I[Ljava/lang/Object;)I
    .locals 3
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p3, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p4, "index"    # I
    .param p5, "propertyAndValues"    # [Ljava/lang/Object;

    .line 109
    const/4 v0, 0x0

    .line 111
    .local v0, "delta":I
    if-eqz p3, :cond_0

    .line 112
    invoke-direct {p0, p4, p5}, Lmiui/android/animation/controller/StateHelper;->getPropertyValue(I[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    .local v2, "value":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 113
    invoke-direct {p0, p2, p3, v2}, Lmiui/android/animation/controller/StateHelper;->addPropertyValue(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/property/FloatProperty;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    add-int/lit8 v0, v0, 0x1

    .line 115
    add-int/lit8 v1, p4, 0x1

    invoke-direct {p0, p1, p3, v1, p5}, Lmiui/android/animation/controller/StateHelper;->setInitVelocity(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;I[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    add-int/lit8 v0, v0, 0x1

    .line 119
    .end local v2    # "value":Ljava/lang/Object;
    :cond_0
    return v0
.end method

.method private addPropertyValue(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/property/FloatProperty;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "value"    # Ljava/lang/Object;

    .line 140
    instance-of v0, p3, Ljava/lang/Integer;

    .line 141
    .local v0, "isInt":Z
    if-nez v0, :cond_1

    instance-of v1, p3, Ljava/lang/Float;

    if-nez v1, :cond_1

    instance-of v1, p3, Ljava/lang/Double;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 149
    :cond_0
    const/4 v1, 0x0

    return v1

    .line 142
    :cond_1
    :goto_0
    instance-of v1, p2, Lmiui/android/animation/property/IIntValueProperty;

    if-eqz v1, :cond_2

    .line 143
    invoke-direct {p0, p3, v0}, Lmiui/android/animation/controller/StateHelper;->toInt(Ljava/lang/Object;Z)I

    move-result v1

    int-to-double v1, v1

    invoke-virtual {p1, p2, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    goto :goto_1

    .line 145
    :cond_2
    invoke-direct {p0, p3, v0}, Lmiui/android/animation/controller/StateHelper;->toFloat(Ljava/lang/Object;Z)F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, p2, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 147
    :goto_1
    const/4 v1, 0x1

    return v1
.end method

.method private checkAndSetAnimConfig(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z
    .locals 6
    .param p1, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p2, "obj"    # Ljava/lang/Object;

    .line 56
    instance-of v0, p2, Lmiui/android/animation/listener/TransitionListener;

    const/4 v1, 0x1

    if-nez v0, :cond_5

    instance-of v0, p2, Lmiui/android/animation/utils/EaseManager$EaseStyle;

    if-eqz v0, :cond_0

    goto :goto_3

    .line 59
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "ret":Z
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 62
    invoke-static {p2, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v4

    .line 63
    .local v4, "element":Ljava/lang/Object;
    invoke-direct {p0, p1, v4}, Lmiui/android/animation/controller/StateHelper;->addConfigToLink(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    move v5, v1

    :goto_2
    move v0, v5

    .line 61
    .end local v4    # "element":Ljava/lang/Object;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 65
    .end local v2    # "i":I
    .end local v3    # "n":I
    :cond_3
    return v0

    .line 67
    .end local v0    # "ret":Z
    :cond_4
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/StateHelper;->addConfigToLink(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 57
    :cond_5
    :goto_3
    invoke-virtual {p1}, Lmiui/android/animation/base/AnimConfigLink;->getHead()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lmiui/android/animation/controller/StateHelper;->setTempConfig(Lmiui/android/animation/base/AnimConfig;Ljava/lang/Object;)V

    .line 58
    return v1
.end method

.method private getProperty(Lmiui/android/animation/IAnimTarget;Ljava/lang/Object;Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "key"    # Ljava/lang/Object;
    .param p3, "value"    # Ljava/lang/Object;

    .line 90
    const/4 v0, 0x0

    .line 91
    .local v0, "property":Lmiui/android/animation/property/FloatProperty;
    instance-of v1, p2, Lmiui/android/animation/property/FloatProperty;

    if-eqz v1, :cond_0

    .line 92
    move-object v0, p2

    check-cast v0, Lmiui/android/animation/property/FloatProperty;

    goto :goto_2

    .line 93
    :cond_0
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_2

    instance-of v1, p1, Lmiui/android/animation/ValueTarget;

    if-eqz v1, :cond_2

    .line 94
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 95
    .local v1, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    move-object v2, p1

    check-cast v2, Lmiui/android/animation/ValueTarget;

    move-object v3, p2

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lmiui/android/animation/ValueTarget;->createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    .end local v1    # "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    goto :goto_1

    .line 96
    :cond_2
    instance-of v1, p2, Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 97
    sget-object v0, Lmiui/android/animation/controller/StateHelper;->DEFAULT_PROPERTY:Lmiui/android/animation/property/ValueProperty;

    goto :goto_2

    .line 96
    :cond_3
    :goto_1
    nop

    .line 104
    :goto_2
    return-object v0
.end method

.method private varargs getPropertyValue(I[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .param p2, "propertyAndValues"    # [Ljava/lang/Object;

    .line 123
    array-length v0, p2

    if-ge p1, v0, :cond_0

    aget-object v0, p2, p1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private isDefaultProperty(Lmiui/android/animation/property/FloatProperty;)Z
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 52
    sget-object v0, Lmiui/android/animation/controller/StateHelper;->DEFAULT_PROPERTY:Lmiui/android/animation/property/ValueProperty;

    if-eq p1, v0, :cond_1

    sget-object v0, Lmiui/android/animation/controller/StateHelper;->DEFAULT_INT_PROPERTY:Lmiui/android/animation/property/IntValueProperty;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private varargs setInitVelocity(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;I[Ljava/lang/Object;)Z
    .locals 3
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "index"    # I
    .param p4, "propertyAndValues"    # [Ljava/lang/Object;

    .line 128
    array-length v0, p4

    const/4 v1, 0x0

    if-lt p3, v0, :cond_0

    .line 129
    return v1

    .line 131
    :cond_0
    aget-object v0, p4, p3

    .line 132
    .local v0, "secondArg":Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/Float;

    if-eqz v2, :cond_1

    .line 133
    move-object v1, v0

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, p2, v1, v2}, Lmiui/android/animation/IAnimTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V

    .line 134
    const/4 v1, 0x1

    return v1

    .line 136
    :cond_1
    return v1
.end method

.method private varargs setPropertyAndValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I
    .locals 13
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p3, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p4, "key"    # Ljava/lang/Object;
    .param p5, "value"    # Ljava/lang/Object;
    .param p6, "idx"    # I
    .param p7, "propertyAndValues"    # [Ljava/lang/Object;

    .line 42
    move-object v6, p0

    move-object/from16 v7, p4

    const/4 v8, 0x0

    .line 43
    .local v8, "delta":I
    move-object/from16 v9, p3

    invoke-direct {p0, v9, v7}, Lmiui/android/animation/controller/StateHelper;->checkAndSetAnimConfig(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    move-object v10, p1

    move-object/from16 v11, p5

    invoke-direct {p0, p1, v7, v11}, Lmiui/android/animation/controller/StateHelper;->getProperty(Lmiui/android/animation/IAnimTarget;Ljava/lang/Object;Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    move-object v12, v0

    .local v12, "property":Lmiui/android/animation/property/FloatProperty;
    if-eqz v0, :cond_2

    .line 45
    invoke-direct {p0, v12}, Lmiui/android/animation/controller/StateHelper;->isDefaultProperty(Lmiui/android/animation/property/FloatProperty;)Z

    move-result v0

    if-eqz v0, :cond_0

    move/from16 v4, p6

    goto :goto_0

    :cond_0
    add-int/lit8 v0, p6, 0x1

    move v4, v0

    .line 46
    .end local p6    # "idx":I
    .local v4, "idx":I
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, v12

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/controller/StateHelper;->addProperty(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/property/FloatProperty;I[Ljava/lang/Object;)I

    move-result v8

    goto :goto_1

    .line 43
    .end local v4    # "idx":I
    .end local v12    # "property":Lmiui/android/animation/property/FloatProperty;
    .restart local p6    # "idx":I
    :cond_1
    move-object v10, p1

    move-object/from16 v11, p5

    .line 48
    :cond_2
    move/from16 v4, p6

    .end local p6    # "idx":I
    .restart local v4    # "idx":I
    :goto_1
    if-lez v8, :cond_3

    add-int v0, v4, v8

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v4, 0x1

    :goto_2
    return v0
.end method

.method private setTempConfig(Lmiui/android/animation/base/AnimConfig;Ljava/lang/Object;)V
    .locals 1
    .param p1, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p2, "setObj"    # Ljava/lang/Object;

    .line 72
    instance-of v0, p2, Lmiui/android/animation/listener/TransitionListener;

    if-eqz v0, :cond_0

    .line 73
    move-object v0, p2

    check-cast v0, Lmiui/android/animation/listener/TransitionListener;

    filled-new-array {v0}, [Lmiui/android/animation/listener/TransitionListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;

    goto :goto_0

    .line 74
    :cond_0
    instance-of v0, p2, Lmiui/android/animation/utils/EaseManager$EaseStyle;

    if-eqz v0, :cond_1

    .line 75
    move-object v0, p2

    check-cast v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;

    invoke-virtual {p1, v0}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 77
    :cond_1
    :goto_0
    return-void
.end method

.method private toFloat(Ljava/lang/Object;Z)F
    .locals 1
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "isInt"    # Z

    .line 157
    if-eqz p2, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_0
    return v0
.end method

.method private toInt(Ljava/lang/Object;Z)I
    .locals 1
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "isInt"    # Z

    .line 153
    if-eqz p2, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-int v0, v0

    :goto_0
    return v0
.end method


# virtual methods
.method varargs parse(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V
    .locals 11
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p3, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p4, "propertyAndValues"    # [Ljava/lang/Object;

    .line 23
    array-length v0, p4

    if-nez v0, :cond_0

    .line 24
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    aget-object v0, p4, v0

    invoke-virtual {p2}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 27
    .local v0, "i":I
    :goto_0
    array-length v1, p4

    if-ge v0, v1, :cond_3

    .line 28
    aget-object v9, p4, v0

    .line 29
    .local v9, "key":Ljava/lang/Object;
    add-int/lit8 v1, v0, 0x1

    array-length v2, p4

    if-ge v1, v2, :cond_1

    add-int/lit8 v1, v0, 0x1

    aget-object v1, p4, v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    move-object v10, v1

    .line 30
    .local v10, "value":Ljava/lang/Object;
    instance-of v1, v9, Ljava/lang/String;

    if-eqz v1, :cond_2

    instance-of v1, v10, Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 31
    add-int/lit8 v0, v0, 0x1

    .line 32
    goto :goto_0

    .line 34
    :cond_2
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v9

    move-object v6, v10

    move v7, v0

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Lmiui/android/animation/controller/StateHelper;->setPropertyAndValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I

    move-result v0

    .line 35
    .end local v9    # "key":Ljava/lang/Object;
    .end local v10    # "value":Ljava/lang/Object;
    goto :goto_0

    .line 36
    :cond_3
    return-void
.end method
