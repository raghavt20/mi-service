class miui.android.animation.controller.FolmeTouch$InnerListViewTouchListener implements android.view.View$OnTouchListener {
	 /* .source "FolmeTouch.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/controller/FolmeTouch; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "InnerListViewTouchListener" */
} // .end annotation
/* # instance fields */
private miui.android.animation.base.AnimConfig mConfigs;
private java.lang.ref.WeakReference mFolmeTouchRef;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lmiui/android/animation/controller/FolmeTouch;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
 miui.android.animation.controller.FolmeTouch$InnerListViewTouchListener ( ) {
/* .locals 1 */
/* .param p1, "folmeTouch" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p2, "configs" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 362 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 363 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mFolmeTouchRef = v0;
/* .line 364 */
this.mConfigs = p2;
/* .line 365 */
return;
} // .end method
/* # virtual methods */
public Boolean onTouch ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 2 */
/* .param p1, "v" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 369 */
v0 = this.mFolmeTouchRef;
/* if-nez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/controller/FolmeTouch; */
/* .line 370 */
/* .local v0, "folmeTouch":Lmiui/android/animation/controller/FolmeTouch; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 371 */
/* if-nez p2, :cond_1 */
/* .line 372 */
v1 = this.mConfigs;
miui.android.animation.controller.FolmeTouch .access$600 ( v0,v1 );
/* .line 374 */
} // :cond_1
v1 = this.mConfigs;
miui.android.animation.controller.FolmeTouch .access$700 ( v0,p1,p2,v1 );
/* .line 377 */
} // :cond_2
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
