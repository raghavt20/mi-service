class miui.android.animation.controller.StateHelper {
	 /* .source "StateHelper.java" */
	 /* # static fields */
	 static final miui.android.animation.property.IntValueProperty DEFAULT_INT_PROPERTY;
	 static final miui.android.animation.property.ValueProperty DEFAULT_PROPERTY;
	 /* # direct methods */
	 static miui.android.animation.controller.StateHelper ( ) {
		 /* .locals 2 */
		 /* .line 19 */
		 /* new-instance v0, Lmiui/android/animation/property/ValueProperty; */
		 final String v1 = "defaultProperty"; // const-string v1, "defaultProperty"
		 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V */
		 /* .line 20 */
		 /* new-instance v0, Lmiui/android/animation/property/IntValueProperty; */
		 final String v1 = "defaultIntProperty"; // const-string v1, "defaultIntProperty"
		 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V */
		 return;
	 } // .end method
	 miui.android.animation.controller.StateHelper ( ) {
		 /* .locals 0 */
		 /* .line 17 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private Boolean addConfigToLink ( miui.android.animation.base.AnimConfigLink p0, java.lang.Object p1 ) {
		 /* .locals 3 */
		 /* .param p1, "link" # Lmiui/android/animation/base/AnimConfigLink; */
		 /* .param p2, "obj" # Ljava/lang/Object; */
		 /* .line 80 */
		 /* instance-of v0, p2, Lmiui/android/animation/base/AnimConfig; */
		 int v1 = 0; // const/4 v1, 0x0
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 81 */
			 /* move-object v0, p2 */
			 /* check-cast v0, Lmiui/android/animation/base/AnimConfig; */
			 /* new-array v1, v1, [Z */
			 (( miui.android.animation.base.AnimConfigLink ) p1 ).add ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V
			 /* .line 82 */
			 int v0 = 1; // const/4 v0, 0x1
			 /* .line 83 */
		 } // :cond_0
		 /* instance-of v0, p2, Lmiui/android/animation/base/AnimConfigLink; */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 84 */
			 /* move-object v0, p2 */
			 /* check-cast v0, Lmiui/android/animation/base/AnimConfigLink; */
			 /* new-array v2, v1, [Z */
			 (( miui.android.animation.base.AnimConfigLink ) p1 ).add ( v0, v2 ); // invoke-virtual {p1, v0, v2}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfigLink;[Z)V
			 /* .line 86 */
		 } // :cond_1
	 } // .end method
	 private Integer addProperty ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1, miui.android.animation.property.FloatProperty p2, Integer p3, java.lang.Object...p4 ) {
		 /* .locals 3 */
		 /* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .param p2, "state" # Lmiui/android/animation/controller/AnimState; */
		 /* .param p3, "property" # Lmiui/android/animation/property/FloatProperty; */
		 /* .param p4, "index" # I */
		 /* .param p5, "propertyAndValues" # [Ljava/lang/Object; */
		 /* .line 109 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 111 */
		 /* .local v0, "delta":I */
		 if ( p3 != null) { // if-eqz p3, :cond_0
			 /* .line 112 */
			 /* invoke-direct {p0, p4, p5}, Lmiui/android/animation/controller/StateHelper;->getPropertyValue(I[Ljava/lang/Object;)Ljava/lang/Object; */
			 /* move-object v2, v1 */
			 /* .local v2, "value":Ljava/lang/Object; */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 113 */
				 v1 = 				 /* invoke-direct {p0, p2, p3, v2}, Lmiui/android/animation/controller/StateHelper;->addPropertyValue(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/property/FloatProperty;Ljava/lang/Object;)Z */
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 /* .line 114 */
					 /* add-int/lit8 v0, v0, 0x1 */
					 /* .line 115 */
					 /* add-int/lit8 v1, p4, 0x1 */
					 v1 = 					 /* invoke-direct {p0, p1, p3, v1, p5}, Lmiui/android/animation/controller/StateHelper;->setInitVelocity(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;I[Ljava/lang/Object;)Z */
					 if ( v1 != null) { // if-eqz v1, :cond_0
						 /* .line 116 */
						 /* add-int/lit8 v0, v0, 0x1 */
						 /* .line 119 */
					 } // .end local v2 # "value":Ljava/lang/Object;
				 } // :cond_0
			 } // .end method
			 private Boolean addPropertyValue ( miui.android.animation.controller.AnimState p0, miui.android.animation.property.FloatProperty p1, java.lang.Object p2 ) {
				 /* .locals 3 */
				 /* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
				 /* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
				 /* .param p3, "value" # Ljava/lang/Object; */
				 /* .line 140 */
				 /* instance-of v0, p3, Ljava/lang/Integer; */
				 /* .line 141 */
				 /* .local v0, "isInt":Z */
				 /* if-nez v0, :cond_1 */
				 /* instance-of v1, p3, Ljava/lang/Float; */
				 /* if-nez v1, :cond_1 */
				 /* instance-of v1, p3, Ljava/lang/Double; */
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 /* .line 149 */
				 } // :cond_0
				 int v1 = 0; // const/4 v1, 0x0
				 /* .line 142 */
			 } // :cond_1
		 } // :goto_0
		 /* instance-of v1, p2, Lmiui/android/animation/property/IIntValueProperty; */
		 if ( v1 != null) { // if-eqz v1, :cond_2
			 /* .line 143 */
			 v1 = 			 /* invoke-direct {p0, p3, v0}, Lmiui/android/animation/controller/StateHelper;->toInt(Ljava/lang/Object;Z)I */
			 /* int-to-double v1, v1 */
			 (( miui.android.animation.controller.AnimState ) p1 ).add ( p2, v1, v2 ); // invoke-virtual {p1, p2, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
			 /* .line 145 */
		 } // :cond_2
		 v1 = 		 /* invoke-direct {p0, p3, v0}, Lmiui/android/animation/controller/StateHelper;->toFloat(Ljava/lang/Object;Z)F */
		 /* float-to-double v1, v1 */
		 (( miui.android.animation.controller.AnimState ) p1 ).add ( p2, v1, v2 ); // invoke-virtual {p1, p2, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
		 /* .line 147 */
	 } // :goto_1
	 int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean checkAndSetAnimConfig ( miui.android.animation.base.AnimConfigLink p0, java.lang.Object p1 ) {
	 /* .locals 6 */
	 /* .param p1, "link" # Lmiui/android/animation/base/AnimConfigLink; */
	 /* .param p2, "obj" # Ljava/lang/Object; */
	 /* .line 56 */
	 /* instance-of v0, p2, Lmiui/android/animation/listener/TransitionListener; */
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-nez v0, :cond_5 */
	 /* instance-of v0, p2, Lmiui/android/animation/utils/EaseManager$EaseStyle; */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 59 */
	 } // :cond_0
	 (( java.lang.Object ) p2 ).getClass ( ); // invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 v0 = 	 (( java.lang.Class ) v0 ).isArray ( ); // invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z
	 if ( v0 != null) { // if-eqz v0, :cond_4
		 /* .line 60 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 61 */
		 /* .local v0, "ret":Z */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
		 v3 = 		 java.lang.reflect.Array .getLength ( p2 );
		 /* .local v3, "n":I */
	 } // :goto_0
	 /* if-ge v2, v3, :cond_3 */
	 /* .line 62 */
	 java.lang.reflect.Array .get ( p2,v2 );
	 /* .line 63 */
	 /* .local v4, "element":Ljava/lang/Object; */
	 v5 = 	 /* invoke-direct {p0, p1, v4}, Lmiui/android/animation/controller/StateHelper;->addConfigToLink(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z */
	 /* if-nez v5, :cond_2 */
	 if ( v0 != null) { // if-eqz v0, :cond_1
	 } // :cond_1
	 int v5 = 0; // const/4 v5, 0x0
} // :cond_2
} // :goto_1
/* move v5, v1 */
} // :goto_2
/* move v0, v5 */
/* .line 61 */
} // .end local v4 # "element":Ljava/lang/Object;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 65 */
} // .end local v2 # "i":I
} // .end local v3 # "n":I
} // :cond_3
/* .line 67 */
} // .end local v0 # "ret":Z
} // :cond_4
v0 = /* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/StateHelper;->addConfigToLink(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z */
/* .line 57 */
} // :cond_5
} // :goto_3
(( miui.android.animation.base.AnimConfigLink ) p1 ).getHead ( ); // invoke-virtual {p1}, Lmiui/android/animation/base/AnimConfigLink;->getHead()Lmiui/android/animation/base/AnimConfig;
/* invoke-direct {p0, v0, p2}, Lmiui/android/animation/controller/StateHelper;->setTempConfig(Lmiui/android/animation/base/AnimConfig;Ljava/lang/Object;)V */
/* .line 58 */
} // .end method
private miui.android.animation.property.FloatProperty getProperty ( miui.android.animation.IAnimTarget p0, java.lang.Object p1, java.lang.Object p2 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "key" # Ljava/lang/Object; */
/* .param p3, "value" # Ljava/lang/Object; */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
/* .line 91 */
/* .local v0, "property":Lmiui/android/animation/property/FloatProperty; */
/* instance-of v1, p2, Lmiui/android/animation/property/FloatProperty; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 92 */
/* move-object v0, p2 */
/* check-cast v0, Lmiui/android/animation/property/FloatProperty; */
/* .line 93 */
} // :cond_0
/* instance-of v1, p2, Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* instance-of v1, p1, Lmiui/android/animation/ValueTarget; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 94 */
if ( p3 != null) { // if-eqz p3, :cond_1
(( java.lang.Object ) p3 ).getClass ( ); // invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .line 95 */
/* .local v1, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
} // :goto_0
/* move-object v2, p1 */
/* check-cast v2, Lmiui/android/animation/ValueTarget; */
/* move-object v3, p2 */
/* check-cast v3, Ljava/lang/String; */
(( miui.android.animation.ValueTarget ) v2 ).createProperty ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Lmiui/android/animation/ValueTarget;->createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;
} // .end local v1 # "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
/* .line 96 */
} // :cond_2
/* instance-of v1, p2, Ljava/lang/Float; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 97 */
v0 = miui.android.animation.controller.StateHelper.DEFAULT_PROPERTY;
/* .line 96 */
} // :cond_3
} // :goto_1
/* nop */
/* .line 104 */
} // :goto_2
} // .end method
private java.lang.Object getPropertyValue ( Integer p0, java.lang.Object...p1 ) {
/* .locals 1 */
/* .param p1, "index" # I */
/* .param p2, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 123 */
/* array-length v0, p2 */
/* if-ge p1, v0, :cond_0 */
/* aget-object v0, p2, p1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private Boolean isDefaultProperty ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 52 */
v0 = miui.android.animation.controller.StateHelper.DEFAULT_PROPERTY;
/* if-eq p1, v0, :cond_1 */
v0 = miui.android.animation.controller.StateHelper.DEFAULT_INT_PROPERTY;
/* if-ne p1, v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private Boolean setInitVelocity ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Integer p2, java.lang.Object...p3 ) {
/* .locals 3 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p3, "index" # I */
/* .param p4, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 128 */
/* array-length v0, p4 */
int v1 = 0; // const/4 v1, 0x0
/* if-lt p3, v0, :cond_0 */
/* .line 129 */
/* .line 131 */
} // :cond_0
/* aget-object v0, p4, p3 */
/* .line 132 */
/* .local v0, "secondArg":Ljava/lang/Object; */
/* instance-of v2, v0, Ljava/lang/Float; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 133 */
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* float-to-double v1, v1 */
(( miui.android.animation.IAnimTarget ) p1 ).setVelocity ( p2, v1, v2 ); // invoke-virtual {p1, p2, v1, v2}, Lmiui/android/animation/IAnimTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V
/* .line 134 */
int v1 = 1; // const/4 v1, 0x1
/* .line 136 */
} // :cond_1
} // .end method
private Integer setPropertyAndValue ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1, miui.android.animation.base.AnimConfigLink p2, java.lang.Object p3, java.lang.Object p4, Integer p5, java.lang.Object...p6 ) {
/* .locals 13 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "state" # Lmiui/android/animation/controller/AnimState; */
/* .param p3, "link" # Lmiui/android/animation/base/AnimConfigLink; */
/* .param p4, "key" # Ljava/lang/Object; */
/* .param p5, "value" # Ljava/lang/Object; */
/* .param p6, "idx" # I */
/* .param p7, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 42 */
/* move-object v6, p0 */
/* move-object/from16 v7, p4 */
int v8 = 0; // const/4 v8, 0x0
/* .line 43 */
/* .local v8, "delta":I */
/* move-object/from16 v9, p3 */
v0 = /* invoke-direct {p0, v9, v7}, Lmiui/android/animation/controller/StateHelper;->checkAndSetAnimConfig(Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;)Z */
/* if-nez v0, :cond_1 */
/* .line 44 */
/* move-object v10, p1 */
/* move-object/from16 v11, p5 */
/* invoke-direct {p0, p1, v7, v11}, Lmiui/android/animation/controller/StateHelper;->getProperty(Lmiui/android/animation/IAnimTarget;Ljava/lang/Object;Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty; */
/* move-object v12, v0 */
/* .local v12, "property":Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 45 */
v0 = /* invoke-direct {p0, v12}, Lmiui/android/animation/controller/StateHelper;->isDefaultProperty(Lmiui/android/animation/property/FloatProperty;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move/from16 v4, p6 */
} // :cond_0
/* add-int/lit8 v0, p6, 0x1 */
/* move v4, v0 */
/* .line 46 */
} // .end local p6 # "idx":I
/* .local v4, "idx":I */
} // :goto_0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, v12 */
/* move-object/from16 v5, p7 */
v8 = /* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/controller/StateHelper;->addProperty(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/property/FloatProperty;I[Ljava/lang/Object;)I */
/* .line 43 */
} // .end local v4 # "idx":I
} // .end local v12 # "property":Lmiui/android/animation/property/FloatProperty;
/* .restart local p6 # "idx":I */
} // :cond_1
/* move-object v10, p1 */
/* move-object/from16 v11, p5 */
/* .line 48 */
} // :cond_2
/* move/from16 v4, p6 */
} // .end local p6 # "idx":I
/* .restart local v4 # "idx":I */
} // :goto_1
/* if-lez v8, :cond_3 */
/* add-int v0, v4, v8 */
} // :cond_3
/* add-int/lit8 v0, v4, 0x1 */
} // :goto_2
} // .end method
private void setTempConfig ( miui.android.animation.base.AnimConfig p0, java.lang.Object p1 ) {
/* .locals 1 */
/* .param p1, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .param p2, "setObj" # Ljava/lang/Object; */
/* .line 72 */
/* instance-of v0, p2, Lmiui/android/animation/listener/TransitionListener; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 73 */
/* move-object v0, p2 */
/* check-cast v0, Lmiui/android/animation/listener/TransitionListener; */
/* filled-new-array {v0}, [Lmiui/android/animation/listener/TransitionListener; */
(( miui.android.animation.base.AnimConfig ) p1 ).addListeners ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
/* .line 74 */
} // :cond_0
/* instance-of v0, p2, Lmiui/android/animation/utils/EaseManager$EaseStyle; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 75 */
/* move-object v0, p2 */
/* check-cast v0, Lmiui/android/animation/utils/EaseManager$EaseStyle; */
(( miui.android.animation.base.AnimConfig ) p1 ).setEase ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 77 */
} // :cond_1
} // :goto_0
return;
} // .end method
private Float toFloat ( java.lang.Object p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "value" # Ljava/lang/Object; */
/* .param p2, "isInt" # Z */
/* .line 157 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* int-to-float v0, v0 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // :goto_0
} // .end method
private Integer toInt ( java.lang.Object p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "value" # Ljava/lang/Object; */
/* .param p2, "isInt" # Z */
/* .line 153 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* float-to-int v0, v0 */
} // :goto_0
} // .end method
/* # virtual methods */
void parse ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1, miui.android.animation.base.AnimConfigLink p2, java.lang.Object...p3 ) {
/* .locals 11 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "state" # Lmiui/android/animation/controller/AnimState; */
/* .param p3, "link" # Lmiui/android/animation/base/AnimConfigLink; */
/* .param p4, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 23 */
/* array-length v0, p4 */
/* if-nez v0, :cond_0 */
/* .line 24 */
return;
/* .line 26 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, p4, v0 */
(( miui.android.animation.controller.AnimState ) p2 ).getTag ( ); // invoke-virtual {p2}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;
v0 = (( java.lang.Object ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
/* .line 27 */
/* .local v0, "i":I */
} // :goto_0
/* array-length v1, p4 */
/* if-ge v0, v1, :cond_3 */
/* .line 28 */
/* aget-object v9, p4, v0 */
/* .line 29 */
/* .local v9, "key":Ljava/lang/Object; */
/* add-int/lit8 v1, v0, 0x1 */
/* array-length v2, p4 */
/* if-ge v1, v2, :cond_1 */
/* add-int/lit8 v1, v0, 0x1 */
/* aget-object v1, p4, v1 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* move-object v10, v1 */
/* .line 30 */
/* .local v10, "value":Ljava/lang/Object; */
/* instance-of v1, v9, Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* instance-of v1, v10, Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 31 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 32 */
/* .line 34 */
} // :cond_2
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, v9 */
/* move-object v6, v10 */
/* move v7, v0 */
/* move-object v8, p4 */
v0 = /* invoke-direct/range {v1 ..v8}, Lmiui/android/animation/controller/StateHelper;->setPropertyAndValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I */
/* .line 35 */
} // .end local v9 # "key":Ljava/lang/Object;
} // .end local v10 # "value":Ljava/lang/Object;
/* .line 36 */
} // :cond_3
return;
} // .end method
