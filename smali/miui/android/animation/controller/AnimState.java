public class miui.android.animation.controller.AnimState {
	 /* .source "AnimState.java" */
	 /* # static fields */
	 public static final Long FLAG_IN_TOUCH;
	 public static final Long FLAG_PARALLEL;
	 public static final Long FLAG_QUEUE;
	 private static final Integer STEP;
	 private static final java.lang.String TAG;
	 public static final Integer VIEW_POS;
	 public static final Integer VIEW_SIZE;
	 private static final java.util.concurrent.atomic.AtomicInteger sId;
	 /* # instance fields */
	 public Long flags;
	 public final Boolean isTemporary;
	 private final miui.android.animation.base.AnimConfig mConfig;
	 private final java.util.Map mMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/Object;", */
	 /* "Ljava/lang/Double;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private volatile java.lang.Object mTag;
miui.android.animation.property.IntValueProperty tempIntValueProperty;
miui.android.animation.property.ValueProperty tempValueProperty;
/* # direct methods */
static miui.android.animation.controller.AnimState ( ) {
/* .locals 1 */
/* .line 33 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V */
return;
} // .end method
public miui.android.animation.controller.AnimState ( ) {
/* .locals 2 */
/* .line 86 */
int v0 = 0; // const/4 v0, 0x0
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v1}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V */
/* .line 87 */
return;
} // .end method
public miui.android.animation.controller.AnimState ( ) {
/* .locals 1 */
/* .param p1, "t" # Ljava/lang/Object; */
/* .line 90 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V */
/* .line 91 */
return;
} // .end method
public miui.android.animation.controller.AnimState ( ) {
/* .locals 2 */
/* .param p1, "t" # Ljava/lang/Object; */
/* .param p2, "temporary" # Z */
/* .line 93 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 44 */
/* new-instance v0, Lmiui/android/animation/property/ValueProperty; */
final String v1 = ""; // const-string v1, ""
/* invoke-direct {v0, v1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V */
this.tempValueProperty = v0;
/* .line 45 */
/* new-instance v0, Lmiui/android/animation/property/IntValueProperty; */
/* invoke-direct {v0, v1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V */
this.tempIntValueProperty = v0;
/* .line 54 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.mConfig = v0;
/* .line 55 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mMap = v0;
/* .line 94 */
(( miui.android.animation.controller.AnimState ) p0 ).setTag ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/AnimState;->setTag(Ljava/lang/Object;)V
/* .line 95 */
/* iput-boolean p2, p0, Lmiui/android/animation/controller/AnimState;->isTemporary:Z */
/* .line 96 */
return;
} // .end method
public static void alignState ( miui.android.animation.controller.AnimState p0, java.util.Collection p1 ) {
/* .locals 5 */
/* .param p0, "state" # Lmiui/android/animation/controller/AnimState; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/android/animation/controller/AnimState;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 58 */
/* .local p1, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 59 */
/* .local v1, "update":Lmiui/android/animation/listener/UpdateInfo; */
v2 = this.property;
v2 = (( miui.android.animation.controller.AnimState ) p0 ).contains ( v2 ); // invoke-virtual {p0, v2}, Lmiui/android/animation/controller/AnimState;->contains(Ljava/lang/Object;)Z
/* if-nez v2, :cond_1 */
/* .line 60 */
/* iget-boolean v2, v1, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 61 */
v2 = this.property;
v3 = this.animInfo;
/* iget-wide v3, v3, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
/* double-to-int v3, v3 */
/* int-to-double v3, v3 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 63 */
} // :cond_0
v2 = this.property;
v3 = this.animInfo;
/* iget-wide v3, v3, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
/* double-to-float v3, v3 */
/* float-to-double v3, v3 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( v2, v3, v4 ); // invoke-virtual {p0, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 66 */
} // .end local v1 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_1
} // :goto_1
/* .line 67 */
} // :cond_2
/* const-class v0, Ljava/util/ArrayList; */
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.ObjectPool .acquire ( v0,v1 );
/* check-cast v0, Ljava/util/List; */
/* .line 68 */
/* .local v0, "delList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;" */
(( miui.android.animation.controller.AnimState ) p0 ).keySet ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 70 */
/* .local v2, "key":Ljava/lang/Object; */
/* instance-of v3, v2, Lmiui/android/animation/property/FloatProperty; */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 71 */
/* move-object v3, v2 */
/* check-cast v3, Lmiui/android/animation/property/FloatProperty; */
miui.android.animation.listener.UpdateInfo .findBy ( p1,v3 );
/* .local v3, "update":Lmiui/android/animation/listener/UpdateInfo; */
/* .line 73 */
} // .end local v3 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_3
/* move-object v3, v2 */
/* check-cast v3, Ljava/lang/String; */
miui.android.animation.listener.UpdateInfo .findByName ( p1,v3 );
/* .line 75 */
/* .restart local v3 # "update":Lmiui/android/animation/listener/UpdateInfo; */
} // :goto_3
/* if-nez v3, :cond_4 */
/* .line 76 */
/* .line 78 */
} // .end local v2 # "key":Ljava/lang/Object;
} // .end local v3 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_4
/* .line 79 */
} // :cond_5
v2 = } // :goto_4
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 80 */
/* .restart local v2 # "key":Ljava/lang/Object; */
(( miui.android.animation.controller.AnimState ) p0 ).remove ( v2 ); // invoke-virtual {p0, v2}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 81 */
} // .end local v2 # "key":Ljava/lang/Object;
/* .line 82 */
} // :cond_6
miui.android.animation.utils.ObjectPool .release ( v0 );
/* .line 83 */
return;
} // .end method
private void append ( miui.android.animation.controller.AnimState p0 ) {
/* .locals 2 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .line 116 */
v0 = this.mConfig;
v1 = this.mConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).copy ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V
/* .line 117 */
v0 = this.mMap;
/* .line 118 */
v0 = this.mMap;
v1 = this.mMap;
/* .line 119 */
return;
} // .end method
private java.lang.Double getMapValue ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 233 */
v0 = this.mMap;
/* check-cast v0, Ljava/lang/Double; */
/* .line 234 */
/* .local v0, "value":Ljava/lang/Double; */
/* if-nez v0, :cond_0 */
/* instance-of v1, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 235 */
v1 = this.mMap;
/* move-object v2, p1 */
/* check-cast v2, Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.property.FloatProperty ) v2 ).getName ( ); // invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
/* move-object v0, v1 */
/* check-cast v0, Ljava/lang/Double; */
/* .line 237 */
} // :cond_0
} // .end method
private Double getProperValue ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Double p2 ) {
/* .locals 7 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p3, "value" # D */
/* .line 249 */
(( miui.android.animation.controller.AnimState ) p0 ).getConfigFlags ( p2 ); // invoke-virtual {p0, p2}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J
/* move-result-wide v0 */
/* .line 250 */
/* .local v0, "flag":J */
/* const-wide/16 v2, 0x1 */
v2 = miui.android.animation.utils.CommonUtils .hasFlags ( v0,v1,v2,v3 );
/* .line 251 */
/* .local v2, "isDelta":Z */
/* if-nez v2, :cond_1 */
/* const-wide v3, 0x412e848000000000L # 1000000.0 */
/* cmpl-double v3, p3, v3 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* const-wide v3, 0x412e854800000000L # 1000100.0 */
/* cmpl-double v3, p3, v3 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* instance-of v3, p2, Lmiui/android/animation/property/ISpecificProperty; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 261 */
} // :cond_0
/* return-wide p3 */
/* .line 253 */
} // :cond_1
} // :goto_0
miui.android.animation.internal.AnimValueUtils .getValue ( p1,p2,p3,p4 );
/* move-result-wide v3 */
/* .line 254 */
/* .local v3, "curValue":D */
if ( v2 != null) { // if-eqz v2, :cond_2
v5 = miui.android.animation.internal.AnimValueUtils .isInvalid ( p3,p4 );
/* if-nez v5, :cond_2 */
/* .line 255 */
/* const-wide/16 v5, -0x2 */
/* and-long/2addr v5, v0 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p2, v5, v6 ); // invoke-virtual {p0, p2, v5, v6}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 256 */
/* add-double/2addr v3, p3 */
/* .line 257 */
/* invoke-direct {p0, p2, v3, v4}, Lmiui/android/animation/controller/AnimState;->setMapValue(Ljava/lang/Object;D)V */
/* .line 259 */
} // :cond_2
/* return-wide v3 */
} // .end method
private void setMapValue ( java.lang.Object p0, Double p1 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "value" # D */
/* .line 241 */
/* instance-of v0, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mMap;
/* move-object v1, p1 */
/* check-cast v1, Lmiui/android/animation/property/FloatProperty; */
v0 = (( miui.android.animation.property.FloatProperty ) v1 ).getName ( ); // invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 242 */
v0 = this.mMap;
/* move-object v1, p1 */
/* check-cast v1, Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.property.FloatProperty ) v1 ).getName ( ); // invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
java.lang.Double .valueOf ( p2,p3 );
/* .line 244 */
} // :cond_0
v0 = this.mMap;
java.lang.Double .valueOf ( p2,p3 );
/* .line 246 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public miui.android.animation.controller.AnimState add ( java.lang.Object p0, Double p1 ) {
/* .locals 0 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "value" # D */
/* .line 166 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/AnimState;->setMapValue(Ljava/lang/Object;D)V */
/* .line 167 */
} // .end method
public miui.android.animation.controller.AnimState add ( java.lang.String p0, Float p1, Long...p2 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .param p3, "flags" # [J */
/* .line 126 */
/* array-length v0, p3 */
/* if-lez v0, :cond_0 */
/* .line 127 */
int v0 = 0; // const/4 v0, 0x0
/* aget-wide v0, p3, v0 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 129 */
} // :cond_0
/* float-to-double v0, p2 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState add ( java.lang.String p0, Integer p1, Long...p2 ) {
/* .locals 5 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .param p3, "flags" # [J */
/* .line 133 */
/* array-length v0, p3 */
/* const-wide/16 v1, 0x4 */
/* if-lez v0, :cond_0 */
/* .line 134 */
int v0 = 0; // const/4 v0, 0x0
/* aget-wide v3, p3, v0 */
/* or-long v0, v3, v1 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 136 */
} // :cond_0
(( miui.android.animation.controller.AnimState ) p0 ).getConfigFlags ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J
/* move-result-wide v3 */
/* or-long v0, v3, v1 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 138 */
} // :goto_0
/* int-to-double v0, p2 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState add ( miui.android.animation.property.FloatProperty p0, Float p1, Long...p2 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .param p3, "flags" # [J */
/* .line 150 */
/* array-length v0, p3 */
/* if-lez v0, :cond_0 */
/* .line 151 */
int v0 = 0; // const/4 v0, 0x0
/* aget-wide v0, p3, v0 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 153 */
} // :cond_0
/* float-to-double v0, p2 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState add ( miui.android.animation.property.FloatProperty p0, Integer p1, Long...p2 ) {
/* .locals 5 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .param p3, "flags" # [J */
/* .line 157 */
/* array-length v0, p3 */
/* const-wide/16 v1, 0x4 */
/* if-lez v0, :cond_0 */
/* .line 158 */
int v0 = 0; // const/4 v0, 0x0
/* aget-wide v3, p3, v0 */
/* or-long v0, v3, v1 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 160 */
} // :cond_0
(( miui.android.animation.controller.AnimState ) p0 ).getConfigFlags ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J
/* move-result-wide v3 */
/* or-long v0, v3, v1 */
(( miui.android.animation.controller.AnimState ) p0 ).setConfigFlag ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 162 */
} // :goto_0
/* int-to-double v0, p2 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState add ( miui.android.animation.property.ViewProperty p0, Float p1, Long...p2 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/ViewProperty; */
/* .param p2, "value" # F */
/* .param p3, "flags" # [J */
/* .line 142 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/FloatProperty;F[J)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState add ( miui.android.animation.property.ViewProperty p0, Integer p1, Long...p2 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/ViewProperty; */
/* .param p2, "value" # I */
/* .param p3, "flags" # [J */
/* .line 146 */
(( miui.android.animation.controller.AnimState ) p0 ).add ( p1, p2, p3 ); // invoke-virtual {p0, p1, p2, p3}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/FloatProperty;I[J)Lmiui/android/animation/controller/AnimState;
} // .end method
public void clear ( ) {
/* .locals 1 */
/* .line 103 */
v0 = this.mConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).clear ( ); // invoke-virtual {v0}, Lmiui/android/animation/base/AnimConfig;->clear()V
/* .line 104 */
v0 = this.mMap;
/* .line 105 */
return;
} // .end method
public Boolean contains ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "property" # Ljava/lang/Object; */
/* .line 179 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 180 */
/* .line 182 */
} // :cond_0
v1 = v1 = this.mMap;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 183 */
int v0 = 1; // const/4 v0, 0x1
/* .line 185 */
} // :cond_1
/* instance-of v1, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 186 */
v0 = this.mMap;
/* move-object v1, p1 */
/* check-cast v1, Lmiui/android/animation/property/FloatProperty; */
v0 = (( miui.android.animation.property.FloatProperty ) v1 ).getName ( ); // invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
/* .line 188 */
} // :cond_2
} // .end method
public Double get ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1 ) {
/* .locals 3 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 225 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double; */
/* .line 226 */
/* .local v0, "value":Ljava/lang/Double; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 227 */
(( java.lang.Double ) v0 ).doubleValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D
/* move-result-wide v1 */
/* invoke-direct {p0, p1, p2, v1, v2}, Lmiui/android/animation/controller/AnimState;->getProperValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D */
/* move-result-wide v1 */
/* return-wide v1 */
/* .line 229 */
} // :cond_0
/* const-wide v1, 0x7fefffffffffffffL # Double.MAX_VALUE */
/* return-wide v1 */
} // .end method
public miui.android.animation.base.AnimConfig getConfig ( ) {
/* .locals 1 */
/* .line 274 */
v0 = this.mConfig;
} // .end method
public Long getConfigFlags ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 268 */
/* instance-of v0, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.property.FloatProperty ) v0 ).getName ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/String; */
/* .line 269 */
/* .local v0, "name":Ljava/lang/String; */
} // :goto_0
v1 = this.mConfig;
(( miui.android.animation.base.AnimConfig ) v1 ).getSpecialConfig ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/base/AnimConfig;->getSpecialConfig(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
/* .line 270 */
/* .local v1, "config":Lmiui/android/animation/base/AnimSpecialConfig; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-wide v2, v1, Lmiui/android/animation/base/AnimSpecialConfig;->flags:J */
} // :cond_1
/* const-wide/16 v2, 0x0 */
} // :goto_1
/* return-wide v2 */
} // .end method
public Float getFloat ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 220 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double; */
/* .line 221 */
/* .local v0, "value":Ljava/lang/Double; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.Double ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F
} // :cond_0
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
} // :goto_0
} // .end method
public Float getFloat ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 215 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double; */
/* .line 216 */
/* .local v0, "value":Ljava/lang/Double; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.Double ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F
} // :cond_0
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
} // :goto_0
} // .end method
public Integer getInt ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 211 */
/* new-instance v0, Lmiui/android/animation/property/IntValueProperty; */
/* invoke-direct {v0, p1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V */
v0 = (( miui.android.animation.controller.AnimState ) p0 ).getInt ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->getInt(Lmiui/android/animation/property/IIntValueProperty;)I
} // .end method
public Integer getInt ( miui.android.animation.property.IIntValueProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/IIntValueProperty; */
/* .line 206 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->getMapValue(Ljava/lang/Object;)Ljava/lang/Double; */
/* .line 207 */
/* .local v0, "value":Ljava/lang/Double; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.Double ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Double;->intValue()I
} // :cond_0
/* const v1, 0x7fffffff */
} // :goto_0
} // .end method
public miui.android.animation.property.FloatProperty getProperty ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 289 */
/* instance-of v0, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 290 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/FloatProperty; */
/* .line 293 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/String; */
/* .line 294 */
/* .local v0, "name":Ljava/lang/String; */
(( miui.android.animation.controller.AnimState ) p0 ).getConfigFlags ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J
/* move-result-wide v1 */
/* const-wide/16 v3, 0x4 */
v1 = miui.android.animation.utils.CommonUtils .hasFlags ( v1,v2,v3,v4 );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 295 */
/* new-instance v1, Lmiui/android/animation/property/IntValueProperty; */
/* invoke-direct {v1, v0}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V */
/* .local v1, "property":Lmiui/android/animation/property/FloatProperty; */
/* .line 297 */
} // .end local v1 # "property":Lmiui/android/animation/property/FloatProperty;
} // :cond_1
/* new-instance v1, Lmiui/android/animation/property/ValueProperty; */
/* invoke-direct {v1, v0}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V */
/* .line 299 */
/* .restart local v1 # "property":Lmiui/android/animation/property/FloatProperty; */
} // :goto_0
} // .end method
public java.lang.Object getTag ( ) {
/* .locals 1 */
/* .line 122 */
v0 = this.mTag;
} // .end method
public miui.android.animation.property.FloatProperty getTempProperty ( java.lang.Object p0 ) {
/* .locals 5 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 307 */
/* instance-of v0, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 308 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/FloatProperty; */
/* .line 310 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/String; */
/* .line 311 */
/* .local v0, "name":Ljava/lang/String; */
(( miui.android.animation.controller.AnimState ) p0 ).getConfigFlags ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->getConfigFlags(Ljava/lang/Object;)J
/* move-result-wide v1 */
/* const-wide/16 v3, 0x4 */
v1 = miui.android.animation.utils.CommonUtils .hasFlags ( v1,v2,v3,v4 );
if ( v1 != null) { // if-eqz v1, :cond_1
v1 = this.tempIntValueProperty;
} // :cond_1
v1 = this.tempValueProperty;
/* .line 313 */
/* .local v1, "property":Lmiui/android/animation/property/ValueProperty; */
} // :goto_0
/* move-object v2, p1 */
/* check-cast v2, Ljava/lang/String; */
(( miui.android.animation.property.ValueProperty ) v1 ).setName ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/animation/property/ValueProperty;->setName(Ljava/lang/String;)V
/* .line 314 */
} // .end method
public Boolean isEmpty ( ) {
/* .locals 1 */
/* .line 195 */
v0 = v0 = this.mMap;
} // .end method
public java.util.Set keySet ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 202 */
v0 = this.mMap;
} // .end method
public miui.android.animation.controller.AnimState remove ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 278 */
v0 = this.mMap;
/* .line 279 */
/* instance-of v0, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 280 */
v0 = this.mMap;
/* move-object v1, p1 */
/* check-cast v1, Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.property.FloatProperty ) v1 ).getName ( ); // invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
/* .line 282 */
} // :cond_0
} // .end method
public void set ( miui.android.animation.controller.AnimState p0 ) {
/* .locals 1 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .line 108 */
/* if-nez p1, :cond_0 */
/* .line 109 */
return;
/* .line 111 */
} // :cond_0
v0 = this.mTag;
(( miui.android.animation.controller.AnimState ) p0 ).setTag ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/AnimState;->setTag(Ljava/lang/Object;)V
/* .line 112 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/AnimState;->append(Lmiui/android/animation/controller/AnimState;)V */
/* .line 113 */
return;
} // .end method
public void setConfigFlag ( java.lang.Object p0, Long p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "flag" # J */
/* .line 171 */
/* instance-of v0, p1, Lmiui/android/animation/property/FloatProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.property.FloatProperty ) v0 ).getName ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Ljava/lang/String; */
/* .line 172 */
/* .local v0, "name":Ljava/lang/String; */
} // :goto_0
v1 = this.mConfig;
(( miui.android.animation.base.AnimConfig ) v1 ).queryAndCreateSpecial ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
/* iput-wide p2, v1, Lmiui/android/animation/base/AnimSpecialConfig;->flags:J */
/* .line 173 */
return;
} // .end method
public final void setTag ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 99 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* move-object v0, p1 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TAG_"; // const-string v1, "TAG_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = miui.android.animation.controller.AnimState.sId;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).incrementAndGet ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // :goto_0
this.mTag = v0;
/* .line 100 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 320 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "\nAnimState{mTag=\'"; // const-string v1, "\nAnimState{mTag=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTag;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", flags:"; // const-string v1, ", flags:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/controller/AnimState;->flags:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", mMaps="; // const-string v1, ", mMaps="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mMap;
/* .line 322 */
final String v2 = " "; // const-string v2, " "
miui.android.animation.utils.CommonUtils .mapToString ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 320 */
} // .end method
