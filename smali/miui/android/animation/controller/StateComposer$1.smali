.class final Lmiui/android/animation/controller/StateComposer$1;
.super Ljava/lang/Object;
.source "StateComposer.java"

# interfaces
.implements Lmiui/android/animation/utils/StyleComposer$IInterceptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/controller/StateComposer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lmiui/android/animation/utils/StyleComposer$IInterceptor<",
        "Lmiui/android/animation/controller/IFolmeStateStyle;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onMethod(Ljava/lang/reflect/Method;[Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p3, [Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-virtual {p0, p1, p2, p3}, Lmiui/android/animation/controller/StateComposer$1;->onMethod(Ljava/lang/reflect/Method;[Ljava/lang/Object;[Lmiui/android/animation/controller/IFolmeStateStyle;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public onMethod(Ljava/lang/reflect/Method;[Ljava/lang/Object;[Lmiui/android/animation/controller/IFolmeStateStyle;)Ljava/lang/Object;
    .locals 3
    .param p1, "method"    # Ljava/lang/reflect/Method;
    .param p2, "args"    # [Ljava/lang/Object;
    .param p3, "styles"    # [Lmiui/android/animation/controller/IFolmeStateStyle;

    .line 24
    array-length v0, p3

    if-lez v0, :cond_1

    array-length v0, p2

    if-lez v0, :cond_1

    .line 25
    const/4 v0, 0x0

    aget-object v1, p3, v0

    aget-object v0, p2, v0

    invoke-interface {v1, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 26
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v2, p3

    if-ge v1, v2, :cond_0

    .line 27
    aget-object v2, p3, v1

    invoke-interface {v2, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->addState(Lmiui/android/animation/controller/AnimState;)V

    .line 26
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 29
    .end local v1    # "i":I
    :cond_0
    return-object v0

    .line 31
    .end local v0    # "state":Lmiui/android/animation/controller/AnimState;
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public shouldIntercept(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Z
    .locals 2
    .param p1, "method"    # Ljava/lang/reflect/Method;
    .param p2, "args"    # [Ljava/lang/Object;

    .line 19
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
