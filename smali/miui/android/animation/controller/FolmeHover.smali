.class public Lmiui/android/animation/controller/FolmeHover;
.super Lmiui/android/animation/controller/FolmeBase;
.source "FolmeHover.java"

# interfaces
.implements Lmiui/android/animation/IHoverStyle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;
    }
.end annotation


# static fields
.field private static final CORNER_DIS:I = 0x24

.field private static final DEFAULT_CORNER:F = 0.5f

.field private static final DEFAULT_SCALE:F = 1.15f

.field private static final SCALE_DIS:I = 0xc

.field private static sHoverRecord:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private HoverMoveType:Ljava/lang/String;

.field private isSetAutoTranslation:Z

.field private mChildView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mClearTint:Z

.field private mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

.field private mDefListener:Lmiui/android/animation/listener/TransitionListener;

.field private mEnterConfig:Lmiui/android/animation/base/AnimConfig;

.field private mExitConfig:Lmiui/android/animation/base/AnimConfig;

.field private mHoverView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mIsEnter:Z

.field private mLocation:[I

.field private mMoveConfig:Lmiui/android/animation/base/AnimConfig;

.field private mParentView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRadius:F

.field private mScaleSetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiui/android/animation/IHoverStyle$HoverType;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mSetTint:Z

.field private mTargetHeight:I

.field private mTargetWidth:I

.field private mTranslateDist:F

.field private mTranslationSetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiui/android/animation/IHoverStyle$HoverType;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mViewRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lmiui/android/animation/controller/FolmeHover;->sHoverRecord:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lmiui/android/animation/IAnimTarget;)V
    .locals 5
    .param p1, "targets"    # [Lmiui/android/animation/IAnimTarget;

    .line 92
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V

    .line 53
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslateDist:F

    .line 55
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    .line 56
    const/4 v3, -0x2

    invoke-static {v3, v2}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mMoveConfig:Lmiui/android/animation/base/AnimConfig;

    .line 58
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mEnterConfig:Lmiui/android/animation/base/AnimConfig;

    .line 59
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mExitConfig:Lmiui/android/animation/base/AnimConfig;

    .line 61
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    .line 62
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslationSetMap:Ljava/util/Map;

    .line 63
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverEffect;->NORMAL:Lmiui/android/animation/IHoverStyle$HoverEffect;

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    .line 67
    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z

    .line 70
    new-array v2, v1, [I

    iput-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mLocation:[I

    .line 72
    const/4 v2, 0x0

    iput v2, p0, Lmiui/android/animation/controller/FolmeHover;->mRadius:F

    .line 73
    iput v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I

    .line 74
    iput v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I

    .line 80
    const-string v2, "MOVE"

    iput-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->HoverMoveType:Ljava/lang/String;

    .line 82
    new-instance v2, Lmiui/android/animation/controller/FolmeHover$1;

    invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeHover$1;-><init>(Lmiui/android/animation/controller/FolmeHover;)V

    iput-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mDefListener:Lmiui/android/animation/listener/TransitionListener;

    .line 93
    array-length v2, p1

    if-lez v2, :cond_0

    aget-object v0, p1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->initDist(Lmiui/android/animation/IAnimTarget;)V

    .line 94
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->updateHoverState(Lmiui/android/animation/IHoverStyle$HoverEffect;)V

    .line 96
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mEnterConfig:Lmiui/android/animation/base/AnimConfig;

    new-array v2, v1, [F

    fill-array-data v2, :array_1

    invoke-static {v3, v2}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 97
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mEnterConfig:Lmiui/android/animation/base/AnimConfig;

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mDefListener:Lmiui/android/animation/listener/TransitionListener;

    filled-new-array {v2}, [Lmiui/android/animation/listener/TransitionListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;

    .line 99
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mExitConfig:Lmiui/android/animation/base/AnimConfig;

    new-array v2, v1, [F

    fill-array-data v2, :array_2

    invoke-virtual {v0, v3, v2}, Lmiui/android/animation/base/AnimConfig;->setEase(I[F)Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    new-array v1, v1, [F

    fill-array-data v1, :array_3

    .line 100
    const-wide/16 v3, -0x2

    invoke-virtual {v0, v2, v3, v4, v1}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;

    .line 102
    return-void

    nop

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3ecccccd    # 0.4f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e19999a    # 0.15f
    .end array-data

    :array_2
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e99999a    # 0.3f
    .end array-data

    :array_3
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method static synthetic access$100(Lmiui/android/animation/controller/FolmeHover;Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeHover;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/MotionEvent;
    .param p3, "x3"    # [Lmiui/android/animation/base/AnimConfig;

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    return-void
.end method

.method private actualTranslatDist(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 654
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 655
    .local v0, "curx":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    .line 656
    .local v1, "cury":F
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mLocation:[I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 658
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mLocation:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 659
    .local v2, "curViewCentreX":F
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeHover;->mLocation:[I

    const/4 v5, 0x1

    aget v3, v3, v5

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v4

    add-float/2addr v3, v5

    .line 660
    .local v3, "curViewCentreY":F
    sub-float v4, v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 661
    .local v4, "ox":F
    sub-float v5, v1, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    .line 663
    .local v5, "oy":F
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v7

    const/high16 v8, -0x40800000    # -1.0f

    invoke-static {v8, v7}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 664
    invoke-static {v6, v5}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v8, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 666
    iget v7, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslateDist:F

    const v8, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v9, v7, v8

    if-nez v9, :cond_0

    move v9, v6

    goto :goto_0

    :cond_0
    move v9, v7

    :goto_0
    mul-float/2addr v4, v9

    .line 667
    cmpl-float v8, v7, v8

    if-nez v8, :cond_1

    goto :goto_1

    :cond_1
    move v6, v7

    :goto_1
    mul-float/2addr v5, v6

    .line 669
    iget-object v6, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    iget-object v7, p0, Lmiui/android/animation/controller/FolmeHover;->HoverMoveType:Ljava/lang/String;

    invoke-interface {v6, v7}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    sget-object v7, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v8, v4

    .line 670
    invoke-virtual {v6, v7, v8, v9}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    sget-object v7, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v8, v5

    .line 671
    invoke-virtual {v6, v7, v8, v9}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    .line 672
    .local v6, "state":Lmiui/android/animation/controller/AnimState;
    iget-object v7, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    iget-object v8, p0, Lmiui/android/animation/controller/FolmeHover;->mMoveConfig:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v8}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v8

    invoke-interface {v7, v6, v8}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 673
    return-void
.end method

.method private static addMagicPoint(Landroid/view/View;Landroid/graphics/Point;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "magicPoint"    # Landroid/graphics/Point;

    .line 789
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/graphics/Point;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 792
    .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v1, "android.view.View"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 793
    .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "addMagicPoint"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 794
    .local v2, "sGet":Ljava/lang/reflect/Method;
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    nop

    .end local v0    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v1    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 795
    :catch_0
    move-exception v0

    .line 796
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMagicPoint failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static addMagicRect(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "rect"    # Landroid/graphics/Rect;

    .line 711
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 714
    .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v1, "android.view.View"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 715
    .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "addMagicRect"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 716
    .local v2, "sGet":Ljava/lang/reflect/Method;
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    nop

    .end local v0    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v1    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 717
    :catch_0
    move-exception v0

    .line 718
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMagicRect failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static clearMagicPoint(Landroid/view/View;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .line 802
    :try_start_0
    const-string v0, "android.view.View"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 803
    .local v0, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v1, "clearMagicPoint"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 804
    .local v1, "sGet":Ljava/lang/reflect/Method;
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 807
    nop

    .end local v0    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 805
    :catch_0
    move-exception v0

    .line 806
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearMagicPoint failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private clearRound()V
    .locals 0

    .line 258
    return-void
.end method

.method private clearScale()V
    .locals 2

    .line 232
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 234
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 236
    :cond_0
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 238
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 240
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 241
    return-void
.end method

.method private clearTranslation()V
    .locals 2

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    .line 245
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_X:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 247
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_Y:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 249
    :cond_0
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_X:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 251
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_Y:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 253
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslationSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 254
    return-void
.end method

.method private varargs doHandleHoverOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 288
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->handleViewHover(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V

    .line 289
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->setHoverView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleViewHover for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    :cond_0
    return-void
.end method

.method private varargs getEnterConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 684
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mEnterConfig:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-static {p1, v0}, Lmiui/android/animation/utils/CommonUtils;->mergeArray([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/base/AnimConfig;

    return-object v0
.end method

.method private varargs getExitConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 688
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mExitConfig:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-static {p1, v0}, Lmiui/android/animation/utils/CommonUtils;->mergeArray([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/base/AnimConfig;

    return-object v0
.end method

.method private varargs getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType;
    .locals 1
    .param p1, "type"    # [Lmiui/android/animation/IHoverStyle$HoverType;

    .line 593
    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    :goto_0
    return-object v0
.end method

.method private varargs handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 376
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 384
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventExit(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    goto :goto_0

    .line 378
    :pswitch_2
    invoke-direct {p0, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventEnter(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 379
    goto :goto_0

    .line 381
    :pswitch_3
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 382
    nop

    .line 388
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private varargs handleViewHover(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 342
    sget-object v0, Lmiui/android/animation/controller/FolmeHover;->sHoverRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;

    .line 343
    .local v0, "hoverListener":Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;
    if-nez v0, :cond_0

    .line 344
    new-instance v1, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;-><init>(Lmiui/android/animation/controller/FolmeHover$1;)V

    move-object v0, v1

    .line 345
    sget-object v1, Lmiui/android/animation/controller/FolmeHover;->sHoverRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 348
    invoke-virtual {v0, p0, p2}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;->addHover(Lmiui/android/animation/controller/FolmeHover;[Lmiui/android/animation/base/AnimConfig;)V

    .line 349
    return-void
.end method

.method private varargs hoverExit(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 491
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mHoverView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mLocation:[I

    invoke-static {v0, v1, p1}, Lmiui/android/animation/controller/FolmeHover;->isOnHoverView(Landroid/view/View;[ILandroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    filled-new-array {v0}, [Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lmiui/android/animation/Folme;->useAt([Landroid/view/View;)Lmiui/android/animation/IFolme;

    move-result-object v0

    invoke-interface {v0}, Lmiui/android/animation/IFolme;->hover()Lmiui/android/animation/IHoverStyle;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lmiui/android/animation/base/AnimConfig;

    invoke-interface {v0, v1}, Lmiui/android/animation/IHoverStyle;->hoverEnter([Lmiui/android/animation/base/AnimConfig;)V

    .line 494
    :cond_0
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_X:Lmiui/android/animation/property/ViewProperty;

    .line 496
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_Y:Lmiui/android/animation/property/ViewProperty;

    .line 497
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 499
    :cond_1
    invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->hoverExit([Lmiui/android/animation/base/AnimConfig;)V

    .line 500
    return-void
.end method

.method private initDist(Lmiui/android/animation/IAnimTarget;)V
    .locals 13
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 623
    instance-of v0, p1, Lmiui/android/animation/ViewTarget;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 624
    .local v0, "view":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_3

    .line 625
    sget-object v1, Lmiui/android/animation/property/ViewProperty;->WIDTH:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {p1, v1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->HEIGHT:Lmiui/android/animation/property/ViewProperty;

    .line 626
    invoke-virtual {p1, v2}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v2

    .line 625
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 627
    .local v1, "maxSize":F
    const/high16 v2, 0x41400000    # 12.0f

    add-float/2addr v2, v1

    div-float/2addr v2, v1

    const v3, 0x3f933333    # 1.15f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 628
    .local v2, "scaleValue":F
    const/16 v3, 0x14

    .line 629
    .local v3, "gravityEdge":I
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    iput v4, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I

    .line 630
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v4

    iput v4, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I

    .line 631
    iget v5, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I

    mul-int/lit8 v6, v3, 0x2

    sub-int/2addr v5, v6

    .line 632
    .local v5, "gravityW":I
    mul-int/lit8 v6, v3, 0x2

    sub-int/2addr v4, v6

    .line 634
    .local v4, "gravityH":I
    int-to-float v6, v5

    const/4 v7, 0x0

    const/high16 v8, 0x43b40000    # 360.0f

    invoke-direct {p0, v6, v7, v8}, Lmiui/android/animation/controller/FolmeHover;->perFromVal(FFF)F

    move-result v6

    .line 635
    .local v6, "wPer":F
    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v9, v6}, Ljava/lang/Math;->min(FF)F

    move-result v10

    invoke-static {v7, v10}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 636
    const/high16 v10, 0x41700000    # 15.0f

    invoke-direct {p0, v6, v10, v7}, Lmiui/android/animation/controller/FolmeHover;->valFromPer(FFF)F

    move-result v11

    .line 637
    .local v11, "ox":F
    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 639
    int-to-float v12, v4

    invoke-direct {p0, v12, v7, v8}, Lmiui/android/animation/controller/FolmeHover;->perFromVal(FFF)F

    move-result v8

    .line 640
    .local v8, "hPer":F
    invoke-static {v9, v8}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static {v7, v12}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 641
    invoke-direct {p0, v8, v10, v7}, Lmiui/android/animation/controller/FolmeHover;->valFromPer(FFF)F

    move-result v12

    .line 642
    .local v12, "oy":F
    invoke-static {v10, v12}, Ljava/lang/Math;->min(FF)F

    move-result v10

    .line 643
    .end local v12    # "oy":F
    .local v10, "oy":F
    cmpl-float v9, v2, v9

    if-nez v9, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v11, v10}, Ljava/lang/Math;->min(FF)F

    move-result v7

    :goto_1
    iput v7, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslateDist:F

    .line 645
    iget v7, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I

    iget v9, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I

    if-ne v7, v9, :cond_2

    const/16 v12, 0x64

    if-ge v7, v12, :cond_2

    if-ge v9, v12, :cond_2

    .line 646
    int-to-float v7, v7

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v7, v9

    float-to-int v7, v7

    int-to-float v7, v7

    invoke-virtual {p0, v7}, Lmiui/android/animation/controller/FolmeHover;->setCorner(F)Lmiui/android/animation/IHoverStyle;

    goto :goto_2

    .line 648
    :cond_2
    const/high16 v7, 0x42100000    # 36.0f

    invoke-virtual {p0, v7}, Lmiui/android/animation/controller/FolmeHover;->setCorner(F)Lmiui/android/animation/IHoverStyle;

    .line 651
    .end local v1    # "maxSize":F
    .end local v2    # "scaleValue":F
    .end local v3    # "gravityEdge":I
    .end local v4    # "gravityH":I
    .end local v5    # "gravityW":I
    .end local v6    # "wPer":F
    .end local v8    # "hPer":F
    .end local v10    # "oy":F
    .end local v11    # "ox":F
    :cond_3
    :goto_2
    return-void
.end method

.method private static isMagicView(Landroid/view/View;)Z
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .line 700
    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.view.View"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 701
    .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v2, "isMagicView"

    new-array v3, v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 702
    .local v2, "sGet":Ljava/lang/reflect/Method;
    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 703
    .end local v1    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "sGet":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 704
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isMagicView failed , e:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    .end local v1    # "e":Ljava/lang/Exception;
    return v0
.end method

.method static isOnHoverView(Landroid/view/View;[ILandroid/view/MotionEvent;)Z
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "location"    # [I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 420
    const/4 v0, 0x1

    if-eqz p0, :cond_1

    .line 421
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 422
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    .line 423
    .local v1, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    .line 424
    .local v2, "y":I
    const/4 v3, 0x0

    aget v4, p1, v3

    if-lt v1, v4, :cond_0

    aget v4, p1, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    if-gt v1, v4, :cond_0

    aget v4, p1, v0

    if-lt v2, v4, :cond_0

    aget v4, p1, v0

    .line 425
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    if-gt v2, v4, :cond_0

    goto :goto_0

    :cond_0
    move v0, v3

    .line 424
    :goto_0
    return v0

    .line 427
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_1
    return v0
.end method

.method private isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z
    .locals 2
    .param p1, "type"    # Lmiui/android/animation/IHoverStyle$HoverType;

    .line 585
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z
    .locals 2
    .param p1, "type"    # Lmiui/android/animation/IHoverStyle$HoverType;

    .line 589
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslationSetMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private varargs onEventEnter(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 391
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventEnter, touchEnter"

    invoke-static {v1, v0}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 394
    :cond_0
    invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->hoverEnter([Lmiui/android/animation/base/AnimConfig;)V

    .line 395
    return-void
.end method

.method private varargs onEventExit(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 406
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z

    if-eqz v0, :cond_1

    .line 407
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventExit, touchExit"

    invoke-static {v1, v0}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 410
    :cond_0
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->hoverExit(Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 411
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->resetTouchStatus()V

    .line 413
    :cond_1
    return-void
.end method

.method private varargs onEventMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 398
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z

    if-eqz v0, :cond_0

    .line 399
    if-eqz p1, :cond_0

    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->isTranslationSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    if-eqz v0, :cond_0

    .line 400
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->actualTranslatDist(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 403
    :cond_0
    return-void
.end method

.method private perFromVal(FFF)F
    .locals 2
    .param p1, "val"    # F
    .param p2, "from"    # F
    .param p3, "to"    # F

    .line 615
    sub-float v0, p1, p2

    sub-float v1, p3, p2

    div-float/2addr v0, v1

    return v0
.end method

.method private resetTouchStatus()V
    .locals 1

    .line 416
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z

    .line 417
    return-void
.end method

.method private resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 676
    .local p1, "viewHolder":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/view/View;>;"
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 677
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 678
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 680
    :cond_0
    return-object v0
.end method

.method private setAutoRound()V
    .locals 0

    .line 123
    return-void
.end method

.method private setAutoScale()V
    .locals 4

    .line 105
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    .line 108
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 110
    return-void
.end method

.method private setAutoTranslation()V
    .locals 4

    .line 113
    const/4 v0, 0x1

    .line 114
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 113
    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    .line 114
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslationSetMap:Ljava/util/Map;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslationSetMap:Ljava/util/Map;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_X:Lmiui/android/animation/property/ViewProperty;

    .line 117
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_Y:Lmiui/android/animation/property/ViewProperty;

    .line 118
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 119
    return-void
.end method

.method private setHoverView(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .line 332
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mHoverView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 333
    .local v0, "hoverView":Landroid/view/View;
    :goto_0
    if-ne v0, p1, :cond_1

    .line 334
    const/4 v1, 0x0

    return v1

    .line 336
    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mHoverView:Ljava/lang/ref/WeakReference;

    .line 337
    const/4 v1, 0x1

    return v1
.end method

.method private static setMagicView(Landroid/view/View;Z)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "isMagicView"    # Z

    .line 724
    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 727
    .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 728
    .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v4, "setMagicView"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 729
    .local v4, "sGet":Ljava/lang/reflect/Method;
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v0, v3

    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 732
    nop

    .end local v1    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v2    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 730
    :catch_0
    move-exception v0

    .line 731
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setMagicView failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static setPointerHide(Landroid/view/View;Z)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "isWrapped"    # Z

    .line 750
    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 753
    .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 754
    .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v4, "setPointerHide"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 755
    .local v4, "sGet":Ljava/lang/reflect/Method;
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v0, v3

    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 758
    nop

    .end local v1    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v2    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 756
    :catch_0
    move-exception v0

    .line 757
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPointerHide failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static setPointerShape(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "pointerShape"    # Landroid/graphics/Bitmap;

    .line 763
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 766
    .local v0, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v1, "android.view.View"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 767
    .local v1, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v2, "setPointerShape"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 768
    .local v2, "sGet":Ljava/lang/reflect/Method;
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 771
    nop

    .end local v0    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v1    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 769
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPointerShape failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private static setPointerShapeType(Landroid/view/View;I)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "pointerShapeType"    # I

    .line 776
    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 779
    .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 780
    .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v4, "setPointerShapeType"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 781
    .local v4, "sGet":Ljava/lang/reflect/Method;
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v3

    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 784
    nop

    .end local v1    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v2    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 782
    :catch_0
    move-exception v0

    .line 783
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setPointerShapeType failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 785
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private setTintColor()V
    .locals 7

    .line 126
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mSetTint:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 129
    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 130
    .local v0, "tintColor":I
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v1

    .line 131
    .local v1, "target":Ljava/lang/Object;
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_2

    .line 132
    move-object v2, v1

    check-cast v2, Landroid/view/View;

    .line 133
    .local v2, "view":Landroid/view/View;
    sget v3, Lmiui/android/folme/R$color;->miuix_folme_color_touch_tint:I

    .line 134
    .local v3, "colorRes":I
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "uimode"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/UiModeManager;

    .line 135
    .local v4, "mgr":Landroid/app/UiModeManager;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 136
    sget v3, Lmiui/android/folme/R$color;->miuix_folme_color_touch_tint_dark:I

    .line 138
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 140
    .end local v2    # "view":Landroid/view/View;
    .end local v3    # "colorRes":I
    .end local v4    # "mgr":Landroid/app/UiModeManager;
    :cond_2
    sget-object v2, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    .line 141
    .local v2, "propFg":Lmiui/android/animation/property/FloatProperty;
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v4, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v3, v4}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    int-to-double v4, v0

    invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 142
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v4, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v3, v4}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 143
    return-void

    .line 127
    .end local v0    # "tintColor":I
    .end local v1    # "target":Ljava/lang/Object;
    .end local v2    # "propFg":Lmiui/android/animation/property/FloatProperty;
    :cond_3
    :goto_0
    return-void
.end method

.method private static setWrapped(Landroid/view/View;Z)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "isWrapped"    # Z

    .line 737
    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 740
    .local v1, "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 741
    .local v2, "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string/jumbo v4, "setWrapped"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 742
    .local v4, "sGet":Ljava/lang/reflect/Method;
    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v0, v3

    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    nop

    .end local v1    # "parameterTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v2    # "forName":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "sGet":Ljava/lang/reflect/Method;
    goto :goto_0

    .line 743
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setWrapped failed , e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateHoverState(Lmiui/android/animation/IHoverStyle$HoverEffect;)V
    .locals 2
    .param p1, "effect"    # Lmiui/android/animation/IHoverStyle$HoverEffect;

    .line 297
    sget-object v0, Lmiui/android/animation/controller/FolmeHover$2;->$SwitchMap$miui$android$animation$IHoverStyle$HoverEffect:[I

    invoke-virtual {p1}, Lmiui/android/animation/IHoverStyle$HoverEffect;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 320
    :pswitch_0
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->NORMAL:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-ne v0, v1, :cond_1

    .line 321
    :cond_0
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeHover;->clearTintColor()Lmiui/android/animation/IHoverStyle;

    .line 323
    :cond_1
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoScale()V

    .line 324
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoTranslation()V

    .line 325
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoRound()V

    .line 326
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    goto :goto_1

    .line 311
    :pswitch_1
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED_WRAPPED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-ne v0, v1, :cond_2

    .line 312
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearRound()V

    .line 314
    :cond_2
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V

    .line 315
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoScale()V

    .line 316
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setAutoTranslation()V

    .line 317
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    .line 318
    goto :goto_1

    .line 299
    :pswitch_2
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-ne v0, v1, :cond_3

    .line 300
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearScale()V

    .line 301
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearTranslation()V

    goto :goto_0

    .line 302
    :cond_3
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED_WRAPPED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-ne v0, v1, :cond_4

    .line 303
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearScale()V

    .line 304
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearTranslation()V

    .line 305
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->clearRound()V

    .line 307
    :cond_4
    :goto_0
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V

    .line 308
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    .line 309
    nop

    .line 329
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private valFromPer(FFF)F
    .locals 1
    .param p1, "per"    # F
    .param p2, "from"    # F
    .param p3, "to"    # F

    .line 619
    sub-float v0, p3, p2

    mul-float/2addr v0, p1

    add-float/2addr v0, p2

    return v0
.end method


# virtual methods
.method public addMagicPoint(Landroid/graphics/Point;)V
    .locals 2
    .param p1, "magicPoint"    # Landroid/graphics/Point;

    .line 570
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 571
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 572
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p1}, Lmiui/android/animation/controller/FolmeHover;->addMagicPoint(Landroid/view/View;Landroid/graphics/Point;)V

    .line 574
    :cond_0
    return-void
.end method

.method public clean()V
    .locals 2

    .line 598
    invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V

    .line 599
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 600
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mHoverView:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 601
    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    .line 602
    iput-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mHoverView:Ljava/lang/ref/WeakReference;

    .line 604
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mChildView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 605
    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    .line 606
    iput-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mChildView:Ljava/lang/ref/WeakReference;

    .line 608
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    .line 609
    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    .line 610
    iput-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    .line 612
    :cond_2
    return-void
.end method

.method public clearMagicPoint()V
    .locals 2

    .line 578
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 579
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 580
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lmiui/android/animation/controller/FolmeHover;->clearMagicPoint(Landroid/view/View;)V

    .line 582
    :cond_0
    return-void
.end method

.method public clearTintColor()Lmiui/android/animation/IHoverStyle;
    .locals 3

    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z

    .line 263
    sget-object v0, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    .line 264
    .local v0, "propFg":Lmiui/android/animation/property/FloatProperty;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 265
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 266
    return-object p0
.end method

.method public varargs handleHoverOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 284
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeHover;->doHandleHoverOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V

    .line 285
    return-void
.end method

.method public varargs hoverEnter([Lmiui/android/animation/base/AnimConfig;)V
    .locals 9
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 450
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mIsEnter:Z

    .line 451
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED_WRAPPED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-ne v1, v2, :cond_1

    .line 452
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mHoverView:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 453
    .local v1, "hoverView":Landroid/view/View;
    :goto_0
    if-eqz v1, :cond_1

    .line 454
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mViewRect:Landroid/graphics/Rect;

    .line 455
    invoke-static {v1, v0}, Lmiui/android/animation/controller/FolmeHover;->setMagicView(Landroid/view/View;Z)V

    .line 456
    invoke-static {v1, v0}, Lmiui/android/animation/controller/FolmeHover;->setWrapped(Landroid/view/View;Z)V

    .line 457
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeHover;->isHideHover()Z

    .line 462
    .end local v1    # "hoverView":Landroid/view/View;
    :cond_1
    iget v0, p0, Lmiui/android/animation/controller/FolmeHover;->mRadius:F

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->setCorner(F)Lmiui/android/animation/IHoverStyle;

    .line 463
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V

    .line 464
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->getEnterConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    .line 465
    .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    .line 466
    .local v1, "state":Lmiui/android/animation/controller/AnimState;
    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-direct {p0, v2}, Lmiui/android/animation/controller/FolmeHover;->isScaleSet(Lmiui/android/animation/IHoverStyle$HoverType;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 467
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    .line 468
    .local v2, "target":Lmiui/android/animation/IAnimTarget;
    sget-object v3, Lmiui/android/animation/property/ViewProperty;->WIDTH:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v2, v3}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v3

    sget-object v4, Lmiui/android/animation/property/ViewProperty;->HEIGHT:Lmiui/android/animation/property/ViewProperty;

    .line 469
    invoke-virtual {v2, v4}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v4

    .line 468
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 470
    .local v3, "maxSize":F
    const/high16 v4, 0x41400000    # 12.0f

    add-float/2addr v4, v3

    div-float/2addr v4, v3

    const v5, 0x3f933333    # 1.15f

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 471
    .local v4, "scaleValue":F
    sget-object v5, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v6, v4

    invoke-virtual {v1, v5, v6, v7}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v5

    sget-object v6, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v7, v4

    .line 472
    invoke-virtual {v5, v6, v7, v8}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 475
    .end local v2    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v3    # "maxSize":F
    .end local v4    # "scaleValue":F
    :cond_2
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_3

    .line 476
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    filled-new-array {v2}, [Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lmiui/android/animation/Folme;->useAt([Landroid/view/View;)Lmiui/android/animation/IFolme;

    move-result-object v2

    .line 477
    invoke-interface {v2}, Lmiui/android/animation/IFolme;->state()Lmiui/android/animation/IStateStyle;

    move-result-object v2

    sget-object v3, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    .line 478
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4}, Lmiui/android/animation/IStateStyle;->add(Lmiui/android/animation/property/FloatProperty;F)Lmiui/android/animation/IStateStyle;

    move-result-object v2

    sget-object v3, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    .line 479
    invoke-interface {v2, v3, v4}, Lmiui/android/animation/IStateStyle;->add(Lmiui/android/animation/property/FloatProperty;F)Lmiui/android/animation/IStateStyle;

    move-result-object v2

    .line 480
    invoke-interface {v2, v0}, Lmiui/android/animation/IStateStyle;->to([Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 482
    :cond_3
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v2, v1, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 483
    return-void
.end method

.method public varargs hoverExit([Lmiui/android/animation/base/AnimConfig;)V
    .locals 4
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 504
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->getExitConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    .line 505
    .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v3, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v2, v3}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 506
    return-void
.end method

.method public varargs hoverMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 487
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->onEventMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 488
    return-void
.end method

.method public ignoreHoverOf(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .line 432
    sget-object v0, Lmiui/android/animation/controller/FolmeHover;->sHoverRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;

    .line 433
    .local v0, "hoverListener":Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;->removeHover(Lmiui/android/animation/controller/FolmeHover;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 434
    sget-object v1, Lmiui/android/animation/controller/FolmeHover;->sHoverRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    :cond_0
    return-void
.end method

.method public isHideHover()Z
    .locals 2

    .line 692
    iget v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetWidth:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    iget v0, p0, Lmiui/android/animation/controller/FolmeHover;->mTargetHeight:I

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mCurrentEffect:Lmiui/android/animation/IHoverStyle$HoverEffect;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverEffect;->FLOATED_WRAPPED:Lmiui/android/animation/IHoverStyle$HoverEffect;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isMagicView()Z
    .locals 2

    .line 521
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 522
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 523
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lmiui/android/animation/controller/FolmeHover;->isMagicView(Landroid/view/View;)Z

    move-result v1

    return v1

    .line 525
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 445
    const/4 v0, 0x0

    new-array v0, v0, [Lmiui/android/animation/base/AnimConfig;

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lmiui/android/animation/controller/FolmeHover;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 446
    return-void
.end method

.method public varargs onMotionEventEx(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 440
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeHover;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 441
    return-void
.end method

.method public varargs setAlpha(F[Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle;
    .locals 4
    .param p1, "alpha"    # F
    .param p2, "type"    # [Lmiui/android/animation/IHoverStyle$HoverType;

    .line 167
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType;

    move-result-object v1

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    float-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 168
    return-object p0
.end method

.method public setBackgroundColor(FFFF)Lmiui/android/animation/IHoverStyle;
    .locals 4
    .param p1, "a"    # F
    .param p2, "r"    # F
    .param p3, "g"    # F
    .param p4, "b"    # F

    .line 218
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float v1, p1, v0

    float-to-int v1, v1

    mul-float v2, p2, v0

    float-to-int v2, v2

    mul-float v3, p3, v0

    float-to-int v3, v3

    mul-float/2addr v0, p4

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->setBackgroundColor(I)Lmiui/android/animation/IHoverStyle;

    move-result-object v0

    return-object v0
.end method

.method public setBackgroundColor(I)Lmiui/android/animation/IHoverStyle;
    .locals 5
    .param p1, "color"    # I

    .line 224
    sget-object v0, Lmiui/android/animation/property/ViewPropertyExt;->BACKGROUND:Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;

    .line 225
    .local v0, "propBg":Lmiui/android/animation/property/FloatProperty;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    int-to-double v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 226
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    .line 227
    invoke-interface {v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-static {v2, v0, v3, v4}, Lmiui/android/animation/internal/AnimValueUtils;->getValueOfTarget(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-double v2, v2

    .line 226
    invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 228
    return-object p0
.end method

.method public setCorner(F)Lmiui/android/animation/IHoverStyle;
    .locals 4
    .param p1, "radius"    # F

    .line 194
    iput p1, p0, Lmiui/android/animation/controller/FolmeHover;->mRadius:F

    .line 195
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 196
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 197
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    const v2, 0x100b0008

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 199
    :cond_0
    return-object p0
.end method

.method public setEffect(Lmiui/android/animation/IHoverStyle$HoverEffect;)Lmiui/android/animation/IHoverStyle;
    .locals 0
    .param p1, "effect"    # Lmiui/android/animation/IHoverStyle$HoverEffect;

    .line 278
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeHover;->updateHoverState(Lmiui/android/animation/IHoverStyle$HoverEffect;)V

    .line 279
    return-object p0
.end method

.method public setHoverEnter()V
    .locals 2

    .line 510
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover;->setTintColor()V

    .line 511
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 512
    return-void
.end method

.method public setHoverExit()V
    .locals 2

    .line 516
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 517
    return-void
.end method

.method public setMagicView(Z)V
    .locals 2
    .param p1, "isMagicView"    # Z

    .line 530
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 531
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 532
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p1}, Lmiui/android/animation/controller/FolmeHover;->setMagicView(Landroid/view/View;Z)V

    .line 534
    :cond_0
    return-void
.end method

.method public setParentView(Landroid/view/View;)Lmiui/android/animation/IHoverStyle;
    .locals 2
    .param p1, "parent"    # Landroid/view/View;

    .line 157
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 158
    .local v0, "parentView":Landroid/view/View;
    :goto_0
    if-ne p1, v0, :cond_1

    .line 159
    return-object p0

    .line 161
    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mParentView:Ljava/lang/ref/WeakReference;

    .line 162
    return-object p0
.end method

.method public setPointerHide(Z)V
    .locals 2
    .param p1, "isWrapped"    # Z

    .line 546
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 547
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 548
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p1}, Lmiui/android/animation/controller/FolmeHover;->setPointerHide(Landroid/view/View;Z)V

    .line 550
    :cond_0
    return-void
.end method

.method public setPointerShape(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "pointerShape"    # Landroid/graphics/Bitmap;

    .line 554
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 555
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 556
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p1}, Lmiui/android/animation/controller/FolmeHover;->setPointerShape(Landroid/view/View;Landroid/graphics/Bitmap;)V

    .line 558
    :cond_0
    return-void
.end method

.method public setPointerShapeType(I)V
    .locals 2
    .param p1, "pointerShapeType"    # I

    .line 562
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 563
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 564
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p1}, Lmiui/android/animation/controller/FolmeHover;->setPointerShapeType(Landroid/view/View;I)V

    .line 566
    :cond_0
    return-void
.end method

.method public varargs setScale(F[Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle;
    .locals 5
    .param p1, "scale"    # F
    .param p2, "type"    # [Lmiui/android/animation/IHoverStyle$HoverType;

    .line 173
    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType;

    move-result-object v0

    .line 174
    .local v0, "relType":Lmiui/android/animation/IHoverStyle$HoverType;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mScaleSetMap:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v3, p1

    .line 176
    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v3, p1

    .line 177
    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 178
    return-object p0
.end method

.method public setTint(FFFF)Lmiui/android/animation/IHoverStyle;
    .locals 4
    .param p1, "a"    # F
    .param p2, "r"    # F
    .param p3, "g"    # F
    .param p4, "b"    # F

    .line 212
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float v1, p1, v0

    float-to-int v1, v1

    mul-float v2, p2, v0

    float-to-int v2, v2

    mul-float v3, p3, v0

    float-to-int v3, v3

    mul-float/2addr v0, p4

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeHover;->setTint(I)Lmiui/android/animation/IHoverStyle;

    move-result-object v0

    return-object v0
.end method

.method public setTint(I)Lmiui/android/animation/IHoverStyle;
    .locals 4
    .param p1, "color"    # I

    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mSetTint:Z

    .line 205
    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->mClearTint:Z

    .line 206
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 207
    return-object p0
.end method

.method public setTintMode(I)Lmiui/android/animation/IHoverStyle;
    .locals 1
    .param p1, "mode"    # I

    .line 271
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mEnterConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;

    .line 272
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mExitConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;

    .line 273
    return-object p0
.end method

.method public varargs setTranslate(F[Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle;
    .locals 5
    .param p1, "translate"    # F
    .param p2, "type"    # [Lmiui/android/animation/IHoverStyle$HoverType;

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeHover;->isSetAutoTranslation:Z

    .line 184
    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeHover;->getType([Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle$HoverType;

    move-result-object v0

    .line 185
    .local v0, "relType":Lmiui/android/animation/IHoverStyle$HoverType;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mTranslationSetMap:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v3, p1

    .line 187
    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->TRANSLATION_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v3, p1

    .line 188
    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 189
    return-object p0
.end method

.method public setWrapped(Z)V
    .locals 2
    .param p1, "isWrapped"    # Z

    .line 538
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeHover;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 539
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 540
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, p1}, Lmiui/android/animation/controller/FolmeHover;->setWrapped(Landroid/view/View;Z)V

    .line 542
    :cond_0
    return-void
.end method
