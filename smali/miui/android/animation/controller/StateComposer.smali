.class public Lmiui/android/animation/controller/StateComposer;
.super Ljava/lang/Object;
.source "StateComposer.java"


# static fields
.field private static final METHOD_GET_STATE:Ljava/lang/String; = "getState"

.field private static final sInterceptor:Lmiui/android/animation/utils/StyleComposer$IInterceptor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/android/animation/utils/StyleComposer$IInterceptor<",
            "Lmiui/android/animation/controller/IFolmeStateStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lmiui/android/animation/controller/StateComposer$1;

    invoke-direct {v0}, Lmiui/android/animation/controller/StateComposer$1;-><init>()V

    sput-object v0, Lmiui/android/animation/controller/StateComposer;->sInterceptor:Lmiui/android/animation/utils/StyleComposer$IInterceptor;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs composeStyle([Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/controller/IFolmeStateStyle;
    .locals 4
    .param p0, "targets"    # [Lmiui/android/animation/IAnimTarget;

    .line 36
    if-eqz p0, :cond_3

    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_1

    .line 39
    :cond_0
    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 40
    new-instance v0, Lmiui/android/animation/controller/FolmeState;

    const/4 v1, 0x0

    aget-object v1, p0, v1

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeState;-><init>(Lmiui/android/animation/IAnimTarget;)V

    return-object v0

    .line 42
    :cond_1
    array-length v0, p0

    new-array v0, v0, [Lmiui/android/animation/controller/FolmeState;

    .line 43
    .local v0, "array":[Lmiui/android/animation/controller/FolmeState;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    .line 44
    new-instance v2, Lmiui/android/animation/controller/FolmeState;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Lmiui/android/animation/controller/FolmeState;-><init>(Lmiui/android/animation/IAnimTarget;)V

    aput-object v2, v0, v1

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "i":I
    :cond_2
    const-class v1, Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/controller/StateComposer;->sInterceptor:Lmiui/android/animation/utils/StyleComposer$IInterceptor;

    invoke-static {v1, v2, v0}, Lmiui/android/animation/utils/StyleComposer;->compose(Ljava/lang/Class;Lmiui/android/animation/utils/StyleComposer$IInterceptor;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/controller/IFolmeStateStyle;

    return-object v1

    .line 37
    .end local v0    # "array":[Lmiui/android/animation/controller/FolmeState;
    :cond_3
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method
