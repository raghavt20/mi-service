.class final Lmiui/android/animation/controller/FolmeTouch$LongClickTask;
.super Ljava/lang/Object;
.source "FolmeTouch.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/controller/FolmeTouch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LongClickTask"
.end annotation


# instance fields
.field private mTouchRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiui/android/animation/controller/FolmeTouch;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiui/android/animation/controller/FolmeTouch$1;)V
    .locals 0
    .param p1, "x0"    # Lmiui/android/animation/controller/FolmeTouch$1;

    .line 478
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 505
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->mTouchRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/FolmeTouch;

    .line 507
    .local v0, "touch":Lmiui/android/animation/controller/FolmeTouch;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    move-object v2, v1

    .local v2, "target":Lmiui/android/animation/IAnimTarget;
    instance-of v1, v1, Lmiui/android/animation/ViewTarget;

    if-eqz v1, :cond_0

    .line 508
    invoke-virtual {v2}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 509
    .local v1, "targetView":Landroid/view/View;
    if-eqz v1, :cond_0

    invoke-static {v0}, Lmiui/android/animation/controller/FolmeTouch;->access$900(Lmiui/android/animation/controller/FolmeTouch;)Landroid/view/View$OnLongClickListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 510
    invoke-virtual {v1}, Landroid/view/View;->performLongClick()Z

    .line 511
    invoke-static {v0, v1}, Lmiui/android/animation/controller/FolmeTouch;->access$400(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;)V

    .line 514
    .end local v1    # "targetView":Landroid/view/View;
    .end local v2    # "target":Lmiui/android/animation/IAnimTarget;
    :cond_0
    return-void
.end method

.method start(Lmiui/android/animation/controller/FolmeTouch;)V
    .locals 4
    .param p1, "touch"    # Lmiui/android/animation/controller/FolmeTouch;

    .line 483
    iget-object v0, p1, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 484
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    instance-of v1, v0, Lmiui/android/animation/ViewTarget;

    if-eqz v1, :cond_0

    .line 485
    move-object v1, v0

    check-cast v1, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v1}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v1

    .line 486
    .local v1, "targetView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 487
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->mTouchRef:Ljava/lang/ref/WeakReference;

    .line 488
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 491
    .end local v1    # "targetView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method stop(Lmiui/android/animation/controller/FolmeTouch;)V
    .locals 2
    .param p1, "touch"    # Lmiui/android/animation/controller/FolmeTouch;

    .line 494
    iget-object v0, p1, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 495
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    instance-of v1, v0, Lmiui/android/animation/ViewTarget;

    if-eqz v1, :cond_0

    .line 496
    move-object v1, v0

    check-cast v1, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v1}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v1

    .line 497
    .local v1, "targetView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 498
    invoke-virtual {v1, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 501
    .end local v1    # "targetView":Landroid/view/View;
    :cond_0
    return-void
.end method
