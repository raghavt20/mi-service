.class public abstract Lmiui/android/animation/controller/FolmeBase;
.super Ljava/lang/Object;
.source "FolmeBase.java"

# interfaces
.implements Lmiui/android/animation/IStateContainer;


# instance fields
.field mState:Lmiui/android/animation/controller/IFolmeStateStyle;


# direct methods
.method varargs constructor <init>([Lmiui/android/animation/IAnimTarget;)V
    .locals 1
    .param p1, "targets"    # [Lmiui/android/animation/IAnimTarget;

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lmiui/android/animation/controller/StateComposer;->composeStyle([Lmiui/android/animation/IAnimTarget;)Lmiui/android/animation/controller/IFolmeStateStyle;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    .line 18
    return-void
.end method


# virtual methods
.method public varargs addConfig(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "configs"    # [Lmiui/android/animation/base/AnimConfig;

    .line 63
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 64
    invoke-interface {v0, p1, p2}, Lmiui/android/animation/controller/IFolmeStateStyle;->addConfig(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)V

    .line 66
    :cond_0
    return-void
.end method

.method public cancel()V
    .locals 1

    .line 35
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 36
    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->cancel()V

    .line 38
    :cond_0
    return-void
.end method

.method public varargs cancel([Ljava/lang/String;)V
    .locals 1
    .param p1, "propertyNames"    # [Ljava/lang/String;

    .line 49
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 50
    invoke-interface {v0, p1}, Lmiui/android/animation/controller/IFolmeStateStyle;->cancel([Ljava/lang/String;)V

    .line 52
    :cond_0
    return-void
.end method

.method public varargs cancel([Lmiui/android/animation/property/FloatProperty;)V
    .locals 1
    .param p1, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 42
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 43
    invoke-interface {v0, p1}, Lmiui/android/animation/controller/IFolmeStateStyle;->cancel([Lmiui/android/animation/property/FloatProperty;)V

    .line 45
    :cond_0
    return-void
.end method

.method public clean()V
    .locals 1

    .line 21
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 22
    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->clean()V

    .line 24
    :cond_0
    return-void
.end method

.method public enableDefaultAnim(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .line 56
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 57
    invoke-interface {v0, p1}, Lmiui/android/animation/controller/IFolmeStateStyle;->enableDefaultAnim(Z)V

    .line 59
    :cond_0
    return-void
.end method

.method public varargs end([Ljava/lang/Object;)V
    .locals 1
    .param p1, "properties"    # [Ljava/lang/Object;

    .line 28
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeBase;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    if-eqz v0, :cond_0

    .line 29
    invoke-interface {v0, p1}, Lmiui/android/animation/controller/IFolmeStateStyle;->end([Ljava/lang/Object;)V

    .line 31
    :cond_0
    return-void
.end method
