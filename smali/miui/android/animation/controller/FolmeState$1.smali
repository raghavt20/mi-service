.class Lmiui/android/animation/controller/FolmeState$1;
.super Ljava/lang/Object;
.source "FolmeState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/android/animation/controller/FolmeState;

.field final synthetic val$oneTimeConfig:Lmiui/android/animation/base/AnimConfigLink;

.field final synthetic val$tag:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lmiui/android/animation/controller/FolmeState;Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 0
    .param p1, "this$0"    # Lmiui/android/animation/controller/FolmeState;

    .line 52
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeState$1;->this$0:Lmiui/android/animation/controller/FolmeState;

    iput-object p2, p0, Lmiui/android/animation/controller/FolmeState$1;->val$tag:Ljava/lang/Object;

    iput-object p3, p0, Lmiui/android/animation/controller/FolmeState$1;->val$oneTimeConfig:Lmiui/android/animation/base/AnimConfigLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 55
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState$1;->this$0:Lmiui/android/animation/controller/FolmeState;

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState$1;->val$tag:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 56
    .local v0, "toState":Lmiui/android/animation/controller/AnimState;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState$1;->this$0:Lmiui/android/animation/controller/FolmeState;

    invoke-virtual {v1}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    .line 57
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FolmeState.setTo, state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    :cond_0
    iget-object v2, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v3, p0, Lmiui/android/animation/controller/FolmeState$1;->val$oneTimeConfig:Lmiui/android/animation/base/AnimConfigLink;

    invoke-virtual {v2, v0, v3}, Lmiui/android/animation/internal/AnimManager;->setTo(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 61
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeState$1;->this$0:Lmiui/android/animation/controller/FolmeState;

    iget-object v2, v2, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v2, v0}, Lmiui/android/animation/controller/StateManager;->clearTempState(Lmiui/android/animation/controller/AnimState;)V

    .line 62
    return-void
.end method
