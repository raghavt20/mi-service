.class public Lmiui/android/animation/controller/FolmeState;
.super Ljava/lang/Object;
.source "FolmeState.java"

# interfaces
.implements Lmiui/android/animation/controller/IFolmeStateStyle;


# instance fields
.field mConfigLink:Lmiui/android/animation/base/AnimConfigLink;

.field private mEnableAnim:Z

.field mStateMgr:Lmiui/android/animation/controller/StateManager;

.field mTarget:Lmiui/android/animation/IAnimTarget;


# direct methods
.method constructor <init>(Lmiui/android/animation/IAnimTarget;)V
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lmiui/android/animation/controller/StateManager;

    invoke-direct {v0}, Lmiui/android/animation/controller/StateManager;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    .line 23
    new-instance v0, Lmiui/android/animation/base/AnimConfigLink;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfigLink;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mConfigLink:Lmiui/android/animation/base/AnimConfigLink;

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeState;->mEnableAnim:Z

    .line 28
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    .line 29
    return-void
.end method

.method private fromTo(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle;
    .locals 5
    .param p1, "fromTag"    # Ljava/lang/Object;
    .param p2, "toTag"    # Ljava/lang/Object;
    .param p3, "oneTimeConfig"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 102
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeState;->mEnableAnim:Z

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p2}, Lmiui/android/animation/controller/StateManager;->setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 104
    if-eqz p1, :cond_0

    .line 105
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 107
    :cond_0
    invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 108
    .local v0, "toState":Lmiui/android/animation/controller/AnimState;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v1, v0, p3}, Lmiui/android/animation/controller/StateManager;->addTempConfig(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 109
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v1

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, p3}, Lmiui/android/animation/internal/AnimRunner;->run(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 110
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v1, v0}, Lmiui/android/animation/controller/StateManager;->clearTempState(Lmiui/android/animation/controller/AnimState;)V

    .line 111
    invoke-virtual {p3}, Lmiui/android/animation/base/AnimConfigLink;->clear()V

    .line 113
    .end local v0    # "toState":Lmiui/android/animation/controller/AnimState;
    :cond_1
    return-object p0
.end method

.method private getConfigLink()Lmiui/android/animation/base/AnimConfigLink;
    .locals 1

    .line 185
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mConfigLink:Lmiui/android/animation/base/AnimConfigLink;

    return-object v0
.end method

.method private setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle;
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "oneTimeConfig"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 46
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    if-nez v0, :cond_0

    .line 47
    return-object p0

    .line 49
    :cond_0
    instance-of v1, p1, Ljava/lang/Integer;

    if-nez v1, :cond_2

    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_1

    goto :goto_0

    .line 52
    :cond_1
    new-instance v1, Lmiui/android/animation/controller/FolmeState$1;

    invoke-direct {v1, p0, p1, p2}, Lmiui/android/animation/controller/FolmeState$1;-><init>(Lmiui/android/animation/controller/FolmeState;Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)V

    invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->executeOnInitialized(Ljava/lang/Runnable;)V

    .line 64
    return-object p0

    .line 50
    :cond_2
    :goto_0
    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeState;->setTo([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public add(Ljava/lang/String;F)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F

    .line 305
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;F)V

    .line 306
    return-object p0
.end method

.method public add(Ljava/lang/String;FJ)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F
    .param p3, "flag"    # J

    .line 311
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;FJ)V

    .line 312
    return-object p0
.end method

.method public add(Ljava/lang/String;I)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 299
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;I)V

    .line 300
    return-object p0
.end method

.method public add(Ljava/lang/String;IJ)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I
    .param p3, "flag"    # J

    .line 275
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;IJ)V

    .line 276
    return-object p0
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;F)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F

    .line 287
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;F)V

    .line 288
    return-object p0
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;FJ)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F
    .param p3, "flag"    # J

    .line 293
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;FJ)V

    .line 294
    return-object p0
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;I)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I

    .line 281
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;I)V

    .line 282
    return-object p0
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;IJ)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I
    .param p3, "flag"    # J

    .line 269
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;IJ)V

    .line 270
    return-object p0
.end method

.method public varargs addConfig(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "configs"    # [Lmiui/android/animation/base/AnimConfig;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 353
    return-void
.end method

.method public addInitProperty(Ljava/lang/String;F)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F

    .line 263
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Ljava/lang/String;F)V

    .line 264
    return-object p0
.end method

.method public addInitProperty(Ljava/lang/String;I)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 257
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Ljava/lang/String;I)V

    .line 258
    return-object p0
.end method

.method public addInitProperty(Lmiui/android/animation/property/FloatProperty;F)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F

    .line 251
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Lmiui/android/animation/property/FloatProperty;F)V

    .line 252
    return-object p0
.end method

.method public addInitProperty(Lmiui/android/animation/property/FloatProperty;I)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I

    .line 245
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Lmiui/android/animation/property/FloatProperty;I)V

    .line 246
    return-object p0
.end method

.method public addListener(Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "listener"    # Lmiui/android/animation/listener/TransitionListener;

    .line 233
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->addListener(Lmiui/android/animation/listener/TransitionListener;)V

    .line 234
    return-object p0
.end method

.method public addState(Lmiui/android/animation/controller/AnimState;)V
    .locals 1
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;

    .line 216
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->addState(Lmiui/android/animation/controller/AnimState;)V

    .line 217
    return-void
.end method

.method public varargs autoSetTo([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 0
    .param p1, "propertyAndValues"    # [Ljava/lang/Object;

    .line 199
    return-object p0
.end method

.method public cancel()V
    .locals 4

    .line 128
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    const/4 v2, 0x0

    move-object v3, v2

    check-cast v3, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/internal/AnimRunner;->cancel(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V

    .line 129
    return-void
.end method

.method public varargs cancel([Ljava/lang/String;)V
    .locals 3
    .param p1, "propertyNames"    # [Ljava/lang/String;

    .line 138
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 139
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    array-length v1, p1

    if-eqz v1, :cond_1

    instance-of v1, v0, Lmiui/android/animation/ValueTarget;

    if-nez v1, :cond_0

    goto :goto_0

    .line 142
    :cond_0
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v1

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v1, v2, p1}, Lmiui/android/animation/internal/AnimRunner;->cancel(Lmiui/android/animation/IAnimTarget;[Ljava/lang/String;)V

    .line 143
    return-void

    .line 140
    :cond_1
    :goto_0
    return-void
.end method

.method public varargs cancel([Lmiui/android/animation/property/FloatProperty;)V
    .locals 2
    .param p1, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 133
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1, p1}, Lmiui/android/animation/internal/AnimRunner;->cancel(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V

    .line 134
    return-void
.end method

.method public clean()V
    .locals 0

    .line 123
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->cancel()V

    .line 124
    return-void
.end method

.method public enableDefaultAnim(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 118
    iput-boolean p1, p0, Lmiui/android/animation/controller/FolmeState;->mEnableAnim:Z

    .line 119
    return-void
.end method

.method public varargs end([Ljava/lang/Object;)V
    .locals 3
    .param p1, "propertyList"    # [Ljava/lang/Object;

    .line 147
    array-length v0, p1

    if-lez v0, :cond_1

    .line 148
    const/4 v0, 0x0

    aget-object v1, p1, v0

    instance-of v1, v1, Lmiui/android/animation/property/FloatProperty;

    if-eqz v1, :cond_0

    .line 149
    array-length v1, p1

    new-array v1, v1, [Lmiui/android/animation/property/FloatProperty;

    .line 150
    .local v1, "propList":[Lmiui/android/animation/property/FloatProperty;
    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v2, v1}, Lmiui/android/animation/internal/AnimRunner;->end(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V

    .line 152
    .end local v1    # "propList":[Lmiui/android/animation/property/FloatProperty;
    goto :goto_0

    .line 153
    :cond_0
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/String;

    .line 154
    .local v1, "propList":[Ljava/lang/String;
    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v2, v1}, Lmiui/android/animation/internal/AnimRunner;->end(Lmiui/android/animation/IAnimTarget;[Ljava/lang/String;)V

    .line 158
    .end local v1    # "propList":[Ljava/lang/String;
    :cond_1
    :goto_0
    return-void
.end method

.method public varargs fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
    .locals 6
    .param p1, "fromTag"    # Ljava/lang/Object;
    .param p2, "toTag"    # Ljava/lang/Object;
    .param p3, "oneTimeConfig"    # [Lmiui/android/animation/base/AnimConfig;

    .line 94
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink;

    move-result-object v0

    .line 95
    .local v0, "configLink":Lmiui/android/animation/base/AnimConfigLink;
    array-length v1, p3

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p3, v3

    .line 96
    .local v4, "config":Lmiui/android/animation/base/AnimConfig;
    new-array v5, v2, [Z

    invoke-virtual {v0, v4, v5}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V

    .line 95
    .end local v4    # "config":Lmiui/android/animation/base/AnimConfig;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 98
    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lmiui/android/animation/controller/FolmeState;->fromTo(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle;

    move-result-object v1

    return-object v1
.end method

.method public getCurrentState()Lmiui/android/animation/controller/AnimState;
    .locals 1

    .line 341
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public getTarget()Lmiui/android/animation/IAnimTarget;
    .locals 1

    .line 32
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mTarget:Lmiui/android/animation/IAnimTarget;

    return-object v0
.end method

.method public varargs predictDuration([Ljava/lang/Object;)J
    .locals 6
    .param p1, "propertyAndValues"    # [Ljava/lang/Object;

    .line 175
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 176
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink;

    move-result-object v1

    .line 177
    .local v1, "configLink":Lmiui/android/animation/base/AnimConfigLink;
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v2, v0, v1, p1}, Lmiui/android/animation/controller/StateManager;->getToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v2

    .line 178
    .local v2, "state":Lmiui/android/animation/controller/AnimState;
    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v1}, Lmiui/android/animation/internal/PredictTask;->predictDuration(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)J

    move-result-wide v3

    .line 179
    .local v3, "duration":J
    iget-object v5, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v5, v2}, Lmiui/android/animation/controller/StateManager;->clearTempState(Lmiui/android/animation/controller/AnimState;)V

    .line 180
    invoke-virtual {v1}, Lmiui/android/animation/base/AnimConfigLink;->clear()V

    .line 181
    return-wide v3
.end method

.method public removeListener(Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "listener"    # Lmiui/android/animation/listener/TransitionListener;

    .line 239
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->removeListener(Lmiui/android/animation/listener/TransitionListener;)V

    .line 240
    return-object p0
.end method

.method public set(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 228
    return-object p0
.end method

.method public varargs setConfig(Lmiui/android/animation/base/AnimConfig;[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IStateStyle;
    .locals 0
    .param p1, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 318
    return-object p0
.end method

.method public varargs setEase(I[F)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "style"    # I
    .param p2, "factors"    # [F

    .line 329
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->setEase(I[F)V

    .line 330
    return-object p0
.end method

.method public varargs setEase(Lmiui/android/animation/property/FloatProperty;I[F)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "style"    # I
    .param p3, "factors"    # [F

    .line 335
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2, p3}, Lmiui/android/animation/controller/StateManager;->setEase(Lmiui/android/animation/property/FloatProperty;I[F)V

    .line 336
    return-object p0
.end method

.method public varargs setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 323
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;[Lmiui/android/animation/property/FloatProperty;)V

    .line 324
    return-object p0
.end method

.method public setFlags(J)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "flag"    # J

    .line 204
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 205
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/IAnimTarget;->setFlags(J)V

    .line 206
    return-object p0
.end method

.method public setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {p0, p1, v0}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method

.method public varargs setTo(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "oneTimeConfig"    # [Lmiui/android/animation/base/AnimConfig;

    .line 42
    invoke-static {p2}, Lmiui/android/animation/base/AnimConfigLink;->linkConfig([Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/base/AnimConfigLink;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method

.method public varargs setTo([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 3
    .param p1, "propertyAndValues"    # [Ljava/lang/Object;

    .line 162
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink;

    move-result-object v0

    .line 163
    .local v0, "configLink":Lmiui/android/animation/base/AnimConfigLink;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p1}, Lmiui/android/animation/controller/StateManager;->getSetToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    .line 164
    .local v1, "state":Lmiui/android/animation/controller/AnimState;
    invoke-direct {p0, v1, v0}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle;

    .line 165
    return-object p0
.end method

.method public varargs setTransitionFlags(J[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IStateStyle;
    .locals 2
    .param p1, "flags"    # J
    .param p3, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 346
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lmiui/android/animation/controller/StateManager;->setTransitionFlags(Ljava/lang/Object;J[Lmiui/android/animation/property/FloatProperty;)V

    .line 347
    return-object p0
.end method

.method public setup(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 222
    return-object p0
.end method

.method public varargs then(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
    .locals 3
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "oneTimeConfig"    # [Lmiui/android/animation/base/AnimConfig;

    .line 88
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/StateManager;->setStateFlags(Ljava/lang/Object;J)V

    .line 89
    invoke-virtual {p0, p1, p2}, Lmiui/android/animation/controller/FolmeState;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method

.method public varargs then([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 4
    .param p1, "propertyAndValues"    # [Ljava/lang/Object;

    .line 190
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    .line 191
    .local v0, "config":Lmiui/android/animation/base/AnimConfig;
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    .line 192
    .local v1, "animState":Lmiui/android/animation/controller/AnimState;
    const-wide/16 v2, 0x1

    iput-wide v2, v1, Lmiui/android/animation/controller/AnimState;->flags:J

    .line 193
    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lmiui/android/animation/controller/FolmeState;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    move-result-object v2

    return-object v2
.end method

.method public varargs to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
    .locals 4
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "oneTimeConfig"    # [Lmiui/android/animation/base/AnimConfig;

    .line 74
    instance-of v0, p1, Lmiui/android/animation/controller/AnimState;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->hasState(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 76
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    .line 78
    .local v0, "length":I
    array-length v1, p2

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Object;

    .line 79
    .local v1, "argArray":[Ljava/lang/Object;
    const/4 v2, 0x0

    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 80
    array-length v3, p2

    invoke-static {p2, v2, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 81
    invoke-virtual {p0, v1}, Lmiui/android/animation/controller/FolmeState;->to([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    move-result-object v2

    return-object v2

    .line 83
    .end local v0    # "length":I
    .end local v1    # "argArray":[Ljava/lang/Object;
    :cond_1
    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeState;->to([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0

    .line 75
    :cond_2
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lmiui/android/animation/controller/FolmeState;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method

.method public varargs to([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
    .locals 3
    .param p1, "propertyAndValues"    # [Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeState;->mStateMgr:Lmiui/android/animation/controller/StateManager;

    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lmiui/android/animation/controller/StateManager;->getToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lmiui/android/animation/base/AnimConfig;

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0, v1}, Lmiui/android/animation/controller/FolmeState;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method

.method public varargs to([Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
    .locals 1
    .param p1, "oneTimeConfig"    # [Lmiui/android/animation/base/AnimConfig;

    .line 69
    invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lmiui/android/animation/controller/FolmeState;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    move-result-object v0

    return-object v0
.end method
