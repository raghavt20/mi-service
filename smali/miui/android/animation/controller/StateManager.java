class miui.android.animation.controller.StateManager {
	 /* .source "StateManager.java" */
	 /* # static fields */
	 static final java.lang.String TAG_AUTO_SET_TO;
	 static final java.lang.String TAG_SET_TO;
	 static final java.lang.String TAG_TO;
	 /* # instance fields */
	 final miui.android.animation.controller.AnimState mAutoSetToState;
	 java.lang.Object mCurTag;
	 final miui.android.animation.controller.AnimState mSetToState;
	 miui.android.animation.controller.StateHelper mStateHelper;
	 final java.util.Map mStateMap;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/Object;", */
	 /* "Lmiui/android/animation/controller/AnimState;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
final miui.android.animation.controller.AnimState mToState;
/* # direct methods */
 miui.android.animation.controller.StateManager ( ) {
/* .locals 3 */
/* .line 15 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 21 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mStateMap = v0;
/* .line 24 */
/* new-instance v0, Lmiui/android/animation/controller/AnimState; */
final String v1 = "defaultTo"; // const-string v1, "defaultTo"
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v0, v1, v2}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V */
this.mToState = v0;
/* .line 25 */
/* new-instance v0, Lmiui/android/animation/controller/AnimState; */
final String v1 = "defaultSetTo"; // const-string v1, "defaultSetTo"
/* invoke-direct {v0, v1, v2}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V */
this.mSetToState = v0;
/* .line 26 */
/* new-instance v0, Lmiui/android/animation/controller/AnimState; */
final String v1 = "autoSetTo"; // const-string v1, "autoSetTo"
/* invoke-direct {v0, v1, v2}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V */
this.mAutoSetToState = v0;
/* .line 28 */
/* new-instance v0, Lmiui/android/animation/controller/StateHelper; */
/* invoke-direct {v0}, Lmiui/android/animation/controller/StateHelper;-><init>()V */
this.mStateHelper = v0;
return;
} // .end method
private miui.android.animation.controller.AnimState getState ( java.lang.Object p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "create" # Z */
/* .line 55 */
/* if-nez p1, :cond_0 */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
/* .line 59 */
} // :cond_0
/* instance-of v0, p1, Lmiui/android/animation/controller/AnimState; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 60 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/controller/AnimState; */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
/* .line 62 */
} // .end local v0 # "state":Lmiui/android/animation/controller/AnimState;
} // :cond_1
v0 = this.mStateMap;
/* check-cast v0, Lmiui/android/animation/controller/AnimState; */
/* .line 63 */
/* .restart local v0 # "state":Lmiui/android/animation/controller/AnimState; */
/* if-nez v0, :cond_2 */
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 64 */
/* new-instance v1, Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {v1, p1}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V */
/* move-object v0, v1 */
/* .line 65 */
(( miui.android.animation.controller.StateManager ) p0 ).addState ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/StateManager;->addState(Lmiui/android/animation/controller/AnimState;)V
/* .line 68 */
} // :cond_2
} // :goto_0
} // .end method
private miui.android.animation.controller.AnimState getStateByArgs ( java.lang.Object p0, java.lang.Object...p1 ) {
/* .locals 3 */
/* .param p1, "defaultTag" # Ljava/lang/Object; */
/* .param p2, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 215 */
int v0 = 0; // const/4 v0, 0x0
/* .line 216 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
/* array-length v1, p2 */
/* if-lez v1, :cond_0 */
/* .line 217 */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v2, p2, v1 */
/* invoke-direct {p0, v2, v1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState; */
/* .line 218 */
/* if-nez v0, :cond_0 */
/* .line 219 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/StateManager;->getStateByName([Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState; */
/* .line 222 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 223 */
(( miui.android.animation.controller.StateManager ) p0 ).getState ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 225 */
} // :cond_1
} // .end method
private miui.android.animation.controller.AnimState getStateByName ( java.lang.Object...p0 ) {
/* .locals 5 */
/* .param p1, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 229 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, p1, v0 */
/* .line 230 */
/* .local v0, "first":Ljava/lang/Object; */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* if-le v1, v3, :cond_0 */
/* aget-object v1, p1, v3 */
} // :cond_0
/* move-object v1, v2 */
/* .line 231 */
/* .local v1, "second":Ljava/lang/Object; */
} // :goto_0
/* instance-of v4, v0, Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* instance-of v4, v1, Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 232 */
/* invoke-direct {p0, v0, v3}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState; */
/* .line 234 */
} // :cond_1
} // .end method
private void setAnimState ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1, miui.android.animation.base.AnimConfigLink p2, java.lang.Object...p3 ) {
/* .locals 1 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "state" # Lmiui/android/animation/controller/AnimState; */
/* .param p3, "link" # Lmiui/android/animation/base/AnimConfigLink; */
/* .param p4, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 239 */
v0 = this.mStateHelper;
(( miui.android.animation.controller.StateHelper ) v0 ).parse ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateHelper;->parse(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V
/* .line 240 */
return;
} // .end method
/* # virtual methods */
public void add ( java.lang.String p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .line 156 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* float-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 157 */
return;
} // .end method
public void add ( java.lang.String p0, Float p1, Long p2 ) {
/* .locals 3 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .param p3, "flag" # J */
/* .line 164 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* .line 165 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
(( miui.android.animation.controller.AnimState ) v0 ).setConfigFlag ( p1, p3, p4 ); // invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 166 */
/* float-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 167 */
return;
} // .end method
public void add ( java.lang.String p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 160 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* int-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 161 */
return;
} // .end method
public void add ( java.lang.String p0, Integer p1, Long p2 ) {
/* .locals 3 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .param p3, "flag" # J */
/* .line 170 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* .line 171 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
(( miui.android.animation.controller.AnimState ) v0 ).setConfigFlag ( p1, p3, p4 ); // invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 172 */
/* int-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 173 */
return;
} // .end method
public void add ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .line 180 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* float-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 181 */
return;
} // .end method
public void add ( miui.android.animation.property.FloatProperty p0, Float p1, Long p2 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .param p3, "flag" # J */
/* .line 190 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* .line 191 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
(( miui.android.animation.controller.AnimState ) v0 ).setConfigFlag ( p1, p3, p4 ); // invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 192 */
/* float-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 193 */
return;
} // .end method
public void add ( miui.android.animation.property.FloatProperty p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .line 176 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* int-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 177 */
return;
} // .end method
public void add ( miui.android.animation.property.FloatProperty p0, Integer p1, Long p2 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .param p3, "flag" # J */
/* .line 184 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* .line 185 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
(( miui.android.animation.controller.AnimState ) v0 ).setConfigFlag ( p1, p3, p4 ); // invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V
/* .line 186 */
/* int-to-double v1, p2 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 187 */
return;
} // .end method
public void addInitProperty ( java.lang.String p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .line 152 */
/* const-wide/16 v0, 0x2 */
(( miui.android.animation.controller.StateManager ) p0 ).add ( p1, p2, v0, v1 ); // invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;FJ)V
/* .line 153 */
return;
} // .end method
public void addInitProperty ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 148 */
/* const-wide/16 v0, 0x2 */
(( miui.android.animation.controller.StateManager ) p0 ).add ( p1, p2, v0, v1 ); // invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;IJ)V
/* .line 149 */
return;
} // .end method
public void addInitProperty ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .line 144 */
/* const-wide/16 v0, 0x2 */
(( miui.android.animation.controller.StateManager ) p0 ).add ( p1, p2, v0, v1 ); // invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;FJ)V
/* .line 145 */
return;
} // .end method
public void addInitProperty ( miui.android.animation.property.FloatProperty p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .line 140 */
/* const-wide/16 v0, 0x2 */
(( miui.android.animation.controller.StateManager ) p0 ).add ( p1, p2, v0, v1 ); // invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;IJ)V
/* .line 141 */
return;
} // .end method
public void addListener ( miui.android.animation.listener.TransitionListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .line 91 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
/* filled-new-array {p1}, [Lmiui/android/animation/listener/TransitionListener; */
(( miui.android.animation.base.AnimConfig ) v0 ).addListeners ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
/* .line 92 */
return;
} // .end method
public void addState ( miui.android.animation.controller.AnimState p0 ) {
/* .locals 2 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .line 35 */
v0 = this.mStateMap;
(( miui.android.animation.controller.AnimState ) p1 ).getTag ( ); // invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;
/* .line 36 */
return;
} // .end method
public void addTempConfig ( miui.android.animation.controller.AnimState p0, miui.android.animation.base.AnimConfigLink p1 ) {
/* .locals 2 */
/* .param p1, "toState" # Lmiui/android/animation/controller/AnimState; */
/* .param p2, "configLink" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 203 */
v0 = this.mToState;
/* if-eq p1, v0, :cond_0 */
/* .line 204 */
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Z */
(( miui.android.animation.base.AnimConfigLink ) p2 ).add ( v0, v1 ); // invoke-virtual {p2, v0, v1}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V
/* .line 206 */
} // :cond_0
return;
} // .end method
public void clear ( ) {
/* .locals 1 */
/* .line 72 */
v0 = this.mStateMap;
/* .line 73 */
return;
} // .end method
public void clearTempState ( miui.android.animation.controller.AnimState p0 ) {
/* .locals 1 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .line 209 */
v0 = this.mToState;
/* if-eq p1, v0, :cond_0 */
v0 = this.mSetToState;
/* if-ne p1, v0, :cond_1 */
/* .line 210 */
} // :cond_0
(( miui.android.animation.controller.AnimState ) p1 ).clear ( ); // invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->clear()V
/* .line 212 */
} // :cond_1
return;
} // .end method
public miui.android.animation.controller.AnimState getCurrentState ( ) {
/* .locals 1 */
/* .line 196 */
v0 = this.mCurTag;
/* if-nez v0, :cond_0 */
/* .line 197 */
v0 = this.mToState;
this.mCurTag = v0;
/* .line 199 */
} // :cond_0
v0 = this.mCurTag;
(( miui.android.animation.controller.StateManager ) p0 ).getState ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState getSetToState ( miui.android.animation.IAnimTarget p0, miui.android.animation.base.AnimConfigLink p1, java.lang.Object...p2 ) {
/* .locals 1 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "link" # Lmiui/android/animation/base/AnimConfigLink; */
/* .param p3, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 43 */
v0 = this.mSetToState;
/* invoke-direct {p0, v0, p3}, Lmiui/android/animation/controller/StateManager;->getStateByArgs(Ljava/lang/Object;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState; */
/* .line 44 */
/* .local v0, "tempState":Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {p0, p1, v0, p2, p3}, Lmiui/android/animation/controller/StateManager;->setAnimState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V */
/* .line 45 */
} // .end method
public miui.android.animation.controller.AnimState getState ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 39 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState; */
} // .end method
public miui.android.animation.controller.AnimState getToState ( miui.android.animation.IAnimTarget p0, miui.android.animation.base.AnimConfigLink p1, java.lang.Object...p2 ) {
/* .locals 1 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "link" # Lmiui/android/animation/base/AnimConfigLink; */
/* .param p3, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 49 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
/* invoke-direct {p0, v0, p3}, Lmiui/android/animation/controller/StateManager;->getStateByArgs(Ljava/lang/Object;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState; */
/* .line 50 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {p0, p1, v0, p2, p3}, Lmiui/android/animation/controller/StateManager;->setAnimState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V */
/* .line 51 */
} // .end method
public Boolean hasState ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 31 */
v0 = v0 = this.mStateMap;
} // .end method
public void removeListener ( miui.android.animation.listener.TransitionListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .line 95 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
/* filled-new-array {p1}, [Lmiui/android/animation/listener/TransitionListener; */
(( miui.android.animation.base.AnimConfig ) v0 ).removeListeners ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->removeListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
/* .line 96 */
return;
} // .end method
public void setEase ( Integer p0, Float...p1 ) {
/* .locals 1 */
/* .param p1, "style" # I */
/* .param p2, "factors" # [F */
/* .line 110 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/base/AnimConfig;->setEase(I[F)Lmiui/android/animation/base/AnimConfig;
/* .line 111 */
return;
} // .end method
public void setEase ( miui.android.animation.property.FloatProperty p0, Integer p1, Float...p2 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "style" # I */
/* .param p3, "factors" # [F */
/* .line 114 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
/* int-to-long v1, p2 */
(( miui.android.animation.base.AnimConfig ) v0 ).setSpecial ( p1, v1, v2, p3 ); // invoke-virtual {v0, p1, v1, v2, p3}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;
/* .line 115 */
return;
} // .end method
public void setEase ( miui.android.animation.utils.EaseManager$EaseStyle p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 6 */
/* .param p1, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 99 */
(( miui.android.animation.controller.StateManager ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
/* .line 100 */
/* .local v0, "config":Lmiui/android/animation/base/AnimConfig; */
/* array-length v1, p2 */
/* if-nez v1, :cond_0 */
/* .line 101 */
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 103 */
} // :cond_0
/* array-length v1, p2 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* aget-object v4, p2, v3 */
/* .line 104 */
/* .local v4, "property":Lmiui/android/animation/property/FloatProperty; */
/* new-array v5, v2, [F */
(( miui.android.animation.base.AnimConfig ) v0 ).setSpecial ( v4, p1, v5 ); // invoke-virtual {v0, v4, p1, v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;[F)Lmiui/android/animation/base/AnimConfig;
/* .line 103 */
} // .end local v4 # "property":Lmiui/android/animation/property/FloatProperty;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 107 */
} // :cond_1
} // :goto_1
return;
} // .end method
public void setStateFlags ( java.lang.Object p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "flags" # J */
/* .line 118 */
(( miui.android.animation.controller.StateManager ) p0 ).getState ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 119 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
/* iput-wide p2, v0, Lmiui/android/animation/controller/AnimState;->flags:J */
/* .line 120 */
return;
} // .end method
public void setTransitionFlags ( java.lang.Object p0, Long p1, miui.android.animation.property.FloatProperty...p2 ) {
/* .locals 7 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "flags" # J */
/* .param p4, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 123 */
(( miui.android.animation.controller.StateManager ) p0 ).getState ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 124 */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
/* .line 125 */
/* .local v1, "config":Lmiui/android/animation/base/AnimConfig; */
/* array-length v2, p4 */
/* if-nez v2, :cond_0 */
/* .line 126 */
/* iput-wide p2, v1, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* .line 128 */
} // :cond_0
/* array-length v2, p4 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_2 */
/* aget-object v4, p4, v3 */
/* .line 129 */
/* .local v4, "property":Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.base.AnimConfig ) v1 ).getSpecialConfig ( v4 ); // invoke-virtual {v1, v4}, Lmiui/android/animation/base/AnimConfig;->getSpecialConfig(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/base/AnimSpecialConfig;
/* .line 130 */
/* .local v5, "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* if-nez v5, :cond_1 */
/* .line 131 */
/* new-instance v6, Lmiui/android/animation/base/AnimSpecialConfig; */
/* invoke-direct {v6}, Lmiui/android/animation/base/AnimSpecialConfig;-><init>()V */
/* move-object v5, v6 */
/* .line 132 */
(( miui.android.animation.base.AnimConfig ) v1 ).setSpecial ( v4, v5 ); // invoke-virtual {v1, v4, v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/base/AnimConfig;
/* .line 134 */
} // :cond_1
/* iput-wide p2, v5, Lmiui/android/animation/base/AnimSpecialConfig;->flags:J */
/* .line 128 */
} // .end local v4 # "property":Lmiui/android/animation/property/FloatProperty;
} // .end local v5 # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 137 */
} // :cond_2
} // :goto_1
return;
} // .end method
public miui.android.animation.controller.AnimState setup ( java.lang.Object p0 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 77 */
/* instance-of v0, p1, Lmiui/android/animation/controller/AnimState; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 78 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/controller/AnimState; */
/* .local v0, "state":Lmiui/android/animation/controller/AnimState; */
/* .line 80 */
} // .end local v0 # "state":Lmiui/android/animation/controller/AnimState;
} // :cond_0
v0 = this.mStateMap;
/* check-cast v0, Lmiui/android/animation/controller/AnimState; */
/* .line 81 */
/* .restart local v0 # "state":Lmiui/android/animation/controller/AnimState; */
/* if-nez v0, :cond_1 */
/* .line 82 */
/* new-instance v1, Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {v1, p1}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V */
/* move-object v0, v1 */
/* .line 83 */
(( miui.android.animation.controller.StateManager ) p0 ).addState ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/StateManager;->addState(Lmiui/android/animation/controller/AnimState;)V
/* .line 86 */
} // :cond_1
} // :goto_0
this.mCurTag = v0;
/* .line 87 */
} // .end method
