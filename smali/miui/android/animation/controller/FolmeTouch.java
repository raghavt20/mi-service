public class miui.android.animation.controller.FolmeTouch extends miui.android.animation.controller.FolmeBase implements miui.android.animation.ITouchStyle {
	 /* .source "FolmeTouch.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/controller/FolmeTouch$LongClickTask;, */
	 /* Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;, */
	 /* Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;, */
	 /* Lmiui/android/animation/controller/FolmeTouch$ListViewInfo; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Float DEFAULT_SCALE;
private static final Integer SCALE_DIS;
private static java.util.WeakHashMap sTouchRecord;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/WeakHashMap<", */
/* "Landroid/view/View;", */
/* "Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private Boolean mClearTint;
private Boolean mClickInvoked;
private android.view.View$OnClickListener mClickListener;
private miui.android.animation.listener.TransitionListener mDefListener;
private miui.android.animation.base.AnimConfig mDownConfig;
private Integer mDownWeight;
private Float mDownX;
private Float mDownY;
private miui.android.animation.controller.FolmeFont mFontStyle;
private Boolean mIsDown;
private java.lang.ref.WeakReference mListView;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private mLocation;
private Boolean mLongClickInvoked;
private android.view.View$OnLongClickListener mLongClickListener;
private miui.android.animation.controller.FolmeTouch$LongClickTask mLongClickTask;
private Float mScaleDist;
private java.util.Map mScaleSetMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/ITouchStyle$TouchType;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mSetTint;
private Integer mTouchIndex;
private java.lang.ref.WeakReference mTouchView;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private miui.android.animation.base.AnimConfig mUpConfig;
private Integer mUpWeight;
/* # direct methods */
static miui.android.animation.controller.FolmeTouch ( ) {
/* .locals 1 */
/* .line 50 */
/* new-instance v0, Ljava/util/WeakHashMap; */
/* invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V */
return;
} // .end method
public miui.android.animation.controller.FolmeTouch ( ) {
/* .locals 7 */
/* .param p1, "targets" # [Lmiui/android/animation/IAnimTarget; */
/* .line 87 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V */
/* .line 62 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v1, v0, [I */
this.mLocation = v1;
/* .line 64 */
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
this.mScaleSetMap = v1;
/* .line 71 */
/* new-instance v1, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.mDownConfig = v1;
/* .line 72 */
/* new-instance v1, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.mUpConfig = v1;
/* .line 75 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z */
/* .line 77 */
/* new-instance v2, Lmiui/android/animation/controller/FolmeTouch$1; */
/* invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeTouch$1;-><init>(Lmiui/android/animation/controller/FolmeTouch;)V */
this.mDefListener = v2;
/* .line 88 */
/* array-length v2, p1 */
/* if-lez v2, :cond_0 */
/* aget-object v1, p1, v1 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* invoke-direct {p0, v1}, Lmiui/android/animation/controller/FolmeTouch;->initScaleDist(Lmiui/android/animation/IAnimTarget;)V */
/* .line 90 */
v1 = miui.android.animation.property.ViewProperty.SCALE_X;
/* .line 91 */
/* .local v1, "propScaleX":Lmiui/android/animation/property/FloatProperty; */
v2 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* .line 93 */
/* .local v2, "propScaleY":Lmiui/android/animation/property/FloatProperty; */
v3 = this.mState;
v4 = miui.android.animation.ITouchStyle$TouchType.UP;
/* .line 94 */
/* const-wide/high16 v4, 0x3ff0000000000000L # 1.0 */
(( miui.android.animation.controller.AnimState ) v3 ).add ( v1, v4, v5 ); // invoke-virtual {v3, v1, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 95 */
(( miui.android.animation.controller.AnimState ) v3 ).add ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 98 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->setTintColor()V */
/* .line 100 */
v3 = this.mDownConfig;
/* new-array v4, v0, [F */
/* fill-array-data v4, :array_0 */
int v5 = -2; // const/4 v5, -0x2
miui.android.animation.utils.EaseManager .getStyle ( v5,v4 );
(( miui.android.animation.base.AnimConfig ) v3 ).setEase ( v4 ); // invoke-virtual {v3, v4}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 101 */
v3 = this.mDownConfig;
v4 = this.mDefListener;
/* filled-new-array {v4}, [Lmiui/android/animation/listener/TransitionListener; */
(( miui.android.animation.base.AnimConfig ) v3 ).addListeners ( v4 ); // invoke-virtual {v3, v4}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
/* .line 103 */
v3 = this.mUpConfig;
/* new-array v4, v0, [F */
/* fill-array-data v4, :array_1 */
(( miui.android.animation.base.AnimConfig ) v3 ).setEase ( v5, v4 ); // invoke-virtual {v3, v5, v4}, Lmiui/android/animation/base/AnimConfig;->setEase(I[F)Lmiui/android/animation/base/AnimConfig;
v4 = miui.android.animation.property.ViewProperty.ALPHA;
/* new-array v0, v0, [F */
/* fill-array-data v0, :array_2 */
/* .line 104 */
/* const-wide/16 v5, -0x2 */
(( miui.android.animation.base.AnimConfig ) v3 ).setSpecial ( v4, v5, v6, v0 ); // invoke-virtual {v3, v4, v5, v6, v0}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;
/* .line 106 */
return;
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x3f7d70a4 # 0.99f */
/* 0x3e19999a # 0.15f */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x3f7d70a4 # 0.99f */
/* 0x3e99999a # 0.3f */
} // .end array-data
/* :array_2 */
/* .array-data 4 */
/* 0x3f666666 # 0.9f */
/* 0x3e4ccccd # 0.2f */
} // .end array-data
} // .end method
static Boolean access$000 ( miui.android.animation.controller.FolmeTouch p0, android.view.View p1, Boolean p2, miui.android.animation.base.AnimConfig[] p3 ) { //synthethic
/* .locals 1 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p1, "x1" # Landroid/view/View; */
/* .param p2, "x2" # Z */
/* .param p3, "x3" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 45 */
v0 = /* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->bindListView(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)Z */
} // .end method
static void access$100 ( miui.android.animation.controller.FolmeTouch p0, android.view.View p1, Boolean p2 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p1, "x1" # Landroid/view/View; */
/* .param p2, "x2" # Z */
/* .line 45 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch;->resetViewTouch(Landroid/view/View;Z)V */
return;
} // .end method
static void access$200 ( miui.android.animation.controller.FolmeTouch p0, android.view.View p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p1, "x1" # Landroid/view/View; */
/* .line 45 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->invokeClick(Landroid/view/View;)V */
return;
} // .end method
static Boolean access$300 ( miui.android.animation.controller.FolmeTouch p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .line 45 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z */
} // .end method
static void access$400 ( miui.android.animation.controller.FolmeTouch p0, android.view.View p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p1, "x1" # Landroid/view/View; */
/* .line 45 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->invokeLongClick(Landroid/view/View;)V */
return;
} // .end method
static void access$600 ( miui.android.animation.controller.FolmeTouch p0, miui.android.animation.base.AnimConfig[] p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p1, "x1" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 45 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->onEventUp([Lmiui/android/animation/base/AnimConfig;)V */
return;
} // .end method
static void access$700 ( miui.android.animation.controller.FolmeTouch p0, android.view.View p1, android.view.MotionEvent p2, miui.android.animation.base.AnimConfig[] p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .param p1, "x1" # Landroid/view/View; */
/* .param p2, "x2" # Landroid/view/MotionEvent; */
/* .param p3, "x3" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 45 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
return;
} // .end method
static android.view.View$OnLongClickListener access$900 ( miui.android.animation.controller.FolmeTouch p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "x0" # Lmiui/android/animation/controller/FolmeTouch; */
/* .line 45 */
v0 = this.mLongClickListener;
} // .end method
private Boolean bindListView ( android.view.View p0, Boolean p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 4 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "setTouchListener" # Z */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 302 */
v0 = this.mState;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->getListViewInfo(Landroid/view/View;)Lmiui/android/animation/controller/FolmeTouch$ListViewInfo; */
/* move-object v2, v0 */
/* .local v2, "info":Lmiui/android/animation/controller/FolmeTouch$ListViewInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.listView;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 303 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 304 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "handleListViewTouch for "; // const-string v3, "handleListViewTouch for "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 306 */
} // :cond_0
v0 = this.listView;
/* invoke-direct {p0, v0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->handleListViewTouch(Landroid/widget/AbsListView;Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 307 */
int v0 = 1; // const/4 v0, 0x1
/* .line 309 */
} // .end local v2 # "info":Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;
} // :cond_1
} // .end method
private void doHandleTouchOf ( android.view.View p0, android.view.View$OnClickListener p1, android.view.View$OnLongClickListener p2, Boolean p3, miui.android.animation.base.AnimConfig...p4 ) {
/* .locals 8 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "clickListener" # Landroid/view/View$OnClickListener; */
/* .param p3, "longClick" # Landroid/view/View$OnLongClickListener; */
/* .param p4, "clickListenerSet" # Z */
/* .param p5, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 241 */
/* invoke-direct {p0, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->setClickAndLongClickListener(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V */
/* .line 242 */
/* invoke-direct {p0, p1, p5}, Lmiui/android/animation/controller/FolmeTouch;->handleViewTouch(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 244 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->setTouchView(Landroid/view/View;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 245 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 246 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "handleViewTouch for "; // const-string v1, "handleViewTouch for "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 248 */
} // :cond_0
v0 = (( android.view.View ) p1 ).isClickable ( ); // invoke-virtual {p1}, Landroid/view/View;->isClickable()Z
/* .line 249 */
/* .local v0, "isClickable":Z */
int v1 = 1; // const/4 v1, 0x1
(( android.view.View ) p1 ).setClickable ( v1 ); // invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V
/* .line 250 */
/* new-instance v1, Lmiui/android/animation/controller/FolmeTouch$3; */
/* move-object v2, v1 */
/* move-object v3, p0 */
/* move v4, p4 */
/* move-object v5, p1 */
/* move-object v6, p5 */
/* move v7, v0 */
/* invoke-direct/range {v2 ..v7}, Lmiui/android/animation/controller/FolmeTouch$3;-><init>(Lmiui/android/animation/controller/FolmeTouch;ZLandroid/view/View;[Lmiui/android/animation/base/AnimConfig;Z)V */
/* .line 258 */
/* .local v1, "task":Ljava/lang/Runnable; */
miui.android.animation.utils.CommonUtils .runOnPreDraw ( p1,v1 );
/* .line 260 */
} // .end local v0 # "isClickable":Z
} // .end local v1 # "task":Ljava/lang/Runnable;
} // :cond_1
return;
} // .end method
private miui.android.animation.base.AnimConfig getDownConfig ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 1 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 698 */
v0 = this.mDownConfig;
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.utils.CommonUtils .mergeArray ( p1,v0 );
/* check-cast v0, [Lmiui/android/animation/base/AnimConfig; */
} // .end method
private miui.android.animation.controller.FolmeTouch$ListViewInfo getListViewInfo ( android.view.View p0 ) {
/* .locals 6 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 318 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;-><init>(Lmiui/android/animation/controller/FolmeTouch$1;)V */
/* .line 319 */
/* .local v0, "info":Lmiui/android/animation/controller/FolmeTouch$ListViewInfo; */
int v1 = 0; // const/4 v1, 0x0
/* .line 320 */
/* .local v1, "listView":Landroid/widget/AbsListView; */
/* move-object v2, p1 */
/* .line 321 */
/* .local v2, "itemView":Landroid/view/View; */
(( android.view.View ) p1 ).getParent ( ); // invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
/* .line 322 */
/* .local v3, "parent":Landroid/view/ViewParent; */
} // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 323 */
/* instance-of v4, v3, Landroid/widget/AbsListView; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 324 */
/* move-object v1, v3 */
/* check-cast v1, Landroid/widget/AbsListView; */
/* .line 325 */
/* .line 327 */
} // :cond_0
/* instance-of v4, v3, Landroid/view/View; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 328 */
/* move-object v2, v3 */
/* check-cast v2, Landroid/view/View; */
/* .line 330 */
} // :cond_1
/* .line 332 */
} // :cond_2
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 333 */
/* new-instance v4, Ljava/lang/ref/WeakReference; */
v5 = this.listView;
/* invoke-direct {v4, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mListView = v4;
/* .line 334 */
this.listView = v1;
/* .line 335 */
this.itemView = v2;
/* .line 337 */
} // :cond_3
} // .end method
public static miui.android.animation.controller.ListViewTouchListener getListViewTouchListener ( android.widget.AbsListView p0 ) {
/* .locals 1 */
/* .param p0, "listView" # Landroid/widget/AbsListView; */
/* .line 341 */
/* const v0, 0x100b0002 */
(( android.widget.AbsListView ) p0 ).getTag ( v0 ); // invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->getTag(I)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/controller/ListViewTouchListener; */
} // .end method
private miui.android.animation.ITouchStyle$TouchType getType ( miui.android.animation.ITouchStyle$TouchType...p0 ) {
/* .locals 1 */
/* .param p1, "type" # [Lmiui/android/animation/ITouchStyle$TouchType; */
/* .line 608 */
/* array-length v0, p1 */
/* if-lez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, p1, v0 */
} // :cond_0
v0 = miui.android.animation.ITouchStyle$TouchType.DOWN;
} // :goto_0
} // .end method
private miui.android.animation.base.AnimConfig getUpConfig ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 1 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 702 */
v0 = this.mUpConfig;
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.utils.CommonUtils .mergeArray ( p1,v0 );
/* check-cast v0, [Lmiui/android/animation/base/AnimConfig; */
} // .end method
private void handleClick ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 537 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mClickListener;
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchIndex:I */
v1 = (( android.view.MotionEvent ) p2 ).getActionIndex ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I
/* if-ne v0, v1, :cond_0 */
/* .line 538 */
v0 = this.mState;
/* .line 539 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
/* instance-of v1, v0, Lmiui/android/animation/ViewTarget; */
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = /* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch;->isInTouchSlop(Landroid/view/View;Landroid/view/MotionEvent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 540 */
/* move-object v1, v0 */
/* check-cast v1, Lmiui/android/animation/ViewTarget; */
(( miui.android.animation.ViewTarget ) v1 ).getTargetObject ( ); // invoke-virtual {v1}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
/* .line 541 */
/* .local v1, "targetView":Landroid/view/View; */
(( android.view.View ) v1 ).performClick ( ); // invoke-virtual {v1}, Landroid/view/View;->performClick()Z
/* .line 542 */
/* invoke-direct {p0, v1}, Lmiui/android/animation/controller/FolmeTouch;->invokeClick(Landroid/view/View;)V */
/* .line 545 */
} // .end local v0 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v1 # "targetView":Landroid/view/View;
} // :cond_0
return;
} // .end method
private void handleListViewTouch ( android.widget.AbsListView p0, android.view.View p1, Boolean p2, miui.android.animation.base.AnimConfig...p3 ) {
/* .locals 2 */
/* .param p1, "listView" # Landroid/widget/AbsListView; */
/* .param p2, "touchView" # Landroid/view/View; */
/* .param p3, "setTouchListener" # Z */
/* .param p4, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 347 */
miui.android.animation.controller.FolmeTouch .getListViewTouchListener ( p1 );
/* .line 348 */
/* .local v0, "listener":Lmiui/android/animation/controller/ListViewTouchListener; */
/* if-nez v0, :cond_0 */
/* .line 349 */
/* new-instance v1, Lmiui/android/animation/controller/ListViewTouchListener; */
/* invoke-direct {v1, p1}, Lmiui/android/animation/controller/ListViewTouchListener;-><init>(Landroid/widget/AbsListView;)V */
/* move-object v0, v1 */
/* .line 350 */
/* const v1, 0x100b0002 */
(( android.widget.AbsListView ) p1 ).setTag ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/widget/AbsListView;->setTag(ILjava/lang/Object;)V
/* .line 352 */
} // :cond_0
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 353 */
(( android.widget.AbsListView ) p1 ).setOnTouchListener ( v0 ); // invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
/* .line 355 */
} // :cond_1
/* new-instance v1, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener; */
/* invoke-direct {v1, p0, p4}, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;-><init>(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V */
(( miui.android.animation.controller.ListViewTouchListener ) v0 ).putListener ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lmiui/android/animation/controller/ListViewTouchListener;->putListener(Landroid/view/View;Landroid/view/View$OnTouchListener;)V
/* .line 356 */
return;
} // .end method
private void handleMotionEvent ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 561 */
v0 = (( android.view.MotionEvent ) p2 ).getActionMasked ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 567 */
/* :pswitch_0 */
/* invoke-direct {p0, p2, p1, p3}, Lmiui/android/animation/controller/FolmeTouch;->onEventMove(Landroid/view/MotionEvent;Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 568 */
/* .line 570 */
/* :pswitch_1 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch;->handleClick(Landroid/view/View;Landroid/view/MotionEvent;)V */
/* .line 563 */
/* :pswitch_2 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeTouch;->recordDownEvent(Landroid/view/MotionEvent;)V */
/* .line 564 */
/* invoke-direct {p0, p3}, Lmiui/android/animation/controller/FolmeTouch;->onEventDown([Lmiui/android/animation/base/AnimConfig;)V */
/* .line 565 */
/* .line 572 */
} // :goto_0
/* invoke-direct {p0, p3}, Lmiui/android/animation/controller/FolmeTouch;->onEventUp([Lmiui/android/animation/base/AnimConfig;)V */
/* .line 575 */
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void handleViewTouch ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 3 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 383 */
v0 = miui.android.animation.controller.FolmeTouch.sTouchRecord;
(( java.util.WeakHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener; */
/* .line 384 */
/* .local v0, "touchListener":Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener; */
/* if-nez v0, :cond_0 */
/* .line 385 */
/* new-instance v1, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v2}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;-><init>(Lmiui/android/animation/controller/FolmeTouch$1;)V */
/* move-object v0, v1 */
/* .line 386 */
v1 = miui.android.animation.controller.FolmeTouch.sTouchRecord;
(( java.util.WeakHashMap ) v1 ).put ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 388 */
} // :cond_0
(( android.view.View ) p1 ).setOnTouchListener ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
/* .line 389 */
(( miui.android.animation.controller.FolmeTouch$InnerViewTouchListener ) v0 ).addTouch ( p0, p2 ); // invoke-virtual {v0, p0, p2}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->addTouch(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V
/* .line 390 */
return;
} // .end method
private void initScaleDist ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 129 */
/* instance-of v0, p1, Lmiui/android/animation/ViewTarget; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/ViewTarget; */
(( miui.android.animation.ViewTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 130 */
/* .local v0, "view":Landroid/view/View; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 131 */
/* nop */
/* .line 132 */
(( android.view.View ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getDisplayMetrics ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* .line 131 */
int v2 = 1; // const/4 v2, 0x1
/* const/high16 v3, 0x41200000 # 10.0f */
v1 = android.util.TypedValue .applyDimension ( v2,v3,v1 );
/* iput v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleDist:F */
/* .line 134 */
} // :cond_1
return;
} // .end method
private void invokeClick ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "targetView" # Landroid/view/View; */
/* .line 548 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickInvoked:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z */
/* if-nez v0, :cond_0 */
/* .line 549 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickInvoked:Z */
/* .line 550 */
v0 = this.mClickListener;
/* .line 552 */
} // :cond_0
return;
} // .end method
private void invokeLongClick ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 530 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z */
/* if-nez v0, :cond_0 */
/* .line 531 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z */
/* .line 532 */
v0 = this.mLongClickListener;
/* .line 534 */
} // :cond_0
return;
} // .end method
private Boolean isInTouchSlop ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 6 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 555 */
v0 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* .line 556 */
/* .local v0, "x":F */
v1 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* .line 557 */
/* .local v1, "y":F */
/* iget v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownX:F */
/* iget v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownY:F */
miui.android.animation.utils.CommonUtils .getDistance ( v2,v3,v0,v1 );
/* move-result-wide v2 */
v4 = miui.android.animation.utils.CommonUtils .getTouchSlop ( p1 );
/* float-to-double v4, v4 */
/* cmpg-double v2, v2, v4 */
/* if-gez v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
static Boolean isOnTouchView ( android.view.View p0, Integer[] p1, android.view.MotionEvent p2 ) {
/* .locals 6 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "location" # [I */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 177 */
int v0 = 1; // const/4 v0, 0x1
if ( p0 != null) { // if-eqz p0, :cond_1
/* .line 178 */
(( android.view.View ) p0 ).getLocationOnScreen ( p1 ); // invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V
/* .line 179 */
v1 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* float-to-int v1, v1 */
/* .line 180 */
/* .local v1, "x":I */
v2 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* float-to-int v2, v2 */
/* .line 181 */
/* .local v2, "y":I */
int v3 = 0; // const/4 v3, 0x0
/* aget v4, p1, v3 */
/* if-lt v1, v4, :cond_0 */
/* aget v4, p1, v3 */
v5 = (( android.view.View ) p0 ).getWidth ( ); // invoke-virtual {p0}, Landroid/view/View;->getWidth()I
/* add-int/2addr v4, v5 */
/* if-gt v1, v4, :cond_0 */
/* aget v4, p1, v0 */
/* if-lt v2, v4, :cond_0 */
/* aget v4, p1, v0 */
/* .line 182 */
v5 = (( android.view.View ) p0 ).getHeight ( ); // invoke-virtual {p0}, Landroid/view/View;->getHeight()I
/* add-int/2addr v4, v5 */
/* if-gt v2, v4, :cond_0 */
} // :cond_0
/* move v0, v3 */
/* .line 181 */
} // :goto_0
/* .line 184 */
} // .end local v1 # "x":I
} // .end local v2 # "y":I
} // :cond_1
} // .end method
private Boolean isScaleSet ( miui.android.animation.ITouchStyle$TouchType p0 ) {
/* .locals 2 */
/* .param p1, "type" # Lmiui/android/animation/ITouchStyle$TouchType; */
/* .line 604 */
v0 = java.lang.Boolean.TRUE;
v1 = this.mScaleSetMap;
v0 = (( java.lang.Boolean ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z
} // .end method
private void onEventDown ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 2 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 429 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 430 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/Object; */
final String v1 = "onEventDown, touchDown"; // const-string v1, "onEventDown, touchDown"
miui.android.animation.utils.LogUtils .debug ( v1,v0 );
/* .line 432 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z */
/* .line 433 */
(( miui.android.animation.controller.FolmeTouch ) p0 ).touchDown ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->touchDown([Lmiui/android/animation/base/AnimConfig;)V
/* .line 434 */
return;
} // .end method
private void onEventMove ( android.view.MotionEvent p0, android.view.View p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "view" # Landroid/view/View; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 437 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 438 */
v0 = this.mLocation;
v0 = miui.android.animation.controller.FolmeTouch .isOnTouchView ( p2,v0,p1 );
/* if-nez v0, :cond_0 */
/* .line 439 */
(( miui.android.animation.controller.FolmeTouch ) p0 ).touchUp ( p3 ); // invoke-virtual {p0, p3}, Lmiui/android/animation/controller/FolmeTouch;->touchUp([Lmiui/android/animation/base/AnimConfig;)V
/* .line 440 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->resetTouchStatus()V */
/* .line 441 */
} // :cond_0
v0 = this.mLongClickTask;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = /* invoke-direct {p0, p2, p1}, Lmiui/android/animation/controller/FolmeTouch;->isInTouchSlop(Landroid/view/View;Landroid/view/MotionEvent;)Z */
/* if-nez v0, :cond_1 */
/* .line 442 */
v0 = this.mLongClickTask;
(( miui.android.animation.controller.FolmeTouch$LongClickTask ) v0 ).stop ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->stop(Lmiui/android/animation/controller/FolmeTouch;)V
/* .line 445 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void onEventUp ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 2 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 448 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 449 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 450 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Ljava/lang/Object; */
final String v1 = "onEventUp, touchUp"; // const-string v1, "onEventUp, touchUp"
miui.android.animation.utils.LogUtils .debug ( v1,v0 );
/* .line 452 */
} // :cond_0
(( miui.android.animation.controller.FolmeTouch ) p0 ).touchUp ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->touchUp([Lmiui/android/animation/base/AnimConfig;)V
/* .line 453 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->resetTouchStatus()V */
/* .line 455 */
} // :cond_1
return;
} // .end method
private void recordDownEvent ( android.view.MotionEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 468 */
v0 = this.mClickListener;
/* if-nez v0, :cond_0 */
v0 = this.mLongClickListener;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 469 */
} // :cond_0
v0 = (( android.view.MotionEvent ) p1 ).getActionIndex ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I
/* iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchIndex:I */
/* .line 470 */
v0 = (( android.view.MotionEvent ) p1 ).getRawX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F
/* iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownX:F */
/* .line 471 */
v0 = (( android.view.MotionEvent ) p1 ).getRawY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F
/* iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownY:F */
/* .line 472 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickInvoked:Z */
/* .line 473 */
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z */
/* .line 474 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->startLongClickTask()V */
/* .line 476 */
} // :cond_1
return;
} // .end method
private void resetTouchStatus ( ) {
/* .locals 1 */
/* .line 458 */
v0 = this.mLongClickTask;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 459 */
(( miui.android.animation.controller.FolmeTouch$LongClickTask ) v0 ).stop ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->stop(Lmiui/android/animation/controller/FolmeTouch;)V
/* .line 461 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z */
/* .line 462 */
/* iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchIndex:I */
/* .line 463 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownX:F */
/* .line 464 */
/* iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownY:F */
/* .line 465 */
return;
} // .end method
private android.view.View resetView ( java.lang.ref.WeakReference p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;)", */
/* "Landroid/view/View;" */
/* } */
} // .end annotation
/* .line 158 */
/* .local p1, "viewHolder":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/view/View;>;" */
(( java.lang.ref.WeakReference ) p1 ).get ( ); // invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 159 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 160 */
int v1 = 0; // const/4 v1, 0x0
(( android.view.View ) v0 ).setOnTouchListener ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
/* .line 162 */
} // :cond_0
} // .end method
private void resetViewTouch ( android.view.View p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "isClickable" # Z */
/* .line 416 */
(( android.view.View ) p1 ).setClickable ( p2 ); // invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V
/* .line 417 */
int v0 = 0; // const/4 v0, 0x0
(( android.view.View ) p1 ).setOnTouchListener ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
/* .line 418 */
return;
} // .end method
private void setClickAndLongClickListener ( android.view.View$OnClickListener p0, android.view.View$OnLongClickListener p1 ) {
/* .locals 4 */
/* .param p1, "click" # Landroid/view/View$OnClickListener; */
/* .param p2, "longClick" # Landroid/view/View$OnLongClickListener; */
/* .line 264 */
int v0 = 0; // const/4 v0, 0x0
/* .line 265 */
/* .local v0, "targetView":Landroid/view/View; */
v1 = this.mState;
/* .line 266 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
/* instance-of v2, v1, Lmiui/android/animation/ViewTarget; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 267 */
/* move-object v2, v1 */
/* check-cast v2, Lmiui/android/animation/ViewTarget; */
(( miui.android.animation.ViewTarget ) v2 ).getTargetObject ( ); // invoke-virtual {v2}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
/* .line 269 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 270 */
return;
/* .line 272 */
} // :cond_1
v2 = this.mClickListener;
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
/* if-nez p1, :cond_2 */
/* .line 273 */
(( android.view.View ) v0 ).setOnClickListener ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
/* .line 274 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 275 */
/* new-instance v2, Lmiui/android/animation/controller/FolmeTouch$4; */
/* invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeTouch$4;-><init>(Lmiui/android/animation/controller/FolmeTouch;)V */
(( android.view.View ) v0 ).setOnClickListener ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
/* .line 282 */
} // :cond_3
} // :goto_0
this.mClickListener = p1;
/* .line 283 */
v2 = this.mLongClickListener;
if ( v2 != null) { // if-eqz v2, :cond_4
/* if-nez p2, :cond_4 */
/* .line 284 */
(( android.view.View ) v0 ).setOnLongClickListener ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
/* .line 285 */
} // :cond_4
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 286 */
/* new-instance v2, Lmiui/android/animation/controller/FolmeTouch$5; */
/* invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeTouch$5;-><init>(Lmiui/android/animation/controller/FolmeTouch;)V */
(( android.view.View ) v0 ).setOnLongClickListener ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
/* .line 297 */
} // :cond_5
} // :goto_1
this.mLongClickListener = p2;
/* .line 298 */
return;
} // .end method
private void setCorner ( Float p0 ) {
/* .locals 4 */
/* .param p1, "radius" # F */
/* .line 706 */
v0 = this.mState;
(( miui.android.animation.IAnimTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 707 */
/* .local v0, "target":Ljava/lang/Object; */
/* instance-of v1, v0, Landroid/view/View; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 708 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/View; */
/* const v2, 0x100b0008 */
java.lang.Float .valueOf ( p1 );
(( android.view.View ) v1 ).setTag ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 710 */
} // :cond_0
return;
} // .end method
private void setTintColor ( ) {
/* .locals 7 */
/* .line 109 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mSetTint:Z */
/* if-nez v0, :cond_3 */
/* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 112 */
} // :cond_0
/* const/16 v0, 0x14 */
int v1 = 0; // const/4 v1, 0x0
v0 = android.graphics.Color .argb ( v0,v1,v1,v1 );
/* .line 113 */
/* .local v0, "tintColor":I */
v1 = this.mState;
(( miui.android.animation.IAnimTarget ) v1 ).getTargetObject ( ); // invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 114 */
/* .local v1, "target":Ljava/lang/Object; */
/* instance-of v2, v1, Landroid/view/View; */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 115 */
/* move-object v2, v1 */
/* check-cast v2, Landroid/view/View; */
/* .line 116 */
/* .local v2, "view":Landroid/view/View; */
/* .line 117 */
/* .local v3, "colorRes":I */
(( android.view.View ) v2 ).getContext ( ); // invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;
/* const-string/jumbo v5, "uimode" */
(( android.content.Context ) v4 ).getSystemService ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v4, Landroid/app/UiModeManager; */
/* .line 118 */
/* .local v4, "mgr":Landroid/app/UiModeManager; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = (( android.app.UiModeManager ) v4 ).getNightMode ( ); // invoke-virtual {v4}, Landroid/app/UiModeManager;->getNightMode()I
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_1 */
/* .line 119 */
/* .line 121 */
} // :cond_1
(( android.view.View ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;
v0 = (( android.content.res.Resources ) v5 ).getColor ( v3 ); // invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I
/* .line 123 */
} // .end local v2 # "view":Landroid/view/View;
} // .end local v3 # "colorRes":I
} // .end local v4 # "mgr":Landroid/app/UiModeManager;
} // :cond_2
v2 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* .line 124 */
/* .local v2, "propFg":Lmiui/android/animation/property/FloatProperty; */
v3 = this.mState;
v4 = miui.android.animation.ITouchStyle$TouchType.DOWN;
/* int-to-double v4, v0 */
(( miui.android.animation.controller.AnimState ) v3 ).add ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 125 */
v3 = this.mState;
v4 = miui.android.animation.ITouchStyle$TouchType.UP;
/* const-wide/16 v4, 0x0 */
(( miui.android.animation.controller.AnimState ) v3 ).add ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 126 */
return;
/* .line 110 */
} // .end local v0 # "tintColor":I
} // .end local v1 # "target":Ljava/lang/Object;
} // .end local v2 # "propFg":Lmiui/android/animation/property/FloatProperty;
} // :cond_3
} // :goto_0
return;
} // .end method
private Boolean setTouchView ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 188 */
v0 = this.mTouchView;
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 189 */
/* .local v0, "touchView":Landroid/view/View; */
} // :goto_0
/* if-ne v0, p1, :cond_1 */
/* .line 190 */
int v1 = 0; // const/4 v1, 0x0
/* .line 192 */
} // :cond_1
/* new-instance v1, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mTouchView = v1;
/* .line 193 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void startLongClickTask ( ) {
/* .locals 2 */
/* .line 520 */
v0 = this.mLongClickListener;
/* if-nez v0, :cond_0 */
/* .line 521 */
return;
/* .line 523 */
} // :cond_0
v0 = this.mLongClickTask;
/* if-nez v0, :cond_1 */
/* .line 524 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeTouch$LongClickTask; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;-><init>(Lmiui/android/animation/controller/FolmeTouch$1;)V */
this.mLongClickTask = v0;
/* .line 526 */
} // :cond_1
v0 = this.mLongClickTask;
(( miui.android.animation.controller.FolmeTouch$LongClickTask ) v0 ).start ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->start(Lmiui/android/animation/controller/FolmeTouch;)V
/* .line 527 */
return;
} // .end method
/* # virtual methods */
public void bindViewOfListItem ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 198 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->setTouchView(Landroid/view/View;)Z */
/* if-nez v0, :cond_0 */
/* .line 199 */
return;
/* .line 201 */
} // :cond_0
/* new-instance v0, Lmiui/android/animation/controller/FolmeTouch$2; */
/* invoke-direct {v0, p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch$2;-><init>(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V */
miui.android.animation.utils.CommonUtils .runOnPreDraw ( p1,v0 );
/* .line 207 */
return;
} // .end method
public void cancel ( ) {
/* .locals 1 */
/* .line 680 */
/* invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->cancel()V */
/* .line 681 */
v0 = this.mFontStyle;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 682 */
(( miui.android.animation.controller.FolmeFont ) v0 ).cancel ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/FolmeFont;->cancel()V
/* .line 684 */
} // :cond_0
return;
} // .end method
public void clean ( ) {
/* .locals 3 */
/* .line 138 */
/* invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V */
/* .line 139 */
v0 = this.mFontStyle;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 140 */
(( miui.android.animation.controller.FolmeFont ) v0 ).clean ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/FolmeFont;->clean()V
/* .line 142 */
} // :cond_0
v0 = this.mScaleSetMap;
/* .line 143 */
v0 = this.mTouchView;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 144 */
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View; */
/* .line 145 */
this.mTouchView = v1;
/* .line 147 */
} // :cond_1
v0 = this.mListView;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 148 */
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View; */
/* .line 149 */
/* .local v0, "listView":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 150 */
/* const v2, 0x100b0002 */
(( android.view.View ) v0 ).setTag ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 152 */
} // :cond_2
this.mListView = v1;
/* .line 154 */
} // .end local v0 # "listView":Landroid/view/View;
} // :cond_3
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->resetTouchStatus()V */
/* .line 155 */
return;
} // .end method
public miui.android.animation.ITouchStyle clearTintColor ( ) {
/* .locals 3 */
/* .line 613 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z */
/* .line 614 */
v0 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* .line 615 */
/* .local v0, "propFg":Lmiui/android/animation/property/FloatProperty; */
v1 = this.mState;
v2 = miui.android.animation.ITouchStyle$TouchType.DOWN;
(( miui.android.animation.controller.AnimState ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 616 */
v1 = this.mState;
v2 = miui.android.animation.ITouchStyle$TouchType.UP;
(( miui.android.animation.controller.AnimState ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 617 */
} // .end method
public void handleTouchOf ( android.view.View p0, android.view.View$OnClickListener p1, android.view.View$OnLongClickListener p2, miui.android.animation.base.AnimConfig...p3 ) {
/* .locals 6 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "click" # Landroid/view/View$OnClickListener; */
/* .param p3, "longClick" # Landroid/view/View$OnLongClickListener; */
/* .param p4, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 230 */
int v4 = 0; // const/4 v4, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* move-object v5, p4 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/controller/FolmeTouch;->doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 231 */
return;
} // .end method
public void handleTouchOf ( android.view.View p0, android.view.View$OnClickListener p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 6 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "click" # Landroid/view/View$OnClickListener; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 224 */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v5, p3 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/controller/FolmeTouch;->doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 225 */
return;
} // .end method
public void handleTouchOf ( android.view.View p0, Boolean p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 6 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "clickListenerSet" # Z */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 235 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move v4, p2 */
/* move-object v5, p3 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/controller/FolmeTouch;->doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 236 */
return;
} // .end method
public void handleTouchOf ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 219 */
int v0 = 0; // const/4 v0, 0x0
(( miui.android.animation.controller.FolmeTouch ) p0 ).handleTouchOf ( p1, v0, p2 ); // invoke-virtual {p0, p1, v0, p2}, Lmiui/android/animation/controller/FolmeTouch;->handleTouchOf(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V
/* .line 220 */
return;
} // .end method
public void ignoreTouchOf ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 211 */
v0 = miui.android.animation.controller.FolmeTouch.sTouchRecord;
(( java.util.WeakHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener; */
/* .line 212 */
/* .local v0, "touchListener":Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( miui.android.animation.controller.FolmeTouch$InnerViewTouchListener ) v0 ).removeTouch ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->removeTouch(Lmiui/android/animation/controller/FolmeTouch;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 213 */
v1 = miui.android.animation.controller.FolmeTouch.sTouchRecord;
(( java.util.WeakHashMap ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 215 */
} // :cond_0
return;
} // .end method
public void onMotionEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 425 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Lmiui/android/animation/base/AnimConfig; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1, p1, v0}, Lmiui/android/animation/controller/FolmeTouch;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 426 */
return;
} // .end method
public void onMotionEventEx ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 0 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .param p3, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 421 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V */
/* .line 422 */
return;
} // .end method
public miui.android.animation.ITouchStyle setAlpha ( Float p0, miui.android.animation.ITouchStyle$TouchType...p1 ) {
/* .locals 4 */
/* .param p1, "alpha" # F */
/* .param p2, "type" # [Lmiui/android/animation/ITouchStyle$TouchType; */
/* .line 589 */
v0 = this.mState;
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeTouch;->getType([Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle$TouchType; */
v1 = miui.android.animation.property.ViewProperty.ALPHA;
/* float-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 590 */
} // .end method
public miui.android.animation.ITouchStyle setBackgroundColor ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 4 */
/* .param p1, "a" # F */
/* .param p2, "r" # F */
/* .param p3, "g" # F */
/* .param p4, "b" # F */
/* .line 645 */
/* const/high16 v0, 0x437f0000 # 255.0f */
/* mul-float v1, p1, v0 */
/* float-to-int v1, v1 */
/* mul-float v2, p2, v0 */
/* float-to-int v2, v2 */
/* mul-float v3, p3, v0 */
/* float-to-int v3, v3 */
/* mul-float/2addr v0, p4 */
/* float-to-int v0, v0 */
v0 = android.graphics.Color .argb ( v1,v2,v3,v0 );
(( miui.android.animation.controller.FolmeTouch ) p0 ).setBackgroundColor ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->setBackgroundColor(I)Lmiui/android/animation/ITouchStyle;
} // .end method
public miui.android.animation.ITouchStyle setBackgroundColor ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "color" # I */
/* .line 636 */
v0 = miui.android.animation.property.ViewPropertyExt.BACKGROUND;
/* .line 637 */
/* .local v0, "propBg":Lmiui/android/animation/property/FloatProperty; */
v1 = this.mState;
v2 = miui.android.animation.ITouchStyle$TouchType.DOWN;
/* int-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 638 */
v1 = this.mState;
v2 = miui.android.animation.ITouchStyle$TouchType.UP;
v2 = this.mState;
/* .line 639 */
/* const-wide/16 v3, 0x0 */
miui.android.animation.internal.AnimValueUtils .getValueOfTarget ( v2,v0,v3,v4 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* int-to-double v2, v2 */
/* .line 638 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 640 */
} // .end method
public void setFontStyle ( miui.android.animation.controller.FolmeFont p0 ) {
/* .locals 0 */
/* .param p1, "style" # Lmiui/android/animation/controller/FolmeFont; */
/* .line 166 */
this.mFontStyle = p1;
/* .line 167 */
return;
} // .end method
public miui.android.animation.ITouchStyle setScale ( Float p0, miui.android.animation.ITouchStyle$TouchType...p1 ) {
/* .locals 5 */
/* .param p1, "scale" # F */
/* .param p2, "type" # [Lmiui/android/animation/ITouchStyle$TouchType; */
/* .line 595 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeTouch;->getType([Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle$TouchType; */
/* .line 596 */
/* .local v0, "relType":Lmiui/android/animation/ITouchStyle$TouchType; */
v1 = this.mScaleSetMap;
int v2 = 1; // const/4 v2, 0x1
java.lang.Boolean .valueOf ( v2 );
/* .line 597 */
v1 = this.mState;
v2 = miui.android.animation.property.ViewProperty.SCALE_X;
/* float-to-double v3, p1 */
/* .line 598 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v2 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* float-to-double v3, p1 */
/* .line 599 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 600 */
} // .end method
public miui.android.animation.ITouchStyle setTint ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 4 */
/* .param p1, "a" # F */
/* .param p2, "r" # F */
/* .param p3, "g" # F */
/* .param p4, "b" # F */
/* .line 630 */
/* const/high16 v0, 0x437f0000 # 255.0f */
/* mul-float v1, p1, v0 */
/* float-to-int v1, v1 */
/* mul-float v2, p2, v0 */
/* float-to-int v2, v2 */
/* mul-float v3, p3, v0 */
/* float-to-int v3, v3 */
/* mul-float/2addr v0, p4 */
/* float-to-int v0, v0 */
v0 = android.graphics.Color .argb ( v1,v2,v3,v0 );
(( miui.android.animation.controller.FolmeTouch ) p0 ).setTint ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->setTint(I)Lmiui/android/animation/ITouchStyle;
} // .end method
public miui.android.animation.ITouchStyle setTint ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "color" # I */
/* .line 622 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mSetTint:Z */
/* .line 623 */
/* if-nez p1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z */
/* .line 624 */
v0 = this.mState;
v1 = miui.android.animation.ITouchStyle$TouchType.DOWN;
v1 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* int-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 625 */
} // .end method
public miui.android.animation.ITouchStyle setTintMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 171 */
v0 = this.mDownConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).setTintMode ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;
/* .line 172 */
v0 = this.mUpConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).setTintMode ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;
/* .line 173 */
} // .end method
public void setTouchDown ( ) {
/* .locals 2 */
/* .line 688 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->setTintColor()V */
/* .line 689 */
v0 = this.mState;
v1 = miui.android.animation.ITouchStyle$TouchType.DOWN;
/* .line 690 */
return;
} // .end method
public void setTouchUp ( ) {
/* .locals 2 */
/* .line 694 */
v0 = this.mState;
v1 = miui.android.animation.ITouchStyle$TouchType.UP;
/* .line 695 */
return;
} // .end method
public void touchDown ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 9 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 651 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->setCorner(F)V */
/* .line 652 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->setTintColor()V */
/* .line 653 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->getDownConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig; */
/* .line 654 */
/* .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig; */
v1 = this.mFontStyle;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 655 */
/* iget v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownWeight:I */
(( miui.android.animation.controller.FolmeFont ) v1 ).to ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lmiui/android/animation/controller/FolmeFont;->to(I[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IVarFontStyle;
/* .line 657 */
} // :cond_0
v1 = this.mState;
v2 = miui.android.animation.ITouchStyle$TouchType.DOWN;
/* .line 658 */
/* .local v1, "state":Lmiui/android/animation/controller/AnimState; */
v2 = miui.android.animation.ITouchStyle$TouchType.DOWN;
v2 = /* invoke-direct {p0, v2}, Lmiui/android/animation/controller/FolmeTouch;->isScaleSet(Lmiui/android/animation/ITouchStyle$TouchType;)Z */
/* if-nez v2, :cond_1 */
/* .line 659 */
v2 = this.mState;
/* .line 660 */
/* .local v2, "target":Lmiui/android/animation/IAnimTarget; */
v3 = miui.android.animation.property.ViewProperty.WIDTH;
v3 = (( miui.android.animation.IAnimTarget ) v2 ).getValue ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
v4 = miui.android.animation.property.ViewProperty.HEIGHT;
/* .line 661 */
v4 = (( miui.android.animation.IAnimTarget ) v2 ).getValue ( v4 ); // invoke-virtual {v2, v4}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* .line 660 */
v3 = java.lang.Math .max ( v3,v4 );
/* .line 662 */
/* .local v3, "maxSize":F */
/* iget v4, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleDist:F */
/* sub-float v4, v3, v4 */
/* div-float/2addr v4, v3 */
/* const v5, 0x3f666666 # 0.9f */
v4 = java.lang.Math .max ( v4,v5 );
/* .line 663 */
/* .local v4, "scaleValue":F */
v5 = miui.android.animation.property.ViewProperty.SCALE_X;
/* float-to-double v6, v4 */
(( miui.android.animation.controller.AnimState ) v1 ).add ( v5, v6, v7 ); // invoke-virtual {v1, v5, v6, v7}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v6 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* float-to-double v7, v4 */
/* .line 664 */
(( miui.android.animation.controller.AnimState ) v5 ).add ( v6, v7, v8 ); // invoke-virtual {v5, v6, v7, v8}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 666 */
} // .end local v2 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v3 # "maxSize":F
} // .end local v4 # "scaleValue":F
} // :cond_1
v2 = this.mState;
/* .line 667 */
return;
} // .end method
public void touchUp ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 4 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 671 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->getUpConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig; */
/* .line 672 */
/* .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig; */
v1 = this.mFontStyle;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 673 */
/* iget v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpWeight:I */
(( miui.android.animation.controller.FolmeFont ) v1 ).to ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lmiui/android/animation/controller/FolmeFont;->to(I[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IVarFontStyle;
/* .line 675 */
} // :cond_0
v1 = this.mState;
v2 = this.mState;
v3 = miui.android.animation.ITouchStyle$TouchType.UP;
/* .line 676 */
return;
} // .end method
public miui.android.animation.ITouchStyle useVarFont ( android.widget.TextView p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/widget/TextView; */
/* .param p2, "fontType" # I */
/* .param p3, "fromFontWeight" # I */
/* .param p4, "toFontWeight" # I */
/* .line 579 */
v0 = this.mFontStyle;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 580 */
/* iput p3, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpWeight:I */
/* .line 581 */
/* iput p4, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownWeight:I */
/* .line 582 */
(( miui.android.animation.controller.FolmeFont ) v0 ).useAt ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeFont;->useAt(Landroid/widget/TextView;II)Lmiui/android/animation/IVarFontStyle;
/* .line 584 */
} // :cond_0
} // .end method
