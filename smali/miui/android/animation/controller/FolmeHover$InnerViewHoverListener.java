class miui.android.animation.controller.FolmeHover$InnerViewHoverListener implements android.view.View$OnHoverListener {
	 /* .source "FolmeHover.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/controller/FolmeHover; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "InnerViewHoverListener" */
} // .end annotation
/* # instance fields */
private java.util.WeakHashMap mHoverMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/WeakHashMap<", */
/* "Lmiui/android/animation/controller/FolmeHover;", */
/* "[", */
/* "Lmiui/android/animation/base/AnimConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
private miui.android.animation.controller.FolmeHover$InnerViewHoverListener ( ) {
/* .locals 1 */
/* .line 352 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 353 */
/* new-instance v0, Ljava/util/WeakHashMap; */
/* invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V */
this.mHoverMap = v0;
return;
} // .end method
 miui.android.animation.controller.FolmeHover$InnerViewHoverListener ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lmiui/android/animation/controller/FolmeHover$1; */
/* .line 352 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeHover$InnerViewHoverListener;-><init>()V */
return;
} // .end method
/* # virtual methods */
void addHover ( miui.android.animation.controller.FolmeHover p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 1 */
/* .param p1, "folmeHover" # Lmiui/android/animation/controller/FolmeHover; */
/* .param p2, "configs" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 356 */
v0 = this.mHoverMap;
(( java.util.WeakHashMap ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 357 */
return;
} // .end method
public Boolean onHover ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 4 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 366 */
v0 = this.mHoverMap;
(( java.util.WeakHashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/WeakHashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 367 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lmiui/android/animation/controller/FolmeHover;[Lmiui/android/animation/base/AnimConfig;>;" */
/* check-cast v2, Lmiui/android/animation/controller/FolmeHover; */
/* .line 368 */
/* .local v2, "folmeHover":Lmiui/android/animation/controller/FolmeHover; */
/* check-cast v3, [Lmiui/android/animation/base/AnimConfig; */
/* .line 369 */
/* .local v3, "configs":[Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.controller.FolmeHover .access$100 ( v2,p1,p2,v3 );
/* .line 370 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lmiui/android/animation/controller/FolmeHover;[Lmiui/android/animation/base/AnimConfig;>;"
} // .end local v2 # "folmeHover":Lmiui/android/animation/controller/FolmeHover;
} // .end local v3 # "configs":[Lmiui/android/animation/base/AnimConfig;
/* .line 371 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean removeHover ( miui.android.animation.controller.FolmeHover p0 ) {
/* .locals 1 */
/* .param p1, "folmeHover" # Lmiui/android/animation/controller/FolmeHover; */
/* .line 360 */
v0 = this.mHoverMap;
(( java.util.WeakHashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 361 */
v0 = this.mHoverMap;
v0 = (( java.util.WeakHashMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/WeakHashMap;->isEmpty()Z
} // .end method
