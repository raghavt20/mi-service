public class miui.android.animation.controller.StateComposer {
	 /* .source "StateComposer.java" */
	 /* # static fields */
	 private static final java.lang.String METHOD_GET_STATE;
	 private static final miui.android.animation.utils.StyleComposer$IInterceptor sInterceptor;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Lmiui/android/animation/utils/StyleComposer$IInterceptor<", */
	 /* "Lmiui/android/animation/controller/IFolmeStateStyle;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.controller.StateComposer ( ) {
/* .locals 1 */
/* .line 15 */
/* new-instance v0, Lmiui/android/animation/controller/StateComposer$1; */
/* invoke-direct {v0}, Lmiui/android/animation/controller/StateComposer$1;-><init>()V */
return;
} // .end method
private miui.android.animation.controller.StateComposer ( ) {
/* .locals 0 */
/* .line 11 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static miui.android.animation.controller.IFolmeStateStyle composeStyle ( miui.android.animation.IAnimTarget...p0 ) {
/* .locals 4 */
/* .param p0, "targets" # [Lmiui/android/animation/IAnimTarget; */
/* .line 36 */
if ( p0 != null) { // if-eqz p0, :cond_3
	 /* array-length v0, p0 */
	 /* if-nez v0, :cond_0 */
	 /* .line 39 */
} // :cond_0
/* array-length v0, p0 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 40 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeState; */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, p0, v1 */
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeState;-><init>(Lmiui/android/animation/IAnimTarget;)V */
/* .line 42 */
} // :cond_1
/* array-length v0, p0 */
/* new-array v0, v0, [Lmiui/android/animation/controller/FolmeState; */
/* .line 43 */
/* .local v0, "array":[Lmiui/android/animation/controller/FolmeState; */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p0 */
/* if-ge v1, v2, :cond_2 */
/* .line 44 */
/* new-instance v2, Lmiui/android/animation/controller/FolmeState; */
/* aget-object v3, p0, v1 */
/* invoke-direct {v2, v3}, Lmiui/android/animation/controller/FolmeState;-><init>(Lmiui/android/animation/IAnimTarget;)V */
/* aput-object v2, v0, v1 */
/* .line 43 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 46 */
} // .end local v1 # "i":I
} // :cond_2
/* const-class v1, Lmiui/android/animation/controller/IFolmeStateStyle; */
v2 = miui.android.animation.controller.StateComposer.sInterceptor;
miui.android.animation.utils.StyleComposer .compose ( v1,v2,v0 );
/* check-cast v1, Lmiui/android/animation/controller/IFolmeStateStyle; */
/* .line 37 */
} // .end local v0 # "array":[Lmiui/android/animation/controller/FolmeState;
} // :cond_3
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
