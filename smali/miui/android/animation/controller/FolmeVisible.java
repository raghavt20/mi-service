public class miui.android.animation.controller.FolmeVisible extends miui.android.animation.controller.FolmeBase implements miui.android.animation.IVisibleStyle {
	 /* .source "FolmeVisible.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private final miui.android.animation.base.AnimConfig mDefConfig;
	 private Boolean mHasMove;
	 private Boolean mHasScale;
	 private Boolean mSetBound;
	 /* # direct methods */
	 public miui.android.animation.controller.FolmeVisible ( ) {
		 /* .locals 5 */
		 /* .param p1, "targets" # [Lmiui/android/animation/IAnimTarget; */
		 /* .line 40 */
		 /* invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V */
		 /* .line 30 */
		 /* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
		 /* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
		 int v1 = 1; // const/4 v1, 0x1
		 /* new-array v2, v1, [Lmiui/android/animation/listener/TransitionListener; */
		 /* new-instance v3, Lmiui/android/animation/controller/FolmeVisible$1; */
		 /* invoke-direct {v3, p0}, Lmiui/android/animation/controller/FolmeVisible$1;-><init>(Lmiui/android/animation/controller/FolmeVisible;)V */
		 int v4 = 0; // const/4 v4, 0x0
		 /* aput-object v3, v2, v4 */
		 (( miui.android.animation.base.AnimConfig ) v0 ).addListeners ( v2 ); // invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
		 this.mDefConfig = v0;
		 /* .line 41 */
		 (( miui.android.animation.controller.FolmeVisible ) p0 ).useAutoAlpha ( v1 ); // invoke-virtual {p0, v1}, Lmiui/android/animation/controller/FolmeVisible;->useAutoAlpha(Z)Lmiui/android/animation/IVisibleStyle;
		 /* .line 42 */
		 return;
	 } // .end method
	 static Boolean access$000 ( miui.android.animation.controller.FolmeVisible p0 ) { //synthethic
		 /* .locals 1 */
		 /* .param p0, "x0" # Lmiui/android/animation/controller/FolmeVisible; */
		 /* .line 24 */
		 /* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mSetBound:Z */
	 } // .end method
	 private miui.android.animation.base.AnimConfig getConfig ( miui.android.animation.IVisibleStyle$VisibleType p0, miui.android.animation.base.AnimConfig...p1 ) {
		 /* .locals 4 */
		 /* .param p1, "type" # Lmiui/android/animation/IVisibleStyle$VisibleType; */
		 /* .param p2, "config" # [Lmiui/android/animation/base/AnimConfig; */
		 /* .line 145 */
		 /* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasScale:Z */
		 int v1 = 2; // const/4 v1, 0x2
		 int v2 = -2; // const/4 v2, -0x2
		 /* if-nez v0, :cond_1 */
		 /* iget-boolean v3, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z */
		 /* if-nez v3, :cond_1 */
		 /* .line 146 */
		 v0 = this.mDefConfig;
		 v3 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
		 /* if-ne p1, v3, :cond_0 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* new-array v1, v1, [F */
		 int v2 = 0; // const/4 v2, 0x0
		 /* const/high16 v3, 0x43960000 # 300.0f */
		 /* aput v3, v1, v2 */
		 /* .line 147 */
		 /* const/16 v2, 0x10 */
		 miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
	 } // :cond_0
	 /* new-array v1, v1, [F */
	 /* fill-array-data v1, :array_0 */
	 /* .line 148 */
	 miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
	 /* .line 146 */
} // :goto_0
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 150 */
} // :cond_1
if ( v0 != null) { // if-eqz v0, :cond_3
/* iget-boolean v3, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z */
/* if-nez v3, :cond_3 */
/* .line 151 */
v0 = this.mDefConfig;
v3 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
/* if-ne p1, v3, :cond_2 */
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_1 */
/* .line 152 */
miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
} // :cond_2
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_2 */
/* .line 153 */
miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
/* .line 151 */
} // :goto_1
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 155 */
} // :cond_3
/* if-nez v0, :cond_5 */
/* .line 156 */
v0 = this.mDefConfig;
v3 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
/* if-ne p1, v3, :cond_4 */
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_3 */
/* .line 157 */
miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
} // :cond_4
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_4 */
/* .line 158 */
miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
/* .line 156 */
} // :goto_2
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 161 */
} // :cond_5
v0 = this.mDefConfig;
v3 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
/* if-ne p1, v3, :cond_6 */
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_5 */
/* .line 162 */
miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
} // :cond_6
/* new-array v1, v1, [F */
/* fill-array-data v1, :array_6 */
/* .line 163 */
miui.android.animation.utils.EaseManager .getStyle ( v2,v1 );
/* .line 161 */
} // :goto_3
(( miui.android.animation.base.AnimConfig ) v0 ).setEase ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 166 */
} // :goto_4
v0 = this.mDefConfig;
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
miui.android.animation.utils.CommonUtils .mergeArray ( p2,v0 );
/* check-cast v0, [Lmiui/android/animation/base/AnimConfig; */
/* nop */
/* :array_0 */
/* .array-data 4 */
/* 0x3f800000 # 1.0f */
/* 0x3e19999a # 0.15f */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0x3f19999a # 0.6f */
/* 0x3eb33333 # 0.35f */
} // .end array-data
/* :array_2 */
/* .array-data 4 */
/* 0x3f400000 # 0.75f */
/* 0x3e4ccccd # 0.2f */
} // .end array-data
/* :array_3 */
/* .array-data 4 */
/* 0x3f400000 # 0.75f */
/* 0x3eb33333 # 0.35f */
} // .end array-data
/* :array_4 */
/* .array-data 4 */
/* 0x3f400000 # 0.75f */
/* 0x3e800000 # 0.25f */
} // .end array-data
/* :array_5 */
/* .array-data 4 */
/* 0x3f266666 # 0.65f */
/* 0x3eb33333 # 0.35f */
} // .end array-data
/* :array_6 */
/* .array-data 4 */
/* 0x3f400000 # 0.75f */
/* 0x3e800000 # 0.25f */
} // .end array-data
} // .end method
private miui.android.animation.IVisibleStyle$VisibleType getType ( miui.android.animation.IVisibleStyle$VisibleType...p0 ) {
/* .locals 1 */
/* .param p1, "type" # [Lmiui/android/animation/IVisibleStyle$VisibleType; */
/* .line 97 */
/* array-length v0, p1 */
/* if-lez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, p1, v0 */
} // :cond_0
v0 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
} // :goto_0
} // .end method
/* # virtual methods */
public void clean ( ) {
/* .locals 1 */
/* .line 46 */
/* invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V */
/* .line 47 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasScale:Z */
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z */
/* .line 48 */
return;
} // .end method
public void hide ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 3 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 129 */
v0 = this.mState;
v1 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
v2 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
/* invoke-direct {p0, v2, p1}, Lmiui/android/animation/controller/FolmeVisible;->getConfig(Lmiui/android/animation/IVisibleStyle$VisibleType;[Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig; */
/* .line 130 */
return;
} // .end method
public miui.android.animation.IVisibleStyle setAlpha ( Float p0, miui.android.animation.IVisibleStyle$VisibleType...p1 ) {
/* .locals 4 */
/* .param p1, "alpha" # F */
/* .param p2, "type" # [Lmiui/android/animation/IVisibleStyle$VisibleType; */
/* .line 83 */
v0 = this.mState;
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeVisible;->getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType; */
v1 = miui.android.animation.property.ViewProperty.AUTO_ALPHA;
/* float-to-double v2, p1 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 84 */
} // .end method
public miui.android.animation.IVisibleStyle setBound ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "left" # I */
/* .param p2, "top" # I */
/* .param p3, "width" # I */
/* .param p4, "height" # I */
/* .line 52 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mSetBound:Z */
/* .line 53 */
v0 = this.mState;
v1 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
v1 = miui.android.animation.property.ViewProperty.X;
/* int-to-double v2, p1 */
/* .line 54 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.Y;
/* int-to-double v2, p2 */
/* .line 55 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.WIDTH;
/* int-to-double v2, p3 */
/* .line 56 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.HEIGHT;
/* int-to-double v2, p4 */
/* .line 57 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 58 */
} // .end method
public miui.android.animation.IVisibleStyle setFlags ( Long p0 ) {
/* .locals 1 */
/* .param p1, "flag" # J */
/* .line 77 */
v0 = this.mState;
/* .line 78 */
} // .end method
public miui.android.animation.IVisibleStyle setHide ( ) {
/* .locals 2 */
/* .line 140 */
v0 = this.mState;
v1 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
/* .line 141 */
} // .end method
public miui.android.animation.IVisibleStyle setMove ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "moveX" # I */
/* .param p2, "moveY" # I */
/* .line 102 */
v0 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
/* filled-new-array {v0}, [Lmiui/android/animation/IVisibleStyle$VisibleType; */
(( miui.android.animation.controller.FolmeVisible ) p0 ).setMove ( p1, p2, v0 ); // invoke-virtual {p0, p1, p2, v0}, Lmiui/android/animation/controller/FolmeVisible;->setMove(II[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
} // .end method
public miui.android.animation.IVisibleStyle setMove ( Integer p0, Integer p1, miui.android.animation.IVisibleStyle$VisibleType...p2 ) {
/* .locals 7 */
/* .param p1, "moveX" # I */
/* .param p2, "moveY" # I */
/* .param p3, "type" # [Lmiui/android/animation/IVisibleStyle$VisibleType; */
/* .line 107 */
v0 = java.lang.Math .abs ( p1 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-gtz v0, :cond_1 */
v0 = java.lang.Math .abs ( p2 );
/* if-lez v0, :cond_0 */
} // :cond_0
/* move v0, v1 */
} // :cond_1
} // :goto_0
/* move v0, v2 */
} // :goto_1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z */
/* .line 108 */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 109 */
v0 = this.mState;
/* invoke-direct {p0, p3}, Lmiui/android/animation/controller/FolmeVisible;->getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType; */
v3 = miui.android.animation.property.ViewProperty.X;
/* new-array v4, v2, [J */
/* const-wide/16 v5, 0x1 */
/* aput-wide v5, v4, v1 */
/* .line 110 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v3, p1, v4 ); // invoke-virtual {v0, v3, p1, v4}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/ViewProperty;I[J)Lmiui/android/animation/controller/AnimState;
v3 = miui.android.animation.property.ViewProperty.Y;
/* new-array v2, v2, [J */
/* aput-wide v5, v2, v1 */
/* .line 111 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v3, p2, v2 ); // invoke-virtual {v0, v3, p2, v2}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/ViewProperty;I[J)Lmiui/android/animation/controller/AnimState;
/* .line 113 */
} // :cond_2
} // .end method
public miui.android.animation.IVisibleStyle setScale ( Float p0, miui.android.animation.IVisibleStyle$VisibleType...p1 ) {
/* .locals 4 */
/* .param p1, "scale" # F */
/* .param p2, "type" # [Lmiui/android/animation/IVisibleStyle$VisibleType; */
/* .line 89 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasScale:Z */
/* .line 90 */
v0 = this.mState;
/* invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeVisible;->getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType; */
v1 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* float-to-double v2, p1 */
/* .line 91 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
v1 = miui.android.animation.property.ViewProperty.SCALE_X;
/* float-to-double v2, p1 */
/* .line 92 */
(( miui.android.animation.controller.AnimState ) v0 ).add ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 93 */
} // .end method
public miui.android.animation.IVisibleStyle setShow ( ) {
/* .locals 2 */
/* .line 134 */
v0 = this.mState;
v1 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
/* .line 135 */
} // .end method
public miui.android.animation.IVisibleStyle setShowDelay ( Long p0 ) {
/* .locals 2 */
/* .param p1, "delay" # J */
/* .line 118 */
v0 = this.mState;
v1 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
(( miui.android.animation.controller.AnimState ) v0 ).getConfig ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
/* iput-wide p1, v0, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* .line 119 */
} // .end method
public void show ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 3 */
/* .param p1, "config" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 124 */
v0 = this.mState;
v1 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
v2 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
/* invoke-direct {p0, v2, p1}, Lmiui/android/animation/controller/FolmeVisible;->getConfig(Lmiui/android/animation/IVisibleStyle$VisibleType;[Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig; */
/* .line 125 */
return;
} // .end method
public miui.android.animation.IVisibleStyle useAutoAlpha ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "useAutoAlpha" # Z */
/* .line 63 */
v0 = miui.android.animation.property.ViewProperty.AUTO_ALPHA;
/* .line 64 */
/* .local v0, "autoAlpha":Lmiui/android/animation/property/FloatProperty; */
v1 = miui.android.animation.property.ViewProperty.ALPHA;
/* .line 65 */
/* .local v1, "alpha":Lmiui/android/animation/property/FloatProperty; */
/* const-wide/16 v2, 0x0 */
/* const-wide/high16 v4, 0x3ff0000000000000L # 1.0 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 66 */
v6 = this.mState;
v7 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
(( miui.android.animation.controller.AnimState ) v6 ).remove ( v1 ); // invoke-virtual {v6, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v6 ).add ( v0, v4, v5 ); // invoke-virtual {v6, v0, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 67 */
v4 = this.mState;
v5 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
(( miui.android.animation.controller.AnimState ) v4 ).remove ( v1 ); // invoke-virtual {v4, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v4 ).add ( v0, v2, v3 ); // invoke-virtual {v4, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 69 */
} // :cond_0
v6 = this.mState;
v7 = miui.android.animation.IVisibleStyle$VisibleType.SHOW;
(( miui.android.animation.controller.AnimState ) v6 ).remove ( v0 ); // invoke-virtual {v6, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v6 ).add ( v1, v4, v5 ); // invoke-virtual {v6, v1, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 70 */
v4 = this.mState;
v5 = miui.android.animation.IVisibleStyle$VisibleType.HIDE;
(( miui.android.animation.controller.AnimState ) v4 ).remove ( v0 ); // invoke-virtual {v4, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.AnimState ) v4 ).add ( v1, v2, v3 ); // invoke-virtual {v4, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;
/* .line 72 */
} // :goto_0
} // .end method
