public class miui.android.animation.controller.FolmeState implements miui.android.animation.controller.IFolmeStateStyle {
	 /* .source "FolmeState.java" */
	 /* # interfaces */
	 /* # instance fields */
	 miui.android.animation.base.AnimConfigLink mConfigLink;
	 private Boolean mEnableAnim;
	 miui.android.animation.controller.StateManager mStateMgr;
	 miui.android.animation.IAnimTarget mTarget;
	 /* # direct methods */
	 miui.android.animation.controller.FolmeState ( ) {
		 /* .locals 1 */
		 /* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .line 27 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 21 */
		 /* new-instance v0, Lmiui/android/animation/controller/StateManager; */
		 /* invoke-direct {v0}, Lmiui/android/animation/controller/StateManager;-><init>()V */
		 this.mStateMgr = v0;
		 /* .line 23 */
		 /* new-instance v0, Lmiui/android/animation/base/AnimConfigLink; */
		 /* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfigLink;-><init>()V */
		 this.mConfigLink = v0;
		 /* .line 25 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeState;->mEnableAnim:Z */
		 /* .line 28 */
		 this.mTarget = p1;
		 /* .line 29 */
		 return;
	 } // .end method
	 private miui.android.animation.IStateStyle fromTo ( java.lang.Object p0, java.lang.Object p1, miui.android.animation.base.AnimConfigLink p2 ) {
		 /* .locals 5 */
		 /* .param p1, "fromTag" # Ljava/lang/Object; */
		 /* .param p2, "toTag" # Ljava/lang/Object; */
		 /* .param p3, "oneTimeConfig" # Lmiui/android/animation/base/AnimConfigLink; */
		 /* .line 102 */
		 /* iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeState;->mEnableAnim:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 103 */
			 v0 = this.mStateMgr;
			 (( miui.android.animation.controller.StateManager ) v0 ).setup ( p2 ); // invoke-virtual {v0, p2}, Lmiui/android/animation/controller/StateManager;->setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
			 /* .line 104 */
			 if ( p1 != null) { // if-eqz p1, :cond_0
				 /* .line 105 */
				 (( miui.android.animation.controller.FolmeState ) p0 ).setTo ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
				 /* .line 107 */
			 } // :cond_0
			 (( miui.android.animation.controller.FolmeState ) p0 ).getState ( p2 ); // invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
			 /* .line 108 */
			 /* .local v0, "toState":Lmiui/android/animation/controller/AnimState; */
			 v1 = this.mStateMgr;
			 (( miui.android.animation.controller.StateManager ) v1 ).addTempConfig ( v0, p3 ); // invoke-virtual {v1, v0, p3}, Lmiui/android/animation/controller/StateManager;->addTempConfig(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
			 /* .line 109 */
			 miui.android.animation.internal.AnimRunner .getInst ( );
			 v2 = this.mTarget;
			 (( miui.android.animation.controller.FolmeState ) p0 ).getState ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
			 (( miui.android.animation.controller.FolmeState ) p0 ).getState ( p2 ); // invoke-virtual {p0, p2}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
			 (( miui.android.animation.internal.AnimRunner ) v1 ).run ( v2, v3, v4, p3 ); // invoke-virtual {v1, v2, v3, v4, p3}, Lmiui/android/animation/internal/AnimRunner;->run(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
			 /* .line 110 */
			 v1 = this.mStateMgr;
			 (( miui.android.animation.controller.StateManager ) v1 ).clearTempState ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/controller/StateManager;->clearTempState(Lmiui/android/animation/controller/AnimState;)V
			 /* .line 111 */
			 (( miui.android.animation.base.AnimConfigLink ) p3 ).clear ( ); // invoke-virtual {p3}, Lmiui/android/animation/base/AnimConfigLink;->clear()V
			 /* .line 113 */
		 } // .end local v0 # "toState":Lmiui/android/animation/controller/AnimState;
	 } // :cond_1
} // .end method
private miui.android.animation.base.AnimConfigLink getConfigLink ( ) {
	 /* .locals 1 */
	 /* .line 185 */
	 v0 = this.mConfigLink;
} // .end method
private miui.android.animation.IStateStyle setTo ( java.lang.Object p0, miui.android.animation.base.AnimConfigLink p1 ) {
	 /* .locals 2 */
	 /* .param p1, "tag" # Ljava/lang/Object; */
	 /* .param p2, "oneTimeConfig" # Lmiui/android/animation/base/AnimConfigLink; */
	 /* .line 46 */
	 v0 = this.mTarget;
	 /* if-nez v0, :cond_0 */
	 /* .line 47 */
	 /* .line 49 */
} // :cond_0
/* instance-of v1, p1, Ljava/lang/Integer; */
/* if-nez v1, :cond_2 */
/* instance-of v1, p1, Ljava/lang/Float; */
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 52 */
} // :cond_1
/* new-instance v1, Lmiui/android/animation/controller/FolmeState$1; */
/* invoke-direct {v1, p0, p1, p2}, Lmiui/android/animation/controller/FolmeState$1;-><init>(Lmiui/android/animation/controller/FolmeState;Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)V */
(( miui.android.animation.IAnimTarget ) v0 ).executeOnInitialized ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->executeOnInitialized(Ljava/lang/Runnable;)V
/* .line 64 */
/* .line 50 */
} // :cond_2
} // :goto_0
/* filled-new-array {p1, p2}, [Ljava/lang/Object; */
(( miui.android.animation.controller.FolmeState ) p0 ).setTo ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeState;->setTo([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
} // .end method
/* # virtual methods */
public miui.android.animation.IStateStyle add ( java.lang.String p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .line 305 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;F)V
/* .line 306 */
} // .end method
public miui.android.animation.IStateStyle add ( java.lang.String p0, Float p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .param p3, "flag" # J */
/* .line 311 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;FJ)V
/* .line 312 */
} // .end method
public miui.android.animation.IStateStyle add ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 299 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;I)V
/* .line 300 */
} // .end method
public miui.android.animation.IStateStyle add ( java.lang.String p0, Integer p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .param p3, "flag" # J */
/* .line 275 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;IJ)V
/* .line 276 */
} // .end method
public miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .line 287 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 288 */
} // .end method
public miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Float p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .param p3, "flag" # J */
/* .line 293 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;FJ)V
/* .line 294 */
} // .end method
public miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .line 281 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;I)V
/* .line 282 */
} // .end method
public miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Integer p1, Long p2 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .param p3, "flag" # J */
/* .line 269 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).add ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;IJ)V
/* .line 270 */
} // .end method
public void addConfig ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 0 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "configs" # [Lmiui/android/animation/base/AnimConfig; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 353 */
return;
} // .end method
public miui.android.animation.IStateStyle addInitProperty ( java.lang.String p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .line 263 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).addInitProperty ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Ljava/lang/String;F)V
/* .line 264 */
} // .end method
public miui.android.animation.IStateStyle addInitProperty ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 257 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).addInitProperty ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Ljava/lang/String;I)V
/* .line 258 */
} // .end method
public miui.android.animation.IStateStyle addInitProperty ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .line 251 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).addInitProperty ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 252 */
} // .end method
public miui.android.animation.IStateStyle addInitProperty ( miui.android.animation.property.FloatProperty p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # I */
/* .line 245 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).addInitProperty ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->addInitProperty(Lmiui/android/animation/property/FloatProperty;I)V
/* .line 246 */
} // .end method
public miui.android.animation.IStateStyle addListener ( miui.android.animation.listener.TransitionListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .line 233 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).addListener ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->addListener(Lmiui/android/animation/listener/TransitionListener;)V
/* .line 234 */
} // .end method
public void addState ( miui.android.animation.controller.AnimState p0 ) {
/* .locals 1 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .line 216 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).addState ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->addState(Lmiui/android/animation/controller/AnimState;)V
/* .line 217 */
return;
} // .end method
public miui.android.animation.IStateStyle autoSetTo ( java.lang.Object...p0 ) {
/* .locals 0 */
/* .param p1, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 199 */
} // .end method
public void cancel ( ) {
/* .locals 4 */
/* .line 128 */
miui.android.animation.internal.AnimRunner .getInst ( );
v1 = this.mTarget;
int v2 = 0; // const/4 v2, 0x0
/* move-object v3, v2 */
/* check-cast v3, [Lmiui/android/animation/property/FloatProperty; */
(( miui.android.animation.internal.AnimRunner ) v0 ).cancel ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/internal/AnimRunner;->cancel(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V
/* .line 129 */
return;
} // .end method
public void cancel ( java.lang.String...p0 ) {
/* .locals 3 */
/* .param p1, "propertyNames" # [Ljava/lang/String; */
/* .line 138 */
(( miui.android.animation.controller.FolmeState ) p0 ).getTarget ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;
/* .line 139 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
/* array-length v1, p1 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* instance-of v1, v0, Lmiui/android/animation/ValueTarget; */
/* if-nez v1, :cond_0 */
/* .line 142 */
} // :cond_0
miui.android.animation.internal.AnimRunner .getInst ( );
v2 = this.mTarget;
(( miui.android.animation.internal.AnimRunner ) v1 ).cancel ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Lmiui/android/animation/internal/AnimRunner;->cancel(Lmiui/android/animation/IAnimTarget;[Ljava/lang/String;)V
/* .line 143 */
return;
/* .line 140 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void cancel ( miui.android.animation.property.FloatProperty...p0 ) {
/* .locals 2 */
/* .param p1, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 133 */
miui.android.animation.internal.AnimRunner .getInst ( );
v1 = this.mTarget;
(( miui.android.animation.internal.AnimRunner ) v0 ).cancel ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lmiui/android/animation/internal/AnimRunner;->cancel(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V
/* .line 134 */
return;
} // .end method
public void clean ( ) {
/* .locals 0 */
/* .line 123 */
(( miui.android.animation.controller.FolmeState ) p0 ).cancel ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->cancel()V
/* .line 124 */
return;
} // .end method
public void enableDefaultAnim ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 118 */
/* iput-boolean p1, p0, Lmiui/android/animation/controller/FolmeState;->mEnableAnim:Z */
/* .line 119 */
return;
} // .end method
public void end ( java.lang.Object...p0 ) {
/* .locals 3 */
/* .param p1, "propertyList" # [Ljava/lang/Object; */
/* .line 147 */
/* array-length v0, p1 */
/* if-lez v0, :cond_1 */
/* .line 148 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v1, p1, v0 */
/* instance-of v1, v1, Lmiui/android/animation/property/FloatProperty; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 149 */
/* array-length v1, p1 */
/* new-array v1, v1, [Lmiui/android/animation/property/FloatProperty; */
/* .line 150 */
/* .local v1, "propList":[Lmiui/android/animation/property/FloatProperty; */
/* array-length v2, p1 */
java.lang.System .arraycopy ( p1,v0,v1,v0,v2 );
/* .line 151 */
miui.android.animation.internal.AnimRunner .getInst ( );
v2 = this.mTarget;
(( miui.android.animation.internal.AnimRunner ) v0 ).end ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lmiui/android/animation/internal/AnimRunner;->end(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V
/* .line 152 */
} // .end local v1 # "propList":[Lmiui/android/animation/property/FloatProperty;
/* .line 153 */
} // :cond_0
/* array-length v1, p1 */
/* new-array v1, v1, [Ljava/lang/String; */
/* .line 154 */
/* .local v1, "propList":[Ljava/lang/String; */
/* array-length v2, p1 */
java.lang.System .arraycopy ( p1,v0,v1,v0,v2 );
/* .line 155 */
miui.android.animation.internal.AnimRunner .getInst ( );
v2 = this.mTarget;
(( miui.android.animation.internal.AnimRunner ) v0 ).end ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lmiui/android/animation/internal/AnimRunner;->end(Lmiui/android/animation/IAnimTarget;[Ljava/lang/String;)V
/* .line 158 */
} // .end local v1 # "propList":[Ljava/lang/String;
} // :cond_1
} // :goto_0
return;
} // .end method
public miui.android.animation.IStateStyle fromTo ( java.lang.Object p0, java.lang.Object p1, miui.android.animation.base.AnimConfig...p2 ) {
/* .locals 6 */
/* .param p1, "fromTag" # Ljava/lang/Object; */
/* .param p2, "toTag" # Ljava/lang/Object; */
/* .param p3, "oneTimeConfig" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 94 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink; */
/* .line 95 */
/* .local v0, "configLink":Lmiui/android/animation/base/AnimConfigLink; */
/* array-length v1, p3 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_0 */
/* aget-object v4, p3, v3 */
/* .line 96 */
/* .local v4, "config":Lmiui/android/animation/base/AnimConfig; */
/* new-array v5, v2, [Z */
(( miui.android.animation.base.AnimConfigLink ) v0 ).add ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V
/* .line 95 */
} // .end local v4 # "config":Lmiui/android/animation/base/AnimConfig;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 98 */
} // :cond_0
/* invoke-direct {p0, p1, p2, v0}, Lmiui/android/animation/controller/FolmeState;->fromTo(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle; */
} // .end method
public miui.android.animation.controller.AnimState getCurrentState ( ) {
/* .locals 1 */
/* .line 341 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.controller.AnimState getState ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 211 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).getState ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
} // .end method
public miui.android.animation.IAnimTarget getTarget ( ) {
/* .locals 1 */
/* .line 32 */
v0 = this.mTarget;
} // .end method
public Long predictDuration ( java.lang.Object...p0 ) {
/* .locals 6 */
/* .param p1, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 175 */
(( miui.android.animation.controller.FolmeState ) p0 ).getTarget ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;
/* .line 176 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink; */
/* .line 177 */
/* .local v1, "configLink":Lmiui/android/animation/base/AnimConfigLink; */
v2 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v2 ).getToState ( v0, v1, p1 ); // invoke-virtual {v2, v0, v1, p1}, Lmiui/android/animation/controller/StateManager;->getToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 178 */
/* .local v2, "state":Lmiui/android/animation/controller/AnimState; */
int v3 = 0; // const/4 v3, 0x0
miui.android.animation.internal.PredictTask .predictDuration ( v0,v3,v2,v1 );
/* move-result-wide v3 */
/* .line 179 */
/* .local v3, "duration":J */
v5 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v5 ).clearTempState ( v2 ); // invoke-virtual {v5, v2}, Lmiui/android/animation/controller/StateManager;->clearTempState(Lmiui/android/animation/controller/AnimState;)V
/* .line 180 */
(( miui.android.animation.base.AnimConfigLink ) v1 ).clear ( ); // invoke-virtual {v1}, Lmiui/android/animation/base/AnimConfigLink;->clear()V
/* .line 181 */
/* return-wide v3 */
} // .end method
public miui.android.animation.IStateStyle removeListener ( miui.android.animation.listener.TransitionListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .line 239 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).removeListener ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->removeListener(Lmiui/android/animation/listener/TransitionListener;)V
/* .line 240 */
} // .end method
public miui.android.animation.IStateStyle set ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 227 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).setup ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 228 */
} // .end method
public miui.android.animation.IStateStyle setConfig ( miui.android.animation.base.AnimConfig p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 0 */
/* .param p1, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 318 */
} // .end method
public miui.android.animation.IStateStyle setEase ( Integer p0, Float...p1 ) {
/* .locals 1 */
/* .param p1, "style" # I */
/* .param p2, "factors" # [F */
/* .line 329 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).setEase ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->setEase(I[F)V
/* .line 330 */
} // .end method
public miui.android.animation.IStateStyle setEase ( miui.android.animation.property.FloatProperty p0, Integer p1, Float...p2 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "style" # I */
/* .param p3, "factors" # [F */
/* .line 335 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).setEase ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lmiui/android/animation/controller/StateManager;->setEase(Lmiui/android/animation/property/FloatProperty;I[F)V
/* .line 336 */
} // .end method
public miui.android.animation.IStateStyle setEase ( miui.android.animation.utils.EaseManager$EaseStyle p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 1 */
/* .param p1, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 323 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).setEase ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/controller/StateManager;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;[Lmiui/android/animation/property/FloatProperty;)V
/* .line 324 */
} // .end method
public miui.android.animation.IStateStyle setFlags ( Long p0 ) {
/* .locals 1 */
/* .param p1, "flag" # J */
/* .line 204 */
(( miui.android.animation.controller.FolmeState ) p0 ).getTarget ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;
/* .line 205 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
(( miui.android.animation.IAnimTarget ) v0 ).setFlags ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/IAnimTarget;->setFlags(J)V
/* .line 206 */
} // .end method
public miui.android.animation.IStateStyle setTo ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 37 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Lmiui/android/animation/base/AnimConfig; */
(( miui.android.animation.controller.FolmeState ) p0 ).setTo ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
} // .end method
public miui.android.animation.IStateStyle setTo ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "oneTimeConfig" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 42 */
miui.android.animation.base.AnimConfigLink .linkConfig ( p2 );
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle; */
} // .end method
public miui.android.animation.IStateStyle setTo ( java.lang.Object...p0 ) {
/* .locals 3 */
/* .param p1, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 162 */
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink; */
/* .line 163 */
/* .local v0, "configLink":Lmiui/android/animation/base/AnimConfigLink; */
v1 = this.mStateMgr;
(( miui.android.animation.controller.FolmeState ) p0 ).getTarget ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;
(( miui.android.animation.controller.StateManager ) v1 ).getSetToState ( v2, v0, p1 ); // invoke-virtual {v1, v2, v0, p1}, Lmiui/android/animation/controller/StateManager;->getSetToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 164 */
/* .local v1, "state":Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {p0, v1, v0}, Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle; */
/* .line 165 */
} // .end method
public miui.android.animation.IStateStyle setTransitionFlags ( Long p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 2 */
/* .param p1, "flags" # J */
/* .param p3, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 346 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).getCurrentState ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.StateManager ) v0 ).setTransitionFlags ( v1, p1, p2, p3 ); // invoke-virtual {v0, v1, p1, p2, p3}, Lmiui/android/animation/controller/StateManager;->setTransitionFlags(Ljava/lang/Object;J[Lmiui/android/animation/property/FloatProperty;)V
/* .line 347 */
} // .end method
public miui.android.animation.IStateStyle setup ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .line 221 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v0 ).setup ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 222 */
} // .end method
public miui.android.animation.IStateStyle then ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 3 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "oneTimeConfig" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 88 */
v0 = this.mStateMgr;
/* const-wide/16 v1, 0x1 */
(( miui.android.animation.controller.StateManager ) v0 ).setStateFlags ( p1, v1, v2 ); // invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/StateManager;->setStateFlags(Ljava/lang/Object;J)V
/* .line 89 */
(( miui.android.animation.controller.FolmeState ) p0 ).to ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/controller/FolmeState;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
} // .end method
public miui.android.animation.IStateStyle then ( java.lang.Object...p0 ) {
/* .locals 4 */
/* .param p1, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 190 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
/* .line 191 */
/* .local v0, "config":Lmiui/android/animation/base/AnimConfig; */
(( miui.android.animation.controller.FolmeState ) p0 ).getState ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 192 */
/* .local v1, "animState":Lmiui/android/animation/controller/AnimState; */
/* const-wide/16 v2, 0x1 */
/* iput-wide v2, v1, Lmiui/android/animation/controller/AnimState;->flags:J */
/* .line 193 */
/* filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig; */
(( miui.android.animation.controller.FolmeState ) p0 ).to ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lmiui/android/animation/controller/FolmeState;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
} // .end method
public miui.android.animation.IStateStyle to ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
/* .locals 4 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "oneTimeConfig" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 74 */
/* instance-of v0, p1, Lmiui/android/animation/controller/AnimState; */
/* if-nez v0, :cond_2 */
v0 = this.mStateMgr;
v0 = (( miui.android.animation.controller.StateManager ) v0 ).hasState ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/StateManager;->hasState(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 76 */
} // :cond_0
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
v0 = (( java.lang.Class ) v0 ).isArray ( ); // invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 77 */
v0 = java.lang.reflect.Array .getLength ( p1 );
/* .line 78 */
/* .local v0, "length":I */
/* array-length v1, p2 */
/* add-int/2addr v1, v0 */
/* new-array v1, v1, [Ljava/lang/Object; */
/* .line 79 */
/* .local v1, "argArray":[Ljava/lang/Object; */
int v2 = 0; // const/4 v2, 0x0
java.lang.System .arraycopy ( p1,v2,v1,v2,v0 );
/* .line 80 */
/* array-length v3, p2 */
java.lang.System .arraycopy ( p2,v2,v1,v0,v3 );
/* .line 81 */
(( miui.android.animation.controller.FolmeState ) p0 ).to ( v1 ); // invoke-virtual {p0, v1}, Lmiui/android/animation/controller/FolmeState;->to([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
/* .line 83 */
} // .end local v0 # "length":I
} // .end local v1 # "argArray":[Ljava/lang/Object;
} // :cond_1
/* filled-new-array {p1, p2}, [Ljava/lang/Object; */
(( miui.android.animation.controller.FolmeState ) p0 ).to ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeState;->to([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
/* .line 75 */
} // :cond_2
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
(( miui.android.animation.controller.FolmeState ) p0 ).getState ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.FolmeState ) p0 ).fromTo ( v0, v1, p2 ); // invoke-virtual {p0, v0, v1, p2}, Lmiui/android/animation/controller/FolmeState;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
} // .end method
public miui.android.animation.IStateStyle to ( java.lang.Object...p0 ) {
/* .locals 3 */
/* .param p1, "propertyAndValues" # [Ljava/lang/Object; */
/* .line 170 */
v0 = this.mStateMgr;
(( miui.android.animation.controller.FolmeState ) p0 ).getTarget ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;
/* invoke-direct {p0}, Lmiui/android/animation/controller/FolmeState;->getConfigLink()Lmiui/android/animation/base/AnimConfigLink; */
(( miui.android.animation.controller.StateManager ) v0 ).getToState ( v1, v2, p1 ); // invoke-virtual {v0, v1, v2, p1}, Lmiui/android/animation/controller/StateManager;->getToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Lmiui/android/animation/base/AnimConfig; */
int v2 = 0; // const/4 v2, 0x0
(( miui.android.animation.controller.FolmeState ) p0 ).fromTo ( v2, v0, v1 ); // invoke-virtual {p0, v2, v0, v1}, Lmiui/android/animation/controller/FolmeState;->fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
} // .end method
public miui.android.animation.IStateStyle to ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 1 */
/* .param p1, "oneTimeConfig" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 69 */
(( miui.android.animation.controller.FolmeState ) p0 ).getCurrentState ( ); // invoke-virtual {p0}, Lmiui/android/animation/controller/FolmeState;->getCurrentState()Lmiui/android/animation/controller/AnimState;
(( miui.android.animation.controller.FolmeState ) p0 ).to ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lmiui/android/animation/controller/FolmeState;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
} // .end method
