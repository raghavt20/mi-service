class miui.android.animation.controller.FolmeState$1 implements java.lang.Runnable {
	 /* .source "FolmeState.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lmiui/android/animation/controller/FolmeState;->setTo(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfigLink;)Lmiui/android/animation/IStateStyle; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final miui.android.animation.controller.FolmeState this$0; //synthetic
final miui.android.animation.base.AnimConfigLink val$oneTimeConfig; //synthetic
final java.lang.Object val$tag; //synthetic
/* # direct methods */
 miui.android.animation.controller.FolmeState$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lmiui/android/animation/controller/FolmeState; */
/* .line 52 */
this.this$0 = p1;
this.val$tag = p2;
this.val$oneTimeConfig = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 55 */
v0 = this.this$0;
v1 = this.val$tag;
(( miui.android.animation.controller.FolmeState ) v0 ).getState ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/FolmeState;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
/* .line 56 */
/* .local v0, "toState":Lmiui/android/animation/controller/AnimState; */
v1 = this.this$0;
(( miui.android.animation.controller.FolmeState ) v1 ).getTarget ( ); // invoke-virtual {v1}, Lmiui/android/animation/controller/FolmeState;->getTarget()Lmiui/android/animation/IAnimTarget;
/* .line 57 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
v2 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 58 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "FolmeState.setTo, state = "; // const-string v3, "FolmeState.setTo, state = "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 int v3 = 0; // const/4 v3, 0x0
	 /* new-array v3, v3, [Ljava/lang/Object; */
	 miui.android.animation.utils.LogUtils .debug ( v2,v3 );
	 /* .line 60 */
} // :cond_0
v2 = this.animManager;
v3 = this.val$oneTimeConfig;
(( miui.android.animation.internal.AnimManager ) v2 ).setTo ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Lmiui/android/animation/internal/AnimManager;->setTo(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
/* .line 61 */
v2 = this.this$0;
v2 = this.mStateMgr;
(( miui.android.animation.controller.StateManager ) v2 ).clearTempState ( v0 ); // invoke-virtual {v2, v0}, Lmiui/android/animation/controller/StateManager;->clearTempState(Lmiui/android/animation/controller/AnimState;)V
/* .line 62 */
return;
} // .end method
