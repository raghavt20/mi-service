public abstract class miui.android.animation.controller.FolmeBase implements miui.android.animation.IStateContainer {
	 /* .source "FolmeBase.java" */
	 /* # interfaces */
	 /* # instance fields */
	 miui.android.animation.controller.IFolmeStateStyle mState;
	 /* # direct methods */
	 miui.android.animation.controller.FolmeBase ( ) {
		 /* .locals 1 */
		 /* .param p1, "targets" # [Lmiui/android/animation/IAnimTarget; */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 17 */
		 miui.android.animation.controller.StateComposer .composeStyle ( p1 );
		 this.mState = v0;
		 /* .line 18 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void addConfig ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
		 /* .locals 1 */
		 /* .param p1, "tag" # Ljava/lang/Object; */
		 /* .param p2, "configs" # [Lmiui/android/animation/base/AnimConfig; */
		 /* .line 63 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 64 */
			 /* .line 66 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void cancel ( ) {
		 /* .locals 1 */
		 /* .line 35 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 36 */
			 /* .line 38 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void cancel ( java.lang.String...p0 ) {
		 /* .locals 1 */
		 /* .param p1, "propertyNames" # [Ljava/lang/String; */
		 /* .line 49 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 50 */
			 /* .line 52 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void cancel ( miui.android.animation.property.FloatProperty...p0 ) {
		 /* .locals 1 */
		 /* .param p1, "properties" # [Lmiui/android/animation/property/FloatProperty; */
		 /* .line 42 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 43 */
			 /* .line 45 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void clean ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 22 */
			 /* .line 24 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void enableDefaultAnim ( Boolean p0 ) {
		 /* .locals 1 */
		 /* .param p1, "enable" # Z */
		 /* .line 56 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 57 */
			 /* .line 59 */
		 } // :cond_0
		 return;
	 } // .end method
	 public void end ( java.lang.Object...p0 ) {
		 /* .locals 1 */
		 /* .param p1, "properties" # [Ljava/lang/Object; */
		 /* .line 28 */
		 v0 = this.mState;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 29 */
			 /* .line 31 */
		 } // :cond_0
		 return;
	 } // .end method
