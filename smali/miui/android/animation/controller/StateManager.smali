.class Lmiui/android/animation/controller/StateManager;
.super Ljava/lang/Object;
.source "StateManager.java"


# static fields
.field static final TAG_AUTO_SET_TO:Ljava/lang/String; = "autoSetTo"

.field static final TAG_SET_TO:Ljava/lang/String; = "defaultSetTo"

.field static final TAG_TO:Ljava/lang/String; = "defaultTo"


# instance fields
.field final mAutoSetToState:Lmiui/android/animation/controller/AnimState;

.field mCurTag:Ljava/lang/Object;

.field final mSetToState:Lmiui/android/animation/controller/AnimState;

.field mStateHelper:Lmiui/android/animation/controller/StateHelper;

.field final mStateMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/controller/AnimState;",
            ">;"
        }
    .end annotation
.end field

.field final mToState:Lmiui/android/animation/controller/AnimState;


# direct methods
.method constructor <init>()V
    .locals 3

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateMap:Ljava/util/Map;

    .line 24
    new-instance v0, Lmiui/android/animation/controller/AnimState;

    const-string v1, "defaultTo"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mToState:Lmiui/android/animation/controller/AnimState;

    .line 25
    new-instance v0, Lmiui/android/animation/controller/AnimState;

    const-string v1, "defaultSetTo"

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mSetToState:Lmiui/android/animation/controller/AnimState;

    .line 26
    new-instance v0, Lmiui/android/animation/controller/AnimState;

    const-string v1, "autoSetTo"

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mAutoSetToState:Lmiui/android/animation/controller/AnimState;

    .line 28
    new-instance v0, Lmiui/android/animation/controller/StateHelper;

    invoke-direct {v0}, Lmiui/android/animation/controller/StateHelper;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateHelper:Lmiui/android/animation/controller/StateHelper;

    return-void
.end method

.method private getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState;
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "create"    # Z

    .line 55
    if-nez p1, :cond_0

    .line 56
    const/4 v0, 0x0

    return-object v0

    .line 59
    :cond_0
    instance-of v0, p1, Lmiui/android/animation/controller/AnimState;

    if-eqz v0, :cond_1

    .line 60
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/controller/AnimState;

    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    goto :goto_0

    .line 62
    .end local v0    # "state":Lmiui/android/animation/controller/AnimState;
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/AnimState;

    .line 63
    .restart local v0    # "state":Lmiui/android/animation/controller/AnimState;
    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 64
    new-instance v1, Lmiui/android/animation/controller/AnimState;

    invoke-direct {v1, p1}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    move-object v0, v1

    .line 65
    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/StateManager;->addState(Lmiui/android/animation/controller/AnimState;)V

    .line 68
    :cond_2
    :goto_0
    return-object v0
.end method

.method private varargs getStateByArgs(Ljava/lang/Object;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 3
    .param p1, "defaultTag"    # Ljava/lang/Object;
    .param p2, "propertyAndValues"    # [Ljava/lang/Object;

    .line 215
    const/4 v0, 0x0

    .line 216
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    array-length v1, p2

    if-lez v1, :cond_0

    .line 217
    const/4 v1, 0x0

    aget-object v2, p2, v1

    invoke-direct {p0, v2, v1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 218
    if-nez v0, :cond_0

    .line 219
    invoke-direct {p0, p2}, Lmiui/android/animation/controller/StateManager;->getStateByName([Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 222
    :cond_0
    if-nez v0, :cond_1

    .line 223
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 225
    :cond_1
    return-object v0
.end method

.method private varargs getStateByName([Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 5
    .param p1, "propertyAndValues"    # [Ljava/lang/Object;

    .line 229
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 230
    .local v0, "first":Ljava/lang/Object;
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    aget-object v1, p1, v3

    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 231
    .local v1, "second":Ljava/lang/Object;
    :goto_0
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_1

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 232
    invoke-direct {p0, v0, v3}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState;

    move-result-object v2

    return-object v2

    .line 234
    :cond_1
    return-object v2
.end method

.method private varargs setAnimState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p3, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p4, "propertyAndValues"    # [Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateHelper:Lmiui/android/animation/controller/StateHelper;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmiui/android/animation/controller/StateHelper;->parse(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V

    .line 240
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;F)V
    .locals 3
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F

    .line 156
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 157
    return-void
.end method

.method public add(Ljava/lang/String;FJ)V
    .locals 3
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F
    .param p3, "flag"    # J

    .line 164
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 165
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 166
    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 167
    return-void
.end method

.method public add(Ljava/lang/String;I)V
    .locals 3
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 160
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 161
    return-void
.end method

.method public add(Ljava/lang/String;IJ)V
    .locals 3
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I
    .param p3, "flag"    # J

    .line 170
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 171
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 172
    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 173
    return-void
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;F)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F

    .line 180
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 181
    return-void
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;FJ)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F
    .param p3, "flag"    # J

    .line 190
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 191
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 192
    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 193
    return-void
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;I)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I

    .line 176
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 177
    return-void
.end method

.method public add(Lmiui/android/animation/property/FloatProperty;IJ)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I
    .param p3, "flag"    # J

    .line 184
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 185
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    invoke-virtual {v0, p1, p3, p4}, Lmiui/android/animation/controller/AnimState;->setConfigFlag(Ljava/lang/Object;J)V

    .line 186
    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 187
    return-void
.end method

.method public addInitProperty(Ljava/lang/String;F)V
    .locals 2
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F

    .line 152
    const-wide/16 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;FJ)V

    .line 153
    return-void
.end method

.method public addInitProperty(Ljava/lang/String;I)V
    .locals 2
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 148
    const-wide/16 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Ljava/lang/String;IJ)V

    .line 149
    return-void
.end method

.method public addInitProperty(Lmiui/android/animation/property/FloatProperty;F)V
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F

    .line 144
    const-wide/16 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;FJ)V

    .line 145
    return-void
.end method

.method public addInitProperty(Lmiui/android/animation/property/FloatProperty;I)V
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # I

    .line 140
    const-wide/16 v0, 0x2

    invoke-virtual {p0, p1, p2, v0, v1}, Lmiui/android/animation/controller/StateManager;->add(Lmiui/android/animation/property/FloatProperty;IJ)V

    .line 141
    return-void
.end method

.method public addListener(Lmiui/android/animation/listener/TransitionListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/android/animation/listener/TransitionListener;

    .line 91
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    filled-new-array {p1}, [Lmiui/android/animation/listener/TransitionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;

    .line 92
    return-void
.end method

.method public addState(Lmiui/android/animation/controller/AnimState;)V
    .locals 2
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;

    .line 35
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateMap:Ljava/util/Map;

    invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public addTempConfig(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 2
    .param p1, "toState"    # Lmiui/android/animation/controller/AnimState;
    .param p2, "configLink"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 203
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mToState:Lmiui/android/animation/controller/AnimState;

    if-eq p1, v0, :cond_0

    .line 204
    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Z

    invoke-virtual {p2, v0, v1}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V

    .line 206
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .line 72
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 73
    return-void
.end method

.method public clearTempState(Lmiui/android/animation/controller/AnimState;)V
    .locals 1
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;

    .line 209
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mToState:Lmiui/android/animation/controller/AnimState;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mSetToState:Lmiui/android/animation/controller/AnimState;

    if-ne p1, v0, :cond_1

    .line 210
    :cond_0
    invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->clear()V

    .line 212
    :cond_1
    return-void
.end method

.method public getCurrentState()Lmiui/android/animation/controller/AnimState;
    .locals 1

    .line 196
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mCurTag:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 197
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mToState:Lmiui/android/animation/controller/AnimState;

    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mCurTag:Ljava/lang/Object;

    .line 199
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mCurTag:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs getSetToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p3, "propertyAndValues"    # [Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mSetToState:Lmiui/android/animation/controller/AnimState;

    invoke-direct {p0, v0, p3}, Lmiui/android/animation/controller/StateManager;->getStateByArgs(Ljava/lang/Object;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 44
    .local v0, "tempState":Lmiui/android/animation/controller/AnimState;
    invoke-direct {p0, p1, v0, p2, p3}, Lmiui/android/animation/controller/StateManager;->setAnimState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V

    .line 45
    return-object v0
.end method

.method public getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .line 39
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;Z)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    return-object v0
.end method

.method public varargs getToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "link"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p3, "propertyAndValues"    # [Ljava/lang/Object;

    .line 49
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lmiui/android/animation/controller/StateManager;->getStateByArgs(Ljava/lang/Object;[Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 50
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    invoke-direct {p0, p1, v0, p2, p3}, Lmiui/android/animation/controller/StateManager;->setAnimState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;[Ljava/lang/Object;)V

    .line 51
    return-object v0
.end method

.method public hasState(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeListener(Lmiui/android/animation/listener/TransitionListener;)V
    .locals 2
    .param p1, "listener"    # Lmiui/android/animation/listener/TransitionListener;

    .line 95
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    filled-new-array {p1}, [Lmiui/android/animation/listener/TransitionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->removeListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;

    .line 96
    return-void
.end method

.method public varargs setEase(I[F)V
    .locals 1
    .param p1, "style"    # I
    .param p2, "factors"    # [F

    .line 110
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/base/AnimConfig;->setEase(I[F)Lmiui/android/animation/base/AnimConfig;

    .line 111
    return-void
.end method

.method public varargs setEase(Lmiui/android/animation/property/FloatProperty;I[F)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "style"    # I
    .param p3, "factors"    # [F

    .line 114
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    int-to-long v1, p2

    invoke-virtual {v0, p1, v1, v2, p3}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;

    .line 115
    return-void
.end method

.method public varargs setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;[Lmiui/android/animation/property/FloatProperty;)V
    .locals 6
    .param p1, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 99
    invoke-virtual {p0}, Lmiui/android/animation/controller/StateManager;->getCurrentState()Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    .line 100
    .local v0, "config":Lmiui/android/animation/base/AnimConfig;
    array-length v1, p2

    if-nez v1, :cond_0

    .line 101
    invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    goto :goto_1

    .line 103
    :cond_0
    array-length v1, p2

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, p2, v3

    .line 104
    .local v4, "property":Lmiui/android/animation/property/FloatProperty;
    new-array v5, v2, [F

    invoke-virtual {v0, v4, p1, v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;[F)Lmiui/android/animation/base/AnimConfig;

    .line 103
    .end local v4    # "property":Lmiui/android/animation/property/FloatProperty;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 107
    :cond_1
    :goto_1
    return-void
.end method

.method public setStateFlags(Ljava/lang/Object;J)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "flags"    # J

    .line 118
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 119
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    iput-wide p2, v0, Lmiui/android/animation/controller/AnimState;->flags:J

    .line 120
    return-void
.end method

.method public varargs setTransitionFlags(Ljava/lang/Object;J[Lmiui/android/animation/property/FloatProperty;)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "flags"    # J
    .param p4, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 123
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/StateManager;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    .line 124
    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v1

    .line 125
    .local v1, "config":Lmiui/android/animation/base/AnimConfig;
    array-length v2, p4

    if-nez v2, :cond_0

    .line 126
    iput-wide p2, v1, Lmiui/android/animation/base/AnimConfig;->flags:J

    goto :goto_1

    .line 128
    :cond_0
    array-length v2, p4

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, p4, v3

    .line 129
    .local v4, "property":Lmiui/android/animation/property/FloatProperty;
    invoke-virtual {v1, v4}, Lmiui/android/animation/base/AnimConfig;->getSpecialConfig(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v5

    .line 130
    .local v5, "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    if-nez v5, :cond_1

    .line 131
    new-instance v6, Lmiui/android/animation/base/AnimSpecialConfig;

    invoke-direct {v6}, Lmiui/android/animation/base/AnimSpecialConfig;-><init>()V

    move-object v5, v6

    .line 132
    invoke-virtual {v1, v4, v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/base/AnimConfig;

    .line 134
    :cond_1
    iput-wide p2, v5, Lmiui/android/animation/base/AnimSpecialConfig;->flags:J

    .line 128
    .end local v4    # "property":Lmiui/android/animation/property/FloatProperty;
    .end local v5    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 137
    :cond_2
    :goto_1
    return-void
.end method

.method public setup(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;

    .line 77
    instance-of v0, p1, Lmiui/android/animation/controller/AnimState;

    if-eqz v0, :cond_0

    .line 78
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/controller/AnimState;

    .local v0, "state":Lmiui/android/animation/controller/AnimState;
    goto :goto_0

    .line 80
    .end local v0    # "state":Lmiui/android/animation/controller/AnimState;
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/StateManager;->mStateMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/AnimState;

    .line 81
    .restart local v0    # "state":Lmiui/android/animation/controller/AnimState;
    if-nez v0, :cond_1

    .line 82
    new-instance v1, Lmiui/android/animation/controller/AnimState;

    invoke-direct {v1, p1}, Lmiui/android/animation/controller/AnimState;-><init>(Ljava/lang/Object;)V

    move-object v0, v1

    .line 83
    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/StateManager;->addState(Lmiui/android/animation/controller/AnimState;)V

    .line 86
    :cond_1
    :goto_0
    iput-object v0, p0, Lmiui/android/animation/controller/StateManager;->mCurTag:Ljava/lang/Object;

    .line 87
    return-object v0
.end method
