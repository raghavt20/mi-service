.class public Lmiui/android/animation/controller/FolmeTouch;
.super Lmiui/android/animation/controller/FolmeBase;
.source "FolmeTouch.java"

# interfaces
.implements Lmiui/android/animation/ITouchStyle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/controller/FolmeTouch$LongClickTask;,
        Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;,
        Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;,
        Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;
    }
.end annotation


# static fields
.field private static final DEFAULT_SCALE:F = 0.9f

.field private static final SCALE_DIS:I = 0xa

.field private static sTouchRecord:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mClearTint:Z

.field private mClickInvoked:Z

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mDefListener:Lmiui/android/animation/listener/TransitionListener;

.field private mDownConfig:Lmiui/android/animation/base/AnimConfig;

.field private mDownWeight:I

.field private mDownX:F

.field private mDownY:F

.field private mFontStyle:Lmiui/android/animation/controller/FolmeFont;

.field private mIsDown:Z

.field private mListView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mLocation:[I

.field private mLongClickInvoked:Z

.field private mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

.field private mScaleDist:F

.field private mScaleSetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiui/android/animation/ITouchStyle$TouchType;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mSetTint:Z

.field private mTouchIndex:I

.field private mTouchView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mUpConfig:Lmiui/android/animation/base/AnimConfig;

.field private mUpWeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 50
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lmiui/android/animation/controller/FolmeTouch;->sTouchRecord:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lmiui/android/animation/IAnimTarget;)V
    .locals 7
    .param p1, "targets"    # [Lmiui/android/animation/IAnimTarget;

    .line 87
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V

    .line 62
    const/4 v0, 0x2

    new-array v1, v0, [I

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mLocation:[I

    .line 64
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleSetMap:Ljava/util/Map;

    .line 71
    new-instance v1, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownConfig:Lmiui/android/animation/base/AnimConfig;

    .line 72
    new-instance v1, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpConfig:Lmiui/android/animation/base/AnimConfig;

    .line 75
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z

    .line 77
    new-instance v2, Lmiui/android/animation/controller/FolmeTouch$1;

    invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeTouch$1;-><init>(Lmiui/android/animation/controller/FolmeTouch;)V

    iput-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mDefListener:Lmiui/android/animation/listener/TransitionListener;

    .line 88
    array-length v2, p1

    if-lez v2, :cond_0

    aget-object v1, p1, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, v1}, Lmiui/android/animation/controller/FolmeTouch;->initScaleDist(Lmiui/android/animation/IAnimTarget;)V

    .line 90
    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    .line 91
    .local v1, "propScaleX":Lmiui/android/animation/property/FloatProperty;
    sget-object v2, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    .line 93
    .local v2, "propScaleY":Lmiui/android/animation/property/FloatProperty;
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v4, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v3, v4}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    .line 94
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v3, v1, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    .line 95
    invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 98
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->setTintColor()V

    .line 100
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownConfig:Lmiui/android/animation/base/AnimConfig;

    new-array v4, v0, [F

    fill-array-data v4, :array_0

    const/4 v5, -0x2

    invoke-static {v5, v4}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 101
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownConfig:Lmiui/android/animation/base/AnimConfig;

    iget-object v4, p0, Lmiui/android/animation/controller/FolmeTouch;->mDefListener:Lmiui/android/animation/listener/TransitionListener;

    filled-new-array {v4}, [Lmiui/android/animation/listener/TransitionListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;

    .line 103
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpConfig:Lmiui/android/animation/base/AnimConfig;

    new-array v4, v0, [F

    fill-array-data v4, :array_1

    invoke-virtual {v3, v5, v4}, Lmiui/android/animation/base/AnimConfig;->setEase(I[F)Lmiui/android/animation/base/AnimConfig;

    move-result-object v3

    sget-object v4, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    new-array v0, v0, [F

    fill-array-data v0, :array_2

    .line 104
    const-wide/16 v5, -0x2

    invoke-virtual {v3, v4, v5, v6, v0}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;

    .line 106
    return-void

    nop

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e19999a    # 0.15f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method static synthetic access$000(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)Z
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z
    .param p3, "x3"    # [Lmiui/android/animation/base/AnimConfig;

    .line 45
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->bindListView(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .line 45
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch;->resetViewTouch(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$200(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p1, "x1"    # Landroid/view/View;

    .line 45
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->invokeClick(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lmiui/android/animation/controller/FolmeTouch;)Z
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;

    .line 45
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z

    return v0
.end method

.method static synthetic access$400(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p1, "x1"    # Landroid/view/View;

    .line 45
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->invokeLongClick(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$600(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p1, "x1"    # [Lmiui/android/animation/base/AnimConfig;

    .line 45
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->onEventUp([Lmiui/android/animation/base/AnimConfig;)V

    return-void
.end method

.method static synthetic access$700(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/MotionEvent;
    .param p3, "x3"    # [Lmiui/android/animation/base/AnimConfig;

    .line 45
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    return-void
.end method

.method static synthetic access$900(Lmiui/android/animation/controller/FolmeTouch;)Landroid/view/View$OnLongClickListener;
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeTouch;

    .line 45
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method private varargs bindListView(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "setTouchListener"    # Z
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 302
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->getListViewInfo(Landroid/view/View;)Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;

    move-result-object v0

    move-object v2, v0

    .local v2, "info":Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;
    if-eqz v0, :cond_1

    iget-object v0, v2, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;->listView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1

    .line 303
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleListViewTouch for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 306
    :cond_0
    iget-object v0, v2, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;->listView:Landroid/widget/AbsListView;

    invoke-direct {p0, v0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->handleListViewTouch(Landroid/widget/AbsListView;Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V

    .line 307
    const/4 v0, 0x1

    return v0

    .line 309
    .end local v2    # "info":Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;
    :cond_1
    return v1
.end method

.method private varargs doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p3, "longClick"    # Landroid/view/View$OnLongClickListener;
    .param p4, "clickListenerSet"    # Z
    .param p5, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 241
    invoke-direct {p0, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->setClickAndLongClickListener(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    .line 242
    invoke-direct {p0, p1, p5}, Lmiui/android/animation/controller/FolmeTouch;->handleViewTouch(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V

    .line 244
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->setTouchView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleViewTouch for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v0

    .line 249
    .local v0, "isClickable":Z
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    .line 250
    new-instance v1, Lmiui/android/animation/controller/FolmeTouch$3;

    move-object v2, v1

    move-object v3, p0

    move v4, p4

    move-object v5, p1

    move-object v6, p5

    move v7, v0

    invoke-direct/range {v2 .. v7}, Lmiui/android/animation/controller/FolmeTouch$3;-><init>(Lmiui/android/animation/controller/FolmeTouch;ZLandroid/view/View;[Lmiui/android/animation/base/AnimConfig;Z)V

    .line 258
    .local v1, "task":Ljava/lang/Runnable;
    invoke-static {p1, v1}, Lmiui/android/animation/utils/CommonUtils;->runOnPreDraw(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 260
    .end local v0    # "isClickable":Z
    .end local v1    # "task":Ljava/lang/Runnable;
    :cond_1
    return-void
.end method

.method private varargs getDownConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 698
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownConfig:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-static {p1, v0}, Lmiui/android/animation/utils/CommonUtils;->mergeArray([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/base/AnimConfig;

    return-object v0
.end method

.method private getListViewInfo(Landroid/view/View;)Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .line 318
    new-instance v0, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;-><init>(Lmiui/android/animation/controller/FolmeTouch$1;)V

    .line 319
    .local v0, "info":Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;
    const/4 v1, 0x0

    .line 320
    .local v1, "listView":Landroid/widget/AbsListView;
    move-object v2, p1

    .line 321
    .local v2, "itemView":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 322
    .local v3, "parent":Landroid/view/ViewParent;
    :goto_0
    if-eqz v3, :cond_2

    .line 323
    instance-of v4, v3, Landroid/widget/AbsListView;

    if-eqz v4, :cond_0

    .line 324
    move-object v1, v3

    check-cast v1, Landroid/widget/AbsListView;

    .line 325
    goto :goto_1

    .line 327
    :cond_0
    instance-of v4, v3, Landroid/view/View;

    if-eqz v4, :cond_1

    .line 328
    move-object v2, v3

    check-cast v2, Landroid/view/View;

    .line 330
    :cond_1
    invoke-interface {v3}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    goto :goto_0

    .line 332
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 333
    new-instance v4, Ljava/lang/ref/WeakReference;

    iget-object v5, v0, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;->listView:Landroid/widget/AbsListView;

    invoke-direct {v4, v5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lmiui/android/animation/controller/FolmeTouch;->mListView:Ljava/lang/ref/WeakReference;

    .line 334
    iput-object v1, v0, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;->listView:Landroid/widget/AbsListView;

    .line 335
    iput-object v2, v0, Lmiui/android/animation/controller/FolmeTouch$ListViewInfo;->itemView:Landroid/view/View;

    .line 337
    :cond_3
    return-object v0
.end method

.method public static getListViewTouchListener(Landroid/widget/AbsListView;)Lmiui/android/animation/controller/ListViewTouchListener;
    .locals 1
    .param p0, "listView"    # Landroid/widget/AbsListView;

    .line 341
    const v0, 0x100b0002

    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/ListViewTouchListener;

    return-object v0
.end method

.method private varargs getType([Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle$TouchType;
    .locals 1
    .param p1, "type"    # [Lmiui/android/animation/ITouchStyle$TouchType;

    .line 608
    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    :goto_0
    return-object v0
.end method

.method private varargs getUpConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 702
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpConfig:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-static {p1, v0}, Lmiui/android/animation/utils/CommonUtils;->mergeArray([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/base/AnimConfig;

    return-object v0
.end method

.method private handleClick(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 537
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchIndex:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 538
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    .line 539
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    instance-of v1, v0, Lmiui/android/animation/ViewTarget;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch;->isInTouchSlop(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 540
    move-object v1, v0

    check-cast v1, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v1}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v1

    .line 541
    .local v1, "targetView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    .line 542
    invoke-direct {p0, v1}, Lmiui/android/animation/controller/FolmeTouch;->invokeClick(Landroid/view/View;)V

    .line 545
    .end local v0    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v1    # "targetView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private varargs handleListViewTouch(Landroid/widget/AbsListView;Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "listView"    # Landroid/widget/AbsListView;
    .param p2, "touchView"    # Landroid/view/View;
    .param p3, "setTouchListener"    # Z
    .param p4, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 347
    invoke-static {p1}, Lmiui/android/animation/controller/FolmeTouch;->getListViewTouchListener(Landroid/widget/AbsListView;)Lmiui/android/animation/controller/ListViewTouchListener;

    move-result-object v0

    .line 348
    .local v0, "listener":Lmiui/android/animation/controller/ListViewTouchListener;
    if-nez v0, :cond_0

    .line 349
    new-instance v1, Lmiui/android/animation/controller/ListViewTouchListener;

    invoke-direct {v1, p1}, Lmiui/android/animation/controller/ListViewTouchListener;-><init>(Landroid/widget/AbsListView;)V

    move-object v0, v1

    .line 350
    const v1, 0x100b0002

    invoke-virtual {p1, v1, v0}, Landroid/widget/AbsListView;->setTag(ILjava/lang/Object;)V

    .line 352
    :cond_0
    if-eqz p3, :cond_1

    .line 353
    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 355
    :cond_1
    new-instance v1, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;

    invoke-direct {v1, p0, p4}, Lmiui/android/animation/controller/FolmeTouch$InnerListViewTouchListener;-><init>(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V

    invoke-virtual {v0, p2, v1}, Lmiui/android/animation/controller/ListViewTouchListener;->putListener(Landroid/view/View;Landroid/view/View$OnTouchListener;)V

    .line 356
    return-void
.end method

.method private varargs handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 561
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 567
    :pswitch_0
    invoke-direct {p0, p2, p1, p3}, Lmiui/android/animation/controller/FolmeTouch;->onEventMove(Landroid/view/MotionEvent;Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V

    .line 568
    goto :goto_1

    .line 570
    :pswitch_1
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch;->handleClick(Landroid/view/View;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 563
    :pswitch_2
    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeTouch;->recordDownEvent(Landroid/view/MotionEvent;)V

    .line 564
    invoke-direct {p0, p3}, Lmiui/android/animation/controller/FolmeTouch;->onEventDown([Lmiui/android/animation/base/AnimConfig;)V

    .line 565
    goto :goto_1

    .line 572
    :goto_0
    invoke-direct {p0, p3}, Lmiui/android/animation/controller/FolmeTouch;->onEventUp([Lmiui/android/animation/base/AnimConfig;)V

    .line 575
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private varargs handleViewTouch(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 383
    sget-object v0, Lmiui/android/animation/controller/FolmeTouch;->sTouchRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;

    .line 384
    .local v0, "touchListener":Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;
    if-nez v0, :cond_0

    .line 385
    new-instance v1, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;-><init>(Lmiui/android/animation/controller/FolmeTouch$1;)V

    move-object v0, v1

    .line 386
    sget-object v1, Lmiui/android/animation/controller/FolmeTouch;->sTouchRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 389
    invoke-virtual {v0, p0, p2}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->addTouch(Lmiui/android/animation/controller/FolmeTouch;[Lmiui/android/animation/base/AnimConfig;)V

    .line 390
    return-void
.end method

.method private initScaleDist(Lmiui/android/animation/IAnimTarget;)V
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 129
    instance-of v0, p1, Lmiui/android/animation/ViewTarget;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 130
    .local v0, "view":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_1

    .line 131
    nop

    .line 132
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 131
    const/4 v2, 0x1

    const/high16 v3, 0x41200000    # 10.0f

    invoke-static {v2, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleDist:F

    .line 134
    :cond_1
    return-void
.end method

.method private invokeClick(Landroid/view/View;)V
    .locals 1
    .param p1, "targetView"    # Landroid/view/View;

    .line 548
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickInvoked:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z

    if-nez v0, :cond_0

    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickInvoked:Z

    .line 550
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 552
    :cond_0
    return-void
.end method

.method private invokeLongClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .line 530
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z

    if-nez v0, :cond_0

    .line 531
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z

    .line 532
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    .line 534
    :cond_0
    return-void
.end method

.method private isInTouchSlop(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 555
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    .line 556
    .local v0, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    .line 557
    .local v1, "y":F
    iget v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownX:F

    iget v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownY:F

    invoke-static {v2, v3, v0, v1}, Lmiui/android/animation/utils/CommonUtils;->getDistance(FFFF)D

    move-result-wide v2

    invoke-static {p1}, Lmiui/android/animation/utils/CommonUtils;->getTouchSlop(Landroid/view/View;)F

    move-result v4

    float-to-double v4, v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method static isOnTouchView(Landroid/view/View;[ILandroid/view/MotionEvent;)Z
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "location"    # [I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 177
    const/4 v0, 0x1

    if-eqz p0, :cond_1

    .line 178
    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 179
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    .line 180
    .local v1, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    .line 181
    .local v2, "y":I
    const/4 v3, 0x0

    aget v4, p1, v3

    if-lt v1, v4, :cond_0

    aget v4, p1, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    if-gt v1, v4, :cond_0

    aget v4, p1, v0

    if-lt v2, v4, :cond_0

    aget v4, p1, v0

    .line 182
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    if-gt v2, v4, :cond_0

    goto :goto_0

    :cond_0
    move v0, v3

    .line 181
    :goto_0
    return v0

    .line 184
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_1
    return v0
.end method

.method private isScaleSet(Lmiui/android/animation/ITouchStyle$TouchType;)Z
    .locals 2
    .param p1, "type"    # Lmiui/android/animation/ITouchStyle$TouchType;

    .line 604
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleSetMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private varargs onEventDown([Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 429
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventDown, touchDown"

    invoke-static {v1, v0}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 432
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z

    .line 433
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->touchDown([Lmiui/android/animation/base/AnimConfig;)V

    .line 434
    return-void
.end method

.method private varargs onEventMove(Landroid/view/MotionEvent;Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 437
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z

    if-eqz v0, :cond_1

    .line 438
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLocation:[I

    invoke-static {p2, v0, p1}, Lmiui/android/animation/controller/FolmeTouch;->isOnTouchView(Landroid/view/View;[ILandroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    invoke-virtual {p0, p3}, Lmiui/android/animation/controller/FolmeTouch;->touchUp([Lmiui/android/animation/base/AnimConfig;)V

    .line 440
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->resetTouchStatus()V

    goto :goto_0

    .line 441
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    if-eqz v0, :cond_1

    invoke-direct {p0, p2, p1}, Lmiui/android/animation/controller/FolmeTouch;->isInTouchSlop(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 442
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->stop(Lmiui/android/animation/controller/FolmeTouch;)V

    .line 445
    :cond_1
    :goto_0
    return-void
.end method

.method private varargs onEventUp([Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 448
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z

    if-eqz v0, :cond_1

    .line 449
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventUp, touchUp"

    invoke-static {v1, v0}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 452
    :cond_0
    invoke-virtual {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->touchUp([Lmiui/android/animation/base/AnimConfig;)V

    .line 453
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->resetTouchStatus()V

    .line 455
    :cond_1
    return-void
.end method

.method private recordDownEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 468
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickListener:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    if-eqz v0, :cond_1

    .line 469
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchIndex:I

    .line 470
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownX:F

    .line 471
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownY:F

    .line 472
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickInvoked:Z

    .line 473
    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickInvoked:Z

    .line 474
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->startLongClickTask()V

    .line 476
    :cond_1
    return-void
.end method

.method private resetTouchStatus()V
    .locals 1

    .line 458
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    if-eqz v0, :cond_0

    .line 459
    invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->stop(Lmiui/android/animation/controller/FolmeTouch;)V

    .line 461
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mIsDown:Z

    .line 462
    iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchIndex:I

    .line 463
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownX:F

    .line 464
    iput v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownY:F

    .line 465
    return-void
.end method

.method private resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .line 158
    .local p1, "viewHolder":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/view/View;>;"
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 159
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 160
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 162
    :cond_0
    return-object v0
.end method

.method private resetViewTouch(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "isClickable"    # Z

    .line 416
    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    .line 417
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 418
    return-void
.end method

.method private setClickAndLongClickListener(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V
    .locals 4
    .param p1, "click"    # Landroid/view/View$OnClickListener;
    .param p2, "longClick"    # Landroid/view/View$OnLongClickListener;

    .line 264
    const/4 v0, 0x0

    .line 265
    .local v0, "targetView":Landroid/view/View;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    .line 266
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    instance-of v2, v1, Lmiui/android/animation/ViewTarget;

    if-eqz v2, :cond_0

    .line 267
    move-object v2, v1

    check-cast v2, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v2}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    .line 269
    :cond_0
    if-nez v0, :cond_1

    .line 270
    return-void

    .line 272
    :cond_1
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickListener:Landroid/view/View$OnClickListener;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    if-nez p1, :cond_2

    .line 273
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 274
    :cond_2
    if-eqz p1, :cond_3

    .line 275
    new-instance v2, Lmiui/android/animation/controller/FolmeTouch$4;

    invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeTouch$4;-><init>(Lmiui/android/animation/controller/FolmeTouch;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    :cond_3
    :goto_0
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeTouch;->mClickListener:Landroid/view/View$OnClickListener;

    .line 283
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    if-eqz v2, :cond_4

    if-nez p2, :cond_4

    .line 284
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_1

    .line 285
    :cond_4
    if-eqz p2, :cond_5

    .line 286
    new-instance v2, Lmiui/android/animation/controller/FolmeTouch$5;

    invoke-direct {v2, p0}, Lmiui/android/animation/controller/FolmeTouch$5;-><init>(Lmiui/android/animation/controller/FolmeTouch;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 297
    :cond_5
    :goto_1
    iput-object p2, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 298
    return-void
.end method

.method private setCorner(F)V
    .locals 4
    .param p1, "radius"    # F

    .line 706
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 707
    .local v0, "target":Ljava/lang/Object;
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    .line 708
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    const v2, 0x100b0008

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 710
    :cond_0
    return-void
.end method

.method private setTintColor()V
    .locals 7

    .line 109
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mSetTint:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z

    if-eqz v0, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 113
    .local v0, "tintColor":I
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v1

    .line 114
    .local v1, "target":Ljava/lang/Object;
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_2

    .line 115
    move-object v2, v1

    check-cast v2, Landroid/view/View;

    .line 116
    .local v2, "view":Landroid/view/View;
    sget v3, Lmiui/android/animation/utils/FolmeResColors;->MIUIX_FOLME_COLOR_TOUCH_TINT:I

    .line 117
    .local v3, "colorRes":I
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "uimode"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/UiModeManager;

    .line 118
    .local v4, "mgr":Landroid/app/UiModeManager;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 119
    sget v3, Lmiui/android/animation/utils/FolmeResColors;->MIUIX_FOLME_COLOR_TOUCH_TINT_DARK:I

    .line 121
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 123
    .end local v2    # "view":Landroid/view/View;
    .end local v3    # "colorRes":I
    .end local v4    # "mgr":Landroid/app/UiModeManager;
    :cond_2
    sget-object v2, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    .line 124
    .local v2, "propFg":Lmiui/android/animation/property/FloatProperty;
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v4, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v3, v4}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    int-to-double v4, v0

    invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 125
    iget-object v3, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v4, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v3, v4}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v2, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 126
    return-void

    .line 110
    .end local v0    # "tintColor":I
    .end local v1    # "target":Ljava/lang/Object;
    .end local v2    # "propFg":Lmiui/android/animation/property/FloatProperty;
    :cond_3
    :goto_0
    return-void
.end method

.method private setTouchView(Landroid/view/View;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .line 188
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 189
    .local v0, "touchView":Landroid/view/View;
    :goto_0
    if-ne v0, p1, :cond_1

    .line 190
    const/4 v1, 0x0

    return v1

    .line 192
    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchView:Ljava/lang/ref/WeakReference;

    .line 193
    const/4 v1, 0x1

    return v1
.end method

.method private startLongClickTask()V
    .locals 2

    .line 520
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    if-nez v0, :cond_0

    .line 521
    return-void

    .line 523
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    if-nez v0, :cond_1

    .line 524
    new-instance v0, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;-><init>(Lmiui/android/animation/controller/FolmeTouch$1;)V

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    .line 526
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mLongClickTask:Lmiui/android/animation/controller/FolmeTouch$LongClickTask;

    invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$LongClickTask;->start(Lmiui/android/animation/controller/FolmeTouch;)V

    .line 527
    return-void
.end method


# virtual methods
.method public varargs bindViewOfListItem(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 198
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->setTouchView(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    return-void

    .line 201
    :cond_0
    new-instance v0, Lmiui/android/animation/controller/FolmeTouch$2;

    invoke-direct {v0, p0, p1, p2}, Lmiui/android/animation/controller/FolmeTouch$2;-><init>(Lmiui/android/animation/controller/FolmeTouch;Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V

    invoke-static {p1, v0}, Lmiui/android/animation/utils/CommonUtils;->runOnPreDraw(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 207
    return-void
.end method

.method public cancel()V
    .locals 1

    .line 680
    invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->cancel()V

    .line 681
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mFontStyle:Lmiui/android/animation/controller/FolmeFont;

    if-eqz v0, :cond_0

    .line 682
    invoke-virtual {v0}, Lmiui/android/animation/controller/FolmeFont;->cancel()V

    .line 684
    :cond_0
    return-void
.end method

.method public clean()V
    .locals 3

    .line 138
    invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V

    .line 139
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mFontStyle:Lmiui/android/animation/controller/FolmeFont;

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0}, Lmiui/android/animation/controller/FolmeFont;->clean()V

    .line 142
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 143
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchView:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 144
    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    .line 145
    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mTouchView:Ljava/lang/ref/WeakReference;

    .line 147
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mListView:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    .line 148
    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->resetView(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    move-result-object v0

    .line 149
    .local v0, "listView":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 150
    const v2, 0x100b0002

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 152
    :cond_2
    iput-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mListView:Ljava/lang/ref/WeakReference;

    .line 154
    .end local v0    # "listView":Landroid/view/View;
    :cond_3
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->resetTouchStatus()V

    .line 155
    return-void
.end method

.method public clearTintColor()Lmiui/android/animation/ITouchStyle;
    .locals 3

    .line 613
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z

    .line 614
    sget-object v0, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    .line 615
    .local v0, "propFg":Lmiui/android/animation/property/FloatProperty;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 616
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    .line 617
    return-object p0
.end method

.method public varargs handleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "click"    # Landroid/view/View$OnClickListener;
    .param p3, "longClick"    # Landroid/view/View$OnLongClickListener;
    .param p4, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 230
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/controller/FolmeTouch;->doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V

    .line 231
    return-void
.end method

.method public varargs handleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "click"    # Landroid/view/View$OnClickListener;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 224
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/controller/FolmeTouch;->doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V

    .line 225
    return-void
.end method

.method public varargs handleTouchOf(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "clickListenerSet"    # Z
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 235
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/controller/FolmeTouch;->doHandleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lmiui/android/animation/base/AnimConfig;)V

    .line 236
    return-void
.end method

.method public varargs handleTouchOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 219
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lmiui/android/animation/controller/FolmeTouch;->handleTouchOf(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V

    .line 220
    return-void
.end method

.method public ignoreTouchOf(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .line 211
    sget-object v0, Lmiui/android/animation/controller/FolmeTouch;->sTouchRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;

    .line 212
    .local v0, "touchListener":Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lmiui/android/animation/controller/FolmeTouch$InnerViewTouchListener;->removeTouch(Lmiui/android/animation/controller/FolmeTouch;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    sget-object v1, Lmiui/android/animation/controller/FolmeTouch;->sTouchRecord:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_0
    return-void
.end method

.method public onMotionEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 425
    const/4 v0, 0x0

    new-array v0, v0, [Lmiui/android/animation/base/AnimConfig;

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1, v0}, Lmiui/android/animation/controller/FolmeTouch;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 426
    return-void
.end method

.method public varargs onMotionEventEx(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 421
    invoke-direct {p0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeTouch;->handleMotionEvent(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V

    .line 422
    return-void
.end method

.method public varargs setAlpha(F[Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle;
    .locals 4
    .param p1, "alpha"    # F
    .param p2, "type"    # [Lmiui/android/animation/ITouchStyle$TouchType;

    .line 589
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeTouch;->getType([Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle$TouchType;

    move-result-object v1

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    float-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 590
    return-object p0
.end method

.method public setBackgroundColor(FFFF)Lmiui/android/animation/ITouchStyle;
    .locals 4
    .param p1, "a"    # F
    .param p2, "r"    # F
    .param p3, "g"    # F
    .param p4, "b"    # F

    .line 645
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float v1, p1, v0

    float-to-int v1, v1

    mul-float v2, p2, v0

    float-to-int v2, v2

    mul-float v3, p3, v0

    float-to-int v3, v3

    mul-float/2addr v0, p4

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->setBackgroundColor(I)Lmiui/android/animation/ITouchStyle;

    move-result-object v0

    return-object v0
.end method

.method public setBackgroundColor(I)Lmiui/android/animation/ITouchStyle;
    .locals 5
    .param p1, "color"    # I

    .line 636
    sget-object v0, Lmiui/android/animation/property/ViewPropertyExt;->BACKGROUND:Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;

    .line 637
    .local v0, "propBg":Lmiui/android/animation/property/FloatProperty;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    int-to-double v2, p1

    invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 638
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    .line 639
    invoke-interface {v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-static {v2, v0, v3, v4}, Lmiui/android/animation/internal/AnimValueUtils;->getValueOfTarget(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-double v2, v2

    .line 638
    invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 640
    return-object p0
.end method

.method public setFontStyle(Lmiui/android/animation/controller/FolmeFont;)V
    .locals 0
    .param p1, "style"    # Lmiui/android/animation/controller/FolmeFont;

    .line 166
    iput-object p1, p0, Lmiui/android/animation/controller/FolmeTouch;->mFontStyle:Lmiui/android/animation/controller/FolmeFont;

    .line 167
    return-void
.end method

.method public varargs setScale(F[Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle;
    .locals 5
    .param p1, "scale"    # F
    .param p2, "type"    # [Lmiui/android/animation/ITouchStyle$TouchType;

    .line 595
    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeTouch;->getType([Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle$TouchType;

    move-result-object v0

    .line 596
    .local v0, "relType":Lmiui/android/animation/ITouchStyle$TouchType;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleSetMap:Ljava/util/Map;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 597
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v1, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v3, p1

    .line 598
    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v3, p1

    .line 599
    invoke-virtual {v1, v2, v3, v4}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 600
    return-object p0
.end method

.method public setTint(FFFF)Lmiui/android/animation/ITouchStyle;
    .locals 4
    .param p1, "a"    # F
    .param p2, "r"    # F
    .param p3, "g"    # F
    .param p4, "b"    # F

    .line 630
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float v1, p1, v0

    float-to-int v1, v1

    mul-float v2, p2, v0

    float-to-int v2, v2

    mul-float v3, p3, v0

    float-to-int v3, v3

    mul-float/2addr v0, p4

    float-to-int v0, v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->setTint(I)Lmiui/android/animation/ITouchStyle;

    move-result-object v0

    return-object v0
.end method

.method public setTint(I)Lmiui/android/animation/ITouchStyle;
    .locals 4
    .param p1, "color"    # I

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mSetTint:Z

    .line 623
    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mClearTint:Z

    .line 624
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 625
    return-object p0
.end method

.method public setTintMode(I)Lmiui/android/animation/ITouchStyle;
    .locals 1
    .param p1, "mode"    # I

    .line 171
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;

    .line 172
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0, p1}, Lmiui/android/animation/base/AnimConfig;->setTintMode(I)Lmiui/android/animation/base/AnimConfig;

    .line 173
    return-object p0
.end method

.method public setTouchDown()V
    .locals 2

    .line 688
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->setTintColor()V

    .line 689
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 690
    return-void
.end method

.method public setTouchUp()V
    .locals 2

    .line 694
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 695
    return-void
.end method

.method public varargs touchDown([Lmiui/android/animation/base/AnimConfig;)V
    .locals 9
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 651
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/android/animation/controller/FolmeTouch;->setCorner(F)V

    .line 652
    invoke-direct {p0}, Lmiui/android/animation/controller/FolmeTouch;->setTintColor()V

    .line 653
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->getDownConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    .line 654
    .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mFontStyle:Lmiui/android/animation/controller/FolmeFont;

    if-eqz v1, :cond_0

    .line 655
    iget v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownWeight:I

    invoke-virtual {v1, v2, v0}, Lmiui/android/animation/controller/FolmeFont;->to(I[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IVarFontStyle;

    .line 657
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v2, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    .line 658
    .local v1, "state":Lmiui/android/animation/controller/AnimState;
    sget-object v2, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-direct {p0, v2}, Lmiui/android/animation/controller/FolmeTouch;->isScaleSet(Lmiui/android/animation/ITouchStyle$TouchType;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 659
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->getTarget()Lmiui/android/animation/IAnimTarget;

    move-result-object v2

    .line 660
    .local v2, "target":Lmiui/android/animation/IAnimTarget;
    sget-object v3, Lmiui/android/animation/property/ViewProperty;->WIDTH:Lmiui/android/animation/property/ViewProperty;

    invoke-virtual {v2, v3}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v3

    sget-object v4, Lmiui/android/animation/property/ViewProperty;->HEIGHT:Lmiui/android/animation/property/ViewProperty;

    .line 661
    invoke-virtual {v2, v4}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v4

    .line 660
    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 662
    .local v3, "maxSize":F
    iget v4, p0, Lmiui/android/animation/controller/FolmeTouch;->mScaleDist:F

    sub-float v4, v3, v4

    div-float/2addr v4, v3

    const v5, 0x3f666666    # 0.9f

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 663
    .local v4, "scaleValue":F
    sget-object v5, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v6, v4

    invoke-virtual {v1, v5, v6, v7}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v5

    sget-object v6, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v7, v4

    .line 664
    invoke-virtual {v5, v6, v7, v8}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 666
    .end local v2    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v3    # "maxSize":F
    .end local v4    # "scaleValue":F
    :cond_1
    iget-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v2, v1, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 667
    return-void
.end method

.method public varargs touchUp([Lmiui/android/animation/base/AnimConfig;)V
    .locals 4
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 671
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeTouch;->getUpConfig([Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    .line 672
    .local v0, "configArray":[Lmiui/android/animation/base/AnimConfig;
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mFontStyle:Lmiui/android/animation/controller/FolmeFont;

    if-eqz v1, :cond_0

    .line 673
    iget v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpWeight:I

    invoke-virtual {v1, v2, v0}, Lmiui/android/animation/controller/FolmeFont;->to(I[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IVarFontStyle;

    .line 675
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    iget-object v2, p0, Lmiui/android/animation/controller/FolmeTouch;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v3, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-interface {v2, v3}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 676
    return-void
.end method

.method public useVarFont(Landroid/widget/TextView;III)Lmiui/android/animation/ITouchStyle;
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "fontType"    # I
    .param p3, "fromFontWeight"    # I
    .param p4, "toFontWeight"    # I

    .line 579
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeTouch;->mFontStyle:Lmiui/android/animation/controller/FolmeFont;

    if-eqz v0, :cond_0

    .line 580
    iput p3, p0, Lmiui/android/animation/controller/FolmeTouch;->mUpWeight:I

    .line 581
    iput p4, p0, Lmiui/android/animation/controller/FolmeTouch;->mDownWeight:I

    .line 582
    invoke-virtual {v0, p1, p2, p3}, Lmiui/android/animation/controller/FolmeFont;->useAt(Landroid/widget/TextView;II)Lmiui/android/animation/IVarFontStyle;

    .line 584
    :cond_0
    return-object p0
.end method
