.class public Lmiui/android/animation/controller/FolmeVisible;
.super Lmiui/android/animation/controller/FolmeBase;
.source "FolmeVisible.java"

# interfaces
.implements Lmiui/android/animation/IVisibleStyle;


# instance fields
.field private final mDefConfig:Lmiui/android/animation/base/AnimConfig;

.field private mHasMove:Z

.field private mHasScale:Z

.field private mSetBound:Z


# direct methods
.method public varargs constructor <init>([Lmiui/android/animation/IAnimTarget;)V
    .locals 5
    .param p1, "targets"    # [Lmiui/android/animation/IAnimTarget;

    .line 40
    invoke-direct {p0, p1}, Lmiui/android/animation/controller/FolmeBase;-><init>([Lmiui/android/animation/IAnimTarget;)V

    .line 30
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    const/4 v1, 0x1

    new-array v2, v1, [Lmiui/android/animation/listener/TransitionListener;

    new-instance v3, Lmiui/android/animation/controller/FolmeVisible$1;

    invoke-direct {v3, p0}, Lmiui/android/animation/controller/FolmeVisible$1;-><init>(Lmiui/android/animation/controller/FolmeVisible;)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Lmiui/android/animation/base/AnimConfig;->addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mDefConfig:Lmiui/android/animation/base/AnimConfig;

    .line 41
    invoke-virtual {p0, v1}, Lmiui/android/animation/controller/FolmeVisible;->useAutoAlpha(Z)Lmiui/android/animation/IVisibleStyle;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lmiui/android/animation/controller/FolmeVisible;)Z
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/controller/FolmeVisible;

    .line 24
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mSetBound:Z

    return v0
.end method

.method private varargs getConfig(Lmiui/android/animation/IVisibleStyle$VisibleType;[Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;
    .locals 4
    .param p1, "type"    # Lmiui/android/animation/IVisibleStyle$VisibleType;
    .param p2, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 145
    iget-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasScale:Z

    const/4 v1, 0x2

    const/4 v2, -0x2

    if-nez v0, :cond_1

    iget-boolean v3, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z

    if-nez v3, :cond_1

    .line 146
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mDefConfig:Lmiui/android/animation/base/AnimConfig;

    sget-object v3, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    if-ne p1, v3, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/high16 v3, 0x43960000    # 300.0f

    aput v3, v1, v2

    .line 147
    const/16 v2, 0x10

    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    goto :goto_0

    :cond_0
    new-array v1, v1, [F

    fill-array-data v1, :array_0

    .line 148
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    .line 146
    :goto_0
    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    goto :goto_4

    .line 150
    :cond_1
    if-eqz v0, :cond_3

    iget-boolean v3, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z

    if-nez v3, :cond_3

    .line 151
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mDefConfig:Lmiui/android/animation/base/AnimConfig;

    sget-object v3, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    if-ne p1, v3, :cond_2

    new-array v1, v1, [F

    fill-array-data v1, :array_1

    .line 152
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    goto :goto_1

    :cond_2
    new-array v1, v1, [F

    fill-array-data v1, :array_2

    .line 153
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    .line 151
    :goto_1
    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    goto :goto_4

    .line 155
    :cond_3
    if-nez v0, :cond_5

    .line 156
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mDefConfig:Lmiui/android/animation/base/AnimConfig;

    sget-object v3, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    if-ne p1, v3, :cond_4

    new-array v1, v1, [F

    fill-array-data v1, :array_3

    .line 157
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    goto :goto_2

    :cond_4
    new-array v1, v1, [F

    fill-array-data v1, :array_4

    .line 158
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    .line 156
    :goto_2
    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    goto :goto_4

    .line 161
    :cond_5
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mDefConfig:Lmiui/android/animation/base/AnimConfig;

    sget-object v3, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    if-ne p1, v3, :cond_6

    new-array v1, v1, [F

    fill-array-data v1, :array_5

    .line 162
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    goto :goto_3

    :cond_6
    new-array v1, v1, [F

    fill-array-data v1, :array_6

    .line 163
    invoke-static {v2, v1}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v1

    .line 161
    :goto_3
    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 166
    :goto_4
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mDefConfig:Lmiui/android/animation/base/AnimConfig;

    filled-new-array {v0}, [Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    invoke-static {p2, v0}, Lmiui/android/animation/utils/CommonUtils;->mergeArray([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/base/AnimConfig;

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_1
    .array-data 4
        0x3f19999a    # 0.6f
        0x3eb33333    # 0.35f
    .end array-data

    :array_2
    .array-data 4
        0x3f400000    # 0.75f
        0x3e4ccccd    # 0.2f
    .end array-data

    :array_3
    .array-data 4
        0x3f400000    # 0.75f
        0x3eb33333    # 0.35f
    .end array-data

    :array_4
    .array-data 4
        0x3f400000    # 0.75f
        0x3e800000    # 0.25f
    .end array-data

    :array_5
    .array-data 4
        0x3f266666    # 0.65f
        0x3eb33333    # 0.35f
    .end array-data

    :array_6
    .array-data 4
        0x3f400000    # 0.75f
        0x3e800000    # 0.25f
    .end array-data
.end method

.method private varargs getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType;
    .locals 1
    .param p1, "type"    # [Lmiui/android/animation/IVisibleStyle$VisibleType;

    .line 97
    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public clean()V
    .locals 1

    .line 46
    invoke-super {p0}, Lmiui/android/animation/controller/FolmeBase;->clean()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasScale:Z

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z

    .line 48
    return-void
.end method

.method public varargs hide([Lmiui/android/animation/base/AnimConfig;)V
    .locals 3
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 129
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    sget-object v2, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-direct {p0, v2, p1}, Lmiui/android/animation/controller/FolmeVisible;->getConfig(Lmiui/android/animation/IVisibleStyle$VisibleType;[Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 130
    return-void
.end method

.method public varargs setAlpha(F[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
    .locals 4
    .param p1, "alpha"    # F
    .param p2, "type"    # [Lmiui/android/animation/IVisibleStyle$VisibleType;

    .line 83
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeVisible;->getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType;

    move-result-object v1

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->AUTO_ALPHA:Lmiui/android/animation/property/ViewProperty;

    float-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 84
    return-object p0
.end method

.method public setBound(IIII)Lmiui/android/animation/IVisibleStyle;
    .locals 4
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mSetBound:Z

    .line 53
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->X:Lmiui/android/animation/property/ViewProperty;

    int-to-double v2, p1

    .line 54
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->Y:Lmiui/android/animation/property/ViewProperty;

    int-to-double v2, p2

    .line 55
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->WIDTH:Lmiui/android/animation/property/ViewProperty;

    int-to-double v2, p3

    .line 56
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->HEIGHT:Lmiui/android/animation/property/ViewProperty;

    int-to-double v2, p4

    .line 57
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 58
    return-object p0
.end method

.method public setFlags(J)Lmiui/android/animation/IVisibleStyle;
    .locals 1
    .param p1, "flag"    # J

    .line 77
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-interface {v0, p1, p2}, Lmiui/android/animation/controller/IFolmeStateStyle;->setFlags(J)Lmiui/android/animation/IStateStyle;

    .line 78
    return-object p0
.end method

.method public setHide()Lmiui/android/animation/IVisibleStyle;
    .locals 2

    .line 140
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 141
    return-object p0
.end method

.method public setMove(II)Lmiui/android/animation/IVisibleStyle;
    .locals 1
    .param p1, "moveX"    # I
    .param p2, "moveY"    # I

    .line 102
    sget-object v0, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    filled-new-array {v0}, [Lmiui/android/animation/IVisibleStyle$VisibleType;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lmiui/android/animation/controller/FolmeVisible;->setMove(II[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;

    move-result-object v0

    return-object v0
.end method

.method public varargs setMove(II[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
    .locals 7
    .param p1, "moveX"    # I
    .param p2, "moveY"    # I
    .param p3, "type"    # [Lmiui/android/animation/IVisibleStyle$VisibleType;

    .line 107
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gtz v0, :cond_1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasMove:Z

    .line 108
    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-direct {p0, p3}, Lmiui/android/animation/controller/FolmeVisible;->getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType;

    move-result-object v3

    invoke-interface {v0, v3}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v3, Lmiui/android/animation/property/ViewProperty;->X:Lmiui/android/animation/property/ViewProperty;

    new-array v4, v2, [J

    const-wide/16 v5, 0x1

    aput-wide v5, v4, v1

    .line 110
    invoke-virtual {v0, v3, p1, v4}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/ViewProperty;I[J)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v3, Lmiui/android/animation/property/ViewProperty;->Y:Lmiui/android/animation/property/ViewProperty;

    new-array v2, v2, [J

    aput-wide v5, v2, v1

    .line 111
    invoke-virtual {v0, v3, p2, v2}, Lmiui/android/animation/controller/AnimState;->add(Lmiui/android/animation/property/ViewProperty;I[J)Lmiui/android/animation/controller/AnimState;

    .line 113
    :cond_2
    return-object p0
.end method

.method public varargs setScale(F[Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle;
    .locals 4
    .param p1, "scale"    # F
    .param p2, "type"    # [Lmiui/android/animation/IVisibleStyle$VisibleType;

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mHasScale:Z

    .line 90
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    invoke-direct {p0, p2}, Lmiui/android/animation/controller/FolmeVisible;->getType([Lmiui/android/animation/IVisibleStyle$VisibleType;)Lmiui/android/animation/IVisibleStyle$VisibleType;

    move-result-object v1

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    float-to-double v2, p1

    .line 91
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    float-to-double v2, p1

    .line 92
    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 93
    return-object p0
.end method

.method public setShow()Lmiui/android/animation/IVisibleStyle;
    .locals 2

    .line 134
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;

    .line 135
    return-object p0
.end method

.method public setShowDelay(J)Lmiui/android/animation/IVisibleStyle;
    .locals 2
    .param p1, "delay"    # J

    .line 118
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v0, v1}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    iput-wide p1, v0, Lmiui/android/animation/base/AnimConfig;->delay:J

    .line 119
    return-object p0
.end method

.method public varargs show([Lmiui/android/animation/base/AnimConfig;)V
    .locals 3
    .param p1, "config"    # [Lmiui/android/animation/base/AnimConfig;

    .line 124
    iget-object v0, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    sget-object v2, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-direct {p0, v2, p1}, Lmiui/android/animation/controller/FolmeVisible;->getConfig(Lmiui/android/animation/IVisibleStyle$VisibleType;[Lmiui/android/animation/base/AnimConfig;)[Lmiui/android/animation/base/AnimConfig;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lmiui/android/animation/controller/IFolmeStateStyle;->to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;

    .line 125
    return-void
.end method

.method public useAutoAlpha(Z)Lmiui/android/animation/IVisibleStyle;
    .locals 8
    .param p1, "useAutoAlpha"    # Z

    .line 63
    sget-object v0, Lmiui/android/animation/property/ViewProperty;->AUTO_ALPHA:Lmiui/android/animation/property/ViewProperty;

    .line 64
    .local v0, "autoAlpha":Lmiui/android/animation/property/FloatProperty;
    sget-object v1, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    .line 65
    .local v1, "alpha":Lmiui/android/animation/property/FloatProperty;
    const-wide/16 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    if-eqz p1, :cond_0

    .line 66
    iget-object v6, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v7, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v6, v7}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    invoke-virtual {v6, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    invoke-virtual {v6, v0, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 67
    iget-object v4, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v5, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v4, v5}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v4

    invoke-virtual {v4, v1}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v4

    invoke-virtual {v4, v0, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    goto :goto_0

    .line 69
    :cond_0
    iget-object v6, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v7, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v6, v7}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    invoke-virtual {v6, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v6

    invoke-virtual {v6, v1, v4, v5}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 70
    iget-object v4, p0, Lmiui/android/animation/controller/FolmeVisible;->mState:Lmiui/android/animation/controller/IFolmeStateStyle;

    sget-object v5, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-interface {v4, v5}, Lmiui/android/animation/controller/IFolmeStateStyle;->getState(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v4

    invoke-virtual {v4, v0}, Lmiui/android/animation/controller/AnimState;->remove(Ljava/lang/Object;)Lmiui/android/animation/controller/AnimState;

    move-result-object v4

    invoke-virtual {v4, v1, v2, v3}, Lmiui/android/animation/controller/AnimState;->add(Ljava/lang/Object;D)Lmiui/android/animation/controller/AnimState;

    .line 72
    :goto_0
    return-object p0
.end method
