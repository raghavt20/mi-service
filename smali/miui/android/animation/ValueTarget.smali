.class public Lmiui/android/animation/ValueTarget;
.super Lmiui/android/animation/IAnimTarget;
.source "ValueTarget.java"


# static fields
.field private static final DEFAULT_MIN_VALUE:F = 0.002f

.field static sCreator:Lmiui/android/animation/ITargetCreator;


# instance fields
.field private mMaxType:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mTargetObj:Lmiui/android/animation/property/ValueTargetObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lmiui/android/animation/ValueTarget$1;

    invoke-direct {v0}, Lmiui/android/animation/ValueTarget$1;-><init>()V

    sput-object v0, Lmiui/android/animation/ValueTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/android/animation/ValueTarget;-><init>(Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .param p1, "targetObj"    # Ljava/lang/Object;

    .line 33
    invoke-direct {p0}, Lmiui/android/animation/IAnimTarget;-><init>()V

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lmiui/android/animation/ValueTarget;->mMaxType:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 34
    new-instance v0, Lmiui/android/animation/property/ValueTargetObject;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiui/android/animation/ValueTarget;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    invoke-direct {v0, v1}, Lmiui/android/animation/property/ValueTargetObject;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Lmiui/android/animation/ValueTarget$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lmiui/android/animation/ValueTarget$1;

    .line 15
    invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private isPredefinedProperty(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "property"    # Ljava/lang/Object;

    .line 134
    instance-of v0, p1, Lmiui/android/animation/property/ValueProperty;

    if-nez v0, :cond_1

    instance-of v0, p1, Lmiui/android/animation/property/ViewProperty;

    if-nez v0, :cond_1

    instance-of v0, p1, Lmiui/android/animation/property/ColorProperty;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public clean()V
    .locals 0

    .line 120
    return-void
.end method

.method public createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;)",
            "Lmiui/android/animation/property/FloatProperty;"
        }
    .end annotation

    .line 123
    .local p2, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_1

    const-class v0, Ljava/lang/Integer;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lmiui/android/animation/property/ValueProperty;

    invoke-direct {v0, p1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    new-instance v0, Lmiui/android/animation/property/IntValueProperty;

    invoke-direct {v0, p1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object v0
.end method

.method public getDefaultMinVisible()F
    .locals 1

    .line 130
    const v0, 0x3b03126f    # 0.002f

    return v0
.end method

.method public getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;

    .line 140
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, p1, v0}, Lmiui/android/animation/ValueTarget;->createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    return-object v0
.end method

.method public getIntValue(Ljava/lang/String;)I
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;

    .line 55
    invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getIntValueProperty(Ljava/lang/String;)Lmiui/android/animation/property/IIntValueProperty;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/ValueTarget;->getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I

    move-result v0

    return v0
.end method

.method public getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/IIntValueProperty;

    .line 83
    invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-interface {p1}, Lmiui/android/animation/property/IIntValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 85
    .local v0, "value":Ljava/lang/Integer;
    if-nez v0, :cond_0

    const v1, 0x7fffffff

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    .line 87
    .end local v0    # "value":Ljava/lang/Integer;
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lmiui/android/animation/property/IIntValueProperty;->getIntValue(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getIntValueProperty(Ljava/lang/String;)Lmiui/android/animation/property/IIntValueProperty;
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;

    .line 144
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, p1, v0}, Lmiui/android/animation/ValueTarget;->createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/property/IIntValueProperty;

    return-object v0
.end method

.method public getMinVisibleChange(Ljava/lang/Object;)F
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .line 101
    instance-of v0, p1, Lmiui/android/animation/property/IIntValueProperty;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lmiui/android/animation/property/ColorProperty;

    if-nez v0, :cond_0

    .line 102
    const/high16 v0, 0x3f800000    # 1.0f

    return v0

    .line 104
    :cond_0
    invoke-super {p0, p1}, Lmiui/android/animation/IAnimTarget;->getMinVisibleChange(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method public getTargetObject()Ljava/lang/Object;
    .locals 1

    .line 114
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    return-object v0
.end method

.method public getValue(Ljava/lang/String;)F
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;

    .line 47
    invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/ValueTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v0

    return v0
.end method

.method public getValue(Lmiui/android/animation/property/FloatProperty;)F
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 64
    invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 66
    .local v0, "value":Ljava/lang/Float;
    if-nez v0, :cond_0

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    :goto_0
    return v1

    .line 68
    .end local v0    # "value":Ljava/lang/Float;
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method public getVelocity(Ljava/lang/String;)D
    .locals 2
    .param p1, "propertyName"    # Ljava/lang/String;

    .line 92
    invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiui/android/animation/ValueTarget;->getVelocity(Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v0

    return-wide v0
.end method

.method public isValid()Z
    .locals 1

    .line 109
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->isValid()Z

    move-result v0

    return v0
.end method

.method public setIntValue(Ljava/lang/String;I)V
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .line 59
    invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getIntValueProperty(Ljava/lang/String;)Lmiui/android/animation/property/IIntValueProperty;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmiui/android/animation/ValueTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V

    .line 60
    return-void
.end method

.method public setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
    .locals 4
    .param p1, "property"    # Lmiui/android/animation/property/IIntValueProperty;
    .param p2, "value"    # I

    .line 74
    invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-interface {p1}, Lmiui/android/animation/property/IIntValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 77
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0, p2}, Lmiui/android/animation/property/IIntValueProperty;->setIntValue(Ljava/lang/Object;I)V

    .line 79
    :goto_0
    return-void
.end method

.method public setValue(Ljava/lang/String;F)V
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "value"    # F

    .line 51
    invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lmiui/android/animation/ValueTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V

    .line 52
    return-void
.end method

.method public setValue(Lmiui/android/animation/property/FloatProperty;F)V
    .locals 4
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F

    .line 39
    invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/ValueTarget;->mTargetObj:Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V

    .line 44
    :goto_0
    return-void
.end method

.method public setVelocity(Ljava/lang/String;D)V
    .locals 1
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "velocity"    # D

    .line 96
    invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lmiui/android/animation/ValueTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V

    .line 97
    return-void
.end method
