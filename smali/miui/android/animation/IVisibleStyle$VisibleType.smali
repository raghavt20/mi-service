.class public final enum Lmiui/android/animation/IVisibleStyle$VisibleType;
.super Ljava/lang/Enum;
.source "IVisibleStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/IVisibleStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VisibleType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiui/android/animation/IVisibleStyle$VisibleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/android/animation/IVisibleStyle$VisibleType;

.field public static final enum HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

.field public static final enum SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 12
    new-instance v0, Lmiui/android/animation/IVisibleStyle$VisibleType;

    const-string v1, "SHOW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/IVisibleStyle$VisibleType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/android/animation/IVisibleStyle$VisibleType;->SHOW:Lmiui/android/animation/IVisibleStyle$VisibleType;

    new-instance v1, Lmiui/android/animation/IVisibleStyle$VisibleType;

    const-string v2, "HIDE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lmiui/android/animation/IVisibleStyle$VisibleType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lmiui/android/animation/IVisibleStyle$VisibleType;->HIDE:Lmiui/android/animation/IVisibleStyle$VisibleType;

    .line 11
    filled-new-array {v0, v1}, [Lmiui/android/animation/IVisibleStyle$VisibleType;

    move-result-object v0

    sput-object v0, Lmiui/android/animation/IVisibleStyle$VisibleType;->$VALUES:[Lmiui/android/animation/IVisibleStyle$VisibleType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/android/animation/IVisibleStyle$VisibleType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 11
    const-class v0, Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/IVisibleStyle$VisibleType;

    return-object v0
.end method

.method public static values()[Lmiui/android/animation/IVisibleStyle$VisibleType;
    .locals 1

    .line 11
    sget-object v0, Lmiui/android/animation/IVisibleStyle$VisibleType;->$VALUES:[Lmiui/android/animation/IVisibleStyle$VisibleType;

    invoke-virtual {v0}, [Lmiui/android/animation/IVisibleStyle$VisibleType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/IVisibleStyle$VisibleType;

    return-object v0
.end method
