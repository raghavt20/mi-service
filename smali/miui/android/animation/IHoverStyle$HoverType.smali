.class public final enum Lmiui/android/animation/IHoverStyle$HoverType;
.super Ljava/lang/Enum;
.source "IHoverStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/IHoverStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HoverType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiui/android/animation/IHoverStyle$HoverType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/android/animation/IHoverStyle$HoverType;

.field public static final enum ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

.field public static final enum EXIT:Lmiui/android/animation/IHoverStyle$HoverType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 17
    new-instance v0, Lmiui/android/animation/IHoverStyle$HoverType;

    const-string v1, "ENTER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/IHoverStyle$HoverType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->ENTER:Lmiui/android/animation/IHoverStyle$HoverType;

    new-instance v1, Lmiui/android/animation/IHoverStyle$HoverType;

    const-string v2, "EXIT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lmiui/android/animation/IHoverStyle$HoverType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lmiui/android/animation/IHoverStyle$HoverType;->EXIT:Lmiui/android/animation/IHoverStyle$HoverType;

    .line 16
    filled-new-array {v0, v1}, [Lmiui/android/animation/IHoverStyle$HoverType;

    move-result-object v0

    sput-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->$VALUES:[Lmiui/android/animation/IHoverStyle$HoverType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/android/animation/IHoverStyle$HoverType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 16
    const-class v0, Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/IHoverStyle$HoverType;

    return-object v0
.end method

.method public static values()[Lmiui/android/animation/IHoverStyle$HoverType;
    .locals 1

    .line 16
    sget-object v0, Lmiui/android/animation/IHoverStyle$HoverType;->$VALUES:[Lmiui/android/animation/IHoverStyle$HoverType;

    invoke-virtual {v0}, [Lmiui/android/animation/IHoverStyle$HoverType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/IHoverStyle$HoverType;

    return-object v0
.end method
