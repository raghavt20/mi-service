public class miui.android.animation.styles.TintDrawable extends android.graphics.drawable.Drawable {
	 /* .source "TintDrawable.java" */
	 /* # static fields */
	 private static final android.view.View$OnAttachStateChangeListener sListener;
	 /* # instance fields */
	 private Boolean isSetCorner;
	 private android.graphics.Bitmap mBitmap;
	 private android.graphics.RectF mBounds;
	 private android.graphics.RectF mCornerBounds;
	 private Float mEadius;
	 private android.graphics.drawable.Drawable mOriDrawable;
	 private android.graphics.Paint mPaint;
	 private android.graphics.Rect mSrcRect;
	 private android.view.View mView;
	 /* # direct methods */
	 static miui.android.animation.styles.TintDrawable ( ) {
		 /* .locals 1 */
		 /* .line 52 */
		 /* new-instance v0, Lmiui/android/animation/styles/TintDrawable$1; */
		 /* invoke-direct {v0}, Lmiui/android/animation/styles/TintDrawable$1;-><init>()V */
		 return;
	 } // .end method
	 public miui.android.animation.styles.TintDrawable ( ) {
		 /* .locals 1 */
		 /* .line 35 */
		 /* invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V */
		 /* .line 75 */
		 /* new-instance v0, Landroid/graphics/Paint; */
		 /* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
		 this.mPaint = v0;
		 /* .line 76 */
		 /* new-instance v0, Landroid/graphics/RectF; */
		 /* invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V */
		 this.mBounds = v0;
		 /* .line 77 */
		 /* new-instance v0, Landroid/graphics/Rect; */
		 /* invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V */
		 this.mSrcRect = v0;
		 /* .line 79 */
		 /* new-instance v0, Landroid/graphics/RectF; */
		 /* invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V */
		 this.mCornerBounds = v0;
		 /* .line 80 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lmiui/android/animation/styles/TintDrawable;->isSetCorner:Z */
		 /* .line 81 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lmiui/android/animation/styles/TintDrawable;->mEadius:F */
		 return;
	 } // .end method
	 static android.graphics.drawable.Drawable access$000 ( miui.android.animation.styles.TintDrawable p0 ) { //synthethic
		 /* .locals 1 */
		 /* .param p0, "x0" # Lmiui/android/animation/styles/TintDrawable; */
		 /* .line 35 */
		 v0 = this.mOriDrawable;
	 } // .end method
	 static void access$100 ( miui.android.animation.styles.TintDrawable p0 ) { //synthethic
		 /* .locals 0 */
		 /* .param p0, "x0" # Lmiui/android/animation/styles/TintDrawable; */
		 /* .line 35 */
		 /* invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->clear()V */
		 return;
	 } // .end method
	 private void clear ( ) {
		 /* .locals 0 */
		 /* .line 150 */
		 /* invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->recycleBitmap()V */
		 /* .line 151 */
		 return;
	 } // .end method
	 private void createBitmap ( Integer p0, Integer p1 ) {
		 /* .locals 3 */
		 /* .param p1, "width" # I */
		 /* .param p2, "height" # I */
		 /* .line 135 */
		 v0 = this.mBitmap;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 136 */
			 v0 = 			 (( android.graphics.Bitmap ) v0 ).getWidth ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I
			 /* if-ne v0, p1, :cond_0 */
			 v0 = this.mBitmap;
			 /* .line 137 */
			 v0 = 			 (( android.graphics.Bitmap ) v0 ).getHeight ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I
			 v1 = this.mView;
			 v1 = 			 (( android.view.View ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/view/View;->getHeight()I
			 /* if-ne v0, v1, :cond_0 */
			 /* .line 138 */
			 return;
			 /* .line 140 */
		 } // :cond_0
		 /* invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->recycleBitmap()V */
		 /* .line 141 */
		 v0 = this.mPaint;
		 int v1 = 1; // const/4 v1, 0x1
		 (( android.graphics.Paint ) v0 ).setAntiAlias ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V
		 /* .line 143 */
		 try { // :try_start_0
			 v0 = android.graphics.Bitmap$Config.ARGB_8888;
			 android.graphics.Bitmap .createBitmap ( p1,p2,v0 );
			 this.mBitmap = v0;
			 /* :try_end_0 */
			 /* .catch Ljava/lang/OutOfMemoryError; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 146 */
			 /* .line 144 */
			 /* :catch_0 */
			 /* move-exception v0 */
			 /* .line 145 */
			 /* .local v0, "e":Ljava/lang/OutOfMemoryError; */
			 final String v1 = "miuix_anim"; // const-string v1, "miuix_anim"
			 final String v2 = "TintDrawable.createBitmap failed, out of memory"; // const-string v2, "TintDrawable.createBitmap failed, out of memory"
			 android.util.Log .w ( v1,v2 );
			 /* .line 147 */
		 } // .end local v0 # "e":Ljava/lang/OutOfMemoryError;
	 } // :goto_0
	 return;
} // .end method
public static miui.android.animation.styles.TintDrawable get ( android.view.View p0 ) {
	 /* .locals 2 */
	 /* .param p0, "view" # Landroid/view/View; */
	 /* .line 84 */
	 /* nop */
	 /* .line 85 */
	 (( android.view.View ) p0 ).getForeground ( ); // invoke-virtual {p0}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;
	 /* .line 86 */
	 /* .local v0, "fg":Landroid/graphics/drawable/Drawable; */
	 /* instance-of v1, v0, Lmiui/android/animation/styles/TintDrawable; */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 87 */
		 /* move-object v1, v0 */
		 /* check-cast v1, Lmiui/android/animation/styles/TintDrawable; */
		 /* .line 90 */
	 } // .end local v0 # "fg":Landroid/graphics/drawable/Drawable;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initBitmap ( Integer p0 ) {
/* .locals 7 */
/* .param p1, "tintMode" # I */
/* .line 161 */
/* nop */
/* .line 164 */
v0 = this.mBitmap;
if ( v0 != null) { // if-eqz v0, :cond_2
	 v0 = 	 (( android.graphics.Bitmap ) v0 ).isRecycled ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 169 */
	 } // :cond_0
	 try { // :try_start_0
		 v0 = this.mBitmap;
		 int v1 = 0; // const/4 v1, 0x0
		 (( android.graphics.Bitmap ) v0 ).eraseColor ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V
		 /* .line 170 */
		 /* new-instance v0, Landroid/graphics/Canvas; */
		 v1 = this.mBitmap;
		 /* invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V */
		 /* .line 171 */
		 /* .local v0, "canvas":Landroid/graphics/Canvas; */
		 v1 = this.mView;
		 v1 = 		 (( android.view.View ) v1 ).getScrollX ( ); // invoke-virtual {v1}, Landroid/view/View;->getScrollX()I
		 /* .line 172 */
		 /* .local v1, "left":I */
		 v2 = this.mView;
		 v2 = 		 (( android.view.View ) v2 ).getScrollY ( ); // invoke-virtual {v2}, Landroid/view/View;->getScrollY()I
		 /* .line 173 */
		 /* .local v2, "top":I */
		 /* neg-int v3, v1 */
		 /* int-to-float v3, v3 */
		 /* neg-int v4, v2 */
		 /* int-to-float v4, v4 */
		 (( android.graphics.Canvas ) v0 ).translate ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V
		 /* .line 174 */
		 v3 = this.mView;
		 v4 = this.mOriDrawable;
		 (( android.view.View ) v3 ).setForeground ( v4 ); // invoke-virtual {v3, v4}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V
		 /* .line 175 */
		 v3 = this.mView;
		 (( android.view.View ) v3 ).draw ( v0 ); // invoke-virtual {v3, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
		 /* .line 176 */
		 v3 = this.mView;
		 (( android.view.View ) v3 ).setForeground ( p0 ); // invoke-virtual {v3, p0}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V
		 /* .line 177 */
		 /* if-nez p1, :cond_1 */
		 /* .line 179 */
		 /* new-instance v3, Landroid/graphics/ColorMatrix; */
		 /* const/16 v4, 0x14 */
		 /* new-array v4, v4, [F */
		 /* fill-array-data v4, :array_0 */
		 /* invoke-direct {v3, v4}, Landroid/graphics/ColorMatrix;-><init>([F)V */
		 /* .line 185 */
		 /* .local v3, "mColorMatrix":Landroid/graphics/ColorMatrix; */
		 v4 = this.mPaint;
		 /* new-instance v5, Landroid/graphics/ColorMatrixColorFilter; */
		 /* invoke-direct {v5, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V */
		 (( android.graphics.Paint ) v4 ).setColorFilter ( v5 ); // invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
		 /* .line 186 */
		 v4 = this.mBitmap;
		 v5 = this.mPaint;
		 int v6 = 0; // const/4 v6, 0x0
		 (( android.graphics.Canvas ) v0 ).drawBitmap ( v4, v6, v6, v5 ); // invoke-virtual {v0, v4, v6, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 190 */
	 } // .end local v0 # "canvas":Landroid/graphics/Canvas;
} // .end local v1 # "left":I
} // .end local v2 # "top":I
} // .end local v3 # "mColorMatrix":Landroid/graphics/ColorMatrix;
} // :cond_1
/* .line 188 */
/* :catch_0 */
/* move-exception v0 */
/* .line 189 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "TintDrawable.initBitmap failed, "; // const-string v2, "TintDrawable.initBitmap failed, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "miuix_anim"; // const-string v2, "miuix_anim"
android.util.Log .w ( v2,v1 );
/* .line 191 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
/* .line 165 */
} // :cond_2
} // :goto_1
v0 = this.mView;
v1 = this.mOriDrawable;
(( android.view.View ) v0 ).setForeground ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V
/* .line 166 */
return;
/* :array_0 */
/* .array-data 4 */
/* 0x3f800000 # 1.0f */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x3f800000 # 1.0f */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x3f800000 # 1.0f */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x0 */
/* 0x7f7fffff # Float.MAX_VALUE */
/* 0x0 */
} // .end array-data
} // .end method
private void recycleBitmap ( ) {
/* .locals 1 */
/* .line 154 */
v0 = this.mBitmap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 155 */
(( android.graphics.Bitmap ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
/* .line 156 */
int v0 = 0; // const/4 v0, 0x0
this.mBitmap = v0;
/* .line 158 */
} // :cond_0
return;
} // .end method
static miui.android.animation.styles.TintDrawable setAndGet ( android.view.View p0 ) {
/* .locals 3 */
/* .param p0, "view" # Landroid/view/View; */
/* .line 94 */
miui.android.animation.styles.TintDrawable .get ( p0 );
/* .line 95 */
/* .local v0, "sd":Lmiui/android/animation/styles/TintDrawable; */
/* if-nez v0, :cond_0 */
/* .line 96 */
/* new-instance v1, Lmiui/android/animation/styles/TintDrawable; */
/* invoke-direct {v1}, Lmiui/android/animation/styles/TintDrawable;-><init>()V */
/* move-object v0, v1 */
/* .line 97 */
this.mView = p0;
/* .line 98 */
(( android.view.View ) p0 ).getForeground ( ); // invoke-virtual {p0}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;
/* invoke-direct {v0, v1}, Lmiui/android/animation/styles/TintDrawable;->setOriDrawable(Landroid/graphics/drawable/Drawable;)V */
/* .line 99 */
/* move-object v1, v0 */
/* .line 100 */
/* .local v1, "ref":Lmiui/android/animation/styles/TintDrawable; */
v2 = miui.android.animation.styles.TintDrawable.sListener;
(( android.view.View ) p0 ).addOnAttachStateChangeListener ( v2 ); // invoke-virtual {p0, v2}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V
/* .line 101 */
/* new-instance v2, Lmiui/android/animation/styles/TintDrawable$2; */
/* invoke-direct {v2, p0, v1}, Lmiui/android/animation/styles/TintDrawable$2;-><init>(Landroid/view/View;Lmiui/android/animation/styles/TintDrawable;)V */
miui.android.animation.Folme .post ( p0,v2 );
/* .line 108 */
} // .end local v1 # "ref":Lmiui/android/animation/styles/TintDrawable;
} // :cond_0
} // .end method
private void setOriDrawable ( android.graphics.drawable.Drawable p0 ) {
/* .locals 0 */
/* .param p1, "d" # Landroid/graphics/drawable/Drawable; */
/* .line 112 */
this.mOriDrawable = p1;
/* .line 113 */
return;
} // .end method
/* # virtual methods */
public void draw ( android.graphics.Canvas p0 ) {
/* .locals 9 */
/* .param p1, "canvas" # Landroid/graphics/Canvas; */
/* .line 195 */
v0 = this.mView;
v0 = (( android.view.View ) v0 ).getScrollX ( ); // invoke-virtual {v0}, Landroid/view/View;->getScrollX()I
/* .line 196 */
/* .local v0, "left":I */
v1 = this.mView;
v1 = (( android.view.View ) v1 ).getScrollY ( ); // invoke-virtual {v1}, Landroid/view/View;->getScrollY()I
/* .line 197 */
/* .local v1, "top":I */
v2 = this.mView;
v2 = (( android.view.View ) v2 ).getWidth ( ); // invoke-virtual {v2}, Landroid/view/View;->getWidth()I
/* .line 198 */
/* .local v2, "width":I */
v3 = this.mView;
v3 = (( android.view.View ) v3 ).getHeight ( ); // invoke-virtual {v3}, Landroid/view/View;->getHeight()I
/* .line 199 */
/* .local v3, "height":I */
v4 = this.mBounds;
/* int-to-float v5, v0 */
/* int-to-float v6, v1 */
/* add-int v7, v0, v2 */
/* int-to-float v7, v7 */
/* add-int v8, v1, v3 */
/* int-to-float v8, v8 */
(( android.graphics.RectF ) v4 ).set ( v5, v6, v7, v8 ); // invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V
/* .line 200 */
v4 = this.mSrcRect;
int v5 = 0; // const/4 v5, 0x0
(( android.graphics.Rect ) v4 ).set ( v5, v5, v2, v3 ); // invoke-virtual {v4, v5, v5, v2, v3}, Landroid/graphics/Rect;->set(IIII)V
/* .line 201 */
(( android.graphics.Canvas ) p1 ).save ( ); // invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I
/* .line 202 */
v4 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
v6 = this.mView;
v4 = (( miui.android.animation.property.ViewPropertyExt$ForegroundProperty ) v4 ).getIntValue ( v6 ); // invoke-virtual {v4, v6}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;->getIntValue(Landroid/view/View;)I
/* .line 204 */
/* .local v4, "color":I */
try { // :try_start_0
v6 = this.mBounds;
(( android.graphics.Canvas ) p1 ).clipRect ( v6 ); // invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z
/* .line 205 */
(( android.graphics.Canvas ) p1 ).drawColor ( v5 ); // invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->drawColor(I)V
/* .line 206 */
v5 = this.mOriDrawable;
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 207 */
(( android.graphics.drawable.Drawable ) v5 ).draw ( p1 ); // invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
/* .line 209 */
} // :cond_0
v5 = this.mBitmap;
if ( v5 != null) { // if-eqz v5, :cond_3
v5 = (( android.graphics.Bitmap ) v5 ).isRecycled ( ); // invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 215 */
} // :cond_1
/* iget-boolean v5, p0, Lmiui/android/animation/styles/TintDrawable;->isSetCorner:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 216 */
v5 = this.mCornerBounds;
v6 = this.mSrcRect;
(( android.graphics.RectF ) v5 ).set ( v6 ); // invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V
/* .line 217 */
v5 = this.mPaint;
/* new-instance v6, Landroid/graphics/PorterDuffColorFilter; */
v7 = android.graphics.PorterDuff$Mode.SRC_IN;
/* invoke-direct {v6, v4, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V */
(( android.graphics.Paint ) v5 ).setColorFilter ( v6 ); // invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
/* .line 218 */
v5 = this.mCornerBounds;
/* iget v6, p0, Lmiui/android/animation/styles/TintDrawable;->mEadius:F */
v7 = this.mPaint;
(( android.graphics.Canvas ) p1 ).drawRoundRect ( v5, v6, v6, v7 ); // invoke-virtual {p1, v5, v6, v6, v7}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
/* .line 220 */
} // :cond_2
v5 = this.mPaint;
/* new-instance v6, Landroid/graphics/PorterDuffColorFilter; */
v7 = android.graphics.PorterDuff$Mode.SRC_IN;
/* invoke-direct {v6, v4, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V */
(( android.graphics.Paint ) v5 ).setColorFilter ( v6 ); // invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;
/* .line 221 */
v5 = this.mBitmap;
v6 = this.mSrcRect;
v7 = this.mBounds;
v8 = this.mPaint;
(( android.graphics.Canvas ) p1 ).drawBitmap ( v5, v6, v7, v8 ); // invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 225 */
} // :goto_0
(( android.graphics.Canvas ) p1 ).restore ( ); // invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
/* .line 226 */
/* nop */
/* .line 227 */
return;
/* .line 210 */
} // :cond_3
} // :goto_1
/* nop */
/* .line 211 */
try { // :try_start_1
v5 = this.mView;
v6 = this.mOriDrawable;
(( android.view.View ) v5 ).setForeground ( v6 ); // invoke-virtual {v5, v6}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 225 */
(( android.graphics.Canvas ) p1 ).restore ( ); // invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
/* .line 213 */
return;
/* .line 225 */
/* :catchall_0 */
/* move-exception v5 */
(( android.graphics.Canvas ) p1 ).restore ( ); // invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
/* .line 226 */
/* throw v5 */
} // .end method
public Integer getOpacity ( ) {
/* .locals 1 */
/* .line 49 */
int v0 = -2; // const/4 v0, -0x2
} // .end method
void initTintBuffer ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "tintMode" # I */
/* .line 121 */
v0 = this.mView;
/* if-nez v0, :cond_0 */
/* .line 122 */
return;
/* .line 124 */
} // :cond_0
v0 = (( android.view.View ) v0 ).getWidth ( ); // invoke-virtual {v0}, Landroid/view/View;->getWidth()I
/* .line 125 */
/* .local v0, "width":I */
v1 = this.mView;
v1 = (( android.view.View ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/view/View;->getHeight()I
/* .line 126 */
/* .local v1, "height":I */
if ( v0 != null) { // if-eqz v0, :cond_2
/* if-nez v1, :cond_1 */
/* .line 130 */
} // :cond_1
/* invoke-direct {p0, v0, v1}, Lmiui/android/animation/styles/TintDrawable;->createBitmap(II)V */
/* .line 131 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/styles/TintDrawable;->initBitmap(I)V */
/* .line 132 */
return;
/* .line 127 */
} // :cond_2
} // :goto_0
/* invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->recycleBitmap()V */
/* .line 128 */
return;
} // .end method
void restoreOriginalDrawable ( ) {
/* .locals 0 */
/* .line 230 */
/* invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->clear()V */
/* .line 231 */
(( miui.android.animation.styles.TintDrawable ) p0 ).invalidateSelf ( ); // invoke-virtual {p0}, Lmiui/android/animation/styles/TintDrawable;->invalidateSelf()V
/* .line 232 */
return;
} // .end method
public void setAlpha ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "alpha" # I */
/* .line 40 */
return;
} // .end method
public void setColorFilter ( android.graphics.ColorFilter p0 ) {
/* .locals 0 */
/* .param p1, "colorFilter" # Landroid/graphics/ColorFilter; */
/* .line 45 */
return;
} // .end method
void setCorner ( Float p0 ) {
/* .locals 1 */
/* .param p1, "radius" # F */
/* .line 116 */
int v0 = 0; // const/4 v0, 0x0
/* cmpl-float v0, p1, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lmiui/android/animation/styles/TintDrawable;->isSetCorner:Z */
/* .line 117 */
/* iput p1, p0, Lmiui/android/animation/styles/TintDrawable;->mEadius:F */
/* .line 118 */
return;
} // .end method
