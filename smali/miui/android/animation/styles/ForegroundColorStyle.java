public class miui.android.animation.styles.ForegroundColorStyle extends miui.android.animation.styles.PropertyStyle {
	 /* .source "ForegroundColorStyle.java" */
	 /* # direct methods */
	 public miui.android.animation.styles.ForegroundColorStyle ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Lmiui/android/animation/styles/PropertyStyle;-><init>()V */
		 return;
	 } // .end method
	 public static void end ( miui.android.animation.IAnimTarget p0, miui.android.animation.listener.UpdateInfo p1 ) {
		 /* .locals 4 */
		 /* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .param p1, "update" # Lmiui/android/animation/listener/UpdateInfo; */
		 /* .line 38 */
		 miui.android.animation.styles.ForegroundColorStyle .getView ( p0 );
		 /* .line 39 */
		 /* .local v0, "targetView":Landroid/view/View; */
		 v1 = 		 miui.android.animation.styles.ForegroundColorStyle .isInvalid ( v0 );
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 40 */
			 return;
			 /* .line 42 */
		 } // :cond_0
		 miui.android.animation.styles.TintDrawable .get ( v0 );
		 /* .line 43 */
		 /* .local v1, "drawable":Lmiui/android/animation/styles/TintDrawable; */
		 v2 = this.animInfo;
		 /* iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->value:D */
		 /* double-to-int v2, v2 */
		 /* .line 44 */
		 /* .local v2, "value":I */
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 v3 = 			 android.graphics.Color .alpha ( v2 );
			 /* if-nez v3, :cond_1 */
			 /* .line 45 */
			 (( miui.android.animation.styles.TintDrawable ) v1 ).restoreOriginalDrawable ( ); // invoke-virtual {v1}, Lmiui/android/animation/styles/TintDrawable;->restoreOriginalDrawable()V
			 /* .line 47 */
		 } // :cond_1
		 return;
	 } // .end method
	 private static android.view.View getView ( miui.android.animation.IAnimTarget p0 ) {
		 /* .locals 1 */
		 /* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .line 50 */
		 /* instance-of v0, p0, Lmiui/android/animation/ViewTarget; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 51 */
			 /* move-object v0, p0 */
			 /* check-cast v0, Lmiui/android/animation/ViewTarget; */
			 (( miui.android.animation.ViewTarget ) v0 ).getTargetObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
			 /* .line 53 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 private static Boolean isInvalid ( android.view.View p0 ) {
		 /* .locals 1 */
		 /* .param p0, "target" # Landroid/view/View; */
		 /* .line 57 */
		 if ( p0 != null) { // if-eqz p0, :cond_0
			 int v0 = 0; // const/4 v0, 0x0
		 } // :cond_0
		 int v0 = 1; // const/4 v0, 0x1
	 } // :goto_0
} // .end method
public static void start ( miui.android.animation.IAnimTarget p0, miui.android.animation.listener.UpdateInfo p1 ) {
	 /* .locals 5 */
	 /* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
	 /* .param p1, "update" # Lmiui/android/animation/listener/UpdateInfo; */
	 /* .line 23 */
	 miui.android.animation.styles.ForegroundColorStyle .getView ( p0 );
	 /* .line 24 */
	 /* .local v0, "targetView":Landroid/view/View; */
	 v1 = 	 miui.android.animation.styles.ForegroundColorStyle .isInvalid ( v0 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 25 */
		 return;
		 /* .line 27 */
	 } // :cond_0
	 v1 = this.animInfo;
	 /* iget v1, v1, Lmiui/android/animation/internal/AnimInfo;->tintMode:I */
	 /* .line 28 */
	 /* .local v1, "tintMode":I */
	 miui.android.animation.styles.TintDrawable .setAndGet ( v0 );
	 /* .line 29 */
	 /* .local v2, "drawable":Lmiui/android/animation/styles/TintDrawable; */
	 /* const v3, 0x100b0008 */
	 (( android.view.View ) v0 ).getTag ( v3 ); // invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
	 /* .line 30 */
	 /* .local v3, "object":Ljava/lang/Object; */
	 if ( v3 != null) { // if-eqz v3, :cond_2
		 /* instance-of v4, v3, Ljava/lang/Float; */
		 /* if-nez v4, :cond_1 */
		 /* instance-of v4, v3, Ljava/lang/Integer; */
		 if ( v4 != null) { // if-eqz v4, :cond_2
			 /* .line 31 */
		 } // :cond_1
		 /* move-object v4, v3 */
		 /* check-cast v4, Ljava/lang/Float; */
		 v4 = 		 (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
		 /* .line 32 */
		 /* .local v4, "rodia":F */
		 (( miui.android.animation.styles.TintDrawable ) v2 ).setCorner ( v4 ); // invoke-virtual {v2, v4}, Lmiui/android/animation/styles/TintDrawable;->setCorner(F)V
		 /* .line 34 */
	 } // .end local v4 # "rodia":F
} // :cond_2
/* and-int/lit8 v4, v1, 0x1 */
(( miui.android.animation.styles.TintDrawable ) v2 ).initTintBuffer ( v4 ); // invoke-virtual {v2, v4}, Lmiui/android/animation/styles/TintDrawable;->initTintBuffer(I)V
/* .line 35 */
return;
} // .end method
