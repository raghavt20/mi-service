.class final Lmiui/android/animation/styles/TintDrawable$1;
.super Ljava/lang/Object;
.source "TintDrawable.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/styles/TintDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .line 56
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .line 60
    invoke-static {p1}, Lmiui/android/animation/styles/TintDrawable;->get(Landroid/view/View;)Lmiui/android/animation/styles/TintDrawable;

    move-result-object v0

    .line 61
    .local v0, "sd":Lmiui/android/animation/styles/TintDrawable;
    if-eqz v0, :cond_1

    .line 62
    invoke-static {v0}, Lmiui/android/animation/styles/TintDrawable;->access$000(Lmiui/android/animation/styles/TintDrawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 63
    .local v1, "oriDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {p1, v1}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 66
    :cond_0
    invoke-static {v0}, Lmiui/android/animation/styles/TintDrawable;->access$100(Lmiui/android/animation/styles/TintDrawable;)V

    .line 67
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 69
    .end local v1    # "oriDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    return-void
.end method
