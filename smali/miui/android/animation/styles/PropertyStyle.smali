.class public Lmiui/android/animation/styles/PropertyStyle;
.super Ljava/lang/Object;
.source "PropertyStyle.java"


# static fields
.field private static final LONGEST_DURATION:J = 0x2710L

.field static final mCheckerLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lmiui/android/animation/physics/EquilibriumChecker;",
            ">;"
        }
    .end annotation
.end field

.field static final sAccelerate:Lmiui/android/animation/physics/AccelerateOperator;

.field static final sFriction:Lmiui/android/animation/physics/FrictionOperator;

.field static final sSpring:Lmiui/android/animation/physics/SpringOperator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lmiui/android/animation/physics/SpringOperator;

    invoke-direct {v0}, Lmiui/android/animation/physics/SpringOperator;-><init>()V

    sput-object v0, Lmiui/android/animation/styles/PropertyStyle;->sSpring:Lmiui/android/animation/physics/SpringOperator;

    .line 27
    new-instance v0, Lmiui/android/animation/physics/AccelerateOperator;

    invoke-direct {v0}, Lmiui/android/animation/physics/AccelerateOperator;-><init>()V

    sput-object v0, Lmiui/android/animation/styles/PropertyStyle;->sAccelerate:Lmiui/android/animation/physics/AccelerateOperator;

    .line 28
    new-instance v0, Lmiui/android/animation/physics/FrictionOperator;

    invoke-direct {v0}, Lmiui/android/animation/physics/FrictionOperator;-><init>()V

    sput-object v0, Lmiui/android/animation/styles/PropertyStyle;->sFriction:Lmiui/android/animation/physics/FrictionOperator;

    .line 30
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lmiui/android/animation/styles/PropertyStyle;->mCheckerLocal:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static doAnimationFrame(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V
    .locals 11
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p2, "totalT"    # J
    .param p4, "deltaT"    # J
    .param p6, "averageDelta"    # J

    .line 34
    move-object v8, p1

    iget-wide v0, v8, Lmiui/android/animation/internal/AnimData;->startTime:J

    sub-long v9, p2, v0

    .line 35
    .local v9, "totalTime":J
    iget-object v0, v8, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    invoke-static {v0}, Lmiui/android/animation/utils/EaseManager;->isPhysicsStyle(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    move-object v0, p0

    move-object v1, p1

    move-wide v2, v9

    move-wide v4, p4

    move-wide/from16 v6, p6

    invoke-static/range {v0 .. v7}, Lmiui/android/animation/styles/PropertyStyle;->updatePhysicsAnim(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V

    goto :goto_0

    .line 38
    :cond_0
    invoke-static {p1, v9, v10}, Lmiui/android/animation/styles/PropertyStyle;->updateInterpolatorAnim(Lmiui/android/animation/internal/AnimData;J)V

    .line 40
    :goto_0
    return-void
.end method

.method private static doPhysicsCalculation(Lmiui/android/animation/internal/AnimData;D)V
    .locals 13
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p1, "delta"    # D

    .line 85
    iget-wide v10, p0, Lmiui/android/animation/internal/AnimData;->velocity:D

    .line 86
    .local v10, "startVelocity":D
    iget-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    invoke-static {v0}, Lmiui/android/animation/styles/PropertyStyle;->getPhyOperator(I)Lmiui/android/animation/physics/PhysicsOperator;

    move-result-object v12

    .line 87
    .local v12, "op":Lmiui/android/animation/physics/PhysicsOperator;
    if-eqz v12, :cond_1

    instance-of v0, v12, Lmiui/android/animation/physics/SpringOperator;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    .line 88
    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget-object v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->parameters:[D

    const/4 v1, 0x0

    aget-wide v3, v0, v1

    iget-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget-object v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->parameters:[D

    const/4 v2, 0x1

    aget-wide v5, v0, v2

    const/4 v0, 0x2

    new-array v9, v0, [D

    iget-wide v7, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    aput-wide v7, v9, v1

    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D

    aput-wide v0, v9, v2

    move-object v0, v12

    move-wide v1, v10

    move-wide v7, p1

    invoke-interface/range {v0 .. v9}, Lmiui/android/animation/physics/PhysicsOperator;->updateVelocity(DDDD[D)D

    move-result-wide v0

    .line 96
    .local v0, "velocity":D
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D

    mul-double v4, v0, p1

    add-double/2addr v2, v4

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D

    .line 97
    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D

    goto :goto_1

    .line 89
    .end local v0    # "velocity":D
    :cond_1
    :goto_0
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D

    .line 99
    :goto_1
    return-void
.end method

.method public static getPhyOperator(I)Lmiui/android/animation/physics/PhysicsOperator;
    .locals 1
    .param p0, "style"    # I

    .line 118
    packed-switch p0, :pswitch_data_0

    .line 126
    const/4 v0, 0x0

    return-object v0

    .line 120
    :pswitch_0
    sget-object v0, Lmiui/android/animation/styles/PropertyStyle;->sSpring:Lmiui/android/animation/physics/SpringOperator;

    return-object v0

    .line 122
    :pswitch_1
    sget-object v0, Lmiui/android/animation/styles/PropertyStyle;->sAccelerate:Lmiui/android/animation/physics/AccelerateOperator;

    return-object v0

    .line 124
    :pswitch_2
    sget-object v0, Lmiui/android/animation/styles/PropertyStyle;->sFriction:Lmiui/android/animation/physics/FrictionOperator;

    return-object v0

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static isAnimRunning(Lmiui/android/animation/physics/EquilibriumChecker;Lmiui/android/animation/property/FloatProperty;IDDJ)Z
    .locals 6
    .param p0, "checker"    # Lmiui/android/animation/physics/EquilibriumChecker;
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "easeStyle"    # I
    .param p3, "value"    # D
    .param p5, "velocity"    # D
    .param p7, "totalTime"    # J

    .line 104
    move-object v0, p0

    move v1, p2

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lmiui/android/animation/physics/EquilibriumChecker;->isAtEquilibrium(IDD)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 105
    .local v0, "isRunning":Z
    if-eqz v0, :cond_0

    const-wide/16 v1, 0x2710

    cmp-long v1, p7, v1

    if-lez v1, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 107
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "animation for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stopped for running time too long, totalTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    :cond_0
    return v0
.end method

.method private static isUsingSpringPhy(Lmiui/android/animation/internal/AnimData;)Z
    .locals 2
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;

    .line 131
    iget-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static setFinishValue(Lmiui/android/animation/internal/AnimData;)V
    .locals 2
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;

    .line 78
    invoke-static {p0}, Lmiui/android/animation/styles/PropertyStyle;->isUsingSpringPhy(Lmiui/android/animation/internal/AnimData;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    return-void

    .line 81
    :cond_0
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D

    .line 82
    return-void
.end method

.method private static updateInterpolatorAnim(Lmiui/android/animation/internal/AnimData;J)V
    .locals 5
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p1, "totalTime"    # J

    .line 43
    iget-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    check-cast v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;

    .line 44
    .local v0, "ease":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;
    invoke-static {v0}, Lmiui/android/animation/utils/EaseManager;->getInterpolator(Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;)Landroid/animation/TimeInterpolator;

    move-result-object v1

    .line 45
    .local v1, "interpolator":Landroid/animation/TimeInterpolator;
    iget-wide v2, v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 46
    long-to-float v2, p1

    iget-wide v3, v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J

    long-to-float v3, v3

    div-float/2addr v2, v3

    invoke-interface {v1, v2}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    float-to-double v2, v2

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D

    .line 47
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D

    goto :goto_0

    .line 49
    :cond_0
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lmiui/android/animation/internal/AnimData;->setOp(B)V

    .line 50
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D

    .line 51
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D

    .line 53
    :goto_0
    return-void
.end method

.method private static updatePhysicsAnim(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V
    .locals 19
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p2, "totalTime"    # J
    .param p4, "deltaT"    # J
    .param p6, "averageDelta"    # J

    .line 58
    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    move-wide/from16 v3, p6

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    .line 59
    long-to-float v5, v1

    long-to-float v6, v3

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .local v5, "frameCount":I
    goto :goto_0

    .line 61
    .end local v5    # "frameCount":I
    :cond_0
    const/4 v5, 0x1

    .line 63
    .restart local v5    # "frameCount":I
    :goto_0
    long-to-double v6, v3

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v8

    .line 64
    .local v6, "delta":D
    sget-object v8, Lmiui/android/animation/styles/PropertyStyle;->mCheckerLocal:Ljava/lang/ThreadLocal;

    const-class v9, Lmiui/android/animation/physics/EquilibriumChecker;

    invoke-static {v8, v9}, Lmiui/android/animation/utils/CommonUtils;->getLocal(Ljava/lang/ThreadLocal;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lmiui/android/animation/physics/EquilibriumChecker;

    .line 65
    .local v8, "checker":Lmiui/android/animation/physics/EquilibriumChecker;
    iget-object v9, v0, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    iget-wide v10, v0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    move-object/from16 v14, p0

    invoke-virtual {v8, v14, v9, v10, v11}, Lmiui/android/animation/physics/EquilibriumChecker;->init(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)V

    .line 66
    const/4 v9, 0x0

    move v15, v9

    .local v15, "i":I
    :goto_1
    if-ge v15, v5, :cond_2

    .line 67
    invoke-static {v0, v6, v7}, Lmiui/android/animation/styles/PropertyStyle;->doPhysicsCalculation(Lmiui/android/animation/internal/AnimData;D)V

    .line 68
    iget-object v10, v0, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    iget-object v9, v0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget v11, v9, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    iget-wide v12, v0, Lmiui/android/animation/internal/AnimData;->value:D

    iget-wide v1, v0, Lmiui/android/animation/internal/AnimData;->velocity:D

    move-object v9, v8

    move/from16 v18, v15

    .end local v15    # "i":I
    .local v18, "i":I
    move-wide v14, v1

    move-wide/from16 v16, p2

    invoke-static/range {v9 .. v17}, Lmiui/android/animation/styles/PropertyStyle;->isAnimRunning(Lmiui/android/animation/physics/EquilibriumChecker;Lmiui/android/animation/property/FloatProperty;IDDJ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 70
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lmiui/android/animation/internal/AnimData;->setOp(B)V

    .line 71
    invoke-static/range {p1 .. p1}, Lmiui/android/animation/styles/PropertyStyle;->setFinishValue(Lmiui/android/animation/internal/AnimData;)V

    .line 72
    goto :goto_2

    .line 66
    :cond_1
    add-int/lit8 v15, v18, 0x1

    move-object/from16 v14, p0

    move-wide/from16 v1, p4

    .end local v18    # "i":I
    .restart local v15    # "i":I
    goto :goto_1

    :cond_2
    move/from16 v18, v15

    .line 75
    .end local v15    # "i":I
    :goto_2
    return-void
.end method
