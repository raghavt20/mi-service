public class miui.android.animation.styles.PropertyStyle {
	 /* .source "PropertyStyle.java" */
	 /* # static fields */
	 private static final Long LONGEST_DURATION;
	 static final java.lang.ThreadLocal mCheckerLocal;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/ThreadLocal<", */
	 /* "Lmiui/android/animation/physics/EquilibriumChecker;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
static final miui.android.animation.physics.AccelerateOperator sAccelerate;
static final miui.android.animation.physics.FrictionOperator sFriction;
static final miui.android.animation.physics.SpringOperator sSpring;
/* # direct methods */
static miui.android.animation.styles.PropertyStyle ( ) {
/* .locals 1 */
/* .line 26 */
/* new-instance v0, Lmiui/android/animation/physics/SpringOperator; */
/* invoke-direct {v0}, Lmiui/android/animation/physics/SpringOperator;-><init>()V */
/* .line 27 */
/* new-instance v0, Lmiui/android/animation/physics/AccelerateOperator; */
/* invoke-direct {v0}, Lmiui/android/animation/physics/AccelerateOperator;-><init>()V */
/* .line 28 */
/* new-instance v0, Lmiui/android/animation/physics/FrictionOperator; */
/* invoke-direct {v0}, Lmiui/android/animation/physics/FrictionOperator;-><init>()V */
/* .line 30 */
/* new-instance v0, Ljava/lang/ThreadLocal; */
/* invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V */
return;
} // .end method
public miui.android.animation.styles.PropertyStyle ( ) {
/* .locals 0 */
/* .line 22 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static void doAnimationFrame ( miui.android.animation.IAnimTarget p0, miui.android.animation.internal.AnimData p1, Long p2, Long p3, Long p4 ) {
/* .locals 11 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p2, "totalT" # J */
/* .param p4, "deltaT" # J */
/* .param p6, "averageDelta" # J */
/* .line 34 */
/* move-object v8, p1 */
/* iget-wide v0, v8, Lmiui/android/animation/internal/AnimData;->startTime:J */
/* sub-long v9, p2, v0 */
/* .line 35 */
/* .local v9, "totalTime":J */
v0 = this.ease;
/* iget v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
v0 = miui.android.animation.utils.EaseManager .isPhysicsStyle ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 36 */
	 /* move-object v0, p0 */
	 /* move-object v1, p1 */
	 /* move-wide v2, v9 */
	 /* move-wide v4, p4 */
	 /* move-wide/from16 v6, p6 */
	 /* invoke-static/range {v0 ..v7}, Lmiui/android/animation/styles/PropertyStyle;->updatePhysicsAnim(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V */
	 /* .line 38 */
} // :cond_0
miui.android.animation.styles.PropertyStyle .updateInterpolatorAnim ( p1,v9,v10 );
/* .line 40 */
} // :goto_0
return;
} // .end method
private static void doPhysicsCalculation ( miui.android.animation.internal.AnimData p0, Double p1 ) {
/* .locals 13 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p1, "delta" # D */
/* .line 85 */
/* iget-wide v10, p0, Lmiui/android/animation/internal/AnimData;->velocity:D */
/* .line 86 */
/* .local v10, "startVelocity":D */
v0 = this.ease;
/* iget v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
miui.android.animation.styles.PropertyStyle .getPhyOperator ( v0 );
/* .line 87 */
/* .local v12, "op":Lmiui/android/animation/physics/PhysicsOperator; */
if ( v12 != null) { // if-eqz v12, :cond_1
/* instance-of v0, v12, Lmiui/android/animation/physics/SpringOperator; */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
	 /* .line 88 */
	 v0 = 	 miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 92 */
	 } // :cond_0
	 v0 = this.ease;
	 v0 = this.parameters;
	 int v1 = 0; // const/4 v1, 0x0
	 /* aget-wide v3, v0, v1 */
	 v0 = this.ease;
	 v0 = this.parameters;
	 int v2 = 1; // const/4 v2, 0x1
	 /* aget-wide v5, v0, v2 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* new-array v9, v0, [D */
	 /* iget-wide v7, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
	 /* aput-wide v7, v9, v1 */
	 /* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D */
	 /* aput-wide v0, v9, v2 */
	 /* move-object v0, v12 */
	 /* move-wide v1, v10 */
	 /* move-wide v7, p1 */
	 /* invoke-interface/range {v0 ..v9}, Lmiui/android/animation/physics/PhysicsOperator;->updateVelocity(DDDD[D)D */
	 /* move-result-wide v0 */
	 /* .line 96 */
	 /* .local v0, "velocity":D */
	 /* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D */
	 /* mul-double v4, v0, p1 */
	 /* add-double/2addr v2, v4 */
	 /* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D */
	 /* .line 97 */
	 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D */
	 /* .line 89 */
} // .end local v0 # "velocity":D
} // :cond_1
} // :goto_0
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 90 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D */
/* .line 99 */
} // :goto_1
return;
} // .end method
public static miui.android.animation.physics.PhysicsOperator getPhyOperator ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "style" # I */
/* .line 118 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 126 */
int v0 = 0; // const/4 v0, 0x0
/* .line 120 */
/* :pswitch_0 */
v0 = miui.android.animation.styles.PropertyStyle.sSpring;
/* .line 122 */
/* :pswitch_1 */
v0 = miui.android.animation.styles.PropertyStyle.sAccelerate;
/* .line 124 */
/* :pswitch_2 */
v0 = miui.android.animation.styles.PropertyStyle.sFriction;
/* :pswitch_data_0 */
/* .packed-switch -0x4 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static Boolean isAnimRunning ( miui.android.animation.physics.EquilibriumChecker p0, miui.android.animation.property.FloatProperty p1, Integer p2, Double p3, Double p4, Long p5 ) {
/* .locals 6 */
/* .param p0, "checker" # Lmiui/android/animation/physics/EquilibriumChecker; */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "easeStyle" # I */
/* .param p3, "value" # D */
/* .param p5, "velocity" # D */
/* .param p7, "totalTime" # J */
/* .line 104 */
/* move-object v0, p0 */
/* move v1, p2 */
/* move-wide v2, p3 */
/* move-wide v4, p5 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lmiui/android/animation/physics/EquilibriumChecker;->isAtEquilibrium(IDD)Z */
/* xor-int/lit8 v0, v0, 0x1 */
/* .line 105 */
/* .local v0, "isRunning":Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const-wide/16 v1, 0x2710 */
/* cmp-long v1, p7, v1 */
/* if-lez v1, :cond_0 */
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* .line 107 */
v1 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 108 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "animation for "; // const-string v2, "animation for "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.android.animation.property.FloatProperty ) p1 ).getName ( ); // invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " stopped for running time too long, totalTime = "; // const-string v2, " stopped for running time too long, totalTime = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p7, p8 ); // invoke-virtual {v1, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v1,v2 );
/* .line 114 */
} // :cond_0
} // .end method
private static Boolean isUsingSpringPhy ( miui.android.animation.internal.AnimData p0 ) {
/* .locals 2 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .line 131 */
v0 = this.ease;
/* iget v0, v0, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
int v1 = -2; // const/4 v1, -0x2
/* if-ne v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private static void setFinishValue ( miui.android.animation.internal.AnimData p0 ) {
/* .locals 2 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .line 78 */
v0 = miui.android.animation.styles.PropertyStyle .isUsingSpringPhy ( p0 );
/* if-nez v0, :cond_0 */
/* .line 79 */
return;
/* .line 81 */
} // :cond_0
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 82 */
return;
} // .end method
private static void updateInterpolatorAnim ( miui.android.animation.internal.AnimData p0, Long p1 ) {
/* .locals 5 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p1, "totalTime" # J */
/* .line 43 */
v0 = this.ease;
/* check-cast v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle; */
/* .line 44 */
/* .local v0, "ease":Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle; */
miui.android.animation.utils.EaseManager .getInterpolator ( v0 );
/* .line 45 */
/* .local v1, "interpolator":Landroid/animation/TimeInterpolator; */
/* iget-wide v2, v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J */
/* cmp-long v2, p1, v2 */
/* if-gez v2, :cond_0 */
/* .line 46 */
/* long-to-float v2, p1 */
/* iget-wide v3, v0, Lmiui/android/animation/utils/EaseManager$InterpolateEaseStyle;->duration:J */
/* long-to-float v3, v3 */
v2 = /* div-float/2addr v2, v3 */
/* float-to-double v2, v2 */
/* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D */
/* .line 47 */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D */
/* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 49 */
} // :cond_0
int v2 = 3; // const/4 v2, 0x3
(( miui.android.animation.internal.AnimData ) p0 ).setOp ( v2 ); // invoke-virtual {p0, v2}, Lmiui/android/animation/internal/AnimData;->setOp(B)V
/* .line 50 */
/* const-wide/high16 v2, 0x3ff0000000000000L # 1.0 */
/* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D */
/* .line 51 */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->progress:D */
/* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 53 */
} // :goto_0
return;
} // .end method
private static void updatePhysicsAnim ( miui.android.animation.IAnimTarget p0, miui.android.animation.internal.AnimData p1, Long p2, Long p3, Long p4 ) {
/* .locals 19 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p2, "totalTime" # J */
/* .param p4, "deltaT" # J */
/* .param p6, "averageDelta" # J */
/* .line 58 */
/* move-object/from16 v0, p1 */
/* move-wide/from16 v1, p4 */
/* move-wide/from16 v3, p6 */
/* cmp-long v5, v1, v3 */
/* if-lez v5, :cond_0 */
/* .line 59 */
/* long-to-float v5, v1 */
/* long-to-float v6, v3 */
/* div-float/2addr v5, v6 */
v5 = java.lang.Math .round ( v5 );
/* .local v5, "frameCount":I */
/* .line 61 */
} // .end local v5 # "frameCount":I
} // :cond_0
int v5 = 1; // const/4 v5, 0x1
/* .line 63 */
/* .restart local v5 # "frameCount":I */
} // :goto_0
/* long-to-double v6, v3 */
/* const-wide v8, 0x408f400000000000L # 1000.0 */
/* div-double/2addr v6, v8 */
/* .line 64 */
/* .local v6, "delta":D */
v8 = miui.android.animation.styles.PropertyStyle.mCheckerLocal;
/* const-class v9, Lmiui/android/animation/physics/EquilibriumChecker; */
miui.android.animation.utils.CommonUtils .getLocal ( v8,v9 );
/* check-cast v8, Lmiui/android/animation/physics/EquilibriumChecker; */
/* .line 65 */
/* .local v8, "checker":Lmiui/android/animation/physics/EquilibriumChecker; */
v9 = this.property;
/* iget-wide v10, v0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* move-object/from16 v14, p0 */
(( miui.android.animation.physics.EquilibriumChecker ) v8 ).init ( v14, v9, v10, v11 ); // invoke-virtual {v8, v14, v9, v10, v11}, Lmiui/android/animation/physics/EquilibriumChecker;->init(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)V
/* .line 66 */
int v9 = 0; // const/4 v9, 0x0
/* move v15, v9 */
/* .local v15, "i":I */
} // :goto_1
/* if-ge v15, v5, :cond_2 */
/* .line 67 */
miui.android.animation.styles.PropertyStyle .doPhysicsCalculation ( v0,v6,v7 );
/* .line 68 */
v10 = this.property;
v9 = this.ease;
/* iget v11, v9, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
/* iget-wide v12, v0, Lmiui/android/animation/internal/AnimData;->value:D */
/* iget-wide v1, v0, Lmiui/android/animation/internal/AnimData;->velocity:D */
/* move-object v9, v8 */
/* move/from16 v18, v15 */
} // .end local v15 # "i":I
/* .local v18, "i":I */
/* move-wide v14, v1 */
/* move-wide/from16 v16, p2 */
v1 = /* invoke-static/range {v9 ..v17}, Lmiui/android/animation/styles/PropertyStyle;->isAnimRunning(Lmiui/android/animation/physics/EquilibriumChecker;Lmiui/android/animation/property/FloatProperty;IDDJ)Z */
/* if-nez v1, :cond_1 */
/* .line 70 */
int v1 = 3; // const/4 v1, 0x3
(( miui.android.animation.internal.AnimData ) v0 ).setOp ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/internal/AnimData;->setOp(B)V
/* .line 71 */
/* invoke-static/range {p1 ..p1}, Lmiui/android/animation/styles/PropertyStyle;->setFinishValue(Lmiui/android/animation/internal/AnimData;)V */
/* .line 72 */
/* .line 66 */
} // :cond_1
/* add-int/lit8 v15, v18, 0x1 */
/* move-object/from16 v14, p0 */
/* move-wide/from16 v1, p4 */
} // .end local v18 # "i":I
/* .restart local v15 # "i":I */
} // :cond_2
/* move/from16 v18, v15 */
/* .line 75 */
} // .end local v15 # "i":I
} // :goto_2
return;
} // .end method
