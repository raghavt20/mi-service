.class public Lmiui/android/animation/styles/TintDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "TintDrawable.java"


# static fields
.field private static final sListener:Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field private isSetCorner:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBounds:Landroid/graphics/RectF;

.field private mCornerBounds:Landroid/graphics/RectF;

.field private mEadius:F

.field private mOriDrawable:Landroid/graphics/drawable/Drawable;

.field private mPaint:Landroid/graphics/Paint;

.field private mSrcRect:Landroid/graphics/Rect;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    new-instance v0, Lmiui/android/animation/styles/TintDrawable$1;

    invoke-direct {v0}, Lmiui/android/animation/styles/TintDrawable$1;-><init>()V

    sput-object v0, Lmiui/android/animation/styles/TintDrawable;->sListener:Landroid/view/View$OnAttachStateChangeListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 75
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    .line 76
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBounds:Landroid/graphics/RectF;

    .line 77
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mSrcRect:Landroid/graphics/Rect;

    .line 79
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mCornerBounds:Landroid/graphics/RectF;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/styles/TintDrawable;->isSetCorner:Z

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/styles/TintDrawable;->mEadius:F

    return-void
.end method

.method static synthetic access$000(Lmiui/android/animation/styles/TintDrawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/styles/TintDrawable;

    .line 35
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mOriDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$100(Lmiui/android/animation/styles/TintDrawable;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/styles/TintDrawable;

    .line 35
    invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->clear()V

    return-void
.end method

.method private clear()V
    .locals 0

    .line 150
    invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->recycleBitmap()V

    .line 151
    return-void
.end method

.method private createBitmap(II)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 135
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    .line 137
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 138
    return-void

    .line 140
    :cond_0
    invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->recycleBitmap()V

    .line 141
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 143
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const-string v1, "miuix_anim"

    const-string v2, "TintDrawable.createBitmap failed, out of memory"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :goto_0
    return-void
.end method

.method public static get(Landroid/view/View;)Lmiui/android/animation/styles/TintDrawable;
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .line 84
    nop

    .line 85
    invoke-virtual {p0}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 86
    .local v0, "fg":Landroid/graphics/drawable/Drawable;
    instance-of v1, v0, Lmiui/android/animation/styles/TintDrawable;

    if-eqz v1, :cond_0

    .line 87
    move-object v1, v0

    check-cast v1, Lmiui/android/animation/styles/TintDrawable;

    return-object v1

    .line 90
    .end local v0    # "fg":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private initBitmap(I)V
    .locals 7
    .param p1, "tintMode"    # I

    .line 161
    nop

    .line 164
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 169
    :cond_0
    :try_start_0
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 170
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 171
    .local v0, "canvas":Landroid/graphics/Canvas;
    iget-object v1, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollX()I

    move-result v1

    .line 172
    .local v1, "left":I
    iget-object v2, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v2

    .line 173
    .local v2, "top":I
    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 174
    iget-object v3, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    iget-object v4, p0, Lmiui/android/animation/styles/TintDrawable;->mOriDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 175
    iget-object v3, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 176
    iget-object v3, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 177
    if-nez p1, :cond_1

    .line 179
    new-instance v3, Landroid/graphics/ColorMatrix;

    const/16 v4, 0x14

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-direct {v3, v4}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 185
    .local v3, "mColorMatrix":Landroid/graphics/ColorMatrix;
    iget-object v4, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    new-instance v5, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v5, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 186
    iget-object v4, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v6, v6, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "left":I
    .end local v2    # "top":I
    .end local v3    # "mColorMatrix":Landroid/graphics/ColorMatrix;
    :cond_1
    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TintDrawable.initBitmap failed, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "miuix_anim"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 165
    :cond_2
    :goto_1
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    iget-object v1, p0, Lmiui/android/animation/styles/TintDrawable;->mOriDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 166
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x7f7fffff    # Float.MAX_VALUE
        0x0
    .end array-data
.end method

.method private recycleBitmap()V
    .locals 1

    .line 154
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 156
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    .line 158
    :cond_0
    return-void
.end method

.method static setAndGet(Landroid/view/View;)Lmiui/android/animation/styles/TintDrawable;
    .locals 3
    .param p0, "view"    # Landroid/view/View;

    .line 94
    invoke-static {p0}, Lmiui/android/animation/styles/TintDrawable;->get(Landroid/view/View;)Lmiui/android/animation/styles/TintDrawable;

    move-result-object v0

    .line 95
    .local v0, "sd":Lmiui/android/animation/styles/TintDrawable;
    if-nez v0, :cond_0

    .line 96
    new-instance v1, Lmiui/android/animation/styles/TintDrawable;

    invoke-direct {v1}, Lmiui/android/animation/styles/TintDrawable;-><init>()V

    move-object v0, v1

    .line 97
    iput-object p0, v0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    .line 98
    invoke-virtual {p0}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/android/animation/styles/TintDrawable;->setOriDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    move-object v1, v0

    .line 100
    .local v1, "ref":Lmiui/android/animation/styles/TintDrawable;
    sget-object v2, Lmiui/android/animation/styles/TintDrawable;->sListener:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p0, v2}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 101
    new-instance v2, Lmiui/android/animation/styles/TintDrawable$2;

    invoke-direct {v2, p0, v1}, Lmiui/android/animation/styles/TintDrawable$2;-><init>(Landroid/view/View;Lmiui/android/animation/styles/TintDrawable;)V

    invoke-static {p0, v2}, Lmiui/android/animation/Folme;->post(Ljava/lang/Object;Ljava/lang/Runnable;)V

    .line 108
    .end local v1    # "ref":Lmiui/android/animation/styles/TintDrawable;
    :cond_0
    return-object v0
.end method

.method private setOriDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .line 112
    iput-object p1, p0, Lmiui/android/animation/styles/TintDrawable;->mOriDrawable:Landroid/graphics/drawable/Drawable;

    .line 113
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 195
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v0

    .line 196
    .local v0, "left":I
    iget-object v1, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getScrollY()I

    move-result v1

    .line 197
    .local v1, "top":I
    iget-object v2, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 198
    .local v2, "width":I
    iget-object v3, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 199
    .local v3, "height":I
    iget-object v4, p0, Lmiui/android/animation/styles/TintDrawable;->mBounds:Landroid/graphics/RectF;

    int-to-float v5, v0

    int-to-float v6, v1

    add-int v7, v0, v2

    int-to-float v7, v7

    add-int v8, v1, v3

    int-to-float v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 200
    iget-object v4, p0, Lmiui/android/animation/styles/TintDrawable;->mSrcRect:Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v5, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 201
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 202
    sget-object v4, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    iget-object v6, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v4, v6}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;->getIntValue(Landroid/view/View;)I

    move-result v4

    .line 204
    .local v4, "color":I
    :try_start_0
    iget-object v6, p0, Lmiui/android/animation/styles/TintDrawable;->mBounds:Landroid/graphics/RectF;

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 205
    invoke-virtual {p1, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 206
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mOriDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_0

    .line 207
    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 209
    :cond_0
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    .line 215
    :cond_1
    iget-boolean v5, p0, Lmiui/android/animation/styles/TintDrawable;->isSetCorner:Z

    if-eqz v5, :cond_2

    .line 216
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mCornerBounds:Landroid/graphics/RectF;

    iget-object v6, p0, Lmiui/android/animation/styles/TintDrawable;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 217
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    new-instance v6, Landroid/graphics/PorterDuffColorFilter;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v4, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 218
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mCornerBounds:Landroid/graphics/RectF;

    iget v6, p0, Lmiui/android/animation/styles/TintDrawable;->mEadius:F

    iget-object v7, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v6, v7}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 220
    :cond_2
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    new-instance v6, Landroid/graphics/PorterDuffColorFilter;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v4, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 221
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lmiui/android/animation/styles/TintDrawable;->mSrcRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lmiui/android/animation/styles/TintDrawable;->mBounds:Landroid/graphics/RectF;

    iget-object v8, p0, Lmiui/android/animation/styles/TintDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 226
    nop

    .line 227
    return-void

    .line 210
    :cond_3
    :goto_1
    nop

    .line 211
    :try_start_1
    iget-object v5, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    iget-object v6, p0, Lmiui/android/animation/styles/TintDrawable;->mOriDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v6}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 225
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 213
    return-void

    .line 225
    :catchall_0
    move-exception v5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 226
    throw v5
.end method

.method public getOpacity()I
    .locals 1

    .line 49
    const/4 v0, -0x2

    return v0
.end method

.method initTintBuffer(I)V
    .locals 2
    .param p1, "tintMode"    # I

    .line 121
    iget-object v0, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 122
    return-void

    .line 124
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 125
    .local v0, "width":I
    iget-object v1, p0, Lmiui/android/animation/styles/TintDrawable;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 126
    .local v1, "height":I
    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    goto :goto_0

    .line 130
    :cond_1
    invoke-direct {p0, v0, v1}, Lmiui/android/animation/styles/TintDrawable;->createBitmap(II)V

    .line 131
    invoke-direct {p0, p1}, Lmiui/android/animation/styles/TintDrawable;->initBitmap(I)V

    .line 132
    return-void

    .line 127
    :cond_2
    :goto_0
    invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->recycleBitmap()V

    .line 128
    return-void
.end method

.method restoreOriginalDrawable()V
    .locals 0

    .line 230
    invoke-direct {p0}, Lmiui/android/animation/styles/TintDrawable;->clear()V

    .line 231
    invoke-virtual {p0}, Lmiui/android/animation/styles/TintDrawable;->invalidateSelf()V

    .line 232
    return-void
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .line 40
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .line 45
    return-void
.end method

.method setCorner(F)V
    .locals 1
    .param p1, "radius"    # F

    .line 116
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiui/android/animation/styles/TintDrawable;->isSetCorner:Z

    .line 117
    iput p1, p0, Lmiui/android/animation/styles/TintDrawable;->mEadius:F

    .line 118
    return-void
.end method
