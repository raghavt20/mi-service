.class public Lmiui/android/animation/styles/ForegroundColorStyle;
.super Lmiui/android/animation/styles/PropertyStyle;
.source "ForegroundColorStyle.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lmiui/android/animation/styles/PropertyStyle;-><init>()V

    return-void
.end method

.method public static end(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 4
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "update"    # Lmiui/android/animation/listener/UpdateInfo;

    .line 38
    invoke-static {p0}, Lmiui/android/animation/styles/ForegroundColorStyle;->getView(Lmiui/android/animation/IAnimTarget;)Landroid/view/View;

    move-result-object v0

    .line 39
    .local v0, "targetView":Landroid/view/View;
    invoke-static {v0}, Lmiui/android/animation/styles/ForegroundColorStyle;->isInvalid(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    return-void

    .line 42
    :cond_0
    invoke-static {v0}, Lmiui/android/animation/styles/TintDrawable;->get(Landroid/view/View;)Lmiui/android/animation/styles/TintDrawable;

    move-result-object v1

    .line 43
    .local v1, "drawable":Lmiui/android/animation/styles/TintDrawable;
    iget-object v2, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->value:D

    double-to-int v2, v2

    .line 44
    .local v2, "value":I
    if-eqz v1, :cond_1

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    if-nez v3, :cond_1

    .line 45
    invoke-virtual {v1}, Lmiui/android/animation/styles/TintDrawable;->restoreOriginalDrawable()V

    .line 47
    :cond_1
    return-void
.end method

.method private static getView(Lmiui/android/animation/IAnimTarget;)Landroid/view/View;
    .locals 1
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 50
    instance-of v0, p0, Lmiui/android/animation/ViewTarget;

    if-eqz v0, :cond_0

    .line 51
    move-object v0, p0

    check-cast v0, Lmiui/android/animation/ViewTarget;

    invoke-virtual {v0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 53
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private static isInvalid(Landroid/view/View;)Z
    .locals 1
    .param p0, "target"    # Landroid/view/View;

    .line 57
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public static start(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 5
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "update"    # Lmiui/android/animation/listener/UpdateInfo;

    .line 23
    invoke-static {p0}, Lmiui/android/animation/styles/ForegroundColorStyle;->getView(Lmiui/android/animation/IAnimTarget;)Landroid/view/View;

    move-result-object v0

    .line 24
    .local v0, "targetView":Landroid/view/View;
    invoke-static {v0}, Lmiui/android/animation/styles/ForegroundColorStyle;->isInvalid(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    return-void

    .line 27
    :cond_0
    iget-object v1, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget v1, v1, Lmiui/android/animation/internal/AnimInfo;->tintMode:I

    .line 28
    .local v1, "tintMode":I
    invoke-static {v0}, Lmiui/android/animation/styles/TintDrawable;->setAndGet(Landroid/view/View;)Lmiui/android/animation/styles/TintDrawable;

    move-result-object v2

    .line 29
    .local v2, "drawable":Lmiui/android/animation/styles/TintDrawable;
    const v3, 0x100b0008

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    .line 30
    .local v3, "object":Ljava/lang/Object;
    if-eqz v3, :cond_2

    instance-of v4, v3, Ljava/lang/Float;

    if-nez v4, :cond_1

    instance-of v4, v3, Ljava/lang/Integer;

    if-eqz v4, :cond_2

    .line 31
    :cond_1
    move-object v4, v3

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 32
    .local v4, "rodia":F
    invoke-virtual {v2, v4}, Lmiui/android/animation/styles/TintDrawable;->setCorner(F)V

    .line 34
    .end local v4    # "rodia":F
    :cond_2
    and-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Lmiui/android/animation/styles/TintDrawable;->initTintBuffer(I)V

    .line 35
    return-void
.end method
