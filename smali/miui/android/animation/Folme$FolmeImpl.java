class miui.android.animation.Folme$FolmeImpl implements miui.android.animation.IFolme {
	 /* .source "Folme.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/Folme; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "FolmeImpl" */
} // .end annotation
/* # instance fields */
private miui.android.animation.IHoverStyle mHover;
private miui.android.animation.IStateStyle mState;
private miui.android.animation.IAnimTarget mTargets;
private miui.android.animation.ITouchStyle mTouch;
private miui.android.animation.IVisibleStyle mVisible;
/* # direct methods */
private miui.android.animation.Folme$FolmeImpl ( ) {
/* .locals 1 */
/* .param p1, "target" # [Lmiui/android/animation/IAnimTarget; */
/* .line 111 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 112 */
this.mTargets = p1;
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
miui.android.animation.Folme .access$000 ( v0 );
/* .line 114 */
return;
} // .end method
 miui.android.animation.Folme$FolmeImpl ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # [Lmiui/android/animation/IAnimTarget; */
/* .param p2, "x1" # Lmiui/android/animation/Folme$1; */
/* .line 99 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/Folme$FolmeImpl;-><init>([Lmiui/android/animation/IAnimTarget;)V */
return;
} // .end method
/* # virtual methods */
void clean ( ) {
/* .locals 1 */
/* .line 117 */
v0 = this.mTouch;
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 118 */
	 /* .line 120 */
} // :cond_0
v0 = this.mVisible;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 121 */
	 /* .line 123 */
} // :cond_1
v0 = this.mState;
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 124 */
	 /* .line 126 */
} // :cond_2
v0 = this.mHover;
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 127 */
	 /* .line 129 */
} // :cond_3
return;
} // .end method
void end ( ) {
/* .locals 3 */
/* .line 132 */
v0 = this.mTouch;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 133 */
	 /* new-array v2, v1, [Ljava/lang/Object; */
	 /* .line 135 */
} // :cond_0
v0 = this.mVisible;
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 136 */
	 /* new-array v2, v1, [Ljava/lang/Object; */
	 /* .line 138 */
} // :cond_1
v0 = this.mState;
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 139 */
	 /* new-array v2, v1, [Ljava/lang/Object; */
	 /* .line 141 */
} // :cond_2
v0 = this.mHover;
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 142 */
	 /* new-array v1, v1, [Ljava/lang/Object; */
	 /* .line 144 */
} // :cond_3
return;
} // .end method
public miui.android.animation.IHoverStyle hover ( ) {
/* .locals 2 */
/* .line 148 */
v0 = this.mHover;
/* if-nez v0, :cond_0 */
/* .line 149 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeHover; */
v1 = this.mTargets;
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeHover;-><init>([Lmiui/android/animation/IAnimTarget;)V */
this.mHover = v0;
/* .line 151 */
} // :cond_0
v0 = this.mHover;
} // .end method
public miui.android.animation.IStateStyle state ( ) {
/* .locals 1 */
/* .line 175 */
v0 = this.mState;
/* if-nez v0, :cond_0 */
/* .line 176 */
v0 = this.mTargets;
miui.android.animation.controller.StateComposer .composeStyle ( v0 );
this.mState = v0;
/* .line 178 */
} // :cond_0
v0 = this.mState;
} // .end method
public miui.android.animation.ITouchStyle touch ( ) {
/* .locals 2 */
/* .line 156 */
v0 = this.mTouch;
/* if-nez v0, :cond_0 */
/* .line 157 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeTouch; */
v1 = this.mTargets;
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeTouch;-><init>([Lmiui/android/animation/IAnimTarget;)V */
/* .line 158 */
/* .local v0, "touch":Lmiui/android/animation/controller/FolmeTouch; */
/* new-instance v1, Lmiui/android/animation/controller/FolmeFont; */
/* invoke-direct {v1}, Lmiui/android/animation/controller/FolmeFont;-><init>()V */
/* .line 159 */
/* .local v1, "fontStyle":Lmiui/android/animation/controller/FolmeFont; */
(( miui.android.animation.controller.FolmeTouch ) v0 ).setFontStyle ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/controller/FolmeTouch;->setFontStyle(Lmiui/android/animation/controller/FolmeFont;)V
/* .line 160 */
this.mTouch = v0;
/* .line 162 */
} // .end local v0 # "touch":Lmiui/android/animation/controller/FolmeTouch;
} // .end local v1 # "fontStyle":Lmiui/android/animation/controller/FolmeFont;
} // :cond_0
v0 = this.mTouch;
} // .end method
public miui.android.animation.IVisibleStyle visible ( ) {
/* .locals 2 */
/* .line 167 */
v0 = this.mVisible;
/* if-nez v0, :cond_0 */
/* .line 168 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeVisible; */
v1 = this.mTargets;
/* invoke-direct {v0, v1}, Lmiui/android/animation/controller/FolmeVisible;-><init>([Lmiui/android/animation/IAnimTarget;)V */
this.mVisible = v0;
/* .line 170 */
} // :cond_0
v0 = this.mVisible;
} // .end method
