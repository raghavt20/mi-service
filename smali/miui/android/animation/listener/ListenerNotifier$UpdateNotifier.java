class miui.android.animation.listener.ListenerNotifier$UpdateNotifier implements miui.android.animation.listener.ListenerNotifier$INotifier {
	 /* .source "ListenerNotifier.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/listener/ListenerNotifier; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "UpdateNotifier" */
} // .end annotation
/* # direct methods */
 miui.android.animation.listener.ListenerNotifier$UpdateNotifier ( ) {
/* .locals 0 */
/* .line 58 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
private void notifySingleProperty ( java.lang.Object p0, miui.android.animation.listener.TransitionListener p1, miui.android.animation.listener.UpdateInfo p2 ) {
/* .locals 9 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .param p3, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .line 72 */
v0 = this.property;
v1 = (( miui.android.animation.listener.UpdateInfo ) p3 ).getFloatValue ( ); // invoke-virtual {p3}, Lmiui/android/animation/listener/UpdateInfo;->getFloatValue()F
/* iget-boolean v2, p3, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
(( miui.android.animation.listener.TransitionListener ) p2 ).onUpdate ( p1, v0, v1, v2 ); // invoke-virtual {p2, p1, v0, v1, v2}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;FZ)V
/* .line 73 */
/* iget-boolean v0, p3, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 74 */
	 v0 = this.property;
	 /* move-object v5, v0 */
	 /* check-cast v5, Lmiui/android/animation/property/IIntValueProperty; */
	 v6 = 	 (( miui.android.animation.listener.UpdateInfo ) p3 ).getIntValue ( ); // invoke-virtual {p3}, Lmiui/android/animation/listener/UpdateInfo;->getIntValue()I
	 /* iget-wide v0, p3, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
	 /* double-to-float v7, v0 */
	 /* iget-boolean v8, p3, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
	 /* move-object v3, p2 */
	 /* move-object v4, p1 */
	 /* invoke-virtual/range {v3 ..v8}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/IIntValueProperty;IFZ)V */
	 /* .line 77 */
} // :cond_0
v5 = this.property;
v6 = (( miui.android.animation.listener.UpdateInfo ) p3 ).getFloatValue ( ); // invoke-virtual {p3}, Lmiui/android/animation/listener/UpdateInfo;->getFloatValue()F
/* iget-wide v0, p3, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
/* double-to-float v7, v0 */
/* iget-boolean v8, p3, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
/* move-object v3, p2 */
/* move-object v4, p1 */
/* invoke-virtual/range {v3 ..v8}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;FFZ)V */
/* .line 80 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void doNotify ( java.lang.Object p0, miui.android.animation.listener.TransitionListener p1, java.util.Collection p2, miui.android.animation.listener.UpdateInfo p3 ) {
/* .locals 2 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .param p4, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 63 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v0 = if ( p3 != null) { // if-eqz p3, :cond_0
/* const/16 v1, 0xfa0 */
/* if-gt v0, v1, :cond_0 */
/* .line 64 */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 65 */
/* .local v1, "updateInfo":Lmiui/android/animation/listener/UpdateInfo; */
/* invoke-direct {p0, p1, p2, v1}, Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;->notifySingleProperty(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 66 */
} // .end local v1 # "updateInfo":Lmiui/android/animation/listener/UpdateInfo;
/* .line 68 */
} // :cond_0
(( miui.android.animation.listener.TransitionListener ) p2 ).onUpdate ( p1, p3 ); // invoke-virtual {p2, p1, p3}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
/* .line 69 */
return;
} // .end method
