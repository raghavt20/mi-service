public class miui.android.animation.listener.UpdateInfo {
	 /* .source "UpdateInfo.java" */
	 /* # instance fields */
	 public final miui.android.animation.internal.AnimInfo animInfo;
	 public volatile Integer frameCount;
	 public volatile Boolean isCompleted;
	 public final miui.android.animation.property.FloatProperty property;
	 public final Boolean useInt;
	 public volatile Double velocity;
	 /* # direct methods */
	 public miui.android.animation.listener.UpdateInfo ( ) {
		 /* .locals 1 */
		 /* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
		 /* .line 43 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 41 */
		 /* new-instance v0, Lmiui/android/animation/internal/AnimInfo; */
		 /* invoke-direct {v0}, Lmiui/android/animation/internal/AnimInfo;-><init>()V */
		 this.animInfo = v0;
		 /* .line 44 */
		 this.property = p1;
		 /* .line 45 */
		 /* instance-of v0, p1, Lmiui/android/animation/property/IIntValueProperty; */
		 /* iput-boolean v0, p0, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z */
		 /* .line 46 */
		 return;
	 } // .end method
	 public static miui.android.animation.listener.UpdateInfo findBy ( java.util.Collection p0, miui.android.animation.property.FloatProperty p1 ) {
		 /* .locals 3 */
		 /* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/Collection<", */
		 /* "Lmiui/android/animation/listener/UpdateInfo;", */
		 /* ">;", */
		 /* "Lmiui/android/animation/property/FloatProperty;", */
		 /* ")", */
		 /* "Lmiui/android/animation/listener/UpdateInfo;" */
		 /* } */
	 } // .end annotation
	 /* .line 23 */
	 /* .local p0, "list":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* check-cast v1, Lmiui/android/animation/listener/UpdateInfo; */
	 /* .line 24 */
	 /* .local v1, "update":Lmiui/android/animation/listener/UpdateInfo; */
	 v2 = this.property;
	 v2 = 	 (( java.lang.Object ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 25 */
		 /* .line 27 */
	 } // .end local v1 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_0
/* .line 28 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static miui.android.animation.listener.UpdateInfo findByName ( java.util.Collection p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Lmiui/android/animation/listener/UpdateInfo;" */
/* } */
} // .end annotation
/* .line 14 */
/* .local p0, "list":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 15 */
/* .local v1, "update":Lmiui/android/animation/listener/UpdateInfo; */
v2 = this.property;
(( miui.android.animation.property.FloatProperty ) v2 ).getName ( ); // invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 16 */
/* .line 18 */
} // .end local v1 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_0
/* .line 19 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public Float getFloatValue ( ) {
/* .locals 6 */
/* .line 63 */
v0 = this.animInfo;
/* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
/* .line 64 */
/* .local v0, "setToValue":D */
/* const-wide v2, 0x7fefffffffffffffL # Double.MAX_VALUE */
/* cmpl-double v4, v0, v2 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 65 */
/* double-to-float v2, v0 */
/* .line 67 */
} // :cond_0
v4 = this.animInfo;
/* iget-wide v4, v4, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* cmpl-double v2, v4, v2 */
/* if-nez v2, :cond_1 */
/* const v2, 0x7f7fffff # Float.MAX_VALUE */
} // :cond_1
v2 = this.animInfo;
/* iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* double-to-float v2, v2 */
} // :goto_0
} // .end method
public Integer getIntValue ( ) {
/* .locals 6 */
/* .line 71 */
v0 = this.animInfo;
/* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
/* .line 72 */
/* .local v0, "setToValue":D */
/* const-wide v2, 0x7fefffffffffffffL # Double.MAX_VALUE */
/* cmpl-double v4, v0, v2 */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 73 */
/* double-to-int v2, v0 */
/* .line 75 */
} // :cond_0
v4 = this.animInfo;
/* iget-wide v4, v4, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* cmpl-double v2, v4, v2 */
/* if-nez v2, :cond_1 */
/* const v2, 0x7fffffff */
} // :cond_1
v2 = this.animInfo;
/* iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* double-to-int v2, v2 */
} // :goto_0
} // .end method
public java.lang.Class getType ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/lang/Class<", */
/* "*>;" */
/* } */
} // .end annotation
/* .line 49 */
v0 = this.property;
/* instance-of v0, v0, Lmiui/android/animation/property/IIntValueProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = java.lang.Integer.TYPE;
} // :cond_0
v0 = java.lang.Float.TYPE;
} // :goto_0
} // .end method
public java.lang.Object getValue ( java.lang.Class p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .line 53 */
/* .local p1, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* const-class v0, Ljava/lang/Float; */
/* if-eq p1, v0, :cond_3 */
v0 = java.lang.Float.TYPE;
/* if-ne p1, v0, :cond_0 */
/* .line 55 */
} // :cond_0
/* const-class v0, Ljava/lang/Double; */
/* if-eq p1, v0, :cond_2 */
v0 = java.lang.Double.TYPE;
/* if-ne p1, v0, :cond_1 */
/* .line 58 */
} // :cond_1
v0 = (( miui.android.animation.listener.UpdateInfo ) p0 ).getIntValue ( ); // invoke-virtual {p0}, Lmiui/android/animation/listener/UpdateInfo;->getIntValue()I
java.lang.Integer .valueOf ( v0 );
/* .line 56 */
} // :cond_2
} // :goto_0
v0 = this.animInfo;
/* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->value:D */
java.lang.Double .valueOf ( v0,v1 );
/* .line 54 */
} // :cond_3
} // :goto_1
v0 = (( miui.android.animation.listener.UpdateInfo ) p0 ).getFloatValue ( ); // invoke-virtual {p0}, Lmiui/android/animation/listener/UpdateInfo;->getFloatValue()F
java.lang.Float .valueOf ( v0 );
} // .end method
public Boolean isValid ( ) {
/* .locals 1 */
/* .line 104 */
v0 = this.property;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void reset ( ) {
/* .locals 1 */
/* .line 91 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
/* .line 92 */
/* iput v0, p0, Lmiui/android/animation/listener/UpdateInfo;->frameCount:I */
/* .line 93 */
return;
} // .end method
public void setOp ( Object p0 ) {
/* .locals 2 */
/* .param p1, "op" # B */
/* .line 96 */
int v0 = 1; // const/4 v0, 0x1
if ( p1 != null) { // if-eqz p1, :cond_1
int v1 = 2; // const/4 v1, 0x2
/* if-le p1, v1, :cond_0 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :cond_1
} // :goto_0
/* move v1, v0 */
} // :goto_1
/* iput-boolean v1, p0, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
/* .line 97 */
/* iget-boolean v1, p0, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.animInfo;
/* iget-byte v1, v1, Lmiui/android/animation/internal/AnimInfo;->op:B */
v1 = miui.android.animation.internal.AnimTask .isRunning ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 98 */
v1 = this.animInfo;
/* iput-boolean v0, v1, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z */
/* .line 100 */
} // :cond_2
v0 = this.animInfo;
/* iput-byte p1, v0, Lmiui/android/animation/internal/AnimInfo;->op:B */
/* .line 101 */
return;
} // .end method
public void setTargetValue ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 2 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 108 */
/* iget-boolean v0, p0, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 109 */
v0 = this.property;
/* check-cast v0, Lmiui/android/animation/property/IIntValueProperty; */
v1 = (( miui.android.animation.listener.UpdateInfo ) p0 ).getIntValue ( ); // invoke-virtual {p0}, Lmiui/android/animation/listener/UpdateInfo;->getIntValue()I
(( miui.android.animation.IAnimTarget ) p1 ).setIntValue ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
/* .line 111 */
} // :cond_0
v0 = this.property;
v1 = (( miui.android.animation.listener.UpdateInfo ) p0 ).getFloatValue ( ); // invoke-virtual {p0}, Lmiui/android/animation/listener/UpdateInfo;->getFloatValue()F
(( miui.android.animation.IAnimTarget ) p1 ).setValue ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lmiui/android/animation/IAnimTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 113 */
} // :goto_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 80 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "UpdateInfo{, property="; // const-string v1, "UpdateInfo{, property="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.property;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", velocity="; // const-string v1, ", velocity="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", value = "; // const-string v1, ", value = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.animInfo;
/* iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->value:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", useInt="; // const-string v1, ", useInt="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", frameCount="; // const-string v1, ", frameCount="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/listener/UpdateInfo;->frameCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", isCompleted="; // const-string v1, ", isCompleted="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
