.class public Lmiui/android/animation/listener/TransitionListener;
.super Ljava/lang/Object;
.source "TransitionListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .param p1, "fs"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method


# virtual methods
.method public onBegin(Ljava/lang/Object;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public onBegin(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 66
    .local p2, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    return-void
.end method

.method public onBegin(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .param p2, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 62
    return-void
.end method

.method public onCancel(Ljava/lang/Object;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public onCancel(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .param p2, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 82
    return-void
.end method

.method public onComplete(Ljava/lang/Object;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public onComplete(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .param p2, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 74
    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 54
    .local p2, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;FFZ)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "value"    # F
    .param p4, "velocity"    # F
    .param p5, "isCompleted"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 41
    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;FZ)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "value"    # F
    .param p4, "isCompleted"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 32
    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/IIntValueProperty;IFZ)V
    .locals 0
    .param p1, "toTag"    # Ljava/lang/Object;
    .param p2, "property"    # Lmiui/android/animation/property/IIntValueProperty;
    .param p3, "value"    # I
    .param p4, "velocity"    # F
    .param p5, "isCompleted"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 50
    return-void
.end method
