.class public Lmiui/android/animation/listener/ListenerNotifier;
.super Ljava/lang/Object;
.source "ListenerNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;,
        Lmiui/android/animation/listener/ListenerNotifier$INotifier;
    }
.end annotation


# static fields
.field static final sBegin:Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;

.field static final sCancelAll:Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;

.field static final sEndAll:Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;

.field static final sMassUpdate:Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;

.field static final sPropertyBegin:Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;

.field static final sPropertyEnd:Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;

.field static final sUpdate:Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;


# instance fields
.field final mListenerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/TransitionListener;",
            ">;>;"
        }
    .end annotation
.end field

.field final mTarget:Lmiui/android/animation/IAnimTarget;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sBegin:Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;

    .line 44
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sPropertyBegin:Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;

    .line 56
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sMassUpdate:Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;

    .line 82
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sUpdate:Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;

    .line 101
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sPropertyEnd:Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;

    .line 111
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sCancelAll:Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;

    .line 121
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;

    invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier;->sEndAll:Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;

    return-void
.end method

.method public constructor <init>(Lmiui/android/animation/IAnimTarget;)V
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/listener/ListenerNotifier;->mListenerMap:Ljava/util/Map;

    .line 128
    iput-object p1, p0, Lmiui/android/animation/listener/ListenerNotifier;->mTarget:Lmiui/android/animation/IAnimTarget;

    .line 129
    return-void
.end method

.method private getListenerSet(Ljava/lang/Object;)Ljava/util/List;
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/TransitionListener;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lmiui/android/animation/listener/ListenerNotifier;->mListenerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 147
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;"
    if-nez v0, :cond_0

    .line 148
    const-class v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lmiui/android/animation/utils/ObjectPool;->acquire(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/List;

    .line 149
    iget-object v1, p0, Lmiui/android/animation/listener/ListenerNotifier;->mListenerMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_0
    return-object v0
.end method

.method private notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;
    .param p3, "notifier"    # Lmiui/android/animation/listener/ListenerNotifier$INotifier;
    .param p5, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/listener/ListenerNotifier$INotifier;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ")V"
        }
    .end annotation

    .line 183
    .local p4, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    iget-object v0, p0, Lmiui/android/animation/listener/ListenerNotifier;->mListenerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 184
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    invoke-static {p2, v0, p3, p4, p5}, Lmiui/android/animation/listener/ListenerNotifier;->notifyListenerSet(Ljava/lang/Object;Ljava/util/List;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 187
    :cond_0
    return-void
.end method

.method private static notifyListenerSet(Ljava/lang/Object;Ljava/util/List;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/Object;
    .param p2, "notifier"    # Lmiui/android/animation/listener/ListenerNotifier$INotifier;
    .param p4, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/TransitionListener;",
            ">;",
            "Lmiui/android/animation/listener/ListenerNotifier$INotifier;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ")V"
        }
    .end annotation

    .line 191
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;"
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    const-class v0, Ljava/util/HashSet;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/ObjectPool;->acquire(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 192
    .local v0, "listenerSet":Ljava/util/Set;, "Ljava/util/Set<Lmiui/android/animation/listener/TransitionListener;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/listener/TransitionListener;

    .line 193
    .local v2, "listener":Lmiui/android/animation/listener/TransitionListener;
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 194
    invoke-interface {p2, p0, v2, p3, p4}, Lmiui/android/animation/listener/ListenerNotifier$INotifier;->doNotify(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 196
    .end local v2    # "listener":Lmiui/android/animation/listener/TransitionListener;
    :cond_0
    goto :goto_0

    .line 197
    :cond_1
    invoke-static {v0}, Lmiui/android/animation/utils/ObjectPool;->release(Ljava/lang/Object;)V

    .line 198
    return-void
.end method


# virtual methods
.method public addListeners(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfig;)Z
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "config"    # Lmiui/android/animation/base/AnimConfig;

    .line 132
    iget-object v0, p2, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    return v0

    .line 135
    :cond_0
    invoke-direct {p0, p1}, Lmiui/android/animation/listener/ListenerNotifier;->getListenerSet(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 136
    .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;"
    iget-object v1, p2, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-static {v1, v0}, Lmiui/android/animation/utils/CommonUtils;->addTo(Ljava/util/Collection;Ljava/util/Collection;)V

    .line 137
    const/4 v1, 0x1

    return v1
.end method

.method public notifyBegin(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;

    .line 155
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sBegin:Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 156
    return-void
.end method

.method public notifyCancelAll(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;

    .line 175
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sCancelAll:Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 176
    return-void
.end method

.method public notifyEndAll(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;

    .line 179
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sEndAll:Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 180
    return-void
.end method

.method public notifyMassUpdate(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;

    .line 163
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sMassUpdate:Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 164
    return-void
.end method

.method public notifyPropertyBegin(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 159
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sPropertyBegin:Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 160
    return-void
.end method

.method public notifyPropertyEnd(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 171
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sPropertyEnd:Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 172
    return-void
.end method

.method public notifyUpdate(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 167
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    sget-object v3, Lmiui/android/animation/listener/ListenerNotifier;->sUpdate:Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 168
    return-void
.end method

.method public removeListeners(Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lmiui/android/animation/listener/ListenerNotifier;->mListenerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 142
    .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;"
    invoke-static {v0}, Lmiui/android/animation/utils/ObjectPool;->release(Ljava/lang/Object;)V

    .line 143
    return-void
.end method
