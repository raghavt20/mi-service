class miui.android.animation.listener.ListenerNotifier$MassUpdateNotifier implements miui.android.animation.listener.ListenerNotifier$INotifier {
	 /* .source "ListenerNotifier.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/listener/ListenerNotifier; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "MassUpdateNotifier" */
} // .end annotation
/* # static fields */
static final java.util.List sEmptyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.listener.ListenerNotifier$MassUpdateNotifier ( ) {
/* .locals 1 */
/* .line 48 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
 miui.android.animation.listener.ListenerNotifier$MassUpdateNotifier ( ) {
/* .locals 0 */
/* .line 46 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void doNotify ( java.lang.Object p0, miui.android.animation.listener.TransitionListener p1, java.util.Collection p2, miui.android.animation.listener.UpdateInfo p3 ) {
/* .locals 1 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .param p4, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 53 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v0 = miui.android.animation.listener.ListenerNotifier$MassUpdateNotifier.sEmptyList;
(( miui.android.animation.listener.TransitionListener ) p2 ).onUpdate ( p1, v0 ); // invoke-virtual {p2, p1, v0}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
/* .line 54 */
return;
} // .end method
