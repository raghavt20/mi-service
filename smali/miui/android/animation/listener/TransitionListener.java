public class miui.android.animation.listener.TransitionListener {
	 /* .source "TransitionListener.java" */
	 /* # direct methods */
	 public miui.android.animation.listener.TransitionListener ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 16 */
		 return;
	 } // .end method
	 public miui.android.animation.listener.TransitionListener ( ) {
		 /* .locals 0 */
		 /* .param p1, "fs" # J */
		 /* .annotation runtime Ljava/lang/Deprecated; */
	 } // .end annotation
	 /* .line 22 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 24 */
	 return;
} // .end method
/* # virtual methods */
public void onBegin ( java.lang.Object p0 ) {
	 /* .locals 0 */
	 /* .param p1, "toTag" # Ljava/lang/Object; */
	 /* .line 86 */
	 return;
} // .end method
public void onBegin ( java.lang.Object p0, java.util.Collection p1 ) {
	 /* .locals 0 */
	 /* .param p1, "toTag" # Ljava/lang/Object; */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/lang/Object;", */
	 /* "Ljava/util/Collection<", */
	 /* "Lmiui/android/animation/listener/UpdateInfo;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
/* .line 66 */
/* .local p2, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
return;
} // .end method
public void onBegin ( java.lang.Object p0, miui.android.animation.listener.UpdateInfo p1 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .param p2, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 62 */
return;
} // .end method
public void onCancel ( java.lang.Object p0 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .line 94 */
return;
} // .end method
public void onCancel ( java.lang.Object p0, miui.android.animation.listener.UpdateInfo p1 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .param p2, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 82 */
return;
} // .end method
public void onComplete ( java.lang.Object p0 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .line 90 */
return;
} // .end method
public void onComplete ( java.lang.Object p0, miui.android.animation.listener.UpdateInfo p1 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .param p2, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 74 */
return;
} // .end method
public void onUpdate ( java.lang.Object p0, java.util.Collection p1 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 54 */
/* .local p2, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
return;
} // .end method
public void onUpdate ( java.lang.Object p0, miui.android.animation.property.FloatProperty p1, Float p2, Float p3, Boolean p4 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p3, "value" # F */
/* .param p4, "velocity" # F */
/* .param p5, "isCompleted" # Z */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 41 */
return;
} // .end method
public void onUpdate ( java.lang.Object p0, miui.android.animation.property.FloatProperty p1, Float p2, Boolean p3 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p3, "value" # F */
/* .param p4, "isCompleted" # Z */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 32 */
return;
} // .end method
public void onUpdate ( java.lang.Object p0, miui.android.animation.property.IIntValueProperty p1, Integer p2, Float p3, Boolean p4 ) {
/* .locals 0 */
/* .param p1, "toTag" # Ljava/lang/Object; */
/* .param p2, "property" # Lmiui/android/animation/property/IIntValueProperty; */
/* .param p3, "value" # I */
/* .param p4, "velocity" # F */
/* .param p5, "isCompleted" # Z */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 50 */
return;
} // .end method
