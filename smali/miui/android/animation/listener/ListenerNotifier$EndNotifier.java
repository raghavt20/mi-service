class miui.android.animation.listener.ListenerNotifier$EndNotifier implements miui.android.animation.listener.ListenerNotifier$INotifier {
	 /* .source "ListenerNotifier.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/listener/ListenerNotifier; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "EndNotifier" */
} // .end annotation
/* # direct methods */
 miui.android.animation.listener.ListenerNotifier$EndNotifier ( ) {
/* .locals 0 */
/* .line 113 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void doNotify ( java.lang.Object p0, miui.android.animation.listener.TransitionListener p1, java.util.Collection p2, miui.android.animation.listener.UpdateInfo p3 ) {
/* .locals 0 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .param p4, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 118 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
(( miui.android.animation.listener.TransitionListener ) p2 ).onComplete ( p1 ); // invoke-virtual {p2, p1}, Lmiui/android/animation/listener/TransitionListener;->onComplete(Ljava/lang/Object;)V
/* .line 119 */
return;
} // .end method
