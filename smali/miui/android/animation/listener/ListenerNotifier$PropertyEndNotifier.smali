.class Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;
.super Ljava/lang/Object;
.source "ListenerNotifier.java"

# interfaces
.implements Lmiui/android/animation/listener/ListenerNotifier$INotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/listener/ListenerNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PropertyEndNotifier"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doNotify(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "listener"    # Lmiui/android/animation/listener/TransitionListener;
    .param p4, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/listener/TransitionListener;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ")V"
        }
    .end annotation

    .line 89
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/listener/UpdateInfo;

    .line 90
    .local v1, "info":Lmiui/android/animation/listener/UpdateInfo;
    iget-boolean v2, v1, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-boolean v2, v2, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z

    if-eqz v2, :cond_1

    .line 91
    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z

    .line 92
    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v2, v2, Lmiui/android/animation/internal/AnimInfo;->op:B

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 93
    invoke-virtual {p2, p1, v1}, Lmiui/android/animation/listener/TransitionListener;->onComplete(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V

    goto :goto_1

    .line 95
    :cond_0
    invoke-virtual {p2, p1, v1}, Lmiui/android/animation/listener/TransitionListener;->onCancel(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 98
    .end local v1    # "info":Lmiui/android/animation/listener/UpdateInfo;
    :cond_1
    :goto_1
    goto :goto_0

    .line 99
    :cond_2
    return-void
.end method
