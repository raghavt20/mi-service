public class miui.android.animation.listener.ListenerNotifier {
	 /* .source "ListenerNotifier.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;, */
	 /* Lmiui/android/animation/listener/ListenerNotifier$INotifier; */
	 /* } */
} // .end annotation
/* # static fields */
static final miui.android.animation.listener.ListenerNotifier$BeginNotifier sBegin;
static final miui.android.animation.listener.ListenerNotifier$CancelNotifier sCancelAll;
static final miui.android.animation.listener.ListenerNotifier$EndNotifier sEndAll;
static final miui.android.animation.listener.ListenerNotifier$MassUpdateNotifier sMassUpdate;
static final miui.android.animation.listener.ListenerNotifier$PropertyBeginNotifier sPropertyBegin;
static final miui.android.animation.listener.ListenerNotifier$PropertyEndNotifier sPropertyEnd;
static final miui.android.animation.listener.ListenerNotifier$UpdateNotifier sUpdate;
/* # instance fields */
final java.util.Map mListenerMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final miui.android.animation.IAnimTarget mTarget;
/* # direct methods */
static miui.android.animation.listener.ListenerNotifier ( ) {
/* .locals 1 */
/* .line 34 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$BeginNotifier;-><init>()V */
/* .line 44 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;-><init>()V */
/* .line 56 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;-><init>()V */
/* .line 82 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;-><init>()V */
/* .line 101 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$PropertyEndNotifier;-><init>()V */
/* .line 111 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$CancelNotifier;-><init>()V */
/* .line 121 */
/* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier$EndNotifier; */
/* invoke-direct {v0}, Lmiui/android/animation/listener/ListenerNotifier$EndNotifier;-><init>()V */
return;
} // .end method
public miui.android.animation.listener.ListenerNotifier ( ) {
/* .locals 1 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 127 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 123 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mListenerMap = v0;
/* .line 128 */
this.mTarget = p1;
/* .line 129 */
return;
} // .end method
private java.util.List getListenerSet ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 146 */
v0 = this.mListenerMap;
/* check-cast v0, Ljava/util/List; */
/* .line 147 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;" */
/* if-nez v0, :cond_0 */
/* .line 148 */
/* const-class v1, Ljava/util/ArrayList; */
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
miui.android.animation.utils.ObjectPool .acquire ( v1,v2 );
/* move-object v0, v1 */
/* check-cast v0, Ljava/util/List; */
/* .line 149 */
v1 = this.mListenerMap;
/* .line 151 */
} // :cond_0
} // .end method
private void notify ( java.lang.Object p0, java.lang.Object p1, miui.android.animation.listener.ListenerNotifier$INotifier p2, java.util.Collection p3, miui.android.animation.listener.UpdateInfo p4 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .param p3, "notifier" # Lmiui/android/animation/listener/ListenerNotifier$INotifier; */
/* .param p5, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* "Lmiui/android/animation/listener/ListenerNotifier$INotifier;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 183 */
/* .local p4, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v0 = this.mListenerMap;
/* check-cast v0, Ljava/util/List; */
/* .line 184 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-nez v1, :cond_0 */
/* .line 185 */
miui.android.animation.listener.ListenerNotifier .notifyListenerSet ( p2,v0,p3,p4,p5 );
/* .line 187 */
} // :cond_0
return;
} // .end method
private static void notifyListenerSet ( java.lang.Object p0, java.util.List p1, miui.android.animation.listener.ListenerNotifier$INotifier p2, java.util.Collection p3, miui.android.animation.listener.UpdateInfo p4 ) {
/* .locals 4 */
/* .param p0, "tag" # Ljava/lang/Object; */
/* .param p2, "notifier" # Lmiui/android/animation/listener/ListenerNotifier$INotifier; */
/* .param p4, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* ">;", */
/* "Lmiui/android/animation/listener/ListenerNotifier$INotifier;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 191 */
/* .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;" */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
/* const-class v0, Ljava/util/HashSet; */
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.ObjectPool .acquire ( v0,v1 );
/* check-cast v0, Ljava/util/Set; */
/* .line 192 */
/* .local v0, "listenerSet":Ljava/util/Set;, "Ljava/util/Set<Lmiui/android/animation/listener/TransitionListener;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lmiui/android/animation/listener/TransitionListener; */
/* .line 193 */
v3 = /* .local v2, "listener":Lmiui/android/animation/listener/TransitionListener; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 194 */
/* .line 196 */
} // .end local v2 # "listener":Lmiui/android/animation/listener/TransitionListener;
} // :cond_0
/* .line 197 */
} // :cond_1
miui.android.animation.utils.ObjectPool .release ( v0 );
/* .line 198 */
return;
} // .end method
/* # virtual methods */
public Boolean addListeners ( java.lang.Object p0, miui.android.animation.base.AnimConfig p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .line 132 */
v0 = this.listeners;
v0 = (( java.util.HashSet ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 133 */
int v0 = 0; // const/4 v0, 0x0
/* .line 135 */
} // :cond_0
/* invoke-direct {p0, p1}, Lmiui/android/animation/listener/ListenerNotifier;->getListenerSet(Ljava/lang/Object;)Ljava/util/List; */
/* .line 136 */
/* .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;" */
v1 = this.listeners;
miui.android.animation.utils.CommonUtils .addTo ( v1,v0 );
/* .line 137 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void notifyBegin ( java.lang.Object p0, java.lang.Object p1 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .line 155 */
v3 = miui.android.animation.listener.ListenerNotifier.sBegin;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 156 */
return;
} // .end method
public void notifyCancelAll ( java.lang.Object p0, java.lang.Object p1 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .line 175 */
v3 = miui.android.animation.listener.ListenerNotifier.sCancelAll;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 176 */
return;
} // .end method
public void notifyEndAll ( java.lang.Object p0, java.lang.Object p1 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .line 179 */
v3 = miui.android.animation.listener.ListenerNotifier.sEndAll;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 180 */
return;
} // .end method
public void notifyMassUpdate ( java.lang.Object p0, java.lang.Object p1 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .line 163 */
v3 = miui.android.animation.listener.ListenerNotifier.sMassUpdate;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 164 */
return;
} // .end method
public void notifyPropertyBegin ( java.lang.Object p0, java.lang.Object p1, java.util.Collection p2 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 159 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v3 = miui.android.animation.listener.ListenerNotifier.sPropertyBegin;
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v4, p3 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 160 */
return;
} // .end method
public void notifyPropertyEnd ( java.lang.Object p0, java.lang.Object p1, java.util.Collection p2 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 171 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v3 = miui.android.animation.listener.ListenerNotifier.sPropertyEnd;
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v4, p3 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 172 */
return;
} // .end method
public void notifyUpdate ( java.lang.Object p0, java.lang.Object p1, java.util.Collection p2 ) {
/* .locals 6 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "tag" # Ljava/lang/Object; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Object;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 167 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v3 = miui.android.animation.listener.ListenerNotifier.sUpdate;
int v5 = 0; // const/4 v5, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v4, p3 */
/* invoke-direct/range {v0 ..v5}, Lmiui/android/animation/listener/ListenerNotifier;->notify(Ljava/lang/Object;Ljava/lang/Object;Lmiui/android/animation/listener/ListenerNotifier$INotifier;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V */
/* .line 168 */
return;
} // .end method
public void removeListeners ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 141 */
v0 = this.mListenerMap;
/* check-cast v0, Ljava/util/List; */
/* .line 142 */
/* .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/TransitionListener;>;" */
miui.android.animation.utils.ObjectPool .release ( v0 );
/* .line 143 */
return;
} // .end method
