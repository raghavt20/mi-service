.class Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;
.super Ljava/lang/Object;
.source "ListenerNotifier.java"

# interfaces
.implements Lmiui/android/animation/listener/ListenerNotifier$INotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/listener/ListenerNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MassUpdateNotifier"
.end annotation


# static fields
.field static final sEmptyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;->sEmptyList:Ljava/util/List;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doNotify(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "listener"    # Lmiui/android/animation/listener/TransitionListener;
    .param p4, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/listener/TransitionListener;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ")V"
        }
    .end annotation

    .line 53
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    sget-object v0, Lmiui/android/animation/listener/ListenerNotifier$MassUpdateNotifier;->sEmptyList:Ljava/util/List;

    invoke-virtual {p2, p1, v0}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V

    .line 54
    return-void
.end method
