class miui.android.animation.listener.ListenerNotifier$PropertyEndNotifier implements miui.android.animation.listener.ListenerNotifier$INotifier {
	 /* .source "ListenerNotifier.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/listener/ListenerNotifier; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "PropertyEndNotifier" */
} // .end annotation
/* # direct methods */
 miui.android.animation.listener.ListenerNotifier$PropertyEndNotifier ( ) {
/* .locals 0 */
/* .line 84 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void doNotify ( java.lang.Object p0, miui.android.animation.listener.TransitionListener p1, java.util.Collection p2, miui.android.animation.listener.UpdateInfo p3 ) {
/* .locals 4 */
/* .param p1, "tag" # Ljava/lang/Object; */
/* .param p2, "listener" # Lmiui/android/animation/listener/TransitionListener; */
/* .param p4, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/Object;", */
/* "Lmiui/android/animation/listener/TransitionListener;", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 89 */
/* .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 90 */
/* .local v1, "info":Lmiui/android/animation/listener/UpdateInfo; */
/* iget-boolean v2, v1, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.animInfo;
/* iget-boolean v2, v2, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 91 */
	 v2 = this.animInfo;
	 int v3 = 0; // const/4 v3, 0x0
	 /* iput-boolean v3, v2, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z */
	 /* .line 92 */
	 v2 = this.animInfo;
	 /* iget-byte v2, v2, Lmiui/android/animation/internal/AnimInfo;->op:B */
	 int v3 = 3; // const/4 v3, 0x3
	 /* if-ne v2, v3, :cond_0 */
	 /* .line 93 */
	 (( miui.android.animation.listener.TransitionListener ) p2 ).onComplete ( p1, v1 ); // invoke-virtual {p2, p1, v1}, Lmiui/android/animation/listener/TransitionListener;->onComplete(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V
	 /* .line 95 */
} // :cond_0
(( miui.android.animation.listener.TransitionListener ) p2 ).onCancel ( p1, v1 ); // invoke-virtual {p2, p1, v1}, Lmiui/android/animation/listener/TransitionListener;->onCancel(Ljava/lang/Object;Lmiui/android/animation/listener/UpdateInfo;)V
/* .line 98 */
} // .end local v1 # "info":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_1
} // :goto_1
/* .line 99 */
} // :cond_2
return;
} // .end method
