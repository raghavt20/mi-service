.class Lmiui/android/animation/listener/ListenerNotifier$PropertyBeginNotifier;
.super Ljava/lang/Object;
.source "ListenerNotifier.java"

# interfaces
.implements Lmiui/android/animation/listener/ListenerNotifier$INotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/listener/ListenerNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PropertyBeginNotifier"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doNotify(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "listener"    # Lmiui/android/animation/listener/TransitionListener;
    .param p4, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/listener/TransitionListener;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ")V"
        }
    .end annotation

    .line 41
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-virtual {p2, p1, p3}, Lmiui/android/animation/listener/TransitionListener;->onBegin(Ljava/lang/Object;Ljava/util/Collection;)V

    .line 42
    return-void
.end method
