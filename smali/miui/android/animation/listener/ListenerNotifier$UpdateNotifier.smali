.class Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;
.super Ljava/lang/Object;
.source "ListenerNotifier.java"

# interfaces
.implements Lmiui/android/animation/listener/ListenerNotifier$INotifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/listener/ListenerNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UpdateNotifier"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private notifySingleProperty(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 9
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "listener"    # Lmiui/android/animation/listener/TransitionListener;
    .param p3, "update"    # Lmiui/android/animation/listener/UpdateInfo;

    .line 72
    iget-object v0, p3, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {p3}, Lmiui/android/animation/listener/UpdateInfo;->getFloatValue()F

    move-result v1

    iget-boolean v2, p3, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z

    invoke-virtual {p2, p1, v0, v1, v2}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;FZ)V

    .line 73
    iget-boolean v0, p3, Lmiui/android/animation/listener/UpdateInfo;->useInt:Z

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p3, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    move-object v5, v0

    check-cast v5, Lmiui/android/animation/property/IIntValueProperty;

    invoke-virtual {p3}, Lmiui/android/animation/listener/UpdateInfo;->getIntValue()I

    move-result v6

    iget-wide v0, p3, Lmiui/android/animation/listener/UpdateInfo;->velocity:D

    double-to-float v7, v0

    iget-boolean v8, p3, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z

    move-object v3, p2

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/IIntValueProperty;IFZ)V

    goto :goto_0

    .line 77
    :cond_0
    iget-object v5, p3, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {p3}, Lmiui/android/animation/listener/UpdateInfo;->getFloatValue()F

    move-result v6

    iget-wide v0, p3, Lmiui/android/animation/listener/UpdateInfo;->velocity:D

    double-to-float v7, v0

    iget-boolean v8, p3, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z

    move-object v3, p2

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;FFZ)V

    .line 80
    :goto_0
    return-void
.end method


# virtual methods
.method public doNotify(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Ljava/util/Collection;Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;
    .param p2, "listener"    # Lmiui/android/animation/listener/TransitionListener;
    .param p4, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/listener/TransitionListener;",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ")V"
        }
    .end annotation

    .line 63
    .local p3, "updateList":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    const/16 v1, 0xfa0

    if-gt v0, v1, :cond_0

    .line 64
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/listener/UpdateInfo;

    .line 65
    .local v1, "updateInfo":Lmiui/android/animation/listener/UpdateInfo;
    invoke-direct {p0, p1, p2, v1}, Lmiui/android/animation/listener/ListenerNotifier$UpdateNotifier;->notifySingleProperty(Ljava/lang/Object;Lmiui/android/animation/listener/TransitionListener;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 66
    .end local v1    # "updateInfo":Lmiui/android/animation/listener/UpdateInfo;
    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {p2, p1, p3}, Lmiui/android/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V

    .line 69
    return-void
.end method
