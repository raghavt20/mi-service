.class public Lmiui/android/animation/internal/AnimData;
.super Ljava/lang/Object;
.source "AnimData.java"


# instance fields
.field public delay:J

.field public ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

.field public frameCount:I

.field public initTime:J

.field public isCompleted:Z

.field justEnd:Z

.field logEnabled:Z

.field public op:B

.field public progress:D

.field public property:Lmiui/android/animation/property/FloatProperty;

.field public startTime:J

.field public startValue:D

.field public targetValue:D

.field public tintMode:I

.field public value:D

.field public velocity:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 39
    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    .line 41
    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D

    return-void
.end method


# virtual methods
.method clear()V
    .locals 1

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 96
    iput-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 97
    return-void
.end method

.method from(Lmiui/android/animation/listener/UpdateInfo;Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)V
    .locals 2
    .param p1, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .param p2, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p3, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 59
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    iput-object v0, p0, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 60
    iget-wide v0, p1, Lmiui/android/animation/listener/UpdateInfo;->velocity:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D

    .line 61
    iget v0, p1, Lmiui/android/animation/listener/UpdateInfo;->frameCount:I

    iput v0, p0, Lmiui/android/animation/internal/AnimData;->frameCount:I

    .line 62
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B

    iput-byte v0, p0, Lmiui/android/animation/internal/AnimData;->op:B

    .line 63
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->initTime:J

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->initTime:J

    .line 64
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->startTime:J

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startTime:J

    .line 65
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->progress:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->progress:D

    .line 66
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 67
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    .line 68
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->value:D

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D

    .line 69
    iget-boolean v0, p1, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z

    iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z

    .line 70
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-boolean v0, v0, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z

    iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->justEnd:Z

    .line 72
    invoke-static {p2, p3}, Lmiui/android/animation/internal/AnimConfigUtils;->getTintMode(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)I

    move-result v0

    iput v0, p0, Lmiui/android/animation/internal/AnimData;->tintMode:I

    .line 73
    invoke-static {p2, p3}, Lmiui/android/animation/internal/AnimConfigUtils;->getEase(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 74
    invoke-static {p2, p3}, Lmiui/android/animation/internal/AnimConfigUtils;->getDelay(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->delay:J

    .line 75
    return-void
.end method

.method reset()V
    .locals 1

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z

    .line 49
    iput v0, p0, Lmiui/android/animation/internal/AnimData;->frameCount:I

    .line 50
    iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->justEnd:Z

    .line 51
    return-void
.end method

.method public setOp(B)V
    .locals 1
    .param p1, "op"    # B

    .line 54
    iput-byte p1, p0, Lmiui/android/animation/internal/AnimData;->op:B

    .line 55
    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z

    .line 56
    return-void
.end method

.method to(Lmiui/android/animation/listener/UpdateInfo;)V
    .locals 3
    .param p1, "update"    # Lmiui/android/animation/listener/UpdateInfo;

    .line 78
    iget v0, p0, Lmiui/android/animation/internal/AnimData;->frameCount:I

    iput v0, p1, Lmiui/android/animation/listener/UpdateInfo;->frameCount:I

    .line 79
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v1, p0, Lmiui/android/animation/internal/AnimData;->op:B

    iput-byte v1, v0, Lmiui/android/animation/internal/AnimInfo;->op:B

    .line 80
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->delay:J

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->delay:J

    .line 81
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget v1, p0, Lmiui/android/animation/internal/AnimData;->tintMode:I

    iput v1, v0, Lmiui/android/animation/internal/AnimInfo;->tintMode:I

    .line 82
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->initTime:J

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->initTime:J

    .line 83
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->startTime:J

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->startTime:J

    .line 84
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->progress:D

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->progress:D

    .line 85
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    .line 86
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D

    .line 87
    iget-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z

    iput-boolean v0, p1, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z

    .line 88
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->value:D

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->value:D

    .line 89
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D

    iput-wide v0, p1, Lmiui/android/animation/listener/UpdateInfo;->velocity:D

    .line 90
    iget-object v0, p1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-boolean v1, p0, Lmiui/android/animation/internal/AnimData;->justEnd:Z

    iput-boolean v1, v0, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z

    .line 91
    invoke-virtual {p0}, Lmiui/android/animation/internal/AnimData;->clear()V

    .line 92
    return-void
.end method
