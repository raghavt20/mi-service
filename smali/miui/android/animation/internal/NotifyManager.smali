.class public Lmiui/android/animation/internal/NotifyManager;
.super Ljava/lang/Object;
.source "NotifyManager.java"


# instance fields
.field private mConfig:Lmiui/android/animation/base/AnimConfig;

.field mNotifier:Lmiui/android/animation/listener/ListenerNotifier;

.field mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

.field mTarget:Lmiui/android/animation/IAnimTarget;


# direct methods
.method public constructor <init>(Lmiui/android/animation/IAnimTarget;)V
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/NotifyManager;->mConfig:Lmiui/android/animation/base/AnimConfig;

    .line 26
    iput-object p1, p0, Lmiui/android/animation/internal/NotifyManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    .line 27
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier;

    invoke-direct {v0, p1}, Lmiui/android/animation/listener/ListenerNotifier;-><init>(Lmiui/android/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    .line 28
    new-instance v0, Lmiui/android/animation/listener/ListenerNotifier;

    invoke-direct {v0, p1}, Lmiui/android/animation/listener/ListenerNotifier;-><init>(Lmiui/android/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiui/android/animation/internal/NotifyManager;->mNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    .line 29
    return-void
.end method


# virtual methods
.method public getNotifier()Lmiui/android/animation/listener/ListenerNotifier;
    .locals 1

    .line 53
    iget-object v0, p0, Lmiui/android/animation/internal/NotifyManager;->mNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    return-object v0
.end method

.method public setToNotify(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 3
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p2, "oneTimeConfigs"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 32
    if-nez p2, :cond_0

    .line 33
    return-void

    .line 35
    :cond_0
    invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 36
    .local v0, "tag":Ljava/lang/Object;
    iget-object v1, p0, Lmiui/android/animation/internal/NotifyManager;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V

    .line 37
    iget-object v1, p0, Lmiui/android/animation/internal/NotifyManager;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {p2, v1}, Lmiui/android/animation/base/AnimConfigLink;->addTo(Lmiui/android/animation/base/AnimConfig;)V

    .line 38
    iget-object v1, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v1, v0, v2}, Lmiui/android/animation/listener/ListenerNotifier;->addListeners(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfig;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 39
    iget-object v1, p0, Lmiui/android/animation/internal/NotifyManager;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v1}, Lmiui/android/animation/base/AnimConfig;->clear()V

    .line 40
    return-void

    .line 42
    :cond_1
    iget-object v1, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    invoke-virtual {v1, v0, v0}, Lmiui/android/animation/listener/ListenerNotifier;->notifyBegin(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 43
    iget-object v1, p0, Lmiui/android/animation/internal/NotifyManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v1, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v1, v1, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 44
    .local v1, "updates":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;"
    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    invoke-virtual {v2, v0, v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyBegin(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 45
    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    invoke-virtual {v2, v0, v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->notifyUpdate(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 46
    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    invoke-virtual {v2, v0, v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyEnd(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 47
    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    invoke-virtual {v2, v0, v0}, Lmiui/android/animation/listener/ListenerNotifier;->notifyEndAll(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 48
    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mSetToNotifier:Lmiui/android/animation/listener/ListenerNotifier;

    invoke-virtual {v2, v0}, Lmiui/android/animation/listener/ListenerNotifier;->removeListeners(Ljava/lang/Object;)V

    .line 49
    iget-object v2, p0, Lmiui/android/animation/internal/NotifyManager;->mConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v2}, Lmiui/android/animation/base/AnimConfig;->clear()V

    .line 50
    return-void
.end method
