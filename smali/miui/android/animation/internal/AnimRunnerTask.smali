.class Lmiui/android/animation/internal/AnimRunnerTask;
.super Ljava/lang/Object;
.source "AnimRunnerTask.java"


# static fields
.field static final animDataLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lmiui/android/animation/internal/AnimData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lmiui/android/animation/internal/AnimRunnerTask;->animDataLocal:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static doAnimationFrame(Lmiui/android/animation/internal/AnimTask;JJZZ)V
    .locals 21
    .param p0, "animTask"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "totalT"    # J
    .param p3, "deltaT"    # J
    .param p5, "updateTarget"    # Z
    .param p6, "toPage"    # Z

    .line 28
    move-object/from16 v0, p0

    .line 29
    .local v0, "task":Lmiui/android/animation/internal/AnimTask;
    sget-object v1, Lmiui/android/animation/internal/AnimRunnerTask;->animDataLocal:Ljava/lang/ThreadLocal;

    const-class v2, Lmiui/android/animation/internal/AnimData;

    invoke-static {v1, v2}, Lmiui/android/animation/utils/CommonUtils;->getLocal(Ljava/lang/ThreadLocal;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/AnimData;

    .line 30
    .local v1, "data":Lmiui/android/animation/internal/AnimData;
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v2

    iput-boolean v2, v1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z

    .line 31
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v2

    invoke-virtual {v2}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J

    move-result-wide v11

    .line 32
    .local v11, "averageDelta":J
    :goto_0
    if-eqz v0, :cond_5

    .line 33
    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    const/4 v3, 0x0

    iput v3, v2, Lmiui/android/animation/internal/AnimStats;->updateCount:I

    .line 34
    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    invoke-virtual {v2}, Lmiui/android/animation/internal/AnimStats;->isStarted()Z

    move-result v2

    const/4 v13, 0x1

    xor-int/2addr v2, v13

    move v14, v2

    .line 35
    .local v14, "needSetup":Z
    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v15, v2, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 36
    .local v15, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v2, v2, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    instance-of v10, v2, Lmiui/android/animation/ViewTarget;

    .line 37
    .local v10, "isViewTarget":Z
    iget v2, v0, Lmiui/android/animation/internal/AnimTask;->startPos:I

    .local v2, "idx":I
    invoke-virtual {v0}, Lmiui/android/animation/internal/AnimTask;->getAnimCount()I

    move-result v3

    add-int v8, v2, v3

    move v9, v2

    .end local v2    # "idx":I
    .local v8, "n":I
    .local v9, "idx":I
    :goto_1
    if-ge v9, v8, :cond_4

    .line 38
    invoke-interface {v15, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lmiui/android/animation/listener/UpdateInfo;

    .line 39
    .local v6, "update":Lmiui/android/animation/listener/UpdateInfo;
    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v2, v2, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    iget-object v3, v6, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v3}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmiui/android/animation/base/AnimConfig;->getSpecialConfig(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v7

    .line 40
    .local v7, "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v2, v2, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v1, v6, v2, v7}, Lmiui/android/animation/internal/AnimData;->from(Lmiui/android/animation/listener/UpdateInfo;Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)V

    .line 41
    if-eqz v14, :cond_0

    .line 42
    iget-object v4, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    move-object v2, v0

    move-object v3, v1

    move-object v5, v7

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    .end local v6    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .end local v7    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    .local v16, "update":Lmiui/android/animation/listener/UpdateInfo;
    .local v17, "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    move-wide/from16 v6, p1

    move/from16 v18, v8

    move/from16 v19, v9

    .end local v8    # "n":I
    .end local v9    # "idx":I
    .local v18, "n":I
    .local v19, "idx":I
    move-wide/from16 v8, p3

    invoke-static/range {v2 .. v9}, Lmiui/android/animation/internal/AnimRunnerTask;->setup(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;Lmiui/android/animation/base/AnimSpecialConfig;JJ)V

    goto :goto_2

    .line 41
    .end local v16    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .end local v17    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    .end local v18    # "n":I
    .end local v19    # "idx":I
    .restart local v6    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .restart local v7    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    .restart local v8    # "n":I
    .restart local v9    # "idx":I
    :cond_0
    move-object/from16 v16, v6

    move-object/from16 v17, v7

    move/from16 v18, v8

    move/from16 v19, v9

    .line 44
    .end local v6    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .end local v7    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    .end local v8    # "n":I
    .end local v9    # "idx":I
    .restart local v16    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .restart local v17    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    .restart local v18    # "n":I
    .restart local v19    # "idx":I
    :goto_2
    iget-byte v2, v1, Lmiui/android/animation/internal/AnimData;->op:B

    if-ne v2, v13, :cond_1

    .line 45
    iget-object v4, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    move-object v2, v0

    move-object v3, v1

    move-wide/from16 v5, p1

    move-wide/from16 v7, p3

    invoke-static/range {v2 .. v8}, Lmiui/android/animation/internal/AnimRunnerTask;->startAnim(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;JJ)V

    .line 47
    :cond_1
    iget-byte v2, v1, Lmiui/android/animation/internal/AnimData;->op:B

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 48
    iget-object v4, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    move-object v2, v0

    move-object v3, v1

    move-wide/from16 v5, p1

    move-wide/from16 v7, p3

    move/from16 v20, v10

    .end local v10    # "isViewTarget":Z
    .local v20, "isViewTarget":Z
    move-wide v9, v11

    invoke-static/range {v2 .. v10}, Lmiui/android/animation/internal/AnimRunnerTask;->updateAnimation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;JJJ)V

    goto :goto_3

    .line 47
    .end local v20    # "isViewTarget":Z
    .restart local v10    # "isViewTarget":Z
    :cond_2
    move/from16 v20, v10

    .line 50
    .end local v10    # "isViewTarget":Z
    .restart local v20    # "isViewTarget":Z
    :goto_3
    move-object/from16 v2, v16

    .end local v16    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .local v2, "update":Lmiui/android/animation/listener/UpdateInfo;
    invoke-virtual {v1, v2}, Lmiui/android/animation/internal/AnimData;->to(Lmiui/android/animation/listener/UpdateInfo;)V

    .line 51
    if-eqz p5, :cond_3

    if-eqz p6, :cond_3

    if-nez v20, :cond_3

    iget-wide v3, v1, Lmiui/android/animation/internal/AnimData;->value:D

    invoke-static {v3, v4}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v3

    if-nez v3, :cond_3

    .line 52
    iget-object v3, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v3, v3, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v2, v3}, Lmiui/android/animation/listener/UpdateInfo;->setTargetValue(Lmiui/android/animation/IAnimTarget;)V

    .line 37
    .end local v2    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .end local v17    # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    :cond_3
    add-int/lit8 v9, v19, 0x1

    move/from16 v8, v18

    move/from16 v10, v20

    .end local v19    # "idx":I
    .restart local v9    # "idx":I
    goto/16 :goto_1

    .end local v18    # "n":I
    .end local v20    # "isViewTarget":Z
    .restart local v8    # "n":I
    .restart local v10    # "isViewTarget":Z
    :cond_4
    move/from16 v18, v8

    move/from16 v19, v9

    move/from16 v20, v10

    .line 55
    .end local v8    # "n":I
    .end local v9    # "idx":I
    .end local v10    # "isViewTarget":Z
    .restart local v20    # "isViewTarget":Z
    invoke-virtual {v0}, Lmiui/android/animation/internal/AnimTask;->remove()Lmiui/android/animation/utils/LinkNode;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lmiui/android/animation/internal/AnimTask;

    .line 56
    .end local v14    # "needSetup":Z
    .end local v15    # "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    .end local v20    # "isViewTarget":Z
    goto/16 :goto_0

    .line 57
    :cond_5
    return-void
.end method

.method private static evaluateValue(Lmiui/android/animation/internal/AnimData;F)D
    .locals 5
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p1, "progress"    # F

    .line 242
    iget-object v0, p0, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-static {v0}, Lmiui/android/animation/internal/AnimRunnerTask;->getEvaluator(Lmiui/android/animation/property/FloatProperty;)Landroid/animation/TypeEvaluator;

    move-result-object v0

    .line 243
    .local v0, "evaluator":Landroid/animation/TypeEvaluator;
    instance-of v1, v0, Landroid/animation/IntEvaluator;

    if-eqz v1, :cond_0

    .line 244
    move-object v1, v0

    check-cast v1, Landroid/animation/IntEvaluator;

    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    double-to-int v2, v2

    .line 245
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-wide v3, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    double-to-int v3, v3

    .line 246
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 244
    invoke-virtual {v1, p1, v2, v3}, Landroid/animation/IntEvaluator;->evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    .line 247
    .local v1, "value":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v2

    return-wide v2

    .line 249
    .end local v1    # "value":Ljava/lang/Integer;
    :cond_0
    move-object v1, v0

    check-cast v1, Landroid/animation/FloatEvaluator;

    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    double-to-float v2, v2

    .line 250
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iget-wide v3, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    double-to-float v3, v3

    .line 251
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 249
    invoke-virtual {v1, p1, v2, v3}, Landroid/animation/FloatEvaluator;->evaluate(FLjava/lang/Number;Ljava/lang/Number;)Ljava/lang/Float;

    move-result-object v1

    .line 252
    .local v1, "value":Ljava/lang/Float;
    invoke-virtual {v1}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v2

    return-wide v2
.end method

.method private static finishProperty(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;)V
    .locals 2
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;

    .line 177
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lmiui/android/animation/internal/AnimData;->setOp(B)V

    .line 178
    iget-object v0, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v1, v0, Lmiui/android/animation/internal/AnimStats;->failCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lmiui/android/animation/internal/AnimStats;->failCount:I

    .line 179
    return-void
.end method

.method private static getEvaluator(Lmiui/android/animation/property/FloatProperty;)Landroid/animation/TypeEvaluator;
    .locals 1
    .param p0, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 257
    sget-object v0, Lmiui/android/animation/property/ViewPropertyExt;->BACKGROUND:Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;

    if-ne p0, v0, :cond_0

    instance-of v0, p0, Lmiui/android/animation/property/ColorProperty;

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lmiui/android/animation/utils/CommonUtils;->sArgbEvaluator:Landroid/animation/ArgbEvaluator;

    return-object v0

    .line 259
    :cond_0
    instance-of v0, p0, Lmiui/android/animation/property/IIntValueProperty;

    if-eqz v0, :cond_1

    .line 260
    new-instance v0, Landroid/animation/IntEvaluator;

    invoke-direct {v0}, Landroid/animation/IntEvaluator;-><init>()V

    return-object v0

    .line 262
    :cond_1
    new-instance v0, Landroid/animation/FloatEvaluator;

    invoke-direct {v0}, Landroid/animation/FloatEvaluator;-><init>()V

    return-object v0
.end method

.method private static initAnimation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;JJ)Z
    .locals 7
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p2, "totalT"    # J
    .param p4, "deltaT"    # J

    .line 129
    invoke-static {p1}, Lmiui/android/animation/internal/AnimRunnerTask;->setValues(Lmiui/android/animation/internal/AnimData;)Z

    move-result v0

    const-string v1, ", value = "

    const-string v2, ", property = "

    const-string v3, "miuix_anim"

    const/4 v4, 0x0

    if-nez v0, :cond_1

    .line 130
    iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z

    if-eqz v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StartTask, set start value failed, break, tag = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v5, v5, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 133
    invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", start value = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", target value = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->targetValue:D

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->value:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v3, v0}, Lmiui/android/animation/utils/LogUtils;->logThread(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    invoke-static {p0, p1}, Lmiui/android/animation/internal/AnimRunnerTask;->finishProperty(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;)V

    .line 140
    return v4

    .line 142
    :cond_1
    invoke-static {p1}, Lmiui/android/animation/internal/AnimRunnerTask;->isValueInvalid(Lmiui/android/animation/internal/AnimData;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 143
    iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z

    if-eqz v0, :cond_2

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StartTask, values invalid, break, tag = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v5, v5, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 146
    invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", startValue = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", targetValue = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->targetValue:D

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->value:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", velocity = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->velocity:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-static {v3, v0}, Lmiui/android/animation/utils/LogUtils;->logThread(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_2
    invoke-virtual {p1}, Lmiui/android/animation/internal/AnimData;->reset()V

    .line 154
    invoke-static {p0, p1}, Lmiui/android/animation/internal/AnimRunnerTask;->finishProperty(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;)V

    .line 155
    return v4

    .line 157
    :cond_3
    sub-long v0, p2, p4

    iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startTime:J

    .line 158
    iput v4, p1, Lmiui/android/animation/internal/AnimData;->frameCount:I

    .line 159
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmiui/android/animation/internal/AnimData;->setOp(B)V

    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method private static isValueInvalid(Lmiui/android/animation/internal/AnimData;)Z
    .locals 4
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;

    .line 182
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D

    .line 183
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4030aaaaa0000000L    # 16.66666603088379

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 182
    :goto_0
    return v0
.end method

.method private static regulateProgress(F)F
    .locals 2
    .param p0, "progress"    # F

    .line 233
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p0, v0

    if-lez v1, :cond_0

    .line 234
    return v0

    .line 235
    :cond_0
    const/4 v0, 0x0

    cmpg-float v1, p0, v0

    if-gez v1, :cond_1

    .line 236
    return v0

    .line 238
    :cond_1
    return p0
.end method

.method private static setStartData(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;)V
    .locals 3
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;

    .line 110
    const-wide/16 v0, 0x0

    iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->progress:D

    .line 111
    invoke-virtual {p1}, Lmiui/android/animation/internal/AnimData;->reset()V

    .line 112
    iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+++++ start anim, target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v1, v1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tag = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v1, v1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", property = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 115
    invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", op = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p1, Lmiui/android/animation/internal/AnimData;->op:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ease = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", delay = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->delay:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", start value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", target value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->targetValue:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->value:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", progress = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->progress:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", velocity = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->velocity:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 113
    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    :cond_0
    return-void
.end method

.method private static setValues(Lmiui/android/animation/internal/AnimData;)Z
    .locals 4
    .param p0, "data"    # Lmiui/android/animation/internal/AnimData;

    .line 164
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D

    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 165
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-static {v2, v3}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 168
    :cond_0
    return v1

    .line 169
    :cond_1
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-static {v2, v3}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-nez v0, :cond_2

    .line 170
    iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D

    iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D

    .line 171
    return v1

    .line 173
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method static setup(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;Lmiui/android/animation/base/AnimSpecialConfig;JJ)V
    .locals 5
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p2, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p3, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;
    .param p4, "totalT"    # J
    .param p6, "deltaT"    # J

    .line 61
    iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 63
    .local v0, "startValue":D
    iget-object v2, p2, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v3, p1, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-static {v2, v3, v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->getValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v2

    iput-wide v2, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 65
    .end local v0    # "startValue":D
    :cond_0
    sub-long v0, p4, p6

    iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->initTime:J

    .line 66
    iget-object v0, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v1, v0, Lmiui/android/animation/internal/AnimStats;->initCount:I

    const/4 v2, 0x1

    add-int/2addr v1, v2

    iput v1, v0, Lmiui/android/animation/internal/AnimStats;->initCount:I

    .line 67
    iget-byte v0, p1, Lmiui/android/animation/internal/AnimData;->op:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->delay:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-lez v0, :cond_1

    goto :goto_0

    .line 74
    :cond_1
    sub-long v0, p4, p6

    iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startTime:J

    .line 75
    iput-wide v3, p1, Lmiui/android/animation/internal/AnimData;->delay:J

    .line 76
    iget-object v0, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I

    sub-int/2addr v1, v2

    iput v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I

    .line 77
    invoke-static {p0, p1}, Lmiui/android/animation/internal/AnimRunnerTask;->setStartData(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;)V

    goto :goto_1

    .line 68
    :cond_2
    :goto_0
    invoke-virtual {p1, v2}, Lmiui/android/animation/internal/AnimData;->setOp(B)V

    .line 69
    iget-object v0, p2, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-static {v0, p3}, Lmiui/android/animation/internal/AnimConfigUtils;->getFromSpeed(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)F

    move-result v0

    .line 70
    .local v0, "fromSpeed":F
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    .line 71
    float-to-double v1, v0

    iput-wide v1, p1, Lmiui/android/animation/internal/AnimData;->velocity:D

    .line 73
    .end local v0    # "fromSpeed":F
    :cond_3
    nop

    .line 79
    :goto_1
    return-void
.end method

.method static startAnim(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;JJ)V
    .locals 6
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p2, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p3, "totalT"    # J
    .param p5, "deltaT"    # J

    .line 83
    iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->delay:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 84
    iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StartTask, tag = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v1, v1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", property = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 86
    invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", delay = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->delay:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", initTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->initTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", totalT = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    .line 85
    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    :cond_0
    iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->initTime:J

    iget-wide v2, p1, Lmiui/android/animation/internal/AnimData;->delay:J

    add-long/2addr v0, v2

    cmp-long v0, p3, v0

    if-gez v0, :cond_1

    .line 93
    return-void

    .line 95
    :cond_1
    iget-object v0, p2, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v1, p1, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v0, v1, v2, v3}, Lmiui/android/animation/internal/AnimValueUtils;->getValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v0

    .line 97
    .local v0, "startValue":D
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_2

    .line 98
    iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 102
    .end local v0    # "startValue":D
    :cond_2
    iget-object v0, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I

    .line 103
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    move-wide v4, p5

    invoke-static/range {v0 .. v5}, Lmiui/android/animation/internal/AnimRunnerTask;->initAnimation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;JJ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 104
    return-void

    .line 106
    :cond_3
    invoke-static {p0, p1}, Lmiui/android/animation/internal/AnimRunnerTask;->setStartData(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;)V

    .line 107
    return-void
.end method

.method private static updateAnimation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;JJJ)V
    .locals 16
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "data"    # Lmiui/android/animation/internal/AnimData;
    .param p2, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p3, "totalT"    # J
    .param p5, "deltaT"    # J
    .param p7, "averageDelta"    # J

    .line 188
    move-object/from16 v0, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    iget-object v1, v0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v2, v1, Lmiui/android/animation/internal/AnimStats;->updateCount:I

    const/4 v11, 0x1

    add-int/2addr v2, v11

    iput v2, v1, Lmiui/android/animation/internal/AnimStats;->updateCount:I

    .line 189
    iget v1, v9, Lmiui/android/animation/internal/AnimData;->frameCount:I

    add-int/2addr v1, v11

    iput v1, v9, Lmiui/android/animation/internal/AnimData;->frameCount:I

    .line 190
    iget-object v1, v9, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    sget-object v2, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    if-eq v1, v2, :cond_1

    iget-object v1, v9, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    sget-object v2, Lmiui/android/animation/property/ViewPropertyExt;->BACKGROUND:Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;

    if-eq v1, v2, :cond_1

    iget-object v1, v9, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    instance-of v1, v1, Lmiui/android/animation/property/ColorProperty;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 206
    :cond_0
    iget-object v1, v10, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-static/range {v1 .. v8}, Lmiui/android/animation/styles/PropertyStyle;->doAnimationFrame(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V

    .line 207
    iget-object v1, v9, Lmiui/android/animation/internal/AnimData;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iget v1, v1, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I

    invoke-static {v1}, Lmiui/android/animation/utils/EaseManager;->isPhysicsStyle(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 208
    iget-wide v1, v9, Lmiui/android/animation/internal/AnimData;->progress:D

    double-to-float v1, v1

    invoke-static {v9, v1}, Lmiui/android/animation/internal/AnimRunnerTask;->evaluateValue(Lmiui/android/animation/internal/AnimData;F)D

    move-result-wide v1

    iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->value:D

    goto :goto_1

    .line 193
    :cond_1
    :goto_0
    iget-wide v12, v9, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 194
    .local v12, "startValue":D
    iget-wide v14, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D

    .line 195
    .local v14, "targetValue":D
    const-wide/16 v1, 0x0

    iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 196
    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D

    .line 197
    iget-wide v1, v9, Lmiui/android/animation/internal/AnimData;->progress:D

    iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->value:D

    .line 198
    iget-object v1, v10, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-static/range {v1 .. v8}, Lmiui/android/animation/styles/PropertyStyle;->doAnimationFrame(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V

    .line 199
    iget-wide v1, v9, Lmiui/android/animation/internal/AnimData;->value:D

    double-to-float v1, v1

    invoke-static {v1}, Lmiui/android/animation/internal/AnimRunnerTask;->regulateProgress(F)F

    move-result v1

    float-to-double v1, v1

    iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->progress:D

    .line 200
    iput-wide v12, v9, Lmiui/android/animation/internal/AnimData;->startValue:D

    .line 201
    iput-wide v14, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D

    .line 202
    sget-object v1, Lmiui/android/animation/utils/CommonUtils;->sArgbEvaluator:Landroid/animation/ArgbEvaluator;

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->progress:D

    double-to-float v2, v2

    iget-wide v3, v9, Lmiui/android/animation/internal/AnimData;->startValue:D

    double-to-int v3, v3

    .line 203
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-wide v4, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 204
    .local v1, "animValue":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v2

    iput-wide v2, v9, Lmiui/android/animation/internal/AnimData;->value:D

    .line 205
    .end local v1    # "animValue":Ljava/lang/Integer;
    .end local v12    # "startValue":D
    .end local v14    # "targetValue":D
    nop

    .line 211
    :cond_2
    :goto_1
    iget-byte v1, v9, Lmiui/android/animation/internal/AnimData;->op:B

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 212
    iput-boolean v11, v9, Lmiui/android/animation/internal/AnimData;->justEnd:Z

    .line 213
    iget-object v1, v0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I

    add-int/2addr v2, v11

    iput v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I

    .line 215
    :cond_3
    iget-boolean v1, v9, Lmiui/android/animation/internal/AnimData;->logEnabled:Z

    if-eqz v1, :cond_4

    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "----- update anim, target = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v2, v2, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    iget-object v2, v2, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", property = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v9, Lmiui/android/animation/internal/AnimData;->property:Lmiui/android/animation/property/FloatProperty;

    .line 218
    invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", op = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, v9, Lmiui/android/animation/internal/AnimData;->op:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", init time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->initTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", start time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->startTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", start value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->startValue:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", target value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->value:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", progress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->progress:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", velocity = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->velocity:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", delta = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-wide/from16 v2, p5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    .line 216
    invoke-static {v1, v4}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 215
    :cond_4
    move-wide/from16 v2, p5

    .line 230
    :goto_2
    return-void
.end method
