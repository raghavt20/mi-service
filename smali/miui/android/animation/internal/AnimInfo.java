public class miui.android.animation.internal.AnimInfo {
	 /* .source "AnimInfo.java" */
	 /* # instance fields */
	 public volatile Long delay;
	 public volatile Long initTime;
	 public volatile Boolean justEnd;
	 public volatile Object op;
	 public volatile Double progress;
	 public volatile Double setToValue;
	 public volatile Long startTime;
	 public volatile Double startValue;
	 public volatile Double targetValue;
	 public volatile Integer tintMode;
	 public volatile Double value;
	 /* # direct methods */
	 public miui.android.animation.internal.AnimInfo ( ) {
		 /* .locals 2 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 14 */
		 /* const-wide v0, 0x7fefffffffffffffL # Double.MAX_VALUE */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
		 /* .line 15 */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
		 /* .line 16 */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimInfo;->value:D */
		 /* .line 17 */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void clear ( ) {
		 /* .locals 3 */
		 /* .line 22 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-byte v0, p0, Lmiui/android/animation/internal/AnimInfo;->op:B */
		 /* .line 23 */
		 /* const-wide/16 v1, 0x0 */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->delay:J */
		 /* .line 24 */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->initTime:J */
		 /* .line 25 */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->startTime:J */
		 /* .line 26 */
		 /* const-wide/16 v1, 0x0 */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->progress:D */
		 /* .line 27 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimInfo;->tintMode:I */
		 /* .line 28 */
		 /* const-wide v1, 0x7fefffffffffffffL # Double.MAX_VALUE */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
		 /* .line 29 */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
		 /* .line 30 */
		 /* iput-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->value:D */
		 /* .line 31 */
		 /* iput-boolean v0, p0, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z */
		 /* .line 32 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 3 */
		 /* .line 36 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "AnimInfo{op="; // const-string v1, "AnimInfo{op="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-byte v1, p0, Lmiui/android/animation/internal/AnimInfo;->op:B */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", delay = "; // const-string v1, ", delay = "
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->delay:J */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v1 = ", initTime="; // const-string v1, ", initTime="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->initTime:J */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v1 = ", startTime="; // const-string v1, ", startTime="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->startTime:J */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v1 = ", progress="; // const-string v1, ", progress="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->progress:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 final String v1 = ", config="; // const-string v1, ", config="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lmiui/android/animation/internal/AnimInfo;->tintMode:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", startValue="; // const-string v1, ", startValue="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 final String v1 = ", targetValue="; // const-string v1, ", targetValue="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 final String v1 = ", value="; // const-string v1, ", value="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->value:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 final String v1 = ", setToValue="; // const-string v1, ", setToValue="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v1, p0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
		 (( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
