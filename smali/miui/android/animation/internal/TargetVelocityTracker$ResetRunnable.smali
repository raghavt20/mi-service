.class Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;
.super Ljava/lang/Object;
.source "TargetVelocityTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/internal/TargetVelocityTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResetRunnable"
.end annotation


# instance fields
.field mMonitorInfo:Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

.field mProperty:Lmiui/android/animation/property/FloatProperty;

.field mTargetRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiui/android/animation/IAnimTarget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;)V
    .locals 0
    .param p1, "monitorInfo"    # Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mMonitorInfo:Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

    .line 30
    return-void
.end method


# virtual methods
.method post(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)V
    .locals 3
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 33
    iget-object v0, p1, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    invoke-virtual {v0, p0}, Lmiui/android/animation/internal/TargetHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 34
    iget-object v0, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mTargetRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    .line 35
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mTargetRef:Ljava/lang/ref/WeakReference;

    .line 37
    :cond_1
    iput-object p2, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mProperty:Lmiui/android/animation/property/FloatProperty;

    .line 38
    iget-object v0, p1, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    const-wide/16 v1, 0x258

    invoke-virtual {v0, p0, v1, v2}, Lmiui/android/animation/internal/TargetHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 39
    return-void
.end method

.method public run()V
    .locals 4

    .line 43
    iget-object v0, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mTargetRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/IAnimTarget;

    .line 44
    .local v0, "target":Lmiui/android/animation/IAnimTarget;
    if-eqz v0, :cond_1

    .line 45
    iget-object v1, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mProperty:Lmiui/android/animation/property/FloatProperty;

    filled-new-array {v1}, [Lmiui/android/animation/property/FloatProperty;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    iget-object v1, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mProperty:Lmiui/android/animation/property/FloatProperty;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/IAnimTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V

    .line 48
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->mMonitorInfo:Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

    iget-object v1, v1, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;->monitor:Lmiui/android/animation/utils/VelocityMonitor;

    invoke-virtual {v1}, Lmiui/android/animation/utils/VelocityMonitor;->clear()V

    .line 50
    :cond_1
    return-void
.end method
