class miui.android.animation.internal.AnimStats implements miui.android.animation.utils.ObjectPool$IPoolObject {
	 /* .source "AnimStats.java" */
	 /* # interfaces */
	 /* # instance fields */
	 public Integer animCount;
	 public Integer cancelCount;
	 public Integer endCount;
	 public Integer failCount;
	 public Integer initCount;
	 public Integer startCount;
	 public Integer updateCount;
	 /* # direct methods */
	 miui.android.animation.internal.AnimStats ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public void add ( miui.android.animation.internal.AnimStats p0 ) {
		 /* .locals 2 */
		 /* .param p1, "stats" # Lmiui/android/animation/internal/AnimStats; */
		 /* .line 17 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->animCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
		 /* .line 18 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->startCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
		 /* .line 19 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->initCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
		 /* .line 20 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->failCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
		 /* .line 21 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
		 /* .line 22 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
		 /* .line 23 */
		 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->endCount:I */
		 /* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
		 /* add-int/2addr v0, v1 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->endCount:I */
		 /* .line 24 */
		 return;
	 } // .end method
	 public void clear ( ) {
		 /* .locals 1 */
		 /* .line 36 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
		 /* .line 37 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
		 /* .line 38 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
		 /* .line 39 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
		 /* .line 40 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
		 /* .line 41 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
		 /* .line 42 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimStats;->endCount:I */
		 /* .line 43 */
		 return;
	 } // .end method
	 public Boolean isRunning ( ) {
		 /* .locals 2 */
		 /* .line 31 */
		 v0 = 		 (( miui.android.animation.internal.AnimStats ) p0 ).isStarted ( ); // invoke-virtual {p0}, Lmiui/android/animation/internal/AnimStats;->isStarted()Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
			 /* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->endCount:I */
			 /* add-int/2addr v0, v1 */
			 /* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
			 /* add-int/2addr v0, v1 */
			 /* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
			 /* if-ge v0, v1, :cond_0 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean isStarted ( ) {
/* .locals 1 */
/* .line 27 */
/* iget v0, p0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 47 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AnimStats{animCount = "; // const-string v1, "AnimStats{animCount = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", startCount="; // const-string v1, ", startCount="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", startedCount = "; // const-string v1, ", startedCount = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", failCount="; // const-string v1, ", failCount="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", updateCount="; // const-string v1, ", updateCount="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", cancelCount="; // const-string v1, ", cancelCount="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", endCount="; // const-string v1, ", endCount="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/internal/AnimStats;->endCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
