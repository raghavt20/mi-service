public class miui.android.animation.internal.ThreadPoolUtil {
	 /* .source "ThreadPoolUtil.java" */
	 /* # static fields */
	 private static final Integer CPU_COUNT;
	 private static final Integer KEEP_ALIVE;
	 private static final Integer KEEP_POOL_SIZE;
	 public static final Integer MAX_SPLIT_COUNT;
	 private static final java.util.concurrent.ThreadPoolExecutor sCacheThread;
	 private static final java.util.concurrent.Executor sSingleThread;
	 /* # direct methods */
	 static miui.android.animation.internal.ThreadPoolUtil ( ) {
		 /* .locals 11 */
		 /* .line 19 */
		 java.lang.Runtime .getRuntime ( );
		 v0 = 		 (( java.lang.Runtime ) v0 ).availableProcessors ( ); // invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I
		 /* .line 21 */
		 /* mul-int/lit8 v1, v0, 0x2 */
		 /* add-int/lit8 v1, v1, 0x1 */
		 /* .line 23 */
		 int v2 = 4; // const/4 v2, 0x4
		 /* if-ge v0, v2, :cond_0 */
		 int v0 = 0; // const/4 v0, 0x0
	 } // :cond_0
	 /* div-int/lit8 v0, v0, 0x2 */
	 /* add-int/lit8 v0, v0, 0x1 */
} // :goto_0
/* move v3, v0 */
/* .line 27 */
/* new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor; */
/* add-int/lit8 v4, v1, 0x3 */
/* const-wide/16 v5, 0x1e */
v7 = java.util.concurrent.TimeUnit.SECONDS;
/* new-instance v8, Ljava/util/concurrent/SynchronousQueue; */
/* invoke-direct {v8}, Ljava/util/concurrent/SynchronousQueue;-><init>()V */
/* .line 33 */
final String v1 = "AnimThread"; // const-string v1, "AnimThread"
miui.android.animation.internal.ThreadPoolUtil .getThreadFactory ( v1 );
/* new-instance v10, Lmiui/android/animation/internal/ThreadPoolUtil$1; */
/* invoke-direct {v10}, Lmiui/android/animation/internal/ThreadPoolUtil$1;-><init>()V */
/* move-object v2, v0 */
/* invoke-direct/range {v2 ..v10}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V */
/* .line 55 */
/* nop */
/* .line 56 */
final String v0 = "WorkThread"; // const-string v0, "WorkThread"
miui.android.animation.internal.ThreadPoolUtil .getThreadFactory ( v0 );
java.util.concurrent.Executors .newSingleThreadExecutor ( v0 );
/* .line 55 */
return;
} // .end method
public miui.android.animation.internal.ThreadPoolUtil ( ) {
/* .locals 0 */
/* .line 17 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static java.util.concurrent.Executor access$000 ( ) { //synthethic
/* .locals 1 */
/* .line 17 */
v0 = miui.android.animation.internal.ThreadPoolUtil.sSingleThread;
} // .end method
public static void getSplitCount ( Integer p0, Integer[] p1 ) {
/* .locals 4 */
/* .param p0, "animCount" # I */
/* .param p1, "splitInfo" # [I */
/* .line 63 */
/* div-int/lit16 v0, p0, 0xfa0 */
/* .line 64 */
/* .local v0, "splitCount":I */
int v1 = 1; // const/4 v1, 0x1
v0 = java.lang.Math .max ( v0,v1 );
/* .line 65 */
/* if-le v0, v2, :cond_0 */
/* .line 66 */
/* .line 68 */
} // :cond_0
/* int-to-float v2, p0 */
/* int-to-float v3, v0 */
/* div-float/2addr v2, v3 */
/* float-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* .line 69 */
/* .local v2, "singleCount":I */
int v3 = 0; // const/4 v3, 0x0
/* aput v0, p1, v3 */
/* .line 70 */
/* aput v2, p1, v1 */
/* .line 71 */
return;
} // .end method
private static java.util.concurrent.ThreadFactory getThreadFactory ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "factoryName" # Ljava/lang/String; */
/* .line 42 */
/* new-instance v0, Lmiui/android/animation/internal/ThreadPoolUtil$2; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/internal/ThreadPoolUtil$2;-><init>(Ljava/lang/String;)V */
} // .end method
public static void post ( java.lang.Runnable p0 ) {
/* .locals 1 */
/* .param p0, "task" # Ljava/lang/Runnable; */
/* .line 59 */
v0 = miui.android.animation.internal.ThreadPoolUtil.sCacheThread;
(( java.util.concurrent.ThreadPoolExecutor ) v0 ).execute ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V
/* .line 60 */
return;
} // .end method
