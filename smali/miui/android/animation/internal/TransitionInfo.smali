.class Lmiui/android/animation/internal/TransitionInfo;
.super Lmiui/android/animation/utils/LinkNode;
.source "TransitionInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/android/animation/utils/LinkNode<",
        "Lmiui/android/animation/internal/TransitionInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final sIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

.field public static final sMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public animTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/AnimTask;",
            ">;"
        }
    .end annotation
.end field

.field public volatile config:Lmiui/android/animation/base/AnimConfig;

.field public volatile from:Lmiui/android/animation/controller/AnimState;

.field public final id:I

.field public volatile key:Ljava/lang/Object;

.field private final mAnimStats:Lmiui/android/animation/internal/AnimStats;

.field public volatile startTime:J

.field public final tag:Ljava/lang/Object;

.field public final target:Lmiui/android/animation/IAnimTarget;

.field public volatile to:Lmiui/android/animation/controller/AnimState;

.field public volatile updateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    .line 25
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiui/android/animation/internal/TransitionInfo;->sIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 3
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "f"    # Lmiui/android/animation/controller/AnimState;
    .param p3, "t"    # Lmiui/android/animation/controller/AnimState;
    .param p4, "c"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 61
    invoke-direct {p0}, Lmiui/android/animation/utils/LinkNode;-><init>()V

    .line 31
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lmiui/android/animation/internal/TransitionInfo;->id:I

    .line 38
    new-instance v1, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    .line 48
    new-instance v1, Lmiui/android/animation/internal/AnimStats;

    invoke-direct {v1}, Lmiui/android/animation/internal/AnimStats;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->mAnimStats:Lmiui/android/animation/internal/AnimStats;

    .line 62
    iput-object p1, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    .line 63
    invoke-direct {p0, p2}, Lmiui/android/animation/internal/TransitionInfo;->getState(Lmiui/android/animation/controller/AnimState;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->from:Lmiui/android/animation/controller/AnimState;

    .line 64
    invoke-direct {p0, p3}, Lmiui/android/animation/internal/TransitionInfo;->getState(Lmiui/android/animation/controller/AnimState;)Lmiui/android/animation/controller/AnimState;

    move-result-object v1

    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    .line 65
    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v1}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    .line 66
    iget-boolean v2, p3, Lmiui/android/animation/controller/AnimState;->isTemporary:Z

    if-eqz v2, :cond_0

    .line 67
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    goto :goto_0

    .line 69
    :cond_0
    iput-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    .line 71
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 72
    invoke-direct {p0}, Lmiui/android/animation/internal/TransitionInfo;->initValueForColorProperty()V

    .line 73
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {p3}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V

    .line 74
    if-eqz p4, :cond_1

    .line 75
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {p4, v0}, Lmiui/android/animation/base/AnimConfigLink;->addTo(Lmiui/android/animation/base/AnimConfig;)V

    .line 77
    :cond_1
    return-void
.end method

.method static decreaseStartCountForDelayAnim(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;B)V
    .locals 5
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "stats"    # Lmiui/android/animation/internal/AnimStats;
    .param p2, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .param p3, "op"    # B

    .line 52
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    iget-object v1, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->delay:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v1, v1, Lmiui/android/animation/internal/AnimStats;->startCount:I

    if-lez v1, :cond_0

    .line 56
    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v2, v1, Lmiui/android/animation/internal/AnimStats;->startCount:I

    sub-int/2addr v2, v0

    iput v2, v1, Lmiui/android/animation/internal/AnimStats;->startCount:I

    .line 57
    iget v1, p1, Lmiui/android/animation/internal/AnimStats;->startCount:I

    sub-int/2addr v1, v0

    iput v1, p1, Lmiui/android/animation/internal/AnimStats;->startCount:I

    .line 59
    :cond_0
    return-void
.end method

.method private getState(Lmiui/android/animation/controller/AnimState;)Lmiui/android/animation/controller/AnimState;
    .locals 1
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;

    .line 106
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lmiui/android/animation/controller/AnimState;->isTemporary:Z

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Lmiui/android/animation/controller/AnimState;

    invoke-direct {v0}, Lmiui/android/animation/controller/AnimState;-><init>()V

    .line 108
    .local v0, "s":Lmiui/android/animation/controller/AnimState;
    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/AnimState;->set(Lmiui/android/animation/controller/AnimState;)V

    .line 109
    return-object v0

    .line 111
    .end local v0    # "s":Lmiui/android/animation/controller/AnimState;
    :cond_0
    return-object p1
.end method

.method private initValueForColorProperty()V
    .locals 10

    .line 124
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->from:Lmiui/android/animation/controller/AnimState;

    if-nez v0, :cond_0

    .line 125
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 128
    .local v1, "key":Ljava/lang/Object;
    iget-object v2, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v2, v1}, Lmiui/android/animation/controller/AnimState;->getTempProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v2

    .line 129
    .local v2, "property":Lmiui/android/animation/property/FloatProperty;
    instance-of v3, v2, Lmiui/android/animation/property/ColorProperty;

    if-nez v3, :cond_1

    .line 130
    goto :goto_0

    .line 132
    :cond_1
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v3, v2, v4, v5}, Lmiui/android/animation/internal/AnimValueUtils;->getValueOfTarget(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v3

    .line 133
    .local v3, "curValue":D
    invoke-static {v3, v4}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v5

    if-nez v5, :cond_2

    .line 134
    goto :goto_0

    .line 136
    :cond_2
    iget-object v5, p0, Lmiui/android/animation/internal/TransitionInfo;->from:Lmiui/android/animation/controller/AnimState;

    iget-object v6, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v5, v6, v2}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v5

    .line 137
    .local v5, "fv":D
    invoke-static {v5, v6}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v7

    if-nez v7, :cond_3

    .line 138
    iget-object v7, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    move-object v8, v2

    check-cast v8, Lmiui/android/animation/property/ColorProperty;

    double-to-int v9, v5

    invoke-virtual {v7, v8, v9}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V

    .line 140
    .end local v1    # "key":Ljava/lang/Object;
    .end local v2    # "property":Lmiui/android/animation/property/FloatProperty;
    .end local v3    # "curValue":D
    .end local v5    # "fv":D
    :cond_3
    goto :goto_0

    .line 141
    :cond_4
    return-void
.end method


# virtual methods
.method public containsProperty(Lmiui/android/animation/property/FloatProperty;)Z
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 120
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v0, p1}, Lmiui/android/animation/controller/AnimState;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAnimCount()I
    .locals 1

    .line 116
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public getAnimStats()Lmiui/android/animation/internal/AnimStats;
    .locals 4

    .line 185
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->mAnimStats:Lmiui/android/animation/internal/AnimStats;

    invoke-virtual {v0}, Lmiui/android/animation/internal/AnimStats;->clear()V

    .line 186
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/AnimTask;

    .line 187
    .local v1, "task":Lmiui/android/animation/internal/AnimTask;
    iget-object v2, p0, Lmiui/android/animation/internal/TransitionInfo;->mAnimStats:Lmiui/android/animation/internal/AnimStats;

    iget-object v3, v1, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimStats;->add(Lmiui/android/animation/internal/AnimStats;)V

    .line 188
    .end local v1    # "task":Lmiui/android/animation/internal/AnimTask;
    goto :goto_0

    .line 189
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->mAnimStats:Lmiui/android/animation/internal/AnimStats;

    return-object v0
.end method

.method public initUpdateList(Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;)V
    .locals 17
    .param p1, "creator"    # Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;

    .line 144
    move-object/from16 v0, p0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    iput-wide v1, v0, Lmiui/android/animation/internal/TransitionInfo;->startTime:J

    .line 145
    iget-object v1, v0, Lmiui/android/animation/internal/TransitionInfo;->from:Lmiui/android/animation/controller/AnimState;

    .line 146
    .local v1, "f":Lmiui/android/animation/controller/AnimState;
    iget-object v2, v0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    .line 147
    .local v2, "t":Lmiui/android/animation/controller/AnimState;
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v3

    .line 148
    .local v3, "logEnabled":Z
    const/4 v4, 0x0

    const-string v5, "-- doSetup, target = "

    if-eqz v3, :cond_0

    .line 149
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", key = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", f = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", t = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\nconfig = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v4, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v6, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-virtual {v2}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 157
    .local v8, "key":Ljava/lang/Object;
    invoke-virtual {v2, v8}, Lmiui/android/animation/controller/AnimState;->getProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v9

    .line 158
    .local v9, "property":Lmiui/android/animation/property/FloatProperty;
    move-object/from16 v10, p1

    invoke-interface {v10, v9}, Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;->getUpdateInfo(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;

    move-result-object v11

    .line 159
    .local v11, "update":Lmiui/android/animation/listener/UpdateInfo;
    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v12, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-object v13, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v2, v13, v9}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v13

    iput-wide v13, v12, Lmiui/android/animation/internal/AnimInfo;->targetValue:D

    .line 161
    if-eqz v1, :cond_1

    .line 162
    iget-object v12, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-object v13, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v1, v13, v9}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v13

    iput-wide v13, v12, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    goto :goto_1

    .line 164
    :cond_1
    iget-object v12, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    .line 165
    .local v12, "startValue":D
    iget-object v14, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-static {v14, v9, v12, v13}, Lmiui/android/animation/internal/AnimValueUtils;->getValueOfTarget(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v14

    .line 167
    .local v14, "curValue":D
    invoke-static {v14, v15}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v16

    if-nez v16, :cond_2

    .line 168
    iget-object v4, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iput-wide v14, v4, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    .line 171
    .end local v12    # "startValue":D
    .end local v14    # "curValue":D
    :cond_2
    :goto_1
    invoke-static {v11}, Lmiui/android/animation/internal/AnimValueUtils;->handleSetToValue(Lmiui/android/animation/listener/UpdateInfo;)Z

    .line 172
    if-eqz v3, :cond_3

    .line 173
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v12, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v12, ", property = "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 174
    invoke-virtual {v9}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v12, ", startValue = "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v12, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->startValue:D

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v12, ", targetValue = "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v12, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->targetValue:D

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v12, ", value = "

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v12, v11, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->value:D

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v12, 0x0

    new-array v13, v12, [Ljava/lang/Object;

    .line 173
    invoke-static {v4, v13}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 172
    :cond_3
    const/4 v12, 0x0

    .line 180
    .end local v8    # "key":Ljava/lang/Object;
    .end local v9    # "property":Lmiui/android/animation/property/FloatProperty;
    .end local v11    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :goto_2
    move v4, v12

    goto/16 :goto_0

    .line 181
    :cond_4
    move-object/from16 v10, p1

    iput-object v6, v0, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 182
    return-void
.end method

.method public setupTasks(Z)V
    .locals 8
    .param p1, "isInit"    # Z

    .line 80
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 81
    .local v0, "animCount":I
    div-int/lit16 v1, v0, 0xfa0

    .line 82
    .local v1, "splitCount":I
    const/4 v2, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 83
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 84
    .local v2, "singleCount":I
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v1, :cond_0

    .line 85
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto :goto_1

    .line 87
    :cond_0
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 88
    iget-object v4, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    new-instance v5, Lmiui/android/animation/internal/AnimTask;

    invoke-direct {v5}, Lmiui/android/animation/internal/AnimTask;-><init>()V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 91
    .end local v3    # "i":I
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 92
    .local v3, "startPos":I
    iget-object v4, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/android/animation/internal/AnimTask;

    .line 93
    .local v5, "task":Lmiui/android/animation/internal/AnimTask;
    iput-object p0, v5, Lmiui/android/animation/internal/AnimTask;->info:Lmiui/android/animation/internal/TransitionInfo;

    .line 94
    add-int v6, v3, v2

    if-le v6, v0, :cond_2

    sub-int v6, v0, v3

    goto :goto_3

    :cond_2
    move v6, v2

    .line 95
    .local v6, "amount":I
    :goto_3
    invoke-virtual {v5, v3, v6}, Lmiui/android/animation/internal/AnimTask;->setup(II)V

    .line 96
    if-eqz p1, :cond_3

    .line 97
    iget-object v7, v5, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iput v6, v7, Lmiui/android/animation/internal/AnimStats;->startCount:I

    goto :goto_4

    .line 99
    :cond_3
    invoke-virtual {v5}, Lmiui/android/animation/internal/AnimTask;->updateAnimStats()V

    .line 101
    :goto_4
    add-int/2addr v3, v6

    .line 102
    .end local v5    # "task":Lmiui/android/animation/internal/AnimTask;
    .end local v6    # "amount":I
    goto :goto_2

    .line 103
    :cond_4
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransitionInfo{target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    if-eqz v1, :cond_0

    .line 195
    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", propSize = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    .line 197
    invoke-virtual {v1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", next = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->next:Lmiui/android/animation/utils/LinkNode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    return-object v0
.end method
