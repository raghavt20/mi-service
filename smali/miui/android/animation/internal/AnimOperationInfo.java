class miui.android.animation.internal.AnimOperationInfo {
	 /* .source "AnimOperationInfo.java" */
	 /* # instance fields */
	 public final Object op;
	 public final java.util.List propList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lmiui/android/animation/property/FloatProperty;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public volatile Long sendTime;
public final miui.android.animation.IAnimTarget target;
public Integer usedCount;
/* # direct methods */
 miui.android.animation.internal.AnimOperationInfo ( ) {
/* .locals 6 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "op" # B */
/* .param p3, "names" # [Ljava/lang/String; */
/* .param p4, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I */
/* .line 22 */
/* iput-byte p2, p0, Lmiui/android/animation/internal/AnimOperationInfo;->op:B */
/* .line 23 */
this.target = p1;
/* .line 24 */
if ( p3 != null) { // if-eqz p3, :cond_1
	 /* instance-of v1, p1, Lmiui/android/animation/ValueTarget; */
	 if ( v1 != null) { // if-eqz v1, :cond_1
		 /* .line 25 */
		 /* move-object v1, p1 */
		 /* check-cast v1, Lmiui/android/animation/ValueTarget; */
		 /* .line 26 */
		 /* .local v1, "vt":Lmiui/android/animation/ValueTarget; */
		 /* new-instance v2, Ljava/util/ArrayList; */
		 /* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
		 this.propList = v2;
		 /* .line 27 */
		 /* array-length v2, p3 */
	 } // :goto_0
	 /* if-ge v0, v2, :cond_0 */
	 /* aget-object v3, p3, v0 */
	 /* .line 28 */
	 /* .local v3, "name":Ljava/lang/String; */
	 v4 = this.propList;
	 (( miui.android.animation.ValueTarget ) v1 ).getFloatProperty ( v3 ); // invoke-virtual {v1, v3}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;
	 /* .line 27 */
} // .end local v3 # "name":Ljava/lang/String;
/* add-int/lit8 v0, v0, 0x1 */
/* .line 30 */
} // .end local v1 # "vt":Lmiui/android/animation/ValueTarget;
} // :cond_0
} // :cond_1
if ( p4 != null) { // if-eqz p4, :cond_2
/* .line 31 */
java.util.Arrays .asList ( p4 );
this.propList = v0;
/* .line 33 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
this.propList = v0;
/* .line 35 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
Boolean isUsed ( ) {
/* .locals 4 */
/* .line 38 */
v0 = this.propList;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* move v0, v1 */
v0 = } // :cond_0
/* .line 39 */
/* .local v0, "size":I */
} // :goto_0
int v2 = 1; // const/4 v2, 0x1
/* iget v3, p0, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I */
/* if-nez v0, :cond_1 */
/* if-lez v3, :cond_2 */
} // :cond_1
/* if-ne v3, v0, :cond_2 */
} // :goto_1
/* move v1, v2 */
} // :cond_2
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 44 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AnimOperationInfo{target="; // const-string v1, "AnimOperationInfo{target="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.target;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", op="; // const-string v1, ", op="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v1, p0, Lmiui/android/animation/internal/AnimOperationInfo;->op:B */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", propList="; // const-string v1, ", propList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.propList;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 47 */
java.util.Arrays .toString ( v1 );
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 44 */
} // .end method
