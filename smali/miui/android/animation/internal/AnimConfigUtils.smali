.class public Lmiui/android/animation/internal/AnimConfigUtils;
.super Ljava/lang/Object;
.source "AnimConfigUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static chooseSpeed(FF)F
    .locals 2
    .param p0, "speed1"    # F
    .param p1, "speed2"    # F

    .line 40
    float-to-double v0, p0

    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    return p1

    .line 42
    :cond_0
    float-to-double v0, p1

    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    return p0

    .line 45
    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static getDelay(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)J
    .locals 4
    .param p0, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p1, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 25
    iget-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->delay:J

    if-eqz p1, :cond_0

    iget-wide v2, p1, Lmiui/android/animation/base/AnimSpecialConfig;->delay:J

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static getEase(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .locals 2
    .param p0, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p1, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 16
    if-eqz p1, :cond_0

    iget-object v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    sget-object v1, Lmiui/android/animation/base/AnimConfig;->sDefEase:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    if-eq v0, v1, :cond_0

    .line 17
    iget-object v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .local v0, "ease":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    goto :goto_0

    .line 19
    .end local v0    # "ease":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 21
    .restart local v0    # "ease":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    :goto_0
    if-nez v0, :cond_1

    sget-object v1, Lmiui/android/animation/base/AnimConfig;->sDefEase:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    goto :goto_1

    :cond_1
    move-object v1, v0

    :goto_1
    return-object v1
.end method

.method static getFromSpeed(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)F
    .locals 2
    .param p0, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p1, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 33
    if-eqz p1, :cond_0

    iget v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->fromSpeed:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->fromSpeed:F

    return v0

    .line 36
    :cond_0
    iget v0, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    return v0
.end method

.method static getTintMode(Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)I
    .locals 2
    .param p0, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p1, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 29
    iget v0, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    if-eqz p1, :cond_0

    iget v1, p1, Lmiui/android/animation/base/AnimSpecialConfig;->tintMode:I

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
