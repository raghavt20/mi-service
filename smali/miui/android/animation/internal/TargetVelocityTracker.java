public class miui.android.animation.internal.TargetVelocityTracker {
	 /* .source "TargetVelocityTracker.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;, */
	 /* Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
	 /* } */
} // .end annotation
/* # instance fields */
private java.util.Map mMonitors;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/property/FloatProperty;", */
/* "Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.internal.TargetVelocityTracker ( ) {
/* .locals 1 */
/* .line 15 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 53 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mMonitors = v0;
return;
} // .end method
private miui.android.animation.internal.TargetVelocityTracker$MonitorInfo getMonitor ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 69 */
v0 = this.mMonitors;
/* check-cast v0, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
/* .line 70 */
/* .local v0, "info":Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
/* if-nez v0, :cond_0 */
/* .line 71 */
/* new-instance v1, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, v2}, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;-><init>(Lmiui/android/animation/internal/TargetVelocityTracker$1;)V */
/* move-object v0, v1 */
/* .line 72 */
v1 = this.mMonitors;
/* .line 74 */
} // :cond_0
} // .end method
/* # virtual methods */
public void trackVelocity ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Double p2 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p3, "value" # D */
/* .line 59 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/internal/TargetVelocityTracker;->getMonitor(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
/* .line 60 */
/* .local v0, "info":Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
v1 = this.monitor;
int v2 = 1; // const/4 v2, 0x1
/* new-array v2, v2, [D */
int v3 = 0; // const/4 v3, 0x0
/* aput-wide p3, v2, v3 */
(( miui.android.animation.utils.VelocityMonitor ) v1 ).update ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/animation/utils/VelocityMonitor;->update([D)V
/* .line 61 */
v1 = this.monitor;
v1 = (( miui.android.animation.utils.VelocityMonitor ) v1 ).getVelocity ( v3 ); // invoke-virtual {v1, v3}, Lmiui/android/animation/utils/VelocityMonitor;->getVelocity(I)F
/* .line 62 */
/* .local v1, "velocity":F */
int v2 = 0; // const/4 v2, 0x0
/* cmpl-float v2, v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 63 */
v2 = this.resetTask;
(( miui.android.animation.internal.TargetVelocityTracker$ResetRunnable ) v2 ).post ( p1, p2 ); // invoke-virtual {v2, p1, p2}, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->post(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)V
/* .line 64 */
/* float-to-double v2, v1 */
(( miui.android.animation.IAnimTarget ) p1 ).setVelocity ( p2, v2, v3 ); // invoke-virtual {p1, p2, v2, v3}, Lmiui/android/animation/IAnimTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V
/* .line 66 */
} // :cond_0
return;
} // .end method
