.class public Lmiui/android/animation/internal/AnimManager;
.super Ljava/lang/Object;
.source "AnimManager.java"

# interfaces
.implements Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;


# instance fields
.field final mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Object;",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mStartAnim:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mTarget:Lmiui/android/animation/IAnimTarget;

.field private mUpdateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;"
        }
    .end annotation
.end field

.field final mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lmiui/android/animation/property/FloatProperty;",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpdateTask:Ljava/lang/Runnable;

.field final mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mStartAnim:Ljava/util/Set;

    .line 30
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    .line 33
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 37
    new-instance v0, Lmiui/android/animation/internal/AnimManager$1;

    invoke-direct {v0, p0}, Lmiui/android/animation/internal/AnimManager$1;-><init>(Lmiui/android/animation/internal/AnimManager;)V

    iput-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateTask:Ljava/lang/Runnable;

    return-void
.end method

.method private varargs containProperties(Lmiui/android/animation/internal/TransitionInfo;[Lmiui/android/animation/property/FloatProperty;)Z
    .locals 5
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 93
    array-length v0, p2

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p2, v2

    .line 94
    .local v3, "property":Lmiui/android/animation/property/FloatProperty;
    invoke-virtual {p1, v3}, Lmiui/android/animation/internal/TransitionInfo;->containsProperty(Lmiui/android/animation/property/FloatProperty;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    const/4 v0, 0x1

    return v0

    .line 93
    .end local v3    # "property":Lmiui/android/animation/property/FloatProperty;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 98
    :cond_1
    return v1
.end method

.method private pendState(Lmiui/android/animation/internal/TransitionInfo;)Z
    .locals 4
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 159
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    iget-wide v0, v0, Lmiui/android/animation/controller/AnimState;->flags:J

    const-wide/16 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lmiui/android/animation/utils/CommonUtils;->hasFlags(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 161
    const/4 v0, 0x1

    return v0

    .line 163
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private removeSameAnim(Lmiui/android/animation/internal/TransitionInfo;)V
    .locals 7
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 120
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/TransitionInfo;

    .line 121
    .local v1, "runInfo":Lmiui/android/animation/internal/TransitionInfo;
    if-ne v1, p1, :cond_0

    .line 122
    goto :goto_0

    .line 124
    :cond_0
    iget-object v2, v1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 125
    .local v2, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    iget-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    if-nez v3, :cond_1

    .line 126
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    .line 128
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/android/animation/listener/UpdateInfo;

    .line 129
    .local v4, "update":Lmiui/android/animation/listener/UpdateInfo;
    iget-object v5, p1, Lmiui/android/animation/internal/TransitionInfo;->to:Lmiui/android/animation/controller/AnimState;

    iget-object v6, v4, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v5, v6}, Lmiui/android/animation/controller/AnimState;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 130
    iget-object v5, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    .end local v4    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :cond_2
    goto :goto_1

    .line 133
    :cond_3
    iget-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 134
    const/4 v3, 0x5

    const/4 v4, 0x4

    invoke-virtual {p0, v1, v3, v4}, Lmiui/android/animation/internal/AnimManager;->notifyTransitionEnd(Lmiui/android/animation/internal/TransitionInfo;II)V

    goto :goto_2

    .line 135
    :cond_4
    iget-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iget-object v4, v1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eq v3, v4, :cond_5

    .line 136
    iget-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    iput-object v3, v1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 137
    const/4 v3, 0x0

    iput-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    .line 138
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lmiui/android/animation/internal/TransitionInfo;->setupTasks(Z)V

    goto :goto_2

    .line 140
    :cond_5
    iget-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 142
    .end local v1    # "runInfo":Lmiui/android/animation/internal/TransitionInfo;
    .end local v2    # "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    :goto_2
    goto :goto_0

    .line 143
    :cond_6
    return-void
.end method

.method private setTargetValue(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 9
    .param p1, "to"    # Lmiui/android/animation/controller/AnimState;
    .param p2, "config"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 178
    invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 179
    .local v1, "key":Ljava/lang/Object;
    invoke-virtual {p1, v1}, Lmiui/android/animation/controller/AnimState;->getTempProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v2

    .line 180
    .local v2, "property":Lmiui/android/animation/property/FloatProperty;
    iget-object v3, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {p1, v3, v2}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v3

    .line 181
    .local v3, "value":D
    iget-object v5, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v5, v5, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v5, v5, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/android/animation/listener/UpdateInfo;

    .line 182
    .local v5, "update":Lmiui/android/animation/listener/UpdateInfo;
    if-eqz v5, :cond_0

    .line 183
    iget-object v6, v5, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iput-wide v3, v6, Lmiui/android/animation/internal/AnimInfo;->setToValue:D

    .line 185
    :cond_0
    instance-of v6, v2, Lmiui/android/animation/property/IIntValueProperty;

    if-eqz v6, :cond_1

    .line 186
    iget-object v6, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    move-object v7, v2

    check-cast v7, Lmiui/android/animation/property/IIntValueProperty;

    double-to-int v8, v3

    invoke-virtual {v6, v7, v8}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V

    goto :goto_1

    .line 188
    :cond_1
    iget-object v6, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    double-to-float v7, v3

    invoke-virtual {v6, v2, v7}, Lmiui/android/animation/IAnimTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V

    .line 190
    :goto_1
    iget-object v6, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v6, v2, v3, v4}, Lmiui/android/animation/IAnimTarget;->trackVelocity(Lmiui/android/animation/property/FloatProperty;D)V

    .line 191
    .end local v1    # "key":Ljava/lang/Object;
    .end local v2    # "property":Lmiui/android/animation/property/FloatProperty;
    .end local v3    # "value":D
    .end local v5    # "update":Lmiui/android/animation/listener/UpdateInfo;
    goto :goto_0

    .line 192
    :cond_2
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/IAnimTarget;->setToNotify(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 193
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 65
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mStartAnim:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 66
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 67
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 68
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 69
    return-void
.end method

.method public getTotalAnimCount()I
    .locals 4

    .line 72
    const/4 v0, 0x0

    .line 73
    .local v0, "count":I
    iget-object v1, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/internal/TransitionInfo;

    .line 74
    .local v2, "info":Lmiui/android/animation/internal/TransitionInfo;
    invoke-virtual {v2}, Lmiui/android/animation/internal/TransitionInfo;->getAnimCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 75
    .end local v2    # "info":Lmiui/android/animation/internal/TransitionInfo;
    goto :goto_0

    .line 76
    :cond_0
    return v0
.end method

.method getTransitionInfos(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;)V"
        }
    .end annotation

    .line 57
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/internal/TransitionInfo;>;"
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/TransitionInfo;

    .line 58
    .local v1, "info":Lmiui/android/animation/internal/TransitionInfo;
    iget-object v2, v1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    .end local v1    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_0
    goto :goto_0

    .line 62
    :cond_1
    return-void
.end method

.method public getUpdateInfo(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 204
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/listener/UpdateInfo;

    .line 205
    .local v0, "update":Lmiui/android/animation/listener/UpdateInfo;
    if-nez v0, :cond_0

    .line 206
    new-instance v1, Lmiui/android/animation/listener/UpdateInfo;

    invoke-direct {v1, p1}, Lmiui/android/animation/listener/UpdateInfo;-><init>(Lmiui/android/animation/property/FloatProperty;)V

    move-object v0, v1

    .line 207
    iget-object v1, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/listener/UpdateInfo;

    .line 208
    .local v1, "prev":Lmiui/android/animation/listener/UpdateInfo;
    if-eqz v1, :cond_0

    .line 209
    move-object v0, v1

    .line 212
    .end local v1    # "prev":Lmiui/android/animation/listener/UpdateInfo;
    :cond_0
    return-object v0
.end method

.method public getVelocity(Lmiui/android/animation/property/FloatProperty;)D
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 196
    invoke-virtual {p0, p1}, Lmiui/android/animation/internal/AnimManager;->getUpdateInfo(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;

    move-result-object v0

    iget-wide v0, v0, Lmiui/android/animation/listener/UpdateInfo;->velocity:D

    return-wide v0
.end method

.method public varargs isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
    .locals 4
    .param p1, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 80
    invoke-static {p1}, Lmiui/android/animation/utils/CommonUtils;->isArrayEmpty([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    .line 81
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    :cond_0
    return v1

    .line 84
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/internal/TransitionInfo;

    .line 85
    .local v2, "info":Lmiui/android/animation/internal/TransitionInfo;
    invoke-direct {p0, v2, p1}, Lmiui/android/animation/internal/AnimManager;->containProperties(Lmiui/android/animation/internal/TransitionInfo;[Lmiui/android/animation/property/FloatProperty;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 86
    return v1

    .line 88
    .end local v2    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_2
    goto :goto_0

    .line 89
    :cond_3
    const/4 v0, 0x0

    return v0
.end method

.method notifyTransitionEnd(Lmiui/android/animation/internal/TransitionInfo;II)V
    .locals 2
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p2, "msg"    # I
    .param p3, "reason"    # I

    .line 146
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mStartAnim:Ljava/util/Set;

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v1, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v0, v0, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    iget v1, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I

    .line 150
    invoke-virtual {v0, p2, v1, p3}, Lmiui/android/animation/internal/TargetHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 153
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {p0, v0}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 156
    :cond_1
    return-void
.end method

.method public runUpdate()V
    .locals 2

    .line 49
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v1, p0, Lmiui/android/animation/internal/AnimManager;->mUpdateTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->post(Ljava/lang/Runnable;)V

    .line 50
    return-void
.end method

.method public setTarget(Lmiui/android/animation/IAnimTarget;)V
    .locals 0
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 53
    iput-object p1, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    .line 54
    return-void
.end method

.method public setTo(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 3
    .param p1, "to"    # Lmiui/android/animation/controller/AnimState;
    .param p2, "config"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 167
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setTo, target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "to = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    :cond_0
    invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v1, 0x96

    if-le v0, v1, :cond_1

    .line 171
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    iget-object v1, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1, p1}, Lmiui/android/animation/internal/RunnerHandler;->addSetToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;)V

    goto :goto_0

    .line 173
    :cond_1
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/internal/AnimManager;->setTargetValue(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 175
    :goto_0
    return-void
.end method

.method public setVelocity(Lmiui/android/animation/property/FloatProperty;F)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "velocity"    # F

    .line 200
    invoke-virtual {p0, p1}, Lmiui/android/animation/internal/AnimManager;->getUpdateInfo(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;

    move-result-object v0

    float-to-double v1, p2

    iput-wide v1, v0, Lmiui/android/animation/listener/UpdateInfo;->velocity:D

    .line 201
    return-void
.end method

.method setupTransition(Lmiui/android/animation/internal/TransitionInfo;)V
    .locals 2
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 113
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mRunningInfo:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    invoke-virtual {p1, p0}, Lmiui/android/animation/internal/TransitionInfo;->initUpdateList(Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;)V

    .line 115
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmiui/android/animation/internal/TransitionInfo;->setupTasks(Z)V

    .line 116
    invoke-direct {p0, p1}, Lmiui/android/animation/internal/AnimManager;->removeSameAnim(Lmiui/android/animation/internal/TransitionInfo;)V

    .line 117
    return-void
.end method

.method public startAnim(Lmiui/android/animation/internal/TransitionInfo;)V
    .locals 4
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 102
    invoke-direct {p0, p1}, Lmiui/android/animation/internal/AnimManager;->pendState(Lmiui/android/animation/internal/TransitionInfo;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".startAnim, pendState"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    return-void

    .line 106
    :cond_0
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v2, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    iget v2, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I

    .line 108
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v2, v1}, Lmiui/android/animation/internal/RunnerHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 110
    return-void
.end method

.method public update(Z)V
    .locals 1
    .param p1, "toPage"    # Z

    .line 45
    iget-object v0, p0, Lmiui/android/animation/internal/AnimManager;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v0, v0, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    invoke-virtual {v0, p1}, Lmiui/android/animation/internal/TargetHandler;->update(Z)V

    .line 46
    return-void
.end method
