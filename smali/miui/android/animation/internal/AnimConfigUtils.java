public class miui.android.animation.internal.AnimConfigUtils {
	 /* .source "AnimConfigUtils.java" */
	 /* # direct methods */
	 private miui.android.animation.internal.AnimConfigUtils ( ) {
		 /* .locals 0 */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Float chooseSpeed ( Float p0, Float p1 ) {
		 /* .locals 2 */
		 /* .param p0, "speed1" # F */
		 /* .param p1, "speed2" # F */
		 /* .line 40 */
		 /* float-to-double v0, p0 */
		 v0 = 		 miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 41 */
			 /* .line 42 */
		 } // :cond_0
		 /* float-to-double v0, p1 */
		 v0 = 		 miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 43 */
			 /* .line 45 */
		 } // :cond_1
		 v0 = 		 java.lang.Math .max ( p0,p1 );
	 } // .end method
	 static Long getDelay ( miui.android.animation.base.AnimConfig p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
		 /* .locals 4 */
		 /* .param p0, "config" # Lmiui/android/animation/base/AnimConfig; */
		 /* .param p1, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
		 /* .line 25 */
		 /* iget-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->delay:J */
		 if ( p1 != null) { // if-eqz p1, :cond_0
			 /* iget-wide v2, p1, Lmiui/android/animation/base/AnimSpecialConfig;->delay:J */
		 } // :cond_0
		 /* const-wide/16 v2, 0x0 */
	 } // :goto_0
	 java.lang.Math .max ( v0,v1,v2,v3 );
	 /* move-result-wide v0 */
	 /* return-wide v0 */
} // .end method
static miui.android.animation.utils.EaseManager$EaseStyle getEase ( miui.android.animation.base.AnimConfig p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
	 /* .locals 2 */
	 /* .param p0, "config" # Lmiui/android/animation/base/AnimConfig; */
	 /* .param p1, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
	 /* .line 16 */
	 if ( p1 != null) { // if-eqz p1, :cond_0
		 v0 = this.ease;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 v0 = this.ease;
			 v1 = miui.android.animation.base.AnimConfig.sDefEase;
			 /* if-eq v0, v1, :cond_0 */
			 /* .line 17 */
			 v0 = this.ease;
			 /* .local v0, "ease":Lmiui/android/animation/utils/EaseManager$EaseStyle; */
			 /* .line 19 */
		 } // .end local v0 # "ease":Lmiui/android/animation/utils/EaseManager$EaseStyle;
	 } // :cond_0
	 v0 = this.ease;
	 /* .line 21 */
	 /* .restart local v0 # "ease":Lmiui/android/animation/utils/EaseManager$EaseStyle; */
} // :goto_0
/* if-nez v0, :cond_1 */
v1 = miui.android.animation.base.AnimConfig.sDefEase;
} // :cond_1
/* move-object v1, v0 */
} // :goto_1
} // .end method
static Float getFromSpeed ( miui.android.animation.base.AnimConfig p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
/* .locals 2 */
/* .param p0, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .param p1, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 33 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->fromSpeed:F */
/* float-to-double v0, v0 */
v0 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 34 */
/* iget v0, p1, Lmiui/android/animation/base/AnimSpecialConfig;->fromSpeed:F */
/* .line 36 */
} // :cond_0
/* iget v0, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
} // .end method
static Integer getTintMode ( miui.android.animation.base.AnimConfig p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
/* .locals 2 */
/* .param p0, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .param p1, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 29 */
/* iget v0, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget v1, p1, Lmiui/android/animation/base/AnimSpecialConfig;->tintMode:I */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
v0 = java.lang.Math .max ( v0,v1 );
} // .end method
