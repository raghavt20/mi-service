class miui.android.animation.internal.AnimRunnerTask {
	 /* .source "AnimRunnerTask.java" */
	 /* # static fields */
	 static final java.lang.ThreadLocal animDataLocal;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/ThreadLocal<", */
	 /* "Lmiui/android/animation/internal/AnimData;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.internal.AnimRunnerTask ( ) {
/* .locals 1 */
/* .line 25 */
/* new-instance v0, Ljava/lang/ThreadLocal; */
/* invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V */
return;
} // .end method
 miui.android.animation.internal.AnimRunnerTask ( ) {
/* .locals 0 */
/* .line 23 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static void doAnimationFrame ( miui.android.animation.internal.AnimTask p0, Long p1, Long p2, Boolean p3, Boolean p4 ) {
/* .locals 21 */
/* .param p0, "animTask" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "totalT" # J */
/* .param p3, "deltaT" # J */
/* .param p5, "updateTarget" # Z */
/* .param p6, "toPage" # Z */
/* .line 28 */
/* move-object/from16 v0, p0 */
/* .line 29 */
/* .local v0, "task":Lmiui/android/animation/internal/AnimTask; */
v1 = miui.android.animation.internal.AnimRunnerTask.animDataLocal;
/* const-class v2, Lmiui/android/animation/internal/AnimData; */
miui.android.animation.utils.CommonUtils .getLocal ( v1,v2 );
/* check-cast v1, Lmiui/android/animation/internal/AnimData; */
/* .line 30 */
/* .local v1, "data":Lmiui/android/animation/internal/AnimData; */
v2 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
/* iput-boolean v2, v1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z */
/* .line 31 */
miui.android.animation.internal.AnimRunner .getInst ( );
(( miui.android.animation.internal.AnimRunner ) v2 ).getAverageDelta ( ); // invoke-virtual {v2}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J
/* move-result-wide v11 */
/* .line 32 */
/* .local v11, "averageDelta":J */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 33 */
v2 = this.animStats;
int v3 = 0; // const/4 v3, 0x0
/* iput v3, v2, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
/* .line 34 */
v2 = this.animStats;
v2 = (( miui.android.animation.internal.AnimStats ) v2 ).isStarted ( ); // invoke-virtual {v2}, Lmiui/android/animation/internal/AnimStats;->isStarted()Z
int v13 = 1; // const/4 v13, 0x1
/* xor-int/2addr v2, v13 */
/* move v14, v2 */
/* .line 35 */
/* .local v14, "needSetup":Z */
v2 = this.info;
v15 = this.updateList;
/* .line 36 */
/* .local v15, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;" */
v2 = this.info;
v2 = this.target;
/* instance-of v10, v2, Lmiui/android/animation/ViewTarget; */
/* .line 37 */
/* .local v10, "isViewTarget":Z */
/* iget v2, v0, Lmiui/android/animation/internal/AnimTask;->startPos:I */
/* .local v2, "idx":I */
v3 = (( miui.android.animation.internal.AnimTask ) v0 ).getAnimCount ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimTask;->getAnimCount()I
/* add-int v8, v2, v3 */
/* move v9, v2 */
} // .end local v2 # "idx":I
/* .local v8, "n":I */
/* .local v9, "idx":I */
} // :goto_1
/* if-ge v9, v8, :cond_4 */
/* .line 38 */
/* move-object v6, v2 */
/* check-cast v6, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 39 */
/* .local v6, "update":Lmiui/android/animation/listener/UpdateInfo; */
v2 = this.info;
v2 = this.config;
v3 = this.property;
(( miui.android.animation.property.FloatProperty ) v3 ).getName ( ); // invoke-virtual {v3}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( miui.android.animation.base.AnimConfig ) v2 ).getSpecialConfig ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/base/AnimConfig;->getSpecialConfig(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
/* .line 40 */
/* .local v7, "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
v2 = this.info;
v2 = this.config;
(( miui.android.animation.internal.AnimData ) v1 ).from ( v6, v2, v7 ); // invoke-virtual {v1, v6, v2, v7}, Lmiui/android/animation/internal/AnimData;->from(Lmiui/android/animation/listener/UpdateInfo;Lmiui/android/animation/base/AnimConfig;Lmiui/android/animation/base/AnimSpecialConfig;)V
/* .line 41 */
if ( v14 != null) { // if-eqz v14, :cond_0
/* .line 42 */
v4 = this.info;
/* move-object v2, v0 */
/* move-object v3, v1 */
/* move-object v5, v7 */
/* move-object/from16 v16, v6 */
/* move-object/from16 v17, v7 */
} // .end local v6 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // .end local v7 # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
/* .local v16, "update":Lmiui/android/animation/listener/UpdateInfo; */
/* .local v17, "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* move-wide/from16 v6, p1 */
/* move/from16 v18, v8 */
/* move/from16 v19, v9 */
} // .end local v8 # "n":I
} // .end local v9 # "idx":I
/* .local v18, "n":I */
/* .local v19, "idx":I */
/* move-wide/from16 v8, p3 */
/* invoke-static/range {v2 ..v9}, Lmiui/android/animation/internal/AnimRunnerTask;->setup(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;Lmiui/android/animation/base/AnimSpecialConfig;JJ)V */
/* .line 41 */
} // .end local v16 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // .end local v17 # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
} // .end local v18 # "n":I
} // .end local v19 # "idx":I
/* .restart local v6 # "update":Lmiui/android/animation/listener/UpdateInfo; */
/* .restart local v7 # "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* .restart local v8 # "n":I */
/* .restart local v9 # "idx":I */
} // :cond_0
/* move-object/from16 v16, v6 */
/* move-object/from16 v17, v7 */
/* move/from16 v18, v8 */
/* move/from16 v19, v9 */
/* .line 44 */
} // .end local v6 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // .end local v7 # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
} // .end local v8 # "n":I
} // .end local v9 # "idx":I
/* .restart local v16 # "update":Lmiui/android/animation/listener/UpdateInfo; */
/* .restart local v17 # "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* .restart local v18 # "n":I */
/* .restart local v19 # "idx":I */
} // :goto_2
/* iget-byte v2, v1, Lmiui/android/animation/internal/AnimData;->op:B */
/* if-ne v2, v13, :cond_1 */
/* .line 45 */
v4 = this.info;
/* move-object v2, v0 */
/* move-object v3, v1 */
/* move-wide/from16 v5, p1 */
/* move-wide/from16 v7, p3 */
/* invoke-static/range {v2 ..v8}, Lmiui/android/animation/internal/AnimRunnerTask;->startAnim(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;JJ)V */
/* .line 47 */
} // :cond_1
/* iget-byte v2, v1, Lmiui/android/animation/internal/AnimData;->op:B */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_2 */
/* .line 48 */
v4 = this.info;
/* move-object v2, v0 */
/* move-object v3, v1 */
/* move-wide/from16 v5, p1 */
/* move-wide/from16 v7, p3 */
/* move/from16 v20, v10 */
} // .end local v10 # "isViewTarget":Z
/* .local v20, "isViewTarget":Z */
/* move-wide v9, v11 */
/* invoke-static/range {v2 ..v10}, Lmiui/android/animation/internal/AnimRunnerTask;->updateAnimation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;Lmiui/android/animation/internal/TransitionInfo;JJJ)V */
/* .line 47 */
} // .end local v20 # "isViewTarget":Z
/* .restart local v10 # "isViewTarget":Z */
} // :cond_2
/* move/from16 v20, v10 */
/* .line 50 */
} // .end local v10 # "isViewTarget":Z
/* .restart local v20 # "isViewTarget":Z */
} // :goto_3
/* move-object/from16 v2, v16 */
} // .end local v16 # "update":Lmiui/android/animation/listener/UpdateInfo;
/* .local v2, "update":Lmiui/android/animation/listener/UpdateInfo; */
(( miui.android.animation.internal.AnimData ) v1 ).to ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/animation/internal/AnimData;->to(Lmiui/android/animation/listener/UpdateInfo;)V
/* .line 51 */
if ( p5 != null) { // if-eqz p5, :cond_3
if ( p6 != null) { // if-eqz p6, :cond_3
/* if-nez v20, :cond_3 */
/* iget-wide v3, v1, Lmiui/android/animation/internal/AnimData;->value:D */
v3 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v3,v4 );
/* if-nez v3, :cond_3 */
/* .line 52 */
v3 = this.info;
v3 = this.target;
(( miui.android.animation.listener.UpdateInfo ) v2 ).setTargetValue ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/listener/UpdateInfo;->setTargetValue(Lmiui/android/animation/IAnimTarget;)V
/* .line 37 */
} // .end local v2 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // .end local v17 # "sc":Lmiui/android/animation/base/AnimSpecialConfig;
} // :cond_3
/* add-int/lit8 v9, v19, 0x1 */
/* move/from16 v8, v18 */
/* move/from16 v10, v20 */
} // .end local v19 # "idx":I
/* .restart local v9 # "idx":I */
/* goto/16 :goto_1 */
} // .end local v18 # "n":I
} // .end local v20 # "isViewTarget":Z
/* .restart local v8 # "n":I */
/* .restart local v10 # "isViewTarget":Z */
} // :cond_4
/* move/from16 v18, v8 */
/* move/from16 v19, v9 */
/* move/from16 v20, v10 */
/* .line 55 */
} // .end local v8 # "n":I
} // .end local v9 # "idx":I
} // .end local v10 # "isViewTarget":Z
/* .restart local v20 # "isViewTarget":Z */
(( miui.android.animation.internal.AnimTask ) v0 ).remove ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimTask;->remove()Lmiui/android/animation/utils/LinkNode;
/* move-object v0, v2 */
/* check-cast v0, Lmiui/android/animation/internal/AnimTask; */
/* .line 56 */
} // .end local v14 # "needSetup":Z
} // .end local v15 # "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
} // .end local v20 # "isViewTarget":Z
/* goto/16 :goto_0 */
/* .line 57 */
} // :cond_5
return;
} // .end method
private static Double evaluateValue ( miui.android.animation.internal.AnimData p0, Float p1 ) {
/* .locals 5 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p1, "progress" # F */
/* .line 242 */
v0 = this.property;
miui.android.animation.internal.AnimRunnerTask .getEvaluator ( v0 );
/* .line 243 */
/* .local v0, "evaluator":Landroid/animation/TypeEvaluator; */
/* instance-of v1, v0, Landroid/animation/IntEvaluator; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 244 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/animation/IntEvaluator; */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* double-to-int v2, v2 */
/* .line 245 */
java.lang.Integer .valueOf ( v2 );
/* iget-wide v3, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* double-to-int v3, v3 */
/* .line 246 */
java.lang.Integer .valueOf ( v3 );
/* .line 244 */
(( android.animation.IntEvaluator ) v1 ).evaluate ( p1, v2, v3 ); // invoke-virtual {v1, p1, v2, v3}, Landroid/animation/IntEvaluator;->evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;
/* .line 247 */
/* .local v1, "value":Ljava/lang/Integer; */
(( java.lang.Integer ) v1 ).doubleValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->doubleValue()D
/* move-result-wide v2 */
/* return-wide v2 */
/* .line 249 */
} // .end local v1 # "value":Ljava/lang/Integer;
} // :cond_0
/* move-object v1, v0 */
/* check-cast v1, Landroid/animation/FloatEvaluator; */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* double-to-float v2, v2 */
/* .line 250 */
java.lang.Float .valueOf ( v2 );
/* iget-wide v3, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* double-to-float v3, v3 */
/* .line 251 */
java.lang.Float .valueOf ( v3 );
/* .line 249 */
(( android.animation.FloatEvaluator ) v1 ).evaluate ( p1, v2, v3 ); // invoke-virtual {v1, p1, v2, v3}, Landroid/animation/FloatEvaluator;->evaluate(FLjava/lang/Number;Ljava/lang/Number;)Ljava/lang/Float;
/* .line 252 */
/* .local v1, "value":Ljava/lang/Float; */
(( java.lang.Float ) v1 ).doubleValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->doubleValue()D
/* move-result-wide v2 */
/* return-wide v2 */
} // .end method
private static void finishProperty ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimData p1 ) {
/* .locals 2 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .line 177 */
int v0 = 5; // const/4 v0, 0x5
(( miui.android.animation.internal.AnimData ) p1 ).setOp ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/internal/AnimData;->setOp(B)V
/* .line 178 */
v0 = this.animStats;
/* iget v1, v0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, v0, Lmiui/android/animation/internal/AnimStats;->failCount:I */
/* .line 179 */
return;
} // .end method
private static android.animation.TypeEvaluator getEvaluator ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p0, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 257 */
v0 = miui.android.animation.property.ViewPropertyExt.BACKGROUND;
/* if-ne p0, v0, :cond_0 */
/* instance-of v0, p0, Lmiui/android/animation/property/ColorProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 258 */
v0 = miui.android.animation.utils.CommonUtils.sArgbEvaluator;
/* .line 259 */
} // :cond_0
/* instance-of v0, p0, Lmiui/android/animation/property/IIntValueProperty; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 260 */
/* new-instance v0, Landroid/animation/IntEvaluator; */
/* invoke-direct {v0}, Landroid/animation/IntEvaluator;-><init>()V */
/* .line 262 */
} // :cond_1
/* new-instance v0, Landroid/animation/FloatEvaluator; */
/* invoke-direct {v0}, Landroid/animation/FloatEvaluator;-><init>()V */
} // .end method
private static Boolean initAnimation ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimData p1, Long p2, Long p3 ) {
/* .locals 7 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p2, "totalT" # J */
/* .param p4, "deltaT" # J */
/* .line 129 */
v0 = miui.android.animation.internal.AnimRunnerTask .setValues ( p1 );
final String v1 = ", value = "; // const-string v1, ", value = "
final String v2 = ", property = "; // const-string v2, ", property = "
final String v3 = "miuix_anim"; // const-string v3, "miuix_anim"
int v4 = 0; // const/4 v4, 0x0
/* if-nez v0, :cond_1 */
/* .line 130 */
/* iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 131 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "StartTask, set start value failed, break, tag = "; // const-string v5, "StartTask, set start value failed, break, tag = "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.info;
v5 = this.key;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.property;
/* .line 133 */
(( miui.android.animation.property.FloatProperty ) v2 ).getName ( ); // invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", start value = "; // const-string v2, ", start value = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", target value = "; // const-string v2, ", target value = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->targetValue:D */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->value:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 131 */
miui.android.animation.utils.LogUtils .logThread ( v3,v0 );
/* .line 139 */
} // :cond_0
miui.android.animation.internal.AnimRunnerTask .finishProperty ( p0,p1 );
/* .line 140 */
/* .line 142 */
} // :cond_1
v0 = miui.android.animation.internal.AnimRunnerTask .isValueInvalid ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 143 */
/* iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 144 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "StartTask, values invalid, break, tag = "; // const-string v5, "StartTask, values invalid, break, tag = "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.info;
v5 = this.key;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.property;
/* .line 146 */
(( miui.android.animation.property.FloatProperty ) v2 ).getName ( ); // invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", startValue = "; // const-string v2, ", startValue = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", targetValue = "; // const-string v2, ", targetValue = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p1, Lmiui/android/animation/internal/AnimData;->targetValue:D */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->value:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", velocity = "; // const-string v1, ", velocity = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->velocity:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 144 */
miui.android.animation.utils.LogUtils .logThread ( v3,v0 );
/* .line 153 */
} // :cond_2
(( miui.android.animation.internal.AnimData ) p1 ).reset ( ); // invoke-virtual {p1}, Lmiui/android/animation/internal/AnimData;->reset()V
/* .line 154 */
miui.android.animation.internal.AnimRunnerTask .finishProperty ( p0,p1 );
/* .line 155 */
/* .line 157 */
} // :cond_3
/* sub-long v0, p2, p4 */
/* iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startTime:J */
/* .line 158 */
/* iput v4, p1, Lmiui/android/animation/internal/AnimData;->frameCount:I */
/* .line 159 */
int v0 = 2; // const/4 v0, 0x2
(( miui.android.animation.internal.AnimData ) p1 ).setOp ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/internal/AnimData;->setOp(B)V
/* .line 160 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private static Boolean isValueInvalid ( miui.android.animation.internal.AnimData p0 ) {
/* .locals 4 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .line 182 */
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* cmpl-double v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D */
/* .line 183 */
java.lang.Math .abs ( v0,v1 );
/* move-result-wide v0 */
/* const-wide v2, 0x4030aaaaa0000000L # 16.66666603088379 */
/* cmpg-double v0, v0, v2 */
/* if-gez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 182 */
} // :goto_0
} // .end method
private static Float regulateProgress ( Float p0 ) {
/* .locals 2 */
/* .param p0, "progress" # F */
/* .line 233 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* cmpl-float v1, p0, v0 */
/* if-lez v1, :cond_0 */
/* .line 234 */
/* .line 235 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v1, p0, v0 */
/* if-gez v1, :cond_1 */
/* .line 236 */
/* .line 238 */
} // :cond_1
} // .end method
private static void setStartData ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimData p1 ) {
/* .locals 3 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .line 110 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->progress:D */
/* .line 111 */
(( miui.android.animation.internal.AnimData ) p1 ).reset ( ); // invoke-virtual {p1}, Lmiui/android/animation/internal/AnimData;->reset()V
/* .line 112 */
/* iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 113 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "+++++ start anim, target = "; // const-string v1, "+++++ start anim, target = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.info;
v1 = this.target;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", tag = "; // const-string v1, ", tag = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.info;
v1 = this.key;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", property = "; // const-string v1, ", property = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.property;
/* .line 115 */
(( miui.android.animation.property.FloatProperty ) v1 ).getName ( ); // invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", op = "; // const-string v1, ", op = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v1, p1, Lmiui/android/animation/internal/AnimData;->op:B */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", ease = "; // const-string v1, ", ease = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.ease;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", delay = "; // const-string v1, ", delay = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->delay:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", start value = "; // const-string v1, ", start value = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", target value = "; // const-string v1, ", target value = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->targetValue:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", value = "; // const-string v1, ", value = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->value:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", progress = "; // const-string v1, ", progress = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->progress:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = ", velocity = "; // const-string v1, ", velocity = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->velocity:D */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
/* .line 113 */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 126 */
} // :cond_0
return;
} // .end method
private static Boolean setValues ( miui.android.animation.internal.AnimData p0 ) {
/* .locals 4 */
/* .param p0, "data" # Lmiui/android/animation/internal/AnimData; */
/* .line 164 */
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D */
v0 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* .line 165 */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
v0 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v2,v3 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 166 */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 168 */
} // :cond_0
/* .line 169 */
} // :cond_1
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
v0 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v2,v3 );
/* if-nez v0, :cond_2 */
/* .line 170 */
/* iget-wide v2, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* iput-wide v2, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 171 */
/* .line 173 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
static void setup ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimData p1, miui.android.animation.internal.TransitionInfo p2, miui.android.animation.base.AnimSpecialConfig p3, Long p4, Long p5 ) {
/* .locals 5 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p2, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .param p3, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .param p4, "totalT" # J */
/* .param p6, "deltaT" # J */
/* .line 61 */
/* iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
v0 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 62 */
/* iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 63 */
/* .local v0, "startValue":D */
v2 = this.target;
v3 = this.property;
miui.android.animation.internal.AnimValueUtils .getValue ( v2,v3,v0,v1 );
/* move-result-wide v2 */
/* iput-wide v2, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 65 */
} // .end local v0 # "startValue":D
} // :cond_0
/* sub-long v0, p4, p6 */
/* iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->initTime:J */
/* .line 66 */
v0 = this.animStats;
/* iget v1, v0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v1, v2 */
/* iput v1, v0, Lmiui/android/animation/internal/AnimStats;->initCount:I */
/* .line 67 */
/* iget-byte v0, p1, Lmiui/android/animation/internal/AnimData;->op:B */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->delay:J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v0, v0, v3 */
/* if-lez v0, :cond_1 */
/* .line 74 */
} // :cond_1
/* sub-long v0, p4, p6 */
/* iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startTime:J */
/* .line 75 */
/* iput-wide v3, p1, Lmiui/android/animation/internal/AnimData;->delay:J */
/* .line 76 */
v0 = this.animStats;
/* iget v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* sub-int/2addr v1, v2 */
/* iput v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* .line 77 */
miui.android.animation.internal.AnimRunnerTask .setStartData ( p0,p1 );
/* .line 68 */
} // :cond_2
} // :goto_0
(( miui.android.animation.internal.AnimData ) p1 ).setOp ( v2 ); // invoke-virtual {p1, v2}, Lmiui/android/animation/internal/AnimData;->setOp(B)V
/* .line 69 */
v0 = this.config;
v0 = miui.android.animation.internal.AnimConfigUtils .getFromSpeed ( v0,p3 );
/* .line 70 */
/* .local v0, "fromSpeed":F */
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
/* cmpl-float v1, v0, v1 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 71 */
/* float-to-double v1, v0 */
/* iput-wide v1, p1, Lmiui/android/animation/internal/AnimData;->velocity:D */
/* .line 73 */
} // .end local v0 # "fromSpeed":F
} // :cond_3
/* nop */
/* .line 79 */
} // :goto_1
return;
} // .end method
static void startAnim ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimData p1, miui.android.animation.internal.TransitionInfo p2, Long p3, Long p4 ) {
/* .locals 6 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p2, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .param p3, "totalT" # J */
/* .param p5, "deltaT" # J */
/* .line 83 */
/* iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->delay:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_2 */
/* .line 84 */
/* iget-boolean v0, p1, Lmiui/android/animation/internal/AnimData;->logEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 85 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StartTask, tag = "; // const-string v1, "StartTask, tag = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.info;
v1 = this.key;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", property = "; // const-string v1, ", property = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.property;
/* .line 86 */
(( miui.android.animation.property.FloatProperty ) v1 ).getName ( ); // invoke-virtual {v1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", delay = "; // const-string v1, ", delay = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->delay:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", initTime = "; // const-string v1, ", initTime = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p1, Lmiui/android/animation/internal/AnimData;->initTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", totalT = "; // const-string v1, ", totalT = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
/* .line 85 */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 92 */
} // :cond_0
/* iget-wide v0, p1, Lmiui/android/animation/internal/AnimData;->initTime:J */
/* iget-wide v2, p1, Lmiui/android/animation/internal/AnimData;->delay:J */
/* add-long/2addr v0, v2 */
/* cmp-long v0, p3, v0 */
/* if-gez v0, :cond_1 */
/* .line 93 */
return;
/* .line 95 */
} // :cond_1
v0 = this.target;
v1 = this.property;
/* const-wide v2, 0x7fefffffffffffffL # Double.MAX_VALUE */
miui.android.animation.internal.AnimValueUtils .getValue ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* .line 97 */
/* .local v0, "startValue":D */
/* cmpl-double v2, v0, v2 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 98 */
/* iput-wide v0, p1, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 102 */
} // .end local v0 # "startValue":D
} // :cond_2
v0 = this.animStats;
/* iget v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* add-int/lit8 v1, v1, -0x1 */
/* iput v1, v0, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* .line 103 */
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-wide v2, p3 */
/* move-wide v4, p5 */
v0 = /* invoke-static/range {v0 ..v5}, Lmiui/android/animation/internal/AnimRunnerTask;->initAnimation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimData;JJ)Z */
/* if-nez v0, :cond_3 */
/* .line 104 */
return;
/* .line 106 */
} // :cond_3
miui.android.animation.internal.AnimRunnerTask .setStartData ( p0,p1 );
/* .line 107 */
return;
} // .end method
private static void updateAnimation ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimData p1, miui.android.animation.internal.TransitionInfo p2, Long p3, Long p4, Long p5 ) {
/* .locals 16 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "data" # Lmiui/android/animation/internal/AnimData; */
/* .param p2, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .param p3, "totalT" # J */
/* .param p5, "deltaT" # J */
/* .param p7, "averageDelta" # J */
/* .line 188 */
/* move-object/from16 v0, p0 */
/* move-object/from16 v9, p1 */
/* move-object/from16 v10, p2 */
v1 = this.animStats;
/* iget v2, v1, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
int v11 = 1; // const/4 v11, 0x1
/* add-int/2addr v2, v11 */
/* iput v2, v1, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
/* .line 189 */
/* iget v1, v9, Lmiui/android/animation/internal/AnimData;->frameCount:I */
/* add-int/2addr v1, v11 */
/* iput v1, v9, Lmiui/android/animation/internal/AnimData;->frameCount:I */
/* .line 190 */
v1 = this.property;
v2 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* if-eq v1, v2, :cond_1 */
v1 = this.property;
v2 = miui.android.animation.property.ViewPropertyExt.BACKGROUND;
/* if-eq v1, v2, :cond_1 */
v1 = this.property;
/* instance-of v1, v1, Lmiui/android/animation/property/ColorProperty; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 206 */
} // :cond_0
v1 = this.target;
/* move-object/from16 v2, p1 */
/* move-wide/from16 v3, p3 */
/* move-wide/from16 v5, p5 */
/* move-wide/from16 v7, p7 */
/* invoke-static/range {v1 ..v8}, Lmiui/android/animation/styles/PropertyStyle;->doAnimationFrame(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V */
/* .line 207 */
v1 = this.ease;
/* iget v1, v1, Lmiui/android/animation/utils/EaseManager$EaseStyle;->style:I */
v1 = miui.android.animation.utils.EaseManager .isPhysicsStyle ( v1 );
/* if-nez v1, :cond_2 */
/* .line 208 */
/* iget-wide v1, v9, Lmiui/android/animation/internal/AnimData;->progress:D */
/* double-to-float v1, v1 */
miui.android.animation.internal.AnimRunnerTask .evaluateValue ( v9,v1 );
/* move-result-wide v1 */
/* iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 193 */
} // :cond_1
} // :goto_0
/* iget-wide v12, v9, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 194 */
/* .local v12, "startValue":D */
/* iget-wide v14, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* .line 195 */
/* .local v14, "targetValue":D */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 196 */
/* const-wide/high16 v1, 0x3ff0000000000000L # 1.0 */
/* iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* .line 197 */
/* iget-wide v1, v9, Lmiui/android/animation/internal/AnimData;->progress:D */
/* iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 198 */
v1 = this.target;
/* move-object/from16 v2, p1 */
/* move-wide/from16 v3, p3 */
/* move-wide/from16 v5, p5 */
/* move-wide/from16 v7, p7 */
/* invoke-static/range {v1 ..v8}, Lmiui/android/animation/styles/PropertyStyle;->doAnimationFrame(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/internal/AnimData;JJJ)V */
/* .line 199 */
/* iget-wide v1, v9, Lmiui/android/animation/internal/AnimData;->value:D */
/* double-to-float v1, v1 */
v1 = miui.android.animation.internal.AnimRunnerTask .regulateProgress ( v1 );
/* float-to-double v1, v1 */
/* iput-wide v1, v9, Lmiui/android/animation/internal/AnimData;->progress:D */
/* .line 200 */
/* iput-wide v12, v9, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* .line 201 */
/* iput-wide v14, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* .line 202 */
v1 = miui.android.animation.utils.CommonUtils.sArgbEvaluator;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->progress:D */
/* double-to-float v2, v2 */
/* iget-wide v3, v9, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* double-to-int v3, v3 */
/* .line 203 */
java.lang.Integer .valueOf ( v3 );
/* iget-wide v4, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* double-to-int v4, v4 */
java.lang.Integer .valueOf ( v4 );
(( android.animation.ArgbEvaluator ) v1 ).evaluate ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
/* .line 204 */
/* .local v1, "animValue":Ljava/lang/Integer; */
(( java.lang.Integer ) v1 ).doubleValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->doubleValue()D
/* move-result-wide v2 */
/* iput-wide v2, v9, Lmiui/android/animation/internal/AnimData;->value:D */
/* .line 205 */
} // .end local v1 # "animValue":Ljava/lang/Integer;
} // .end local v12 # "startValue":D
} // .end local v14 # "targetValue":D
/* nop */
/* .line 211 */
} // :cond_2
} // :goto_1
/* iget-byte v1, v9, Lmiui/android/animation/internal/AnimData;->op:B */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_3 */
/* .line 212 */
/* iput-boolean v11, v9, Lmiui/android/animation/internal/AnimData;->justEnd:Z */
/* .line 213 */
v1 = this.animStats;
/* iget v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* add-int/2addr v2, v11 */
/* iput v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* .line 215 */
} // :cond_3
/* iget-boolean v1, v9, Lmiui/android/animation/internal/AnimData;->logEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 216 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "----- update anim, target = "; // const-string v2, "----- update anim, target = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.info;
v2 = this.target;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", tag = "; // const-string v2, ", tag = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.info;
v2 = this.key;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ", property = "; // const-string v2, ", property = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.property;
/* .line 218 */
(( miui.android.animation.property.FloatProperty ) v2 ).getName ( ); // invoke-virtual {v2}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", op = "; // const-string v2, ", op = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-byte v2, v9, Lmiui/android/animation/internal/AnimData;->op:B */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", init time = "; // const-string v2, ", init time = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->initTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", start time = "; // const-string v2, ", start time = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->startTime:J */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", start value = "; // const-string v2, ", start value = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->startValue:D */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", target value = "; // const-string v2, ", target value = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->targetValue:D */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", value = "; // const-string v2, ", value = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->value:D */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", progress = "; // const-string v2, ", progress = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->progress:D */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", velocity = "; // const-string v2, ", velocity = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v2, v9, Lmiui/android/animation/internal/AnimData;->velocity:D */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v2 = ", delta = "; // const-string v2, ", delta = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-wide/from16 v2, p5 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v4 = 0; // const/4 v4, 0x0
/* new-array v4, v4, [Ljava/lang/Object; */
/* .line 216 */
miui.android.animation.utils.LogUtils .debug ( v1,v4 );
/* .line 215 */
} // :cond_4
/* move-wide/from16 v2, p5 */
/* .line 230 */
} // :goto_2
return;
} // .end method
