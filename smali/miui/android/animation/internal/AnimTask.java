public class miui.android.animation.internal.AnimTask extends miui.android.animation.utils.LinkNode implements java.lang.Runnable {
	 /* .source "AnimTask.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Lmiui/android/animation/utils/LinkNode<", */
	 /* "Lmiui/android/animation/internal/AnimTask;", */
	 /* ">;", */
	 /* "Ljava/lang/Runnable;" */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer MAX_PAGE_SIZE;
public static final Integer MAX_SINGLE_TASK_SIZE;
public static final Integer MAX_TO_PAGE_SIZE;
public static final Object OP_CANCEL;
public static final Object OP_END;
public static final Object OP_FAILED;
public static final Object OP_INVALID;
public static final Object OP_START;
public static final Object OP_UPDATE;
public static final java.util.concurrent.atomic.AtomicInteger sTaskCount;
/* # instance fields */
public final miui.android.animation.internal.AnimStats animStats;
public volatile Long deltaT;
public volatile miui.android.animation.internal.TransitionInfo info;
public volatile Integer startPos;
public volatile Boolean toPage;
public volatile Long totalT;
/* # direct methods */
static miui.android.animation.internal.AnimTask ( ) {
	 /* .locals 1 */
	 /* .line 19 */
	 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
	 /* invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V */
	 return;
} // .end method
public miui.android.animation.internal.AnimTask ( ) {
	 /* .locals 1 */
	 /* .line 13 */
	 /* invoke-direct {p0}, Lmiui/android/animation/utils/LinkNode;-><init>()V */
	 /* .line 28 */
	 /* new-instance v0, Lmiui/android/animation/internal/AnimStats; */
	 /* invoke-direct {v0}, Lmiui/android/animation/internal/AnimStats;-><init>()V */
	 this.animStats = v0;
	 return;
} // .end method
public static Boolean isRunning ( Object p0 ) {
	 /* .locals 2 */
	 /* .param p0, "op" # B */
	 /* .line 39 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* if-eq p0, v0, :cond_1 */
	 int v1 = 2; // const/4 v1, 0x2
	 /* if-ne p0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
} // .end method
/* # virtual methods */
public Integer getAnimCount ( ) {
/* .locals 1 */
/* .line 56 */
v0 = this.animStats;
/* iget v0, v0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
} // .end method
public Integer getTotalAnimCount ( ) {
/* .locals 3 */
/* .line 60 */
int v0 = 0; // const/4 v0, 0x0
/* .line 61 */
/* .local v0, "count":I */
/* move-object v1, p0 */
/* .line 62 */
/* .local v1, "task":Lmiui/android/animation/internal/AnimTask; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 63 */
v2 = this.animStats;
/* iget v2, v2, Lmiui/android/animation/internal/AnimStats;->animCount:I */
/* add-int/2addr v0, v2 */
/* .line 64 */
v2 = this.next;
/* move-object v1, v2 */
/* check-cast v1, Lmiui/android/animation/internal/AnimTask; */
/* .line 66 */
} // :cond_0
} // .end method
public void run ( ) {
/* .locals 7 */
/* .line 96 */
try { // :try_start_0
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimTask;->totalT:J */
/* iget-wide v3, p0, Lmiui/android/animation/internal/AnimTask;->deltaT:J */
int v5 = 1; // const/4 v5, 0x1
/* iget-boolean v6, p0, Lmiui/android/animation/internal/AnimTask;->toPage:Z */
/* move-object v0, p0 */
/* invoke-static/range {v0 ..v6}, Lmiui/android/animation/internal/AnimRunnerTask;->doAnimationFrame(Lmiui/android/animation/internal/AnimTask;JJZZ)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 99 */
/* .line 97 */
/* :catch_0 */
/* move-exception v0 */
/* .line 98 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "miuix_anim"; // const-string v1, "miuix_anim"
final String v2 = "doAnimationFrame failed"; // const-string v2, "doAnimationFrame failed"
android.util.Log .d ( v1,v2,v0 );
/* .line 100 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
v0 = miui.android.animation.internal.AnimTask.sTaskCount;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).decrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
/* if-nez v0, :cond_0 */
/* .line 101 */
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
int v1 = 2; // const/4 v1, 0x2
(( miui.android.animation.internal.RunnerHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/internal/RunnerHandler;->sendEmptyMessage(I)Z
/* .line 103 */
} // :cond_0
return;
} // .end method
public void setup ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pos" # I */
/* .param p2, "amount" # I */
/* .line 43 */
v0 = this.animStats;
(( miui.android.animation.internal.AnimStats ) v0 ).clear ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimStats;->clear()V
/* .line 44 */
v0 = this.animStats;
/* iput p2, v0, Lmiui/android/animation/internal/AnimStats;->animCount:I */
/* .line 45 */
/* iput p1, p0, Lmiui/android/animation/internal/AnimTask;->startPos:I */
/* .line 46 */
return;
} // .end method
public void start ( Long p0, Long p1, Boolean p2 ) {
/* .locals 0 */
/* .param p1, "totalT" # J */
/* .param p3, "deltaT" # J */
/* .param p5, "toPage" # Z */
/* .line 49 */
/* iput-wide p1, p0, Lmiui/android/animation/internal/AnimTask;->totalT:J */
/* .line 50 */
/* iput-wide p3, p0, Lmiui/android/animation/internal/AnimTask;->deltaT:J */
/* .line 51 */
/* iput-boolean p5, p0, Lmiui/android/animation/internal/AnimTask;->toPage:Z */
/* .line 52 */
miui.android.animation.internal.ThreadPoolUtil .post ( p0 );
/* .line 53 */
return;
} // .end method
void updateAnimStats ( ) {
/* .locals 6 */
/* .line 70 */
/* iget v0, p0, Lmiui/android/animation/internal/AnimTask;->startPos:I */
/* .local v0, "i":I */
/* iget v1, p0, Lmiui/android/animation/internal/AnimTask;->startPos:I */
v2 = this.animStats;
/* iget v2, v2, Lmiui/android/animation/internal/AnimStats;->animCount:I */
/* add-int/2addr v1, v2 */
/* .local v1, "n":I */
} // :goto_0
/* if-ge v0, v1, :cond_2 */
/* .line 71 */
v2 = this.info;
v2 = this.updateList;
/* check-cast v2, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 72 */
/* .local v2, "update":Lmiui/android/animation/listener/UpdateInfo; */
v3 = this.animInfo;
/* iget-byte v3, v3, Lmiui/android/animation/internal/AnimInfo;->op:B */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_1
v3 = this.animInfo;
/* iget-byte v3, v3, Lmiui/android/animation/internal/AnimInfo;->op:B */
/* if-ne v3, v4, :cond_0 */
/* .line 75 */
} // :cond_0
v3 = this.animStats;
/* iget v5, v3, Lmiui/android/animation/internal/AnimStats;->initCount:I */
/* add-int/2addr v5, v4 */
/* iput v5, v3, Lmiui/android/animation/internal/AnimStats;->initCount:I */
/* .line 76 */
v3 = this.animInfo;
/* iget-byte v3, v3, Lmiui/android/animation/internal/AnimInfo;->op:B */
/* packed-switch v3, :pswitch_data_0 */
/* .line 78 */
/* :pswitch_0 */
v3 = this.animStats;
/* iget v5, v3, Lmiui/android/animation/internal/AnimStats;->failCount:I */
/* add-int/2addr v5, v4 */
/* iput v5, v3, Lmiui/android/animation/internal/AnimStats;->failCount:I */
/* .line 79 */
/* .line 81 */
/* :pswitch_1 */
v3 = this.animStats;
/* iget v5, v3, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* add-int/2addr v5, v4 */
/* iput v5, v3, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* .line 82 */
/* .line 84 */
/* :pswitch_2 */
v3 = this.animStats;
/* iget v5, v3, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* add-int/2addr v5, v4 */
/* iput v5, v3, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* .line 85 */
/* .line 73 */
} // :cond_1
} // :goto_1
v3 = this.animStats;
/* iget v5, v3, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* add-int/2addr v5, v4 */
/* iput v5, v3, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* .line 70 */
} // .end local v2 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 91 */
} // .end local v0 # "i":I
} // .end local v1 # "n":I
} // :cond_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
