.class public final Lmiui/android/animation/internal/TargetHandler;
.super Landroid/os/Handler;
.source "TargetHandler.java"


# static fields
.field public static final ANIM_MSG_END:I = 0x2

.field public static final ANIM_MSG_REMOVE_WAIT:I = 0x3

.field public static final ANIM_MSG_REPLACED:I = 0x5

.field public static final ANIM_MSG_START_TAG:I = 0x0

.field public static final ANIM_MSG_UPDATE_LISTENER:I = 0x4

.field private static final MASS_UPDATE_THRESHOLD:I = 0x9c40


# instance fields
.field private final mTarget:Lmiui/android/animation/IAnimTarget;

.field private final mTransList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final threadId:J


# direct methods
.method public constructor <init>(Lmiui/android/animation/IAnimTarget;)V
    .locals 2
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 40
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTransList:Ljava/util/List;

    .line 41
    iput-object p1, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    .line 42
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lmiui/android/animation/internal/TargetHandler;->threadId:J

    .line 43
    return-void
.end method

.method private static notifyStartOrEnd(Lmiui/android/animation/internal/TransitionInfo;Z)V
    .locals 5
    .param p0, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p1, "notifyStart"    # Z

    .line 100
    invoke-virtual {p0}, Lmiui/android/animation/internal/TransitionInfo;->getAnimCount()I

    move-result v0

    const/16 v1, 0xfa0

    if-le v0, v1, :cond_0

    .line 101
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 104
    .local v0, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/listener/UpdateInfo;

    .line 105
    .local v2, "update":Lmiui/android/animation/listener/UpdateInfo;
    iget-object v3, v2, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    sget-object v4, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    if-eq v3, v4, :cond_1

    .line 106
    goto :goto_0

    .line 108
    :cond_1
    if-eqz p1, :cond_2

    .line 109
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-static {v3, v2}, Lmiui/android/animation/styles/ForegroundColorStyle;->start(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/listener/UpdateInfo;)V

    goto :goto_1

    .line 111
    :cond_2
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-static {v3, v2}, Lmiui/android/animation/styles/ForegroundColorStyle;->end(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/listener/UpdateInfo;)V

    .line 113
    .end local v2    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :goto_1
    goto :goto_0

    .line 114
    :cond_3
    return-void
.end method

.method private onEnd(Lmiui/android/animation/internal/TransitionInfo;I)V
    .locals 3
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p2, "reason"    # I

    .line 145
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< onEnd, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", info.key = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    :cond_0
    invoke-direct {p0, v1, p1}, Lmiui/android/animation/internal/TargetHandler;->update(ZLmiui/android/animation/internal/TransitionInfo;)V

    .line 149
    invoke-static {p1, v1}, Lmiui/android/animation/internal/TargetHandler;->notifyStartOrEnd(Lmiui/android/animation/internal/TransitionInfo;Z)V

    .line 150
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 151
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/listener/ListenerNotifier;->notifyCancelAll(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/listener/ListenerNotifier;->notifyEndAll(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 155
    :goto_0
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->removeListeners(Ljava/lang/Object;)V

    .line 156
    return-void
.end method

.method private onReplaced(Lmiui/android/animation/internal/TransitionInfo;)V
    .locals 4
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 159
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<<< onReplaced, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info.key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 162
    :cond_0
    invoke-virtual {p1}, Lmiui/android/animation/internal/TransitionInfo;->getAnimCount()I

    move-result v0

    const/16 v1, 0xfa0

    if-gt v0, v1, :cond_1

    .line 163
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    iget-object v3, p1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyEnd(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 165
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/listener/ListenerNotifier;->notifyCancelAll(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->removeListeners(Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method private onStart(Lmiui/android/animation/internal/TransitionInfo;)V
    .locals 4
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 87
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ">>> onStart, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", info.key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :cond_0
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/listener/ListenerNotifier;->addListeners(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfig;)Z

    .line 91
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/listener/ListenerNotifier;->notifyBegin(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 92
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 93
    .local v0, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xfa0

    if-gt v1, v2, :cond_1

    .line 94
    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v1

    iget-object v2, p1, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v3, p1, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyBegin(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 96
    :cond_1
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lmiui/android/animation/internal/TargetHandler;->notifyStartOrEnd(Lmiui/android/animation/internal/TransitionInfo;Z)V

    .line 97
    return-void
.end method

.method private static setValueAndNotify(Lmiui/android/animation/IAnimTarget;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Z)V
    .locals 2
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "tag"    # Ljava/lang/Object;
    .param p4, "toPage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/android/animation/IAnimTarget;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;Z)V"
        }
    .end annotation

    .line 133
    .local p3, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    if-eqz p4, :cond_0

    instance-of v0, p0, Lmiui/android/animation/ViewTarget;

    if-eqz v0, :cond_1

    .line 134
    :cond_0
    invoke-static {p0, p3}, Lmiui/android/animation/internal/TargetHandler;->updateValueAndVelocity(Lmiui/android/animation/IAnimTarget;Ljava/util/List;)V

    .line 136
    :cond_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const v1, 0x9c40

    if-le v0, v1, :cond_2

    .line 137
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/listener/ListenerNotifier;->notifyMassUpdate(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 139
    :cond_2
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyEnd(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 140
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lmiui/android/animation/listener/ListenerNotifier;->notifyUpdate(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    .line 142
    :goto_0
    return-void
.end method

.method private update(ZLmiui/android/animation/internal/TransitionInfo;)V
    .locals 4
    .param p1, "toPage"    # Z
    .param p2, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 125
    iget-object v0, p2, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 126
    .local v0, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    iget-object v1, p2, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v2, p2, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v3, p2, Lmiui/android/animation/internal/TransitionInfo;->tag:Ljava/lang/Object;

    invoke-static {v1, v2, v3, v0, p1}, Lmiui/android/animation/internal/TargetHandler;->setValueAndNotify(Lmiui/android/animation/IAnimTarget;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Z)V

    .line 129
    :cond_0
    return-void
.end method

.method private static updateValueAndVelocity(Lmiui/android/animation/IAnimTarget;Ljava/util/List;)V
    .locals 4
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/android/animation/IAnimTarget;",
            "Ljava/util/List<",
            "Lmiui/android/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    .line 170
    .local p1, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/listener/UpdateInfo;

    .line 171
    .local v1, "update":Lmiui/android/animation/listener/UpdateInfo;
    iget-object v2, v1, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->value:D

    invoke-static {v2, v3}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    goto :goto_0

    .line 174
    :cond_0
    invoke-virtual {v1, p0}, Lmiui/android/animation/listener/UpdateInfo;->setTargetValue(Lmiui/android/animation/IAnimTarget;)V

    .line 175
    .end local v1    # "update":Lmiui/android/animation/listener/UpdateInfo;
    goto :goto_0

    .line 176
    :cond_1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 47
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 69
    :pswitch_1
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 70
    .local v0, "info":Lmiui/android/animation/internal/TransitionInfo;
    if-eqz v0, :cond_0

    .line 71
    iget-object v1, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v1

    iget-object v2, v0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lmiui/android/animation/listener/ListenerNotifier;->removeListeners(Ljava/lang/Object;)V

    .line 72
    iget-object v1, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v1

    iget-object v2, v0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    iget-object v3, v0, Lmiui/android/animation/internal/TransitionInfo;->config:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v1, v2, v3}, Lmiui/android/animation/listener/ListenerNotifier;->addListeners(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfig;)Z

    .line 75
    .end local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_0
    goto :goto_0

    .line 77
    :pswitch_2
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v0, v0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v0, v0, Lmiui/android/animation/internal/AnimManager;->mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto :goto_0

    .line 56
    :pswitch_3
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 57
    .restart local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    if-eqz v0, :cond_1

    .line 58
    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1}, Lmiui/android/animation/internal/TargetHandler;->onEnd(Lmiui/android/animation/internal/TransitionInfo;I)V

    .line 62
    .end local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_1
    :pswitch_4
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 63
    .restart local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    if-eqz v0, :cond_2

    .line 64
    invoke-direct {p0, v0}, Lmiui/android/animation/internal/TargetHandler;->onReplaced(Lmiui/android/animation/internal/TransitionInfo;)V

    .line 67
    .end local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_2
    goto :goto_0

    .line 49
    :pswitch_5
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 50
    .restart local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    if-eqz v0, :cond_3

    .line 51
    invoke-direct {p0, v0}, Lmiui/android/animation/internal/TargetHandler;->onStart(Lmiui/android/animation/internal/TransitionInfo;)V

    .line 54
    .end local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_3
    nop

    .line 80
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public isInTargetThread()Z
    .locals 2

    .line 83
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Lmiui/android/animation/internal/TargetHandler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public update(Z)V
    .locals 2
    .param p1, "toPage"    # Z

    .line 117
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTarget:Lmiui/android/animation/IAnimTarget;

    iget-object v0, v0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v1, p0, Lmiui/android/animation/internal/TargetHandler;->mTransList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lmiui/android/animation/internal/AnimManager;->getTransitionInfos(Ljava/util/List;)V

    .line 118
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTransList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/TransitionInfo;

    .line 119
    .local v1, "transInfo":Lmiui/android/animation/internal/TransitionInfo;
    invoke-direct {p0, p1, v1}, Lmiui/android/animation/internal/TargetHandler;->update(ZLmiui/android/animation/internal/TransitionInfo;)V

    .line 120
    .end local v1    # "transInfo":Lmiui/android/animation/internal/TransitionInfo;
    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/internal/TargetHandler;->mTransList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 122
    return-void
.end method
