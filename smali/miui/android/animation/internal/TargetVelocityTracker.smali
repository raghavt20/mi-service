.class public Lmiui/android/animation/internal/TargetVelocityTracker;
.super Ljava/lang/Object;
.source "TargetVelocityTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;,
        Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;
    }
.end annotation


# instance fields
.field private mMonitors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiui/android/animation/property/FloatProperty;",
            "Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/TargetVelocityTracker;->mMonitors:Ljava/util/Map;

    return-void
.end method

.method private getMonitor(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 69
    iget-object v0, p0, Lmiui/android/animation/internal/TargetVelocityTracker;->mMonitors:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

    .line 70
    .local v0, "info":Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;
    if-nez v0, :cond_0

    .line 71
    new-instance v1, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;-><init>(Lmiui/android/animation/internal/TargetVelocityTracker$1;)V

    move-object v0, v1

    .line 72
    iget-object v1, p0, Lmiui/android/animation/internal/TargetVelocityTracker;->mMonitors:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    return-object v0
.end method


# virtual methods
.method public trackVelocity(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)V
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "value"    # D

    .line 59
    invoke-direct {p0, p2}, Lmiui/android/animation/internal/TargetVelocityTracker;->getMonitor(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;

    move-result-object v0

    .line 60
    .local v0, "info":Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;
    iget-object v1, v0, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;->monitor:Lmiui/android/animation/utils/VelocityMonitor;

    const/4 v2, 0x1

    new-array v2, v2, [D

    const/4 v3, 0x0

    aput-wide p3, v2, v3

    invoke-virtual {v1, v2}, Lmiui/android/animation/utils/VelocityMonitor;->update([D)V

    .line 61
    iget-object v1, v0, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;->monitor:Lmiui/android/animation/utils/VelocityMonitor;

    invoke-virtual {v1, v3}, Lmiui/android/animation/utils/VelocityMonitor;->getVelocity(I)F

    move-result v1

    .line 62
    .local v1, "velocity":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 63
    iget-object v2, v0, Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo;->resetTask:Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;

    invoke-virtual {v2, p1, p2}, Lmiui/android/animation/internal/TargetVelocityTracker$ResetRunnable;->post(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)V

    .line 64
    float-to-double v2, v1

    invoke-virtual {p1, p2, v2, v3}, Lmiui/android/animation/IAnimTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V

    .line 66
    :cond_0
    return-void
.end method
