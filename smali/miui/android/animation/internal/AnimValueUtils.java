public class miui.android.animation.internal.AnimValueUtils {
	 /* .source "AnimValueUtils.java" */
	 /* # direct methods */
	 private miui.android.animation.internal.AnimValueUtils ( ) {
		 /* .locals 0 */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static Double getCurTargetValue ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Double p2 ) {
		 /* .locals 8 */
		 /* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
		 /* .param p2, "value" # D */
		 /* .line 41 */
		 java.lang.Math .signum ( p2,p3 );
		 /* move-result-wide v0 */
		 /* .line 42 */
		 /* .local v0, "sig":D */
		 java.lang.Math .abs ( p2,p3 );
		 /* move-result-wide v2 */
		 /* .line 43 */
		 /* .local v2, "absValue":D */
		 /* const-wide v4, 0x412e848000000000L # 1000000.0 */
		 /* cmpl-double v4, v2, v4 */
		 /* if-nez v4, :cond_0 */
		 /* .line 44 */
		 v4 = 		 miui.android.animation.utils.CommonUtils .getSize ( p0,p1 );
		 /* float-to-double v4, v4 */
		 /* mul-double/2addr v4, v0 */
		 /* return-wide v4 */
		 /* .line 46 */
	 } // :cond_0
	 /* instance-of v4, p1, Lmiui/android/animation/property/IIntValueProperty; */
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 /* move-object v4, p1 */
		 /* check-cast v4, Lmiui/android/animation/property/IIntValueProperty; */
		 /* .line 47 */
		 v4 = 		 (( miui.android.animation.IAnimTarget ) p0 ).getIntValue ( v4 ); // invoke-virtual {p0, v4}, Lmiui/android/animation/IAnimTarget;->getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I
		 /* int-to-double v4, v4 */
	 } // :cond_1
	 v4 = 	 (( miui.android.animation.IAnimTarget ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
	 /* float-to-double v4, v4 */
	 /* .line 48 */
	 /* .local v4, "curValue":D */
} // :goto_0
/* const-wide v6, 0x412e854800000000L # 1000100.0 */
/* cmpl-double v6, v2, v6 */
/* if-nez v6, :cond_2 */
/* .line 49 */
/* mul-double v6, v4, v0 */
/* return-wide v6 */
/* .line 51 */
} // :cond_2
/* return-wide v4 */
} // .end method
public static Double getValue ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Double p2 ) {
/* .locals 2 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # D */
/* .line 33 */
/* instance-of v0, p1, Lmiui/android/animation/property/ISpecificProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 34 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ISpecificProperty; */
v0 = /* double-to-float v1, p2 */
/* float-to-double v0, v0 */
/* return-wide v0 */
/* .line 36 */
} // :cond_0
miui.android.animation.internal.AnimValueUtils .getCurTargetValue ( p0,p1,p2,p3 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public static Double getValueOfTarget ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Double p2 ) {
/* .locals 2 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # D */
/* .line 23 */
/* const-wide v0, 0x41dfffffffc00000L # 2.147483647E9 */
/* cmpl-double v0, p2, v0 */
/* if-nez v0, :cond_0 */
/* .line 24 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/IIntValueProperty; */
v0 = (( miui.android.animation.IAnimTarget ) p0 ).getIntValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/IAnimTarget;->getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I
/* int-to-double v0, v0 */
/* return-wide v0 */
/* .line 25 */
} // :cond_0
/* const-wide v0, 0x47efffffe0000000L # 3.4028234663852886E38 */
/* cmpl-double v0, p2, v0 */
/* if-nez v0, :cond_1 */
/* .line 26 */
v0 = (( miui.android.animation.IAnimTarget ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
/* float-to-double v0, v0 */
/* return-wide v0 */
/* .line 28 */
} // :cond_1
miui.android.animation.internal.AnimValueUtils .getValue ( p0,p1,p2,p3 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public static Boolean handleSetToValue ( miui.android.animation.listener.UpdateInfo p0 ) {
/* .locals 3 */
/* .param p0, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .line 60 */
v0 = this.animInfo;
/* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
v0 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 61 */
v0 = this.animInfo;
v1 = this.animInfo;
/* iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* .line 62 */
v0 = this.animInfo;
/* const-wide v1, 0x7fefffffffffffffL # Double.MAX_VALUE */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
/* .line 63 */
int v0 = 1; // const/4 v0, 0x1
/* .line 65 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isInvalid ( Double p0 ) {
/* .locals 2 */
/* .param p0, "value" # D */
/* .line 56 */
/* const-wide v0, 0x7fefffffffffffffL # Double.MAX_VALUE */
/* cmpl-double v0, p0, v0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const-wide v0, 0x47efffffe0000000L # 3.4028234663852886E38 */
/* cmpl-double v0, p0, v0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* const-wide v0, 0x41dfffffffc00000L # 2.147483647E9 */
/* cmpl-double v0, p0, v0 */
/* if-nez v0, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
