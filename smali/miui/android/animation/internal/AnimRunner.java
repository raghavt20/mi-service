public class miui.android.animation.internal.AnimRunner implements miui.android.animation.physics.AnimationHandler$AnimationFrameCallback {
	 /* .source "AnimRunner.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/internal/AnimRunner$Holder; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Long MAX_DELTA;
private static final Integer MAX_RECORD;
private static final Integer MSG_END;
private static final Integer MSG_START;
private static final android.os.Handler sMainHandler;
public static final miui.android.animation.internal.RunnerHandler sRunnerHandler;
private static final android.os.HandlerThread sRunnerThread;
/* # instance fields */
private volatile Long mAverageDelta;
private mDeltaRecord;
private volatile Boolean mIsRunning;
private Long mLastFrameTime;
private Float mRatio;
private Integer mRecordCount;
/* # direct methods */
static miui.android.animation.internal.AnimRunner ( ) {
	 /* .locals 3 */
	 /* .line 42 */
	 /* new-instance v0, Landroid/os/HandlerThread; */
	 final String v1 = "AnimRunnerThread"; // const-string v1, "AnimRunnerThread"
	 int v2 = 5; // const/4 v2, 0x5
	 /* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
	 /* .line 50 */
	 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
	 /* .line 51 */
	 /* new-instance v1, Lmiui/android/animation/internal/RunnerHandler; */
	 (( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, v0}, Lmiui/android/animation/internal/RunnerHandler;-><init>(Landroid/os/Looper;)V */
	 /* .line 54 */
	 /* new-instance v0, Lmiui/android/animation/internal/AnimRunner$1; */
	 android.os.Looper .getMainLooper ( );
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/internal/AnimRunner$1;-><init>(Landroid/os/Looper;)V */
	 return;
} // .end method
private miui.android.animation.internal.AnimRunner ( ) {
	 /* .locals 2 */
	 /* .line 118 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 110 */
	 /* const-wide/16 v0, 0x10 */
	 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimRunner;->mAverageDelta:J */
	 /* .line 112 */
	 int v0 = 5; // const/4 v0, 0x5
	 /* new-array v0, v0, [J */
	 /* fill-array-data v0, :array_0 */
	 this.mDeltaRecord = v0;
	 /* .line 113 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lmiui/android/animation/internal/AnimRunner;->mRecordCount:I */
	 /* .line 119 */
	 return;
	 /* nop */
	 /* :array_0 */
	 /* .array-data 8 */
	 /* 0x0 */
	 /* 0x0 */
	 /* 0x0 */
	 /* 0x0 */
	 /* 0x0 */
} // .end array-data
} // .end method
 miui.android.animation.internal.AnimRunner ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lmiui/android/animation/internal/AnimRunner$1; */
/* .line 23 */
/* invoke-direct {p0}, Lmiui/android/animation/internal/AnimRunner;-><init>()V */
return;
} // .end method
static void access$100 ( ) { //synthethic
/* .locals 0 */
/* .line 23 */
miui.android.animation.internal.AnimRunner .startAnimRunner ( );
return;
} // .end method
static void access$200 ( ) { //synthethic
/* .locals 0 */
/* .line 23 */
miui.android.animation.internal.AnimRunner .endAnimation ( );
return;
} // .end method
private Long average ( Long[] p0 ) {
/* .locals 9 */
/* .param p1, "array" # [J */
/* .line 233 */
/* const-wide/16 v0, 0x0 */
/* .line 234 */
/* .local v0, "total":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 235 */
/* .local v2, "count":I */
/* array-length v3, p1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* const-wide/16 v5, 0x0 */
/* if-ge v4, v3, :cond_1 */
/* aget-wide v7, p1, v4 */
/* .line 236 */
/* .local v7, "a":J */
/* add-long/2addr v0, v7 */
/* .line 237 */
/* cmp-long v5, v7, v5 */
/* if-lez v5, :cond_0 */
/* add-int/lit8 v5, v2, 0x1 */
} // :cond_0
/* move v5, v2 */
} // :goto_1
/* move v2, v5 */
/* .line 235 */
} // .end local v7 # "a":J
/* add-int/lit8 v4, v4, 0x1 */
/* .line 239 */
} // :cond_1
/* if-lez v2, :cond_2 */
/* int-to-long v3, v2 */
/* div-long v5, v0, v3 */
} // :cond_2
/* return-wide v5 */
} // .end method
private Long calculateAverageDelta ( Long p0 ) {
/* .locals 6 */
/* .param p1, "deltaT" # J */
/* .line 209 */
v0 = this.mDeltaRecord;
/* invoke-direct {p0, v0}, Lmiui/android/animation/internal/AnimRunner;->average([J)J */
/* move-result-wide v0 */
/* .line 210 */
/* .local v0, "average":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v4, v0, v2 */
/* if-lez v4, :cond_0 */
/* move-wide v4, v0 */
} // :cond_0
/* move-wide v4, p1 */
} // :goto_0
/* move-wide p1, v4 */
/* .line 211 */
/* cmp-long v2, p1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* const-wide/16 v2, 0x10 */
/* cmp-long v2, p1, v2 */
/* if-lez v2, :cond_2 */
/* .line 212 */
} // :cond_1
/* const-wide/16 p1, 0x10 */
/* .line 214 */
} // :cond_2
/* long-to-float v2, p1 */
/* iget v3, p0, Lmiui/android/animation/internal/AnimRunner;->mRatio:F */
/* div-float/2addr v2, v3 */
/* float-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-long v2, v2 */
/* return-wide v2 */
} // .end method
private static void endAnimation ( ) {
/* .locals 4 */
/* .line 99 */
miui.android.animation.internal.AnimRunner .getInst ( );
/* .line 100 */
/* .local v0, "runner":Lmiui/android/animation/internal/AnimRunner; */
/* iget-boolean v1, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z */
/* if-nez v1, :cond_0 */
/* .line 101 */
return;
/* .line 103 */
} // :cond_0
v1 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 104 */
final String v1 = "AnimRunner.endAnimation"; // const-string v1, "AnimRunner.endAnimation"
/* new-array v3, v2, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v1,v3 );
/* .line 106 */
} // :cond_1
/* iput-boolean v2, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z */
/* .line 107 */
miui.android.animation.physics.AnimationHandler .getInstance ( );
(( miui.android.animation.physics.AnimationHandler ) v1 ).removeCallback ( v0 ); // invoke-virtual {v1, v0}, Lmiui/android/animation/physics/AnimationHandler;->removeCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;)V
/* .line 108 */
return;
} // .end method
public static miui.android.animation.internal.AnimRunner getInst ( ) {
/* .locals 1 */
/* .line 36 */
v0 = miui.android.animation.internal.AnimRunner$Holder.inst;
} // .end method
private static void startAnimRunner ( ) {
/* .locals 4 */
/* .line 86 */
miui.android.animation.internal.AnimRunner .getInst ( );
/* .line 87 */
/* .local v0, "runner":Lmiui/android/animation/internal/AnimRunner; */
/* iget-boolean v1, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 88 */
return;
/* .line 90 */
} // :cond_0
v1 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 91 */
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Ljava/lang/Object; */
final String v2 = "AnimRunner.start"; // const-string v2, "AnimRunner.start"
miui.android.animation.utils.LogUtils .debug ( v2,v1 );
/* .line 93 */
} // :cond_1
v1 = miui.android.animation.Folme .getTimeRatio ( );
/* iput v1, v0, Lmiui/android/animation/internal/AnimRunner;->mRatio:F */
/* .line 94 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z */
/* .line 95 */
miui.android.animation.physics.AnimationHandler .getInstance ( );
/* const-wide/16 v2, 0x0 */
(( miui.android.animation.physics.AnimationHandler ) v1 ).addAnimationFrameCallback ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/physics/AnimationHandler;->addAnimationFrameCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)V
/* .line 96 */
return;
} // .end method
private static void updateAnimRunner ( java.util.Collection p0, Boolean p1 ) {
/* .locals 5 */
/* .param p1, "toPage" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 71 */
/* .local p0, "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lmiui/android/animation/IAnimTarget; */
/* .line 72 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
v2 = this.animManager;
int v3 = 0; // const/4 v3, 0x0
/* new-array v4, v3, [Lmiui/android/animation/property/FloatProperty; */
v2 = (( miui.android.animation.internal.AnimManager ) v2 ).isAnimRunning ( v4 ); // invoke-virtual {v2, v4}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* .line 73 */
/* .local v2, "isAnimRunning":Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 74 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 75 */
v3 = this.animManager;
(( miui.android.animation.internal.AnimManager ) v3 ).runUpdate ( ); // invoke-virtual {v3}, Lmiui/android/animation/internal/AnimManager;->runUpdate()V
/* .line 77 */
} // :cond_0
v4 = this.animManager;
(( miui.android.animation.internal.AnimManager ) v4 ).update ( v3 ); // invoke-virtual {v4, v3}, Lmiui/android/animation/internal/AnimManager;->update(Z)V
/* .line 79 */
} // :cond_1
/* if-nez v2, :cond_2 */
/* const-wide/16 v3, 0x1 */
v3 = (( miui.android.animation.IAnimTarget ) v1 ).hasFlags ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Lmiui/android/animation/IAnimTarget;->hasFlags(J)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 80 */
/* filled-new-array {v1}, [Lmiui/android/animation/IAnimTarget; */
miui.android.animation.Folme .clean ( v3 );
/* .line 82 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v2 # "isAnimRunning":Z
} // :cond_2
} // :goto_1
/* .line 83 */
} // :cond_3
return;
} // .end method
private void updateRunningTime ( Long p0 ) {
/* .locals 6 */
/* .param p1, "frameTime" # J */
/* .line 219 */
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimRunner;->mLastFrameTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
/* if-nez v2, :cond_0 */
/* .line 220 */
/* iput-wide p1, p0, Lmiui/android/animation/internal/AnimRunner;->mLastFrameTime:J */
/* .line 221 */
/* const-wide/16 v0, 0x0 */
/* .local v0, "deltaT":J */
/* .line 223 */
} // .end local v0 # "deltaT":J
} // :cond_0
/* sub-long v0, p1, v0 */
/* .line 224 */
/* .restart local v0 # "deltaT":J */
/* iput-wide p1, p0, Lmiui/android/animation/internal/AnimRunner;->mLastFrameTime:J */
/* .line 226 */
} // :goto_0
/* iget v2, p0, Lmiui/android/animation/internal/AnimRunner;->mRecordCount:I */
/* rem-int/lit8 v3, v2, 0x5 */
/* .line 227 */
/* .local v3, "idx":I */
v4 = this.mDeltaRecord;
/* aput-wide v0, v4, v3 */
/* .line 228 */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, p0, Lmiui/android/animation/internal/AnimRunner;->mRecordCount:I */
/* .line 229 */
/* invoke-direct {p0, v0, v1}, Lmiui/android/animation/internal/AnimRunner;->calculateAverageDelta(J)J */
/* move-result-wide v4 */
/* iput-wide v4, p0, Lmiui/android/animation/internal/AnimRunner;->mAverageDelta:J */
/* .line 230 */
return;
} // .end method
/* # virtual methods */
public void cancel ( miui.android.animation.IAnimTarget p0, java.lang.String...p1 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "propertyNames" # [Ljava/lang/String; */
/* .line 152 */
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
/* new-instance v1, Lmiui/android/animation/internal/AnimOperationInfo; */
int v2 = 4; // const/4 v2, 0x4
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v1, p1, v2, p2, v3}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V */
(( miui.android.animation.internal.RunnerHandler ) v0 ).setOperation ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V
/* .line 153 */
return;
} // .end method
public void cancel ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 156 */
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
/* new-instance v1, Lmiui/android/animation/internal/AnimOperationInfo; */
int v2 = 4; // const/4 v2, 0x4
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v1, p1, v2, v3, p2}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V */
(( miui.android.animation.internal.RunnerHandler ) v0 ).setOperation ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V
/* .line 157 */
return;
} // .end method
public Boolean doAnimationFrame ( Long p0 ) {
/* .locals 6 */
/* .param p1, "frameTime" # J */
/* .line 123 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/internal/AnimRunner;->updateRunningTime(J)V */
/* .line 124 */
/* iget-boolean v0, p0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 125 */
miui.android.animation.Folme .getTargets ( );
/* .line 126 */
/* .local v0, "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 127 */
/* .local v1, "animCounnt":I */
v3 = } // :goto_0
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lmiui/android/animation/IAnimTarget; */
/* .line 128 */
/* .local v3, "target":Lmiui/android/animation/IAnimTarget; */
v5 = this.animManager;
/* new-array v4, v4, [Lmiui/android/animation/property/FloatProperty; */
v4 = (( miui.android.animation.internal.AnimManager ) v5 ).isAnimRunning ( v4 ); // invoke-virtual {v5, v4}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* .line 129 */
/* .local v4, "isAnimRunning":Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 130 */
v5 = this.animManager;
v5 = (( miui.android.animation.internal.AnimManager ) v5 ).getTotalAnimCount ( ); // invoke-virtual {v5}, Lmiui/android/animation/internal/AnimManager;->getTotalAnimCount()I
/* add-int/2addr v1, v5 */
/* .line 132 */
} // .end local v3 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v4 # "isAnimRunning":Z
} // :cond_0
/* .line 133 */
} // :cond_1
/* const/16 v2, 0x1f4 */
/* if-le v1, v2, :cond_2 */
int v4 = 1; // const/4 v4, 0x1
} // :cond_2
/* move v2, v4 */
/* .line 135 */
/* .local v2, "toPage":Z */
v3 = /* if-nez v2, :cond_3 */
/* if-lez v3, :cond_3 */
/* .line 136 */
miui.android.animation.internal.AnimRunner .updateAnimRunner ( v0,v2 );
/* .line 139 */
} // :cond_3
v3 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
(( miui.android.animation.internal.RunnerHandler ) v3 ).obtainMessage ( ); // invoke-virtual {v3}, Lmiui/android/animation/internal/RunnerHandler;->obtainMessage()Landroid/os/Message;
/* .line 140 */
/* .local v4, "msg":Landroid/os/Message; */
int v5 = 3; // const/4 v5, 0x3
/* iput v5, v4, Landroid/os/Message;->what:I */
/* .line 141 */
java.lang.Boolean .valueOf ( v2 );
this.obj = v5;
/* .line 142 */
(( miui.android.animation.internal.RunnerHandler ) v3 ).sendMessage ( v4 ); // invoke-virtual {v3, v4}, Lmiui/android/animation/internal/RunnerHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 144 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 145 */
miui.android.animation.internal.AnimRunner .updateAnimRunner ( v0,v2 );
/* .line 148 */
} // .end local v0 # "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;"
} // .end local v1 # "animCounnt":I
} // .end local v2 # "toPage":Z
} // .end local v4 # "msg":Landroid/os/Message;
} // :cond_4
/* iget-boolean v0, p0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z */
} // .end method
void end ( ) {
/* .locals 2 */
/* .line 197 */
android.os.Looper .myLooper ( );
android.os.Looper .getMainLooper ( );
/* if-ne v0, v1, :cond_0 */
/* .line 198 */
miui.android.animation.internal.AnimRunner .endAnimation ( );
/* .line 200 */
} // :cond_0
v0 = miui.android.animation.internal.AnimRunner.sMainHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 202 */
} // :goto_0
return;
} // .end method
public void end ( miui.android.animation.IAnimTarget p0, java.lang.String...p1 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "propertyNames" # [Ljava/lang/String; */
/* .line 160 */
v0 = miui.android.animation.utils.CommonUtils .isArrayEmpty ( p2 );
int v1 = 3; // const/4 v1, 0x3
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 161 */
v0 = this.handler;
(( miui.android.animation.internal.TargetHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/internal/TargetHandler;->sendEmptyMessage(I)Z
/* .line 163 */
} // :cond_0
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
/* new-instance v2, Lmiui/android/animation/internal/AnimOperationInfo; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p1, v1, p2, v3}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V */
(( miui.android.animation.internal.RunnerHandler ) v0 ).setOperation ( v2 ); // invoke-virtual {v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V
/* .line 164 */
return;
} // .end method
public void end ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 4 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 167 */
v0 = miui.android.animation.utils.CommonUtils .isArrayEmpty ( p2 );
int v1 = 3; // const/4 v1, 0x3
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 168 */
v0 = this.handler;
(( miui.android.animation.internal.TargetHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/internal/TargetHandler;->sendEmptyMessage(I)Z
/* .line 170 */
} // :cond_0
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
/* new-instance v2, Lmiui/android/animation/internal/AnimOperationInfo; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p1, v1, v3, p2}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V */
(( miui.android.animation.internal.RunnerHandler ) v0 ).setOperation ( v2 ); // invoke-virtual {v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V
/* .line 171 */
return;
} // .end method
public Long getAverageDelta ( ) {
/* .locals 2 */
/* .line 205 */
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimRunner;->mAverageDelta:J */
/* return-wide v0 */
} // .end method
public void run ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1, miui.android.animation.controller.AnimState p2, miui.android.animation.base.AnimConfigLink p3 ) {
/* .locals 1 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "from" # Lmiui/android/animation/controller/AnimState; */
/* .param p3, "to" # Lmiui/android/animation/controller/AnimState; */
/* .param p4, "config" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 175 */
/* new-instance v0, Lmiui/android/animation/internal/TransitionInfo; */
/* invoke-direct {v0, p1, p2, p3, p4}, Lmiui/android/animation/internal/TransitionInfo;-><init>(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V */
/* .line 176 */
/* .local v0, "info":Lmiui/android/animation/internal/TransitionInfo; */
(( miui.android.animation.internal.AnimRunner ) p0 ).run ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/internal/AnimRunner;->run(Lmiui/android/animation/internal/TransitionInfo;)V
/* .line 177 */
return;
} // .end method
public void run ( miui.android.animation.internal.TransitionInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .line 180 */
v0 = this.target;
/* new-instance v1, Lmiui/android/animation/internal/AnimRunner$2; */
/* invoke-direct {v1, p0, p1}, Lmiui/android/animation/internal/AnimRunner$2;-><init>(Lmiui/android/animation/internal/AnimRunner;Lmiui/android/animation/internal/TransitionInfo;)V */
(( miui.android.animation.IAnimTarget ) v0 ).executeOnInitialized ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->executeOnInitialized(Ljava/lang/Runnable;)V
/* .line 186 */
return;
} // .end method
void start ( ) {
/* .locals 2 */
/* .line 189 */
android.os.Looper .myLooper ( );
android.os.Looper .getMainLooper ( );
/* if-ne v0, v1, :cond_0 */
/* .line 190 */
miui.android.animation.internal.AnimRunner .startAnimRunner ( );
/* .line 192 */
} // :cond_0
v0 = miui.android.animation.internal.AnimRunner.sMainHandler;
int v1 = 0; // const/4 v1, 0x0
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 194 */
} // :goto_0
return;
} // .end method
