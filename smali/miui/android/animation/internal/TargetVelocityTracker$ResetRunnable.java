class miui.android.animation.internal.TargetVelocityTracker$ResetRunnable implements java.lang.Runnable {
	 /* .source "TargetVelocityTracker.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/internal/TargetVelocityTracker; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "ResetRunnable" */
} // .end annotation
/* # instance fields */
miui.android.animation.internal.TargetVelocityTracker$MonitorInfo mMonitorInfo;
miui.android.animation.property.FloatProperty mProperty;
java.lang.ref.WeakReference mTargetRef;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
 miui.android.animation.internal.TargetVelocityTracker$ResetRunnable ( ) {
/* .locals 0 */
/* .param p1, "monitorInfo" # Lmiui/android/animation/internal/TargetVelocityTracker$MonitorInfo; */
/* .line 28 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 29 */
this.mMonitorInfo = p1;
/* .line 30 */
return;
} // .end method
/* # virtual methods */
void post ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1 ) {
/* .locals 3 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 33 */
v0 = this.handler;
(( miui.android.animation.internal.TargetHandler ) v0 ).removeCallbacks ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/internal/TargetHandler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 34 */
v0 = this.mTargetRef;
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* if-eq v0, p1, :cond_1 */
/* .line 35 */
} // :cond_0
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mTargetRef = v0;
/* .line 37 */
} // :cond_1
this.mProperty = p2;
/* .line 38 */
v0 = this.handler;
/* const-wide/16 v1, 0x258 */
(( miui.android.animation.internal.TargetHandler ) v0 ).postDelayed ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Lmiui/android/animation/internal/TargetHandler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 39 */
return;
} // .end method
public void run ( ) {
/* .locals 4 */
/* .line 43 */
v0 = this.mTargetRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/IAnimTarget; */
/* .line 44 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 45 */
v1 = this.mProperty;
/* filled-new-array {v1}, [Lmiui/android/animation/property/FloatProperty; */
v1 = (( miui.android.animation.IAnimTarget ) v0 ).isAnimRunning ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* if-nez v1, :cond_0 */
/* .line 46 */
v1 = this.mProperty;
/* const-wide/16 v2, 0x0 */
(( miui.android.animation.IAnimTarget ) v0 ).setVelocity ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/IAnimTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V
/* .line 48 */
} // :cond_0
v1 = this.mMonitorInfo;
v1 = this.monitor;
(( miui.android.animation.utils.VelocityMonitor ) v1 ).clear ( ); // invoke-virtual {v1}, Lmiui/android/animation/utils/VelocityMonitor;->clear()V
/* .line 50 */
} // :cond_1
return;
} // .end method
