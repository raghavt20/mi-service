class miui.android.animation.internal.RunnerHandler extends android.os.Handler {
	 /* .source "RunnerHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/internal/RunnerHandler$SetToInfo; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer ANIM_MSG_RUNNER_RUN;
public static final Integer ANIM_MSG_SETUP;
public static final Integer ANIM_MSG_SET_TO;
public static final Integer ANIM_MSG_UPDATE;
/* # instance fields */
private final java.util.List mDelList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mFrameCount;
private Boolean mIsTaskRunning;
private Long mLastRun;
private final java.util.Map mOpMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* "Lmiui/android/animation/internal/AnimOperationInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mRunnerStart;
private final mSplitInfo;
private Boolean mStart;
private final java.util.List mTaskList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/internal/AnimTask;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mTotalT;
private final java.util.List mTransList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mTransMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Set runningTarget;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.internal.RunnerHandler ( ) {
/* .locals 2 */
/* .param p1, "looper" # Landroid/os/Looper; */
/* .line 66 */
/* invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 39 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.runningTarget = v0;
/* .line 41 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mOpMap = v0;
/* .line 48 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mTransMap = v0;
/* .line 49 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTaskList = v0;
/* .line 50 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDelList = v0;
/* .line 52 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTransList = v0;
/* .line 56 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mLastRun:J */
/* .line 57 */
/* iput-wide v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
/* .line 58 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I */
/* .line 63 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [I */
this.mSplitInfo = v0;
/* .line 67 */
return;
} // .end method
private void addAnimTask ( java.util.List p0, Integer p1, Integer p2 ) {
/* .locals 7 */
/* .param p2, "singleCount" # I */
/* .param p3, "splitCount" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;II)V" */
/* } */
} // .end annotation
/* .line 350 */
/* .local p1, "transList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/internal/TransitionInfo;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 351 */
/* .local v1, "info":Lmiui/android/animation/internal/TransitionInfo; */
v2 = this.animTasks;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lmiui/android/animation/internal/AnimTask; */
/* .line 352 */
/* .local v3, "task":Lmiui/android/animation/internal/AnimTask; */
/* invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->getTaskOfMinCount()Lmiui/android/animation/internal/AnimTask; */
/* .line 353 */
/* .local v4, "curTask":Lmiui/android/animation/internal/AnimTask; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = v5 = this.mTaskList;
/* if-ge v5, p3, :cond_0 */
/* .line 354 */
v5 = (( miui.android.animation.internal.AnimTask ) v4 ).getTotalAnimCount ( ); // invoke-virtual {v4}, Lmiui/android/animation/internal/AnimTask;->getTotalAnimCount()I
v6 = (( miui.android.animation.internal.AnimTask ) v3 ).getAnimCount ( ); // invoke-virtual {v3}, Lmiui/android/animation/internal/AnimTask;->getAnimCount()I
/* add-int/2addr v5, v6 */
/* if-le v5, p2, :cond_0 */
/* .line 357 */
} // :cond_0
(( miui.android.animation.internal.AnimTask ) v4 ).addToTail ( v3 ); // invoke-virtual {v4, v3}, Lmiui/android/animation/internal/AnimTask;->addToTail(Lmiui/android/animation/utils/LinkNode;)V
/* .line 355 */
} // :cond_1
} // :goto_2
v5 = this.mTaskList;
/* .line 359 */
} // .end local v3 # "task":Lmiui/android/animation/internal/AnimTask;
} // .end local v4 # "curTask":Lmiui/android/animation/internal/AnimTask;
} // :goto_3
/* .line 360 */
} // .end local v1 # "info":Lmiui/android/animation/internal/TransitionInfo;
} // :cond_2
/* .line 361 */
} // :cond_3
return;
} // .end method
private void addToMap ( miui.android.animation.IAnimTarget p0, miui.android.animation.utils.LinkNode p1, java.util.Map p2 ) {
/* .locals 1 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Lmiui/android/animation/utils/LinkNode;", */
/* ">(", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* "TT;", */
/* "Ljava/util/Map<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* "TT;>;)V" */
/* } */
} // .end annotation
/* .line 154 */
/* .local p2, "node":Lmiui/android/animation/utils/LinkNode;, "TT;" */
/* .local p3, "map":Ljava/util/Map;, "Ljava/util/Map<Lmiui/android/animation/IAnimTarget;TT;>;" */
/* check-cast v0, Lmiui/android/animation/utils/LinkNode; */
/* .line 155 */
/* .local v0, "head":Lmiui/android/animation/utils/LinkNode;, "TT;" */
/* if-nez v0, :cond_0 */
/* .line 156 */
/* .line 158 */
} // :cond_0
(( miui.android.animation.utils.LinkNode ) v0 ).addToTail ( p2 ); // invoke-virtual {v0, p2}, Lmiui/android/animation/utils/LinkNode;->addToTail(Lmiui/android/animation/utils/LinkNode;)V
/* .line 160 */
} // :goto_0
return;
} // .end method
private static void doSetOperation ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimStats p1, miui.android.animation.listener.UpdateInfo p2, miui.android.animation.internal.AnimOperationInfo p3 ) {
/* .locals 5 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "stats" # Lmiui/android/animation/internal/AnimStats; */
/* .param p2, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .param p3, "opInfo" # Lmiui/android/animation/internal/AnimOperationInfo; */
/* .line 299 */
v0 = this.animInfo;
/* iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B */
/* .line 300 */
/* .local v0, "op":B */
v1 = miui.android.animation.internal.AnimTask .isRunning ( v0 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B */
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.propList;
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = this.propList;
v2 = this.property;
v1 = /* .line 302 */
if ( v1 != null) { // if-eqz v1, :cond_4
} // :cond_0
v1 = this.animInfo;
/* iget-byte v1, v1, Lmiui/android/animation/internal/AnimInfo;->op:B */
/* .line 303 */
v1 = miui.android.animation.internal.AnimTask .isRunning ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 304 */
/* iget v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I */
/* .line 305 */
/* iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_2 */
/* .line 306 */
v1 = this.animInfo;
/* iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
/* const-wide v3, 0x7fefffffffffffffL # Double.MAX_VALUE */
/* cmpl-double v1, v1, v3 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 307 */
v1 = this.animInfo;
v2 = this.animInfo;
/* iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
/* iput-wide v2, v1, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* .line 309 */
} // :cond_1
v1 = this.animStats;
/* iget v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* .line 310 */
/* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p1, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* .line 311 */
} // :cond_2
/* iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B */
int v2 = 4; // const/4 v2, 0x4
/* if-ne v1, v2, :cond_3 */
/* .line 312 */
v1 = this.animStats;
/* iget v2, v1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, v1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* .line 313 */
/* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* add-int/lit8 v1, v1, 0x1 */
/* iput v1, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* .line 315 */
} // :cond_3
} // :goto_0
/* iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B */
(( miui.android.animation.listener.UpdateInfo ) p2 ).setOp ( v1 ); // invoke-virtual {p2, v1}, Lmiui/android/animation/listener/UpdateInfo;->setOp(B)V
/* .line 316 */
miui.android.animation.internal.TransitionInfo .decreaseStartCountForDelayAnim ( p0,p1,p2,v0 );
/* .line 318 */
} // :cond_4
return;
} // .end method
private void doSetup ( ) {
/* .locals 4 */
/* .line 385 */
v0 = this.mTransMap;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 386 */
/* .local v1, "info":Lmiui/android/animation/internal/TransitionInfo; */
v2 = this.runningTarget;
v3 = this.target;
/* .line 387 */
/* move-object v2, v1 */
/* .line 389 */
/* .local v2, "head":Lmiui/android/animation/internal/TransitionInfo; */
} // :cond_0
v3 = this.target;
v3 = this.animManager;
(( miui.android.animation.internal.AnimManager ) v3 ).setupTransition ( v2 ); // invoke-virtual {v3, v2}, Lmiui/android/animation/internal/AnimManager;->setupTransition(Lmiui/android/animation/internal/TransitionInfo;)V
/* .line 390 */
(( miui.android.animation.internal.TransitionInfo ) v2 ).remove ( ); // invoke-virtual {v2}, Lmiui/android/animation/internal/TransitionInfo;->remove()Lmiui/android/animation/utils/LinkNode;
/* move-object v2, v3 */
/* check-cast v2, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 391 */
/* if-nez v2, :cond_0 */
/* .line 392 */
} // .end local v1 # "info":Lmiui/android/animation/internal/TransitionInfo;
} // .end local v2 # "head":Lmiui/android/animation/internal/TransitionInfo;
/* .line 393 */
} // :cond_1
v0 = this.mTransMap;
/* .line 394 */
/* iget-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z */
/* if-nez v0, :cond_2 */
/* .line 395 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z */
/* .line 396 */
miui.android.animation.internal.AnimRunner .getInst ( );
(( miui.android.animation.internal.AnimRunner ) v0 ).start ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimRunner;->start()V
/* .line 398 */
} // :cond_2
return;
} // .end method
private miui.android.animation.internal.AnimTask getTaskOfMinCount ( ) {
/* .locals 5 */
/* .line 364 */
int v0 = 0; // const/4 v0, 0x0
/* .line 365 */
/* .local v0, "state":Lmiui/android/animation/internal/AnimTask; */
/* const v1, 0x7fffffff */
/* .line 366 */
/* .local v1, "min":I */
v2 = this.mTaskList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lmiui/android/animation/internal/AnimTask; */
/* .line 367 */
/* .local v3, "s":Lmiui/android/animation/internal/AnimTask; */
v4 = (( miui.android.animation.internal.AnimTask ) v3 ).getTotalAnimCount ( ); // invoke-virtual {v3}, Lmiui/android/animation/internal/AnimTask;->getTotalAnimCount()I
/* .line 368 */
/* .local v4, "totalCount":I */
/* if-ge v4, v1, :cond_0 */
/* .line 369 */
/* move v1, v4 */
/* .line 370 */
/* move-object v0, v3 */
/* .line 372 */
} // .end local v3 # "s":Lmiui/android/animation/internal/AnimTask;
} // .end local v4 # "totalCount":I
} // :cond_0
/* .line 373 */
} // :cond_1
} // .end method
private Integer getTotalAnimCount ( ) {
/* .locals 4 */
/* .line 377 */
int v0 = 0; // const/4 v0, 0x0
/* .line 378 */
/* .local v0, "count":I */
v1 = this.runningTarget;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lmiui/android/animation/IAnimTarget; */
/* .line 379 */
/* .local v2, "target":Lmiui/android/animation/IAnimTarget; */
v3 = this.animManager;
v3 = (( miui.android.animation.internal.AnimManager ) v3 ).getTotalAnimCount ( ); // invoke-virtual {v3}, Lmiui/android/animation/internal/AnimManager;->getTotalAnimCount()I
/* add-int/2addr v0, v3 */
/* .line 380 */
} // .end local v2 # "target":Lmiui/android/animation/IAnimTarget;
/* .line 381 */
} // :cond_0
} // .end method
private static Boolean handleSetTo ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimStats p1, miui.android.animation.listener.UpdateInfo p2 ) {
/* .locals 3 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "stats" # Lmiui/android/animation/internal/AnimStats; */
/* .param p2, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .line 285 */
v0 = miui.android.animation.internal.AnimValueUtils .handleSetToValue ( p2 );
/* if-nez v0, :cond_0 */
/* .line 286 */
int v0 = 0; // const/4 v0, 0x0
/* .line 288 */
} // :cond_0
v0 = this.animInfo;
/* iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B */
v0 = miui.android.animation.internal.AnimTask .isRunning ( v0 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 289 */
v0 = this.animStats;
/* iget v2, v0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* add-int/2addr v2, v1 */
/* iput v2, v0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* .line 290 */
/* iget v0, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* add-int/2addr v0, v1 */
/* iput v0, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* .line 291 */
int v0 = 4; // const/4 v0, 0x4
(( miui.android.animation.listener.UpdateInfo ) p2 ).setOp ( v0 ); // invoke-virtual {p2, v0}, Lmiui/android/animation/listener/UpdateInfo;->setOp(B)V
/* .line 292 */
v0 = this.animInfo;
/* iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B */
miui.android.animation.internal.TransitionInfo .decreaseStartCountForDelayAnim ( p0,p1,p2,v0 );
/* .line 294 */
} // :cond_1
} // .end method
private static void handleUpdate ( miui.android.animation.internal.TransitionInfo p0, miui.android.animation.internal.AnimOperationInfo p1, miui.android.animation.internal.AnimStats p2 ) {
/* .locals 8 */
/* .param p0, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .param p1, "opInfo" # Lmiui/android/animation/internal/AnimOperationInfo; */
/* .param p2, "stats" # Lmiui/android/animation/internal/AnimStats; */
/* .line 264 */
v0 = this.target;
v0 = this.animManager;
v0 = this.mStartAnim;
v0 = v1 = this.key;
/* .line 265 */
/* .local v0, "isRunningAnim":Z */
v1 = this.animTasks;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lmiui/android/animation/internal/AnimTask; */
/* .line 266 */
/* .local v2, "task":Lmiui/android/animation/internal/AnimTask; */
v3 = this.updateList;
/* .line 267 */
/* .local v3, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;" */
/* iget v4, v2, Lmiui/android/animation/internal/AnimTask;->startPos:I */
/* .local v4, "i":I */
v5 = (( miui.android.animation.internal.AnimTask ) v2 ).getAnimCount ( ); // invoke-virtual {v2}, Lmiui/android/animation/internal/AnimTask;->getAnimCount()I
/* add-int/2addr v5, v4 */
/* .local v5, "n":I */
} // :goto_1
/* if-ge v4, v5, :cond_1 */
/* .line 268 */
/* check-cast v6, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 269 */
/* .local v6, "update":Lmiui/android/animation/listener/UpdateInfo; */
v7 = miui.android.animation.internal.RunnerHandler .handleSetTo ( v2,p2,v6 );
/* if-nez v7, :cond_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 270 */
miui.android.animation.internal.RunnerHandler .doSetOperation ( v2,p2,v6,p1 );
/* .line 267 */
} // .end local v6 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 273 */
} // .end local v2 # "task":Lmiui/android/animation/internal/AnimTask;
} // .end local v3 # "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
} // .end local v4 # "i":I
} // .end local v5 # "n":I
} // :cond_1
/* .line 275 */
} // :cond_2
v1 = this.target;
v1 = this.animManager;
v1 = this.mStartAnim;
v1 = v2 = this.key;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 276 */
v1 = (( miui.android.animation.internal.AnimStats ) p2 ).isRunning ( ); // invoke-virtual {p2}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget v1, p2, Lmiui/android/animation/internal/AnimStats;->updateCount:I */
/* if-lez v1, :cond_3 */
/* .line 277 */
v1 = miui.android.animation.internal.TransitionInfo.sMap;
/* iget v2, p0, Lmiui/android/animation/internal/TransitionInfo;->id:I */
java.lang.Integer .valueOf ( v2 );
/* .line 278 */
v1 = this.target;
v1 = this.handler;
/* iget v2, p0, Lmiui/android/animation/internal/TransitionInfo;->id:I */
/* .line 279 */
int v3 = 0; // const/4 v3, 0x0
(( miui.android.animation.internal.TargetHandler ) v1 ).obtainMessage ( v3, v2, v3 ); // invoke-virtual {v1, v3, v2, v3}, Lmiui/android/animation/internal/TargetHandler;->obtainMessage(III)Landroid/os/Message;
/* .line 280 */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 282 */
} // :cond_3
return;
} // .end method
private Boolean isInfoInTransMap ( miui.android.animation.internal.TransitionInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .line 210 */
v0 = this.mTransMap;
v1 = this.target;
/* check-cast v0, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 211 */
/* .local v0, "node":Lmiui/android/animation/internal/TransitionInfo; */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 212 */
/* if-ne v0, p1, :cond_0 */
/* .line 213 */
int v1 = 1; // const/4 v1, 0x1
/* .line 215 */
} // :cond_0
v1 = this.next;
/* move-object v0, v1 */
/* check-cast v0, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 217 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void onSetTo ( miui.android.animation.internal.RunnerHandler$SetToInfo p0 ) {
/* .locals 10 */
/* .param p1, "setInfo" # Lmiui/android/animation/internal/RunnerHandler$SetToInfo; */
/* .line 129 */
v0 = this.target;
/* instance-of v0, v0, Lmiui/android/animation/ViewTarget; */
/* .line 130 */
/* .local v0, "isViewTarget":Z */
v1 = this.state;
(( miui.android.animation.controller.AnimState ) v1 ).keySet ( ); // invoke-virtual {v1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 131 */
/* .local v2, "key":Ljava/lang/Object; */
v3 = this.state;
(( miui.android.animation.controller.AnimState ) v3 ).getProperty ( v2 ); // invoke-virtual {v3, v2}, Lmiui/android/animation/controller/AnimState;->getProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
/* .line 132 */
/* .local v3, "property":Lmiui/android/animation/property/FloatProperty; */
v4 = this.target;
v4 = this.animManager;
v4 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 133 */
/* .local v4, "update":Lmiui/android/animation/listener/UpdateInfo; */
v5 = this.state;
v6 = this.target;
(( miui.android.animation.controller.AnimState ) v5 ).get ( v6, v3 ); // invoke-virtual {v5, v6, v3}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v5 */
/* .line 134 */
/* .local v5, "value":D */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 135 */
v7 = this.animInfo;
/* iput-wide v5, v7, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
/* .line 136 */
/* if-nez v0, :cond_2 */
/* .line 137 */
v7 = this.target;
(( miui.android.animation.listener.UpdateInfo ) v4 ).setTargetValue ( v7 ); // invoke-virtual {v4, v7}, Lmiui/android/animation/listener/UpdateInfo;->setTargetValue(Lmiui/android/animation/IAnimTarget;)V
/* .line 140 */
} // :cond_0
/* instance-of v7, v3, Lmiui/android/animation/property/IIntValueProperty; */
if ( v7 != null) { // if-eqz v7, :cond_1
/* .line 141 */
v7 = this.target;
/* move-object v8, v3 */
/* check-cast v8, Lmiui/android/animation/property/IIntValueProperty; */
/* double-to-int v9, v5 */
(( miui.android.animation.IAnimTarget ) v7 ).setIntValue ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
/* .line 143 */
} // :cond_1
v7 = this.target;
/* double-to-float v8, v5 */
(( miui.android.animation.IAnimTarget ) v7 ).setValue ( v3, v8 ); // invoke-virtual {v7, v3, v8}, Lmiui/android/animation/IAnimTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 146 */
} // .end local v2 # "key":Ljava/lang/Object;
} // .end local v3 # "property":Lmiui/android/animation/property/FloatProperty;
} // .end local v4 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // .end local v5 # "value":D
} // :cond_2
} // :goto_1
/* .line 147 */
} // :cond_3
v1 = this.target;
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Lmiui/android/animation/property/FloatProperty; */
v1 = (( miui.android.animation.IAnimTarget ) v1 ).isAnimRunning ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/animation/IAnimTarget;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* if-nez v1, :cond_4 */
/* .line 148 */
v1 = this.target;
v1 = this.animManager;
v1 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 150 */
} // :cond_4
return;
} // .end method
private void runAnim ( Long p0, Long p1, Boolean p2 ) {
/* .locals 18 */
/* .param p1, "now" # J */
/* .param p3, "deltaT" # J */
/* .param p5, "toPage" # Z */
/* .line 321 */
/* move-object/from16 v0, p0 */
v1 = v1 = this.runningTarget;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 322 */
/* invoke-direct/range {p0 ..p0}, Lmiui/android/animation/internal/RunnerHandler;->stopAnimRunner()V */
/* .line 323 */
return;
/* .line 325 */
} // :cond_0
/* move-wide/from16 v1, p1 */
/* iput-wide v1, v0, Lmiui/android/animation/internal/RunnerHandler;->mLastRun:J */
/* .line 326 */
miui.android.animation.internal.AnimRunner .getInst ( );
(( miui.android.animation.internal.AnimRunner ) v3 ).getAverageDelta ( ); // invoke-virtual {v3}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J
/* move-result-wide v3 */
/* .line 327 */
/* .local v3, "averageDelta":J */
/* iget v5, v0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I */
int v6 = 1; // const/4 v6, 0x1
/* if-ne v5, v6, :cond_1 */
/* const-wide/16 v7, 0x2 */
/* mul-long/2addr v7, v3 */
/* cmp-long v7, p3, v7 */
/* if-lez v7, :cond_1 */
/* .line 328 */
/* move-wide v7, v3 */
} // .end local p3 # "deltaT":J
/* .local v7, "deltaT":J */
/* .line 330 */
} // .end local v7 # "deltaT":J
/* .restart local p3 # "deltaT":J */
} // :cond_1
/* move-wide/from16 v7, p3 */
} // .end local p3 # "deltaT":J
/* .restart local v7 # "deltaT":J */
} // :goto_0
/* iget-wide v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
/* add-long/2addr v9, v7 */
/* iput-wide v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
/* .line 331 */
/* add-int/2addr v5, v6 */
/* iput v5, v0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I */
/* .line 332 */
v5 = /* invoke-direct/range {p0 ..p0}, Lmiui/android/animation/internal/RunnerHandler;->getTotalAnimCount()I */
/* .line 333 */
/* .local v5, "animCount":I */
v9 = this.mSplitInfo;
miui.android.animation.internal.ThreadPoolUtil .getSplitCount ( v5,v9 );
/* .line 334 */
v9 = this.mSplitInfo;
int v10 = 0; // const/4 v10, 0x0
/* aget v15, v9, v10 */
/* .line 335 */
/* .local v15, "splitCount":I */
/* aget v14, v9, v6 */
/* .line 336 */
/* .local v14, "singleCount":I */
v9 = this.runningTarget;
v10 = } // :goto_1
if ( v10 != null) { // if-eqz v10, :cond_2
/* check-cast v10, Lmiui/android/animation/IAnimTarget; */
/* .line 337 */
/* .local v10, "target":Lmiui/android/animation/IAnimTarget; */
v11 = this.animManager;
v12 = this.mTransList;
(( miui.android.animation.internal.AnimManager ) v11 ).getTransitionInfos ( v12 ); // invoke-virtual {v11, v12}, Lmiui/android/animation/internal/AnimManager;->getTransitionInfos(Ljava/util/List;)V
/* .line 338 */
} // .end local v10 # "target":Lmiui/android/animation/IAnimTarget;
/* .line 339 */
} // :cond_2
v9 = this.mTransList;
/* invoke-direct {v0, v9, v14, v15}, Lmiui/android/animation/internal/RunnerHandler;->addAnimTask(Ljava/util/List;II)V */
/* .line 340 */
v9 = v9 = this.mTaskList;
/* xor-int/2addr v6, v9 */
/* iput-boolean v6, v0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z */
/* .line 341 */
v6 = miui.android.animation.internal.AnimTask.sTaskCount;
v9 = v9 = this.mTaskList;
(( java.util.concurrent.atomic.AtomicInteger ) v6 ).set ( v9 ); // invoke-virtual {v6, v9}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
/* .line 342 */
v6 = this.mTaskList;
v9 = } // :goto_2
if ( v9 != null) { // if-eqz v9, :cond_3
/* move-object/from16 v16, v9 */
/* check-cast v16, Lmiui/android/animation/internal/AnimTask; */
/* .line 343 */
/* .local v16, "task":Lmiui/android/animation/internal/AnimTask; */
/* iget-wide v10, v0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
/* move-object/from16 v9, v16 */
/* move-wide v12, v7 */
/* move/from16 v17, v14 */
} // .end local v14 # "singleCount":I
/* .local v17, "singleCount":I */
/* move/from16 v14, p5 */
/* invoke-virtual/range {v9 ..v14}, Lmiui/android/animation/internal/AnimTask;->start(JJZ)V */
/* .line 344 */
} // .end local v16 # "task":Lmiui/android/animation/internal/AnimTask;
/* move/from16 v14, v17 */
/* .line 345 */
} // .end local v17 # "singleCount":I
/* .restart local v14 # "singleCount":I */
} // :cond_3
v6 = this.mTransList;
/* .line 346 */
v6 = this.mTaskList;
/* .line 347 */
return;
} // .end method
private Boolean setupWaitTrans ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 3 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 201 */
v0 = this.animManager;
v0 = this.mWaitState;
(( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).poll ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 202 */
/* .local v0, "info":Lmiui/android/animation/internal/TransitionInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 203 */
v1 = this.target;
v2 = this.mTransMap;
/* invoke-direct {p0, v1, v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->addToMap(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/utils/LinkNode;Ljava/util/Map;)V */
/* .line 204 */
int v1 = 1; // const/4 v1, 0x1
/* .line 206 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
private void stopAnimRunner ( ) {
/* .locals 3 */
/* .line 163 */
/* iget-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 164 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 165 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "total time = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "frame count = "; // const-string v2, "frame count = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* filled-new-array {v0, v1}, [Ljava/lang/Object; */
final String v1 = "RunnerHandler.stopAnimRunner"; // const-string v1, "RunnerHandler.stopAnimRunner"
miui.android.animation.utils.LogUtils .debug ( v1,v0 );
/* .line 170 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z */
/* .line 171 */
/* iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z */
/* .line 172 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
/* .line 173 */
/* iput v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I */
/* .line 174 */
miui.android.animation.internal.AnimRunner .getInst ( );
(( miui.android.animation.internal.AnimRunner ) v0 ).end ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimRunner;->end()V
/* .line 176 */
} // :cond_1
return;
} // .end method
private void updateAnim ( ) {
/* .locals 4 */
/* .line 179 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z */
/* .line 180 */
int v0 = 0; // const/4 v0, 0x0
/* .line 181 */
/* .local v0, "isRunning":Z */
v1 = this.runningTarget;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lmiui/android/animation/IAnimTarget; */
/* .line 182 */
/* .local v2, "target":Lmiui/android/animation/IAnimTarget; */
v3 = this.mTransList;
v3 = /* invoke-direct {p0, v2, v3}, Lmiui/android/animation/internal/RunnerHandler;->updateTarget(Lmiui/android/animation/IAnimTarget;Ljava/util/List;)Z */
/* if-nez v3, :cond_1 */
v3 = /* invoke-direct {p0, v2}, Lmiui/android/animation/internal/RunnerHandler;->setupWaitTrans(Lmiui/android/animation/IAnimTarget;)Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 185 */
} // :cond_0
v3 = this.mDelList;
/* .line 183 */
} // :cond_1
} // :goto_1
int v0 = 1; // const/4 v0, 0x1
/* .line 187 */
} // :goto_2
v3 = this.mTransList;
/* .line 188 */
} // .end local v2 # "target":Lmiui/android/animation/IAnimTarget;
/* .line 189 */
} // :cond_2
v1 = this.runningTarget;
v2 = this.mDelList;
/* .line 190 */
v1 = this.mDelList;
/* .line 191 */
v1 = v1 = this.mTransMap;
/* if-nez v1, :cond_3 */
/* .line 192 */
/* invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->doSetup()V */
/* .line 193 */
int v0 = 1; // const/4 v0, 0x1
/* .line 195 */
} // :cond_3
/* if-nez v0, :cond_4 */
/* .line 196 */
/* invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->stopAnimRunner()V */
/* .line 198 */
} // :cond_4
return;
} // .end method
private Boolean updateTarget ( miui.android.animation.IAnimTarget p0, java.util.List p1 ) {
/* .locals 16 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;)Z" */
/* } */
} // .end annotation
/* .line 221 */
/* .local p2, "transList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/internal/TransitionInfo;>;" */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
v2 = this.animManager;
/* move-object/from16 v3, p2 */
(( miui.android.animation.internal.AnimManager ) v2 ).getTransitionInfos ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimManager;->getTransitionInfos(Ljava/util/List;)V
/* .line 222 */
int v2 = 0; // const/4 v2, 0x0
/* .line 223 */
/* .local v2, "runCount":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 224 */
/* .local v4, "animStartAfterCancel":I */
v5 = this.mOpMap;
/* check-cast v5, Lmiui/android/animation/internal/AnimOperationInfo; */
/* .line 225 */
/* .local v5, "opInfo":Lmiui/android/animation/internal/AnimOperationInfo; */
/* invoke-interface/range {p2 ..p2}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
v7 = } // :goto_0
int v8 = 0; // const/4 v8, 0x0
int v9 = 1; // const/4 v9, 0x1
if ( v7 != null) { // if-eqz v7, :cond_7
/* check-cast v7, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 226 */
/* .local v7, "info":Lmiui/android/animation/internal/TransitionInfo; */
v10 = /* invoke-direct {v0, v7}, Lmiui/android/animation/internal/RunnerHandler;->isInfoInTransMap(Lmiui/android/animation/internal/TransitionInfo;)Z */
if ( v10 != null) { // if-eqz v10, :cond_0
/* .line 227 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 228 */
/* .line 230 */
} // :cond_0
/* move-object v10, v5 */
/* .line 231 */
/* .local v10, "useOp":Lmiui/android/animation/internal/AnimOperationInfo; */
if ( v10 != null) { // if-eqz v10, :cond_1
/* iget-wide v11, v7, Lmiui/android/animation/internal/TransitionInfo;->startTime:J */
/* iget-wide v13, v10, Lmiui/android/animation/internal/AnimOperationInfo;->sendTime:J */
/* cmp-long v11, v11, v13 */
/* if-lez v11, :cond_1 */
/* .line 232 */
int v10 = 0; // const/4 v10, 0x0
/* .line 233 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 235 */
} // :cond_1
(( miui.android.animation.internal.TransitionInfo ) v7 ).getAnimStats ( ); // invoke-virtual {v7}, Lmiui/android/animation/internal/TransitionInfo;->getAnimStats()Lmiui/android/animation/internal/AnimStats;
/* .line 236 */
/* .local v11, "stats":Lmiui/android/animation/internal/AnimStats; */
v12 = (( miui.android.animation.internal.AnimStats ) v11 ).isStarted ( ); // invoke-virtual {v11}, Lmiui/android/animation/internal/AnimStats;->isStarted()Z
if ( v12 != null) { // if-eqz v12, :cond_2
/* .line 237 */
miui.android.animation.internal.RunnerHandler .handleUpdate ( v7,v10,v11 );
/* .line 239 */
} // :cond_2
v12 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v12 != null) { // if-eqz v12, :cond_4
/* .line 240 */
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = "---- updateAnim, target = "; // const-string v13, "---- updateAnim, target = "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v1 ); // invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v13 = 6; // const/4 v13, 0x6
/* new-array v13, v13, [Ljava/lang/Object; */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
final String v15 = "key = "; // const-string v15, "key = "
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v15 = this.key;
(( java.lang.StringBuilder ) v14 ).append ( v15 ); // invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v14 ).toString ( ); // invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* aput-object v14, v13, v8 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v14, "useOp = " */
(( java.lang.StringBuilder ) v8 ).append ( v14 ); // invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v10 ); // invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* aput-object v8, v13, v9 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "info.startTime = "; // const-string v9, "info.startTime = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v14, v7, Lmiui/android/animation/internal/TransitionInfo;->startTime:J */
(( java.lang.StringBuilder ) v8 ).append ( v14, v15 ); // invoke-virtual {v8, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v9 = 2; // const/4 v9, 0x2
/* aput-object v8, v13, v9 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "opInfo.time = "; // const-string v9, "opInfo.time = "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
if ( v5 != null) { // if-eqz v5, :cond_3
/* iget-wide v14, v5, Lmiui/android/animation/internal/AnimOperationInfo;->sendTime:J */
/* .line 244 */
java.lang.Long .valueOf ( v14,v15 );
} // :cond_3
int v9 = 0; // const/4 v9, 0x0
} // :goto_1
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v9 = 3; // const/4 v9, 0x3
/* aput-object v8, v13, v9 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v14, "stats.isRunning = " */
(( java.lang.StringBuilder ) v8 ).append ( v14 ); // invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 245 */
v14 = (( miui.android.animation.internal.AnimStats ) v11 ).isRunning ( ); // invoke-virtual {v11}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z
(( java.lang.StringBuilder ) v8 ).append ( v14 ); // invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v14 = 4; // const/4 v14, 0x4
/* aput-object v8, v13, v14 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v15, "stats = " */
(( java.lang.StringBuilder ) v8 ).append ( v15 ); // invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v11 ); // invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v15 = 5; // const/4 v15, 0x5
/* aput-object v8, v13, v15 */
/* .line 240 */
miui.android.animation.utils.LogUtils .debug ( v12,v13 );
/* .line 239 */
} // :cond_4
int v9 = 3; // const/4 v9, 0x3
int v14 = 4; // const/4 v14, 0x4
/* .line 249 */
} // :goto_2
v8 = (( miui.android.animation.internal.AnimStats ) v11 ).isRunning ( ); // invoke-virtual {v11}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z
/* if-nez v8, :cond_6 */
/* .line 250 */
v8 = this.animManager;
/* iget v12, v11, Lmiui/android/animation/internal/AnimStats;->cancelCount:I */
/* iget v13, v11, Lmiui/android/animation/internal/AnimStats;->endCount:I */
/* if-le v12, v13, :cond_5 */
/* move v13, v14 */
} // :cond_5
/* move v13, v9 */
} // :goto_3
int v9 = 2; // const/4 v9, 0x2
(( miui.android.animation.internal.AnimManager ) v8 ).notifyTransitionEnd ( v7, v9, v13 ); // invoke-virtual {v8, v7, v9, v13}, Lmiui/android/animation/internal/AnimManager;->notifyTransitionEnd(Lmiui/android/animation/internal/TransitionInfo;II)V
/* .line 253 */
} // :cond_6
/* add-int/lit8 v2, v2, 0x1 */
/* .line 255 */
} // .end local v7 # "info":Lmiui/android/animation/internal/TransitionInfo;
} // .end local v10 # "useOp":Lmiui/android/animation/internal/AnimOperationInfo;
} // .end local v11 # "stats":Lmiui/android/animation/internal/AnimStats;
} // :goto_4
/* goto/16 :goto_0 */
/* .line 256 */
} // :cond_7
if ( v5 != null) { // if-eqz v5, :cond_9
v6 = /* invoke-interface/range {p2 ..p2}, Ljava/util/List;->size()I */
/* if-eq v4, v6, :cond_8 */
v6 = (( miui.android.animation.internal.AnimOperationInfo ) v5 ).isUsed ( ); // invoke-virtual {v5}, Lmiui/android/animation/internal/AnimOperationInfo;->isUsed()Z
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 257 */
} // :cond_8
v6 = this.mOpMap;
/* .line 259 */
} // :cond_9
/* invoke-interface/range {p2 ..p2}, Ljava/util/List;->clear()V */
/* .line 260 */
/* if-lez v2, :cond_a */
/* move v8, v9 */
} // :cond_a
} // .end method
/* # virtual methods */
public void addSetToState ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1 ) {
/* .locals 2 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "to" # Lmiui/android/animation/controller/AnimState; */
/* .line 77 */
/* new-instance v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;-><init>(Lmiui/android/animation/internal/RunnerHandler$1;)V */
/* .line 78 */
/* .local v0, "info":Lmiui/android/animation/internal/RunnerHandler$SetToInfo; */
this.target = p1;
/* .line 79 */
/* iget-boolean v1, p2, Lmiui/android/animation/controller/AnimState;->isTemporary:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 80 */
/* new-instance v1, Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {v1}, Lmiui/android/animation/controller/AnimState;-><init>()V */
this.state = v1;
/* .line 81 */
v1 = this.state;
(( miui.android.animation.controller.AnimState ) v1 ).set ( p2 ); // invoke-virtual {v1, p2}, Lmiui/android/animation/controller/AnimState;->set(Lmiui/android/animation/controller/AnimState;)V
/* .line 83 */
} // :cond_0
this.state = p2;
/* .line 85 */
} // :goto_0
int v1 = 4; // const/4 v1, 0x4
(( miui.android.animation.internal.RunnerHandler ) p0 ).obtainMessage ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lmiui/android/animation/internal/RunnerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
/* .line 86 */
return;
} // .end method
public void handleMessage ( android.os.Message p0 ) {
/* .locals 11 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 90 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 92 */
/* :pswitch_0 */
v0 = this.obj;
/* check-cast v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo; */
/* invoke-direct {p0, v0}, Lmiui/android/animation/internal/RunnerHandler;->onSetTo(Lmiui/android/animation/internal/RunnerHandler$SetToInfo;)V */
/* .line 93 */
/* .line 105 */
/* :pswitch_1 */
/* iget-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 106 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v7 */
/* .line 107 */
/* .local v7, "now":J */
miui.android.animation.internal.AnimRunner .getInst ( );
(( miui.android.animation.internal.AnimRunner ) v0 ).getAverageDelta ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J
/* move-result-wide v9 */
/* .line 108 */
/* .local v9, "averageDelta":J */
v0 = this.obj;
/* check-cast v0, Ljava/lang/Boolean; */
v0 = (( java.lang.Boolean ) v0 ).booleanValue ( ); // invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 109 */
/* .local v0, "toPage":Z */
/* iget-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z */
/* if-nez v1, :cond_0 */
/* .line 110 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z */
/* .line 111 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J */
/* .line 112 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I */
/* .line 113 */
/* move-object v1, p0 */
/* move-wide v2, v7 */
/* move-wide v4, v9 */
/* move v6, v0 */
/* invoke-direct/range {v1 ..v6}, Lmiui/android/animation/internal/RunnerHandler;->runAnim(JJZ)V */
/* .line 114 */
} // :cond_0
/* iget-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z */
/* if-nez v1, :cond_1 */
/* .line 115 */
/* iget-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mLastRun:J */
/* sub-long v4, v7, v1 */
/* move-object v1, p0 */
/* move-wide v2, v7 */
/* move v6, v0 */
/* invoke-direct/range {v1 ..v6}, Lmiui/android/animation/internal/RunnerHandler;->runAnim(JJZ)V */
/* .line 117 */
} // .end local v0 # "toPage":Z
} // .end local v7 # "now":J
} // .end local v9 # "averageDelta":J
} // :cond_1
} // :goto_0
/* .line 120 */
/* :pswitch_2 */
/* invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->updateAnim()V */
/* .line 121 */
/* .line 95 */
/* :pswitch_3 */
v0 = miui.android.animation.internal.TransitionInfo.sMap;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
java.lang.Integer .valueOf ( v1 );
/* check-cast v0, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 96 */
/* .local v0, "info":Lmiui/android/animation/internal/TransitionInfo; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 97 */
v1 = this.target;
v2 = this.mTransMap;
/* invoke-direct {p0, v1, v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->addToMap(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/utils/LinkNode;Ljava/util/Map;)V */
/* .line 98 */
/* iget-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z */
/* if-nez v1, :cond_2 */
/* .line 99 */
/* invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->doSetup()V */
/* .line 103 */
} // .end local v0 # "info":Lmiui/android/animation/internal/TransitionInfo;
} // :cond_2
/* nop */
/* .line 125 */
} // :cond_3
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
this.obj = v0;
/* .line 126 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void setOperation ( miui.android.animation.internal.AnimOperationInfo p0 ) {
/* .locals 2 */
/* .param p1, "opInfo" # Lmiui/android/animation/internal/AnimOperationInfo; */
/* .line 70 */
v0 = this.target;
int v1 = 0; // const/4 v1, 0x0
/* new-array v1, v1, [Lmiui/android/animation/property/FloatProperty; */
v0 = (( miui.android.animation.IAnimTarget ) v0 ).isAnimRunning ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 71 */
java.lang.System .nanoTime ( );
/* move-result-wide v0 */
/* iput-wide v0, p1, Lmiui/android/animation/internal/AnimOperationInfo;->sendTime:J */
/* .line 72 */
v0 = this.mOpMap;
v1 = this.target;
/* .line 74 */
} // :cond_0
return;
} // .end method
