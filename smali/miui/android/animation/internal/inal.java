class inal implements java.util.concurrent.ThreadFactory {
	 /* .source "ThreadPoolUtil.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lmiui/android/animation/internal/ThreadPoolUtil;->getThreadFactory(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = null */
} // .end annotation
/* # instance fields */
final java.util.concurrent.atomic.AtomicInteger threadNumber;
final java.lang.String val$factoryName; //synthetic
/* # direct methods */
 inal ( ) {
/* .locals 1 */
/* .line 42 */
this.val$factoryName = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 43 */
/* new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger; */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
this.threadNumber = p1;
return;
} // .end method
/* # virtual methods */
public java.lang.Thread newThread ( java.lang.Runnable p0 ) {
/* .locals 3 */
/* .param p1, "runnable" # Ljava/lang/Runnable; */
/* .line 47 */
/* new-instance v0, Ljava/lang/Thread; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.val$factoryName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "-"; // const-string v2, "-"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.threadNumber;
/* .line 48 */
v2 = (( java.util.concurrent.atomic.AtomicInteger ) v2 ).getAndIncrement ( ); // invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, p1, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V */
/* .line 49 */
/* .local v0, "thread":Ljava/lang/Thread; */
int v1 = 5; // const/4 v1, 0x5
(( java.lang.Thread ) v0 ).setPriority ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V
/* .line 50 */
} // .end method
