public class miui.android.animation.internal.AnimManager implements miui.android.animation.internal.TransitionInfo$IUpdateInfoCreator {
	 /* .source "AnimManager.java" */
	 /* # interfaces */
	 /* # instance fields */
	 final java.util.concurrent.ConcurrentHashMap mRunningInfo;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/concurrent/ConcurrentHashMap<", */
	 /* "Ljava/lang/Object;", */
	 /* "Lmiui/android/animation/internal/TransitionInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
final java.util.Set mStartAnim;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
miui.android.animation.IAnimTarget mTarget;
private java.util.List mUpdateList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final java.util.concurrent.ConcurrentHashMap mUpdateMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Lmiui/android/animation/property/FloatProperty;", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Runnable mUpdateTask;
final java.util.concurrent.ConcurrentLinkedQueue mWaitState;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentLinkedQueue<", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.internal.AnimManager ( ) {
/* .locals 1 */
/* .line 24 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 28 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mStartAnim = v0;
/* .line 30 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mUpdateMap = v0;
/* .line 31 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mRunningInfo = v0;
/* .line 33 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V */
this.mWaitState = v0;
/* .line 37 */
/* new-instance v0, Lmiui/android/animation/internal/AnimManager$1; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/internal/AnimManager$1;-><init>(Lmiui/android/animation/internal/AnimManager;)V */
this.mUpdateTask = v0;
return;
} // .end method
private Boolean containProperties ( miui.android.animation.internal.TransitionInfo p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 5 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 93 */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
/* move v2, v1 */
} // :goto_0
/* if-ge v2, v0, :cond_1 */
/* aget-object v3, p2, v2 */
/* .line 94 */
/* .local v3, "property":Lmiui/android/animation/property/FloatProperty; */
v4 = (( miui.android.animation.internal.TransitionInfo ) p1 ).containsProperty ( v3 ); // invoke-virtual {p1, v3}, Lmiui/android/animation/internal/TransitionInfo;->containsProperty(Lmiui/android/animation/property/FloatProperty;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 95 */
int v0 = 1; // const/4 v0, 0x1
/* .line 93 */
} // .end local v3 # "property":Lmiui/android/animation/property/FloatProperty;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 98 */
} // :cond_1
} // .end method
private Boolean pendState ( miui.android.animation.internal.TransitionInfo p0 ) {
/* .locals 4 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .line 159 */
v0 = this.to;
/* iget-wide v0, v0, Lmiui/android/animation/controller/AnimState;->flags:J */
/* const-wide/16 v2, 0x1 */
v0 = miui.android.animation.utils.CommonUtils .hasFlags ( v0,v1,v2,v3 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 160 */
v0 = this.mWaitState;
(( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z
/* .line 161 */
int v0 = 1; // const/4 v0, 0x1
/* .line 163 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void removeSameAnim ( miui.android.animation.internal.TransitionInfo p0 ) {
/* .locals 7 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .line 120 */
v0 = this.mRunningInfo;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_6
/* check-cast v1, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 121 */
/* .local v1, "runInfo":Lmiui/android/animation/internal/TransitionInfo; */
/* if-ne v1, p1, :cond_0 */
/* .line 122 */
/* .line 124 */
} // :cond_0
v2 = this.updateList;
/* .line 125 */
/* .local v2, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;" */
v3 = this.mUpdateList;
/* if-nez v3, :cond_1 */
/* .line 126 */
/* new-instance v3, Ljava/util/ArrayList; */
/* invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V */
this.mUpdateList = v3;
/* .line 128 */
} // :cond_1
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 129 */
/* .local v4, "update":Lmiui/android/animation/listener/UpdateInfo; */
v5 = this.to;
v6 = this.property;
v5 = (( miui.android.animation.controller.AnimState ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Lmiui/android/animation/controller/AnimState;->contains(Ljava/lang/Object;)Z
/* if-nez v5, :cond_2 */
/* .line 130 */
v5 = this.mUpdateList;
/* .line 132 */
} // .end local v4 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_2
/* .line 133 */
} // :cond_3
v3 = v3 = this.mUpdateList;
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 134 */
int v3 = 5; // const/4 v3, 0x5
int v4 = 4; // const/4 v4, 0x4
(( miui.android.animation.internal.AnimManager ) p0 ).notifyTransitionEnd ( v1, v3, v4 ); // invoke-virtual {p0, v1, v3, v4}, Lmiui/android/animation/internal/AnimManager;->notifyTransitionEnd(Lmiui/android/animation/internal/TransitionInfo;II)V
/* .line 135 */
} // :cond_4
v3 = v3 = this.mUpdateList;
v4 = v4 = this.updateList;
/* if-eq v3, v4, :cond_5 */
/* .line 136 */
v3 = this.mUpdateList;
this.updateList = v3;
/* .line 137 */
int v3 = 0; // const/4 v3, 0x0
this.mUpdateList = v3;
/* .line 138 */
int v3 = 0; // const/4 v3, 0x0
(( miui.android.animation.internal.TransitionInfo ) v1 ).setupTasks ( v3 ); // invoke-virtual {v1, v3}, Lmiui/android/animation/internal/TransitionInfo;->setupTasks(Z)V
/* .line 140 */
} // :cond_5
v3 = this.mUpdateList;
/* .line 142 */
} // .end local v1 # "runInfo":Lmiui/android/animation/internal/TransitionInfo;
} // .end local v2 # "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
} // :goto_2
/* .line 143 */
} // :cond_6
return;
} // .end method
private void setTargetValue ( miui.android.animation.controller.AnimState p0, miui.android.animation.base.AnimConfigLink p1 ) {
/* .locals 9 */
/* .param p1, "to" # Lmiui/android/animation/controller/AnimState; */
/* .param p2, "config" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 178 */
(( miui.android.animation.controller.AnimState ) p1 ).keySet ( ); // invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 179 */
/* .local v1, "key":Ljava/lang/Object; */
(( miui.android.animation.controller.AnimState ) p1 ).getTempProperty ( v1 ); // invoke-virtual {p1, v1}, Lmiui/android/animation/controller/AnimState;->getTempProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
/* .line 180 */
/* .local v2, "property":Lmiui/android/animation/property/FloatProperty; */
v3 = this.mTarget;
(( miui.android.animation.controller.AnimState ) p1 ).get ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v3 */
/* .line 181 */
/* .local v3, "value":D */
v5 = this.mTarget;
v5 = this.animManager;
v5 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v5 ).get ( v2 ); // invoke-virtual {v5, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 182 */
/* .local v5, "update":Lmiui/android/animation/listener/UpdateInfo; */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 183 */
v6 = this.animInfo;
/* iput-wide v3, v6, Lmiui/android/animation/internal/AnimInfo;->setToValue:D */
/* .line 185 */
} // :cond_0
/* instance-of v6, v2, Lmiui/android/animation/property/IIntValueProperty; */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 186 */
v6 = this.mTarget;
/* move-object v7, v2 */
/* check-cast v7, Lmiui/android/animation/property/IIntValueProperty; */
/* double-to-int v8, v3 */
(( miui.android.animation.IAnimTarget ) v6 ).setIntValue ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
/* .line 188 */
} // :cond_1
v6 = this.mTarget;
/* double-to-float v7, v3 */
(( miui.android.animation.IAnimTarget ) v6 ).setValue ( v2, v7 ); // invoke-virtual {v6, v2, v7}, Lmiui/android/animation/IAnimTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 190 */
} // :goto_1
v6 = this.mTarget;
(( miui.android.animation.IAnimTarget ) v6 ).trackVelocity ( v2, v3, v4 ); // invoke-virtual {v6, v2, v3, v4}, Lmiui/android/animation/IAnimTarget;->trackVelocity(Lmiui/android/animation/property/FloatProperty;D)V
/* .line 191 */
} // .end local v1 # "key":Ljava/lang/Object;
} // .end local v2 # "property":Lmiui/android/animation/property/FloatProperty;
} // .end local v3 # "value":D
} // .end local v5 # "update":Lmiui/android/animation/listener/UpdateInfo;
/* .line 192 */
} // :cond_2
v0 = this.mTarget;
(( miui.android.animation.IAnimTarget ) v0 ).setToNotify ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/IAnimTarget;->setToNotify(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
/* .line 193 */
return;
} // .end method
/* # virtual methods */
public void clear ( ) {
/* .locals 1 */
/* .line 65 */
v0 = this.mStartAnim;
/* .line 66 */
v0 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 67 */
v0 = this.mRunningInfo;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 68 */
v0 = this.mWaitState;
(( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V
/* .line 69 */
return;
} // .end method
public Integer getTotalAnimCount ( ) {
/* .locals 4 */
/* .line 72 */
int v0 = 0; // const/4 v0, 0x0
/* .line 73 */
/* .local v0, "count":I */
v1 = this.mRunningInfo;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 74 */
/* .local v2, "info":Lmiui/android/animation/internal/TransitionInfo; */
v3 = (( miui.android.animation.internal.TransitionInfo ) v2 ).getAnimCount ( ); // invoke-virtual {v2}, Lmiui/android/animation/internal/TransitionInfo;->getAnimCount()I
/* add-int/2addr v0, v3 */
/* .line 75 */
} // .end local v2 # "info":Lmiui/android/animation/internal/TransitionInfo;
/* .line 76 */
} // :cond_0
} // .end method
void getTransitionInfos ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 57 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/internal/TransitionInfo;>;" */
v0 = this.mRunningInfo;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 58 */
/* .local v1, "info":Lmiui/android/animation/internal/TransitionInfo; */
v2 = this.updateList;
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = v2 = this.updateList;
/* if-nez v2, :cond_0 */
/* .line 59 */
/* .line 61 */
} // .end local v1 # "info":Lmiui/android/animation/internal/TransitionInfo;
} // :cond_0
/* .line 62 */
} // :cond_1
return;
} // .end method
public miui.android.animation.listener.UpdateInfo getUpdateInfo ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 204 */
v0 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 205 */
/* .local v0, "update":Lmiui/android/animation/listener/UpdateInfo; */
/* if-nez v0, :cond_0 */
/* .line 206 */
/* new-instance v1, Lmiui/android/animation/listener/UpdateInfo; */
/* invoke-direct {v1, p1}, Lmiui/android/animation/listener/UpdateInfo;-><init>(Lmiui/android/animation/property/FloatProperty;)V */
/* move-object v0, v1 */
/* .line 207 */
v1 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).putIfAbsent ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/android/animation/listener/UpdateInfo; */
/* .line 208 */
/* .local v1, "prev":Lmiui/android/animation/listener/UpdateInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 209 */
/* move-object v0, v1 */
/* .line 212 */
} // .end local v1 # "prev":Lmiui/android/animation/listener/UpdateInfo;
} // :cond_0
} // .end method
public Double getVelocity ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 196 */
(( miui.android.animation.internal.AnimManager ) p0 ).getUpdateInfo ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/internal/AnimManager;->getUpdateInfo(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;
/* iget-wide v0, v0, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
/* return-wide v0 */
} // .end method
public Boolean isAnimRunning ( miui.android.animation.property.FloatProperty...p0 ) {
/* .locals 4 */
/* .param p1, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 80 */
v0 = miui.android.animation.utils.CommonUtils .isArrayEmpty ( p1 );
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mRunningInfo;
/* .line 81 */
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mWaitState;
v0 = (( java.util.concurrent.ConcurrentLinkedQueue ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z
/* if-nez v0, :cond_1 */
/* .line 82 */
} // :cond_0
/* .line 84 */
} // :cond_1
v0 = this.mRunningInfo;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lmiui/android/animation/internal/TransitionInfo; */
/* .line 85 */
/* .local v2, "info":Lmiui/android/animation/internal/TransitionInfo; */
v3 = /* invoke-direct {p0, v2, p1}, Lmiui/android/animation/internal/AnimManager;->containProperties(Lmiui/android/animation/internal/TransitionInfo;[Lmiui/android/animation/property/FloatProperty;)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 86 */
/* .line 88 */
} // .end local v2 # "info":Lmiui/android/animation/internal/TransitionInfo;
} // :cond_2
/* .line 89 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
} // .end method
void notifyTransitionEnd ( miui.android.animation.internal.TransitionInfo p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .param p2, "msg" # I */
/* .param p3, "reason" # I */
/* .line 146 */
v0 = this.mRunningInfo;
v1 = this.key;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 147 */
v0 = this.mStartAnim;
v0 = v1 = this.key;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 148 */
v0 = miui.android.animation.internal.TransitionInfo.sMap;
/* iget v1, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I */
java.lang.Integer .valueOf ( v1 );
/* .line 149 */
v0 = this.mTarget;
v0 = this.handler;
/* iget v1, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I */
/* .line 150 */
(( miui.android.animation.internal.TargetHandler ) v0 ).obtainMessage ( p2, v1, p3 ); // invoke-virtual {v0, p2, v1, p3}, Lmiui/android/animation/internal/TargetHandler;->obtainMessage(III)Landroid/os/Message;
/* .line 151 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 153 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Lmiui/android/animation/property/FloatProperty; */
v0 = (( miui.android.animation.internal.AnimManager ) p0 ).isAnimRunning ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* if-nez v0, :cond_1 */
/* .line 154 */
v0 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
/* .line 156 */
} // :cond_1
return;
} // .end method
public void runUpdate ( ) {
/* .locals 2 */
/* .line 49 */
v0 = this.mTarget;
v1 = this.mUpdateTask;
(( miui.android.animation.IAnimTarget ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->post(Ljava/lang/Runnable;)V
/* .line 50 */
return;
} // .end method
public void setTarget ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 0 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 53 */
this.mTarget = p1;
/* .line 54 */
return;
} // .end method
public void setTo ( miui.android.animation.controller.AnimState p0, miui.android.animation.base.AnimConfigLink p1 ) {
/* .locals 3 */
/* .param p1, "to" # Lmiui/android/animation/controller/AnimState; */
/* .param p2, "config" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 167 */
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 168 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setTo, target = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTarget;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "to = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* filled-new-array {v1}, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 170 */
} // :cond_0
v0 = (( miui.android.animation.controller.AnimState ) p1 ).keySet ( ); // invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
/* const/16 v1, 0x96 */
/* if-le v0, v1, :cond_1 */
/* .line 171 */
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
v1 = this.mTarget;
(( miui.android.animation.internal.RunnerHandler ) v0 ).addSetToState ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lmiui/android/animation/internal/RunnerHandler;->addSetToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;)V
/* .line 173 */
} // :cond_1
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/internal/AnimManager;->setTargetValue(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V */
/* .line 175 */
} // :goto_0
return;
} // .end method
public void setVelocity ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "velocity" # F */
/* .line 200 */
(( miui.android.animation.internal.AnimManager ) p0 ).getUpdateInfo ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/internal/AnimManager;->getUpdateInfo(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/listener/UpdateInfo;
/* float-to-double v1, p2 */
/* iput-wide v1, v0, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
/* .line 201 */
return;
} // .end method
void setupTransition ( miui.android.animation.internal.TransitionInfo p0 ) {
/* .locals 2 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .line 113 */
v0 = this.mRunningInfo;
v1 = this.key;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 114 */
(( miui.android.animation.internal.TransitionInfo ) p1 ).initUpdateList ( p0 ); // invoke-virtual {p1, p0}, Lmiui/android/animation/internal/TransitionInfo;->initUpdateList(Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;)V
/* .line 115 */
int v0 = 1; // const/4 v0, 0x1
(( miui.android.animation.internal.TransitionInfo ) p1 ).setupTasks ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/internal/TransitionInfo;->setupTasks(Z)V
/* .line 116 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/internal/AnimManager;->removeSameAnim(Lmiui/android/animation/internal/TransitionInfo;)V */
/* .line 117 */
return;
} // .end method
public void startAnim ( miui.android.animation.internal.TransitionInfo p0 ) {
/* .locals 4 */
/* .param p1, "info" # Lmiui/android/animation/internal/TransitionInfo; */
/* .line 102 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/internal/AnimManager;->pendState(Lmiui/android/animation/internal/TransitionInfo;)Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 103 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v2 = ".startAnim, pendState"; // const-string v2, ".startAnim, pendState"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-array v1, v1, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v0,v1 );
/* .line 104 */
return;
/* .line 106 */
} // :cond_0
v0 = miui.android.animation.internal.TransitionInfo.sMap;
/* iget v2, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I */
java.lang.Integer .valueOf ( v2 );
/* .line 107 */
v0 = miui.android.animation.internal.AnimRunner.sRunnerHandler;
/* iget v2, p1, Lmiui/android/animation/internal/TransitionInfo;->id:I */
/* .line 108 */
int v3 = 1; // const/4 v3, 0x1
(( miui.android.animation.internal.RunnerHandler ) v0 ).obtainMessage ( v3, v2, v1 ); // invoke-virtual {v0, v3, v2, v1}, Lmiui/android/animation/internal/RunnerHandler;->obtainMessage(III)Landroid/os/Message;
/* .line 109 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 110 */
return;
} // .end method
public void update ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "toPage" # Z */
/* .line 45 */
v0 = this.mTarget;
v0 = this.handler;
(( miui.android.animation.internal.TargetHandler ) v0 ).update ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/internal/TargetHandler;->update(Z)V
/* .line 46 */
return;
} // .end method
