public class miui.android.animation.internal.AnimData {
	 /* .source "AnimData.java" */
	 /* # instance fields */
	 public Long delay;
	 public miui.android.animation.utils.EaseManager$EaseStyle ease;
	 public Integer frameCount;
	 public Long initTime;
	 public Boolean isCompleted;
	 Boolean justEnd;
	 Boolean logEnabled;
	 public Object op;
	 public Double progress;
	 public miui.android.animation.property.FloatProperty property;
	 public Long startTime;
	 public Double startValue;
	 public Double targetValue;
	 public Integer tintMode;
	 public Double value;
	 public Double velocity;
	 /* # direct methods */
	 public miui.android.animation.internal.AnimData ( ) {
		 /* .locals 2 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 37 */
		 /* const-wide v0, 0x7fefffffffffffffL # Double.MAX_VALUE */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
		 /* .line 39 */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
		 /* .line 41 */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D */
		 return;
	 } // .end method
	 /* # virtual methods */
	 void clear ( ) {
		 /* .locals 1 */
		 /* .line 95 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.property = v0;
		 /* .line 96 */
		 this.ease = v0;
		 /* .line 97 */
		 return;
	 } // .end method
	 void from ( miui.android.animation.listener.UpdateInfo p0, miui.android.animation.base.AnimConfig p1, miui.android.animation.base.AnimSpecialConfig p2 ) {
		 /* .locals 2 */
		 /* .param p1, "update" # Lmiui/android/animation/listener/UpdateInfo; */
		 /* .param p2, "config" # Lmiui/android/animation/base/AnimConfig; */
		 /* .param p3, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
		 /* .line 59 */
		 v0 = this.property;
		 this.property = v0;
		 /* .line 60 */
		 /* iget-wide v0, p1, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D */
		 /* .line 61 */
		 /* iget v0, p1, Lmiui/android/animation/listener/UpdateInfo;->frameCount:I */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimData;->frameCount:I */
		 /* .line 62 */
		 v0 = this.animInfo;
		 /* iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B */
		 /* iput-byte v0, p0, Lmiui/android/animation/internal/AnimData;->op:B */
		 /* .line 63 */
		 v0 = this.animInfo;
		 /* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->initTime:J */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->initTime:J */
		 /* .line 64 */
		 v0 = this.animInfo;
		 /* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->startTime:J */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startTime:J */
		 /* .line 65 */
		 v0 = this.animInfo;
		 /* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->progress:D */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->progress:D */
		 /* .line 66 */
		 v0 = this.animInfo;
		 /* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
		 /* .line 67 */
		 v0 = this.animInfo;
		 /* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
		 /* .line 68 */
		 v0 = this.animInfo;
		 /* iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->value:D */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->value:D */
		 /* .line 69 */
		 /* iget-boolean v0, p1, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
		 /* iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z */
		 /* .line 70 */
		 v0 = this.animInfo;
		 /* iget-boolean v0, v0, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z */
		 /* iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->justEnd:Z */
		 /* .line 72 */
		 v0 = 		 miui.android.animation.internal.AnimConfigUtils .getTintMode ( p2,p3 );
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimData;->tintMode:I */
		 /* .line 73 */
		 miui.android.animation.internal.AnimConfigUtils .getEase ( p2,p3 );
		 this.ease = v0;
		 /* .line 74 */
		 miui.android.animation.internal.AnimConfigUtils .getDelay ( p2,p3 );
		 /* move-result-wide v0 */
		 /* iput-wide v0, p0, Lmiui/android/animation/internal/AnimData;->delay:J */
		 /* .line 75 */
		 return;
	 } // .end method
	 void reset ( ) {
		 /* .locals 1 */
		 /* .line 48 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z */
		 /* .line 49 */
		 /* iput v0, p0, Lmiui/android/animation/internal/AnimData;->frameCount:I */
		 /* .line 50 */
		 /* iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->justEnd:Z */
		 /* .line 51 */
		 return;
	 } // .end method
	 public void setOp ( Object p0 ) {
		 /* .locals 1 */
		 /* .param p1, "op" # B */
		 /* .line 54 */
		 /* iput-byte p1, p0, Lmiui/android/animation/internal/AnimData;->op:B */
		 /* .line 55 */
		 if ( p1 != null) { // if-eqz p1, :cond_1
			 int v0 = 2; // const/4 v0, 0x2
			 /* if-le p1, v0, :cond_0 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
/* iput-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z */
/* .line 56 */
return;
} // .end method
void to ( miui.android.animation.listener.UpdateInfo p0 ) {
/* .locals 3 */
/* .param p1, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .line 78 */
/* iget v0, p0, Lmiui/android/animation/internal/AnimData;->frameCount:I */
/* iput v0, p1, Lmiui/android/animation/listener/UpdateInfo;->frameCount:I */
/* .line 79 */
v0 = this.animInfo;
/* iget-byte v1, p0, Lmiui/android/animation/internal/AnimData;->op:B */
/* iput-byte v1, v0, Lmiui/android/animation/internal/AnimInfo;->op:B */
/* .line 80 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->delay:J */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->delay:J */
/* .line 81 */
v0 = this.animInfo;
/* iget v1, p0, Lmiui/android/animation/internal/AnimData;->tintMode:I */
/* iput v1, v0, Lmiui/android/animation/internal/AnimInfo;->tintMode:I */
/* .line 82 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->initTime:J */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->initTime:J */
/* .line 83 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->startTime:J */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->startTime:J */
/* .line 84 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->progress:D */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->progress:D */
/* .line 85 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->startValue:D */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
/* .line 86 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->targetValue:D */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
/* .line 87 */
/* iget-boolean v0, p0, Lmiui/android/animation/internal/AnimData;->isCompleted:Z */
/* iput-boolean v0, p1, Lmiui/android/animation/listener/UpdateInfo;->isCompleted:Z */
/* .line 88 */
v0 = this.animInfo;
/* iget-wide v1, p0, Lmiui/android/animation/internal/AnimData;->value:D */
/* iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->value:D */
/* .line 89 */
/* iget-wide v0, p0, Lmiui/android/animation/internal/AnimData;->velocity:D */
/* iput-wide v0, p1, Lmiui/android/animation/listener/UpdateInfo;->velocity:D */
/* .line 90 */
v0 = this.animInfo;
/* iget-boolean v1, p0, Lmiui/android/animation/internal/AnimData;->justEnd:Z */
/* iput-boolean v1, v0, Lmiui/android/animation/internal/AnimInfo;->justEnd:Z */
/* .line 91 */
(( miui.android.animation.internal.AnimData ) p0 ).clear ( ); // invoke-virtual {p0}, Lmiui/android/animation/internal/AnimData;->clear()V
/* .line 92 */
return;
} // .end method
