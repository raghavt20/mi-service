.class public Lmiui/android/animation/internal/AnimRunner;
.super Ljava/lang/Object;
.source "AnimRunner.java"

# interfaces
.implements Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/internal/AnimRunner$Holder;
    }
.end annotation


# static fields
.field public static final MAX_DELTA:J = 0x10L

.field private static final MAX_RECORD:I = 0x5

.field private static final MSG_END:I = 0x1

.field private static final MSG_START:I

.field private static final sMainHandler:Landroid/os/Handler;

.field public static final sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

.field private static final sRunnerThread:Landroid/os/HandlerThread;


# instance fields
.field private volatile mAverageDelta:J

.field private mDeltaRecord:[J

.field private volatile mIsRunning:Z

.field private mLastFrameTime:J

.field private mRatio:F

.field private mRecordCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 42
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AnimRunnerThread"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerThread:Landroid/os/HandlerThread;

    .line 50
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 51
    new-instance v1, Lmiui/android/animation/internal/RunnerHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Lmiui/android/animation/internal/RunnerHandler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    .line 54
    new-instance v0, Lmiui/android/animation/internal/AnimRunner$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lmiui/android/animation/internal/AnimRunner$1;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lmiui/android/animation/internal/AnimRunner;->sMainHandler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const-wide/16 v0, 0x10

    iput-wide v0, p0, Lmiui/android/animation/internal/AnimRunner;->mAverageDelta:J

    .line 112
    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lmiui/android/animation/internal/AnimRunner;->mDeltaRecord:[J

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/internal/AnimRunner;->mRecordCount:I

    .line 119
    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method synthetic constructor <init>(Lmiui/android/animation/internal/AnimRunner$1;)V
    .locals 0
    .param p1, "x0"    # Lmiui/android/animation/internal/AnimRunner$1;

    .line 23
    invoke-direct {p0}, Lmiui/android/animation/internal/AnimRunner;-><init>()V

    return-void
.end method

.method static synthetic access$100()V
    .locals 0

    .line 23
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->startAnimRunner()V

    return-void
.end method

.method static synthetic access$200()V
    .locals 0

    .line 23
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->endAnimation()V

    return-void
.end method

.method private average([J)J
    .locals 9
    .param p1, "array"    # [J

    .line 233
    const-wide/16 v0, 0x0

    .line 234
    .local v0, "total":J
    const/4 v2, 0x0

    .line 235
    .local v2, "count":I
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    const-wide/16 v5, 0x0

    if-ge v4, v3, :cond_1

    aget-wide v7, p1, v4

    .line 236
    .local v7, "a":J
    add-long/2addr v0, v7

    .line 237
    cmp-long v5, v7, v5

    if-lez v5, :cond_0

    add-int/lit8 v5, v2, 0x1

    goto :goto_1

    :cond_0
    move v5, v2

    :goto_1
    move v2, v5

    .line 235
    .end local v7    # "a":J
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 239
    :cond_1
    if-lez v2, :cond_2

    int-to-long v3, v2

    div-long v5, v0, v3

    :cond_2
    return-wide v5
.end method

.method private calculateAverageDelta(J)J
    .locals 6
    .param p1, "deltaT"    # J

    .line 209
    iget-object v0, p0, Lmiui/android/animation/internal/AnimRunner;->mDeltaRecord:[J

    invoke-direct {p0, v0}, Lmiui/android/animation/internal/AnimRunner;->average([J)J

    move-result-wide v0

    .line 210
    .local v0, "average":J
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    move-wide v4, v0

    goto :goto_0

    :cond_0
    move-wide v4, p1

    :goto_0
    move-wide p1, v4

    .line 211
    cmp-long v2, p1, v2

    if-eqz v2, :cond_1

    const-wide/16 v2, 0x10

    cmp-long v2, p1, v2

    if-lez v2, :cond_2

    .line 212
    :cond_1
    const-wide/16 p1, 0x10

    .line 214
    :cond_2
    long-to-float v2, p1

    iget v3, p0, Lmiui/android/animation/internal/AnimRunner;->mRatio:F

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-long v2, v2

    return-wide v2
.end method

.method private static endAnimation()V
    .locals 4

    .line 99
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    .line 100
    .local v0, "runner":Lmiui/android/animation/internal/AnimRunner;
    iget-boolean v1, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z

    if-nez v1, :cond_0

    .line 101
    return-void

    .line 103
    :cond_0
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 104
    const-string v1, "AnimRunner.endAnimation"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    :cond_1
    iput-boolean v2, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z

    .line 107
    invoke-static {}, Lmiui/android/animation/physics/AnimationHandler;->getInstance()Lmiui/android/animation/physics/AnimationHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/android/animation/physics/AnimationHandler;->removeCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;)V

    .line 108
    return-void
.end method

.method public static getInst()Lmiui/android/animation/internal/AnimRunner;
    .locals 1

    .line 36
    sget-object v0, Lmiui/android/animation/internal/AnimRunner$Holder;->inst:Lmiui/android/animation/internal/AnimRunner;

    return-object v0
.end method

.method private static startAnimRunner()V
    .locals 4

    .line 86
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    .line 87
    .local v0, "runner":Lmiui/android/animation/internal/AnimRunner;
    iget-boolean v1, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z

    if-eqz v1, :cond_0

    .line 88
    return-void

    .line 90
    :cond_0
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "AnimRunner.start"

    invoke-static {v2, v1}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    :cond_1
    invoke-static {}, Lmiui/android/animation/Folme;->getTimeRatio()F

    move-result v1

    iput v1, v0, Lmiui/android/animation/internal/AnimRunner;->mRatio:F

    .line 94
    const/4 v1, 0x1

    iput-boolean v1, v0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z

    .line 95
    invoke-static {}, Lmiui/android/animation/physics/AnimationHandler;->getInstance()Lmiui/android/animation/physics/AnimationHandler;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lmiui/android/animation/physics/AnimationHandler;->addAnimationFrameCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)V

    .line 96
    return-void
.end method

.method private static updateAnimRunner(Ljava/util/Collection;Z)V
    .locals 5
    .param p1, "toPage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lmiui/android/animation/IAnimTarget;",
            ">;Z)V"
        }
    .end annotation

    .line 71
    .local p0, "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;"
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/IAnimTarget;

    .line 72
    .local v1, "target":Lmiui/android/animation/IAnimTarget;
    iget-object v2, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    const/4 v3, 0x0

    new-array v4, v3, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v2, v4}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v2

    .line 73
    .local v2, "isAnimRunning":Z
    if-eqz v2, :cond_1

    .line 74
    if-eqz p1, :cond_0

    .line 75
    iget-object v3, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v3}, Lmiui/android/animation/internal/AnimManager;->runUpdate()V

    goto :goto_1

    .line 77
    :cond_0
    iget-object v4, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v4, v3}, Lmiui/android/animation/internal/AnimManager;->update(Z)V

    goto :goto_1

    .line 79
    :cond_1
    if-nez v2, :cond_2

    const-wide/16 v3, 0x1

    invoke-virtual {v1, v3, v4}, Lmiui/android/animation/IAnimTarget;->hasFlags(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 80
    filled-new-array {v1}, [Lmiui/android/animation/IAnimTarget;

    move-result-object v3

    invoke-static {v3}, Lmiui/android/animation/Folme;->clean([Ljava/lang/Object;)V

    .line 82
    .end local v1    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v2    # "isAnimRunning":Z
    :cond_2
    :goto_1
    goto :goto_0

    .line 83
    :cond_3
    return-void
.end method

.method private updateRunningTime(J)V
    .locals 6
    .param p1, "frameTime"    # J

    .line 219
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimRunner;->mLastFrameTime:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 220
    iput-wide p1, p0, Lmiui/android/animation/internal/AnimRunner;->mLastFrameTime:J

    .line 221
    const-wide/16 v0, 0x0

    .local v0, "deltaT":J
    goto :goto_0

    .line 223
    .end local v0    # "deltaT":J
    :cond_0
    sub-long v0, p1, v0

    .line 224
    .restart local v0    # "deltaT":J
    iput-wide p1, p0, Lmiui/android/animation/internal/AnimRunner;->mLastFrameTime:J

    .line 226
    :goto_0
    iget v2, p0, Lmiui/android/animation/internal/AnimRunner;->mRecordCount:I

    rem-int/lit8 v3, v2, 0x5

    .line 227
    .local v3, "idx":I
    iget-object v4, p0, Lmiui/android/animation/internal/AnimRunner;->mDeltaRecord:[J

    aput-wide v0, v4, v3

    .line 228
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lmiui/android/animation/internal/AnimRunner;->mRecordCount:I

    .line 229
    invoke-direct {p0, v0, v1}, Lmiui/android/animation/internal/AnimRunner;->calculateAverageDelta(J)J

    move-result-wide v4

    iput-wide v4, p0, Lmiui/android/animation/internal/AnimRunner;->mAverageDelta:J

    .line 230
    return-void
.end method


# virtual methods
.method public varargs cancel(Lmiui/android/animation/IAnimTarget;[Ljava/lang/String;)V
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "propertyNames"    # [Ljava/lang/String;

    .line 152
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    new-instance v1, Lmiui/android/animation/internal/AnimOperationInfo;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, p2, v3}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V

    invoke-virtual {v0, v1}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V

    .line 153
    return-void
.end method

.method public varargs cancel(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 156
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    new-instance v1, Lmiui/android/animation/internal/AnimOperationInfo;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3, p2}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V

    invoke-virtual {v0, v1}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V

    .line 157
    return-void
.end method

.method public doAnimationFrame(J)Z
    .locals 6
    .param p1, "frameTime"    # J

    .line 123
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/internal/AnimRunner;->updateRunningTime(J)V

    .line 124
    iget-boolean v0, p0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z

    if-eqz v0, :cond_4

    .line 125
    invoke-static {}, Lmiui/android/animation/Folme;->getTargets()Ljava/util/Collection;

    move-result-object v0

    .line 126
    .local v0, "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;"
    const/4 v1, 0x0

    .line 127
    .local v1, "animCounnt":I
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/android/animation/IAnimTarget;

    .line 128
    .local v3, "target":Lmiui/android/animation/IAnimTarget;
    iget-object v5, v3, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    new-array v4, v4, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v5, v4}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v4

    .line 129
    .local v4, "isAnimRunning":Z
    if-eqz v4, :cond_0

    .line 130
    iget-object v5, v3, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v5}, Lmiui/android/animation/internal/AnimManager;->getTotalAnimCount()I

    move-result v5

    add-int/2addr v1, v5

    .line 132
    .end local v3    # "target":Lmiui/android/animation/IAnimTarget;
    .end local v4    # "isAnimRunning":Z
    :cond_0
    goto :goto_0

    .line 133
    :cond_1
    const/16 v2, 0x1f4

    if-le v1, v2, :cond_2

    const/4 v4, 0x1

    :cond_2
    move v2, v4

    .line 135
    .local v2, "toPage":Z
    if-nez v2, :cond_3

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 136
    invoke-static {v0, v2}, Lmiui/android/animation/internal/AnimRunner;->updateAnimRunner(Ljava/util/Collection;Z)V

    .line 139
    :cond_3
    sget-object v3, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    invoke-virtual {v3}, Lmiui/android/animation/internal/RunnerHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v4

    .line 140
    .local v4, "msg":Landroid/os/Message;
    const/4 v5, 0x3

    iput v5, v4, Landroid/os/Message;->what:I

    .line 141
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 142
    invoke-virtual {v3, v4}, Lmiui/android/animation/internal/RunnerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 144
    if-eqz v2, :cond_4

    .line 145
    invoke-static {v0, v2}, Lmiui/android/animation/internal/AnimRunner;->updateAnimRunner(Ljava/util/Collection;Z)V

    .line 148
    .end local v0    # "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;"
    .end local v1    # "animCounnt":I
    .end local v2    # "toPage":Z
    .end local v4    # "msg":Landroid/os/Message;
    :cond_4
    iget-boolean v0, p0, Lmiui/android/animation/internal/AnimRunner;->mIsRunning:Z

    return v0
.end method

.method end()V
    .locals 2

    .line 197
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 198
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->endAnimation()V

    goto :goto_0

    .line 200
    :cond_0
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sMainHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 202
    :goto_0
    return-void
.end method

.method public varargs end(Lmiui/android/animation/IAnimTarget;[Ljava/lang/String;)V
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "propertyNames"    # [Ljava/lang/String;

    .line 160
    invoke-static {p2}, Lmiui/android/animation/utils/CommonUtils;->isArrayEmpty([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p1, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    invoke-virtual {v0, v1}, Lmiui/android/animation/internal/TargetHandler;->sendEmptyMessage(I)Z

    .line 163
    :cond_0
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    new-instance v2, Lmiui/android/animation/internal/AnimOperationInfo;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, p2, v3}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V

    invoke-virtual {v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V

    .line 164
    return-void
.end method

.method public varargs end(Lmiui/android/animation/IAnimTarget;[Lmiui/android/animation/property/FloatProperty;)V
    .locals 4
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 167
    invoke-static {p2}, Lmiui/android/animation/utils/CommonUtils;->isArrayEmpty([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p1, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    invoke-virtual {v0, v1}, Lmiui/android/animation/internal/TargetHandler;->sendEmptyMessage(I)Z

    .line 170
    :cond_0
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sRunnerHandler:Lmiui/android/animation/internal/RunnerHandler;

    new-instance v2, Lmiui/android/animation/internal/AnimOperationInfo;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, v3, p2}, Lmiui/android/animation/internal/AnimOperationInfo;-><init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V

    invoke-virtual {v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V

    .line 171
    return-void
.end method

.method public getAverageDelta()J
    .locals 2

    .line 205
    iget-wide v0, p0, Lmiui/android/animation/internal/AnimRunner;->mAverageDelta:J

    return-wide v0
.end method

.method public run(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "from"    # Lmiui/android/animation/controller/AnimState;
    .param p3, "to"    # Lmiui/android/animation/controller/AnimState;
    .param p4, "config"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 175
    new-instance v0, Lmiui/android/animation/internal/TransitionInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lmiui/android/animation/internal/TransitionInfo;-><init>(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 176
    .local v0, "info":Lmiui/android/animation/internal/TransitionInfo;
    invoke-virtual {p0, v0}, Lmiui/android/animation/internal/AnimRunner;->run(Lmiui/android/animation/internal/TransitionInfo;)V

    .line 177
    return-void
.end method

.method public run(Lmiui/android/animation/internal/TransitionInfo;)V
    .locals 2
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 180
    iget-object v0, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    new-instance v1, Lmiui/android/animation/internal/AnimRunner$2;

    invoke-direct {v1, p0, p1}, Lmiui/android/animation/internal/AnimRunner$2;-><init>(Lmiui/android/animation/internal/AnimRunner;Lmiui/android/animation/internal/TransitionInfo;)V

    invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->executeOnInitialized(Ljava/lang/Runnable;)V

    .line 186
    return-void
.end method

.method start()V
    .locals 2

    .line 189
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 190
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->startAnimRunner()V

    goto :goto_0

    .line 192
    :cond_0
    sget-object v0, Lmiui/android/animation/internal/AnimRunner;->sMainHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 194
    :goto_0
    return-void
.end method
