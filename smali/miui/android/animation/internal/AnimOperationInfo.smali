.class Lmiui/android/animation/internal/AnimOperationInfo;
.super Ljava/lang/Object;
.source "AnimOperationInfo.java"


# instance fields
.field public final op:B

.field public final propList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/property/FloatProperty;",
            ">;"
        }
    .end annotation
.end field

.field public volatile sendTime:J

.field public final target:Lmiui/android/animation/IAnimTarget;

.field public usedCount:I


# direct methods
.method constructor <init>(Lmiui/android/animation/IAnimTarget;B[Ljava/lang/String;[Lmiui/android/animation/property/FloatProperty;)V
    .locals 6
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "op"    # B
    .param p3, "names"    # [Ljava/lang/String;
    .param p4, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I

    .line 22
    iput-byte p2, p0, Lmiui/android/animation/internal/AnimOperationInfo;->op:B

    .line 23
    iput-object p1, p0, Lmiui/android/animation/internal/AnimOperationInfo;->target:Lmiui/android/animation/IAnimTarget;

    .line 24
    if-eqz p3, :cond_1

    instance-of v1, p1, Lmiui/android/animation/ValueTarget;

    if-eqz v1, :cond_1

    .line 25
    move-object v1, p1

    check-cast v1, Lmiui/android/animation/ValueTarget;

    .line 26
    .local v1, "vt":Lmiui/android/animation/ValueTarget;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    .line 27
    array-length v2, p3

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p3, v0

    .line 28
    .local v3, "name":Ljava/lang/String;
    iget-object v4, p0, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    invoke-virtual {v1, v3}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    .end local v3    # "name":Ljava/lang/String;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    .end local v1    # "vt":Lmiui/android/animation/ValueTarget;
    :cond_0
    goto :goto_1

    :cond_1
    if-eqz p4, :cond_2

    .line 31
    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    goto :goto_1

    .line 33
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    .line 35
    :goto_1
    return-void
.end method


# virtual methods
.method isUsed()Z
    .locals 4

    .line 38
    iget-object v0, p0, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 39
    .local v0, "size":I
    :goto_0
    const/4 v2, 0x1

    iget v3, p0, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I

    if-nez v0, :cond_1

    if-lez v3, :cond_2

    goto :goto_1

    :cond_1
    if-ne v3, v0, :cond_2

    :goto_1
    move v1, v2

    :cond_2
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimOperationInfo{target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimOperationInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", op="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lmiui/android/animation/internal/AnimOperationInfo;->op:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", propList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 47
    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 44
    return-object v0
.end method
