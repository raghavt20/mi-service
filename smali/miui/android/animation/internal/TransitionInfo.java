class miui.android.animation.internal.TransitionInfo extends miui.android.animation.utils.LinkNode {
	 /* .source "TransitionInfo.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator; */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lmiui/android/animation/utils/LinkNode<", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
private static final java.util.concurrent.atomic.AtomicInteger sIdGenerator;
public static final java.util.Map sMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Lmiui/android/animation/internal/TransitionInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
public java.util.List animTasks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/internal/AnimTask;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public volatile miui.android.animation.base.AnimConfig config;
public volatile miui.android.animation.controller.AnimState from;
public final Integer id;
public volatile java.lang.Object key;
private final miui.android.animation.internal.AnimStats mAnimStats;
public volatile Long startTime;
public final java.lang.Object tag;
public final miui.android.animation.IAnimTarget target;
public volatile miui.android.animation.controller.AnimState to;
public volatile java.util.List updateList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lmiui/android/animation/listener/UpdateInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.internal.TransitionInfo ( ) {
/* .locals 1 */
/* .line 23 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
/* .line 25 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V */
return;
} // .end method
public miui.android.animation.internal.TransitionInfo ( ) {
/* .locals 3 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "f" # Lmiui/android/animation/controller/AnimState; */
/* .param p3, "t" # Lmiui/android/animation/controller/AnimState; */
/* .param p4, "c" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 61 */
/* invoke-direct {p0}, Lmiui/android/animation/utils/LinkNode;-><init>()V */
/* .line 31 */
v0 = miui.android.animation.internal.TransitionInfo.sIdGenerator;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).incrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
/* iput v0, p0, Lmiui/android/animation/internal/TransitionInfo;->id:I */
/* .line 38 */
/* new-instance v1, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v1}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.config = v1;
/* .line 46 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.animTasks = v1;
/* .line 48 */
/* new-instance v1, Lmiui/android/animation/internal/AnimStats; */
/* invoke-direct {v1}, Lmiui/android/animation/internal/AnimStats;-><init>()V */
this.mAnimStats = v1;
/* .line 62 */
this.target = p1;
/* .line 63 */
/* invoke-direct {p0, p2}, Lmiui/android/animation/internal/TransitionInfo;->getState(Lmiui/android/animation/controller/AnimState;)Lmiui/android/animation/controller/AnimState; */
this.from = v1;
/* .line 64 */
/* invoke-direct {p0, p3}, Lmiui/android/animation/internal/TransitionInfo;->getState(Lmiui/android/animation/controller/AnimState;)Lmiui/android/animation/controller/AnimState; */
this.to = v1;
/* .line 65 */
v1 = this.to;
(( miui.android.animation.controller.AnimState ) v1 ).getTag ( ); // invoke-virtual {v1}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;
this.tag = v1;
/* .line 66 */
/* iget-boolean v2, p3, Lmiui/android/animation/controller/AnimState;->isTemporary:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 67 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( v0 );
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
this.key = v0;
/* .line 69 */
} // :cond_0
this.key = v1;
/* .line 71 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
this.updateList = v0;
/* .line 72 */
/* invoke-direct {p0}, Lmiui/android/animation/internal/TransitionInfo;->initValueForColorProperty()V */
/* .line 73 */
v0 = this.config;
(( miui.android.animation.controller.AnimState ) p3 ).getConfig ( ); // invoke-virtual {p3}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).copy ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V
/* .line 74 */
if ( p4 != null) { // if-eqz p4, :cond_1
/* .line 75 */
v0 = this.config;
(( miui.android.animation.base.AnimConfigLink ) p4 ).addTo ( v0 ); // invoke-virtual {p4, v0}, Lmiui/android/animation/base/AnimConfigLink;->addTo(Lmiui/android/animation/base/AnimConfig;)V
/* .line 77 */
} // :cond_1
return;
} // .end method
static void decreaseStartCountForDelayAnim ( miui.android.animation.internal.AnimTask p0, miui.android.animation.internal.AnimStats p1, miui.android.animation.listener.UpdateInfo p2, Object p3 ) {
/* .locals 5 */
/* .param p0, "task" # Lmiui/android/animation/internal/AnimTask; */
/* .param p1, "stats" # Lmiui/android/animation/internal/AnimStats; */
/* .param p2, "update" # Lmiui/android/animation/listener/UpdateInfo; */
/* .param p3, "op" # B */
/* .line 52 */
if ( p0 != null) { // if-eqz p0, :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p3, v0, :cond_0 */
v1 = this.animInfo;
/* iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->delay:J */
/* const-wide/16 v3, 0x0 */
/* cmp-long v1, v1, v3 */
/* if-lez v1, :cond_0 */
v1 = this.animStats;
/* iget v1, v1, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* if-lez v1, :cond_0 */
/* .line 56 */
v1 = this.animStats;
/* iget v2, v1, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* sub-int/2addr v2, v0 */
/* iput v2, v1, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* .line 57 */
/* iget v1, p1, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* sub-int/2addr v1, v0 */
/* iput v1, p1, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* .line 59 */
} // :cond_0
return;
} // .end method
private miui.android.animation.controller.AnimState getState ( miui.android.animation.controller.AnimState p0 ) {
/* .locals 1 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .line 106 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v0, p1, Lmiui/android/animation/controller/AnimState;->isTemporary:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
/* new-instance v0, Lmiui/android/animation/controller/AnimState; */
/* invoke-direct {v0}, Lmiui/android/animation/controller/AnimState;-><init>()V */
/* .line 108 */
/* .local v0, "s":Lmiui/android/animation/controller/AnimState; */
(( miui.android.animation.controller.AnimState ) v0 ).set ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/AnimState;->set(Lmiui/android/animation/controller/AnimState;)V
/* .line 109 */
/* .line 111 */
} // .end local v0 # "s":Lmiui/android/animation/controller/AnimState;
} // :cond_0
} // .end method
private void initValueForColorProperty ( ) {
/* .locals 10 */
/* .line 124 */
v0 = this.from;
/* if-nez v0, :cond_0 */
/* .line 125 */
return;
/* .line 127 */
} // :cond_0
v0 = this.to;
(( miui.android.animation.controller.AnimState ) v0 ).keySet ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 128 */
/* .local v1, "key":Ljava/lang/Object; */
v2 = this.to;
(( miui.android.animation.controller.AnimState ) v2 ).getTempProperty ( v1 ); // invoke-virtual {v2, v1}, Lmiui/android/animation/controller/AnimState;->getTempProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
/* .line 129 */
/* .local v2, "property":Lmiui/android/animation/property/FloatProperty; */
/* instance-of v3, v2, Lmiui/android/animation/property/ColorProperty; */
/* if-nez v3, :cond_1 */
/* .line 130 */
/* .line 132 */
} // :cond_1
v3 = this.target;
/* const-wide v4, 0x7fefffffffffffffL # Double.MAX_VALUE */
miui.android.animation.internal.AnimValueUtils .getValueOfTarget ( v3,v2,v4,v5 );
/* move-result-wide v3 */
/* .line 133 */
/* .local v3, "curValue":D */
v5 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v3,v4 );
/* if-nez v5, :cond_2 */
/* .line 134 */
/* .line 136 */
} // :cond_2
v5 = this.from;
v6 = this.target;
(( miui.android.animation.controller.AnimState ) v5 ).get ( v6, v2 ); // invoke-virtual {v5, v6, v2}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v5 */
/* .line 137 */
/* .local v5, "fv":D */
v7 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v5,v6 );
/* if-nez v7, :cond_3 */
/* .line 138 */
v7 = this.target;
/* move-object v8, v2 */
/* check-cast v8, Lmiui/android/animation/property/ColorProperty; */
/* double-to-int v9, v5 */
(( miui.android.animation.IAnimTarget ) v7 ).setIntValue ( v8, v9 ); // invoke-virtual {v7, v8, v9}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
/* .line 140 */
} // .end local v1 # "key":Ljava/lang/Object;
} // .end local v2 # "property":Lmiui/android/animation/property/FloatProperty;
} // .end local v3 # "curValue":D
} // .end local v5 # "fv":D
} // :cond_3
/* .line 141 */
} // :cond_4
return;
} // .end method
/* # virtual methods */
public Boolean containsProperty ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 120 */
v0 = this.to;
v0 = (( miui.android.animation.controller.AnimState ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/controller/AnimState;->contains(Ljava/lang/Object;)Z
} // .end method
public Integer getAnimCount ( ) {
/* .locals 1 */
/* .line 116 */
v0 = this.to;
v0 = (( miui.android.animation.controller.AnimState ) v0 ).keySet ( ); // invoke-virtual {v0}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
} // .end method
public miui.android.animation.internal.AnimStats getAnimStats ( ) {
/* .locals 4 */
/* .line 185 */
v0 = this.mAnimStats;
(( miui.android.animation.internal.AnimStats ) v0 ).clear ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/AnimStats;->clear()V
/* .line 186 */
v0 = this.animTasks;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lmiui/android/animation/internal/AnimTask; */
/* .line 187 */
/* .local v1, "task":Lmiui/android/animation/internal/AnimTask; */
v2 = this.mAnimStats;
v3 = this.animStats;
(( miui.android.animation.internal.AnimStats ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimStats;->add(Lmiui/android/animation/internal/AnimStats;)V
/* .line 188 */
} // .end local v1 # "task":Lmiui/android/animation/internal/AnimTask;
/* .line 189 */
} // :cond_0
v0 = this.mAnimStats;
} // .end method
public void initUpdateList ( miui.android.animation.internal.TransitionInfo$IUpdateInfoCreator p0 ) {
/* .locals 17 */
/* .param p1, "creator" # Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator; */
/* .line 144 */
/* move-object/from16 v0, p0 */
java.lang.System .nanoTime ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lmiui/android/animation/internal/TransitionInfo;->startTime:J */
/* .line 145 */
v1 = this.from;
/* .line 146 */
/* .local v1, "f":Lmiui/android/animation/controller/AnimState; */
v2 = this.to;
/* .line 147 */
/* .local v2, "t":Lmiui/android/animation/controller/AnimState; */
v3 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
/* .line 148 */
/* .local v3, "logEnabled":Z */
int v4 = 0; // const/4 v4, 0x0
final String v5 = "-- doSetup, target = "; // const-string v5, "-- doSetup, target = "
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 149 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.target;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ", key = "; // const-string v7, ", key = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.key;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ", f = "; // const-string v7, ", f = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = ", t = "; // const-string v7, ", t = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v7 = "\nconfig = "; // const-string v7, "\nconfig = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.config;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* new-array v7, v4, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v6,v7 );
/* .line 155 */
} // :cond_0
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* .line 156 */
/* .local v6, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;" */
(( miui.android.animation.controller.AnimState ) v2 ).keySet ( ); // invoke-virtual {v2}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
v8 = } // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 157 */
/* .local v8, "key":Ljava/lang/Object; */
(( miui.android.animation.controller.AnimState ) v2 ).getProperty ( v8 ); // invoke-virtual {v2, v8}, Lmiui/android/animation/controller/AnimState;->getProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;
/* .line 158 */
/* .local v9, "property":Lmiui/android/animation/property/FloatProperty; */
/* move-object/from16 v10, p1 */
/* .line 159 */
/* .local v11, "update":Lmiui/android/animation/listener/UpdateInfo; */
/* .line 160 */
v12 = this.animInfo;
v13 = this.target;
(( miui.android.animation.controller.AnimState ) v2 ).get ( v13, v9 ); // invoke-virtual {v2, v13, v9}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v13 */
/* iput-wide v13, v12, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
/* .line 161 */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 162 */
v12 = this.animInfo;
v13 = this.target;
(( miui.android.animation.controller.AnimState ) v1 ).get ( v13, v9 ); // invoke-virtual {v1, v13, v9}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v13 */
/* iput-wide v13, v12, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
/* .line 164 */
} // :cond_1
v12 = this.animInfo;
/* iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
/* .line 165 */
/* .local v12, "startValue":D */
v14 = this.target;
miui.android.animation.internal.AnimValueUtils .getValueOfTarget ( v14,v9,v12,v13 );
/* move-result-wide v14 */
/* .line 167 */
/* .local v14, "curValue":D */
v16 = miui.android.animation.internal.AnimValueUtils .isInvalid ( v14,v15 );
/* if-nez v16, :cond_2 */
/* .line 168 */
v4 = this.animInfo;
/* iput-wide v14, v4, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
/* .line 171 */
} // .end local v12 # "startValue":D
} // .end local v14 # "curValue":D
} // :cond_2
} // :goto_1
miui.android.animation.internal.AnimValueUtils .handleSetToValue ( v11 );
/* .line 172 */
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 173 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.target;
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v12 = ", property = "; // const-string v12, ", property = "
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 174 */
(( miui.android.animation.property.FloatProperty ) v9 ).getName ( ); // invoke-virtual {v9}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = ", startValue = "; // const-string v12, ", startValue = "
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.animInfo;
/* iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->startValue:D */
(( java.lang.StringBuilder ) v4 ).append ( v12, v13 ); // invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v12 = ", targetValue = "; // const-string v12, ", targetValue = "
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.animInfo;
/* iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->targetValue:D */
(( java.lang.StringBuilder ) v4 ).append ( v12, v13 ); // invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v12 = ", value = "; // const-string v12, ", value = "
(( java.lang.StringBuilder ) v4 ).append ( v12 ); // invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v12 = this.animInfo;
/* iget-wide v12, v12, Lmiui/android/animation/internal/AnimInfo;->value:D */
(( java.lang.StringBuilder ) v4 ).append ( v12, v13 ); // invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v12 = 0; // const/4 v12, 0x0
/* new-array v13, v12, [Ljava/lang/Object; */
/* .line 173 */
miui.android.animation.utils.LogUtils .debug ( v4,v13 );
/* .line 172 */
} // :cond_3
int v12 = 0; // const/4 v12, 0x0
/* .line 180 */
} // .end local v8 # "key":Ljava/lang/Object;
} // .end local v9 # "property":Lmiui/android/animation/property/FloatProperty;
} // .end local v11 # "update":Lmiui/android/animation/listener/UpdateInfo;
} // :goto_2
/* move v4, v12 */
/* goto/16 :goto_0 */
/* .line 181 */
} // :cond_4
/* move-object/from16 v10, p1 */
this.updateList = v6;
/* .line 182 */
return;
} // .end method
public void setupTasks ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "isInit" # Z */
/* .line 80 */
v0 = v0 = this.updateList;
/* .line 81 */
/* .local v0, "animCount":I */
/* div-int/lit16 v1, v0, 0xfa0 */
/* .line 82 */
/* .local v1, "splitCount":I */
int v2 = 1; // const/4 v2, 0x1
v1 = java.lang.Math .max ( v2,v1 );
/* .line 83 */
/* int-to-float v2, v0 */
/* int-to-float v3, v1 */
/* div-float/2addr v2, v3 */
/* float-to-double v2, v2 */
java.lang.Math .ceil ( v2,v3 );
/* move-result-wide v2 */
/* double-to-int v2, v2 */
/* .line 84 */
/* .local v2, "singleCount":I */
v3 = v3 = this.animTasks;
/* if-le v3, v1, :cond_0 */
/* .line 85 */
v4 = v3 = this.animTasks;
/* .line 87 */
} // :cond_0
v3 = v3 = this.animTasks;
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* .line 88 */
v4 = this.animTasks;
/* new-instance v5, Lmiui/android/animation/internal/AnimTask; */
/* invoke-direct {v5}, Lmiui/android/animation/internal/AnimTask;-><init>()V */
/* .line 87 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 91 */
} // .end local v3 # "i":I
} // :cond_1
} // :goto_1
int v3 = 0; // const/4 v3, 0x0
/* .line 92 */
/* .local v3, "startPos":I */
v4 = this.animTasks;
v5 = } // :goto_2
if ( v5 != null) { // if-eqz v5, :cond_4
/* check-cast v5, Lmiui/android/animation/internal/AnimTask; */
/* .line 93 */
/* .local v5, "task":Lmiui/android/animation/internal/AnimTask; */
this.info = p0;
/* .line 94 */
/* add-int v6, v3, v2 */
/* if-le v6, v0, :cond_2 */
/* sub-int v6, v0, v3 */
} // :cond_2
/* move v6, v2 */
/* .line 95 */
/* .local v6, "amount":I */
} // :goto_3
(( miui.android.animation.internal.AnimTask ) v5 ).setup ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Lmiui/android/animation/internal/AnimTask;->setup(II)V
/* .line 96 */
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 97 */
v7 = this.animStats;
/* iput v6, v7, Lmiui/android/animation/internal/AnimStats;->startCount:I */
/* .line 99 */
} // :cond_3
(( miui.android.animation.internal.AnimTask ) v5 ).updateAnimStats ( ); // invoke-virtual {v5}, Lmiui/android/animation/internal/AnimTask;->updateAnimStats()V
/* .line 101 */
} // :goto_4
/* add-int/2addr v3, v6 */
/* .line 102 */
} // .end local v5 # "task":Lmiui/android/animation/internal/AnimTask;
} // .end local v6 # "amount":I
/* .line 103 */
} // :cond_4
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 194 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "TransitionInfo{target = "; // const-string v1, "TransitionInfo{target = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.target;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 195 */
(( miui.android.animation.IAnimTarget ) v1 ).getTargetObject ( ); // invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", key = "; // const-string v1, ", key = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.key;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", propSize = "; // const-string v1, ", propSize = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.to;
/* .line 197 */
v1 = (( miui.android.animation.controller.AnimState ) v1 ).keySet ( ); // invoke-virtual {v1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", next = "; // const-string v1, ", next = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.next;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 194 */
} // .end method
