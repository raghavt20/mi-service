public class miui.android.animation.internal.PredictTask {
	 /* .source "PredictTask.java" */
	 /* # static fields */
	 private static final miui.android.animation.internal.TransitionInfo$IUpdateInfoCreator sCreator;
	 /* # direct methods */
	 static miui.android.animation.internal.PredictTask ( ) {
		 /* .locals 1 */
		 /* .line 15 */
		 /* new-instance v0, Lmiui/android/animation/internal/PredictTask$1; */
		 /* invoke-direct {v0}, Lmiui/android/animation/internal/PredictTask$1;-><init>()V */
		 return;
	 } // .end method
	 public miui.android.animation.internal.PredictTask ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Long predictDuration ( miui.android.animation.IAnimTarget p0, miui.android.animation.controller.AnimState p1, miui.android.animation.controller.AnimState p2, miui.android.animation.base.AnimConfigLink p3 ) {
		 /* .locals 18 */
		 /* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .param p1, "from" # Lmiui/android/animation/controller/AnimState; */
		 /* .param p2, "to" # Lmiui/android/animation/controller/AnimState; */
		 /* .param p3, "configLink" # Lmiui/android/animation/base/AnimConfigLink; */
		 /* .line 24 */
		 /* new-instance v0, Lmiui/android/animation/internal/TransitionInfo; */
		 /* move-object/from16 v1, p0 */
		 /* move-object/from16 v2, p1 */
		 /* move-object/from16 v3, p2 */
		 /* move-object/from16 v4, p3 */
		 /* invoke-direct {v0, v1, v2, v3, v4}, Lmiui/android/animation/internal/TransitionInfo;-><init>(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V */
		 /* .line 25 */
		 /* .local v0, "transInfo":Lmiui/android/animation/internal/TransitionInfo; */
		 v5 = miui.android.animation.internal.PredictTask.sCreator;
		 (( miui.android.animation.internal.TransitionInfo ) v0 ).initUpdateList ( v5 ); // invoke-virtual {v0, v5}, Lmiui/android/animation/internal/TransitionInfo;->initUpdateList(Lmiui/android/animation/internal/TransitionInfo$IUpdateInfoCreator;)V
		 /* .line 26 */
		 int v5 = 1; // const/4 v5, 0x1
		 (( miui.android.animation.internal.TransitionInfo ) v0 ).setupTasks ( v5 ); // invoke-virtual {v0, v5}, Lmiui/android/animation/internal/TransitionInfo;->setupTasks(Z)V
		 /* .line 27 */
		 miui.android.animation.internal.AnimRunner .getInst ( );
		 (( miui.android.animation.internal.AnimRunner ) v5 ).getAverageDelta ( ); // invoke-virtual {v5}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J
		 /* move-result-wide v13 */
		 /* .line 28 */
		 /* .local v13, "deltaT":J */
		 /* move-wide v5, v13 */
		 /* move-wide v15, v5 */
		 /* .line 30 */
		 /* .local v15, "totalT":J */
	 } // :goto_0
	 v5 = this.animTasks;
v6 = } // :goto_1
if ( v6 != null) { // if-eqz v6, :cond_0
	 /* move-object/from16 v17, v6 */
	 /* check-cast v17, Lmiui/android/animation/internal/AnimTask; */
	 /* .line 31 */
	 /* .local v17, "task":Lmiui/android/animation/internal/AnimTask; */
	 int v11 = 0; // const/4 v11, 0x0
	 int v12 = 1; // const/4 v12, 0x1
	 /* move-object/from16 v6, v17 */
	 /* move-wide v7, v15 */
	 /* move-wide v9, v13 */
	 /* invoke-static/range {v6 ..v12}, Lmiui/android/animation/internal/AnimRunnerTask;->doAnimationFrame(Lmiui/android/animation/internal/AnimTask;JJZZ)V */
	 /* .line 32 */
} // .end local v17 # "task":Lmiui/android/animation/internal/AnimTask;
/* .line 33 */
} // :cond_0
(( miui.android.animation.internal.TransitionInfo ) v0 ).getAnimStats ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/TransitionInfo;->getAnimStats()Lmiui/android/animation/internal/AnimStats;
/* .line 34 */
/* .local v5, "stats":Lmiui/android/animation/internal/AnimStats; */
v6 = (( miui.android.animation.internal.AnimStats ) v5 ).isRunning ( ); // invoke-virtual {v5}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 35 */
/* add-long/2addr v15, v13 */
/* .line 39 */
} // .end local v5 # "stats":Lmiui/android/animation/internal/AnimStats;
/* .line 40 */
} // :cond_1
/* return-wide v15 */
} // .end method
