.class Lmiui/android/animation/internal/RunnerHandler;
.super Landroid/os/Handler;
.source "RunnerHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/internal/RunnerHandler$SetToInfo;
    }
.end annotation


# static fields
.field public static final ANIM_MSG_RUNNER_RUN:I = 0x3

.field public static final ANIM_MSG_SETUP:I = 0x1

.field public static final ANIM_MSG_SET_TO:I = 0x4

.field public static final ANIM_MSG_UPDATE:I = 0x2


# instance fields
.field private final mDelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/IAnimTarget;",
            ">;"
        }
    .end annotation
.end field

.field private mFrameCount:I

.field private mIsTaskRunning:Z

.field private mLastRun:J

.field private final mOpMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiui/android/animation/IAnimTarget;",
            "Lmiui/android/animation/internal/AnimOperationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRunnerStart:Z

.field private final mSplitInfo:[I

.field private mStart:Z

.field private final mTaskList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/AnimTask;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalT:J

.field private final mTransList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mTransMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiui/android/animation/IAnimTarget;",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final runningTarget:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiui/android/animation/IAnimTarget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;

    .line 66
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    .line 41
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mOpMap:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mDelList:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransList:Ljava/util/List;

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mLastRun:J

    .line 57
    iput-wide v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mSplitInfo:[I

    .line 67
    return-void
.end method

.method private addAnimTask(Ljava/util/List;II)V
    .locals 7
    .param p2, "singleCount"    # I
    .param p3, "splitCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;II)V"
        }
    .end annotation

    .line 350
    .local p1, "transList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/internal/TransitionInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/TransitionInfo;

    .line 351
    .local v1, "info":Lmiui/android/animation/internal/TransitionInfo;
    iget-object v2, v1, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/android/animation/internal/AnimTask;

    .line 352
    .local v3, "task":Lmiui/android/animation/internal/AnimTask;
    invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->getTaskOfMinCount()Lmiui/android/animation/internal/AnimTask;

    move-result-object v4

    .line 353
    .local v4, "curTask":Lmiui/android/animation/internal/AnimTask;
    if-eqz v4, :cond_1

    iget-object v5, p0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v5, p3, :cond_0

    .line 354
    invoke-virtual {v4}, Lmiui/android/animation/internal/AnimTask;->getTotalAnimCount()I

    move-result v5

    invoke-virtual {v3}, Lmiui/android/animation/internal/AnimTask;->getAnimCount()I

    move-result v6

    add-int/2addr v5, v6

    if-le v5, p2, :cond_0

    goto :goto_2

    .line 357
    :cond_0
    invoke-virtual {v4, v3}, Lmiui/android/animation/internal/AnimTask;->addToTail(Lmiui/android/animation/utils/LinkNode;)V

    goto :goto_3

    .line 355
    :cond_1
    :goto_2
    iget-object v5, p0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    .end local v3    # "task":Lmiui/android/animation/internal/AnimTask;
    .end local v4    # "curTask":Lmiui/android/animation/internal/AnimTask;
    :goto_3
    goto :goto_1

    .line 360
    .end local v1    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_2
    goto :goto_0

    .line 361
    :cond_3
    return-void
.end method

.method private addToMap(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/utils/LinkNode;Ljava/util/Map;)V
    .locals 1
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lmiui/android/animation/utils/LinkNode;",
            ">(",
            "Lmiui/android/animation/IAnimTarget;",
            "TT;",
            "Ljava/util/Map<",
            "Lmiui/android/animation/IAnimTarget;",
            "TT;>;)V"
        }
    .end annotation

    .line 154
    .local p2, "node":Lmiui/android/animation/utils/LinkNode;, "TT;"
    .local p3, "map":Ljava/util/Map;, "Ljava/util/Map<Lmiui/android/animation/IAnimTarget;TT;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/utils/LinkNode;

    .line 155
    .local v0, "head":Lmiui/android/animation/utils/LinkNode;, "TT;"
    if-nez v0, :cond_0

    .line 156
    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 158
    :cond_0
    invoke-virtual {v0, p2}, Lmiui/android/animation/utils/LinkNode;->addToTail(Lmiui/android/animation/utils/LinkNode;)V

    .line 160
    :goto_0
    return-void
.end method

.method private static doSetOperation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;Lmiui/android/animation/internal/AnimOperationInfo;)V
    .locals 5
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "stats"    # Lmiui/android/animation/internal/AnimStats;
    .param p2, "update"    # Lmiui/android/animation/listener/UpdateInfo;
    .param p3, "opInfo"    # Lmiui/android/animation/internal/AnimOperationInfo;

    .line 299
    iget-object v0, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B

    .line 300
    .local v0, "op":B
    invoke-static {v0}, Lmiui/android/animation/internal/AnimTask;->isRunning(B)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B

    if-eqz v1, :cond_4

    iget-object v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->propList:Ljava/util/List;

    iget-object v2, p2, Lmiui/android/animation/listener/UpdateInfo;->property:Lmiui/android/animation/property/FloatProperty;

    .line 302
    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    iget-object v1, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v1, v1, Lmiui/android/animation/internal/AnimInfo;->op:B

    .line 303
    invoke-static {v1}, Lmiui/android/animation/internal/AnimTask;->isRunning(B)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 304
    iget v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->usedCount:I

    .line 305
    iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 306
    iget-object v1, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->targetValue:D

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_1

    .line 307
    iget-object v1, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-object v2, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v2, v2, Lmiui/android/animation/internal/AnimInfo;->targetValue:D

    iput-wide v2, v1, Lmiui/android/animation/internal/AnimInfo;->value:D

    .line 309
    :cond_1
    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lmiui/android/animation/internal/AnimStats;->endCount:I

    .line 310
    iget v1, p1, Lmiui/android/animation/internal/AnimStats;->endCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lmiui/android/animation/internal/AnimStats;->endCount:I

    goto :goto_0

    .line 311
    :cond_2
    iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 312
    iget-object v1, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v2, v1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    .line 313
    iget v1, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    .line 315
    :cond_3
    :goto_0
    iget-byte v1, p3, Lmiui/android/animation/internal/AnimOperationInfo;->op:B

    invoke-virtual {p2, v1}, Lmiui/android/animation/listener/UpdateInfo;->setOp(B)V

    .line 316
    invoke-static {p0, p1, p2, v0}, Lmiui/android/animation/internal/TransitionInfo;->decreaseStartCountForDelayAnim(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;B)V

    .line 318
    :cond_4
    return-void
.end method

.method private doSetup()V
    .locals 4

    .line 385
    iget-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/internal/TransitionInfo;

    .line 386
    .local v1, "info":Lmiui/android/animation/internal/TransitionInfo;
    iget-object v2, p0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    iget-object v3, v1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 387
    move-object v2, v1

    .line 389
    .local v2, "head":Lmiui/android/animation/internal/TransitionInfo;
    :cond_0
    iget-object v3, v2, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v3, v3, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v3, v2}, Lmiui/android/animation/internal/AnimManager;->setupTransition(Lmiui/android/animation/internal/TransitionInfo;)V

    .line 390
    invoke-virtual {v2}, Lmiui/android/animation/internal/TransitionInfo;->remove()Lmiui/android/animation/utils/LinkNode;

    move-result-object v3

    move-object v2, v3

    check-cast v2, Lmiui/android/animation/internal/TransitionInfo;

    .line 391
    if-nez v2, :cond_0

    .line 392
    .end local v1    # "info":Lmiui/android/animation/internal/TransitionInfo;
    .end local v2    # "head":Lmiui/android/animation/internal/TransitionInfo;
    goto :goto_0

    .line 393
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 394
    iget-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z

    if-nez v0, :cond_2

    .line 395
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z

    .line 396
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/internal/AnimRunner;->start()V

    .line 398
    :cond_2
    return-void
.end method

.method private getTaskOfMinCount()Lmiui/android/animation/internal/AnimTask;
    .locals 5

    .line 364
    const/4 v0, 0x0

    .line 365
    .local v0, "state":Lmiui/android/animation/internal/AnimTask;
    const v1, 0x7fffffff

    .line 366
    .local v1, "min":I
    iget-object v2, p0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/android/animation/internal/AnimTask;

    .line 367
    .local v3, "s":Lmiui/android/animation/internal/AnimTask;
    invoke-virtual {v3}, Lmiui/android/animation/internal/AnimTask;->getTotalAnimCount()I

    move-result v4

    .line 368
    .local v4, "totalCount":I
    if-ge v4, v1, :cond_0

    .line 369
    move v1, v4

    .line 370
    move-object v0, v3

    .line 372
    .end local v3    # "s":Lmiui/android/animation/internal/AnimTask;
    .end local v4    # "totalCount":I
    :cond_0
    goto :goto_0

    .line 373
    :cond_1
    return-object v0
.end method

.method private getTotalAnimCount()I
    .locals 4

    .line 377
    const/4 v0, 0x0

    .line 378
    .local v0, "count":I
    iget-object v1, p0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/IAnimTarget;

    .line 379
    .local v2, "target":Lmiui/android/animation/IAnimTarget;
    iget-object v3, v2, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v3}, Lmiui/android/animation/internal/AnimManager;->getTotalAnimCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 380
    .end local v2    # "target":Lmiui/android/animation/IAnimTarget;
    goto :goto_0

    .line 381
    :cond_0
    return v0
.end method

.method private static handleSetTo(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;)Z
    .locals 3
    .param p0, "task"    # Lmiui/android/animation/internal/AnimTask;
    .param p1, "stats"    # Lmiui/android/animation/internal/AnimStats;
    .param p2, "update"    # Lmiui/android/animation/listener/UpdateInfo;

    .line 285
    invoke-static {p2}, Lmiui/android/animation/internal/AnimValueUtils;->handleSetToValue(Lmiui/android/animation/listener/UpdateInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    const/4 v0, 0x0

    return v0

    .line 288
    :cond_0
    iget-object v0, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B

    invoke-static {v0}, Lmiui/android/animation/internal/AnimTask;->isRunning(B)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lmiui/android/animation/internal/AnimTask;->animStats:Lmiui/android/animation/internal/AnimStats;

    iget v2, v0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    add-int/2addr v2, v1

    iput v2, v0, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    .line 290
    iget v0, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    add-int/2addr v0, v1

    iput v0, p1, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    .line 291
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lmiui/android/animation/listener/UpdateInfo;->setOp(B)V

    .line 292
    iget-object v0, p2, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-byte v0, v0, Lmiui/android/animation/internal/AnimInfo;->op:B

    invoke-static {p0, p1, p2, v0}, Lmiui/android/animation/internal/TransitionInfo;->decreaseStartCountForDelayAnim(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;B)V

    .line 294
    :cond_1
    return v1
.end method

.method private static handleUpdate(Lmiui/android/animation/internal/TransitionInfo;Lmiui/android/animation/internal/AnimOperationInfo;Lmiui/android/animation/internal/AnimStats;)V
    .locals 8
    .param p0, "info"    # Lmiui/android/animation/internal/TransitionInfo;
    .param p1, "opInfo"    # Lmiui/android/animation/internal/AnimOperationInfo;
    .param p2, "stats"    # Lmiui/android/animation/internal/AnimStats;

    .line 264
    iget-object v0, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v0, v0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v0, v0, Lmiui/android/animation/internal/AnimManager;->mStartAnim:Ljava/util/Set;

    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 265
    .local v0, "isRunningAnim":Z
    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->animTasks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/internal/AnimTask;

    .line 266
    .local v2, "task":Lmiui/android/animation/internal/AnimTask;
    iget-object v3, p0, Lmiui/android/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    .line 267
    .local v3, "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    iget v4, v2, Lmiui/android/animation/internal/AnimTask;->startPos:I

    .local v4, "i":I
    invoke-virtual {v2}, Lmiui/android/animation/internal/AnimTask;->getAnimCount()I

    move-result v5

    add-int/2addr v5, v4

    .local v5, "n":I
    :goto_1
    if-ge v4, v5, :cond_1

    .line 268
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiui/android/animation/listener/UpdateInfo;

    .line 269
    .local v6, "update":Lmiui/android/animation/listener/UpdateInfo;
    invoke-static {v2, p2, v6}, Lmiui/android/animation/internal/RunnerHandler;->handleSetTo(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;)Z

    move-result v7

    if-nez v7, :cond_0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 270
    invoke-static {v2, p2, v6, p1}, Lmiui/android/animation/internal/RunnerHandler;->doSetOperation(Lmiui/android/animation/internal/AnimTask;Lmiui/android/animation/internal/AnimStats;Lmiui/android/animation/listener/UpdateInfo;Lmiui/android/animation/internal/AnimOperationInfo;)V

    .line 267
    .end local v6    # "update":Lmiui/android/animation/listener/UpdateInfo;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 273
    .end local v2    # "task":Lmiui/android/animation/internal/AnimTask;
    .end local v3    # "updateList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/listener/UpdateInfo;>;"
    .end local v4    # "i":I
    .end local v5    # "n":I
    :cond_1
    goto :goto_0

    .line 275
    :cond_2
    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v1, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v1, v1, Lmiui/android/animation/internal/AnimManager;->mStartAnim:Ljava/util/Set;

    iget-object v2, p0, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 276
    invoke-virtual {p2}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p2, Lmiui/android/animation/internal/AnimStats;->updateCount:I

    if-lez v1, :cond_3

    .line 277
    sget-object v1, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v2, p0, Lmiui/android/animation/internal/TransitionInfo;->id:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v1, p0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v1, v1, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    iget v2, p0, Lmiui/android/animation/internal/TransitionInfo;->id:I

    .line 279
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2, v3}, Lmiui/android/animation/internal/TargetHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 280
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 282
    :cond_3
    return-void
.end method

.method private isInfoInTransMap(Lmiui/android/animation/internal/TransitionInfo;)Z
    .locals 2
    .param p1, "info"    # Lmiui/android/animation/internal/TransitionInfo;

    .line 210
    iget-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    iget-object v1, p1, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 211
    .local v0, "node":Lmiui/android/animation/internal/TransitionInfo;
    :goto_0
    if-eqz v0, :cond_1

    .line 212
    if-ne v0, p1, :cond_0

    .line 213
    const/4 v1, 0x1

    return v1

    .line 215
    :cond_0
    iget-object v1, v0, Lmiui/android/animation/internal/TransitionInfo;->next:Lmiui/android/animation/utils/LinkNode;

    move-object v0, v1

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    goto :goto_0

    .line 217
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method private onSetTo(Lmiui/android/animation/internal/RunnerHandler$SetToInfo;)V
    .locals 10
    .param p1, "setInfo"    # Lmiui/android/animation/internal/RunnerHandler$SetToInfo;

    .line 129
    iget-object v0, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    instance-of v0, v0, Lmiui/android/animation/ViewTarget;

    .line 130
    .local v0, "isViewTarget":Z
    iget-object v1, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->state:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v1}, Lmiui/android/animation/controller/AnimState;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 131
    .local v2, "key":Ljava/lang/Object;
    iget-object v3, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->state:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v3, v2}, Lmiui/android/animation/controller/AnimState;->getProperty(Ljava/lang/Object;)Lmiui/android/animation/property/FloatProperty;

    move-result-object v3

    .line 132
    .local v3, "property":Lmiui/android/animation/property/FloatProperty;
    iget-object v4, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v4, v4, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v4, v4, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/android/animation/listener/UpdateInfo;

    .line 133
    .local v4, "update":Lmiui/android/animation/listener/UpdateInfo;
    iget-object v5, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->state:Lmiui/android/animation/controller/AnimState;

    iget-object v6, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v5, v6, v3}, Lmiui/android/animation/controller/AnimState;->get(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v5

    .line 134
    .local v5, "value":D
    if-eqz v4, :cond_0

    .line 135
    iget-object v7, v4, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iput-wide v5, v7, Lmiui/android/animation/internal/AnimInfo;->setToValue:D

    .line 136
    if-nez v0, :cond_2

    .line 137
    iget-object v7, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-virtual {v4, v7}, Lmiui/android/animation/listener/UpdateInfo;->setTargetValue(Lmiui/android/animation/IAnimTarget;)V

    goto :goto_1

    .line 140
    :cond_0
    instance-of v7, v3, Lmiui/android/animation/property/IIntValueProperty;

    if-eqz v7, :cond_1

    .line 141
    iget-object v7, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    move-object v8, v3

    check-cast v8, Lmiui/android/animation/property/IIntValueProperty;

    double-to-int v9, v5

    invoke-virtual {v7, v8, v9}, Lmiui/android/animation/IAnimTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V

    goto :goto_1

    .line 143
    :cond_1
    iget-object v7, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    double-to-float v8, v5

    invoke-virtual {v7, v3, v8}, Lmiui/android/animation/IAnimTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V

    .line 146
    .end local v2    # "key":Ljava/lang/Object;
    .end local v3    # "property":Lmiui/android/animation/property/FloatProperty;
    .end local v4    # "update":Lmiui/android/animation/listener/UpdateInfo;
    .end local v5    # "value":D
    :cond_2
    :goto_1
    goto :goto_0

    .line 147
    :cond_3
    iget-object v1, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    const/4 v2, 0x0

    new-array v2, v2, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v1, v2}, Lmiui/android/animation/IAnimTarget;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 148
    iget-object v1, p1, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v1, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v1, v1, Lmiui/android/animation/internal/AnimManager;->mUpdateMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 150
    :cond_4
    return-void
.end method

.method private runAnim(JJZ)V
    .locals 18
    .param p1, "now"    # J
    .param p3, "deltaT"    # J
    .param p5, "toPage"    # Z

    .line 321
    move-object/from16 v0, p0

    iget-object v1, v0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    invoke-direct/range {p0 .. p0}, Lmiui/android/animation/internal/RunnerHandler;->stopAnimRunner()V

    .line 323
    return-void

    .line 325
    :cond_0
    move-wide/from16 v1, p1

    iput-wide v1, v0, Lmiui/android/animation/internal/RunnerHandler;->mLastRun:J

    .line 326
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v3

    invoke-virtual {v3}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J

    move-result-wide v3

    .line 327
    .local v3, "averageDelta":J
    iget v5, v0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    const-wide/16 v7, 0x2

    mul-long/2addr v7, v3

    cmp-long v7, p3, v7

    if-lez v7, :cond_1

    .line 328
    move-wide v7, v3

    .end local p3    # "deltaT":J
    .local v7, "deltaT":J
    goto :goto_0

    .line 330
    .end local v7    # "deltaT":J
    .restart local p3    # "deltaT":J
    :cond_1
    move-wide/from16 v7, p3

    .end local p3    # "deltaT":J
    .restart local v7    # "deltaT":J
    :goto_0
    iget-wide v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    add-long/2addr v9, v7

    iput-wide v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    .line 331
    add-int/2addr v5, v6

    iput v5, v0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I

    .line 332
    invoke-direct/range {p0 .. p0}, Lmiui/android/animation/internal/RunnerHandler;->getTotalAnimCount()I

    move-result v5

    .line 333
    .local v5, "animCount":I
    iget-object v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mSplitInfo:[I

    invoke-static {v5, v9}, Lmiui/android/animation/internal/ThreadPoolUtil;->getSplitCount(I[I)V

    .line 334
    iget-object v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mSplitInfo:[I

    const/4 v10, 0x0

    aget v15, v9, v10

    .line 335
    .local v15, "splitCount":I
    aget v14, v9, v6

    .line 336
    .local v14, "singleCount":I
    iget-object v9, v0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lmiui/android/animation/IAnimTarget;

    .line 337
    .local v10, "target":Lmiui/android/animation/IAnimTarget;
    iget-object v11, v10, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v12, v0, Lmiui/android/animation/internal/RunnerHandler;->mTransList:Ljava/util/List;

    invoke-virtual {v11, v12}, Lmiui/android/animation/internal/AnimManager;->getTransitionInfos(Ljava/util/List;)V

    .line 338
    .end local v10    # "target":Lmiui/android/animation/IAnimTarget;
    goto :goto_1

    .line 339
    :cond_2
    iget-object v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTransList:Ljava/util/List;

    invoke-direct {v0, v9, v14, v15}, Lmiui/android/animation/internal/RunnerHandler;->addAnimTask(Ljava/util/List;II)V

    .line 340
    iget-object v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v9

    xor-int/2addr v6, v9

    iput-boolean v6, v0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z

    .line 341
    sget-object v6, Lmiui/android/animation/internal/AnimTask;->sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object v9, v0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v6, v9}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 342
    iget-object v6, v0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    move-object/from16 v16, v9

    check-cast v16, Lmiui/android/animation/internal/AnimTask;

    .line 343
    .local v16, "task":Lmiui/android/animation/internal/AnimTask;
    iget-wide v10, v0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    move-object/from16 v9, v16

    move-wide v12, v7

    move/from16 v17, v14

    .end local v14    # "singleCount":I
    .local v17, "singleCount":I
    move/from16 v14, p5

    invoke-virtual/range {v9 .. v14}, Lmiui/android/animation/internal/AnimTask;->start(JJZ)V

    .line 344
    .end local v16    # "task":Lmiui/android/animation/internal/AnimTask;
    move/from16 v14, v17

    goto :goto_2

    .line 345
    .end local v17    # "singleCount":I
    .restart local v14    # "singleCount":I
    :cond_3
    iget-object v6, v0, Lmiui/android/animation/internal/RunnerHandler;->mTransList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 346
    iget-object v6, v0, Lmiui/android/animation/internal/RunnerHandler;->mTaskList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 347
    return-void
.end method

.method private setupWaitTrans(Lmiui/android/animation/IAnimTarget;)Z
    .locals 3
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;

    .line 201
    iget-object v0, p1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget-object v0, v0, Lmiui/android/animation/internal/AnimManager;->mWaitState:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 202
    .local v0, "info":Lmiui/android/animation/internal/TransitionInfo;
    if-eqz v0, :cond_0

    .line 203
    iget-object v1, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v2, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    invoke-direct {p0, v1, v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->addToMap(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/utils/LinkNode;Ljava/util/Map;)V

    .line 204
    const/4 v1, 0x1

    return v1

    .line 206
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method private stopAnimRunner()V
    .locals 3

    .line 163
    iget-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z

    if-eqz v0, :cond_1

    .line 164
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "total time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "frame count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "RunnerHandler.stopAnimRunner"

    invoke-static {v1, v0}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z

    .line 171
    iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z

    .line 172
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    .line 173
    iput v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I

    .line 174
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/internal/AnimRunner;->end()V

    .line 176
    :cond_1
    return-void
.end method

.method private updateAnim()V
    .locals 4

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z

    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "isRunning":Z
    iget-object v1, p0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/android/animation/IAnimTarget;

    .line 182
    .local v2, "target":Lmiui/android/animation/IAnimTarget;
    iget-object v3, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransList:Ljava/util/List;

    invoke-direct {p0, v2, v3}, Lmiui/android/animation/internal/RunnerHandler;->updateTarget(Lmiui/android/animation/IAnimTarget;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v2}, Lmiui/android/animation/internal/RunnerHandler;->setupWaitTrans(Lmiui/android/animation/IAnimTarget;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    .line 185
    :cond_0
    iget-object v3, p0, Lmiui/android/animation/internal/RunnerHandler;->mDelList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 183
    :cond_1
    :goto_1
    const/4 v0, 0x1

    .line 187
    :goto_2
    iget-object v3, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 188
    .end local v2    # "target":Lmiui/android/animation/IAnimTarget;
    goto :goto_0

    .line 189
    :cond_2
    iget-object v1, p0, Lmiui/android/animation/internal/RunnerHandler;->runningTarget:Ljava/util/Set;

    iget-object v2, p0, Lmiui/android/animation/internal/RunnerHandler;->mDelList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 190
    iget-object v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mDelList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 191
    iget-object v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 192
    invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->doSetup()V

    .line 193
    const/4 v0, 0x1

    .line 195
    :cond_3
    if-nez v0, :cond_4

    .line 196
    invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->stopAnimRunner()V

    .line 198
    :cond_4
    return-void
.end method

.method private updateTarget(Lmiui/android/animation/IAnimTarget;Ljava/util/List;)Z
    .locals 16
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/android/animation/IAnimTarget;",
            "Ljava/util/List<",
            "Lmiui/android/animation/internal/TransitionInfo;",
            ">;)Z"
        }
    .end annotation

    .line 221
    .local p2, "transList":Ljava/util/List;, "Ljava/util/List<Lmiui/android/animation/internal/TransitionInfo;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    move-object/from16 v3, p2

    invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimManager;->getTransitionInfos(Ljava/util/List;)V

    .line 222
    const/4 v2, 0x0

    .line 223
    .local v2, "runCount":I
    const/4 v4, 0x0

    .line 224
    .local v4, "animStartAfterCancel":I
    iget-object v5, v0, Lmiui/android/animation/internal/RunnerHandler;->mOpMap:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiui/android/animation/internal/AnimOperationInfo;

    .line 225
    .local v5, "opInfo":Lmiui/android/animation/internal/AnimOperationInfo;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-eqz v7, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lmiui/android/animation/internal/TransitionInfo;

    .line 226
    .local v7, "info":Lmiui/android/animation/internal/TransitionInfo;
    invoke-direct {v0, v7}, Lmiui/android/animation/internal/RunnerHandler;->isInfoInTransMap(Lmiui/android/animation/internal/TransitionInfo;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 227
    add-int/lit8 v2, v2, 0x1

    .line 228
    goto :goto_0

    .line 230
    :cond_0
    move-object v10, v5

    .line 231
    .local v10, "useOp":Lmiui/android/animation/internal/AnimOperationInfo;
    if-eqz v10, :cond_1

    iget-wide v11, v7, Lmiui/android/animation/internal/TransitionInfo;->startTime:J

    iget-wide v13, v10, Lmiui/android/animation/internal/AnimOperationInfo;->sendTime:J

    cmp-long v11, v11, v13

    if-lez v11, :cond_1

    .line 232
    const/4 v10, 0x0

    .line 233
    add-int/lit8 v4, v4, 0x1

    .line 235
    :cond_1
    invoke-virtual {v7}, Lmiui/android/animation/internal/TransitionInfo;->getAnimStats()Lmiui/android/animation/internal/AnimStats;

    move-result-object v11

    .line 236
    .local v11, "stats":Lmiui/android/animation/internal/AnimStats;
    invoke-virtual {v11}, Lmiui/android/animation/internal/AnimStats;->isStarted()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 237
    invoke-static {v7, v10, v11}, Lmiui/android/animation/internal/RunnerHandler;->handleUpdate(Lmiui/android/animation/internal/TransitionInfo;Lmiui/android/animation/internal/AnimOperationInfo;Lmiui/android/animation/internal/AnimStats;)V

    .line 239
    :cond_2
    invoke-static {}, Lmiui/android/animation/utils/LogUtils;->isLogEnabled()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 240
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "---- updateAnim, target = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x6

    new-array v13, v13, [Ljava/lang/Object;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "key = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v7, Lmiui/android/animation/internal/TransitionInfo;->key:Ljava/lang/Object;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "useOp = "

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v13, v9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "info.startTime = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v14, v7, Lmiui/android/animation/internal/TransitionInfo;->startTime:J

    invoke-virtual {v8, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    aput-object v8, v13, v9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "opInfo.time = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz v5, :cond_3

    iget-wide v14, v5, Lmiui/android/animation/internal/AnimOperationInfo;->sendTime:J

    .line 244
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    :goto_1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    aput-object v8, v13, v9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v14, "stats.isRunning = "

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 245
    invoke-virtual {v11}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z

    move-result v14

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v14, 0x4

    aput-object v8, v13, v14

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v15, "stats = "

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v15, 0x5

    aput-object v8, v13, v15

    .line 240
    invoke-static {v12, v13}, Lmiui/android/animation/utils/LogUtils;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 239
    :cond_4
    const/4 v9, 0x3

    const/4 v14, 0x4

    .line 249
    :goto_2
    invoke-virtual {v11}, Lmiui/android/animation/internal/AnimStats;->isRunning()Z

    move-result v8

    if-nez v8, :cond_6

    .line 250
    iget-object v8, v1, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    iget v12, v11, Lmiui/android/animation/internal/AnimStats;->cancelCount:I

    iget v13, v11, Lmiui/android/animation/internal/AnimStats;->endCount:I

    if-le v12, v13, :cond_5

    move v13, v14

    goto :goto_3

    :cond_5
    move v13, v9

    :goto_3
    const/4 v9, 0x2

    invoke-virtual {v8, v7, v9, v13}, Lmiui/android/animation/internal/AnimManager;->notifyTransitionEnd(Lmiui/android/animation/internal/TransitionInfo;II)V

    goto :goto_4

    .line 253
    :cond_6
    add-int/lit8 v2, v2, 0x1

    .line 255
    .end local v7    # "info":Lmiui/android/animation/internal/TransitionInfo;
    .end local v10    # "useOp":Lmiui/android/animation/internal/AnimOperationInfo;
    .end local v11    # "stats":Lmiui/android/animation/internal/AnimStats;
    :goto_4
    goto/16 :goto_0

    .line 256
    :cond_7
    if-eqz v5, :cond_9

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v6

    if-eq v4, v6, :cond_8

    invoke-virtual {v5}, Lmiui/android/animation/internal/AnimOperationInfo;->isUsed()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 257
    :cond_8
    iget-object v6, v0, Lmiui/android/animation/internal/RunnerHandler;->mOpMap:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    :cond_9
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->clear()V

    .line 260
    if-lez v2, :cond_a

    move v8, v9

    :cond_a
    return v8
.end method


# virtual methods
.method public addSetToState(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/controller/AnimState;)V
    .locals 2
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "to"    # Lmiui/android/animation/controller/AnimState;

    .line 77
    new-instance v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;-><init>(Lmiui/android/animation/internal/RunnerHandler$1;)V

    .line 78
    .local v0, "info":Lmiui/android/animation/internal/RunnerHandler$SetToInfo;
    iput-object p1, v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->target:Lmiui/android/animation/IAnimTarget;

    .line 79
    iget-boolean v1, p2, Lmiui/android/animation/controller/AnimState;->isTemporary:Z

    if-eqz v1, :cond_0

    .line 80
    new-instance v1, Lmiui/android/animation/controller/AnimState;

    invoke-direct {v1}, Lmiui/android/animation/controller/AnimState;-><init>()V

    iput-object v1, v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->state:Lmiui/android/animation/controller/AnimState;

    .line 81
    iget-object v1, v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->state:Lmiui/android/animation/controller/AnimState;

    invoke-virtual {v1, p2}, Lmiui/android/animation/controller/AnimState;->set(Lmiui/android/animation/controller/AnimState;)V

    goto :goto_0

    .line 83
    :cond_0
    iput-object p2, v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;->state:Lmiui/android/animation/controller/AnimState;

    .line 85
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lmiui/android/animation/internal/RunnerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 86
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .line 90
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_1

    .line 92
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmiui/android/animation/internal/RunnerHandler$SetToInfo;

    invoke-direct {p0, v0}, Lmiui/android/animation/internal/RunnerHandler;->onSetTo(Lmiui/android/animation/internal/RunnerHandler$SetToInfo;)V

    .line 93
    goto :goto_1

    .line 105
    :pswitch_1
    iget-boolean v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mRunnerStart:Z

    if-eqz v0, :cond_3

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 107
    .local v7, "now":J
    invoke-static {}, Lmiui/android/animation/internal/AnimRunner;->getInst()Lmiui/android/animation/internal/AnimRunner;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/internal/AnimRunner;->getAverageDelta()J

    move-result-wide v9

    .line 108
    .local v9, "averageDelta":J
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 109
    .local v0, "toPage":Z
    iget-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z

    if-nez v1, :cond_0

    .line 110
    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mStart:Z

    .line 111
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mTotalT:J

    .line 112
    const/4 v1, 0x0

    iput v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mFrameCount:I

    .line 113
    move-object v1, p0

    move-wide v2, v7

    move-wide v4, v9

    move v6, v0

    invoke-direct/range {v1 .. v6}, Lmiui/android/animation/internal/RunnerHandler;->runAnim(JJZ)V

    goto :goto_0

    .line 114
    :cond_0
    iget-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z

    if-nez v1, :cond_1

    .line 115
    iget-wide v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mLastRun:J

    sub-long v4, v7, v1

    move-object v1, p0

    move-wide v2, v7

    move v6, v0

    invoke-direct/range {v1 .. v6}, Lmiui/android/animation/internal/RunnerHandler;->runAnim(JJZ)V

    .line 117
    .end local v0    # "toPage":Z
    .end local v7    # "now":J
    .end local v9    # "averageDelta":J
    :cond_1
    :goto_0
    goto :goto_1

    .line 120
    :pswitch_2
    invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->updateAnim()V

    .line 121
    goto :goto_1

    .line 95
    :pswitch_3
    sget-object v0, Lmiui/android/animation/internal/TransitionInfo;->sMap:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/internal/TransitionInfo;

    .line 96
    .local v0, "info":Lmiui/android/animation/internal/TransitionInfo;
    if-eqz v0, :cond_2

    .line 97
    iget-object v1, v0, Lmiui/android/animation/internal/TransitionInfo;->target:Lmiui/android/animation/IAnimTarget;

    iget-object v2, p0, Lmiui/android/animation/internal/RunnerHandler;->mTransMap:Ljava/util/Map;

    invoke-direct {p0, v1, v0, v2}, Lmiui/android/animation/internal/RunnerHandler;->addToMap(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/utils/LinkNode;Ljava/util/Map;)V

    .line 98
    iget-boolean v1, p0, Lmiui/android/animation/internal/RunnerHandler;->mIsTaskRunning:Z

    if-nez v1, :cond_2

    .line 99
    invoke-direct {p0}, Lmiui/android/animation/internal/RunnerHandler;->doSetup()V

    .line 103
    .end local v0    # "info":Lmiui/android/animation/internal/TransitionInfo;
    :cond_2
    nop

    .line 125
    :cond_3
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 126
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setOperation(Lmiui/android/animation/internal/AnimOperationInfo;)V
    .locals 2
    .param p1, "opInfo"    # Lmiui/android/animation/internal/AnimOperationInfo;

    .line 70
    iget-object v0, p1, Lmiui/android/animation/internal/AnimOperationInfo;->target:Lmiui/android/animation/IAnimTarget;

    const/4 v1, 0x0

    new-array v1, v1, [Lmiui/android/animation/property/FloatProperty;

    invoke-virtual {v0, v1}, Lmiui/android/animation/IAnimTarget;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p1, Lmiui/android/animation/internal/AnimOperationInfo;->sendTime:J

    .line 72
    iget-object v0, p0, Lmiui/android/animation/internal/RunnerHandler;->mOpMap:Ljava/util/Map;

    iget-object v1, p1, Lmiui/android/animation/internal/AnimOperationInfo;->target:Lmiui/android/animation/IAnimTarget;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    return-void
.end method
