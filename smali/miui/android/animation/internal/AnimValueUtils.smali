.class public Lmiui/android/animation/internal/AnimValueUtils;
.super Ljava/lang/Object;
.source "AnimValueUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCurTargetValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D
    .locals 8
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # D

    .line 41
    invoke-static {p2, p3}, Ljava/lang/Math;->signum(D)D

    move-result-wide v0

    .line 42
    .local v0, "sig":D
    invoke-static {p2, p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 43
    .local v2, "absValue":D
    const-wide v4, 0x412e848000000000L    # 1000000.0

    cmpl-double v4, v2, v4

    if-nez v4, :cond_0

    .line 44
    invoke-static {p0, p1}, Lmiui/android/animation/utils/CommonUtils;->getSize(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v4, v0

    return-wide v4

    .line 46
    :cond_0
    instance-of v4, p1, Lmiui/android/animation/property/IIntValueProperty;

    if-eqz v4, :cond_1

    move-object v4, p1

    check-cast v4, Lmiui/android/animation/property/IIntValueProperty;

    .line 47
    invoke-virtual {p0, v4}, Lmiui/android/animation/IAnimTarget;->getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I

    move-result v4

    int-to-double v4, v4

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v4

    float-to-double v4, v4

    .line 48
    .local v4, "curValue":D
    :goto_0
    const-wide v6, 0x412e854800000000L    # 1000100.0

    cmpl-double v6, v2, v6

    if-nez v6, :cond_2

    .line 49
    mul-double v6, v4, v0

    return-wide v6

    .line 51
    :cond_2
    return-wide v4
.end method

.method public static getValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D
    .locals 2
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # D

    .line 33
    instance-of v0, p1, Lmiui/android/animation/property/ISpecificProperty;

    if-eqz v0, :cond_0

    .line 34
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ISpecificProperty;

    double-to-float v1, p2

    invoke-interface {v0, v1}, Lmiui/android/animation/property/ISpecificProperty;->getSpecificValue(F)F

    move-result v0

    float-to-double v0, v0

    return-wide v0

    .line 36
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lmiui/android/animation/internal/AnimValueUtils;->getCurTargetValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getValueOfTarget(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D
    .locals 2
    .param p0, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # D

    .line 23
    const-wide v0, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    .line 24
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/IIntValueProperty;

    invoke-virtual {p0, v0}, Lmiui/android/animation/IAnimTarget;->getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I

    move-result v0

    int-to-double v0, v0

    return-wide v0

    .line 25
    :cond_0
    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p2, v0

    if-nez v0, :cond_1

    .line 26
    invoke-virtual {p0, p1}, Lmiui/android/animation/IAnimTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F

    move-result v0

    float-to-double v0, v0

    return-wide v0

    .line 28
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lmiui/android/animation/internal/AnimValueUtils;->getValue(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static handleSetToValue(Lmiui/android/animation/listener/UpdateInfo;)Z
    .locals 3
    .param p0, "update"    # Lmiui/android/animation/listener/UpdateInfo;

    .line 60
    iget-object v0, p0, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v0, v0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D

    invoke-static {v0, v1}, Lmiui/android/animation/internal/AnimValueUtils;->isInvalid(D)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-object v1, p0, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    iget-wide v1, v1, Lmiui/android/animation/internal/AnimInfo;->setToValue:D

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->value:D

    .line 62
    iget-object v0, p0, Lmiui/android/animation/listener/UpdateInfo;->animInfo:Lmiui/android/animation/internal/AnimInfo;

    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v1, v0, Lmiui/android/animation/internal/AnimInfo;->setToValue:D

    .line 63
    const/4 v0, 0x1

    return v0

    .line 65
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static isInvalid(D)Z
    .locals 2
    .param p0, "value"    # D

    .line 56
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_1

    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_1

    const-wide v0, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v0, p0, v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
