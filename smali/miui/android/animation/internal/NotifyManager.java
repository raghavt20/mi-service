public class miui.android.animation.internal.NotifyManager {
	 /* .source "NotifyManager.java" */
	 /* # instance fields */
	 private miui.android.animation.base.AnimConfig mConfig;
	 miui.android.animation.listener.ListenerNotifier mNotifier;
	 miui.android.animation.listener.ListenerNotifier mSetToNotifier;
	 miui.android.animation.IAnimTarget mTarget;
	 /* # direct methods */
	 public miui.android.animation.internal.NotifyManager ( ) {
		 /* .locals 1 */
		 /* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
		 /* .line 25 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 23 */
		 /* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
		 /* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
		 this.mConfig = v0;
		 /* .line 26 */
		 this.mTarget = p1;
		 /* .line 27 */
		 /* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier; */
		 /* invoke-direct {v0, p1}, Lmiui/android/animation/listener/ListenerNotifier;-><init>(Lmiui/android/animation/IAnimTarget;)V */
		 this.mSetToNotifier = v0;
		 /* .line 28 */
		 /* new-instance v0, Lmiui/android/animation/listener/ListenerNotifier; */
		 /* invoke-direct {v0, p1}, Lmiui/android/animation/listener/ListenerNotifier;-><init>(Lmiui/android/animation/IAnimTarget;)V */
		 this.mNotifier = v0;
		 /* .line 29 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public miui.android.animation.listener.ListenerNotifier getNotifier ( ) {
		 /* .locals 1 */
		 /* .line 53 */
		 v0 = this.mNotifier;
	 } // .end method
	 public void setToNotify ( miui.android.animation.controller.AnimState p0, miui.android.animation.base.AnimConfigLink p1 ) {
		 /* .locals 3 */
		 /* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
		 /* .param p2, "oneTimeConfigs" # Lmiui/android/animation/base/AnimConfigLink; */
		 /* .line 32 */
		 /* if-nez p2, :cond_0 */
		 /* .line 33 */
		 return;
		 /* .line 35 */
	 } // :cond_0
	 (( miui.android.animation.controller.AnimState ) p1 ).getTag ( ); // invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->getTag()Ljava/lang/Object;
	 /* .line 36 */
	 /* .local v0, "tag":Ljava/lang/Object; */
	 v1 = this.mConfig;
	 (( miui.android.animation.controller.AnimState ) p1 ).getConfig ( ); // invoke-virtual {p1}, Lmiui/android/animation/controller/AnimState;->getConfig()Lmiui/android/animation/base/AnimConfig;
	 (( miui.android.animation.base.AnimConfig ) v1 ).copy ( v2 ); // invoke-virtual {v1, v2}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V
	 /* .line 37 */
	 v1 = this.mConfig;
	 (( miui.android.animation.base.AnimConfigLink ) p2 ).addTo ( v1 ); // invoke-virtual {p2, v1}, Lmiui/android/animation/base/AnimConfigLink;->addTo(Lmiui/android/animation/base/AnimConfig;)V
	 /* .line 38 */
	 v1 = this.mSetToNotifier;
	 v2 = this.mConfig;
	 v1 = 	 (( miui.android.animation.listener.ListenerNotifier ) v1 ).addListeners ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lmiui/android/animation/listener/ListenerNotifier;->addListeners(Ljava/lang/Object;Lmiui/android/animation/base/AnimConfig;)Z
	 /* if-nez v1, :cond_1 */
	 /* .line 39 */
	 v1 = this.mConfig;
	 (( miui.android.animation.base.AnimConfig ) v1 ).clear ( ); // invoke-virtual {v1}, Lmiui/android/animation/base/AnimConfig;->clear()V
	 /* .line 40 */
	 return;
	 /* .line 42 */
} // :cond_1
v1 = this.mSetToNotifier;
(( miui.android.animation.listener.ListenerNotifier ) v1 ).notifyBegin ( v0, v0 ); // invoke-virtual {v1, v0, v0}, Lmiui/android/animation/listener/ListenerNotifier;->notifyBegin(Ljava/lang/Object;Ljava/lang/Object;)V
/* .line 43 */
v1 = this.mTarget;
v1 = this.animManager;
v1 = this.mUpdateMap;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;
/* .line 44 */
/* .local v1, "updates":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/listener/UpdateInfo;>;" */
v2 = this.mSetToNotifier;
(( miui.android.animation.listener.ListenerNotifier ) v2 ).notifyPropertyBegin ( v0, v0, v1 ); // invoke-virtual {v2, v0, v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyBegin(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
/* .line 45 */
v2 = this.mSetToNotifier;
(( miui.android.animation.listener.ListenerNotifier ) v2 ).notifyUpdate ( v0, v0, v1 ); // invoke-virtual {v2, v0, v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->notifyUpdate(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
/* .line 46 */
v2 = this.mSetToNotifier;
(( miui.android.animation.listener.ListenerNotifier ) v2 ).notifyPropertyEnd ( v0, v0, v1 ); // invoke-virtual {v2, v0, v0, v1}, Lmiui/android/animation/listener/ListenerNotifier;->notifyPropertyEnd(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
/* .line 47 */
v2 = this.mSetToNotifier;
(( miui.android.animation.listener.ListenerNotifier ) v2 ).notifyEndAll ( v0, v0 ); // invoke-virtual {v2, v0, v0}, Lmiui/android/animation/listener/ListenerNotifier;->notifyEndAll(Ljava/lang/Object;Ljava/lang/Object;)V
/* .line 48 */
v2 = this.mSetToNotifier;
(( miui.android.animation.listener.ListenerNotifier ) v2 ).removeListeners ( v0 ); // invoke-virtual {v2, v0}, Lmiui/android/animation/listener/ListenerNotifier;->removeListeners(Ljava/lang/Object;)V
/* .line 49 */
v2 = this.mConfig;
(( miui.android.animation.base.AnimConfig ) v2 ).clear ( ); // invoke-virtual {v2}, Lmiui/android/animation/base/AnimConfig;->clear()V
/* .line 50 */
return;
} // .end method
