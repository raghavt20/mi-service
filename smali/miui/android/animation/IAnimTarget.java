public abstract class miui.android.animation.IAnimTarget {
	 /* .source "IAnimTarget.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # static fields */
public static final Long FLAT_ONESHOT;
static final java.util.concurrent.atomic.AtomicInteger sTargetIds;
/* # instance fields */
public final miui.android.animation.internal.AnimManager animManager;
public final miui.android.animation.internal.TargetHandler handler;
public final Integer id;
Float mDefaultMinVisible;
Long mFlags;
java.util.Map mMinVisibleChanges;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Object;", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final miui.android.animation.internal.TargetVelocityTracker mTracker;
miui.android.animation.internal.NotifyManager notifyManager;
/* # direct methods */
static miui.android.animation.IAnimTarget ( ) {
/* .locals 2 */
/* .line 38 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* const v1, 0x7fffffff */
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
return;
} // .end method
public miui.android.animation.IAnimTarget ( ) {
/* .locals 6 */
/* .line 50 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 27 */
/* new-instance v0, Lmiui/android/animation/internal/TargetHandler; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/internal/TargetHandler;-><init>(Lmiui/android/animation/IAnimTarget;)V */
this.handler = v0;
/* .line 32 */
/* new-instance v0, Lmiui/android/animation/internal/AnimManager; */
/* invoke-direct {v0}, Lmiui/android/animation/internal/AnimManager;-><init>()V */
this.animManager = v0;
/* .line 36 */
/* new-instance v1, Lmiui/android/animation/internal/NotifyManager; */
/* invoke-direct {v1, p0}, Lmiui/android/animation/internal/NotifyManager;-><init>(Lmiui/android/animation/IAnimTarget;)V */
this.notifyManager = v1;
/* .line 40 */
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
/* iput v1, p0, Lmiui/android/animation/IAnimTarget;->mDefaultMinVisible:F */
/* .line 42 */
/* new-instance v1, Landroid/util/ArrayMap; */
/* invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V */
this.mMinVisibleChanges = v1;
/* .line 46 */
v1 = miui.android.animation.IAnimTarget.sTargetIds;
v1 = (( java.util.concurrent.atomic.AtomicInteger ) v1 ).decrementAndGet ( ); // invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
/* iput v1, p0, Lmiui/android/animation/IAnimTarget;->id:I */
/* .line 48 */
/* new-instance v1, Lmiui/android/animation/internal/TargetVelocityTracker; */
/* invoke-direct {v1}, Lmiui/android/animation/internal/TargetVelocityTracker;-><init>()V */
this.mTracker = v1;
/* .line 51 */
(( miui.android.animation.internal.AnimManager ) v0 ).setTarget ( p0 ); // invoke-virtual {v0, p0}, Lmiui/android/animation/internal/AnimManager;->setTarget(Lmiui/android/animation/IAnimTarget;)V
/* .line 52 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v1, v0, [Lmiui/android/animation/property/FloatProperty; */
v2 = miui.android.animation.property.ViewProperty.ROTATION;
int v3 = 0; // const/4 v3, 0x0
/* aput-object v2, v1, v3 */
v2 = miui.android.animation.property.ViewProperty.ROTATION_X;
int v4 = 1; // const/4 v4, 0x1
/* aput-object v2, v1, v4 */
v2 = miui.android.animation.property.ViewProperty.ROTATION_Y;
int v5 = 2; // const/4 v5, 0x2
/* aput-object v2, v1, v5 */
/* const v2, 0x3dcccccd # 0.1f */
(( miui.android.animation.IAnimTarget ) p0 ).setMinVisibleChange ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;
/* .line 54 */
int v1 = 4; // const/4 v1, 0x4
/* new-array v1, v1, [Lmiui/android/animation/property/FloatProperty; */
v2 = miui.android.animation.property.ViewProperty.ALPHA;
/* aput-object v2, v1, v3 */
v2 = miui.android.animation.property.ViewProperty.AUTO_ALPHA;
/* aput-object v2, v1, v4 */
v2 = miui.android.animation.property.ViewPropertyExt.FOREGROUND;
/* aput-object v2, v1, v5 */
v2 = miui.android.animation.property.ViewPropertyExt.BACKGROUND;
/* aput-object v2, v1, v0 */
/* const/high16 v0, 0x3b800000 # 0.00390625f */
(( miui.android.animation.IAnimTarget ) p0 ).setMinVisibleChange ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;
/* .line 57 */
/* new-array v0, v5, [Lmiui/android/animation/property/FloatProperty; */
v1 = miui.android.animation.property.ViewProperty.SCALE_X;
/* aput-object v1, v0, v3 */
v1 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* aput-object v1, v0, v4 */
/* const v1, 0x3b03126f # 0.002f */
(( miui.android.animation.IAnimTarget ) p0 ).setMinVisibleChange ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;
/* .line 59 */
return;
} // .end method
/* # virtual methods */
public Boolean allowAnimRun ( ) {
/* .locals 1 */
/* .line 82 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public abstract void clean ( ) {
} // .end method
public void executeOnInitialized ( java.lang.Runnable p0 ) {
/* .locals 0 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 132 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
(( miui.android.animation.IAnimTarget ) p0 ).post ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/IAnimTarget;->post(Ljava/lang/Runnable;)V
/* .line 133 */
return;
} // .end method
public Float getDefaultMinVisible ( ) {
/* .locals 1 */
/* .line 200 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* const/high16 v0, 0x3f800000 # 1.0f */
} // .end method
public Integer getId ( ) {
/* .locals 1 */
/* .line 86 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* iget v0, p0, Lmiui/android/animation/IAnimTarget;->id:I */
} // .end method
public Integer getIntValue ( miui.android.animation.property.IIntValueProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/IIntValueProperty; */
/* .line 163 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
(( miui.android.animation.IAnimTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 164 */
/* .local v0, "targetObj":Ljava/lang/Object;, "TT;" */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = /* .line 165 */
/* .line 167 */
} // :cond_0
/* const v1, 0x7fffffff */
} // .end method
public void getLocationOnScreen ( Integer[] p0 ) {
/* .locals 2 */
/* .param p1, "location" # [I */
/* .line 144 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
/* aput v1, p1, v0 */
/* aput v1, p1, v1 */
/* .line 145 */
return;
} // .end method
public Float getMinVisibleChange ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 98 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.mMinVisibleChanges;
/* check-cast v0, Ljava/lang/Float; */
/* .line 99 */
/* .local v0, "threshold":Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 100 */
v1 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 101 */
} // :cond_0
/* iget v1, p0, Lmiui/android/animation/IAnimTarget;->mDefaultMinVisible:F */
/* const v2, 0x7f7fffff # Float.MAX_VALUE */
/* cmpl-float v2, v1, v2 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 102 */
/* .line 104 */
} // :cond_1
v1 = (( miui.android.animation.IAnimTarget ) p0 ).getDefaultMinVisible ( ); // invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getDefaultMinVisible()F
} // .end method
public miui.android.animation.listener.ListenerNotifier getNotifier ( ) {
/* .locals 1 */
/* .line 65 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.notifyManager;
(( miui.android.animation.internal.NotifyManager ) v0 ).getNotifier ( ); // invoke-virtual {v0}, Lmiui/android/animation/internal/NotifyManager;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;
} // .end method
public abstract java.lang.Object getTargetObject ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()TT;" */
/* } */
} // .end annotation
} // .end method
public Float getValue ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 148 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
(( miui.android.animation.IAnimTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 149 */
/* .local v0, "targetObj":Ljava/lang/Object;, "TT;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 150 */
v1 = (( miui.android.animation.property.FloatProperty ) p1 ).getValue ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F
/* .line 152 */
} // :cond_0
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
} // .end method
public Double getVelocity ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 178 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.animManager;
(( miui.android.animation.internal.AnimManager ) v0 ).getVelocity ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/internal/AnimManager;->getVelocity(Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Boolean hasFlags ( Long p0 ) {
/* .locals 2 */
/* .param p1, "flags" # J */
/* .line 94 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* iget-wide v0, p0, Lmiui/android/animation/IAnimTarget;->mFlags:J */
v0 = miui.android.animation.utils.CommonUtils .hasFlags ( v0,v1,p1,p2 );
} // .end method
public Boolean isAnimRunning ( miui.android.animation.property.FloatProperty...p0 ) {
/* .locals 1 */
/* .param p1, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 75 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.animManager;
v0 = (( miui.android.animation.internal.AnimManager ) v0 ).isAnimRunning ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
} // .end method
public Boolean isValid ( ) {
/* .locals 1 */
/* .line 136 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void onFrameEnd ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isFinished" # Z */
/* .line 141 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
return;
} // .end method
public void post ( java.lang.Runnable p0 ) {
/* .locals 4 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 188 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.handler;
/* iget-wide v0, v0, Lmiui/android/animation/internal/TargetHandler;->threadId:J */
java.lang.Thread .currentThread ( );
(( java.lang.Thread ) v2 ).getId ( ); // invoke-virtual {v2}, Ljava/lang/Thread;->getId()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 189 */
/* .line 191 */
} // :cond_0
v0 = this.handler;
(( miui.android.animation.internal.TargetHandler ) v0 ).post ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/internal/TargetHandler;->post(Ljava/lang/Runnable;)Z
/* .line 193 */
} // :goto_0
return;
} // .end method
public miui.android.animation.IAnimTarget setDefaultMinVisibleChange ( Float p0 ) {
/* .locals 0 */
/* .param p1, "defaultMinVisible" # F */
/* .line 108 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* iput p1, p0, Lmiui/android/animation/IAnimTarget;->mDefaultMinVisible:F */
/* .line 109 */
} // .end method
public void setFlags ( Long p0 ) {
/* .locals 0 */
/* .param p1, "flags" # J */
/* .line 90 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* iput-wide p1, p0, Lmiui/android/animation/IAnimTarget;->mFlags:J */
/* .line 91 */
return;
} // .end method
public void setIntValue ( miui.android.animation.property.IIntValueProperty p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/IIntValueProperty; */
/* .param p2, "value" # I */
/* .line 171 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
(( miui.android.animation.IAnimTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 172 */
/* .local v0, "targetObj":Ljava/lang/Object;, "TT;" */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = java.lang.Math .abs ( p2 );
/* const v2, 0x7fffffff */
/* if-eq v1, v2, :cond_0 */
/* .line 173 */
/* .line 175 */
} // :cond_0
return;
} // .end method
public miui.android.animation.IAnimTarget setMinVisibleChange ( Float p0, java.lang.String...p1 ) {
/* .locals 4 */
/* .param p1, "minVisible" # F */
/* .param p2, "names" # [Ljava/lang/String; */
/* .line 125 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* aget-object v2, p2, v1 */
/* .line 126 */
/* .local v2, "name":Ljava/lang/String; */
/* new-instance v3, Lmiui/android/animation/property/ValueProperty; */
/* invoke-direct {v3, v2}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V */
(( miui.android.animation.IAnimTarget ) p0 ).setMinVisibleChange ( v3, p1 ); // invoke-virtual {p0, v3, p1}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(Ljava/lang/Object;F)Lmiui/android/animation/IAnimTarget;
/* .line 125 */
} // .end local v2 # "name":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 128 */
} // :cond_0
} // .end method
public miui.android.animation.IAnimTarget setMinVisibleChange ( Float p0, miui.android.animation.property.FloatProperty...p1 ) {
/* .locals 5 */
/* .param p1, "minVisible" # F */
/* .param p2, "properties" # [Lmiui/android/animation/property/FloatProperty; */
/* .line 113 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* array-length v0, p2 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_0 */
/* aget-object v2, p2, v1 */
/* .line 114 */
/* .local v2, "prop":Lmiui/android/animation/property/FloatProperty; */
v3 = this.mMinVisibleChanges;
java.lang.Float .valueOf ( p1 );
/* .line 113 */
} // .end local v2 # "prop":Lmiui/android/animation/property/FloatProperty;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 116 */
} // :cond_0
} // .end method
public miui.android.animation.IAnimTarget setMinVisibleChange ( java.lang.Object p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .param p2, "minVisible" # F */
/* .line 120 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.mMinVisibleChanges;
java.lang.Float .valueOf ( p2 );
/* .line 121 */
} // .end method
public void setToNotify ( miui.android.animation.controller.AnimState p0, miui.android.animation.base.AnimConfigLink p1 ) {
/* .locals 1 */
/* .param p1, "state" # Lmiui/android/animation/controller/AnimState; */
/* .param p2, "configLink" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 71 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.notifyManager;
(( miui.android.animation.internal.NotifyManager ) v0 ).setToNotify ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/internal/NotifyManager;->setToNotify(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
/* .line 72 */
return;
} // .end method
public void setValue ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .line 156 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
(( miui.android.animation.IAnimTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 157 */
/* .local v0, "targetObj":Ljava/lang/Object;, "TT;" */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = java.lang.Math .abs ( p2 );
/* const v2, 0x7f7fffff # Float.MAX_VALUE */
/* cmpl-float v1, v1, v2 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 158 */
(( miui.android.animation.property.FloatProperty ) p1 ).setValue ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V
/* .line 160 */
} // :cond_0
return;
} // .end method
public void setVelocity ( miui.android.animation.property.FloatProperty p0, Double p1 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "v" # D */
/* .line 182 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* const-wide v0, 0x47efffffe0000000L # 3.4028234663852886E38 */
/* cmpl-double v0, p2, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 183 */
v0 = this.animManager;
/* double-to-float v1, p2 */
(( miui.android.animation.internal.AnimManager ) v0 ).setVelocity ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lmiui/android/animation/internal/AnimManager;->setVelocity(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 185 */
} // :cond_0
return;
} // .end method
public Boolean shouldUseIntValue ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 204 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* instance-of v0, p1, Lmiui/android/animation/property/IIntValueProperty; */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 216 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "IAnimTarget{"; // const-string v1, "IAnimTarget{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.android.animation.IAnimTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void trackVelocity ( miui.android.animation.property.FloatProperty p0, Double p1 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # D */
/* .line 211 */
/* .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;" */
v0 = this.mTracker;
(( miui.android.animation.internal.TargetVelocityTracker ) v0 ).trackVelocity ( p0, p1, p2, p3 ); // invoke-virtual {v0, p0, p1, p2, p3}, Lmiui/android/animation/internal/TargetVelocityTracker;->trackVelocity(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)V
/* .line 212 */
return;
} // .end method
