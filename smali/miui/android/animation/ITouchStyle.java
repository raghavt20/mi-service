public abstract class miui.android.animation.ITouchStyle implements miui.android.animation.IStateContainer {
	 /* .source "ITouchStyle.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/ITouchStyle$TouchType; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void bindViewOfListItem ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle clearTintColor ( ) {
} // .end method
public abstract void handleTouchOf ( android.view.View p0, android.view.View$OnClickListener p1, android.view.View$OnLongClickListener p2, miui.android.animation.base.AnimConfig...p3 ) {
} // .end method
public abstract void handleTouchOf ( android.view.View p0, android.view.View$OnClickListener p1, miui.android.animation.base.AnimConfig...p2 ) {
} // .end method
public abstract void handleTouchOf ( android.view.View p0, Boolean p1, miui.android.animation.base.AnimConfig...p2 ) {
} // .end method
public abstract void handleTouchOf ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
} // .end method
public abstract void ignoreTouchOf ( android.view.View p0 ) {
} // .end method
public abstract void onMotionEvent ( android.view.MotionEvent p0 ) {
} // .end method
public abstract void onMotionEventEx ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setAlpha ( Float p0, miui.android.animation.ITouchStyle$TouchType...p1 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setBackgroundColor ( Float p0, Float p1, Float p2, Float p3 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setBackgroundColor ( Integer p0 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setScale ( Float p0, miui.android.animation.ITouchStyle$TouchType...p1 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setTint ( Float p0, Float p1, Float p2, Float p3 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setTint ( Integer p0 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle setTintMode ( Integer p0 ) {
} // .end method
public abstract void setTouchDown ( ) {
} // .end method
public abstract void setTouchUp ( ) {
} // .end method
public abstract void touchDown ( miui.android.animation.base.AnimConfig...p0 ) {
} // .end method
public abstract void touchUp ( miui.android.animation.base.AnimConfig...p0 ) {
} // .end method
public abstract miui.android.animation.ITouchStyle useVarFont ( android.widget.TextView p0, Integer p1, Integer p2, Integer p3 ) {
} // .end method
