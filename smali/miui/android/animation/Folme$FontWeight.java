public abstract class miui.android.animation.Folme$FontWeight {
	 /* .source "Folme.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/Folme; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "FontWeight" */
} // .end annotation
/* # static fields */
public static final Integer BOLD;
public static final Integer DEMI_BOLD;
public static final Integer EXTRA_LIGHT;
public static final Integer HEAVY;
public static final Integer LIGHT;
public static final Integer MEDIUM;
public static final Integer NORMAL;
public static final Integer REGULAR;
public static final Integer SEMI_BOLD;
public static final Integer THIN;
