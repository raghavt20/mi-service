public class miui.android.animation.ValueTarget extends miui.android.animation.IAnimTarget {
	 /* .source "ValueTarget.java" */
	 /* # static fields */
	 private static final Float DEFAULT_MIN_VALUE;
	 static miui.android.animation.ITargetCreator sCreator;
	 /* # instance fields */
	 private java.util.concurrent.atomic.AtomicInteger mMaxType;
	 private miui.android.animation.property.ValueTargetObject mTargetObj;
	 /* # direct methods */
	 static miui.android.animation.ValueTarget ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 /* new-instance v0, Lmiui/android/animation/ValueTarget$1; */
		 /* invoke-direct {v0}, Lmiui/android/animation/ValueTarget$1;-><init>()V */
		 return;
	 } // .end method
	 public miui.android.animation.ValueTarget ( ) {
		 /* .locals 1 */
		 /* .line 30 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* invoke-direct {p0, v0}, Lmiui/android/animation/ValueTarget;-><init>(Ljava/lang/Object;)V */
		 /* .line 31 */
		 return;
	 } // .end method
	 private miui.android.animation.ValueTarget ( ) {
		 /* .locals 2 */
		 /* .param p1, "targetObj" # Ljava/lang/Object; */
		 /* .line 33 */
		 /* invoke-direct {p0}, Lmiui/android/animation/IAnimTarget;-><init>()V */
		 /* .line 27 */
		 /* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
		 /* const/16 v1, 0x3e8 */
		 /* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V */
		 this.mMaxType = v0;
		 /* .line 34 */
		 /* new-instance v0, Lmiui/android/animation/property/ValueTargetObject; */
		 /* if-nez p1, :cond_0 */
		 v1 = 		 (( miui.android.animation.ValueTarget ) p0 ).getId ( ); // invoke-virtual {p0}, Lmiui/android/animation/ValueTarget;->getId()I
		 java.lang.Integer .valueOf ( v1 );
	 } // :cond_0
	 /* move-object v1, p1 */
} // :goto_0
/* invoke-direct {v0, v1}, Lmiui/android/animation/property/ValueTargetObject;-><init>(Ljava/lang/Object;)V */
this.mTargetObj = v0;
/* .line 35 */
return;
} // .end method
 miui.android.animation.ValueTarget ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Ljava/lang/Object; */
/* .param p2, "x1" # Lmiui/android/animation/ValueTarget$1; */
/* .line 15 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;-><init>(Ljava/lang/Object;)V */
return;
} // .end method
private Boolean isPredefinedProperty ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "property" # Ljava/lang/Object; */
/* .line 134 */
/* instance-of v0, p1, Lmiui/android/animation/property/ValueProperty; */
/* if-nez v0, :cond_1 */
/* instance-of v0, p1, Lmiui/android/animation/property/ViewProperty; */
/* if-nez v0, :cond_1 */
/* instance-of v0, p1, Lmiui/android/animation/property/ColorProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
/* # virtual methods */
public void clean ( ) {
/* .locals 0 */
/* .line 120 */
return;
} // .end method
public miui.android.animation.property.FloatProperty createProperty ( java.lang.String p0, java.lang.Class p1 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "*>;)", */
/* "Lmiui/android/animation/property/FloatProperty;" */
/* } */
} // .end annotation
/* .line 123 */
/* .local p2, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;" */
v0 = java.lang.Integer.TYPE;
/* if-eq p2, v0, :cond_1 */
/* const-class v0, Ljava/lang/Integer; */
/* if-ne p2, v0, :cond_0 */
} // :cond_0
/* new-instance v0, Lmiui/android/animation/property/ValueProperty; */
/* invoke-direct {v0, p1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V */
} // :cond_1
} // :goto_0
/* new-instance v0, Lmiui/android/animation/property/IntValueProperty; */
/* invoke-direct {v0, p1}, Lmiui/android/animation/property/IntValueProperty;-><init>(Ljava/lang/String;)V */
} // :goto_1
} // .end method
public Float getDefaultMinVisible ( ) {
/* .locals 1 */
/* .line 130 */
/* const v0, 0x3b03126f # 0.002f */
} // .end method
public miui.android.animation.property.FloatProperty getFloatProperty ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .line 140 */
v0 = java.lang.Float.TYPE;
(( miui.android.animation.ValueTarget ) p0 ).createProperty ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lmiui/android/animation/ValueTarget;->createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;
} // .end method
public Integer getIntValue ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .line 55 */
(( miui.android.animation.ValueTarget ) p0 ).getIntValueProperty ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getIntValueProperty(Ljava/lang/String;)Lmiui/android/animation/property/IIntValueProperty;
v0 = (( miui.android.animation.ValueTarget ) p0 ).getIntValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/ValueTarget;->getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I
} // .end method
public Integer getIntValue ( miui.android.animation.property.IIntValueProperty p0 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/IIntValueProperty; */
/* .line 83 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 84 */
v0 = this.mTargetObj;
v2 = java.lang.Integer.TYPE;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getPropertyValue ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Integer; */
/* .line 85 */
/* .local v0, "value":Ljava/lang/Integer; */
/* if-nez v0, :cond_0 */
/* const v1, 0x7fffffff */
} // :cond_0
v1 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // :goto_0
/* .line 87 */
} // .end local v0 # "value":Ljava/lang/Integer;
} // :cond_1
v0 = this.mTargetObj;
v0 = (( miui.android.animation.property.ValueTargetObject ) v0 ).getRealObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
} // .end method
public miui.android.animation.property.IIntValueProperty getIntValueProperty ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .line 144 */
v0 = java.lang.Integer.TYPE;
(( miui.android.animation.ValueTarget ) p0 ).createProperty ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lmiui/android/animation/ValueTarget;->createProperty(Ljava/lang/String;Ljava/lang/Class;)Lmiui/android/animation/property/FloatProperty;
/* check-cast v0, Lmiui/android/animation/property/IIntValueProperty; */
} // .end method
public Float getMinVisibleChange ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "key" # Ljava/lang/Object; */
/* .line 101 */
/* instance-of v0, p1, Lmiui/android/animation/property/IIntValueProperty; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* instance-of v0, p1, Lmiui/android/animation/property/ColorProperty; */
/* if-nez v0, :cond_0 */
/* .line 102 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* .line 104 */
} // :cond_0
v0 = /* invoke-super {p0, p1}, Lmiui/android/animation/IAnimTarget;->getMinVisibleChange(Ljava/lang/Object;)F */
} // .end method
public java.lang.Object getTargetObject ( ) {
/* .locals 1 */
/* .line 114 */
v0 = this.mTargetObj;
} // .end method
public Float getValue ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .line 47 */
(( miui.android.animation.ValueTarget ) p0 ).getFloatProperty ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;
v0 = (( miui.android.animation.ValueTarget ) p0 ).getValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/ValueTarget;->getValue(Lmiui/android/animation/property/FloatProperty;)F
} // .end method
public Float getValue ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 3 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 64 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 65 */
v0 = this.mTargetObj;
(( miui.android.animation.property.FloatProperty ) p1 ).getName ( ); // invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
v2 = java.lang.Float.TYPE;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getPropertyValue ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Float; */
/* .line 66 */
/* .local v0, "value":Ljava/lang/Float; */
/* if-nez v0, :cond_0 */
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
} // :cond_0
v1 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // :goto_0
/* .line 68 */
} // .end local v0 # "value":Ljava/lang/Float;
} // :cond_1
v0 = this.mTargetObj;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getRealObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
v0 = (( miui.android.animation.property.FloatProperty ) p1 ).getValue ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F
} // .end method
public Double getVelocity ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .line 92 */
(( miui.android.animation.ValueTarget ) p0 ).getFloatProperty ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;
(( miui.android.animation.ValueTarget ) p0 ).getVelocity ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/ValueTarget;->getVelocity(Lmiui/android/animation/property/FloatProperty;)D
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Boolean isValid ( ) {
/* .locals 1 */
/* .line 109 */
v0 = this.mTargetObj;
v0 = (( miui.android.animation.property.ValueTargetObject ) v0 ).isValid ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->isValid()Z
} // .end method
public void setIntValue ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # I */
/* .line 59 */
(( miui.android.animation.ValueTarget ) p0 ).getIntValueProperty ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getIntValueProperty(Ljava/lang/String;)Lmiui/android/animation/property/IIntValueProperty;
(( miui.android.animation.ValueTarget ) p0 ).setIntValue ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lmiui/android/animation/ValueTarget;->setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
/* .line 60 */
return;
} // .end method
public void setIntValue ( miui.android.animation.property.IIntValueProperty p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "property" # Lmiui/android/animation/property/IIntValueProperty; */
/* .param p2, "value" # I */
/* .line 74 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 75 */
v0 = this.mTargetObj;
v2 = java.lang.Integer.TYPE;
java.lang.Integer .valueOf ( p2 );
(( miui.android.animation.property.ValueTargetObject ) v0 ).setPropertyValue ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 77 */
} // :cond_0
v0 = this.mTargetObj;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getRealObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
/* .line 79 */
} // :goto_0
return;
} // .end method
public void setValue ( java.lang.String p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "value" # F */
/* .line 51 */
(( miui.android.animation.ValueTarget ) p0 ).getFloatProperty ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;
(( miui.android.animation.ValueTarget ) p0 ).setValue ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lmiui/android/animation/ValueTarget;->setValue(Lmiui/android/animation/property/FloatProperty;F)V
/* .line 52 */
return;
} // .end method
public void setValue ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
/* .locals 4 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "value" # F */
/* .line 39 */
v0 = /* invoke-direct {p0, p1}, Lmiui/android/animation/ValueTarget;->isPredefinedProperty(Ljava/lang/Object;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 40 */
v0 = this.mTargetObj;
(( miui.android.animation.property.FloatProperty ) p1 ).getName ( ); // invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
v2 = java.lang.Float.TYPE;
java.lang.Float .valueOf ( p2 );
(( miui.android.animation.property.ValueTargetObject ) v0 ).setPropertyValue ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 42 */
} // :cond_0
v0 = this.mTargetObj;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getRealObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
(( miui.android.animation.property.FloatProperty ) p1 ).setValue ( v0, p2 ); // invoke-virtual {p1, v0, p2}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V
/* .line 44 */
} // :goto_0
return;
} // .end method
public void setVelocity ( java.lang.String p0, Double p1 ) {
/* .locals 1 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .param p2, "velocity" # D */
/* .line 96 */
(( miui.android.animation.ValueTarget ) p0 ).getFloatProperty ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ValueTarget;->getFloatProperty(Ljava/lang/String;)Lmiui/android/animation/property/FloatProperty;
(( miui.android.animation.ValueTarget ) p0 ).setVelocity ( v0, p2, p3 ); // invoke-virtual {p0, v0, p2, p3}, Lmiui/android/animation/ValueTarget;->setVelocity(Lmiui/android/animation/property/FloatProperty;D)V
/* .line 97 */
return;
} // .end method
