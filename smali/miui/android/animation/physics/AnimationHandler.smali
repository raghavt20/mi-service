.class public Lmiui/android/animation/physics/AnimationHandler;
.super Ljava/lang/Object;
.source "AnimationHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;,
        Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14;,
        Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16;,
        Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;,
        Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;
    }
.end annotation


# static fields
.field private static final FRAME_DELAY_MS:J = 0xaL

.field public static final sAnimatorHandler:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lmiui/android/animation/physics/AnimationHandler;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAnimationCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbackDispatcher:Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;

.field private mCurrentFrameTime:J

.field private final mDelayedCallbackStartTime:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mListDirty:Z

.field private mProvider:Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 70
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lmiui/android/animation/physics/AnimationHandler;->sAnimatorHandler:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mDelayedCallbackStartTime:Landroid/util/ArrayMap;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    .line 79
    new-instance v0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;

    invoke-direct {v0, p0}, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;-><init>(Lmiui/android/animation/physics/AnimationHandler;)V

    iput-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mCallbackDispatcher:Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;

    .line 83
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z

    return-void
.end method

.method static synthetic access$000(Lmiui/android/animation/physics/AnimationHandler;)J
    .locals 2
    .param p0, "x0"    # Lmiui/android/animation/physics/AnimationHandler;

    .line 41
    iget-wide v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lmiui/android/animation/physics/AnimationHandler;J)J
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/physics/AnimationHandler;
    .param p1, "x1"    # J

    .line 41
    iput-wide p1, p0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lmiui/android/animation/physics/AnimationHandler;J)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/physics/AnimationHandler;
    .param p1, "x1"    # J

    .line 41
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/physics/AnimationHandler;->doAnimationFrame(J)V

    return-void
.end method

.method static synthetic access$200(Lmiui/android/animation/physics/AnimationHandler;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/physics/AnimationHandler;

    .line 41
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lmiui/android/animation/physics/AnimationHandler;)Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;
    .locals 1
    .param p0, "x0"    # Lmiui/android/animation/physics/AnimationHandler;

    .line 41
    invoke-direct {p0}, Lmiui/android/animation/physics/AnimationHandler;->getProvider()Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    move-result-object v0

    return-object v0
.end method

.method private cleanUpList()V
    .locals 2

    .line 180
    iget-boolean v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 182
    iget-object v1, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 183
    iget-object v1, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 181
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 186
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z

    .line 188
    :cond_2
    return-void
.end method

.method private doAnimationFrame(J)V
    .locals 5
    .param p1, "frameTime"    # J

    .line 148
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 149
    .local v0, "currentTime":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 150
    iget-object v3, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;

    .line 151
    .local v3, "callback":Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;
    if-nez v3, :cond_0

    .line 152
    goto :goto_1

    .line 154
    :cond_0
    invoke-direct {p0, v3, v0, v1}, Lmiui/android/animation/physics/AnimationHandler;->isCallbackDue(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 155
    invoke-interface {v3, p1, p2}, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;->doAnimationFrame(J)Z

    .line 149
    .end local v3    # "callback":Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 158
    .end local v2    # "i":I
    :cond_2
    invoke-direct {p0}, Lmiui/android/animation/physics/AnimationHandler;->cleanUpList()V

    .line 159
    return-void
.end method

.method public static getFrameTime()J
    .locals 2

    .line 94
    sget-object v0, Lmiui/android/animation/physics/AnimationHandler;->sAnimatorHandler:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 95
    const-wide/16 v0, 0x0

    return-wide v0

    .line 97
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/physics/AnimationHandler;

    iget-wide v0, v0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J

    return-wide v0
.end method

.method public static getInstance()Lmiui/android/animation/physics/AnimationHandler;
    .locals 2

    .line 87
    sget-object v0, Lmiui/android/animation/physics/AnimationHandler;->sAnimatorHandler:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 88
    new-instance v1, Lmiui/android/animation/physics/AnimationHandler;

    invoke-direct {v1}, Lmiui/android/animation/physics/AnimationHandler;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 90
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/physics/AnimationHandler;

    return-object v0
.end method

.method private getProvider()Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;
    .locals 2

    .line 109
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mProvider:Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    if-nez v0, :cond_0

    .line 110
    nop

    .line 111
    new-instance v0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16;

    iget-object v1, p0, Lmiui/android/animation/physics/AnimationHandler;->mCallbackDispatcher:Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;

    invoke-direct {v0, v1}, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16;-><init>(Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;)V

    iput-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mProvider:Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    .line 116
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mProvider:Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    return-object v0
.end method

.method private isCallbackDue(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)Z
    .locals 4
    .param p1, "callback"    # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;
    .param p2, "currentTime"    # J

    .line 168
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mDelayedCallbackStartTime:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 169
    .local v0, "startTime":Ljava/lang/Long;
    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 170
    return v1

    .line 172
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, p2

    if-gez v2, :cond_1

    .line 173
    iget-object v2, p0, Lmiui/android/animation/physics/AnimationHandler;->mDelayedCallbackStartTime:Landroid/util/ArrayMap;

    invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    return v1

    .line 176
    :cond_1
    const/4 v1, 0x0

    return v1
.end method


# virtual methods
.method public addAnimationFrameCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)V
    .locals 3
    .param p1, "callback"    # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;
    .param p2, "delay"    # J

    .line 123
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-direct {p0}, Lmiui/android/animation/physics/AnimationHandler;->getProvider()Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;->postFrameCallback()V

    .line 126
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    .line 131
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mDelayedCallbackStartTime:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    :cond_2
    return-void
.end method

.method public removeCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;)V
    .locals 3
    .param p1, "callback"    # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;

    .line 139
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mDelayedCallbackStartTime:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 141
    .local v0, "id":I
    if-ltz v0, :cond_0

    .line 142
    iget-object v1, p0, Lmiui/android/animation/physics/AnimationHandler;->mAnimationCallbacks:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 143
    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z

    .line 145
    :cond_0
    return-void
.end method

.method public setProvider(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;)V
    .locals 0
    .param p1, "provider"    # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    .line 105
    iput-object p1, p0, Lmiui/android/animation/physics/AnimationHandler;->mProvider:Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    .line 106
    return-void
.end method
