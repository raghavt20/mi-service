.class public Lmiui/android/animation/physics/FrictionOperator;
.super Ljava/lang/Object;
.source "FrictionOperator.java"

# interfaces
.implements Lmiui/android/animation/physics/PhysicsOperator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getParameters([F[D)V
    .locals 7
    .param p1, "factors"    # [F
    .param p2, "params"    # [D

    .line 7
    const/4 v0, 0x0

    aget v1, p1, v0

    float-to-double v1, v1

    .line 8
    .local v1, "friction":D
    const-wide v3, -0x3fef333340000000L    # -4.199999809265137

    mul-double/2addr v3, v1

    const-wide v5, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v5, v3

    aput-wide v5, p2, v0

    .line 9
    return-void
.end method

.method public varargs updateVelocity(DDDD[D)D
    .locals 2
    .param p1, "velocity"    # D
    .param p3, "p0"    # D
    .param p5, "p1"    # D
    .param p7, "deltaT"    # D
    .param p9, "factors"    # [D

    .line 14
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, p3

    invoke-static {v0, v1, p7, p8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    mul-double/2addr v0, p1

    return-wide v0
.end method
