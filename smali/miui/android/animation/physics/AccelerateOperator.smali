.class public Lmiui/android/animation/physics/AccelerateOperator;
.super Ljava/lang/Object;
.source "AccelerateOperator.java"

# interfaces
.implements Lmiui/android/animation/physics/PhysicsOperator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getParameters([F[D)V
    .locals 5
    .param p1, "factors"    # [F
    .param p2, "params"    # [D

    .line 8
    const/4 v0, 0x0

    aget v1, p1, v0

    float-to-double v1, v1

    .line 9
    .local v1, "acc":D
    const-wide v3, 0x408f400000000000L    # 1000.0

    mul-double/2addr v3, v1

    aput-wide v3, p2, v0

    .line 10
    return-void
.end method

.method public varargs updateVelocity(DDDD[D)D
    .locals 2
    .param p1, "velocity"    # D
    .param p3, "p0"    # D
    .param p5, "p1"    # D
    .param p7, "deltaT"    # D
    .param p9, "factors"    # [D

    .line 15
    mul-double v0, p3, p7

    add-double/2addr v0, p1

    return-wide v0
.end method
