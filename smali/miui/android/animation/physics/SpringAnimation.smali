.class public final Lmiui/android/animation/physics/SpringAnimation;
.super Lmiui/android/animation/physics/DynamicAnimation;
.source "SpringAnimation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/android/animation/physics/DynamicAnimation<",
        "Lmiui/android/animation/physics/SpringAnimation;",
        ">;"
    }
.end annotation


# static fields
.field private static final UNSET:F = 3.4028235E38f


# instance fields
.field private mEndRequested:Z

.field private mPendingPosition:F

.field private mSpring:Lmiui/android/animation/physics/SpringForce;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiui/android/animation/property/FloatProperty<",
            "TK;>;)V"
        }
    .end annotation

    .line 96
    .local p1, "object":Ljava/lang/Object;, "TK;"
    .local p2, "property":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TK;>;"
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    .line 66
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mEndRequested:Z

    .line 97
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;F)V
    .locals 1
    .param p3, "finalPosition"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiui/android/animation/property/FloatProperty<",
            "TK;>;F)V"
        }
    .end annotation

    .line 111
    .local p1, "object":Ljava/lang/Object;, "TK;"
    .local p2, "property":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TK;>;"
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    .line 66
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mEndRequested:Z

    .line 112
    new-instance v0, Lmiui/android/animation/physics/SpringForce;

    invoke-direct {v0, p3}, Lmiui/android/animation/physics/SpringForce;-><init>(F)V

    iput-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    .line 113
    return-void
.end method

.method public constructor <init>(Lmiui/android/animation/property/FloatValueHolder;)V
    .locals 1
    .param p1, "floatValueHolder"    # Lmiui/android/animation/property/FloatValueHolder;

    .line 83
    invoke-direct {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Lmiui/android/animation/property/FloatValueHolder;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    .line 66
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mEndRequested:Z

    .line 84
    return-void
.end method

.method private sanityCheck()V
    .locals 4

    .line 202
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    if-eqz v0, :cond_2

    .line 206
    invoke-virtual {v0}, Lmiui/android/animation/physics/SpringForce;->getFinalPosition()F

    move-result v0

    float-to-double v0, v0

    .line 207
    .local v0, "finalPosition":D
    iget v2, p0, Lmiui/android/animation/physics/SpringAnimation;->mMaxValue:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    .line 210
    iget v2, p0, Lmiui/android/animation/physics/SpringAnimation;->mMinValue:F

    float-to-double v2, v2

    cmpg-double v2, v0, v2

    if-ltz v2, :cond_0

    .line 214
    return-void

    .line 211
    :cond_0
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Final position of the spring cannot be less than the min value."

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 208
    :cond_1
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 203
    .end local v0    # "finalPosition":D
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public animateToFinalPosition(F)V
    .locals 1
    .param p1, "finalPosition"    # F

    .line 157
    invoke-virtual {p0}, Lmiui/android/animation/physics/SpringAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iput p1, p0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    if-nez v0, :cond_1

    .line 161
    new-instance v0, Lmiui/android/animation/physics/SpringForce;

    invoke-direct {v0, p1}, Lmiui/android/animation/physics/SpringForce;-><init>(F)V

    iput-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    .line 163
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v0, p1}, Lmiui/android/animation/physics/SpringForce;->setFinalPosition(F)Lmiui/android/animation/physics/SpringForce;

    .line 164
    invoke-virtual {p0}, Lmiui/android/animation/physics/SpringAnimation;->start()V

    .line 166
    :goto_0
    return-void
.end method

.method public canSkipToEnd()Z
    .locals 4

    .line 196
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    iget-wide v0, v0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method getAcceleration(FF)F
    .locals 1
    .param p1, "value"    # F
    .param p2, "velocity"    # F

    .line 262
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/physics/SpringForce;->getAcceleration(FF)F

    move-result v0

    return v0
.end method

.method public getSpring()Lmiui/android/animation/physics/SpringForce;
    .locals 1

    .line 121
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    return-object v0
.end method

.method isAtEquilibrium(FF)Z
    .locals 1
    .param p1, "value"    # F
    .param p2, "velocity"    # F

    .line 267
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/physics/SpringForce;->isAtEquilibrium(FF)Z

    move-result v0

    return v0
.end method

.method public setSpring(Lmiui/android/animation/physics/SpringForce;)Lmiui/android/animation/physics/SpringAnimation;
    .locals 0
    .param p1, "force"    # Lmiui/android/animation/physics/SpringForce;

    .line 133
    iput-object p1, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    .line 134
    return-object p0
.end method

.method setValueThreshold(F)V
    .locals 0
    .param p1, "threshold"    # F

    .line 272
    return-void
.end method

.method public skipToEnd()V
    .locals 2

    .line 178
    invoke-virtual {p0}, Lmiui/android/animation/physics/SpringAnimation;->canSkipToEnd()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 185
    iget-boolean v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mRunning:Z

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mEndRequested:Z

    .line 188
    :cond_0
    return-void

    .line 183
    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Spring animations can only come to an end when there is damping"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public start()V
    .locals 3

    .line 139
    invoke-direct {p0}, Lmiui/android/animation/physics/SpringAnimation;->sanityCheck()V

    .line 140
    iget-object v0, p0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {p0}, Lmiui/android/animation/physics/SpringAnimation;->getValueThreshold()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/physics/SpringForce;->setValueThreshold(D)V

    .line 141
    invoke-super {p0}, Lmiui/android/animation/physics/DynamicAnimation;->start()V

    .line 142
    return-void
.end method

.method updateValueAndVelocity(J)Z
    .locals 19
    .param p1, "deltaT"    # J

    .line 220
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mEndRequested:Z

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    if-eqz v1, :cond_1

    .line 221
    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    cmpl-float v3, v1, v2

    if-eqz v3, :cond_0

    .line 222
    iget-object v3, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v3, v1}, Lmiui/android/animation/physics/SpringForce;->setFinalPosition(F)Lmiui/android/animation/physics/SpringForce;

    .line 223
    iput v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    .line 225
    :cond_0
    iget-object v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v1}, Lmiui/android/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    iput v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    .line 226
    iput v10, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    .line 227
    iput-boolean v9, v0, Lmiui/android/animation/physics/SpringAnimation;->mEndRequested:Z

    .line 228
    return v8

    .line 231
    :cond_1
    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    .line 232
    iget-object v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v1}, Lmiui/android/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    float-to-double v3, v1

    .line 235
    .local v3, "lastPosition":D
    iget-object v11, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    float-to-double v12, v1

    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    float-to-double v14, v1

    const-wide/16 v5, 0x2

    div-long v16, p1, v5

    invoke-virtual/range {v11 .. v17}, Lmiui/android/animation/physics/SpringForce;->updateValues(DDJ)Lmiui/android/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    .line 236
    .local v1, "massState":Lmiui/android/animation/physics/DynamicAnimation$MassState;
    iget-object v7, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    iget v11, v0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    invoke-virtual {v7, v11}, Lmiui/android/animation/physics/SpringForce;->setFinalPosition(F)Lmiui/android/animation/physics/SpringForce;

    .line 237
    iput v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mPendingPosition:F

    .line 239
    iget-object v12, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    iget v2, v1, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mValue:F

    float-to-double v13, v2

    iget v2, v1, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    float-to-double v8, v2

    div-long v17, p1, v5

    move-wide v15, v8

    invoke-virtual/range {v12 .. v18}, Lmiui/android/animation/physics/SpringForce;->updateValues(DDJ)Lmiui/android/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    .line 240
    iget v2, v1, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mValue:F

    iput v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    .line 241
    iget v2, v1, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    iput v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    .line 243
    .end local v1    # "massState":Lmiui/android/animation/physics/DynamicAnimation$MassState;
    .end local v3    # "lastPosition":D
    goto :goto_0

    .line 244
    :cond_2
    iget-object v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    iget v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    float-to-double v2, v2

    iget v4, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    float-to-double v4, v4

    move-wide/from16 v6, p1

    invoke-virtual/range {v1 .. v7}, Lmiui/android/animation/physics/SpringForce;->updateValues(DDJ)Lmiui/android/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    .line 245
    .restart local v1    # "massState":Lmiui/android/animation/physics/DynamicAnimation$MassState;
    iget v2, v1, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mValue:F

    iput v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    .line 246
    iget v2, v1, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    iput v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    .line 249
    .end local v1    # "massState":Lmiui/android/animation/physics/DynamicAnimation$MassState;
    :goto_0
    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    iget v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mMinValue:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    .line 250
    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    iget v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mMaxValue:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    .line 252
    iget v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    iget v2, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/physics/SpringAnimation;->isAtEquilibrium(FF)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 253
    iget-object v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mSpring:Lmiui/android/animation/physics/SpringForce;

    invoke-virtual {v1}, Lmiui/android/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    iput v1, v0, Lmiui/android/animation/physics/SpringAnimation;->mValue:F

    .line 254
    iput v10, v0, Lmiui/android/animation/physics/SpringAnimation;->mVelocity:F

    .line 255
    const/4 v1, 0x1

    return v1

    .line 257
    :cond_3
    const/4 v1, 0x0

    return v1
.end method
