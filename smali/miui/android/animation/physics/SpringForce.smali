.class public final Lmiui/android/animation/physics/SpringForce;
.super Ljava/lang/Object;
.source "SpringForce.java"

# interfaces
.implements Lmiui/android/animation/physics/Force;


# static fields
.field public static final DAMPING_RATIO_HIGH_BOUNCY:F = 0.2f

.field public static final DAMPING_RATIO_LOW_BOUNCY:F = 0.75f

.field public static final DAMPING_RATIO_MEDIUM_BOUNCY:F = 0.5f

.field public static final DAMPING_RATIO_NO_BOUNCY:F = 1.0f

.field public static final STIFFNESS_HIGH:F = 10000.0f

.field public static final STIFFNESS_LOW:F = 200.0f

.field public static final STIFFNESS_MEDIUM:F = 1500.0f

.field public static final STIFFNESS_VERY_LOW:F = 50.0f

.field private static final UNSET:D = 1.7976931348623157E308

.field private static final VELOCITY_THRESHOLD_MULTIPLIER:D = 62.5


# instance fields
.field private mDampedFreq:D

.field mDampingRatio:D

.field private mFinalPosition:D

.field private mGammaMinus:D

.field private mGammaPlus:D

.field private mInitialized:Z

.field private final mMassState:Lmiui/android/animation/physics/DynamicAnimation$MassState;

.field mNaturalFreq:D

.field private mValueThreshold:D

.field private mVelocityThreshold:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    .line 84
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringForce;->mInitialized:Z

    .line 103
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    .line 106
    new-instance v0, Lmiui/android/animation/physics/DynamicAnimation$MassState;

    invoke-direct {v0}, Lmiui/android/animation/physics/DynamicAnimation$MassState;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/SpringForce;->mMassState:Lmiui/android/animation/physics/DynamicAnimation$MassState;

    .line 114
    return-void
.end method

.method public constructor <init>(F)V
    .locals 2
    .param p1, "finalPosition"    # F

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    .line 84
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    .line 90
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringForce;->mInitialized:Z

    .line 103
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    .line 106
    new-instance v0, Lmiui/android/animation/physics/DynamicAnimation$MassState;

    invoke-direct {v0}, Lmiui/android/animation/physics/DynamicAnimation$MassState;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/SpringForce;->mMassState:Lmiui/android/animation/physics/DynamicAnimation$MassState;

    .line 122
    float-to-double v0, p1

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    .line 123
    return-void
.end method

.method private init()V
    .locals 8

    .line 243
    iget-boolean v0, p0, Lmiui/android/animation/physics/SpringForce;->mInitialized:Z

    if-eqz v0, :cond_0

    .line 244
    return-void

    .line 247
    :cond_0
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    .line 252
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    .line 254
    neg-double v4, v0

    iget-wide v6, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    .line 255
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    add-double/2addr v4, v6

    iput-wide v4, p0, Lmiui/android/animation/physics/SpringForce;->mGammaPlus:D

    .line 256
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    neg-double v4, v0

    iget-wide v6, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    .line 257
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    sub-double/2addr v4, v6

    iput-wide v4, p0, Lmiui/android/animation/physics/SpringForce;->mGammaMinus:D

    goto :goto_0

    .line 258
    :cond_1
    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    .line 260
    iget-wide v4, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double/2addr v0, v0

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v4, v0

    iput-wide v4, p0, Lmiui/android/animation/physics/SpringForce;->mDampedFreq:D

    .line 263
    :cond_2
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringForce;->mInitialized:Z

    .line 264
    return-void

    .line 248
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error: Final position of the spring must be set before the miuix.animation starts"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getAcceleration(FF)F
    .locals 8
    .param p1, "lastDisplacement"    # F
    .param p2, "lastVelocity"    # F

    .line 215
    invoke-virtual {p0}, Lmiui/android/animation/physics/SpringForce;->getFinalPosition()F

    move-result v0

    sub-float/2addr p1, v0

    .line 217
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double v2, v0, v0

    .line 218
    .local v2, "k":D
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v4

    iget-wide v4, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    mul-double/2addr v0, v4

    .line 220
    .local v0, "c":D
    neg-double v4, v2

    float-to-double v6, p1

    mul-double/2addr v4, v6

    float-to-double v6, p2

    mul-double/2addr v6, v0

    sub-double/2addr v4, v6

    double-to-float v4, v4

    return v4
.end method

.method public getDampingRatio()F
    .locals 2

    .line 184
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    double-to-float v0, v0

    return v0
.end method

.method public getFinalPosition()F
    .locals 2

    .line 204
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    double-to-float v0, v0

    return v0
.end method

.method public getStiffness()F
    .locals 2

    .line 150
    iget-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double/2addr v0, v0

    double-to-float v0, v0

    return v0
.end method

.method public isAtEquilibrium(FF)Z
    .locals 4
    .param p1, "value"    # F
    .param p2, "velocity"    # F

    .line 228
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, Lmiui/android/animation/physics/SpringForce;->mVelocityThreshold:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 229
    invoke-virtual {p0}, Lmiui/android/animation/physics/SpringForce;->getFinalPosition()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, Lmiui/android/animation/physics/SpringForce;->mValueThreshold:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 230
    const/4 v0, 0x1

    return v0

    .line 232
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setDampingRatio(F)Lmiui/android/animation/physics/SpringForce;
    .locals 2
    .param p1, "dampingRatio"    # F

    .line 169
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    .line 172
    float-to-double v0, p1

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringForce;->mInitialized:Z

    .line 175
    return-object p0

    .line 170
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Damping ratio must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setFinalPosition(F)Lmiui/android/animation/physics/SpringForce;
    .locals 2
    .param p1, "finalPosition"    # F

    .line 194
    float-to-double v0, p1

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    .line 195
    return-object p0
.end method

.method public setStiffness(F)Lmiui/android/animation/physics/SpringForce;
    .locals 2
    .param p1, "stiffness"    # F

    .line 135
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    .line 138
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/SpringForce;->mInitialized:Z

    .line 141
    return-object p0

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Spring stiffness constant must be positive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setValueThreshold(D)V
    .locals 4
    .param p1, "threshold"    # D

    .line 325
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mValueThreshold:D

    .line 326
    const-wide v2, 0x404f400000000000L    # 62.5

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lmiui/android/animation/physics/SpringForce;->mVelocityThreshold:D

    .line 327
    return-void
.end method

.method updateValues(DDJ)Lmiui/android/animation/physics/DynamicAnimation$MassState;
    .locals 22
    .param p1, "lastDisplacement"    # D
    .param p3, "lastVelocity"    # D
    .param p5, "timeElapsed"    # J

    .line 272
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Lmiui/android/animation/physics/SpringForce;->init()V

    .line 274
    move-wide/from16 v1, p5

    long-to-double v3, v1

    const-wide v5, 0x408f400000000000L    # 1000.0

    div-double/2addr v3, v5

    .line 275
    .local v3, "deltaT":D
    iget-wide v5, v0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    sub-double v5, p1, v5

    .line 278
    .end local p1    # "lastDisplacement":D
    .local v5, "lastDisplacement":D
    iget-wide v7, v0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    cmpl-double v11, v7, v9

    const-wide v12, 0x4005bf0a8b145769L    # Math.E

    if-lez v11, :cond_0

    .line 280
    iget-wide v7, v0, Lmiui/android/animation/physics/SpringForce;->mGammaMinus:D

    mul-double v9, v7, v5

    sub-double v9, v9, p3

    iget-wide v14, v0, Lmiui/android/animation/physics/SpringForce;->mGammaPlus:D

    sub-double v16, v7, v14

    div-double v9, v9, v16

    sub-double v9, v5, v9

    .line 282
    .local v9, "coeffA":D
    mul-double v16, v7, v5

    sub-double v16, v16, p3

    sub-double v14, v7, v14

    div-double v16, v16, v14

    .line 284
    .local v16, "coeffB":D
    mul-double/2addr v7, v3

    invoke-static {v12, v13, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    mul-double/2addr v7, v9

    iget-wide v14, v0, Lmiui/android/animation/physics/SpringForce;->mGammaPlus:D

    mul-double/2addr v14, v3

    .line 285
    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double v14, v14, v16

    add-double/2addr v7, v14

    .line 286
    .local v7, "displacement":D
    iget-wide v14, v0, Lmiui/android/animation/physics/SpringForce;->mGammaMinus:D

    mul-double v18, v9, v14

    mul-double/2addr v14, v3

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double v18, v18, v14

    iget-wide v14, v0, Lmiui/android/animation/physics/SpringForce;->mGammaPlus:D

    mul-double v20, v16, v14

    mul-double/2addr v14, v3

    .line 287
    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    mul-double v20, v20, v11

    add-double v18, v18, v20

    .line 288
    .end local v9    # "coeffA":D
    .end local v16    # "coeffB":D
    .local v18, "currentVelocity":D
    move-wide v15, v5

    move-wide/from16 v1, v18

    goto/16 :goto_0

    .end local v7    # "displacement":D
    .end local v18    # "currentVelocity":D
    :cond_0
    cmpl-double v11, v7, v9

    if-nez v11, :cond_1

    .line 290
    move-wide v7, v5

    .line 291
    .local v7, "coeffA":D
    iget-wide v9, v0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double v14, v9, v5

    add-double v14, p3, v14

    .line 292
    .local v14, "coeffB":D
    mul-double v16, v14, v3

    add-double v16, v7, v16

    neg-double v9, v9

    mul-double/2addr v9, v3

    invoke-static {v12, v13, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    mul-double v9, v9, v16

    .line 293
    .local v9, "displacement":D
    mul-double v16, v14, v3

    add-double v16, v7, v16

    iget-wide v12, v0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    neg-double v11, v12

    mul-double/2addr v11, v3

    const-wide v1, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v1, v2, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    mul-double v16, v16, v11

    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    neg-double v1, v11

    mul-double v16, v16, v1

    neg-double v1, v11

    mul-double/2addr v1, v3

    .line 294
    const-wide v11, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v11, v12, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    mul-double/2addr v1, v14

    add-double v18, v16, v1

    .line 295
    .end local v7    # "coeffA":D
    .end local v14    # "coeffB":D
    .restart local v18    # "currentVelocity":D
    move-wide v15, v5

    move-wide v7, v9

    move-wide/from16 v1, v18

    goto :goto_0

    .line 297
    .end local v9    # "displacement":D
    .end local v18    # "currentVelocity":D
    :cond_1
    move-wide v1, v5

    .line 298
    .local v1, "cosCoeff":D
    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mDampedFreq:D

    div-double/2addr v9, v11

    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    mul-double v13, v7, v11

    mul-double/2addr v13, v5

    add-double v13, v13, p3

    mul-double/2addr v9, v13

    .line 300
    .local v9, "sinCoeff":D
    neg-double v7, v7

    mul-double/2addr v7, v11

    mul-double/2addr v7, v3

    const-wide v11, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v11, v12, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mDampedFreq:D

    mul-double/2addr v11, v3

    .line 301
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    mul-double/2addr v11, v1

    iget-wide v13, v0, Lmiui/android/animation/physics/SpringForce;->mDampedFreq:D

    mul-double/2addr v13, v3

    .line 302
    invoke-static {v13, v14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    mul-double/2addr v13, v9

    add-double/2addr v11, v13

    mul-double/2addr v7, v11

    .line 303
    .local v7, "displacement":D
    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mNaturalFreq:D

    neg-double v13, v11

    mul-double/2addr v13, v7

    move-wide v15, v5

    .end local v5    # "lastDisplacement":D
    .local v15, "lastDisplacement":D
    iget-wide v5, v0, Lmiui/android/animation/physics/SpringForce;->mDampingRatio:D

    mul-double/2addr v13, v5

    neg-double v5, v5

    mul-double/2addr v5, v11

    mul-double/2addr v5, v3

    .line 304
    const-wide v11, 0x4005bf0a8b145769L    # Math.E

    invoke-static {v11, v12, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mDampedFreq:D

    move-wide/from16 p1, v7

    .end local v7    # "displacement":D
    .local p1, "displacement":D
    neg-double v7, v11

    mul-double/2addr v7, v1

    mul-double/2addr v11, v3

    .line 305
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    mul-double/2addr v7, v11

    iget-wide v11, v0, Lmiui/android/animation/physics/SpringForce;->mDampedFreq:D

    mul-double v17, v11, v9

    mul-double/2addr v11, v3

    .line 306
    invoke-static {v11, v12}, Ljava/lang/Math;->cos(D)D

    move-result-wide v11

    mul-double v17, v17, v11

    add-double v7, v7, v17

    mul-double/2addr v5, v7

    add-double v18, v13, v5

    move-wide/from16 v7, p1

    move-wide/from16 v1, v18

    .line 309
    .end local v9    # "sinCoeff":D
    .end local p1    # "displacement":D
    .local v1, "currentVelocity":D
    .restart local v7    # "displacement":D
    :goto_0
    iget-object v5, v0, Lmiui/android/animation/physics/SpringForce;->mMassState:Lmiui/android/animation/physics/DynamicAnimation$MassState;

    iget-wide v9, v0, Lmiui/android/animation/physics/SpringForce;->mFinalPosition:D

    add-double/2addr v9, v7

    double-to-float v6, v9

    iput v6, v5, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mValue:F

    .line 310
    iget-object v5, v0, Lmiui/android/animation/physics/SpringForce;->mMassState:Lmiui/android/animation/physics/DynamicAnimation$MassState;

    double-to-float v6, v1

    iput v6, v5, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    .line 311
    iget-object v5, v0, Lmiui/android/animation/physics/SpringForce;->mMassState:Lmiui/android/animation/physics/DynamicAnimation$MassState;

    return-object v5
.end method
