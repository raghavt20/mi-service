.class Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;
.super Ljava/lang/Object;
.source "AnimationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/physics/AnimationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AnimationCallbackDispatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/android/animation/physics/AnimationHandler;


# direct methods
.method constructor <init>(Lmiui/android/animation/physics/AnimationHandler;)V
    .locals 0
    .param p1, "this$0"    # Lmiui/android/animation/physics/AnimationHandler;

    .line 59
    iput-object p1, p0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;->this$0:Lmiui/android/animation/physics/AnimationHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method dispatchAnimationFrame()V
    .locals 3

    .line 61
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;->this$0:Lmiui/android/animation/physics/AnimationHandler;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lmiui/android/animation/physics/AnimationHandler;->access$002(Lmiui/android/animation/physics/AnimationHandler;J)J

    .line 62
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;->this$0:Lmiui/android/animation/physics/AnimationHandler;

    invoke-static {v0}, Lmiui/android/animation/physics/AnimationHandler;->access$000(Lmiui/android/animation/physics/AnimationHandler;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lmiui/android/animation/physics/AnimationHandler;->access$100(Lmiui/android/animation/physics/AnimationHandler;J)V

    .line 63
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;->this$0:Lmiui/android/animation/physics/AnimationHandler;

    invoke-static {v0}, Lmiui/android/animation/physics/AnimationHandler;->access$200(Lmiui/android/animation/physics/AnimationHandler;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 64
    iget-object v0, p0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;->this$0:Lmiui/android/animation/physics/AnimationHandler;

    invoke-static {v0}, Lmiui/android/animation/physics/AnimationHandler;->access$300(Lmiui/android/animation/physics/AnimationHandler;)Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;->postFrameCallback()V

    .line 66
    :cond_0
    return-void
.end method
