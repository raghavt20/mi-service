public class miui.android.animation.physics.EquilibriumChecker {
	 /* .source "EquilibriumChecker.java" */
	 /* # static fields */
	 public static final Float MIN_VISIBLE_CHANGE_ALPHA;
	 public static final Float MIN_VISIBLE_CHANGE_PIXELS;
	 public static final Float MIN_VISIBLE_CHANGE_ROTATION_DEGREES;
	 public static final Float MIN_VISIBLE_CHANGE_SCALE;
	 private static final Float THRESHOLD_MULTIPLIER;
	 public static final Float VELOCITY_THRESHOLD_MULTIPLIER;
	 /* # instance fields */
	 private Double mTargetValue;
	 private Float mValueThreshold;
	 private Float mVelocityThreshold;
	 /* # direct methods */
	 public miui.android.animation.physics.EquilibriumChecker ( ) {
		 /* .locals 2 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 33 */
		 /* const-wide v0, 0x7fefffffffffffffL # Double.MAX_VALUE */
		 /* iput-wide v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D */
		 return;
	 } // .end method
	 private Boolean isAt ( Double p0, Double p1 ) {
		 /* .locals 4 */
		 /* .param p1, "value" # D */
		 /* .param p3, "target" # D */
		 /* .line 51 */
		 /* iget-wide v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D */
		 java.lang.Math .abs ( v0,v1 );
		 /* move-result-wide v0 */
		 /* const-wide v2, 0x47efffffe0000000L # 3.4028234663852886E38 */
		 /* cmpl-double v0, v0, v2 */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* sub-double v0, p1, p3 */
			 /* .line 52 */
			 java.lang.Math .abs ( v0,v1 );
			 /* move-result-wide v0 */
			 /* iget v2, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mValueThreshold:F */
			 /* float-to-double v2, v2 */
			 /* cmpg-double v0, v0, v2 */
			 /* if-gez v0, :cond_0 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 51 */
} // :goto_1
} // .end method
/* # virtual methods */
public void init ( miui.android.animation.IAnimTarget p0, miui.android.animation.property.FloatProperty p1, Double p2 ) {
/* .locals 2 */
/* .param p1, "target" # Lmiui/android/animation/IAnimTarget; */
/* .param p2, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p3, "targetValue" # D */
/* .line 40 */
v0 = (( miui.android.animation.IAnimTarget ) p1 ).getMinVisibleChange ( p2 ); // invoke-virtual {p1, p2}, Lmiui/android/animation/IAnimTarget;->getMinVisibleChange(Ljava/lang/Object;)F
/* const/high16 v1, 0x3f400000 # 0.75f */
/* mul-float/2addr v0, v1 */
/* iput v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mValueThreshold:F */
/* .line 41 */
/* const v1, 0x41855555 */
/* mul-float/2addr v0, v1 */
/* iput v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mVelocityThreshold:F */
/* .line 42 */
/* iput-wide p3, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D */
/* .line 43 */
return;
} // .end method
public Boolean isAtEquilibrium ( Integer p0, Double p1, Double p2 ) {
/* .locals 4 */
/* .param p1, "easeStyle" # I */
/* .param p2, "value" # D */
/* .param p4, "velocity" # D */
/* .line 46 */
int v0 = -2; // const/4 v0, -0x2
/* if-ne p1, v0, :cond_0 */
/* iget-wide v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D */
v0 = /* invoke-direct {p0, p2, p3, v0, v1}, Lmiui/android/animation/physics/EquilibriumChecker;->isAt(DD)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 47 */
} // :cond_0
java.lang.Math .abs ( p4,p5 );
/* move-result-wide v0 */
/* iget v2, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mVelocityThreshold:F */
/* float-to-double v2, v2 */
/* cmpg-double v0, v0, v2 */
/* if-gez v0, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 46 */
} // :goto_0
} // .end method
