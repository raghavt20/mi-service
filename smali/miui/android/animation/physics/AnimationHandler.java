public class miui.android.animation.physics.AnimationHandler {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;, */
	 /* Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14;, */
	 /* Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16;, */
	 /* Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;, */
	 /* Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long FRAME_DELAY_MS;
public static final java.lang.ThreadLocal sAnimatorHandler;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ThreadLocal<", */
/* "Lmiui/android/animation/physics/AnimationHandler;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.util.ArrayList mAnimationCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final miui.android.animation.physics.AnimationHandler$AnimationCallbackDispatcher mCallbackDispatcher;
private Long mCurrentFrameTime;
private final android.util.ArrayMap mDelayedCallbackStartTime;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/ArrayMap<", */
/* "Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mListDirty;
private miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider mProvider;
/* # direct methods */
static miui.android.animation.physics.AnimationHandler ( ) {
/* .locals 1 */
/* .line 70 */
/* new-instance v0, Ljava/lang/ThreadLocal; */
/* invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V */
return;
} // .end method
public miui.android.animation.physics.AnimationHandler ( ) {
/* .locals 2 */
/* .line 41 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 76 */
/* new-instance v0, Landroid/util/ArrayMap; */
/* invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V */
this.mDelayedCallbackStartTime = v0;
/* .line 78 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAnimationCallbacks = v0;
/* .line 79 */
/* new-instance v0, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;-><init>(Lmiui/android/animation/physics/AnimationHandler;)V */
this.mCallbackDispatcher = v0;
/* .line 83 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J */
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z */
return;
} // .end method
static Long access$000 ( miui.android.animation.physics.AnimationHandler p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "x0" # Lmiui/android/animation/physics/AnimationHandler; */
/* .line 41 */
/* iget-wide v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J */
/* return-wide v0 */
} // .end method
static Long access$002 ( miui.android.animation.physics.AnimationHandler p0, Long p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/physics/AnimationHandler; */
/* .param p1, "x1" # J */
/* .line 41 */
/* iput-wide p1, p0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J */
/* return-wide p1 */
} // .end method
static void access$100 ( miui.android.animation.physics.AnimationHandler p0, Long p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/physics/AnimationHandler; */
/* .param p1, "x1" # J */
/* .line 41 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/physics/AnimationHandler;->doAnimationFrame(J)V */
return;
} // .end method
static java.util.ArrayList access$200 ( miui.android.animation.physics.AnimationHandler p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "x0" # Lmiui/android/animation/physics/AnimationHandler; */
/* .line 41 */
v0 = this.mAnimationCallbacks;
} // .end method
static miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider access$300 ( miui.android.animation.physics.AnimationHandler p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "x0" # Lmiui/android/animation/physics/AnimationHandler; */
/* .line 41 */
/* invoke-direct {p0}, Lmiui/android/animation/physics/AnimationHandler;->getProvider()Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider; */
} // .end method
private void cleanUpList ( ) {
/* .locals 2 */
/* .line 180 */
/* iget-boolean v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 181 */
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 182 */
v1 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 183 */
v1 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 181 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 186 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z */
/* .line 188 */
} // :cond_2
return;
} // .end method
private void doAnimationFrame ( Long p0 ) {
/* .locals 5 */
/* .param p1, "frameTime" # J */
/* .line 148 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 149 */
/* .local v0, "currentTime":J */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = this.mAnimationCallbacks;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 150 */
v3 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback; */
/* .line 151 */
/* .local v3, "callback":Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback; */
/* if-nez v3, :cond_0 */
/* .line 152 */
/* .line 154 */
} // :cond_0
v4 = /* invoke-direct {p0, v3, v0, v1}, Lmiui/android/animation/physics/AnimationHandler;->isCallbackDue(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 155 */
/* .line 149 */
} // .end local v3 # "callback":Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;
} // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 158 */
} // .end local v2 # "i":I
} // :cond_2
/* invoke-direct {p0}, Lmiui/android/animation/physics/AnimationHandler;->cleanUpList()V */
/* .line 159 */
return;
} // .end method
public static Long getFrameTime ( ) {
/* .locals 2 */
/* .line 94 */
v0 = miui.android.animation.physics.AnimationHandler.sAnimatorHandler;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 95 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
/* .line 97 */
} // :cond_0
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/physics/AnimationHandler; */
/* iget-wide v0, v0, Lmiui/android/animation/physics/AnimationHandler;->mCurrentFrameTime:J */
/* return-wide v0 */
} // .end method
public static miui.android.animation.physics.AnimationHandler getInstance ( ) {
/* .locals 2 */
/* .line 87 */
v0 = miui.android.animation.physics.AnimationHandler.sAnimatorHandler;
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 88 */
/* new-instance v1, Lmiui/android/animation/physics/AnimationHandler; */
/* invoke-direct {v1}, Lmiui/android/animation/physics/AnimationHandler;-><init>()V */
(( java.lang.ThreadLocal ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
/* .line 90 */
} // :cond_0
(( java.lang.ThreadLocal ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/physics/AnimationHandler; */
} // .end method
private miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider getProvider ( ) {
/* .locals 2 */
/* .line 109 */
v0 = this.mProvider;
/* if-nez v0, :cond_0 */
/* .line 110 */
/* nop */
/* .line 111 */
/* new-instance v0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16; */
v1 = this.mCallbackDispatcher;
/* invoke-direct {v0, v1}, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16;-><init>(Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;)V */
this.mProvider = v0;
/* .line 116 */
} // :cond_0
v0 = this.mProvider;
} // .end method
private Boolean isCallbackDue ( miui.android.animation.physics.AnimationHandler$AnimationFrameCallback p0, Long p1 ) {
/* .locals 4 */
/* .param p1, "callback" # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback; */
/* .param p2, "currentTime" # J */
/* .line 168 */
v0 = this.mDelayedCallbackStartTime;
(( android.util.ArrayMap ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Long; */
/* .line 169 */
/* .local v0, "startTime":Ljava/lang/Long; */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* .line 170 */
/* .line 172 */
} // :cond_0
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v2 */
/* cmp-long v2, v2, p2 */
/* if-gez v2, :cond_1 */
/* .line 173 */
v2 = this.mDelayedCallbackStartTime;
(( android.util.ArrayMap ) v2 ).remove ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 174 */
/* .line 176 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
/* # virtual methods */
public void addAnimationFrameCallback ( miui.android.animation.physics.AnimationHandler$AnimationFrameCallback p0, Long p1 ) {
/* .locals 3 */
/* .param p1, "callback" # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback; */
/* .param p2, "delay" # J */
/* .line 123 */
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-nez v0, :cond_0 */
/* .line 124 */
/* invoke-direct {p0}, Lmiui/android/animation/physics/AnimationHandler;->getProvider()Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider; */
(( miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider ) v0 ).postFrameCallback ( ); // invoke-virtual {v0}, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;->postFrameCallback()V
/* .line 126 */
} // :cond_0
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 127 */
v0 = this.mAnimationCallbacks;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 130 */
} // :cond_1
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p2, v0 */
/* if-lez v0, :cond_2 */
/* .line 131 */
v0 = this.mDelayedCallbackStartTime;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* add-long/2addr v1, p2 */
java.lang.Long .valueOf ( v1,v2 );
(( android.util.ArrayMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 133 */
} // :cond_2
return;
} // .end method
public void removeCallback ( miui.android.animation.physics.AnimationHandler$AnimationFrameCallback p0 ) {
/* .locals 3 */
/* .param p1, "callback" # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback; */
/* .line 139 */
v0 = this.mDelayedCallbackStartTime;
(( android.util.ArrayMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 140 */
v0 = this.mAnimationCallbacks;
v0 = (( java.util.ArrayList ) v0 ).indexOf ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 141 */
/* .local v0, "id":I */
/* if-ltz v0, :cond_0 */
/* .line 142 */
v1 = this.mAnimationCallbacks;
int v2 = 0; // const/4 v2, 0x0
(( java.util.ArrayList ) v1 ).set ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 143 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lmiui/android/animation/physics/AnimationHandler;->mListDirty:Z */
/* .line 145 */
} // :cond_0
return;
} // .end method
public void setProvider ( miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider p0 ) {
/* .locals 0 */
/* .param p1, "provider" # Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider; */
/* .line 105 */
this.mProvider = p1;
/* .line 106 */
return;
} // .end method
