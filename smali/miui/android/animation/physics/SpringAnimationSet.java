public class miui.android.animation.physics.SpringAnimationSet {
	 /* .source "SpringAnimationSet.java" */
	 /* # instance fields */
	 private java.util.List mAnimationContainer;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lmiui/android/animation/physics/SpringAnimation;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.physics.SpringAnimationSet ( ) {
/* .locals 1 */
/* .line 10 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 11 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mAnimationContainer = v0;
/* .line 12 */
return;
} // .end method
/* # virtual methods */
public void cancel ( ) {
/* .locals 2 */
/* .line 25 */
v0 = v0 = this.mAnimationContainer;
/* if-nez v0, :cond_2 */
/* .line 26 */
v0 = this.mAnimationContainer;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/physics/SpringAnimation; */
/* .line 27 */
/* .local v1, "springAnimation":Lmiui/android/animation/physics/SpringAnimation; */
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 28 */
	 (( miui.android.animation.physics.SpringAnimation ) v1 ).cancel ( ); // invoke-virtual {v1}, Lmiui/android/animation/physics/SpringAnimation;->cancel()V
	 /* .line 30 */
} // .end local v1 # "springAnimation":Lmiui/android/animation/physics/SpringAnimation;
} // :cond_0
/* .line 31 */
} // :cond_1
v0 = this.mAnimationContainer;
/* .line 33 */
} // :cond_2
return;
} // .end method
public void endAnimation ( ) {
/* .locals 2 */
/* .line 36 */
v0 = v0 = this.mAnimationContainer;
/* if-nez v0, :cond_2 */
/* .line 37 */
v0 = this.mAnimationContainer;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/physics/SpringAnimation; */
/* .line 38 */
/* .local v1, "springAnimation":Lmiui/android/animation/physics/SpringAnimation; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 39 */
(( miui.android.animation.physics.SpringAnimation ) v1 ).skipToEnd ( ); // invoke-virtual {v1}, Lmiui/android/animation/physics/SpringAnimation;->skipToEnd()V
/* .line 41 */
} // .end local v1 # "springAnimation":Lmiui/android/animation/physics/SpringAnimation;
} // :cond_0
/* .line 42 */
} // :cond_1
v0 = this.mAnimationContainer;
/* .line 44 */
} // :cond_2
return;
} // .end method
public void play ( miui.android.animation.physics.SpringAnimation p0 ) {
/* .locals 1 */
/* .param p1, "springAnimation" # Lmiui/android/animation/physics/SpringAnimation; */
/* .line 47 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 48 */
v0 = this.mAnimationContainer;
/* .line 50 */
} // :cond_0
return;
} // .end method
public void playTogether ( miui.android.animation.physics.SpringAnimation...p0 ) {
/* .locals 4 */
/* .param p1, "animations" # [Lmiui/android/animation/physics/SpringAnimation; */
/* .line 53 */
/* array-length v0, p1 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_1 */
/* aget-object v2, p1, v1 */
/* .line 54 */
/* .local v2, "springAnimation":Lmiui/android/animation/physics/SpringAnimation; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 55 */
v3 = this.mAnimationContainer;
/* .line 53 */
} // .end local v2 # "springAnimation":Lmiui/android/animation/physics/SpringAnimation;
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 58 */
} // :cond_1
return;
} // .end method
public void start ( ) {
/* .locals 2 */
/* .line 15 */
v0 = v0 = this.mAnimationContainer;
/* if-nez v0, :cond_1 */
/* .line 16 */
v0 = this.mAnimationContainer;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/physics/SpringAnimation; */
/* .line 17 */
/* .local v1, "springAnimation":Lmiui/android/animation/physics/SpringAnimation; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 18 */
(( miui.android.animation.physics.SpringAnimation ) v1 ).start ( ); // invoke-virtual {v1}, Lmiui/android/animation/physics/SpringAnimation;->start()V
/* .line 20 */
} // .end local v1 # "springAnimation":Lmiui/android/animation/physics/SpringAnimation;
} // :cond_0
/* .line 22 */
} // :cond_1
return;
} // .end method
