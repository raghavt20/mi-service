.class public abstract Lmiui/android/animation/physics/DynamicAnimation;
.super Ljava/lang/Object;
.source "DynamicAnimation.java"

# interfaces
.implements Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;,
        Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;,
        Lmiui/android/animation/physics/DynamicAnimation$MassState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lmiui/android/animation/physics/DynamicAnimation<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;"
    }
.end annotation


# static fields
.field public static final MIN_VISIBLE_CHANGE_ALPHA:F = 0.00390625f

.field public static final MIN_VISIBLE_CHANGE_PIXELS:F = 1.0f

.field public static final MIN_VISIBLE_CHANGE_ROTATION_DEGREES:F = 0.1f

.field public static final MIN_VISIBLE_CHANGE_SCALE:F = 0.002f

.field private static final THRESHOLD_MULTIPLIER:F = 0.75f

.field private static final UNSET:F = 3.4028235E38f


# instance fields
.field private final mEndListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLastFrameTime:J

.field mMaxValue:F

.field mMinValue:F

.field private mMinVisibleChange:F

.field final mProperty:Lmiui/android/animation/property/FloatProperty;

.field mRunning:Z

.field private mStartDelay:J

.field mStartValueIsSet:Z

.field final mTarget:Ljava/lang/Object;

.field private final mUpdateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field mValue:F

.field mVelocity:F


# direct methods
.method constructor <init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiui/android/animation/property/FloatProperty<",
            "TK;>;)V"
        }
    .end annotation

    .line 139
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TK;"
    .local p2, "property":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TK;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F

    .line 72
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    .line 76
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z

    .line 85
    iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    .line 88
    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F

    .line 89
    neg-float v0, v0

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F

    .line 92
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J

    .line 97
    iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    .line 140
    iput-object p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mTarget:Ljava/lang/Object;

    .line 141
    iput-object p2, p0, Lmiui/android/animation/physics/DynamicAnimation;->mProperty:Lmiui/android/animation/property/FloatProperty;

    .line 142
    sget-object v0, Lmiui/android/animation/property/ViewProperty;->ROTATION:Lmiui/android/animation/property/ViewProperty;

    if-eq p2, v0, :cond_4

    sget-object v0, Lmiui/android/animation/property/ViewProperty;->ROTATION_X:Lmiui/android/animation/property/ViewProperty;

    if-eq p2, v0, :cond_4

    sget-object v0, Lmiui/android/animation/property/ViewProperty;->ROTATION_Y:Lmiui/android/animation/property/ViewProperty;

    if-ne p2, v0, :cond_0

    goto :goto_1

    .line 145
    :cond_0
    sget-object v0, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    if-ne p2, v0, :cond_1

    .line 146
    const/high16 v0, 0x3b800000    # 0.00390625f

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    goto :goto_2

    .line 147
    :cond_1
    sget-object v0, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    if-eq p2, v0, :cond_3

    sget-object v0, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    if-ne p2, v0, :cond_2

    goto :goto_0

    .line 150
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    goto :goto_2

    .line 148
    :cond_3
    :goto_0
    const v0, 0x3b03126f    # 0.002f

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    goto :goto_2

    .line 144
    :cond_4
    :goto_1
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    .line 152
    :goto_2
    return-void
.end method

.method constructor <init>(Lmiui/android/animation/property/FloatValueHolder;)V
    .locals 2
    .param p1, "floatValueHolder"    # Lmiui/android/animation/property/FloatValueHolder;

    .line 116
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F

    .line 72
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    .line 76
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z

    .line 85
    iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    .line 88
    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F

    .line 89
    neg-float v0, v0

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F

    .line 92
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J

    .line 97
    iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mTarget:Ljava/lang/Object;

    .line 118
    new-instance v0, Lmiui/android/animation/physics/DynamicAnimation$1;

    const-string v1, "FloatValueHolder"

    invoke-direct {v0, p0, v1, p1}, Lmiui/android/animation/physics/DynamicAnimation$1;-><init>(Lmiui/android/animation/physics/DynamicAnimation;Ljava/lang/String;Lmiui/android/animation/property/FloatValueHolder;)V

    iput-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mProperty:Lmiui/android/animation/property/FloatProperty;

    .line 129
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    .line 130
    return-void
.end method

.method private endAnimationInternal(Z)V
    .locals 4
    .param p1, "canceled"    # Z

    .line 471
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    .line 472
    invoke-static {}, Lmiui/android/animation/physics/AnimationHandler;->getInstance()Lmiui/android/animation/physics/AnimationHandler;

    move-result-object v1

    invoke-virtual {v1, p0}, Lmiui/android/animation/physics/AnimationHandler;->removeCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;)V

    .line 473
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J

    .line 474
    iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z

    .line 475
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 476
    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 477
    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;

    iget v2, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    iget v3, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F

    invoke-interface {v1, p0, p1, v2, v3}, Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;->onAnimationEnd(Lmiui/android/animation/physics/DynamicAnimation;ZFF)V

    .line 475
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 480
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-static {v0}, Lmiui/android/animation/physics/DynamicAnimation;->removeNullEntries(Ljava/util/ArrayList;)V

    .line 481
    return-void
.end method

.method private getPropertyValue()F
    .locals 2

    .line 507
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mProperty:Lmiui/android/animation/property/FloatProperty;

    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mTarget:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method private static removeEntry(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 356
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    .local p1, "entry":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 357
    .local v0, "id":I
    if-ltz v0, :cond_0

    .line 358
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 360
    :cond_0
    return-void
.end method

.method private static removeNullEntries(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    .line 345
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 346
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 347
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 345
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 350
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private startAnimationInternal()V
    .locals 3

    .line 411
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    if-nez v0, :cond_2

    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    .line 413
    iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z

    if-nez v0, :cond_0

    .line 414
    invoke-direct {p0}, Lmiui/android/animation/physics/DynamicAnimation;->getPropertyValue()F

    move-result v0

    iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    .line 417
    :cond_0
    iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    iget v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 421
    invoke-static {}, Lmiui/android/animation/physics/AnimationHandler;->getInstance()Lmiui/android/animation/physics/AnimationHandler;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J

    invoke-virtual {v0, p0, v1, v2}, Lmiui/android/animation/physics/AnimationHandler;->addAnimationFrameCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)V

    goto :goto_0

    .line 418
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Starting value need to be in between min value and max value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 423
    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public addEndListener(Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 1
    .param p1, "listener"    # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;",
            ")TT;"
        }
    .end annotation

    .line 243
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    :cond_0
    return-object p0
.end method

.method public addUpdateListener(Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 2
    .param p1, "listener"    # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;",
            ")TT;"
        }
    .end annotation

    .line 271
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    invoke-virtual {p0}, Lmiui/android/animation/physics/DynamicAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 277
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    :cond_0
    return-object p0

    .line 274
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Error: Update listeners must be added beforethe miuix.animation."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cancel()V
    .locals 2

    .line 389
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 392
    iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    if-eqz v0, :cond_0

    .line 393
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lmiui/android/animation/physics/DynamicAnimation;->endAnimationInternal(Z)V

    .line 395
    :cond_0
    return-void

    .line 390
    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be canceled on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public doAnimationFrame(J)Z
    .locals 6
    .param p1, "frameTime"    # J

    .line 436
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 438
    iput-wide p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J

    .line 439
    iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    invoke-virtual {p0, v0}, Lmiui/android/animation/physics/DynamicAnimation;->setPropertyValue(F)V

    .line 440
    return v3

    .line 442
    :cond_0
    sub-long v0, p1, v0

    .line 443
    .local v0, "deltaT":J
    iput-wide p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J

    .line 444
    invoke-virtual {p0, v0, v1}, Lmiui/android/animation/physics/DynamicAnimation;->updateValueAndVelocity(J)Z

    move-result v2

    .line 446
    .local v2, "finished":Z
    iget v4, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    iget v5, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iput v4, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    .line 447
    iget v5, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    .line 449
    invoke-virtual {p0, v4}, Lmiui/android/animation/physics/DynamicAnimation;->setPropertyValue(F)V

    .line 451
    if-eqz v2, :cond_1

    .line 452
    invoke-direct {p0, v3}, Lmiui/android/animation/physics/DynamicAnimation;->endAnimationInternal(Z)V

    .line 454
    :cond_1
    return v2
.end method

.method abstract getAcceleration(FF)F
.end method

.method public getMinimumVisibleChange()F
    .locals 1

    .line 337
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    return v0
.end method

.method getValueThreshold()F
    .locals 2

    .line 500
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    return v0
.end method

.method abstract isAtEquilibrium(FF)Z
.end method

.method public isRunning()Z
    .locals 1

    .line 403
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    return v0
.end method

.method public removeEndListener(Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;)V
    .locals 1
    .param p1, "listener"    # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;

    .line 255
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mEndListeners:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->removeEntry(Ljava/util/ArrayList;Ljava/lang/Object;)V

    .line 256
    return-void
.end method

.method public removeUpdateListener(Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;

    .line 290
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->removeEntry(Ljava/util/ArrayList;Ljava/lang/Object;)V

    .line 291
    return-void
.end method

.method public setMaxValue(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 0
    .param p1, "max"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .line 203
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F

    .line 204
    return-object p0
.end method

.method public setMinValue(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 0
    .param p1, "min"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .line 216
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F

    .line 217
    return-object p0
.end method

.method public setMinimumVisibleChange(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 2
    .param p1, "minimumVisibleChange"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .line 322
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    .line 325
    iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F

    .line 326
    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr v0, p1

    invoke-virtual {p0, v0}, Lmiui/android/animation/physics/DynamicAnimation;->setValueThreshold(F)V

    .line 327
    return-object p0

    .line 323
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Minimum visible change must be positive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method setPropertyValue(F)V
    .locals 4
    .param p1, "value"    # F

    .line 487
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mProperty:Lmiui/android/animation/property/FloatProperty;

    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mTarget:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V

    .line 488
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 489
    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 490
    iget-object v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;

    iget v2, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    iget v3, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F

    invoke-interface {v1, p0, v2, v3}, Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;->onAnimationUpdate(Lmiui/android/animation/physics/DynamicAnimation;FF)V

    .line 488
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 493
    .end local v0    # "i":I
    :cond_1
    iget-object v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mUpdateListeners:Ljava/util/ArrayList;

    invoke-static {v0}, Lmiui/android/animation/physics/DynamicAnimation;->removeNullEntries(Ljava/util/ArrayList;)V

    .line 494
    return-void
.end method

.method public setStartDelay(J)V
    .locals 2
    .param p1, "startDelay"    # J

    .line 229
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 230
    const-wide/16 p1, 0x0

    .line 232
    :cond_0
    iput-wide p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J

    .line 233
    return-void
.end method

.method public setStartValue(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 1
    .param p1, "startValue"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .line 162
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z

    .line 164
    return-object p0
.end method

.method public setStartVelocity(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 0
    .param p1, "startVelocity"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    .line 186
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F

    .line 187
    return-object p0
.end method

.method abstract setValueThreshold(F)V
.end method

.method public start()V
    .locals 2

    .line 374
    .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;"
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 377
    iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z

    if-nez v0, :cond_0

    .line 378
    invoke-direct {p0}, Lmiui/android/animation/physics/DynamicAnimation;->startAnimationInternal()V

    .line 380
    :cond_0
    return-void

    .line 375
    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method abstract updateValueAndVelocity(J)Z
.end method
