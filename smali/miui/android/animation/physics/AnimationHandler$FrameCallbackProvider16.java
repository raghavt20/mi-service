class miui.android.animation.physics.AnimationHandler$FrameCallbackProvider16 extends miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/physics/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "FrameCallbackProvider16" */
} // .end annotation
/* # instance fields */
private final android.view.Choreographer mChoreographer;
private final android.view.Choreographer$FrameCallback mChoreographerCallback;
/* # direct methods */
 miui.android.animation.physics.AnimationHandler$FrameCallbackProvider16 ( ) {
/* .locals 1 */
/* .param p1, "dispatcher" # Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher; */
/* .line 199 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;-><init>(Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;)V */
/* .line 195 */
android.view.Choreographer .getInstance ( );
this.mChoreographer = v0;
/* .line 200 */
/* new-instance v0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16$1; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16$1;-><init>(Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider16;)V */
this.mChoreographerCallback = v0;
/* .line 206 */
return;
} // .end method
/* # virtual methods */
void postFrameCallback ( ) {
/* .locals 2 */
/* .line 210 */
v0 = this.mChoreographer;
v1 = this.mChoreographerCallback;
(( android.view.Choreographer ) v0 ).postFrameCallback ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V
/* .line 211 */
return;
} // .end method
