abstract class miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/physics/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x408 */
/* name = "AnimationFrameCallbackProvider" */
} // .end annotation
/* # instance fields */
final miui.android.animation.physics.AnimationHandler$AnimationCallbackDispatcher mDispatcher;
/* # direct methods */
 miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider ( ) {
/* .locals 0 */
/* .param p1, "dispatcher" # Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher; */
/* .line 252 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 253 */
this.mDispatcher = p1;
/* .line 254 */
return;
} // .end method
/* # virtual methods */
abstract void postFrameCallback ( ) {
} // .end method
