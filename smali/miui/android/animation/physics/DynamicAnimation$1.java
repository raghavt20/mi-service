class miui.android.animation.physics.DynamicAnimation$1 extends miui.android.animation.property.FloatProperty {
	 /* .source "DynamicAnimation.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lmiui/android/animation/physics/DynamicAnimation;-><init>(Lmiui/android/animation/property/FloatValueHolder;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final miui.android.animation.physics.DynamicAnimation this$0; //synthetic
final miui.android.animation.property.FloatValueHolder val$floatValueHolder; //synthetic
/* # direct methods */
 miui.android.animation.physics.DynamicAnimation$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lmiui/android/animation/physics/DynamicAnimation; */
/* .param p2, "name" # Ljava/lang/String; */
/* .line 118 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation$1;, "Lmiui/android/animation/physics/DynamicAnimation$1;" */
this.this$0 = p1;
this.val$floatValueHolder = p3;
/* invoke-direct {p0, p2}, Lmiui/android/animation/property/FloatProperty;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "object" # Ljava/lang/Object; */
/* .line 121 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation$1;, "Lmiui/android/animation/physics/DynamicAnimation$1;" */
v0 = this.val$floatValueHolder;
v0 = (( miui.android.animation.property.FloatValueHolder ) v0 ).getValue ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/FloatValueHolder;->getValue()F
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "object" # Ljava/lang/Object; */
/* .param p2, "value" # F */
/* .line 126 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation$1;, "Lmiui/android/animation/physics/DynamicAnimation$1;" */
v0 = this.val$floatValueHolder;
(( miui.android.animation.property.FloatValueHolder ) v0 ).setValue ( p2 ); // invoke-virtual {v0, p2}, Lmiui/android/animation/property/FloatValueHolder;->setValue(F)V
/* .line 127 */
return;
} // .end method
