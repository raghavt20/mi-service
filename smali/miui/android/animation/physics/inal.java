public class inal extends miui.android.animation.physics.DynamicAnimation {
	 /* .source "FlingAnimation.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/physics/FlingAnimation$DragForce; */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lmiui/android/animation/physics/DynamicAnimation<", */
/* "Lmiui/android/animation/physics/FlingAnimation;", */
/* ">;" */
/* } */
} // .end annotation
/* # instance fields */
private final miui.android.animation.physics.FlingAnimation$DragForce mFlingForce;
/* # direct methods */
public inal ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<K:", */
/* "Ljava/lang/Object;", */
/* ">(TK;", */
/* "Lmiui/android/animation/property/FloatProperty<", */
/* "TK;>;)V" */
/* } */
} // .end annotation
/* .line 73 */
/* .local p1, "object":Ljava/lang/Object;, "TK;" */
/* .local p2, "property":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TK;>;" */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V */
/* .line 46 */
/* new-instance v0, Lmiui/android/animation/physics/FlingAnimation$DragForce; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;-><init>(Lmiui/android/animation/physics/FlingAnimation$1;)V */
this.mFlingForce = v0;
/* .line 74 */
v1 = (( miui.android.animation.physics.FlingAnimation ) p0 ).getValueThreshold ( ); // invoke-virtual {p0}, Lmiui/android/animation/physics/FlingAnimation;->getValueThreshold()F
(( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).setValueThreshold ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setValueThreshold(F)V
/* .line 75 */
return;
} // .end method
public inal ( ) {
/* .locals 2 */
/* .param p1, "floatValueHolder" # Lmiui/android/animation/property/FloatValueHolder; */
/* .line 61 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Lmiui/android/animation/property/FloatValueHolder;)V */
/* .line 46 */
/* new-instance v0, Lmiui/android/animation/physics/FlingAnimation$DragForce; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;-><init>(Lmiui/android/animation/physics/FlingAnimation$1;)V */
this.mFlingForce = v0;
/* .line 62 */
v1 = (( miui.android.animation.physics.FlingAnimation ) p0 ).getValueThreshold ( ); // invoke-virtual {p0}, Lmiui/android/animation/physics/FlingAnimation;->getValueThreshold()F
(( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).setValueThreshold ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setValueThreshold(F)V
/* .line 63 */
return;
} // .end method
/* # virtual methods */
Float getAcceleration ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "value" # F */
/* .param p2, "velocity" # F */
/* .line 182 */
v0 = this.mFlingForce;
v0 = (( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).getAcceleration ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->getAcceleration(FF)F
} // .end method
public Float getFriction ( ) {
/* .locals 1 */
/* .line 101 */
v0 = this.mFlingForce;
v0 = (( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).getFrictionScalar ( ); // invoke-virtual {v0}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->getFrictionScalar()F
} // .end method
Boolean isAtEquilibrium ( Float p0, Float p1 ) {
/* .locals 1 */
/* .param p1, "value" # F */
/* .param p2, "velocity" # F */
/* .line 187 */
/* iget v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mMaxValue:F */
/* cmpl-float v0, p1, v0 */
/* if-gez v0, :cond_1 */
/* iget v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mMinValue:F */
/* cmpg-float v0, p1, v0 */
/* if-lez v0, :cond_1 */
v0 = this.mFlingForce;
/* .line 189 */
v0 = (( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).isAtEquilibrium ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->isAtEquilibrium(FF)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 187 */
} // :goto_1
} // .end method
public miui.android.animation.physics.FlingAnimation setFriction ( Float p0 ) {
/* .locals 2 */
/* .param p1, "friction" # F */
/* .line 87 */
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 90 */
v0 = this.mFlingForce;
(( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).setFrictionScalar ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setFrictionScalar(F)V
/* .line 91 */
/* .line 88 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Friction must be positive"; // const-string v1, "Friction must be positive"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public miui.android.animation.physics.DynamicAnimation setMaxValue ( Float p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 44 */
(( miui.android.animation.physics.FlingAnimation ) p0 ).setMaxValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/physics/FlingAnimation;->setMaxValue(F)Lmiui/android/animation/physics/FlingAnimation;
} // .end method
public miui.android.animation.physics.FlingAnimation setMaxValue ( Float p0 ) {
/* .locals 0 */
/* .param p1, "maxValue" # F */
/* .line 126 */
/* invoke-super {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->setMaxValue(F)Lmiui/android/animation/physics/DynamicAnimation; */
/* .line 127 */
} // .end method
public miui.android.animation.physics.DynamicAnimation setMinValue ( Float p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 44 */
(( miui.android.animation.physics.FlingAnimation ) p0 ).setMinValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/physics/FlingAnimation;->setMinValue(F)Lmiui/android/animation/physics/FlingAnimation;
} // .end method
public miui.android.animation.physics.FlingAnimation setMinValue ( Float p0 ) {
/* .locals 0 */
/* .param p1, "minValue" # F */
/* .line 113 */
/* invoke-super {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->setMinValue(F)Lmiui/android/animation/physics/DynamicAnimation; */
/* .line 114 */
} // .end method
public miui.android.animation.physics.DynamicAnimation setStartVelocity ( Float p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 44 */
(( miui.android.animation.physics.FlingAnimation ) p0 ).setStartVelocity ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/physics/FlingAnimation;->setStartVelocity(F)Lmiui/android/animation/physics/FlingAnimation;
} // .end method
public miui.android.animation.physics.FlingAnimation setStartVelocity ( Float p0 ) {
/* .locals 0 */
/* .param p1, "startVelocity" # F */
/* .line 153 */
/* invoke-super {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->setStartVelocity(F)Lmiui/android/animation/physics/DynamicAnimation; */
/* .line 154 */
} // .end method
void setValueThreshold ( Float p0 ) {
/* .locals 1 */
/* .param p1, "threshold" # F */
/* .line 194 */
v0 = this.mFlingForce;
(( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).setValueThreshold ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setValueThreshold(F)V
/* .line 195 */
return;
} // .end method
Boolean updateValueAndVelocity ( Long p0 ) {
/* .locals 4 */
/* .param p1, "deltaT" # J */
/* .line 160 */
v0 = this.mFlingForce;
/* iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* iget v2, p0, Lmiui/android/animation/physics/FlingAnimation;->mVelocity:F */
(( miui.android.animation.physics.FlingAnimation$DragForce ) v0 ).updateValueAndVelocity ( v1, v2, p1, p2 ); // invoke-virtual {v0, v1, v2, p1, p2}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->updateValueAndVelocity(FFJ)Lmiui/android/animation/physics/DynamicAnimation$MassState;
/* .line 161 */
/* .local v0, "state":Lmiui/android/animation/physics/DynamicAnimation$MassState; */
/* iget v1, v0, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mValue:F */
/* iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* .line 162 */
/* iget v1, v0, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mVelocity:F */
/* iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mVelocity:F */
/* .line 165 */
/* iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* iget v2, p0, Lmiui/android/animation/physics/FlingAnimation;->mMinValue:F */
/* cmpg-float v1, v1, v2 */
int v2 = 1; // const/4 v2, 0x1
/* if-gez v1, :cond_0 */
/* .line 166 */
/* iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mMinValue:F */
/* iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* .line 167 */
/* .line 169 */
} // :cond_0
/* iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* iget v3, p0, Lmiui/android/animation/physics/FlingAnimation;->mMaxValue:F */
/* cmpl-float v1, v1, v3 */
/* if-lez v1, :cond_1 */
/* .line 170 */
/* iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mMaxValue:F */
/* iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* .line 171 */
/* .line 174 */
} // :cond_1
/* iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F */
/* iget v3, p0, Lmiui/android/animation/physics/FlingAnimation;->mVelocity:F */
v1 = (( miui.android.animation.physics.FlingAnimation ) p0 ).isAtEquilibrium ( v1, v3 ); // invoke-virtual {p0, v1, v3}, Lmiui/android/animation/physics/FlingAnimation;->isAtEquilibrium(FF)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 175 */
/* .line 177 */
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // .end method
