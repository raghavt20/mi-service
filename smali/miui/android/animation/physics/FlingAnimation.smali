.class public final Lmiui/android/animation/physics/FlingAnimation;
.super Lmiui/android/animation/physics/DynamicAnimation;
.source "FlingAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/physics/FlingAnimation$DragForce;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/android/animation/physics/DynamicAnimation<",
        "Lmiui/android/animation/physics/FlingAnimation;",
        ">;"
    }
.end annotation


# instance fields
.field private final mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiui/android/animation/property/FloatProperty<",
            "TK;>;)V"
        }
    .end annotation

    .line 73
    .local p1, "object":Ljava/lang/Object;, "TK;"
    .local p2, "property":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TK;>;"
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiui/android/animation/property/FloatProperty;)V

    .line 46
    new-instance v0, Lmiui/android/animation/physics/FlingAnimation$DragForce;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;-><init>(Lmiui/android/animation/physics/FlingAnimation$1;)V

    iput-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    .line 74
    invoke-virtual {p0}, Lmiui/android/animation/physics/FlingAnimation;->getValueThreshold()F

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setValueThreshold(F)V

    .line 75
    return-void
.end method

.method public constructor <init>(Lmiui/android/animation/property/FloatValueHolder;)V
    .locals 2
    .param p1, "floatValueHolder"    # Lmiui/android/animation/property/FloatValueHolder;

    .line 61
    invoke-direct {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;-><init>(Lmiui/android/animation/property/FloatValueHolder;)V

    .line 46
    new-instance v0, Lmiui/android/animation/physics/FlingAnimation$DragForce;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;-><init>(Lmiui/android/animation/physics/FlingAnimation$1;)V

    iput-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    .line 62
    invoke-virtual {p0}, Lmiui/android/animation/physics/FlingAnimation;->getValueThreshold()F

    move-result v1

    invoke-virtual {v0, v1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setValueThreshold(F)V

    .line 63
    return-void
.end method


# virtual methods
.method getAcceleration(FF)F
    .locals 1
    .param p1, "value"    # F
    .param p2, "velocity"    # F

    .line 182
    iget-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->getAcceleration(FF)F

    move-result v0

    return v0
.end method

.method public getFriction()F
    .locals 1

    .line 101
    iget-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    invoke-virtual {v0}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->getFrictionScalar()F

    move-result v0

    return v0
.end method

.method isAtEquilibrium(FF)Z
    .locals 1
    .param p1, "value"    # F
    .param p2, "velocity"    # F

    .line 187
    iget v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mMaxValue:F

    cmpl-float v0, p1, v0

    if-gez v0, :cond_1

    iget v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mMinValue:F

    cmpg-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    .line 189
    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->isAtEquilibrium(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 187
    :goto_1
    return v0
.end method

.method public setFriction(F)Lmiui/android/animation/physics/FlingAnimation;
    .locals 2
    .param p1, "friction"    # F

    .line 87
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    .line 90
    iget-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    invoke-virtual {v0, p1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setFrictionScalar(F)V

    .line 91
    return-object p0

    .line 88
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Friction must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic setMaxValue(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lmiui/android/animation/physics/FlingAnimation;->setMaxValue(F)Lmiui/android/animation/physics/FlingAnimation;

    move-result-object p1

    return-object p1
.end method

.method public setMaxValue(F)Lmiui/android/animation/physics/FlingAnimation;
    .locals 0
    .param p1, "maxValue"    # F

    .line 126
    invoke-super {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->setMaxValue(F)Lmiui/android/animation/physics/DynamicAnimation;

    .line 127
    return-object p0
.end method

.method public bridge synthetic setMinValue(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lmiui/android/animation/physics/FlingAnimation;->setMinValue(F)Lmiui/android/animation/physics/FlingAnimation;

    move-result-object p1

    return-object p1
.end method

.method public setMinValue(F)Lmiui/android/animation/physics/FlingAnimation;
    .locals 0
    .param p1, "minValue"    # F

    .line 113
    invoke-super {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->setMinValue(F)Lmiui/android/animation/physics/DynamicAnimation;

    .line 114
    return-object p0
.end method

.method public bridge synthetic setStartVelocity(F)Lmiui/android/animation/physics/DynamicAnimation;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lmiui/android/animation/physics/FlingAnimation;->setStartVelocity(F)Lmiui/android/animation/physics/FlingAnimation;

    move-result-object p1

    return-object p1
.end method

.method public setStartVelocity(F)Lmiui/android/animation/physics/FlingAnimation;
    .locals 0
    .param p1, "startVelocity"    # F

    .line 153
    invoke-super {p0, p1}, Lmiui/android/animation/physics/DynamicAnimation;->setStartVelocity(F)Lmiui/android/animation/physics/DynamicAnimation;

    .line 154
    return-object p0
.end method

.method setValueThreshold(F)V
    .locals 1
    .param p1, "threshold"    # F

    .line 194
    iget-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    invoke-virtual {v0, p1}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->setValueThreshold(F)V

    .line 195
    return-void
.end method

.method updateValueAndVelocity(J)Z
    .locals 4
    .param p1, "deltaT"    # J

    .line 160
    iget-object v0, p0, Lmiui/android/animation/physics/FlingAnimation;->mFlingForce:Lmiui/android/animation/physics/FlingAnimation$DragForce;

    iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    iget v2, p0, Lmiui/android/animation/physics/FlingAnimation;->mVelocity:F

    invoke-virtual {v0, v1, v2, p1, p2}, Lmiui/android/animation/physics/FlingAnimation$DragForce;->updateValueAndVelocity(FFJ)Lmiui/android/animation/physics/DynamicAnimation$MassState;

    move-result-object v0

    .line 161
    .local v0, "state":Lmiui/android/animation/physics/DynamicAnimation$MassState;
    iget v1, v0, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mValue:F

    iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    .line 162
    iget v1, v0, Lmiui/android/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mVelocity:F

    .line 165
    iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    iget v2, p0, Lmiui/android/animation/physics/FlingAnimation;->mMinValue:F

    cmpg-float v1, v1, v2

    const/4 v2, 0x1

    if-gez v1, :cond_0

    .line 166
    iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mMinValue:F

    iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    .line 167
    return v2

    .line 169
    :cond_0
    iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    iget v3, p0, Lmiui/android/animation/physics/FlingAnimation;->mMaxValue:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    .line 170
    iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mMaxValue:F

    iput v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    .line 171
    return v2

    .line 174
    :cond_1
    iget v1, p0, Lmiui/android/animation/physics/FlingAnimation;->mValue:F

    iget v3, p0, Lmiui/android/animation/physics/FlingAnimation;->mVelocity:F

    invoke-virtual {p0, v1, v3}, Lmiui/android/animation/physics/FlingAnimation;->isAtEquilibrium(FF)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    return v2

    .line 177
    :cond_2
    const/4 v1, 0x0

    return v1
.end method
