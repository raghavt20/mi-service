class miui.android.animation.physics.AnimationHandler$FrameCallbackProvider14 extends miui.android.animation.physics.AnimationHandler$AnimationFrameCallbackProvider {
	 /* .source "AnimationHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/physics/AnimationHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "FrameCallbackProvider14" */
} // .end annotation
/* # instance fields */
private final android.os.Handler mHandler;
private Long mLastFrameTime;
private final java.lang.Runnable mRunnable;
/* # direct methods */
 miui.android.animation.physics.AnimationHandler$FrameCallbackProvider14 ( ) {
/* .locals 2 */
/* .param p1, "dispatcher" # Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher; */
/* .line 225 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallbackProvider;-><init>(Lmiui/android/animation/physics/AnimationHandler$AnimationCallbackDispatcher;)V */
/* .line 222 */
/* const-wide/16 v0, -0x1 */
/* iput-wide v0, p0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14;->mLastFrameTime:J */
/* .line 226 */
/* new-instance v0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14$1; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14$1;-><init>(Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14;)V */
this.mRunnable = v0;
/* .line 233 */
/* new-instance v0, Landroid/os/Handler; */
android.os.Looper .myLooper ( );
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 234 */
return;
} // .end method
static Long access$402 ( miui.android.animation.physics.AnimationHandler$FrameCallbackProvider14 p0, Long p1 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14; */
/* .param p1, "x1" # J */
/* .line 218 */
/* iput-wide p1, p0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14;->mLastFrameTime:J */
/* return-wide p1 */
} // .end method
/* # virtual methods */
void postFrameCallback ( ) {
/* .locals 4 */
/* .line 238 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lmiui/android/animation/physics/AnimationHandler$FrameCallbackProvider14;->mLastFrameTime:J */
/* sub-long/2addr v0, v2 */
/* const-wide/16 v2, 0xa */
/* sub-long/2addr v2, v0 */
/* .line 239 */
/* .local v2, "delay":J */
/* const-wide/16 v0, 0x0 */
java.lang.Math .max ( v2,v3,v0,v1 );
/* move-result-wide v0 */
/* .line 240 */
} // .end local v2 # "delay":J
/* .local v0, "delay":J */
v2 = this.mHandler;
v3 = this.mRunnable;
(( android.os.Handler ) v2 ).postDelayed ( v3, v0, v1 ); // invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 241 */
return;
} // .end method
