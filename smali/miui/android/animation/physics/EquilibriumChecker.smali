.class public Lmiui/android/animation/physics/EquilibriumChecker;
.super Ljava/lang/Object;
.source "EquilibriumChecker.java"


# static fields
.field public static final MIN_VISIBLE_CHANGE_ALPHA:F = 0.00390625f

.field public static final MIN_VISIBLE_CHANGE_PIXELS:F = 1.0f

.field public static final MIN_VISIBLE_CHANGE_ROTATION_DEGREES:F = 0.1f

.field public static final MIN_VISIBLE_CHANGE_SCALE:F = 0.002f

.field private static final THRESHOLD_MULTIPLIER:F = 0.75f

.field public static final VELOCITY_THRESHOLD_MULTIPLIER:F = 16.666666f


# instance fields
.field private mTargetValue:D

.field private mValueThreshold:F

.field private mVelocityThreshold:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D

    return-void
.end method

.method private isAt(DD)Z
    .locals 4
    .param p1, "value"    # D
    .param p3, "target"    # D

    .line 51
    iget-wide v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    sub-double v0, p1, p3

    .line 52
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget v2, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mValueThreshold:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 51
    :goto_1
    return v0
.end method


# virtual methods
.method public init(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)V
    .locals 2
    .param p1, "target"    # Lmiui/android/animation/IAnimTarget;
    .param p2, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p3, "targetValue"    # D

    .line 40
    invoke-virtual {p1, p2}, Lmiui/android/animation/IAnimTarget;->getMinVisibleChange(Ljava/lang/Object;)F

    move-result v0

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    iput v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mValueThreshold:F

    .line 41
    const v1, 0x41855555

    mul-float/2addr v0, v1

    iput v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mVelocityThreshold:F

    .line 42
    iput-wide p3, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D

    .line 43
    return-void
.end method

.method public isAtEquilibrium(IDD)Z
    .locals 4
    .param p1, "easeStyle"    # I
    .param p2, "value"    # D
    .param p4, "velocity"    # D

    .line 46
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    iget-wide v0, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mTargetValue:D

    invoke-direct {p0, p2, p3, v0, v1}, Lmiui/android/animation/physics/EquilibriumChecker;->isAt(DD)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    :cond_0
    invoke-static {p4, p5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget v2, p0, Lmiui/android/animation/physics/EquilibriumChecker;->mVelocityThreshold:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0
.end method
