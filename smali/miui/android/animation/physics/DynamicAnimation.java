public abstract class miui.android.animation.physics.DynamicAnimation implements miui.android.animation.physics.AnimationHandler$AnimationFrameCallback {
	 /* .source "DynamicAnimation.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;, */
	 /* Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;, */
	 /* Lmiui/android/animation/physics/DynamicAnimation$MassState; */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Lmiui/android/animation/physics/DynamicAnimation<", */
/* "TT;>;>", */
/* "Ljava/lang/Object;", */
/* "Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;" */
/* } */
} // .end annotation
/* # static fields */
public static final Float MIN_VISIBLE_CHANGE_ALPHA;
public static final Float MIN_VISIBLE_CHANGE_PIXELS;
public static final Float MIN_VISIBLE_CHANGE_ROTATION_DEGREES;
public static final Float MIN_VISIBLE_CHANGE_SCALE;
private static final Float THRESHOLD_MULTIPLIER;
private static final Float UNSET;
/* # instance fields */
private final java.util.ArrayList mEndListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Long mLastFrameTime;
Float mMaxValue;
Float mMinValue;
private Float mMinVisibleChange;
final miui.android.animation.property.FloatProperty mProperty;
Boolean mRunning;
private Long mStartDelay;
Boolean mStartValueIsSet;
final java.lang.Object mTarget;
private final java.util.ArrayList mUpdateListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Float mValue;
Float mVelocity;
/* # direct methods */
 miui.android.animation.physics.DynamicAnimation ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<K:", */
/* "Ljava/lang/Object;", */
/* ">(TK;", */
/* "Lmiui/android/animation/property/FloatProperty<", */
/* "TK;>;)V" */
/* } */
} // .end annotation
/* .line 139 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* .local p1, "object":Ljava/lang/Object;, "TK;" */
/* .local p2, "property":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TK;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F */
/* .line 72 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* .line 76 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 85 */
/* iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
/* .line 88 */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F */
/* .line 89 */
/* neg-float v0, v0 */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F */
/* .line 92 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J */
/* .line 97 */
/* iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J */
/* .line 100 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mEndListeners = v0;
/* .line 103 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mUpdateListeners = v0;
/* .line 140 */
this.mTarget = p1;
/* .line 141 */
this.mProperty = p2;
/* .line 142 */
v0 = miui.android.animation.property.ViewProperty.ROTATION;
/* if-eq p2, v0, :cond_4 */
v0 = miui.android.animation.property.ViewProperty.ROTATION_X;
/* if-eq p2, v0, :cond_4 */
v0 = miui.android.animation.property.ViewProperty.ROTATION_Y;
/* if-ne p2, v0, :cond_0 */
/* .line 145 */
} // :cond_0
v0 = miui.android.animation.property.ViewProperty.ALPHA;
/* if-ne p2, v0, :cond_1 */
/* .line 146 */
/* const/high16 v0, 0x3b800000 # 0.00390625f */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* .line 147 */
} // :cond_1
v0 = miui.android.animation.property.ViewProperty.SCALE_X;
/* if-eq p2, v0, :cond_3 */
v0 = miui.android.animation.property.ViewProperty.SCALE_Y;
/* if-ne p2, v0, :cond_2 */
/* .line 150 */
} // :cond_2
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* .line 148 */
} // :cond_3
} // :goto_0
/* const v0, 0x3b03126f # 0.002f */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* .line 144 */
} // :cond_4
} // :goto_1
/* const v0, 0x3dcccccd # 0.1f */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* .line 152 */
} // :goto_2
return;
} // .end method
 miui.android.animation.physics.DynamicAnimation ( ) {
/* .locals 2 */
/* .param p1, "floatValueHolder" # Lmiui/android/animation/property/FloatValueHolder; */
/* .line 116 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F */
/* .line 72 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* .line 76 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 85 */
/* iput-boolean v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
/* .line 88 */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F */
/* .line 89 */
/* neg-float v0, v0 */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F */
/* .line 92 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J */
/* .line 97 */
/* iput-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J */
/* .line 100 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mEndListeners = v0;
/* .line 103 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mUpdateListeners = v0;
/* .line 117 */
int v0 = 0; // const/4 v0, 0x0
this.mTarget = v0;
/* .line 118 */
/* new-instance v0, Lmiui/android/animation/physics/DynamicAnimation$1; */
final String v1 = "FloatValueHolder"; // const-string v1, "FloatValueHolder"
/* invoke-direct {v0, p0, v1, p1}, Lmiui/android/animation/physics/DynamicAnimation$1;-><init>(Lmiui/android/animation/physics/DynamicAnimation;Ljava/lang/String;Lmiui/android/animation/property/FloatValueHolder;)V */
this.mProperty = v0;
/* .line 129 */
/* const/high16 v0, 0x3f800000 # 1.0f */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* .line 130 */
return;
} // .end method
private void endAnimationInternal ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "canceled" # Z */
/* .line 471 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
/* .line 472 */
miui.android.animation.physics.AnimationHandler .getInstance ( );
(( miui.android.animation.physics.AnimationHandler ) v1 ).removeCallback ( p0 ); // invoke-virtual {v1, p0}, Lmiui/android/animation/physics/AnimationHandler;->removeCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;)V
/* .line 473 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J */
/* .line 474 */
/* iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 475 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mEndListeners;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 476 */
v1 = this.mEndListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 477 */
v1 = this.mEndListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener; */
/* iget v2, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* iget v3, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F */
/* .line 475 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 480 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mEndListeners;
miui.android.animation.physics.DynamicAnimation .removeNullEntries ( v0 );
/* .line 481 */
return;
} // .end method
private Float getPropertyValue ( ) {
/* .locals 2 */
/* .line 507 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
v0 = this.mProperty;
v1 = this.mTarget;
v0 = (( miui.android.animation.property.FloatProperty ) v0 ).getValue ( v1 ); // invoke-virtual {v0, v1}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F
} // .end method
private static void removeEntry ( java.util.ArrayList p0, java.lang.Object p1 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/util/ArrayList<", */
/* "TT;>;TT;)V" */
/* } */
} // .end annotation
/* .line 356 */
/* .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;" */
/* .local p1, "entry":Ljava/lang/Object;, "TT;" */
v0 = (( java.util.ArrayList ) p0 ).indexOf ( p1 ); // invoke-virtual {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
/* .line 357 */
/* .local v0, "id":I */
/* if-ltz v0, :cond_0 */
/* .line 358 */
int v1 = 0; // const/4 v1, 0x0
(( java.util.ArrayList ) p0 ).set ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 360 */
} // :cond_0
return;
} // .end method
private static void removeNullEntries ( java.util.ArrayList p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/util/ArrayList<", */
/* "TT;>;)V" */
/* } */
} // .end annotation
/* .line 345 */
/* .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TT;>;" */
v0 = (( java.util.ArrayList ) p0 ).size ( ); // invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
/* add-int/lit8 v0, v0, -0x1 */
/* .local v0, "i":I */
} // :goto_0
/* if-ltz v0, :cond_1 */
/* .line 346 */
(( java.util.ArrayList ) p0 ).get ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* if-nez v1, :cond_0 */
/* .line 347 */
(( java.util.ArrayList ) p0 ).remove ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
/* .line 345 */
} // :cond_0
/* add-int/lit8 v0, v0, -0x1 */
/* .line 350 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
private void startAnimationInternal ( ) {
/* .locals 3 */
/* .line 411 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
/* if-nez v0, :cond_2 */
/* .line 412 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
/* .line 413 */
/* iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z */
/* if-nez v0, :cond_0 */
/* .line 414 */
v0 = /* invoke-direct {p0}, Lmiui/android/animation/physics/DynamicAnimation;->getPropertyValue()F */
/* iput v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* .line 417 */
} // :cond_0
/* iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* iget v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F */
/* cmpl-float v1, v0, v1 */
/* if-gtz v1, :cond_1 */
/* iget v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F */
/* cmpg-float v0, v0, v1 */
/* if-ltz v0, :cond_1 */
/* .line 421 */
miui.android.animation.physics.AnimationHandler .getInstance ( );
/* iget-wide v1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J */
(( miui.android.animation.physics.AnimationHandler ) v0 ).addAnimationFrameCallback ( p0, v1, v2 ); // invoke-virtual {v0, p0, v1, v2}, Lmiui/android/animation/physics/AnimationHandler;->addAnimationFrameCallback(Lmiui/android/animation/physics/AnimationHandler$AnimationFrameCallback;J)V
/* .line 418 */
} // :cond_1
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Starting value need to be in between min value and max value"; // const-string v1, "Starting value need to be in between min value and max value"
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
/* .line 423 */
} // :cond_2
} // :goto_0
return;
} // .end method
/* # virtual methods */
public miui.android.animation.physics.DynamicAnimation addEndListener ( miui.android.animation.physics.DynamicAnimation$OnAnimationEndListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 243 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
v0 = this.mEndListeners;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 244 */
v0 = this.mEndListeners;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 246 */
} // :cond_0
} // .end method
public miui.android.animation.physics.DynamicAnimation addUpdateListener ( miui.android.animation.physics.DynamicAnimation$OnAnimationUpdateListener p0 ) {
/* .locals 2 */
/* .param p1, "listener" # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener;", */
/* ")TT;" */
/* } */
} // .end annotation
/* .line 271 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
v0 = (( miui.android.animation.physics.DynamicAnimation ) p0 ).isRunning ( ); // invoke-virtual {p0}, Lmiui/android/animation/physics/DynamicAnimation;->isRunning()Z
/* if-nez v0, :cond_1 */
/* .line 277 */
v0 = this.mUpdateListeners;
v0 = (( java.util.ArrayList ) v0 ).contains ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 278 */
v0 = this.mUpdateListeners;
(( java.util.ArrayList ) v0 ).add ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 280 */
} // :cond_0
/* .line 274 */
} // :cond_1
/* new-instance v0, Ljava/lang/UnsupportedOperationException; */
final String v1 = "Error: Update listeners must be added beforethe miuix.animation."; // const-string v1, "Error: Update listeners must be added beforethe miuix.animation."
/* invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void cancel ( ) {
/* .locals 2 */
/* .line 389 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
android.os.Looper .myLooper ( );
android.os.Looper .getMainLooper ( );
/* if-ne v0, v1, :cond_1 */
/* .line 392 */
/* iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 393 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lmiui/android/animation/physics/DynamicAnimation;->endAnimationInternal(Z)V */
/* .line 395 */
} // :cond_0
return;
/* .line 390 */
} // :cond_1
/* new-instance v0, Landroid/util/AndroidRuntimeException; */
final String v1 = "Animations may only be canceled on the main thread"; // const-string v1, "Animations may only be canceled on the main thread"
/* invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public Boolean doAnimationFrame ( Long p0 ) {
/* .locals 6 */
/* .param p1, "frameTime" # J */
/* .line 436 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iget-wide v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v2, v0, v2 */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 438 */
/* iput-wide p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J */
/* .line 439 */
/* iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
(( miui.android.animation.physics.DynamicAnimation ) p0 ).setPropertyValue ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/physics/DynamicAnimation;->setPropertyValue(F)V
/* .line 440 */
/* .line 442 */
} // :cond_0
/* sub-long v0, p1, v0 */
/* .line 443 */
/* .local v0, "deltaT":J */
/* iput-wide p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mLastFrameTime:J */
/* .line 444 */
v2 = (( miui.android.animation.physics.DynamicAnimation ) p0 ).updateValueAndVelocity ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/animation/physics/DynamicAnimation;->updateValueAndVelocity(J)Z
/* .line 446 */
/* .local v2, "finished":Z */
/* iget v4, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* iget v5, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F */
v4 = java.lang.Math .min ( v4,v5 );
/* iput v4, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* .line 447 */
/* iget v5, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F */
v4 = java.lang.Math .max ( v4,v5 );
/* iput v4, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* .line 449 */
(( miui.android.animation.physics.DynamicAnimation ) p0 ).setPropertyValue ( v4 ); // invoke-virtual {p0, v4}, Lmiui/android/animation/physics/DynamicAnimation;->setPropertyValue(F)V
/* .line 451 */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 452 */
/* invoke-direct {p0, v3}, Lmiui/android/animation/physics/DynamicAnimation;->endAnimationInternal(Z)V */
/* .line 454 */
} // :cond_1
} // .end method
abstract Float getAcceleration ( Float p0, Float p1 ) {
} // .end method
public Float getMinimumVisibleChange ( ) {
/* .locals 1 */
/* .line 337 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
} // .end method
Float getValueThreshold ( ) {
/* .locals 2 */
/* .line 500 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iget v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* const/high16 v1, 0x3f400000 # 0.75f */
/* mul-float/2addr v0, v1 */
} // .end method
abstract Boolean isAtEquilibrium ( Float p0, Float p1 ) {
} // .end method
public Boolean isRunning ( ) {
/* .locals 1 */
/* .line 403 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
} // .end method
public void removeEndListener ( miui.android.animation.physics.DynamicAnimation$OnAnimationEndListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationEndListener; */
/* .line 255 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
v0 = this.mEndListeners;
miui.android.animation.physics.DynamicAnimation .removeEntry ( v0,p1 );
/* .line 256 */
return;
} // .end method
public void removeUpdateListener ( miui.android.animation.physics.DynamicAnimation$OnAnimationUpdateListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener; */
/* .line 290 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
v0 = this.mUpdateListeners;
miui.android.animation.physics.DynamicAnimation .removeEntry ( v0,p1 );
/* .line 291 */
return;
} // .end method
public miui.android.animation.physics.DynamicAnimation setMaxValue ( Float p0 ) {
/* .locals 0 */
/* .param p1, "max" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 203 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMaxValue:F */
/* .line 204 */
} // .end method
public miui.android.animation.physics.DynamicAnimation setMinValue ( Float p0 ) {
/* .locals 0 */
/* .param p1, "min" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 216 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinValue:F */
/* .line 217 */
} // .end method
public miui.android.animation.physics.DynamicAnimation setMinimumVisibleChange ( Float p0 ) {
/* .locals 2 */
/* .param p1, "minimumVisibleChange" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 322 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* cmpg-float v0, p1, v0 */
/* if-lez v0, :cond_0 */
/* .line 325 */
/* iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mMinVisibleChange:F */
/* .line 326 */
/* const/high16 v0, 0x3f400000 # 0.75f */
/* mul-float/2addr v0, p1 */
(( miui.android.animation.physics.DynamicAnimation ) p0 ).setValueThreshold ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/animation/physics/DynamicAnimation;->setValueThreshold(F)V
/* .line 327 */
/* .line 323 */
} // :cond_0
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
final String v1 = "Minimum visible change must be positive."; // const-string v1, "Minimum visible change must be positive."
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
void setPropertyValue ( Float p0 ) {
/* .locals 4 */
/* .param p1, "value" # F */
/* .line 487 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
v0 = this.mProperty;
v1 = this.mTarget;
(( miui.android.animation.property.FloatProperty ) v0 ).setValue ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V
/* .line 488 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mUpdateListeners;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v1, :cond_1 */
/* .line 489 */
v1 = this.mUpdateListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 490 */
v1 = this.mUpdateListeners;
(( java.util.ArrayList ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Lmiui/android/animation/physics/DynamicAnimation$OnAnimationUpdateListener; */
/* iget v2, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* iget v3, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F */
/* .line 488 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 493 */
} // .end local v0 # "i":I
} // :cond_1
v0 = this.mUpdateListeners;
miui.android.animation.physics.DynamicAnimation .removeNullEntries ( v0 );
/* .line 494 */
return;
} // .end method
public void setStartDelay ( Long p0 ) {
/* .locals 2 */
/* .param p1, "startDelay" # J */
/* .line 229 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p1, v0 */
/* if-gez v0, :cond_0 */
/* .line 230 */
/* const-wide/16 p1, 0x0 */
/* .line 232 */
} // :cond_0
/* iput-wide p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartDelay:J */
/* .line 233 */
return;
} // .end method
public miui.android.animation.physics.DynamicAnimation setStartValue ( Float p0 ) {
/* .locals 1 */
/* .param p1, "startValue" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 162 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mValue:F */
/* .line 163 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mStartValueIsSet:Z */
/* .line 164 */
} // .end method
public miui.android.animation.physics.DynamicAnimation setStartVelocity ( Float p0 ) {
/* .locals 0 */
/* .param p1, "startVelocity" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(F)TT;" */
/* } */
} // .end annotation
/* .line 186 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
/* iput p1, p0, Lmiui/android/animation/physics/DynamicAnimation;->mVelocity:F */
/* .line 187 */
} // .end method
abstract void setValueThreshold ( Float p0 ) {
} // .end method
public void start ( ) {
/* .locals 2 */
/* .line 374 */
/* .local p0, "this":Lmiui/android/animation/physics/DynamicAnimation;, "Lmiui/android/animation/physics/DynamicAnimation<TT;>;" */
android.os.Looper .myLooper ( );
android.os.Looper .getMainLooper ( );
/* if-ne v0, v1, :cond_1 */
/* .line 377 */
/* iget-boolean v0, p0, Lmiui/android/animation/physics/DynamicAnimation;->mRunning:Z */
/* if-nez v0, :cond_0 */
/* .line 378 */
/* invoke-direct {p0}, Lmiui/android/animation/physics/DynamicAnimation;->startAnimationInternal()V */
/* .line 380 */
} // :cond_0
return;
/* .line 375 */
} // :cond_1
/* new-instance v0, Landroid/util/AndroidRuntimeException; */
final String v1 = "Animations may only be started on the main thread"; // const-string v1, "Animations may only be started on the main thread"
/* invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
abstract Boolean updateValueAndVelocity ( Long p0 ) {
} // .end method
