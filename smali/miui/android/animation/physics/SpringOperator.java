public class miui.android.animation.physics.SpringOperator implements miui.android.animation.physics.PhysicsOperator {
	 /* .source "SpringOperator.java" */
	 /* # interfaces */
	 /* # instance fields */
	 params;
	 /* # direct methods */
	 public miui.android.animation.physics.SpringOperator ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public miui.android.animation.physics.SpringOperator ( ) {
		 /* .locals 3 */
		 /* .param p1, "damp" # F */
		 /* .param p2, "response" # F */
		 /* .annotation runtime Ljava/lang/Deprecated; */
	 } // .end annotation
	 /* .line 10 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 11 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* new-array v1, v0, [D */
	 this.params = v1;
	 /* .line 12 */
	 /* new-array v0, v0, [F */
	 int v2 = 0; // const/4 v2, 0x0
	 /* aput p1, v0, v2 */
	 int v2 = 1; // const/4 v2, 0x1
	 /* aput p2, v0, v2 */
	 (( miui.android.animation.physics.SpringOperator ) p0 ).getParameters ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/animation/physics/SpringOperator;->getParameters([F[D)V
	 /* .line 13 */
	 return;
} // .end method
/* # virtual methods */
public void getParameters ( Float[] p0, Double[] p1 ) {
	 /* .locals 10 */
	 /* .param p1, "factors" # [F */
	 /* .param p2, "params" # [D */
	 /* .line 29 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* aget v1, p1, v0 */
	 /* float-to-double v1, v1 */
	 /* .line 30 */
	 /* .local v1, "damp":D */
	 int v3 = 1; // const/4 v3, 0x1
	 /* aget v4, p1, v3 */
	 /* float-to-double v4, v4 */
	 /* .line 31 */
	 /* .local v4, "response":D */
	 /* const-wide v6, 0x401921fb54442d18L # 6.283185307179586 */
	 /* div-double/2addr v6, v4 */
	 /* const-wide/high16 v8, 0x4000000000000000L # 2.0 */
	 java.lang.Math .pow ( v6,v7,v8,v9 );
	 /* move-result-wide v6 */
	 /* aput-wide v6, p2, v0 */
	 /* .line 33 */
	 /* const-wide v6, 0x402921fb54442d18L # 12.566370614359172 */
	 /* mul-double/2addr v6, v1 */
	 /* div-double/2addr v6, v4 */
	 /* const-wide/high16 v8, 0x404e000000000000L # 60.0 */
	 java.lang.Math .min ( v6,v7,v8,v9 );
	 /* move-result-wide v6 */
	 /* aput-wide v6, p2, v3 */
	 /* .line 34 */
	 return;
} // .end method
public Double updateVelocity ( Double p0, Double p1, Double p2, Double p3, Double...p4 ) {
	 /* .locals 8 */
	 /* .param p1, "velocity" # D */
	 /* .param p3, "p0" # D */
	 /* .param p5, "p1" # D */
	 /* .param p7, "deltaT" # D */
	 /* .param p9, "factors" # [D */
	 /* .line 39 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* aget-wide v0, p9, v0 */
	 /* .line 40 */
	 /* .local v0, "targetValue":D */
	 int v2 = 1; // const/4 v2, 0x1
	 /* aget-wide v2, p9, v2 */
	 /* .line 41 */
	 /* .local v2, "curValue":D */
	 /* const-wide/high16 v4, 0x3ff0000000000000L # 1.0 */
	 /* mul-double v6, p5, p7 */
	 /* sub-double/2addr v4, v6 */
	 /* mul-double/2addr v4, p1 */
	 /* .line 42 */
} // .end local p1 # "velocity":D
/* .local v4, "velocity":D */
/* sub-double v6, v0, v2 */
/* mul-double/2addr v6, p3 */
/* mul-double/2addr v6, p7 */
/* double-to-float v6, v6 */
/* float-to-double v6, v6 */
/* add-double/2addr v6, v4 */
/* return-wide v6 */
} // .end method
public Double updateVelocity ( Double p0, Float p1, Float...p2 ) {
/* .locals 11 */
/* .param p1, "velocity" # D */
/* .param p3, "deltaT" # F */
/* .param p4, "factors" # [F */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 17 */
v0 = this.params;
/* if-nez v0, :cond_0 */
/* .line 18 */
/* return-wide p1 */
/* .line 20 */
} // :cond_0
/* array-length v0, p4 */
/* new-array v0, v0, [D */
/* .line 21 */
/* .local v0, "doubleFactors":[D */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, p4 */
/* if-ge v1, v2, :cond_1 */
/* .line 22 */
/* aget v2, p4, v1 */
/* float-to-double v2, v2 */
/* aput-wide v2, v0, v1 */
/* .line 21 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 24 */
} // .end local v1 # "i":I
} // :cond_1
v1 = this.params;
int v2 = 0; // const/4 v2, 0x0
/* aget-wide v4, v1, v2 */
int v2 = 1; // const/4 v2, 0x1
/* aget-wide v6, v1, v2 */
/* float-to-double v8, p3 */
/* move-object v1, p0 */
/* move-wide v2, p1 */
/* move-object v10, v0 */
/* invoke-virtual/range {v1 ..v10}, Lmiui/android/animation/physics/SpringOperator;->updateVelocity(DDDD[D)D */
/* move-result-wide v1 */
/* return-wide v1 */
} // .end method
