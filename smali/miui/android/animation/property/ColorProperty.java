public class miui.android.animation.property.ColorProperty extends miui.android.animation.property.FloatProperty implements miui.android.animation.property.IIntValueProperty {
	 /* .source "ColorProperty.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Lmiui/android/animation/property/FloatProperty<", */
	 /* "TT;>;", */
	 /* "Lmiui/android/animation/property/IIntValueProperty<", */
	 /* "TT;>;" */
	 /* } */
} // .end annotation
/* # instance fields */
private Integer mColorValue;
/* # direct methods */
public miui.android.animation.property.ColorProperty ( ) {
	 /* .locals 0 */
	 /* .param p1, "name" # Ljava/lang/String; */
	 /* .line 10 */
	 /* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
	 /* invoke-direct {p0, p1}, Lmiui/android/animation/property/FloatProperty;-><init>(Ljava/lang/String;)V */
	 /* .line 11 */
	 return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
	 /* .locals 3 */
	 /* .param p1, "o" # Ljava/lang/Object; */
	 /* .line 43 */
	 /* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
	 /* if-ne p0, p1, :cond_0 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* .line 44 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_2
	 (( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 (( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
	 /* if-eq v0, v1, :cond_1 */
	 /* .line 45 */
} // :cond_1
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ColorProperty; */
/* .line 46 */
/* .local v0, "that":Lmiui/android/animation/property/ColorProperty; */
v1 = this.mPropertyName;
v2 = this.mPropertyName;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 44 */
} // .end local v0 # "that":Lmiui/android/animation/property/ColorProperty;
} // :cond_2
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getIntValue ( java.lang.Object p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)I" */
/* } */
} // .end annotation
/* .line 34 */
/* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
/* .local p1, "o":Ljava/lang/Object;, "TT;" */
/* instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 35 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
/* .line 36 */
/* .local v0, "obj":Lmiui/android/animation/property/ValueTargetObject; */
(( miui.android.animation.property.ColorProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ColorProperty;->getName()Ljava/lang/String;
v2 = java.lang.Integer.TYPE;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getPropertyValue ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* iput v1, p0, Lmiui/android/animation/property/ColorProperty;->mColorValue:I */
/* .line 38 */
} // .end local v0 # "obj":Lmiui/android/animation/property/ValueTargetObject;
} // :cond_0
/* iget v0, p0, Lmiui/android/animation/property/ColorProperty;->mColorValue:I */
} // .end method
public Float getValue ( java.lang.Object p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)F" */
/* } */
} // .end annotation
/* .line 20 */
/* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
/* .local p1, "o":Ljava/lang/Object;, "TT;" */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer hashCode ( ) {
/* .locals 1 */
/* .line 51 */
/* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
v0 = this.mPropertyName;
/* filled-new-array {v0}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
public void setIntValue ( java.lang.Object p0, Integer p1 ) {
/* .locals 4 */
/* .param p2, "i" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;I)V" */
/* } */
} // .end annotation
/* .line 25 */
/* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
/* .local p1, "o":Ljava/lang/Object;, "TT;" */
/* iput p2, p0, Lmiui/android/animation/property/ColorProperty;->mColorValue:I */
/* .line 26 */
/* instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 27 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
/* .line 28 */
/* .local v0, "obj":Lmiui/android/animation/property/ValueTargetObject; */
(( miui.android.animation.property.ColorProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ColorProperty;->getName()Ljava/lang/String;
v2 = java.lang.Integer.TYPE;
java.lang.Integer .valueOf ( p2 );
(( miui.android.animation.property.ValueTargetObject ) v0 ).setPropertyValue ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 30 */
} // .end local v0 # "obj":Lmiui/android/animation/property/ValueTargetObject;
} // :cond_0
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) {
/* .locals 0 */
/* .param p2, "value" # F */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;F)V" */
/* } */
} // .end annotation
/* .line 16 */
/* .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;" */
/* .local p1, "object":Ljava/lang/Object;, "TT;" */
return;
} // .end method
