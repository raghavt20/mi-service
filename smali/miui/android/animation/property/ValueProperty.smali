.class public Lmiui/android/animation/property/ValueProperty;
.super Lmiui/android/animation/property/FloatProperty;
.source "ValueProperty.java"


# instance fields
.field private volatile mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 10
    invoke-direct {p0, p1}, Lmiui/android/animation/property/FloatProperty;-><init>(Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .line 42
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    return v0

    .line 43
    :cond_0
    if-eqz p1, :cond_2

    const-class v0, Lmiui/android/animation/property/ValueProperty;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 44
    :cond_1
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueProperty;

    .line 45
    .local v0, "that":Lmiui/android/animation/property/ValueProperty;
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 43
    .end local v0    # "that":Lmiui/android/animation/property/ValueProperty;
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lmiui/android/animation/property/ValueProperty;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiui/android/animation/property/ValueProperty;->mName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getValue(Ljava/lang/Object;)F
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .line 24
    instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject;

    if-eqz v0, :cond_0

    .line 25
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 26
    .local v0, "value":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    return v1

    .line 30
    .end local v0    # "value":Ljava/lang/Float;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 50
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lmiui/android/animation/property/ValueProperty;->mName:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setValue(Ljava/lang/Object;F)V
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "v"    # F

    .line 35
    instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject;

    if-eqz v0, :cond_0

    .line 36
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 38
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ValueProperty{name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 56
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    return-object v0
.end method
