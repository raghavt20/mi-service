class inal extends miui.android.animation.property.ViewProperty {
	 /* .source "ViewProperty.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/property/ViewProperty; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = null */
} // .end annotation
/* # direct methods */
 inal ( ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 160 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V */
return;
} // .end method
/* # virtual methods */
public Float getValue ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 168 */
v0 = (( android.view.View ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/View;->getX()F
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 160 */
/* check-cast p1, Landroid/view/View; */
p1 = (( miui.android.animation.property.ViewProperty$9 ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewProperty$9;->getValue(Landroid/view/View;)F
} // .end method
public void setValue ( android.view.View p0, Float p1 ) {
/* .locals 0 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "value" # F */
/* .line 163 */
(( android.view.View ) p1 ).setX ( p2 ); // invoke-virtual {p1, p2}, Landroid/view/View;->setX(F)V
/* .line 164 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 160 */
/* check-cast p1, Landroid/view/View; */
(( miui.android.animation.property.ViewProperty$9 ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewProperty$9;->setValue(Landroid/view/View;F)V
return;
} // .end method
