public abstract class miui.android.animation.property.IIntValueProperty {
	 /* .source "IIntValueProperty.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Ljava/lang/Object;" */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract Integer getIntValue ( java.lang.Object p0 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(TT;)I" */
	 /* } */
} // .end annotation
} // .end method
public abstract java.lang.String getName ( ) {
} // .end method
public abstract void setIntValue ( java.lang.Object p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;I)V" */
/* } */
} // .end annotation
} // .end method
