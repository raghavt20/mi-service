public class miui.android.animation.property.PropertyType {
	 /* .source "PropertyType.java" */
	 /* # static fields */
	 public static final Integer ALPHA;
	 public static final Integer AUTO_ALPHA;
	 public static final Integer BACKGROUND;
	 public static final Integer FOREGROUND;
	 public static final Integer HEIGHT;
	 public static final Integer INVALID;
	 public static final Integer ROTATION;
	 public static final Integer ROTATION_X;
	 public static final Integer ROTATION_Y;
	 public static final Integer SCALE_X;
	 public static final Integer SCALE_Y;
	 public static final Integer SCROLL_X;
	 public static final Integer SCROLL_Y;
	 public static final Integer TRANSLATION_X;
	 public static final Integer TRANSLATION_Y;
	 public static final Integer TRANSLATION_Z;
	 public static final Integer WIDTH;
	 public static final Integer X;
	 public static final Integer Y;
	 public static final Integer Z;
	 /* # direct methods */
	 private miui.android.animation.property.PropertyType ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
