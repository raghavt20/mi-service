.class public Lmiui/android/animation/property/ValueTargetObject;
.super Ljava/lang/Object;
.source "ValueTargetObject.java"


# instance fields
.field private mFieldManager:Lmiui/android/animation/utils/FieldManager;

.field private mRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mTempObj:Ljava/lang/Object;

.field private mValueMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Lmiui/android/animation/utils/FieldManager;

    invoke-direct {v0}, Lmiui/android/animation/utils/FieldManager;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/property/ValueTargetObject;->mFieldManager:Lmiui/android/animation/utils/FieldManager;

    .line 20
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/property/ValueTargetObject;->mValueMap:Ljava/util/Map;

    .line 23
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lmiui/android/animation/utils/CommonUtils;->isBuiltInClass(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    iput-object p1, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    goto :goto_0

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/property/ValueTargetObject;->mRef:Ljava/lang/ref/WeakReference;

    .line 28
    :goto_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .line 61
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    return v0

    .line 62
    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 63
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_4

    .line 64
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    if-eqz v1, :cond_2

    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 66
    :cond_2
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v1

    .line 67
    .local v1, "refObj":Ljava/lang/Object;
    if-eqz v1, :cond_3

    .line 68
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 71
    .end local v1    # "refObj":Ljava/lang/Object;
    :cond_3
    return v0

    .line 73
    :cond_4
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    .line 74
    .local v0, "that":Lmiui/android/animation/property/ValueTargetObject;
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    if-eqz v1, :cond_5

    .line 75
    iget-object v2, v0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 77
    :cond_5
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .param p1, "propertyName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 39
    .local p2, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    .line 40
    .local v0, "target":Ljava/lang/Object;
    if-eqz v0, :cond_2

    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    if-ne v1, v0, :cond_0

    goto :goto_1

    .line 43
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mValueMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 44
    .local v1, "value":Ljava/lang/Object;, "TT;"
    if-eqz v1, :cond_1

    move-object v2, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lmiui/android/animation/property/ValueTargetObject;->mFieldManager:Lmiui/android/animation/utils/FieldManager;

    invoke-virtual {v2, v0, p1, p2}, Lmiui/android/animation/utils/FieldManager;->getField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    :goto_0
    return-object v2

    .line 41
    .end local v1    # "value":Ljava/lang/Object;, "TT;"
    :cond_2
    :goto_1
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mValueMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getRealObject()Ljava/lang/Object;
    .locals 1

    .line 35
    iget-object v0, p0, Lmiui/android/animation/property/ValueTargetObject;->mRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    :goto_0
    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .line 83
    iget-object v0, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0

    .line 86
    :cond_0
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    .line 87
    .local v0, "obj":Ljava/lang/Object;
    if-eqz v0, :cond_1

    .line 88
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    return v1

    .line 91
    .end local v0    # "obj":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .line 31
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 2
    .param p1, "propertyName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 48
    .local p2, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p3, "value":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v0

    .line 49
    .local v0, "target":Ljava/lang/Object;
    if-eqz v0, :cond_3

    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mTempObj:Ljava/lang/Object;

    if-ne v1, v0, :cond_0

    goto :goto_0

    .line 53
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mValueMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mFieldManager:Lmiui/android/animation/utils/FieldManager;

    .line 54
    invoke-virtual {v1, v0, p1, p2, p3}, Lmiui/android/animation/utils/FieldManager;->setField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 55
    :cond_1
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mValueMap:Ljava/util/Map;

    invoke-interface {v1, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_2
    return-void

    .line 50
    :cond_3
    :goto_0
    iget-object v1, p0, Lmiui/android/animation/property/ValueTargetObject;->mValueMap:Ljava/util/Map;

    invoke-interface {v1, p1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ValueTargetObject{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
