public class miui.android.animation.property.ValueTargetObject {
	 /* .source "ValueTargetObject.java" */
	 /* # instance fields */
	 private miui.android.animation.utils.FieldManager mFieldManager;
	 private java.lang.ref.WeakReference mRef;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/ref/WeakReference<", */
	 /* "Ljava/lang/Object;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.lang.Object mTempObj;
private java.util.Map mValueMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Object;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.property.ValueTargetObject ( ) {
/* .locals 1 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 22 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
/* new-instance v0, Lmiui/android/animation/utils/FieldManager; */
/* invoke-direct {v0}, Lmiui/android/animation/utils/FieldManager;-><init>()V */
this.mFieldManager = v0;
/* .line 20 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.mValueMap = v0;
/* .line 23 */
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
v0 = miui.android.animation.utils.CommonUtils .isBuiltInClass ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 24 */
this.mTempObj = p1;
/* .line 26 */
} // :cond_0
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mRef = v0;
/* .line 28 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 61 */
/* if-ne p0, p1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 62 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_1 */
/* .line 63 */
} // :cond_1
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* if-eq v1, v2, :cond_4 */
/* .line 64 */
v1 = this.mTempObj;
if ( v1 != null) { // if-eqz v1, :cond_2
v0 = java.util.Objects .equals ( v1,p1 );
/* .line 66 */
} // :cond_2
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
/* .line 67 */
/* .local v1, "refObj":Ljava/lang/Object; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 68 */
v0 = java.util.Objects .equals ( v1,p1 );
/* .line 71 */
} // .end local v1 # "refObj":Ljava/lang/Object;
} // :cond_3
/* .line 73 */
} // :cond_4
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
/* .line 74 */
/* .local v0, "that":Lmiui/android/animation/property/ValueTargetObject; */
v1 = this.mTempObj;
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 75 */
v2 = this.mTempObj;
v1 = java.util.Objects .equals ( v1,v2 );
/* .line 77 */
} // :cond_5
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getRealObject ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
v1 = java.util.Objects .equals ( v1,v2 );
} // .end method
public java.lang.Object getPropertyValue ( java.lang.String p0, java.lang.Class p1 ) {
/* .locals 3 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "TT;>;)TT;" */
/* } */
} // .end annotation
/* .line 39 */
/* .local p2, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
/* .line 40 */
/* .local v0, "target":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_2
v1 = this.mTempObj;
/* if-ne v1, v0, :cond_0 */
/* .line 43 */
} // :cond_0
v1 = this.mValueMap;
/* .line 44 */
/* .local v1, "value":Ljava/lang/Object;, "TT;" */
if ( v1 != null) { // if-eqz v1, :cond_1
/* move-object v2, v1 */
} // :cond_1
v2 = this.mFieldManager;
(( miui.android.animation.utils.FieldManager ) v2 ).getField ( v0, p1, p2 ); // invoke-virtual {v2, v0, p1, p2}, Lmiui/android/animation/utils/FieldManager;->getField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
} // :goto_0
/* .line 41 */
} // .end local v1 # "value":Ljava/lang/Object;, "TT;"
} // :cond_2
} // :goto_1
v1 = this.mValueMap;
} // .end method
public java.lang.Object getRealObject ( ) {
/* .locals 1 */
/* .line 35 */
v0 = this.mRef;
if ( v0 != null) { // if-eqz v0, :cond_0
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
} // :cond_0
v0 = this.mTempObj;
} // :goto_0
} // .end method
public Integer hashCode ( ) {
/* .locals 2 */
/* .line 83 */
v0 = this.mTempObj;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 84 */
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
/* .line 86 */
} // :cond_0
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
/* .line 87 */
/* .local v0, "obj":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 88 */
v1 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
/* .line 91 */
} // .end local v0 # "obj":Ljava/lang/Object;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isValid ( ) {
/* .locals 1 */
/* .line 31 */
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void setPropertyValue ( java.lang.String p0, java.lang.Class p1, java.lang.Object p2 ) {
/* .locals 2 */
/* .param p1, "propertyName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Class<", */
/* "TT;>;TT;)V" */
/* } */
} // .end annotation
/* .line 48 */
/* .local p2, "clz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;" */
/* .local p3, "value":Ljava/lang/Object;, "TT;" */
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
/* .line 49 */
/* .local v0, "target":Ljava/lang/Object; */
if ( v0 != null) { // if-eqz v0, :cond_3
v1 = this.mTempObj;
/* if-ne v1, v0, :cond_0 */
/* .line 53 */
} // :cond_0
v1 = v1 = this.mValueMap;
/* if-nez v1, :cond_1 */
v1 = this.mFieldManager;
/* .line 54 */
v1 = (( miui.android.animation.utils.FieldManager ) v1 ).setField ( v0, p1, p2, p3 ); // invoke-virtual {v1, v0, p1, p2, p3}, Lmiui/android/animation/utils/FieldManager;->setField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)Z
/* if-nez v1, :cond_2 */
/* .line 55 */
} // :cond_1
v1 = this.mValueMap;
/* .line 57 */
} // :cond_2
return;
/* .line 50 */
} // :cond_3
} // :goto_0
v1 = this.mValueMap;
/* .line 51 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ValueTargetObject{"; // const-string v1, "ValueTargetObject{"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.android.animation.property.ValueTargetObject ) p0 ).getRealObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueTargetObject;->getRealObject()Ljava/lang/Object;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
