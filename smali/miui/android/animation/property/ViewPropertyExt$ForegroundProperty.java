public class miui.android.animation.property.ViewPropertyExt$ForegroundProperty extends miui.android.animation.property.ViewProperty implements miui.android.animation.property.IIntValueProperty {
	 /* .source "ViewPropertyExt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/property/ViewPropertyExt; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "ForegroundProperty" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lmiui/android/animation/property/ViewProperty;", */
/* "Lmiui/android/animation/property/IIntValueProperty<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
private miui.android.animation.property.ViewPropertyExt$ForegroundProperty ( ) {
/* .locals 1 */
/* .line 17 */
final String v0 = "foreground"; // const-string v0, "foreground"
/* invoke-direct {p0, v0}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V */
/* .line 18 */
return;
} // .end method
 miui.android.animation.property.ViewPropertyExt$ForegroundProperty ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lmiui/android/animation/property/ViewPropertyExt$1; */
/* .line 14 */
/* invoke-direct {p0}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer getIntValue ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 32 */
/* const v0, 0x100b0003 */
(( android.view.View ) p1 ).getTag ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
/* .line 33 */
/* .local v0, "tag":Ljava/lang/Object; */
/* instance-of v1, v0, Ljava/lang/Integer; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Integer getIntValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 14 */
/* check-cast p1, Landroid/view/View; */
p1 = (( miui.android.animation.property.ViewPropertyExt$ForegroundProperty ) p0 ).getIntValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;->getIntValue(Landroid/view/View;)I
} // .end method
public Float getValue ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "object" # Landroid/view/View; */
/* .line 22 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 14 */
/* check-cast p1, Landroid/view/View; */
p1 = (( miui.android.animation.property.ViewPropertyExt$ForegroundProperty ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;->getValue(Landroid/view/View;)F
} // .end method
public void setIntValue ( android.view.View p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "value" # I */
/* .line 38 */
/* const v0, 0x100b0003 */
java.lang.Integer .valueOf ( p2 );
(( android.view.View ) p1 ).setTag ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 39 */
/* nop */
/* .line 40 */
(( android.view.View ) p1 ).getForeground ( ); // invoke-virtual {p1}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;
/* .line 41 */
/* .local v0, "fg":Landroid/graphics/drawable/Drawable; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 42 */
(( android.graphics.drawable.Drawable ) v0 ).invalidateSelf ( ); // invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V
/* .line 45 */
} // .end local v0 # "fg":Landroid/graphics/drawable/Drawable;
} // :cond_0
return;
} // .end method
public void setIntValue ( java.lang.Object p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 14 */
/* check-cast p1, Landroid/view/View; */
(( miui.android.animation.property.ViewPropertyExt$ForegroundProperty ) p0 ).setIntValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;->setIntValue(Landroid/view/View;I)V
return;
} // .end method
public void setValue ( android.view.View p0, Float p1 ) {
/* .locals 0 */
/* .param p1, "object" # Landroid/view/View; */
/* .param p2, "value" # F */
/* .line 28 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 14 */
/* check-cast p1, Landroid/view/View; */
(( miui.android.animation.property.ViewPropertyExt$ForegroundProperty ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;->setValue(Landroid/view/View;F)V
return;
} // .end method
