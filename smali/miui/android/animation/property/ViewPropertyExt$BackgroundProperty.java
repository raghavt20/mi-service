public class miui.android.animation.property.ViewPropertyExt$BackgroundProperty extends miui.android.animation.property.ViewProperty implements miui.android.animation.property.IIntValueProperty {
	 /* .source "ViewPropertyExt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/animation/property/ViewPropertyExt; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "BackgroundProperty" */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lmiui/android/animation/property/ViewProperty;", */
/* "Lmiui/android/animation/property/IIntValueProperty<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
private miui.android.animation.property.ViewPropertyExt$BackgroundProperty ( ) {
/* .locals 1 */
/* .line 51 */
final String v0 = "background"; // const-string v0, "background"
/* invoke-direct {p0, v0}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V */
/* .line 52 */
return;
} // .end method
 miui.android.animation.property.ViewPropertyExt$BackgroundProperty ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Lmiui/android/animation/property/ViewPropertyExt$1; */
/* .line 48 */
/* invoke-direct {p0}, Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Integer getIntValue ( android.view.View p0 ) {
/* .locals 2 */
/* .param p1, "target" # Landroid/view/View; */
/* .line 71 */
(( android.view.View ) p1 ).getBackground ( ); // invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;
/* .line 72 */
/* .local v0, "bg":Landroid/graphics/drawable/Drawable; */
/* instance-of v1, v0, Landroid/graphics/drawable/ColorDrawable; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 73 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/graphics/drawable/ColorDrawable; */
v1 = (( android.graphics.drawable.ColorDrawable ) v1 ).getColor ( ); // invoke-virtual {v1}, Landroid/graphics/drawable/ColorDrawable;->getColor()I
/* .line 75 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Integer getIntValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 48 */
/* check-cast p1, Landroid/view/View; */
p1 = (( miui.android.animation.property.ViewPropertyExt$BackgroundProperty ) p0 ).getIntValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;->getIntValue(Landroid/view/View;)I
} // .end method
public Float getValue ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "object" # Landroid/view/View; */
/* .line 56 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 48 */
/* check-cast p1, Landroid/view/View; */
p1 = (( miui.android.animation.property.ViewPropertyExt$BackgroundProperty ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;->getValue(Landroid/view/View;)F
} // .end method
public void setIntValue ( android.view.View p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "target" # Landroid/view/View; */
/* .param p2, "value" # I */
/* .line 66 */
(( android.view.View ) p1 ).setBackgroundColor ( p2 ); // invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V
/* .line 67 */
return;
} // .end method
public void setIntValue ( java.lang.Object p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 48 */
/* check-cast p1, Landroid/view/View; */
(( miui.android.animation.property.ViewPropertyExt$BackgroundProperty ) p0 ).setIntValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;->setIntValue(Landroid/view/View;I)V
return;
} // .end method
public void setValue ( android.view.View p0, Float p1 ) {
/* .locals 0 */
/* .param p1, "object" # Landroid/view/View; */
/* .param p2, "value" # F */
/* .line 62 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 48 */
/* check-cast p1, Landroid/view/View; */
(( miui.android.animation.property.ViewPropertyExt$BackgroundProperty ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;->setValue(Landroid/view/View;F)V
return;
} // .end method
