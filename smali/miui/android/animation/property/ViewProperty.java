public abstract class miui.android.animation.property.ViewProperty extends miui.android.animation.property.FloatProperty {
	 /* .source "ViewProperty.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Lmiui/android/animation/property/FloatProperty<", */
	 /* "Landroid/view/View;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* # static fields */
public static final miui.android.animation.property.ViewProperty ALPHA;
public static final miui.android.animation.property.ViewProperty AUTO_ALPHA;
public static final miui.android.animation.property.ViewProperty BACKGROUND;
public static final miui.android.animation.property.ViewProperty FOREGROUND;
public static final miui.android.animation.property.ViewProperty HEIGHT;
public static final miui.android.animation.property.ViewProperty ROTATION;
public static final miui.android.animation.property.ViewProperty ROTATION_X;
public static final miui.android.animation.property.ViewProperty ROTATION_Y;
public static final miui.android.animation.property.ViewProperty SCALE_X;
public static final miui.android.animation.property.ViewProperty SCALE_Y;
public static final miui.android.animation.property.ViewProperty SCROLL_X;
public static final miui.android.animation.property.ViewProperty SCROLL_Y;
public static final miui.android.animation.property.ViewProperty TRANSLATION_X;
public static final miui.android.animation.property.ViewProperty TRANSLATION_Y;
public static final miui.android.animation.property.ViewProperty TRANSLATION_Z;
public static final miui.android.animation.property.ViewProperty WIDTH;
public static final miui.android.animation.property.ViewProperty X;
public static final miui.android.animation.property.ViewProperty Y;
public static final miui.android.animation.property.ViewProperty Z;
/* # direct methods */
static miui.android.animation.property.ViewProperty ( ) {
	 /* .locals 2 */
	 /* .line 35 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$1; */
	 /* const-string/jumbo v1, "translationX" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$1;-><init>(Ljava/lang/String;)V */
	 /* .line 50 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$2; */
	 /* const-string/jumbo v1, "translationY" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$2;-><init>(Ljava/lang/String;)V */
	 /* .line 65 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$3; */
	 /* const-string/jumbo v1, "translationZ" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$3;-><init>(Ljava/lang/String;)V */
	 /* .line 85 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$4; */
	 final String v1 = "scaleX"; // const-string v1, "scaleX"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$4;-><init>(Ljava/lang/String;)V */
	 /* .line 100 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$5; */
	 final String v1 = "scaleY"; // const-string v1, "scaleY"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$5;-><init>(Ljava/lang/String;)V */
	 /* .line 115 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$6; */
	 final String v1 = "rotation"; // const-string v1, "rotation"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$6;-><init>(Ljava/lang/String;)V */
	 /* .line 130 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$7; */
	 final String v1 = "rotationX"; // const-string v1, "rotationX"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$7;-><init>(Ljava/lang/String;)V */
	 /* .line 145 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$8; */
	 final String v1 = "rotationY"; // const-string v1, "rotationY"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$8;-><init>(Ljava/lang/String;)V */
	 /* .line 160 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$9; */
	 /* const-string/jumbo v1, "x" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$9;-><init>(Ljava/lang/String;)V */
	 /* .line 175 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$10; */
	 /* const-string/jumbo v1, "y" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$10;-><init>(Ljava/lang/String;)V */
	 /* .line 190 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$11; */
	 /* const-string/jumbo v1, "z" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$11;-><init>(Ljava/lang/String;)V */
	 /* .line 210 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$12; */
	 final String v1 = "height"; // const-string v1, "height"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$12;-><init>(Ljava/lang/String;)V */
	 /* .line 236 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$13; */
	 /* const-string/jumbo v1, "width" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$13;-><init>(Ljava/lang/String;)V */
	 /* .line 262 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$14; */
	 final String v1 = "alpha"; // const-string v1, "alpha"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$14;-><init>(Ljava/lang/String;)V */
	 /* .line 277 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$15; */
	 final String v1 = "autoAlpha"; // const-string v1, "autoAlpha"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$15;-><init>(Ljava/lang/String;)V */
	 /* .line 299 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$16; */
	 /* const-string/jumbo v1, "scrollX" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$16;-><init>(Ljava/lang/String;)V */
	 /* .line 314 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$17; */
	 /* const-string/jumbo v1, "scrollY" */
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$17;-><init>(Ljava/lang/String;)V */
	 /* .line 331 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$18; */
	 final String v1 = "deprecated_foreground"; // const-string v1, "deprecated_foreground"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$18;-><init>(Ljava/lang/String;)V */
	 /* .line 348 */
	 /* new-instance v0, Lmiui/android/animation/property/ViewProperty$19; */
	 final String v1 = "deprecated_background"; // const-string v1, "deprecated_background"
	 /* invoke-direct {v0, v1}, Lmiui/android/animation/property/ViewProperty$19;-><init>(Ljava/lang/String;)V */
	 return;
} // .end method
public miui.android.animation.property.ViewProperty ( ) {
	 /* .locals 0 */
	 /* .param p1, "name" # Ljava/lang/String; */
	 /* .line 22 */
	 /* invoke-direct {p0, p1}, Lmiui/android/animation/property/FloatProperty;-><init>(Ljava/lang/String;)V */
	 /* .line 23 */
	 return;
} // .end method
static Boolean access$000 ( android.view.View p0 ) { //synthethic
	 /* .locals 1 */
	 /* .param p0, "x0" # Landroid/view/View; */
	 /* .line 19 */
	 v0 = 	 miui.android.animation.property.ViewProperty .isInInitLayout ( p0 );
} // .end method
private static Boolean isInInitLayout ( android.view.View p0 ) {
	 /* .locals 1 */
	 /* .param p0, "view" # Landroid/view/View; */
	 /* .line 361 */
	 /* const v0, 0x100b0004 */
	 (( android.view.View ) p0 ).getTag ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 27 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ViewProperty{mPropertyName=\'"; // const-string v1, "ViewProperty{mPropertyName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPropertyName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
