.class public Lmiui/android/animation/property/ColorProperty;
.super Lmiui/android/animation/property/FloatProperty;
.source "ColorProperty.java"

# interfaces
.implements Lmiui/android/animation/property/IIntValueProperty;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lmiui/android/animation/property/FloatProperty<",
        "TT;>;",
        "Lmiui/android/animation/property/IIntValueProperty<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private mColorValue:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 10
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    invoke-direct {p0, p1}, Lmiui/android/animation/property/FloatProperty;-><init>(Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .line 43
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    return v0

    .line 44
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 45
    :cond_1
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ColorProperty;

    .line 46
    .local v0, "that":Lmiui/android/animation/property/ColorProperty;
    iget-object v1, p0, Lmiui/android/animation/property/ColorProperty;->mPropertyName:Ljava/lang/String;

    iget-object v2, v0, Lmiui/android/animation/property/ColorProperty;->mPropertyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 44
    .end local v0    # "that":Lmiui/android/animation/property/ColorProperty;
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getIntValue(Ljava/lang/Object;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .line 34
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    .local p1, "o":Ljava/lang/Object;, "TT;"
    instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject;

    if-eqz v0, :cond_0

    .line 35
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    .line 36
    .local v0, "obj":Lmiui/android/animation/property/ValueTargetObject;
    invoke-virtual {p0}, Lmiui/android/animation/property/ColorProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lmiui/android/animation/property/ColorProperty;->mColorValue:I

    .line 38
    .end local v0    # "obj":Lmiui/android/animation/property/ValueTargetObject;
    :cond_0
    iget v0, p0, Lmiui/android/animation/property/ColorProperty;->mColorValue:I

    return v0
.end method

.method public getValue(Ljava/lang/Object;)F
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)F"
        }
    .end annotation

    .line 20
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    .local p1, "o":Ljava/lang/Object;, "TT;"
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 51
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/property/ColorProperty;->mPropertyName:Ljava/lang/String;

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setIntValue(Ljava/lang/Object;I)V
    .locals 4
    .param p2, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .line 25
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    .local p1, "o":Ljava/lang/Object;, "TT;"
    iput p2, p0, Lmiui/android/animation/property/ColorProperty;->mColorValue:I

    .line 26
    instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject;

    if-eqz v0, :cond_0

    .line 27
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    .line 28
    .local v0, "obj":Lmiui/android/animation/property/ValueTargetObject;
    invoke-virtual {p0}, Lmiui/android/animation/property/ColorProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 30
    .end local v0    # "obj":Lmiui/android/animation/property/ValueTargetObject;
    :cond_0
    return-void
.end method

.method public setValue(Ljava/lang/Object;F)V
    .locals 0
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;F)V"
        }
    .end annotation

    .line 16
    .local p0, "this":Lmiui/android/animation/property/ColorProperty;, "Lmiui/android/animation/property/ColorProperty<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TT;"
    return-void
.end method
