.class final Lmiui/android/animation/property/ViewProperty$13;
.super Lmiui/android/animation/property/ViewProperty;
.source "ViewProperty.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/property/ViewProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 236
    invoke-direct {p0, p1}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getValue(Landroid/view/View;)F
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .line 247
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 248
    .local v0, "width":I
    const v1, 0x100b0006

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    .line 249
    .local v1, "value":Ljava/lang/Float;
    if-eqz v1, :cond_0

    .line 250
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    return v2

    .line 252
    :cond_0
    if-nez v0, :cond_1

    invoke-static {p1}, Lmiui/android/animation/property/ViewProperty;->access$000(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 253
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 255
    :cond_1
    int-to-float v2, v0

    return v2
.end method

.method public bridge synthetic getValue(Ljava/lang/Object;)F
    .locals 0

    .line 236
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewProperty$13;->getValue(Landroid/view/View;)F

    move-result p1

    return p1
.end method

.method public setValue(Landroid/view/View;F)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "value"    # F

    .line 240
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    float-to-int v1, p2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 241
    const v0, 0x100b0006

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 242
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    .line 243
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;F)V
    .locals 0

    .line 236
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewProperty$13;->setValue(Landroid/view/View;F)V

    return-void
.end method
