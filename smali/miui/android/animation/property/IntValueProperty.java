public class miui.android.animation.property.IntValueProperty extends miui.android.animation.property.ValueProperty implements miui.android.animation.property.IIntValueProperty {
	 /* .source "IntValueProperty.java" */
	 /* # interfaces */
	 /* # direct methods */
	 public miui.android.animation.property.IntValueProperty ( ) {
		 /* .locals 0 */
		 /* .param p1, "name" # Ljava/lang/String; */
		 /* .line 6 */
		 /* invoke-direct {p0, p1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V */
		 /* .line 7 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getIntValue ( java.lang.Object p0 ) {
		 /* .locals 3 */
		 /* .param p1, "o" # Ljava/lang/Object; */
		 /* .line 18 */
		 /* instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 19 */
			 /* move-object v0, p1 */
			 /* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
			 (( miui.android.animation.property.IntValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/IntValueProperty;->getName()Ljava/lang/String;
			 v2 = java.lang.Integer.TYPE;
			 (( miui.android.animation.property.ValueTargetObject ) v0 ).getPropertyValue ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
			 /* check-cast v0, Ljava/lang/Integer; */
			 /* .line 20 */
			 /* .local v0, "value":Ljava/lang/Integer; */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 21 */
				 v1 = 				 (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
				 /* .line 24 */
			 } // .end local v0 # "value":Ljava/lang/Integer;
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
	 } // .end method
	 public void setIntValue ( java.lang.Object p0, Integer p1 ) {
		 /* .locals 4 */
		 /* .param p1, "o" # Ljava/lang/Object; */
		 /* .param p2, "i" # I */
		 /* .line 11 */
		 /* instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject; */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 12 */
			 /* move-object v0, p1 */
			 /* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
			 (( miui.android.animation.property.IntValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/IntValueProperty;->getName()Ljava/lang/String;
			 v2 = java.lang.Integer.TYPE;
			 java.lang.Integer .valueOf ( p2 );
			 (( miui.android.animation.property.ValueTargetObject ) v0 ).setPropertyValue ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
			 /* .line 14 */
		 } // :cond_0
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 29 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "IntValueProperty{name="; // const-string v1, "IntValueProperty{name="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 30 */
		 (( miui.android.animation.property.IntValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/IntValueProperty;->getName()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 29 */
	 } // .end method
