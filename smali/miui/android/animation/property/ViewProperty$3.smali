.class final Lmiui/android/animation/property/ViewProperty$3;
.super Lmiui/android/animation/property/ViewProperty;
.source "ViewProperty.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/property/ViewProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getValue(Landroid/view/View;)F
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .line 75
    nop

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getTranslationZ()F

    move-result v0

    return v0
.end method

.method public bridge synthetic getValue(Ljava/lang/Object;)F
    .locals 0

    .line 65
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lmiui/android/animation/property/ViewProperty$3;->getValue(Landroid/view/View;)F

    move-result p1

    return p1
.end method

.method public setValue(Landroid/view/View;F)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "value"    # F

    .line 68
    nop

    .line 69
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationZ(F)V

    .line 71
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;F)V
    .locals 0

    .line 65
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/ViewProperty$3;->setValue(Landroid/view/View;F)V

    return-void
.end method
