public class miui.android.animation.property.ValueProperty extends miui.android.animation.property.FloatProperty {
	 /* .source "ValueProperty.java" */
	 /* # instance fields */
	 private volatile java.lang.String mName;
	 /* # direct methods */
	 public miui.android.animation.property.ValueProperty ( ) {
		 /* .locals 0 */
		 /* .param p1, "name" # Ljava/lang/String; */
		 /* .line 10 */
		 /* invoke-direct {p0, p1}, Lmiui/android/animation/property/FloatProperty;-><init>(Ljava/lang/String;)V */
		 /* .line 11 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean equals ( java.lang.Object p0 ) {
		 /* .locals 3 */
		 /* .param p1, "o" # Ljava/lang/Object; */
		 /* .line 42 */
		 /* if-ne p0, p1, :cond_0 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 43 */
	 } // :cond_0
	 if ( p1 != null) { // if-eqz p1, :cond_2
		 /* const-class v0, Lmiui/android/animation/property/ValueProperty; */
		 (( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
		 v0 = 		 (( java.lang.Class ) v0 ).isAssignableFrom ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
		 /* if-nez v0, :cond_1 */
		 /* .line 44 */
	 } // :cond_1
	 /* move-object v0, p1 */
	 /* check-cast v0, Lmiui/android/animation/property/ValueProperty; */
	 /* .line 45 */
	 /* .local v0, "that":Lmiui/android/animation/property/ValueProperty; */
	 (( miui.android.animation.property.ValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;
	 (( miui.android.animation.property.ValueProperty ) v0 ).getName ( ); // invoke-virtual {v0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;
	 v1 = 	 java.util.Objects .equals ( v1,v2 );
	 /* .line 43 */
} // .end local v0 # "that":Lmiui/android/animation/property/ValueProperty;
} // :cond_2
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 15 */
v0 = this.mName;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mName;
} // :cond_0
/* invoke-super {p0}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String; */
} // :goto_0
} // .end method
public Float getValue ( java.lang.Object p0 ) {
/* .locals 3 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 24 */
/* instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 25 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
(( miui.android.animation.property.ValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;
v2 = java.lang.Float.TYPE;
(( miui.android.animation.property.ValueTargetObject ) v0 ).getPropertyValue ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Float; */
/* .line 26 */
/* .local v0, "value":Ljava/lang/Float; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 27 */
v1 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 30 */
} // .end local v0 # "value":Ljava/lang/Float;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer hashCode ( ) {
/* .locals 1 */
/* .line 50 */
(( miui.android.animation.property.ValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;
/* filled-new-array {v0}, [Ljava/lang/Object; */
v0 = java.util.Objects .hash ( v0 );
} // .end method
public void setName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 19 */
this.mName = p1;
/* .line 20 */
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) {
/* .locals 4 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .param p2, "v" # F */
/* .line 35 */
/* instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 36 */
/* move-object v0, p1 */
/* check-cast v0, Lmiui/android/animation/property/ValueTargetObject; */
(( miui.android.animation.property.ValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;
v2 = java.lang.Float.TYPE;
java.lang.Float .valueOf ( p2 );
(( miui.android.animation.property.ValueTargetObject ) v0 ).setPropertyValue ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V
/* .line 38 */
} // :cond_0
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 55 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ValueProperty{name="; // const-string v1, "ValueProperty{name="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 56 */
(( miui.android.animation.property.ValueProperty ) p0 ).getName ( ); // invoke-virtual {p0}, Lmiui/android/animation/property/ValueProperty;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 55 */
} // .end method
