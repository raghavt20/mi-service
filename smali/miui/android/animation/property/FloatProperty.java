public abstract class miui.android.animation.property.FloatProperty extends android.util.Property {
	 /* .source "FloatProperty.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "<T:", */
	 /* "Ljava/lang/Object;", */
	 /* ">", */
	 /* "Landroid/util/Property<", */
	 /* "TT;", */
	 /* "Ljava/lang/Float;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* # instance fields */
final java.lang.String mPropertyName;
/* # direct methods */
public miui.android.animation.property.FloatProperty ( ) {
	 /* .locals 1 */
	 /* .param p1, "name" # Ljava/lang/String; */
	 /* .line 36 */
	 /* .local p0, "this":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TT;>;" */
	 /* const-class v0, Ljava/lang/Float; */
	 /* invoke-direct {p0, v0, p1}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V */
	 /* .line 37 */
	 this.mPropertyName = p1;
	 /* .line 38 */
	 return;
} // .end method
/* # virtual methods */
public java.lang.Float get ( java.lang.Object p0 ) {
	 /* .locals 1 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(TT;)", */
	 /* "Ljava/lang/Float;" */
	 /* } */
} // .end annotation
/* .line 42 */
/* .local p0, "this":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TT;>;" */
/* .local p1, "object":Ljava/lang/Object;, "TT;" */
/* if-nez p1, :cond_0 */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
java.lang.Float .valueOf ( v0 );
/* .line 45 */
} // :cond_0
v0 = (( miui.android.animation.property.FloatProperty ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F
java.lang.Float .valueOf ( v0 );
} // .end method
public java.lang.Object get ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 29 */
/* .local p0, "this":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TT;>;" */
(( miui.android.animation.property.FloatProperty ) p0 ).get ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/property/FloatProperty;->get(Ljava/lang/Object;)Ljava/lang/Float;
} // .end method
public abstract Float getValue ( java.lang.Object p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;)F" */
/* } */
} // .end annotation
} // .end method
public final void set ( java.lang.Object p0, java.lang.Float p1 ) {
/* .locals 1 */
/* .param p2, "value" # Ljava/lang/Float; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;", */
/* "Ljava/lang/Float;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 50 */
/* .local p0, "this":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TT;>;" */
/* .local p1, "object":Ljava/lang/Object;, "TT;" */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 51 */
v0 = (( java.lang.Float ) p2 ).floatValue ( ); // invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F
(( miui.android.animation.property.FloatProperty ) p0 ).setValue ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V
/* .line 53 */
} // :cond_0
return;
} // .end method
public void set ( java.lang.Object p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 29 */
/* .local p0, "this":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TT;>;" */
/* check-cast p2, Ljava/lang/Float; */
(( miui.android.animation.property.FloatProperty ) p0 ).set ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/property/FloatProperty;->set(Ljava/lang/Object;Ljava/lang/Float;)V
return;
} // .end method
public abstract void setValue ( java.lang.Object p0, Float p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(TT;F)V" */
/* } */
} // .end annotation
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 73 */
/* .local p0, "this":Lmiui/android/animation/property/FloatProperty;, "Lmiui/android/animation/property/FloatProperty<TT;>;" */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "{mPropertyName=\'" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPropertyName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
