.class public Lmiui/android/animation/property/IntValueProperty;
.super Lmiui/android/animation/property/ValueProperty;
.source "IntValueProperty.java"

# interfaces
.implements Lmiui/android/animation/property/IIntValueProperty;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 6
    invoke-direct {p0, p1}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V

    .line 7
    return-void
.end method


# virtual methods
.method public getIntValue(Ljava/lang/Object;)I
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .line 18
    instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject;

    if-eqz v0, :cond_0

    .line 19
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {p0}, Lmiui/android/animation/property/IntValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Lmiui/android/animation/property/ValueTargetObject;->getPropertyValue(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 20
    .local v0, "value":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    .line 24
    .end local v0    # "value":Ljava/lang/Integer;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setIntValue(Ljava/lang/Object;I)V
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "i"    # I

    .line 11
    instance-of v0, p1, Lmiui/android/animation/property/ValueTargetObject;

    if-eqz v0, :cond_0

    .line 12
    move-object v0, p1

    check-cast v0, Lmiui/android/animation/property/ValueTargetObject;

    invoke-virtual {p0}, Lmiui/android/animation/property/IntValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lmiui/android/animation/property/ValueTargetObject;->setPropertyValue(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 14
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IntValueProperty{name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 30
    invoke-virtual {p0}, Lmiui/android/animation/property/IntValueProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 29
    return-object v0
.end method
