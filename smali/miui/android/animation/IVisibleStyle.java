public abstract class miui.android.animation.IVisibleStyle implements miui.android.animation.IStateContainer {
	 /* .source "IVisibleStyle.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/IVisibleStyle$VisibleType; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void hide ( miui.android.animation.base.AnimConfig...p0 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setAlpha ( Float p0, miui.android.animation.IVisibleStyle$VisibleType...p1 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setBound ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setFlags ( Long p0 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setHide ( ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setMove ( Integer p0, Integer p1 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setMove ( Integer p0, Integer p1, miui.android.animation.IVisibleStyle$VisibleType...p2 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setScale ( Float p0, miui.android.animation.IVisibleStyle$VisibleType...p1 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setShow ( ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle setShowDelay ( Long p0 ) {
} // .end method
public abstract void show ( miui.android.animation.base.AnimConfig...p0 ) {
} // .end method
public abstract miui.android.animation.IVisibleStyle useAutoAlpha ( Boolean p0 ) {
} // .end method
