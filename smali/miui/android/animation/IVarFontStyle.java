public abstract class miui.android.animation.IVarFontStyle implements miui.android.animation.IStateContainer {
	 /* .source "IVarFontStyle.java" */
	 /* # interfaces */
	 /* # virtual methods */
	 public abstract miui.android.animation.IVarFontStyle fromTo ( Integer p0, Integer p1, miui.android.animation.base.AnimConfig...p2 ) {
	 } // .end method
	 public abstract miui.android.animation.IVarFontStyle setTo ( Integer p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IVarFontStyle to ( Integer p0, miui.android.animation.base.AnimConfig...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IVarFontStyle useAt ( android.widget.TextView p0, Integer p1, Integer p2 ) {
	 } // .end method
