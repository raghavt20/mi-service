.class public Lmiui/android/animation/ViewTarget;
.super Lmiui/android/animation/IAnimTarget;
.source "ViewTarget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/ViewTarget$LifecycleCallbacks;,
        Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiui/android/animation/IAnimTarget<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final sCreator:Lmiui/android/animation/ITargetCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiui/android/animation/ITargetCreator<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mLifecycleCallbacks:Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

.field private mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

.field private mViewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lmiui/android/animation/ViewTarget$1;

    invoke-direct {v0}, Lmiui/android/animation/ViewTarget$1;-><init>()V

    sput-object v0, Lmiui/android/animation/ViewTarget;->sCreator:Lmiui/android/animation/ITargetCreator;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .line 43
    invoke-direct {p0}, Lmiui/android/animation/IAnimTarget;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    .line 45
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiui/android/animation/ViewTarget;->registerLifecycle(Landroid/content/Context;)Z

    .line 46
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lmiui/android/animation/ViewTarget$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lmiui/android/animation/ViewTarget$1;

    .line 28
    invoke-direct {p0, p1}, Lmiui/android/animation/ViewTarget;-><init>(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lmiui/android/animation/ViewTarget;Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/ViewTarget;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Ljava/lang/Runnable;

    .line 28
    invoke-direct {p0, p1, p2}, Lmiui/android/animation/ViewTarget;->initLayout(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$200(Lmiui/android/animation/ViewTarget;)V
    .locals 0
    .param p0, "x0"    # Lmiui/android/animation/ViewTarget;

    .line 28
    invoke-direct {p0}, Lmiui/android/animation/ViewTarget;->cleanViewTarget()V

    return-void
.end method

.method private cleanViewTarget()V
    .locals 1

    .line 261
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mContextRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Lmiui/android/animation/ViewTarget;->unRegisterLifecycle(Landroid/content/Context;)Z

    .line 264
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/android/animation/ViewTarget;->setCorner(F)V

    .line 265
    filled-new-array {p0}, [Lmiui/android/animation/ViewTarget;

    move-result-object v0

    invoke-static {v0}, Lmiui/android/animation/Folme;->clean([Ljava/lang/Object;)V

    .line 266
    return-void
.end method

.method private executeTask(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "task"    # Ljava/lang/Runnable;

    .line 208
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    .line 210
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ViewTarget.executeTask failed, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "miuix_anim"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 212
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private initLayout(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "task"    # Ljava/lang/Runnable;

    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 161
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    .line 162
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const v2, 0x100b0004

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 163
    move-object v1, v0

    check-cast v1, Landroid/view/ViewGroup;

    .line 164
    .local v1, "vp":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    .line 165
    .local v3, "left":I
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    .line 166
    .local v4, "top":I
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v5

    .line 167
    .local v5, "visibility":I
    const/16 v6, 0x8

    if-ne v5, v6, :cond_0

    .line 168
    const/4 v6, 0x4

    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 170
    :cond_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewGroup;->measure(II)V

    .line 171
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v1, v3, v4, v6, v7}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 172
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 173
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 174
    const/4 v6, 0x0

    invoke-virtual {p1, v2, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 176
    .end local v1    # "vp":Landroid/view/ViewGroup;
    .end local v3    # "left":I
    .end local v4    # "top":I
    .end local v5    # "visibility":I
    :cond_1
    return-void
.end method

.method private registerLifecycle(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 49
    nop

    :goto_0
    if-eqz p1, :cond_5

    .line 50
    instance-of v0, p1, Landroidx/lifecycle/LifecycleOwner;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 51
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/ViewTarget;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 52
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    invoke-direct {v0, p0}, Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;-><init>(Lmiui/android/animation/ViewTarget;)V

    iput-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    .line 55
    :cond_0
    move-object v0, p1

    check-cast v0, Landroidx/lifecycle/LifecycleOwner;

    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object v0

    iget-object v2, p0, Lmiui/android/animation/ViewTarget;->mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    invoke-virtual {v0, v2}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 56
    return v1

    .line 57
    :cond_1
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_3

    .line 58
    nop

    .line 59
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/ViewTarget;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 60
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mLifecycleCallbacks:Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

    if-nez v0, :cond_2

    .line 61
    new-instance v0, Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

    invoke-direct {v0, p0}, Lmiui/android/animation/ViewTarget$LifecycleCallbacks;-><init>(Lmiui/android/animation/ViewTarget;)V

    iput-object v0, p0, Lmiui/android/animation/ViewTarget;->mLifecycleCallbacks:Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

    .line 63
    :cond_2
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    iget-object v2, p0, Lmiui/android/animation/ViewTarget;->mLifecycleCallbacks:Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 64
    return v1

    .line 70
    :cond_3
    instance-of v0, p1, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Landroid/content/ContextWrapper;

    .line 71
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    :goto_1
    move-object p1, v0

    goto :goto_0

    .line 75
    :cond_5
    const/4 v0, 0x0

    return v0
.end method

.method private setCorner(F)V
    .locals 3
    .param p1, "radius"    # F

    .line 269
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 270
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 271
    const v1, 0x100b0008

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 273
    :cond_0
    return-void
.end method

.method private unRegisterLifecycle(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 79
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 80
    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Landroidx/lifecycle/LifecycleOwner;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 83
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    if-eqz v0, :cond_1

    .line 84
    move-object v0, p1

    check-cast v0, Landroidx/lifecycle/LifecycleOwner;

    invoke-interface {v0}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/ViewTarget;->mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 86
    :cond_1
    iput-object v3, p0, Lmiui/android/animation/ViewTarget;->mViewLifecyclerObserver:Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;

    .line 87
    return v2

    .line 88
    :cond_2
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_3

    .line 89
    iget-object v1, p0, Lmiui/android/animation/ViewTarget;->mLifecycleCallbacks:Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

    if-eqz v1, :cond_3

    .line 90
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 91
    iput-object v3, p0, Lmiui/android/animation/ViewTarget;->mLifecycleCallbacks:Lmiui/android/animation/ViewTarget$LifecycleCallbacks;

    .line 92
    return v2

    .line 97
    :cond_3
    return v0
.end method


# virtual methods
.method public allowAnimRun()Z
    .locals 2

    .line 189
    invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    .line 190
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lmiui/android/animation/Folme;->isInDraggingState(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public clean()V
    .locals 0

    .line 113
    return-void
.end method

.method public executeOnInitialized(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "task"    # Ljava/lang/Runnable;

    .line 143
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 144
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 145
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isLaidOut()Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    if-nez v1, :cond_1

    .line 147
    :cond_0
    new-instance v1, Lmiui/android/animation/ViewTarget$2;

    invoke-direct {v1, p0, v0, p1}, Lmiui/android/animation/ViewTarget$2;-><init>(Lmiui/android/animation/ViewTarget;Landroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v1}, Lmiui/android/animation/ViewTarget;->post(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 154
    :cond_1
    invoke-virtual {p0, p1}, Lmiui/android/animation/ViewTarget;->post(Ljava/lang/Runnable;)V

    .line 157
    :cond_2
    :goto_0
    return-void
.end method

.method public getLocationOnScreen([I)V
    .locals 3
    .param p1, "location"    # [I

    .line 123
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 124
    .local v0, "view":Landroid/view/View;
    if-nez v0, :cond_0

    .line 125
    const/4 v1, 0x1

    const v2, 0x7fffffff

    aput v2, p1, v1

    const/4 v1, 0x0

    aput v2, p1, v1

    goto :goto_0

    .line 127
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 129
    :goto_0
    return-void
.end method

.method public getTargetObject()Landroid/view/View;
    .locals 1

    .line 102
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public bridge synthetic getTargetObject()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Z
    .locals 2

    .line 117
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 118
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public onFrameEnd(Z)V
    .locals 3
    .param p1, "isFinished"    # Z

    .line 133
    iget-object v0, p0, Lmiui/android/animation/ViewTarget;->mViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 134
    .local v0, "view":Landroid/view/View;
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 135
    const v1, 0x100b0007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 136
    const v1, 0x100b0006

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 137
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const v2, 0x100b0008

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 139
    :cond_0
    return-void
.end method

.method public post(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "task"    # Ljava/lang/Runnable;

    .line 195
    invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;

    move-result-object v0

    .line 196
    .local v0, "view":Landroid/view/View;
    if-nez v0, :cond_0

    .line 197
    return-void

    .line 199
    :cond_0
    iget-object v1, p0, Lmiui/android/animation/ViewTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    invoke-virtual {v1}, Lmiui/android/animation/internal/TargetHandler;->isInTargetThread()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 202
    :cond_1
    invoke-direct {p0, p1}, Lmiui/android/animation/ViewTarget;->executeTask(Ljava/lang/Runnable;)V

    .line 204
    :goto_0
    return-void
.end method

.method public shouldUseIntValue(Lmiui/android/animation/property/FloatProperty;)Z
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 180
    sget-object v0, Lmiui/android/animation/property/ViewProperty;->WIDTH:Lmiui/android/animation/property/ViewProperty;

    if-eq p1, v0, :cond_1

    sget-object v0, Lmiui/android/animation/property/ViewProperty;->HEIGHT:Lmiui/android/animation/property/ViewProperty;

    if-eq p1, v0, :cond_1

    sget-object v0, Lmiui/android/animation/property/ViewProperty;->SCROLL_X:Lmiui/android/animation/property/ViewProperty;

    if-eq p1, v0, :cond_1

    sget-object v0, Lmiui/android/animation/property/ViewProperty;->SCROLL_Y:Lmiui/android/animation/property/ViewProperty;

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 184
    :cond_0
    invoke-super {p0, p1}, Lmiui/android/animation/IAnimTarget;->shouldUseIntValue(Lmiui/android/animation/property/FloatProperty;)Z

    move-result v0

    return v0

    .line 182
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method
