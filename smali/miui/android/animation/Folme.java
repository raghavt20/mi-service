public class miui.android.animation.Folme {
	 /* .source "Folme.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/Folme$FolmeImpl;, */
	 /* Lmiui/android/animation/Folme$FontWeight;, */
	 /* Lmiui/android/animation/Folme$FontType; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long DELAY_TIME;
private static final Integer MSG_TARGET;
private static final java.util.concurrent.ConcurrentHashMap sImplMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* "Lmiui/android/animation/Folme$FolmeImpl;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final android.os.Handler sMainHandler;
private static java.util.concurrent.atomic.AtomicReference sTimeRatio;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/atomic/AtomicReference<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.Folme ( ) {
/* .locals 2 */
/* .line 39 */
/* new-instance v0, Lmiui/android/animation/Folme$1; */
/* invoke-direct {v0}, Lmiui/android/animation/Folme$1;-><init>()V */
miui.android.animation.internal.ThreadPoolUtil .post ( v0 );
/* .line 47 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicReference; */
/* const/high16 v1, 0x3f800000 # 1.0f */
java.lang.Float .valueOf ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V */
/* .line 89 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
/* .line 371 */
/* new-instance v0, Lmiui/android/animation/Folme$2; */
android.os.Looper .getMainLooper ( );
/* invoke-direct {v0, v1}, Lmiui/android/animation/Folme$2;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
public miui.android.animation.Folme ( ) {
/* .locals 0 */
/* .line 36 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static void access$000 ( Boolean p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Z */
/* .line 36 */
miui.android.animation.Folme .sendToTargetMessage ( p0 );
return;
} // .end method
static void access$200 ( ) { //synthethic
/* .locals 0 */
/* .line 36 */
miui.android.animation.Folme .clearTargets ( );
return;
} // .end method
public static void clean ( java.lang.Object...p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">([TT;)V" */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/SafeVarargs; */
} // .end annotation
/* .line 244 */
/* .local p0, "targetObjects":[Ljava/lang/Object;, "[TT;" */
v0 = miui.android.animation.utils.CommonUtils .isArrayEmpty ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 245 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lmiui/android/animation/IAnimTarget; */
/* .line 246 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
miui.android.animation.Folme .cleanAnimTarget ( v1 );
/* .line 247 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
} // :cond_0
/* .line 249 */
} // :cond_1
/* array-length v0, p0 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* if-ge v1, v0, :cond_2 */
/* aget-object v2, p0, v1 */
/* .line 250 */
/* .local v2, "targetObject":Ljava/lang/Object;, "TT;" */
miui.android.animation.Folme .doClean ( v2 );
/* .line 249 */
} // .end local v2 # "targetObject":Ljava/lang/Object;, "TT;"
/* add-int/lit8 v1, v1, 0x1 */
/* .line 253 */
} // :cond_2
} // :goto_2
return;
} // .end method
private static void cleanAnimTarget ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 2 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 281 */
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 282 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).remove ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/Folme$FolmeImpl; */
/* .line 283 */
/* .local v0, "impl":Lmiui/android/animation/Folme$FolmeImpl; */
v1 = this.animManager;
(( miui.android.animation.internal.AnimManager ) v1 ).clear ( ); // invoke-virtual {v1}, Lmiui/android/animation/internal/AnimManager;->clear()V
/* .line 284 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 285 */
(( miui.android.animation.Folme$FolmeImpl ) v0 ).clean ( ); // invoke-virtual {v0}, Lmiui/android/animation/Folme$FolmeImpl;->clean()V
/* .line 288 */
} // .end local v0 # "impl":Lmiui/android/animation/Folme$FolmeImpl;
} // :cond_0
return;
} // .end method
private static void clearTargetMessage ( ) {
/* .locals 3 */
/* .line 398 */
v0 = miui.android.animation.Folme.sMainHandler;
int v1 = 1; // const/4 v1, 0x1
v2 = (( android.os.Handler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 399 */
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 401 */
} // :cond_0
return;
} // .end method
private static void clearTargets ( ) {
/* .locals 4 */
/* .line 361 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lmiui/android/animation/IAnimTarget; */
/* .line 362 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
v2 = (( miui.android.animation.IAnimTarget ) v1 ).isValid ( ); // invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->isValid()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* const-wide/16 v2, 0x1 */
v2 = (( miui.android.animation.IAnimTarget ) v1 ).hasFlags ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lmiui/android/animation/IAnimTarget;->hasFlags(J)Z
if ( v2 != null) { // if-eqz v2, :cond_1
v2 = this.animManager;
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Lmiui/android/animation/property/FloatProperty; */
v2 = (( miui.android.animation.internal.AnimManager ) v2 ).isAnimRunning ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* if-nez v2, :cond_1 */
/* .line 363 */
} // :cond_0
/* filled-new-array {v1}, [Lmiui/android/animation/IAnimTarget; */
miui.android.animation.Folme .clean ( v2 );
/* .line 365 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
} // :cond_1
/* .line 366 */
} // :cond_2
return;
} // .end method
private static void doClean ( java.lang.Object p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(TT;)V" */
/* } */
} // .end annotation
/* .line 276 */
/* .local p0, "targetObject":Ljava/lang/Object;, "TT;" */
int v0 = 0; // const/4 v0, 0x0
miui.android.animation.Folme .getTarget ( p0,v0 );
/* .line 277 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
miui.android.animation.Folme .cleanAnimTarget ( v0 );
/* .line 278 */
return;
} // .end method
public static void end ( java.lang.Object...p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">([TT;)V" */
/* } */
} // .end annotation
/* .line 256 */
/* .local p0, "targetObjects":[Ljava/lang/Object;, "[TT;" */
/* array-length v0, p0 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* if-ge v1, v0, :cond_2 */
/* aget-object v2, p0, v1 */
/* .line 257 */
/* .local v2, "targetObject":Ljava/lang/Object;, "TT;" */
int v3 = 0; // const/4 v3, 0x0
miui.android.animation.Folme .getTarget ( v2,v3 );
/* .line 258 */
/* .local v3, "target":Lmiui/android/animation/IAnimTarget; */
/* if-nez v3, :cond_0 */
/* .line 259 */
/* .line 261 */
} // :cond_0
v4 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Lmiui/android/animation/Folme$FolmeImpl; */
/* .line 262 */
/* .local v4, "impl":Lmiui/android/animation/Folme$FolmeImpl; */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 263 */
(( miui.android.animation.Folme$FolmeImpl ) v4 ).end ( ); // invoke-virtual {v4}, Lmiui/android/animation/Folme$FolmeImpl;->end()V
/* .line 256 */
} // .end local v2 # "targetObject":Ljava/lang/Object;, "TT;"
} // .end local v3 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v4 # "impl":Lmiui/android/animation/Folme$FolmeImpl;
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 266 */
} // :cond_2
return;
} // .end method
private static miui.android.animation.Folme$FolmeImpl fillTargetArrayAndGetImpl ( android.view.View[] p0, miui.android.animation.IAnimTarget[] p1 ) {
/* .locals 5 */
/* .param p0, "views" # [Landroid/view/View; */
/* .param p1, "targets" # [Lmiui/android/animation/IAnimTarget; */
/* .line 228 */
int v0 = 0; // const/4 v0, 0x0
/* .line 229 */
/* .local v0, "createImpl":Z */
int v1 = 0; // const/4 v1, 0x0
/* .line 230 */
/* .local v1, "impl":Lmiui/android/animation/Folme$FolmeImpl; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* array-length v3, p0 */
/* if-ge v2, v3, :cond_2 */
/* .line 231 */
/* aget-object v3, p0, v2 */
v4 = miui.android.animation.ViewTarget.sCreator;
miui.android.animation.Folme .getTarget ( v3,v4 );
/* aput-object v3, p1, v2 */
/* .line 232 */
v3 = miui.android.animation.Folme.sImplMap;
/* aget-object v4, p1, v2 */
(( java.util.concurrent.ConcurrentHashMap ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lmiui/android/animation/Folme$FolmeImpl; */
/* .line 233 */
/* .local v3, "cur":Lmiui/android/animation/Folme$FolmeImpl; */
/* if-nez v1, :cond_0 */
/* .line 234 */
/* move-object v1, v3 */
/* .line 235 */
} // :cond_0
/* if-eq v1, v3, :cond_1 */
/* .line 236 */
int v0 = 1; // const/4 v0, 0x1
/* .line 230 */
} // .end local v3 # "cur":Lmiui/android/animation/Folme$FolmeImpl;
} // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 239 */
} // .end local v2 # "i":I
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
int v2 = 0; // const/4 v2, 0x0
} // :cond_3
/* move-object v2, v1 */
} // :goto_2
} // .end method
public static miui.android.animation.IAnimTarget getTarget ( java.lang.Object p0, miui.android.animation.ITargetCreator p1 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(TT;", */
/* "Lmiui/android/animation/ITargetCreator<", */
/* "TT;>;)", */
/* "Lmiui/android/animation/IAnimTarget;" */
/* } */
} // .end annotation
/* .line 295 */
/* .local p0, "targetObject":Ljava/lang/Object;, "TT;" */
/* .local p1, "creator":Lmiui/android/animation/ITargetCreator;, "Lmiui/android/animation/ITargetCreator<TT;>;" */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p0, :cond_0 */
/* .line 296 */
/* .line 298 */
} // :cond_0
/* instance-of v1, p0, Lmiui/android/animation/IAnimTarget; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 299 */
/* move-object v0, p0 */
/* check-cast v0, Lmiui/android/animation/IAnimTarget; */
/* .line 301 */
} // :cond_1
v1 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lmiui/android/animation/IAnimTarget; */
/* .line 302 */
/* .local v2, "target":Lmiui/android/animation/IAnimTarget; */
(( miui.android.animation.IAnimTarget ) v2 ).getTargetObject ( ); // invoke-virtual {v2}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
/* .line 303 */
/* .local v3, "obj":Ljava/lang/Object; */
if ( v3 != null) { // if-eqz v3, :cond_2
v4 = (( java.lang.Object ) v3 ).equals ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 304 */
/* .line 306 */
} // .end local v2 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v3 # "obj":Ljava/lang/Object;
} // :cond_2
/* .line 307 */
} // :cond_3
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 308 */
/* .line 309 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 310 */
miui.android.animation.Folme .useAt ( v1 );
/* .line 311 */
/* .line 314 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
} // :cond_4
} // .end method
public static miui.android.animation.IAnimTarget getTargetById ( Integer p0 ) {
/* .locals 3 */
/* .param p0, "id" # I */
/* .line 352 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/IAnimTarget; */
/* .line 353 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
/* iget v2, v1, Lmiui/android/animation/IAnimTarget;->id:I */
/* if-ne v2, p0, :cond_0 */
/* .line 354 */
/* .line 356 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
} // :cond_0
/* .line 357 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static java.util.Collection getTargets ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 92 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
} // .end method
public static void getTargets ( java.util.Collection p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Collection<", */
/* "Lmiui/android/animation/IAnimTarget;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 339 */
/* .local p0, "targets":Ljava/util/Collection;, "Ljava/util/Collection<Lmiui/android/animation/IAnimTarget;>;" */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Lmiui/android/animation/IAnimTarget; */
/* .line 340 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
v2 = (( miui.android.animation.IAnimTarget ) v1 ).isValid ( ); // invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->isValid()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const-wide/16 v2, 0x1 */
v2 = (( miui.android.animation.IAnimTarget ) v1 ).hasFlags ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Lmiui/android/animation/IAnimTarget;->hasFlags(J)Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = this.animManager;
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Lmiui/android/animation/property/FloatProperty; */
v2 = (( miui.android.animation.internal.AnimManager ) v2 ).isAnimRunning ( v3 ); // invoke-virtual {v2, v3}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
/* if-nez v2, :cond_0 */
/* .line 343 */
} // :cond_0
/* .line 341 */
} // :cond_1
} // :goto_1
/* filled-new-array {v1}, [Lmiui/android/animation/IAnimTarget; */
miui.android.animation.Folme .clean ( v2 );
/* .line 345 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
} // :goto_2
/* .line 346 */
} // :cond_2
return;
} // .end method
public static Float getTimeRatio ( ) {
/* .locals 1 */
/* .line 60 */
v0 = miui.android.animation.Folme.sTimeRatio;
(( java.util.concurrent.atomic.AtomicReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // .end method
public static miui.android.animation.ValueTarget getValueTarget ( java.lang.Object p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(TT;)", */
/* "Lmiui/android/animation/ValueTarget;" */
/* } */
} // .end annotation
/* .line 291 */
/* .local p0, "targetObject":Ljava/lang/Object;, "TT;" */
v0 = miui.android.animation.ValueTarget.sCreator;
miui.android.animation.Folme .getTarget ( p0,v0 );
/* check-cast v0, Lmiui/android/animation/ValueTarget; */
} // .end method
public static Boolean isInDraggingState ( android.view.View p0 ) {
/* .locals 1 */
/* .param p0, "view" # Landroid/view/View; */
/* .line 332 */
/* const v0, 0x100b0005 */
(( android.view.View ) p0 ).getTag ( v0 ); // invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static void onListViewTouchEvent ( android.widget.AbsListView p0, android.view.MotionEvent p1 ) {
/* .locals 1 */
/* .param p0, "listView" # Landroid/widget/AbsListView; */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 269 */
miui.android.animation.controller.FolmeTouch .getListViewTouchListener ( p0 );
/* .line 270 */
/* .local v0, "listener":Lmiui/android/animation/controller/ListViewTouchListener; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 271 */
(( miui.android.animation.controller.ListViewTouchListener ) v0 ).onTouch ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lmiui/android/animation/controller/ListViewTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
/* .line 273 */
} // :cond_0
return;
} // .end method
public static void post ( java.lang.Object p0, java.lang.Runnable p1 ) {
/* .locals 1 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(TT;", */
/* "Ljava/lang/Runnable;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 64 */
/* .local p0, "targetObject":Ljava/lang/Object;, "TT;" */
int v0 = 0; // const/4 v0, 0x0
miui.android.animation.Folme .getTarget ( p0,v0 );
/* .line 65 */
/* .local v0, "target":Lmiui/android/animation/IAnimTarget; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 66 */
(( miui.android.animation.IAnimTarget ) v0 ).post ( p1 ); // invoke-virtual {v0, p1}, Lmiui/android/animation/IAnimTarget;->post(Ljava/lang/Runnable;)V
/* .line 68 */
} // :cond_0
return;
} // .end method
private static void sendToTargetMessage ( Boolean p0 ) {
/* .locals 4 */
/* .param p0, "fromAuto" # Z */
/* .line 384 */
miui.android.animation.Folme .clearTargetMessage ( );
/* .line 385 */
if ( p0 != null) { // if-eqz p0, :cond_0
v0 = miui.android.animation.utils.LogUtils .isLogEnabled ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 386 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).keySet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lmiui/android/animation/IAnimTarget; */
/* .line 387 */
/* .local v1, "target":Lmiui/android/animation/IAnimTarget; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "exist target:"; // const-string v3, "exist target:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.android.animation.IAnimTarget ) v1 ).getTargetObject ( ); // invoke-virtual {v1}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
/* new-array v3, v3, [Ljava/lang/Object; */
miui.android.animation.utils.LogUtils .debug ( v2,v3 );
/* .line 388 */
} // .end local v1 # "target":Lmiui/android/animation/IAnimTarget;
/* .line 390 */
} // :cond_0
v0 = miui.android.animation.Folme.sImplMap;
v0 = (( java.util.concurrent.ConcurrentHashMap ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I
/* if-lez v0, :cond_1 */
/* .line 391 */
v0 = miui.android.animation.Folme.sMainHandler;
int v1 = 1; // const/4 v1, 0x1
/* const-wide/16 v2, 0x4e20 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 393 */
} // :cond_1
miui.android.animation.Folme .clearTargetMessage ( );
/* .line 395 */
} // :goto_1
return;
} // .end method
public static void setAnimPlayRatio ( Float p0 ) {
/* .locals 2 */
/* .param p0, "ratio" # F */
/* .line 56 */
v0 = miui.android.animation.Folme.sTimeRatio;
java.lang.Float .valueOf ( p0 );
(( java.util.concurrent.atomic.AtomicReference ) v0 ).set ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
/* .line 57 */
return;
} // .end method
public static void setDraggingState ( android.view.View p0, Boolean p1 ) {
/* .locals 2 */
/* .param p0, "view" # Landroid/view/View; */
/* .param p1, "isDragging" # Z */
/* .line 321 */
/* const v0, 0x100b0005 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 322 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Boolean .valueOf ( v1 );
(( android.view.View ) p0 ).setTag ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 324 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( android.view.View ) p0 ).setTag ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 326 */
} // :goto_0
return;
} // .end method
public static miui.android.animation.IFolme useAt ( miui.android.animation.IAnimTarget p0 ) {
/* .locals 5 */
/* .param p0, "target" # Lmiui/android/animation/IAnimTarget; */
/* .line 195 */
v0 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/android/animation/Folme$FolmeImpl; */
/* .line 196 */
/* .local v1, "mc":Lmiui/android/animation/Folme$FolmeImpl; */
/* if-nez v1, :cond_0 */
/* .line 197 */
/* new-instance v2, Lmiui/android/animation/Folme$FolmeImpl; */
/* filled-new-array {p0}, [Lmiui/android/animation/IAnimTarget; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v2, v3, v4}, Lmiui/android/animation/Folme$FolmeImpl;-><init>([Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/Folme$1;)V */
/* move-object v1, v2 */
/* .line 198 */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).putIfAbsent ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v0, Lmiui/android/animation/Folme$FolmeImpl; */
/* .line 199 */
/* .local v0, "prev":Lmiui/android/animation/Folme$FolmeImpl; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 200 */
/* move-object v1, v0 */
/* .line 203 */
} // .end local v0 # "prev":Lmiui/android/animation/Folme$FolmeImpl;
} // :cond_0
} // .end method
public static miui.android.animation.IFolme useAt ( android.view.View...p0 ) {
/* .locals 6 */
/* .param p0, "views" # [Landroid/view/View; */
/* .line 207 */
/* array-length v0, p0 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 210 */
/* array-length v0, p0 */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* .line 211 */
/* aget-object v0, p0, v1 */
v1 = miui.android.animation.ViewTarget.sCreator;
miui.android.animation.Folme .getTarget ( v0,v1 );
miui.android.animation.Folme .useAt ( v0 );
/* .line 213 */
} // :cond_0
/* array-length v0, p0 */
/* new-array v0, v0, [Lmiui/android/animation/IAnimTarget; */
/* .line 214 */
/* .local v0, "targets":[Lmiui/android/animation/IAnimTarget; */
miui.android.animation.Folme .fillTargetArrayAndGetImpl ( p0,v0 );
/* .line 215 */
/* .local v2, "impl":Lmiui/android/animation/Folme$FolmeImpl; */
/* if-nez v2, :cond_2 */
/* .line 216 */
/* new-instance v3, Lmiui/android/animation/Folme$FolmeImpl; */
int v4 = 0; // const/4 v4, 0x0
/* invoke-direct {v3, v0, v4}, Lmiui/android/animation/Folme$FolmeImpl;-><init>([Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/Folme$1;)V */
/* move-object v2, v3 */
/* .line 217 */
/* array-length v3, v0 */
} // :goto_0
/* if-ge v1, v3, :cond_2 */
/* aget-object v4, v0, v1 */
/* .line 218 */
/* .local v4, "target":Lmiui/android/animation/IAnimTarget; */
v5 = miui.android.animation.Folme.sImplMap;
(( java.util.concurrent.ConcurrentHashMap ) v5 ).put ( v4, v2 ); // invoke-virtual {v5, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Lmiui/android/animation/Folme$FolmeImpl; */
/* .line 219 */
/* .local v5, "prevImpl":Lmiui/android/animation/Folme$FolmeImpl; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 220 */
(( miui.android.animation.Folme$FolmeImpl ) v5 ).clean ( ); // invoke-virtual {v5}, Lmiui/android/animation/Folme$FolmeImpl;->clean()V
/* .line 217 */
} // .end local v4 # "target":Lmiui/android/animation/IAnimTarget;
} // .end local v5 # "prevImpl":Lmiui/android/animation/Folme$FolmeImpl;
} // :cond_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 224 */
} // :cond_2
/* .line 208 */
} // .end local v0 # "targets":[Lmiui/android/animation/IAnimTarget;
} // .end local v2 # "impl":Lmiui/android/animation/Folme$FolmeImpl;
} // :cond_3
/* new-instance v0, Ljava/lang/IllegalArgumentException; */
/* const-string/jumbo v1, "useAt can not be applied to empty views array" */
/* invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public static void useSystemAnimatorDurationScale ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 50 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "animator_duration_scale"; // const-string v1, "animator_duration_scale"
/* const/high16 v2, 0x3f800000 # 1.0f */
v0 = android.provider.Settings$Global .getFloat ( v0,v1,v2 );
/* .line 52 */
/* .local v0, "scale":F */
v1 = miui.android.animation.Folme.sTimeRatio;
java.lang.Float .valueOf ( v0 );
(( java.util.concurrent.atomic.AtomicReference ) v1 ).set ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
/* .line 53 */
return;
} // .end method
public static miui.android.animation.IStateStyle useValue ( java.lang.Object...p0 ) {
/* .locals 3 */
/* .param p0, "targetObj" # [Ljava/lang/Object; */
/* .line 184 */
/* array-length v0, p0 */
/* if-lez v0, :cond_0 */
/* .line 185 */
int v0 = 0; // const/4 v0, 0x0
/* aget-object v0, p0, v0 */
v1 = miui.android.animation.ValueTarget.sCreator;
miui.android.animation.Folme .getTarget ( v0,v1 );
miui.android.animation.Folme .useAt ( v0 );
/* .local v0, "folme":Lmiui/android/animation/IFolme; */
/* .line 187 */
} // .end local v0 # "folme":Lmiui/android/animation/IFolme;
} // :cond_0
/* new-instance v0, Lmiui/android/animation/ValueTarget; */
/* invoke-direct {v0}, Lmiui/android/animation/ValueTarget;-><init>()V */
/* .line 188 */
/* .local v0, "target":Lmiui/android/animation/ValueTarget; */
/* const-wide/16 v1, 0x1 */
(( miui.android.animation.ValueTarget ) v0 ).setFlags ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/android/animation/ValueTarget;->setFlags(J)V
/* .line 189 */
miui.android.animation.Folme .useAt ( v0 );
/* move-object v0, v1 */
/* .line 191 */
/* .local v0, "folme":Lmiui/android/animation/IFolme; */
} // :goto_0
} // .end method
public static miui.android.animation.IVarFontStyle useVarFontAt ( android.widget.TextView p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p0, "view" # Landroid/widget/TextView; */
/* .param p1, "fontType" # I */
/* .param p2, "initFontWeight" # I */
/* .line 96 */
/* new-instance v0, Lmiui/android/animation/controller/FolmeFont; */
/* invoke-direct {v0}, Lmiui/android/animation/controller/FolmeFont;-><init>()V */
(( miui.android.animation.controller.FolmeFont ) v0 ).useAt ( p0, p1, p2 ); // invoke-virtual {v0, p0, p1, p2}, Lmiui/android/animation/controller/FolmeFont;->useAt(Landroid/widget/TextView;II)Lmiui/android/animation/IVarFontStyle;
} // .end method
