public abstract class miui.android.animation.IStateStyle implements miui.android.animation.IStateContainer {
	 /* .source "IStateStyle.java" */
	 /* # interfaces */
	 /* # virtual methods */
	 public abstract miui.android.animation.IStateStyle add ( java.lang.String p0, Float p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( java.lang.String p0, Float p1, Long p2 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( java.lang.String p0, Integer p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( java.lang.String p0, Integer p1, Long p2 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Float p1, Long p2 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Integer p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle add ( miui.android.animation.property.FloatProperty p0, Integer p1, Long p2 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle addInitProperty ( java.lang.String p0, Float p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle addInitProperty ( java.lang.String p0, Integer p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle addInitProperty ( miui.android.animation.property.FloatProperty p0, Float p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle addInitProperty ( miui.android.animation.property.FloatProperty p0, Integer p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle addListener ( miui.android.animation.listener.TransitionListener p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle autoSetTo ( java.lang.Object...p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle fromTo ( java.lang.Object p0, java.lang.Object p1, miui.android.animation.base.AnimConfig...p2 ) {
	 } // .end method
	 public abstract miui.android.animation.controller.AnimState getCurrentState ( ) {
	 } // .end method
	 public abstract Long predictDuration ( java.lang.Object...p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle removeListener ( miui.android.animation.listener.TransitionListener p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle set ( java.lang.Object p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setConfig ( miui.android.animation.base.AnimConfig p0, miui.android.animation.property.FloatProperty...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setEase ( Integer p0, Float...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setEase ( miui.android.animation.property.FloatProperty p0, Integer p1, Float...p2 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setEase ( miui.android.animation.utils.EaseManager$EaseStyle p0, miui.android.animation.property.FloatProperty...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setFlags ( Long p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setTo ( java.lang.Object p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setTo ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setTo ( java.lang.Object...p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setTransitionFlags ( Long p0, miui.android.animation.property.FloatProperty...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle setup ( java.lang.Object p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle then ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle then ( java.lang.Object...p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle to ( java.lang.Object p0, miui.android.animation.base.AnimConfig...p1 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle to ( java.lang.Object...p0 ) {
	 } // .end method
	 public abstract miui.android.animation.IStateStyle to ( miui.android.animation.base.AnimConfig...p0 ) {
	 } // .end method
