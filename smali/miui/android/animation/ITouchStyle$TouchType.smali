.class public final enum Lmiui/android/animation/ITouchStyle$TouchType;
.super Ljava/lang/Enum;
.source "ITouchStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/android/animation/ITouchStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TouchType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiui/android/animation/ITouchStyle$TouchType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lmiui/android/animation/ITouchStyle$TouchType;

.field public static final enum DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

.field public static final enum UP:Lmiui/android/animation/ITouchStyle$TouchType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 15
    new-instance v0, Lmiui/android/animation/ITouchStyle$TouchType;

    const-string v1, "UP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiui/android/animation/ITouchStyle$TouchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiui/android/animation/ITouchStyle$TouchType;->UP:Lmiui/android/animation/ITouchStyle$TouchType;

    new-instance v1, Lmiui/android/animation/ITouchStyle$TouchType;

    const-string v2, "DOWN"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lmiui/android/animation/ITouchStyle$TouchType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lmiui/android/animation/ITouchStyle$TouchType;->DOWN:Lmiui/android/animation/ITouchStyle$TouchType;

    .line 14
    filled-new-array {v0, v1}, [Lmiui/android/animation/ITouchStyle$TouchType;

    move-result-object v0

    sput-object v0, Lmiui/android/animation/ITouchStyle$TouchType;->$VALUES:[Lmiui/android/animation/ITouchStyle$TouchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiui/android/animation/ITouchStyle$TouchType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 14
    const-class v0, Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/ITouchStyle$TouchType;

    return-object v0
.end method

.method public static values()[Lmiui/android/animation/ITouchStyle$TouchType;
    .locals 1

    .line 14
    sget-object v0, Lmiui/android/animation/ITouchStyle$TouchType;->$VALUES:[Lmiui/android/animation/ITouchStyle$TouchType;

    invoke-virtual {v0}, [Lmiui/android/animation/ITouchStyle$TouchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiui/android/animation/ITouchStyle$TouchType;

    return-object v0
.end method
