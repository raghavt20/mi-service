public class miui.android.animation.ViewTarget extends miui.android.animation.IAnimTarget {
	 /* .source "ViewTarget.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/ViewTarget$LifecycleCallbacks;, */
	 /* Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver; */
	 /* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lmiui/android/animation/IAnimTarget<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
/* # static fields */
public static final miui.android.animation.ITargetCreator sCreator;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Lmiui/android/animation/ITargetCreator<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.lang.ref.WeakReference mContextRef;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/content/Context;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private miui.android.animation.ViewTarget$LifecycleCallbacks mLifecycleCallbacks;
private miui.android.animation.ViewTarget$ViewLifecyclerObserver mViewLifecyclerObserver;
private java.lang.ref.WeakReference mViewRef;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Landroid/view/View;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static miui.android.animation.ViewTarget ( ) {
/* .locals 1 */
/* .line 35 */
/* new-instance v0, Lmiui/android/animation/ViewTarget$1; */
/* invoke-direct {v0}, Lmiui/android/animation/ViewTarget$1;-><init>()V */
return;
} // .end method
private miui.android.animation.ViewTarget ( ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/view/View; */
/* .line 43 */
/* invoke-direct {p0}, Lmiui/android/animation/IAnimTarget;-><init>()V */
/* .line 44 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mViewRef = v0;
/* .line 45 */
(( android.view.View ) p1 ).getContext ( ); // invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;
/* invoke-direct {p0, v0}, Lmiui/android/animation/ViewTarget;->registerLifecycle(Landroid/content/Context;)Z */
/* .line 46 */
return;
} // .end method
 miui.android.animation.ViewTarget ( ) { //synthethic
/* .locals 0 */
/* .param p1, "x0" # Landroid/view/View; */
/* .param p2, "x1" # Lmiui/android/animation/ViewTarget$1; */
/* .line 28 */
/* invoke-direct {p0, p1}, Lmiui/android/animation/ViewTarget;-><init>(Landroid/view/View;)V */
return;
} // .end method
static void access$100 ( miui.android.animation.ViewTarget p0, android.view.View p1, java.lang.Runnable p2 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/ViewTarget; */
/* .param p1, "x1" # Landroid/view/View; */
/* .param p2, "x2" # Ljava/lang/Runnable; */
/* .line 28 */
/* invoke-direct {p0, p1, p2}, Lmiui/android/animation/ViewTarget;->initLayout(Landroid/view/View;Ljava/lang/Runnable;)V */
return;
} // .end method
static void access$200 ( miui.android.animation.ViewTarget p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "x0" # Lmiui/android/animation/ViewTarget; */
/* .line 28 */
/* invoke-direct {p0}, Lmiui/android/animation/ViewTarget;->cleanViewTarget()V */
return;
} // .end method
private void cleanViewTarget ( ) {
/* .locals 1 */
/* .line 261 */
v0 = this.mContextRef;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 262 */
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/content/Context; */
/* invoke-direct {p0, v0}, Lmiui/android/animation/ViewTarget;->unRegisterLifecycle(Landroid/content/Context;)Z */
/* .line 264 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lmiui/android/animation/ViewTarget;->setCorner(F)V */
/* .line 265 */
/* filled-new-array {p0}, [Lmiui/android/animation/ViewTarget; */
miui.android.animation.Folme .clean ( v0 );
/* .line 266 */
return;
} // .end method
private void executeTask ( java.lang.Runnable p0 ) {
/* .locals 3 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 208 */
try { // :try_start_0
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 211 */
/* .line 209 */
/* :catch_0 */
/* move-exception v0 */
/* .line 210 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "ViewTarget.executeTask failed, "; // const-string v2, "ViewTarget.executeTask failed, "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( miui.android.animation.ViewTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "miuix_anim"; // const-string v2, "miuix_anim"
android.util.Log .w ( v2,v1,v0 );
/* .line 212 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void initLayout ( android.view.View p0, java.lang.Runnable p1 ) {
/* .locals 8 */
/* .param p1, "view" # Landroid/view/View; */
/* .param p2, "task" # Ljava/lang/Runnable; */
/* .line 160 */
(( android.view.View ) p1 ).getParent ( ); // invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
/* .line 161 */
/* .local v0, "parent":Landroid/view/ViewParent; */
/* instance-of v1, v0, Landroid/view/ViewGroup; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 162 */
int v1 = 1; // const/4 v1, 0x1
java.lang.Boolean .valueOf ( v1 );
/* const v2, 0x100b0004 */
(( android.view.View ) p1 ).setTag ( v2, v1 ); // invoke-virtual {p1, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 163 */
/* move-object v1, v0 */
/* check-cast v1, Landroid/view/ViewGroup; */
/* .line 164 */
/* .local v1, "vp":Landroid/view/ViewGroup; */
v3 = (( android.view.ViewGroup ) v1 ).getLeft ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getLeft()I
/* .line 165 */
/* .local v3, "left":I */
v4 = (( android.view.ViewGroup ) v1 ).getTop ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getTop()I
/* .line 166 */
/* .local v4, "top":I */
v5 = (( android.view.View ) p1 ).getVisibility ( ); // invoke-virtual {p1}, Landroid/view/View;->getVisibility()I
/* .line 167 */
/* .local v5, "visibility":I */
/* const/16 v6, 0x8 */
/* if-ne v5, v6, :cond_0 */
/* .line 168 */
int v6 = 4; // const/4 v6, 0x4
(( android.view.View ) p1 ).setVisibility ( v6 ); // invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V
/* .line 170 */
} // :cond_0
v6 = (( android.view.ViewGroup ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I
v7 = (( android.view.ViewGroup ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I
(( android.view.ViewGroup ) v1 ).measure ( v6, v7 ); // invoke-virtual {v1, v6, v7}, Landroid/view/ViewGroup;->measure(II)V
/* .line 171 */
v6 = (( android.view.ViewGroup ) v1 ).getWidth ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I
/* add-int/2addr v6, v3 */
v7 = (( android.view.ViewGroup ) v1 ).getHeight ( ); // invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I
/* add-int/2addr v7, v4 */
(( android.view.ViewGroup ) v1 ).layout ( v3, v4, v6, v7 ); // invoke-virtual {v1, v3, v4, v6, v7}, Landroid/view/ViewGroup;->layout(IIII)V
/* .line 172 */
(( android.view.View ) p1 ).setVisibility ( v5 ); // invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V
/* .line 173 */
/* .line 174 */
int v6 = 0; // const/4 v6, 0x0
(( android.view.View ) p1 ).setTag ( v2, v6 ); // invoke-virtual {p1, v2, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 176 */
} // .end local v1 # "vp":Landroid/view/ViewGroup;
} // .end local v3 # "left":I
} // .end local v4 # "top":I
} // .end local v5 # "visibility":I
} // :cond_1
return;
} // .end method
private Boolean registerLifecycle ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 49 */
/* nop */
} // :goto_0
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 50 */
/* instance-of v0, p1, Landroidx/lifecycle/LifecycleOwner; */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 51 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mContextRef = v0;
/* .line 52 */
v0 = this.mViewLifecyclerObserver;
/* if-nez v0, :cond_0 */
/* .line 53 */
/* new-instance v0, Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/ViewTarget$ViewLifecyclerObserver;-><init>(Lmiui/android/animation/ViewTarget;)V */
this.mViewLifecyclerObserver = v0;
/* .line 55 */
} // :cond_0
/* move-object v0, p1 */
/* check-cast v0, Landroidx/lifecycle/LifecycleOwner; */
v2 = this.mViewLifecyclerObserver;
(( androidx.lifecycle.Lifecycle ) v0 ).addObserver ( v2 ); // invoke-virtual {v0, v2}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V
/* .line 56 */
/* .line 57 */
} // :cond_1
/* instance-of v0, p1, Landroid/app/Activity; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 58 */
/* nop */
/* .line 59 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mContextRef = v0;
/* .line 60 */
v0 = this.mLifecycleCallbacks;
/* if-nez v0, :cond_2 */
/* .line 61 */
/* new-instance v0, Lmiui/android/animation/ViewTarget$LifecycleCallbacks; */
/* invoke-direct {v0, p0}, Lmiui/android/animation/ViewTarget$LifecycleCallbacks;-><init>(Lmiui/android/animation/ViewTarget;)V */
this.mLifecycleCallbacks = v0;
/* .line 63 */
} // :cond_2
/* move-object v0, p1 */
/* check-cast v0, Landroid/app/Activity; */
v2 = this.mLifecycleCallbacks;
(( android.app.Activity ) v0 ).registerActivityLifecycleCallbacks ( v2 ); // invoke-virtual {v0, v2}, Landroid/app/Activity;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
/* .line 64 */
/* .line 70 */
} // :cond_3
/* instance-of v0, p1, Landroid/content/ContextWrapper; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* move-object v0, p1 */
/* check-cast v0, Landroid/content/ContextWrapper; */
/* .line 71 */
(( android.content.ContextWrapper ) v0 ).getBaseContext ( ); // invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // :goto_1
/* move-object p1, v0 */
/* .line 75 */
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void setCorner ( Float p0 ) {
/* .locals 3 */
/* .param p1, "radius" # F */
/* .line 269 */
v0 = this.mViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 270 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 271 */
/* const v1, 0x100b0008 */
java.lang.Float .valueOf ( p1 );
(( android.view.View ) v0 ).setTag ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 273 */
} // :cond_0
return;
} // .end method
private Boolean unRegisterLifecycle ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 79 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 80 */
/* .line 82 */
} // :cond_0
/* instance-of v1, p1, Landroidx/lifecycle/LifecycleOwner; */
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 83 */
v0 = this.mViewLifecyclerObserver;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 84 */
/* move-object v0, p1 */
/* check-cast v0, Landroidx/lifecycle/LifecycleOwner; */
v1 = this.mViewLifecyclerObserver;
(( androidx.lifecycle.Lifecycle ) v0 ).removeObserver ( v1 ); // invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V
/* .line 86 */
} // :cond_1
this.mViewLifecyclerObserver = v3;
/* .line 87 */
/* .line 88 */
} // :cond_2
/* instance-of v1, p1, Landroid/app/Activity; */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 89 */
v1 = this.mLifecycleCallbacks;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 90 */
/* move-object v0, p1 */
/* check-cast v0, Landroid/app/Activity; */
(( android.app.Activity ) v0 ).unregisterActivityLifecycleCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V
/* .line 91 */
this.mLifecycleCallbacks = v3;
/* .line 92 */
/* .line 97 */
} // :cond_3
} // .end method
/* # virtual methods */
public Boolean allowAnimRun ( ) {
/* .locals 2 */
/* .line 189 */
(( miui.android.animation.ViewTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
/* .line 190 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = miui.android.animation.Folme .isInDraggingState ( v0 );
/* if-nez v1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void clean ( ) {
/* .locals 0 */
/* .line 113 */
return;
} // .end method
public void executeOnInitialized ( java.lang.Runnable p0 ) {
/* .locals 3 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 143 */
v0 = this.mViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 144 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 145 */
v1 = (( android.view.View ) v0 ).getVisibility ( ); // invoke-virtual {v0}, Landroid/view/View;->getVisibility()I
/* const/16 v2, 0x8 */
/* if-ne v1, v2, :cond_1 */
v1 = (( android.view.View ) v0 ).isLaidOut ( ); // invoke-virtual {v0}, Landroid/view/View;->isLaidOut()Z
/* if-nez v1, :cond_1 */
/* .line 146 */
v1 = (( android.view.View ) v0 ).getWidth ( ); // invoke-virtual {v0}, Landroid/view/View;->getWidth()I
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( android.view.View ) v0 ).getHeight ( ); // invoke-virtual {v0}, Landroid/view/View;->getHeight()I
/* if-nez v1, :cond_1 */
/* .line 147 */
} // :cond_0
/* new-instance v1, Lmiui/android/animation/ViewTarget$2; */
/* invoke-direct {v1, p0, v0, p1}, Lmiui/android/animation/ViewTarget$2;-><init>(Lmiui/android/animation/ViewTarget;Landroid/view/View;Ljava/lang/Runnable;)V */
(( miui.android.animation.ViewTarget ) p0 ).post ( v1 ); // invoke-virtual {p0, v1}, Lmiui/android/animation/ViewTarget;->post(Ljava/lang/Runnable;)V
/* .line 154 */
} // :cond_1
(( miui.android.animation.ViewTarget ) p0 ).post ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/ViewTarget;->post(Ljava/lang/Runnable;)V
/* .line 157 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void getLocationOnScreen ( Integer[] p0 ) {
/* .locals 3 */
/* .param p1, "location" # [I */
/* .line 123 */
v0 = this.mViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 124 */
/* .local v0, "view":Landroid/view/View; */
/* if-nez v0, :cond_0 */
/* .line 125 */
int v1 = 1; // const/4 v1, 0x1
/* const v2, 0x7fffffff */
/* aput v2, p1, v1 */
int v1 = 0; // const/4 v1, 0x0
/* aput v2, p1, v1 */
/* .line 127 */
} // :cond_0
(( android.view.View ) v0 ).getLocationOnScreen ( p1 ); // invoke-virtual {v0, p1}, Landroid/view/View;->getLocationOnScreen([I)V
/* .line 129 */
} // :goto_0
return;
} // .end method
public android.view.View getTargetObject ( ) {
/* .locals 1 */
/* .line 102 */
v0 = this.mViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
} // .end method
public java.lang.Object getTargetObject ( ) { //bridge//synthethic
/* .locals 1 */
/* .line 28 */
(( miui.android.animation.ViewTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
} // .end method
public Boolean isValid ( ) {
/* .locals 2 */
/* .line 117 */
v0 = this.mViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 118 */
/* .local v0, "view":Landroid/view/View; */
if ( v0 != null) { // if-eqz v0, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public void onFrameEnd ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isFinished" # Z */
/* .line 133 */
v0 = this.mViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/view/View; */
/* .line 134 */
/* .local v0, "view":Landroid/view/View; */
if ( p1 != null) { // if-eqz p1, :cond_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 135 */
/* const v1, 0x100b0007 */
int v2 = 0; // const/4 v2, 0x0
(( android.view.View ) v0 ).setTag ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 136 */
/* const v1, 0x100b0006 */
(( android.view.View ) v0 ).setTag ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 137 */
int v1 = 0; // const/4 v1, 0x0
java.lang.Float .valueOf ( v1 );
/* const v2, 0x100b0008 */
(( android.view.View ) v0 ).setTag ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V
/* .line 139 */
} // :cond_0
return;
} // .end method
public void post ( java.lang.Runnable p0 ) {
/* .locals 2 */
/* .param p1, "task" # Ljava/lang/Runnable; */
/* .line 195 */
(( miui.android.animation.ViewTarget ) p0 ).getTargetObject ( ); // invoke-virtual {p0}, Lmiui/android/animation/ViewTarget;->getTargetObject()Landroid/view/View;
/* .line 196 */
/* .local v0, "view":Landroid/view/View; */
/* if-nez v0, :cond_0 */
/* .line 197 */
return;
/* .line 199 */
} // :cond_0
v1 = this.handler;
v1 = (( miui.android.animation.internal.TargetHandler ) v1 ).isInTargetThread ( ); // invoke-virtual {v1}, Lmiui/android/animation/internal/TargetHandler;->isInTargetThread()Z
/* if-nez v1, :cond_1 */
v1 = (( android.view.View ) v0 ).isAttachedToWindow ( ); // invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 200 */
(( android.view.View ) v0 ).post ( p1 ); // invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z
/* .line 202 */
} // :cond_1
/* invoke-direct {p0, p1}, Lmiui/android/animation/ViewTarget;->executeTask(Ljava/lang/Runnable;)V */
/* .line 204 */
} // :goto_0
return;
} // .end method
public Boolean shouldUseIntValue ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 180 */
v0 = miui.android.animation.property.ViewProperty.WIDTH;
/* if-eq p1, v0, :cond_1 */
v0 = miui.android.animation.property.ViewProperty.HEIGHT;
/* if-eq p1, v0, :cond_1 */
v0 = miui.android.animation.property.ViewProperty.SCROLL_X;
/* if-eq p1, v0, :cond_1 */
v0 = miui.android.animation.property.ViewProperty.SCROLL_Y;
/* if-ne p1, v0, :cond_0 */
/* .line 184 */
} // :cond_0
v0 = /* invoke-super {p0, p1}, Lmiui/android/animation/IAnimTarget;->shouldUseIntValue(Lmiui/android/animation/property/FloatProperty;)Z */
/* .line 182 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
