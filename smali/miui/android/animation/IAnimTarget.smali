.class public abstract Lmiui/android/animation/IAnimTarget;
.super Ljava/lang/Object;
.source "IAnimTarget.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final FLAT_ONESHOT:J = 0x1L

.field static final sTargetIds:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final animManager:Lmiui/android/animation/internal/AnimManager;

.field public final handler:Lmiui/android/animation/internal/TargetHandler;

.field public final id:I

.field mDefaultMinVisible:F

.field mFlags:J

.field mMinVisibleChanges:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field final mTracker:Lmiui/android/animation/internal/TargetVelocityTracker;

.field notifyManager:Lmiui/android/animation/internal/NotifyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const v1, 0x7fffffff

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lmiui/android/animation/IAnimTarget;->sTargetIds:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .line 50
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lmiui/android/animation/internal/TargetHandler;

    invoke-direct {v0, p0}, Lmiui/android/animation/internal/TargetHandler;-><init>(Lmiui/android/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    .line 32
    new-instance v0, Lmiui/android/animation/internal/AnimManager;

    invoke-direct {v0}, Lmiui/android/animation/internal/AnimManager;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    .line 36
    new-instance v1, Lmiui/android/animation/internal/NotifyManager;

    invoke-direct {v1, p0}, Lmiui/android/animation/internal/NotifyManager;-><init>(Lmiui/android/animation/IAnimTarget;)V

    iput-object v1, p0, Lmiui/android/animation/IAnimTarget;->notifyManager:Lmiui/android/animation/internal/NotifyManager;

    .line 40
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    iput v1, p0, Lmiui/android/animation/IAnimTarget;->mDefaultMinVisible:F

    .line 42
    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/IAnimTarget;->mMinVisibleChanges:Ljava/util/Map;

    .line 46
    sget-object v1, Lmiui/android/animation/IAnimTarget;->sTargetIds:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    iput v1, p0, Lmiui/android/animation/IAnimTarget;->id:I

    .line 48
    new-instance v1, Lmiui/android/animation/internal/TargetVelocityTracker;

    invoke-direct {v1}, Lmiui/android/animation/internal/TargetVelocityTracker;-><init>()V

    iput-object v1, p0, Lmiui/android/animation/IAnimTarget;->mTracker:Lmiui/android/animation/internal/TargetVelocityTracker;

    .line 51
    invoke-virtual {v0, p0}, Lmiui/android/animation/internal/AnimManager;->setTarget(Lmiui/android/animation/IAnimTarget;)V

    .line 52
    const/4 v0, 0x3

    new-array v1, v0, [Lmiui/android/animation/property/FloatProperty;

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->ROTATION:Lmiui/android/animation/property/ViewProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->ROTATION_X:Lmiui/android/animation/property/ViewProperty;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->ROTATION_Y:Lmiui/android/animation/property/ViewProperty;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const v2, 0x3dcccccd    # 0.1f

    invoke-virtual {p0, v2, v1}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;

    .line 54
    const/4 v1, 0x4

    new-array v1, v1, [Lmiui/android/animation/property/FloatProperty;

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->ALPHA:Lmiui/android/animation/property/ViewProperty;

    aput-object v2, v1, v3

    sget-object v2, Lmiui/android/animation/property/ViewProperty;->AUTO_ALPHA:Lmiui/android/animation/property/ViewProperty;

    aput-object v2, v1, v4

    sget-object v2, Lmiui/android/animation/property/ViewPropertyExt;->FOREGROUND:Lmiui/android/animation/property/ViewPropertyExt$ForegroundProperty;

    aput-object v2, v1, v5

    sget-object v2, Lmiui/android/animation/property/ViewPropertyExt;->BACKGROUND:Lmiui/android/animation/property/ViewPropertyExt$BackgroundProperty;

    aput-object v2, v1, v0

    const/high16 v0, 0x3b800000    # 0.00390625f

    invoke-virtual {p0, v0, v1}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;

    .line 57
    new-array v0, v5, [Lmiui/android/animation/property/FloatProperty;

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_X:Lmiui/android/animation/property/ViewProperty;

    aput-object v1, v0, v3

    sget-object v1, Lmiui/android/animation/property/ViewProperty;->SCALE_Y:Lmiui/android/animation/property/ViewProperty;

    aput-object v1, v0, v4

    const v1, 0x3b03126f    # 0.002f

    invoke-virtual {p0, v1, v0}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;

    .line 59
    return-void
.end method


# virtual methods
.method public allowAnimRun()Z
    .locals 1

    .line 82
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public abstract clean()V
.end method

.method public executeOnInitialized(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "task"    # Ljava/lang/Runnable;

    .line 132
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    invoke-virtual {p0, p1}, Lmiui/android/animation/IAnimTarget;->post(Ljava/lang/Runnable;)V

    .line 133
    return-void
.end method

.method public getDefaultMinVisible()F
    .locals 1

    .line 200
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public getId()I
    .locals 1

    .line 86
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget v0, p0, Lmiui/android/animation/IAnimTarget;->id:I

    return v0
.end method

.method public getIntValue(Lmiui/android/animation/property/IIntValueProperty;)I
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/IIntValueProperty;

    .line 163
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 164
    .local v0, "targetObj":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    .line 165
    invoke-interface {p1, v0}, Lmiui/android/animation/property/IIntValueProperty;->getIntValue(Ljava/lang/Object;)I

    move-result v1

    return v1

    .line 167
    :cond_0
    const v1, 0x7fffffff

    return v1
.end method

.method public getLocationOnScreen([I)V
    .locals 2
    .param p1, "location"    # [I

    .line 144
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, p1, v0

    aput v1, p1, v1

    .line 145
    return-void
.end method

.method public getMinVisibleChange(Ljava/lang/Object;)F
    .locals 3
    .param p1, "key"    # Ljava/lang/Object;

    .line 98
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->mMinVisibleChanges:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 99
    .local v0, "threshold":Ljava/lang/Float;
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    return v1

    .line 101
    :cond_0
    iget v1, p0, Lmiui/android/animation/IAnimTarget;->mDefaultMinVisible:F

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_1

    .line 102
    return v1

    .line 104
    :cond_1
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getDefaultMinVisible()F

    move-result v1

    return v1
.end method

.method public getNotifier()Lmiui/android/animation/listener/ListenerNotifier;
    .locals 1

    .line 65
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->notifyManager:Lmiui/android/animation/internal/NotifyManager;

    invoke-virtual {v0}, Lmiui/android/animation/internal/NotifyManager;->getNotifier()Lmiui/android/animation/listener/ListenerNotifier;

    move-result-object v0

    return-object v0
.end method

.method public abstract getTargetObject()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getValue(Lmiui/android/animation/property/FloatProperty;)F
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 148
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 149
    .local v0, "targetObj":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p1, v0}, Lmiui/android/animation/property/FloatProperty;->getValue(Ljava/lang/Object;)F

    move-result v1

    return v1

    .line 152
    :cond_0
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    return v1
.end method

.method public getVelocity(Lmiui/android/animation/property/FloatProperty;)D
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 178
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/internal/AnimManager;->getVelocity(Lmiui/android/animation/property/FloatProperty;)D

    move-result-wide v0

    return-wide v0
.end method

.method public hasFlags(J)Z
    .locals 2
    .param p1, "flags"    # J

    .line 94
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-wide v0, p0, Lmiui/android/animation/IAnimTarget;->mFlags:J

    invoke-static {v0, v1, p1, p2}, Lmiui/android/animation/utils/CommonUtils;->hasFlags(JJ)Z

    move-result v0

    return v0
.end method

.method public varargs isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z
    .locals 1
    .param p1, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 75
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    invoke-virtual {v0, p1}, Lmiui/android/animation/internal/AnimManager;->isAnimRunning([Lmiui/android/animation/property/FloatProperty;)Z

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 1

    .line 136
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public onFrameEnd(Z)V
    .locals 0
    .param p1, "isFinished"    # Z

    .line 141
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    return-void
.end method

.method public post(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "task"    # Ljava/lang/Runnable;

    .line 188
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    iget-wide v0, v0, Lmiui/android/animation/internal/TargetHandler;->threadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 189
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 191
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->handler:Lmiui/android/animation/internal/TargetHandler;

    invoke-virtual {v0, p1}, Lmiui/android/animation/internal/TargetHandler;->post(Ljava/lang/Runnable;)Z

    .line 193
    :goto_0
    return-void
.end method

.method public setDefaultMinVisibleChange(F)Lmiui/android/animation/IAnimTarget;
    .locals 0
    .param p1, "defaultMinVisible"    # F

    .line 108
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iput p1, p0, Lmiui/android/animation/IAnimTarget;->mDefaultMinVisible:F

    .line 109
    return-object p0
.end method

.method public setFlags(J)V
    .locals 0
    .param p1, "flags"    # J

    .line 90
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iput-wide p1, p0, Lmiui/android/animation/IAnimTarget;->mFlags:J

    .line 91
    return-void
.end method

.method public setIntValue(Lmiui/android/animation/property/IIntValueProperty;I)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/IIntValueProperty;
    .param p2, "value"    # I

    .line 171
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 172
    .local v0, "targetObj":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    .line 173
    invoke-interface {p1, v0, p2}, Lmiui/android/animation/property/IIntValueProperty;->setIntValue(Ljava/lang/Object;I)V

    .line 175
    :cond_0
    return-void
.end method

.method public varargs setMinVisibleChange(F[Ljava/lang/String;)Lmiui/android/animation/IAnimTarget;
    .locals 4
    .param p1, "minVisible"    # F
    .param p2, "names"    # [Ljava/lang/String;

    .line 125
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    .line 126
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Lmiui/android/animation/property/ValueProperty;

    invoke-direct {v3, v2}, Lmiui/android/animation/property/ValueProperty;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, p1}, Lmiui/android/animation/IAnimTarget;->setMinVisibleChange(Ljava/lang/Object;F)Lmiui/android/animation/IAnimTarget;

    .line 125
    .end local v2    # "name":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 128
    :cond_0
    return-object p0
.end method

.method public varargs setMinVisibleChange(F[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IAnimTarget;
    .locals 5
    .param p1, "minVisible"    # F
    .param p2, "properties"    # [Lmiui/android/animation/property/FloatProperty;

    .line 113
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    .line 114
    .local v2, "prop":Lmiui/android/animation/property/FloatProperty;
    iget-object v3, p0, Lmiui/android/animation/IAnimTarget;->mMinVisibleChanges:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    .end local v2    # "prop":Lmiui/android/animation/property/FloatProperty;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    :cond_0
    return-object p0
.end method

.method public setMinVisibleChange(Ljava/lang/Object;F)Lmiui/android/animation/IAnimTarget;
    .locals 2
    .param p1, "key"    # Ljava/lang/Object;
    .param p2, "minVisible"    # F

    .line 120
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->mMinVisibleChanges:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    return-object p0
.end method

.method public setToNotify(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 1
    .param p1, "state"    # Lmiui/android/animation/controller/AnimState;
    .param p2, "configLink"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 71
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->notifyManager:Lmiui/android/animation/internal/NotifyManager;

    invoke-virtual {v0, p1, p2}, Lmiui/android/animation/internal/NotifyManager;->setToNotify(Lmiui/android/animation/controller/AnimState;Lmiui/android/animation/base/AnimConfigLink;)V

    .line 72
    return-void
.end method

.method public setValue(Lmiui/android/animation/property/FloatProperty;F)V
    .locals 3
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # F

    .line 156
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v0

    .line 157
    .local v0, "targetObj":Ljava/lang/Object;, "TT;"
    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 158
    invoke-virtual {p1, v0, p2}, Lmiui/android/animation/property/FloatProperty;->setValue(Ljava/lang/Object;F)V

    .line 160
    :cond_0
    return-void
.end method

.method public setVelocity(Lmiui/android/animation/property/FloatProperty;D)V
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "v"    # D

    .line 182
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->animManager:Lmiui/android/animation/internal/AnimManager;

    double-to-float v1, p2

    invoke-virtual {v0, p1, v1}, Lmiui/android/animation/internal/AnimManager;->setVelocity(Lmiui/android/animation/property/FloatProperty;F)V

    .line 185
    :cond_0
    return-void
.end method

.method public shouldUseIntValue(Lmiui/android/animation/property/FloatProperty;)Z
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 204
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    instance-of v0, p1, Lmiui/android/animation/property/IIntValueProperty;

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 216
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IAnimTarget{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lmiui/android/animation/IAnimTarget;->getTargetObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trackVelocity(Lmiui/android/animation/property/FloatProperty;D)V
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "value"    # D

    .line 211
    .local p0, "this":Lmiui/android/animation/IAnimTarget;, "Lmiui/android/animation/IAnimTarget<TT;>;"
    iget-object v0, p0, Lmiui/android/animation/IAnimTarget;->mTracker:Lmiui/android/animation/internal/TargetVelocityTracker;

    invoke-virtual {v0, p0, p1, p2, p3}, Lmiui/android/animation/internal/TargetVelocityTracker;->trackVelocity(Lmiui/android/animation/IAnimTarget;Lmiui/android/animation/property/FloatProperty;D)V

    .line 212
    return-void
.end method
