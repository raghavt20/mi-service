.class public interface abstract Lmiui/android/animation/IHoverStyle;
.super Ljava/lang/Object;
.source "IHoverStyle.java"

# interfaces
.implements Lmiui/android/animation/IStateContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/IHoverStyle$HoverEffect;,
        Lmiui/android/animation/IHoverStyle$HoverType;
    }
.end annotation


# virtual methods
.method public abstract addMagicPoint(Landroid/graphics/Point;)V
.end method

.method public abstract clearMagicPoint()V
.end method

.method public abstract clearTintColor()Lmiui/android/animation/IHoverStyle;
.end method

.method public varargs abstract handleHoverOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract hoverEnter([Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract hoverExit([Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract hoverMove(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public abstract ignoreHoverOf(Landroid/view/View;)V
.end method

.method public abstract isMagicView()Z
.end method

.method public abstract onMotionEvent(Landroid/view/MotionEvent;)V
.end method

.method public varargs abstract onMotionEventEx(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract setAlpha(F[Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setBackgroundColor(FFFF)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setBackgroundColor(I)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setCorner(F)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setEffect(Lmiui/android/animation/IHoverStyle$HoverEffect;)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setHoverEnter()V
.end method

.method public abstract setHoverExit()V
.end method

.method public abstract setMagicView(Z)V
.end method

.method public abstract setParentView(Landroid/view/View;)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setPointerHide(Z)V
.end method

.method public abstract setPointerShape(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setPointerShapeType(I)V
.end method

.method public varargs abstract setScale(F[Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setTint(FFFF)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setTint(I)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setTintMode(I)Lmiui/android/animation/IHoverStyle;
.end method

.method public varargs abstract setTranslate(F[Lmiui/android/animation/IHoverStyle$HoverType;)Lmiui/android/animation/IHoverStyle;
.end method

.method public abstract setWrapped(Z)V
.end method
