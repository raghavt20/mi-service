.class public Lmiui/android/animation/font/FontWeightProperty;
.super Lmiui/android/animation/property/ViewProperty;
.source "FontWeightProperty.java"

# interfaces
.implements Lmiui/android/animation/property/ISpecificProperty;


# static fields
.field private static final NAME:Ljava/lang/String; = "fontweight"


# instance fields
.field private mCurWeight:F

.field private mFontType:I

.field private mTextViewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/widget/TextView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "fontType"    # I

    .line 27
    const-string v0, "fontweight"

    invoke-direct {p0, v0}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V

    .line 24
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mCurWeight:F

    .line 28
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    .line 29
    iput p2, p0, Lmiui/android/animation/font/FontWeightProperty;->mFontType:I

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .line 60
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 61
    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 62
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 63
    :cond_2
    move-object v2, p1

    check-cast v2, Lmiui/android/animation/font/FontWeightProperty;

    .line 64
    .local v2, "that":Lmiui/android/animation/font/FontWeightProperty;
    iget-object v3, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 65
    .local v3, "textView":Landroid/widget/TextView;
    iget-object v4, v2, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 66
    .local v4, "textView1":Landroid/widget/TextView;
    if-eqz v3, :cond_3

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_0
    return v0

    .line 61
    .end local v2    # "that":Lmiui/android/animation/font/FontWeightProperty;
    .end local v3    # "textView":Landroid/widget/TextView;
    .end local v4    # "textView1":Landroid/widget/TextView;
    :cond_4
    :goto_1
    return v1
.end method

.method public getScaledTextSize()F
    .locals 3

    .line 37
    iget-object v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 40
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v1, v2

    .line 39
    return v1

    .line 42
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public getSpecificValue(F)F
    .locals 5
    .param p1, "value"    # F

    .line 79
    iget-object v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    .local v0, "textView":Landroid/widget/TextView;
    sget v1, Lmiui/android/animation/font/VarFontUtils;->MIN_WGHT:I

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lmiui/android/animation/font/VarFontUtils;->getSysFontScale(Landroid/content/Context;)I

    move-result v1

    .line 82
    .local v1, "sysScale":I
    float-to-int v2, p1

    invoke-virtual {p0}, Lmiui/android/animation/font/FontWeightProperty;->getScaledTextSize()F

    move-result v3

    iget v4, p0, Lmiui/android/animation/font/FontWeightProperty;->mFontType:I

    invoke-static {v2, v3, v4, v1}, Lmiui/android/animation/font/VarFontUtils;->getScaleWght(IFII)I

    move-result v2

    int-to-float v2, v2

    return v2

    .line 85
    .end local v1    # "sysScale":I
    :cond_0
    return p1
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .line 33
    iget-object v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public getValue(Landroid/view/View;)F
    .locals 1
    .param p1, "object"    # Landroid/view/View;

    .line 47
    iget v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mCurWeight:F

    return v0
.end method

.method public bridge synthetic getValue(Ljava/lang/Object;)F
    .locals 0

    .line 17
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lmiui/android/animation/font/FontWeightProperty;->getValue(Landroid/view/View;)F

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 3

    .line 70
    iget-object v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 71
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 72
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1, v0}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v1

    return v1

    .line 74
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    filled-new-array {v1, v2}, [Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method public setValue(Landroid/view/View;F)V
    .locals 2
    .param p1, "object"    # Landroid/view/View;
    .param p2, "value"    # F

    .line 52
    iput p2, p0, Lmiui/android/animation/font/FontWeightProperty;->mCurWeight:F

    .line 53
    iget-object v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mTextViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 54
    .local v0, "textView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 55
    float-to-int v1, p2

    invoke-static {v0, v1}, Lmiui/android/animation/font/VarFontUtils;->setVariationFont(Landroid/widget/TextView;I)V

    .line 57
    :cond_0
    return-void
.end method

.method public bridge synthetic setValue(Ljava/lang/Object;F)V
    .locals 0

    .line 17
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lmiui/android/animation/font/FontWeightProperty;->setValue(Landroid/view/View;F)V

    return-void
.end method
