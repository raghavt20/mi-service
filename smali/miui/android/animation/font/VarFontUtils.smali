.class public Lmiui/android/animation/font/VarFontUtils;
.super Ljava/lang/Object;
.source "VarFontUtils.java"


# static fields
.field private static final DEFAULT_WGHT:I = 0x32

.field private static final FORMAT_WGHT:Ljava/lang/String; = "\'wght\' "

.field private static final IS_USING_VAR_FONT:Z

.field private static final KEY_FONT_WEIGHT:Ljava/lang/String; = "key_miui_font_weight_scale"

.field public static final MIN_WGHT:I

.field private static final MITYPE_WGHT:[I

.field private static final MIUI_WGHT:[I

.field private static final RULES:[[[I

.field private static final THEME_FONT_DIR:Ljava/lang/String; = "/data/system/theme/fonts/"


# direct methods
.method static constructor <clinit>()V
    .locals 32

    .line 66
    invoke-static {}, Lmiui/android/animation/font/VarFontUtils;->isUsingThemeFont()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x6

    const/4 v3, 0x0

    if-nez v0, :cond_0

    .line 67
    invoke-static {}, Lmiuix/animation/utils/DeviceUtils;->getTotalRam()I

    move-result v0

    if-le v0, v2, :cond_0

    .line 68
    invoke-static {}, Lmiui/android/animation/font/VarFontUtils;->isFontAnimDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    invoke-static {}, Lmiuix/animation/utils/DeviceUtils;->getDeviceLevel()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    sput-boolean v0, Lmiui/android/animation/font/VarFontUtils;->IS_USING_VAR_FONT:Z

    .line 71
    if-eqz v0, :cond_1

    .line 72
    const/16 v0, 0xa

    new-array v4, v0, [I

    fill-array-data v4, :array_0

    sput-object v4, Lmiui/android/animation/font/VarFontUtils;->MIUI_WGHT:[I

    .line 76
    new-array v4, v0, [I

    fill-array-data v4, :array_1

    sput-object v4, Lmiui/android/animation/font/VarFontUtils;->MITYPE_WGHT:[I

    .line 80
    sput v0, Lmiui/android/animation/font/VarFontUtils;->MIN_WGHT:I

    .line 82
    const/4 v0, 0x3

    new-array v4, v0, [[[I

    sput-object v4, Lmiui/android/animation/font/VarFontUtils;->RULES:[[[I

    .line 84
    const/4 v5, 0x5

    filled-new-array {v3, v5}, [I

    move-result-object v6

    filled-new-array {v3, v5}, [I

    move-result-object v7

    filled-new-array {v1, v2}, [I

    move-result-object v8

    const/4 v15, 0x2

    filled-new-array {v15, v2}, [I

    move-result-object v9

    const/4 v14, 0x7

    filled-new-array {v15, v14}, [I

    move-result-object v10

    const/16 v13, 0x8

    filled-new-array {v0, v13}, [I

    move-result-object v11

    const/4 v12, 0x4

    filled-new-array {v12, v13}, [I

    move-result-object v16

    const/16 v1, 0x9

    filled-new-array {v5, v1}, [I

    move-result-object v17

    filled-new-array {v2, v1}, [I

    move-result-object v18

    filled-new-array {v14, v1}, [I

    move-result-object v19

    move v1, v12

    move-object/from16 v12, v16

    move-object/from16 v13, v17

    move-object/from16 v14, v18

    move v2, v15

    move-object/from16 v15, v19

    filled-new-array/range {v6 .. v15}, [[I

    move-result-object v6

    aput-object v6, v4, v3

    .line 97
    filled-new-array {v3, v2}, [I

    move-result-object v22

    filled-new-array {v3, v0}, [I

    move-result-object v23

    const/4 v6, 0x1

    filled-new-array {v6, v1}, [I

    move-result-object v24

    filled-new-array {v6, v5}, [I

    move-result-object v25

    const/4 v6, 0x6

    filled-new-array {v2, v6}, [I

    move-result-object v26

    const/4 v7, 0x7

    filled-new-array {v2, v7}, [I

    move-result-object v27

    const/16 v8, 0x8

    filled-new-array {v0, v8}, [I

    move-result-object v28

    const/16 v9, 0x9

    filled-new-array {v1, v9}, [I

    move-result-object v29

    filled-new-array {v5, v9}, [I

    move-result-object v30

    filled-new-array {v6, v9}, [I

    move-result-object v31

    filled-new-array/range {v22 .. v31}, [[I

    move-result-object v6

    const/4 v9, 0x1

    aput-object v6, v4, v9

    .line 110
    filled-new-array {v3, v5}, [I

    move-result-object v20

    filled-new-array {v9, v5}, [I

    move-result-object v21

    filled-new-array {v2, v5}, [I

    move-result-object v22

    const/4 v3, 0x6

    filled-new-array {v0, v3}, [I

    move-result-object v23

    filled-new-array {v0, v3}, [I

    move-result-object v24

    filled-new-array {v1, v7}, [I

    move-result-object v25

    filled-new-array {v5, v8}, [I

    move-result-object v26

    filled-new-array {v3, v8}, [I

    move-result-object v27

    filled-new-array {v7, v8}, [I

    move-result-object v28

    const/16 v0, 0x9

    filled-new-array {v8, v0}, [I

    move-result-object v29

    filled-new-array/range {v20 .. v29}, [[I

    move-result-object v0

    aput-object v0, v4, v2

    goto :goto_1

    .line 123
    :cond_1
    sput v3, Lmiui/android/animation/font/VarFontUtils;->MIN_WGHT:I

    .line 124
    new-array v0, v3, [I

    sput-object v0, Lmiui/android/animation/font/VarFontUtils;->MITYPE_WGHT:[I

    sput-object v0, Lmiui/android/animation/font/VarFontUtils;->MIUI_WGHT:[I

    .line 125
    new-array v0, v3, [[[I

    sput-object v0, Lmiui/android/animation/font/VarFontUtils;->RULES:[[[I

    .line 127
    :goto_1
    return-void

    :array_0
    .array-data 4
        0x96
        0xc8
        0xfa
        0x131
        0x154
        0x190
        0x1e0
        0x21c
        0x276
        0x2bc
    .end array-data

    :array_1
    .array-data 4
        0xa
        0x14
        0x1e
        0x28
        0x32
        0x3c
        0x46
        0x50
        0x5a
        0x64
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getScaleWght(IFII)I
    .locals 8
    .param p0, "fontWeight"    # I
    .param p1, "scaleTextSize"    # F
    .param p2, "fontType"    # I
    .param p3, "scale"    # I

    .line 132
    sget-boolean v0, Lmiui/android/animation/font/VarFontUtils;->IS_USING_VAR_FONT:Z

    if-nez v0, :cond_0

    .line 133
    return p0

    .line 135
    :cond_0
    invoke-static {p0, p1}, Lmiui/android/animation/font/VarFontUtils;->getWghtRange(IF)[I

    move-result-object v0

    .line 137
    .local v0, "range":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    invoke-static {v1, p2}, Lmiui/android/animation/font/VarFontUtils;->getWghtByType(II)I

    move-result v1

    .line 138
    .local v1, "startWght":I
    invoke-static {p0, p2}, Lmiui/android/animation/font/VarFontUtils;->getWghtByType(II)I

    move-result v2

    .line 139
    .local v2, "midWght":I
    const/4 v3, 0x1

    aget v3, v0, v3

    invoke-static {v3, p2}, Lmiui/android/animation/font/VarFontUtils;->getWghtByType(II)I

    move-result v3

    .line 141
    .local v3, "endWght":I
    move v4, v2

    .line 142
    .local v4, "wght":I
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x42480000    # 50.0f

    const/16 v7, 0x32

    if-ge p3, v7, :cond_1

    .line 143
    int-to-float v7, p3

    div-float/2addr v7, v6

    .line 144
    .local v7, "t":F
    sub-float/2addr v5, v7

    int-to-float v6, v1

    mul-float/2addr v5, v6

    int-to-float v6, v2

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    float-to-int v4, v5

    .end local v7    # "t":F
    goto :goto_0

    .line 145
    :cond_1
    if-le p3, v7, :cond_2

    .line 146
    add-int/lit8 v7, p3, -0x32

    int-to-float v7, v7

    div-float/2addr v7, v6

    .line 147
    .restart local v7    # "t":F
    sub-float/2addr v5, v7

    int-to-float v6, v2

    mul-float/2addr v5, v6

    int-to-float v6, v3

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    float-to-int v4, v5

    goto :goto_1

    .line 145
    .end local v7    # "t":F
    :cond_2
    :goto_0
    nop

    .line 149
    :goto_1
    return v4
.end method

.method static getSysFontScale(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .line 164
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 165
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "key_miui_font_weight_scale"

    const/16 v2, 0x32

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private static getWghtArray(I)[I
    .locals 1
    .param p0, "fontType"    # I

    .line 159
    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lmiui/android/animation/font/VarFontUtils;->MIUI_WGHT:[I

    goto :goto_1

    :cond_1
    :goto_0
    sget-object v0, Lmiui/android/animation/font/VarFontUtils;->MITYPE_WGHT:[I

    :goto_1
    return-object v0
.end method

.method private static getWghtByType(II)I
    .locals 1
    .param p0, "fontWeight"    # I
    .param p1, "fontType"    # I

    .line 179
    invoke-static {p1}, Lmiui/android/animation/font/VarFontUtils;->getWghtArray(I)[I

    move-result-object v0

    aget v0, v0, p0

    return v0
.end method

.method private static getWghtRange(IF)[I
    .locals 2
    .param p0, "fontWeight"    # I
    .param p1, "textSize"    # F

    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "idx":I
    const/high16 v1, 0x41a00000    # 20.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 171
    const/4 v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    const/4 v1, 0x0

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    const/high16 v1, 0x41400000    # 12.0f

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 173
    const/4 v0, 0x2

    .line 175
    :cond_1
    :goto_0
    sget-object v1, Lmiui/android/animation/font/VarFontUtils;->RULES:[[[I

    aget-object v1, v1, v0

    aget-object v1, v1, p0

    return-object v1
.end method

.method private static isFontAnimDisabled()Z
    .locals 3

    .line 57
    :try_start_0
    const-string v0, "ro.miui.ui.font.animation"

    invoke-static {v0}, Lmiui/android/animation/utils/CommonUtils;->readProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "value":Ljava/lang/String;
    const-string v1, "disable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    .line 59
    .end local v0    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "miuix_anim"

    const-string v2, "isFontAnimationEnabled failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    .end local v0    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return v0
.end method

.method private static isUsingThemeFont()Z
    .locals 5

    .line 43
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/theme/fonts/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "dir":Ljava/io/File;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "files":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v3, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 51
    .end local v2    # "files":[Ljava/lang/String;
    :cond_1
    goto :goto_0

    .line 49
    :catch_0
    move-exception v2

    .line 50
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "miuix_anim"

    const-string v4, "isUsingThemeFont, failed to check theme font directory"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 52
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_0
    return v1
.end method

.method static setVariationFont(Landroid/widget/TextView;I)V
    .locals 2
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "wght"    # I

    .line 153
    sget-boolean v0, Lmiui/android/animation/font/VarFontUtils;->IS_USING_VAR_FONT:Z

    if-eqz v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\'wght\' "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setFontVariationSettings(Ljava/lang/String;)Z

    .line 156
    :cond_0
    return-void
.end method
