public class miui.android.animation.font.FontWeightProperty extends miui.android.animation.property.ViewProperty implements miui.android.animation.property.ISpecificProperty {
	 /* .source "FontWeightProperty.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String NAME;
	 /* # instance fields */
	 private Float mCurWeight;
	 private Integer mFontType;
	 private java.lang.ref.WeakReference mTextViewRef;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/ref/WeakReference<", */
	 /* "Landroid/widget/TextView;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
public miui.android.animation.font.FontWeightProperty ( ) {
/* .locals 1 */
/* .param p1, "view" # Landroid/widget/TextView; */
/* .param p2, "fontType" # I */
/* .line 27 */
final String v0 = "fontweight"; // const-string v0, "fontweight"
/* invoke-direct {p0, v0}, Lmiui/android/animation/property/ViewProperty;-><init>(Ljava/lang/String;)V */
/* .line 24 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mCurWeight:F */
/* .line 28 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
this.mTextViewRef = v0;
/* .line 29 */
/* iput p2, p0, Lmiui/android/animation/font/FontWeightProperty;->mFontType:I */
/* .line 30 */
return;
} // .end method
/* # virtual methods */
public Boolean equals ( java.lang.Object p0 ) {
/* .locals 6 */
/* .param p1, "object" # Ljava/lang/Object; */
/* .line 60 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, p1, :cond_0 */
/* .line 61 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_4
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
/* if-eq v2, v3, :cond_1 */
/* .line 62 */
} // :cond_1
v2 = /* invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z */
/* if-nez v2, :cond_2 */
/* .line 63 */
} // :cond_2
/* move-object v2, p1 */
/* check-cast v2, Lmiui/android/animation/font/FontWeightProperty; */
/* .line 64 */
/* .local v2, "that":Lmiui/android/animation/font/FontWeightProperty; */
v3 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v3 ).get ( ); // invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v3, Landroid/widget/TextView; */
/* .line 65 */
/* .local v3, "textView":Landroid/widget/TextView; */
v4 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v4 ).get ( ); // invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v4, Landroid/widget/TextView; */
/* .line 66 */
/* .local v4, "textView1":Landroid/widget/TextView; */
if ( v3 != null) { // if-eqz v3, :cond_3
v5 = (( java.lang.Object ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_3
} // :cond_3
/* move v0, v1 */
} // :goto_0
/* .line 61 */
} // .end local v2 # "that":Lmiui/android/animation/font/FontWeightProperty;
} // .end local v3 # "textView":Landroid/widget/TextView;
} // .end local v4 # "textView1":Landroid/widget/TextView;
} // :cond_4
} // :goto_1
} // .end method
public Float getScaledTextSize ( ) {
/* .locals 3 */
/* .line 37 */
v0 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/widget/TextView; */
/* .line 38 */
/* .local v0, "textView":Landroid/widget/TextView; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 39 */
v1 = (( android.widget.TextView ) v0 ).getTextSize ( ); // invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F
(( android.widget.TextView ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;
/* .line 40 */
(( android.content.res.Resources ) v2 ).getDisplayMetrics ( ); // invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v2, v2, Landroid/util/DisplayMetrics;->scaledDensity:F */
/* div-float/2addr v1, v2 */
/* .line 39 */
/* .line 42 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Float getSpecificValue ( Float p0 ) {
/* .locals 5 */
/* .param p1, "value" # F */
/* .line 79 */
v0 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/widget/TextView; */
/* .line 80 */
/* .local v0, "textView":Landroid/widget/TextView; */
/* int-to-float v1, v1 */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 81 */
(( android.widget.TextView ) v0 ).getContext ( ); // invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;
v1 = miui.android.animation.font.VarFontUtils .getSysFontScale ( v1 );
/* .line 82 */
/* .local v1, "sysScale":I */
/* float-to-int v2, p1 */
v3 = (( miui.android.animation.font.FontWeightProperty ) p0 ).getScaledTextSize ( ); // invoke-virtual {p0}, Lmiui/android/animation/font/FontWeightProperty;->getScaledTextSize()F
/* iget v4, p0, Lmiui/android/animation/font/FontWeightProperty;->mFontType:I */
v2 = miui.android.animation.font.VarFontUtils .getScaleWght ( v2,v3,v4,v1 );
/* int-to-float v2, v2 */
/* .line 85 */
} // .end local v1 # "sysScale":I
} // :cond_0
} // .end method
public android.widget.TextView getTextView ( ) {
/* .locals 1 */
/* .line 33 */
v0 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/widget/TextView; */
} // .end method
public Float getValue ( android.view.View p0 ) {
/* .locals 1 */
/* .param p1, "object" # Landroid/view/View; */
/* .line 47 */
/* iget v0, p0, Lmiui/android/animation/font/FontWeightProperty;->mCurWeight:F */
} // .end method
public Float getValue ( java.lang.Object p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 17 */
/* check-cast p1, Landroid/view/View; */
p1 = (( miui.android.animation.font.FontWeightProperty ) p0 ).getValue ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/font/FontWeightProperty;->getValue(Landroid/view/View;)F
} // .end method
public Integer hashCode ( ) {
/* .locals 3 */
/* .line 70 */
v0 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/widget/TextView; */
/* .line 71 */
/* .local v0, "textView":Landroid/widget/TextView; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
v1 = /* invoke-super {p0}, Ljava/lang/Object;->hashCode()I */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v1, v0}, [Ljava/lang/Object; */
v1 = java.util.Objects .hash ( v1 );
/* .line 74 */
} // :cond_0
v1 = /* invoke-super {p0}, Ljava/lang/Object;->hashCode()I */
java.lang.Integer .valueOf ( v1 );
v2 = this.mTextViewRef;
/* filled-new-array {v1, v2}, [Ljava/lang/Object; */
v1 = java.util.Objects .hash ( v1 );
} // .end method
public void setValue ( android.view.View p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "object" # Landroid/view/View; */
/* .param p2, "value" # F */
/* .line 52 */
/* iput p2, p0, Lmiui/android/animation/font/FontWeightProperty;->mCurWeight:F */
/* .line 53 */
v0 = this.mTextViewRef;
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Landroid/widget/TextView; */
/* .line 54 */
/* .local v0, "textView":Landroid/widget/TextView; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 55 */
/* float-to-int v1, p2 */
miui.android.animation.font.VarFontUtils .setVariationFont ( v0,v1 );
/* .line 57 */
} // :cond_0
return;
} // .end method
public void setValue ( java.lang.Object p0, Float p1 ) { //bridge//synthethic
/* .locals 0 */
/* .line 17 */
/* check-cast p1, Landroid/view/View; */
(( miui.android.animation.font.FontWeightProperty ) p0 ).setValue ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lmiui/android/animation/font/FontWeightProperty;->setValue(Landroid/view/View;F)V
return;
} // .end method
