public class miui.android.animation.font.VarFontUtils {
	 /* .source "VarFontUtils.java" */
	 /* # static fields */
	 private static final Integer DEFAULT_WGHT;
	 private static final java.lang.String FORMAT_WGHT;
	 private static final Boolean IS_USING_VAR_FONT;
	 private static final java.lang.String KEY_FONT_WEIGHT;
	 public static final Integer MIN_WGHT;
	 private static final MITYPE_WGHT;
	 private static final MIUI_WGHT;
	 private static final [I RULES;
	 private static final java.lang.String THEME_FONT_DIR;
	 /* # direct methods */
	 static miui.android.animation.font.VarFontUtils ( ) {
		 /* .locals 32 */
		 /* .line 66 */
		 v0 = 		 miui.android.animation.font.VarFontUtils .isUsingThemeFont ( );
		 int v1 = 1; // const/4 v1, 0x1
		 int v2 = 6; // const/4 v2, 0x6
		 int v3 = 0; // const/4 v3, 0x0
		 /* if-nez v0, :cond_0 */
		 /* .line 67 */
		 v0 = 		 miuix.animation.utils.DeviceUtils .getTotalRam ( );
		 /* if-le v0, v2, :cond_0 */
		 /* .line 68 */
		 v0 = 		 miui.android.animation.font.VarFontUtils .isFontAnimDisabled ( );
		 /* if-nez v0, :cond_0 */
		 /* .line 69 */
		 v0 = 		 miuix.animation.utils.DeviceUtils .getDeviceLevel ( );
		 /* if-lez v0, :cond_0 */
		 /* move v0, v1 */
	 } // :cond_0
	 /* move v0, v3 */
} // :goto_0
miui.android.animation.font.VarFontUtils.IS_USING_VAR_FONT = (v0!= 0);
/* .line 71 */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 72 */
	 /* const/16 v0, 0xa */
	 /* new-array v4, v0, [I */
	 /* fill-array-data v4, :array_0 */
	 /* .line 76 */
	 /* new-array v4, v0, [I */
	 /* fill-array-data v4, :array_1 */
	 /* .line 80 */
	 /* .line 82 */
	 int v0 = 3; // const/4 v0, 0x3
	 /* new-array v4, v0, [[[I */
	 /* .line 84 */
	 int v5 = 5; // const/4 v5, 0x5
	 /* filled-new-array {v3, v5}, [I */
	 /* filled-new-array {v3, v5}, [I */
	 /* filled-new-array {v1, v2}, [I */
	 int v15 = 2; // const/4 v15, 0x2
	 /* filled-new-array {v15, v2}, [I */
	 int v14 = 7; // const/4 v14, 0x7
	 /* filled-new-array {v15, v14}, [I */
	 /* const/16 v13, 0x8 */
	 /* filled-new-array {v0, v13}, [I */
	 int v12 = 4; // const/4 v12, 0x4
	 /* filled-new-array {v12, v13}, [I */
	 /* const/16 v1, 0x9 */
	 /* filled-new-array {v5, v1}, [I */
	 /* filled-new-array {v2, v1}, [I */
	 /* filled-new-array {v14, v1}, [I */
	 /* move v1, v12 */
	 /* move-object/from16 v12, v16 */
	 /* move-object/from16 v13, v17 */
	 /* move-object/from16 v14, v18 */
	 /* move v2, v15 */
	 /* move-object/from16 v15, v19 */
	 /* filled-new-array/range {v6 ..v15}, [[I */
	 /* aput-object v6, v4, v3 */
	 /* .line 97 */
	 /* filled-new-array {v3, v2}, [I */
	 /* filled-new-array {v3, v0}, [I */
	 int v6 = 1; // const/4 v6, 0x1
	 /* filled-new-array {v6, v1}, [I */
	 /* filled-new-array {v6, v5}, [I */
	 int v6 = 6; // const/4 v6, 0x6
	 /* filled-new-array {v2, v6}, [I */
	 int v7 = 7; // const/4 v7, 0x7
	 /* filled-new-array {v2, v7}, [I */
	 /* const/16 v8, 0x8 */
	 /* filled-new-array {v0, v8}, [I */
	 /* const/16 v9, 0x9 */
	 /* filled-new-array {v1, v9}, [I */
	 /* filled-new-array {v5, v9}, [I */
	 /* filled-new-array {v6, v9}, [I */
	 /* filled-new-array/range {v22 ..v31}, [[I */
	 int v9 = 1; // const/4 v9, 0x1
	 /* aput-object v6, v4, v9 */
	 /* .line 110 */
	 /* filled-new-array {v3, v5}, [I */
	 /* filled-new-array {v9, v5}, [I */
	 /* filled-new-array {v2, v5}, [I */
	 int v3 = 6; // const/4 v3, 0x6
	 /* filled-new-array {v0, v3}, [I */
	 /* filled-new-array {v0, v3}, [I */
	 /* filled-new-array {v1, v7}, [I */
	 /* filled-new-array {v5, v8}, [I */
	 /* filled-new-array {v3, v8}, [I */
	 /* filled-new-array {v7, v8}, [I */
	 /* const/16 v0, 0x9 */
	 /* filled-new-array {v8, v0}, [I */
	 /* filled-new-array/range {v20 ..v29}, [[I */
	 /* aput-object v0, v4, v2 */
	 /* .line 123 */
} // :cond_1
/* .line 124 */
/* new-array v0, v3, [I */
/* .line 125 */
/* new-array v0, v3, [[[I */
/* .line 127 */
} // :goto_1
return;
/* :array_0 */
/* .array-data 4 */
/* 0x96 */
/* 0xc8 */
/* 0xfa */
/* 0x131 */
/* 0x154 */
/* 0x190 */
/* 0x1e0 */
/* 0x21c */
/* 0x276 */
/* 0x2bc */
} // .end array-data
/* :array_1 */
/* .array-data 4 */
/* 0xa */
/* 0x14 */
/* 0x1e */
/* 0x28 */
/* 0x32 */
/* 0x3c */
/* 0x46 */
/* 0x50 */
/* 0x5a */
/* 0x64 */
} // .end array-data
} // .end method
private miui.android.animation.font.VarFontUtils ( ) {
/* .locals 0 */
/* .line 129 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
static Integer getScaleWght ( Integer p0, Float p1, Integer p2, Integer p3 ) {
/* .locals 8 */
/* .param p0, "fontWeight" # I */
/* .param p1, "scaleTextSize" # F */
/* .param p2, "fontType" # I */
/* .param p3, "scale" # I */
/* .line 132 */
/* sget-boolean v0, Lmiui/android/animation/font/VarFontUtils;->IS_USING_VAR_FONT:Z */
/* if-nez v0, :cond_0 */
/* .line 133 */
/* .line 135 */
} // :cond_0
miui.android.animation.font.VarFontUtils .getWghtRange ( p0,p1 );
/* .line 137 */
/* .local v0, "range":[I */
int v1 = 0; // const/4 v1, 0x0
/* aget v1, v0, v1 */
v1 = miui.android.animation.font.VarFontUtils .getWghtByType ( v1,p2 );
/* .line 138 */
/* .local v1, "startWght":I */
v2 = miui.android.animation.font.VarFontUtils .getWghtByType ( p0,p2 );
/* .line 139 */
/* .local v2, "midWght":I */
int v3 = 1; // const/4 v3, 0x1
/* aget v3, v0, v3 */
v3 = miui.android.animation.font.VarFontUtils .getWghtByType ( v3,p2 );
/* .line 141 */
/* .local v3, "endWght":I */
/* move v4, v2 */
/* .line 142 */
/* .local v4, "wght":I */
/* const/high16 v5, 0x3f800000 # 1.0f */
/* const/high16 v6, 0x42480000 # 50.0f */
/* const/16 v7, 0x32 */
/* if-ge p3, v7, :cond_1 */
/* .line 143 */
/* int-to-float v7, p3 */
/* div-float/2addr v7, v6 */
/* .line 144 */
/* .local v7, "t":F */
/* sub-float/2addr v5, v7 */
/* int-to-float v6, v1 */
/* mul-float/2addr v5, v6 */
/* int-to-float v6, v2 */
/* mul-float/2addr v6, v7 */
/* add-float/2addr v5, v6 */
/* float-to-int v4, v5 */
} // .end local v7 # "t":F
/* .line 145 */
} // :cond_1
/* if-le p3, v7, :cond_2 */
/* .line 146 */
/* add-int/lit8 v7, p3, -0x32 */
/* int-to-float v7, v7 */
/* div-float/2addr v7, v6 */
/* .line 147 */
/* .restart local v7 # "t":F */
/* sub-float/2addr v5, v7 */
/* int-to-float v6, v2 */
/* mul-float/2addr v5, v6 */
/* int-to-float v6, v3 */
/* mul-float/2addr v6, v7 */
/* add-float/2addr v5, v6 */
/* float-to-int v4, v5 */
/* .line 145 */
} // .end local v7 # "t":F
} // :cond_2
} // :goto_0
/* nop */
/* .line 149 */
} // :goto_1
} // .end method
static Integer getSysFontScale ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 164 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 165 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "key_miui_font_weight_scale"; // const-string v1, "key_miui_font_weight_scale"
/* const/16 v2, 0x32 */
v1 = android.provider.Settings$System .getInt ( v0,v1,v2 );
} // .end method
private static getWghtArray ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "fontType" # I */
/* .line 159 */
int v0 = 1; // const/4 v0, 0x1
/* if-eq p0, v0, :cond_1 */
int v0 = 2; // const/4 v0, 0x2
/* if-ne p0, v0, :cond_0 */
} // :cond_0
v0 = miui.android.animation.font.VarFontUtils.MIUI_WGHT;
} // :cond_1
} // :goto_0
v0 = miui.android.animation.font.VarFontUtils.MITYPE_WGHT;
} // :goto_1
} // .end method
private static Integer getWghtByType ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p0, "fontWeight" # I */
/* .param p1, "fontType" # I */
/* .line 179 */
miui.android.animation.font.VarFontUtils .getWghtArray ( p1 );
/* aget v0, v0, p0 */
} // .end method
private static getWghtRange ( Integer p0, Float p1 ) {
/* .locals 2 */
/* .param p0, "fontWeight" # I */
/* .param p1, "textSize" # F */
/* .line 169 */
int v0 = 0; // const/4 v0, 0x0
/* .line 170 */
/* .local v0, "idx":I */
/* const/high16 v1, 0x41a00000 # 20.0f */
/* cmpl-float v1, p1, v1 */
/* if-lez v1, :cond_0 */
/* .line 171 */
int v0 = 1; // const/4 v0, 0x1
/* .line 172 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* cmpl-float v1, p1, v1 */
/* if-lez v1, :cond_1 */
/* const/high16 v1, 0x41400000 # 12.0f */
/* cmpg-float v1, p1, v1 */
/* if-gez v1, :cond_1 */
/* .line 173 */
int v0 = 2; // const/4 v0, 0x2
/* .line 175 */
} // :cond_1
} // :goto_0
v1 = miui.android.animation.font.VarFontUtils.RULES;
/* aget-object v1, v1, v0 */
/* aget-object v1, v1, p0 */
} // .end method
private static Boolean isFontAnimDisabled ( ) {
/* .locals 3 */
/* .line 57 */
try { // :try_start_0
final String v0 = "ro.miui.ui.font.animation"; // const-string v0, "ro.miui.ui.font.animation"
miui.android.animation.utils.CommonUtils .readProp ( v0 );
/* .line 58 */
/* .local v0, "value":Ljava/lang/String; */
final String v1 = "disable"; // const-string v1, "disable"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 59 */
} // .end local v0 # "value":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* .line 60 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "miuix_anim"; // const-string v1, "miuix_anim"
final String v2 = "isFontAnimationEnabled failed"; // const-string v2, "isFontAnimationEnabled failed"
android.util.Log .w ( v1,v2,v0 );
/* .line 62 */
} // .end local v0 # "e":Ljava/lang/Exception;
int v0 = 0; // const/4 v0, 0x0
} // .end method
private static Boolean isUsingThemeFont ( ) {
/* .locals 5 */
/* .line 43 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/theme/fonts/"; // const-string v1, "/data/system/theme/fonts/"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 45 */
/* .local v0, "dir":Ljava/io/File; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 46 */
(( java.io.File ) v0 ).list ( ); // invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;
/* .line 47 */
/* .local v2, "files":[Ljava/lang/String; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* array-length v3, v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-lez v3, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 51 */
} // .end local v2 # "files":[Ljava/lang/String;
} // :cond_1
/* .line 49 */
/* :catch_0 */
/* move-exception v2 */
/* .line 50 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "miuix_anim"; // const-string v3, "miuix_anim"
final String v4 = "isUsingThemeFont, failed to check theme font directory"; // const-string v4, "isUsingThemeFont, failed to check theme font directory"
android.util.Log .w ( v3,v4,v2 );
/* .line 52 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
static void setVariationFont ( android.widget.TextView p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "textView" # Landroid/widget/TextView; */
/* .param p1, "wght" # I */
/* .line 153 */
/* sget-boolean v0, Lmiui/android/animation/font/VarFontUtils;->IS_USING_VAR_FONT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 154 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "\'wght\' "; // const-string v1, "\'wght\' "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.widget.TextView ) p0 ).setFontVariationSettings ( v0 ); // invoke-virtual {p0, v0}, Landroid/widget/TextView;->setFontVariationSettings(Ljava/lang/String;)Z
/* .line 156 */
} // :cond_0
return;
} // .end method
