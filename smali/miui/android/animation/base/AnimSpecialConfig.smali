.class public Lmiui/android/animation/base/AnimSpecialConfig;
.super Lmiui/android/animation/base/AnimConfig;
.source "AnimSpecialConfig.java"


# instance fields
.field public maxValue:D

.field public minValue:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lmiui/android/animation/base/AnimConfig;-><init>(Z)V

    .line 12
    return-void
.end method


# virtual methods
.method public getSpecialConfig(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 80
    return-object p0
.end method

.method public getSpecialConfig(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 0
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 62
    return-object p0
.end method

.method public queryAndCreateSpecial(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 74
    return-object p0
.end method

.method public queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 0
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 68
    return-object p0
.end method

.method public setMinAndMax(DD)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 0
    .param p1, "min"    # D
    .param p3, "max"    # D

    .line 15
    iput-wide p1, p0, Lmiui/android/animation/base/AnimSpecialConfig;->minValue:D

    .line 16
    iput-wide p3, p0, Lmiui/android/animation/base/AnimSpecialConfig;->maxValue:D

    .line 17
    return-object p0
.end method

.method public varargs setSpecial(Ljava/lang/String;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "delay"    # J
    .param p4, "velocity"    # [F

    .line 22
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p0

    move-wide v3, p2

    move-object v5, p4

    invoke-super/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 23
    return-object p0
.end method

.method public setSpecial(Ljava/lang/String;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 92
    return-object p0
.end method

.method public varargs setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "delay"    # J
    .param p5, "velocity"    # [F

    .line 35
    move-object v0, p0

    move-object v1, p0

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-super/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 36
    return-object p0
.end method

.method public varargs setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "velocity"    # [F

    .line 28
    const-wide/16 v3, -0x1

    move-object v0, p0

    move-object v1, p0

    move-object v2, p2

    move-object v5, p3

    invoke-super/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 29
    return-object p0
.end method

.method public varargs setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "delay"    # J
    .param p4, "velocity"    # [F

    .line 41
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p0

    move-wide v3, p2

    move-object v5, p4

    invoke-super/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 42
    return-object p0
.end method

.method public setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 86
    return-object p0
.end method

.method public varargs setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "delay"    # J
    .param p5, "velocity"    # [F

    .line 55
    move-object v0, p0

    move-object v1, p0

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-super/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 56
    return-object p0
.end method

.method public varargs setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "velocity"    # [F

    .line 48
    const-wide/16 v3, -0x1

    move-object v0, p0

    move-object v1, p0

    move-object v2, p2

    move-object v5, p3

    invoke-super/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 49
    return-object p0
.end method
