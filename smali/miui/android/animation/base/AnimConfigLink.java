public class miui.android.animation.base.AnimConfigLink {
	 /* .source "AnimConfigLink.java" */
	 /* # static fields */
	 private static final java.util.concurrent.atomic.AtomicInteger sIdGenerator;
	 /* # instance fields */
	 public final java.util.List configList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lmiui/android/animation/base/AnimConfig;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final Integer id;
private final miui.android.animation.base.AnimConfig mHeadConfig;
/* # direct methods */
static miui.android.animation.base.AnimConfigLink ( ) {
/* .locals 1 */
/* .line 16 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V */
return;
} // .end method
public miui.android.animation.base.AnimConfigLink ( ) {
/* .locals 1 */
/* .line 14 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 18 */
v0 = miui.android.animation.base.AnimConfigLink.sIdGenerator;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).getAndIncrement ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I
/* iput v0, p0, Lmiui/android/animation/base/AnimConfigLink;->id:I */
/* .line 20 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.configList = v0;
/* .line 21 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V */
this.mHeadConfig = v0;
return;
} // .end method
private void doClear ( ) {
/* .locals 1 */
/* .line 93 */
v0 = this.configList;
/* .line 94 */
v0 = this.mHeadConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).clear ( ); // invoke-virtual {v0}, Lmiui/android/animation/base/AnimConfig;->clear()V
/* .line 95 */
return;
} // .end method
public static miui.android.animation.base.AnimConfigLink linkConfig ( miui.android.animation.base.AnimConfig...p0 ) {
/* .locals 6 */
/* .param p0, "configs" # [Lmiui/android/animation/base/AnimConfig; */
/* .line 24 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfigLink; */
/* invoke-direct {v0}, Lmiui/android/animation/base/AnimConfigLink;-><init>()V */
/* .line 25 */
/* .local v0, "link":Lmiui/android/animation/base/AnimConfigLink; */
/* array-length v1, p0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_0 */
/* aget-object v4, p0, v3 */
/* .line 26 */
/* .local v4, "config":Lmiui/android/animation/base/AnimConfig; */
/* new-array v5, v2, [Z */
(( miui.android.animation.base.AnimConfigLink ) v0 ).add ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V
/* .line 25 */
} // .end local v4 # "config":Lmiui/android/animation/base/AnimConfig;
/* add-int/lit8 v3, v3, 0x1 */
/* .line 28 */
} // :cond_0
} // .end method
/* # virtual methods */
public void add ( miui.android.animation.base.AnimConfig p0, Boolean...p1 ) {
/* .locals 2 */
/* .param p1, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .param p2, "copy" # [Z */
/* .line 32 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = v0 = this.configList;
/* if-nez v0, :cond_1 */
/* .line 33 */
/* array-length v0, p2 */
/* if-lez v0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* aget-boolean v0, p2, v0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 34 */
/* new-instance v0, Lmiui/android/animation/base/AnimConfig; */
/* invoke-direct {v0, p1}, Lmiui/android/animation/base/AnimConfig;-><init>(Lmiui/android/animation/base/AnimConfig;)V */
/* .line 35 */
/* .local v0, "c":Lmiui/android/animation/base/AnimConfig; */
v1 = this.configList;
/* .line 36 */
} // .end local v0 # "c":Lmiui/android/animation/base/AnimConfig;
/* .line 37 */
} // :cond_0
v0 = this.configList;
/* .line 40 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void add ( miui.android.animation.base.AnimConfigLink p0, Boolean...p1 ) {
/* .locals 2 */
/* .param p1, "configLink" # Lmiui/android/animation/base/AnimConfigLink; */
/* .param p2, "copy" # [Z */
/* .line 43 */
/* if-nez p1, :cond_0 */
/* .line 44 */
return;
/* .line 46 */
} // :cond_0
v0 = this.configList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/base/AnimConfig; */
/* .line 47 */
/* .local v1, "config":Lmiui/android/animation/base/AnimConfig; */
(( miui.android.animation.base.AnimConfigLink ) p0 ).add ( v1, p2 ); // invoke-virtual {p0, v1, p2}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V
/* .line 48 */
} // .end local v1 # "config":Lmiui/android/animation/base/AnimConfig;
/* .line 49 */
} // :cond_1
return;
} // .end method
public void addTo ( miui.android.animation.base.AnimConfig p0 ) {
/* .locals 8 */
/* .param p1, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .line 73 */
v0 = this.configList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lmiui/android/animation/base/AnimConfig; */
/* .line 74 */
/* .local v1, "c":Lmiui/android/animation/base/AnimConfig; */
/* iget-wide v2, p1, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* iget-wide v4, v1, Lmiui/android/animation/base/AnimConfig;->delay:J */
java.lang.Math .max ( v2,v3,v4,v5 );
/* move-result-wide v2 */
/* iput-wide v2, p1, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* .line 75 */
v2 = this.ease;
/* .line 76 */
/* .local v2, "configEase":Lmiui/android/animation/utils/EaseManager$EaseStyle; */
v3 = this.ease;
/* .line 77 */
/* .local v3, "curEase":Lmiui/android/animation/utils/EaseManager$EaseStyle; */
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = miui.android.animation.base.AnimConfig.sDefEase;
/* if-eq v3, v4, :cond_0 */
/* move-object v4, v3 */
} // :cond_0
/* move-object v4, v2 */
} // :goto_1
(( miui.android.animation.base.AnimConfig ) p1 ).setEase ( v4 ); // invoke-virtual {p1, v4}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 78 */
v4 = this.listeners;
v5 = this.listeners;
(( java.util.HashSet ) v4 ).addAll ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 79 */
/* iget-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* iget-wide v6, v1, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* or-long/2addr v4, v6 */
/* iput-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* .line 80 */
/* iget v4, p1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* iget v5, v1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
v4 = miui.android.animation.internal.AnimConfigUtils .chooseSpeed ( v4,v5 );
/* iput v4, p1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* .line 81 */
/* iget-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
/* iget-wide v6, v1, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
java.lang.Math .max ( v4,v5,v6,v7 );
/* move-result-wide v4 */
/* iput-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
/* .line 82 */
/* iget v4, p1, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
/* iget v5, v1, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
v4 = java.lang.Math .max ( v4,v5 );
/* iput v4, p1, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
/* .line 83 */
(( miui.android.animation.base.AnimConfig ) p1 ).addSpecialConfigs ( v1 ); // invoke-virtual {p1, v1}, Lmiui/android/animation/base/AnimConfig;->addSpecialConfigs(Lmiui/android/animation/base/AnimConfig;)V
/* .line 84 */
} // .end local v1 # "c":Lmiui/android/animation/base/AnimConfig;
} // .end local v2 # "configEase":Lmiui/android/animation/utils/EaseManager$EaseStyle;
} // .end local v3 # "curEase":Lmiui/android/animation/utils/EaseManager$EaseStyle;
/* .line 85 */
} // :cond_1
return;
} // .end method
public void clear ( ) {
/* .locals 2 */
/* .line 88 */
/* invoke-direct {p0}, Lmiui/android/animation/base/AnimConfigLink;->doClear()V */
/* .line 89 */
v0 = this.configList;
v1 = this.mHeadConfig;
/* .line 90 */
return;
} // .end method
public void copy ( miui.android.animation.base.AnimConfigLink p0 ) {
/* .locals 2 */
/* .param p1, "configLink" # Lmiui/android/animation/base/AnimConfigLink; */
/* .line 66 */
/* invoke-direct {p0}, Lmiui/android/animation/base/AnimConfigLink;->doClear()V */
/* .line 67 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 68 */
v0 = this.configList;
v1 = this.configList;
/* .line 70 */
} // :cond_0
return;
} // .end method
public miui.android.animation.base.AnimConfig getHead ( ) {
/* .locals 2 */
/* .line 98 */
v0 = v0 = this.configList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 99 */
v0 = this.configList;
v1 = this.mHeadConfig;
/* .line 101 */
} // :cond_0
v0 = this.configList;
int v1 = 0; // const/4 v1, 0x0
/* check-cast v0, Lmiui/android/animation/base/AnimConfig; */
} // .end method
public void remove ( miui.android.animation.base.AnimConfig p0 ) {
/* .locals 2 */
/* .param p1, "config" # Lmiui/android/animation/base/AnimConfig; */
/* .line 56 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 57 */
v0 = this.configList;
/* .line 58 */
v0 = v0 = this.configList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 59 */
v0 = this.mHeadConfig;
(( miui.android.animation.base.AnimConfig ) v0 ).clear ( ); // invoke-virtual {v0}, Lmiui/android/animation/base/AnimConfig;->clear()V
/* .line 60 */
v0 = this.configList;
v1 = this.mHeadConfig;
/* .line 63 */
} // :cond_0
return;
} // .end method
public Integer size ( ) {
/* .locals 1 */
/* .line 52 */
v0 = v0 = this.configList;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 106 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AnimConfigLink{id = "; // const-string v1, "AnimConfigLink{id = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/base/AnimConfigLink;->id:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", configList="; // const-string v1, ", configList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.configList;
/* .line 108 */
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 106 */
} // .end method
