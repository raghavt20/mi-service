.class public Lmiui/android/animation/base/AnimConfigLink;
.super Ljava/lang/Object;
.source "AnimConfigLink.java"


# static fields
.field private static final sIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final configList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/android/animation/base/AnimConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final id:I

.field private final mHeadConfig:Lmiui/android/animation/base/AnimConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiui/android/animation/base/AnimConfigLink;->sIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget-object v0, Lmiui/android/animation/base/AnimConfigLink;->sIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lmiui/android/animation/base/AnimConfigLink;->id:I

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    .line 21
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfig;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->mHeadConfig:Lmiui/android/animation/base/AnimConfig;

    return-void
.end method

.method private doClear()V
    .locals 1

    .line 93
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 94
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->mHeadConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0}, Lmiui/android/animation/base/AnimConfig;->clear()V

    .line 95
    return-void
.end method

.method public static varargs linkConfig([Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/base/AnimConfigLink;
    .locals 6
    .param p0, "configs"    # [Lmiui/android/animation/base/AnimConfig;

    .line 24
    new-instance v0, Lmiui/android/animation/base/AnimConfigLink;

    invoke-direct {v0}, Lmiui/android/animation/base/AnimConfigLink;-><init>()V

    .line 25
    .local v0, "link":Lmiui/android/animation/base/AnimConfigLink;
    array-length v1, p0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p0, v3

    .line 26
    .local v4, "config":Lmiui/android/animation/base/AnimConfig;
    new-array v5, v2, [Z

    invoke-virtual {v0, v4, v5}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V

    .line 25
    .end local v4    # "config":Lmiui/android/animation/base/AnimConfig;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 28
    :cond_0
    return-object v0
.end method


# virtual methods
.method public varargs add(Lmiui/android/animation/base/AnimConfig;[Z)V
    .locals 2
    .param p1, "config"    # Lmiui/android/animation/base/AnimConfig;
    .param p2, "copy"    # [Z

    .line 32
    if-eqz p1, :cond_1

    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 33
    array-length v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-boolean v0, p2, v0

    if-eqz v0, :cond_0

    .line 34
    new-instance v0, Lmiui/android/animation/base/AnimConfig;

    invoke-direct {v0, p1}, Lmiui/android/animation/base/AnimConfig;-><init>(Lmiui/android/animation/base/AnimConfig;)V

    .line 35
    .local v0, "c":Lmiui/android/animation/base/AnimConfig;
    iget-object v1, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    .end local v0    # "c":Lmiui/android/animation/base/AnimConfig;
    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_1
    :goto_0
    return-void
.end method

.method public varargs add(Lmiui/android/animation/base/AnimConfigLink;[Z)V
    .locals 2
    .param p1, "configLink"    # Lmiui/android/animation/base/AnimConfigLink;
    .param p2, "copy"    # [Z

    .line 43
    if-nez p1, :cond_0

    .line 44
    return-void

    .line 46
    :cond_0
    iget-object v0, p1, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/base/AnimConfig;

    .line 47
    .local v1, "config":Lmiui/android/animation/base/AnimConfig;
    invoke-virtual {p0, v1, p2}, Lmiui/android/animation/base/AnimConfigLink;->add(Lmiui/android/animation/base/AnimConfig;[Z)V

    .line 48
    .end local v1    # "config":Lmiui/android/animation/base/AnimConfig;
    goto :goto_0

    .line 49
    :cond_1
    return-void
.end method

.method public addTo(Lmiui/android/animation/base/AnimConfig;)V
    .locals 8
    .param p1, "config"    # Lmiui/android/animation/base/AnimConfig;

    .line 73
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/android/animation/base/AnimConfig;

    .line 74
    .local v1, "c":Lmiui/android/animation/base/AnimConfig;
    iget-wide v2, p1, Lmiui/android/animation/base/AnimConfig;->delay:J

    iget-wide v4, v1, Lmiui/android/animation/base/AnimConfig;->delay:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p1, Lmiui/android/animation/base/AnimConfig;->delay:J

    .line 75
    iget-object v2, p1, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 76
    .local v2, "configEase":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    iget-object v3, v1, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 77
    .local v3, "curEase":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    if-eqz v3, :cond_0

    sget-object v4, Lmiui/android/animation/base/AnimConfig;->sDefEase:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    if-eq v3, v4, :cond_0

    move-object v4, v3

    goto :goto_1

    :cond_0
    move-object v4, v2

    :goto_1
    invoke-virtual {p1, v4}, Lmiui/android/animation/base/AnimConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 78
    iget-object v4, p1, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    iget-object v5, v1, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 79
    iget-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->flags:J

    iget-wide v6, v1, Lmiui/android/animation/base/AnimConfig;->flags:J

    or-long/2addr v4, v6

    iput-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->flags:J

    .line 80
    iget v4, p1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    iget v5, v1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    invoke-static {v4, v5}, Lmiui/android/animation/internal/AnimConfigUtils;->chooseSpeed(FF)F

    move-result v4

    iput v4, p1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    .line 81
    iget-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    iget-wide v6, v1, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, p1, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    .line 82
    iget v4, p1, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    iget v5, v1, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p1, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    .line 83
    invoke-virtual {p1, v1}, Lmiui/android/animation/base/AnimConfig;->addSpecialConfigs(Lmiui/android/animation/base/AnimConfig;)V

    .line 84
    .end local v1    # "c":Lmiui/android/animation/base/AnimConfig;
    .end local v2    # "configEase":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .end local v3    # "curEase":Lmiui/android/animation/utils/EaseManager$EaseStyle;
    goto :goto_0

    .line 85
    :cond_1
    return-void
.end method

.method public clear()V
    .locals 2

    .line 88
    invoke-direct {p0}, Lmiui/android/animation/base/AnimConfigLink;->doClear()V

    .line 89
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfigLink;->mHeadConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public copy(Lmiui/android/animation/base/AnimConfigLink;)V
    .locals 2
    .param p1, "configLink"    # Lmiui/android/animation/base/AnimConfigLink;

    .line 66
    invoke-direct {p0}, Lmiui/android/animation/base/AnimConfigLink;->doClear()V

    .line 67
    if-eqz p1, :cond_0

    .line 68
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    iget-object v1, p1, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 70
    :cond_0
    return-void
.end method

.method public getHead()Lmiui/android/animation/base/AnimConfig;
    .locals 2

    .line 98
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfigLink;->mHeadConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/base/AnimConfig;

    return-object v0
.end method

.method public remove(Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "config"    # Lmiui/android/animation/base/AnimConfig;

    .line 56
    if-eqz p1, :cond_0

    .line 57
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->mHeadConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-virtual {v0}, Lmiui/android/animation/base/AnimConfig;->clear()V

    .line 60
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfigLink;->mHeadConfig:Lmiui/android/animation/base/AnimConfig;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    return-void
.end method

.method public size()I
    .locals 1

    .line 52
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimConfigLink{id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmiui/android/animation/base/AnimConfigLink;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", configList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfigLink;->configList:Ljava/util/List;

    .line 108
    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    return-object v0
.end method
