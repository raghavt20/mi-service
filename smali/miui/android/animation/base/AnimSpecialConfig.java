public class miui.android.animation.base.AnimSpecialConfig extends miui.android.animation.base.AnimConfig {
	 /* .source "AnimSpecialConfig.java" */
	 /* # instance fields */
	 public Double maxValue;
	 public Double minValue;
	 /* # direct methods */
	 public miui.android.animation.base.AnimSpecialConfig ( ) {
		 /* .locals 1 */
		 /* .line 11 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* invoke-direct {p0, v0}, Lmiui/android/animation/base/AnimConfig;-><init>(Z)V */
		 /* .line 12 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public miui.android.animation.base.AnimSpecialConfig getSpecialConfig ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "name" # Ljava/lang/String; */
		 /* .annotation runtime Ljava/lang/Deprecated; */
	 } // .end annotation
	 /* .line 80 */
} // .end method
public miui.android.animation.base.AnimSpecialConfig getSpecialConfig ( miui.android.animation.property.FloatProperty p0 ) {
	 /* .locals 0 */
	 /* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
	 /* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 62 */
} // .end method
public miui.android.animation.base.AnimSpecialConfig queryAndCreateSpecial ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 74 */
} // .end method
public miui.android.animation.base.AnimSpecialConfig queryAndCreateSpecial ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 0 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 68 */
} // .end method
public miui.android.animation.base.AnimSpecialConfig setMinAndMax ( Double p0, Double p1 ) {
/* .locals 0 */
/* .param p1, "min" # D */
/* .param p3, "max" # D */
/* .line 15 */
/* iput-wide p1, p0, Lmiui/android/animation/base/AnimSpecialConfig;->minValue:D */
/* .line 16 */
/* iput-wide p3, p0, Lmiui/android/animation/base/AnimSpecialConfig;->maxValue:D */
/* .line 17 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, Long p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "delay" # J */
/* .param p4, "velocity" # [F */
/* .line 22 */
int v2 = 0; // const/4 v2, 0x0
/* move-object v0, p0 */
/* move-object v1, p0 */
/* move-wide v3, p2 */
/* move-object v5, p4 */
/* invoke-super/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 23 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 92 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Long p2, Float...p3 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "delay" # J */
/* .param p5, "velocity" # [F */
/* .line 35 */
/* move-object v0, p0 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-wide v3, p3 */
/* move-object v5, p5 */
/* invoke-super/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 36 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "velocity" # [F */
/* .line 28 */
/* const-wide/16 v3, -0x1 */
/* move-object v0, p0 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-object v5, p3 */
/* invoke-super/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 29 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, Long p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "delay" # J */
/* .param p4, "velocity" # [F */
/* .line 41 */
int v2 = 0; // const/4 v2, 0x0
/* move-object v0, p0 */
/* move-object v1, p0 */
/* move-wide v3, p2 */
/* move-object v5, p4 */
/* invoke-super/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 42 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
/* .locals 0 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 86 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Long p2, Float...p3 ) {
/* .locals 6 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "delay" # J */
/* .param p5, "velocity" # [F */
/* .line 55 */
/* move-object v0, p0 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-wide v3, p3 */
/* move-object v5, p5 */
/* invoke-super/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 56 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "velocity" # [F */
/* .line 48 */
/* const-wide/16 v3, -0x1 */
/* move-object v0, p0 */
/* move-object v1, p0 */
/* move-object v2, p2 */
/* move-object v5, p3 */
/* invoke-super/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 49 */
} // .end method
