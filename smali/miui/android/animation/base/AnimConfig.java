public class miui.android.animation.base.AnimConfig {
	 /* .source "AnimConfig.java" */
	 /* # static fields */
	 public static final Long FLAG_DELTA;
	 public static final Long FLAG_INIT;
	 public static final Long FLAG_INT;
	 public static final Integer TINT_ALPHA;
	 public static final Integer TINT_OPAQUE;
	 public static final miui.android.animation.utils.EaseManager$EaseStyle sDefEase;
	 /* # instance fields */
	 public Long delay;
	 public miui.android.animation.utils.EaseManager$EaseStyle ease;
	 public Long flags;
	 public Float fromSpeed;
	 public final java.util.HashSet listeners;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/HashSet<", */
	 /* "Lmiui/android/animation/listener/TransitionListener;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.Map mSpecialNameMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lmiui/android/animation/base/AnimSpecialConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Long minDuration;
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
} // .end field
public java.lang.Object tag;
public Integer tintMode;
/* # direct methods */
static miui.android.animation.base.AnimConfig ( ) {
/* .locals 2 */
/* .line 22 */
int v0 = 2; // const/4 v0, 0x2
/* new-array v0, v0, [F */
/* fill-array-data v0, :array_0 */
int v1 = -2; // const/4 v1, -0x2
miui.android.animation.utils.EaseManager .getStyle ( v1,v0 );
return;
/* :array_0 */
/* .array-data 4 */
/* 0x3f59999a # 0.85f */
/* 0x3e99999a # 0.3f */
} // .end array-data
} // .end method
public miui.android.animation.base.AnimConfig ( ) {
/* .locals 1 */
/* .line 62 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lmiui/android/animation/base/AnimConfig;-><init>(Z)V */
/* .line 63 */
return;
} // .end method
public miui.android.animation.base.AnimConfig ( ) {
/* .locals 1 */
/* .param p1, "src" # Lmiui/android/animation/base/AnimConfig; */
/* .line 76 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lmiui/android/animation/base/AnimConfig;-><init>(Z)V */
/* .line 77 */
(( miui.android.animation.base.AnimConfig ) p0 ).copy ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V
/* .line 78 */
return;
} // .end method
public miui.android.animation.base.AnimConfig ( ) {
/* .locals 1 */
/* .param p1, "isSpecial" # Z */
/* .line 65 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 37 */
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* iput v0, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* .line 66 */
/* if-nez p1, :cond_0 */
/* .line 67 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mSpecialNameMap = v0;
/* .line 68 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.listeners = v0;
/* .line 70 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mSpecialNameMap = v0;
/* .line 71 */
this.listeners = v0;
/* .line 73 */
} // :goto_0
return;
} // .end method
private miui.android.animation.base.AnimSpecialConfig queryAndCreateSpecial ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "create" # Z */
/* .line 245 */
v0 = this.mSpecialNameMap;
/* check-cast v0, Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 246 */
/* .local v0, "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* if-nez v0, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 247 */
/* new-instance v1, Lmiui/android/animation/base/AnimSpecialConfig; */
/* invoke-direct {v1}, Lmiui/android/animation/base/AnimSpecialConfig;-><init>()V */
/* move-object v0, v1 */
/* .line 248 */
v1 = this.mSpecialNameMap;
/* .line 250 */
} // :cond_0
} // .end method
private miui.android.animation.base.AnimSpecialConfig queryAndCreateSpecial ( miui.android.animation.property.FloatProperty p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "create" # Z */
/* .line 216 */
/* if-nez p1, :cond_0 */
/* .line 217 */
int v0 = 0; // const/4 v0, 0x0
/* .line 219 */
} // :cond_0
(( miui.android.animation.property.FloatProperty ) p1 ).getName ( ); // invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
/* invoke-direct {p0, v0, p2}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
} // .end method
/* # virtual methods */
public miui.android.animation.base.AnimConfig addListeners ( miui.android.animation.listener.TransitionListener...p0 ) {
/* .locals 1 */
/* .param p1, "l" # [Lmiui/android/animation/listener/TransitionListener; */
/* .line 132 */
v0 = this.listeners;
java.util.Collections .addAll ( v0,p1 );
/* .line 133 */
} // .end method
public void addSpecialConfigs ( miui.android.animation.base.AnimConfig p0 ) {
/* .locals 2 */
/* .param p1, "src" # Lmiui/android/animation/base/AnimConfig; */
/* .line 142 */
v0 = this.mSpecialNameMap;
v1 = this.mSpecialNameMap;
/* .line 143 */
return;
} // .end method
public void clear ( ) {
/* .locals 4 */
/* .line 98 */
/* const-wide/16 v0, 0x0 */
/* iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* .line 99 */
int v2 = 0; // const/4 v2, 0x0
this.ease = v2;
/* .line 100 */
v3 = this.listeners;
(( java.util.HashSet ) v3 ).clear ( ); // invoke-virtual {v3}, Ljava/util/HashSet;->clear()V
/* .line 101 */
this.tag = v2;
/* .line 102 */
/* iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* .line 103 */
/* const v2, 0x7f7fffff # Float.MAX_VALUE */
/* iput v2, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* .line 104 */
/* iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
/* .line 105 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
/* .line 106 */
v0 = this.mSpecialNameMap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
/* .line 109 */
} // :cond_0
return;
} // .end method
public void copy ( miui.android.animation.base.AnimConfig p0 ) {
/* .locals 2 */
/* .param p1, "src" # Lmiui/android/animation/base/AnimConfig; */
/* .line 81 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* if-eq p1, p0, :cond_0 */
/* .line 82 */
/* iget-wide v0, p1, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* .line 83 */
v0 = this.ease;
this.ease = v0;
/* .line 84 */
v0 = this.listeners;
v1 = this.listeners;
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 85 */
v0 = this.tag;
this.tag = v0;
/* .line 86 */
/* iget-wide v0, p1, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->flags:J */
/* .line 87 */
/* iget v0, p1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* iput v0, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* .line 88 */
/* iget-wide v0, p1, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
/* iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
/* .line 89 */
/* iget v0, p1, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
/* iput v0, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
/* .line 90 */
v0 = this.mSpecialNameMap;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 91 */
/* .line 92 */
v0 = this.mSpecialNameMap;
v1 = this.mSpecialNameMap;
/* .line 95 */
} // :cond_0
return;
} // .end method
public miui.android.animation.base.AnimSpecialConfig getSpecialConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 223 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
} // .end method
public miui.android.animation.base.AnimSpecialConfig getSpecialConfig ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 204 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
} // .end method
public miui.android.animation.base.AnimSpecialConfig queryAndCreateSpecial ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 212 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
} // .end method
public miui.android.animation.base.AnimSpecialConfig queryAndCreateSpecial ( miui.android.animation.property.FloatProperty p0 ) {
/* .locals 1 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .line 208 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
} // .end method
public miui.android.animation.base.AnimConfig removeListeners ( miui.android.animation.listener.TransitionListener...p0 ) {
/* .locals 2 */
/* .param p1, "l" # [Lmiui/android/animation/listener/TransitionListener; */
/* .line 146 */
/* array-length v0, p1 */
/* if-nez v0, :cond_0 */
/* .line 147 */
v0 = this.listeners;
(( java.util.HashSet ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
/* .line 149 */
} // :cond_0
v0 = this.listeners;
java.util.Arrays .asList ( p1 );
(( java.util.HashSet ) v0 ).removeAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z
/* .line 151 */
} // :goto_0
} // .end method
public miui.android.animation.base.AnimConfig setDelay ( Long p0 ) {
/* .locals 0 */
/* .param p1, "d" # J */
/* .line 122 */
/* iput-wide p1, p0, Lmiui/android/animation/base/AnimConfig;->delay:J */
/* .line 123 */
} // .end method
public miui.android.animation.base.AnimConfig setEase ( Integer p0, Float...p1 ) {
/* .locals 1 */
/* .param p1, "style" # I */
/* .param p2, "factors" # [F */
/* .line 117 */
miui.android.animation.utils.EaseManager .getStyle ( p1,p2 );
this.ease = v0;
/* .line 118 */
} // .end method
public miui.android.animation.base.AnimConfig setEase ( miui.android.animation.utils.EaseManager$EaseStyle p0 ) {
/* .locals 0 */
/* .param p1, "e" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .line 112 */
this.ease = p1;
/* .line 113 */
} // .end method
public miui.android.animation.base.AnimConfig setFromSpeed ( Float p0 ) {
/* .locals 0 */
/* .param p1, "speed" # F */
/* .line 127 */
/* iput p1, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
/* .line 128 */
} // .end method
public miui.android.animation.base.AnimConfig setMinDuration ( Long p0 ) {
/* .locals 0 */
/* .param p1, "md" # J */
/* .line 160 */
/* iput-wide p1, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
/* .line 161 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, Long p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "delay" # J */
/* .param p4, "velocity" # [F */
/* .line 165 */
int v2 = 0; // const/4 v2, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-wide v3, p2 */
/* move-object v5, p4 */
/* invoke-virtual/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig; */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 236 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 237 */
v0 = this.mSpecialNameMap;
/* .line 239 */
} // :cond_0
v0 = this.mSpecialNameMap;
/* .line 241 */
} // :goto_0
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Long p2, Float...p3 ) {
/* .locals 7 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "delay" # J */
/* .param p5, "velocity" # [F */
/* .line 173 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 174 */
/* .local v0, "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* move-object v1, p0 */
/* move-object v2, v0 */
/* move-object v3, p2 */
/* move-wide v4, p3 */
/* move-object v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 175 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( java.lang.String p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "velocity" # [F */
/* .line 169 */
/* const-wide/16 v3, 0x0 */
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v5, p3 */
/* invoke-virtual/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig; */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, Long p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "delay" # J */
/* .param p4, "velocity" # [F */
/* .line 179 */
int v2 = 0; // const/4 v2, 0x0
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-wide v3, p2 */
/* move-object v5, p4 */
/* invoke-virtual/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig; */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, miui.android.animation.base.AnimSpecialConfig p1 ) {
/* .locals 2 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 227 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 228 */
v0 = this.mSpecialNameMap;
(( miui.android.animation.property.FloatProperty ) p1 ).getName ( ); // invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
/* .line 230 */
} // :cond_0
v0 = this.mSpecialNameMap;
(( miui.android.animation.property.FloatProperty ) p1 ).getName ( ); // invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;
/* .line 232 */
} // :goto_0
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Long p2, Float...p3 ) {
/* .locals 7 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "delay" # J */
/* .param p5, "velocity" # [F */
/* .line 188 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig; */
/* .line 189 */
/* .local v0, "sc":Lmiui/android/animation/base/AnimSpecialConfig; */
/* move-object v1, p0 */
/* move-object v2, v0 */
/* move-object v3, p2 */
/* move-wide v4, p3 */
/* move-object v6, p5 */
/* invoke-virtual/range {v1 ..v6}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V */
/* .line 190 */
} // .end method
public miui.android.animation.base.AnimConfig setSpecial ( miui.android.animation.property.FloatProperty p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Float...p2 ) {
/* .locals 6 */
/* .param p1, "property" # Lmiui/android/animation/property/FloatProperty; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "velocity" # [F */
/* .line 183 */
/* const-wide/16 v3, -0x1 */
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v5, p3 */
/* invoke-virtual/range {v0 ..v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig; */
/* .line 184 */
} // .end method
void setSpecial ( miui.android.animation.base.AnimSpecialConfig p0, miui.android.animation.utils.EaseManager$EaseStyle p1, Long p2, Float...p3 ) {
/* .locals 2 */
/* .param p1, "sc" # Lmiui/android/animation/base/AnimSpecialConfig; */
/* .param p2, "ease" # Lmiui/android/animation/utils/EaseManager$EaseStyle; */
/* .param p3, "delay" # J */
/* .param p5, "velocity" # [F */
/* .line 194 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 195 */
(( miui.android.animation.base.AnimSpecialConfig ) p1 ).setEase ( p2 ); // invoke-virtual {p1, p2}, Lmiui/android/animation/base/AnimSpecialConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
/* .line 197 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p3, v0 */
/* if-lez v0, :cond_1 */
/* .line 198 */
(( miui.android.animation.base.AnimSpecialConfig ) p1 ).setDelay ( p3, p4 ); // invoke-virtual {p1, p3, p4}, Lmiui/android/animation/base/AnimSpecialConfig;->setDelay(J)Lmiui/android/animation/base/AnimConfig;
/* .line 200 */
} // :cond_1
/* array-length v0, p5 */
/* if-lez v0, :cond_2 */
int v0 = 0; // const/4 v0, 0x0
/* aget v0, p5, v0 */
(( miui.android.animation.base.AnimSpecialConfig ) p1 ).setFromSpeed ( v0 ); // invoke-virtual {p1, v0}, Lmiui/android/animation/base/AnimSpecialConfig;->setFromSpeed(F)Lmiui/android/animation/base/AnimConfig;
/* .line 201 */
} // :cond_2
return;
} // .end method
public miui.android.animation.base.AnimConfig setTag ( java.lang.Object p0 ) {
/* .locals 0 */
/* .param p1, "t" # Ljava/lang/Object; */
/* .line 155 */
this.tag = p1;
/* .line 156 */
} // .end method
public miui.android.animation.base.AnimConfig setTintMode ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "mode" # I */
/* .line 137 */
/* iput p1, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
/* .line 138 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 255 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AnimConfig{delay="; // const-string v1, "AnimConfig{delay="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/base/AnimConfig;->delay:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", minDuration="; // const-string v1, ", minDuration="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", ease="; // const-string v1, ", ease="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.ease;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", fromSpeed="; // const-string v1, ", fromSpeed="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", tintMode="; // const-string v1, ", tintMode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", tag="; // const-string v1, ", tag="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.tag;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", flags="; // const-string v1, ", flags="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lmiui/android/animation/base/AnimConfig;->flags:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", listeners="; // const-string v1, ", listeners="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.listeners;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", specialNameMap = "; // const-string v1, ", specialNameMap = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mSpecialNameMap;
/* .line 264 */
final String v2 = " "; // const-string v2, " "
miui.android.animation.utils.CommonUtils .mapToString ( v1,v2 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 255 */
} // .end method
