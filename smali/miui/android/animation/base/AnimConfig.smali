.class public Lmiui/android/animation/base/AnimConfig;
.super Ljava/lang/Object;
.source "AnimConfig.java"


# static fields
.field public static final FLAG_DELTA:J = 0x1L

.field public static final FLAG_INIT:J = 0x2L

.field public static final FLAG_INT:J = 0x4L

.field public static final TINT_ALPHA:I = 0x0

.field public static final TINT_OPAQUE:I = 0x1

.field public static final sDefEase:Lmiui/android/animation/utils/EaseManager$EaseStyle;


# instance fields
.field public delay:J

.field public ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

.field public flags:J

.field public fromSpeed:F

.field public final listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lmiui/android/animation/listener/TransitionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mSpecialNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lmiui/android/animation/base/AnimSpecialConfig;",
            ">;"
        }
    .end annotation
.end field

.field public minDuration:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public tag:Ljava/lang/Object;

.field public tintMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    const/4 v1, -0x2

    invoke-static {v1, v0}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v0

    sput-object v0, Lmiui/android/animation/base/AnimConfig;->sDefEase:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    return-void

    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3e99999a    # 0.3f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/android/animation/base/AnimConfig;-><init>(Z)V

    .line 63
    return-void
.end method

.method public constructor <init>(Lmiui/android/animation/base/AnimConfig;)V
    .locals 1
    .param p1, "src"    # Lmiui/android/animation/base/AnimConfig;

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiui/android/animation/base/AnimConfig;-><init>(Z)V

    .line 77
    invoke-virtual {p0, p1}, Lmiui/android/animation/base/AnimConfig;->copy(Lmiui/android/animation/base/AnimConfig;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isSpecial"    # Z

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    .line 66
    if-nez p1, :cond_0

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    goto :goto_0

    .line 70
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    .line 71
    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    .line 73
    :goto_0
    return-void
.end method

.method private queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "create"    # Z

    .line 245
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/android/animation/base/AnimSpecialConfig;

    .line 246
    .local v0, "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 247
    new-instance v1, Lmiui/android/animation/base/AnimSpecialConfig;

    invoke-direct {v1}, Lmiui/android/animation/base/AnimSpecialConfig;-><init>()V

    move-object v0, v1

    .line 248
    iget-object v1, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :cond_0
    return-object v0
.end method

.method private queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "create"    # Z

    .line 216
    if-nez p1, :cond_0

    .line 217
    const/4 v0, 0x0

    return-object v0

    .line 219
    :cond_0
    invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public varargs addListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "l"    # [Lmiui/android/animation/listener/TransitionListener;

    .line 132
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 133
    return-object p0
.end method

.method public addSpecialConfigs(Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "src"    # Lmiui/android/animation/base/AnimConfig;

    .line 142
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    iget-object v1, p1, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 143
    return-void
.end method

.method public clear()V
    .locals 4

    .line 98
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->delay:J

    .line 99
    const/4 v2, 0x0

    iput-object v2, p0, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 100
    iget-object v3, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    .line 101
    iput-object v2, p0, Lmiui/android/animation/base/AnimConfig;->tag:Ljava/lang/Object;

    .line 102
    iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->flags:J

    .line 103
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    iput v2, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    .line 104
    iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    .line 106
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 107
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 109
    :cond_0
    return-void
.end method

.method public copy(Lmiui/android/animation/base/AnimConfig;)V
    .locals 2
    .param p1, "src"    # Lmiui/android/animation/base/AnimConfig;

    .line 81
    if-eqz p1, :cond_0

    if-eq p1, p0, :cond_0

    .line 82
    iget-wide v0, p1, Lmiui/android/animation/base/AnimConfig;->delay:J

    iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->delay:J

    .line 83
    iget-object v0, p1, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 84
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    iget-object v1, p1, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 85
    iget-object v0, p1, Lmiui/android/animation/base/AnimConfig;->tag:Ljava/lang/Object;

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->tag:Ljava/lang/Object;

    .line 86
    iget-wide v0, p1, Lmiui/android/animation/base/AnimConfig;->flags:J

    iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->flags:J

    .line 87
    iget v0, p1, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    iput v0, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    .line 88
    iget-wide v0, p1, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    iput-wide v0, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    .line 89
    iget v0, p1, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    iput v0, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    .line 90
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 91
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 92
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    iget-object v1, p1, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 95
    :cond_0
    return-void
.end method

.method public getSpecialConfig(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    return-object v0
.end method

.method public getSpecialConfig(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 204
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    return-object v0
.end method

.method public queryAndCreateSpecial(Ljava/lang/String;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .line 212
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    return-object v0
.end method

.method public queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/base/AnimSpecialConfig;
    .locals 1
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;

    .line 208
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    return-object v0
.end method

.method public varargs removeListeners([Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/base/AnimConfig;
    .locals 2
    .param p1, "l"    # [Lmiui/android/animation/listener/TransitionListener;

    .line 146
    array-length v0, p1

    if-nez v0, :cond_0

    .line 147
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto :goto_0

    .line 149
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 151
    :goto_0
    return-object p0
.end method

.method public setDelay(J)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "d"    # J

    .line 122
    iput-wide p1, p0, Lmiui/android/animation/base/AnimConfig;->delay:J

    .line 123
    return-object p0
.end method

.method public varargs setEase(I[F)Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "style"    # I
    .param p2, "factors"    # [F

    .line 117
    invoke-static {p1, p2}, Lmiui/android/animation/utils/EaseManager;->getStyle(I[F)Lmiui/android/animation/utils/EaseManager$EaseStyle;

    move-result-object v0

    iput-object v0, p0, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 118
    return-object p0
.end method

.method public setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "e"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 112
    iput-object p1, p0, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    .line 113
    return-object p0
.end method

.method public setFromSpeed(F)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "speed"    # F

    .line 127
    iput p1, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    .line 128
    return-object p0
.end method

.method public setMinDuration(J)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "md"    # J

    .line 160
    iput-wide p1, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    .line 161
    return-object p0
.end method

.method public varargs setSpecial(Ljava/lang/String;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "delay"    # J
    .param p4, "velocity"    # [F

    .line 165
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    return-object v0
.end method

.method public setSpecial(Ljava/lang/String;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/base/AnimConfig;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 236
    if-eqz p2, :cond_0

    .line 237
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 239
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    :goto_0
    return-object p0
.end method

.method public varargs setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "delay"    # J
    .param p5, "velocity"    # [F

    .line 173
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Ljava/lang/String;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    .line 174
    .local v0, "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    move-object v1, p0

    move-object v2, v0

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 175
    return-object p0
.end method

.method public varargs setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "velocity"    # [F

    .line 169
    const-wide/16 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Ljava/lang/String;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    return-object v0
.end method

.method public varargs setSpecial(Lmiui/android/animation/property/FloatProperty;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "delay"    # J
    .param p4, "velocity"    # [F

    .line 179
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;

    move-result-object v0

    return-object v0
.end method

.method public setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/base/AnimSpecialConfig;)Lmiui/android/animation/base/AnimConfig;
    .locals 2
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;

    .line 227
    if-eqz p2, :cond_0

    .line 228
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    :cond_0
    iget-object v0, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    invoke-virtual {p1}, Lmiui/android/animation/property/FloatProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    :goto_0
    return-object p0
.end method

.method public varargs setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;
    .locals 7
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "delay"    # J
    .param p5, "velocity"    # [F

    .line 188
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiui/android/animation/base/AnimConfig;->queryAndCreateSpecial(Lmiui/android/animation/property/FloatProperty;Z)Lmiui/android/animation/base/AnimSpecialConfig;

    move-result-object v0

    .line 189
    .local v0, "sc":Lmiui/android/animation/base/AnimSpecialConfig;
    move-object v1, p0

    move-object v2, v0

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V

    .line 190
    return-object p0
.end method

.method public varargs setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;[F)Lmiui/android/animation/base/AnimConfig;
    .locals 6
    .param p1, "property"    # Lmiui/android/animation/property/FloatProperty;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "velocity"    # [F

    .line 183
    const-wide/16 v3, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lmiui/android/animation/base/AnimConfig;->setSpecial(Lmiui/android/animation/property/FloatProperty;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)Lmiui/android/animation/base/AnimConfig;

    .line 184
    return-object p0
.end method

.method varargs setSpecial(Lmiui/android/animation/base/AnimSpecialConfig;Lmiui/android/animation/utils/EaseManager$EaseStyle;J[F)V
    .locals 2
    .param p1, "sc"    # Lmiui/android/animation/base/AnimSpecialConfig;
    .param p2, "ease"    # Lmiui/android/animation/utils/EaseManager$EaseStyle;
    .param p3, "delay"    # J
    .param p5, "velocity"    # [F

    .line 194
    if-eqz p2, :cond_0

    .line 195
    invoke-virtual {p1, p2}, Lmiui/android/animation/base/AnimSpecialConfig;->setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;)Lmiui/android/animation/base/AnimConfig;

    .line 197
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    .line 198
    invoke-virtual {p1, p3, p4}, Lmiui/android/animation/base/AnimSpecialConfig;->setDelay(J)Lmiui/android/animation/base/AnimConfig;

    .line 200
    :cond_1
    array-length v0, p5

    if-lez v0, :cond_2

    const/4 v0, 0x0

    aget v0, p5, v0

    invoke-virtual {p1, v0}, Lmiui/android/animation/base/AnimSpecialConfig;->setFromSpeed(F)Lmiui/android/animation/base/AnimConfig;

    .line 201
    :cond_2
    return-void
.end method

.method public setTag(Ljava/lang/Object;)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "t"    # Ljava/lang/Object;

    .line 155
    iput-object p1, p0, Lmiui/android/animation/base/AnimConfig;->tag:Ljava/lang/Object;

    .line 156
    return-object p0
.end method

.method public setTintMode(I)Lmiui/android/animation/base/AnimConfig;
    .locals 0
    .param p1, "mode"    # I

    .line 137
    iput p1, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    .line 138
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimConfig{delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/base/AnimConfig;->delay:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", minDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/base/AnimConfig;->minDuration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfig;->ease:Lmiui/android/animation/utils/EaseManager$EaseStyle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", fromSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmiui/android/animation/base/AnimConfig;->fromSpeed:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tintMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmiui/android/animation/base/AnimConfig;->tintMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfig;->tag:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmiui/android/animation/base/AnimConfig;->flags:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", listeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfig;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", specialNameMap = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmiui/android/animation/base/AnimConfig;->mSpecialNameMap:Ljava/util/Map;

    .line 264
    const-string v2, "    "

    invoke-static {v1, v2}, Lmiui/android/animation/utils/CommonUtils;->mapToString(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 255
    return-object v0
.end method
