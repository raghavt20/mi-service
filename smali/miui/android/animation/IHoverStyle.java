public abstract class miui.android.animation.IHoverStyle implements miui.android.animation.IStateContainer {
	 /* .source "IHoverStyle.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/animation/IHoverStyle$HoverEffect;, */
	 /* Lmiui/android/animation/IHoverStyle$HoverType; */
	 /* } */
} // .end annotation
/* # virtual methods */
public abstract void addMagicPoint ( android.graphics.Point p0 ) {
} // .end method
public abstract void clearMagicPoint ( ) {
} // .end method
public abstract miui.android.animation.IHoverStyle clearTintColor ( ) {
} // .end method
public abstract void handleHoverOf ( android.view.View p0, miui.android.animation.base.AnimConfig...p1 ) {
} // .end method
public abstract void hoverEnter ( miui.android.animation.base.AnimConfig...p0 ) {
} // .end method
public abstract void hoverExit ( miui.android.animation.base.AnimConfig...p0 ) {
} // .end method
public abstract void hoverMove ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
} // .end method
public abstract void ignoreHoverOf ( android.view.View p0 ) {
} // .end method
public abstract Boolean isMagicView ( ) {
} // .end method
public abstract void onMotionEvent ( android.view.MotionEvent p0 ) {
} // .end method
public abstract void onMotionEventEx ( android.view.View p0, android.view.MotionEvent p1, miui.android.animation.base.AnimConfig...p2 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setAlpha ( Float p0, miui.android.animation.IHoverStyle$HoverType...p1 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setBackgroundColor ( Float p0, Float p1, Float p2, Float p3 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setBackgroundColor ( Integer p0 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setCorner ( Float p0 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setEffect ( miui.android.animation.IHoverStyle$HoverEffect p0 ) {
} // .end method
public abstract void setHoverEnter ( ) {
} // .end method
public abstract void setHoverExit ( ) {
} // .end method
public abstract void setMagicView ( Boolean p0 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setParentView ( android.view.View p0 ) {
} // .end method
public abstract void setPointerHide ( Boolean p0 ) {
} // .end method
public abstract void setPointerShape ( android.graphics.Bitmap p0 ) {
} // .end method
public abstract void setPointerShapeType ( Integer p0 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setScale ( Float p0, miui.android.animation.IHoverStyle$HoverType...p1 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setTint ( Float p0, Float p1, Float p2, Float p3 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setTint ( Integer p0 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setTintMode ( Integer p0 ) {
} // .end method
public abstract miui.android.animation.IHoverStyle setTranslate ( Float p0, miui.android.animation.IHoverStyle$HoverType...p1 ) {
} // .end method
public abstract void setWrapped ( Boolean p0 ) {
} // .end method
