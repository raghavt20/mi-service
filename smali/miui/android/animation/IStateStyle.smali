.class public interface abstract Lmiui/android/animation/IStateStyle;
.super Ljava/lang/Object;
.source "IStateStyle.java"

# interfaces
.implements Lmiui/android/animation/IStateContainer;


# virtual methods
.method public abstract add(Ljava/lang/String;F)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Ljava/lang/String;FJ)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Ljava/lang/String;I)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Ljava/lang/String;IJ)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Lmiui/android/animation/property/FloatProperty;F)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Lmiui/android/animation/property/FloatProperty;FJ)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Lmiui/android/animation/property/FloatProperty;I)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract add(Lmiui/android/animation/property/FloatProperty;IJ)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract addInitProperty(Ljava/lang/String;F)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract addInitProperty(Ljava/lang/String;I)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract addInitProperty(Lmiui/android/animation/property/FloatProperty;F)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract addInitProperty(Lmiui/android/animation/property/FloatProperty;I)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract addListener(Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract autoSetTo([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract fromTo(Ljava/lang/Object;Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract getCurrentState()Lmiui/android/animation/controller/AnimState;
.end method

.method public varargs abstract predictDuration([Ljava/lang/Object;)J
.end method

.method public abstract removeListener(Lmiui/android/animation/listener/TransitionListener;)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract set(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setConfig(Lmiui/android/animation/base/AnimConfig;[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setEase(I[F)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setEase(Lmiui/android/animation/property/FloatProperty;I[F)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setEase(Lmiui/android/animation/utils/EaseManager$EaseStyle;[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract setFlags(J)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract setTo(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setTo(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setTo([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract setTransitionFlags(J[Lmiui/android/animation/property/FloatProperty;)Lmiui/android/animation/IStateStyle;
.end method

.method public abstract setup(Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract then(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract then([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract to(Ljava/lang/Object;[Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract to([Ljava/lang/Object;)Lmiui/android/animation/IStateStyle;
.end method

.method public varargs abstract to([Lmiui/android/animation/base/AnimConfig;)Lmiui/android/animation/IStateStyle;
.end method
