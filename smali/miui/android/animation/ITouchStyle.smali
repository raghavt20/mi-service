.class public interface abstract Lmiui/android/animation/ITouchStyle;
.super Ljava/lang/Object;
.source "ITouchStyle.java"

# interfaces
.implements Lmiui/android/animation/IStateContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/android/animation/ITouchStyle$TouchType;
    }
.end annotation


# virtual methods
.method public varargs abstract bindViewOfListItem(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public abstract clearTintColor()Lmiui/android/animation/ITouchStyle;
.end method

.method public varargs abstract handleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract handleTouchOf(Landroid/view/View;Landroid/view/View$OnClickListener;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract handleTouchOf(Landroid/view/View;Z[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract handleTouchOf(Landroid/view/View;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public abstract ignoreTouchOf(Landroid/view/View;)V
.end method

.method public abstract onMotionEvent(Landroid/view/MotionEvent;)V
.end method

.method public varargs abstract onMotionEventEx(Landroid/view/View;Landroid/view/MotionEvent;[Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract setAlpha(F[Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle;
.end method

.method public abstract setBackgroundColor(FFFF)Lmiui/android/animation/ITouchStyle;
.end method

.method public abstract setBackgroundColor(I)Lmiui/android/animation/ITouchStyle;
.end method

.method public varargs abstract setScale(F[Lmiui/android/animation/ITouchStyle$TouchType;)Lmiui/android/animation/ITouchStyle;
.end method

.method public abstract setTint(FFFF)Lmiui/android/animation/ITouchStyle;
.end method

.method public abstract setTint(I)Lmiui/android/animation/ITouchStyle;
.end method

.method public abstract setTintMode(I)Lmiui/android/animation/ITouchStyle;
.end method

.method public abstract setTouchDown()V
.end method

.method public abstract setTouchUp()V
.end method

.method public varargs abstract touchDown([Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public varargs abstract touchUp([Lmiui/android/animation/base/AnimConfig;)V
.end method

.method public abstract useVarFont(Landroid/widget/TextView;III)Lmiui/android/animation/ITouchStyle;
.end method
