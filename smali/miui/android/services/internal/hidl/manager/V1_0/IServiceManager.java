public abstract class miui.android.services.internal.hidl.manager.V1_0.IServiceManager implements miui.android.services.internal.hidl.base.V1_0.IBase {
	 /* .source "IServiceManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Proxy;, */
	 /* Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;, */
	 /* Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$InstanceDebugInfo;, */
	 /* Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$PidConstant;, */
	 /* Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Transport; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String kInterfaceName;
/* # direct methods */
public static miui.android.services.internal.hidl.manager.V1_0.IServiceManager asInterface ( android.os.IHwBinder p0 ) {
	 /* .locals 7 */
	 /* .param p0, "binder" # Landroid/os/IHwBinder; */
	 /* .line 29 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p0, :cond_0 */
	 /* .line 30 */
	 /* .line 33 */
} // :cond_0
/* nop */
/* .line 34 */
final String v1 = "android.hidl.manager@1.0::IServiceManager"; // const-string v1, "android.hidl.manager@1.0::IServiceManager"
/* .line 36 */
/* .local v2, "iface":Landroid/os/IHwInterface; */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* instance-of v3, v2, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager; */
	 if ( v3 != null) { // if-eqz v3, :cond_1
		 /* .line 37 */
		 /* move-object v0, v2 */
		 /* check-cast v0, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager; */
		 /* .line 40 */
	 } // :cond_1
	 /* new-instance v3, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Proxy; */
	 /* invoke-direct {v3, p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Proxy;-><init>(Landroid/os/IHwBinder;)V */
	 /* .line 43 */
	 /* .local v3, "proxy":Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager; */
	 try { // :try_start_0
		 (( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
	 v5 = 	 } // :goto_0
	 if ( v5 != null) { // if-eqz v5, :cond_3
		 /* check-cast v5, Ljava/lang/String; */
		 /* .line 44 */
		 /* .local v5, "descriptor":Ljava/lang/String; */
		 v6 = 		 (( java.lang.String ) v5 ).equals ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 /* :try_end_0 */
		 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
		 if ( v6 != null) { // if-eqz v6, :cond_2
			 /* .line 45 */
			 /* .line 47 */
		 } // .end local v5 # "descriptor":Ljava/lang/String;
	 } // :cond_2
	 /* .line 49 */
} // :cond_3
/* .line 48 */
/* :catch_0 */
/* move-exception v1 */
/* .line 51 */
} // :goto_1
} // .end method
public static miui.android.services.internal.hidl.manager.V1_0.IServiceManager castFrom ( android.os.IHwInterface p0 ) {
/* .locals 1 */
/* .param p0, "iface" # Landroid/os/IHwInterface; */
/* .line 58 */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
} // :cond_0
miui.android.services.internal.hidl.manager.V1_0.IServiceManager .asInterface ( v0 );
} // :goto_0
} // .end method
public static miui.android.services.internal.hidl.manager.V1_0.IServiceManager getService ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 100 */
final String v0 = "default"; // const-string v0, "default"
miui.android.services.internal.hidl.manager.V1_0.IServiceManager .getService ( v0 );
} // .end method
public static miui.android.services.internal.hidl.manager.V1_0.IServiceManager getService ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .annotation runtime Ljava/lang/Deprecated; */
} // .end annotation
/* .line 90 */
final String v0 = "android.hidl.manager@1.0::IServiceManager"; // const-string v0, "android.hidl.manager@1.0::IServiceManager"
android.os.HwBinder .getService ( v0,p0 );
miui.android.services.internal.hidl.manager.V1_0.IServiceManager .asInterface ( v0 );
} // .end method
public static miui.android.services.internal.hidl.manager.V1_0.IServiceManager getService ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p0, "serviceName" # Ljava/lang/String; */
/* .param p1, "retry" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 73 */
final String v0 = "android.hidl.manager@1.0::IServiceManager"; // const-string v0, "android.hidl.manager@1.0::IServiceManager"
android.os.HwBinder .getService ( v0,p0,p1 );
miui.android.services.internal.hidl.manager.V1_0.IServiceManager .asInterface ( v0 );
} // .end method
public static miui.android.services.internal.hidl.manager.V1_0.IServiceManager getService ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "retry" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 80 */
final String v0 = "default"; // const-string v0, "default"
miui.android.services.internal.hidl.manager.V1_0.IServiceManager .getService ( v0,p0 );
} // .end method
/* # virtual methods */
public abstract Boolean add ( java.lang.String p0, miui.android.services.internal.hidl.base.V1_0.IBase p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract android.os.IHwBinder asBinder ( ) {
} // .end method
public abstract void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList debugDump ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$InstanceDebugInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract miui.android.services.internal.hidl.base.V1_0.IBase get ( java.lang.String p0, java.lang.String p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList getHashChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Object getTransport ( java.lang.String p0, java.lang.String p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList interfaceChain ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String interfaceDescriptor ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList list ( ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.util.ArrayList listByInterface ( java.lang.String p0 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void notifySyspropsChanged ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void ping ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean registerForNotifications ( java.lang.String p0, java.lang.String p1, miui.android.services.internal.hidl.manager.V1_0.IServiceNotification p2 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void registerPassthroughClient ( java.lang.String p0, java.lang.String p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setHALInstrumentation ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
