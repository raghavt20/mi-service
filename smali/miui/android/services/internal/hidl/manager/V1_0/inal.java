public class inal {
	 /* .source "IServiceManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x19 */
/* name = "Transport" */
} // .end annotation
/* # static fields */
public static final Object EMPTY;
public static final Object HWBINDER;
public static final Object PASSTHROUGH;
/* # direct methods */
public inal ( ) {
/* .locals 0 */
/* .line 103 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static final java.lang.String dumpBitfield ( Object p0 ) {
/* .locals 4 */
/* .param p0, "o" # B */
/* .line 121 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 122 */
/* .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 123 */
/* .local v1, "flipped":B */
final String v2 = "EMPTY"; // const-string v2, "EMPTY"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 124 */
/* and-int/lit8 v2, p0, 0x1 */
int v3 = 1; // const/4 v3, 0x1
/* if-ne v2, v3, :cond_0 */
/* .line 125 */
final String v2 = "HWBINDER"; // const-string v2, "HWBINDER"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 126 */
/* or-int/lit8 v2, v1, 0x1 */
/* int-to-byte v1, v2 */
/* .line 128 */
} // :cond_0
/* and-int/lit8 v2, p0, 0x2 */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_1 */
/* .line 129 */
final String v2 = "PASSTHROUGH"; // const-string v2, "PASSTHROUGH"
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 130 */
/* or-int/lit8 v2, v1, 0x2 */
/* int-to-byte v1, v2 */
/* .line 132 */
} // :cond_1
/* if-eq p0, v1, :cond_2 */
/* .line 133 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "0x"; // const-string v3, "0x"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* not-int v3, v1 */
/* and-int/2addr v3, p0 */
/* int-to-byte v3, v3 */
v3 = java.lang.Byte .toUnsignedInt ( v3 );
java.lang.Integer .toHexString ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 135 */
} // :cond_2
final String v2 = " | "; // const-string v2, " | "
java.lang.String .join ( v2,v0 );
} // .end method
public static final java.lang.String toString ( Object p0 ) {
/* .locals 2 */
/* .param p0, "o" # B */
/* .line 108 */
/* if-nez p0, :cond_0 */
/* .line 109 */
final String v0 = "EMPTY"; // const-string v0, "EMPTY"
/* .line 111 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, v0, :cond_1 */
/* .line 112 */
final String v0 = "HWBINDER"; // const-string v0, "HWBINDER"
/* .line 114 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* if-ne p0, v0, :cond_2 */
/* .line 115 */
final String v0 = "PASSTHROUGH"; // const-string v0, "PASSTHROUGH"
/* .line 117 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "0x"; // const-string v1, "0x"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = java.lang.Byte .toUnsignedInt ( p0 );
java.lang.Integer .toHexString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
