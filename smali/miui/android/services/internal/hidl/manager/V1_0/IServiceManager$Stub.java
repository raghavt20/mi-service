public abstract class miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub extends android.os.HwBinder implements miui.android.services.internal.hidl.manager.V1_0.IServiceManager {
	 /* .source "IServiceManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ( ) {
/* .locals 0 */
/* .line 913 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 916 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 929 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 966 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 967 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 968 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 969 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 970 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 941 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* -0x7bt */
/* 0x39t */
/* 0x4ft */
/* -0x76t */
/* 0xdt */
/* 0x15t */
/* -0x19t */
/* -0x5t */
/* 0x2et */
/* -0x1ct */
/* 0x5ct */
/* 0x52t */
/* -0x2ft */
/* -0x5t */
/* -0x75t */
/* -0x71t */
/* -0x2dt */
/* -0x3ft */
/* 0x3ct */
/* 0x33t */
/* 0x3et */
/* 0x63t */
/* -0x39t */
/* -0x74t */
/* 0x4at */
/* -0x5ft */
/* -0x1t */
/* -0x7at */
/* -0x7ct */
/* 0xct */
/* -0xat */
/* -0x24t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 921 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "android.hidl.manager@1.0::IServiceManager"; // const-string v1, "android.hidl.manager@1.0::IServiceManager"
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 935 */
final String v0 = "android.hidl.manager@1.0::IServiceManager"; // const-string v0, "android.hidl.manager@1.0::IServiceManager"
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 954 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 976 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 978 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 1006 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
final String v1 = "android.hidl.manager@1.0::IServiceManager"; // const-string v1, "android.hidl.manager@1.0::IServiceManager"
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_2 */
/* .line 1210 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1212 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->notifySyspropsChanged()V
/* .line 1213 */
/* goto/16 :goto_2 */
/* .line 1199 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1201 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 1202 */
/* .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1203 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 1204 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1205 */
/* goto/16 :goto_2 */
/* .line 1189 */
} // .end local v0 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1191 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->ping()V
/* .line 1192 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1193 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1194 */
/* goto/16 :goto_2 */
/* .line 1184 */
/* :sswitch_3 */
/* goto/16 :goto_2 */
/* .line 1176 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1178 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->setHALInstrumentation()V
/* .line 1179 */
/* goto/16 :goto_2 */
/* .line 1142 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1144 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 1145 */
/* .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1147 */
/* new-instance v1, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v1, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1149 */
/* .local v1, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 1150 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v1 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v1, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 1151 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v1 ).putBool ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 1152 */
/* new-instance v2, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v2, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 1153 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 1155 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 1156 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 1158 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 1162 */
(( android.os.HwBlob ) v2 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v2, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 1163 */
/* nop */
/* .line 1153 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1159 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 1166 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v1 ).putBlob ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 1168 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 1170 */
} // .end local v1 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1171 */
/* goto/16 :goto_2 */
/* .line 1131 */
} // .end local v0 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1133 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 1134 */
/* .local v0, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1135 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 1136 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1137 */
/* goto/16 :goto_2 */
/* .line 1119 */
} // .end local v0 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1121 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 1122 */
/* .local v0, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 1123 */
/* .local v1, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).debug ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 1124 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1125 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1126 */
/* goto/16 :goto_2 */
/* .line 1108 */
} // .end local v0 # "fd":Landroid/os/NativeHandle;
} // .end local v1 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1110 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 1111 */
/* .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1112 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 1113 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1114 */
/* goto/16 :goto_2 */
/* .line 1096 */
} // .end local v0 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1098 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1099 */
/* .local v0, "fqName":Ljava/lang/String; */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1100 */
/* .local v1, "name":Ljava/lang/String; */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).registerPassthroughClient ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->registerPassthroughClient(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1101 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1102 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1103 */
/* goto/16 :goto_2 */
/* .line 1085 */
} // .end local v0 # "fqName":Ljava/lang/String;
} // .end local v1 # "name":Ljava/lang/String;
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1087 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).debugDump ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->debugDump()Ljava/util/ArrayList;
/* .line 1088 */
/* .local v0, "_hidl_out_info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$InstanceDebugInfo;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1089 */
miui.android.services.internal.hidl.manager.V1_0.IServiceManager$InstanceDebugInfo .writeVectorToParcel ( p3,v0 );
/* .line 1090 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1091 */
/* goto/16 :goto_2 */
/* .line 1071 */
} // .end local v0 # "_hidl_out_info":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$InstanceDebugInfo;>;"
/* :sswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1073 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1074 */
/* .local v0, "fqName":Ljava/lang/String; */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1075 */
/* .restart local v1 # "name":Ljava/lang/String; */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
miui.android.services.internal.hidl.manager.V1_0.IServiceNotification .asInterface ( v3 );
/* .line 1076 */
/* .local v3, "callback":Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification; */
v4 = (( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).registerForNotifications ( v0, v1, v3 ); // invoke-virtual {p0, v0, v1, v3}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->registerForNotifications(Ljava/lang/String;Ljava/lang/String;Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification;)Z
/* .line 1077 */
/* .local v4, "_hidl_out_success":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1078 */
(( android.os.HwParcel ) p3 ).writeBool ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1079 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1080 */
/* goto/16 :goto_2 */
/* .line 1059 */
} // .end local v0 # "fqName":Ljava/lang/String;
} // .end local v1 # "name":Ljava/lang/String;
} // .end local v3 # "callback":Lmiui/android/services/internal/hidl/manager/V1_0/IServiceNotification;
} // .end local v4 # "_hidl_out_success":Z
/* :sswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1061 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1062 */
/* .restart local v0 # "fqName":Ljava/lang/String; */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).listByInterface ( v0 ); // invoke-virtual {p0, v0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->listByInterface(Ljava/lang/String;)Ljava/util/ArrayList;
/* .line 1063 */
/* .local v1, "_hidl_out_instanceNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1064 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 1065 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1066 */
/* .line 1048 */
} // .end local v0 # "fqName":Ljava/lang/String;
} // .end local v1 # "_hidl_out_instanceNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1050 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).list ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->list()Ljava/util/ArrayList;
/* .line 1051 */
/* .local v0, "_hidl_out_fqInstanceNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1052 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 1053 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1054 */
/* .line 1035 */
} // .end local v0 # "_hidl_out_fqInstanceNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1037 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1038 */
/* .local v0, "fqName":Ljava/lang/String; */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1039 */
/* .local v1, "name":Ljava/lang/String; */
v3 = (( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).getTransport ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->getTransport(Ljava/lang/String;Ljava/lang/String;)B
/* .line 1040 */
/* .local v3, "_hidl_out_transport":B */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1041 */
(( android.os.HwParcel ) p3 ).writeInt8 ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeInt8(B)V
/* .line 1042 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1043 */
/* .line 1022 */
} // .end local v0 # "fqName":Ljava/lang/String;
} // .end local v1 # "name":Ljava/lang/String;
} // .end local v3 # "_hidl_out_transport":B
/* :sswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1024 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1025 */
/* .local v0, "name":Ljava/lang/String; */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
miui.android.services.internal.hidl.base.V1_0.IBase .asInterface ( v1 );
/* .line 1026 */
/* .local v1, "service":Lmiui/android/services/internal/hidl/base/V1_0/IBase; */
v3 = (( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).add ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->add(Ljava/lang/String;Lmiui/android/services/internal/hidl/base/V1_0/IBase;)Z
/* .line 1027 */
/* .local v3, "_hidl_out_success":Z */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1028 */
(( android.os.HwParcel ) p3 ).writeBool ( v3 ); // invoke-virtual {p3, v3}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 1029 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1030 */
/* .line 1009 */
} // .end local v0 # "name":Ljava/lang/String;
} // .end local v1 # "service":Lmiui/android/services/internal/hidl/base/V1_0/IBase;
} // .end local v3 # "_hidl_out_success":Z
/* :sswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1011 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1012 */
/* .local v0, "fqName":Ljava/lang/String; */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 1013 */
/* .local v1, "name":Ljava/lang/String; */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).get ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->get(Ljava/lang/String;Ljava/lang/String;)Lmiui/android/services/internal/hidl/base/V1_0/IBase;
/* .line 1014 */
/* .local v3, "_hidl_out_service":Lmiui/android/services/internal/hidl/base/V1_0/IBase; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 1015 */
/* if-nez v3, :cond_2 */
int v2 = 0; // const/4 v2, 0x0
} // :cond_2
} // :goto_1
(( android.os.HwParcel ) p3 ).writeStrongBinder ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 1016 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 1017 */
/* nop */
/* .line 1222 */
} // .end local v0 # "fqName":Ljava/lang/String;
} // .end local v1 # "name":Ljava/lang/String;
} // .end local v3 # "_hidl_out_service":Lmiui/android/services/internal/hidl/base/V1_0/IBase;
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_10 */
/* 0x2 -> :sswitch_f */
/* 0x3 -> :sswitch_e */
/* 0x4 -> :sswitch_d */
/* 0x5 -> :sswitch_c */
/* 0x6 -> :sswitch_b */
/* 0x7 -> :sswitch_a */
/* 0x8 -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 960 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 988 */
final String v0 = "android.hidl.manager@1.0::IServiceManager"; // const-string v0, "android.hidl.manager@1.0::IServiceManager"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 989 */
/* .line 991 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 995 */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->registerService(Ljava/lang/String;)V
/* .line 996 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 950 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 1000 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( miui.android.services.internal.hidl.manager.V1_0.IServiceManager$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lmiui/android/services/internal/hidl/manager/V1_0/IServiceManager$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 982 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
