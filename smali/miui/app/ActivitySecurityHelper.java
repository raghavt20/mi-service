public class miui.app.ActivitySecurityHelper {
	 /* .source "ActivitySecurityHelper.java" */
	 /* # static fields */
	 private static final java.lang.String PACKAGE_ANDRIOD;
	 private static final java.lang.String PACKAGE_SECURITYCENTER;
	 private static final java.lang.String START_ACTIVITY_CALLEE_PKGNAME;
	 private static final java.lang.String START_ACTIVITY_CALLEE_UID;
	 private static final java.lang.String START_ACTIVITY_CALLER_PKGNAME;
	 private static final java.lang.String START_ACTIVITY_CALLER_UID;
	 private static final java.lang.String START_ACTIVITY_DIALOG_TYPE;
	 private static final java.lang.String START_ACTIVITY_RESTRICT_FOR_CHAIN;
	 private static final java.lang.String START_ACTIVITY_USERID;
	 private static final java.lang.String START_RECORD_RECENT_TASK;
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.app.AppOpsManager mAppOpsManager;
	 private final android.content.Context mContext;
	 private final android.content.pm.PackageManagerInternal mPmi;
	 private miui.security.SecurityManagerInternal mSecurityManagerInternal;
	 private final miui.security.SecurityManagerInternal mSmi;
	 /* # direct methods */
	 public static android.content.pm.ApplicationInfo $r8$lambda$VJYeE98fRSqzp-JpofJWkgOQCNQ ( miui.app.ActivitySecurityHelper p0, java.lang.String p1, Integer p2 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1, p2}, Lmiui/app/ActivitySecurityHelper;->lambda$getApplicationInfo$0(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
	 } // .end method
	 public miui.app.ActivitySecurityHelper ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 46 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 47 */
		 this.mContext = p1;
		 /* .line 48 */
		 /* const-class v0, Landroid/content/pm/PackageManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Landroid/content/pm/PackageManagerInternal; */
		 this.mPmi = v0;
		 /* .line 49 */
		 /* const-class v0, Lmiui/security/SecurityManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lmiui/security/SecurityManagerInternal; */
		 this.mSmi = v0;
		 /* .line 50 */
		 /* const-class v0, Lmiui/security/SecurityManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lmiui/security/SecurityManagerInternal; */
		 this.mSecurityManagerInternal = v0;
		 /* .line 51 */
		 final String v0 = "appops"; // const-string v0, "appops"
		 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/app/AppOpsManager; */
		 this.mAppOpsManager = v0;
		 /* .line 52 */
		 return;
	 } // .end method
	 private android.content.Intent buildIntentForRestrictChain ( android.content.pm.ApplicationInfo p0, java.lang.String p1, Integer p2, android.content.Intent p3, Boolean p4, Integer p5, Integer p6 ) {
		 /* .locals 11 */
		 /* .param p1, "callerAppInfo" # Landroid/content/pm/ApplicationInfo; */
		 /* .param p2, "calleePackageName" # Ljava/lang/String; */
		 /* .param p3, "calleeUid" # I */
		 /* .param p4, "intent" # Landroid/content/Intent; */
		 /* .param p5, "fromActivity" # Z */
		 /* .param p6, "requestCode" # I */
		 /* .param p7, "userId" # I */
		 /* .line 170 */
		 /* move-object v0, p1 */
		 v2 = this.packageName;
		 /* iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I */
		 int v10 = -1; // const/4 v10, -0x1
		 /* move-object v1, p0 */
		 /* move-object v3, p2 */
		 /* move/from16 v4, p7 */
		 /* move v6, p3 */
		 /* move-object v7, p4 */
		 /* move/from16 v8, p5 */
		 /* move/from16 v9, p6 */
		 /* invoke-direct/range {v1 ..v10}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent; */
		 /* .line 172 */
		 /* .local v1, "startIntent":Landroid/content/Intent; */
		 final String v2 = "restrictForChain"; // const-string v2, "restrictForChain"
		 int v3 = 1; // const/4 v3, 0x1
		 (( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
		 /* .line 173 */
	 } // .end method
	 private android.content.Intent buildStartIntent ( java.lang.String p0, java.lang.String p1, Integer p2, Integer p3, Integer p4, android.content.Intent p5, Boolean p6, Integer p7, Integer p8 ) {
		 /* .locals 3 */
		 /* .param p1, "callerPackage" # Ljava/lang/String; */
		 /* .param p2, "calleePackage" # Ljava/lang/String; */
		 /* .param p3, "userId" # I */
		 /* .param p4, "callerUid" # I */
		 /* .param p5, "calleeUid" # I */
		 /* .param p6, "sourceIntent" # Landroid/content/Intent; */
		 /* .param p7, "fromActivity" # Z */
		 /* .param p8, "requestCode" # I */
		 /* .param p9, "type" # I */
		 /* .line 203 */
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "android.app.action.CHECK_ALLOW_START_ACTIVITY"; // const-string v1, "android.app.action.CHECK_ALLOW_START_ACTIVITY"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 204 */
		 /* .local v0, "result":Landroid/content/Intent; */
		 final String v1 = "CallerPkgName"; // const-string v1, "CallerPkgName"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 205 */
		 final String v1 = "CalleePkgName"; // const-string v1, "CalleePkgName"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 206 */
		 final String v1 = "UserId"; // const-string v1, "UserId"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 207 */
		 final String v1 = "callerUserId"; // const-string v1, "callerUserId"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p4 ); // invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 208 */
		 final String v1 = "calleeUserId"; // const-string v1, "calleeUserId"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p5 ); // invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 209 */
		 final String v1 = "dialogType"; // const-string v1, "dialogType"
		 (( android.content.Intent ) v0 ).putExtra ( v1, p9 ); // invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 210 */
		 /* const/high16 v1, 0x800000 */
		 (( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
		 /* .line 211 */
		 final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 212 */
		 if ( p6 != null) { // if-eqz p6, :cond_3
			 /* .line 215 */
			 v1 = 			 (( android.content.Intent ) p6 ).getFlags ( ); // invoke-virtual {p6}, Landroid/content/Intent;->getFlags()I
			 /* const/high16 v2, 0x2000000 */
			 /* and-int/2addr v1, v2 */
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 /* .line 216 */
				 (( android.content.Intent ) v0 ).addFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
				 /* .line 218 */
			 } // :cond_0
			 /* const/high16 v1, 0x1000000 */
			 (( android.content.Intent ) p6 ).addFlags ( v1 ); // invoke-virtual {p6, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
			 /* .line 219 */
			 if ( p7 != null) { // if-eqz p7, :cond_1
				 /* .line 221 */
				 /* if-ltz p8, :cond_2 */
				 /* .line 222 */
				 (( android.content.Intent ) p6 ).addFlags ( v2 ); // invoke-virtual {p6, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
				 /* .line 226 */
			 } // :cond_1
			 /* const/high16 v1, 0x10000000 */
			 (( android.content.Intent ) p6 ).addFlags ( v1 ); // invoke-virtual {p6, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
			 /* .line 228 */
		 } // :cond_2
	 } // :goto_0
	 final String v1 = "android.intent.extra.INTENT"; // const-string v1, "android.intent.extra.INTENT"
	 (( android.content.Intent ) v0 ).putExtra ( v1, p6 ); // invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
	 /* .line 230 */
} // :cond_3
} // .end method
private android.content.Intent checkShareActionForStorageRestricted ( android.content.pm.ApplicationInfo p0, android.content.pm.ApplicationInfo p1, android.content.Intent p2, Boolean p3, Integer p4, Integer p5 ) {
/* .locals 15 */
/* .param p1, "callerInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "calleeInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "fromActivity" # Z */
/* .param p5, "requestCode" # I */
/* .param p6, "userId" # I */
/* .line 178 */
/* move-object v10, p0 */
/* move-object/from16 v11, p1 */
/* move-object/from16 v12, p2 */
/* move-object/from16 v13, p3 */
final String v0 = "android"; // const-string v0, "android"
v1 = this.packageName;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 179 */
	 /* .line 181 */
} // :cond_0
/* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getAction()Ljava/lang/String; */
/* .line 182 */
/* .local v14, "action":Ljava/lang/String; */
final String v0 = "android.intent.action.PICK"; // const-string v0, "android.intent.action.PICK"
v0 = (( java.lang.String ) v0 ).equals ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 183 */
	 v0 = this.mContext;
	 v2 = this.packageName;
	 /* iget v3, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
	 v0 = 	 miui.app.StorageRestrictedPathManager .isDenyAccessGallery ( v0,v2,v3,v13 );
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 185 */
		 v1 = this.packageName;
		 v2 = this.packageName;
		 /* iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
		 /* iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
		 /* const/16 v9, 0x2732 */
		 /* move-object v0, p0 */
		 /* move/from16 v3, p6 */
		 /* move-object/from16 v6, p3 */
		 /* move/from16 v7, p4 */
		 /* move/from16 v8, p5 */
		 /* invoke-direct/range {v0 ..v9}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent; */
		 /* .line 189 */
	 } // :cond_1
	 final String v0 = "android.intent.action.SEND"; // const-string v0, "android.intent.action.SEND"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 190 */
		 v0 = this.mContext;
		 v2 = this.packageName;
		 /* iget v3, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
		 v0 = 		 miui.app.StorageRestrictedPathManager .isDenyAccessGallery ( v0,v2,v3,v13 );
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* .line 192 */
			 v1 = this.packageName;
			 v2 = this.packageName;
			 /* iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
			 /* iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
			 /* const/16 v9, 0x2732 */
			 /* move-object v0, p0 */
			 /* move/from16 v3, p6 */
			 /* move-object/from16 v6, p3 */
			 /* move/from16 v7, p4 */
			 /* move/from16 v8, p5 */
			 /* invoke-direct/range {v0 ..v9}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent; */
			 /* .line 197 */
		 } // :cond_2
	 } // .end method
	 private android.content.pm.ApplicationInfo getApplicationInfo ( java.lang.String p0, Integer p1 ) {
		 /* .locals 1 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "userId" # I */
		 /* .line 89 */
		 v0 = 		 android.text.TextUtils .isEmpty ( p1 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 90 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .line 92 */
		 } // :cond_0
		 /* new-instance v0, Lmiui/app/ActivitySecurityHelper$$ExternalSyntheticLambda0; */
		 /* invoke-direct {v0, p0, p1, p2}, Lmiui/app/ActivitySecurityHelper$$ExternalSyntheticLambda0;-><init>(Lmiui/app/ActivitySecurityHelper;Ljava/lang/String;I)V */
		 android.os.Binder .withCleanCallingIdentity ( v0 );
		 /* check-cast v0, Landroid/content/pm/ApplicationInfo; */
	 } // .end method
	 private android.content.pm.ApplicationInfo lambda$getApplicationInfo$0 ( java.lang.String p0, Integer p1 ) { //synthethic
		 /* .locals 6 */
		 /* .param p1, "packageName" # Ljava/lang/String; */
		 /* .param p2, "userId" # I */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/lang/Exception; */
		 /* } */
	 } // .end annotation
	 /* .line 93 */
	 v0 = this.mPmi;
	 /* const-wide/16 v2, 0x0 */
	 v4 = 	 android.os.Process .myUid ( );
	 /* move-object v1, p1 */
	 /* move v5, p2 */
	 /* invoke-virtual/range {v0 ..v5}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo; */
} // .end method
private Boolean restrictForChain ( android.content.pm.ApplicationInfo p0 ) {
	 /* .locals 5 */
	 /* .param p1, "callerInfo" # Landroid/content/pm/ApplicationInfo; */
	 /* .line 160 */
	 int v0 = 0; // const/4 v0, 0x0
	 try { // :try_start_0
		 v1 = this.mAppOpsManager;
		 /* iget v2, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
		 v3 = this.packageName;
		 /* const/16 v4, 0x2741 */
		 v1 = 		 (( android.app.AppOpsManager ) v1 ).checkOpNoThrow ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 162 */
		 /* .local v1, "mode":I */
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :cond_0
		 /* .line 163 */
	 } // .end local v1 # "mode":I
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 164 */
	 /* .local v1, "e":Ljava/lang/Exception; */
} // .end method
/* # virtual methods */
public android.content.Intent getCheckAccessControlIntent ( java.lang.String p0, Integer p1, android.content.Intent p2, Boolean p3, Integer p4, Integer p5, android.os.Bundle p6 ) {
	 /* .locals 12 */
	 /* .param p1, "packageName" # Ljava/lang/String; */
	 /* .param p2, "uid" # I */
	 /* .param p3, "intent" # Landroid/content/Intent; */
	 /* .param p4, "fromActivity" # Z */
	 /* .param p5, "requestCode" # I */
	 /* .param p6, "userId" # I */
	 /* .param p7, "bOptions" # Landroid/os/Bundle; */
	 /* .line 249 */
	 /* move-object v0, p0 */
	 /* move-object v8, p3 */
	 /* move/from16 v9, p6 */
	 v1 = this.mSmi;
	 v1 = 	 (( miui.security.SecurityManagerInternal ) v1 ).isAccessControlActive ( v9 ); // invoke-virtual {v1, v9}, Lmiui/security/SecurityManagerInternal;->isAccessControlActive(I)Z
	 int v2 = 0; // const/4 v2, 0x0
	 /* if-nez v1, :cond_0 */
	 /* .line 250 */
	 /* .line 254 */
} // :cond_0
if ( v8 != null) { // if-eqz v8, :cond_2
	 v1 = 	 android.os.Binder .getCallingUid ( );
	 /* move v10, p2 */
	 /* if-eq v10, v1, :cond_1 */
} // :cond_1
/* move-object v11, p1 */
} // :cond_2
/* move v10, p2 */
} // :goto_0
v1 = this.mSmi;
/* .line 255 */
/* move-object v11, p1 */
v1 = (( miui.security.SecurityManagerInternal ) v1 ).checkAccessControlPassAsUser ( p1, p3, v9 ); // invoke-virtual {v1, p1, p3, v9}, Lmiui/security/SecurityManagerInternal;->checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z
/* if-nez v1, :cond_4 */
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 256 */
v1 = (( android.content.Intent ) p3 ).getFlags ( ); // invoke-virtual {p3}, Landroid/content/Intent;->getFlags()I
/* const/high16 v3, 0x100000 */
/* and-int/2addr v1, v3 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 259 */
} // :cond_3
int v1 = 1; // const/4 v1, 0x1
/* move-object v2, p1 */
/* move-object v3, p3 */
/* move/from16 v4, p5 */
/* move/from16 v5, p4 */
/* move/from16 v6, p6 */
/* move-object/from16 v7, p7 */
/* invoke-static/range {v1 ..v7}, Lmiui/security/SecurityManager;->getCheckAccessIntent(ZLjava/lang/String;Landroid/content/Intent;IZILandroid/os/Bundle;)Landroid/content/Intent; */
/* .line 257 */
} // :cond_4
} // :goto_1
} // .end method
public android.content.Intent getCheckGameBoosterAntiMsgIntent ( java.lang.String p0, java.lang.String p1, android.content.Intent p2, Boolean p3, Integer p4, Integer p5 ) {
/* .locals 9 */
/* .param p1, "callPackageName" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "fromActivity" # Z */
/* .param p5, "requestCode" # I */
/* .param p6, "userId" # I */
/* .line 236 */
final String v0 = "com.miui.securitycenter"; // const-string v0, "com.miui.securitycenter"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_2 */
v0 = this.mSmi;
v0 = (( miui.security.SecurityManagerInternal ) v0 ).isGameBoosterActive ( p6 ); // invoke-virtual {v0, p6}, Lmiui/security/SecurityManagerInternal;->isGameBoosterActive(I)Z
/* if-nez v0, :cond_0 */
/* .line 239 */
} // :cond_0
v0 = this.mSmi;
v0 = (( miui.security.SecurityManagerInternal ) v0 ).checkGameBoosterAntiMsgPassAsUser ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lmiui/security/SecurityManagerInternal;->checkGameBoosterAntiMsgPassAsUser(Ljava/lang/String;Landroid/content/Intent;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 240 */
/* .line 242 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move v5, p5 */
/* move v6, p4 */
/* move v7, p6 */
/* invoke-static/range {v2 ..v8}, Lmiui/security/SecurityManager;->getCheckAccessIntent(ZLjava/lang/String;Landroid/content/Intent;IZILandroid/os/Bundle;)Landroid/content/Intent; */
/* .line 237 */
} // :cond_2
} // :goto_0
} // .end method
public android.content.Intent getCheckIntent ( java.lang.String p0, java.lang.String p1, android.content.Intent p2, Boolean p3, Integer p4, Boolean p5, Integer p6, Integer p7, android.os.Bundle p8 ) {
/* .locals 17 */
/* .param p1, "callingPackage" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "fromActivity" # Z */
/* .param p5, "requestCode" # I */
/* .param p6, "calleeAlreadyStarted" # Z */
/* .param p7, "userId" # I */
/* .param p8, "callingUid" # I */
/* .param p9, "bOptions" # Landroid/os/Bundle; */
/* .line 58 */
/* move-object/from16 v9, p0 */
/* move-object/from16 v10, p2 */
v11 = /* invoke-static/range {p8 ..p8}, Landroid/os/UserHandle;->getUserId(I)I */
/* .line 59 */
/* .local v11, "callingUser":I */
/* move-object/from16 v12, p1 */
/* invoke-direct {v9, v12, v11}, Lmiui/app/ActivitySecurityHelper;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 60 */
/* .local v13, "callerInfo":Landroid/content/pm/ApplicationInfo; */
/* move/from16 v14, p7 */
/* invoke-direct {v9, v10, v14}, Lmiui/app/ActivitySecurityHelper;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo; */
/* .line 61 */
/* .local v15, "calleeInfo":Landroid/content/pm/ApplicationInfo; */
if ( v13 != null) { // if-eqz v13, :cond_4
/* if-nez v15, :cond_0 */
/* .line 65 */
} // :cond_0
v0 = this.mContext;
/* move-object/from16 v8, p3 */
/* move/from16 v7, p4 */
/* move/from16 v6, p5 */
miui.security.AppRunningControlManager .getBlockActivityIntent ( v0,v10,v8,v7,v6 );
/* .line 67 */
/* .local v16, "ret":Landroid/content/Intent; */
if ( v16 != null) { // if-eqz v16, :cond_1
/* .line 68 */
/* .line 71 */
} // :cond_1
/* move-object/from16 v0, p0 */
/* move-object v1, v13 */
/* move-object v2, v15 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move/from16 v5, p5 */
/* move/from16 v6, p6 */
/* move/from16 v7, p7 */
/* move v8, v11 */
/* invoke-virtual/range {v0 ..v8}, Lmiui/app/ActivitySecurityHelper;->getCheckStartActivityIntent(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/Intent;ZIZII)Landroid/content/Intent; */
/* .line 73 */
} // .end local v16 # "ret":Landroid/content/Intent;
/* .local v7, "ret":Landroid/content/Intent; */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 74 */
/* .line 77 */
} // :cond_2
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move/from16 v5, p5 */
/* move/from16 v6, p7 */
/* invoke-virtual/range {v0 ..v6}, Lmiui/app/ActivitySecurityHelper;->getCheckGameBoosterAntiMsgIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;ZII)Landroid/content/Intent; */
/* .line 79 */
} // .end local v7 # "ret":Landroid/content/Intent;
/* .local v8, "ret":Landroid/content/Intent; */
if ( v8 != null) { // if-eqz v8, :cond_3
/* .line 80 */
/* .line 83 */
} // :cond_3
/* iget v2, v15, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p2 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move/from16 v5, p5 */
/* move/from16 v6, p7 */
/* move-object/from16 v7, p9 */
/* invoke-virtual/range {v0 ..v7}, Lmiui/app/ActivitySecurityHelper;->getCheckAccessControlIntent(Ljava/lang/String;ILandroid/content/Intent;ZIILandroid/os/Bundle;)Landroid/content/Intent; */
/* .line 85 */
} // .end local v8 # "ret":Landroid/content/Intent;
/* .local v0, "ret":Landroid/content/Intent; */
/* .line 62 */
} // .end local v0 # "ret":Landroid/content/Intent;
} // :cond_4
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public android.content.Intent getCheckStartActivityIntent ( android.content.pm.ApplicationInfo p0, android.content.pm.ApplicationInfo p1, android.content.Intent p2, Boolean p3, Integer p4, Boolean p5, Integer p6, Integer p7 ) {
/* .locals 29 */
/* .param p1, "callerInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "calleeInfo" # Landroid/content/pm/ApplicationInfo; */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .param p4, "fromActivity" # Z */
/* .param p5, "requestCode" # I */
/* .param p6, "calleeAlreadyStarted" # Z */
/* .param p7, "userId" # I */
/* .param p8, "callingUser" # I */
/* .line 99 */
/* move-object/from16 v10, p0 */
/* move-object/from16 v11, p1 */
/* move-object/from16 v12, p2 */
int v7 = 0; // const/4 v7, 0x0
if ( p3 != null) { // if-eqz p3, :cond_c
v0 = android.miui.AppOpsUtils .isXOptMode ( );
/* if-nez v0, :cond_c */
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* goto/16 :goto_3 */
/* .line 103 */
} // :cond_0
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v2, p2 */
/* move-object/from16 v3, p3 */
/* move/from16 v4, p4 */
/* move/from16 v5, p5 */
/* move/from16 v6, p7 */
/* invoke-direct/range {v0 ..v6}, Lmiui/app/ActivitySecurityHelper;->checkShareActionForStorageRestricted(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/Intent;ZII)Landroid/content/Intent; */
/* move-object v13, v0 */
/* .local v13, "result":Landroid/content/Intent; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 105 */
/* .line 107 */
} // :cond_1
/* iget v0, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = android.os.UserHandle .getAppId ( v0 );
/* const/16 v1, 0x2710 */
/* if-lt v0, v1, :cond_b */
/* .line 108 */
v0 = /* invoke-virtual/range {p1 ..p1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* goto/16 :goto_2 */
/* .line 113 */
} // :cond_2
/* iget v0, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
v0 = android.os.UserHandle .getAppId ( v0 );
/* if-lt v0, v1, :cond_a */
/* .line 114 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* goto/16 :goto_1 */
/* .line 120 */
} // :cond_3
v0 = this.packageName;
v1 = this.packageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 121 */
v0 = /* invoke-virtual/range {p3 ..p3}, Landroid/content/Intent;->getFlags()I */
/* const/high16 v1, 0x100000 */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 123 */
miui.security.WakePathChecker .getInstance ( );
final String v15 = "recentTask"; // const-string v15, "recentTask"
v0 = this.packageName;
/* const/16 v17, 0x1 */
/* .line 124 */
v18 = android.os.UserHandle .myUserId ( );
/* const/16 v20, 0x1 */
/* .line 123 */
/* move-object/from16 v16, v0 */
/* move/from16 v19, p7 */
/* invoke-virtual/range {v14 ..v20}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 127 */
} // :cond_4
v0 = this.mSecurityManagerInternal;
v1 = this.packageName;
v2 = this.packageName;
v14 = (( miui.security.SecurityManagerInternal ) v0 ).exemptByRestrictChain ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/security/SecurityManagerInternal;->exemptByRestrictChain(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 129 */
/* .local v14, "exemptByRestrictChain":Z */
v0 = this.packageName;
v1 = this.packageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
/* if-nez v0, :cond_5 */
v0 = /* invoke-direct/range {p0 ..p1}, Lmiui/app/ActivitySecurityHelper;->restrictForChain(Landroid/content/pm/ApplicationInfo;)Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* if-nez v14, :cond_5 */
/* .line 131 */
v2 = this.packageName;
/* iget v3, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
/* move-object/from16 v0, p0 */
/* move-object/from16 v1, p1 */
/* move-object/from16 v4, p3 */
/* move/from16 v5, p4 */
/* move/from16 v6, p5 */
/* move/from16 v7, p7 */
/* invoke-direct/range {v0 ..v7}, Lmiui/app/ActivitySecurityHelper;->buildIntentForRestrictChain(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;ILandroid/content/Intent;ZII)Landroid/content/Intent; */
/* .line 135 */
} // :cond_5
if ( v14 != null) { // if-eqz v14, :cond_6
/* .line 136 */
/* .line 139 */
} // :cond_6
if ( p6 != null) { // if-eqz p6, :cond_7
/* .line 140 */
miui.security.WakePathChecker .getInstance ( );
v1 = this.packageName;
v2 = this.packageName;
int v3 = 1; // const/4 v3, 0x1
int v6 = 1; // const/4 v6, 0x1
/* move/from16 v4, p8 */
/* move/from16 v5, p7 */
/* invoke-virtual/range {v0 ..v6}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 142 */
/* .line 145 */
} // :cond_7
v0 = this.packageName;
v1 = this.packageName;
v0 = android.text.TextUtils .equals ( v0,v1 );
/* if-nez v0, :cond_9 */
v0 = this.mSmi;
v1 = this.packageName;
v2 = this.packageName;
/* iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
/* .line 146 */
/* move-object/from16 v3, p3 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lmiui/security/SecurityManagerInternal;->checkAllowStartActivity(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;II)Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 154 */
} // :cond_8
v1 = this.packageName;
v2 = this.packageName;
/* iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I */
/* iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I */
int v9 = -1; // const/4 v9, -0x1
/* move-object/from16 v0, p0 */
/* move/from16 v3, p7 */
/* move-object/from16 v6, p3 */
/* move/from16 v7, p4 */
/* move/from16 v8, p5 */
/* invoke-direct/range {v0 ..v9}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent; */
/* .line 148 */
} // :cond_9
} // :goto_0
miui.security.WakePathChecker .getInstance ( );
v0 = this.packageName;
v1 = this.packageName;
/* const/16 v18, 0x1 */
/* const/16 v21, 0x1 */
/* move-object/from16 v16, v0 */
/* move-object/from16 v17, v1 */
/* move/from16 v19, p8 */
/* move/from16 v20, p7 */
/* invoke-virtual/range {v15 ..v21}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 151 */
/* .line 115 */
} // .end local v14 # "exemptByRestrictChain":Z
} // :cond_a
} // :goto_1
miui.security.WakePathChecker .getInstance ( );
v0 = this.packageName;
v1 = this.packageName;
/* const/16 v25, 0x1 */
/* const/16 v28, 0x1 */
/* move-object/from16 v23, v0 */
/* move-object/from16 v24, v1 */
/* move/from16 v26, p8 */
/* move/from16 v27, p7 */
/* invoke-virtual/range {v22 ..v28}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 117 */
/* .line 109 */
} // :cond_b
} // :goto_2
miui.security.WakePathChecker .getInstance ( );
v15 = this.packageName;
v0 = this.packageName;
/* const/16 v17, 0x1 */
/* const/16 v20, 0x1 */
/* move-object/from16 v16, v0 */
/* move/from16 v18, p8 */
/* move/from16 v19, p7 */
/* invoke-virtual/range {v14 ..v20}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V */
/* .line 111 */
/* .line 100 */
} // .end local v13 # "result":Landroid/content/Intent;
} // :cond_c
} // :goto_3
} // .end method
