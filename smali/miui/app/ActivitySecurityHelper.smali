.class public Lmiui/app/ActivitySecurityHelper;
.super Ljava/lang/Object;
.source "ActivitySecurityHelper.java"


# static fields
.field private static final PACKAGE_ANDRIOD:Ljava/lang/String; = "android"

.field private static final PACKAGE_SECURITYCENTER:Ljava/lang/String; = "com.miui.securitycenter"

.field private static final START_ACTIVITY_CALLEE_PKGNAME:Ljava/lang/String; = "CalleePkgName"

.field private static final START_ACTIVITY_CALLEE_UID:Ljava/lang/String; = "calleeUserId"

.field private static final START_ACTIVITY_CALLER_PKGNAME:Ljava/lang/String; = "CallerPkgName"

.field private static final START_ACTIVITY_CALLER_UID:Ljava/lang/String; = "callerUserId"

.field private static final START_ACTIVITY_DIALOG_TYPE:Ljava/lang/String; = "dialogType"

.field private static final START_ACTIVITY_RESTRICT_FOR_CHAIN:Ljava/lang/String; = "restrictForChain"

.field private static final START_ACTIVITY_USERID:Ljava/lang/String; = "UserId"

.field private static final START_RECORD_RECENT_TASK:Ljava/lang/String; = "recentTask"

.field private static final TAG:Ljava/lang/String; = "ActivitySecurityHelper"


# instance fields
.field private mAppOpsManager:Landroid/app/AppOpsManager;

.field private final mContext:Landroid/content/Context;

.field private final mPmi:Landroid/content/pm/PackageManagerInternal;

.field private mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

.field private final mSmi:Lmiui/security/SecurityManagerInternal;


# direct methods
.method public static synthetic $r8$lambda$VJYeE98fRSqzp-JpofJWkgOQCNQ(Lmiui/app/ActivitySecurityHelper;Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiui/app/ActivitySecurityHelper;->lambda$getApplicationInfo$0(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p0

    return-object p0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lmiui/app/ActivitySecurityHelper;->mContext:Landroid/content/Context;

    .line 48
    const-class v0, Landroid/content/pm/PackageManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManagerInternal;

    iput-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mPmi:Landroid/content/pm/PackageManagerInternal;

    .line 49
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mSmi:Lmiui/security/SecurityManagerInternal;

    .line 50
    const-class v0, Lmiui/security/SecurityManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiui/security/SecurityManagerInternal;

    iput-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    .line 51
    const-string v0, "appops"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mAppOpsManager:Landroid/app/AppOpsManager;

    .line 52
    return-void
.end method

.method private buildIntentForRestrictChain(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;ILandroid/content/Intent;ZII)Landroid/content/Intent;
    .locals 11
    .param p1, "callerAppInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "calleePackageName"    # Ljava/lang/String;
    .param p3, "calleeUid"    # I
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "fromActivity"    # Z
    .param p6, "requestCode"    # I
    .param p7, "userId"    # I

    .line 170
    move-object v0, p1

    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v5, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    const/4 v10, -0x1

    move-object v1, p0

    move-object v3, p2

    move/from16 v4, p7

    move v6, p3

    move-object v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v1 .. v10}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v1

    .line 172
    .local v1, "startIntent":Landroid/content/Intent;
    const-string v2, "restrictForChain"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 173
    return-object v1
.end method

.method private buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent;
    .locals 3
    .param p1, "callerPackage"    # Ljava/lang/String;
    .param p2, "calleePackage"    # Ljava/lang/String;
    .param p3, "userId"    # I
    .param p4, "callerUid"    # I
    .param p5, "calleeUid"    # I
    .param p6, "sourceIntent"    # Landroid/content/Intent;
    .param p7, "fromActivity"    # Z
    .param p8, "requestCode"    # I
    .param p9, "type"    # I

    .line 203
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.app.action.CHECK_ALLOW_START_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "CallerPkgName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string v1, "CalleePkgName"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    const-string v1, "UserId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 207
    const-string v1, "callerUserId"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 208
    const-string v1, "calleeUserId"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 209
    const-string v1, "dialogType"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 210
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 211
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    if-eqz p6, :cond_3

    .line 215
    invoke-virtual {p6}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v2, 0x2000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 216
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 218
    :cond_0
    const/high16 v1, 0x1000000

    invoke-virtual {p6, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 219
    if-eqz p7, :cond_1

    .line 221
    if-ltz p8, :cond_2

    .line 222
    invoke-virtual {p6, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 226
    :cond_1
    const/high16 v1, 0x10000000

    invoke-virtual {p6, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 228
    :cond_2
    :goto_0
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 230
    :cond_3
    return-object v0
.end method

.method private checkShareActionForStorageRestricted(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/Intent;ZII)Landroid/content/Intent;
    .locals 15
    .param p1, "callerInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "calleeInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "fromActivity"    # Z
    .param p5, "requestCode"    # I
    .param p6, "userId"    # I

    .line 178
    move-object v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    const-string v0, "android"

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 179
    return-object v1

    .line 181
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    .line 182
    .local v14, "action":Ljava/lang/String;
    const-string v0, "android.intent.action.PICK"

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, v10, Lmiui/app/ActivitySecurityHelper;->mContext:Landroid/content/Context;

    iget-object v2, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v3, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0, v2, v3, v13}, Lmiui/app/StorageRestrictedPathManager;->isDenyAccessGallery(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    iget-object v1, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v9, 0x2732

    move-object v0, p0

    move/from16 v3, p6

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    invoke-direct/range {v0 .. v9}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 189
    :cond_1
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, v10, Lmiui/app/ActivitySecurityHelper;->mContext:Landroid/content/Context;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v3, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0, v2, v3, v13}, Lmiui/app/StorageRestrictedPathManager;->isDenyAccessGallery(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    iget-object v1, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    const/16 v9, 0x2732

    move-object v0, p0

    move/from16 v3, p6

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    invoke-direct/range {v0 .. v9}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 197
    :cond_2
    return-object v1
.end method

.method private getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 89
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const/4 v0, 0x0

    return-object v0

    .line 92
    :cond_0
    new-instance v0, Lmiui/app/ActivitySecurityHelper$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0, p1, p2}, Lmiui/app/ActivitySecurityHelper$$ExternalSyntheticLambda0;-><init>(Lmiui/app/ActivitySecurityHelper;Ljava/lang/String;I)V

    invoke-static {v0}, Landroid/os/Binder;->withCleanCallingIdentity(Lcom/android/internal/util/FunctionalUtils$ThrowingSupplier;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method private synthetic lambda$getApplicationInfo$0(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mPmi:Landroid/content/pm/PackageManagerInternal;

    const-wide/16 v2, 0x0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/pm/PackageManagerInternal;->getApplicationInfo(Ljava/lang/String;JII)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    return-object v0
.end method

.method private restrictForChain(Landroid/content/pm/ApplicationInfo;)Z
    .locals 5
    .param p1, "callerInfo"    # Landroid/content/pm/ApplicationInfo;

    .line 160
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lmiui/app/ActivitySecurityHelper;->mAppOpsManager:Landroid/app/AppOpsManager;

    iget v2, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v3, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v4, 0x2741

    invoke-virtual {v1, v4, v2, v3}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .local v1, "mode":I
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    .line 163
    .end local v1    # "mode":I
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Ljava/lang/Exception;
    return v0
.end method


# virtual methods
.method public getCheckAccessControlIntent(Ljava/lang/String;ILandroid/content/Intent;ZIILandroid/os/Bundle;)Landroid/content/Intent;
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "fromActivity"    # Z
    .param p5, "requestCode"    # I
    .param p6, "userId"    # I
    .param p7, "bOptions"    # Landroid/os/Bundle;

    .line 249
    move-object v0, p0

    move-object v8, p3

    move/from16 v9, p6

    iget-object v1, v0, Lmiui/app/ActivitySecurityHelper;->mSmi:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v1, v9}, Lmiui/security/SecurityManagerInternal;->isAccessControlActive(I)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 250
    return-object v2

    .line 254
    :cond_0
    if-eqz v8, :cond_2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    move v10, p2

    if-eq v10, v1, :cond_1

    goto :goto_0

    :cond_1
    move-object v11, p1

    goto :goto_1

    :cond_2
    move v10, p2

    :goto_0
    iget-object v1, v0, Lmiui/app/ActivitySecurityHelper;->mSmi:Lmiui/security/SecurityManagerInternal;

    .line 255
    move-object v11, p1

    invoke-virtual {v1, p1, p3, v9}, Lmiui/security/SecurityManagerInternal;->checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v1

    if-nez v1, :cond_4

    if-eqz v8, :cond_3

    .line 256
    invoke-virtual {p3}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v3, 0x100000

    and-int/2addr v1, v3

    if-eqz v1, :cond_3

    goto :goto_1

    .line 259
    :cond_3
    const/4 v1, 0x1

    move-object v2, p1

    move-object v3, p3

    move/from16 v4, p5

    move/from16 v5, p4

    move/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v1 .. v7}, Lmiui/security/SecurityManager;->getCheckAccessIntent(ZLjava/lang/String;Landroid/content/Intent;IZILandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    return-object v1

    .line 257
    :cond_4
    :goto_1
    return-object v2
.end method

.method public getCheckGameBoosterAntiMsgIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;ZII)Landroid/content/Intent;
    .locals 9
    .param p1, "callPackageName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "fromActivity"    # Z
    .param p5, "requestCode"    # I
    .param p6, "userId"    # I

    .line 236
    const-string v0, "com.miui.securitycenter"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mSmi:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v0, p6}, Lmiui/security/SecurityManagerInternal;->isGameBoosterActive(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 239
    :cond_0
    iget-object v0, p0, Lmiui/app/ActivitySecurityHelper;->mSmi:Lmiui/security/SecurityManagerInternal;

    invoke-virtual {v0, p2, p3}, Lmiui/security/SecurityManagerInternal;->checkGameBoosterAntiMsgPassAsUser(Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    return-object v1

    .line 242
    :cond_1
    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    move-object v4, p3

    move v5, p5

    move v6, p4

    move v7, p6

    invoke-static/range {v2 .. v8}, Lmiui/security/SecurityManager;->getCheckAccessIntent(ZLjava/lang/String;Landroid/content/Intent;IZILandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 237
    :cond_2
    :goto_0
    return-object v1
.end method

.method public getCheckIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;ZIZIILandroid/os/Bundle;)Landroid/content/Intent;
    .locals 17
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "fromActivity"    # Z
    .param p5, "requestCode"    # I
    .param p6, "calleeAlreadyStarted"    # Z
    .param p7, "userId"    # I
    .param p8, "callingUid"    # I
    .param p9, "bOptions"    # Landroid/os/Bundle;

    .line 58
    move-object/from16 v9, p0

    move-object/from16 v10, p2

    invoke-static/range {p8 .. p8}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v11

    .line 59
    .local v11, "callingUser":I
    move-object/from16 v12, p1

    invoke-direct {v9, v12, v11}, Lmiui/app/ActivitySecurityHelper;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v13

    .line 60
    .local v13, "callerInfo":Landroid/content/pm/ApplicationInfo;
    move/from16 v14, p7

    invoke-direct {v9, v10, v14}, Lmiui/app/ActivitySecurityHelper;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v15

    .line 61
    .local v15, "calleeInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v13, :cond_4

    if-nez v15, :cond_0

    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, v9, Lmiui/app/ActivitySecurityHelper;->mContext:Landroid/content/Context;

    move-object/from16 v8, p3

    move/from16 v7, p4

    move/from16 v6, p5

    invoke-static {v0, v10, v8, v7, v6}, Lmiui/security/AppRunningControlManager;->getBlockActivityIntent(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;ZI)Landroid/content/Intent;

    move-result-object v16

    .line 67
    .local v16, "ret":Landroid/content/Intent;
    if-eqz v16, :cond_1

    .line 68
    return-object v16

    .line 71
    :cond_1
    move-object/from16 v0, p0

    move-object v1, v13

    move-object v2, v15

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move v8, v11

    invoke-virtual/range {v0 .. v8}, Lmiui/app/ActivitySecurityHelper;->getCheckStartActivityIntent(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/Intent;ZIZII)Landroid/content/Intent;

    move-result-object v7

    .line 73
    .end local v16    # "ret":Landroid/content/Intent;
    .local v7, "ret":Landroid/content/Intent;
    if-eqz v7, :cond_2

    .line 74
    return-object v7

    .line 77
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p7

    invoke-virtual/range {v0 .. v6}, Lmiui/app/ActivitySecurityHelper;->getCheckGameBoosterAntiMsgIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v8

    .line 79
    .end local v7    # "ret":Landroid/content/Intent;
    .local v8, "ret":Landroid/content/Intent;
    if-eqz v8, :cond_3

    .line 80
    return-object v8

    .line 83
    :cond_3
    iget v2, v15, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p7

    move-object/from16 v7, p9

    invoke-virtual/range {v0 .. v7}, Lmiui/app/ActivitySecurityHelper;->getCheckAccessControlIntent(Ljava/lang/String;ILandroid/content/Intent;ZIILandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 85
    .end local v8    # "ret":Landroid/content/Intent;
    .local v0, "ret":Landroid/content/Intent;
    return-object v0

    .line 62
    .end local v0    # "ret":Landroid/content/Intent;
    :cond_4
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCheckStartActivityIntent(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/Intent;ZIZII)Landroid/content/Intent;
    .locals 29
    .param p1, "callerInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "calleeInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "fromActivity"    # Z
    .param p5, "requestCode"    # I
    .param p6, "calleeAlreadyStarted"    # Z
    .param p7, "userId"    # I
    .param p8, "callingUser"    # I

    .line 99
    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    const/4 v7, 0x0

    if-eqz p3, :cond_c

    invoke-static {}, Landroid/miui/AppOpsUtils;->isXOptMode()Z

    move-result v0

    if-nez v0, :cond_c

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    goto/16 :goto_3

    .line 103
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p7

    invoke-direct/range {v0 .. v6}, Lmiui/app/ActivitySecurityHelper;->checkShareActionForStorageRestricted(Landroid/content/pm/ApplicationInfo;Landroid/content/pm/ApplicationInfo;Landroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v0

    move-object v13, v0

    .local v13, "result":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 105
    return-object v13

    .line 107
    :cond_1
    iget v0, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_b

    .line 108
    invoke-virtual/range {p1 .. p1}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v0

    if-eqz v0, :cond_2

    goto/16 :goto_2

    .line 113
    :cond_2
    iget v0, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0}, Landroid/os/UserHandle;->getAppId(I)I

    move-result v0

    if-lt v0, v1, :cond_a

    .line 114
    invoke-virtual/range {p2 .. p2}, Landroid/content/pm/ApplicationInfo;->isSystemApp()Z

    move-result v0

    if-eqz v0, :cond_3

    goto/16 :goto_1

    .line 120
    :cond_3
    iget-object v0, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    if-eqz v0, :cond_4

    .line 123
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v14

    const-string v15, "recentTask"

    iget-object v0, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v17, 0x1

    .line 124
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v18

    const/16 v20, 0x1

    .line 123
    move-object/from16 v16, v0

    move/from16 v19, p7

    invoke-virtual/range {v14 .. v20}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 127
    :cond_4
    iget-object v0, v10, Lmiui/app/ActivitySecurityHelper;->mSecurityManagerInternal:Lmiui/security/SecurityManagerInternal;

    iget-object v1, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lmiui/security/SecurityManagerInternal;->exemptByRestrictChain(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v14

    .line 129
    .local v14, "exemptByRestrictChain":Z
    iget-object v0, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct/range {p0 .. p1}, Lmiui/app/ActivitySecurityHelper;->restrictForChain(Landroid/content/pm/ApplicationInfo;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-nez v14, :cond_5

    .line 131
    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v3, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lmiui/app/ActivitySecurityHelper;->buildIntentForRestrictChain(Landroid/content/pm/ApplicationInfo;Ljava/lang/String;ILandroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 135
    :cond_5
    if-eqz v14, :cond_6

    .line 136
    return-object v7

    .line 139
    :cond_6
    if-eqz p6, :cond_7

    .line 140
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v0

    iget-object v1, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v6, 0x1

    move/from16 v4, p8

    move/from16 v5, p7

    invoke-virtual/range {v0 .. v6}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 142
    return-object v7

    .line 145
    :cond_7
    iget-object v0, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, v10, Lmiui/app/ActivitySecurityHelper;->mSmi:Lmiui/security/SecurityManagerInternal;

    iget-object v1, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 146
    move-object/from16 v3, p3

    invoke-virtual/range {v0 .. v5}, Lmiui/security/SecurityManagerInternal;->checkAllowStartActivity(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;II)Z

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_0

    .line 154
    :cond_8
    iget-object v1, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v4, v11, Landroid/content/pm/ApplicationInfo;->uid:I

    iget v5, v12, Landroid/content/pm/ApplicationInfo;->uid:I

    const/4 v9, -0x1

    move-object/from16 v0, p0

    move/from16 v3, p7

    move-object/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    invoke-direct/range {v0 .. v9}, Lmiui/app/ActivitySecurityHelper;->buildStartIntent(Ljava/lang/String;Ljava/lang/String;IIILandroid/content/Intent;ZII)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 148
    :cond_9
    :goto_0
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v15

    iget-object v0, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v18, 0x1

    const/16 v21, 0x1

    move-object/from16 v16, v0

    move-object/from16 v17, v1

    move/from16 v19, p8

    move/from16 v20, p7

    invoke-virtual/range {v15 .. v21}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 151
    return-object v7

    .line 115
    .end local v14    # "exemptByRestrictChain":Z
    :cond_a
    :goto_1
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v22

    iget-object v0, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v25, 0x1

    const/16 v28, 0x1

    move-object/from16 v23, v0

    move-object/from16 v24, v1

    move/from16 v26, p8

    move/from16 v27, p7

    invoke-virtual/range {v22 .. v28}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 117
    return-object v7

    .line 109
    :cond_b
    :goto_2
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v14

    iget-object v15, v11, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v0, v12, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v17, 0x1

    const/16 v20, 0x1

    move-object/from16 v16, v0

    move/from16 v18, p8

    move/from16 v19, p7

    invoke-virtual/range {v14 .. v20}, Lmiui/security/WakePathChecker;->recordWakePathCall(Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 111
    return-object v7

    .line 100
    .end local v13    # "result":Landroid/content/Intent;
    :cond_c
    :goto_3
    return-object v7
.end method
