public class miui.app.StorageRestrictedPathManager {
	 /* .source "StorageRestrictedPathManager.java" */
	 /* # static fields */
	 private static final java.lang.String KEY_ISOLATED_GALLERY_PATH;
	 private static final java.lang.String KEY_ISOLATED_SOCIALITY_PATH;
	 public static final java.lang.String SPLIT_MULTI_PATH;
	 private static final java.lang.String TAG;
	 private static final android.net.Uri URI_ISOLATED_GALLERY_PATH;
	 private static final android.net.Uri URI_ISOLATED_SOCIALITY_PATH;
	 private static miui.app.StorageRestrictedPathManager sInstance;
	 /* # instance fields */
	 private final java.util.Set mGalleryPathSet;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final java.util.Set mSocialityPathSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static java.util.Set -$$Nest$fgetmGalleryPathSet ( miui.app.StorageRestrictedPathManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mGalleryPathSet;
} // .end method
static java.util.Set -$$Nest$fgetmSocialityPathSet ( miui.app.StorageRestrictedPathManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSocialityPathSet;
} // .end method
static void -$$Nest$mupdatePolicy ( miui.app.StorageRestrictedPathManager p0, android.content.ContentResolver p1, java.lang.String p2, java.util.Set p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lmiui/app/StorageRestrictedPathManager;->updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V */
return;
} // .end method
static android.net.Uri -$$Nest$sfgetURI_ISOLATED_GALLERY_PATH ( ) { //bridge//synthethic
/* .locals 1 */
v0 = miui.app.StorageRestrictedPathManager.URI_ISOLATED_GALLERY_PATH;
} // .end method
static android.net.Uri -$$Nest$sfgetURI_ISOLATED_SOCIALITY_PATH ( ) { //bridge//synthethic
/* .locals 1 */
v0 = miui.app.StorageRestrictedPathManager.URI_ISOLATED_SOCIALITY_PATH;
} // .end method
static miui.app.StorageRestrictedPathManager ( ) {
/* .locals 1 */
/* .line 36 */
final String v0 = "key_isolated_gallery_path"; // const-string v0, "key_isolated_gallery_path"
android.provider.Settings$Global .getUriFor ( v0 );
/* .line 38 */
final String v0 = "key_isolated_sociality_path"; // const-string v0, "key_isolated_sociality_path"
android.provider.Settings$Global .getUriFor ( v0 );
return;
} // .end method
private miui.app.StorageRestrictedPathManager ( ) {
/* .locals 1 */
/* .line 50 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 31 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mGalleryPathSet = v0;
/* .line 32 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mSocialityPathSet = v0;
/* .line 51 */
return;
} // .end method
public static synchronized miui.app.StorageRestrictedPathManager getInstance ( ) {
/* .locals 2 */
/* const-class v0, Lmiui/app/StorageRestrictedPathManager; */
/* monitor-enter v0 */
/* .line 44 */
try { // :try_start_0
v1 = miui.app.StorageRestrictedPathManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 45 */
/* new-instance v1, Lmiui/app/StorageRestrictedPathManager; */
/* invoke-direct {v1}, Lmiui/app/StorageRestrictedPathManager;-><init>()V */
/* .line 47 */
} // :cond_0
v1 = miui.app.StorageRestrictedPathManager.sInstance;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit v0 */
/* .line 43 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* throw v1 */
} // .end method
private static Boolean isAllowAccessSpecialPath ( android.app.AppOpsManager p0, Integer p1, java.lang.String p2, Integer p3, android.content.Intent p4 ) {
/* .locals 11 */
/* .param p0, "aom" # Landroid/app/AppOpsManager; */
/* .param p1, "code" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .param p4, "intent" # Landroid/content/Intent; */
/* .line 112 */
miui.app.StorageRestrictedPathManager .getInstance ( );
(( miui.app.StorageRestrictedPathManager ) v0 ).getRestrictedPathSet ( p1 ); // invoke-virtual {v0, p1}, Lmiui/app/StorageRestrictedPathManager;->getRestrictedPathSet(I)Ljava/util/Set;
/* .line 113 */
/* .local v0, "restrictedArray":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v1 = 1; // const/4 v1, 0x1
v2 = if ( v0 != null) { // if-eqz v0, :cond_6
/* if-nez v2, :cond_0 */
/* .line 116 */
} // :cond_0
v2 = (( android.app.AppOpsManager ) p0 ).checkOpNoThrow ( p1, p3, p2 ); // invoke-virtual {p0, p1, p3, p2}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I
/* .line 117 */
/* .local v2, "mode":I */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 118 */
int v3 = 0; // const/4 v3, 0x0
if ( p4 != null) { // if-eqz p4, :cond_4
(( android.content.Intent ) p4 ).getClipData ( ); // invoke-virtual {p4}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;
if ( v4 != null) { // if-eqz v4, :cond_4
	 /* .line 119 */
	 (( android.content.Intent ) p4 ).getClipData ( ); // invoke-virtual {p4}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;
	 /* .line 120 */
	 /* .local v4, "data":Landroid/content/ClipData; */
	 int v5 = 0; // const/4 v5, 0x0
	 /* .local v5, "i":I */
} // :goto_0
v6 = (( android.content.ClipData ) v4 ).getItemCount ( ); // invoke-virtual {v4}, Landroid/content/ClipData;->getItemCount()I
/* if-ge v5, v6, :cond_3 */
/* .line 121 */
(( android.content.ClipData ) v4 ).getItemAt ( v5 ); // invoke-virtual {v4, v5}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;
(( android.content.ClipData$Item ) v6 ).getUri ( ); // invoke-virtual {v6}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;
/* .line 122 */
/* .local v6, "uri":Landroid/net/Uri; */
if ( v6 != null) { // if-eqz v6, :cond_2
	 /* .line 123 */
v8 = } // :goto_1
if ( v8 != null) { // if-eqz v8, :cond_2
	 /* check-cast v8, Ljava/lang/String; */
	 /* .line 124 */
	 /* .local v8, "restricted":Ljava/lang/String; */
	 (( android.net.Uri ) v6 ).getLastPathSegment ( ); // invoke-virtual {v6}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;
	 (( java.lang.String ) v9 ).toLowerCase ( ); // invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
	 /* .line 125 */
	 (( java.lang.String ) v8 ).toLowerCase ( ); // invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
	 /* .line 124 */
	 v9 = 	 (( java.lang.String ) v9 ).startsWith ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
	 if ( v9 != null) { // if-eqz v9, :cond_1
		 /* .line 126 */
		 /* .line 128 */
	 } // .end local v8 # "restricted":Ljava/lang/String;
} // :cond_1
/* .line 120 */
} // .end local v6 # "uri":Landroid/net/Uri;
} // :cond_2
/* add-int/lit8 v5, v5, 0x1 */
/* .line 131 */
} // .end local v4 # "data":Landroid/content/ClipData;
} // .end local v5 # "i":I
} // :cond_3
/* .line 132 */
} // :cond_4
/* .line 135 */
} // :cond_5
} // :goto_2
/* .line 114 */
} // .end local v2 # "mode":I
} // :cond_6
} // :goto_3
} // .end method
public static Boolean isDenyAccessGallery ( android.content.Context p0, java.lang.String p1, Integer p2, android.content.Intent p3 ) {
/* .locals 6 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "intent" # Landroid/content/Intent; */
/* .line 95 */
android.os.Binder .clearCallingIdentity ( );
/* move-result-wide v0 */
/* .line 97 */
/* .local v0, "identity":J */
try { // :try_start_0
final String v2 = "appops"; // const-string v2, "appops"
(( android.content.Context ) p0 ).getSystemService ( v2 ); // invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v2, Landroid/app/AppOpsManager; */
/* .line 98 */
/* .local v2, "aom":Landroid/app/AppOpsManager; */
/* const/16 v3, 0x2732 */
v3 = miui.app.StorageRestrictedPathManager .isAllowAccessSpecialPath ( v2,v3,p1,p2,p3 );
/* .line 100 */
/* .local v3, "isAllowGallery":Z */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 101 */
/* const/16 v5, 0x2733 */
v5 = miui.app.StorageRestrictedPathManager .isAllowAccessSpecialPath ( v2,v5,p1,p2,p3 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 106 */
/* xor-int/2addr v4, v5 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 101 */
/* .line 104 */
} // :cond_0
/* nop */
/* .line 106 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 104 */
/* .line 106 */
} // .end local v2 # "aom":Landroid/app/AppOpsManager;
} // .end local v3 # "isAllowGallery":Z
/* :catchall_0 */
/* move-exception v2 */
android.os.Binder .restoreCallingIdentity ( v0,v1 );
/* .line 107 */
/* throw v2 */
} // .end method
private void updatePolicy ( android.content.ContentResolver p0, java.lang.String p1, java.util.Set p2 ) {
/* .locals 4 */
/* .param p1, "resolver" # Landroid/content/ContentResolver; */
/* .param p2, "key" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/ContentResolver;", */
/* "Ljava/lang/String;", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 72 */
/* .local p3, "targetSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* .line 73 */
android.provider.Settings$Global .getString ( p1,p2 );
/* .line 74 */
/* .local v0, "rawPath":Ljava/lang/String; */
v1 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v1, :cond_0 */
/* .line 75 */
final String v1 = ";"; // const-string v1, ";"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 76 */
/* .local v1, "galleryPathArray":[Ljava/lang/String; */
java.util.stream.Stream .of ( v1 );
java.util.stream.Collectors .toSet ( );
/* check-cast v2, Ljava/util/Collection; */
/* .line 77 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "policy has filled with "; // const-string v3, "policy has filled with "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "StorageRestrictedPathManager"; // const-string v3, "StorageRestrictedPathManager"
android.util.Log .i ( v3,v2 );
/* .line 79 */
} // .end local v1 # "galleryPathArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
/* # virtual methods */
public java.util.Set getRestrictedPathSet ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 82 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 90 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 86 */
/* :pswitch_0 */
v0 = this.mSocialityPathSet;
/* .line 84 */
/* :pswitch_1 */
v0 = this.mGalleryPathSet;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2732 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void init ( android.content.Context p0 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 54 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 55 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* new-instance v1, Lmiui/app/StorageRestrictedPathManager$1; */
com.android.internal.os.BackgroundThread .getHandler ( );
/* invoke-direct {v1, p0, v2, v0}, Lmiui/app/StorageRestrictedPathManager$1;-><init>(Lmiui/app/StorageRestrictedPathManager;Landroid/os/Handler;Landroid/content/ContentResolver;)V */
/* .line 65 */
/* .local v1, "observer":Landroid/database/ContentObserver; */
v2 = miui.app.StorageRestrictedPathManager.URI_ISOLATED_GALLERY_PATH;
int v3 = 0; // const/4 v3, 0x0
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 66 */
v2 = miui.app.StorageRestrictedPathManager.URI_ISOLATED_SOCIALITY_PATH;
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v3, v1 ); // invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 67 */
final String v2 = "key_isolated_gallery_path"; // const-string v2, "key_isolated_gallery_path"
v3 = this.mGalleryPathSet;
/* invoke-direct {p0, v0, v2, v3}, Lmiui/app/StorageRestrictedPathManager;->updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V */
/* .line 68 */
final String v2 = "key_isolated_sociality_path"; // const-string v2, "key_isolated_sociality_path"
v3 = this.mSocialityPathSet;
/* invoke-direct {p0, v0, v2, v3}, Lmiui/app/StorageRestrictedPathManager;->updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V */
/* .line 69 */
return;
} // .end method
