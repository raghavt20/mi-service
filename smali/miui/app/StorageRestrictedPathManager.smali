.class public Lmiui/app/StorageRestrictedPathManager;
.super Ljava/lang/Object;
.source "StorageRestrictedPathManager.java"


# static fields
.field private static final KEY_ISOLATED_GALLERY_PATH:Ljava/lang/String; = "key_isolated_gallery_path"

.field private static final KEY_ISOLATED_SOCIALITY_PATH:Ljava/lang/String; = "key_isolated_sociality_path"

.field public static final SPLIT_MULTI_PATH:Ljava/lang/String; = ";"

.field private static final TAG:Ljava/lang/String; = "StorageRestrictedPathManager"

.field private static final URI_ISOLATED_GALLERY_PATH:Landroid/net/Uri;

.field private static final URI_ISOLATED_SOCIALITY_PATH:Landroid/net/Uri;

.field private static sInstance:Lmiui/app/StorageRestrictedPathManager;


# instance fields
.field private final mGalleryPathSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSocialityPathSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$fgetmGalleryPathSet(Lmiui/app/StorageRestrictedPathManager;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lmiui/app/StorageRestrictedPathManager;->mGalleryPathSet:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSocialityPathSet(Lmiui/app/StorageRestrictedPathManager;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lmiui/app/StorageRestrictedPathManager;->mSocialityPathSet:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdatePolicy(Lmiui/app/StorageRestrictedPathManager;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiui/app/StorageRestrictedPathManager;->updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetURI_ISOLATED_GALLERY_PATH()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lmiui/app/StorageRestrictedPathManager;->URI_ISOLATED_GALLERY_PATH:Landroid/net/Uri;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetURI_ISOLATED_SOCIALITY_PATH()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lmiui/app/StorageRestrictedPathManager;->URI_ISOLATED_SOCIALITY_PATH:Landroid/net/Uri;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 36
    const-string v0, "key_isolated_gallery_path"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lmiui/app/StorageRestrictedPathManager;->URI_ISOLATED_GALLERY_PATH:Landroid/net/Uri;

    .line 38
    const-string v0, "key_isolated_sociality_path"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lmiui/app/StorageRestrictedPathManager;->URI_ISOLATED_SOCIALITY_PATH:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/StorageRestrictedPathManager;->mGalleryPathSet:Ljava/util/Set;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmiui/app/StorageRestrictedPathManager;->mSocialityPathSet:Ljava/util/Set;

    .line 51
    return-void
.end method

.method public static declared-synchronized getInstance()Lmiui/app/StorageRestrictedPathManager;
    .locals 2

    const-class v0, Lmiui/app/StorageRestrictedPathManager;

    monitor-enter v0

    .line 44
    :try_start_0
    sget-object v1, Lmiui/app/StorageRestrictedPathManager;->sInstance:Lmiui/app/StorageRestrictedPathManager;

    if-nez v1, :cond_0

    .line 45
    new-instance v1, Lmiui/app/StorageRestrictedPathManager;

    invoke-direct {v1}, Lmiui/app/StorageRestrictedPathManager;-><init>()V

    sput-object v1, Lmiui/app/StorageRestrictedPathManager;->sInstance:Lmiui/app/StorageRestrictedPathManager;

    .line 47
    :cond_0
    sget-object v1, Lmiui/app/StorageRestrictedPathManager;->sInstance:Lmiui/app/StorageRestrictedPathManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static isAllowAccessSpecialPath(Landroid/app/AppOpsManager;ILjava/lang/String;ILandroid/content/Intent;)Z
    .locals 11
    .param p0, "aom"    # Landroid/app/AppOpsManager;
    .param p1, "code"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "intent"    # Landroid/content/Intent;

    .line 112
    invoke-static {}, Lmiui/app/StorageRestrictedPathManager;->getInstance()Lmiui/app/StorageRestrictedPathManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmiui/app/StorageRestrictedPathManager;->getRestrictedPathSet(I)Ljava/util/Set;

    move-result-object v0

    .line 113
    .local v0, "restrictedArray":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x1

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_3

    .line 116
    :cond_0
    invoke-virtual {p0, p1, p3, p2}, Landroid/app/AppOpsManager;->checkOpNoThrow(IILjava/lang/String;)I

    move-result v2

    .line 117
    .local v2, "mode":I
    if-eqz v2, :cond_5

    .line 118
    const/4 v3, 0x0

    if-eqz p4, :cond_4

    invoke-virtual {p4}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 119
    invoke-virtual {p4}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v4

    .line 120
    .local v4, "data":Landroid/content/ClipData;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v4}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 121
    invoke-virtual {v4, v5}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v6

    .line 122
    .local v6, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_2

    .line 123
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 124
    .local v8, "restricted":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    .line 125
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    .line 124
    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 126
    return v3

    .line 128
    .end local v8    # "restricted":Ljava/lang/String;
    :cond_1
    goto :goto_1

    .line 120
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 131
    .end local v4    # "data":Landroid/content/ClipData;
    .end local v5    # "i":I
    :cond_3
    goto :goto_2

    .line 132
    :cond_4
    return v3

    .line 135
    :cond_5
    :goto_2
    return v1

    .line 114
    .end local v2    # "mode":I
    :cond_6
    :goto_3
    return v1
.end method

.method public static isDenyAccessGallery(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .line 95
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 97
    .local v0, "identity":J
    :try_start_0
    const-string v2, "appops"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AppOpsManager;

    .line 98
    .local v2, "aom":Landroid/app/AppOpsManager;
    const/16 v3, 0x2732

    invoke-static {v2, v3, p1, p2, p3}, Lmiui/app/StorageRestrictedPathManager;->isAllowAccessSpecialPath(Landroid/app/AppOpsManager;ILjava/lang/String;ILandroid/content/Intent;)Z

    move-result v3

    .line 100
    .local v3, "isAllowGallery":Z
    const/4 v4, 0x1

    if-eqz v3, :cond_0

    .line 101
    const/16 v5, 0x2733

    invoke-static {v2, v5, p1, p2, p3}, Lmiui/app/StorageRestrictedPathManager;->isAllowAccessSpecialPath(Landroid/app/AppOpsManager;ILjava/lang/String;ILandroid/content/Intent;)Z

    move-result v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    xor-int/2addr v4, v5

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 101
    return v4

    .line 104
    :cond_0
    nop

    .line 106
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 104
    return v4

    .line 106
    .end local v2    # "aom":Landroid/app/AppOpsManager;
    .end local v3    # "isAllowGallery":Z
    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 107
    throw v2
.end method

.method private updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V
    .locals 4
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 72
    .local p3, "targetSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p3}, Ljava/util/Set;->clear()V

    .line 73
    invoke-static {p1, p2}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "rawPath":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "galleryPathArray":[Ljava/lang/String;
    invoke-static {v1}, Ljava/util/stream/Stream;->of([Ljava/lang/Object;)Ljava/util/stream/Stream;

    move-result-object v2

    invoke-static {}, Ljava/util/stream/Collectors;->toSet()Ljava/util/stream/Collector;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {p3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "policy has filled with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "StorageRestrictedPathManager"

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    .end local v1    # "galleryPathArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public getRestrictedPathSet(I)Ljava/util/Set;
    .locals 1
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 82
    packed-switch p1, :pswitch_data_0

    .line 90
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0

    .line 86
    :pswitch_0
    iget-object v0, p0, Lmiui/app/StorageRestrictedPathManager;->mSocialityPathSet:Ljava/util/Set;

    return-object v0

    .line 84
    :pswitch_1
    iget-object v0, p0, Lmiui/app/StorageRestrictedPathManager;->mGalleryPathSet:Ljava/util/Set;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x2732
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 55
    .local v0, "resolver":Landroid/content/ContentResolver;
    new-instance v1, Lmiui/app/StorageRestrictedPathManager$1;

    invoke-static {}, Lcom/android/internal/os/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lmiui/app/StorageRestrictedPathManager$1;-><init>(Lmiui/app/StorageRestrictedPathManager;Landroid/os/Handler;Landroid/content/ContentResolver;)V

    .line 65
    .local v1, "observer":Landroid/database/ContentObserver;
    sget-object v2, Lmiui/app/StorageRestrictedPathManager;->URI_ISOLATED_GALLERY_PATH:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 66
    sget-object v2, Lmiui/app/StorageRestrictedPathManager;->URI_ISOLATED_SOCIALITY_PATH:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 67
    const-string v2, "key_isolated_gallery_path"

    iget-object v3, p0, Lmiui/app/StorageRestrictedPathManager;->mGalleryPathSet:Ljava/util/Set;

    invoke-direct {p0, v0, v2, v3}, Lmiui/app/StorageRestrictedPathManager;->updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V

    .line 68
    const-string v2, "key_isolated_sociality_path"

    iget-object v3, p0, Lmiui/app/StorageRestrictedPathManager;->mSocialityPathSet:Ljava/util/Set;

    invoke-direct {p0, v0, v2, v3}, Lmiui/app/StorageRestrictedPathManager;->updatePolicy(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V

    .line 69
    return-void
.end method
