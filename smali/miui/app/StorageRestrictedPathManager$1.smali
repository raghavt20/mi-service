.class Lmiui/app/StorageRestrictedPathManager$1;
.super Landroid/database/ContentObserver;
.source "StorageRestrictedPathManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmiui/app/StorageRestrictedPathManager;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmiui/app/StorageRestrictedPathManager;

.field final synthetic val$resolver:Landroid/content/ContentResolver;


# direct methods
.method constructor <init>(Lmiui/app/StorageRestrictedPathManager;Landroid/os/Handler;Landroid/content/ContentResolver;)V
    .locals 0
    .param p1, "this$0"    # Lmiui/app/StorageRestrictedPathManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 55
    iput-object p1, p0, Lmiui/app/StorageRestrictedPathManager$1;->this$0:Lmiui/app/StorageRestrictedPathManager;

    iput-object p3, p0, Lmiui/app/StorageRestrictedPathManager$1;->val$resolver:Landroid/content/ContentResolver;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 58
    invoke-static {}, Lmiui/app/StorageRestrictedPathManager;->-$$Nest$sfgetURI_ISOLATED_GALLERY_PATH()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lmiui/app/StorageRestrictedPathManager$1;->this$0:Lmiui/app/StorageRestrictedPathManager;

    iget-object v1, p0, Lmiui/app/StorageRestrictedPathManager$1;->val$resolver:Landroid/content/ContentResolver;

    const-string v2, "key_isolated_gallery_path"

    invoke-static {v0}, Lmiui/app/StorageRestrictedPathManager;->-$$Nest$fgetmGalleryPathSet(Lmiui/app/StorageRestrictedPathManager;)Ljava/util/Set;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lmiui/app/StorageRestrictedPathManager;->-$$Nest$mupdatePolicy(Lmiui/app/StorageRestrictedPathManager;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V

    goto :goto_0

    .line 60
    :cond_0
    invoke-static {}, Lmiui/app/StorageRestrictedPathManager;->-$$Nest$sfgetURI_ISOLATED_SOCIALITY_PATH()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lmiui/app/StorageRestrictedPathManager$1;->this$0:Lmiui/app/StorageRestrictedPathManager;

    iget-object v1, p0, Lmiui/app/StorageRestrictedPathManager$1;->val$resolver:Landroid/content/ContentResolver;

    const-string v2, "key_isolated_sociality_path"

    invoke-static {v0}, Lmiui/app/StorageRestrictedPathManager;->-$$Nest$fgetmSocialityPathSet(Lmiui/app/StorageRestrictedPathManager;)Ljava/util/Set;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lmiui/app/StorageRestrictedPathManager;->-$$Nest$mupdatePolicy(Lmiui/app/StorageRestrictedPathManager;Landroid/content/ContentResolver;Ljava/lang/String;Ljava/util/Set;)V

    .line 63
    :cond_1
    :goto_0
    return-void
.end method
