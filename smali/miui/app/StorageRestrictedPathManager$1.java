class miui.app.StorageRestrictedPathManager$1 extends android.database.ContentObserver {
	 /* .source "StorageRestrictedPathManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lmiui/app/StorageRestrictedPathManager;->init(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final miui.app.StorageRestrictedPathManager this$0; //synthetic
final android.content.ContentResolver val$resolver; //synthetic
/* # direct methods */
 miui.app.StorageRestrictedPathManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lmiui/app/StorageRestrictedPathManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 55 */
this.this$0 = p1;
this.val$resolver = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 58 */
miui.app.StorageRestrictedPathManager .-$$Nest$sfgetURI_ISOLATED_GALLERY_PATH ( );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 59 */
	 v0 = this.this$0;
	 v1 = this.val$resolver;
	 final String v2 = "key_isolated_gallery_path"; // const-string v2, "key_isolated_gallery_path"
	 miui.app.StorageRestrictedPathManager .-$$Nest$fgetmGalleryPathSet ( v0 );
	 miui.app.StorageRestrictedPathManager .-$$Nest$mupdatePolicy ( v0,v1,v2,v3 );
	 /* .line 60 */
} // :cond_0
miui.app.StorageRestrictedPathManager .-$$Nest$sfgetURI_ISOLATED_SOCIALITY_PATH ( );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 61 */
	 v0 = this.this$0;
	 v1 = this.val$resolver;
	 final String v2 = "key_isolated_sociality_path"; // const-string v2, "key_isolated_sociality_path"
	 miui.app.StorageRestrictedPathManager .-$$Nest$fgetmSocialityPathSet ( v0 );
	 miui.app.StorageRestrictedPathManager .-$$Nest$mupdatePolicy ( v0,v1,v2,v3 );
	 /* .line 63 */
} // :cond_1
} // :goto_0
return;
} // .end method
