.class Landroid/hardware/fingerprint/MiFxTunnelAidl$2;
.super Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;
.source "MiFxTunnelAidl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/fingerprint/MiFxTunnelAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/fingerprint/MiFxTunnelAidl;


# direct methods
.method constructor <init>(Landroid/hardware/fingerprint/MiFxTunnelAidl;)V
    .locals 0
    .param p1, "this$0"    # Landroid/hardware/fingerprint/MiFxTunnelAidl;

    .line 171
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl$2;->this$0:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    invoke-direct {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterfaceHash()Ljava/lang/String;
    .locals 1

    .line 190
    const-string v0, "cac0cec9bbd7ce7545b32873c89cd67844627700"

    return-object v0
.end method

.method public getInterfaceVersion()I
    .locals 1

    .line 185
    const/4 v0, 0x1

    return v0
.end method

.method public onDaemonMessage(JII[B)V
    .locals 2
    .param p1, "devId"    # J
    .param p3, "msgId"    # I
    .param p4, "cmdId"    # I
    .param p5, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 174
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    if-ne p4, v0, :cond_0

    .line 175
    const-string v0, "FingerprintServiceAidl"

    const-string v1, "onDaemonMessage: callback has been replaced!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 177
    :cond_0
    invoke-static {p3, p4, p5}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->heartRateDataCallback(II[B)Z

    .line 179
    :goto_0
    return-void
.end method
