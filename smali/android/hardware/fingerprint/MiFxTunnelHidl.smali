.class public Landroid/hardware/fingerprint/MiFxTunnelHidl;
.super Ljava/lang/Object;
.source "MiFxTunnelHidl.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FingerprintServiceHidl"

.field private static volatile sInstance:Landroid/hardware/fingerprint/MiFxTunnelHidl;


# instance fields
.field private mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

.field private mHandler:Landroid/os/Handler;

.field private mMiFxTunnelCallback:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback;

.field private miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDeathRecipient(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Landroid/os/IHwBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    .locals 0

    iget-object p0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;)V
    .locals 0

    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetMiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    .locals 0

    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object p0

    return-object p0
.end method

.method private constructor <init>()V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    .line 53
    new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;

    invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;)V

    iput-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    .line 142
    new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelHidl$4;

    invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl$4;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;)V

    iput-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mMiFxTunnelCallback:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback;

    .line 35
    return-void
.end method

.method public static arrayListToByteArray(Ljava/util/ArrayList;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)[B"
        }
    .end annotation

    .line 166
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Byte;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Byte;

    .line 167
    .local v0, "byteArray":[Ljava/lang/Byte;
    const/4 v1, 0x0

    .line 168
    .local v1, "i":I
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [B

    .line 169
    .local v2, "result":[B
    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v0, v4

    .line 170
    .local v5, "b":Ljava/lang/Byte;
    add-int/lit8 v6, v1, 0x1

    .end local v1    # "i":I
    .local v6, "i":I
    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    aput-byte v7, v2, v1

    .line 169
    .end local v5    # "b":Ljava/lang/Byte;
    add-int/lit8 v4, v4, 0x1

    move v1, v6

    goto :goto_0

    .line 172
    .end local v6    # "i":I
    .restart local v1    # "i":I
    :cond_0
    return-object v2
.end method

.method public static byteArrayToArrayList([B)Ljava/util/ArrayList;
    .locals 4
    .param p0, "array"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    if-nez p0, :cond_0

    .line 157
    return-object v0

    .line 159
    :cond_0
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-byte v3, p0, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    .line 160
    .local v3, "b":Ljava/lang/Byte;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    .end local v3    # "b":Ljava/lang/Byte;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 162
    :cond_1
    return-object v0
.end method

.method public static getInstance()Landroid/hardware/fingerprint/MiFxTunnelHidl;
    .locals 3

    .line 38
    const-string v0, "FingerprintServiceHidl"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    if-nez v0, :cond_1

    .line 40
    const-string v0, "FingerprintServiceHidl"

    const-string v1, "getInstance null"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    const-class v0, Landroid/hardware/fingerprint/MiFxTunnelHidl;

    monitor-enter v0

    .line 42
    :try_start_0
    const-string v1, "FingerprintServiceHidl"

    const-string v2, "getInstance class"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    sget-object v1, Landroid/hardware/fingerprint/MiFxTunnelHidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    if-nez v1, :cond_0

    .line 44
    const-string v1, "FingerprintServiceHidl"

    const-string v2, "getInstance null 2"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    new-instance v1, Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-direct {v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;-><init>()V

    sput-object v1, Landroid/hardware/fingerprint/MiFxTunnelHidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    .line 47
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 49
    :cond_1
    :goto_0
    const-string v0, "FingerprintServiceHidl"

    const-string v1, "getInstance out"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    return-object v0
.end method

.method private getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 71
    const-string v0, "getMiFxTunnel"

    const-string v1, "FingerprintServiceHidl"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    if-nez v0, :cond_0

    .line 73
    const-string v0, "getMiFxTunnel1"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    invoke-static {}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;->getService()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v0

    iput-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    .line 75
    const-string v0, "getMiFxTunnel2"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mDeathRecipient:Landroid/os/IHwBinder$DeathRecipient;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;->linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z

    .line 78
    :cond_0
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->miFxTunnel:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;
    .locals 6
    .param p1, "cmdId"    # I
    .param p2, "params"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    .line 109
    :try_start_0
    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v0

    .line 110
    .local v0, "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    new-instance v1, Landroid/hardware/fingerprint/HalDataCmdResult;

    invoke-direct {v1}, Landroid/hardware/fingerprint/HalDataCmdResult;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    .local v1, "result":Landroid/hardware/fingerprint/HalDataCmdResult;
    if-eqz v0, :cond_0

    .line 113
    :try_start_1
    invoke-static {p2}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->byteArrayToArrayList([B)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;

    invoke-direct {v3, p0, p1, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;ILandroid/hardware/fingerprint/HalDataCmdResult;)V

    invoke-interface {v0, p1, v2, v3}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;->invokeCommand(ILjava/util/ArrayList;Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel$invokeCommandCallback;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    goto :goto_0

    .line 125
    .end local p0    # "this":Landroid/hardware/fingerprint/MiFxTunnelHidl;
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "FingerprintServiceHidl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get HalData failed. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 108
    .end local v0    # "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    .end local v1    # "result":Landroid/hardware/fingerprint/HalDataCmdResult;
    .end local p1    # "cmdId":I
    .end local p2    # "params":[B
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public registerCallback(Landroid/os/Handler;)V
    .locals 3
    .param p1, "handler"    # Landroid/os/Handler;

    .line 134
    :try_start_0
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mHandler:Landroid/os/Handler;

    .line 135
    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v0

    .line 136
    .local v0, "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl;->mMiFxTunnelCallback:Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback;

    invoke-interface {v0, v1}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;->setNotify(Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    .end local v0    # "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FingerprintServiceHidl"

    const-string v2, "registerCallback err"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public declared-synchronized sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
    .locals 4
    .param p1, "cmdId"    # I
    .param p2, "params"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    .line 82
    :try_start_0
    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v0

    .line 83
    .local v0, "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    new-instance v1, Landroid/hardware/fingerprint/HeartRateCmdResult;

    invoke-direct {v1}, Landroid/hardware/fingerprint/HeartRateCmdResult;-><init>()V

    .line 84
    .local v1, "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
    if-eqz v0, :cond_0

    .line 85
    invoke-static {p2}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->byteArrayToArrayList([B)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;

    invoke-direct {v3, p0, p1, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;ILandroid/hardware/fingerprint/HeartRateCmdResult;)V

    invoke-interface {v0, p1, v2, v3}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;->invokeCommand(ILjava/util/ArrayList;Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel$invokeCommandCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    .end local p0    # "this":Landroid/hardware/fingerprint/MiFxTunnelHidl;
    :cond_0
    monitor-exit p0

    return-object v1

    .line 81
    .end local v0    # "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
    .end local v1    # "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
    .end local p1    # "cmdId":I
    .end local p2    # "params":[B
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
