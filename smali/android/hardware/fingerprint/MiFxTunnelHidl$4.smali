.class Landroid/hardware/fingerprint/MiFxTunnelHidl$4;
.super Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback$Stub;
.source "MiFxTunnelHidl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/fingerprint/MiFxTunnelHidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;


# direct methods
.method constructor <init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;)V
    .locals 0
    .param p1, "this$0"    # Landroid/hardware/fingerprint/MiFxTunnelHidl;

    .line 142
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$4;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-direct {p0}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onMessage(JIILjava/util/ArrayList;)V
    .locals 2
    .param p1, "devId"    # J
    .param p3, "msgId"    # I
    .param p4, "cmdId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JII",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 145
    .local p5, "msg_data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    if-ne p4, v0, :cond_0

    .line 146
    const-string v0, "FingerprintServiceHidl"

    const-string v1, "onMessage: callback has been replaced!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 148
    :cond_0
    invoke-static {p5}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->arrayListToByteArray(Ljava/util/ArrayList;)[B

    move-result-object v0

    invoke-static {p3, p4, v0}, Lcom/android/server/biometrics/sensors/fingerprint/HeartRateFingerprintServiceStubImpl;->heartRateDataCallback(II[B)Z

    .line 150
    :goto_0
    return-void
.end method
