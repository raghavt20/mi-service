.class public Landroid/hardware/fingerprint/MiFxTunnelAidl;
.super Ljava/lang/Object;
.source "MiFxTunnelAidl.java"


# static fields
.field private static final DEFAULT:Ljava/lang/String; = "default"

.field private static final IMIFXTUNNEL_AIDL_INTERFACE:Ljava/lang/String; = "vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel/default"

.field private static final SERVICE_NAME_V1:Ljava/lang/String; = "vendor.xiaomi.hardware.fx.tunnel@1.0::IMiFxTunnel"

.field private static final TAG:Ljava/lang/String; = "FingerprintServiceAidl"

.field private static volatile sInstance:Landroid/hardware/fingerprint/MiFxTunnelAidl;

.field private static xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;


# instance fields
.field private mDeathRecipientAidl:Landroid/os/IBinder$DeathRecipient;

.field private mHandler:Landroid/os/Handler;

.field private mMiFxTunnelCallback:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;


# direct methods
.method static bridge synthetic -$$Nest$fgetmDeathRecipientAidl(Landroid/hardware/fingerprint/MiFxTunnelAidl;)Landroid/os/IBinder$DeathRecipient;
    .locals 0

    iget-object p0, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mDeathRecipientAidl:Landroid/os/IBinder$DeathRecipient;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetMiFxTunnelAidl(Landroid/hardware/fingerprint/MiFxTunnelAidl;)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;
    .locals 0

    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetxFxAJ()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;
    .locals 1

    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfputxFxAJ(Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;)V
    .locals 0

    sput-object p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelAidl$1;

    invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl$1;-><init>(Landroid/hardware/fingerprint/MiFxTunnelAidl;)V

    iput-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mDeathRecipientAidl:Landroid/os/IBinder$DeathRecipient;

    .line 171
    new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelAidl$2;

    invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl$2;-><init>(Landroid/hardware/fingerprint/MiFxTunnelAidl;)V

    iput-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mMiFxTunnelCallback:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;

    .line 41
    return-void
.end method

.method public static arrayListToByteArray(Ljava/util/ArrayList;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)[B"
        }
    .end annotation

    .line 207
    .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Byte;

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Byte;

    .line 208
    .local v0, "byteArray":[Ljava/lang/Byte;
    const/4 v1, 0x0

    .line 209
    .local v1, "i":I
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [B

    .line 210
    .local v2, "result":[B
    array-length v3, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v0, v4

    .line 211
    .local v5, "b":Ljava/lang/Byte;
    add-int/lit8 v6, v1, 0x1

    .end local v1    # "i":I
    .local v6, "i":I
    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v7

    aput-byte v7, v2, v1

    .line 210
    .end local v5    # "b":Ljava/lang/Byte;
    add-int/lit8 v4, v4, 0x1

    move v1, v6

    goto :goto_0

    .line 213
    .end local v6    # "i":I
    .restart local v1    # "i":I
    :cond_0
    return-object v2
.end method

.method public static byteArrayToArrayList([B)Ljava/util/ArrayList;
    .locals 4
    .param p0, "array"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 197
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    if-nez p0, :cond_0

    .line 198
    return-object v0

    .line 200
    :cond_0
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-byte v3, p0, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    .line 201
    .local v3, "b":Ljava/lang/Byte;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    .end local v3    # "b":Ljava/lang/Byte;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 203
    :cond_1
    return-object v0
.end method

.method public static getInstance()Landroid/hardware/fingerprint/MiFxTunnelAidl;
    .locals 2

    .line 44
    const-string v0, "FingerprintServiceAidl"

    const-string v1, "getInstance"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    if-nez v0, :cond_1

    .line 46
    const-class v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;

    monitor-enter v0

    .line 47
    :try_start_0
    sget-object v1, Landroid/hardware/fingerprint/MiFxTunnelAidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    if-nez v1, :cond_0

    .line 48
    new-instance v1, Landroid/hardware/fingerprint/MiFxTunnelAidl;

    invoke-direct {v1}, Landroid/hardware/fingerprint/MiFxTunnelAidl;-><init>()V

    sput-object v1, Landroid/hardware/fingerprint/MiFxTunnelAidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    .line 50
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 52
    :cond_1
    :goto_0
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->sInstance:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    return-object v0
.end method

.method private getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;
    .locals 5

    .line 69
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    if-nez v0, :cond_2

    .line 70
    const-string/jumbo v0, "vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel/default"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 71
    .local v0, "binder":Landroid/os/IBinder;
    const-string v1, "FingerprintServiceAidl"

    if-nez v0, :cond_0

    .line 72
    const-string v2, "[JAVA] Getting vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel/default service daemon binder failed!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 75
    :cond_0
    :try_start_0
    invoke-static {v0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->asInterface(Landroid/os/IBinder;)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v2

    sput-object v2, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    .line 76
    if-nez v2, :cond_1

    .line 77
    const-string v2, "[JAVA] Getting IMiFxTunnel AIDL daemon interface failed!"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    :cond_1
    invoke-interface {v2}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mDeathRecipientAidl:Landroid/os/IBinder$DeathRecipient;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    goto :goto_1

    .line 81
    :catch_0
    move-exception v2

    .line 82
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[JAVA] linkToDeath failed. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    .end local v0    # "binder":Landroid/os/IBinder;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_2
    :goto_1
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    return-object v0
.end method


# virtual methods
.method public getAIDLHalDataInt([B)V
    .locals 5
    .param p1, "data"    # [B

    .line 120
    const/4 v0, 0x0

    .line 121
    .local v0, "currentIndex":I
    const/4 v1, 0x0

    .line 122
    .local v1, "val":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 123
    array-length v2, p1

    add-int/lit8 v3, v0, 0x4

    if-ge v2, v3, :cond_0

    .line 124
    return-void

    .line 126
    :cond_0
    add-int/lit8 v2, v0, 0x1

    .end local v0    # "currentIndex":I
    .local v2, "currentIndex":I
    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "currentIndex":I
    .local v3, "currentIndex":I
    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x8

    const v4, 0xff00

    and-int/2addr v2, v4

    or-int/2addr v0, v2

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "currentIndex":I
    .restart local v2    # "currentIndex":I
    aget-byte v3, p1, v3

    shl-int/lit8 v3, v3, 0x10

    const/high16 v4, 0xff0000

    and-int/2addr v3, v4

    or-int/2addr v0, v3

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "currentIndex":I
    .restart local v3    # "currentIndex":I
    aget-byte v2, p1, v2

    shl-int/lit8 v2, v2, 0x18

    const/high16 v4, -0x1000000

    and-int/2addr v2, v4

    or-int v1, v0, v2

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAIDLHalDataInt: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "FingerprintServiceAidl"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    goto :goto_0

    .line 133
    .end local v3    # "currentIndex":I
    .restart local v0    # "currentIndex":I
    :cond_1
    return-void
.end method

.method public declared-synchronized getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;
    .locals 5
    .param p1, "cmdId"    # I
    .param p2, "params"    # [B

    monitor-enter p0

    .line 136
    :try_start_0
    new-instance v0, Landroid/hardware/fingerprint/HalDataCmdResult;

    invoke-direct {v0}, Landroid/hardware/fingerprint/HalDataCmdResult;-><init>()V

    .line 137
    .local v0, "result":Landroid/hardware/fingerprint/HalDataCmdResult;
    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 138
    const-string v1, "FingerprintServiceAidl"

    const-string v2, "[JAVA] aidl daemon not found"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-object v0

    .line 142
    .end local p0    # "this":Landroid/hardware/fingerprint/MiFxTunnelAidl;
    :cond_0
    :try_start_1
    sget-object v1, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    if-eqz v1, :cond_2

    .line 143
    const/4 v2, 0x1

    new-array v3, v2, [B

    const/4 v4, 0x0

    aput-byte v2, v3, v4

    move-object p2, v3

    .line 144
    invoke-interface {v1, p1, p2}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;->invokeCommand(I[B)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;

    move-result-object v1

    .line 145
    .local v1, "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
    iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I

    if-nez v2, :cond_1

    .line 146
    iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I

    iput v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I

    .line 147
    iget-object v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->data:[B

    iput-object v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultData:[B

    .line 148
    iget-object v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultData:[B

    invoke-virtual {p0, v2}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getAIDLHalDataInt([B)V

    goto :goto_0

    .line 150
    :cond_1
    iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I

    iput v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I

    .line 151
    const/4 v2, 0x0

    iput-object v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultData:[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    .end local v1    # "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
    :cond_2
    :goto_0
    goto :goto_1

    .line 154
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "FingerprintServiceAidl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get HalData failed. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 157
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    monitor-exit p0

    return-object v0

    .line 135
    .end local v0    # "result":Landroid/hardware/fingerprint/HalDataCmdResult;
    .end local p1    # "cmdId":I
    .end local p2    # "params":[B
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public registerCallback(Landroid/os/Handler;)V
    .locals 3
    .param p1, "handler"    # Landroid/os/Handler;

    .line 162
    :try_start_0
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mHandler:Landroid/os/Handler;

    .line 163
    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 164
    sget-object v0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mMiFxTunnelCallback:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;

    invoke-interface {v0, v1}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;->setNotify(Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    goto :goto_0

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FingerprintServiceAidl"

    const-string v2, "registerCallback err"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public declared-synchronized sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
    .locals 9
    .param p1, "cmdId"    # I
    .param p2, "params"    # [B

    monitor-enter p0

    .line 90
    :try_start_0
    new-instance v0, Landroid/hardware/fingerprint/HeartRateCmdResult;

    invoke-direct {v0}, Landroid/hardware/fingerprint/HeartRateCmdResult;-><init>()V

    .line 91
    .local v0, "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
    invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v1

    if-nez v1, :cond_0

    .line 92
    const-string v1, "FingerprintServiceAidl"

    const-string v2, "[JAVA] aidl daemon not found"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    monitor-exit p0

    return-object v0

    .line 95
    .end local p0    # "this":Landroid/hardware/fingerprint/MiFxTunnelAidl;
    :cond_0
    :try_start_1
    const-string v1, "FingerprintServiceAidl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HeartRateCmdResult HeartRateCmdResult: cmdId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", params="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :try_start_2
    sget-object v1, Landroid/hardware/fingerprint/MiFxTunnelAidl;->xFxAJ:Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    if-eqz v1, :cond_2

    .line 98
    invoke-interface {v1, p1, p2}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;->invokeCommand(I[B)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;

    move-result-object v1

    .line 99
    .local v1, "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
    iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I

    if-nez v2, :cond_1

    .line 102
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-string v7, "com.mi.health"

    const/4 v8, 0x0

    move v5, p1

    invoke-interface/range {v3 .. v8}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I

    .line 103
    iget-object v2, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 104
    .local v2, "msg":Landroid/os/Message;
    iput p1, v2, Landroid/os/Message;->arg1:I

    .line 105
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 106
    iget v3, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I

    iput v3, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I

    .line 107
    iget-object v3, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->data:[B

    iput-object v3, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultData:[B

    .line 108
    .end local v2    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 109
    :cond_1
    iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I

    iput v2, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I

    .line 110
    const/4 v2, 0x0

    iput-object v2, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultData:[B
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 115
    .end local v1    # "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
    :cond_2
    :goto_0
    goto :goto_1

    .line 113
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v2, "FingerprintServiceAidl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[JAVA] transact failed. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 116
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_1
    monitor-exit p0

    return-object v0

    .line 89
    .end local v0    # "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
    .end local p1    # "cmdId":I
    .end local p2    # "params":[B
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
