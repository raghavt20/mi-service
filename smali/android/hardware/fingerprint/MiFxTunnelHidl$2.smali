.class Landroid/hardware/fingerprint/MiFxTunnelHidl$2;
.super Ljava/lang/Object;
.source "MiFxTunnelHidl.java"

# interfaces
.implements Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel$invokeCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/hardware/fingerprint/MiFxTunnelHidl;->sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

.field final synthetic val$cmdId:I

.field final synthetic val$result:Landroid/hardware/fingerprint/HeartRateCmdResult;


# direct methods
.method constructor <init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;ILandroid/hardware/fingerprint/HeartRateCmdResult;)V
    .locals 0
    .param p1, "this$0"    # Landroid/hardware/fingerprint/MiFxTunnelHidl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 85
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    iput p2, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I

    iput-object p3, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$result:Landroid/hardware/fingerprint/HeartRateCmdResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValues(ILjava/util/ArrayList;)V
    .locals 6
    .param p1, "resultCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .line 88
    .local p2, "out_buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    if-nez p1, :cond_0

    .line 92
    invoke-static {}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->getInstance()Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;

    move-result-object v0

    const/4 v1, 0x0

    iget v2, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I

    const/4 v3, 0x0

    const-string v4, "com.mi.health"

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I

    .line 93
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$fgetmHandler(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Landroid/os/Handler;

    move-result-object v0

    iget v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 94
    .local v0, "msg":Landroid/os/Message;
    iget v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 95
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 96
    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$result:Landroid/hardware/fingerprint/HeartRateCmdResult;

    iput p1, v1, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I

    .line 97
    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$result:Landroid/hardware/fingerprint/HeartRateCmdResult;

    invoke-static {p2}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->arrayListToByteArray(Ljava/util/ArrayList;)[B

    move-result-object v2

    iput-object v2, v1, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultData:[B

    .line 98
    .end local v0    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 99
    :cond_0
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$result:Landroid/hardware/fingerprint/HeartRateCmdResult;

    iput p1, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I

    .line 100
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$result:Landroid/hardware/fingerprint/HeartRateCmdResult;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultData:[B

    .line 102
    :goto_0
    return-void
.end method
