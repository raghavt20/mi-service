.class Landroid/hardware/fingerprint/MiFxTunnelHidl$1;
.super Ljava/lang/Object;
.source "MiFxTunnelHidl.java"

# interfaces
.implements Landroid/os/IHwBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/fingerprint/MiFxTunnelHidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;


# direct methods
.method constructor <init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;)V
    .locals 0
    .param p1, "this$0"    # Landroid/hardware/fingerprint/MiFxTunnelHidl;

    .line 53
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serviceDied(J)V
    .locals 2
    .param p1, "cookie"    # J

    .line 57
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$fgetmiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 58
    return-void

    .line 61
    :cond_0
    :try_start_0
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$fgetmiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v0

    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-static {v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$fgetmDeathRecipient(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Landroid/os/IHwBinder$DeathRecipient;

    move-result-object v1

    invoke-interface {v0, v1}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;->unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z

    .line 62
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$fputmiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;)V

    .line 63
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$mgetMiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;)Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->-$$Nest$fputmiFxTunnel(Landroid/hardware/fingerprint/MiFxTunnelHidl;Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 67
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
