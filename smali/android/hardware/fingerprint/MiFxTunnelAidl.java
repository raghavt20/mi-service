public class android.hardware.fingerprint.MiFxTunnelAidl {
	 /* .source "MiFxTunnelAidl.java" */
	 /* # static fields */
	 private static final java.lang.String DEFAULT;
	 private static final java.lang.String IMIFXTUNNEL_AIDL_INTERFACE;
	 private static final java.lang.String SERVICE_NAME_V1;
	 private static final java.lang.String TAG;
	 private static volatile android.hardware.fingerprint.MiFxTunnelAidl sInstance;
	 private static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel xFxAJ;
	 /* # instance fields */
	 private android.os.IBinder$DeathRecipient mDeathRecipientAidl;
	 private android.os.Handler mHandler;
	 private vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback mMiFxTunnelCallback;
	 /* # direct methods */
	 static android.os.IBinder$DeathRecipient -$$Nest$fgetmDeathRecipientAidl ( android.hardware.fingerprint.MiFxTunnelAidl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mDeathRecipientAidl;
	 } // .end method
	 static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel -$$Nest$mgetMiFxTunnelAidl ( android.hardware.fingerprint.MiFxTunnelAidl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
	 } // .end method
	 static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel -$$Nest$sfgetxFxAJ ( ) { //bridge//synthethic
		 /* .locals 1 */
		 v0 = android.hardware.fingerprint.MiFxTunnelAidl.xFxAJ;
	 } // .end method
	 static void -$$Nest$sfputxFxAJ ( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 return;
	 } // .end method
	 private android.hardware.fingerprint.MiFxTunnelAidl ( ) {
		 /* .locals 1 */
		 /* .line 41 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 55 */
		 /* new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelAidl$1; */
		 /* invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl$1;-><init>(Landroid/hardware/fingerprint/MiFxTunnelAidl;)V */
		 this.mDeathRecipientAidl = v0;
		 /* .line 171 */
		 /* new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelAidl$2; */
		 /* invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl$2;-><init>(Landroid/hardware/fingerprint/MiFxTunnelAidl;)V */
		 this.mMiFxTunnelCallback = v0;
		 /* .line 41 */
		 return;
	 } // .end method
	 public static arrayListToByteArray ( java.util.ArrayList p0 ) {
		 /* .locals 8 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/ArrayList<", */
		 /* "Ljava/lang/Byte;", */
		 /* ">;)[B" */
		 /* } */
	 } // .end annotation
	 /* .line 207 */
	 /* .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
	 v0 = 	 (( java.util.ArrayList ) p0 ).size ( ); // invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
	 /* new-array v0, v0, [Ljava/lang/Byte; */
	 (( java.util.ArrayList ) p0 ).toArray ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
	 /* check-cast v0, [Ljava/lang/Byte; */
	 /* .line 208 */
	 /* .local v0, "byteArray":[Ljava/lang/Byte; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 209 */
	 /* .local v1, "i":I */
	 v2 = 	 (( java.util.ArrayList ) p0 ).size ( ); // invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
	 /* new-array v2, v2, [B */
	 /* .line 210 */
	 /* .local v2, "result":[B */
	 /* array-length v3, v0 */
	 int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* aget-object v5, v0, v4 */
/* .line 211 */
/* .local v5, "b":Ljava/lang/Byte; */
/* add-int/lit8 v6, v1, 0x1 */
} // .end local v1 # "i":I
/* .local v6, "i":I */
v7 = (( java.lang.Byte ) v5 ).byteValue ( ); // invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v7, v2, v1 */
/* .line 210 */
} // .end local v5 # "b":Ljava/lang/Byte;
/* add-int/lit8 v4, v4, 0x1 */
/* move v1, v6 */
/* .line 213 */
} // .end local v6 # "i":I
/* .restart local v1 # "i":I */
} // :cond_0
} // .end method
public static java.util.ArrayList byteArrayToArrayList ( Object[] p0 ) {
/* .locals 4 */
/* .param p0, "array" # [B */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([B)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 196 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 197 */
/* .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* if-nez p0, :cond_0 */
/* .line 198 */
/* .line 200 */
} // :cond_0
/* array-length v1, p0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* aget-byte v3, p0, v2 */
java.lang.Byte .valueOf ( v3 );
/* .line 201 */
/* .local v3, "b":Ljava/lang/Byte; */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 200 */
} // .end local v3 # "b":Ljava/lang/Byte;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 203 */
} // :cond_1
} // .end method
public static android.hardware.fingerprint.MiFxTunnelAidl getInstance ( ) {
/* .locals 2 */
/* .line 44 */
final String v0 = "FingerprintServiceAidl"; // const-string v0, "FingerprintServiceAidl"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 45 */
v0 = android.hardware.fingerprint.MiFxTunnelAidl.sInstance;
/* if-nez v0, :cond_1 */
/* .line 46 */
/* const-class v0, Landroid/hardware/fingerprint/MiFxTunnelAidl; */
/* monitor-enter v0 */
/* .line 47 */
try { // :try_start_0
v1 = android.hardware.fingerprint.MiFxTunnelAidl.sInstance;
/* if-nez v1, :cond_0 */
/* .line 48 */
/* new-instance v1, Landroid/hardware/fingerprint/MiFxTunnelAidl; */
/* invoke-direct {v1}, Landroid/hardware/fingerprint/MiFxTunnelAidl;-><init>()V */
/* .line 50 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 52 */
} // :cond_1
} // :goto_0
v0 = android.hardware.fingerprint.MiFxTunnelAidl.sInstance;
} // .end method
private vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel getMiFxTunnelAidl ( ) {
/* .locals 5 */
/* .line 69 */
v0 = android.hardware.fingerprint.MiFxTunnelAidl.xFxAJ;
/* if-nez v0, :cond_2 */
/* .line 70 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel/default" */
android.os.ServiceManager .getService ( v0 );
/* .line 71 */
/* .local v0, "binder":Landroid/os/IBinder; */
final String v1 = "FingerprintServiceAidl"; // const-string v1, "FingerprintServiceAidl"
/* if-nez v0, :cond_0 */
/* .line 72 */
final String v2 = "[JAVA] Getting vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel/default service daemon binder failed!"; // const-string v2, "[JAVA] Getting vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel/default service daemon binder failed!"
android.util.Slog .e ( v1,v2 );
/* .line 75 */
} // :cond_0
try { // :try_start_0
vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub .asInterface ( v0 );
/* .line 76 */
/* if-nez v2, :cond_1 */
/* .line 77 */
final String v2 = "[JAVA] Getting IMiFxTunnel AIDL daemon interface failed!"; // const-string v2, "[JAVA] Getting IMiFxTunnel AIDL daemon interface failed!"
android.util.Slog .e ( v1,v2 );
/* .line 79 */
} // :cond_1
v3 = this.mDeathRecipientAidl;
int v4 = 0; // const/4 v4, 0x0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 83 */
} // :goto_0
/* .line 81 */
/* :catch_0 */
/* move-exception v2 */
/* .line 82 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[JAVA] linkToDeath failed."; // const-string v4, "[JAVA] linkToDeath failed."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v3 );
/* .line 86 */
} // .end local v0 # "binder":Landroid/os/IBinder;
} // .end local v2 # "e":Landroid/os/RemoteException;
} // :cond_2
} // :goto_1
v0 = android.hardware.fingerprint.MiFxTunnelAidl.xFxAJ;
} // .end method
/* # virtual methods */
public void getAIDLHalDataInt ( Object[] p0 ) {
/* .locals 5 */
/* .param p1, "data" # [B */
/* .line 120 */
int v0 = 0; // const/4 v0, 0x0
/* .line 121 */
/* .local v0, "currentIndex":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 122 */
/* .local v1, "val":I */
} // :goto_0
/* array-length v2, p1 */
/* if-ge v0, v2, :cond_1 */
/* .line 123 */
/* array-length v2, p1 */
/* add-int/lit8 v3, v0, 0x4 */
/* if-ge v2, v3, :cond_0 */
/* .line 124 */
return;
/* .line 126 */
} // :cond_0
/* add-int/lit8 v2, v0, 0x1 */
} // .end local v0 # "currentIndex":I
/* .local v2, "currentIndex":I */
/* aget-byte v0, p1, v0 */
/* and-int/lit16 v0, v0, 0xff */
/* add-int/lit8 v3, v2, 0x1 */
} // .end local v2 # "currentIndex":I
/* .local v3, "currentIndex":I */
/* aget-byte v2, p1, v2 */
/* shl-int/lit8 v2, v2, 0x8 */
/* const v4, 0xff00 */
/* and-int/2addr v2, v4 */
/* or-int/2addr v0, v2 */
/* add-int/lit8 v2, v3, 0x1 */
} // .end local v3 # "currentIndex":I
/* .restart local v2 # "currentIndex":I */
/* aget-byte v3, p1, v3 */
/* shl-int/lit8 v3, v3, 0x10 */
/* const/high16 v4, 0xff0000 */
/* and-int/2addr v3, v4 */
/* or-int/2addr v0, v3 */
/* add-int/lit8 v3, v2, 0x1 */
} // .end local v2 # "currentIndex":I
/* .restart local v3 # "currentIndex":I */
/* aget-byte v2, p1, v2 */
/* shl-int/lit8 v2, v2, 0x18 */
/* const/high16 v4, -0x1000000 */
/* and-int/2addr v2, v4 */
/* or-int v1, v0, v2 */
/* .line 130 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getAIDLHalDataInt: "; // const-string v2, "getAIDLHalDataInt: "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "FingerprintServiceAidl"; // const-string v2, "FingerprintServiceAidl"
android.util.Slog .i ( v2,v0 );
/* move v0, v3 */
/* .line 133 */
} // .end local v3 # "currentIndex":I
/* .restart local v0 # "currentIndex":I */
} // :cond_1
return;
} // .end method
public synchronized android.hardware.fingerprint.HalDataCmdResult getHalData ( Integer p0, Object[] p1 ) {
/* .locals 5 */
/* .param p1, "cmdId" # I */
/* .param p2, "params" # [B */
/* monitor-enter p0 */
/* .line 136 */
try { // :try_start_0
/* new-instance v0, Landroid/hardware/fingerprint/HalDataCmdResult; */
/* invoke-direct {v0}, Landroid/hardware/fingerprint/HalDataCmdResult;-><init>()V */
/* .line 137 */
/* .local v0, "result":Landroid/hardware/fingerprint/HalDataCmdResult; */
/* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
/* if-nez v1, :cond_0 */
/* .line 138 */
final String v1 = "FingerprintServiceAidl"; // const-string v1, "FingerprintServiceAidl"
final String v2 = "[JAVA] aidl daemon not found"; // const-string v2, "[JAVA] aidl daemon not found"
android.util.Slog .e ( v1,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 139 */
/* monitor-exit p0 */
/* .line 142 */
} // .end local p0 # "this":Landroid/hardware/fingerprint/MiFxTunnelAidl;
} // :cond_0
try { // :try_start_1
v1 = android.hardware.fingerprint.MiFxTunnelAidl.xFxAJ;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 143 */
int v2 = 1; // const/4 v2, 0x1
/* new-array v3, v2, [B */
int v4 = 0; // const/4 v4, 0x0
/* aput-byte v2, v3, v4 */
/* move-object p2, v3 */
/* .line 144 */
/* .line 145 */
/* .local v1, "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
/* iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* if-nez v2, :cond_1 */
/* .line 146 */
/* iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* iput v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I */
/* .line 147 */
v2 = this.data;
this.mResultData = v2;
/* .line 148 */
v2 = this.mResultData;
(( android.hardware.fingerprint.MiFxTunnelAidl ) p0 ).getAIDLHalDataInt ( v2 ); // invoke-virtual {p0, v2}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getAIDLHalDataInt([B)V
/* .line 150 */
} // :cond_1
/* iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* iput v2, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I */
/* .line 151 */
int v2 = 0; // const/4 v2, 0x0
this.mResultData = v2;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 156 */
} // .end local v1 # "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
} // :cond_2
} // :goto_0
/* .line 154 */
/* :catch_0 */
/* move-exception v1 */
/* .line 155 */
/* .local v1, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v2 = "FingerprintServiceAidl"; // const-string v2, "FingerprintServiceAidl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "get HalData failed."; // const-string v4, "get HalData failed."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 157 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
/* monitor-exit p0 */
/* .line 135 */
} // .end local v0 # "result":Landroid/hardware/fingerprint/HalDataCmdResult;
} // .end local p1 # "cmdId":I
} // .end local p2 # "params":[B
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void registerCallback ( android.os.Handler p0 ) {
/* .locals 3 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .line 162 */
try { // :try_start_0
this.mHandler = p1;
/* .line 163 */
/* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 164 */
v0 = android.hardware.fingerprint.MiFxTunnelAidl.xFxAJ;
v1 = this.mMiFxTunnelCallback;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 168 */
} // :cond_0
/* .line 166 */
/* :catch_0 */
/* move-exception v0 */
/* .line 167 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "FingerprintServiceAidl"; // const-string v1, "FingerprintServiceAidl"
final String v2 = "registerCallback err"; // const-string v2, "registerCallback err"
android.util.Slog .d ( v1,v2 );
/* .line 169 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public synchronized android.hardware.fingerprint.HeartRateCmdResult sendCommand ( Integer p0, Object[] p1 ) {
/* .locals 9 */
/* .param p1, "cmdId" # I */
/* .param p2, "params" # [B */
/* monitor-enter p0 */
/* .line 90 */
try { // :try_start_0
/* new-instance v0, Landroid/hardware/fingerprint/HeartRateCmdResult; */
/* invoke-direct {v0}, Landroid/hardware/fingerprint/HeartRateCmdResult;-><init>()V */
/* .line 91 */
/* .local v0, "result":Landroid/hardware/fingerprint/HeartRateCmdResult; */
/* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->getMiFxTunnelAidl()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
/* if-nez v1, :cond_0 */
/* .line 92 */
final String v1 = "FingerprintServiceAidl"; // const-string v1, "FingerprintServiceAidl"
final String v2 = "[JAVA] aidl daemon not found"; // const-string v2, "[JAVA] aidl daemon not found"
android.util.Slog .e ( v1,v2 );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 93 */
/* monitor-exit p0 */
/* .line 95 */
} // .end local p0 # "this":Landroid/hardware/fingerprint/MiFxTunnelAidl;
} // :cond_0
try { // :try_start_1
final String v1 = "FingerprintServiceAidl"; // const-string v1, "FingerprintServiceAidl"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "HeartRateCmdResult HeartRateCmdResult: cmdId="; // const-string v3, "HeartRateCmdResult HeartRateCmdResult: cmdId="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", params="; // const-string v3, ", params="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 97 */
try { // :try_start_2
v1 = android.hardware.fingerprint.MiFxTunnelAidl.xFxAJ;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 98 */
/* .line 99 */
/* .local v1, "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
/* iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* if-nez v2, :cond_1 */
/* .line 102 */
com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStub .getInstance ( );
int v4 = 0; // const/4 v4, 0x0
int v6 = 0; // const/4 v6, 0x0
final String v7 = "com.mi.health"; // const-string v7, "com.mi.health"
int v8 = 0; // const/4 v8, 0x0
/* move v5, p1 */
/* invoke-interface/range {v3 ..v8}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I */
/* .line 103 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( p1 ); // invoke-virtual {v2, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 104 */
/* .local v2, "msg":Landroid/os/Message; */
/* iput p1, v2, Landroid/os/Message;->arg1:I */
/* .line 105 */
(( android.os.Message ) v2 ).sendToTarget ( ); // invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
/* .line 106 */
/* iget v3, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* iput v3, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I */
/* .line 107 */
v3 = this.data;
this.mResultData = v3;
/* .line 108 */
} // .end local v2 # "msg":Landroid/os/Message;
/* .line 109 */
} // :cond_1
/* iget v2, v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* iput v2, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I */
/* .line 110 */
int v2 = 0; // const/4 v2, 0x0
this.mResultData = v2;
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 115 */
} // .end local v1 # "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
} // :cond_2
} // :goto_0
/* .line 113 */
/* :catch_0 */
/* move-exception v1 */
/* .line 114 */
/* .local v1, "e":Landroid/os/RemoteException; */
try { // :try_start_3
final String v2 = "FingerprintServiceAidl"; // const-string v2, "FingerprintServiceAidl"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "[JAVA] transact failed."; // const-string v4, "[JAVA] transact failed."
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 116 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_1
/* monitor-exit p0 */
/* .line 89 */
} // .end local v0 # "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
} // .end local p1 # "cmdId":I
} // .end local p2 # "params":[B
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
