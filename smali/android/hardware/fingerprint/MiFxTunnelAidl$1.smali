.class Landroid/hardware/fingerprint/MiFxTunnelAidl$1;
.super Ljava/lang/Object;
.source "MiFxTunnelAidl.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/hardware/fingerprint/MiFxTunnelAidl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/fingerprint/MiFxTunnelAidl;


# direct methods
.method constructor <init>(Landroid/hardware/fingerprint/MiFxTunnelAidl;)V
    .locals 0
    .param p1, "this$0"    # Landroid/hardware/fingerprint/MiFxTunnelAidl;

    .line 55
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .line 58
    const-string v0, "FingerprintServiceAidl"

    const-string/jumbo v1, "xFxAJ Died"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->-$$Nest$sfgetxFxAJ()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 60
    return-void

    .line 62
    :cond_0
    invoke-static {}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->-$$Nest$sfgetxFxAJ()Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v0

    invoke-interface {v0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    invoke-static {v1}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->-$$Nest$fgetmDeathRecipientAidl(Landroid/hardware/fingerprint/MiFxTunnelAidl;)Landroid/os/IBinder$DeathRecipient;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 63
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->-$$Nest$sfputxFxAJ(Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;)V

    .line 64
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelAidl$1;->this$0:Landroid/hardware/fingerprint/MiFxTunnelAidl;

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->-$$Nest$mgetMiFxTunnelAidl(Landroid/hardware/fingerprint/MiFxTunnelAidl;)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/fingerprint/MiFxTunnelAidl;->-$$Nest$sfputxFxAJ(Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;)V

    .line 65
    return-void
.end method
