public class android.hardware.fingerprint.MiFxTunnelHidl {
	 /* .source "MiFxTunnelHidl.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static volatile android.hardware.fingerprint.MiFxTunnelHidl sInstance;
	 /* # instance fields */
	 private android.os.IHwBinder$DeathRecipient mDeathRecipient;
	 private android.os.Handler mHandler;
	 private vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnelCallback mMiFxTunnelCallback;
	 private vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel miFxTunnel;
	 /* # direct methods */
	 static android.os.IHwBinder$DeathRecipient -$$Nest$fgetmDeathRecipient ( android.hardware.fingerprint.MiFxTunnelHidl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mDeathRecipient;
	 } // .end method
	 static android.os.Handler -$$Nest$fgetmHandler ( android.hardware.fingerprint.MiFxTunnelHidl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mHandler;
	 } // .end method
	 static vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel -$$Nest$fgetmiFxTunnel ( android.hardware.fingerprint.MiFxTunnelHidl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.miFxTunnel;
	 } // .end method
	 static void -$$Nest$fputmiFxTunnel ( android.hardware.fingerprint.MiFxTunnelHidl p0, vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.miFxTunnel = p1;
		 return;
	 } // .end method
	 static vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel -$$Nest$mgetMiFxTunnel ( android.hardware.fingerprint.MiFxTunnelHidl p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
	 } // .end method
	 private android.hardware.fingerprint.MiFxTunnelHidl ( ) {
		 /* .locals 1 */
		 /* .line 35 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 32 */
		 int v0 = 0; // const/4 v0, 0x0
		 this.miFxTunnel = v0;
		 /* .line 53 */
		 /* new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelHidl$1; */
		 /* invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl$1;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;)V */
		 this.mDeathRecipient = v0;
		 /* .line 142 */
		 /* new-instance v0, Landroid/hardware/fingerprint/MiFxTunnelHidl$4; */
		 /* invoke-direct {v0, p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl$4;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;)V */
		 this.mMiFxTunnelCallback = v0;
		 /* .line 35 */
		 return;
	 } // .end method
	 public static arrayListToByteArray ( java.util.ArrayList p0 ) {
		 /* .locals 8 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/ArrayList<", */
		 /* "Ljava/lang/Byte;", */
		 /* ">;)[B" */
		 /* } */
	 } // .end annotation
	 /* .line 166 */
	 /* .local p0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
	 v0 = 	 (( java.util.ArrayList ) p0 ).size ( ); // invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
	 /* new-array v0, v0, [Ljava/lang/Byte; */
	 (( java.util.ArrayList ) p0 ).toArray ( v0 ); // invoke-virtual {p0, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
	 /* check-cast v0, [Ljava/lang/Byte; */
	 /* .line 167 */
	 /* .local v0, "byteArray":[Ljava/lang/Byte; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 168 */
	 /* .local v1, "i":I */
	 v2 = 	 (( java.util.ArrayList ) p0 ).size ( ); // invoke-virtual {p0}, Ljava/util/ArrayList;->size()I
	 /* new-array v2, v2, [B */
	 /* .line 169 */
	 /* .local v2, "result":[B */
	 /* array-length v3, v0 */
	 int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* aget-object v5, v0, v4 */
/* .line 170 */
/* .local v5, "b":Ljava/lang/Byte; */
/* add-int/lit8 v6, v1, 0x1 */
} // .end local v1 # "i":I
/* .local v6, "i":I */
v7 = (( java.lang.Byte ) v5 ).byteValue ( ); // invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B
/* aput-byte v7, v2, v1 */
/* .line 169 */
} // .end local v5 # "b":Ljava/lang/Byte;
/* add-int/lit8 v4, v4, 0x1 */
/* move v1, v6 */
/* .line 172 */
} // .end local v6 # "i":I
/* .restart local v1 # "i":I */
} // :cond_0
} // .end method
public static java.util.ArrayList byteArrayToArrayList ( Object[] p0 ) {
/* .locals 4 */
/* .param p0, "array" # [B */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([B)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 155 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 156 */
/* .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* if-nez p0, :cond_0 */
/* .line 157 */
/* .line 159 */
} // :cond_0
/* array-length v1, p0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_1 */
/* aget-byte v3, p0, v2 */
java.lang.Byte .valueOf ( v3 );
/* .line 160 */
/* .local v3, "b":Ljava/lang/Byte; */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 159 */
} // .end local v3 # "b":Ljava/lang/Byte;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 162 */
} // :cond_1
} // .end method
public static android.hardware.fingerprint.MiFxTunnelHidl getInstance ( ) {
/* .locals 3 */
/* .line 38 */
final String v0 = "FingerprintServiceHidl"; // const-string v0, "FingerprintServiceHidl"
final String v1 = "getInstance"; // const-string v1, "getInstance"
android.util.Slog .d ( v0,v1 );
/* .line 39 */
v0 = android.hardware.fingerprint.MiFxTunnelHidl.sInstance;
/* if-nez v0, :cond_1 */
/* .line 40 */
final String v0 = "FingerprintServiceHidl"; // const-string v0, "FingerprintServiceHidl"
final String v1 = "getInstance null"; // const-string v1, "getInstance null"
android.util.Slog .d ( v0,v1 );
/* .line 41 */
/* const-class v0, Landroid/hardware/fingerprint/MiFxTunnelHidl; */
/* monitor-enter v0 */
/* .line 42 */
try { // :try_start_0
final String v1 = "FingerprintServiceHidl"; // const-string v1, "FingerprintServiceHidl"
final String v2 = "getInstance class"; // const-string v2, "getInstance class"
android.util.Slog .d ( v1,v2 );
/* .line 43 */
v1 = android.hardware.fingerprint.MiFxTunnelHidl.sInstance;
/* if-nez v1, :cond_0 */
/* .line 44 */
final String v1 = "FingerprintServiceHidl"; // const-string v1, "FingerprintServiceHidl"
final String v2 = "getInstance null 2"; // const-string v2, "getInstance null 2"
android.util.Slog .d ( v1,v2 );
/* .line 45 */
/* new-instance v1, Landroid/hardware/fingerprint/MiFxTunnelHidl; */
/* invoke-direct {v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl;-><init>()V */
/* .line 47 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 49 */
} // :cond_1
} // :goto_0
final String v0 = "FingerprintServiceHidl"; // const-string v0, "FingerprintServiceHidl"
final String v1 = "getInstance out"; // const-string v1, "getInstance out"
android.util.Slog .d ( v0,v1 );
/* .line 50 */
v0 = android.hardware.fingerprint.MiFxTunnelHidl.sInstance;
} // .end method
private vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel getMiFxTunnel ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 71 */
final String v0 = "getMiFxTunnel"; // const-string v0, "getMiFxTunnel"
final String v1 = "FingerprintServiceHidl"; // const-string v1, "FingerprintServiceHidl"
android.util.Slog .i ( v1,v0 );
/* .line 72 */
v0 = this.miFxTunnel;
/* if-nez v0, :cond_0 */
/* .line 73 */
final String v0 = "getMiFxTunnel1"; // const-string v0, "getMiFxTunnel1"
android.util.Slog .i ( v1,v0 );
/* .line 74 */
vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel .getService ( );
this.miFxTunnel = v0;
/* .line 75 */
final String v0 = "getMiFxTunnel2"; // const-string v0, "getMiFxTunnel2"
android.util.Slog .i ( v1,v0 );
/* .line 76 */
v0 = this.miFxTunnel;
v1 = this.mDeathRecipient;
/* const-wide/16 v2, 0x0 */
/* .line 78 */
} // :cond_0
v0 = this.miFxTunnel;
} // .end method
/* # virtual methods */
public synchronized android.hardware.fingerprint.HalDataCmdResult getHalData ( Integer p0, Object[] p1 ) {
/* .locals 6 */
/* .param p1, "cmdId" # I */
/* .param p2, "params" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 109 */
try { // :try_start_0
/* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
/* .line 110 */
/* .local v0, "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
/* new-instance v1, Landroid/hardware/fingerprint/HalDataCmdResult; */
/* invoke-direct {v1}, Landroid/hardware/fingerprint/HalDataCmdResult;-><init>()V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 111 */
/* .local v1, "result":Landroid/hardware/fingerprint/HalDataCmdResult; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 113 */
try { // :try_start_1
android.hardware.fingerprint.MiFxTunnelHidl .byteArrayToArrayList ( p2 );
/* new-instance v3, Landroid/hardware/fingerprint/MiFxTunnelHidl$3; */
/* invoke-direct {v3, p0, p1, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;ILandroid/hardware/fingerprint/HalDataCmdResult;)V */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 127 */
/* .line 125 */
} // .end local p0 # "this":Landroid/hardware/fingerprint/MiFxTunnelHidl;
/* :catch_0 */
/* move-exception v2 */
/* .line 126 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
final String v3 = "FingerprintServiceHidl"; // const-string v3, "FingerprintServiceHidl"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "get HalData failed."; // const-string v5, "get HalData failed."
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 129 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
/* monitor-exit p0 */
/* .line 108 */
} // .end local v0 # "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
} // .end local v1 # "result":Landroid/hardware/fingerprint/HalDataCmdResult;
} // .end local p1 # "cmdId":I
} // .end local p2 # "params":[B
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void registerCallback ( android.os.Handler p0 ) {
/* .locals 3 */
/* .param p1, "handler" # Landroid/os/Handler; */
/* .line 134 */
try { // :try_start_0
this.mHandler = p1;
/* .line 135 */
/* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
/* .line 136 */
/* .local v0, "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
v1 = this.mMiFxTunnelCallback;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 139 */
} // .end local v0 # "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
/* .line 137 */
/* :catch_0 */
/* move-exception v0 */
/* .line 138 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "FingerprintServiceHidl"; // const-string v1, "FingerprintServiceHidl"
final String v2 = "registerCallback err"; // const-string v2, "registerCallback err"
android.util.Slog .d ( v1,v2 );
/* .line 140 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public synchronized android.hardware.fingerprint.HeartRateCmdResult sendCommand ( Integer p0, Object[] p1 ) {
/* .locals 4 */
/* .param p1, "cmdId" # I */
/* .param p2, "params" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 82 */
try { // :try_start_0
/* invoke-direct {p0}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->getMiFxTunnel()Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
/* .line 83 */
/* .local v0, "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel; */
/* new-instance v1, Landroid/hardware/fingerprint/HeartRateCmdResult; */
/* invoke-direct {v1}, Landroid/hardware/fingerprint/HeartRateCmdResult;-><init>()V */
/* .line 84 */
/* .local v1, "result":Landroid/hardware/fingerprint/HeartRateCmdResult; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 85 */
android.hardware.fingerprint.MiFxTunnelHidl .byteArrayToArrayList ( p2 );
/* new-instance v3, Landroid/hardware/fingerprint/MiFxTunnelHidl$2; */
/* invoke-direct {v3, p0, p1, v1}, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;-><init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;ILandroid/hardware/fingerprint/HeartRateCmdResult;)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 105 */
} // .end local p0 # "this":Landroid/hardware/fingerprint/MiFxTunnelHidl;
} // :cond_0
/* monitor-exit p0 */
/* .line 81 */
} // .end local v0 # "miFxTunnel":Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel;
} // .end local v1 # "result":Landroid/hardware/fingerprint/HeartRateCmdResult;
} // .end local p1 # "cmdId":I
} // .end local p2 # "params":[B
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
