class android.hardware.fingerprint.MiFxTunnelHidl$2 implements vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnel$invokeCommandCallback {
	 /* .source "MiFxTunnelHidl.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Landroid/hardware/fingerprint/MiFxTunnelHidl;->sendCommand(I[B)Landroid/hardware/fingerprint/HeartRateCmdResult; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final android.hardware.fingerprint.MiFxTunnelHidl this$0; //synthetic
final Integer val$cmdId; //synthetic
final android.hardware.fingerprint.HeartRateCmdResult val$result; //synthetic
/* # direct methods */
 android.hardware.fingerprint.MiFxTunnelHidl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Landroid/hardware/fingerprint/MiFxTunnelHidl; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 85 */
this.this$0 = p1;
/* iput p2, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I */
this.val$result = p3;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onValues ( Integer p0, java.util.ArrayList p1 ) {
/* .locals 6 */
/* .param p1, "resultCode" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 88 */
/* .local p2, "out_buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* if-nez p1, :cond_0 */
/* .line 92 */
com.android.server.biometrics.sensors.fingerprint.FodFingerprintServiceStub .getInstance ( );
int v1 = 0; // const/4 v1, 0x0
/* iget v2, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I */
int v3 = 0; // const/4 v3, 0x0
final String v4 = "com.mi.health"; // const-string v4, "com.mi.health"
int v5 = 0; // const/4 v5, 0x0
/* invoke-interface/range {v0 ..v5}, Lcom/android/server/biometrics/sensors/fingerprint/FodFingerprintServiceStub;->fodCallBack(Landroid/content/Context;IILjava/lang/String;Lcom/android/server/biometrics/sensors/BaseClientMonitor;)I */
/* .line 93 */
v0 = this.this$0;
android.hardware.fingerprint.MiFxTunnelHidl .-$$Nest$fgetmHandler ( v0 );
/* iget v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I */
(( android.os.Handler ) v0 ).obtainMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 94 */
/* .local v0, "msg":Landroid/os/Message; */
/* iget v1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$2;->val$cmdId:I */
/* iput v1, v0, Landroid/os/Message;->arg1:I */
/* .line 95 */
(( android.os.Message ) v0 ).sendToTarget ( ); // invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
/* .line 96 */
v1 = this.val$result;
/* iput p1, v1, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I */
/* .line 97 */
v1 = this.val$result;
android.hardware.fingerprint.MiFxTunnelHidl .arrayListToByteArray ( p2 );
this.mResultData = v2;
/* .line 98 */
} // .end local v0 # "msg":Landroid/os/Message;
/* .line 99 */
} // :cond_0
v0 = this.val$result;
/* iput p1, v0, Landroid/hardware/fingerprint/HeartRateCmdResult;->mResultCode:I */
/* .line 100 */
v0 = this.val$result;
int v1 = 0; // const/4 v1, 0x0
this.mResultData = v1;
/* .line 102 */
} // :goto_0
return;
} // .end method
