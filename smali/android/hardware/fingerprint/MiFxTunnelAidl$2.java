class android.hardware.fingerprint.MiFxTunnelAidl$2 extends vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub {
	 /* .source "MiFxTunnelAidl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Landroid/hardware/fingerprint/MiFxTunnelAidl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final android.hardware.fingerprint.MiFxTunnelAidl this$0; //synthetic
/* # direct methods */
 android.hardware.fingerprint.MiFxTunnelAidl$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Landroid/hardware/fingerprint/MiFxTunnelAidl; */
/* .line 171 */
this.this$0 = p1;
/* invoke-direct {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.String getInterfaceHash ( ) {
/* .locals 1 */
/* .line 190 */
final String v0 = "cac0cec9bbd7ce7545b32873c89cd67844627700"; // const-string v0, "cac0cec9bbd7ce7545b32873c89cd67844627700"
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 1 */
/* .line 185 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public void onDaemonMessage ( Long p0, Integer p1, Integer p2, Object[] p3 ) {
/* .locals 2 */
/* .param p1, "devId" # J */
/* .param p3, "msgId" # I */
/* .param p4, "cmdId" # I */
/* .param p5, "data" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 174 */
/* const-wide/16 v0, -0x1 */
/* cmp-long v0, p1, v0 */
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p3, v0, :cond_0 */
/* if-ne p4, v0, :cond_0 */
/* .line 175 */
final String v0 = "FingerprintServiceAidl"; // const-string v0, "FingerprintServiceAidl"
final String v1 = "onDaemonMessage: callback has been replaced!"; // const-string v1, "onDaemonMessage: callback has been replaced!"
android.util.Slog .d ( v0,v1 );
/* .line 177 */
} // :cond_0
com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl .heartRateDataCallback ( p3,p4,p5 );
/* .line 179 */
} // :goto_0
return;
} // .end method
