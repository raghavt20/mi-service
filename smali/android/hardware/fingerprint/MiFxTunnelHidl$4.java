class android.hardware.fingerprint.MiFxTunnelHidl$4 extends vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnelCallback$Stub {
	 /* .source "MiFxTunnelHidl.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Landroid/hardware/fingerprint/MiFxTunnelHidl; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final android.hardware.fingerprint.MiFxTunnelHidl this$0; //synthetic
/* # direct methods */
 android.hardware.fingerprint.MiFxTunnelHidl$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Landroid/hardware/fingerprint/MiFxTunnelHidl; */
/* .line 142 */
this.this$0 = p1;
/* invoke-direct {p0}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onMessage ( Long p0, Integer p1, Integer p2, java.util.ArrayList p3 ) {
/* .locals 2 */
/* .param p1, "devId" # J */
/* .param p3, "msgId" # I */
/* .param p4, "cmdId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(JII", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 145 */
/* .local p5, "msg_data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* const-wide/16 v0, -0x1 */
/* cmp-long v0, p1, v0 */
/* if-nez v0, :cond_0 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p3, v0, :cond_0 */
/* if-ne p4, v0, :cond_0 */
/* .line 146 */
final String v0 = "FingerprintServiceHidl"; // const-string v0, "FingerprintServiceHidl"
final String v1 = "onMessage: callback has been replaced!"; // const-string v1, "onMessage: callback has been replaced!"
android.util.Slog .d ( v0,v1 );
/* .line 148 */
} // :cond_0
android.hardware.fingerprint.MiFxTunnelHidl .arrayListToByteArray ( p5 );
com.android.server.biometrics.sensors.fingerprint.HeartRateFingerprintServiceStubImpl .heartRateDataCallback ( p3,p4,v0 );
/* .line 150 */
} // :goto_0
return;
} // .end method
