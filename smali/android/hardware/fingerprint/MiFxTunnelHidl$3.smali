.class Landroid/hardware/fingerprint/MiFxTunnelHidl$3;
.super Ljava/lang/Object;
.source "MiFxTunnelHidl.java"

# interfaces
.implements Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnel$invokeCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/hardware/fingerprint/MiFxTunnelHidl;->getHalData(I[B)Landroid/hardware/fingerprint/HalDataCmdResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

.field final synthetic val$cmdId:I

.field final synthetic val$result:Landroid/hardware/fingerprint/HalDataCmdResult;


# direct methods
.method constructor <init>(Landroid/hardware/fingerprint/MiFxTunnelHidl;ILandroid/hardware/fingerprint/HalDataCmdResult;)V
    .locals 0
    .param p1, "this$0"    # Landroid/hardware/fingerprint/MiFxTunnelHidl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 113
    iput-object p1, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->this$0:Landroid/hardware/fingerprint/MiFxTunnelHidl;

    iput p2, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$cmdId:I

    iput-object p3, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$result:Landroid/hardware/fingerprint/HalDataCmdResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValues(ILjava/util/ArrayList;)V
    .locals 2
    .param p1, "resultCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Byte;",
            ">;)V"
        }
    .end annotation

    .line 116
    .local p2, "out_buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    iget v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$cmdId:I

    if-ne p1, v0, :cond_0

    .line 117
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$result:Landroid/hardware/fingerprint/HalDataCmdResult;

    iput p1, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I

    .line 118
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$result:Landroid/hardware/fingerprint/HalDataCmdResult;

    invoke-static {p2}, Landroid/hardware/fingerprint/MiFxTunnelHidl;->arrayListToByteArray(Ljava/util/ArrayList;)[B

    move-result-object v1

    iput-object v1, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultData:[B

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$result:Landroid/hardware/fingerprint/HalDataCmdResult;

    iput p1, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultCode:I

    .line 121
    iget-object v0, p0, Landroid/hardware/fingerprint/MiFxTunnelHidl$3;->val$result:Landroid/hardware/fingerprint/HalDataCmdResult;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/hardware/fingerprint/HalDataCmdResult;->mResultData:[B

    .line 123
    :goto_0
    return-void
.end method
