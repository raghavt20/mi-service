public class inal {
	 /* .source "StatusCode.java" */
	 /* # static fields */
	 public static final Integer FAILURE;
	 public static final Integer SUCCESS;
	 /* # direct methods */
	 public inal ( ) {
		 /* .locals 0 */
		 /* .line 4 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static final java.lang.String dumpBitfield ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p0, "o" # I */
		 /* .line 24 */
		 /* new-instance v0, Ljava/util/ArrayList; */
		 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
		 /* .line 25 */
		 /* .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 26 */
		 /* .local v1, "flipped":I */
		 /* and-int/lit8 v2, p0, -0x1 */
		 int v3 = -1; // const/4 v3, -0x1
		 /* if-ne v2, v3, :cond_0 */
		 /* .line 27 */
		 final String v2 = "FAILURE"; // const-string v2, "FAILURE"
		 (( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
		 /* .line 28 */
		 /* or-int/lit8 v1, v1, -0x1 */
		 /* .line 30 */
	 } // :cond_0
	 final String v2 = "SUCCESS"; // const-string v2, "SUCCESS"
	 (( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
	 /* .line 31 */
	 /* if-eq p0, v1, :cond_1 */
	 /* .line 32 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "0x"; // const-string v3, "0x"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* not-int v3, v1 */
	 /* and-int/2addr v3, p0 */
	 java.lang.Integer .toHexString ( v3 );
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
	 /* .line 34 */
} // :cond_1
final String v2 = " | "; // const-string v2, " | "
java.lang.String .join ( v2,v0 );
} // .end method
public static final java.lang.String toString ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "o" # I */
/* .line 14 */
int v0 = -1; // const/4 v0, -0x1
/* if-ne p0, v0, :cond_0 */
/* .line 15 */
final String v0 = "FAILURE"; // const-string v0, "FAILURE"
/* .line 17 */
} // :cond_0
/* if-nez p0, :cond_1 */
/* .line 18 */
final String v0 = "SUCCESS"; // const-string v0, "SUCCESS"
/* .line 20 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "0x"; // const-string v1, "0x"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toHexString ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
