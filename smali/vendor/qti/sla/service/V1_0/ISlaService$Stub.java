public abstract class vendor.qti.sla.service.V1_0.ISlaService$Stub extends android.os.HwBinder implements vendor.qti.sla.service.V1_0.ISlaService {
	 /* .source "ISlaService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/qti/sla/service/V1_0/ISlaService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public vendor.qti.sla.service.V1_0.ISlaService$Stub ( ) {
/* .locals 0 */
/* .line 669 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 672 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 685 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 722 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 723 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 724 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 725 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 726 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 697 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* 0x3ct */
/* -0x23t */
/* 0x39t */
/* -0x4bt */
/* -0x28t */
/* -0x52t */
/* -0x43t */
/* 0x7ct */
/* 0x2ct */
/* -0x3ft */
/* -0x47t */
/* -0x75t */
/* 0x1ft */
/* -0x27t */
/* 0x16t */
/* -0x77t */
/* 0x7t */
/* 0x11t */
/* -0xdt */
/* -0x13t */
/* -0x14t */
/* 0x52t */
/* -0x1bt */
/* 0x9t */
/* 0x4bt */
/* 0x70t */
/* -0x3dt */
/* -0x38t */
/* 0x14t */
/* -0x46t */
/* 0x3dt */
/* -0x42t */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 677 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.qti.sla.service@1.0::ISlaService" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 691 */
/* const-string/jumbo v0, "vendor.qti.sla.service@1.0::ISlaService" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 710 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 732 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 734 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 762 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
int v1 = 0; // const/4 v1, 0x0
/* const-string/jumbo v2, "vendor.qti.sla.service@1.0::ISlaService" */
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 992 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 994 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->notifySyspropsChanged()V
/* .line 995 */
/* goto/16 :goto_1 */
/* .line 981 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 983 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 984 */
/* .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 985 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 986 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 987 */
/* goto/16 :goto_1 */
/* .line 971 */
} // .end local v0 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 973 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->ping()V
/* .line 974 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 975 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 976 */
/* goto/16 :goto_1 */
/* .line 966 */
/* :sswitch_3 */
/* goto/16 :goto_1 */
/* .line 958 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 960 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setHALInstrumentation()V
/* .line 961 */
/* goto/16 :goto_1 */
/* .line 924 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 926 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 927 */
/* .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 929 */
/* new-instance v2, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v2, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 931 */
/* .local v2, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 932 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v2 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v2, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 933 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v2 ).putBool ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 934 */
/* new-instance v1, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v1, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 935 */
/* .local v1, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 937 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 938 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 940 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 944 */
(( android.os.HwBlob ) v1 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v1, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 945 */
/* nop */
/* .line 935 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 941 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 948 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v2 ).putBlob ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 950 */
} // .end local v1 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 952 */
} // .end local v2 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 953 */
/* goto/16 :goto_1 */
/* .line 913 */
} // .end local v0 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 915 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 916 */
/* .local v0, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 917 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 918 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 919 */
/* goto/16 :goto_1 */
/* .line 901 */
} // .end local v0 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 903 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 904 */
/* .local v0, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 905 */
/* .local v2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).debug ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 906 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 907 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 908 */
/* goto/16 :goto_1 */
/* .line 890 */
} // .end local v0 # "fd":Landroid/os/NativeHandle;
} // .end local v2 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 892 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 893 */
/* .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 894 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 895 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 896 */
/* goto/16 :goto_1 */
/* .line 881 */
} // .end local v0 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 883 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 884 */
/* .local v0, "uid":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setDDUidList ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setDDUidList(Ljava/lang/String;)V
/* .line 885 */
/* goto/16 :goto_1 */
/* .line 870 */
} // .end local v0 # "uid":Ljava/lang/String;
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 872 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 873 */
/* .local v0, "address":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setWifiGateway ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setWifiGateway(Ljava/lang/String;)V
/* .line 874 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 875 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 876 */
/* goto/16 :goto_1 */
/* .line 859 */
} // .end local v0 # "address":Ljava/lang/String;
/* :sswitch_b */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 861 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 862 */
/* .local v0, "enable":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setDWMonitorEnable ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setDWMonitorEnable(Ljava/lang/String;)V
/* .line 863 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 864 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 865 */
/* goto/16 :goto_1 */
/* .line 850 */
} // .end local v0 # "enable":Ljava/lang/String;
/* :sswitch_c */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 852 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 853 */
/* .local v0, "uid":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setDWUidList ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setDWUidList(Ljava/lang/String;)V
/* .line 854 */
/* goto/16 :goto_1 */
/* .line 841 */
} // .end local v0 # "uid":Ljava/lang/String;
/* :sswitch_d */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 843 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 844 */
/* .restart local v0 # "uid":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setSLSVoIPStart ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setSLSVoIPStart(Ljava/lang/String;)V
/* .line 845 */
/* .line 832 */
} // .end local v0 # "uid":Ljava/lang/String;
/* :sswitch_e */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 834 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 835 */
/* .restart local v0 # "uid":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setSLSGameStop ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setSLSGameStop(Ljava/lang/String;)V
/* .line 836 */
/* .line 823 */
} // .end local v0 # "uid":Ljava/lang/String;
/* :sswitch_f */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 825 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 826 */
/* .restart local v0 # "uid":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setSLSGameStart ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setSLSGameStart(Ljava/lang/String;)V
/* .line 827 */
/* .line 814 */
} // .end local v0 # "uid":Ljava/lang/String;
/* :sswitch_10 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 816 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 817 */
/* .local v0, "bssid":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setSLSBSSID ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setSLSBSSID(Ljava/lang/String;)V
/* .line 818 */
/* .line 805 */
} // .end local v0 # "bssid":Ljava/lang/String;
/* :sswitch_11 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 807 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 808 */
/* .local v0, "mode":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setSLAMode ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setSLAMode(Ljava/lang/String;)V
/* .line 809 */
/* .line 796 */
} // .end local v0 # "mode":Ljava/lang/String;
/* :sswitch_12 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 798 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 799 */
/* .local v0, "uidList":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setSLAUidList ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setSLAUidList(Ljava/lang/String;)V
/* .line 800 */
/* .line 785 */
} // .end local v0 # "uidList":Ljava/lang/String;
/* :sswitch_13 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 787 */
(( android.os.HwParcel ) p2 ).readString ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* .line 788 */
/* .local v0, "ifaces":Ljava/lang/String; */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).setInterface ( v0 ); // invoke-virtual {p0, v0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->setInterface(Ljava/lang/String;)V
/* .line 789 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 790 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 791 */
/* .line 775 */
} // .end local v0 # "ifaces":Ljava/lang/String;
/* :sswitch_14 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 777 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).stopSlad ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->stopSlad()V
/* .line 778 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 779 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 780 */
/* .line 765 */
/* :sswitch_15 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v2 ); // invoke-virtual {p2, v2}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 767 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).startSlad ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->startSlad()V
/* .line 768 */
(( android.os.HwParcel ) p3 ).writeStatus ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 769 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 770 */
/* nop */
/* .line 1004 */
} // :goto_1
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_15 */
/* 0x2 -> :sswitch_14 */
/* 0x3 -> :sswitch_13 */
/* 0x4 -> :sswitch_12 */
/* 0x5 -> :sswitch_11 */
/* 0x6 -> :sswitch_10 */
/* 0x7 -> :sswitch_f */
/* 0x8 -> :sswitch_e */
/* 0x9 -> :sswitch_d */
/* 0xa -> :sswitch_c */
/* 0xb -> :sswitch_b */
/* 0xc -> :sswitch_a */
/* 0xd -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 716 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 744 */
/* const-string/jumbo v0, "vendor.qti.sla.service@1.0::ISlaService" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 745 */
/* .line 747 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 751 */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->registerService(Ljava/lang/String;)V
/* .line 752 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 706 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 756 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( vendor.qti.sla.service.V1_0.ISlaService$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/qti/sla/service/V1_0/ISlaService$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 738 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
