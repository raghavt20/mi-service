public class inal implements vendor.xiaomi.hardware.keyboardnanoapp.V1_0.INanoappCallback {
	 /* .source "INanoappCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x19 */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IHwBinder mRemote;
/* # direct methods */
public inal ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IHwBinder; */
/* .line 205 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 206 */
java.util.Objects .requireNonNull ( p1 );
/* check-cast v0, Landroid/os/IHwBinder; */
this.mRemote = v0;
/* .line 207 */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 1 */
/* .line 211 */
v0 = this.mRemote;
} // .end method
public void dataReceive ( java.util.ArrayList p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 238 */
/* .local p1, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 239 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.keyboardnanoapp@1.0::INanoappCallback" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 240 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 242 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 244 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
/* .line 245 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 247 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 248 */
/* nop */
/* .line 249 */
return;
/* .line 247 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 248 */
/* throw v2 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 290 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 291 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 292 */
(( android.os.HwParcel ) v0 ).writeNativeHandle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeNativeHandle(Landroid/os/NativeHandle;)V
/* .line 293 */
(( android.os.HwParcel ) v0 ).writeStringVector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 295 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 297 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf444247 */
int v4 = 0; // const/4 v4, 0x0
/* .line 298 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 299 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 301 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 302 */
/* nop */
/* .line 303 */
return;
/* .line 301 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 302 */
/* throw v2 */
} // .end method
public final Boolean equals ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "other" # Ljava/lang/Object; */
/* .line 226 */
v0 = android.os.HidlSupport .interfacesEqual ( p0,p1 );
} // .end method
public void errorReceive ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "errorCode" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 254 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 255 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.keyboardnanoapp@1.0::INanoappCallback" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 256 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 258 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 260 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
int v4 = 1; // const/4 v4, 0x1
/* .line 261 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 263 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 264 */
/* nop */
/* .line 265 */
return;
/* .line 263 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 264 */
/* throw v2 */
} // .end method
public miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 402 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 403 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 405 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 407 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf524546 */
int v4 = 0; // const/4 v4, 0x0
/* .line 408 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 409 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 411 */
/* new-instance v2, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v2}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 412 */
/* .local v2, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v2 ).readFromParcel ( v1 ); // invoke-virtual {v2, v1}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->readFromParcel(Landroid/os/HwParcel;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 413 */
/* nop */
/* .line 415 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 413 */
/* .line 415 */
} // .end local v2 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 416 */
/* throw v2 */
} // .end method
public java.util.ArrayList getHashChain ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 327 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 328 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 330 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 332 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf485348 */
int v4 = 0; // const/4 v4, 0x0
/* .line 333 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 334 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 336 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v10, v2 */
/* .line 338 */
/* .local v10, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
/* const-wide/16 v2, 0x10 */
(( android.os.HwParcel ) v1 ).readBuffer ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/HwParcel;->readBuffer(J)Landroid/os/HwBlob;
/* move-object v11, v2 */
/* .line 340 */
/* .local v11, "_hidl_blob":Landroid/os/HwBlob; */
/* const-wide/16 v2, 0x8 */
v2 = (( android.os.HwBlob ) v11 ).getInt32 ( v2, v3 ); // invoke-virtual {v11, v2, v3}, Landroid/os/HwBlob;->getInt32(J)I
/* move v12, v2 */
/* .line 341 */
/* .local v12, "_hidl_vec_size":I */
/* mul-int/lit8 v2, v12, 0x20 */
/* int-to-long v3, v2 */
/* .line 342 */
(( android.os.HwBlob ) v11 ).handle ( ); // invoke-virtual {v11}, Landroid/os/HwBlob;->handle()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
int v9 = 1; // const/4 v9, 0x1
/* .line 341 */
/* move-object v2, v1 */
/* invoke-virtual/range {v2 ..v9}, Landroid/os/HwParcel;->readEmbeddedBuffer(JJJZ)Landroid/os/HwBlob; */
/* .line 345 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
(( java.util.ArrayList ) v10 ).clear ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V
/* .line 346 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "_hidl_index_0":I */
} // :goto_0
/* if-ge v3, v12, :cond_0 */
/* .line 347 */
/* const/16 v4, 0x20 */
/* new-array v5, v4, [B */
/* .line 349 */
/* .local v5, "_hidl_vec_element":[B */
/* mul-int/lit8 v6, v3, 0x20 */
/* int-to-long v6, v6 */
/* .line 350 */
/* .local v6, "_hidl_array_offset_1":J */
(( android.os.HwBlob ) v2 ).copyToInt8Array ( v6, v7, v5, v4 ); // invoke-virtual {v2, v6, v7, v5, v4}, Landroid/os/HwBlob;->copyToInt8Array(J[BI)V
/* .line 351 */
/* nop */
/* .line 353 */
} // .end local v6 # "_hidl_array_offset_1":J
(( java.util.ArrayList ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 346 */
/* nop */
} // .end local v5 # "_hidl_vec_element":[B
/* add-int/lit8 v3, v3, 0x1 */
/* .line 357 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_index_0":I
} // .end local v11 # "_hidl_blob":Landroid/os/HwBlob;
} // .end local v12 # "_hidl_vec_size":I
} // :cond_0
/* nop */
/* .line 359 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 357 */
/* .line 359 */
} // .end local v10 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 360 */
/* throw v2 */
} // .end method
public final Integer hashCode ( ) {
/* .locals 1 */
/* .line 231 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.INanoappCallback$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback$Proxy;->asBinder()Landroid/os/IHwBinder;
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
} // .end method
public java.util.ArrayList interfaceChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 271 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 272 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 274 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 276 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf43484e */
int v4 = 0; // const/4 v4, 0x0
/* .line 277 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 278 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 280 */
(( android.os.HwParcel ) v1 ).readStringVector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 281 */
/* .local v2, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* nop */
/* .line 283 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 281 */
/* .line 283 */
} // .end local v2 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 284 */
/* throw v2 */
} // .end method
public java.lang.String interfaceDescriptor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 308 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 309 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 311 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 313 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf445343 */
int v4 = 0; // const/4 v4, 0x0
/* .line 314 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 315 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 317 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 318 */
/* .local v2, "_hidl_out_descriptor":Ljava/lang/String; */
/* nop */
/* .line 320 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 318 */
/* .line 320 */
} // .end local v2 # "_hidl_out_descriptor":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 321 */
/* throw v2 */
} // .end method
public Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 381 */
v0 = v0 = this.mRemote;
} // .end method
public void notifySyspropsChanged ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 422 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 423 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 425 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 427 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf535953 */
int v4 = 1; // const/4 v4, 0x1
/* .line 428 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 430 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 431 */
/* nop */
/* .line 432 */
return;
/* .line 430 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 431 */
/* throw v2 */
} // .end method
public void ping ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 386 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 387 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 389 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 391 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf504e47 */
int v4 = 0; // const/4 v4, 0x0
/* .line 392 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 393 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 395 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 396 */
/* nop */
/* .line 397 */
return;
/* .line 395 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 396 */
/* throw v2 */
} // .end method
public void setHALInstrumentation ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 366 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 367 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 369 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 371 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf494e54 */
int v4 = 1; // const/4 v4, 0x1
/* .line 372 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 374 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 375 */
/* nop */
/* .line 376 */
return;
/* .line 374 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 375 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 217 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.INanoappCallback$Proxy ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback$Proxy;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Proxy"; // const-string v1, "@Proxy"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 218 */
/* :catch_0 */
/* move-exception v0 */
/* .line 221 */
final String v0 = "[class or subclass of vendor.xiaomi.hardware.keyboardnanoapp@1.0::INanoappCallback]@Proxy"; // const-string v0, "[class or subclass of vendor.xiaomi.hardware.keyboardnanoapp@1.0::INanoappCallback]@Proxy"
} // .end method
public Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 437 */
v0 = v0 = this.mRemote;
} // .end method
