public abstract class vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub extends android.os.HwBinder implements vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp {
	 /* .source "IKeyboardNanoapp.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ( ) {
/* .locals 0 */
/* .line 446 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 449 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 462 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 499 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 500 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 501 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 502 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 503 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 474 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* 0x0t */
/* 0x1ft */
/* -0x2t */
/* -0x9t */
/* -0x7ft */
/* 0x72t */
/* 0x18t */
/* -0x79t */
/* -0x78t */
/* -0x17t */
/* 0x6dt */
/* 0x26t */
/* -0x78t */
/* -0x34t */
/* 0x75t */
/* 0x14t */
/* -0x31t */
/* 0x6at */
/* 0x3bt */
/* -0x18t */
/* 0x15t */
/* 0x6bt */
/* -0x61t */
/* -0x18t */
/* 0x14t */
/* -0x27t */
/* 0x39t */
/* -0x2ft */
/* -0x29t */
/* 0x45t */
/* 0x73t */
/* 0x4ct */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 454 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.keyboardnanoapp@1.0::IKeyboardNanoapp" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 468 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.keyboardnanoapp@1.0::IKeyboardNanoapp" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 487 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 509 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 511 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 10 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 539 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.keyboardnanoapp@1.0::IKeyboardNanoapp" */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 667 */
/* :sswitch_0 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 669 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->notifySyspropsChanged()V
/* .line 670 */
/* goto/16 :goto_1 */
/* .line 656 */
/* :sswitch_1 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 658 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 659 */
/* .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 660 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( p3 ); // invoke-virtual {v0, p3}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 661 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 662 */
/* goto/16 :goto_1 */
/* .line 646 */
} // .end local v0 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 648 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->ping()V
/* .line 649 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 650 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 651 */
/* goto/16 :goto_1 */
/* .line 641 */
/* :sswitch_3 */
/* goto/16 :goto_1 */
/* .line 633 */
/* :sswitch_4 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 635 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->setHALInstrumentation()V
/* .line 636 */
/* goto/16 :goto_1 */
/* .line 599 */
/* :sswitch_5 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 601 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 602 */
/* .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 604 */
/* new-instance v1, Landroid/os/HwBlob; */
/* const/16 v3, 0x10 */
/* invoke-direct {v1, v3}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 606 */
/* .local v1, "_hidl_blob":Landroid/os/HwBlob; */
v3 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 607 */
/* .local v3, "_hidl_vec_size":I */
/* const-wide/16 v4, 0x8 */
(( android.os.HwBlob ) v1 ).putInt32 ( v4, v5, v3 ); // invoke-virtual {v1, v4, v5, v3}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 608 */
/* const-wide/16 v4, 0xc */
(( android.os.HwBlob ) v1 ).putBool ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 609 */
/* new-instance v2, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v3, 0x20 */
/* invoke-direct {v2, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 610 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* .line 612 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v5, v5 */
/* .line 613 */
/* .local v5, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v7, [B */
/* .line 615 */
/* .local v7, "_hidl_array_item_1":[B */
if ( v7 != null) { // if-eqz v7, :cond_0
/* array-length v8, v7 */
/* const/16 v9, 0x20 */
/* if-ne v8, v9, :cond_0 */
/* .line 619 */
(( android.os.HwBlob ) v2 ).putInt8Array ( v5, v6, v7 ); // invoke-virtual {v2, v5, v6, v7}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 620 */
/* nop */
/* .line 610 */
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
/* add-int/lit8 v4, v4, 0x1 */
/* .line 616 */
/* .restart local v5 # "_hidl_array_offset_1":J */
/* .restart local v7 # "_hidl_array_item_1":[B */
} // :cond_0
/* new-instance v8, Ljava/lang/IllegalArgumentException; */
final String v9 = "Array element is not of the expected length"; // const-string v9, "Array element is not of the expected length"
/* invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v8 */
/* .line 623 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_offset_1":J
} // .end local v7 # "_hidl_array_item_1":[B
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v1 ).putBlob ( v4, v5, v2 ); // invoke-virtual {v1, v4, v5, v2}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 625 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_vec_size":I
(( android.os.HwParcel ) p3 ).writeBuffer ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 627 */
} // .end local v1 # "_hidl_blob":Landroid/os/HwBlob;
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 628 */
/* .line 588 */
} // .end local v0 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 590 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 591 */
/* .local v0, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 592 */
(( android.os.HwParcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 593 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 594 */
/* .line 576 */
} // .end local v0 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 578 */
(( android.os.HwParcel ) p2 ).readNativeHandle ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;
/* .line 579 */
/* .local v0, "fd":Landroid/os/NativeHandle; */
(( android.os.HwParcel ) p2 ).readStringVector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* .line 580 */
/* .local v1, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).debug ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 581 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 582 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 583 */
/* .line 565 */
} // .end local v0 # "fd":Landroid/os/NativeHandle;
} // .end local v1 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 567 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 568 */
/* .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 569 */
(( android.os.HwParcel ) p3 ).writeStringVector ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 570 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 571 */
/* .line 554 */
} // .end local v0 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 556 */
(( android.os.HwParcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;
vendor.xiaomi.hardware.keyboardnanoapp.V1_0.INanoappCallback .asInterface ( v0 );
/* .line 557 */
/* .local v0, "mNanoappCallback":Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback; */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).setCallback ( v0 ); // invoke-virtual {p0, v0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->setCallback(Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback;)V
/* .line 558 */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 559 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 560 */
/* .line 542 */
} // .end local v0 # "mNanoappCallback":Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/INanoappCallback;
/* :sswitch_a */
(( android.os.HwParcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 544 */
(( android.os.HwParcel ) p2 ).readInt8Vector ( ); // invoke-virtual {p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;
/* .line 545 */
/* .local v0, "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = (( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).sendCmd ( v0 ); // invoke-virtual {p0, v0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->sendCmd(Ljava/util/ArrayList;)I
/* .line 546 */
/* .local v1, "_hidl_out_ret":I */
(( android.os.HwParcel ) p3 ).writeStatus ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 547 */
(( android.os.HwParcel ) p3 ).writeInt32 ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 548 */
(( android.os.HwParcel ) p3 ).send ( ); // invoke-virtual {p3}, Landroid/os/HwParcel;->send()V
/* .line 549 */
/* nop */
/* .line 679 */
} // .end local v0 # "buf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "_hidl_out_ret":I
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_a */
/* 0x2 -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 493 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 521 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.keyboardnanoapp@1.0::IKeyboardNanoapp" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 522 */
/* .line 524 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 528 */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->registerService(Ljava/lang/String;)V
/* .line 529 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 483 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 533 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( vendor.xiaomi.hardware.keyboardnanoapp.V1_0.IKeyboardNanoapp$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/keyboardnanoapp/V1_0/IKeyboardNanoapp$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 515 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
