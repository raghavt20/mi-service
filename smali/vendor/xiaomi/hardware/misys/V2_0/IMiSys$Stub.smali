.class public abstract Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;
.super Landroid/os/HwBinder;
.source "IMiSys.java"

# interfaces
.implements Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvendor/xiaomi/hardware/misys/V2_0/IMiSys;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 676
    invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IHwBinder;
    .locals 0

    .line 679
    return-object p0
.end method

.method public debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "fd"    # Landroid/os/NativeHandle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/NativeHandle;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 692
    .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    return-void
.end method

.method public final getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
    .locals 3

    .line 729
    new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;

    invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V

    .line 730
    .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
    invoke-static {}, Landroid/os/HidlSupport;->getPidIfSharable()I

    move-result v1

    iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I

    .line 731
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J

    .line 732
    const/4 v1, 0x0

    iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I

    .line 733
    return-object v0
.end method

.method public final getHashChain()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "[B>;"
        }
    .end annotation

    .line 704
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    new-array v1, v1, [B

    fill-array-data v1, :array_1

    filled-new-array {v2, v1}, [[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :array_0
    .array-data 1
        -0x25t
        0x5at
        -0x31t
        0x44t
        0x63t
        -0x6ft
        0x31t
        0x2at
        0x2ft
        0x1ft
        0x53t
        0x7dt
        0x1t
        0x19t
        0x21t
        0x6ft
        0x9t
        0x3ct
        0x2et
        -0x19t
        0x7dt
        -0x4ft
        0x47t
        0x3t
        -0x70t
        0xct
        0x2ft
        -0x2at
        0x75t
        0x2at
        -0x31t
        -0x2ft
    .end array-data

    :array_1
    .array-data 1
        -0x14t
        0x7ft
        -0x29t
        -0x62t
        -0x30t
        0x2dt
        -0x6t
        -0x7bt
        -0x44t
        0x49t
        -0x6ct
        0x26t
        -0x53t
        -0x52t
        0x3et
        -0x42t
        0x23t
        -0x11t
        0x5t
        0x24t
        -0xdt
        -0x33t
        0x69t
        0x57t
        0x13t
        -0x6dt
        0x24t
        -0x48t
        0x3bt
        0x18t
        -0x36t
        0x4ct
    .end array-data
.end method

.method public final interfaceChain()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 684
    new-instance v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "vendor.xiaomi.hardware.misys@2.0::IMiSys"

    const-string v2, "android.hidl.base@1.0::IBase"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final interfaceDescriptor()Ljava/lang/String;
    .locals 1

    .line 698
    const-string/jumbo v0, "vendor.xiaomi.hardware.misys@2.0::IMiSys"

    return-object v0
.end method

.method public final linkToDeath(Landroid/os/IHwBinder$DeathRecipient;J)Z
    .locals 1
    .param p1, "recipient"    # Landroid/os/IHwBinder$DeathRecipient;
    .param p2, "cookie"    # J

    .line 717
    const/4 v0, 0x1

    return v0
.end method

.method public final notifySyspropsChanged()V
    .locals 0

    .line 739
    invoke-static {}, Landroid/os/HwBinder;->enableInstrumentation()V

    .line 741
    return-void
.end method

.method public onTransact(ILandroid/os/HwParcel;Landroid/os/HwParcel;I)V
    .locals 15
    .param p1, "_hidl_code"    # I
    .param p2, "_hidl_request"    # Landroid/os/HwParcel;
    .param p3, "_hidl_reply"    # Landroid/os/HwParcel;
    .param p4, "_hidl_flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 769
    move-object v6, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    const-string v0, "android.hidl.base@1.0::IBase"

    const-string/jumbo v1, "vendor.xiaomi.hardware.misys@2.0::IMiSys"

    const/4 v9, 0x0

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_1

    .line 998
    :sswitch_0
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->notifySyspropsChanged()V

    .line 1001
    goto/16 :goto_1

    .line 987
    :sswitch_1
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 989
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;

    move-result-object v0

    .line 990
    .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 991
    invoke-virtual {v0, v8}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V

    .line 992
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 993
    goto/16 :goto_1

    .line 977
    .end local v0    # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
    :sswitch_2
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 979
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->ping()V

    .line 980
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 981
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 982
    goto/16 :goto_1

    .line 972
    :sswitch_3
    goto/16 :goto_1

    .line 964
    :sswitch_4
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 966
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->setHALInstrumentation()V

    .line 967
    goto/16 :goto_1

    .line 930
    :sswitch_5
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 932
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->getHashChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 933
    .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 935
    new-instance v1, Landroid/os/HwBlob;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Landroid/os/HwBlob;-><init>(I)V

    .line 937
    .local v1, "_hidl_blob":Landroid/os/HwBlob;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 938
    .local v2, "_hidl_vec_size":I
    const-wide/16 v3, 0x8

    invoke-virtual {v1, v3, v4, v2}, Landroid/os/HwBlob;->putInt32(JI)V

    .line 939
    const-wide/16 v3, 0xc

    invoke-virtual {v1, v3, v4, v9}, Landroid/os/HwBlob;->putBool(JZ)V

    .line 940
    new-instance v3, Landroid/os/HwBlob;

    mul-int/lit8 v4, v2, 0x20

    invoke-direct {v3, v4}, Landroid/os/HwBlob;-><init>(I)V

    .line 941
    .local v3, "childBlob":Landroid/os/HwBlob;
    const/4 v4, 0x0

    .local v4, "_hidl_index_0":I
    :goto_0
    if-ge v4, v2, :cond_1

    .line 943
    mul-int/lit8 v5, v4, 0x20

    int-to-long v9, v5

    .line 944
    .local v9, "_hidl_array_offset_1":J
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    .line 946
    .local v5, "_hidl_array_item_1":[B
    if-eqz v5, :cond_0

    array-length v11, v5

    const/16 v12, 0x20

    if-ne v11, v12, :cond_0

    .line 950
    invoke-virtual {v3, v9, v10, v5}, Landroid/os/HwBlob;->putInt8Array(J[B)V

    .line 951
    nop

    .line 941
    .end local v5    # "_hidl_array_item_1":[B
    .end local v9    # "_hidl_array_offset_1":J
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 947
    .restart local v5    # "_hidl_array_item_1":[B
    .restart local v9    # "_hidl_array_offset_1":J
    :cond_0
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "Array element is not of the expected length"

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 954
    .end local v4    # "_hidl_index_0":I
    .end local v5    # "_hidl_array_item_1":[B
    .end local v9    # "_hidl_array_offset_1":J
    :cond_1
    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5, v3}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V

    .line 956
    .end local v2    # "_hidl_vec_size":I
    .end local v3    # "childBlob":Landroid/os/HwBlob;
    invoke-virtual {v8, v1}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V

    .line 958
    .end local v1    # "_hidl_blob":Landroid/os/HwBlob;
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 959
    goto/16 :goto_1

    .line 919
    .end local v0    # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :sswitch_6
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 921
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    .line 922
    .local v0, "_hidl_out_descriptor":Ljava/lang/String;
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 923
    invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V

    .line 924
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 925
    goto/16 :goto_1

    .line 907
    .end local v0    # "_hidl_out_descriptor":Ljava/lang/String;
    :sswitch_7
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 909
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle;

    move-result-object v0

    .line 910
    .local v0, "fd":Landroid/os/NativeHandle;
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;

    move-result-object v1

    .line 911
    .local v1, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V

    .line 912
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 913
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 914
    goto/16 :goto_1

    .line 896
    .end local v0    # "fd":Landroid/os/NativeHandle;
    .end local v1    # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_8
    invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 898
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->interfaceChain()Ljava/util/ArrayList;

    move-result-object v0

    .line 899
    .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 900
    invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V

    .line 901
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 902
    goto/16 :goto_1

    .line 886
    .end local v0    # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_9
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 888
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->UnregisterVCameraCallback()V

    .line 889
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 890
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 891
    goto/16 :goto_1

    .line 875
    :sswitch_a
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 877
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder;

    move-result-object v0

    invoke-static {v0}, Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;->asInterface(Landroid/os/IHwBinder;)Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;

    move-result-object v0

    .line 878
    .local v0, "callback":Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;
    invoke-virtual {p0, v0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->RegisterVCameraCallback(Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;)V

    .line 879
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 880
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 881
    goto/16 :goto_1

    .line 862
    .end local v0    # "callback":Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;
    :sswitch_b
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 864
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v0

    .line 865
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v1

    .line 866
    .local v1, "len":I
    invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->OnFrameData(Ljava/util/ArrayList;I)Z

    move-result v2

    .line 867
    .local v2, "_hidl_out_result":Z
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 868
    invoke-virtual {v8, v2}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 869
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 870
    goto/16 :goto_1

    .line 851
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .end local v1    # "len":I
    .end local v2    # "_hidl_out_result":Z
    :sswitch_c
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 853
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->DisconnectVirtualCamera()Z

    move-result v0

    .line 854
    .local v0, "_hidl_out_result":Z
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 855
    invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 856
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 857
    goto/16 :goto_1

    .line 840
    .end local v0    # "_hidl_out_result":Z
    :sswitch_d
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 842
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->ConnectVirtualCamera()Z

    move-result v0

    .line 843
    .restart local v0    # "_hidl_out_result":Z
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 844
    invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 845
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 846
    goto/16 :goto_1

    .line 826
    .end local v0    # "_hidl_out_result":Z
    :sswitch_e
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 828
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v0

    .line 829
    .local v0, "width":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readInt32()I

    move-result v1

    .line 830
    .local v1, "height":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readDouble()D

    move-result-wide v2

    .line 831
    .local v2, "frameRate":D
    invoke-virtual {p0, v0, v1, v2, v3}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->SetVirtualCameraConfig(IID)Z

    move-result v4

    .line 832
    .local v4, "_hidl_out_result":Z
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 833
    invoke-virtual {v8, v4}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 834
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 835
    goto/16 :goto_1

    .line 813
    .end local v0    # "width":I
    .end local v1    # "height":I
    .end local v2    # "frameRate":D
    .end local v4    # "_hidl_out_result":Z
    :sswitch_f
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 815
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 816
    .local v0, "path":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 817
    .local v1, "file_name":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->IsExists(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 818
    .local v2, "_hidl_out_result":Z
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 819
    invoke-virtual {v8, v2}, Landroid/os/HwParcel;->writeBool(Z)V

    .line 820
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 821
    goto :goto_1

    .line 800
    .end local v0    # "path":Ljava/lang/String;
    .end local v1    # "file_name":Ljava/lang/String;
    .end local v2    # "_hidl_out_result":Z
    :sswitch_10
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 802
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 803
    .restart local v0    # "path":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 804
    .restart local v1    # "file_name":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->MiSysReadBuffer(Ljava/lang/String;Ljava/lang/String;)Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;

    move-result-object v2

    .line 805
    .local v2, "_hidl_out_result":Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 806
    invoke-virtual {v2, v8}, Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;->writeToParcel(Landroid/os/HwParcel;)V

    .line 807
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 808
    goto :goto_1

    .line 785
    .end local v0    # "path":Ljava/lang/String;
    .end local v1    # "file_name":Ljava/lang/String;
    .end local v2    # "_hidl_out_result":Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;
    :sswitch_11
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 787
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 788
    .local v10, "path":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 789
    .local v11, "file_name":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList;

    move-result-object v12

    .line 790
    .local v12, "writebuf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readInt64()J

    move-result-wide v13

    .line 791
    .local v13, "buf_len":J
    move-object v0, p0

    move-object v1, v10

    move-object v2, v11

    move-object v3, v12

    move-wide v4, v13

    invoke-virtual/range {v0 .. v5}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->MiSysWriteBuffer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;J)I

    move-result v0

    .line 792
    .local v0, "_hidl_out_result":I
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 793
    invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 794
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 795
    goto :goto_1

    .line 772
    .end local v0    # "_hidl_out_result":I
    .end local v10    # "path":Ljava/lang/String;
    .end local v11    # "file_name":Ljava/lang/String;
    .end local v12    # "writebuf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .end local v13    # "buf_len":J
    :sswitch_12
    invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V

    .line 774
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 775
    .local v0, "path":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 776
    .local v1, "folder_name":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->MiSysCreateFolder(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 777
    .local v2, "_hidl_out_result":I
    invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V

    .line 778
    invoke-virtual {v8, v2}, Landroid/os/HwParcel;->writeInt32(I)V

    .line 779
    invoke-virtual/range {p3 .. p3}, Landroid/os/HwParcel;->send()V

    .line 780
    nop

    .line 1010
    .end local v0    # "path":Ljava/lang/String;
    .end local v1    # "folder_name":Ljava/lang/String;
    .end local v2    # "_hidl_out_result":I
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_12
        0x2 -> :sswitch_11
        0x3 -> :sswitch_10
        0x4 -> :sswitch_f
        0x5 -> :sswitch_e
        0x6 -> :sswitch_d
        0x7 -> :sswitch_c
        0x8 -> :sswitch_b
        0x9 -> :sswitch_a
        0xa -> :sswitch_9
        0xf43484e -> :sswitch_8
        0xf444247 -> :sswitch_7
        0xf445343 -> :sswitch_6
        0xf485348 -> :sswitch_5
        0xf494e54 -> :sswitch_4
        0xf4c5444 -> :sswitch_3
        0xf504e47 -> :sswitch_2
        0xf524546 -> :sswitch_1
        0xf535953 -> :sswitch_0
    .end sparse-switch
.end method

.method public final ping()V
    .locals 0

    .line 723
    return-void
.end method

.method public queryLocalInterface(Ljava/lang/String;)Landroid/os/IHwInterface;
    .locals 1
    .param p1, "descriptor"    # Ljava/lang/String;

    .line 751
    const-string/jumbo v0, "vendor.xiaomi.hardware.misys@2.0::IMiSys"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 752
    return-object p0

    .line 754
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public registerAsService(Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 758
    invoke-virtual {p0, p1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->registerService(Ljava/lang/String;)V

    .line 759
    return-void
.end method

.method public final setHALInstrumentation()V
    .locals 0

    .line 713
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 763
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->interfaceDescriptor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@Stub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final unlinkToDeath(Landroid/os/IHwBinder$DeathRecipient;)Z
    .locals 1
    .param p1, "recipient"    # Landroid/os/IHwBinder$DeathRecipient;

    .line 745
    const/4 v0, 0x1

    return v0
.end method
