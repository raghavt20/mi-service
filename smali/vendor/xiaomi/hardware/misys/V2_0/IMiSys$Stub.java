public abstract class vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub extends android.os.HwBinder implements vendor.xiaomi.hardware.misys.V2_0.IMiSys {
	 /* .source "IMiSys.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/misys/V2_0/IMiSys; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* # direct methods */
public vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ( ) {
/* .locals 0 */
/* .line 676 */
/* invoke-direct {p0}, Landroid/os/HwBinder;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 0 */
/* .line 679 */
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 0 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 692 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
return;
} // .end method
public final miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 3 */
/* .line 729 */
/* new-instance v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v0}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 730 */
/* .local v0, "info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
v1 = android.os.HidlSupport .getPidIfSharable ( );
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->pid:I */
/* .line 731 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->ptr:J */
/* .line 732 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->arch:I */
/* .line 733 */
} // .end method
public final java.util.ArrayList getHashChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .line 704 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x20 */
/* new-array v2, v1, [B */
/* fill-array-data v2, :array_0 */
/* new-array v1, v1, [B */
/* fill-array-data v1, :array_1 */
/* filled-new-array {v2, v1}, [[B */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* :array_0 */
/* .array-data 1 */
/* -0x25t */
/* 0x5at */
/* -0x31t */
/* 0x44t */
/* 0x63t */
/* -0x6ft */
/* 0x31t */
/* 0x2at */
/* 0x2ft */
/* 0x1ft */
/* 0x53t */
/* 0x7dt */
/* 0x1t */
/* 0x19t */
/* 0x21t */
/* 0x6ft */
/* 0x9t */
/* 0x3ct */
/* 0x2et */
/* -0x19t */
/* 0x7dt */
/* -0x4ft */
/* 0x47t */
/* 0x3t */
/* -0x70t */
/* 0xct */
/* 0x2ft */
/* -0x2at */
/* 0x75t */
/* 0x2at */
/* -0x31t */
/* -0x2ft */
} // .end array-data
/* :array_1 */
/* .array-data 1 */
/* -0x14t */
/* 0x7ft */
/* -0x29t */
/* -0x62t */
/* -0x30t */
/* 0x2dt */
/* -0x6t */
/* -0x7bt */
/* -0x44t */
/* 0x49t */
/* -0x6ct */
/* 0x26t */
/* -0x53t */
/* -0x52t */
/* 0x3et */
/* -0x42t */
/* 0x23t */
/* -0x11t */
/* 0x5t */
/* 0x24t */
/* -0xdt */
/* -0x33t */
/* 0x69t */
/* 0x57t */
/* 0x13t */
/* -0x6dt */
/* 0x24t */
/* -0x48t */
/* 0x3bt */
/* 0x18t */
/* -0x36t */
/* 0x4ct */
} // .end array-data
} // .end method
public final java.util.ArrayList interfaceChain ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 684 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.misys@2.0::IMiSys" */
final String v2 = "android.hidl.base@1.0::IBase"; // const-string v2, "android.hidl.base@1.0::IBase"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
} // .end method
public final java.lang.String interfaceDescriptor ( ) {
/* .locals 1 */
/* .line 698 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.misys@2.0::IMiSys" */
} // .end method
public final Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .line 717 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void notifySyspropsChanged ( ) {
/* .locals 0 */
/* .line 739 */
android.os.HwBinder .enableInstrumentation ( );
/* .line 741 */
return;
} // .end method
public void onTransact ( Integer p0, android.os.HwParcel p1, android.os.HwParcel p2, Integer p3 ) {
/* .locals 15 */
/* .param p1, "_hidl_code" # I */
/* .param p2, "_hidl_request" # Landroid/os/HwParcel; */
/* .param p3, "_hidl_reply" # Landroid/os/HwParcel; */
/* .param p4, "_hidl_flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 769 */
/* move-object v6, p0 */
/* move-object/from16 v7, p2 */
/* move-object/from16 v8, p3 */
final String v0 = "android.hidl.base@1.0::IBase"; // const-string v0, "android.hidl.base@1.0::IBase"
/* const-string/jumbo v1, "vendor.xiaomi.hardware.misys@2.0::IMiSys" */
int v9 = 0; // const/4 v9, 0x0
/* sparse-switch p1, :sswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 998 */
/* :sswitch_0 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 1000 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).notifySyspropsChanged ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->notifySyspropsChanged()V
/* .line 1001 */
/* goto/16 :goto_1 */
/* .line 987 */
/* :sswitch_1 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 989 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).getDebugInfo ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->getDebugInfo()Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* .line 990 */
/* .local v0, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 991 */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v0 ).writeToParcel ( v8 ); // invoke-virtual {v0, v8}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 992 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 993 */
/* goto/16 :goto_1 */
/* .line 977 */
} // .end local v0 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :sswitch_2 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 979 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).ping ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->ping()V
/* .line 980 */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 981 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 982 */
/* goto/16 :goto_1 */
/* .line 972 */
/* :sswitch_3 */
/* goto/16 :goto_1 */
/* .line 964 */
/* :sswitch_4 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 966 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).setHALInstrumentation ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->setHALInstrumentation()V
/* .line 967 */
/* goto/16 :goto_1 */
/* .line 930 */
/* :sswitch_5 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 932 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).getHashChain ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->getHashChain()Ljava/util/ArrayList;
/* .line 933 */
/* .local v0, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 935 */
/* new-instance v1, Landroid/os/HwBlob; */
/* const/16 v2, 0x10 */
/* invoke-direct {v1, v2}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 937 */
/* .local v1, "_hidl_blob":Landroid/os/HwBlob; */
v2 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* .line 938 */
/* .local v2, "_hidl_vec_size":I */
/* const-wide/16 v3, 0x8 */
(( android.os.HwBlob ) v1 ).putInt32 ( v3, v4, v2 ); // invoke-virtual {v1, v3, v4, v2}, Landroid/os/HwBlob;->putInt32(JI)V
/* .line 939 */
/* const-wide/16 v3, 0xc */
(( android.os.HwBlob ) v1 ).putBool ( v3, v4, v9 ); // invoke-virtual {v1, v3, v4, v9}, Landroid/os/HwBlob;->putBool(JZ)V
/* .line 940 */
/* new-instance v3, Landroid/os/HwBlob; */
/* mul-int/lit8 v4, v2, 0x20 */
/* invoke-direct {v3, v4}, Landroid/os/HwBlob;-><init>(I)V */
/* .line 941 */
/* .local v3, "childBlob":Landroid/os/HwBlob; */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "_hidl_index_0":I */
} // :goto_0
/* if-ge v4, v2, :cond_1 */
/* .line 943 */
/* mul-int/lit8 v5, v4, 0x20 */
/* int-to-long v9, v5 */
/* .line 944 */
/* .local v9, "_hidl_array_offset_1":J */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v5, [B */
/* .line 946 */
/* .local v5, "_hidl_array_item_1":[B */
if ( v5 != null) { // if-eqz v5, :cond_0
/* array-length v11, v5 */
/* const/16 v12, 0x20 */
/* if-ne v11, v12, :cond_0 */
/* .line 950 */
(( android.os.HwBlob ) v3 ).putInt8Array ( v9, v10, v5 ); // invoke-virtual {v3, v9, v10, v5}, Landroid/os/HwBlob;->putInt8Array(J[B)V
/* .line 951 */
/* nop */
/* .line 941 */
} // .end local v5 # "_hidl_array_item_1":[B
} // .end local v9 # "_hidl_array_offset_1":J
/* add-int/lit8 v4, v4, 0x1 */
/* .line 947 */
/* .restart local v5 # "_hidl_array_item_1":[B */
/* .restart local v9 # "_hidl_array_offset_1":J */
} // :cond_0
/* new-instance v11, Ljava/lang/IllegalArgumentException; */
final String v12 = "Array element is not of the expected length"; // const-string v12, "Array element is not of the expected length"
/* invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V */
/* throw v11 */
/* .line 954 */
} // .end local v4 # "_hidl_index_0":I
} // .end local v5 # "_hidl_array_item_1":[B
} // .end local v9 # "_hidl_array_offset_1":J
} // :cond_1
/* const-wide/16 v4, 0x0 */
(( android.os.HwBlob ) v1 ).putBlob ( v4, v5, v3 ); // invoke-virtual {v1, v4, v5, v3}, Landroid/os/HwBlob;->putBlob(JLandroid/os/HwBlob;)V
/* .line 956 */
} // .end local v2 # "_hidl_vec_size":I
} // .end local v3 # "childBlob":Landroid/os/HwBlob;
(( android.os.HwParcel ) v8 ).writeBuffer ( v1 ); // invoke-virtual {v8, v1}, Landroid/os/HwParcel;->writeBuffer(Landroid/os/HwBlob;)V
/* .line 958 */
} // .end local v1 # "_hidl_blob":Landroid/os/HwBlob;
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 959 */
/* goto/16 :goto_1 */
/* .line 919 */
} // .end local v0 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :sswitch_6 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 921 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->interfaceDescriptor()Ljava/lang/String;
/* .line 922 */
/* .local v0, "_hidl_out_descriptor":Ljava/lang/String; */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 923 */
(( android.os.HwParcel ) v8 ).writeString ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 924 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 925 */
/* goto/16 :goto_1 */
/* .line 907 */
} // .end local v0 # "_hidl_out_descriptor":Ljava/lang/String;
/* :sswitch_7 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 909 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readNativeHandle()Landroid/os/NativeHandle; */
/* .line 910 */
/* .local v0, "fd":Landroid/os/NativeHandle; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList; */
/* .line 911 */
/* .local v1, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).debug ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->debug(Landroid/os/NativeHandle;Ljava/util/ArrayList;)V
/* .line 912 */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 913 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 914 */
/* goto/16 :goto_1 */
/* .line 896 */
} // .end local v0 # "fd":Landroid/os/NativeHandle;
} // .end local v1 # "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_8 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v0 ); // invoke-virtual {v7, v0}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 898 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).interfaceChain ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->interfaceChain()Ljava/util/ArrayList;
/* .line 899 */
/* .local v0, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 900 */
(( android.os.HwParcel ) v8 ).writeStringVector ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 901 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 902 */
/* goto/16 :goto_1 */
/* .line 886 */
} // .end local v0 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :sswitch_9 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 888 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).UnregisterVCameraCallback ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->UnregisterVCameraCallback()V
/* .line 889 */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 890 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 891 */
/* goto/16 :goto_1 */
/* .line 875 */
/* :sswitch_a */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 877 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readStrongBinder()Landroid/os/IHwBinder; */
vendor.xiaomi.hardware.misys.V2_0.IVCameraCallback .asInterface ( v0 );
/* .line 878 */
/* .local v0, "callback":Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback; */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).RegisterVCameraCallback ( v0 ); // invoke-virtual {p0, v0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->RegisterVCameraCallback(Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;)V
/* .line 879 */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 880 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 881 */
/* goto/16 :goto_1 */
/* .line 862 */
} // .end local v0 # "callback":Lvendor/xiaomi/hardware/misys/V2_0/IVCameraCallback;
/* :sswitch_b */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 864 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList; */
/* .line 865 */
/* .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readInt32()I */
/* .line 866 */
/* .local v1, "len":I */
v2 = (( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).OnFrameData ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->OnFrameData(Ljava/util/ArrayList;I)Z
/* .line 867 */
/* .local v2, "_hidl_out_result":Z */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 868 */
(( android.os.HwParcel ) v8 ).writeBool ( v2 ); // invoke-virtual {v8, v2}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 869 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 870 */
/* goto/16 :goto_1 */
/* .line 851 */
} // .end local v0 # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v1 # "len":I
} // .end local v2 # "_hidl_out_result":Z
/* :sswitch_c */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 853 */
v0 = (( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).DisconnectVirtualCamera ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->DisconnectVirtualCamera()Z
/* .line 854 */
/* .local v0, "_hidl_out_result":Z */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 855 */
(( android.os.HwParcel ) v8 ).writeBool ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 856 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 857 */
/* goto/16 :goto_1 */
/* .line 840 */
} // .end local v0 # "_hidl_out_result":Z
/* :sswitch_d */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 842 */
v0 = (( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).ConnectVirtualCamera ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->ConnectVirtualCamera()Z
/* .line 843 */
/* .restart local v0 # "_hidl_out_result":Z */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 844 */
(( android.os.HwParcel ) v8 ).writeBool ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 845 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 846 */
/* goto/16 :goto_1 */
/* .line 826 */
} // .end local v0 # "_hidl_out_result":Z
/* :sswitch_e */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 828 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readInt32()I */
/* .line 829 */
/* .local v0, "width":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readInt32()I */
/* .line 830 */
/* .local v1, "height":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readDouble()D */
/* move-result-wide v2 */
/* .line 831 */
/* .local v2, "frameRate":D */
v4 = (( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).SetVirtualCameraConfig ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->SetVirtualCameraConfig(IID)Z
/* .line 832 */
/* .local v4, "_hidl_out_result":Z */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 833 */
(( android.os.HwParcel ) v8 ).writeBool ( v4 ); // invoke-virtual {v8, v4}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 834 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 835 */
/* goto/16 :goto_1 */
/* .line 813 */
} // .end local v0 # "width":I
} // .end local v1 # "height":I
} // .end local v2 # "frameRate":D
} // .end local v4 # "_hidl_out_result":Z
/* :sswitch_f */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 815 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 816 */
/* .local v0, "path":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 817 */
/* .local v1, "file_name":Ljava/lang/String; */
v2 = (( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).IsExists ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->IsExists(Ljava/lang/String;Ljava/lang/String;)Z
/* .line 818 */
/* .local v2, "_hidl_out_result":Z */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 819 */
(( android.os.HwParcel ) v8 ).writeBool ( v2 ); // invoke-virtual {v8, v2}, Landroid/os/HwParcel;->writeBool(Z)V
/* .line 820 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 821 */
/* .line 800 */
} // .end local v0 # "path":Ljava/lang/String;
} // .end local v1 # "file_name":Ljava/lang/String;
} // .end local v2 # "_hidl_out_result":Z
/* :sswitch_10 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 802 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 803 */
/* .restart local v0 # "path":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 804 */
/* .restart local v1 # "file_name":Ljava/lang/String; */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).MiSysReadBuffer ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->MiSysReadBuffer(Ljava/lang/String;Ljava/lang/String;)Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;
/* .line 805 */
/* .local v2, "_hidl_out_result":Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult; */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 806 */
(( vendor.xiaomi.hardware.misys.V2_0.IBufferReadResult ) v2 ).writeToParcel ( v8 ); // invoke-virtual {v2, v8}, Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;->writeToParcel(Landroid/os/HwParcel;)V
/* .line 807 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 808 */
/* .line 785 */
} // .end local v0 # "path":Ljava/lang/String;
} // .end local v1 # "file_name":Ljava/lang/String;
} // .end local v2 # "_hidl_out_result":Lvendor/xiaomi/hardware/misys/V2_0/IBufferReadResult;
/* :sswitch_11 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 787 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 788 */
/* .local v10, "path":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 789 */
/* .local v11, "file_name":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readInt8Vector()Ljava/util/ArrayList; */
/* .line 790 */
/* .local v12, "writebuf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readInt64()J */
/* move-result-wide v13 */
/* .line 791 */
/* .local v13, "buf_len":J */
/* move-object v0, p0 */
/* move-object v1, v10 */
/* move-object v2, v11 */
/* move-object v3, v12 */
/* move-wide v4, v13 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->MiSysWriteBuffer(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;J)I */
/* .line 792 */
/* .local v0, "_hidl_out_result":I */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 793 */
(( android.os.HwParcel ) v8 ).writeInt32 ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 794 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 795 */
/* .line 772 */
} // .end local v0 # "_hidl_out_result":I
} // .end local v10 # "path":Ljava/lang/String;
} // .end local v11 # "file_name":Ljava/lang/String;
} // .end local v12 # "writebuf":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
} // .end local v13 # "buf_len":J
/* :sswitch_12 */
(( android.os.HwParcel ) v7 ).enforceInterface ( v1 ); // invoke-virtual {v7, v1}, Landroid/os/HwParcel;->enforceInterface(Ljava/lang/String;)V
/* .line 774 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 775 */
/* .local v0, "path":Ljava/lang/String; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HwParcel;->readString()Ljava/lang/String; */
/* .line 776 */
/* .local v1, "folder_name":Ljava/lang/String; */
v2 = (( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).MiSysCreateFolder ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->MiSysCreateFolder(Ljava/lang/String;Ljava/lang/String;)I
/* .line 777 */
/* .local v2, "_hidl_out_result":I */
(( android.os.HwParcel ) v8 ).writeStatus ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/HwParcel;->writeStatus(I)V
/* .line 778 */
(( android.os.HwParcel ) v8 ).writeInt32 ( v2 ); // invoke-virtual {v8, v2}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 779 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/HwParcel;->send()V */
/* .line 780 */
/* nop */
/* .line 1010 */
} // .end local v0 # "path":Ljava/lang/String;
} // .end local v1 # "folder_name":Ljava/lang/String;
} // .end local v2 # "_hidl_out_result":I
} // :goto_1
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_12 */
/* 0x2 -> :sswitch_11 */
/* 0x3 -> :sswitch_10 */
/* 0x4 -> :sswitch_f */
/* 0x5 -> :sswitch_e */
/* 0x6 -> :sswitch_d */
/* 0x7 -> :sswitch_c */
/* 0x8 -> :sswitch_b */
/* 0x9 -> :sswitch_a */
/* 0xa -> :sswitch_9 */
/* 0xf43484e -> :sswitch_8 */
/* 0xf444247 -> :sswitch_7 */
/* 0xf445343 -> :sswitch_6 */
/* 0xf485348 -> :sswitch_5 */
/* 0xf494e54 -> :sswitch_4 */
/* 0xf4c5444 -> :sswitch_3 */
/* 0xf504e47 -> :sswitch_2 */
/* 0xf524546 -> :sswitch_1 */
/* 0xf535953 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public final void ping ( ) {
/* .locals 0 */
/* .line 723 */
return;
} // .end method
public android.os.IHwInterface queryLocalInterface ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "descriptor" # Ljava/lang/String; */
/* .line 751 */
/* const-string/jumbo v0, "vendor.xiaomi.hardware.misys@2.0::IMiSys" */
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 752 */
/* .line 754 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void registerAsService ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "serviceName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 758 */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).registerService ( p1 ); // invoke-virtual {p0, p1}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->registerService(Ljava/lang/String;)V
/* .line 759 */
return;
} // .end method
public final void setHALInstrumentation ( ) {
/* .locals 0 */
/* .line 713 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 763 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( vendor.xiaomi.hardware.misys.V2_0.IMiSys$Stub ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/misys/V2_0/IMiSys$Stub;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Stub"; // const-string v1, "@Stub"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public final Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .line 745 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
