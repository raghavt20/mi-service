.class public interface abstract Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint;
.super Ljava/lang/Object;
.source "IXiaomiFingerprint.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub;,
        Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String;

.field public static final HASH:Ljava/lang/String; = "1ed45089cc89154986c15fc1c591d72e9ac7ae0d"

.field public static final VERSION:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 188
    const/16 v0, 0x24

    const/16 v1, 0x2e

    const-string/jumbo v2, "vendor$xiaomi$hardware$fingerprintextension$IXiaomiFingerprint"

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint;->DESCRIPTOR:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract extCmd(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getInterfaceHash()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getInterfaceVersion()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
