public abstract class vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub extends android.os.Binder implements vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint {
	 /* .source "IXiaomiFingerprint.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_extCmd;
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
/* # direct methods */
public vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub ( ) {
/* .locals 1 */
/* .line 40 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 41 */
(( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub;->markVintfStability()V
/* .line 42 */
v0 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub.DESCRIPTOR;
(( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 43 */
return;
} // .end method
public static vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 50 */
/* if-nez p0, :cond_0 */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
/* .line 53 */
} // :cond_0
v0 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub.DESCRIPTOR;
/* .line 54 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 55 */
/* move-object v1, v0 */
/* check-cast v1, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint; */
/* .line 57 */
} // :cond_1
/* new-instance v1, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 61 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 65 */
v0 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub.DESCRIPTOR;
/* .line 66 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-lt p1, v1, :cond_0 */
/* const v2, 0xffffff */
/* if-gt p1, v2, :cond_0 */
/* .line 67 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 69 */
} // :cond_0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 89 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 105 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 73 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 74 */
/* .line 78 */
/* :sswitch_1 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 79 */
v2 = (( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub ) p0 ).getInterfaceVersion ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub;->getInterfaceVersion()I
(( android.os.Parcel ) p3 ).writeInt ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 80 */
/* .line 84 */
/* :sswitch_2 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 85 */
(( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub ) p0 ).getInterfaceHash ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub;->getInterfaceHash()Ljava/lang/String;
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 86 */
/* .line 94 */
/* :pswitch_0 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 96 */
/* .local v2, "_arg0":I */
v3 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 97 */
/* .local v3, "_arg1":I */
(( android.os.Parcel ) p2 ).enforceNoDataAvail ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V
/* .line 98 */
v4 = (( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub ) p0 ).extCmd ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub;->extCmd(II)I
/* .line 99 */
/* .local v4, "_result":I */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 100 */
(( android.os.Parcel ) p3 ).writeInt ( v4 ); // invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 101 */
/* nop */
/* .line 108 */
} // .end local v2 # "_arg0":I
} // .end local v3 # "_arg1":I
} // .end local v4 # "_result":I
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
