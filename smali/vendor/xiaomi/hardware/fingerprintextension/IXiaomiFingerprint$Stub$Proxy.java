class vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy implements vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint {
	 /* .source "IXiaomiFingerprint.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 114 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 117 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->mCachedVersion:I */
/* .line 118 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 115 */
this.mRemote = p1;
/* .line 116 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 121 */
v0 = this.mRemote;
} // .end method
public Integer extCmd ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "cmd" # I */
/* .param p2, "param1" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 129 */
(( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 130 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 133 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 134 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 135 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 136 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 137 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 140 */
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* .line 141 */
	 v3 = 	 (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* move v2, v3 */
	 /* .line 144 */
	 /* .local v2, "_result":I */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 145 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 146 */
	 /* nop */
	 /* .line 147 */
	 /* .line 138 */
	 /* .local v2, "_status":Z */
} // :cond_0
try { // :try_start_1
	 /* new-instance v3, Landroid/os/RemoteException; */
	 final String v4 = "Method extCmd is unimplemented."; // const-string v4, "Method extCmd is unimplemented."
	 /* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;
} // .end local p1 # "cmd":I
} // .end local p2 # "param1":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 144 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy; */
/* .restart local p1 # "cmd":I */
/* .restart local p2 # "param1":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 145 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 146 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 125 */
v0 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy.DESCRIPTOR;
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 168 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 169 */
(( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 170 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 172 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_1
v2 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 173 */
v2 = this.mRemote;
/* const v3, 0xfffffe */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 174 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 175 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mCachedHash = v3;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 177 */
} // .end local v2 # "_status":Z
try { // :try_start_2
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 178 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 179 */
/* .line 177 */
} // .end local p0 # "this":Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 178 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 179 */
/* throw v2 */
/* .line 181 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
v0 = this.mCachedHash;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* monitor-exit p0 */
/* .line 167 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 151 */
/* iget v0, p0, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 152 */
(( vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 153 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 155 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.fingerprintextension.IXiaomiFingerprint$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 156 */
v2 = this.mRemote;
/* const v3, 0xffffff */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 157 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 158 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v3, p0, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 160 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 161 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 162 */
/* .line 160 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 161 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 162 */
/* throw v2 */
/* .line 164 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lvendor/xiaomi/hardware/fingerprintextension/IXiaomiFingerprint$Stub$Proxy;->mCachedVersion:I */
} // .end method
