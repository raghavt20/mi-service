public abstract class vendor.xiaomi.hardware.aon.IAlwaysOnListener implements android.os.IInterface {
	 /* .source "IAlwaysOnListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;, */
	 /* Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # direct methods */
static vendor.xiaomi.hardware.aon.IAlwaysOnListener ( ) {
	 /* .locals 3 */
	 /* .line 183 */
	 /* const/16 v0, 0x24 */
	 /* const/16 v1, 0x2e */
	 /* const-string/jumbo v2, "vendor$xiaomi$hardware$aon$IAlwaysOnListener" */
	 (( java.lang.String ) v2 ).replace ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
	 return;
} // .end method
/* # virtual methods */
public abstract java.lang.String getInterfaceHash ( ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onCallbackListener ( Integer p0, Integer[] p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
