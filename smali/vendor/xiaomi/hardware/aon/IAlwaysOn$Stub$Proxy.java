class vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy implements vendor.xiaomi.hardware.aon.IAlwaysOn {
	 /* .source "IAlwaysOn.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 163 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 166 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->mCachedVersion:I */
/* .line 167 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 164 */
this.mRemote = p1;
/* .line 165 */
return;
} // .end method
/* # virtual methods */
public void aon_update_parameter ( Integer p0, Float p1, Integer p2, Long p3 ) {
/* .locals 5 */
/* .param p1, "Type" # I */
/* .param p2, "fps" # F */
/* .param p3, "timeout" # I */
/* .param p4, "data" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 244 */
(( vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 245 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 247 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 248 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 249 */
(( android.os.Parcel ) v0 ).writeFloat ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 250 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 251 */
(( android.os.Parcel ) v0 ).writeLong ( p4, p5 ); // invoke-virtual {v0, p4, p5}, Landroid/os/Parcel;->writeLong(J)V
/* .line 252 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 253 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 256 */
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 259 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 260 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 261 */
/* nop */
/* .line 262 */
return;
/* .line 254 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method aon_update_parameter is unimplemented."; // const-string v4, "Method aon_update_parameter is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;
} // .end local p1 # "Type":I
} // .end local p2 # "fps":F
} // .end local p3 # "timeout":I
} // .end local p4 # "data":J
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 259 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy; */
/* .restart local p1 # "Type":I */
/* .restart local p2 # "fps":F */
/* .restart local p3 # "timeout":I */
/* .restart local p4 # "data":J */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 260 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 261 */
/* throw v2 */
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 170 */
v0 = this.mRemote;
} // .end method
public Integer getCapability ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 178 */
(( vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 179 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 182 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 183 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 184 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 187 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 188 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 191 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 192 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 193 */
/* nop */
/* .line 194 */
/* .line 185 */
/* .local v2, "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method getCapability is unimplemented."; // const-string v4, "Method getCapability is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 191 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 192 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 193 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 174 */
v0 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 282 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 283 */
(( vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 284 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 286 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_1
v2 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 287 */
v2 = this.mRemote;
/* const v3, 0xfffffe */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 288 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 289 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mCachedHash = v3;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 291 */
} // .end local v2 # "_status":Z
try { // :try_start_2
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 292 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 293 */
/* .line 291 */
} // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 292 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 293 */
/* throw v2 */
/* .line 295 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
v0 = this.mCachedHash;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* monitor-exit p0 */
/* .line 281 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 265 */
/* iget v0, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 266 */
(( vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 267 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 269 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 270 */
v2 = this.mRemote;
/* const v3, 0xffffff */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 271 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 272 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v3, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 274 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 275 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 276 */
/* .line 274 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 275 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 276 */
/* throw v2 */
/* .line 278 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->mCachedVersion:I */
} // .end method
public Integer registerListener ( Integer p0, Float p1, Integer p2, vendor.xiaomi.hardware.aon.IAlwaysOnListener p3 ) {
/* .locals 5 */
/* .param p1, "Type" # I */
/* .param p2, "fps" # F */
/* .param p3, "timeout" # I */
/* .param p4, "listener" # Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 198 */
(( vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 199 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 202 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 203 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 204 */
(( android.os.Parcel ) v0 ).writeFloat ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 205 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 206 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 207 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 208 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 211 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 212 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 215 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 216 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 217 */
/* nop */
/* .line 218 */
/* .line 209 */
/* .local v2, "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method registerListener is unimplemented."; // const-string v4, "Method registerListener is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;
} // .end local p1 # "Type":I
} // .end local p2 # "fps":F
} // .end local p3 # "timeout":I
} // .end local p4 # "listener":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 215 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy; */
/* .restart local p1 # "Type":I */
/* .restart local p2 # "fps":F */
/* .restart local p3 # "timeout":I */
/* .restart local p4 # "listener":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 216 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 217 */
/* throw v2 */
} // .end method
public Integer unregisterListener ( Integer p0, vendor.xiaomi.hardware.aon.IAlwaysOnListener p1 ) {
/* .locals 5 */
/* .param p1, "Type" # I */
/* .param p2, "listener" # Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 222 */
(( vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 223 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 226 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOn$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 227 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 228 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 229 */
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 230 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 233 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 234 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move v2, v3 */
/* .line 237 */
/* .local v2, "_result":I */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 238 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 239 */
/* nop */
/* .line 240 */
/* .line 231 */
/* .local v2, "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method unregisterListener is unimplemented."; // const-string v4, "Method unregisterListener is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;
} // .end local p1 # "Type":I
} // .end local p2 # "listener":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 237 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy; */
/* .restart local p1 # "Type":I */
/* .restart local p2 # "listener":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 238 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 239 */
/* throw v2 */
} // .end method
