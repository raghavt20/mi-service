.class public abstract Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;
.super Landroid/os/Binder;
.source "IAlwaysOn.java"

# interfaces
.implements Lvendor/xiaomi/hardware/aon/IAlwaysOn;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvendor/xiaomi/hardware/aon/IAlwaysOn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;
    }
.end annotation


# static fields
.field static final TRANSACTION_aon_update_parameter:I = 0x4

.field static final TRANSACTION_getCapability:I = 0x1

.field static final TRANSACTION_getInterfaceHash:I = 0xfffffe

.field static final TRANSACTION_getInterfaceVersion:I = 0xffffff

.field static final TRANSACTION_registerListener:I = 0x2

.field static final TRANSACTION_unregisterListener:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 51
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 52
    invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->markVintfStability()V

    .line 53
    sget-object v0, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {p0, p0, v0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lvendor/xiaomi/hardware/aon/IAlwaysOn;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 61
    if-nez p0, :cond_0

    .line 62
    const/4 v0, 0x0

    return-object v0

    .line 64
    :cond_0
    sget-object v0, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->DESCRIPTOR:Ljava/lang/String;

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 65
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lvendor/xiaomi/hardware/aon/IAlwaysOn;

    if-eqz v1, :cond_1

    .line 66
    move-object v1, v0

    check-cast v1, Lvendor/xiaomi/hardware/aon/IAlwaysOn;

    return-object v1

    .line 68
    :cond_1
    new-instance v1, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;

    invoke-direct {v1, p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 72
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 17
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 76
    move-object/from16 v6, p0

    move/from16 v7, p1

    move-object/from16 v8, p3

    sget-object v9, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->DESCRIPTOR:Ljava/lang/String;

    .line 77
    .local v9, "descriptor":Ljava/lang/String;
    const/4 v10, 0x1

    if-lt v7, v10, :cond_0

    const v0, 0xffffff

    if-gt v7, v0, :cond_0

    .line 78
    move-object/from16 v11, p2

    invoke-virtual {v11, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_0
    move-object/from16 v11, p2

    .line 80
    :goto_0
    sparse-switch v7, :sswitch_data_0

    .line 100
    packed-switch v7, :pswitch_data_0

    .line 154
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 84
    :sswitch_0
    invoke-virtual {v8, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    return v10

    .line 89
    :sswitch_1
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 90
    invoke-virtual/range {p0 .. p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->getInterfaceVersion()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    return v10

    .line 95
    :sswitch_2
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 96
    invoke-virtual/range {p0 .. p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->getInterfaceHash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    return v10

    .line 140
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 142
    .local v12, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    move-result v13

    .line 144
    .local v13, "_arg1":F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 146
    .local v14, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v15

    .line 147
    .local v15, "_arg3":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 148
    move-object/from16 v0, p0

    move v1, v12

    move v2, v13

    move v3, v14

    move-wide v4, v15

    invoke-virtual/range {v0 .. v5}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->aon_update_parameter(IFIJ)V

    .line 149
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    goto :goto_1

    .line 128
    .end local v12    # "_arg0":I
    .end local v13    # "_arg1":F
    .end local v14    # "_arg2":I
    .end local v15    # "_arg3":J
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 130
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->asInterface(Landroid/os/IBinder;)Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;

    move-result-object v1

    .line 131
    .local v1, "_arg1":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 132
    invoke-virtual {v6, v0, v1}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->unregisterListener(ILvendor/xiaomi/hardware/aon/IAlwaysOnListener;)I

    move-result v2

    .line 133
    .local v2, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    invoke-virtual {v8, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    goto :goto_1

    .line 112
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
    .end local v2    # "_result":I
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 114
    .restart local v0    # "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 116
    .local v1, "_arg1":F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 118
    .local v2, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->asInterface(Landroid/os/IBinder;)Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;

    move-result-object v3

    .line 119
    .local v3, "_arg3":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 120
    invoke-virtual {v6, v0, v1, v2, v3}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->registerListener(IFILvendor/xiaomi/hardware/aon/IAlwaysOnListener;)I

    move-result v4

    .line 121
    .local v4, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    invoke-virtual {v8, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    goto :goto_1

    .line 104
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":F
    .end local v2    # "_arg2":I
    .end local v3    # "_arg3":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
    .end local v4    # "_result":I
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;->getCapability()I

    move-result v0

    .line 105
    .local v0, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    nop

    .line 157
    .end local v0    # "_result":I
    :goto_1
    return v10

    nop

    :sswitch_data_0
    .sparse-switch
        0xfffffe -> :sswitch_2
        0xffffff -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
