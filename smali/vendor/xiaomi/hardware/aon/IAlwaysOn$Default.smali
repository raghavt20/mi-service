.class public Lvendor/xiaomi/hardware/aon/IAlwaysOn$Default;
.super Ljava/lang/Object;
.source "IAlwaysOn.java"

# interfaces
.implements Lvendor/xiaomi/hardware/aon/IAlwaysOn;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvendor/xiaomi/hardware/aon/IAlwaysOn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Default"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public aon_update_parameter(IFIJ)V
    .locals 0
    .param p1, "Type"    # I
    .param p2, "fps"    # F
    .param p3, "timeout"    # I
    .param p4, "data"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 32
    return-void
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCapability()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 20
    const/4 v0, 0x0

    return v0
.end method

.method public getInterfaceHash()Ljava/lang/String;
    .locals 1

    .line 39
    const-string v0, ""

    return-object v0
.end method

.method public getInterfaceVersion()I
    .locals 1

    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public registerListener(IFILvendor/xiaomi/hardware/aon/IAlwaysOnListener;)I
    .locals 1
    .param p1, "Type"    # I
    .param p2, "fps"    # F
    .param p3, "timeout"    # I
    .param p4, "listener"    # Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterListener(ILvendor/xiaomi/hardware/aon/IAlwaysOnListener;)I
    .locals 1
    .param p1, "Type"    # I
    .param p2, "listener"    # Lvendor/xiaomi/hardware/aon/IAlwaysOnListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 28
    const/4 v0, 0x0

    return v0
.end method
