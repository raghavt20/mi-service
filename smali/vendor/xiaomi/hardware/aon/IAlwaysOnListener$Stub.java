public abstract class vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub extends android.os.Binder implements vendor.xiaomi.hardware.aon.IAlwaysOnListener {
	 /* .source "IAlwaysOnListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
static final Integer TRANSACTION_onCallbackListener;
/* # direct methods */
public vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub ( ) {
/* .locals 1 */
/* .line 39 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 40 */
(( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->markVintfStability()V
/* .line 41 */
v0 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub.DESCRIPTOR;
(( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 42 */
return;
} // .end method
public static vendor.xiaomi.hardware.aon.IAlwaysOnListener asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 49 */
/* if-nez p0, :cond_0 */
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
} // :cond_0
v0 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub.DESCRIPTOR;
/* .line 53 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 54 */
/* move-object v1, v0 */
/* check-cast v1, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* .line 56 */
} // :cond_1
/* new-instance v1, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 60 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 4 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 64 */
v0 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub.DESCRIPTOR;
/* .line 65 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-lt p1, v1, :cond_0 */
/* const v2, 0xffffff */
/* if-gt p1, v2, :cond_0 */
/* .line 66 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 68 */
} // :cond_0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 88 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 103 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 72 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 73 */
/* .line 77 */
/* :sswitch_1 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 78 */
v2 = (( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub ) p0 ).getInterfaceVersion ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->getInterfaceVersion()I
(( android.os.Parcel ) p3 ).writeInt ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 79 */
/* .line 83 */
/* :sswitch_2 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 84 */
(( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub ) p0 ).getInterfaceHash ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->getInterfaceHash()Ljava/lang/String;
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 85 */
/* .line 93 */
/* :pswitch_0 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 95 */
/* .local v2, "_arg0":I */
(( android.os.Parcel ) p2 ).createIntArray ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I
/* .line 96 */
/* .local v3, "_arg1":[I */
(( android.os.Parcel ) p2 ).enforceNoDataAvail ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V
/* .line 97 */
(( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub ) p0 ).onCallbackListener ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub;->onCallbackListener(I[I)V
/* .line 98 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 99 */
/* nop */
/* .line 106 */
} // .end local v2 # "_arg0":I
} // .end local v3 # "_arg1":[I
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
