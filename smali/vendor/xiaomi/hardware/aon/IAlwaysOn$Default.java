public class vendor.xiaomi.hardware.aon.IAlwaysOn$Default implements vendor.xiaomi.hardware.aon.IAlwaysOn {
	 /* .source "IAlwaysOn.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/aon/IAlwaysOn; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Default" */
} // .end annotation
/* # direct methods */
public vendor.xiaomi.hardware.aon.IAlwaysOn$Default ( ) {
/* .locals 0 */
/* .line 16 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void aon_update_parameter ( Integer p0, Float p1, Integer p2, Long p3 ) {
/* .locals 0 */
/* .param p1, "Type" # I */
/* .param p2, "fps" # F */
/* .param p3, "timeout" # I */
/* .param p4, "data" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 32 */
return;
} // .end method
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer getCapability ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 20 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public java.lang.String getInterfaceHash ( ) {
/* .locals 1 */
/* .line 39 */
final String v0 = ""; // const-string v0, ""
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 1 */
/* .line 35 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer registerListener ( Integer p0, Float p1, Integer p2, vendor.xiaomi.hardware.aon.IAlwaysOnListener p3 ) {
/* .locals 1 */
/* .param p1, "Type" # I */
/* .param p2, "fps" # F */
/* .param p3, "timeout" # I */
/* .param p4, "listener" # Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 24 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Integer unregisterListener ( Integer p0, vendor.xiaomi.hardware.aon.IAlwaysOnListener p1 ) {
/* .locals 1 */
/* .param p1, "Type" # I */
/* .param p2, "listener" # Lvendor/xiaomi/hardware/aon/IAlwaysOnListener; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 28 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
