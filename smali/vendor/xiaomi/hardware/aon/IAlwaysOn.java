public abstract class vendor.xiaomi.hardware.aon.IAlwaysOn implements android.os.IInterface {
	 /* .source "IAlwaysOn.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lvendor/xiaomi/hardware/aon/IAlwaysOn$Stub;, */
	 /* Lvendor/xiaomi/hardware/aon/IAlwaysOn$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # direct methods */
static vendor.xiaomi.hardware.aon.IAlwaysOn ( ) {
	 /* .locals 3 */
	 /* .line 305 */
	 /* const/16 v0, 0x24 */
	 /* const/16 v1, 0x2e */
	 /* const-string/jumbo v2, "vendor$xiaomi$hardware$aon$IAlwaysOn" */
	 (( java.lang.String ) v2 ).replace ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
	 return;
} // .end method
/* # virtual methods */
public abstract void aon_update_parameter ( Integer p0, Float p1, Integer p2, Long p3 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getCapability ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract java.lang.String getInterfaceHash ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer registerListener ( Integer p0, Float p1, Integer p2, vendor.xiaomi.hardware.aon.IAlwaysOnListener p3 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer unregisterListener ( Integer p0, vendor.xiaomi.hardware.aon.IAlwaysOnListener p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
