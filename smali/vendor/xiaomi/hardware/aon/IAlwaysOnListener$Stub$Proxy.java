class vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy implements vendor.xiaomi.hardware.aon.IAlwaysOnListener {
	 /* .source "IAlwaysOnListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 112 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 115 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->mCachedVersion:I */
/* .line 116 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 113 */
this.mRemote = p1;
/* .line 114 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 119 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 123 */
v0 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy.DESCRIPTOR;
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 163 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 164 */
	 (( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->asBinder()Landroid/os/IBinder;
	 android.os.Parcel .obtain ( v0 );
	 /* .line 165 */
	 /* .local v0, "data":Landroid/os/Parcel; */
	 android.os.Parcel .obtain ( );
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
	 /* .line 167 */
	 /* .local v1, "reply":Landroid/os/Parcel; */
	 try { // :try_start_1
		 v2 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy.DESCRIPTOR;
		 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
		 /* .line 168 */
		 v2 = this.mRemote;
		 /* const v3, 0xfffffe */
		 v2 = 		 int v4 = 0; // const/4 v4, 0x0
		 /* .line 169 */
		 /* .local v2, "_status":Z */
		 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
		 /* .line 170 */
		 (( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
		 this.mCachedHash = v3;
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 172 */
	 } // .end local v2 # "_status":Z
	 try { // :try_start_2
		 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
		 /* .line 173 */
		 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
		 /* .line 174 */
		 /* .line 172 */
	 } // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;
	 /* :catchall_0 */
	 /* move-exception v2 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 173 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 174 */
	 /* throw v2 */
	 /* .line 176 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
v0 = this.mCachedHash;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* monitor-exit p0 */
/* .line 162 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 146 */
/* iget v0, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 147 */
(( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 148 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 150 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 151 */
v2 = this.mRemote;
/* const v3, 0xffffff */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 152 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 153 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v3, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 155 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 156 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 157 */
/* .line 155 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 156 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 157 */
/* throw v2 */
/* .line 159 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->mCachedVersion:I */
} // .end method
public void onCallbackListener ( Integer p0, Integer[] p1 ) {
/* .locals 5 */
/* .param p1, "Type" # I */
/* .param p2, "data" # [I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 127 */
(( vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 128 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 130 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.aon.IAlwaysOnListener$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 131 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 132 */
(( android.os.Parcel ) v0 ).writeIntArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 133 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 134 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 137 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 140 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 141 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 142 */
/* nop */
/* .line 143 */
return;
/* .line 135 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method onCallbackListener is unimplemented."; // const-string v4, "Method onCallbackListener is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy;
} // .end local p1 # "Type":I
} // .end local p2 # "data":[I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 140 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/aon/IAlwaysOnListener$Stub$Proxy; */
/* .restart local p1 # "Type":I */
/* .restart local p2 # "data":[I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 141 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 142 */
/* throw v2 */
} // .end method
