class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy implements vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel {
	 /* .source "IMiFxTunnel.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 126 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 129 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->mCachedVersion:I */
/* .line 130 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 127 */
this.mRemote = p1;
/* .line 128 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 133 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 137 */
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy.DESCRIPTOR;
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 198 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 199 */
	 (( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->asBinder()Landroid/os/IBinder;
	 android.os.Parcel .obtain ( v0 );
	 /* .line 200 */
	 /* .local v0, "data":Landroid/os/Parcel; */
	 android.os.Parcel .obtain ( );
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
	 /* .line 202 */
	 /* .local v1, "reply":Landroid/os/Parcel; */
	 try { // :try_start_1
		 v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy.DESCRIPTOR;
		 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
		 /* .line 203 */
		 v2 = this.mRemote;
		 /* const v3, 0xfffffe */
		 v2 = 		 int v4 = 0; // const/4 v4, 0x0
		 /* .line 204 */
		 /* .local v2, "_status":Z */
		 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
		 /* .line 205 */
		 (( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
		 this.mCachedHash = v3;
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 207 */
	 } // .end local v2 # "_status":Z
	 try { // :try_start_2
		 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
		 /* .line 208 */
		 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
		 /* .line 209 */
		 /* .line 207 */
	 } // .end local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;
	 /* :catchall_0 */
	 /* move-exception v2 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 208 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 209 */
	 /* throw v2 */
	 /* .line 211 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
v0 = this.mCachedHash;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* monitor-exit p0 */
/* .line 197 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 181 */
/* iget v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 182 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 183 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 185 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 186 */
v2 = this.mRemote;
/* const v3, 0xffffff */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 187 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 188 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v3, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 190 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 191 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 192 */
/* .line 190 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 191 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 192 */
/* throw v2 */
/* .line 194 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->mCachedVersion:I */
} // .end method
public vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult invokeCommand ( Integer p0, Object[] p1 ) {
/* .locals 5 */
/* .param p1, "cmdId" # I */
/* .param p2, "param" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 141 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 142 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 145 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 146 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 147 */
(( android.os.Parcel ) v0 ).writeByteArray ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 148 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 149 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 152 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 153 */
v3 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult.CREATOR;
(( android.os.Parcel ) v1 ).readTypedObject ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Parcel;->readTypedObject(Landroid/os/Parcelable$Creator;)Ljava/lang/Object;
/* check-cast v3, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v2, v3 */
/* .line 156 */
/* .local v2, "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 157 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 158 */
/* nop */
/* .line 159 */
/* .line 150 */
/* .local v2, "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method invokeCommand is unimplemented."; // const-string v4, "Method invokeCommand is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;
} // .end local p1 # "cmdId":I
} // .end local p2 # "param":[B
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 156 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy; */
/* .restart local p1 # "cmdId":I */
/* .restart local p2 # "param":[B */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 157 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 158 */
/* throw v2 */
} // .end method
public void setNotify ( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback p0 ) {
/* .locals 5 */
/* .param p1, "callback" # Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 163 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 164 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 166 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 167 */
(( android.os.Parcel ) v0 ).writeStrongInterface ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeStrongInterface(Landroid/os/IInterface;)V
/* .line 168 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 169 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 172 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 175 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 176 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 177 */
/* nop */
/* .line 178 */
return;
/* .line 170 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setNotify is unimplemented."; // const-string v4, "Method setNotify is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;
} // .end local p1 # "callback":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 175 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy; */
/* .restart local p1 # "callback":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 176 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 177 */
/* throw v2 */
} // .end method
