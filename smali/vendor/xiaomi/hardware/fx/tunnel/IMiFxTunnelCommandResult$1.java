class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult$1 implements android.os.Parcelable$Creator {
	 /* .source "IMiFxTunnelCommandResult.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Landroid/os/Parcelable$Creator<", */
/* "Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult$1 ( ) {
/* .locals 0 */
/* .line 11 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public java.lang.Object createFromParcel ( android.os.Parcel p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 11 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult$1 ) p0 ).createFromParcel ( p1 ); // invoke-virtual {p0, p1}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult$1;->createFromParcel(Landroid/os/Parcel;)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
} // .end method
public vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult createFromParcel ( android.os.Parcel p0 ) {
/* .locals 1 */
/* .param p1, "_aidl_source" # Landroid/os/Parcel; */
/* .line 14 */
/* new-instance v0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
/* invoke-direct {v0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;-><init>()V */
/* .line 15 */
/* .local v0, "_aidl_out":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult ) v0 ).readFromParcel ( p1 ); // invoke-virtual {v0, p1}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->readFromParcel(Landroid/os/Parcel;)V
/* .line 16 */
} // .end method
public java.lang.Object newArray ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 11 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult$1 ) p0 ).newArray ( p1 ); // invoke-virtual {p0, p1}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult$1;->newArray(I)[Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
} // .end method
public vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult newArray ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "_aidl_size" # I */
/* .line 20 */
/* new-array v0, p1, [Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
} // .end method
