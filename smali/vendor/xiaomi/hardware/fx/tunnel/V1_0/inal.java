public class inal implements vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnelCallback {
	 /* .source "IMiFxTunnelCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x19 */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IHwBinder mRemote;
/* # direct methods */
public inal ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IHwBinder; */
/* .line 203 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 204 */
java.util.Objects .requireNonNull ( p1 );
/* check-cast v0, Landroid/os/IHwBinder; */
this.mRemote = v0;
/* .line 205 */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 1 */
/* .line 209 */
v0 = this.mRemote;
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 275 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 276 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 277 */
(( android.os.HwParcel ) v0 ).writeNativeHandle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeNativeHandle(Landroid/os/NativeHandle;)V
/* .line 278 */
(( android.os.HwParcel ) v0 ).writeStringVector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 280 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 282 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf444247 */
int v4 = 0; // const/4 v4, 0x0
/* .line 283 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 284 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 286 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 287 */
/* nop */
/* .line 288 */
return;
/* .line 286 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 287 */
/* throw v2 */
} // .end method
public final Boolean equals ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "other" # Ljava/lang/Object; */
/* .line 224 */
v0 = android.os.HidlSupport .interfacesEqual ( p0,p1 );
} // .end method
public miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 387 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 388 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 390 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 392 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf524546 */
int v4 = 0; // const/4 v4, 0x0
/* .line 393 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 394 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 396 */
/* new-instance v2, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v2}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 397 */
/* .local v2, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v2 ).readFromParcel ( v1 ); // invoke-virtual {v2, v1}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->readFromParcel(Landroid/os/HwParcel;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 398 */
/* nop */
/* .line 400 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 398 */
/* .line 400 */
} // .end local v2 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 401 */
/* throw v2 */
} // .end method
public java.util.ArrayList getHashChain ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 312 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 313 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 315 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 317 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf485348 */
int v4 = 0; // const/4 v4, 0x0
/* .line 318 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 319 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 321 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v10, v2 */
/* .line 323 */
/* .local v10, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
/* const-wide/16 v2, 0x10 */
(( android.os.HwParcel ) v1 ).readBuffer ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/HwParcel;->readBuffer(J)Landroid/os/HwBlob;
/* move-object v11, v2 */
/* .line 325 */
/* .local v11, "_hidl_blob":Landroid/os/HwBlob; */
/* const-wide/16 v2, 0x8 */
v2 = (( android.os.HwBlob ) v11 ).getInt32 ( v2, v3 ); // invoke-virtual {v11, v2, v3}, Landroid/os/HwBlob;->getInt32(J)I
/* move v12, v2 */
/* .line 326 */
/* .local v12, "_hidl_vec_size":I */
/* mul-int/lit8 v2, v12, 0x20 */
/* int-to-long v3, v2 */
/* .line 327 */
(( android.os.HwBlob ) v11 ).handle ( ); // invoke-virtual {v11}, Landroid/os/HwBlob;->handle()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
int v9 = 1; // const/4 v9, 0x1
/* .line 326 */
/* move-object v2, v1 */
/* invoke-virtual/range {v2 ..v9}, Landroid/os/HwParcel;->readEmbeddedBuffer(JJJZ)Landroid/os/HwBlob; */
/* .line 330 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
(( java.util.ArrayList ) v10 ).clear ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V
/* .line 331 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "_hidl_index_0":I */
} // :goto_0
/* if-ge v3, v12, :cond_0 */
/* .line 332 */
/* const/16 v4, 0x20 */
/* new-array v5, v4, [B */
/* .line 334 */
/* .local v5, "_hidl_vec_element":[B */
/* mul-int/lit8 v6, v3, 0x20 */
/* int-to-long v6, v6 */
/* .line 335 */
/* .local v6, "_hidl_array_offset_1":J */
(( android.os.HwBlob ) v2 ).copyToInt8Array ( v6, v7, v5, v4 ); // invoke-virtual {v2, v6, v7, v5, v4}, Landroid/os/HwBlob;->copyToInt8Array(J[BI)V
/* .line 336 */
/* nop */
/* .line 338 */
} // .end local v6 # "_hidl_array_offset_1":J
(( java.util.ArrayList ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 331 */
/* nop */
} // .end local v5 # "_hidl_vec_element":[B
/* add-int/lit8 v3, v3, 0x1 */
/* .line 342 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_index_0":I
} // .end local v11 # "_hidl_blob":Landroid/os/HwBlob;
} // .end local v12 # "_hidl_vec_size":I
} // :cond_0
/* nop */
/* .line 344 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 342 */
/* .line 344 */
} // .end local v10 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 345 */
/* throw v2 */
} // .end method
public final Integer hashCode ( ) {
/* .locals 1 */
/* .line 229 */
(( vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnelCallback$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback$Proxy;->asBinder()Landroid/os/IHwBinder;
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
} // .end method
public java.util.ArrayList interfaceChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 256 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 257 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 259 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 261 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf43484e */
int v4 = 0; // const/4 v4, 0x0
/* .line 262 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 263 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 265 */
(( android.os.HwParcel ) v1 ).readStringVector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 266 */
/* .local v2, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* nop */
/* .line 268 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 266 */
/* .line 268 */
} // .end local v2 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 269 */
/* throw v2 */
} // .end method
public java.lang.String interfaceDescriptor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 293 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 294 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 296 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 298 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf445343 */
int v4 = 0; // const/4 v4, 0x0
/* .line 299 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 300 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 302 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 303 */
/* .local v2, "_hidl_out_descriptor":Ljava/lang/String; */
/* nop */
/* .line 305 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 303 */
/* .line 305 */
} // .end local v2 # "_hidl_out_descriptor":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 306 */
/* throw v2 */
} // .end method
public Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 366 */
v0 = v0 = this.mRemote;
} // .end method
public void notifySyspropsChanged ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 407 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 408 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 410 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 412 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf535953 */
int v4 = 1; // const/4 v4, 0x1
/* .line 413 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 415 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 416 */
/* nop */
/* .line 417 */
return;
/* .line 415 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 416 */
/* throw v2 */
} // .end method
public void onMessage ( Long p0, Integer p1, Integer p2, java.util.ArrayList p3 ) {
/* .locals 4 */
/* .param p1, "devId" # J */
/* .param p3, "msgId" # I */
/* .param p4, "cmdId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(JII", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Byte;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 236 */
/* .local p5, "msg_data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 237 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hardware.fx.tunnel@1.0::IMiFxTunnelCallback" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 238 */
(( android.os.HwParcel ) v0 ).writeInt64 ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/HwParcel;->writeInt64(J)V
/* .line 239 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 240 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 241 */
(( android.os.HwParcel ) v0 ).writeInt8Vector ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/HwParcel;->writeInt8Vector(Ljava/util/ArrayList;)V
/* .line 243 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 245 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
/* .line 246 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 248 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 249 */
/* nop */
/* .line 250 */
return;
/* .line 248 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 249 */
/* throw v2 */
} // .end method
public void ping ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 371 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 372 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 374 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 376 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf504e47 */
int v4 = 0; // const/4 v4, 0x0
/* .line 377 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 378 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 380 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 381 */
/* nop */
/* .line 382 */
return;
/* .line 380 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 381 */
/* throw v2 */
} // .end method
public void setHALInstrumentation ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 351 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 352 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 354 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 356 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf494e54 */
int v4 = 1; // const/4 v4, 0x1
/* .line 357 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 359 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 360 */
/* nop */
/* .line 361 */
return;
/* .line 359 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 360 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 215 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( vendor.xiaomi.hardware.fx.tunnel.V1_0.IMiFxTunnelCallback$Proxy ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/V1_0/IMiFxTunnelCallback$Proxy;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Proxy"; // const-string v1, "@Proxy"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 216 */
/* :catch_0 */
/* move-exception v0 */
/* .line 219 */
final String v0 = "[class or subclass of vendor.xiaomi.hardware.fx.tunnel@1.0::IMiFxTunnelCallback]@Proxy"; // const-string v0, "[class or subclass of vendor.xiaomi.hardware.fx.tunnel@1.0::IMiFxTunnelCallback]@Proxy"
} // .end method
public Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 422 */
v0 = v0 = this.mRemote;
} // .end method
