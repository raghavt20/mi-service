class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy implements vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback {
	 /* .source "IMiFxTunnelCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 115 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 118 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->mCachedVersion:I */
/* .line 119 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 116 */
this.mRemote = p1;
/* .line 117 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 122 */
v0 = this.mRemote;
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 126 */
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy.DESCRIPTOR;
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 165 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 166 */
	 (( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->asBinder()Landroid/os/IBinder;
	 android.os.Parcel .obtain ( v0 );
	 /* .line 167 */
	 /* .local v0, "data":Landroid/os/Parcel; */
	 android.os.Parcel .obtain ( );
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
	 /* .line 169 */
	 /* .local v1, "reply":Landroid/os/Parcel; */
	 try { // :try_start_1
		 v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy.DESCRIPTOR;
		 (( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
		 /* .line 170 */
		 v2 = this.mRemote;
		 /* const v3, 0xfffffe */
		 v2 = 		 int v4 = 0; // const/4 v4, 0x0
		 /* .line 171 */
		 /* .local v2, "_status":Z */
		 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
		 /* .line 172 */
		 (( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
		 this.mCachedHash = v3;
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* .line 174 */
	 } // .end local v2 # "_status":Z
	 try { // :try_start_2
		 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
		 /* .line 175 */
		 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
		 /* .line 176 */
		 /* .line 174 */
	 } // .end local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;
	 /* :catchall_0 */
	 /* move-exception v2 */
	 (( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
	 /* .line 175 */
	 (( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
	 /* .line 176 */
	 /* throw v2 */
	 /* .line 178 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
v0 = this.mCachedHash;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* monitor-exit p0 */
/* .line 164 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 148 */
/* iget v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 149 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 150 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 152 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 153 */
v2 = this.mRemote;
/* const v3, 0xffffff */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 154 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 155 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v3, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 157 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 158 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 159 */
/* .line 157 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 158 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 159 */
/* throw v2 */
/* .line 161 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->mCachedVersion:I */
} // .end method
public void onDaemonMessage ( Long p0, Integer p1, Integer p2, Object[] p3 ) {
/* .locals 4 */
/* .param p1, "devId" # J */
/* .param p3, "msgId" # I */
/* .param p4, "cmdId" # I */
/* .param p5, "data" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 130 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 132 */
/* .local v0, "_data":Landroid/os/Parcel; */
try { // :try_start_0
v1 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 133 */
(( android.os.Parcel ) v0 ).writeLong ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V
/* .line 134 */
(( android.os.Parcel ) v0 ).writeInt ( p3 ); // invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V
/* .line 135 */
(( android.os.Parcel ) v0 ).writeInt ( p4 ); // invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V
/* .line 136 */
(( android.os.Parcel ) v0 ).writeByteArray ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 137 */
v1 = this.mRemote;
int v2 = 0; // const/4 v2, 0x0
v1 = int v3 = 1; // const/4 v3, 0x1
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 138 */
/* .local v1, "_status":Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 143 */
} // .end local v1 # "_status":Z
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 144 */
/* nop */
/* .line 145 */
return;
/* .line 139 */
/* .restart local v1 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v2, Landroid/os/RemoteException; */
final String v3 = "Method onDaemonMessage is unimplemented."; // const-string v3, "Method onDaemonMessage is unimplemented."
/* invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;
} // .end local p1 # "devId":J
} // .end local p3 # "msgId":I
} // .end local p4 # "cmdId":I
} // .end local p5 # "data":[B
/* throw v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 143 */
} // .end local v1 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy; */
/* .restart local p1 # "devId":J */
/* .restart local p3 # "msgId":I */
/* .restart local p4 # "cmdId":I */
/* .restart local p5 # "data":[B */
/* :catchall_0 */
/* move-exception v1 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 144 */
/* throw v1 */
} // .end method
