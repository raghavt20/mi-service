public abstract class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub extends android.os.Binder implements vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel {
	 /* .source "IMiFxTunnel.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
static final Integer TRANSACTION_invokeCommand;
static final Integer TRANSACTION_setNotify;
/* # direct methods */
public vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ( ) {
/* .locals 1 */
/* .line 43 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 44 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->markVintfStability()V
/* .line 45 */
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub.DESCRIPTOR;
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 46 */
return;
} // .end method
public static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 53 */
/* if-nez p0, :cond_0 */
/* .line 54 */
int v0 = 0; // const/4 v0, 0x0
/* .line 56 */
} // :cond_0
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub.DESCRIPTOR;
/* .line 57 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 58 */
/* move-object v1, v0 */
/* check-cast v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel; */
/* .line 60 */
} // :cond_1
/* new-instance v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 64 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 68 */
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub.DESCRIPTOR;
/* .line 69 */
/* .local v0, "descriptor":Ljava/lang/String; */
int v1 = 1; // const/4 v1, 0x1
/* if-lt p1, v1, :cond_0 */
/* const v2, 0xffffff */
/* if-gt p1, v2, :cond_0 */
/* .line 70 */
(( android.os.Parcel ) p2 ).enforceInterface ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 72 */
} // :cond_0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 92 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 117 */
v1 = /* invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 76 */
/* :sswitch_0 */
(( android.os.Parcel ) p3 ).writeString ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 77 */
/* .line 81 */
/* :sswitch_1 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 82 */
v2 = (( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ) p0 ).getInterfaceVersion ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->getInterfaceVersion()I
(( android.os.Parcel ) p3 ).writeInt ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 83 */
/* .line 87 */
/* :sswitch_2 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 88 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ) p0 ).getInterfaceHash ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->getInterfaceHash()Ljava/lang/String;
(( android.os.Parcel ) p3 ).writeString ( v2 ); // invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 89 */
/* .line 109 */
/* :pswitch_0 */
(( android.os.Parcel ) p2 ).readStrongBinder ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;
vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub .asInterface ( v2 );
/* .line 110 */
/* .local v2, "_arg0":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback; */
(( android.os.Parcel ) p2 ).enforceNoDataAvail ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V
/* .line 111 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ) p0 ).setNotify ( v2 ); // invoke-virtual {p0, v2}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->setNotify(Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;)V
/* .line 112 */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 113 */
/* .line 97 */
} // .end local v2 # "_arg0":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;
/* :pswitch_1 */
v2 = (( android.os.Parcel ) p2 ).readInt ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I
/* .line 99 */
/* .local v2, "_arg0":I */
(( android.os.Parcel ) p2 ).createByteArray ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B
/* .line 100 */
/* .local v3, "_arg1":[B */
(( android.os.Parcel ) p2 ).enforceNoDataAvail ( ); // invoke-virtual {p2}, Landroid/os/Parcel;->enforceNoDataAvail()V
/* .line 101 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel$Stub ) p0 ).invokeCommand ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;->invokeCommand(I[B)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
/* .line 102 */
/* .local v4, "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
(( android.os.Parcel ) p3 ).writeNoException ( ); // invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V
/* .line 103 */
(( android.os.Parcel ) p3 ).writeTypedObject ( v4, v1 ); // invoke-virtual {p3, v4, v1}, Landroid/os/Parcel;->writeTypedObject(Landroid/os/Parcelable;I)V
/* .line 104 */
/* nop */
/* .line 120 */
} // .end local v2 # "_arg0":I
} // .end local v3 # "_arg1":[B
} // .end local v4 # "_result":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
} // :goto_0
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
