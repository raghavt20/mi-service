public abstract class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback implements android.os.IInterface {
	 /* .source "IMiFxTunnelCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;, */
	 /* Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # direct methods */
static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback ( ) {
	 /* .locals 3 */
	 /* .line 185 */
	 /* const/16 v0, 0x24 */
	 /* const/16 v1, 0x2e */
	 /* const-string/jumbo v2, "vendor$xiaomi$hardware$fx$tunnel$IMiFxTunnelCallback" */
	 (( java.lang.String ) v2 ).replace ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
	 return;
} // .end method
/* # virtual methods */
public abstract java.lang.String getInterfaceHash ( ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void onDaemonMessage ( Long p0, Integer p1, Integer p2, Object[] p3 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
