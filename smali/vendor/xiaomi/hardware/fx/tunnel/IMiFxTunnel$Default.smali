.class public Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Default;
.super Ljava/lang/Object;
.source "IMiFxTunnel.java"

# interfaces
.implements Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Default"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInterfaceHash()Ljava/lang/String;
    .locals 1

    .line 31
    const-string v0, ""

    return-object v0
.end method

.method public getInterfaceVersion()I
    .locals 1

    .line 27
    const/4 v0, 0x0

    return v0
.end method

.method public invokeCommand(I[B)Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
    .locals 1
    .param p1, "cmdId"    # I
    .param p2, "param"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 20
    const/4 v0, 0x0

    return-object v0
.end method

.method public setNotify(Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;)V
    .locals 0
    .param p1, "callback"    # Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 24
    return-void
.end method
