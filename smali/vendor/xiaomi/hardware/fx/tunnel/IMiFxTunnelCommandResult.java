public class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult implements android.os.Parcelable {
	 /* .source "IMiFxTunnelCommandResult.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final android.os.Parcelable$Creator CREATOR;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/os/Parcelable$Creator<", */
	 /* "Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
public data;
public Integer errCode;
/* # direct methods */
static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult ( ) {
/* .locals 1 */
/* .line 11 */
/* new-instance v0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult$1; */
/* invoke-direct {v0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult$1;-><init>()V */
return;
} // .end method
public vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult ( ) {
/* .locals 1 */
/* .line 5 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 7 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
return;
} // .end method
/* # virtual methods */
public Integer describeContents ( ) {
/* .locals 1 */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
/* .line 54 */
/* .local v0, "_mask":I */
} // .end method
public final Integer getStability ( ) {
/* .locals 1 */
/* .line 10 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void readFromParcel ( android.os.Parcel p0 ) {
/* .locals 6 */
/* .param p1, "_aidl_parcel" # Landroid/os/Parcel; */
/* .line 36 */
v0 = (( android.os.Parcel ) p1 ).dataPosition ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I
/* .line 37 */
/* .local v0, "_aidl_start_pos":I */
v1 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* .line 39 */
/* .local v1, "_aidl_parcelable_size":I */
int v2 = 4; // const/4 v2, 0x4
final String v3 = "Overflow in the size of parcelable"; // const-string v3, "Overflow in the size of parcelable"
/* const v4, 0x7fffffff */
/* if-lt v1, v2, :cond_5 */
/* .line 40 */
try { // :try_start_0
	 v2 = 	 (( android.os.Parcel ) p1 ).dataPosition ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* sub-int/2addr v2, v0 */
	 /* if-lt v2, v1, :cond_1 */
	 /* .line 45 */
	 /* sub-int/2addr v4, v1 */
	 /* if-gt v0, v4, :cond_0 */
	 /* .line 48 */
	 /* add-int v2, v0, v1 */
	 (( android.os.Parcel ) p1 ).setDataPosition ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Parcel;->setDataPosition(I)V
	 /* .line 40 */
	 return;
	 /* .line 46 */
} // :cond_0
/* new-instance v2, Landroid/os/BadParcelableException; */
/* invoke-direct {v2, v3}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 41 */
} // :cond_1
try { // :try_start_1
v2 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* iput v2, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
/* .line 42 */
v2 = (( android.os.Parcel ) p1 ).dataPosition ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* sub-int/2addr v2, v0 */
/* if-lt v2, v1, :cond_3 */
/* .line 45 */
/* sub-int/2addr v4, v1 */
/* if-gt v0, v4, :cond_2 */
/* .line 48 */
/* add-int v2, v0, v1 */
(( android.os.Parcel ) p1 ).setDataPosition ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 42 */
return;
/* .line 46 */
} // :cond_2
/* new-instance v2, Landroid/os/BadParcelableException; */
/* invoke-direct {v2, v3}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 43 */
} // :cond_3
try { // :try_start_2
(( android.os.Parcel ) p1 ).createByteArray ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B
this.data = v2;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 45 */
/* sub-int/2addr v4, v1 */
/* if-gt v0, v4, :cond_4 */
/* .line 48 */
/* add-int v2, v0, v1 */
(( android.os.Parcel ) p1 ).setDataPosition ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 49 */
/* nop */
/* .line 50 */
return;
/* .line 46 */
} // :cond_4
/* new-instance v2, Landroid/os/BadParcelableException; */
/* invoke-direct {v2, v3}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 45 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 39 */
} // :cond_5
try { // :try_start_3
/* new-instance v2, Landroid/os/BadParcelableException; */
final String v5 = "Parcelable too small"; // const-string v5, "Parcelable too small"
/* invoke-direct {v2, v5}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_aidl_start_pos":I
} // .end local v1 # "_aidl_parcelable_size":I
} // .end local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;
} // .end local p1 # "_aidl_parcel":Landroid/os/Parcel;
/* throw v2 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 45 */
/* .restart local v0 # "_aidl_start_pos":I */
/* .restart local v1 # "_aidl_parcelable_size":I */
/* .restart local p0 # "this":Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult; */
/* .restart local p1 # "_aidl_parcel":Landroid/os/Parcel; */
} // :goto_0
/* sub-int/2addr v4, v1 */
/* if-le v0, v4, :cond_6 */
/* .line 46 */
/* new-instance v2, Landroid/os/BadParcelableException; */
/* invoke-direct {v2, v3}, Landroid/os/BadParcelableException;-><init>(Ljava/lang/String;)V */
/* throw v2 */
/* .line 48 */
} // :cond_6
/* add-int v3, v0, v1 */
(( android.os.Parcel ) p1 ).setDataPosition ( v3 ); // invoke-virtual {p1, v3}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 49 */
/* throw v2 */
} // .end method
public final void writeToParcel ( android.os.Parcel p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "_aidl_parcel" # Landroid/os/Parcel; */
/* .param p2, "_aidl_flag" # I */
/* .line 25 */
v0 = (( android.os.Parcel ) p1 ).dataPosition ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I
/* .line 26 */
/* .local v0, "_aidl_start_pos":I */
int v1 = 0; // const/4 v1, 0x0
(( android.os.Parcel ) p1 ).writeInt ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 27 */
/* iget v1, p0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCommandResult;->errCode:I */
(( android.os.Parcel ) p1 ).writeInt ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 28 */
v1 = this.data;
(( android.os.Parcel ) p1 ).writeByteArray ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V
/* .line 29 */
v1 = (( android.os.Parcel ) p1 ).dataPosition ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I
/* .line 30 */
/* .local v1, "_aidl_end_pos":I */
(( android.os.Parcel ) p1 ).setDataPosition ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 31 */
/* sub-int v2, v1, v0 */
(( android.os.Parcel ) p1 ).writeInt ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 32 */
(( android.os.Parcel ) p1 ).setDataPosition ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 33 */
return;
} // .end method
