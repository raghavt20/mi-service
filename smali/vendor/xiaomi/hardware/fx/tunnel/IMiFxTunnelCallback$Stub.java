public abstract class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub extends android.os.Binder implements vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback {
	 /* .source "IMiFxTunnelCallback.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
static final Integer TRANSACTION_onDaemonMessage;
/* # direct methods */
public vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub ( ) {
/* .locals 1 */
/* .line 39 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 40 */
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;->markVintfStability()V
/* .line 41 */
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub.DESCRIPTOR;
(( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 42 */
return;
} // .end method
public static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 49 */
/* if-nez p0, :cond_0 */
/* .line 50 */
int v0 = 0; // const/4 v0, 0x0
/* .line 52 */
} // :cond_0
v0 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub.DESCRIPTOR;
/* .line 53 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 54 */
/* move-object v1, v0 */
/* check-cast v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback; */
/* .line 56 */
} // :cond_1
/* new-instance v1, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 60 */
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 16 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 64 */
/* move/from16 v0, p1 */
/* move-object/from16 v1, p3 */
v2 = vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback$Stub.DESCRIPTOR;
/* .line 65 */
/* .local v2, "descriptor":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
/* if-lt v0, v3, :cond_0 */
/* const v4, 0xffffff */
/* if-gt v0, v4, :cond_0 */
/* .line 66 */
/* move-object/from16 v4, p2 */
(( android.os.Parcel ) v4 ).enforceInterface ( v2 ); // invoke-virtual {v4, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 65 */
} // :cond_0
/* move-object/from16 v4, p2 */
/* .line 68 */
} // :goto_0
/* sparse-switch v0, :sswitch_data_0 */
/* .line 88 */
/* packed-switch v0, :pswitch_data_0 */
/* .line 106 */
v3 = /* invoke-super/range {p0 ..p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 72 */
/* :sswitch_0 */
(( android.os.Parcel ) v1 ).writeString ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 73 */
/* .line 77 */
/* :sswitch_1 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 78 */
v5 = /* invoke-virtual/range {p0 ..p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;->getInterfaceVersion()I */
(( android.os.Parcel ) v1 ).writeInt ( v5 ); // invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 79 */
/* .line 83 */
/* :sswitch_2 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 84 */
/* invoke-virtual/range {p0 ..p0}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;->getInterfaceHash()Ljava/lang/String; */
(( android.os.Parcel ) v1 ).writeString ( v5 ); // invoke-virtual {v1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 85 */
/* .line 93 */
/* :pswitch_0 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readLong()J */
/* move-result-wide v12 */
/* .line 95 */
/* .local v12, "_arg0":J */
v5 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 97 */
/* .local v5, "_arg1":I */
v14 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 99 */
/* .local v14, "_arg2":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createByteArray()[B */
/* .line 100 */
/* .local v15, "_arg3":[B */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 101 */
/* move-object/from16 v6, p0 */
/* move-wide v7, v12 */
/* move v9, v5 */
/* move v10, v14 */
/* move-object v11, v15 */
/* invoke-virtual/range {v6 ..v11}, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;->onDaemonMessage(JII[B)V */
/* .line 102 */
/* nop */
/* .line 109 */
} // .end local v5 # "_arg1":I
} // .end local v12 # "_arg0":J
} // .end local v14 # "_arg2":I
} // .end local v15 # "_arg3":[B
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
