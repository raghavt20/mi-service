public abstract class vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel implements android.os.IInterface {
	 /* .source "IMiFxTunnel.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Stub;, */
	 /* Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnel$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # direct methods */
static vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnel ( ) {
	 /* .locals 3 */
	 /* .line 219 */
	 /* const/16 v0, 0x24 */
	 /* const/16 v1, 0x2e */
	 /* const-string/jumbo v2, "vendor$xiaomi$hardware$fx$tunnel$IMiFxTunnel" */
	 (( java.lang.String ) v2 ).replace ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
	 return;
} // .end method
/* # virtual methods */
public abstract java.lang.String getInterfaceHash ( ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCommandResult invokeCommand ( Integer p0, Object[] p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setNotify ( vendor.xiaomi.hardware.fx.tunnel.IMiFxTunnelCallback p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
