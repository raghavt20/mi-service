.class public interface abstract Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;
.super Ljava/lang/Object;
.source "IMiFxTunnelCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Stub;,
        Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback$Default;
    }
.end annotation


# static fields
.field public static final DESCRIPTOR:Ljava/lang/String;

.field public static final HASH:Ljava/lang/String; = "cac0cec9bbd7ce7545b32873c89cd67844627700"

.field public static final VERSION:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 185
    const/16 v0, 0x24

    const/16 v1, 0x2e

    const-string/jumbo v2, "vendor$xiaomi$hardware$fx$tunnel$IMiFxTunnelCallback"

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lvendor/xiaomi/hardware/fx/tunnel/IMiFxTunnelCallback;->DESCRIPTOR:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract getInterfaceHash()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getInterfaceVersion()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onDaemonMessage(JII[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
