public class inal implements vendor.xiaomi.hidl.minet.V1_0.IMiNetService {
	 /* .source "IMiNetService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x19 */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private android.os.IHwBinder mRemote;
/* # direct methods */
public inal ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IHwBinder; */
/* .line 213 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 214 */
java.util.Objects .requireNonNull ( p1 );
/* check-cast v0, Landroid/os/IHwBinder; */
this.mRemote = v0;
/* .line 215 */
return;
} // .end method
/* # virtual methods */
public android.os.IHwBinder asBinder ( ) {
/* .locals 1 */
/* .line 219 */
v0 = this.mRemote;
} // .end method
public void debug ( android.os.NativeHandle p0, java.util.ArrayList p1 ) {
/* .locals 5 */
/* .param p1, "fd" # Landroid/os/NativeHandle; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/os/NativeHandle;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 385 */
/* .local p2, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 386 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 387 */
(( android.os.HwParcel ) v0 ).writeNativeHandle ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeNativeHandle(Landroid/os/NativeHandle;)V
/* .line 388 */
(( android.os.HwParcel ) v0 ).writeStringVector ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeStringVector(Ljava/util/ArrayList;)V
/* .line 390 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 392 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf444247 */
int v4 = 0; // const/4 v4, 0x0
/* .line 393 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 394 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 396 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 397 */
/* nop */
/* .line 398 */
return;
/* .line 396 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 397 */
/* throw v2 */
} // .end method
public final Boolean equals ( java.lang.Object p0 ) {
/* .locals 1 */
/* .param p1, "other" # Ljava/lang/Object; */
/* .line 234 */
v0 = android.os.HidlSupport .interfacesEqual ( p0,p1 );
} // .end method
public miui.android.services.internal.hidl.base.V1_0.DebugInfo getDebugInfo ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 497 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 498 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 500 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 502 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf524546 */
int v4 = 0; // const/4 v4, 0x0
/* .line 503 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 504 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 506 */
/* new-instance v2, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
/* invoke-direct {v2}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;-><init>()V */
/* .line 507 */
/* .local v2, "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo; */
(( miui.android.services.internal.hidl.base.V1_0.DebugInfo ) v2 ).readFromParcel ( v1 ); // invoke-virtual {v2, v1}, Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;->readFromParcel(Landroid/os/HwParcel;)V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 508 */
/* nop */
/* .line 510 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 508 */
/* .line 510 */
} // .end local v2 # "_hidl_out_info":Lmiui/android/services/internal/hidl/base/V1_0/DebugInfo;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 511 */
/* throw v2 */
} // .end method
public java.util.ArrayList getHashChain ( ) {
/* .locals 13 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "[B>;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 422 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 423 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 425 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 427 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf485348 */
int v4 = 0; // const/4 v4, 0x0
/* .line 428 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 429 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 431 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v10, v2 */
/* .line 433 */
/* .local v10, "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;" */
/* const-wide/16 v2, 0x10 */
(( android.os.HwParcel ) v1 ).readBuffer ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/os/HwParcel;->readBuffer(J)Landroid/os/HwBlob;
/* move-object v11, v2 */
/* .line 435 */
/* .local v11, "_hidl_blob":Landroid/os/HwBlob; */
/* const-wide/16 v2, 0x8 */
v2 = (( android.os.HwBlob ) v11 ).getInt32 ( v2, v3 ); // invoke-virtual {v11, v2, v3}, Landroid/os/HwBlob;->getInt32(J)I
/* move v12, v2 */
/* .line 436 */
/* .local v12, "_hidl_vec_size":I */
/* mul-int/lit8 v2, v12, 0x20 */
/* int-to-long v3, v2 */
/* .line 437 */
(( android.os.HwBlob ) v11 ).handle ( ); // invoke-virtual {v11}, Landroid/os/HwBlob;->handle()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x0 */
int v9 = 1; // const/4 v9, 0x1
/* .line 436 */
/* move-object v2, v1 */
/* invoke-virtual/range {v2 ..v9}, Landroid/os/HwParcel;->readEmbeddedBuffer(JJJZ)Landroid/os/HwBlob; */
/* .line 440 */
/* .local v2, "childBlob":Landroid/os/HwBlob; */
(( java.util.ArrayList ) v10 ).clear ( ); // invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V
/* .line 441 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "_hidl_index_0":I */
} // :goto_0
/* if-ge v3, v12, :cond_0 */
/* .line 442 */
/* const/16 v4, 0x20 */
/* new-array v5, v4, [B */
/* .line 444 */
/* .local v5, "_hidl_vec_element":[B */
/* mul-int/lit8 v6, v3, 0x20 */
/* int-to-long v6, v6 */
/* .line 445 */
/* .local v6, "_hidl_array_offset_1":J */
(( android.os.HwBlob ) v2 ).copyToInt8Array ( v6, v7, v5, v4 ); // invoke-virtual {v2, v6, v7, v5, v4}, Landroid/os/HwBlob;->copyToInt8Array(J[BI)V
/* .line 446 */
/* nop */
/* .line 448 */
} // .end local v6 # "_hidl_array_offset_1":J
(( java.util.ArrayList ) v10 ).add ( v5 ); // invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 441 */
/* nop */
} // .end local v5 # "_hidl_vec_element":[B
/* add-int/lit8 v3, v3, 0x1 */
/* .line 452 */
} // .end local v2 # "childBlob":Landroid/os/HwBlob;
} // .end local v3 # "_hidl_index_0":I
} // .end local v11 # "_hidl_blob":Landroid/os/HwBlob;
} // .end local v12 # "_hidl_vec_size":I
} // :cond_0
/* nop */
/* .line 454 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 452 */
/* .line 454 */
} // .end local v10 # "_hidl_out_hashchain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 455 */
/* throw v2 */
} // .end method
public final Integer hashCode ( ) {
/* .locals 1 */
/* .line 239 */
(( vendor.xiaomi.hidl.minet.V1_0.IMiNetService$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService$Proxy;->asBinder()Landroid/os/IHwBinder;
v0 = (( java.lang.Object ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I
} // .end method
public java.util.ArrayList interfaceChain ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 366 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 367 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 369 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 371 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf43484e */
int v4 = 0; // const/4 v4, 0x0
/* .line 372 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 373 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 375 */
(( android.os.HwParcel ) v1 ).readStringVector ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readStringVector()Ljava/util/ArrayList;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 376 */
/* .local v2, "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* nop */
/* .line 378 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 376 */
/* .line 378 */
} // .end local v2 # "_hidl_out_descriptors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 379 */
/* throw v2 */
} // .end method
public java.lang.String interfaceDescriptor ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 403 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 404 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 406 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 408 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf445343 */
int v4 = 0; // const/4 v4, 0x0
/* .line 409 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 410 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 412 */
(( android.os.HwParcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readString()Ljava/lang/String;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 413 */
/* .local v2, "_hidl_out_descriptor":Ljava/lang/String; */
/* nop */
/* .line 415 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 413 */
/* .line 415 */
} // .end local v2 # "_hidl_out_descriptor":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 416 */
/* throw v2 */
} // .end method
public Boolean linkToDeath ( android.os.IHwBinder$DeathRecipient p0, Long p1 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .param p2, "cookie" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 476 */
v0 = v0 = this.mRemote;
} // .end method
public void notifySyspropsChanged ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 517 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 518 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 520 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 522 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf535953 */
int v4 = 1; // const/4 v4, 0x1
/* .line 523 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 525 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 526 */
/* nop */
/* .line 527 */
return;
/* .line 525 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 526 */
/* throw v2 */
} // .end method
public void ping ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 481 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 482 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 484 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 486 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf504e47 */
int v4 = 0; // const/4 v4, 0x0
/* .line 487 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 488 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 490 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 491 */
/* nop */
/* .line 492 */
return;
/* .line 490 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 491 */
/* throw v2 */
} // .end method
public Boolean registerCallback ( vendor.xiaomi.hidl.minet.V1_0.IMiNetCallback p0 ) {
/* .locals 5 */
/* .param p1, "callback" # Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 325 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 326 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hidl.minet@1.0::IMiNetService" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 327 */
/* if-nez p1, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
} // :goto_0
(( android.os.HwParcel ) v0 ).writeStrongBinder ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 329 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 331 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
int v4 = 0; // const/4 v4, 0x0
/* .line 332 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 333 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 335 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 336 */
/* .local v2, "_hidl_out_result":Z */
/* nop */
/* .line 338 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 336 */
/* .line 338 */
} // .end local v2 # "_hidl_out_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 339 */
/* throw v2 */
} // .end method
public Boolean setAppUidList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "uidList" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 305 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 306 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hidl.minet@1.0::IMiNetService" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 307 */
(( android.os.HwParcel ) v0 ).writeString ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 309 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 311 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
int v4 = 0; // const/4 v4, 0x0
/* .line 312 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 313 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 315 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 316 */
/* .local v2, "_hidl_out_result":Z */
/* nop */
/* .line 318 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 316 */
/* .line 318 */
} // .end local v2 # "_hidl_out_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 319 */
/* throw v2 */
} // .end method
public Boolean setCommon ( Integer p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p1, "cmd" # I */
/* .param p2, "attr" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 284 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 285 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hidl.minet@1.0::IMiNetService" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 286 */
(( android.os.HwParcel ) v0 ).writeInt32 ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/HwParcel;->writeInt32(I)V
/* .line 287 */
(( android.os.HwParcel ) v0 ).writeString ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/HwParcel;->writeString(Ljava/lang/String;)V
/* .line 289 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 291 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
int v4 = 0; // const/4 v4, 0x0
/* .line 292 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 293 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 295 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 296 */
/* .local v2, "_hidl_out_result":Z */
/* nop */
/* .line 298 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 296 */
/* .line 298 */
} // .end local v2 # "_hidl_out_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 299 */
/* throw v2 */
} // .end method
public void setHALInstrumentation ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 461 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 462 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
final String v1 = "android.hidl.base@1.0::IBase"; // const-string v1, "android.hidl.base@1.0::IBase"
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 464 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 466 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
/* const v3, 0xf494e54 */
int v4 = 1; // const/4 v4, 0x1
/* .line 467 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 469 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 470 */
/* nop */
/* .line 471 */
return;
/* .line 469 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 470 */
/* throw v2 */
} // .end method
public Boolean startMiNetd ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 246 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 247 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hidl.minet@1.0::IMiNetService" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 249 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 251 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* .line 252 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 253 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 255 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 256 */
/* .local v2, "_hidl_out_result":Z */
/* nop */
/* .line 258 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 256 */
/* .line 258 */
} // .end local v2 # "_hidl_out_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 259 */
/* throw v2 */
} // .end method
public Boolean stopMiNetd ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 265 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 266 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hidl.minet@1.0::IMiNetService" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 268 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 270 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
/* .line 271 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 272 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 274 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 275 */
/* .local v2, "_hidl_out_result":Z */
/* nop */
/* .line 277 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 275 */
/* .line 277 */
} // .end local v2 # "_hidl_out_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 278 */
/* throw v2 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 225 */
try { // :try_start_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( vendor.xiaomi.hidl.minet.V1_0.IMiNetService$Proxy ) p0 ).interfaceDescriptor ( ); // invoke-virtual {p0}, Lvendor/xiaomi/hidl/minet/V1_0/IMiNetService$Proxy;->interfaceDescriptor()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "@Proxy"; // const-string v1, "@Proxy"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 226 */
/* :catch_0 */
/* move-exception v0 */
/* .line 229 */
final String v0 = "[class or subclass of vendor.xiaomi.hidl.minet@1.0::IMiNetService]@Proxy"; // const-string v0, "[class or subclass of vendor.xiaomi.hidl.minet@1.0::IMiNetService]@Proxy"
} // .end method
public Boolean unlinkToDeath ( android.os.IHwBinder$DeathRecipient p0 ) {
/* .locals 1 */
/* .param p1, "recipient" # Landroid/os/IHwBinder$DeathRecipient; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 532 */
v0 = v0 = this.mRemote;
} // .end method
public Boolean unregisterCallback ( vendor.xiaomi.hidl.minet.V1_0.IMiNetCallback p0 ) {
/* .locals 5 */
/* .param p1, "callback" # Lvendor/xiaomi/hidl/minet/V1_0/IMiNetCallback; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 345 */
/* new-instance v0, Landroid/os/HwParcel; */
/* invoke-direct {v0}, Landroid/os/HwParcel;-><init>()V */
/* .line 346 */
/* .local v0, "_hidl_request":Landroid/os/HwParcel; */
/* const-string/jumbo v1, "vendor.xiaomi.hidl.minet@1.0::IMiNetService" */
(( android.os.HwParcel ) v0 ).writeInterfaceToken ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 347 */
/* if-nez p1, :cond_0 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_0
} // :goto_0
(( android.os.HwParcel ) v0 ).writeStrongBinder ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/HwParcel;->writeStrongBinder(Landroid/os/IHwBinder;)V
/* .line 349 */
/* new-instance v1, Landroid/os/HwParcel; */
/* invoke-direct {v1}, Landroid/os/HwParcel;-><init>()V */
/* .line 351 */
/* .local v1, "_hidl_reply":Landroid/os/HwParcel; */
try { // :try_start_0
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
int v4 = 0; // const/4 v4, 0x0
/* .line 352 */
(( android.os.HwParcel ) v1 ).verifySuccess ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->verifySuccess()V
/* .line 353 */
(( android.os.HwParcel ) v0 ).releaseTemporaryStorage ( ); // invoke-virtual {v0}, Landroid/os/HwParcel;->releaseTemporaryStorage()V
/* .line 355 */
v2 = (( android.os.HwParcel ) v1 ).readBool ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->readBool()Z
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 356 */
/* .local v2, "_hidl_out_result":Z */
/* nop */
/* .line 358 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 356 */
/* .line 358 */
} // .end local v2 # "_hidl_out_result":Z
/* :catchall_0 */
/* move-exception v2 */
(( android.os.HwParcel ) v1 ).release ( ); // invoke-virtual {v1}, Landroid/os/HwParcel;->release()V
/* .line 359 */
/* throw v2 */
} // .end method
