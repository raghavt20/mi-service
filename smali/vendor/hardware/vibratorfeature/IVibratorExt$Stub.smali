.class public abstract Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;
.super Landroid/os/Binder;
.source "IVibratorExt.java"

# interfaces
.implements Lvendor/hardware/vibratorfeature/IVibratorExt;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvendor/hardware/vibratorfeature/IVibratorExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
    }
.end annotation


# static fields
.field static final TRANSACTION_configStrengthForEffect:I = 0x6

.field static final TRANSACTION_getInterfaceHash:I = 0xfffffe

.field static final TRANSACTION_getInterfaceVersion:I = 0xffffff

.field static final TRANSACTION_pause:I = 0x2

.field static final TRANSACTION_play:I = 0x1

.field static final TRANSACTION_seekTo:I = 0x4

.field static final TRANSACTION_setAmplitudeExt:I = 0x5

.field static final TRANSACTION_setUsageExt:I = 0x7

.field static final TRANSACTION_stop:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 57
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 58
    invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->markVintfStability()V

    .line 59
    sget-object v0, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->DESCRIPTOR:Ljava/lang/String;

    invoke-virtual {p0, p0, v0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lvendor/hardware/vibratorfeature/IVibratorExt;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .line 67
    if-nez p0, :cond_0

    .line 68
    const/4 v0, 0x0

    return-object v0

    .line 70
    :cond_0
    sget-object v0, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->DESCRIPTOR:Ljava/lang/String;

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 71
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lvendor/hardware/vibratorfeature/IVibratorExt;

    if-eqz v1, :cond_1

    .line 72
    move-object v1, v0

    check-cast v1, Lvendor/hardware/vibratorfeature/IVibratorExt;

    return-object v1

    .line 74
    :cond_1
    new-instance v1, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;

    invoke-direct {v1, p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    return-object v1
.end method

.method public static getDefaultTransactionName(I)Ljava/lang/String;
    .locals 1
    .param p0, "transactionCode"    # I

    .line 83
    sparse-switch p0, :sswitch_data_0

    .line 123
    const/4 v0, 0x0

    return-object v0

    .line 115
    :sswitch_0
    const-string v0, "getInterfaceVersion"

    return-object v0

    .line 119
    :sswitch_1
    const-string v0, "getInterfaceHash"

    return-object v0

    .line 111
    :sswitch_2
    const-string/jumbo v0, "setUsageExt"

    return-object v0

    .line 107
    :sswitch_3
    const-string v0, "configStrengthForEffect"

    return-object v0

    .line 103
    :sswitch_4
    const-string/jumbo v0, "setAmplitudeExt"

    return-object v0

    .line 99
    :sswitch_5
    const-string/jumbo v0, "seekTo"

    return-object v0

    .line 95
    :sswitch_6
    const-string/jumbo v0, "stop"

    return-object v0

    .line 91
    :sswitch_7
    const-string v0, "pause"

    return-object v0

    .line 87
    :sswitch_8
    const-string v0, "play"

    return-object v0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_7
        0x3 -> :sswitch_6
        0x4 -> :sswitch_5
        0x5 -> :sswitch_4
        0x6 -> :sswitch_3
        0x7 -> :sswitch_2
        0xfffffe -> :sswitch_1
        0xffffff -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .line 78
    return-object p0
.end method

.method public getMaxTransactionId()I
    .locals 1

    .line 427
    const v0, 0xfffffe

    return v0
.end method

.method public getTransactionName(I)Ljava/lang/String;
    .locals 1
    .param p1, "transactionCode"    # I

    .line 130
    invoke-static {p1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->getDefaultTransactionName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 17
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 134
    move-object/from16 v6, p0

    move/from16 v7, p1

    move-object/from16 v8, p3

    sget-object v9, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->DESCRIPTOR:Ljava/lang/String;

    .line 135
    .local v9, "descriptor":Ljava/lang/String;
    const/4 v10, 0x1

    if-lt v7, v10, :cond_0

    const v0, 0xffffff

    if-gt v7, v0, :cond_0

    .line 136
    move-object/from16 v11, p2

    invoke-virtual {v11, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_0
    move-object/from16 v11, p2

    .line 138
    :goto_0
    sparse-switch v7, :sswitch_data_0

    .line 158
    packed-switch v7, :pswitch_data_0

    .line 229
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0

    .line 142
    :sswitch_0
    invoke-virtual {v8, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    return v10

    .line 147
    :sswitch_1
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    invoke-virtual/range {p0 .. p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->getInterfaceVersion()I

    move-result v0

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    return v10

    .line 153
    :sswitch_2
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 154
    invoke-virtual/range {p0 .. p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->getInterfaceHash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 155
    return v10

    .line 221
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 222
    .local v0, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 223
    invoke-virtual {v6, v0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->setUsageExt(I)V

    .line 224
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 225
    goto :goto_1

    .line 210
    .end local v0    # "_arg0":I
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 212
    .restart local v0    # "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 213
    .local v1, "_arg1":F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 214
    invoke-virtual {v6, v0, v1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->configStrengthForEffect(IF)V

    .line 215
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 216
    goto :goto_1

    .line 199
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":F
    :pswitch_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    .line 201
    .local v0, "_arg0":F
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 202
    .local v1, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 203
    invoke-virtual {v6, v0, v1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->setAmplitudeExt(FI)V

    .line 204
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 205
    goto :goto_1

    .line 190
    .end local v0    # "_arg0":F
    .end local v1    # "_arg1":I
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 191
    .local v0, "_arg0":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 192
    invoke-virtual {v6, v0, v1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->seekTo(J)V

    .line 193
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    goto :goto_1

    .line 183
    .end local v0    # "_arg0":J
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->stop()V

    .line 184
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 185
    goto :goto_1

    .line 177
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->pause()V

    .line 178
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 179
    goto :goto_1

    .line 163
    :pswitch_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v12

    .line 165
    .local v12, "_arg0":[I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v13

    .line 167
    .local v13, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v14

    .line 169
    .local v14, "_arg2":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v16

    .line 170
    .local v16, "_arg3":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->enforceNoDataAvail()V

    .line 171
    move-object/from16 v0, p0

    move-object v1, v12

    move v2, v13

    move-wide v3, v14

    move/from16 v5, v16

    invoke-virtual/range {v0 .. v5}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->play([IIJI)V

    .line 172
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 173
    nop

    .line 232
    .end local v12    # "_arg0":[I
    .end local v13    # "_arg1":I
    .end local v14    # "_arg2":J
    .end local v16    # "_arg3":I
    :goto_1
    return v10

    nop

    :sswitch_data_0
    .sparse-switch
        0xfffffe -> :sswitch_2
        0xffffff -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
