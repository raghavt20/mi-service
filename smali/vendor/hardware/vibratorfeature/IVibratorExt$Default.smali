.class public Lvendor/hardware/vibratorfeature/IVibratorExt$Default;
.super Ljava/lang/Object;
.source "IVibratorExt.java"

# interfaces
.implements Lvendor/hardware/vibratorfeature/IVibratorExt;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvendor/hardware/vibratorfeature/IVibratorExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Default"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public configStrengthForEffect(IF)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "strength"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 35
    return-void
.end method

.method public getInterfaceHash()Ljava/lang/String;
    .locals 1

    .line 45
    const-string v0, ""

    return-object v0
.end method

.method public getInterfaceVersion()I
    .locals 1

    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 23
    return-void
.end method

.method public play([IIJI)V
    .locals 0
    .param p1, "effect"    # [I
    .param p2, "loop"    # I
    .param p3, "interval"    # J
    .param p5, "amplitude"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 20
    return-void
.end method

.method public seekTo(J)V
    .locals 0
    .param p1, "time"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 29
    return-void
.end method

.method public setAmplitudeExt(FI)V
    .locals 0
    .param p1, "amplitude"    # F
    .param p2, "flag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 32
    return-void
.end method

.method public setUsageExt(I)V
    .locals 0
    .param p1, "usageExt"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 38
    return-void
.end method

.method public stop()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 26
    return-void
.end method
