class vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy implements vendor.hardware.vibratorfeature.IVibratorExt {
	 /* .source "IVibratorExt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/hardware/vibratorfeature/IVibratorExt$Stub; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "Proxy" */
} // .end annotation
/* # instance fields */
private java.lang.String mCachedHash;
private Integer mCachedVersion;
private android.os.IBinder mRemote;
/* # direct methods */
 vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ( ) {
/* .locals 1 */
/* .param p1, "remote" # Landroid/os/IBinder; */
/* .line 238 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 241 */
int v0 = -1; // const/4 v0, -0x1
/* iput v0, p0, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->mCachedVersion:I */
/* .line 242 */
final String v0 = "-1"; // const-string v0, "-1"
this.mCachedHash = v0;
/* .line 239 */
this.mRemote = p1;
/* .line 240 */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 245 */
v0 = this.mRemote;
} // .end method
public void configStrengthForEffect ( Integer p0, Float p1 ) {
/* .locals 5 */
/* .param p1, "id" # I */
/* .param p2, "strength" # F */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 345 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 346 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 348 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 349 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 350 */
(( android.os.Parcel ) v0 ).writeFloat ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 351 */
v2 = this.mRemote;
int v3 = 6; // const/4 v3, 0x6
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 352 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 355 */
	 (( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 358 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 359 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 360 */
/* nop */
/* .line 361 */
return;
/* .line 353 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method configStrengthForEffect is unimplemented."; // const-string v4, "Method configStrengthForEffect is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
} // .end local p1 # "id":I
} // .end local p2 # "strength":F
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 358 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* .restart local p1 # "id":I */
/* .restart local p2 # "strength":F */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 359 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 360 */
/* throw v2 */
} // .end method
public java.lang.String getInterfaceDescriptor ( ) {
/* .locals 1 */
/* .line 249 */
v0 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
} // .end method
public synchronized java.lang.String getInterfaceHash ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* monitor-enter p0 */
/* .line 399 */
try { // :try_start_0
final String v0 = "-1"; // const-string v0, "-1"
v1 = this.mCachedHash;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 400 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 401 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 403 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_1
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 404 */
v2 = this.mRemote;
/* const v3, 0xfffffe */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 405 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 406 */
(( android.os.Parcel ) v1 ).readString ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.mCachedHash = v3;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 408 */
} // .end local v2 # "_status":Z
try { // :try_start_2
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 409 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 410 */
/* .line 408 */
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 409 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 410 */
/* throw v2 */
/* .line 412 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
v0 = this.mCachedHash;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* monitor-exit p0 */
/* .line 398 */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 382 */
/* iget v0, p0, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->mCachedVersion:I */
int v1 = -1; // const/4 v1, -0x1
/* if-ne v0, v1, :cond_0 */
/* .line 383 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 384 */
/* .local v0, "data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 386 */
/* .local v1, "reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 387 */
v2 = this.mRemote;
/* const v3, 0xffffff */
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 388 */
/* .local v2, "_status":Z */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* .line 389 */
v3 = (( android.os.Parcel ) v1 ).readInt ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
/* iput v3, p0, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->mCachedVersion:I */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 391 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 392 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 393 */
/* .line 391 */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 392 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 393 */
/* throw v2 */
/* .line 395 */
} // .end local v0 # "data":Landroid/os/Parcel;
} // .end local v1 # "reply":Landroid/os/Parcel;
} // :cond_0
} // :goto_0
/* iget v0, p0, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->mCachedVersion:I */
} // .end method
public void pause ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 274 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 275 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 277 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 278 */
v2 = this.mRemote;
int v3 = 2; // const/4 v3, 0x2
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 279 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 282 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 285 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 286 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 287 */
/* nop */
/* .line 288 */
return;
/* .line 280 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method pause is unimplemented."; // const-string v4, "Method pause is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 285 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 286 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 287 */
/* throw v2 */
} // .end method
public void play ( Integer[] p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 5 */
/* .param p1, "effect" # [I */
/* .param p2, "loop" # I */
/* .param p3, "interval" # J */
/* .param p5, "amplitude" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 253 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 254 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 256 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 257 */
(( android.os.Parcel ) v0 ).writeIntArray ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V
/* .line 258 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 259 */
(( android.os.Parcel ) v0 ).writeLong ( p3, p4 ); // invoke-virtual {v0, p3, p4}, Landroid/os/Parcel;->writeLong(J)V
/* .line 260 */
(( android.os.Parcel ) v0 ).writeInt ( p5 ); // invoke-virtual {v0, p5}, Landroid/os/Parcel;->writeInt(I)V
/* .line 261 */
v2 = this.mRemote;
int v3 = 1; // const/4 v3, 0x1
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 262 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 265 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 268 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 269 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 270 */
/* nop */
/* .line 271 */
return;
/* .line 263 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method play is unimplemented."; // const-string v4, "Method play is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
} // .end local p1 # "effect":[I
} // .end local p2 # "loop":I
} // .end local p3 # "interval":J
} // .end local p5 # "amplitude":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 268 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* .restart local p1 # "effect":[I */
/* .restart local p2 # "loop":I */
/* .restart local p3 # "interval":J */
/* .restart local p5 # "amplitude":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 269 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 270 */
/* throw v2 */
} // .end method
public void seekTo ( Long p0 ) {
/* .locals 5 */
/* .param p1, "time" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 308 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 309 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 311 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 312 */
(( android.os.Parcel ) v0 ).writeLong ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/os/Parcel;->writeLong(J)V
/* .line 313 */
v2 = this.mRemote;
int v3 = 4; // const/4 v3, 0x4
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 314 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 317 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 320 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 321 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 322 */
/* nop */
/* .line 323 */
return;
/* .line 315 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method seekTo is unimplemented."; // const-string v4, "Method seekTo is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
} // .end local p1 # "time":J
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 320 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* .restart local p1 # "time":J */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 321 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 322 */
/* throw v2 */
} // .end method
public void setAmplitudeExt ( Float p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "amplitude" # F */
/* .param p2, "flag" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 326 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 327 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 329 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 330 */
(( android.os.Parcel ) v0 ).writeFloat ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeFloat(F)V
/* .line 331 */
(( android.os.Parcel ) v0 ).writeInt ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 332 */
v2 = this.mRemote;
int v3 = 5; // const/4 v3, 0x5
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 333 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 336 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 339 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 340 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 341 */
/* nop */
/* .line 342 */
return;
/* .line 334 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setAmplitudeExt is unimplemented."; // const-string v4, "Method setAmplitudeExt is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
} // .end local p1 # "amplitude":F
} // .end local p2 # "flag":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 339 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* .restart local p1 # "amplitude":F */
/* .restart local p2 # "flag":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 340 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 341 */
/* throw v2 */
} // .end method
public void setUsageExt ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "usageExt" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 364 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 365 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 367 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 368 */
(( android.os.Parcel ) v0 ).writeInt ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V
/* .line 369 */
v2 = this.mRemote;
int v3 = 7; // const/4 v3, 0x7
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 370 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 373 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 376 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 377 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 378 */
/* nop */
/* .line 379 */
return;
/* .line 371 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method setUsageExt is unimplemented."; // const-string v4, "Method setUsageExt is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
} // .end local p1 # "usageExt":I
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 376 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* .restart local p1 # "usageExt":I */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 377 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 378 */
/* throw v2 */
} // .end method
public void stop ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 291 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy ) p0 ).asBinder ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;->asBinder()Landroid/os/IBinder;
android.os.Parcel .obtain ( v0 );
/* .line 292 */
/* .local v0, "_data":Landroid/os/Parcel; */
android.os.Parcel .obtain ( );
/* .line 294 */
/* .local v1, "_reply":Landroid/os/Parcel; */
try { // :try_start_0
v2 = vendor.hardware.vibratorfeature.IVibratorExt$Stub$Proxy.DESCRIPTOR;
(( android.os.Parcel ) v0 ).writeInterfaceToken ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 295 */
v2 = this.mRemote;
int v3 = 3; // const/4 v3, 0x3
v2 = int v4 = 0; // const/4 v4, 0x0
/* .line 296 */
/* .local v2, "_status":Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 299 */
(( android.os.Parcel ) v1 ).readException ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 302 */
} // .end local v2 # "_status":Z
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 303 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 304 */
/* nop */
/* .line 305 */
return;
/* .line 297 */
/* .restart local v2 # "_status":Z */
} // :cond_0
try { // :try_start_1
/* new-instance v3, Landroid/os/RemoteException; */
final String v4 = "Method stop is unimplemented."; // const-string v4, "Method stop is unimplemented."
/* invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V */
} // .end local v0 # "_data":Landroid/os/Parcel;
} // .end local v1 # "_reply":Landroid/os/Parcel;
} // .end local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;
/* throw v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 302 */
} // .end local v2 # "_status":Z
/* .restart local v0 # "_data":Landroid/os/Parcel; */
/* .restart local v1 # "_reply":Landroid/os/Parcel; */
/* .restart local p0 # "this":Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* :catchall_0 */
/* move-exception v2 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 303 */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 304 */
/* throw v2 */
} // .end method
