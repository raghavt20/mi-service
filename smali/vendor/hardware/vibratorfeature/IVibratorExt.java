public abstract class vendor.hardware.vibratorfeature.IVibratorExt implements android.os.IInterface {
	 /* .source "IVibratorExt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;, */
	 /* Lvendor/hardware/vibratorfeature/IVibratorExt$Default; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String DESCRIPTOR;
public static final java.lang.String HASH;
public static final Integer VERSION;
/* # direct methods */
static vendor.hardware.vibratorfeature.IVibratorExt ( ) {
	 /* .locals 3 */
	 /* .line 430 */
	 /* const/16 v0, 0x24 */
	 /* const/16 v1, 0x2e */
	 /* const-string/jumbo v2, "vendor$hardware$vibratorfeature$IVibratorExt" */
	 (( java.lang.String ) v2 ).replace ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;
	 return;
} // .end method
/* # virtual methods */
public abstract void configStrengthForEffect ( Integer p0, Float p1 ) {
	 /* .annotation system Ldalvik/annotation/Throws; */
	 /* value = { */
	 /* Landroid/os/RemoteException; */
	 /* } */
} // .end annotation
} // .end method
public abstract java.lang.String getInterfaceHash ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Integer getInterfaceVersion ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void pause ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void play ( Integer[] p0, Integer p1, Long p2, Integer p3 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void seekTo ( Long p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setAmplitudeExt ( Float p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void setUsageExt ( Integer p0 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract void stop ( ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
