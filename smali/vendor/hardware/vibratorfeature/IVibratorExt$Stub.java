public abstract class vendor.hardware.vibratorfeature.IVibratorExt$Stub extends android.os.Binder implements vendor.hardware.vibratorfeature.IVibratorExt {
	 /* .source "IVibratorExt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/hardware/vibratorfeature/IVibratorExt; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x409 */
/* name = "Stub" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* } */
} // .end annotation
/* # static fields */
static final Integer TRANSACTION_configStrengthForEffect;
static final Integer TRANSACTION_getInterfaceHash;
static final Integer TRANSACTION_getInterfaceVersion;
static final Integer TRANSACTION_pause;
static final Integer TRANSACTION_play;
static final Integer TRANSACTION_seekTo;
static final Integer TRANSACTION_setAmplitudeExt;
static final Integer TRANSACTION_setUsageExt;
static final Integer TRANSACTION_stop;
/* # direct methods */
public vendor.hardware.vibratorfeature.IVibratorExt$Stub ( ) {
/* .locals 1 */
/* .line 57 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 58 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub ) p0 ).markVintfStability ( ); // invoke-virtual {p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->markVintfStability()V
/* .line 59 */
v0 = vendor.hardware.vibratorfeature.IVibratorExt$Stub.DESCRIPTOR;
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub ) p0 ).attachInterface ( p0, v0 ); // invoke-virtual {p0, p0, v0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
/* .line 60 */
return;
} // .end method
public static vendor.hardware.vibratorfeature.IVibratorExt asInterface ( android.os.IBinder p0 ) {
/* .locals 2 */
/* .param p0, "obj" # Landroid/os/IBinder; */
/* .line 67 */
/* if-nez p0, :cond_0 */
/* .line 68 */
int v0 = 0; // const/4 v0, 0x0
/* .line 70 */
} // :cond_0
v0 = vendor.hardware.vibratorfeature.IVibratorExt$Stub.DESCRIPTOR;
/* .line 71 */
/* .local v0, "iin":Landroid/os/IInterface; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* instance-of v1, v0, Lvendor/hardware/vibratorfeature/IVibratorExt; */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 72 */
/* move-object v1, v0 */
/* check-cast v1, Lvendor/hardware/vibratorfeature/IVibratorExt; */
/* .line 74 */
} // :cond_1
/* new-instance v1, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy; */
/* invoke-direct {v1, p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub$Proxy;-><init>(Landroid/os/IBinder;)V */
} // .end method
public static java.lang.String getDefaultTransactionName ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "transactionCode" # I */
/* .line 83 */
/* sparse-switch p0, :sswitch_data_0 */
/* .line 123 */
int v0 = 0; // const/4 v0, 0x0
/* .line 115 */
/* :sswitch_0 */
final String v0 = "getInterfaceVersion"; // const-string v0, "getInterfaceVersion"
/* .line 119 */
/* :sswitch_1 */
final String v0 = "getInterfaceHash"; // const-string v0, "getInterfaceHash"
/* .line 111 */
/* :sswitch_2 */
/* const-string/jumbo v0, "setUsageExt" */
/* .line 107 */
/* :sswitch_3 */
final String v0 = "configStrengthForEffect"; // const-string v0, "configStrengthForEffect"
/* .line 103 */
/* :sswitch_4 */
/* const-string/jumbo v0, "setAmplitudeExt" */
/* .line 99 */
/* :sswitch_5 */
/* const-string/jumbo v0, "seekTo" */
/* .line 95 */
/* :sswitch_6 */
/* const-string/jumbo v0, "stop" */
/* .line 91 */
/* :sswitch_7 */
final String v0 = "pause"; // const-string v0, "pause"
/* .line 87 */
/* :sswitch_8 */
final String v0 = "play"; // const-string v0, "play"
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x1 -> :sswitch_8 */
/* 0x2 -> :sswitch_7 */
/* 0x3 -> :sswitch_6 */
/* 0x4 -> :sswitch_5 */
/* 0x5 -> :sswitch_4 */
/* 0x6 -> :sswitch_3 */
/* 0x7 -> :sswitch_2 */
/* 0xfffffe -> :sswitch_1 */
/* 0xffffff -> :sswitch_0 */
} // .end sparse-switch
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 0 */
/* .line 78 */
} // .end method
public Integer getMaxTransactionId ( ) {
/* .locals 1 */
/* .line 427 */
/* const v0, 0xfffffe */
} // .end method
public java.lang.String getTransactionName ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "transactionCode" # I */
/* .line 130 */
vendor.hardware.vibratorfeature.IVibratorExt$Stub .getDefaultTransactionName ( p1 );
} // .end method
public Boolean onTransact ( Integer p0, android.os.Parcel p1, android.os.Parcel p2, Integer p3 ) {
/* .locals 17 */
/* .param p1, "code" # I */
/* .param p2, "data" # Landroid/os/Parcel; */
/* .param p3, "reply" # Landroid/os/Parcel; */
/* .param p4, "flags" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 134 */
/* move-object/from16 v6, p0 */
/* move/from16 v7, p1 */
/* move-object/from16 v8, p3 */
v9 = vendor.hardware.vibratorfeature.IVibratorExt$Stub.DESCRIPTOR;
/* .line 135 */
/* .local v9, "descriptor":Ljava/lang/String; */
int v10 = 1; // const/4 v10, 0x1
/* if-lt v7, v10, :cond_0 */
/* const v0, 0xffffff */
/* if-gt v7, v0, :cond_0 */
/* .line 136 */
/* move-object/from16 v11, p2 */
(( android.os.Parcel ) v11 ).enforceInterface ( v9 ); // invoke-virtual {v11, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V
/* .line 135 */
} // :cond_0
/* move-object/from16 v11, p2 */
/* .line 138 */
} // :goto_0
/* sparse-switch v7, :sswitch_data_0 */
/* .line 158 */
/* packed-switch v7, :pswitch_data_0 */
/* .line 229 */
v0 = /* invoke-super/range {p0 ..p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z */
/* .line 142 */
/* :sswitch_0 */
(( android.os.Parcel ) v8 ).writeString ( v9 ); // invoke-virtual {v8, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 143 */
/* .line 147 */
/* :sswitch_1 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 148 */
v0 = /* invoke-virtual/range {p0 ..p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->getInterfaceVersion()I */
(( android.os.Parcel ) v8 ).writeInt ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 149 */
/* .line 153 */
/* :sswitch_2 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 154 */
/* invoke-virtual/range {p0 ..p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->getInterfaceHash()Ljava/lang/String; */
(( android.os.Parcel ) v8 ).writeString ( v0 ); // invoke-virtual {v8, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 155 */
/* .line 221 */
/* :pswitch_0 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 222 */
/* .local v0, "_arg0":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 223 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub ) v6 ).setUsageExt ( v0 ); // invoke-virtual {v6, v0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->setUsageExt(I)V
/* .line 224 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 225 */
/* .line 210 */
} // .end local v0 # "_arg0":I
/* :pswitch_1 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 212 */
/* .restart local v0 # "_arg0":I */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readFloat()F */
/* .line 213 */
/* .local v1, "_arg1":F */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 214 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub ) v6 ).configStrengthForEffect ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->configStrengthForEffect(IF)V
/* .line 215 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 216 */
/* .line 199 */
} // .end local v0 # "_arg0":I
} // .end local v1 # "_arg1":F
/* :pswitch_2 */
v0 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readFloat()F */
/* .line 201 */
/* .local v0, "_arg0":F */
v1 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 202 */
/* .local v1, "_arg1":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 203 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub ) v6 ).setAmplitudeExt ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->setAmplitudeExt(FI)V
/* .line 204 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 205 */
/* .line 190 */
} // .end local v0 # "_arg0":F
} // .end local v1 # "_arg1":I
/* :pswitch_3 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readLong()J */
/* move-result-wide v0 */
/* .line 191 */
/* .local v0, "_arg0":J */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 192 */
(( vendor.hardware.vibratorfeature.IVibratorExt$Stub ) v6 ).seekTo ( v0, v1 ); // invoke-virtual {v6, v0, v1}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->seekTo(J)V
/* .line 193 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 194 */
/* .line 183 */
} // .end local v0 # "_arg0":J
/* :pswitch_4 */
/* invoke-virtual/range {p0 ..p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->stop()V */
/* .line 184 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 185 */
/* .line 177 */
/* :pswitch_5 */
/* invoke-virtual/range {p0 ..p0}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->pause()V */
/* .line 178 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 179 */
/* .line 163 */
/* :pswitch_6 */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->createIntArray()[I */
/* .line 165 */
/* .local v12, "_arg0":[I */
v13 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 167 */
/* .local v13, "_arg1":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readLong()J */
/* move-result-wide v14 */
/* .line 169 */
/* .local v14, "_arg2":J */
v16 = /* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->readInt()I */
/* .line 170 */
/* .local v16, "_arg3":I */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/Parcel;->enforceNoDataAvail()V */
/* .line 171 */
/* move-object/from16 v0, p0 */
/* move-object v1, v12 */
/* move v2, v13 */
/* move-wide v3, v14 */
/* move/from16 v5, v16 */
/* invoke-virtual/range {v0 ..v5}, Lvendor/hardware/vibratorfeature/IVibratorExt$Stub;->play([IIJI)V */
/* .line 172 */
/* invoke-virtual/range {p3 ..p3}, Landroid/os/Parcel;->writeNoException()V */
/* .line 173 */
/* nop */
/* .line 232 */
} // .end local v12 # "_arg0":[I
} // .end local v13 # "_arg1":I
} // .end local v14 # "_arg2":J
} // .end local v16 # "_arg3":I
} // :goto_1
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0xfffffe -> :sswitch_2 */
/* 0xffffff -> :sswitch_1 */
/* 0x5f4e5446 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
