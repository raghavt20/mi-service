public class vendor.hardware.vibratorfeature.IVibratorExt$Default implements vendor.hardware.vibratorfeature.IVibratorExt {
	 /* .source "IVibratorExt.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lvendor/hardware/vibratorfeature/IVibratorExt; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "Default" */
} // .end annotation
/* # direct methods */
public vendor.hardware.vibratorfeature.IVibratorExt$Default ( ) {
/* .locals 0 */
/* .line 16 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void configStrengthForEffect ( Integer p0, Float p1 ) {
/* .locals 0 */
/* .param p1, "id" # I */
/* .param p2, "strength" # F */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 35 */
return;
} // .end method
public java.lang.String getInterfaceHash ( ) {
/* .locals 1 */
/* .line 45 */
final String v0 = ""; // const-string v0, ""
} // .end method
public Integer getInterfaceVersion ( ) {
/* .locals 1 */
/* .line 41 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void pause ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 23 */
return;
} // .end method
public void play ( Integer[] p0, Integer p1, Long p2, Integer p3 ) {
/* .locals 0 */
/* .param p1, "effect" # [I */
/* .param p2, "loop" # I */
/* .param p3, "interval" # J */
/* .param p5, "amplitude" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 20 */
return;
} // .end method
public void seekTo ( Long p0 ) {
/* .locals 0 */
/* .param p1, "time" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 29 */
return;
} // .end method
public void setAmplitudeExt ( Float p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "amplitude" # F */
/* .param p2, "flag" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 32 */
return;
} // .end method
public void setUsageExt ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "usageExt" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 38 */
return;
} // .end method
public void stop ( ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 26 */
return;
} // .end method
