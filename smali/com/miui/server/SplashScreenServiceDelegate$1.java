class com.miui.server.SplashScreenServiceDelegate$1 extends android.content.BroadcastReceiver {
	 /* .source "SplashScreenServiceDelegate.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/SplashScreenServiceDelegate; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.SplashScreenServiceDelegate this$0; //synthetic
/* # direct methods */
 com.miui.server.SplashScreenServiceDelegate$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/SplashScreenServiceDelegate; */
/* .line 91 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 95 */
/* if-nez p2, :cond_0 */
/* .line 96 */
return;
/* .line 99 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 100 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 101 */
v1 = this.this$0;
final String v3 = "Boot completed, delay to bind SplashScreenService"; // const-string v3, "Boot completed, delay to bind SplashScreenService"
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v1,v3,v2 );
/* .line 102 */
v1 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mdelayToBindServiceAfterBootCompleted ( v1 );
/* .line 103 */
} // :cond_1
final String v1 = "miui.intent.action.ad.DEBUG_ON"; // const-string v1, "miui.intent.action.ad.DEBUG_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 104 */
v1 = this.this$0;
final String v3 = "Debug On"; // const-string v3, "Debug On"
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v1,v3,v2 );
/* .line 105 */
com.miui.server.SplashScreenServiceDelegate .-$$Nest$sfputsDebug ( v2 );
/* .line 106 */
} // :cond_2
final String v1 = "miui.intent.action.ad.DEBUG_OFF"; // const-string v1, "miui.intent.action.ad.DEBUG_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 107 */
v1 = this.this$0;
final String v3 = "Debug Off"; // const-string v3, "Debug Off"
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v1,v3,v2 );
/* .line 108 */
int v1 = 0; // const/4 v1, 0x0
com.miui.server.SplashScreenServiceDelegate .-$$Nest$sfputsDebug ( v1 );
/* .line 110 */
} // :cond_3
} // :goto_0
return;
} // .end method
