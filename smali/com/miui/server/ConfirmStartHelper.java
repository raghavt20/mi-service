public class com.miui.server.ConfirmStartHelper {
	 /* .source "ConfirmStartHelper.java" */
	 /* # static fields */
	 private static final java.lang.String CONFIRM_ACCESS_CONTROL_ACTIVITY_NAME;
	 private static final java.lang.String CONFIRM_START_ACTIVITY_ACTION;
	 private static final java.lang.String CONFIRM_START_ACTIVITY_NAME;
	 private static final java.lang.String PACKAGE_SECURITYCENTER;
	 /* # direct methods */
	 public com.miui.server.ConfirmStartHelper ( ) {
		 /* .locals 0 */
		 /* .line 8 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean isAllowStartCurrentActivity ( Integer p0, android.content.Intent p1 ) {
		 /* .locals 7 */
		 /* .param p0, "callerAppUid" # I */
		 /* .param p1, "intent" # Landroid/content/Intent; */
		 /* .line 27 */
		 /* const/16 v0, 0x3e8 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* if-eq p0, v0, :cond_5 */
		 if ( p1 != null) { // if-eqz p1, :cond_5
			 /* .line 28 */
			 final String v0 = "android.app.action.CHECK_ALLOW_START_ACTIVITY"; // const-string v0, "android.app.action.CHECK_ALLOW_START_ACTIVITY"
			 (( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
			 v0 = 			 (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 int v2 = 0; // const/4 v2, 0x0
			 /* if-nez v0, :cond_4 */
			 /* .line 29 */
			 (( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
			 final String v3 = "miui.intent.action.CHECK_ACCESS_CONTROL"; // const-string v3, "miui.intent.action.CHECK_ACCESS_CONTROL"
			 v0 = 			 (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 final String v3 = "android.intent.extra.INTENT"; // const-string v3, "android.intent.extra.INTENT"
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 30 */
				 (( android.content.Intent ) p1 ).getParcelableExtra ( v3 ); // invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* .line 34 */
				 } // :cond_0
				 (( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
				 if ( v0 != null) { // if-eqz v0, :cond_5
					 /* .line 35 */
					 (( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
					 /* .line 37 */
					 /* .local v0, "curComponent":Landroid/content/ComponentName; */
					 (( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
					 final String v5 = "com.miui.securitycenter"; // const-string v5, "com.miui.securitycenter"
					 v4 = 					 (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v4 != null) { // if-eqz v4, :cond_1
						 /* .line 38 */
						 (( android.content.ComponentName ) v0 ).getClassName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
						 final String v6 = "com.miui.wakepath.ui.ConfirmStartActivity"; // const-string v6, "com.miui.wakepath.ui.ConfirmStartActivity"
						 v4 = 						 (( java.lang.String ) v6 ).equals ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v4 != null) { // if-eqz v4, :cond_1
							 /* .line 39 */
							 /* .line 42 */
						 } // :cond_1
						 (( android.content.ComponentName ) v0 ).getPackageName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
						 v4 = 						 (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v4 != null) { // if-eqz v4, :cond_3
							 /* .line 43 */
							 (( android.content.ComponentName ) v0 ).getClassName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
							 final String v5 = "com.miui.applicationlock.ConfirmAccessControl"; // const-string v5, "com.miui.applicationlock.ConfirmAccessControl"
							 v4 = 							 (( java.lang.String ) v5 ).equals ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
							 if ( v4 != null) { // if-eqz v4, :cond_3
								 /* .line 44 */
								 (( android.content.Intent ) p1 ).getParcelableExtra ( v3 ); // invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
								 /* if-nez v3, :cond_2 */
							 } // :cond_2
							 /* move v1, v2 */
						 } // :cond_3
					 } // :goto_0
					 /* nop */
					 /* .line 42 */
				 } // :goto_1
				 /* .line 31 */
			 } // .end local v0 # "curComponent":Landroid/content/ComponentName;
		 } // :cond_4
	 } // :goto_2
	 /* .line 47 */
} // :cond_5
} // .end method
public static Boolean isAllowStartCurrentActivity ( android.content.Intent p0 ) {
/* .locals 6 */
/* .param p0, "intent" # Landroid/content/Intent; */
/* .line 51 */
int v0 = 1; // const/4 v0, 0x1
if ( p0 != null) { // if-eqz p0, :cond_4
	 /* .line 52 */
	 final String v1 = "android.app.action.CHECK_ALLOW_START_ACTIVITY"; // const-string v1, "android.app.action.CHECK_ALLOW_START_ACTIVITY"
	 (( android.content.Intent ) p0 ).getAction ( ); // invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 int v2 = 0; // const/4 v2, 0x0
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 53 */
		 /* .line 56 */
	 } // :cond_0
	 (( android.content.Intent ) p0 ).getComponent ( ); // invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
	 if ( v1 != null) { // if-eqz v1, :cond_4
		 /* .line 57 */
		 (( android.content.Intent ) p0 ).getComponent ( ); // invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
		 /* .line 59 */
		 /* .local v1, "curComponent":Landroid/content/ComponentName; */
		 (( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
		 final String v4 = "com.miui.securitycenter"; // const-string v4, "com.miui.securitycenter"
		 v3 = 		 (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v3 != null) { // if-eqz v3, :cond_1
			 /* .line 60 */
			 (( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
			 final String v5 = "com.miui.wakepath.ui.ConfirmStartActivity"; // const-string v5, "com.miui.wakepath.ui.ConfirmStartActivity"
			 v3 = 			 (( java.lang.String ) v5 ).equals ( v3 ); // invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_1
				 /* .line 61 */
				 /* .line 64 */
			 } // :cond_1
			 (( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
			 v3 = 			 (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v3 != null) { // if-eqz v3, :cond_3
				 /* .line 65 */
				 (( android.content.ComponentName ) v1 ).getClassName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
				 final String v4 = "com.miui.applicationlock.ConfirmAccessControl"; // const-string v4, "com.miui.applicationlock.ConfirmAccessControl"
				 v3 = 				 (( java.lang.String ) v4 ).equals ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v3 != null) { // if-eqz v3, :cond_3
					 /* .line 66 */
					 final String v3 = "android.intent.extra.INTENT"; // const-string v3, "android.intent.extra.INTENT"
					 (( android.content.Intent ) p0 ).getParcelableExtra ( v3 ); // invoke-virtual {p0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
					 /* if-nez v3, :cond_2 */
				 } // :cond_2
				 /* move v0, v2 */
			 } // :cond_3
		 } // :goto_0
		 /* nop */
		 /* .line 64 */
	 } // :goto_1
	 /* .line 69 */
} // .end local v1 # "curComponent":Landroid/content/ComponentName;
} // :cond_4
} // .end method
