.class public Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MiuiSwapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/MiuiSwapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsbStatusReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/MiuiSwapService;


# direct methods
.method public constructor <init>(Lcom/miui/server/MiuiSwapService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/MiuiSwapService;

    .line 270
    iput-object p1, p0, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;->this$0:Lcom/miui/server/MiuiSwapService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 274
    if-eqz p2, :cond_3

    sget-boolean v0, Lcom/miui/server/MiuiSwapService;->SWAP_DEBUG:Z

    if-eqz v0, :cond_0

    goto :goto_1

    .line 278
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "connected"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 281
    .local v1, "connected":Z
    if-eqz v1, :cond_1

    .line 282
    iget-object v2, p0, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;->this$0:Lcom/miui/server/MiuiSwapService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/miui/server/MiuiSwapService;->-$$Nest$fputmUsbConnect(Lcom/miui/server/MiuiSwapService;Z)V

    .line 283
    invoke-static {}, Lcom/miui/server/MiuiSwapService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Usb connect"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 285
    :cond_1
    iget-object v2, p0, Lcom/miui/server/MiuiSwapService$UsbStatusReceiver;->this$0:Lcom/miui/server/MiuiSwapService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/miui/server/MiuiSwapService;->-$$Nest$fputmUsbConnect(Lcom/miui/server/MiuiSwapService;Z)V

    .line 286
    invoke-static {}, Lcom/miui/server/MiuiSwapService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Usb disconnect"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    .end local v1    # "connected":Z
    :cond_2
    :goto_0
    return-void

    .line 275
    .end local v0    # "action":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-static {}, Lcom/miui/server/MiuiSwapService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "intent is null or debug"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    return-void
.end method
