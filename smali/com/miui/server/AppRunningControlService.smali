.class public Lcom/miui/server/AppRunningControlService;
.super Lmiui/security/IAppRunningControlManager$Stub;
.source "AppRunningControlService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AppRunningControlService"

.field private static final sNotDisallow:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAppsDisallowRunning:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDisallowRunningAppIntent:Landroid/content/Intent;

.field private mIsBlackListEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/AppRunningControlService;->sNotDisallow:Ljava/util/ArrayList;

    .line 24
    const-string v1, "com.lbe.security.miui"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    const-string v1, "com.android.updater"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    const-string v1, "com.xiaomi.market"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    const-string v1, "com.xiaomi.finddevice"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lmiui/security/IAppRunningControlManager$Stub;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/AppRunningControlService;->mAppsDisallowRunning:Ljava/util/List;

    .line 33
    iput-object p1, p0, Lcom/miui/server/AppRunningControlService;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method

.method private checkPermission()V
    .locals 3

    .line 117
    iget-object v0, p0, Lcom/miui/server/AppRunningControlService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.FORCE_STOP_PACKAGES"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 126
    return-void

    .line 119
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Permission Denial from pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 120
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 121
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " requires "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "msg":Ljava/lang/String;
    const-string v1, "AppRunningControlService"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private matchRuleInner(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "wakeType"    # I

    .line 98
    iget-boolean v0, p0, Lcom/miui/server/AppRunningControlService;->mIsBlackListEnable:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 99
    return v1

    .line 102
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 103
    return v1

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/miui/server/AppRunningControlService;->mAppsDisallowRunning:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getBlockActivityIntent(Ljava/lang/String;Landroid/content/Intent;ZI)Landroid/content/Intent;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "fromActivity"    # Z
    .param p4, "requestCode"    # I

    .line 65
    iget-boolean v0, p0, Lcom/miui/server/AppRunningControlService;->mIsBlackListEnable:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 66
    return-object v1

    .line 68
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    const-string v0, "AppRunningControlService"

    const-string v2, "getBlockActivityIntent packageName can\'t be null"

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-object v1

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/miui/server/AppRunningControlService;->mAppsDisallowRunning:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 73
    iget-object v0, p0, Lcom/miui/server/AppRunningControlService;->mDisallowRunningAppIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 74
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "packageName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    if-eqz p2, :cond_4

    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v2, 0x2000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 79
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 81
    :cond_2
    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 82
    if-eqz p3, :cond_3

    .line 84
    if-ltz p4, :cond_4

    .line 85
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 89
    :cond_3
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 92
    :cond_4
    :goto_0
    return-object v0

    .line 94
    .end local v0    # "result":Landroid/content/Intent;
    :cond_5
    return-object v1
.end method

.method public getNotDisallowList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 142
    sget-object v0, Lcom/miui/server/AppRunningControlService;->sNotDisallow:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isBlockActivity(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 109
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/server/AppRunningControlService;->mDisallowRunningAppIntent:Landroid/content/Intent;

    if-nez v0, :cond_0

    goto :goto_0

    .line 112
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/AppRunningControlService;->mDisallowRunningAppIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    return v1

    .line 110
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public matchRule(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "wakeType"    # I

    .line 137
    invoke-direct {p0, p1, p2}, Lcom/miui/server/AppRunningControlService;->matchRuleInner(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public setBlackListEnable(Z)V
    .locals 0
    .param p1, "isEnable"    # Z

    .line 59
    invoke-direct {p0}, Lcom/miui/server/AppRunningControlService;->checkPermission()V

    .line 60
    iput-boolean p1, p0, Lcom/miui/server/AppRunningControlService;->mIsBlackListEnable:Z

    .line 61
    return-void
.end method

.method public setDisallowRunningList(Ljava/util/List;Landroid/content/Intent;)V
    .locals 3
    .param p2, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .line 38
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/miui/server/AppRunningControlService;->checkPermission()V

    .line 39
    const-string v0, "AppRunningControlService"

    if-nez p2, :cond_0

    .line 40
    const-string/jumbo v1, "setDisallowRunningList intent can\'t be null"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    return-void

    .line 43
    :cond_0
    iput-object p2, p0, Lcom/miui/server/AppRunningControlService;->mDisallowRunningAppIntent:Landroid/content/Intent;

    .line 44
    iget-object v1, p0, Lcom/miui/server/AppRunningControlService;->mAppsDisallowRunning:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 45
    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 49
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 50
    .local v1, "pkgName":Ljava/lang/String;
    sget-object v2, Lcom/miui/server/AppRunningControlService;->sNotDisallow:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 51
    goto :goto_0

    .line 53
    :cond_2
    iget-object v2, p0, Lcom/miui/server/AppRunningControlService;->mAppsDisallowRunning:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    .end local v1    # "pkgName":Ljava/lang/String;
    goto :goto_0

    .line 55
    :cond_3
    return-void

    .line 46
    :cond_4
    :goto_1
    const-string/jumbo v1, "setDisallowRunningList clear list."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    return-void
.end method
