class com.miui.server.EnableStateManager$1 extends java.lang.Thread {
	 /* .source "EnableStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/EnableStateManager;->registerReceiverIfNeed()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # direct methods */
 com.miui.server.EnableStateManager$1 ( ) {
/* .locals 0 */
/* .line 124 */
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 127 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 128 */
/* .local v0, "filter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_ADDED"; // const-string v1, "android.intent.action.PACKAGE_ADDED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 129 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 130 */
com.miui.server.EnableStateManager .-$$Nest$sfgetmContext ( );
/* new-instance v2, Lcom/miui/server/EnableStateManager$PackageAddedReceiver; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, v3}, Lcom/miui/server/EnableStateManager$PackageAddedReceiver;-><init>(Lcom/miui/server/EnableStateManager$PackageAddedReceiver-IA;)V */
(( android.content.Context ) v1 ).registerReceiver ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 132 */
/* new-instance v1, Landroid/content/IntentFilter; */
/* invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V */
/* .line 133 */
/* .local v1, "otherFilter":Landroid/content/IntentFilter; */
final String v2 = "com.android.updater.action.COTA_CARRIER"; // const-string v2, "com.android.updater.action.COTA_CARRIER"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 134 */
final String v2 = "android.provision.action.PROVISION_COMPLETE"; // const-string v2, "android.provision.action.PROVISION_COMPLETE"
(( android.content.IntentFilter ) v1 ).addAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 135 */
com.miui.server.EnableStateManager .-$$Nest$sfgetmContext ( );
/* new-instance v4, Lcom/miui/server/EnableStateManager$OtherChangedReceiver; */
/* invoke-direct {v4, v3}, Lcom/miui/server/EnableStateManager$OtherChangedReceiver;-><init>(Lcom/miui/server/EnableStateManager$OtherChangedReceiver-IA;)V */
(( android.content.Context ) v2 ).registerReceiver ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 136 */
return;
} // .end method
