class com.miui.server.BackupManagerService$2 implements java.lang.Runnable {
	 /* .source "BackupManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/BackupManagerService;->startConfirmationUi(ILjava/lang/String;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.BackupManagerService this$0; //synthetic
final Integer val$token; //synthetic
/* # direct methods */
 com.miui.server.BackupManagerService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/BackupManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 290 */
this.this$0 = p1;
/* iput p2, p0, Lcom/miui/server/BackupManagerService$2;->val$token:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 8 */
/* .line 293 */
/* nop */
/* .line 294 */
final String v0 = "backup"; // const-string v0, "backup"
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Landroid/app/backup/IBackupManager; */
/* .line 296 */
/* .local v0, "bm":Landroid/app/backup/IBackupManager; */
try { // :try_start_0
/* iget v2, p0, Lcom/miui/server/BackupManagerService$2;->val$token:I */
int v3 = 1; // const/4 v3, 0x1
final String v4 = ""; // const-string v4, ""
v1 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmPwd ( v1 );
/* new-instance v6, Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver; */
v1 = this.this$0;
int v7 = 0; // const/4 v7, 0x0
/* invoke-direct {v6, v1, v7}, Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver;-><init>(Lcom/miui/server/BackupManagerService;Lcom/miui/server/BackupManagerService$FullBackupRestoreObserver-IA;)V */
/* move-object v1, v0 */
/* invoke-interface/range {v1 ..v6}, Landroid/app/backup/IBackupManager;->acknowledgeFullBackupOrRestore(IZLjava/lang/String;Ljava/lang/String;Landroid/app/backup/IFullBackupRestoreObserver;)V */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 299 */
/* .line 297 */
/* :catch_0 */
/* move-exception v1 */
/* .line 298 */
/* .local v1, "e":Landroid/os/RemoteException; */
final String v2 = "Backup:BackupManagerService"; // const-string v2, "Backup:BackupManagerService"
final String v3 = "acknowledgeFullBackupOrRestore failed"; // const-string v3, "acknowledgeFullBackupOrRestore failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 300 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
