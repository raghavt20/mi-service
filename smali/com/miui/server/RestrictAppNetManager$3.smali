.class Lcom/miui/server/RestrictAppNetManager$3;
.super Landroid/content/BroadcastReceiver;
.source "RestrictAppNetManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/RestrictAppNetManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 204
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 207
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 208
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 209
    .local v1, "packageName":Ljava/lang/String;
    :goto_0
    const-string v2, "android.intent.extra.UID"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 210
    .local v2, "uid":I
    const-string v4, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 212
    .local v3, "replacing":Z
    invoke-static {}, Lcom/miui/server/RestrictAppNetManager;->-$$Nest$sfgetsService()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    if-lez v2, :cond_3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 216
    :cond_1
    invoke-static {v1}, Lcom/miui/server/RestrictAppNetManager;->-$$Nest$smisAllowAccessInternet(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 217
    invoke-static {p1}, Lcom/miui/server/RestrictAppNetManager;->-$$Nest$smtryDownloadCloudData(Landroid/content/Context;)V

    .line 218
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RULE_RESTRICT packageName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "RestrictAppNetManager"

    invoke-static {v5, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-static {}, Lcom/miui/server/RestrictAppNetManager;->-$$Nest$sfgetsService()Lcom/android/server/net/NetworkManagementServiceStub;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x3

    invoke-interface {v4, v1, v2, v5, v6}, Lcom/android/server/net/NetworkManagementServiceStub;->setMiuiFirewallRule(Ljava/lang/String;III)Z

    .line 221
    :cond_2
    return-void

    .line 213
    :cond_3
    :goto_1
    return-void
.end method
