.class Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Landroid/app/AlarmManager$OnAlarmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlarmListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0

    .line 348
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$AlarmListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    return-void
.end method


# virtual methods
.method public onAlarm()V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v0

    const/4 v1, 0x7

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    .line 352
    const-string v0, "GreezeManager"

    const-string v1, "loop handshake restore"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmLoopCount(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputmLoopCount(Lcom/miui/server/greeze/GreezeManagerService;I)V

    .line 356
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmLoopCount(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 357
    const-string/jumbo v0, "sys.millet.monitor"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 360
    :goto_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$msetLoopAlarm(Lcom/miui/server/greeze/GreezeManagerService;)V

    .line 361
    return-void
.end method
