.class Lcom/miui/server/greeze/GreezeManagerService$7;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1550
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->val$uid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1552
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->val$uid:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V

    .line 1553
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v0, :cond_0

    .line 1554
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->val$uid:I

    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v2, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(II)V

    goto :goto_0

    .line 1555
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1556
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->val$uid:I

    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$7;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v2, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(II)V

    .line 1558
    :cond_1
    :goto_0
    return-void
.end method
