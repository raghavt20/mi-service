.class final Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;
.super Landroid/os/Handler;
.source "AurogonImmobulusMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/AurogonImmobulusMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ImmobulusHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;


# direct methods
.method private constructor <init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 1312
    iput-object p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    .line 1313
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1314
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Looper;Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler-IA;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 1317
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1319
    .local v0, "what":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1333
    :pswitch_0
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 1334
    .local v1, "flag":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1335
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->QuitLaunchModeAction(Z)V

    goto :goto_0

    .line 1337
    :cond_0
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v3, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->QuitLaunchModeAction(Z)V

    .line 1339
    goto :goto_0

    .line 1329
    .end local v1    # "flag":I
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 1330
    .local v1, "uid":I
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerLaunchModeAction(I)V

    .line 1331
    goto :goto_0

    .line 1345
    .end local v1    # "uid":I
    :pswitch_2
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->removeAllMsg()V

    .line 1346
    goto :goto_0

    .line 1341
    :pswitch_3
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 1342
    .local v1, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;)V

    .line 1343
    goto :goto_0

    .line 1326
    .end local v1    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :pswitch_4
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->QuitImmobulusModeAction()V

    .line 1327
    goto :goto_0

    .line 1321
    :pswitch_5
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 1322
    .local v1, "uid":I
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1323
    .local v2, "reason":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v3, v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->TriggerImmobulusModeAction(ILjava/lang/String;)V

    .line 1324
    nop

    .line 1350
    .end local v1    # "uid":I
    .end local v2    # "reason":Ljava/lang/String;
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
