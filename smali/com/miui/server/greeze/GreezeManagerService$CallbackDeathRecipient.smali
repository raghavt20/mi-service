.class Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CallbackDeathRecipient"
.end annotation


# instance fields
.field module:I

.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .param p2, "module"    # I

    .line 719
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 720
    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I

    .line 721
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 6

    .line 725
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I

    move-result-object v0

    .line 727
    .local v0, "frozenUids":[I
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget v4, v0, v3

    .line 728
    .local v4, "frozenuid":I
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v5, v4, v2}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V

    .line 727
    .end local v4    # "frozenuid":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 730
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "module: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has died!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GreezeManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I

    const-string v3, "module died"

    invoke-virtual {v1, v2, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(IILjava/lang/String;)Ljava/util/List;

    .line 732
    return-void
.end method
