.class Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;
.super Lcom/android/server/ServiceThread;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GreezeThread"
.end annotation


# static fields
.field private static sInstance:Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;


# direct methods
.method private constructor <init>()V
    .locals 3

    .line 2617
    const/4 v0, -0x2

    const/4 v1, 0x1

    const-string v2, "Greezer"

    invoke-direct {p0, v2, v0, v1}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    .line 2618
    return-void
.end method

.method private static ensureThreadLocked()V
    .locals 1

    .line 2621
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->sInstance:Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    if-nez v0, :cond_0

    .line 2622
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    invoke-direct {v0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;-><init>()V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->sInstance:Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    .line 2623
    invoke-virtual {v0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->start()V

    .line 2625
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;
    .locals 2

    .line 2628
    const-class v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    monitor-enter v0

    .line 2629
    :try_start_0
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->ensureThreadLocked()V

    .line 2630
    sget-object v1, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->sInstance:Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    monitor-exit v0

    return-object v1

    .line 2631
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
