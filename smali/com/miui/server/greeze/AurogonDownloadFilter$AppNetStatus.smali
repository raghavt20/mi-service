.class Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
.super Ljava/lang/Object;
.source "AurogonDownloadFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/AurogonDownloadFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AppNetStatus"
.end annotation


# instance fields
.field downloadSpeed:F

.field isDownload:Z

.field mLastRxBytes:J

.field mLastTxBytes:J

.field mPacakgeName:Ljava/lang/String;

.field mUid:I

.field final synthetic this$0:Lcom/miui/server/greeze/AurogonDownloadFilter;

.field time:J


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/AurogonDownloadFilter;ILjava/lang/String;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/greeze/AurogonDownloadFilter;
    .param p2, "uid"    # I
    .param p3, "pacakgeName"    # Ljava/lang/String;

    .line 203
    iput-object p1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->this$0:Lcom/miui/server/greeze/AurogonDownloadFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z

    .line 204
    iput p2, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I

    .line 205
    iput-object p3, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mPacakgeName:Ljava/lang/String;

    .line 206
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 210
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AppNetStatus mPacakgeName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mPacakgeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " downloadSpeed = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " kb/s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
