class com.miui.server.greeze.GreezeManagerService$GreezeThread extends com.android.server.ServiceThread {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "GreezeThread" */
} // .end annotation
/* # static fields */
private static com.miui.server.greeze.GreezeManagerService$GreezeThread sInstance;
/* # direct methods */
private com.miui.server.greeze.GreezeManagerService$GreezeThread ( ) {
/* .locals 3 */
/* .line 2617 */
int v0 = -2; // const/4 v0, -0x2
int v1 = 1; // const/4 v1, 0x1
final String v2 = "Greezer"; // const-string v2, "Greezer"
/* invoke-direct {p0, v2, v0, v1}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
/* .line 2618 */
return;
} // .end method
private static void ensureThreadLocked ( ) {
/* .locals 1 */
/* .line 2621 */
v0 = com.miui.server.greeze.GreezeManagerService$GreezeThread.sInstance;
/* if-nez v0, :cond_0 */
/* .line 2622 */
/* new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread; */
/* invoke-direct {v0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;-><init>()V */
/* .line 2623 */
(( com.miui.server.greeze.GreezeManagerService$GreezeThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->start()V
/* .line 2625 */
} // :cond_0
return;
} // .end method
public static com.miui.server.greeze.GreezeManagerService$GreezeThread getInstance ( ) {
/* .locals 2 */
/* .line 2628 */
/* const-class v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread; */
/* monitor-enter v0 */
/* .line 2629 */
try { // :try_start_0
com.miui.server.greeze.GreezeManagerService$GreezeThread .ensureThreadLocked ( );
/* .line 2630 */
v1 = com.miui.server.greeze.GreezeManagerService$GreezeThread.sInstance;
/* monitor-exit v0 */
/* .line 2631 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
