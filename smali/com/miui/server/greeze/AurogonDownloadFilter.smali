.class public Lcom/miui/server/greeze/AurogonDownloadFilter;
.super Ljava/lang/Object;
.source "AurogonDownloadFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;,
        Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    }
.end annotation


# static fields
.field private static final DOWNLOAD_SPEED_COMMEN_LIMIT:I = 0x1f4

.field private static final DOWNLOAD_SPEED_HIGH_LIMIT:I = 0x3e8

.field private static final DOWNLOAD_SPEED_LOW_LIMIT:I = 0x64

.field private static final MSG_AUROGON_DOWNLOAD_SPEED_UPDATE:I = 0x3e9

.field private static final TIME_NET_DETECT_CYCLE:I = 0x1388

.field public static mImportantDownloadApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final sInstance:Lcom/miui/server/greeze/AurogonDownloadFilter;


# instance fields
.field private mAppNetCheckList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

.field private mLastLaunchedAppUid:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.tencent.tmgp.pubgmhd"

    const-string v2, "com.tencent.tmgp.sgame"

    const-string v3, "com.miHoYo.ys.mi"

    const-string v4, "com.miHoYo.Yuanshen"

    const-string v5, "com.miHoYo.ys.bilibili"

    const-string v6, "com.tencent.lolm"

    const-string v7, "com.tencent.jkchess"

    const-string v8, "com.tencent.android.qqdownloader"

    const-string v9, "com.hunantv.imgo.activity"

    const-string v10, "com.youku.phone"

    const-string v11, "com.cmcc.cmvideo"

    const-string v12, "com.letv.android.client"

    filled-new-array/range {v1 .. v12}, [Ljava/lang/String;

    move-result-object v1

    .line 24
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mImportantDownloadApp:Ljava/util/List;

    .line 29
    new-instance v0, Lcom/miui/server/greeze/AurogonDownloadFilter;

    invoke-direct {v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;-><init>()V

    sput-object v0, Lcom/miui/server/greeze/AurogonDownloadFilter;->sInstance:Lcom/miui/server/greeze/AurogonDownloadFilter;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mHandler:Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

    .line 32
    iput-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    .line 33
    const/4 v1, -0x1

    iput v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mLastLaunchedAppUid:I

    .line 36
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    .line 37
    new-instance v1, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->getInstance()Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;-><init>(Lcom/miui/server/greeze/AurogonDownloadFilter;Landroid/os/Looper;Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler-IA;)V

    iput-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mHandler:Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

    .line 38
    return-void
.end method

.method private getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    .locals 4
    .param p1, "uid"    # I

    .line 166
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    monitor-enter v0

    .line 167
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    .line 168
    .local v2, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    iget v3, v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I

    if-ne v3, p1, :cond_0

    .line 169
    monitor-exit v0

    return-object v2

    .line 171
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    :cond_0
    goto :goto_0

    .line 172
    :cond_1
    monitor-exit v0

    .line 173
    const/4 v0, 0x0

    return-object v0

    .line 172
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getAppNetStatus(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    monitor-enter v0

    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    .line 157
    .local v2, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    iget-object v3, v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mPacakgeName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 158
    monitor-exit v0

    return-object v2

    .line 160
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    :cond_0
    goto :goto_0

    .line 161
    :cond_1
    monitor-exit v0

    .line 162
    const/4 v0, 0x0

    return-object v0

    .line 161
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getInstance()Lcom/miui/server/greeze/AurogonDownloadFilter;
    .locals 1

    .line 41
    sget-object v0, Lcom/miui/server/greeze/AurogonDownloadFilter;->sInstance:Lcom/miui/server/greeze/AurogonDownloadFilter;

    return-object v0
.end method


# virtual methods
.method public addAppNetCheckList(ILjava/lang/String;)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 121
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mHandler:Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mHandler:Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 125
    :cond_1
    new-instance v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;-><init>(Lcom/miui/server/greeze/AurogonDownloadFilter;ILjava/lang/String;)V

    .line 126
    .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidTxBytes(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastTxBytes:J

    .line 127
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidRxBytes(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastRxBytes:J

    .line 128
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->time:J

    .line 129
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    monitor-enter v1

    .line 130
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    monitor-exit v1

    .line 132
    return-void

    .line 131
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public calcAppSPeed(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V
    .locals 14
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    .line 90
    iget v0, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidTxBytes(I)J

    move-result-wide v0

    .line 91
    .local v0, "txBytes":J
    iget v2, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I

    invoke-virtual {p0, v2}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidRxBytes(I)J

    move-result-wide v2

    .line 92
    .local v2, "rxBytes":J
    add-long v4, v0, v2

    .line 93
    .local v4, "totalBytes":J
    iget-wide v6, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastRxBytes:J

    iget-wide v8, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastTxBytes:J

    add-long/2addr v6, v8

    sub-long v6, v4, v6

    .line 94
    .local v6, "diffTotalBytes":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 95
    .local v8, "now":J
    iget-wide v10, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->time:J

    sub-long v10, v8, v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    .line 96
    .local v10, "time":J
    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-eqz v12, :cond_0

    .line 97
    const-wide/16 v12, 0x400

    div-long v12, v6, v12

    div-long/2addr v12, v10

    long-to-float v12, v12

    iput v12, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F

    .line 99
    :cond_0
    iput-wide v2, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastRxBytes:J

    .line 100
    iput-wide v0, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastTxBytes:J

    .line 101
    iput-wide v8, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->time:J

    .line 102
    return-void
.end method

.method public getTotalRxBytes()J
    .locals 2

    .line 58
    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalTxBytes()J
    .locals 2

    .line 54
    invoke-static {}, Landroid/net/TrafficStats;->getTotalTxBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUidRxBytes(I)J
    .locals 2
    .param p1, "uid"    # I

    .line 50
    invoke-static {p1}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getUidTxBytes(I)J
    .locals 2
    .param p1, "uid"    # I

    .line 46
    invoke-static {p1}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public isDownloadApp(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 105
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    move-result-object v0

    .line 106
    .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    if-eqz v0, :cond_0

    .line 107
    iget-boolean v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z

    return v1

    .line 109
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isDownloadApp(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pacakgeName"    # Ljava/lang/String;

    .line 113
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    move-result-object v0

    .line 114
    .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    if-eqz v0, :cond_0

    .line 115
    iget-boolean v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z

    return v1

    .line 117
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public removeAppNetCheckList(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 144
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    move-result-object v0

    .line 145
    .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V

    .line 146
    return-void
.end method

.method public removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V
    .locals 2
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    .line 149
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    monitor-enter v0

    .line 150
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 151
    monitor-exit v0

    .line 152
    return-void

    .line 151
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeAppNetCheckList(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .line 139
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    move-result-object v0

    .line 140
    .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V

    .line 141
    return-void
.end method

.method public setMoveToFgApp(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 135
    iput p1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mLastLaunchedAppUid:I

    .line 136
    return-void
.end method

.method public updateAppSpeed()V
    .locals 7

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v0, "removeList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    .line 65
    .local v3, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    invoke-virtual {p0, v3}, Lcom/miui/server/greeze/AurogonDownloadFilter;->calcAppSPeed(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V

    .line 66
    iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F

    const/high16 v5, 0x447a0000    # 1000.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 67
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 68
    const-string v4, "Aurogon isDownload "

    invoke-virtual {v3}, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z

    goto :goto_1

    .line 70
    :cond_1
    iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F

    const/high16 v5, 0x43fa0000    # 500.0f

    cmpl-float v4, v4, v5

    const/4 v5, 0x0

    if-lez v4, :cond_2

    .line 71
    iput-boolean v5, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z

    goto :goto_1

    .line 72
    :cond_2
    iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F

    const/high16 v6, 0x42c80000    # 100.0f

    cmpg-float v4, v4, v6

    if-gez v4, :cond_3

    .line 73
    iput-boolean v5, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z

    .line 74
    iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I

    iget v5, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mLastLaunchedAppUid:I

    if-eq v4, v5, :cond_3

    .line 75
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    .end local v3    # "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    :cond_3
    :goto_1
    goto :goto_0

    .line 79
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;

    .line 81
    .local v2, "removeapp":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    invoke-virtual {p0, v2}, Lcom/miui/server/greeze/AurogonDownloadFilter;->removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V

    .line 82
    .end local v2    # "removeapp":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
    goto :goto_2

    .line 84
    :cond_5
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mAppNetCheckList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_6

    .line 85
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mHandler:Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;

    const/16 v2, 0x3e9

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 87
    :cond_6
    return-void

    .line 79
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
