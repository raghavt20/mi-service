.class public Lcom/miui/server/greeze/GreezeManagerService;
.super Lmiui/greeze/IGreezeManager$Stub;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;,
        Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;,
        Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;,
        Lcom/miui/server/greeze/GreezeManagerService$H;,
        Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;,
        Lcom/miui/server/greeze/GreezeManagerService$LocalService;,
        Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;,
        Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;,
        Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;,
        Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;,
        Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;,
        Lcom/miui/server/greeze/GreezeManagerService$Lifecycle;
    }
.end annotation


# static fields
.field public static final BINDER_STATE_IN_BUSY:I = 0x1

.field public static final BINDER_STATE_IN_IDLE:I = 0x0

.field public static final BINDER_STATE_IN_TRANSACTION:I = 0x4

.field public static final BINDER_STATE_PROC_IN_BUSY:I = 0x3

.field public static final BINDER_STATE_THREAD_IN_BUSY:I = 0x2

.field private static final CLOUD_AUROGON_ALARM_ALLOW_LIST:Ljava/lang/String; = "cloud_aurogon_alarm_allow_list"

.field private static final CLOUD_GREEZER_ENABLE:Ljava/lang/String; = "cloud_greezer_enable"

.field private static final CMD_ADJ_ADD:I = 0x0

.field private static final CMD_ADJ_REMOVE:I = 0x1

.field private static final CMD_ADJ_THAWALL:I = 0x2

.field private static final DUMPSYS_HISTORY_DURATION:I = 0xdbba00

.field private static final HISTORY_SIZE:I

.field private static final IGreezeManagerSingleton:Landroid/util/Singleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Singleton<",
            "Lmiui/greeze/IGreezeManager;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOOPONCE_DELAY_TIME:J = 0x1388L

.field private static final MAX_HISTORY_ITEMS:I = 0x1000

.field private static final MILLET_DELAY_CEILING:J = 0x2710L

.field private static final MILLET_DELAY_THRASHOLD:J = 0x32L

.field private static final MILLET_MONITOR_ALL:I = 0x7

.field private static final MILLET_MONITOR_BINDER:I = 0x1

.field private static final MILLET_MONITOR_NET:I = 0x4

.field private static final MILLET_MONITOR_SIGNAL:I = 0x2

.field public static final SERVICE_NAME:Ljava/lang/String; = "greezer"

.field public static final TAG:Ljava/lang/String; = "GreezeManager"

.field private static final TIME_FORMAT_PATTERN:Ljava/lang/String; = "HH:mm:ss.SSS"

.field static callbacks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Lmiui/greeze/IGreezeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private static mAllowBroadcast:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mCNDeferBroadcast:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mMiuiDeferBroadcast:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mNeedCachedBroadcast:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public DISABLE_IMMOB_MODE_DEVICE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ENABLE_LAUNCH_MODE_DEVICE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ENABLE_VIDEO_MODE_DEVICE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private am:Landroid/app/AlarmManager;

.field private cm:Landroid/net/ConnectivityManager;

.field private isBarExpand:Z

.field private isoPids:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mActivityCtrlBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityManager:Landroid/app/ActivityManager;

.field private final mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field mActivityTaskManager:Landroid/app/IActivityTaskManager;

.field private mAlarmLoop:Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;

.field mAlarmManager:Landroid/app/IAlarmManager;

.field private mAudioZeroPkgs:Ljava/lang/String;

.field private mAurogonAlarmAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAurogonAlarmForbidList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mAurogonLock:Ljava/lang/Object;

.field public mBGOrderBroadcastAction:Ljava/lang/String;

.field public mBGOrderBroadcastAppUid:I

.field private mBroadcastCallerWhiteList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mBroadcastCtrlCloud:Z

.field private mBroadcastCtrlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mBroadcastIntentDenyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBroadcastTargetWhiteList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public mCachedBCList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Landroid/content/Intent;",
            ">;>;"
        }
    .end annotation
.end field

.field final mCloudAurogonAlarmListObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field mDisplayManager:Landroid/hardware/display/DisplayManager;

.field mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field public mExcuteServiceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mFGOrderBroadcastAction:Ljava/lang/String;

.field public mFGOrderBroadcastAppUid:I

.field public mFreeformSmallWinList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFrozenHistory:[Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

.field private final mFrozenPids:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mFsgNavBar:Z

.field private mGetCastPid:Ljava/lang/reflect/Method;

.field public mHandler:Landroid/os/Handler;

.field private mHistoryIndexNext:I

.field private mHistoryLog:Landroid/util/LocalLog;

.field public mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

.field private mImmobulusModeWhiteList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public mInFreeformSmallWinMode:Z

.field private mInitCtsStatused:Z

.field private mInited:Z

.field private mLoopCount:I

.field private final mMonitorTokens:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lmiui/greeze/IMonitorToken;",
            ">;"
        }
    .end annotation
.end field

.field private mNms:Landroid/os/INetworkManagementService;

.field private mPm:Landroid/content/pm/PackageManager;

.field private final mProcessObserver:Landroid/app/IProcessObserver;

.field public mRecentLaunchAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRegisteredMonitor:I

.field public mScreenOnOff:Z

.field private mSystemUiPid:I

.field private mThread:Lcom/android/server/ServiceThread;

.field public mTopAppPackageName:Ljava/lang/String;

.field public mTopAppUid:I

.field private mUidAdjs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mUidObserver:Landroid/app/IUidObserver;

.field public mWindowManager:Landroid/view/IWindowManager;

.field private powerManagerServiceImpl:Ljava/lang/Object;


# direct methods
.method static bridge synthetic -$$Nest$fgetENABLE_VIDEO_MODE_DEVICE(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->ENABLE_VIDEO_MODE_DEVICE:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->isoPids:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmActivityManagerService(Lcom/miui/server/greeze/GreezeManagerService;)Lcom/android/server/am/ActivityManagerService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAudioZeroPkgs(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAudioZeroPkgs:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmAurogonAlarmAllowList(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonAlarmAllowList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFrozenPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHistoryLog(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/LocalLog;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmInited(Lcom/miui/server/greeze/GreezeManagerService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLoopCount(Lcom/miui/server/greeze/GreezeManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMonitorTokens(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mMonitorTokens:Landroid/util/SparseArray;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmUidAdjs(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mUidAdjs:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputisBarExpand(Lcom/miui/server/greeze/GreezeManagerService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->isBarExpand:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmAudioZeroPkgs(Lcom/miui/server/greeze/GreezeManagerService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAudioZeroPkgs:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLoopCount(Lcom/miui/server/greeze/GreezeManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmSystemUiPid(Lcom/miui/server/greeze/GreezeManagerService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckPermission(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetAlarmManagerService(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getAlarmManagerService()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetFrozenInfo(Lcom/miui/server/greeze/GreezeManagerService;I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetInfoUid(Lcom/miui/server/greeze/GreezeManagerService;I)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getInfoUid(I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mgetNavBarInfo(Lcom/miui/server/greeze/GreezeManagerService;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getNavBarInfo(Landroid/content/Context;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetSystemUiPid(Lcom/miui/server/greeze/GreezeManagerService;)I
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getSystemUiPid()I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mhandleAppZygoteStart(Lcom/miui/server/greeze/GreezeManagerService;Landroid/content/pm/ApplicationInfo;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetLoopAlarm(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->setLoopAlarm()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smnLoopOnce()V
    .locals 0

    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->nLoopOnce()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smstartService()V
    .locals 0

    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->startService()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 12

    .line 85
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MONKEY:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4000

    goto :goto_0

    :cond_0
    const/16 v0, 0x1000

    :goto_0
    sput v0, Lcom/miui/server/greeze/GreezeManagerService;->HISTORY_SIZE:I

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "android.intent.action.SCREEN_OFF"

    const-string v2, "android.intent.action.SCREEN_ON"

    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    const-string v4, "android.intent.action.ACTION_POWER_CONNECTED"

    const-string v5, "android.os.action.DEVICE_IDLE_MODE_CHANGED"

    const-string v6, "android.os.action.LIGHT_DEVICE_IDLE_MODE_CHANGED"

    const-string v7, "com.ss.android.ugc.aweme.search.widget.APPWIDGET_RESET_ALARM"

    const-string v8, "com.tencent.mm.TrafficStatsReceiver"

    const-string v9, "com.xiaomi.mipush.MESSAGE_ARRIVED"

    const-string v10, "com.tencent.mm.plugin.report.service.KVCommCrossProcessReceiver"

    const-string v11, "android.intent.action.wutao"

    filled-new-array/range {v1 .. v11}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mNeedCachedBroadcast:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mAllowBroadcast:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mMiuiDeferBroadcast:Ljava/util/List;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.google.android.gcm.CONNECTED"

    const-string v2, "com.google.android.gms.gcm.HEARTBEAT_ALARM"

    const-string v3, "com.google.android.intent.action.GCM_RECONNECT"

    const-string v4, "com.google.android.gcm.DISCONNECTED"

    filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mCNDeferBroadcast:Ljava/util/List;

    .line 237
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$2;

    invoke-direct {v0}, Lcom/miui/server/greeze/GreezeManagerService$2;-><init>()V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService;->IGreezeManagerSingleton:Landroid/util/Singleton;

    .line 668
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 49
    .param p1, "context"    # Landroid/content/Context;

    .line 176
    move-object/from16 v1, p0

    invoke-direct/range {p0 .. p0}, Lmiui/greeze/IGreezeManager$Stub;-><init>()V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    const-string/jumbo v2, "thor"

    const-string v3, "dagu"

    const-string/jumbo v4, "zizhan"

    const-string/jumbo v5, "unicorn"

    const-string v6, "mayfly"

    const-string v7, "cupid"

    const-string/jumbo v8, "zeus"

    const-string v9, "nuwa"

    const-string v10, "fuxi"

    const-string/jumbo v11, "socrates"

    const-string v12, "marble"

    const-string v13, "liuqin"

    const-string v14, "pipa"

    const-string v15, "ishtar"

    const-string v16, "daumier"

    const-string v17, "matisse"

    const-string v18, "rubens"

    const-string/jumbo v19, "xaga"

    const-string/jumbo v20, "yuechu"

    const-string/jumbo v21, "sky"

    const-string v22, "pearl"

    const-string v23, "babylon"

    const-string/jumbo v24, "selene"

    const-string/jumbo v25, "veux"

    const-string v26, "earth"

    const-string v27, "evergo"

    const-string v28, "corot"

    const-string/jumbo v29, "yudi"

    const-string/jumbo v30, "xun"

    const-string v31, "river"

    const-string/jumbo v32, "shennong"

    const-string v33, "houji"

    const-string v34, "manet"

    const-string/jumbo v35, "vermeer"

    const-string v36, "duchamp"

    const-string v37, "mondrian"

    const-string/jumbo v38, "sheng"

    const-string v39, "aurora"

    const-string v40, "ruyi"

    const-string v41, "chenfeng"

    const-string v42, "garnet"

    const-string/jumbo v43, "zircon"

    const-string v44, "dizi"

    const-string v45, "goku"

    const-string v46, "peridot"

    const-string v47, "breeze"

    const-string v48, "ruan"

    filled-new-array/range {v2 .. v48}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->ENABLE_LAUNCH_MODE_DEVICE:Ljava/util/List;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    const-string v2, "river"

    const-string v3, "breeze"

    const-string/jumbo v4, "sky"

    filled-new-array {v4, v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->DISABLE_IMMOB_MODE_DEVICE:Ljava/util/List;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    const-string v2, "liuqin"

    const-string v3, "pipa"

    const-string v4, "fuxi"

    const-string/jumbo v5, "yuechu"

    const-string/jumbo v6, "yudi"

    const-string/jumbo v7, "shennong"

    const-string v8, "houji"

    const-string/jumbo v9, "sheng"

    const-string v10, "aurora"

    const-string v11, "chenfeng"

    filled-new-array/range {v2 .. v11}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->ENABLE_VIDEO_MODE_DEVICE:Ljava/util/List;

    .line 97
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    .line 98
    const-string v0, "com.tencent.lolm, com.netease.dwrg, com.tencent.tmgp.pubgm, com.tencent.tmgp.dwrg, com.tencent.tmgp.pubgmhd, com.tencent.tmgp.sgame, com.netease.dwrg.mi, com.duowan.kiwi"

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAudioZeroPkgs:Ljava/lang/String;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusModeWhiteList:Ljava/util/Map;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    const-string v2, "com.tencent.android.qqdownloader"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mActivityCtrlBlackList:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCallerWhiteList:Ljava/util/Map;

    .line 112
    sget v0, Lcom/miui/server/greeze/GreezeManagerService;->HISTORY_SIZE:I

    new-array v0, v0, [Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenHistory:[Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 116
    const/4 v0, 0x0

    iput v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I

    .line 118
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonLock:Ljava/lang/Object;

    .line 119
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    .line 120
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->cm:Landroid/net/ConnectivityManager;

    .line 121
    const/4 v3, 0x1

    iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z

    .line 122
    iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z

    .line 288
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mMonitorTokens:Landroid/util/SparseArray;

    .line 335
    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;

    invoke-direct {v4, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$AlarmListener-IA;)V

    iput-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAlarmLoop:Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;

    .line 336
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->am:Landroid/app/AlarmManager;

    .line 337
    iput v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I

    .line 403
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->isoPids:Landroid/util/SparseArray;

    .line 642
    iput-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mInitCtsStatused:Z

    .line 1078
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mUidAdjs:Ljava/util/Set;

    .line 1725
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mNms:Landroid/os/INetworkManagementService;

    .line 1765
    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$8;

    invoke-direct {v4, v1}, Lcom/miui/server/greeze/GreezeManagerService$8;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    iput-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mUidObserver:Landroid/app/IUidObserver;

    .line 1798
    new-instance v5, Lcom/miui/server/greeze/GreezeManagerService$9;

    invoke-direct {v5, v1}, Lcom/miui/server/greeze/GreezeManagerService$9;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    iput-object v5, v1, Lcom/miui/server/greeze/GreezeManagerService;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 1868
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    .line 1869
    iput-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z

    .line 2011
    const-string v6, ""

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAction:Ljava/lang/String;

    .line 2012
    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAction:Ljava/lang/String;

    .line 2013
    const/4 v7, -0x1

    iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I

    .line 2014
    iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I

    .line 2057
    new-instance v8, Landroid/util/LocalLog;

    const/16 v9, 0x1000

    invoke-direct {v8, v9}, Landroid/util/LocalLog;-><init>(I)V

    iput-object v8, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    .line 2071
    iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    .line 2072
    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppPackageName:Ljava/lang/String;

    .line 2074
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mPm:Landroid/content/pm/PackageManager;

    .line 2076
    new-instance v6, Ljava/util/ArrayList;

    const-string v8, "com.tencent.mm"

    const-string v9, "com.tencent.mobileqq"

    filled-new-array {v8, v9}, [Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonAlarmAllowList:Ljava/util/List;

    .line 2077
    new-instance v6, Ljava/util/ArrayList;

    const-string v8, "com.miui.player"

    filled-new-array {v8}, [Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonAlarmForbidList:Ljava/util/List;

    .line 2078
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    .line 2079
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAlarmManager:Landroid/app/IAlarmManager;

    .line 2080
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    .line 2081
    iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    .line 2082
    iput-object v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 2084
    new-instance v6, Ljava/util/ArrayList;

    const-string v8, "android.intent.action.BATTERY_CHANGED"

    const-string v9, "android.net.wifi.STATE_CHANGE"

    const-string v10, "android.intent.action.DROPBOX_ENTRY_ADDED"

    const-string v11, "android.net.wifi.RSSI_CHANGED"

    const-string v12, "android.net.wifi.supplicant.STATE_CHANGE"

    const-string v13, "com.android.server.action.NETWORK_STATS_UPDATED"

    const-string v14, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    const-string v15, "android.intent.action.TIME_TICK"

    const-string v16, "android.net.conn.CONNECTIVITY_CHANGE"

    const-string v17, "android.net.wifi.WIFI_STATE_CHANGED"

    const-string v18, "android.net.wifi.SCAN_RESULTS"

    filled-new-array/range {v8 .. v18}, [Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastIntentDenyList:Ljava/util/List;

    .line 2154
    new-instance v6, Lcom/miui/server/greeze/GreezeManagerService$13;

    iget-object v8, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v6, v1, v8}, Lcom/miui/server/greeze/GreezeManagerService$13;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Handler;)V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mCloudAurogonAlarmListObserver:Landroid/database/ContentObserver;

    .line 2181
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mExcuteServiceList:Ljava/util/List;

    .line 2244
    iput-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->isBarExpand:Z

    .line 2245
    iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I

    .line 2314
    iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z

    .line 2315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlMap:Ljava/util/Map;

    .line 2540
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    .line 177
    move-object/from16 v3, p1

    iput-object v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    .line 178
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->getInstance()Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;

    move-result-object v0

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mThread:Lcom/android/server/ServiceThread;

    .line 179
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$H;

    iget-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v6}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v0, v1, v6}, Lcom/miui/server/greeze/GreezeManagerService$H;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Looper;)V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    .line 180
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 181
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 182
    iget-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    const-string v8, "activity"

    invoke-virtual {v6, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager;

    iput-object v6, v1, Lcom/miui/server/greeze/GreezeManagerService;->mActivityManager:Landroid/app/ActivityManager;

    .line 183
    sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    if-eqz v6, :cond_0

    .line 184
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->registerCloudObserver(Landroid/content/Context;)V

    .line 185
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "cloud_greezer_enable"

    const/4 v9, -0x2

    invoke-static {v6, v8, v9}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 186
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v8, v9}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    sput-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    .line 189
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->registerObserverForAurogon()V

    .line 190
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->getWindowManagerService()V

    .line 191
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->getAlarmManagerService()V

    .line 193
    :try_start_0
    invoke-virtual {v0, v5}, Lcom/android/server/am/ActivityManagerService;->registerProcessObserver(Landroid/app/IProcessObserver;)V

    .line 194
    const/16 v5, 0xe

    invoke-virtual {v0, v4, v5, v7, v2}, Lcom/android/server/am/ActivityManagerService;->registerUidObserver(Landroid/app/IUidObserver;IILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 199
    :goto_0
    const-class v0, Landroid/hardware/display/DisplayManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 200
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;

    invoke-direct {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    .line 202
    new-instance v0, Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    iget-object v5, v1, Lcom/miui/server/greeze/GreezeManagerService;->mThread:Lcom/android/server/ServiceThread;

    invoke-direct {v0, v4, v5, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;-><init>(Landroid/content/Context;Landroid/os/HandlerThread;Lcom/miui/server/greeze/GreezeManagerService;)V

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    .line 204
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    const-class v4, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->cm:Landroid/net/ConnectivityManager;

    .line 205
    const-class v0, Lcom/miui/server/greeze/GreezeManagerInternal;

    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$LocalService;

    invoke-direct {v4, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$LocalService;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$LocalService-IA;)V

    invoke-static {v0, v4}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 206
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->initArgs()V

    .line 207
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->am:Landroid/app/AlarmManager;

    .line 208
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    .line 209
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    const-string v2, "display"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 210
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->registerDisplayChanageListener()V

    .line 211
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v0, :cond_1

    .line 212
    const-string v0, "millet_binder"

    const-string v2, "ctl.start"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v0, "millet_pkg"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v0, "millet_sig"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_1
    return-void
.end method

.method private addHistoryInfo(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 2819
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenHistory:[Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I

    aput-object p1, v0, v1

    .line 2820
    const/4 v0, 0x1

    sget v2, Lcom/miui/server/greeze/GreezeManagerService;->HISTORY_SIZE:I

    invoke-static {v1, v0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->ringAdvance(III)I

    move-result v0

    iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I

    .line 2821
    return-void
.end method

.method private checkAndFreezeIsolated(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "freeze"    # Z

    .line 1366
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_0

    return-void

    .line 1367
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$5;-><init>(Lcom/miui/server/greeze/GreezeManagerService;IZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1401
    return-void
.end method

.method private checkPermission()V
    .locals 4

    .line 279
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 280
    .local v0, "uid":I
    invoke-static {v0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    return-void

    .line 283
    :cond_0
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " does not have permission to greezer"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private checkStateForScrOff(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1104
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1105
    return v1

    .line 1108
    :cond_0
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x4e1f

    if-gt p1, v0, :cond_1

    .line 1109
    const/4 v0, 0x1

    return v0

    .line 1110
    :cond_1
    return v1
.end method

.method private deferBroadcastForMiui(Ljava/lang/String;)Z
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 2448
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mMiuiDeferBroadcast:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2449
    return v1

    .line 2450
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mCNDeferBroadcast:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2451
    return v1

    .line 2452
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningImmobulusMode()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mMiuiDeferBroadcast:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2453
    return v1

    .line 2454
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private dumpFreezeAction(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 2060
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    const-string v1, "Frozen processes:"

    if-eqz v0, :cond_0

    .line 2061
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 2063
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x270f

    invoke-virtual {p0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenPids(I)[I

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2064
    :goto_0
    const-string v0, "Greezer History : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2065
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v0, p1, p2, p3}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2068
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2069
    return-void
.end method

.method private getAlarmManagerService()V
    .locals 1

    .line 2091
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAlarmManager:Landroid/app/IAlarmManager;

    if-nez v0, :cond_0

    .line 2092
    const-string v0, "alarm"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/IAlarmManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IAlarmManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAlarmManager:Landroid/app/IAlarmManager;

    .line 2094
    :cond_0
    return-void
.end method

.method private getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .locals 5
    .param p1, "uid"    # I

    .line 390
    const/4 v0, 0x0

    .line 391
    .local v0, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 392
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 393
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 394
    .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-eqz v3, :cond_0

    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    if-ne v4, p1, :cond_0

    .line 395
    move-object v0, v3

    .line 396
    goto :goto_1

    .line 392
    .end local v3    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 399
    .end local v2    # "i":I
    :cond_1
    :goto_1
    monitor-exit v1

    .line 400
    return-object v0

    .line 399
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getFrozenNewPids()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1581
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1582
    .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 1583
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1584
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1585
    .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1583
    nop

    .end local v3    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1587
    .end local v2    # "i":I
    :cond_0
    monitor-exit v1

    .line 1588
    return-object v0

    .line 1587
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getHistoryInfos(J)Ljava/util/List;
    .locals 7
    .param p1, "sinceUptime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;",
            ">;"
        }
    .end annotation

    .line 2824
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2825
    .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I

    sget v2, Lcom/miui/server/greeze/GreezeManagerService;->HISTORY_SIZE:I

    const/4 v3, -0x1

    invoke-static {v1, v3, v2}, Lcom/miui/server/greeze/GreezeManagerService;->ringAdvance(III)I

    move-result v1

    .line 2827
    .local v1, "index":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget v4, Lcom/miui/server/greeze/GreezeManagerService;->HISTORY_SIZE:I

    if-ge v2, v4, :cond_1

    .line 2828
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenHistory:[Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    aget-object v5, v5, v1

    if-eqz v5, :cond_1

    iget-wide v5, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J

    cmp-long v5, v5, p1

    if-gez v5, :cond_0

    .line 2830
    goto :goto_1

    .line 2832
    :cond_0
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenHistory:[Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    aget-object v5, v5, v1

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2833
    invoke-static {v1, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->ringAdvance(III)I

    move-result v1

    .line 2827
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2835
    .end local v2    # "i":I
    :cond_1
    :goto_1
    return-object v0
.end method

.method private getInfoUid(I)I
    .locals 5
    .param p1, "pid"    # I

    .line 405
    const/4 v0, -0x1

    .line 406
    .local v0, "infoUid":I
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->isoPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 407
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->isoPids:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_2

    .line 408
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->isoPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 409
    .local v3, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-nez v3, :cond_0

    goto :goto_1

    .line 410
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 411
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->isoPids:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    move v0, v4

    .line 412
    goto :goto_2

    .line 407
    .end local v3    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 415
    .end local v2    # "i":I
    :cond_2
    :goto_2
    monitor-exit v1

    .line 416
    return v0

    .line 415
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private getNavBarInfo(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 167
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_fsg_nav_bar"

    const/4 v2, -0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "val":Ljava/lang/String;
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mFsgNavBar :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GreezeManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getSystemUiPid()I

    move-result v1

    iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I

    goto :goto_0

    .line 174
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z

    .line 175
    :goto_0
    return-void
.end method

.method private getNmsService()Landroid/os/INetworkManagementService;
    .locals 1

    .line 1728
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mNms:Landroid/os/INetworkManagementService;

    if-nez v0, :cond_0

    .line 1729
    nop

    .line 1730
    const-string v0, "network_management"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 1729
    invoke-static {v0}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mNms:Landroid/os/INetworkManagementService;

    .line 1732
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mNms:Landroid/os/INetworkManagementService;

    return-object v0
.end method

.method public static getService()Lcom/miui/server/greeze/GreezeManagerService;
    .locals 1

    .line 252
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->IGreezeManagerSingleton:Landroid/util/Singleton;

    invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/greeze/GreezeManagerService;

    return-object v0
.end method

.method private getSystemUiPid()I
    .locals 5

    .line 2328
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v0

    .line 2329
    .local v0, "procList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/RunningProcess;

    .line 2330
    .local v2, "proc":Lcom/miui/server/greeze/RunningProcess;
    if-eqz v2, :cond_0

    const-string v3, "com.android.systemui"

    iget-object v4, v2, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2331
    iget v1, v2, Lcom/miui/server/greeze/RunningProcess;->pid:I

    return v1

    .line 2332
    .end local v2    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :cond_0
    goto :goto_0

    .line 2333
    :cond_1
    const/4 v1, -0x1

    return v1
.end method

.method private getWindowManagerService()V
    .locals 1

    .line 2097
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v0, :cond_0

    .line 2098
    invoke-static {}, Landroid/view/WindowManagerGlobal;->getWindowManagerService()Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    .line 2100
    :cond_0
    return-void
.end method

.method private handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V
    .locals 3
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "start"    # Z

    .line 2520
    if-nez p1, :cond_0

    .line 2521
    return-void

    .line 2523
    :cond_0
    if-eqz p2, :cond_1

    .line 2524
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    const/4 v1, 0x1

    const-string v2, "AppZygote"

    invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 2526
    :cond_1
    return-void
.end method

.method private initArgs()V
    .locals 5

    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget-object v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMessageApp:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 223
    sget-object v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMessageApp:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 224
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    .end local v2    # "pkgName":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 226
    .end local v1    # "i":I
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v1, "callerAction":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    const-string v2, "android.appwidget.action.APPWIDGET_ENABLED"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCallerWhiteList:Ljava/util/Map;

    const-string v3, "android"

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v2, "immobulusModeActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "*"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusModeWhiteList:Ljava/util/Map;

    const-string v4, "com.xiaomi.metoknlp"

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->getNavBarInfo(Landroid/content/Context;)V

    .line 235
    return-void
.end method

.method private static native nAddConcernedUid(I)V
.end method

.method private static native nClearConcernedUid()V
.end method

.method private static native nDelConcernedUid(I)V
.end method

.method private static native nLoopOnce()V
.end method

.method private static native nQueryBinder(I)V
.end method

.method private registerCloudObserver(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 133
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$1;

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, p1}, Lcom/miui/server/greeze/GreezeManagerService$1;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Handler;Landroid/content/Context;)V

    .line 151
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 152
    const-string v2, "cloud_greezer_enable"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 151
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 155
    const-string/jumbo v2, "zeropkgs"

    invoke-static {v2}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 154
    invoke-virtual {v1, v5, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 158
    const-string v5, "force_fsg_nav_bar"

    invoke-static {v5}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 157
    invoke-virtual {v1, v5, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 160
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "data":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-le v2, v3, :cond_0

    .line 163
    iput-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAudioZeroPkgs:Ljava/lang/String;

    .line 164
    :cond_0
    return-void
.end method

.method private registerDisplayChanageListener()V
    .locals 3

    .line 3379
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener-IA;)V

    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 3380
    return-void
.end method

.method private registerObserverForAurogon()V
    .locals 5

    .line 2175
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2176
    const-string v1, "cloud_aurogon_alarm_allow_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCloudAurogonAlarmListObserver:Landroid/database/ContentObserver;

    .line 2175
    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 2178
    return-void
.end method

.method private static ringAdvance(III)I
    .locals 2
    .param p0, "origin"    # I
    .param p1, "increment"    # I
    .param p2, "size"    # I

    .line 2814
    add-int v0, p0, p1

    rem-int/2addr v0, p2

    .line 2815
    .local v0, "index":I
    if-gez v0, :cond_0

    add-int v1, v0, p2

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    return v1
.end method

.method private sendMilletLoop()V
    .locals 4

    .line 248
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 249
    return-void
.end method

.method private setLoopAlarm()V
    .locals 9

    .line 339
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->am:Landroid/app/AlarmManager;

    const-string v1, "GreezeManager"

    if-nez v0, :cond_0

    .line 340
    const-string/jumbo v0, "setLoopAlarm am == null"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    return-void

    .line 343
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setLoopAlarm am cnt = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->am:Landroid/app/AlarmManager;

    const/4 v3, 0x3

    .line 345
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/32 v4, 0x493e0

    add-long/2addr v4, v0

    const-string v6, "monitorloop"

    iget-object v7, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAlarmLoop:Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;

    const/4 v8, 0x0

    .line 344
    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V

    .line 347
    return-void
.end method

.method private setWakeLockState(Ljava/util/List;Z)V
    .locals 6
    .param p2, "disable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .line 755
    .local p1, "ls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 756
    .local v1, "uid":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->isIsolated(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 760
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/android/server/power/PowerManagerServiceStub;->get()Lcom/android/server/power/PowerManagerServiceStub;

    move-result-object v2

    .line 761
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const-string v4, "greeze"

    .line 760
    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, p2, v4}, Lcom/android/server/power/PowerManagerServiceStub;->setUidPartialWakeLockDisabledState(ILjava/lang/String;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 765
    goto :goto_1

    .line 763
    :catch_0
    move-exception v2

    .line 764
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "GreezeManager"

    const-string/jumbo v4, "updateWakelockBlockedUid"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 766
    .end local v1    # "uid":Ljava/lang/Integer;
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_0

    .line 767
    :cond_1
    return-void
.end method

.method private static startService()V
    .locals 2

    .line 256
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->getService()Lcom/miui/server/greeze/GreezeManagerService;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "persist.sys.millet.handshake"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->getService()Lcom/miui/server/greeze/GreezeManagerService;

    move-result-object v0

    invoke-direct {v0}, Lcom/miui/server/greeze/GreezeManagerService;->sendMilletLoop()V

    .line 258
    const-string v0, "persist.sys.millet.cgroup"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    .line 260
    :cond_0
    return-void
.end method

.method public static stateToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # I

    .line 742
    packed-switch p0, :pswitch_data_0

    .line 748
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 747
    :pswitch_0
    const-string v0, "BINDER_IN_TRANSACTION"

    return-object v0

    .line 746
    :pswitch_1
    const-string v0, "BINDER_PROC_IN_BUSY"

    return-object v0

    .line 745
    :pswitch_2
    const-string v0, "BINDER_THREAD_IN_BUSY"

    return-object v0

    .line 744
    :pswitch_3
    const-string v0, "BINDER_IN_BUSY"

    return-object v0

    .line 743
    :pswitch_4
    const-string v0, "BINDER_IN_IDLE"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static toArray(Ljava/util/List;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)[I"
        }
    .end annotation

    .line 2599
    .local p0, "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-nez p0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0

    .line 2600
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    .line 2601
    .local v0, "arr":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 2602
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v1

    .line 2601
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2604
    .end local v1    # "i":I
    :cond_1
    return-object v0
.end method


# virtual methods
.method public CachedBroadcasForAurogon(Landroid/content/Intent;I)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "uid"    # I

    .line 2543
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    monitor-enter v0

    .line 2544
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2545
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    if-nez v1, :cond_0

    .line 2546
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 2547
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2549
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 2550
    .local v3, "old":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2551
    monitor-exit v0

    return-void

    .line 2553
    .end local v3    # "old":Landroid/content/Intent;
    :cond_1
    goto :goto_0

    .line 2554
    :cond_2
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2555
    nop

    .end local v1    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    monitor-exit v0

    .line 2556
    return-void

    .line 2555
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public addToDumpHistory(Ljava/lang/String;)V
    .locals 1
    .param p1, "log"    # Ljava/lang/String;

    .line 1355
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v0, p1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1356
    return-void
.end method

.method public checkAurogonIntentDenyList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 2591
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastIntentDenyList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2592
    const/4 v0, 0x1

    return v0

    .line 2594
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public checkFreeformSmallWin(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 1937
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1938
    .local v0, "packageName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1939
    const/4 v1, 0x0

    return v1

    .line 1941
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    monitor-enter v1

    .line 1942
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    monitor-exit v1

    return v2

    .line 1943
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public checkFreeformSmallWin(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1931
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    monitor-enter v0

    .line 1932
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1933
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method checkImmobulusModeRestrict(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "targetPkgName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 2529
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusModeWhiteList:Ljava/util/Map;

    monitor-enter v0

    .line 2530
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusModeWhiteList:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2531
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusModeWhiteList:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2532
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "*"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2533
    :cond_0
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 2536
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 2537
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public checkOrderBCRecivingApp(I)Z
    .locals 4
    .param p1, "uid"    # I

    .line 2043
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I

    const/4 v1, 0x1

    const/4 v2, -0x1

    const-string v3, ""

    if-ne p1, v0, :cond_0

    .line 2044
    iput-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAction:Ljava/lang/String;

    .line 2045
    iput v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I

    .line 2046
    return v1

    .line 2047
    :cond_0
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I

    if-ne p1, v0, :cond_1

    .line 2048
    iput-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAction:Ljava/lang/String;

    .line 2049
    iput v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I

    .line 2050
    return v1

    .line 2052
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public checkRecentLuanchedApp(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 1954
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    monitor-enter v0

    .line 1955
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1956
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public clearMonitorNet()V
    .locals 0

    .line 780
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->nClearConcernedUid()V

    .line 781
    return-void
.end method

.method public clearMonitorNet(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 776
    invoke-static {p1}, Lcom/miui/server/greeze/GreezeManagerService;->nDelConcernedUid(I)V

    .line 777
    return-void
.end method

.method public closeSocketForAurogon(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 1718
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "closeSocketForAurogon uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AurogonImmobulusMode"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 1721
    .local v0, "uids":[I
    const/4 v1, 0x0

    aput p1, v0, v1

    .line 1722
    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->closeSocketForAurogon([I)V

    .line 1723
    return-void
.end method

.method public closeSocketForAurogon([I)V
    .locals 3
    .param p1, "uids"    # [I

    .line 1708
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getNmsService()Landroid/os/INetworkManagementService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1710
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mNms:Landroid/os/INetworkManagementService;

    invoke-interface {v0, p1}, Landroid/os/INetworkManagementService;->closeSocketForAurogon([I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/ServiceSpecificException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1714
    :cond_0
    goto :goto_0

    .line 1712
    :catch_0
    move-exception v0

    .line 1713
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "GreezeManager"

    const-string v2, "failed to close socket for aurogon!"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1715
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public dealAdjSet(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "cmd"    # I

    .line 1083
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/miui/server/greeze/GreezeManagerService$4;-><init>(Lcom/miui/server/greeze/GreezeManagerService;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1102
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 2904
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    const-string v1, "GreezeManager"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 2905
    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->dumpSettings(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    .line 2907
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->dumpFreezeAction(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2908
    array-length v1, p3

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    aget-object v1, p3, v1

    const-string v2, "old"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2909
    invoke-virtual {p0, v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->dumpHistory(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    .line 2910
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2911
    .local v1, "temp":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " FreeformSmallWin uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2912
    .end local v1    # "temp":Ljava/lang/String;
    goto :goto_0

    .line 2913
    :cond_2
    return-void
.end method

.method dumpFrozen(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .locals 10
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 2876
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_0

    .line 2877
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozonTids()Ljava/util/List;

    move-result-object v0

    .line 2878
    .local v0, "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Frozen tids: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2879
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v0

    .line 2880
    .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_0

    .line 2881
    .end local v0    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenNewPids()Ljava/util/List;

    move-result-object v0

    .line 2882
    .restart local v0    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Frozen pids: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2884
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm:ss.SSS"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2885
    .local v1, "formater":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Frozen processes:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2886
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v2

    .line 2887
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 2888
    .local v3, "n":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v3, :cond_2

    .line 2889
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 2890
    .local v5, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2891
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " pid="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2892
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_2
    iget-object v7, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 2893
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "    "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v7, "fz: "

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2894
    new-instance v7, Ljava/util/Date;

    iget-object v8, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2895
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeReasons:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2896
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2892
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2888
    .end local v5    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v6    # "index":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 2899
    .end local v3    # "n":I
    .end local v4    # "i":I
    :cond_2
    monitor-exit v2

    .line 2900
    return-void

    .line 2899
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method dumpHistory(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .locals 10
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 2839
    const-string v0, "Frozen processes in history:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2840
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xdbba00

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getHistoryInfos(J)Ljava/util/List;

    move-result-object v0

    .line 2841
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
    const/4 v1, 0x1

    .line 2842
    .local v1, "index":I
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm:ss.SSS"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2843
    .local v2, "formater":Ljava/text/SimpleDateFormat;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 2844
    .local v4, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "#"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    .end local v1    # "index":I
    .local v6, "index":I
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2845
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v7, Ljava/util/Date;

    iget-wide v8, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2846
    iget v1, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2847
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2848
    iget-object v1, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->processName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2849
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getFrozenDuration()J

    move-result-wide v7

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "ms"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2850
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const-string v8, "    "

    if-ge v1, v7, :cond_2

    .line 2851
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v7, "fz: "

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2852
    new-instance v7, Ljava/util/Date;

    iget-object v8, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2853
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeReasons:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2854
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2850
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2856
    .end local v1    # "i":I
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string/jumbo v1, "th: "

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2857
    new-instance v1, Ljava/util/Date;

    iget-wide v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J

    invoke-direct {v1, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2858
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawReason:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2859
    .end local v4    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    move v1, v6

    goto/16 :goto_0

    .line 2860
    .end local v6    # "index":I
    .local v1, "index":I
    :cond_3
    return-void
.end method

.method dumpSettings(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 2863
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Settings:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2864
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "persist.sys.powmillet.enable"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2865
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  debug="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "persist.sys.gz.debug"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2866
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  monkey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MONKEY:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "persist.sys.gz.monkey"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2867
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  fz_timeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-wide v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "persist.sys.gz.fztimeout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2868
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  monitor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2869
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    monitor-enter v0

    .line 2870
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  mBroadcastTargetWhiteList="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2871
    monitor-exit v0

    .line 2872
    return-void

    .line 2871
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public finishLaunchMode(Ljava/lang/String;I)V
    .locals 0
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1352
    return-void
.end method

.method public forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 1763
    return-void
.end method

.method public freezeAction(IILjava/lang/String;Z)Z
    .locals 17
    .param p1, "uid"    # I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "isNeedCompact"    # Z

    .line 1117
    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z

    move-result v0

    const/4 v5, 0x0

    if-nez v0, :cond_0

    .line 1118
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppShowOnWindows(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1119
    return v5

    .line 1131
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v6, v0

    .line 1132
    .local v6, "log":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .line 1133
    .local v7, "done":Z
    iget-object v8, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonLock:Ljava/lang/Object;

    monitor-enter v8

    .line 1135
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1136
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 1137
    const-string v0, "Greeze"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "uid = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "has be freeze"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    :cond_1
    monitor-exit v8

    return v5

    .line 1141
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "FZ uid = "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->readPidFromCgroup(I)Ljava/util/List;

    move-result-object v0

    move-object v9, v0

    .line 1143
    .local v9, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    monitor-exit v8

    return v7

    .line 1144
    :cond_3
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getGameUids()Ljava/util/Set;

    move-result-object v0

    move-object v10, v0

    .line 1145
    .local v10, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1146
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v11, 0x7

    invoke-virtual {v0, v11, v5, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1147
    :cond_4
    const-string v0, " pid = [ "

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1148
    const/4 v5, 0x1

    .line 1149
    .local v5, "isCgroupPidError":Z
    iget-object v11, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 1150
    :try_start_1
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 1152
    .local v12, "pid":I
    invoke-virtual {v1, v2, v12}, Lcom/miui/server/greeze/GreezeManagerService;->readPidStatus(II)Z

    move-result v14

    if-nez v14, :cond_5

    .line 1154
    goto :goto_0

    .line 1156
    :cond_5
    const/4 v5, 0x0

    .line 1157
    invoke-static {v12, v2}, Lcom/miui/server/greeze/FreezeUtils;->freezePid(II)Z

    move-result v14

    move v7, v14

    .line 1158
    if-eqz v7, :cond_a

    .line 1159
    iget-object v14, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v14, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1160
    .local v14, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-nez v14, :cond_6

    .line 1161
    :try_start_2
    new-instance v15, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    invoke-direct {v15, v2, v12}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;-><init>(II)V

    move-object v14, v15

    .line 1162
    iget-object v15, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v15, v12, v14}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v16, v0

    goto :goto_1

    .line 1189
    .end local v12    # "pid":I
    .end local v14    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :catchall_0
    move-exception v0

    move-object v13, v9

    move-object v15, v10

    goto/16 :goto_5

    .line 1163
    .restart local v12    # "pid":I
    .restart local v14    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_6
    :try_start_3
    iget v15, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eq v2, v15, :cond_7

    .line 1164
    :try_start_4
    const-string v15, "GreezeManager"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v0

    const-string/jumbo v0, "uid-pid mismatch old uid = "

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v13, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v13, " pid = "

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v13, " new uid = "

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v15, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1165
    iput v2, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1163
    :cond_7
    move-object/from16 v16, v0

    .line 1167
    :goto_1
    move-object v13, v9

    move-object v15, v10

    .end local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v13, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v15, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v14, v9, v10, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V

    .line 1168
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v0, :cond_8

    .line 1169
    const/4 v0, 0x1

    iput-boolean v0, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z

    goto :goto_2

    .line 1170
    :cond_8
    const/4 v0, 0x1

    and-int/lit8 v9, v3, 0x10

    if-eqz v9, :cond_9

    .line 1171
    iput-boolean v0, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    .line 1173
    :cond_9
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1184
    nop

    .end local v14    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    goto :goto_3

    .line 1186
    .end local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_a
    move-object/from16 v16, v0

    move-object v13, v9

    move-object v15, v10

    .end local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const-string v0, "AurogonImmobulusMode"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " Freeze uid = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " pid = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " error !"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    .end local v12    # "pid":I
    :goto_3
    move-object v9, v13

    move-object v10, v15

    move-object/from16 v0, v16

    goto/16 :goto_0

    .line 1189
    .end local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_b
    move-object v13, v9

    move-object v15, v10

    .end local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1190
    if-eqz v5, :cond_c

    .line 1192
    :try_start_6
    const-string v0, "AurogonImmobulusMode"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " Freeze uid = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " error due pid-uid mismatch"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    monitor-exit v8

    const/4 v0, 0x1

    return v0

    .line 1196
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "] reason : "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, " caller : "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1197
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->PID_DEBUG:Z

    if-eqz v0, :cond_e

    .line 1198
    :cond_d
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1200
    .end local v5    # "isCgroupPidError":Z
    .end local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_e
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 1203
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-eqz v0, :cond_f

    .line 1204
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->closeSocketForAurogon(I)V

    .line 1205
    const/4 v0, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V

    goto :goto_4

    .line 1206
    :cond_f
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isNeedRestictNetworkPolicy(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1208
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V

    .line 1211
    :cond_10
    :goto_4
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V

    .line 1212
    const/4 v0, 0x1

    invoke-direct {v1, v2, v0}, Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V

    .line 1213
    return v7

    .line 1189
    .restart local v5    # "isCgroupPidError":Z
    .restart local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_1
    move-exception v0

    move-object v13, v9

    move-object v15, v10

    .end local v9    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v10    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_5
    :try_start_7
    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .end local v6    # "log":Ljava/lang/StringBuilder;
    .end local v7    # "done":Z
    .end local p0    # "this":Lcom/miui/server/greeze/GreezeManagerService;
    .end local p1    # "uid":I
    .end local p2    # "fromWho":I
    .end local p3    # "reason":Ljava/lang/String;
    .end local p4    # "isNeedCompact":Z
    :try_start_8
    throw v0

    .restart local v6    # "log":Ljava/lang/StringBuilder;
    .restart local v7    # "done":Z
    .restart local p0    # "this":Lcom/miui/server/greeze/GreezeManagerService;
    .restart local p1    # "uid":I
    .restart local p2    # "fromWho":I
    .restart local p3    # "reason":Ljava/lang/String;
    .restart local p4    # "isNeedCompact":Z
    :catchall_2
    move-exception v0

    goto :goto_5

    .line 1200
    .end local v5    # "isCgroupPidError":Z
    .end local v13    # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v15    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_3
    move-exception v0

    monitor-exit v8
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0
.end method

.method public freezePids([IJILjava/lang/String;)Ljava/util/List;
    .locals 18
    .param p1, "pids"    # [I
    .param p2, "timeout"    # J
    .param p4, "fromWho"    # I
    .param p5, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 893
    move-object/from16 v0, p1

    move-wide/from16 v7, p2

    move/from16 v9, p4

    move-object/from16 v10, p5

    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 894
    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    const-string v11, "GreezeManager"

    const-string v12, ", "

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AIDL freezePids("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 896
    :cond_0
    if-nez v0, :cond_1

    .line 897
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1

    .line 899
    :cond_1
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v13

    .line 900
    .local v13, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v14, v1

    .line 901
    .local v14, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    array-length v15, v0

    const/4 v1, 0x0

    move v6, v1

    :goto_0
    if-ge v6, v15, :cond_7

    aget v5, v0, v6

    .line 902
    .local v5, "pid":I
    const/4 v1, 0x0

    .line 903
    .local v1, "target":Lcom/miui/server/greeze/RunningProcess;
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v3, v1

    .end local v1    # "target":Lcom/miui/server/greeze/RunningProcess;
    .local v3, "target":Lcom/miui/server/greeze/RunningProcess;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/greeze/RunningProcess;

    .line 904
    .local v1, "proc":Lcom/miui/server/greeze/RunningProcess;
    iget v4, v1, Lcom/miui/server/greeze/RunningProcess;->pid:I

    if-ne v5, v4, :cond_2

    .line 905
    move-object v3, v1

    .line 907
    .end local v1    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :cond_2
    goto :goto_1

    .line 908
    :cond_3
    if-nez v3, :cond_4

    .line 909
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to freeze invalid pid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    move/from16 v17, v6

    move-object/from16 v16, v13

    goto :goto_2

    .line 912
    :cond_4
    move-object/from16 v1, p0

    move-object v2, v3

    move-object v0, v3

    .end local v3    # "target":Lcom/miui/server/greeze/RunningProcess;
    .local v0, "target":Lcom/miui/server/greeze/RunningProcess;
    move-wide/from16 v3, p2

    move-object/from16 v16, v13

    move v13, v5

    .end local v5    # "pid":I
    .local v13, "pid":I
    .local v16, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    move/from16 v5, p4

    move/from16 v17, v6

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/greeze/GreezeManagerService;->freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 914
    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AIDL freezePid("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") failed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v11, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 917
    :cond_5
    iget v1, v0, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 901
    .end local v0    # "target":Lcom/miui/server/greeze/RunningProcess;
    .end local v13    # "pid":I
    :cond_6
    :goto_2
    add-int/lit8 v6, v17, 0x1

    move-object/from16 v0, p1

    move-object/from16 v13, v16

    goto/16 :goto_0

    .line 921
    .end local v16    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    .local v13, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    :cond_7
    move-object/from16 v16, v13

    .end local v13    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    .restart local v16    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v0, :cond_8

    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AIDL freezePids result: frozen "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    :cond_8
    return-object v14
.end method

.method public freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z
    .locals 8
    .param p1, "proc"    # Lcom/miui/server/greeze/RunningProcess;
    .param p2, "timeout"    # J
    .param p4, "fromWho"    # I
    .param p5, "reason"    # Ljava/lang/String;

    .line 859
    iget v0, p1, Lcom/miui/server/greeze/RunningProcess;->pid:I

    .line 860
    .local v0, "pid":I
    if-lez v0, :cond_5

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    if-eq v1, v0, :cond_5

    invoke-static {v0}, Landroid/os/Process;->getUidForPid(I)I

    move-result v1

    iget v2, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I

    if-eq v1, v2, :cond_0

    goto/16 :goto_1

    .line 863
    :cond_0
    const/4 v1, 0x0

    .line 864
    .local v1, "done":Z
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v2, :cond_1

    .line 865
    invoke-static {v0}, Lcom/miui/server/greeze/FreezeUtils;->freezePid(I)Z

    move-result v1

    goto :goto_0

    .line 867
    :cond_1
    iget v2, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I

    invoke-static {v0, v2}, Lcom/miui/server/greeze/FreezeUtils;->freezePid(II)Z

    move-result v1

    .line 868
    :goto_0
    const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v2}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget v3, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I

    invoke-interface {v2, v3, v0}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->addFrozenPid(II)V

    .line 869
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v2

    .line 870
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 871
    .local v3, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-eqz v3, :cond_2

    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    iget v5, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I

    if-eq v4, v5, :cond_2

    .line 872
    const-string v4, "GreezeManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "freeze uid-pid mismatch "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 873
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->remove(I)V

    .line 875
    :cond_2
    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    invoke-direct {v4, p1}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;-><init>(Lcom/miui/server/greeze/RunningProcess;)V

    move-object v3, v4

    .line 876
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v4, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 877
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5, p4, p5}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V

    .line 878
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 879
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 881
    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v4, p2, v6

    if-eqz v4, :cond_4

    .line 882
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    .line 883
    .local v4, "msg":Landroid/os/Message;
    iput v0, v4, Landroid/os/Message;->arg1:I

    .line 884
    iput p4, v4, Landroid/os/Message;->arg2:I

    .line 885
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v4, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 887
    .end local v3    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v4    # "msg":Landroid/os/Message;
    :cond_4
    monitor-exit v2

    .line 888
    return v1

    .line 887
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 861
    .end local v1    # "done":Z
    :cond_5
    :goto_1
    const/4 v1, 0x0

    return v1
.end method

.method public freezeThread(I)V
    .locals 1
    .param p1, "tid"    # I

    .line 854
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_0

    .line 855
    invoke-static {p1}, Lcom/miui/server/greeze/FreezeUtils;->freezeTid(I)Z

    .line 856
    :cond_0
    return-void
.end method

.method public freezeUids([IJILjava/lang/String;Z)Ljava/util/List;
    .locals 25
    .param p1, "uids"    # [I
    .param p2, "timeout"    # J
    .param p4, "fromWho"    # I
    .param p5, "reason"    # Ljava/lang/String;
    .param p6, "checkAudioGps"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJI",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 927
    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move/from16 v9, p4

    move-object/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 928
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v0, :cond_0

    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AIDL freezeUids("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-wide/from16 v12, p2

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    move-wide/from16 v12, p2

    .line 930
    :goto_0
    if-eqz v8, :cond_23

    iget-object v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z

    if-eqz v0, :cond_1

    move-object v2, v7

    goto/16 :goto_12

    .line 933
    :cond_1
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getUidMap()Landroid/util/SparseArray;

    move-result-object v14

    .line 934
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v15, v0

    .line 935
    .local v15, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getGameUids()Ljava/util/Set;

    move-result-object v6

    .line 936
    .local v6, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v5, v0

    .line 937
    .local v5, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    array-length v0, v8

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    aget v2, v8, v1

    .line 938
    .local v2, "u":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 937
    .end local v2    # "u":I
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 940
    :cond_2
    iget-object v1, v7, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 941
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    :try_start_0
    iget-object v2, v7, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_d

    const/4 v4, 0x1

    if-ge v0, v2, :cond_6

    .line 942
    :try_start_1
    iget-object v2, v7, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 943
    .local v2, "inf":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-nez v2, :cond_3

    goto :goto_3

    .line 944
    :cond_3
    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-ne v9, v4, :cond_4

    .line 945
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4, v9, v10}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V

    .line 946
    goto :goto_3

    .line 948
    :cond_4
    if-eqz v2, :cond_5

    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 949
    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 950
    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 951
    if-ne v9, v4, :cond_5

    .line 952
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4, v9, v10}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 941
    .end local v2    # "inf":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 955
    .end local v0    # "i":I
    :catchall_0
    move-exception v0

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    goto/16 :goto_11

    :cond_6
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_d

    .line 956
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 957
    .local v3, "uid":I
    invoke-virtual {v14, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, Ljava/util/List;

    .line 958
    .local v18, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    if-nez v18, :cond_7

    .line 959
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to freeze invalid uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    goto :goto_4

    .line 963
    :cond_7
    invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-boolean v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v0, :cond_8

    .line 964
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Skit uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reason : FgApp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    goto :goto_4

    .line 968
    :cond_8
    iget-object v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isIMEApp(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 969
    const-string v0, "Aurogon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was IME app, skip it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 970
    goto/16 :goto_4

    .line 974
    :cond_9
    iget-boolean v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v0, :cond_a

    iget-object v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWidgetApp(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 975
    goto/16 :goto_4

    .line 978
    :cond_a
    invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkFreeformSmallWin(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 979
    invoke-direct {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkStateForScrOff(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 980
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add small win uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 982
    :cond_b
    const-string v0, "Aurogon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was Freeform small window, skip it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    goto/16 :goto_4

    .line 987
    :cond_c
    :goto_5
    if-eqz v11, :cond_d

    invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isUidActive(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 988
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is using audio or GPS or vibrator, won\'t freeze it, skip it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    goto/16 :goto_4

    .line 991
    :cond_d
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->isBackuping(I)Z

    move-result v0

    if-nez v0, :cond_20

    .line 992
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->isActiveInstruUid(I)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 993
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->isVibratorActive(I)Z

    move-result v0

    if-eqz v0, :cond_e

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    const/16 v19, 0x0

    move v14, v3

    move v3, v4

    goto/16 :goto_10

    .line 997
    :cond_e
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getActivityControllerUid()I

    move-result v0

    if-ne v0, v3, :cond_f

    .line 998
    invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 999
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_f

    iget-object v1, v7, Lcom/miui/server/greeze/GreezeManagerService;->mActivityCtrlBlackList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1000
    const-string v1, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uid "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is activityc"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 1005
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_f
    iget-object v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    if-eqz v0, :cond_11

    .line 1007
    :try_start_3
    invoke-interface {v0, v3}, Landroid/view/IWindowManager;->checkAppOnWindowsStatus(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1008
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was show on screen, skip it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1009
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 1013
    :cond_10
    goto :goto_6

    .line 1011
    :catch_0
    move-exception v0

    .line 1017
    :cond_11
    :goto_6
    iget-object v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    if-eqz v0, :cond_12

    .line 1018
    invoke-virtual {v0, v3}, Landroid/hardware/display/DisplayManagerInternal;->isUsingVirtualDisplay(I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1019
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was using virtual display, skip it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    const/4 v4, 0x1

    goto/16 :goto_4

    .line 1024
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v4, v0

    .line 1025
    .local v4, "success":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v2, v0

    .line 1026
    .local v2, "failed":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v1, v0

    .line 1027
    .local v1, "log":Ljava/lang/StringBuilder;
    iget-object v8, v7, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonLock:Ljava/lang/Object;

    monitor-enter v8

    .line 1028
    :try_start_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_b

    move-object/from16 v20, v2

    .end local v2    # "failed":Ljava/lang/StringBuilder;
    .local v20, "failed":Ljava/lang/StringBuilder;
    :try_start_5
    const-string v2, "FZ uid = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1029
    const/4 v0, 0x0

    .line 1030
    .local v0, "skipAdj":Z
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v21
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_a

    if-eqz v21, :cond_16

    :try_start_6
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/miui/server/greeze/RunningProcess;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object/from16 v22, v21

    .line 1031
    .local v22, "proc":Lcom/miui/server/greeze/RunningProcess;
    move/from16 v21, v0

    move-object/from16 v0, v22

    move-object/from16 v22, v1

    .end local v1    # "log":Ljava/lang/StringBuilder;
    .local v0, "proc":Lcom/miui/server/greeze/RunningProcess;
    .local v21, "skipAdj":Z
    .local v22, "log":Ljava/lang/StringBuilder;
    :try_start_7
    iget v1, v0, Lcom/miui/server/greeze/RunningProcess;->adj:I

    if-nez v1, :cond_15

    .line 1032
    iget-boolean v1, v7, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v1, :cond_13

    invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v1

    if-nez v1, :cond_13

    .line 1033
    const-string v1, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v23, v0

    .end local v0    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .local v23, "proc":Lcom/miui/server/greeze/RunningProcess;
    const-string v0, "FZ uid = "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " adj"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    const/4 v0, 0x1

    .line 1035
    .end local v21    # "skipAdj":Z
    .local v0, "skipAdj":Z
    goto :goto_9

    .line 1032
    .end local v23    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .local v0, "proc":Lcom/miui/server/greeze/RunningProcess;
    .restart local v21    # "skipAdj":Z
    :cond_13
    move-object/from16 v23, v0

    .line 1036
    .end local v0    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .restart local v23    # "proc":Lcom/miui/server/greeze/RunningProcess;
    invoke-direct {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkStateForScrOff(I)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1037
    const/4 v1, 0x0

    invoke-virtual {v7, v3, v1}, Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V

    .line 1038
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v24, v2

    const-string v2, "add uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " adj"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 1036
    :cond_14
    move-object/from16 v24, v2

    goto :goto_8

    .line 1031
    .end local v23    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .restart local v0    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :cond_15
    move-object/from16 v23, v0

    move-object/from16 v24, v2

    .line 1041
    .end local v0    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :goto_8
    move/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    goto :goto_7

    .line 1067
    .end local v21    # "skipAdj":Z
    .end local v22    # "log":Ljava/lang/StringBuilder;
    .restart local v1    # "log":Ljava/lang/StringBuilder;
    :catchall_1
    move-exception v0

    move-object v11, v1

    move-object v1, v4

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v20, v14

    move v14, v3

    .end local v1    # "log":Ljava/lang/StringBuilder;
    .restart local v22    # "log":Ljava/lang/StringBuilder;
    goto/16 :goto_f

    .line 1030
    .end local v22    # "log":Ljava/lang/StringBuilder;
    .local v0, "skipAdj":Z
    .restart local v1    # "log":Ljava/lang/StringBuilder;
    :cond_16
    move/from16 v21, v0

    move-object/from16 v22, v1

    .line 1042
    .end local v1    # "log":Ljava/lang/StringBuilder;
    .restart local v22    # "log":Ljava/lang/StringBuilder;
    :goto_9
    if-eqz v0, :cond_17

    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object/from16 v8, p1

    const/4 v4, 0x1

    goto/16 :goto_4

    .line 1067
    .end local v0    # "skipAdj":Z
    :catchall_2
    move-exception v0

    move-object v1, v4

    move-object/from16 v21, v5

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v11, v22

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    goto/16 :goto_f

    .line 1043
    .restart local v0    # "skipAdj":Z
    :cond_17
    :try_start_8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_9

    if-eqz v1, :cond_18

    .line 1044
    :try_start_9
    iget-object v1, v7, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    const/4 v2, 0x7

    move-object/from16 v21, v4

    const/4 v4, 0x0

    .end local v4    # "success":Ljava/lang/StringBuilder;
    .local v21, "success":Ljava/lang/StringBuilder;
    :try_start_a
    invoke-virtual {v1, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto :goto_a

    .line 1067
    .end local v0    # "skipAdj":Z
    :catchall_3
    move-exception v0

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v1, v21

    move-object/from16 v11, v22

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    goto/16 :goto_f

    .end local v21    # "success":Ljava/lang/StringBuilder;
    .restart local v4    # "success":Ljava/lang/StringBuilder;
    :catchall_4
    move-exception v0

    move-object v1, v4

    move-object/from16 v21, v5

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v11, v22

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    .end local v4    # "success":Ljava/lang/StringBuilder;
    .restart local v21    # "success":Ljava/lang/StringBuilder;
    goto/16 :goto_f

    .line 1043
    .end local v21    # "success":Ljava/lang/StringBuilder;
    .restart local v0    # "skipAdj":Z
    .restart local v4    # "success":Ljava/lang/StringBuilder;
    :cond_18
    move-object/from16 v21, v4

    const/4 v4, 0x0

    .line 1045
    .end local v4    # "success":Ljava/lang/StringBuilder;
    .restart local v21    # "success":Ljava/lang/StringBuilder;
    :goto_a
    :try_start_b
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_8

    if-eqz v1, :cond_1b

    :try_start_c
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/greeze/RunningProcess;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    move-object v2, v1

    .line 1046
    .local v2, "proc":Lcom/miui/server/greeze/RunningProcess;
    move-object/from16 v11, v22

    .end local v22    # "log":Ljava/lang/StringBuilder;
    .local v11, "log":Ljava/lang/StringBuilder;
    move-object/from16 v1, p0

    move-object v13, v2

    move-object/from16 v12, v20

    .end local v2    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .end local v20    # "failed":Ljava/lang/StringBuilder;
    .local v12, "failed":Ljava/lang/StringBuilder;
    .local v13, "proc":Lcom/miui/server/greeze/RunningProcess;
    move/from16 v19, v4

    move-object/from16 v20, v14

    move-object/from16 v7, v21

    move v14, v3

    .end local v3    # "uid":I
    .end local v21    # "success":Ljava/lang/StringBuilder;
    .local v7, "success":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    move-wide/from16 v3, p2

    move-object/from16 v21, v5

    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move/from16 v5, p4

    move-object/from16 v22, v6

    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object/from16 v6, p5

    :try_start_d
    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/greeze/GreezeManagerService;->freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 1047
    const-string v1, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FZ uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pid ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " failed!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_c

    .line 1050
    :cond_19
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1a

    .line 1051
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1052
    :cond_1a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    .line 1054
    .end local v13    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :goto_c
    move v3, v14

    move/from16 v4, v19

    move-object/from16 v14, v20

    move-object/from16 v5, v21

    move-object/from16 v6, v22

    move-object/from16 v21, v7

    move-object/from16 v22, v11

    move-object/from16 v20, v12

    move-object/from16 v7, p0

    move-wide/from16 v12, p2

    move/from16 v11, p6

    goto/16 :goto_b

    .line 1067
    .end local v0    # "skipAdj":Z
    :catchall_5
    move-exception v0

    move-object/from16 v2, p0

    move-object v1, v7

    goto/16 :goto_f

    .end local v7    # "success":Ljava/lang/StringBuilder;
    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .restart local v3    # "uid":I
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v20, "failed":Ljava/lang/StringBuilder;
    .local v21, "success":Ljava/lang/StringBuilder;
    .local v22, "log":Ljava/lang/StringBuilder;
    :catchall_6
    move-exception v0

    move-object/from16 v12, v20

    move-object/from16 v7, v21

    move-object/from16 v11, v22

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    move-object/from16 v2, p0

    move-object v1, v7

    .end local v3    # "uid":I
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v7    # "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    goto/16 :goto_f

    .line 1055
    .end local v7    # "success":Ljava/lang/StringBuilder;
    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .restart local v0    # "skipAdj":Z
    .restart local v3    # "uid":I
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v20, "failed":Ljava/lang/StringBuilder;
    .local v21, "success":Ljava/lang/StringBuilder;
    .local v22, "log":Ljava/lang/StringBuilder;
    :cond_1b
    move/from16 v19, v4

    move-object/from16 v12, v20

    move-object/from16 v7, v21

    move-object/from16 v11, v22

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    .end local v3    # "uid":I
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v7    # "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :try_start_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " pid = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1056
    const-string v1, ""

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    if-nez v1, :cond_1c

    .line 1057
    :try_start_f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed = [ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    goto :goto_d

    .line 1059
    :cond_1c
    :try_start_10
    const-string v1, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FZ uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reason ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " success !"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    :goto_d
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " reason : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " caller : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    .line 1062
    move-object/from16 v2, p0

    move-object v1, v7

    .end local v7    # "success":Ljava/lang/StringBuilder;
    .local v1, "success":Ljava/lang/StringBuilder;
    :try_start_11
    iget-object v3, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v3, v10}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1d

    .line 1063
    iget-object v3, v2, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    goto :goto_e

    .line 1065
    :cond_1d
    const-string v3, "GreezeManager"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    .end local v0    # "skipAdj":Z
    :goto_e
    monitor-exit v8
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_c

    .line 1068
    iget-object v0, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v14}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isNeedRestictNetworkPolicy(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1069
    invoke-virtual {v2, v14}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V

    .line 1071
    :cond_1e
    const/4 v3, 0x1

    invoke-direct {v2, v14, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V

    .line 1072
    .end local v1    # "success":Ljava/lang/StringBuilder;
    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .end local v14    # "uid":I
    .end local v18    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    move-object/from16 v8, p1

    move-wide/from16 v12, p2

    move/from16 v11, p6

    move-object v7, v2

    move v4, v3

    move-object/from16 v14, v20

    move-object/from16 v5, v21

    move-object/from16 v6, v22

    goto/16 :goto_4

    .line 1067
    .restart local v7    # "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .restart local v14    # "uid":I
    .restart local v18    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    :catchall_7
    move-exception v0

    move-object/from16 v2, p0

    move-object v1, v7

    .end local v7    # "success":Ljava/lang/StringBuilder;
    .restart local v1    # "success":Ljava/lang/StringBuilder;
    goto :goto_f

    .end local v1    # "success":Ljava/lang/StringBuilder;
    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .restart local v3    # "uid":I
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v20, "failed":Ljava/lang/StringBuilder;
    .local v21, "success":Ljava/lang/StringBuilder;
    .local v22, "log":Ljava/lang/StringBuilder;
    :catchall_8
    move-exception v0

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v1, v21

    move-object/from16 v11, v22

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    .end local v3    # "uid":I
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v1    # "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    goto :goto_f

    .end local v1    # "success":Ljava/lang/StringBuilder;
    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v3    # "uid":I
    .restart local v4    # "success":Ljava/lang/StringBuilder;
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v20, "failed":Ljava/lang/StringBuilder;
    .local v22, "log":Ljava/lang/StringBuilder;
    :catchall_9
    move-exception v0

    move-object v1, v4

    move-object/from16 v21, v5

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v11, v22

    move-object/from16 v22, v6

    move-object/from16 v20, v14

    move v14, v3

    .end local v3    # "uid":I
    .end local v4    # "success":Ljava/lang/StringBuilder;
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v1    # "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    goto :goto_f

    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v1, "log":Ljava/lang/StringBuilder;
    .restart local v3    # "uid":I
    .restart local v4    # "success":Ljava/lang/StringBuilder;
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v20, "failed":Ljava/lang/StringBuilder;
    :catchall_a
    move-exception v0

    move-object v11, v1

    move-object v1, v4

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v12, v20

    move-object/from16 v20, v14

    move v14, v3

    .end local v3    # "uid":I
    .end local v4    # "success":Ljava/lang/StringBuilder;
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v1, "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    goto :goto_f

    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .end local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v1, "log":Ljava/lang/StringBuilder;
    .local v2, "failed":Ljava/lang/StringBuilder;
    .restart local v3    # "uid":I
    .restart local v4    # "success":Ljava/lang/StringBuilder;
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    :catchall_b
    move-exception v0

    move-object v11, v1

    move-object v12, v2

    move-object v1, v4

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    move v14, v3

    .end local v2    # "failed":Ljava/lang/StringBuilder;
    .end local v3    # "uid":I
    .end local v4    # "success":Ljava/lang/StringBuilder;
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v1, "success":Ljava/lang/StringBuilder;
    .restart local v11    # "log":Ljava/lang/StringBuilder;
    .restart local v12    # "failed":Ljava/lang/StringBuilder;
    .local v14, "uid":I
    .restart local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_f
    :try_start_12
    monitor-exit v8
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_c

    throw v0

    :catchall_c
    move-exception v0

    goto :goto_f

    .line 992
    .end local v1    # "success":Ljava/lang/StringBuilder;
    .end local v11    # "log":Ljava/lang/StringBuilder;
    .end local v12    # "failed":Ljava/lang/StringBuilder;
    .end local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v3    # "uid":I
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    :cond_1f
    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    const/16 v19, 0x0

    move v14, v3

    move v3, v4

    .end local v3    # "uid":I
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uid":I
    .restart local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    goto :goto_10

    .line 991
    .end local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v3    # "uid":I
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    :cond_20
    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    const/16 v19, 0x0

    move v14, v3

    move v3, v4

    .line 994
    .end local v3    # "uid":I
    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uid":I
    .restart local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_10
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uid "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is backing, instr or vibrator"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    move-object/from16 v8, p1

    move-wide/from16 v12, p2

    move/from16 v11, p6

    move-object v7, v2

    move v4, v3

    move-object/from16 v14, v20

    move-object/from16 v5, v21

    move-object/from16 v6, v22

    goto/16 :goto_4

    .line 1073
    .end local v18    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    .end local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    :cond_21
    move v3, v4

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v14    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-ne v9, v3, :cond_22

    .line 1074
    invoke-direct {v2, v15, v3}, Lcom/miui/server/greeze/GreezeManagerService;->setWakeLockState(Ljava/util/List;Z)V

    .line 1075
    :cond_22
    return-object v15

    .line 955
    .end local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v14    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    :catchall_d
    move-exception v0

    move-object/from16 v21, v5

    move-object/from16 v22, v6

    move-object v2, v7

    move-object/from16 v20, v14

    .end local v5    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v6    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v14    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :goto_11
    :try_start_13
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_e

    throw v0

    :catchall_e
    move-exception v0

    goto :goto_11

    .line 930
    .end local v15    # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v20    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v21    # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v22    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_23
    move-object v2, v7

    .line 931
    :goto_12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getBroadcastConfig()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 2324
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlMap:Ljava/util/Map;

    return-object v0
.end method

.method public getBroadcastCtrl()Z
    .locals 1

    .line 2317
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z

    return v0
.end method

.method public getFrozenPids(I)[I
    .locals 5
    .param p1, "module"    # I

    .line 1630
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v0, :cond_0

    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AIDL getFrozenPids("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1631
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 1654
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0

    .line 1634
    :sswitch_0
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_1

    .line 1635
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v0

    .local v0, "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_0

    .line 1637
    .end local v0    # "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenNewPids()Ljava/util/List;

    move-result-object v0

    .line 1638
    .restart local v0    # "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->toArray(Ljava/util/List;)[I

    move-result-object v1

    return-object v1

    .line 1642
    .end local v0    # "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :sswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1643
    .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 1644
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 1645
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1646
    .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget-object v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_2

    .line 1647
    invoke-virtual {v3}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I

    move-result v4

    if-ne v4, p1, :cond_2

    .line 1648
    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1644
    .end local v3    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1651
    .end local v2    # "i":I
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1652
    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->toArray(Ljava/util/List;)[I

    move-result-object v1

    return-object v1

    .line 1651
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x270f -> :sswitch_0
    .end sparse-switch
.end method

.method public getFrozenUids(I)[I
    .locals 5
    .param p1, "module"    # I

    .line 1659
    sparse-switch p1, :sswitch_data_0

    .line 1683
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0

    .line 1664
    :sswitch_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1665
    .local v0, "uids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 1666
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 1667
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1668
    .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    const/16 v4, 0x270f

    if-eq p1, v4, :cond_0

    iget-object v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_1

    .line 1669
    invoke-virtual {v3}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I

    move-result v4

    if-ne v4, p1, :cond_1

    .line 1670
    :cond_0
    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1666
    .end local v3    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1673
    .end local v2    # "i":I
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1674
    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [I

    .line 1675
    .local v1, "rst":[I
    const/4 v2, 0x0

    .line 1676
    .local v2, "index":I
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1677
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1678
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v1, v2

    .line 1679
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1681
    :cond_3
    return-object v1

    .line 1673
    .end local v1    # "rst":[I
    .end local v2    # "index":I
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x270f -> :sswitch_0
    .end sparse-switch
.end method

.method public getLastThawedTime(II)J
    .locals 5
    .param p1, "uid"    # I
    .param p2, "module"    # I

    .line 687
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xdbba00

    sub-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getHistoryInfos(J)Ljava/util/List;

    move-result-object v0

    .line 688
    .local v0, "historyInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 689
    .local v2, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    if-ne v3, p1, :cond_0

    .line 690
    iget-wide v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawUptime:J

    return-wide v3

    .line 691
    .end local v2    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_0
    goto :goto_0

    .line 692
    :cond_1
    const-wide/16 v1, -0x1

    return-wide v1
.end method

.method getPackageNameFromUid(I)Ljava/lang/String;
    .locals 3
    .param p1, "uid"    # I

    .line 2138
    const/4 v0, 0x0

    .line 2140
    .local v0, "packName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mPm:Landroid/content/pm/PackageManager;

    if-nez v1, :cond_0

    .line 2141
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mPm:Landroid/content/pm/PackageManager;

    .line 2144
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mPm:Landroid/content/pm/PackageManager;

    if-eqz v1, :cond_1

    .line 2145
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 2148
    :cond_1
    if-nez v0, :cond_2

    .line 2149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get caller pkgname failed uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GreezeManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2151
    :cond_2
    return-object v0
.end method

.method getPkgMap()Lcom/android/internal/app/ProcessMap;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/internal/app/ProcessMap<",
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/RunningProcess;",
            ">;>;"
        }
    .end annotation

    .line 814
    new-instance v0, Lcom/android/internal/app/ProcessMap;

    invoke-direct {v0}, Lcom/android/internal/app/ProcessMap;-><init>()V

    .line 815
    .local v0, "map":Lcom/android/internal/app/ProcessMap;, "Lcom/android/internal/app/ProcessMap<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v1

    .line 816
    .local v1, "procList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/RunningProcess;

    .line 817
    .local v3, "proc":Lcom/miui/server/greeze/RunningProcess;
    iget v4, v3, Lcom/miui/server/greeze/RunningProcess;->uid:I

    .line 818
    .local v4, "uid":I
    iget-object v5, v3, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    if-nez v5, :cond_0

    goto :goto_0

    .line 819
    :cond_0
    iget-object v5, v3, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    array-length v6, v5

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v6, :cond_2

    aget-object v8, v5, v7

    .line 820
    .local v8, "packageName":Ljava/lang/String;
    invoke-virtual {v0, v8, v4}, Lcom/android/internal/app/ProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 821
    .local v9, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    if-nez v9, :cond_1

    .line 822
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object v9, v10

    .line 823
    invoke-virtual {v0, v8, v4, v9}, Lcom/android/internal/app/ProcessMap;->put(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;

    .line 825
    :cond_1
    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 819
    .end local v8    # "packageName":Ljava/lang/String;
    .end local v9    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 827
    .end local v3    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .end local v4    # "uid":I
    :cond_2
    goto :goto_0

    .line 828
    :cond_3
    return-object v0
.end method

.method getProcessByPid(I)Lcom/miui/server/greeze/RunningProcess;
    .locals 4
    .param p1, "pid"    # I

    .line 799
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v0

    .line 800
    .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/RunningProcess;

    .line 801
    .local v2, "proc":Lcom/miui/server/greeze/RunningProcess;
    iget v3, v2, Lcom/miui/server/greeze/RunningProcess;->pid:I

    if-ne p1, v3, :cond_0

    .line 802
    return-object v2

    .line 804
    .end local v2    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :cond_0
    goto :goto_0

    .line 805
    :cond_1
    const/4 v1, 0x0

    return-object v1
.end method

.method getProcessByUid(I)Ljava/util/List;
    .locals 2
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/RunningProcess;",
            ">;"
        }
    .end annotation

    .line 790
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getUidMap()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 791
    .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    if-eqz v0, :cond_0

    .line 792
    return-object v0

    .line 794
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    return-object v1
.end method

.method public getUidByPackageName(Ljava/lang/String;)I
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 3383
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 3387
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I

    move-result v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3388
    .local v0, "uid":I
    return v0

    .line 3389
    .end local v0    # "uid":I
    :catch_0
    move-exception v0

    .line 3390
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can not found the app for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GreezeManager"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3391
    return v1

    .line 3384
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    :goto_0
    return v1
.end method

.method isAllowBroadcast(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "callerUid"    # I
    .param p2, "callerPkgName"    # Ljava/lang/String;
    .param p3, "calleeUid"    # I
    .param p4, "calleePkgName"    # Ljava/lang/String;
    .param p5, "action"    # Ljava/lang/String;

    .line 2458
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    monitor-enter v0

    .line 2459
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    invoke-interface {v1, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    .line 2460
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastTargetWhiteList:Ljava/util/Map;

    invoke-interface {v1, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2461
    .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, p5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "*"

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2462
    :cond_0
    monitor-exit v0

    return v2

    .line 2465
    .end local v1    # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 2467
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCallerWhiteList:Ljava/util/Map;

    monitor-enter v1

    .line 2468
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCallerWhiteList:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2469
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCallerWhiteList:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2470
    .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0, p5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "*"

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2471
    :cond_2
    monitor-exit v1

    return v2

    .line 2474
    .end local v0    # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2476
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlMap:Ljava/util/Map;

    monitor-enter v0

    .line 2477
    :try_start_2
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlMap:Ljava/util/Map;

    invoke-interface {v1, p4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlMap:Ljava/util/Map;

    invoke-interface {v1, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "ALL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlMap:Ljava/util/Map;

    .line 2478
    invoke-interface {v1, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2479
    :cond_4
    monitor-exit v0

    return v2

    .line 2481
    :cond_5
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2483
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, p4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_d

    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mAllowBroadcast:Ljava/util/List;

    .line 2484
    invoke-interface {v0, p5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_1

    .line 2494
    :cond_6
    invoke-virtual {p0, p5}, Lcom/miui/server/greeze/GreezeManagerService;->checkAurogonIntentDenyList(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2495
    return v1

    .line 2498
    :cond_7
    const/16 v0, 0x403

    if-eq p1, v0, :cond_c

    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_8

    goto :goto_0

    .line 2502
    :cond_8
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v0, :cond_b

    .line 2504
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2505
    return v2

    .line 2508
    :cond_9
    if-ne p1, p3, :cond_a

    .line 2509
    return v2

    .line 2512
    :cond_a
    const-string v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2513
    return v2

    .line 2516
    :cond_b
    return v1

    .line 2499
    :cond_c
    :goto_0
    return v2

    .line 2485
    :cond_d
    :goto_1
    const-string v0, "com.google.android.gms"

    invoke-virtual {p4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-nez v0, :cond_e

    .line 2486
    return v1

    .line 2489
    :cond_e
    invoke-direct {p0, p5}, Lcom/miui/server/greeze/GreezeManagerService;->deferBroadcastForMiui(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2490
    return v1

    .line 2491
    :cond_f
    return v2

    .line 2481
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 2474
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2465
    :catchall_2
    move-exception v1

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v1
.end method

.method public isAllowWakeUpList(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 708
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 709
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 710
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isAllowWakeUpList(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 712
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isAppRunning(I)Z
    .locals 6
    .param p1, "uid"    # I

    .line 1742
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 1744
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->readPidFromCgroup(I)Ljava/util/List;

    move-result-object v0

    .line 1745
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_1

    return v3

    .line 1746
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1747
    .local v4, "pid":I
    invoke-virtual {p0, p1, v4}, Lcom/miui/server/greeze/GreezeManagerService;->readPidStatus(II)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1748
    return v1

    .line 1750
    .end local v4    # "pid":I
    :cond_2
    goto :goto_0

    .line 1751
    :cond_3
    return v3
.end method

.method public isAppRunningInFg(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 1756
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isAppShowOnWindows(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 1230
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    if-eqz v0, :cond_1

    .line 1232
    :try_start_0
    invoke-interface {v0, p1}, Landroid/view/IWindowManager;->checkAppOnWindowsStatus(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1233
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was show on screen, skip it"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1234
    const/4 v0, 0x1

    return v0

    .line 1237
    :cond_0
    goto :goto_0

    .line 1236
    :catch_0
    move-exception v0

    .line 1239
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method isNeedAllowRequest(ILjava/lang/String;I)Z
    .locals 2
    .param p1, "callerUid"    # I
    .param p2, "callerPkgName"    # Ljava/lang/String;
    .param p3, "calleeUid"    # I

    .line 2374
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 2375
    const/16 v0, 0x403

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_4

    .line 2376
    :cond_0
    return v1

    .line 2380
    :cond_1
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2381
    return v1

    .line 2384
    :cond_2
    if-ne p1, p3, :cond_3

    .line 2385
    return v1

    .line 2388
    :cond_3
    const-string v0, "com.xiaomi.xmsf"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2389
    return v1

    .line 2392
    :cond_4
    const/4 v0, 0x0

    return v0
.end method

.method public isNeedCachedAlarmForAurogon(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 1737
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedCachedAlarmForAurogonInner(I)Z

    move-result v0

    return v0
.end method

.method public isNeedCachedAlarmForAurogonInner(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 2103
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 2105
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 2106
    .local v0, "packageName":Ljava/lang/String;
    if-nez v0, :cond_1

    return v1

    .line 2108
    :cond_1
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "xiaomi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "miui"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonAlarmForbidList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0

    .line 2112
    :cond_2
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonAlarmAllowList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2113
    return v1

    .line 2115
    :cond_3
    const-string v1, "GreezeManager"

    const-string v2, "cached alarm!"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2116
    const/4 v1, 0x1

    return v1

    .line 2109
    :cond_4
    :goto_0
    return v1
.end method

.method public isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .line 2396
    invoke-virtual {p0, p2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 2397
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->checkAurogonIntentDenyList(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2398
    return v1

    .line 2400
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "drop bc, selector:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "GreezeManager"

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2402
    return v1

    .line 2404
    :cond_1
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->mNeedCachedBroadcast:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2405
    invoke-virtual {p1, p3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2406
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->CachedBroadcasForAurogon(Landroid/content/Intent;I)V

    .line 2407
    const/4 v0, 0x1

    return v0

    .line 2410
    :cond_2
    return v1
.end method

.method public isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z
    .locals 4
    .param p1, "localhost"    # Ljava/lang/String;
    .param p2, "callerUid"    # I
    .param p3, "callerPkgName"    # Ljava/lang/String;
    .param p4, "calleeUid"    # I
    .param p5, "calleePkgName"    # Ljava/lang/String;

    .line 2339
    const/4 v0, 0x0

    .line 2340
    .local v0, "ret":Z
    invoke-virtual {p0, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 2341
    return v2

    .line 2343
    :cond_0
    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 2344
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "localhost = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " callerUid = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " callerPkgName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " calleeUid = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " calleePkgName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "GreezeManager"

    invoke-static {v3, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2347
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v3, "provider"

    sparse-switch v1, :sswitch_data_0

    :cond_2
    goto :goto_0

    :sswitch_0
    const-string/jumbo v1, "startservice"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_1
    const-string v1, "bindservice"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_1

    :sswitch_3
    const-string v1, "broadcast"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :goto_0
    const/4 v2, -0x1

    :goto_1
    packed-switch v2, :pswitch_data_0

    goto :goto_2

    .line 2356
    :pswitch_0
    invoke-virtual {p0, p2, p3, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedAllowRequest(ILjava/lang/String;I)Z

    move-result v0

    .line 2357
    goto :goto_2

    .line 2352
    :pswitch_1
    const/4 v0, 0x1

    .line 2353
    goto :goto_2

    .line 2349
    :pswitch_2
    invoke-virtual {p0, p2, p3, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedAllowRequest(ILjava/lang/String;I)Z

    move-result v0

    .line 2350
    nop

    .line 2362
    :goto_2
    if-eqz v0, :cond_4

    .line 2363
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2364
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1, p4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V

    .line 2365
    return v0

    .line 2367
    :cond_3
    const/16 v1, 0x3e8

    invoke-virtual {p0, p4, v1, p1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 2370
    :cond_4
    return v0

    :sswitch_data_0
    .sparse-switch
        -0x607e173f -> :sswitch_3
        -0x3adbfa0f -> :sswitch_2
        -0xeb32c28 -> :sswitch_1
        0xb07b013 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callerUid"    # I
    .param p3, "callerPkgName"    # Ljava/lang/String;
    .param p4, "calleeUid"    # I
    .param p5, "calleePkgName"    # Ljava/lang/String;

    .line 2415
    const/4 v0, 0x0

    .line 2416
    .local v0, "ret":Z
    iget-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z

    const/4 v2, 0x0

    const/16 v3, 0x3e8

    if-nez v1, :cond_0

    .line 2417
    const-string v1, "broadcast_cl"

    invoke-virtual {p0, p4, v3, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 2418
    return v2

    .line 2420
    :cond_0
    invoke-virtual {p0, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2421
    return v2

    .line 2423
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 2424
    .local v1, "action":Ljava/lang/String;
    const-string v4, "GreezeManager"

    if-nez v1, :cond_2

    .line 2425
    const-string v5, "bc_action"

    invoke-virtual {p0, p4, v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 2426
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isNeedCachedBroadcast white action ="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2427
    return v2

    .line 2430
    :cond_2
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v2, :cond_3

    .line 2431
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Broadcast callerUid = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " callerPkgName = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " calleeUid = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " calleePkgName = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " action = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2434
    :cond_3
    move-object v4, p0

    move v5, p2

    move-object v6, p3

    move v7, p4

    move-object v8, p5

    move-object v9, v1

    invoke-virtual/range {v4 .. v9}, Lcom/miui/server/greeze/GreezeManagerService;->isAllowBroadcast(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    move v0, v2

    .line 2435
    if-nez v0, :cond_5

    .line 2436
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, p5, v1}, Lcom/miui/server/greeze/GreezeManagerService;->checkImmobulusModeRestrict(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2437
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2, p4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V

    goto :goto_0

    .line 2439
    :cond_4
    const-string v2, "broadcast"

    invoke-virtual {p0, p4, v3, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    goto :goto_0

    .line 2442
    :cond_5
    invoke-virtual {p0, p1, p4, p5}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;)Z

    .line 2444
    :goto_0
    return v0
.end method

.method public isUidActive(I)Z
    .locals 5
    .param p1, "uid"    # I

    .line 837
    const/4 v0, 0x3

    const/4 v1, 0x1

    :try_start_0
    invoke-static {v0}, Lmiui/process/ProcessManager;->getActiveUidInfo(I)Ljava/util/List;

    move-result-object v0

    .line 838
    .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/process/ActiveUidInfo;

    .line 839
    .local v3, "info":Lmiui/process/ActiveUidInfo;
    iget v4, v3, Lmiui/process/ActiveUidInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v4, p1, :cond_0

    .line 840
    return v1

    .line 842
    .end local v3    # "info":Lmiui/process/ActiveUidInfo;
    :cond_0
    goto :goto_0

    .line 847
    .end local v0    # "infos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
    :cond_1
    nop

    .line 848
    const/4 v0, 0x0

    return v0

    .line 843
    :catch_0
    move-exception v0

    .line 844
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get active audio info. Going to freeze uid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " regardless of whether it using audio"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GreezeManager"

    invoke-static {v3, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 846
    return v1
.end method

.method public isUidFrozen(I)Z
    .locals 6
    .param p1, "uid"    # I

    .line 697
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 698
    const/16 v1, 0x270f

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I

    move-result-object v1

    .line 699
    .local v1, "frozenUids":[I
    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_1

    aget v5, v1, v4

    .line 700
    .local v5, "frozenuid":I
    if-ne v5, p1, :cond_0

    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 699
    .end local v5    # "frozenuid":I
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 702
    :cond_1
    monitor-exit v0

    return v3

    .line 703
    .end local v1    # "frozenUids":[I
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public monitorNet(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 772
    invoke-static {p1}, Lcom/miui/server/greeze/GreezeManagerService;->nAddConcernedUid(I)V

    .line 773
    return-void
.end method

.method public notifyBackup(IZ)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "start"    # Z

    .line 2610
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2611
    return-void
.end method

.method public notifyDumpAllInfo()V
    .locals 2

    .line 2214
    const-string v0, "GreezeManager"

    const-string v1, "notify dump all info"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2215
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$16;

    invoke-direct {v1, p0}, Lcom/miui/server/greeze/GreezeManagerService$16;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2220
    return-void
.end method

.method public notifyDumpAppInfo(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 2203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notify dump uid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GreezeManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2204
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$15;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$15;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2211
    :cond_0
    return-void
.end method

.method public notifyExcuteServices(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 2183
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x4e1f

    if-le p1, v0, :cond_0

    goto :goto_0

    .line 2186
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mExcuteServiceList:Ljava/util/List;

    monitor-enter v0

    .line 2187
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mExcuteServiceList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2188
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mExcuteServiceList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2190
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2191
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$14;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$14;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2200
    return-void

    .line 2190
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 2184
    :cond_2
    :goto_0
    return-void
.end method

.method public notifyFreeformModeFocus(Ljava/lang/String;I)V
    .locals 7
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .line 1910
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1913
    .local v0, "str":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 1914
    const/4 v1, 0x1

    aget-object v2, v0, v1

    .line 1915
    .local v2, "packageName":Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v4, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1919
    .local v4, "uid":I
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 1920
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " notifyFreeformModeFocus packageName = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Aurogon"

    invoke-static {v6, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1922
    :cond_0
    if-nez p2, :cond_1

    move v5, v1

    goto :goto_0

    :cond_1
    move v5, v3

    :goto_0
    if-ne p2, v1, :cond_2

    move v6, v1

    goto :goto_1

    :cond_2
    move v6, v3

    :goto_1
    or-int/2addr v5, v6

    if-eqz v5, :cond_3

    .line 1923
    const/16 v3, 0x3e8

    const-string v5, "FreeformMode"

    invoke-virtual {p0, v4, v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 1924
    invoke-virtual {p0, v2, v1}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V

    goto :goto_2

    .line 1926
    :cond_3
    invoke-virtual {p0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V

    .line 1928
    :goto_2
    return-void

    .line 1917
    .end local v2    # "packageName":Ljava/lang/String;
    .end local v4    # "uid":I
    :cond_4
    return-void
.end method

.method public notifyMovetoFront(IZ)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "inFreeformSmallWinMode"    # Z

    .line 1871
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1872
    const-string v0, "Aurogon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyMovetoFront uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " inFreeformSmallWinMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1874
    :cond_0
    if-eqz p2, :cond_3

    .line 1875
    iput-boolean p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z

    .line 1876
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1877
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1878
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->checkFreeformSmallWin(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1879
    invoke-virtual {p0, v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->triggerLaunchMode(Ljava/lang/String;I)V

    .line 1881
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V

    .line 1883
    :cond_2
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v1, :cond_3

    .line 1884
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1887
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_3
    const/16 v0, 0x3e8

    const-string v1, "Activity Front"

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 1888
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    monitor-enter v0

    .line 1889
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1890
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1891
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->removetLaunchAppListDelay(I)V

    .line 1893
    :cond_4
    monitor-exit v0

    .line 1895
    return-void

    .line 1893
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public notifyMultitaskLaunch(ILjava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 1947
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1948
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyMultitaskLaunch uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " packageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Aurogon"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1950
    :cond_0
    const/16 v0, 0x3e8

    const-string v1, "Multitask"

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 1951
    return-void
.end method

.method public notifyOtherModule(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;I)V
    .locals 5
    .param p1, "info"    # Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .param p2, "fromWho"    # I

    .line 1404
    iget v0, p1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I

    .line 1405
    .local v0, "state":I
    iget v1, p1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    .line 1406
    .local v1, "uid":I
    iget v2, p1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    .line 1407
    .local v2, "pid":I
    invoke-static {v1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1408
    return-void

    .line 1409
    :cond_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$6;

    invoke-direct {v4, p0, v1, v2, p2}, Lcom/miui/server/greeze/GreezeManagerService$6;-><init>(Lcom/miui/server/greeze/GreezeManagerService;III)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1426
    return-void
.end method

.method public notifyResumeTopActivity(ILjava/lang/String;Z)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "inMultiWindowMode"    # Z

    .line 1970
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1971
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyResumeTopActivity uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " packageName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  inMultiWindowMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1973
    :cond_0
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    if-eq p1, v0, :cond_7

    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 1974
    :cond_1
    const/16 v0, 0x3e8

    const-string v1, "Activity Resume"

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 1975
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    monitor-enter v0

    .line 1976
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1977
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRecentLaunchAppList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1978
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->removetLaunchAppListDelay(I)V

    .line 1980
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1981
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppPackageName:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1982
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    invoke-static {v0}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1983
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->reOrderTargetList(I)V

    .line 1985
    :cond_3
    iput-object p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppPackageName:Ljava/lang/String;

    .line 1986
    iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    .line 1987
    if-eqz p3, :cond_4

    .line 1988
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addTempMutiWindowsApp(I)V

    .line 1989
    return-void

    .line 1993
    :cond_4
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z

    if-eqz v0, :cond_5

    .line 1994
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z

    .line 1995
    return-void

    .line 1997
    :cond_5
    invoke-virtual {p0, p2, p1}, Lcom/miui/server/greeze/GreezeManagerService;->triggerLaunchMode(Ljava/lang/String;I)V

    .line 1999
    :cond_6
    return-void

    .line 1980
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1973
    :cond_7
    :goto_0
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;

    .line 2920
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;

    invoke-direct {v0, p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    .line 2921
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 2922
    return-void
.end method

.method public queryBinderState(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 784
    invoke-static {p1}, Lcom/miui/server/greeze/GreezeManagerService;->nQueryBinder(I)V

    .line 785
    return-void
.end method

.method public readPidFromCgroup(I)Ljava/util/List;
    .locals 9
    .param p1, "uid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/sys/fs/cgroup/uid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1245
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1246
    .local v1, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1248
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 1250
    .local v3, "tempList":[Ljava/io/File;
    if-nez v3, :cond_0

    return-object v1

    .line 1252
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v3

    if-ge v4, v5, :cond_3

    .line 1253
    aget-object v5, v3, v4

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1254
    .local v5, "temp":Ljava/lang/String;
    const-string v6, "pid_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1255
    goto :goto_1

    .line 1258
    :cond_1
    aget-object v6, v3, v4

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1259
    .local v6, "Str":[Ljava/lang/String;
    array-length v7, v6

    const/4 v8, 0x2

    if-le v7, v8, :cond_2

    if-eqz v6, :cond_2

    aget-object v7, v6, v8

    if-eqz v7, :cond_2

    .line 1260
    aget-object v7, v6, v8

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1252
    .end local v5    # "temp":Ljava/lang/String;
    .end local v6    # "Str":[Ljava/lang/String;
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1263
    .end local v4    # "i":I
    :cond_3
    return-object v1
.end method

.method public readPidStatus(I)Z
    .locals 3
    .param p1, "pid"    # I

    .line 1272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/proc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/status"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1273
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1274
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1275
    const/4 v2, 0x1

    return v2

    .line 1277
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public readPidStatus(II)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 1267
    invoke-static {p2}, Landroid/os/Process;->getUidForPid(I)I

    move-result v0

    .line 1268
    .local v0, "tempUid":I
    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
    .locals 4
    .param p1, "callback"    # Lmiui/greeze/IGreezeCallback;
    .param p2, "module"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 672
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 673
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    .line 674
    return v2

    .line 675
    :cond_0
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 676
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Already registed callback for module: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GreezeManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    :cond_1
    const/4 v0, 0x4

    if-eq p2, v0, :cond_3

    const/4 v0, 0x3

    if-ne p2, v0, :cond_2

    goto :goto_0

    .line 679
    :cond_2
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    invoke-interface {p1}, Lmiui/greeze/IGreezeCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;

    invoke-direct {v1, p0, p2}, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 681
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    invoke-interface {p1, v0}, Lmiui/greeze/IGreezeCallback;->serviceReady(Z)V

    .line 682
    const/4 v0, 0x1

    return v0

    .line 678
    :cond_3
    :goto_0
    return v2
.end method

.method public registerMonitor(Lmiui/greeze/IMonitorToken;I)Z
    .locals 5
    .param p1, "token"    # Lmiui/greeze/IMonitorToken;
    .param p2, "type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 366
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 367
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Monitor registered, type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 369
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mMonitorTokens:Landroid/util/SparseArray;

    monitor-enter v0

    .line 370
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mMonitorTokens:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 371
    invoke-interface {p1}, Lmiui/greeze/IMonitorToken;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    new-instance v3, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;

    invoke-direct {v3, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lmiui/greeze/IMonitorToken;I)V

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 372
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 374
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    goto :goto_0

    .line 375
    :cond_0
    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 376
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    or-int/2addr v1, v2

    iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    goto :goto_0

    .line 377
    :cond_1
    if-ne p2, v1, :cond_2

    .line 378
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    .line 380
    :cond_2
    :goto_0
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    const/4 v2, 0x7

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_4

    .line 381
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->am:Landroid/app/AlarmManager;

    if-eqz v1, :cond_3

    .line 382
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mAlarmLoop:Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;

    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/AlarmManager$OnAlarmListener;)V

    .line 383
    :cond_3
    iput v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I

    .line 384
    const-string v1, "GreezeManager"

    const-string v2, "All monitors registered, about to loop once"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_4
    return v0

    .line 372
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public removePendingBroadcast(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 2585
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    monitor-enter v0

    .line 2586
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2587
    monitor-exit v0

    .line 2588
    return-void

    .line 2587
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removetLaunchAppListDelay(I)V
    .locals 4
    .param p1, "uid"    # I

    .line 1960
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$10;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$10;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1967
    return-void
.end method

.method public reportBinderState(IIIIJ)V
    .locals 18
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "tid"    # I
    .param p4, "binderState"    # I
    .param p5, "now"    # J

    .line 600
    move-object/from16 v1, p0

    move/from16 v9, p1

    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 601
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move-wide/from16 v7, p5

    invoke-interface/range {v2 .. v8}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportBinderState(IIIIJ)V

    .line 602
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v10, v2, p5

    .line 603
    .local v10, "delay":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive binder state: uid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " pid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v12, p2

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " tid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v13, p3

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " delay="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ms binderState="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 605
    invoke-static/range {p4 .. p4}, Lcom/miui/server/greeze/GreezeManagerService;->stateToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 606
    .local v14, "msg":Ljava/lang/String;
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    if-eqz v0, :cond_0

    const-string v0, "GreezeManager"

    invoke-static {v0, v14}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    :cond_0
    const-wide/16 v2, 0x32

    cmp-long v0, v10, v2

    if-lez v0, :cond_1

    const-wide/16 v2, 0x2710

    cmp-long v0, v10, v2

    if-gez v0, :cond_1

    .line 608
    const-string v0, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow Greezer: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    :cond_1
    const/4 v2, -0x1

    .line 611
    .local v2, "owner":I
    const/4 v3, 0x0

    .line 612
    .local v3, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v4

    .line 613
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-object v15, v0

    .line 614
    .end local v3    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .local v15, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-nez v15, :cond_3

    .line 615
    :try_start_1
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_2

    const-string v0, "GreezeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reportBinderState null uid = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    :cond_2
    monitor-exit v4

    return-void

    .line 619
    :cond_3
    invoke-virtual {v15}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I

    move-result v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move/from16 v16, v0

    .line 620
    .end local v2    # "owner":I
    .local v16, "owner":I
    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 621
    iget v0, v15, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_0
    move/from16 v17, v0

    .line 622
    .local v17, "state":Z
    move/from16 v7, p4

    if-ne v7, v2, :cond_7

    .line 623
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v0, :cond_7

    .line 624
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v9}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 625
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 626
    const-string v0, "GreezeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addLaunchModeQiutList uid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "reason = check binder"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    :cond_5
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v9}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V

    goto :goto_1

    .line 629
    :cond_6
    const/16 v0, 0x3e8

    const-string v3, "Check Binder"

    invoke-virtual {v1, v9, v0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 635
    :cond_7
    :goto_1
    :try_start_3
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    if-eqz v17, :cond_8

    .line 636
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lmiui/greeze/IGreezeCallback;

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move-wide/from16 v7, p5

    invoke-interface/range {v2 .. v8}, Lmiui/greeze/IGreezeCallback;->reportBinderState(IIIIJ)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 639
    :cond_8
    goto :goto_2

    .line 638
    :catch_0
    move-exception v0

    .line 640
    :goto_2
    return-void

    .line 620
    .end local v17    # "state":Z
    :catchall_0
    move-exception v0

    move-object v3, v15

    move/from16 v2, v16

    goto :goto_3

    .end local v16    # "owner":I
    .restart local v2    # "owner":I
    :catchall_1
    move-exception v0

    move-object v3, v15

    goto :goto_3

    .end local v15    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v3    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :catchall_2
    move-exception v0

    :goto_3
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0
.end method

.method public reportBinderTrans(IIIIIZJI)V
    .locals 23
    .param p1, "dstUid"    # I
    .param p2, "dstPid"    # I
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I
    .param p5, "callerTid"    # I
    .param p6, "isOneway"    # Z
    .param p7, "now"    # J
    .param p9, "buffer"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 539
    move-object/from16 v1, p0

    move/from16 v13, p1

    move/from16 v14, p4

    move/from16 v15, p6

    move/from16 v11, p9

    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 540
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    int-to-long v9, v11

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-wide/from16 v16, v9

    move-wide/from16 v9, p7

    move-wide/from16 v11, v16

    invoke-interface/range {v2 .. v12}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportBinderTrans(IIIIIZJJ)V

    .line 542
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v11, v2, p7

    .line 543
    .local v11, "delay":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Receive frozen binder trans: dstUid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " dstPid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v9, p2

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " callerUid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v10, p3

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " callerPid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " callerTid="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move/from16 v8, p5

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " delay="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ms oneway="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 551
    .local v7, "msg":Ljava/lang/String;
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    if-eqz v0, :cond_0

    const-string v0, "GreezeManager"

    invoke-static {v0, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_0
    const-wide/16 v2, 0x32

    cmp-long v0, v11, v2

    if-lez v0, :cond_1

    const-wide/16 v2, 0x2710

    cmp-long v0, v11, v2

    if-gez v0, :cond_1

    .line 553
    const-string v0, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Slow Greezer: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :cond_1
    const/4 v2, 0x0

    .line 556
    .local v2, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    const/4 v3, -0x1

    .line 557
    .local v3, "owner":I
    iget-object v4, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v4

    .line 558
    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    move-object v6, v0

    .line 559
    .end local v2    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .local v6, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-nez v6, :cond_3

    .line 560
    :try_start_1
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_2

    const-string v0, "GreezeManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reportBinderTrans null uid = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 564
    :catchall_0
    move-exception v0

    move-object v2, v6

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    goto/16 :goto_3

    .line 563
    :cond_3
    :try_start_2
    invoke-virtual {v6}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I

    move-result v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move v5, v0

    .line 564
    .end local v3    # "owner":I
    .local v5, "owner":I
    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 566
    iget v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I

    const/16 v2, 0x3e8

    if-ne v14, v0, :cond_5

    iget-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->isBarExpand:Z

    if-nez v0, :cond_4

    iget-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z

    if-nez v0, :cond_5

    .line 567
    :cond_4
    iget v0, v6, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    const-string v3, "UI bar"

    invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 568
    return-void

    .line 571
    :cond_5
    move/from16 v4, p9

    if-nez v4, :cond_6

    .line 572
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v0, :cond_6

    .line 573
    iget v0, v6, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    const-string v3, "BF"

    invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 574
    return-void

    .line 578
    :cond_6
    if-nez v15, :cond_7

    .line 579
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v0, :cond_7

    .line 580
    iget v0, v6, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    const-string v3, "Sync Binder"

    invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 581
    return-void

    .line 585
    :cond_7
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v17, v0

    check-cast v17, Ljava/util/Map$Entry;

    .line 587
    .local v17, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lmiui/greeze/IGreezeCallback;>;"
    :try_start_4
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v5, :cond_8

    .line 588
    invoke-interface/range {v17 .. v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lmiui/greeze/IGreezeCallback;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    int-to-long v0, v4

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v18, v5

    .end local v5    # "owner":I
    .local v18, "owner":I
    move/from16 v5, p3

    move-object/from16 v19, v6

    .end local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .local v19, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    move/from16 v6, p4

    move-object/from16 v20, v7

    .end local v7    # "msg":Ljava/lang/String;
    .local v20, "msg":Ljava/lang/String;
    move/from16 v7, p5

    move/from16 v8, p6

    move-wide/from16 v9, p7

    move-wide/from16 v21, v11

    .end local v11    # "delay":J
    .local v21, "delay":J
    move-wide v11, v0

    :try_start_5
    invoke-interface/range {v2 .. v12}, Lmiui/greeze/IGreezeCallback;->reportBinderTrans(IIIIIZJJ)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    .line 590
    goto :goto_2

    .line 592
    :catch_0
    move-exception v0

    goto :goto_1

    .line 594
    .end local v18    # "owner":I
    .end local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v20    # "msg":Ljava/lang/String;
    .end local v21    # "delay":J
    .restart local v5    # "owner":I
    .restart local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v11    # "delay":J
    :cond_8
    move/from16 v18, v5

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    .end local v5    # "owner":I
    .end local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v7    # "msg":Ljava/lang/String;
    .end local v11    # "delay":J
    .restart local v18    # "owner":I
    .restart local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v20    # "msg":Ljava/lang/String;
    .restart local v21    # "delay":J
    goto :goto_1

    .line 592
    .end local v18    # "owner":I
    .end local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v20    # "msg":Ljava/lang/String;
    .end local v21    # "delay":J
    .restart local v5    # "owner":I
    .restart local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v11    # "delay":J
    :catch_1
    move-exception v0

    move/from16 v18, v5

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    .line 595
    .end local v5    # "owner":I
    .end local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v7    # "msg":Ljava/lang/String;
    .end local v11    # "delay":J
    .end local v17    # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lmiui/greeze/IGreezeCallback;>;"
    .restart local v18    # "owner":I
    .restart local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v20    # "msg":Ljava/lang/String;
    .restart local v21    # "delay":J
    :goto_1
    move-object/from16 v1, p0

    move/from16 v9, p2

    move/from16 v10, p3

    move/from16 v8, p5

    move/from16 v4, p9

    move/from16 v5, v18

    move-object/from16 v6, v19

    move-object/from16 v7, v20

    move-wide/from16 v11, v21

    goto :goto_0

    .line 585
    .end local v18    # "owner":I
    .end local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v20    # "msg":Ljava/lang/String;
    .end local v21    # "delay":J
    .restart local v5    # "owner":I
    .restart local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v11    # "delay":J
    :cond_9
    move/from16 v18, v5

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    .line 596
    .end local v5    # "owner":I
    .end local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v7    # "msg":Ljava/lang/String;
    .end local v11    # "delay":J
    .restart local v18    # "owner":I
    .restart local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v20    # "msg":Ljava/lang/String;
    .restart local v21    # "delay":J
    :goto_2
    return-void

    .line 564
    .end local v18    # "owner":I
    .end local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v20    # "msg":Ljava/lang/String;
    .end local v21    # "delay":J
    .restart local v5    # "owner":I
    .restart local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v11    # "delay":J
    :catchall_1
    move-exception v0

    move/from16 v18, v5

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    move/from16 v3, v18

    move-object/from16 v2, v19

    .end local v5    # "owner":I
    .end local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v7    # "msg":Ljava/lang/String;
    .end local v11    # "delay":J
    .restart local v18    # "owner":I
    .restart local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v20    # "msg":Ljava/lang/String;
    .restart local v21    # "delay":J
    goto :goto_3

    .end local v18    # "owner":I
    .end local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v20    # "msg":Ljava/lang/String;
    .end local v21    # "delay":J
    .restart local v3    # "owner":I
    .restart local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v11    # "delay":J
    :catchall_2
    move-exception v0

    move-object/from16 v19, v6

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    move-object/from16 v2, v19

    .end local v6    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v7    # "msg":Ljava/lang/String;
    .end local v11    # "delay":J
    .restart local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v20    # "msg":Ljava/lang/String;
    .restart local v21    # "delay":J
    goto :goto_3

    .end local v19    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v20    # "msg":Ljava/lang/String;
    .end local v21    # "delay":J
    .restart local v2    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .restart local v7    # "msg":Ljava/lang/String;
    .restart local v11    # "delay":J
    :catchall_3
    move-exception v0

    move-object/from16 v20, v7

    move-wide/from16 v21, v11

    .end local v7    # "msg":Ljava/lang/String;
    .end local v11    # "delay":J
    .restart local v20    # "msg":Ljava/lang/String;
    .restart local v21    # "delay":J
    :goto_3
    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    throw v0

    :catchall_4
    move-exception v0

    goto :goto_3
.end method

.method public reportLoopOnce()V
    .locals 5

    .line 645
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 646
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->powerFrozenServiceReady(Z)V

    .line 647
    const-string v0, "Receive millet loop once msg"

    .line 648
    .local v0, "msg":Ljava/lang/String;
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    const-string v3, "GreezeManager"

    if-eqz v2, :cond_0

    invoke-static {v3, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :cond_0
    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I

    const/4 v4, 0x7

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_3

    .line 650
    iget-boolean v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInitCtsStatused:Z

    if-nez v2, :cond_1

    .line 651
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->initCtsStatus()V

    .line 652
    iput-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInitCtsStatused:Z

    .line 654
    :cond_1
    sput-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    .line 655
    const-string v1, "Receive millet loop once, gz begin to work"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    sget-object v1, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/greeze/IGreezeCallback;

    .line 658
    .local v2, "callback":Lmiui/greeze/IGreezeCallback;
    :try_start_0
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    invoke-interface {v2, v3}, Lmiui/greeze/IGreezeCallback;->serviceReady(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 661
    goto :goto_1

    .line 659
    :catch_0
    move-exception v3

    .line 662
    .end local v2    # "callback":Lmiui/greeze/IGreezeCallback;
    :goto_1
    goto :goto_0

    :cond_2
    goto :goto_2

    .line 664
    :cond_3
    const-string v1, "Receive millet loop once, but monitor not ready"

    invoke-static {v3, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    :goto_2
    return-void
.end method

.method public reportNet(IJ)V
    .locals 9
    .param p1, "uid"    # I
    .param p2, "now"    # J

    .line 492
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 493
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1, p2, p3}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportNet(IJ)V

    .line 494
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    .line 495
    .local v0, "delay":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive frozen pkg net: uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 496
    .local v2, "msg":Ljava/lang/String;
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    if-eqz v3, :cond_0

    const-string v3, "GreezeManager"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    :cond_0
    const-wide/16 v3, 0x32

    cmp-long v3, v0, v3

    if-lez v3, :cond_1

    const-wide/16 v3, 0x2710

    cmp-long v3, v0, v3

    if-gez v3, :cond_1

    .line 498
    const-string v3, "GreezeManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Slow Greezer: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_1
    const/4 v3, -0x1

    .line 501
    .local v3, "owner":I
    const/4 v4, 0x0

    .line 502
    .local v4, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v5

    .line 503
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    move-result-object v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v4, v6

    .line 504
    if-nez v4, :cond_4

    .line 506
    :try_start_1
    iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    if-ne v6, p1, :cond_2

    .line 507
    sget-object v6, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiui/greeze/IGreezeCallback;

    invoke-interface {v6, p1, p2, p3}, Lmiui/greeze/IGreezeCallback;->reportNet(IJ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 508
    :catch_0
    move-exception v6

    :cond_2
    :goto_0
    nop

    .line 509
    :try_start_2
    sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v6, :cond_3

    const-string v6, "GreezeManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "reportNet null uid = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    :cond_3
    monitor-exit v5

    return-void

    .line 512
    :cond_4
    invoke-virtual {v4}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I

    move-result v6

    move v3, v6

    .line 513
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 515
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v5, :cond_6

    .line 516
    iget-boolean v5, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    if-eqz v5, :cond_5

    .line 517
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v5, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V

    goto :goto_1

    .line 519
    :cond_5
    const/16 v5, 0x3e8

    const-string v6, "PACKET"

    invoke-virtual {p0, p1, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 533
    :cond_6
    :goto_1
    return-void

    .line 513
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6
.end method

.method public reportSignal(IIJ)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "now"    # J

    .line 421
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 422
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportSignal(IIJ)V

    .line 423
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p3

    .line 424
    .local v0, "delay":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Receive frozen signal: uid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " delay="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, "msg":Ljava/lang/String;
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    const-string v4, "GreezeManager"

    if-eqz v3, :cond_0

    invoke-static {v4, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_0
    const-wide/16 v5, 0x32

    cmp-long v3, v0, v5

    if-lez v3, :cond_1

    const-wide/16 v5, 0x2710

    cmp-long v3, v0, v5

    if-gez v3, :cond_1

    .line 428
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Slow Greezer: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :cond_1
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$3;

    invoke-direct {v4, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$3;-><init>(Lcom/miui/server/greeze/GreezeManagerService;II)V

    const-wide/16 v5, 0xc8

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 488
    return-void
.end method

.method public resetCgroupUidStatus(I)V
    .locals 6
    .param p1, "uid"    # I

    .line 1217
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_0

    return-void

    .line 1218
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->readPidFromCgroup(I)Ljava/util/List;

    move-result-object v0

    .line 1219
    .local v0, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1220
    .local v1, "log":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reset uid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pid = [ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1221
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1222
    .local v3, "pid":I
    invoke-static {v3, p1}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(II)Z

    .line 1223
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1224
    .end local v3    # "pid":I
    goto :goto_0

    .line 1225
    :cond_1
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1226
    const-string v2, "GreezeManager"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    return-void
.end method

.method public resetStatusForImmobulus(I)V
    .locals 5
    .param p1, "type"    # I

    .line 1297
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 1298
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1299
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1300
    .local v2, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    and-int/lit8 v3, p1, 0x8

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    .line 1301
    iget-boolean v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z

    if-eqz v3, :cond_1

    .line 1302
    iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z

    goto :goto_1

    .line 1304
    :cond_0
    and-int/lit8 v3, p1, 0x10

    if-eqz v3, :cond_1

    .line 1305
    iget-boolean v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    if-eqz v3, :cond_1

    .line 1306
    iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    .line 1298
    .end local v2    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1310
    .end local v1    # "i":I
    :cond_2
    monitor-exit v0

    .line 1311
    return-void

    .line 1310
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public sendPendingAlarmForAurogon(I)V
    .locals 4
    .param p1, "uid"    # I

    .line 2120
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$12;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$12;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2135
    return-void
.end method

.method public sendPendingBroadcastForAurogon(I)V
    .locals 6
    .param p1, "uid"    # I

    .line 2559
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    monitor-enter v0

    .line 2560
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2561
    .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 2562
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 2563
    .local v3, "intent":Landroid/content/Intent;
    iget-boolean v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v4, :cond_2

    .line 2564
    const-string v4, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2565
    goto :goto_0

    .line 2567
    :cond_2
    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2568
    goto :goto_0

    .line 2570
    :cond_3
    const-string v4, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    .line 2571
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2572
    goto :goto_0

    .line 2574
    :cond_4
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/miui/server/greeze/GreezeManagerService$17;

    invoke-direct {v5, p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$17;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/content/Intent;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2579
    nop

    .end local v3    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 2580
    :cond_5
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mCachedBCList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2581
    nop

    .end local v1    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    monitor-exit v0

    .line 2582
    return-void

    .line 2561
    .restart local v1    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_6
    :goto_1
    monitor-exit v0

    return-void

    .line 2581
    .end local v1    # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setBroadcastCtrl(Z)V
    .locals 2
    .param p1, "val"    # Z

    .line 2320
    iput-boolean p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z

    .line 2321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "setBroadcastCtrl "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GreezeManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2322
    return-void
.end method

.method public thawAll(IILjava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "module"    # I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1622
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 1623
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AIDL thawAll("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GreezeManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1625
    :cond_0
    const/16 v0, 0x270f

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-thawAll from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public thawAll(Ljava/lang/String;)Z
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .line 1592
    const/16 v0, 0x270f

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-thawAll"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 1593
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1594
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_2

    .line 1596
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozonTids()Ljava/util/List;

    move-result-object v0

    .line 1597
    .local v0, "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1598
    .local v3, "tid":I
    invoke-static {v3}, Lcom/miui/server/greeze/FreezeUtils;->thawTid(I)Z

    .line 1599
    .end local v3    # "tid":I
    goto :goto_0

    .line 1600
    :cond_0
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 1602
    .end local v0    # "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_2
    return v1
.end method

.method public thawPids([IILjava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "pids"    # [I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1459
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 1460
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    const-string v1, "GreezeManager"

    const-string v2, ", "

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AIDL thawPids("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1463
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget v5, p1, v4

    .line 1464
    .local v5, "pid":I
    invoke-virtual {p0, v5, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawProcess(IILjava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1465
    sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AIDL thawPid("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") failed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1468
    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1463
    .end local v5    # "pid":I
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1471
    :cond_3
    return-object v0
.end method

.method public thawProcess(IILjava/lang/String;)Z
    .locals 8
    .param p1, "pid"    # I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 1429
    const/4 v0, 0x0

    .line 1430
    .local v0, "done":Z
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 1431
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1432
    .local v2, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-nez v2, :cond_1

    .line 1433
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v3, :cond_0

    const-string v3, "GreezeManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Thawing a non-frozen process (pid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), won\'t add into history, reason "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1434
    :cond_0
    monitor-exit v1

    return v0

    .line 1437
    :cond_1
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v3, :cond_2

    .line 1438
    invoke-static {p1}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(I)Z

    move-result v3

    move v0, v3

    goto :goto_0

    .line 1440
    :cond_2
    iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {p1, v3}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(II)Z

    move-result v3

    move v0, v3

    .line 1441
    :goto_0
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getGameUids()Ljava/util/Set;

    move-result-object v3

    .line 1442
    .local v3, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1443
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    iget v5, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1444
    :cond_3
    const-class v4, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v4}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget v5, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-interface {v4, v5, p1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->removeFrozenPid(II)V

    .line 1445
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J

    .line 1446
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawUptime:J

    .line 1447
    iput-object p3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawReason:Ljava/lang/String;

    .line 1448
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 1449
    invoke-direct {p0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->addHistoryInfo(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;)V

    .line 1450
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v2}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1451
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 1453
    .end local v2    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v3    # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_4
    monitor-exit v1

    .line 1454
    return v0

    .line 1453
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public thawThread(I)V
    .locals 1
    .param p1, "tid"    # I

    .line 1361
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_0

    .line 1362
    invoke-static {p1}, Lcom/miui/server/greeze/FreezeUtils;->thawTid(I)Z

    .line 1363
    :cond_0
    return-void
.end method

.method public thawUid(IILjava/lang/String;)Z
    .locals 17
    .param p1, "uid"    # I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 1476
    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    .line 1477
    const-string v0, "alarm"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1478
    return v5

    .line 1482
    :cond_0
    const/4 v6, 0x1

    .line 1483
    .local v6, "allDone":Z
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v7, v0

    .line 1484
    .local v7, "toThaw":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
    iget-object v8, v1, Lcom/miui/server/greeze/GreezeManagerService;->mAurogonLock:Ljava/lang/Object;

    monitor-enter v8

    .line 1485
    :try_start_0
    iget-object v9, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1486
    :try_start_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1487
    .local v0, "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v11, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v11}, Landroid/util/SparseArray;->size()I

    move-result v11

    if-ge v10, v11, :cond_2

    .line 1488
    iget-object v11, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v11, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1489
    .local v11, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget v12, v11, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    if-ne v12, v2, :cond_1

    .line 1490
    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1491
    iget v12, v11, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1487
    .end local v11    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 1495
    .end local v10    # "i":I
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-nez v10, :cond_3

    .line 1496
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return v11

    .line 1499
    :cond_3
    :try_start_3
    sget-boolean v10, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v10, :cond_6

    .line 1500
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenNewPids()Ljava/util/List;

    move-result-object v10

    .line 1501
    .local v10, "curPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v12

    .line 1502
    .local v12, "allPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v12, v10}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1503
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    .line 1504
    .local v14, "item":Ljava/lang/Integer;
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v15

    invoke-static {v15}, Landroid/os/Process;->getParentPid(I)I

    move-result v15

    .line 1505
    .local v15, "ppid":I
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1506
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(I)Z

    .line 1507
    const-string v5, "GreezeManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v16, v0

    .end local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .local v16, "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const-string v0, "isolated pid: "

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v11, ", ppid: "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1505
    .end local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_4
    move-object/from16 v16, v0

    .line 1509
    .end local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .end local v14    # "item":Ljava/lang/Integer;
    .end local v15    # "ppid":I
    .restart local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :goto_2
    move-object/from16 v0, v16

    const/4 v5, 0x0

    const/4 v11, 0x1

    goto :goto_1

    .line 1503
    .end local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_5
    move-object/from16 v16, v0

    .end local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    goto :goto_3

    .line 1499
    .end local v10    # "curPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v12    # "allPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_6
    move-object/from16 v16, v0

    .line 1512
    .end local v0    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    .restart local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1513
    .local v0, "success":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1514
    .local v5, "failed":Ljava/lang/StringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1515
    .local v10, "log":Ljava/lang/StringBuilder;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "THAW uid = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1516
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1517
    .local v12, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget v13, v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-virtual {v1, v13, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawProcess(IILjava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 1518
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    iget v14, v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1519
    const/4 v6, 0x0

    goto :goto_5

    .line 1521
    :cond_7
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    iget v14, v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1523
    .end local v12    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :goto_5
    goto :goto_4

    .line 1524
    :cond_8
    const/4 v11, 0x1

    invoke-virtual {v1, v2, v11}, Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V

    .line 1525
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " pid = [ "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1526
    const-string v11, ""

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 1527
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "failed = [ "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1529
    :cond_9
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " reason : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " caller : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1530
    iget-object v11, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v11, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_a

    if-nez v6, :cond_b

    .line 1531
    :cond_a
    const-string v11, "GreezeManager"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1532
    iget-object v11, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryLog:Landroid/util/LocalLog;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 1534
    .end local v0    # "success":Ljava/lang/StringBuilder;
    .end local v5    # "failed":Ljava/lang/StringBuilder;
    .end local v10    # "log":Ljava/lang/StringBuilder;
    .end local v16    # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    :cond_b
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1535
    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1537
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isNeedNotifyAppStatus(I)Z

    move-result v0

    if-eqz v0, :cond_c

    goto :goto_6

    :cond_c
    const/4 v0, 0x1

    goto :goto_7

    .line 1538
    :cond_d
    :goto_6
    const/4 v0, 0x1

    if-eq v3, v0, :cond_e

    .line 1539
    const/4 v5, 0x0

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    invoke-virtual {v1, v8, v3}, Lcom/miui/server/greeze/GreezeManagerService;->notifyOtherModule(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;I)V

    .line 1542
    :cond_e
    :goto_7
    if-eq v3, v0, :cond_10

    const/16 v0, 0x3e8

    if-ne v3, v0, :cond_f

    goto :goto_8

    :cond_f
    const/4 v5, 0x0

    goto :goto_9

    .line 1543
    :cond_10
    :goto_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1544
    .local v0, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1545
    const/4 v5, 0x0

    invoke-direct {v1, v0, v5}, Lcom/miui/server/greeze/GreezeManagerService;->setWakeLockState(Ljava/util/List;Z)V

    .line 1547
    .end local v0    # "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_9
    invoke-direct {v1, v2, v5}, Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V

    .line 1548
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->sendPendingAlarmForAurogon(I)V

    .line 1549
    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/GreezeManagerService;->sendPendingBroadcastForAurogon(I)V

    .line 1550
    iget-object v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/miui/server/greeze/GreezeManagerService$7;

    invoke-direct {v5, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$7;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V

    invoke-virtual {v0, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1560
    return v6

    .line 1534
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .end local v6    # "allDone":Z
    .end local v7    # "toThaw":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
    .end local p0    # "this":Lcom/miui/server/greeze/GreezeManagerService;
    .end local p1    # "uid":I
    .end local p2    # "fromWho":I
    .end local p3    # "reason":Ljava/lang/String;
    :try_start_6
    throw v0

    .line 1535
    .restart local v6    # "allDone":Z
    .restart local v7    # "toThaw":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
    .restart local p0    # "this":Lcom/miui/server/greeze/GreezeManagerService;
    .restart local p1    # "uid":I
    .restart local p2    # "fromWho":I
    .restart local p3    # "reason":Ljava/lang/String;
    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0
.end method

.method public thawUidAsync(IILjava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "caller"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 2002
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$11;

    invoke-direct {v1, p0, p1, p3}, Lcom/miui/server/greeze/GreezeManagerService$11;-><init>(Lcom/miui/server/greeze/GreezeManagerService;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2009
    return-void
.end method

.method public thawUids([IILjava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "uids"    # [I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1565
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V

    .line 1566
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    const-string v1, "GreezeManager"

    const-string v2, ", "

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AIDL thawUids("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1568
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1569
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget v5, p1, v4

    .line 1570
    .local v5, "uid":I
    invoke-virtual {p0, v5, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1571
    sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "AIDL thawUid("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") failed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1574
    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1569
    .end local v5    # "uid":I
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1577
    :cond_3
    return-object v0
.end method

.method public thawuidsAll(Ljava/lang/String;)V
    .locals 5
    .param p1, "reason"    # Ljava/lang/String;

    .line 1606
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1607
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v1

    .line 1608
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 1609
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1610
    .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1611
    iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1608
    .end local v3    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1614
    .end local v2    # "i":I
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1615
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1616
    .local v2, "uid":I
    const/16 v3, 0x3e8

    invoke-virtual {p0, v2, v3, p1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 1617
    .end local v2    # "uid":I
    goto :goto_1

    .line 1618
    :cond_2
    return-void

    .line 1614
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public triggerLaunchMode(Ljava/lang/String;I)V
    .locals 2
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1318
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    if-nez v0, :cond_0

    goto/16 :goto_0

    .line 1321
    :cond_0
    const-string v0, "com.miui.home"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1322
    iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z

    .line 1323
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1324
    return-void

    .line 1328
    :cond_2
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->ENABLE_LAUNCH_MODE_DEVICE:Ljava/util/List;

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "com.android.camera"

    if-nez v0, :cond_3

    .line 1329
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnabledLMCamera:Z

    if-eqz v0, :cond_3

    .line 1330
    return-void

    .line 1334
    :cond_3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnabledLMCamera:Z

    if-eqz v0, :cond_4

    .line 1335
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const-string v1, "Camera"

    invoke-virtual {v0, p2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    .line 1338
    :cond_4
    invoke-static {p2}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1339
    return-void

    .line 1342
    :cond_5
    invoke-virtual {p0, p2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thaw uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Activity Start!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Aurogon"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1344
    const/16 v0, 0x3e8

    const-string v1, "Activity Start"

    invoke-virtual {p0, p2, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 1347
    :cond_6
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerLaunchMode(Ljava/lang/String;I)V

    .line 1348
    return-void

    .line 1318
    :cond_7
    :goto_0
    return-void
.end method

.method public updateAurogonUidRule(IZ)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "allow"    # Z

    .line 1689
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-nez v0, :cond_0

    return-void

    .line 1690
    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    .line 1691
    return-void

    .line 1693
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->cm:Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_2

    .line 1695
    const-string v0, "android.net.ConnectivityManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 1696
    .local v0, "clazz":Ljava/lang/Class;
    const-string/jumbo v1, "updateAurogonUidRule"

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1697
    .local v1, "method":Ljava/lang/reflect/Method;
    invoke-virtual {v1, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1698
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService;->cm:Landroid/net/ConnectivityManager;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1702
    .end local v0    # "clazz":Ljava/lang/Class;
    .end local v1    # "method":Ljava/lang/reflect/Method;
    :cond_2
    goto :goto_0

    .line 1700
    :catch_0
    move-exception v0

    .line 1701
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "GreezeManager"

    const-string/jumbo v2, "updateAurogonUidRule error "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1703
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public updateFreeformSmallWinList(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "allow"    # Z

    .line 1898
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    monitor-enter v0

    .line 1899
    if-eqz p2, :cond_0

    .line 1900
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1901
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1904
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1906
    :cond_1
    :goto_0
    monitor-exit v0

    .line 1907
    return-void

    .line 1906
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateFrozenInfoForImmobulus(II)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "type"    # I

    .line 1283
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    monitor-enter v0

    .line 1284
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 1285
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFrozenPids:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 1286
    .local v2, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    and-int/lit8 v3, p2, 0x8

    const/4 v4, 0x1

    if-eqz v3, :cond_0

    .line 1287
    iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z

    goto :goto_1

    .line 1288
    :cond_0
    and-int/lit8 v3, p2, 0x10

    if-eqz v3, :cond_1

    .line 1289
    iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    .line 1284
    .end local v2    # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1292
    .end local v1    # "i":I
    :cond_2
    monitor-exit v0

    .line 1293
    return-void

    .line 1292
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateOrderBCStatus(Ljava/lang/String;IZZ)V
    .locals 3
    .param p1, "intentAction"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isforeground"    # Z
    .param p4, "allow"    # Z

    .line 2017
    if-nez p1, :cond_0

    return-void

    .line 2018
    :cond_0
    if-eqz p4, :cond_2

    .line 2019
    if-eqz p3, :cond_1

    .line 2020
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAction:Ljava/lang/String;

    .line 2021
    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I

    goto :goto_0

    .line 2023
    :cond_1
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAction:Ljava/lang/String;

    .line 2024
    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I

    goto :goto_0

    .line 2027
    :cond_2
    const/4 v0, -0x1

    const-string v1, ""

    if-eqz p3, :cond_3

    .line 2028
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAction:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2029
    iput-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAction:Ljava/lang/String;

    .line 2030
    iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I

    goto :goto_0

    .line 2033
    :cond_3
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAction:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2034
    iput-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAction:Ljava/lang/String;

    .line 2035
    iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I

    .line 2039
    :cond_4
    :goto_0
    return-void
.end method
