.class Lcom/miui/server/greeze/GreezeManagerService$6;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService;->notifyOtherModule(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;

.field final synthetic val$fromWho:I

.field final synthetic val$pid:I

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;III)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1409
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I

    iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$pid:I

    iput p4, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1411
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$pid:I

    invoke-interface {v0, v1, v2}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->thawedByOther(II)V

    .line 1412
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_1

    .line 1413
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1416
    :try_start_0
    sget-object v1, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/greeze/IGreezeCallback;

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$pid:I

    iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I

    invoke-interface {v1, v2, v3, v4}, Lmiui/greeze/IGreezeCallback;->thawedByOther(III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1420
    goto :goto_1

    .line 1417
    :catch_0
    move-exception v1

    .line 1418
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 1419
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notify other module fail uid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GreezeManager"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1424
    .end local v0    # "i":I
    :cond_1
    return-void
.end method
