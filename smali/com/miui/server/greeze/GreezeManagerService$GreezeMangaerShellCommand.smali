.class Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;
.super Landroid/os/ShellCommand;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GreezeMangaerShellCommand"
.end annotation


# instance fields
.field mService:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0
    .param p1, "service"    # Lcom/miui/server/greeze/GreezeManagerService;

    .line 2928
    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 2929
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    .line 2930
    return-void
.end method

.method private dumpSkipUid()V
    .locals 7

    .line 2982
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 2983
    .local v0, "pw":Ljava/io/PrintWriter;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "audio uid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getAudioUid()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2984
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ime uid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getIMEUid()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2986
    :try_start_0
    invoke-static {}, Lmiui/process/ProcessManager;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v1

    .line 2987
    .local v1, "foregroundInfo":Lmiui/process/ForegroundInfo;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "foreground uid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2988
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "multi window uid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2991
    .end local v1    # "foregroundInfo":Lmiui/process/ForegroundInfo;
    goto :goto_0

    .line 2989
    :catch_0
    move-exception v1

    .line 2990
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "ShellCommand"

    const-string v3, "Failed to get foreground info from ProcessManager"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2992
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v1

    .line 2993
    .local v1, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    .line 2994
    .local v2, "foreActs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v3, Landroid/util/ArraySet;

    invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V

    .line 2995
    .local v3, "foreSvcs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/greeze/RunningProcess;

    .line 2996
    .local v5, "proc":Lcom/miui/server/greeze/RunningProcess;
    iget-boolean v6, v5, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z

    if-eqz v6, :cond_0

    .line 2997
    iget v6, v5, Lcom/miui/server/greeze/RunningProcess;->uid:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2999
    :cond_0
    iget-boolean v6, v5, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z

    if-eqz v6, :cond_1

    .line 3000
    iget v6, v5, Lcom/miui/server/greeze/RunningProcess;->uid:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3002
    .end local v5    # "proc":Lcom/miui/server/greeze/RunningProcess;
    :cond_1
    goto :goto_1

    .line 3003
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fore act uid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3004
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fore svc uid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3005
    return-void
.end method

.method private runDumpHistory()V
    .locals 4

    .line 2933
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->dumpHistory(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    .line 2934
    return-void
.end method

.method private runDumpPackages()V
    .locals 12

    .line 2947
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 2948
    .local v0, "pw":Ljava/io/PrintWriter;
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v1}, Lcom/miui/server/greeze/GreezeManagerService;->getPkgMap()Lcom/android/internal/app/ProcessMap;

    move-result-object v1

    .line 2949
    .local v1, "procMap":Lcom/android/internal/app/ProcessMap;, "Lcom/android/internal/app/ProcessMap<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    invoke-virtual {v1}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2950
    .local v3, "pkgName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pkg "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2951
    invoke-virtual {v1}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/SparseArray;

    .line 2952
    .local v4, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v5, v6, :cond_2

    .line 2953
    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    .line 2954
    .local v6, "uid":I
    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 2955
    .local v7, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    if-nez v7, :cond_0

    .line 2956
    goto :goto_3

    .line 2958
    :cond_0
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/miui/server/greeze/RunningProcess;

    .line 2959
    .local v9, "proc":Lcom/miui/server/greeze/RunningProcess;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Lcom/miui/server/greeze/RunningProcess;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2960
    .end local v9    # "proc":Lcom/miui/server/greeze/RunningProcess;
    goto :goto_2

    .line 2952
    .end local v6    # "uid":I
    .end local v7    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    :cond_1
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 2962
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v5    # "i":I
    :cond_2
    goto :goto_0

    .line 2963
    :cond_3
    return-void
.end method

.method private runDumpUids()V
    .locals 10

    .line 2966
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 2967
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getUidMap()Landroid/util/SparseArray;

    move-result-object v1

    .line 2968
    .local v1, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 2969
    .local v2, "N":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "uid total "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2970
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 2971
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 2972
    .local v4, "uid":I
    add-int/lit8 v5, v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    filled-new-array {v5, v6}, [Ljava/lang/Object;

    move-result-object v5

    const-string v6, "#%d uid %d"

    invoke-virtual {v0, v6, v5}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 2973
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 2974
    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 2975
    .local v5, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/greeze/RunningProcess;

    .line 2976
    .local v7, "proc":Lcom/miui/server/greeze/RunningProcess;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Lcom/miui/server/greeze/RunningProcess;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2977
    .end local v7    # "proc":Lcom/miui/server/greeze/RunningProcess;
    goto :goto_1

    .line 2970
    .end local v4    # "uid":I
    .end local v5    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2979
    .end local v3    # "i":I
    :cond_1
    return-void
.end method

.method private runListProcesses()V
    .locals 6

    .line 2937
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 2938
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v1

    .line 2939
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "process total "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2940
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 2941
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/RunningProcess;

    .line 2942
    .local v3, "proc":Lcom/miui/server/greeze/RunningProcess;
    add-int/lit8 v4, v2, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3}, Lcom/miui/server/greeze/RunningProcess;->toString()Ljava/lang/String;

    move-result-object v5

    filled-new-array {v4, v5}, [Ljava/lang/Object;

    move-result-object v4

    const-string v5, "  #%d %s"

    invoke-virtual {v0, v5, v4}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 2940
    .end local v3    # "proc":Lcom/miui/server/greeze/RunningProcess;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2944
    .end local v2    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 12
    .param p1, "cmd"    # Ljava/lang/String;

    .line 3009
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$mcheckPermission(Lcom/miui/server/greeze/GreezeManagerService;)V

    .line 3010
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 3011
    .local v0, "pw":Ljava/io/PrintWriter;
    if-nez p1, :cond_0

    .line 3012
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 3015
    :cond_0
    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    sparse-switch v2, :sswitch_data_0

    :cond_1
    goto/16 :goto_0

    :sswitch_0
    const-string v2, "monitor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x5

    goto/16 :goto_1

    :sswitch_1
    const-string v2, "history"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x11

    goto/16 :goto_1

    :sswitch_2
    const-string/jumbo v2, "thuid"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto/16 :goto_1

    :sswitch_3
    const-string/jumbo v2, "thpid"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    goto/16 :goto_1

    :sswitch_4
    const-string v2, "query"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x8

    goto/16 :goto_1

    :sswitch_5
    const-string v2, "fzuid"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    goto/16 :goto_1

    :sswitch_6
    const-string v2, "fzpid"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v3

    goto/16 :goto_1

    :sswitch_7
    const-string v2, "debug"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x10

    goto/16 :goto_1

    :sswitch_8
    const-string/jumbo v2, "thaw"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    goto/16 :goto_1

    :sswitch_9
    const-string/jumbo v2, "skip"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xf

    goto/16 :goto_1

    :sswitch_a
    const-string v2, "lsfz"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xa

    goto/16 :goto_1

    :sswitch_b
    const-string v2, "loop"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x12

    goto/16 :goto_1

    :sswitch_c
    const-string/jumbo v2, "uid"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xb

    goto/16 :goto_1

    :sswitch_d
    const-string v2, "pkg"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xd

    goto :goto_1

    :sswitch_e
    const-string v2, "iso"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x15

    goto :goto_1

    :sswitch_f
    const-string v2, "ps"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xc

    goto :goto_1

    :sswitch_10
    const-string v2, "ls"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x9

    goto :goto_1

    :sswitch_11
    const-string v2, "getUids"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x13

    goto :goto_1

    :sswitch_12
    const-string v2, "callback"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x14

    goto :goto_1

    :sswitch_13
    const-string v2, "clearmonitor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x7

    goto :goto_1

    :sswitch_14
    const-string v2, "enable"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xe

    goto :goto_1

    :sswitch_15
    const-string/jumbo v2, "unmonitor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x6

    goto :goto_1

    :goto_0
    move v2, v1

    :goto_1
    packed-switch v2, :pswitch_data_0

    .line 3104
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_5

    .line 3094
    :pswitch_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v2}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v2

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3095
    :try_start_1
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v4}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    sub-int/2addr v4, v3

    .local v4, "i":I
    :goto_2
    if-ltz v4, :cond_3

    .line 3096
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v3}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 3097
    .local v3, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-nez v3, :cond_2

    goto :goto_3

    .line 3098
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v6}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pids:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3095
    .end local v3    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_3
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 3101
    .end local v4    # "i":I
    :cond_3
    monitor-exit v2

    .line 3102
    goto :goto_4

    .line 3101
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "pw":Ljava/io/PrintWriter;
    .end local p0    # "this":Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;
    .end local p1    # "cmd":Ljava/lang/String;
    :try_start_2
    throw v3

    .line 3090
    .restart local v0    # "pw":Ljava/io/PrintWriter;
    .restart local p0    # "this":Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;
    .restart local p1    # "cmd":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 3091
    .local v2, "tmp_module":I
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;

    invoke-direct {v4, v2}, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;-><init>(I)V

    invoke-virtual {v3, v4, v2}, Lcom/miui/server/greeze/GreezeManagerService;->registerCallback(Lmiui/greeze/IGreezeCallback;I)Z

    .line 3092
    goto :goto_4

    .line 3085
    .end local v2    # "tmp_module":I
    :pswitch_2
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 3086
    .local v2, "module":I
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v3, v2}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I

    move-result-object v3

    .line 3087
    .local v3, "rst":[I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Frozen uids : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3088
    nop

    .line 3108
    .end local v2    # "module":I
    .end local v3    # "rst":[I
    :goto_4
    goto/16 :goto_6

    .line 3082
    :pswitch_3
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$smnLoopOnce()V

    .line 3083
    return v4

    .line 3079
    :pswitch_4
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runDumpHistory()V

    .line 3080
    return v4

    .line 3073
    :pswitch_5
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 3074
    .local v2, "debug":Z
    sput-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    sput-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_LAUNCH_FROM_HOME:Z

    sput-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    sput-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    .line 3075
    sput-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    .line 3076
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "launch debug log enabled "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3077
    return v4

    .line 3070
    .end local v2    # "debug":Z
    :pswitch_6
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->dumpSkipUid()V

    .line 3071
    return v4

    .line 3065
    :pswitch_7
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 3066
    .local v2, "enable":Z
    sput-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    .line 3067
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "launch freeze enabled "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3068
    return v4

    .line 3062
    .end local v2    # "enable":Z
    :pswitch_8
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runDumpPackages()V

    .line 3063
    return v4

    .line 3059
    :pswitch_9
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runListProcesses()V

    .line 3060
    return v4

    .line 3056
    :pswitch_a
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runDumpUids()V

    .line 3057
    return v4

    .line 3052
    :pswitch_b
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    .line 3053
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenPids(I)[I

    move-result-object v3

    .line 3052
    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3054
    return v4

    .line 3048
    :pswitch_c
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v3, ""

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v6

    invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->dumpSettings(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    .line 3049
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v3, ""

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v6

    invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->dumpFrozen(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V

    .line 3050
    return v4

    .line 3045
    :pswitch_d
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V

    .line 3046
    return v4

    .line 3042
    :pswitch_e
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v2}, Lcom/miui/server/greeze/GreezeManagerService;->clearMonitorNet()V

    .line 3043
    return v4

    .line 3039
    :pswitch_f
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->clearMonitorNet(I)V

    .line 3040
    return v4

    .line 3036
    :pswitch_10
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V

    .line 3037
    return v4

    .line 3033
    :pswitch_11
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v3, "ShellCommand: thaw all"

    const/16 v5, 0x270f

    invoke-virtual {v2, v5, v5, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(IILjava/lang/String;)Ljava/util/List;

    .line 3034
    return v4

    .line 3029
    :pswitch_12
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    new-array v3, v3, [I

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 3030
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, "cmd: thpid"

    .line 3029
    invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;

    .line 3031
    return v4

    .line 3025
    :pswitch_13
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    new-array v3, v3, [I

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 3026
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, "cmd: thuid"

    .line 3025
    invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 3027
    return v4

    .line 3021
    :pswitch_14
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    new-array v6, v3, [I

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v6, v4

    const-wide/16 v7, 0x0

    .line 3022
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const-string v10, "cmd: fzpid"

    .line 3021
    invoke-virtual/range {v5 .. v10}, Lcom/miui/server/greeze/GreezeManagerService;->freezePids([IJILjava/lang/String;)Ljava/util/List;

    .line 3023
    return v4

    .line 3017
    :pswitch_15
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    new-array v6, v3, [I

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v6, v4

    const-wide/16 v7, 0x0

    .line 3018
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    const-string v10, "cmd: fzuid"

    const/4 v11, 0x0

    .line 3017
    invoke-virtual/range {v5 .. v11}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 3019
    return v4

    .line 3104
    :goto_5
    return v1

    .line 3106
    :catch_0
    move-exception v2

    .line 3107
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 3109
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_6
    return v1

    :sswitch_data_0
    .sparse-switch
        -0x50473f5f -> :sswitch_15
        -0x4d6ada7d -> :sswitch_14
        -0x1af33993 -> :sswitch_13
        -0xa43dfbb -> :sswitch_12
        -0x479d167 -> :sswitch_11
        0xd87 -> :sswitch_10
        0xe03 -> :sswitch_f
        0x19885 -> :sswitch_e
        0x1b1cc -> :sswitch_d
        0x1c450 -> :sswitch_c
        0x32c6a4 -> :sswitch_b
        0x32d49b -> :sswitch_a
        0x35e57f -> :sswitch_9
        0x364daa -> :sswitch_8
        0x5b09653 -> :sswitch_7
        0x5d68437 -> :sswitch_6
        0x5d696fc -> :sswitch_5
        0x66f18c8 -> :sswitch_4
        0x6939e97 -> :sswitch_3
        0x693b15c -> :sswitch_2
        0x373fe494 -> :sswitch_1
        0x49b0bd5a -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 3114
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 3115
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "Greeze manager (greezer) commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3116
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3117
    const-string v1, "  ls lsfz"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3118
    const-string v1, "  history"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3119
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3120
    const-string v1, "  fzpid PID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3121
    const-string v1, "  fzuid UID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3122
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3123
    const-string v1, "  thpid PID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3124
    const-string v1, "  thuid UID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3125
    const-string v1, "  thaw"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3126
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3127
    const-string v1, "  monitor/unmonitor UID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3128
    const-string v1, "  clearmonitor"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3129
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3130
    const-string v1, "  query UID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3131
    const-string v1, "    Query binder state in all processes of UID"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3132
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3133
    const-string v1, "  uid pkg ps"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3134
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 3135
    const-string v1, "  enable true/false"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 3136
    return-void
.end method
