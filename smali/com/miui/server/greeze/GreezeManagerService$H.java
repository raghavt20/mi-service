class com.miui.server.greeze.GreezeManagerService$H extends android.os.Handler {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
static final Integer MSG_BACKUP_OP;
static final Integer MSG_GET_SYSTEM_PID;
static final Integer MSG_LAUNCH_BOOST;
static final Integer MSG_MILLET_LOOPONCE;
static final Integer MSG_RECEIVE_FZ;
static final Integer MSG_RECEIVE_THAW;
static final Integer MSG_REPORT_FZ;
static final Integer MSG_THAW_ALL;
static final Integer MSG_THAW_PID;
/* # instance fields */
java.util.Set delayUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.Set freezeingUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.Set thawingUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
public com.miui.server.greeze.GreezeManagerService$H ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 2650 */
this.this$0 = p1;
/* .line 2651 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 2646 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.freezeingUids = v0;
/* .line 2647 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.thawingUids = v0;
/* .line 2648 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.delayUids = v0;
/* .line 2652 */
return;
} // .end method
private void reportFz ( ) {
/* .locals 7 */
/* .line 2655 */
v0 = v0 = this.delayUids;
/* if-lez v0, :cond_0 */
v0 = v0 = this.freezeingUids;
/* if-lez v0, :cond_0 */
/* .line 2656 */
v0 = this.delayUids;
v1 = this.freezeingUids;
/* .line 2658 */
} // :cond_0
v0 = v0 = this.delayUids;
/* if-lez v0, :cond_1 */
v0 = v0 = this.thawingUids;
/* if-lez v0, :cond_1 */
/* .line 2659 */
v0 = this.delayUids;
v1 = this.thawingUids;
/* .line 2661 */
} // :cond_1
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 2662 */
/* .local v0, "tmp":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
v1 = this.freezeingUids;
(( java.util.HashSet ) v0 ).addAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
/* .line 2663 */
v1 = this.thawingUids;
(( java.util.HashSet ) v0 ).retainAll ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z
/* .line 2664 */
final String v1 = ""; // const-string v1, ""
/* .line 2665 */
/* .local v1, "result":Ljava/lang/String; */
v2 = this.thawingUids;
/* .line 2666 */
v2 = this.freezeingUids;
/* .line 2667 */
v2 = this.delayUids;
/* .line 2668 */
/* .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 2669 */
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 2670 */
/* .local v3, "u":I */
v4 = this.this$0;
v4 = (( com.miui.server.greeze.GreezeManagerService ) v4 ).isUidFrozen ( v3 ); // invoke-virtual {v4, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
/* if-nez v4, :cond_2 */
/* .line 2671 */
v4 = this.thawingUids;
java.lang.Integer .valueOf ( v3 );
/* .line 2672 */
/* .line 2674 */
} // .end local v3 # "u":I
} // :cond_2
/* .line 2675 */
} // :cond_3
v3 = v3 = this.thawingUids;
int v4 = 1; // const/4 v4, 0x1
/* if-lez v3, :cond_4 */
/* .line 2676 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "thaw:" */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.thawingUids;
(( java.lang.Object ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
v6 = this.thawingUids;
/* .line 2677 */
(( java.lang.Object ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
/* sub-int/2addr v6, v4 */
/* .line 2676 */
(( java.lang.String ) v5 ).substring ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2680 */
} // :cond_4
v3 = this.delayUids;
v5 = this.freezeingUids;
/* .line 2681 */
v3 = v3 = this.delayUids;
/* if-lez v3, :cond_5 */
/* .line 2682 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ";freeze:"; // const-string v5, ";freeze:"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.delayUids;
(( java.lang.Object ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;
v6 = this.delayUids;
/* .line 2683 */
(( java.lang.Object ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;
v6 = (( java.lang.String ) v6 ).length ( ); // invoke-virtual {v6}, Ljava/lang/String;->length()I
/* sub-int/2addr v6, v4 */
/* .line 2682 */
(( java.lang.String ) v5 ).substring ( v4, v6 ); // invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2684 */
} // :cond_5
v3 = this.delayUids;
/* .line 2685 */
v3 = this.delayUids;
/* .line 2686 */
v3 = v3 = this.delayUids;
/* if-lez v3, :cond_6 */
/* const/16 v3, 0x9 */
v4 = (( com.miui.server.greeze.GreezeManagerService$H ) p0 ).hasMessages ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$H;->hasMessages(I)Z
/* if-nez v4, :cond_6 */
/* .line 2687 */
/* const-wide/16 v4, 0x3e8 */
(( com.miui.server.greeze.GreezeManagerService$H ) p0 ).sendEmptyMessageDelayed ( v3, v4, v5 ); // invoke-virtual {p0, v3, v4, v5}, Lcom/miui/server/greeze/GreezeManagerService$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 2688 */
} // :cond_6
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_7
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " settings result:"; // const-string v4, " settings result:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "GreezeManager"; // const-string v4, "GreezeManager"
android.util.Slog .d ( v4,v3 );
/* .line 2689 */
} // :cond_7
v3 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v4 = 2; // const/4 v4, 0x2
/* if-le v3, v4, :cond_8 */
/* .line 2690 */
v3 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmContext ( v3 );
(( android.content.Context ) v3 ).getContentResolver ( ); // invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "miui_freeze"; // const-string v4, "miui_freeze"
android.provider.Settings$Secure .putString ( v3,v4,v1 );
/* .line 2691 */
} // :cond_8
v3 = this.freezeingUids;
/* .line 2692 */
v3 = this.thawingUids;
/* .line 2693 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 9 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 2697 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const-wide/16 v1, 0x3e8 */
/* const/16 v3, 0x9 */
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_0 */
/* .line 2706 */
/* :pswitch_1 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2707 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "delayUids:"; // const-string v1, "delayUids:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.delayUids;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " freezeingUids:"; // const-string v1, " freezeingUids:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.freezeingUids;
/* .line 2708 */
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " thawingUids:"; // const-string v1, " thawingUids:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.thawingUids;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2707 */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
android.util.Slog .d ( v1,v0 );
/* .line 2709 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$H;->reportFz()V */
/* .line 2710 */
/* goto/16 :goto_0 */
/* .line 2712 */
/* :pswitch_2 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* if-nez v0, :cond_1 */
/* .line 2713 */
v0 = this.thawingUids;
/* iget v4, p1, Landroid/os/Message;->arg2:I */
java.lang.Integer .valueOf ( v4 );
/* .line 2714 */
} // :cond_1
v0 = (( com.miui.server.greeze.GreezeManagerService$H ) p0 ).hasMessages ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$H;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2715 */
/* goto/16 :goto_0 */
/* .line 2716 */
} // :cond_2
(( com.miui.server.greeze.GreezeManagerService$H ) p0 ).sendEmptyMessageDelayed ( v3, v1, v2 ); // invoke-virtual {p0, v3, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 2717 */
/* goto/16 :goto_0 */
/* .line 2699 */
/* :pswitch_3 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* if-nez v0, :cond_3 */
/* .line 2700 */
v0 = this.freezeingUids;
/* iget v4, p1, Landroid/os/Message;->arg2:I */
java.lang.Integer .valueOf ( v4 );
/* .line 2701 */
} // :cond_3
v0 = (( com.miui.server.greeze.GreezeManagerService$H ) p0 ).hasMessages ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$H;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 2702 */
/* goto/16 :goto_0 */
/* .line 2703 */
} // :cond_4
(( com.miui.server.greeze.GreezeManagerService$H ) p0 ).sendEmptyMessageDelayed ( v3, v1, v2 ); // invoke-virtual {p0, v3, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$H;->sendEmptyMessageDelayed(IJ)Z
/* .line 2704 */
/* goto/16 :goto_0 */
/* .line 2735 */
/* :pswitch_4 */
/* iget v0, p1, Landroid/os/Message;->arg2:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_5 */
v0 = this.this$0;
/* iget v2, p1, Landroid/os/Message;->arg1:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isUidFrozen ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 2736 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* const/16 v2, 0x3e8 */
final String v3 = "backing"; // const-string v3, "backing"
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUid ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 2737 */
} // :cond_5
/* iget v0, p1, Landroid/os/Message;->arg2:I */
/* if-nez v0, :cond_6 */
v0 = this.this$0;
/* iget v2, p1, Landroid/os/Message;->arg1:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isUidFrozen ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
/* if-nez v0, :cond_6 */
/* .line 2738 */
/* new-array v0, v1, [I */
/* .line 2739 */
/* .local v0, "uids":[I */
int v1 = 0; // const/4 v1, 0x0
/* iget v2, p1, Landroid/os/Message;->arg1:I */
/* aput v2, v0, v1 */
/* .line 2740 */
v2 = this.this$0;
/* const-wide/16 v4, 0x0 */
/* const/16 v6, 0x3e8 */
final String v7 = "backingE"; // const-string v7, "backingE"
int v8 = 1; // const/4 v8, 0x1
/* move-object v3, v0 */
/* invoke-virtual/range {v2 ..v8}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List; */
/* .line 2732 */
} // .end local v0 # "uids":[I
/* :pswitch_5 */
v0 = this.this$0;
v1 = com.miui.server.greeze.GreezeManagerService .-$$Nest$mgetSystemUiPid ( v0 );
com.miui.server.greeze.GreezeManagerService .-$$Nest$fputmSystemUiPid ( v0,v1 );
/* .line 2733 */
/* .line 2729 */
/* :pswitch_6 */
com.miui.server.greeze.GreezeManagerService .-$$Nest$smnLoopOnce ( );
/* .line 2730 */
/* .line 2726 */
/* :pswitch_7 */
v0 = this.this$0;
final String v1 = "from msg"; // const-string v1, "from msg"
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawAll ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(Ljava/lang/String;)Z
/* .line 2727 */
/* .line 2719 */
/* :pswitch_8 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 2720 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 2721 */
/* .local v0, "pid":I */
v1 = this.obj;
/* check-cast v1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 2722 */
/* .local v1, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
v2 = this.this$0;
/* iget v3, p1, Landroid/os/Message;->arg2:I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Timeout pid "; // const-string v5, "Timeout pid "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v2 ).thawProcess ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawProcess(IILjava/lang/String;)Z
/* .line 2723 */
} // .end local v0 # "pid":I
} // .end local v1 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* nop */
/* .line 2744 */
} // :cond_6
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_8 */
/* :pswitch_0 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
