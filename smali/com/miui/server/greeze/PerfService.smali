.class public Lcom/miui/server/greeze/PerfService;
.super Lmiui/greeze/IGreezeCallback$Stub;
.source "PerfService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;
    }
.end annotation


# static fields
.field public static final BINDER_STATE_IN_BUSY:I = 0x1

.field public static final BINDER_STATE_IN_IDLE:I = 0x0

.field public static final BINDER_STATE_IN_TRANSACTION:I = 0x4

.field public static final BINDER_STATE_PROC_IN_BUSY:I = 0x3

.field public static final BINDER_STATE_THREAD_IN_BUSY:I = 0x2

.field private static final SERVICE_NAME:Ljava/lang/String; = "greezer"

.field private static TAG:Ljava/lang/String;

.field private static final WHITELIST_PKG:[Ljava/lang/String;

.field private static final singleton:Landroid/util/Singleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Singleton<",
            "Lcom/miui/server/greeze/PerfService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAms:Lcom/android/server/am/ActivityManagerService;

.field private mGetCastPid:Ljava/lang/reflect/Method;

.field private mService:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAms(Lcom/miui/server/greeze/PerfService;)Lcom/android/server/am/ActivityManagerService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/PerfService;->mAms:Lcom/android/server/am/ActivityManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmGetCastPid(Lcom/miui/server/greeze/PerfService;)Ljava/lang/reflect/Method;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/PerfService;->mGetCastPid:Ljava/lang/reflect/Method;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmService(Lcom/miui/server/greeze/PerfService;)Lcom/miui/server/greeze/GreezeManagerService;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/greeze/PerfService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$sfgetWHITELIST_PKG()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/greeze/PerfService;->WHITELIST_PKG:[Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 7

    .line 30
    const-string v0, "GreezeManager:PerfService"

    sput-object v0, Lcom/miui/server/greeze/PerfService;->TAG:Ljava/lang/String;

    .line 40
    const-string v1, "com.miui.home"

    const-string v2, "com.miui.systemAdSolution"

    const-string v3, "com.xiaomi.xmsf"

    const-string v4, "com.miui.hybrid"

    const-string v5, "com.android.providers.media.module"

    const-string v6, "com.google.android.providers.media.module"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/greeze/PerfService;->WHITELIST_PKG:[Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/miui/server/greeze/PerfService$1;

    invoke-direct {v0}, Lcom/miui/server/greeze/PerfService$1;-><init>()V

    sput-object v0, Lcom/miui/server/greeze/PerfService;->singleton:Landroid/util/Singleton;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 57
    invoke-direct {p0}, Lmiui/greeze/IGreezeCallback$Stub;-><init>()V

    .line 58
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->getService()Lcom/miui/server/greeze/GreezeManagerService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    .line 59
    invoke-static {}, Landroid/app/ActivityManager;->getService()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/miui/server/greeze/PerfService;->mAms:Lcom/android/server/am/ActivityManagerService;

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/greeze/PerfService-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/PerfService;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/server/greeze/PerfService;
    .locals 1

    .line 63
    sget-object v0, Lcom/miui/server/greeze/PerfService;->singleton:Landroid/util/Singleton;

    invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/greeze/PerfService;

    return-object v0
.end method


# virtual methods
.method public reportBinderState(IIIIJ)V
    .locals 7
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "tid"    # I
    .param p4, "binderState"    # I
    .param p5, "now"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 238
    const-string v0, " tid="

    const-string v1, " pid="

    const-string v2, "Receive binder state: uid="

    packed-switch p4, :pswitch_data_0

    goto :goto_0

    .line 248
    :pswitch_0
    iget-object v3, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    filled-new-array {p2}, [I

    move-result-object v4

    sget v5, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;

    .line 250
    goto :goto_0

    .line 242
    :pswitch_1
    iget-object v3, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    filled-new-array {p1}, [I

    move-result-object v4

    sget v5, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 244
    goto :goto_0

    .line 240
    :pswitch_2
    nop

    .line 254
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public reportBinderTrans(IIIIIZJJ)V
    .locals 5
    .param p1, "dstUid"    # I
    .param p2, "dstPid"    # I
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I
    .param p5, "callerTid"    # I
    .param p6, "isOneway"    # Z
    .param p7, "now"    # J
    .param p9, "buffer"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    filled-new-array {p1}, [I

    move-result-object v1

    sget v2, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive frozen binder trans: dstUid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " dstPid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " callerUid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " callerPid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " callerTid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " oneway="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 229
    return-void
.end method

.method public reportNet(IJ)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "now"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    filled-new-array {p1}, [I

    move-result-object v1

    sget v2, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive frozen pkg net: uid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 218
    return-void
.end method

.method public reportSignal(IIJ)V
    .locals 5
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "now"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 212
    iget-object v0, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    filled-new-array {p1}, [I

    move-result-object v1

    sget v2, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive frozen signal: uid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 213
    return-void
.end method

.method public serviceReady(Z)V
    .locals 0
    .param p1, "ready"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 234
    return-void
.end method

.method public startLaunchBoost(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 191
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 192
    :cond_0
    const-string/jumbo v0, "uid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 193
    .local v0, "launchinguid":I
    const-string v1, "launchingActivity"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, "launchingActivity":Ljava/lang/String;
    const-string v2, "fromUid"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 195
    .local v2, "fromUid":I
    const-string v3, "fromPkg"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 196
    .local v3, "fromPkg":Ljava/lang/String;
    iget-object v4, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    filled-new-array {v0}, [I

    move-result-object v5

    sget v6, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Launching "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " from "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 198
    const-string v4, "com.miui.home"

    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    .line 200
    .local v4, "isFromHome":Z
    if-nez v4, :cond_2

    .line 201
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_LAUNCH_FROM_HOME:Z

    if-eqz v5, :cond_1

    sget-object v5, Lcom/miui/server/greeze/PerfService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :cond_1
    return-void

    .line 206
    :cond_2
    iget-object v5, p0, Lcom/miui/server/greeze/PerfService;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;

    invoke-direct {v6, p0, p1}, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;-><init>(Lcom/miui/server/greeze/PerfService;Landroid/os/Bundle;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 208
    return-void
.end method

.method public thawedByOther(III)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "module"    # I

    .line 258
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 259
    sget-object v0, Lcom/miui/server/greeze/PerfService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "thawed uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " by:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_0
    return-void
.end method
