class com.miui.server.greeze.GreezeManagerService$TmpCallback extends miui.greeze.IGreezeCallback$Stub {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "TmpCallback" */
} // .end annotation
/* # instance fields */
Integer module;
/* # direct methods */
public com.miui.server.greeze.GreezeManagerService$TmpCallback ( ) {
/* .locals 0 */
/* .param p1, "module" # I */
/* .line 3143 */
/* invoke-direct {p0}, Lmiui/greeze/IGreezeCallback$Stub;-><init>()V */
/* .line 3144 */
/* iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;->module:I */
/* .line 3145 */
return;
} // .end method
/* # virtual methods */
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .line 3162 */
return;
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
/* .locals 0 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # J */
/* .line 3158 */
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .line 3153 */
return;
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .line 3149 */
return;
} // .end method
public void serviceReady ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "ready" # Z */
/* .line 3166 */
return;
} // .end method
public void thawedByOther ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "module" # I */
/* .line 3169 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;->module:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ": thawed uid:"; // const-string v1, ": thawed uid:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " by:"; // const-string v1, " by:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
android.util.Log .e ( v1,v0 );
/* .line 3170 */
return;
} // .end method
