class com.miui.server.greeze.GreezeManagerService$9$1 implements java.lang.Runnable {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService$9;->onForegroundActivitiesChanged(IIZ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService$9 this$1; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$9$1 ( ) {
/* .locals 0 */
/* .param p1, "this$1" # Lcom/miui/server/greeze/GreezeManagerService$9; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1809 */
this.this$1 = p1;
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 1811 */
v0 = this.this$1;
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.greeze.GreezeManagerService ) v0 ).updateAurogonUidRule ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V
/* .line 1813 */
com.miui.server.greeze.AurogonDownloadFilter .getInstance ( );
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
(( com.miui.server.greeze.AurogonDownloadFilter ) v0 ).setMoveToFgApp ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->setMoveToFgApp(I)V
/* .line 1814 */
v0 = this.this$1;
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getPackageNameFromUid ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 1815 */
/* .local v0, "packageName":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
return;
/* .line 1816 */
} // :cond_0
v1 = this.this$1;
v1 = this.this$0;
v1 = this.mImmobulusMode;
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).isSystemOrMiuiImportantApp ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
final String v1 = "com.xiaomi.market"; // const-string v1, "com.xiaomi.market"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1817 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
v1 = android.os.UserHandle .isApp ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1818 */
com.miui.server.greeze.AurogonDownloadFilter .getInstance ( );
/* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
(( com.miui.server.greeze.AurogonDownloadFilter ) v1 ).addAppNetCheckList ( v3, v0 ); // invoke-virtual {v1, v3, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->addAppNetCheckList(ILjava/lang/String;)V
/* .line 1821 */
} // :cond_2
v1 = this.this$1;
v1 = this.this$0;
v1 = this.mImmobulusMode;
/* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
v3 = this.this$1;
v3 = this.this$0;
v3 = this.mImmobulusMode;
/* iget v3, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I */
/* if-eq v1, v3, :cond_3 */
/* .line 1822 */
v1 = this.this$1;
v1 = this.this$0;
v1 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).quitImmobulusMode ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 1823 */
v1 = this.this$1;
v1 = this.this$0;
v1 = this.mImmobulusMode;
/* iput-boolean v2, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* .line 1826 */
} // :cond_3
v1 = this.this$1;
v1 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetENABLE_VIDEO_MODE_DEVICE ( v1 );
v1 = v3 = android.os.Build.DEVICE;
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 1827 */
v1 = this.this$1;
v1 = this.this$0;
v1 = this.mImmobulusMode;
/* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
	 /* .line 1828 */
	 v1 = this.this$1;
	 v1 = this.this$0;
	 v1 = this.mImmobulusMode;
	 /* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
	 (( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).triggerVideoMode ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerVideoMode(ZLjava/lang/String;I)V
	 /* .line 1830 */
} // :cond_4
v1 = this.this$1;
v1 = this.this$0;
v1 = this.mImmobulusMode;
/* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
	 v1 = this.this$1;
	 v1 = this.this$0;
	 v1 = this.mImmobulusMode;
	 v1 = 	 v1 = this.mVideoAppList;
	 if ( v1 != null) { // if-eqz v1, :cond_5
		 /* .line 1831 */
		 v1 = this.this$1;
		 v1 = this.this$0;
		 v1 = this.mImmobulusMode;
		 int v2 = 1; // const/4 v2, 0x1
		 /* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I */
		 (( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).triggerVideoMode ( v2, v0, v3 ); // invoke-virtual {v1, v2, v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerVideoMode(ZLjava/lang/String;I)V
		 /* .line 1834 */
	 } // :cond_5
	 return;
} // .end method
