class com.miui.server.greeze.GreezeManagerService$1 extends android.database.ContentObserver {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService;->registerCloudObserver(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
final android.content.Context val$context; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 133 */
this.this$0 = p1;
this.val$context = p3;
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 136 */
/* if-nez p2, :cond_0 */
return;
/* .line 137 */
} // :cond_0
final String v0 = "cloud_greezer_enable"; // const-string v0, "cloud_greezer_enable"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
int v3 = -2; // const/4 v3, -0x2
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 138 */
v1 = this.val$context;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .getStringForUser ( v1,v0,v3 );
v0 = java.lang.Boolean .parseBoolean ( v0 );
com.miui.server.greeze.GreezeManagerDebugConfig.sEnable = (v0!= 0);
/* .line 140 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "cloud control set received :"; // const-string v1, "cloud control set received :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 141 */
} // :cond_1
/* const-string/jumbo v0, "zeropkgs" */
android.provider.Settings$Global .getUriFor ( v0 );
v1 = (( android.net.Uri ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 142 */
v1 = this.this$0;
v4 = this.val$context;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .getStringForUser ( v4,v0,v3 );
com.miui.server.greeze.GreezeManagerService .-$$Nest$fputmAudioZeroPkgs ( v1,v0 );
/* .line 144 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mAudioZeroPkgs :"; // const-string v1, "mAudioZeroPkgs :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmAudioZeroPkgs ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 145 */
} // :cond_2
final String v0 = "force_fsg_nav_bar"; // const-string v0, "force_fsg_nav_bar"
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 146 */
v0 = this.this$0;
v1 = this.val$context;
com.miui.server.greeze.GreezeManagerService .-$$Nest$mgetNavBarInfo ( v0,v1 );
/* .line 148 */
} // :cond_3
} // :goto_0
return;
} // .end method
