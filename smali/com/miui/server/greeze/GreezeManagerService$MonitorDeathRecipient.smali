.class Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MonitorDeathRecipient"
.end annotation


# instance fields
.field mMonitorToken:Lmiui/greeze/IMonitorToken;

.field mType:I

.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Lmiui/greeze/IMonitorToken;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .param p2, "token"    # Lmiui/greeze/IMonitorToken;
    .param p3, "type"    # I

    .line 294
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    iput-object p2, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mMonitorToken:Lmiui/greeze/IMonitorToken;

    .line 296
    iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I

    .line 297
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 4

    .line 301
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmMonitorTokens(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v0

    monitor-enter v0

    .line 302
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmMonitorTokens(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 303
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 305
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I

    const/4 v2, 0x1

    const/4 v3, 0x2

    if-ne v0, v2, :cond_0

    .line 306
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v1

    and-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;I)V

    goto :goto_0

    .line 307
    :cond_0
    if-ne v0, v3, :cond_1

    .line 308
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v1

    and-int/lit8 v1, v1, -0x3

    invoke-static {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;I)V

    goto :goto_0

    .line 309
    :cond_1
    if-ne v0, v1, :cond_2

    .line 310
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v1

    and-int/lit8 v1, v1, -0x5

    invoke-static {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;I)V

    .line 312
    :cond_2
    :goto_0
    const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->powerFrozenServiceReady(Z)V

    .line 313
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmRegisteredMonitor(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v0

    const/4 v2, 0x7

    and-int/2addr v0, v2

    if-eq v0, v2, :cond_3

    .line 314
    sput-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    .line 315
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerService;->callbacks:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/greeze/IGreezeCallback;

    .line 317
    .local v1, "callback":Lmiui/greeze/IGreezeCallback;
    :try_start_1
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    invoke-interface {v1, v2}, Lmiui/greeze/IGreezeCallback;->serviceReady(Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 320
    goto :goto_2

    .line 318
    :catch_0
    move-exception v2

    .line 321
    .end local v1    # "callback":Lmiui/greeze/IGreezeCallback;
    :goto_2
    goto :goto_1

    .line 323
    :cond_3
    const-string v0, "GreezeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Monitor (type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") died, gz stop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I

    if-ne v0, v3, :cond_4

    .line 325
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$msetLoopAlarm(Lcom/miui/server/greeze/GreezeManagerService;)V

    .line 326
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient$1;

    invoke-direct {v1, p0}, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient$1;-><init>(Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 332
    :cond_4
    return-void

    .line 303
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
