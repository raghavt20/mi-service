class com.miui.server.greeze.GreezeManagerService$6 implements java.lang.Runnable {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService;->notifyOtherModule(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;I)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
final Integer val$fromWho; //synthetic
final Integer val$pid; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$6 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1409 */
this.this$0 = p1;
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I */
/* iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$pid:I */
/* iput p4, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 5 */
/* .line 1411 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I */
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$pid:I */
/* .line 1412 */
int v0 = 1; // const/4 v0, 0x1
/* .local v0, "i":I */
} // :goto_0
int v1 = 5; // const/4 v1, 0x5
/* if-ge v0, v1, :cond_1 */
/* .line 1413 */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I */
/* if-eq v0, v1, :cond_0 */
v1 = com.miui.server.greeze.GreezeManagerService.callbacks;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1416 */
try { // :try_start_0
v1 = com.miui.server.greeze.GreezeManagerService.callbacks;
java.lang.Integer .valueOf ( v0 );
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Lmiui/greeze/IGreezeCallback; */
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I */
/* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$pid:I */
/* iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1420 */
/* .line 1417 */
/* :catch_0 */
/* move-exception v1 */
/* .line 1418 */
/* .local v1, "e":Ljava/lang/Exception; */
/* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 1419 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "notify other module fail uid : "; // const-string v3, "notify other module fail uid : "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$uid:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v3 = " from :"; // const-string v3, " from :"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$6;->val$fromWho:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
	 android.util.Slog .e ( v3,v2 );
	 /* .line 1412 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 1424 */
} // .end local v0 # "i":I
} // :cond_1
return;
} // .end method
