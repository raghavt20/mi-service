class com.miui.server.greeze.GreezeManagerService$FrozenInfo {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "FrozenInfo" */
} // .end annotation
/* # instance fields */
Boolean isAudioZero;
Boolean isFrozenByImmobulus;
Boolean isFrozenByLaunchMode;
java.util.List mFreezeReasons;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.List mFreezeTimes;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Long;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.List mFromWho;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.lang.String mThawReason;
Long mThawTime;
Long mThawUptime;
Integer pid;
java.lang.String processName;
Integer state;
Integer uid;
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$FrozenInfo ( ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 2769 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 2756 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x10 */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFromWho = v0;
/* .line 2757 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFreezeTimes = v0;
/* .line 2758 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFreezeReasons = v0;
/* .line 2759 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z */
/* .line 2760 */
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
/* .line 2761 */
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isAudioZero:Z */
/* .line 2770 */
/* iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* .line 2771 */
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
/* .line 2772 */
return;
} // .end method
 com.miui.server.greeze.GreezeManagerService$FrozenInfo ( ) {
/* .locals 2 */
/* .param p1, "processRecord" # Lcom/miui/server/greeze/RunningProcess; */
/* .line 2763 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 2756 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x10 */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFromWho = v0;
/* .line 2757 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFreezeTimes = v0;
/* .line 2758 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mFreezeReasons = v0;
/* .line 2759 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z */
/* .line 2760 */
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
/* .line 2761 */
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isAudioZero:Z */
/* .line 2764 */
/* iget v0, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I */
/* iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* .line 2765 */
/* iget v0, p1, Lcom/miui/server/greeze/RunningProcess;->pid:I */
/* iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
/* .line 2766 */
v0 = this.processName;
this.processName = v0;
/* .line 2767 */
return;
} // .end method
/* # virtual methods */
void addFreezeInfo ( Long p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "curTime" # J */
/* .param p3, "fromWho" # I */
/* .param p4, "reason" # Ljava/lang/String; */
/* .line 2775 */
v0 = this.mFreezeTimes;
java.lang.Long .valueOf ( p1,p2 );
/* .line 2776 */
v0 = this.mFromWho;
java.lang.Integer .valueOf ( p3 );
/* .line 2777 */
v0 = this.mFreezeReasons;
/* .line 2778 */
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I */
int v1 = 1; // const/4 v1, 0x1
/* shl-int/2addr v1, p3 */
/* or-int/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I */
/* .line 2779 */
return;
} // .end method
Long getEndTime ( ) {
/* .locals 2 */
/* .line 2790 */
/* iget-wide v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J */
/* return-wide v0 */
} // .end method
Long getFrozenDuration ( ) {
/* .locals 4 */
/* .line 2794 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) p0 ).getStartTime ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getStartTime()J
/* move-result-wide v0 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) p0 ).getEndTime ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getEndTime()J
/* move-result-wide v2 */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 2795 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) p0 ).getEndTime ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getEndTime()J
/* move-result-wide v0 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) p0 ).getStartTime ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getStartTime()J
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
/* return-wide v0 */
/* .line 2797 */
} // :cond_0
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
} // .end method
Integer getOwner ( ) {
/* .locals 2 */
/* .line 2801 */
v0 = v0 = this.mFromWho;
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2802 */
/* .line 2804 */
} // :cond_0
v0 = this.mFromWho;
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
} // .end method
Long getStartTime ( ) {
/* .locals 2 */
/* .line 2782 */
v0 = v0 = this.mFreezeTimes;
/* if-nez v0, :cond_0 */
/* .line 2783 */
/* const-wide/16 v0, 0x0 */
/* return-wide v0 */
/* .line 2785 */
} // :cond_0
v0 = this.mFreezeTimes;
int v1 = 0; // const/4 v1, 0x0
/* check-cast v0, Ljava/lang/Long; */
(( java.lang.Long ) v0 ).longValue ( ); // invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 2809 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
