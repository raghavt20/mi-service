.class Lcom/miui/server/greeze/GreezeManagerService$H;
.super Landroid/os/Handler;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field static final MSG_BACKUP_OP:I = 0x6

.field static final MSG_GET_SYSTEM_PID:I = 0x5

.field static final MSG_LAUNCH_BOOST:I = 0x2

.field static final MSG_MILLET_LOOPONCE:I = 0x4

.field static final MSG_RECEIVE_FZ:I = 0x7

.field static final MSG_RECEIVE_THAW:I = 0x8

.field static final MSG_REPORT_FZ:I = 0x9

.field static final MSG_THAW_ALL:I = 0x3

.field static final MSG_THAW_PID:I = 0x1


# instance fields
.field delayUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field freezeingUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field thawingUids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Looper;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 2650
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    .line 2651
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2646
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    .line 2647
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    .line 2648
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    .line 2652
    return-void
.end method

.method private reportFz()V
    .locals 7

    .line 2655
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2656
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2658
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2659
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2661
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2662
    .local v0, "tmp":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 2663
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    .line 2664
    const-string v1, ""

    .line 2665
    .local v1, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2666
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2667
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2668
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2669
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2670
    .local v3, "u":I
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v4, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2671
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2672
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 2674
    .end local v3    # "u":I
    :cond_2
    goto :goto_0

    .line 2675
    :cond_3
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    const/4 v4, 0x1

    if-lez v3, :cond_4

    .line 2676
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "thaw:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    .line 2677
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v4

    .line 2676
    invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2680
    :cond_4
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    invoke-interface {v3, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2681
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 2682
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ";freeze:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    .line 2683
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v6, v4

    .line 2682
    invoke-virtual {v5, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2684
    :cond_5
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 2685
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2686
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_6

    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$H;->hasMessages(I)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2687
    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v3, v4, v5}, Lcom/miui/server/greeze/GreezeManagerService$H;->sendEmptyMessageDelayed(IJ)Z

    .line 2688
    :cond_6
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v3, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " settings result:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GreezeManager"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2689
    :cond_7
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_8

    .line 2690
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v3}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmContext(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "miui_freeze"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 2691
    :cond_8
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 2692
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 2693
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .line 2697
    iget v0, p1, Landroid/os/Message;->what:I

    const-wide/16 v1, 0x3e8

    const/16 v3, 0x9

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 2706
    :pswitch_1
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 2707
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "delayUids:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->delayUids:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " freezeingUids:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    .line 2708
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " thawingUids:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2707
    const-string v1, "GreezeManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2709
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$H;->reportFz()V

    .line 2710
    goto/16 :goto_0

    .line 2712
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_1

    .line 2713
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->thawingUids:Ljava/util/Set;

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2714
    :cond_1
    invoke-virtual {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$H;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2715
    goto/16 :goto_0

    .line 2716
    :cond_2
    invoke-virtual {p0, v3, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$H;->sendEmptyMessageDelayed(IJ)Z

    .line 2717
    goto/16 :goto_0

    .line 2699
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_3

    .line 2700
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->freezeingUids:Ljava/util/Set;

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2701
    :cond_3
    invoke-virtual {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$H;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2702
    goto/16 :goto_0

    .line 2703
    :cond_4
    invoke-virtual {p0, v3, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$H;->sendEmptyMessageDelayed(IJ)Z

    .line 2704
    goto/16 :goto_0

    .line 2735
    :pswitch_4
    iget v0, p1, Landroid/os/Message;->arg2:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2736
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Landroid/os/Message;->arg1:I

    const/16 v2, 0x3e8

    const-string v3, "backing"

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    goto :goto_0

    .line 2737
    :cond_5
    iget v0, p1, Landroid/os/Message;->arg2:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2738
    new-array v0, v1, [I

    .line 2739
    .local v0, "uids":[I
    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->arg1:I

    aput v2, v0, v1

    .line 2740
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const-wide/16 v4, 0x0

    const/16 v6, 0x3e8

    const-string v7, "backingE"

    const/4 v8, 0x1

    move-object v3, v0

    invoke-virtual/range {v2 .. v8}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List;

    goto :goto_0

    .line 2732
    .end local v0    # "uids":[I
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$mgetSystemUiPid(Lcom/miui/server/greeze/GreezeManagerService;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputmSystemUiPid(Lcom/miui/server/greeze/GreezeManagerService;I)V

    .line 2733
    goto :goto_0

    .line 2729
    :pswitch_6
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$smnLoopOnce()V

    .line 2730
    goto :goto_0

    .line 2726
    :pswitch_7
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v1, "from msg"

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(Ljava/lang/String;)Z

    .line 2727
    goto :goto_0

    .line 2719
    :pswitch_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_6

    .line 2720
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 2721
    .local v0, "pid":I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    .line 2722
    .local v1, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$H;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v3, p1, Landroid/os/Message;->arg2:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Timeout pid "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawProcess(IILjava/lang/String;)Z

    .line 2723
    .end local v0    # "pid":I
    .end local v1    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    nop

    .line 2744
    :cond_6
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
