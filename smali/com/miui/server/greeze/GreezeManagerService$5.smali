.class Lcom/miui/server/greeze/GreezeManagerService$5;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;

.field final synthetic val$freeze:Z

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1367
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I

    iput-boolean p3, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 1370
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmActivityManagerService(Lcom/miui/server/greeze/GreezeManagerService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/am/ActivityManagerService;->mInternal:Landroid/app/ActivityManagerInternal;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I

    invoke-virtual {v0, v1}, Landroid/app/ActivityManagerInternal;->getIsolatedProcesses(I)Ljava/util/List;

    move-result-object v0

    .line 1371
    .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z

    if-nez v1, :cond_0

    .line 1372
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v1

    monitor-enter v1

    .line 1373
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v2}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 1374
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1376
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1377
    return-void

    .line 1378
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1379
    .local v1, "rr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    .line 1380
    .local v2, "index":I
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1381
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1382
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1383
    .local v4, "p":I
    invoke-static {v4}, Landroid/os/Process;->getUidForPid(I)I

    move-result v5

    .line 1384
    .local v5, "isouid":I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    invoke-static {v5}, Landroid/os/Process;->isIsolated(I)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1385
    goto :goto_1

    .line 1386
    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1387
    .end local v4    # "p":I
    .end local v5    # "isouid":I
    goto :goto_1

    .line 1388
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_5

    return-void

    .line 1389
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v4

    new-instance v5, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;

    invoke-direct {v5}, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v4, v5}, Ljava/util/stream/Stream;->mapToInt(Ljava/util/function/ToIntFunction;)Ljava/util/stream/IntStream;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/stream/IntStream;->toArray()[I

    move-result-object v4

    .line 1390
    .local v4, "rst":[I
    iget-boolean v5, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z

    if-eqz v5, :cond_6

    .line 1391
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const-wide/16 v7, 0x0

    const/16 v9, 0x3e8

    const-string v10, "iso"

    move-object v6, v4

    invoke-virtual/range {v5 .. v10}, Lcom/miui/server/greeze/GreezeManagerService;->freezePids([IJILjava/lang/String;)Ljava/util/List;

    .line 1392
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v5}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v5

    monitor-enter v5

    .line 1393
    :try_start_1
    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v6}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetisoPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v6

    iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I

    invoke-virtual {v6, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1394
    monitor-exit v5

    goto :goto_2

    :catchall_1
    move-exception v6

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v6

    .line 1396
    :cond_6
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/16 v6, 0x3e8

    const-string v7, "iso"

    invoke-virtual {v5, v4, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;

    .line 1398
    :goto_2
    const-string v5, "GreezeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "iso uid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " p:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1399
    return-void
.end method
