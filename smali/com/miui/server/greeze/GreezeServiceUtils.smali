.class public Lcom/miui/server/greeze/GreezeServiceUtils;
.super Ljava/lang/Object;
.source "GreezeServiceUtils.java"


# static fields
.field public static GREEZER_MODULE_ALL:I

.field public static GREEZER_MODULE_GAME:I

.field public static GREEZER_MODULE_PERFORMANCE:I

.field public static GREEZER_MODULE_POWER:I

.field public static GREEZER_MODULE_PRELOAD:I

.field public static GREEZER_MODULE_UNKNOWN:I

.field public static TAG:Ljava/lang/String;

.field private static mGameReportPkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mGameReportUids:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mSupportDevice:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 27
    sget v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_UNKNOWN:I

    sput v0, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_UNKNOWN:I

    .line 28
    sget v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_POWER:I

    sput v0, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_POWER:I

    .line 29
    sget v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PERFORMANCE:I

    sput v0, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    .line 30
    sget v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_GAME:I

    sput v0, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_GAME:I

    .line 31
    sget v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PRELOAD:I

    sput v0, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PRELOAD:I

    .line 32
    sget v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_ALL:I

    sput v0, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_ALL:I

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.tencent.tmgp.pubgmhd"

    const-string v2, "com.tencent.tmgp.sgame"

    const-string v3, "com.miHoYo.ys.mi"

    const-string v4, "com.miHoYo.Yuanshen"

    const-string v5, "com.miHoYo.ys.bilibili"

    const-string v6, "com.tencent.lolm"

    const-string v7, "com.tencent.jkchess"

    filled-new-array/range {v1 .. v7}, [Ljava/lang/String;

    move-result-object v1

    .line 35
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/GreezeServiceUtils;->mGameReportPkgs:Ljava/util/List;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "liuqin"

    const-string v2, "pipa"

    const-string v3, "ishtar"

    const-string/jumbo v4, "yuechu"

    const-string/jumbo v5, "yudi"

    const-string v6, "corot"

    const-string/jumbo v7, "vermeer"

    const-string v8, "duchamp"

    const-string v9, "manet"

    filled-new-array/range {v1 .. v9}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/GreezeServiceUtils;->mSupportDevice:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/miui/server/greeze/GreezeServiceUtils;->mGameReportUids:Ljava/util/HashMap;

    .line 40
    const-string v0, "GreezeServiceUtils"

    sput-object v0, Lcom/miui/server/greeze/GreezeServiceUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAudioUid()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 45
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 47
    .local v0, "activeUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v1, 0x3

    :try_start_0
    invoke-static {v1}, Lmiui/process/ProcessManager;->getActiveUidInfo(I)Ljava/util/List;

    move-result-object v1

    .line 48
    .local v1, "uidInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiui/process/ActiveUidInfo;

    .line 49
    .local v3, "info":Lmiui/process/ActiveUidInfo;
    iget v4, v3, Lmiui/process/ActiveUidInfo;->uid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    nop

    .end local v3    # "info":Lmiui/process/ActiveUidInfo;
    goto :goto_0

    .line 53
    .end local v1    # "uidInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
    :cond_0
    goto :goto_1

    .line 51
    :catch_0
    move-exception v1

    .line 52
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/greeze/GreezeServiceUtils;->TAG:Ljava/lang/String;

    const-string v3, "Failed to get active audio info from ProcessManager"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 54
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public static getGameUids()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 72
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 73
    .local v0, "ret":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    sget-object v1, Lcom/miui/server/greeze/GreezeServiceUtils;->mGameReportUids:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 74
    .local v2, "uid":Ljava/lang/Integer;
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    .end local v2    # "uid":Ljava/lang/Integer;
    :cond_0
    return-object v0
.end method

.method public static getIMEUid()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 58
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 60
    .local v0, "uids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :try_start_0
    const-class v1, Lcom/android/server/inputmethod/InputMethodManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/server/inputmethod/InputMethodManagerInternal;

    .line 61
    .local v1, "imm":Lcom/android/server/inputmethod/InputMethodManagerInternal;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/server/inputmethod/InputMethodManagerInternal;->getInputMethodListAsUser(I)Ljava/util/List;

    move-result-object v2

    .line 62
    .local v2, "inputMetheds":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodInfo;

    .line 63
    .local v4, "info":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    nop

    .end local v4    # "info":Landroid/view/inputmethod/InputMethodInfo;
    goto :goto_0

    .line 67
    .end local v1    # "imm":Lcom/android/server/inputmethod/InputMethodManagerInternal;
    .end local v2    # "inputMetheds":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_0
    goto :goto_1

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/greeze/GreezeServiceUtils;->TAG:Ljava/lang/String;

    const-string v3, "Failed to get IME Uid from InputMethodManagerInternal"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 68
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public static getProcessList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/RunningProcess;",
            ">;"
        }
    .end annotation

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    :try_start_0
    const-class v1, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/process/ProcessManagerInternal;

    .line 106
    .local v1, "pmi":Lcom/miui/server/process/ProcessManagerInternal;
    invoke-virtual {v1}, Lcom/miui/server/process/ProcessManagerInternal;->getAllRunningProcessInfo()Ljava/util/List;

    move-result-object v2

    .line 107
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/process/RunningProcessInfo;

    .line 108
    .local v4, "info":Lmiui/process/RunningProcessInfo;
    if-nez v4, :cond_0

    goto :goto_0

    .line 109
    :cond_0
    new-instance v5, Lcom/miui/server/greeze/RunningProcess;

    invoke-direct {v5, v4}, Lcom/miui/server/greeze/RunningProcess;-><init>(Lmiui/process/RunningProcessInfo;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    nop

    .end local v4    # "info":Lmiui/process/RunningProcessInfo;
    goto :goto_0

    .line 113
    .end local v1    # "pmi":Lcom/miui/server/process/ProcessManagerInternal;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
    :cond_1
    goto :goto_1

    .line 111
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/greeze/GreezeServiceUtils;->TAG:Ljava/lang/String;

    const-string v3, "Failed to get RunningProcessInfo from ProcessManager"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public static getProcessListFromAMS(Lcom/android/server/am/ActivityManagerService;)Ljava/util/List;
    .locals 5
    .param p0, "ams"    # Lcom/android/server/am/ActivityManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ActivityManagerService;",
            ")",
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/RunningProcess;",
            ">;"
        }
    .end annotation

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/am/ActivityManagerService;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 139
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 140
    .local v3, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    if-nez v3, :cond_0

    goto :goto_0

    .line 141
    :cond_0
    new-instance v4, Lcom/miui/server/greeze/RunningProcess;

    invoke-direct {v4, v3}, Lcom/miui/server/greeze/RunningProcess;-><init>(Landroid/app/ActivityManager$RunningAppProcessInfo;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    nop

    .end local v3    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    goto :goto_0

    .line 145
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_1
    goto :goto_1

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/greeze/GreezeServiceUtils;->TAG:Ljava/lang/String;

    const-string v3, "Failed to get RunningProcessInfo from ProcessManager"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 146
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v0
.end method

.method public static getUidMap()Landroid/util/SparseArray;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray<",
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/RunningProcess;",
            ">;>;"
        }
    .end annotation

    .line 82
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 83
    .local v0, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getProcessList()Ljava/util/List;

    move-result-object v1

    .line 84
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/RunningProcess;

    .line 85
    .local v3, "proc":Lcom/miui/server/greeze/RunningProcess;
    iget v4, v3, Lcom/miui/server/greeze/RunningProcess;->uid:I

    .line 86
    .local v4, "uid":I
    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 87
    .local v5, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    if-nez v5, :cond_0

    .line 88
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v5, v6

    .line 89
    invoke-virtual {v0, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 91
    :cond_0
    sget-object v6, Lcom/miui/server/greeze/GreezeServiceUtils;->mSupportDevice:Ljava/util/List;

    sget-object v7, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, v3, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    if-eqz v6, :cond_1

    sget-object v6, Lcom/miui/server/greeze/GreezeServiceUtils;->mGameReportPkgs:Ljava/util/List;

    iget-object v7, v3, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    .line 92
    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 93
    sget-object v6, Lcom/miui/server/greeze/GreezeServiceUtils;->mGameReportUids:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 94
    sget-object v6, Lcom/miui/server/greeze/GreezeServiceUtils;->mGameReportUids:Ljava/util/HashMap;

    iget-object v7, v3, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    :cond_1
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    .end local v3    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .end local v4    # "uid":I
    .end local v5    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    goto :goto_0

    .line 98
    :cond_2
    return-object v0
.end method
