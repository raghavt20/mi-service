public class com.miui.server.greeze.AurogonAppInfo {
	 /* .source "AurogonAppInfo.java" */
	 /* # instance fields */
	 public Long freezeTime;
	 public Boolean hasIcon;
	 public Boolean isFreezed;
	 public Boolean isFreezedByGame;
	 public Boolean isFreezedByLaunchMode;
	 public Boolean isSystemApp;
	 public Integer level;
	 public java.lang.String mPackageName;
	 public Integer mUid;
	 public Integer mUserId;
	 public Long unFreezeTime;
	 /* # direct methods */
	 public com.miui.server.greeze.AurogonAppInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "uid" # I */
		 /* .param p2, "packageName" # Ljava/lang/String; */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 17 */
		 /* iput p1, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
		 /* .line 18 */
		 this.mPackageName = p2;
		 /* .line 19 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Boolean equals ( java.lang.Object p0 ) {
		 /* .locals 4 */
		 /* .param p1, "o" # Ljava/lang/Object; */
		 /* .line 23 */
		 int v0 = 0; // const/4 v0, 0x0
		 if ( p1 != null) { // if-eqz p1, :cond_2
			 /* instance-of v1, p1, Lcom/miui/server/greeze/AurogonAppInfo; */
			 /* if-nez v1, :cond_0 */
			 /* .line 26 */
		 } // :cond_0
		 /* move-object v1, p1 */
		 /* check-cast v1, Lcom/miui/server/greeze/AurogonAppInfo; */
		 /* .line 27 */
		 /* .local v1, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
		 /* iget v2, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
		 /* iget v3, v1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
		 /* if-ne v2, v3, :cond_1 */
		 int v0 = 1; // const/4 v0, 0x1
	 } // :cond_1
	 /* .line 24 */
} // .end local v1 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // :cond_2
} // :goto_0
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 32 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v1, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
