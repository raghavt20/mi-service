.class Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;
.super Lmiui/greeze/IGreezeCallback$Stub;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TmpCallback"
.end annotation


# instance fields
.field module:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "module"    # I

    .line 3143
    invoke-direct {p0}, Lmiui/greeze/IGreezeCallback$Stub;-><init>()V

    .line 3144
    iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;->module:I

    .line 3145
    return-void
.end method


# virtual methods
.method public reportBinderState(IIIIJ)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "tid"    # I
    .param p4, "binderState"    # I
    .param p5, "now"    # J

    .line 3162
    return-void
.end method

.method public reportBinderTrans(IIIIIZJJ)V
    .locals 0
    .param p1, "dstUid"    # I
    .param p2, "dstPid"    # I
    .param p3, "callerUid"    # I
    .param p4, "callerPid"    # I
    .param p5, "callerTid"    # I
    .param p6, "isOneway"    # Z
    .param p7, "now"    # J
    .param p9, "buffer"    # J

    .line 3158
    return-void
.end method

.method public reportNet(IJ)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "now"    # J

    .line 3153
    return-void
.end method

.method public reportSignal(IIJ)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "now"    # J

    .line 3149
    return-void
.end method

.method public serviceReady(Z)V
    .locals 0
    .param p1, "ready"    # Z

    .line 3166
    return-void
.end method

.method public thawedByOther(III)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "module"    # I

    .line 3169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;->module:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": thawed uid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " by:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GreezeManager"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3170
    return-void
.end method
