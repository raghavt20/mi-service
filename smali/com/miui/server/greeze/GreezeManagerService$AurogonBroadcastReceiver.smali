.class public Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AurogonBroadcastReceiver"
.end annotation


# instance fields
.field public actionUI:Ljava/lang/String;

.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method public constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 2
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;

    .line 2249
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2248
    const-string v0, "com.android.systemui.fsgesture"

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->actionUI:Ljava/lang/String;

    .line 2250
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2251
    .local v0, "intent":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2252
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2253
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->actionUI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2254
    invoke-static {p1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmContext(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2255
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 2259
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2260
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 2261
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput-boolean v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    .line 2262
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [I

    .line 2263
    .local v1, "uids":[I
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v4, v2, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    monitor-enter v4

    .line 2264
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v2, v2, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 2265
    const/4 v2, 0x0

    .line 2266
    .local v2, "i":I
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mFreeformSmallWinList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 2267
    .local v6, "name":Ljava/lang/String;
    iget-object v7, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v7, v6}, Lcom/miui/server/greeze/GreezeManagerService;->getUidByPackageName(Ljava/lang/String;)I

    move-result v7

    .line 2268
    .local v7, "uid":I
    add-int/lit8 v8, v2, 0x1

    .end local v2    # "i":I
    .local v8, "i":I
    aput v7, v1, v2

    .line 2269
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "uid":I
    move v2, v8

    goto :goto_0

    .line 2271
    .end local v8    # "i":I
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2272
    array-length v2, v1

    if-lez v2, :cond_1

    .line 2273
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/16 v4, 0x3e8

    const-string/jumbo v5, "smallw"

    invoke-virtual {v2, v1, v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    .line 2274
    :cond_1
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V

    .line 2275
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v2}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmHistoryLog(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/LocalLog;

    move-result-object v2

    const-string v3, "SCREEN ON!"

    invoke-virtual {v2, v3}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 2276
    .end local v1    # "uids":[I
    goto/16 :goto_1

    .line 2271
    .restart local v1    # "uids":[I
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 2276
    .end local v1    # "uids":[I
    :cond_2
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2277
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    .line 2278
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmHistoryLog(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/LocalLog;

    move-result-object v1

    const-string v2, "SCREEN OFF!"

    invoke-virtual {v1, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 2279
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->finishLaunchMode()V

    .line 2280
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-nez v1, :cond_9

    .line 2281
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    goto/16 :goto_1

    .line 2283
    :cond_3
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->actionUI:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2284
    const-string/jumbo v1, "typeFrom"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2285
    .local v1, "type":Ljava/lang/String;
    const-string v4, "isEnter"

    invoke-virtual {p2, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 2286
    .local v4, "isEnter":Z
    if-eqz v1, :cond_9

    const-string/jumbo v5, "typefrom_status_bar_expansion"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2287
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v5, v4}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputisBarExpand(Lcom/miui/server/greeze/GreezeManagerService;Z)V

    .line 2288
    if-eqz v4, :cond_5

    .line 2289
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2290
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v5, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v5, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v5, :cond_9

    .line 2291
    :cond_4
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 2292
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v3, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    .line 2293
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v2, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z

    goto :goto_1

    .line 2296
    :cond_5
    const-string v5, ""

    .line 2298
    .local v5, "ret":Ljava/lang/String;
    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v6, v6, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    iget-object v7, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v7, v7, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v7, v7, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I

    if-ne v6, v7, :cond_6

    .line 2299
    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v6, v6, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v2, v6, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    .line 2300
    const-string v5, "Camera"

    .line 2302
    :cond_6
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v2, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z

    if-eqz v2, :cond_7

    .line 2303
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v2, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z

    .line 2304
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-BarExpand"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2306
    :cond_7
    const-string v2, "Camera"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "BarExpand"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2307
    :cond_8
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v2, v2, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    invoke-virtual {v2, v3, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    .line 2311
    .end local v1    # "type":Ljava/lang/String;
    .end local v4    # "isEnter":Z
    .end local v5    # "ret":Ljava/lang/String;
    :cond_9
    :goto_1
    return-void
.end method
