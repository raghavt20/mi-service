.class Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FrozenInfo"
.end annotation


# instance fields
.field isAudioZero:Z

.field isFrozenByImmobulus:Z

.field isFrozenByLaunchMode:Z

.field mFreezeReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mFreezeTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field mFromWho:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mThawReason:Ljava/lang/String;

.field mThawTime:J

.field mThawUptime:J

.field pid:I

.field processName:Ljava/lang/String;

.field state:I

.field uid:I


# direct methods
.method constructor <init>(II)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 2769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    .line 2757
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    .line 2758
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeReasons:Ljava/util/List;

    .line 2759
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z

    .line 2760
    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    .line 2761
    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isAudioZero:Z

    .line 2770
    iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    .line 2771
    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    .line 2772
    return-void
.end method

.method constructor <init>(Lcom/miui/server/greeze/RunningProcess;)V
    .locals 2
    .param p1, "processRecord"    # Lcom/miui/server/greeze/RunningProcess;

    .line 2763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2756
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    .line 2757
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    .line 2758
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeReasons:Ljava/util/List;

    .line 2759
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z

    .line 2760
    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z

    .line 2761
    iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isAudioZero:Z

    .line 2764
    iget v0, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I

    iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    .line 2765
    iget v0, p1, Lcom/miui/server/greeze/RunningProcess;->pid:I

    iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    .line 2766
    iget-object v0, p1, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->processName:Ljava/lang/String;

    .line 2767
    return-void
.end method


# virtual methods
.method addFreezeInfo(JILjava/lang/String;)V
    .locals 2
    .param p1, "curTime"    # J
    .param p3, "fromWho"    # I
    .param p4, "reason"    # Ljava/lang/String;

    .line 2775
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2776
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2777
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeReasons:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2778
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I

    const/4 v1, 0x1

    shl-int/2addr v1, p3

    or-int/2addr v0, v1

    iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I

    .line 2779
    return-void
.end method

.method getEndTime()J
    .locals 2

    .line 2790
    iget-wide v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J

    return-wide v0
.end method

.method getFrozenDuration()J
    .locals 4

    .line 2794
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getStartTime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getEndTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2795
    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getEndTime()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getStartTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0

    .line 2797
    :cond_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method getOwner()I
    .locals 2

    .line 2801
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 2802
    return v1

    .line 2804
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFromWho:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method getStartTime()J
    .locals 2

    .line 2782
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2783
    const-wide/16 v0, 0x0

    return-wide v0

    .line 2785
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mFreezeTimes:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 2809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
