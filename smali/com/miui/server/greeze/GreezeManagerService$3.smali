.class Lcom/miui/server/greeze/GreezeManagerService$3;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService;->reportSignal(IIJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;

.field final synthetic val$pid:I

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 430
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 432
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-static {v0}, Landroid/os/Process;->isIsolated(I)Z

    move-result v0

    .line 433
    .local v0, "isIsoUid":Z
    const/4 v1, -0x1

    .line 434
    .local v1, "infoUid":I
    if-eqz v0, :cond_0

    .line 435
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-static {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$mgetInfoUid(Lcom/miui/server/greeze/GreezeManagerService;I)I

    move-result v1

    .line 436
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    if-eqz v2, :cond_0

    .line 437
    const-string v2, "GreezeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reportSig infoUid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_0
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v2}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmFrozenPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v2

    monitor-enter v2

    .line 440
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v3}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmFrozenPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 441
    const-string v3, "GreezeManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " report signal uid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " pid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "is not frozen."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    monitor-exit v2

    return-void

    .line 445
    :cond_1
    const/16 v3, 0x3e8

    if-eqz v0, :cond_2

    if-lez v1, :cond_2

    .line 446
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v5, "Signal iso"

    invoke-virtual {v4, v1, v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 449
    :cond_2
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->readPidStatus(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 450
    const-string v4, "GreezeManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " report signal uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "is not died."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    const-string v6, "Signal other"

    invoke-virtual {v4, v5, v3, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V

    .line 456
    monitor-exit v2

    return-void

    .line 459
    :cond_3
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v3}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmFrozenPids(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/SparseArray;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->remove(I)V

    .line 460
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-static {v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$mgetFrozenInfo(Lcom/miui/server/greeze/GreezeManagerService;I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;

    move-result-object v3

    .line 461
    .local v3, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    if-nez v3, :cond_6

    .line 462
    const-string v4, ""

    .line 463
    .local v4, "isolatedPid":Ljava/lang/String;
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v5, :cond_4

    iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-static {v5}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 464
    iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-static {v5}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(I)Z

    .line 465
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " isolatedPid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 467
    :cond_4
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_5

    const-string v5, "GreezeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reportSignal null uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_5
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->removePendingBroadcast(I)V

    .line 470
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 471
    .local v5, "log":Ljava/lang/StringBuilder;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Died uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 472
    iget-object v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v6}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmHistoryLog(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/util/LocalLog;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V

    .line 474
    .end local v3    # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
    .end local v4    # "isolatedPid":Ljava/lang/String;
    .end local v5    # "log":Ljava/lang/StringBuilder;
    :cond_6
    monitor-exit v2

    .line 475
    return-void

    .line 474
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
