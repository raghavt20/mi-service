.class Lcom/miui/server/greeze/GreezeManagerService$4;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;

.field final synthetic val$cmd:I

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1083
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->val$cmd:I

    iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->val$uid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1085
    iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->val$cmd:I

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 1093
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmUidAdjs(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1094
    .local v1, "uu":I
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/16 v3, 0x3e8

    const-string v4, "adj"

    invoke-virtual {v2, v1, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 1095
    .end local v1    # "uu":I
    goto :goto_0

    .line 1096
    :cond_0
    goto :goto_1

    .line 1090
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmUidAdjs(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/Set;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->val$uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1091
    goto :goto_1

    .line 1087
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmUidAdjs(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/Set;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$4;->val$uid:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1088
    nop

    .line 1100
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
