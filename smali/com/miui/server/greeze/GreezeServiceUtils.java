public class com.miui.server.greeze.GreezeServiceUtils {
	 /* .source "GreezeServiceUtils.java" */
	 /* # static fields */
	 public static Integer GREEZER_MODULE_ALL;
	 public static Integer GREEZER_MODULE_GAME;
	 public static Integer GREEZER_MODULE_PERFORMANCE;
	 public static Integer GREEZER_MODULE_POWER;
	 public static Integer GREEZER_MODULE_PRELOAD;
	 public static Integer GREEZER_MODULE_UNKNOWN;
	 public static java.lang.String TAG;
	 private static java.util.List mGameReportPkgs;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static java.util.HashMap mGameReportUids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mSupportDevice;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.greeze.GreezeServiceUtils ( ) {
/* .locals 10 */
/* .line 27 */
/* .line 28 */
/* .line 29 */
/* .line 30 */
/* .line 31 */
/* .line 32 */
/* .line 34 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.tencent.tmgp.pubgmhd"; // const-string v1, "com.tencent.tmgp.pubgmhd"
final String v2 = "com.tencent.tmgp.sgame"; // const-string v2, "com.tencent.tmgp.sgame"
final String v3 = "com.miHoYo.ys.mi"; // const-string v3, "com.miHoYo.ys.mi"
final String v4 = "com.miHoYo.Yuanshen"; // const-string v4, "com.miHoYo.Yuanshen"
final String v5 = "com.miHoYo.ys.bilibili"; // const-string v5, "com.miHoYo.ys.bilibili"
final String v6 = "com.tencent.lolm"; // const-string v6, "com.tencent.lolm"
final String v7 = "com.tencent.jkchess"; // const-string v7, "com.tencent.jkchess"
/* filled-new-array/range {v1 ..v7}, [Ljava/lang/String; */
/* .line 35 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 37 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "liuqin"; // const-string v1, "liuqin"
final String v2 = "pipa"; // const-string v2, "pipa"
final String v3 = "ishtar"; // const-string v3, "ishtar"
/* const-string/jumbo v4, "yuechu" */
/* const-string/jumbo v5, "yudi" */
final String v6 = "corot"; // const-string v6, "corot"
/* const-string/jumbo v7, "vermeer" */
final String v8 = "duchamp"; // const-string v8, "duchamp"
final String v9 = "manet"; // const-string v9, "manet"
/* filled-new-array/range {v1 ..v9}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 39 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 40 */
final String v0 = "GreezeServiceUtils"; // const-string v0, "GreezeServiceUtils"
return;
} // .end method
public com.miui.server.greeze.GreezeServiceUtils ( ) {
/* .locals 0 */
/* .line 25 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static java.util.Set getAudioUid ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 45 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 47 */
/* .local v0, "activeUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
int v1 = 3; // const/4 v1, 0x3
try { // :try_start_0
miui.process.ProcessManager .getActiveUidInfo ( v1 );
/* .line 48 */
/* .local v1, "uidInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lmiui/process/ActiveUidInfo; */
/* .line 49 */
/* .local v3, "info":Lmiui/process/ActiveUidInfo; */
/* iget v4, v3, Lmiui/process/ActiveUidInfo;->uid:I */
java.lang.Integer .valueOf ( v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 50 */
/* nop */
} // .end local v3 # "info":Lmiui/process/ActiveUidInfo;
/* .line 53 */
} // .end local v1 # "uidInfos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
} // :cond_0
/* .line 51 */
/* :catch_0 */
/* move-exception v1 */
/* .line 52 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.greeze.GreezeServiceUtils.TAG;
final String v3 = "Failed to get active audio info from ProcessManager"; // const-string v3, "Failed to get active audio info from ProcessManager"
android.util.Slog .w ( v2,v3,v1 );
/* .line 54 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static java.util.Set getGameUids ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 72 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 73 */
/* .local v0, "ret":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
v1 = com.miui.server.greeze.GreezeServiceUtils.mGameReportUids;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/Integer; */
/* .line 74 */
/* .local v2, "uid":Ljava/lang/Integer; */
(( java.util.HashSet ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 75 */
} // .end local v2 # "uid":Ljava/lang/Integer;
} // :cond_0
} // .end method
public static java.util.Set getIMEUid ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 58 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 60 */
/* .local v0, "uids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
try { // :try_start_0
/* const-class v1, Lcom/android/server/inputmethod/InputMethodManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/android/server/inputmethod/InputMethodManagerInternal; */
/* .line 61 */
/* .local v1, "imm":Lcom/android/server/inputmethod/InputMethodManagerInternal; */
v2 = android.os.UserHandle .myUserId ( );
(( com.android.server.inputmethod.InputMethodManagerInternal ) v1 ).getInputMethodListAsUser ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/inputmethod/InputMethodManagerInternal;->getInputMethodListAsUser(I)Ljava/util/List;
/* .line 62 */
/* .local v2, "inputMetheds":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_0
/* check-cast v4, Landroid/view/inputmethod/InputMethodInfo; */
/* .line 63 */
/* .local v4, "info":Landroid/view/inputmethod/InputMethodInfo; */
(( android.view.inputmethod.InputMethodInfo ) v4 ).getServiceInfo ( ); // invoke-virtual {v4}, Landroid/view/inputmethod/InputMethodInfo;->getServiceInfo()Landroid/content/pm/ServiceInfo;
v5 = this.applicationInfo;
/* iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I */
java.lang.Integer .valueOf ( v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 64 */
/* nop */
} // .end local v4 # "info":Landroid/view/inputmethod/InputMethodInfo;
/* .line 67 */
} // .end local v1 # "imm":Lcom/android/server/inputmethod/InputMethodManagerInternal;
} // .end local v2 # "inputMetheds":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
} // :cond_0
/* .line 65 */
/* :catch_0 */
/* move-exception v1 */
/* .line 66 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.greeze.GreezeServiceUtils.TAG;
final String v3 = "Failed to get IME Uid from InputMethodManagerInternal"; // const-string v3, "Failed to get IME Uid from InputMethodManagerInternal"
android.util.Slog .w ( v2,v3,v1 );
/* .line 68 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static java.util.List getProcessList ( ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/RunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 103 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 105 */
/* .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
try { // :try_start_0
/* const-class v1, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* check-cast v1, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 106 */
/* .local v1, "pmi":Lcom/miui/server/process/ProcessManagerInternal; */
(( com.miui.server.process.ProcessManagerInternal ) v1 ).getAllRunningProcessInfo ( ); // invoke-virtual {v1}, Lcom/miui/server/process/ProcessManagerInternal;->getAllRunningProcessInfo()Ljava/util/List;
/* .line 107 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;" */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_1
/* check-cast v4, Lmiui/process/RunningProcessInfo; */
/* .line 108 */
/* .local v4, "info":Lmiui/process/RunningProcessInfo; */
/* if-nez v4, :cond_0 */
/* .line 109 */
} // :cond_0
/* new-instance v5, Lcom/miui/server/greeze/RunningProcess; */
/* invoke-direct {v5, v4}, Lcom/miui/server/greeze/RunningProcess;-><init>(Lmiui/process/RunningProcessInfo;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 110 */
/* nop */
} // .end local v4 # "info":Lmiui/process/RunningProcessInfo;
/* .line 113 */
} // .end local v1 # "pmi":Lcom/miui/server/process/ProcessManagerInternal;
} // .end local v2 # "list":Ljava/util/List;, "Ljava/util/List<Lmiui/process/RunningProcessInfo;>;"
} // :cond_1
/* .line 111 */
/* :catch_0 */
/* move-exception v1 */
/* .line 112 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.greeze.GreezeServiceUtils.TAG;
final String v3 = "Failed to get RunningProcessInfo from ProcessManager"; // const-string v3, "Failed to get RunningProcessInfo from ProcessManager"
android.util.Slog .w ( v2,v3,v1 );
/* .line 132 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static java.util.List getProcessListFromAMS ( com.android.server.am.ActivityManagerService p0 ) {
/* .locals 5 */
/* .param p0, "ams" # Lcom/android/server/am/ActivityManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Lcom/android/server/am/ActivityManagerService;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/RunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 136 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 138 */
/* .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
try { // :try_start_0
(( com.android.server.am.ActivityManagerService ) p0 ).getRunningAppProcesses ( ); // invoke-virtual {p0}, Lcom/android/server/am/ActivityManagerService;->getRunningAppProcesses()Ljava/util/List;
/* .line 139 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 140 */
/* .local v3, "info":Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* if-nez v3, :cond_0 */
/* .line 141 */
} // :cond_0
/* new-instance v4, Lcom/miui/server/greeze/RunningProcess; */
/* invoke-direct {v4, v3}, Lcom/miui/server/greeze/RunningProcess;-><init>(Landroid/app/ActivityManager$RunningAppProcessInfo;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 142 */
/* nop */
} // .end local v3 # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
/* .line 145 */
} // .end local v1 # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
} // :cond_1
/* .line 143 */
/* :catch_0 */
/* move-exception v1 */
/* .line 144 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.greeze.GreezeServiceUtils.TAG;
final String v3 = "Failed to get RunningProcessInfo from ProcessManager"; // const-string v3, "Failed to get RunningProcessInfo from ProcessManager"
android.util.Slog .w ( v2,v3,v1 );
/* .line 146 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static android.util.SparseArray getUidMap ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/RunningProcess;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 82 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
/* .line 83 */
/* .local v0, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 84 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Lcom/miui/server/greeze/RunningProcess; */
/* .line 85 */
/* .local v3, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* iget v4, v3, Lcom/miui/server/greeze/RunningProcess;->uid:I */
/* .line 86 */
/* .local v4, "uid":I */
(( android.util.SparseArray ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v5, Ljava/util/List; */
/* .line 87 */
/* .local v5, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* if-nez v5, :cond_0 */
/* .line 88 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
/* move-object v5, v6 */
/* .line 89 */
(( android.util.SparseArray ) v0 ).put ( v4, v5 ); // invoke-virtual {v0, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 91 */
} // :cond_0
v6 = com.miui.server.greeze.GreezeServiceUtils.mSupportDevice;
v6 = v7 = android.os.Build.DEVICE;
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = this.processName;
if ( v6 != null) { // if-eqz v6, :cond_1
v6 = com.miui.server.greeze.GreezeServiceUtils.mGameReportPkgs;
v7 = this.processName;
v6 = /* .line 92 */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 93 */
v6 = com.miui.server.greeze.GreezeServiceUtils.mGameReportUids;
(( java.util.HashMap ) v6 ).values ( ); // invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v6 = java.lang.Integer .valueOf ( v4 );
/* if-nez v6, :cond_1 */
/* .line 94 */
v6 = com.miui.server.greeze.GreezeServiceUtils.mGameReportUids;
v7 = this.processName;
java.lang.Integer .valueOf ( v4 );
(( java.util.HashMap ) v6 ).put ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 96 */
} // :cond_1
/* .line 97 */
} // .end local v3 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // .end local v4 # "uid":I
} // .end local v5 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
/* .line 98 */
} // :cond_2
} // .end method
