class com.miui.server.greeze.GreezeManagerService$CallbackDeathRecipient implements android.os.IBinder$DeathRecipient {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "CallbackDeathRecipient" */
} // .end annotation
/* # instance fields */
Integer module;
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$CallbackDeathRecipient ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .param p2, "module" # I */
/* .line 719 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 720 */
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I */
/* .line 721 */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 6 */
/* .line 725 */
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.HashMap ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 726 */
v0 = this.this$0;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I */
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getFrozenUids ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I
/* .line 727 */
/* .local v0, "frozenUids":[I */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_0 */
/* aget v4, v0, v3 */
/* .line 728 */
/* .local v4, "frozenuid":I */
v5 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v5 ).updateAurogonUidRule ( v4, v2 ); // invoke-virtual {v5, v4, v2}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V
/* .line 727 */
} // .end local v4 # "frozenuid":I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 730 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "module: "; // const-string v2, "module: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " has died!"; // const-string v2, " has died!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
android.util.Slog .i ( v2,v1 );
/* .line 731 */
v1 = this.this$0;
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;->module:I */
final String v3 = "module died"; // const-string v3, "module died"
(( com.miui.server.greeze.GreezeManagerService ) v1 ).thawAll ( v2, v2, v3 ); // invoke-virtual {v1, v2, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(IILjava/lang/String;)Ljava/util/List;
/* .line 732 */
return;
} // .end method
