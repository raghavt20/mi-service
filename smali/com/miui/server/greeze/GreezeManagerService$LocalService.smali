.class Lcom/miui/server/greeze/GreezeManagerService$LocalService;
.super Lcom/miui/server/greeze/GreezeManagerInternal;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0

    .line 3173
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$LocalService;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    return-void
.end method


# virtual methods
.method public checkAurogonIntentDenyList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .line 3315
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->checkAurogonIntentDenyList(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public finishLaunchMode(Ljava/lang/String;I)V
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 3279
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->finishLaunchMode(Ljava/lang/String;I)V

    .line 3280
    return-void
.end method

.method public freezePid(I)Z
    .locals 1
    .param p1, "pid"    # I

    .line 3289
    invoke-static {p1}, Lcom/miui/server/greeze/FreezeUtils;->freezePid(I)Z

    move-result v0

    return v0
.end method

.method public freezePid(II)Z
    .locals 1
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 3294
    invoke-static {p1, p2}, Lcom/miui/server/greeze/FreezeUtils;->freezePid(II)Z

    move-result v0

    return v0
.end method

.method public freezePids([IJILjava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "pids"    # [I
    .param p2, "timeout"    # J
    .param p4, "fromWho"    # I
    .param p5, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 3269
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/greeze/GreezeManagerService;->freezePids([IJILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public freezeUids([IJILjava/lang/String;Z)Ljava/util/List;
    .locals 7
    .param p1, "uids"    # [I
    .param p2, "timeout"    # J
    .param p4, "fromWho"    # I
    .param p5, "reason"    # Ljava/lang/String;
    .param p6, "checkAudioGps"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJI",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 3264
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFrozenPids(I)[I
    .locals 1
    .param p1, "module"    # I

    .line 3194
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenPids(I)[I

    move-result-object v0

    return-object v0
.end method

.method public getFrozenUids(I)[I
    .locals 1
    .param p1, "module"    # I

    .line 3189
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I

    move-result-object v0

    return-object v0
.end method

.method public getLastThawedTime(II)J
    .locals 2
    .param p1, "uid"    # I
    .param p2, "module"    # I

    .line 3219
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->getLastThawedTime(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V
    .locals 1
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "start"    # Z

    .line 3330
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$mhandleAppZygoteStart(Lcom/miui/server/greeze/GreezeManagerService;Landroid/content/pm/ApplicationInfo;Z)V

    .line 3331
    return-void
.end method

.method public isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;Z)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "uid"    # I
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "force"    # Z

    .line 3320
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z
    .locals 6
    .param p1, "localhost"    # Ljava/lang/String;
    .param p2, "callerUid"    # I
    .param p3, "callerPkgName"    # Ljava/lang/String;
    .param p4, "calleeUid"    # I
    .param p5, "calleePkgName"    # Ljava/lang/String;

    .line 3178
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/greeze/GreezeManagerService;->isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "callerUid"    # I
    .param p3, "callerPkgName"    # Ljava/lang/String;
    .param p4, "calleeUid"    # I
    .param p5, "calleePkgName"    # Ljava/lang/String;

    .line 3325
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/greeze/GreezeManagerService;->isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUidFrozen(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 3184
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    return v0
.end method

.method public notifyBackup(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "start"    # Z

    .line 3204
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyBackup(IZ)V

    .line 3205
    return-void
.end method

.method public notifyDumpAllInfo()V
    .locals 1

    .line 3214
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0}, Lcom/miui/server/greeze/GreezeManagerService;->notifyDumpAllInfo()V

    .line 3215
    return-void
.end method

.method public notifyDumpAppInfo(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I

    .line 3209
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyDumpAppInfo(II)V

    .line 3210
    return-void
.end method

.method public notifyExcuteServices(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 3199
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->notifyExcuteServices(I)V

    .line 3200
    return-void
.end method

.method public notifyFreeformModeFocus(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .line 3239
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyFreeformModeFocus(Ljava/lang/String;I)V

    .line 3240
    return-void
.end method

.method public notifyMovetoFront(IZ)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "inFreeformSmallWinMode"    # Z

    .line 3229
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyMovetoFront(IZ)V

    .line 3230
    return-void
.end method

.method public notifyMultitaskLaunch(ILjava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 3234
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyMultitaskLaunch(ILjava/lang/String;)V

    .line 3235
    return-void
.end method

.method public notifyResumeTopActivity(ILjava/lang/String;Z)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "inMultiWindowMode"    # Z

    .line 3224
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->notifyResumeTopActivity(ILjava/lang/String;Z)V

    .line 3225
    return-void
.end method

.method public queryBinderState(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 3274
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V

    .line 3275
    return-void
.end method

.method public registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
    .locals 1
    .param p1, "callback"    # Lmiui/greeze/IGreezeCallback;
    .param p2, "module"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 3244
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->registerCallback(Lmiui/greeze/IGreezeCallback;I)Z

    move-result v0

    return v0
.end method

.method public thawPid(I)Z
    .locals 1
    .param p1, "pid"    # I

    .line 3299
    invoke-static {p1}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(I)Z

    move-result v0

    return v0
.end method

.method public thawPid(II)Z
    .locals 1
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 3304
    invoke-static {p1, p2}, Lcom/miui/server/greeze/FreezeUtils;->thawPid(II)Z

    move-result v0

    return v0
.end method

.method public thawPids([IILjava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "pids"    # [I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 3259
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public thawUid(IILjava/lang/String;)Z
    .locals 1
    .param p1, "uid"    # I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;

    .line 3249
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public thawUids([IILjava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "uids"    # [I
    .param p2, "fromWho"    # I
    .param p3, "reason"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 3254
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public triggerLaunchMode(Ljava/lang/String;I)V
    .locals 1
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 3284
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->triggerLaunchMode(Ljava/lang/String;I)V

    .line 3285
    return-void
.end method

.method public updateOrderBCStatus(Ljava/lang/String;IZZ)V
    .locals 1
    .param p1, "intentAction"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isforeground"    # Z
    .param p4, "allow"    # Z

    .line 3310
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$LocalService;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/greeze/GreezeManagerService;->updateOrderBCStatus(Ljava/lang/String;IZZ)V

    .line 3311
    return-void
.end method
