class com.miui.server.greeze.GreezeManagerService$MonitorDeathRecipient implements android.os.IBinder$DeathRecipient {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MonitorDeathRecipient" */
} // .end annotation
/* # instance fields */
miui.greeze.IMonitorToken mMonitorToken;
Integer mType;
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$MonitorDeathRecipient ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .param p2, "token" # Lmiui/greeze/IMonitorToken; */
/* .param p3, "type" # I */
/* .line 294 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 295 */
this.mMonitorToken = p2;
/* .line 296 */
/* iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I */
/* .line 297 */
return;
} // .end method
/* # virtual methods */
public void binderDied ( ) {
/* .locals 4 */
/* .line 301 */
v0 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmMonitorTokens ( v0 );
/* monitor-enter v0 */
/* .line 302 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmMonitorTokens ( v1 );
	 /* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I */
	 (( android.util.SparseArray ) v1 ).remove ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V
	 /* .line 303 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 304 */
	 v0 = this.this$0;
	 v0 = this.mHandler;
	 int v1 = 3; // const/4 v1, 0x3
	 (( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
	 /* .line 305 */
	 /* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I */
	 int v2 = 1; // const/4 v2, 0x1
	 int v3 = 2; // const/4 v3, 0x2
	 /* if-ne v0, v2, :cond_0 */
	 /* .line 306 */
	 v0 = this.this$0;
	 v1 = 	 com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmRegisteredMonitor ( v0 );
	 /* and-int/lit8 v1, v1, -0x2 */
	 com.miui.server.greeze.GreezeManagerService .-$$Nest$fputmRegisteredMonitor ( v0,v1 );
	 /* .line 307 */
} // :cond_0
/* if-ne v0, v3, :cond_1 */
/* .line 308 */
v0 = this.this$0;
v1 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmRegisteredMonitor ( v0 );
/* and-int/lit8 v1, v1, -0x3 */
com.miui.server.greeze.GreezeManagerService .-$$Nest$fputmRegisteredMonitor ( v0,v1 );
/* .line 309 */
} // :cond_1
/* if-ne v0, v1, :cond_2 */
/* .line 310 */
v0 = this.this$0;
v1 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmRegisteredMonitor ( v0 );
/* and-int/lit8 v1, v1, -0x5 */
com.miui.server.greeze.GreezeManagerService .-$$Nest$fputmRegisteredMonitor ( v0,v1 );
/* .line 312 */
} // :cond_2
} // :goto_0
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
int v1 = 0; // const/4 v1, 0x0
/* .line 313 */
v0 = this.this$0;
v0 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmRegisteredMonitor ( v0 );
int v2 = 7; // const/4 v2, 0x7
/* and-int/2addr v0, v2 */
/* if-eq v0, v2, :cond_3 */
/* .line 314 */
com.miui.server.greeze.GreezeManagerDebugConfig.milletEnable = (v1!= 0);
/* .line 315 */
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
(( java.util.HashMap ) v0 ).values ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lmiui/greeze/IGreezeCallback; */
/* .line 317 */
/* .local v1, "callback":Lmiui/greeze/IGreezeCallback; */
try { // :try_start_1
/* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 320 */
/* .line 318 */
/* :catch_0 */
/* move-exception v2 */
/* .line 321 */
} // .end local v1 # "callback":Lmiui/greeze/IGreezeCallback;
} // :goto_2
/* .line 323 */
} // :cond_3
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Monitor (type "; // const-string v2, "Monitor (type "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ") died, gz stop"; // const-string v2, ") died, gz stop"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 324 */
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;->mType:I */
/* if-ne v0, v3, :cond_4 */
/* .line 325 */
v0 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$msetLoopAlarm ( v0 );
/* .line 326 */
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient$1;-><init>(Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;)V */
/* const-wide/16 v2, 0x258 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 332 */
} // :cond_4
return;
/* .line 303 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_2
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v1 */
} // .end method
