.class Lcom/miui/server/greeze/GreezeManagerService$13;
.super Landroid/database/ContentObserver;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 2154
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$13;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 2157
    if-eqz p2, :cond_0

    const-string v0, "cloud_aurogon_alarm_allow_list"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2158
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$13;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmContext(Lcom/miui/server/greeze/GreezeManagerService;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 2160
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2161
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2162
    .local v1, "pkgName":[Ljava/lang/String;
    array-length v2, v1

    if-lez v2, :cond_0

    .line 2163
    iget-object v2, p0, Lcom/miui/server/greeze/GreezeManagerService$13;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v2}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmAurogonAlarmAllowList(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2164
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-object v4, v1, v3

    .line 2165
    .local v4, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$13;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v5}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmAurogonAlarmAllowList(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2164
    .end local v4    # "name":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2170
    .end local v0    # "str":Ljava/lang/String;
    .end local v1    # "pkgName":[Ljava/lang/String;
    :cond_0
    return-void
.end method
