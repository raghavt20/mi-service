.class public Lcom/miui/server/greeze/AurogonAppInfo;
.super Ljava/lang/Object;
.source "AurogonAppInfo.java"


# instance fields
.field public freezeTime:J

.field public hasIcon:Z

.field public isFreezed:Z

.field public isFreezedByGame:Z

.field public isFreezedByLaunchMode:Z

.field public isSystemApp:Z

.field public level:I

.field public mPackageName:Ljava/lang/String;

.field public mUid:I

.field public mUserId:I

.field public unFreezeTime:J


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    .line 18
    iput-object p2, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .line 23
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    instance-of v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;

    if-nez v1, :cond_0

    goto :goto_0

    .line 26
    :cond_0
    move-object v1, p1

    check-cast v1, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 27
    .local v1, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget v2, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    iget v3, v1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    if-ne v2, v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 24
    .end local v1    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_2
    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
