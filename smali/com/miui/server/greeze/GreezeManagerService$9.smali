.class Lcom/miui/server/greeze/GreezeManagerService$9;
.super Landroid/app/IProcessObserver$Stub;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;

    .line 1798
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundActivitiesChanged(IIZ)V
    .locals 3
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "foregroundActivities"    # Z

    .line 1801
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmInited(Lcom/miui/server/greeze/GreezeManagerService;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1802
    :cond_0
    const-string v0, " uid = "

    const-string v1, "Aurogon"

    if-eqz p3, :cond_2

    .line 1803
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " switch to FG"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1804
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1805
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/16 v1, 0x3e8

    const-string v2, "FG activity"

    invoke-virtual {v0, p2, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 1809
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$9$1;

    invoke-direct {v1, p0, p2}, Lcom/miui/server/greeze/GreezeManagerService$9$1;-><init>(Lcom/miui/server/greeze/GreezeManagerService$9;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1837
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " switch to BG"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1838
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$9$2;

    invoke-direct {v1, p0, p2}, Lcom/miui/server/greeze/GreezeManagerService$9$2;-><init>(Lcom/miui/server/greeze/GreezeManagerService$9;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1850
    :goto_0
    return-void
.end method

.method public onForegroundServicesChanged(III)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "serviceTypes"    # I

    .line 1856
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$9$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$9$3;-><init>(Lcom/miui/server/greeze/GreezeManagerService$9;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1861
    return-void
.end method

.method public onProcessDied(II)V
    .locals 0
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 1865
    return-void
.end method
