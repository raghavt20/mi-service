public class com.miui.server.greeze.GreezeManagerDebugConfig {
	 /* .source "GreezeManagerDebugConfig.java" */
	 /* # static fields */
	 public static Boolean DEBUG;
	 static Boolean DEBUG_AIDL;
	 public static Boolean DEBUG_LAUNCH_FROM_HOME;
	 static Boolean DEBUG_MILLET;
	 static final Boolean DEBUG_MONKEY;
	 public static Boolean DEBUG_SKIPUID;
	 public static final Long LAUNCH_FZ_TIMEOUT;
	 public static Boolean PID_DEBUG;
	 static final java.lang.String PROPERTY_GZ_CGROUPV1;
	 static final java.lang.String PROPERTY_GZ_DEBUG;
	 private static final java.lang.String PROPERTY_GZ_ENABLE;
	 static final java.lang.String PROPERTY_GZ_FZTIMEOUT;
	 static final java.lang.String PROPERTY_GZ_HANDSHAKE;
	 static final java.lang.String PROPERTY_GZ_MONKEY;
	 static final java.lang.String PROPERTY_PID_DEBUG;
	 static final java.lang.String PROP_POWERMILLET_ENABLE;
	 public static Boolean mCgroupV1Flag;
	 public static final Boolean mPowerMilletEnable;
	 protected static Boolean milletEnable;
	 protected static Boolean sEnable;
	 /* # direct methods */
	 static com.miui.server.greeze.GreezeManagerDebugConfig ( ) {
		 /* .locals 4 */
		 /* .line 7 */
		 final String v0 = "persist.sys.gz.monkey"; // const-string v0, "persist.sys.gz.monkey"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_MONKEY = (v0!= 0);
		 /* .line 9 */
		 final String v0 = "persist.sys.gz.fztimeout"; // const-string v0, "persist.sys.gz.fztimeout"
		 /* const-wide/16 v2, 0x1388 */
		 android.os.SystemProperties .getLong ( v0,v2,v3 );
		 /* move-result-wide v2 */
		 /* sput-wide v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J */
		 /* .line 13 */
		 final String v0 = "persist.sys.pid.debug"; // const-string v0, "persist.sys.pid.debug"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.greeze.GreezeManagerDebugConfig.PID_DEBUG = (v0!= 0);
		 /* .line 16 */
		 final String v0 = "persist.sys.powmillet.enable"; // const-string v0, "persist.sys.powmillet.enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.greeze.GreezeManagerDebugConfig.mPowerMilletEnable = (v0!= 0);
		 /* .line 17 */
		 com.miui.server.greeze.GreezeManagerDebugConfig.mCgroupV1Flag = (v1!= 0);
		 /* .line 18 */
		 final String v0 = "persist.sys.gz.enable"; // const-string v0, "persist.sys.gz.enable"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.greeze.GreezeManagerDebugConfig.sEnable = (v0!= 0);
		 /* .line 19 */
		 com.miui.server.greeze.GreezeManagerDebugConfig.milletEnable = (v1!= 0);
		 /* .line 21 */
		 final String v0 = "persist.sys.gz.debug"; // const-string v0, "persist.sys.gz.debug"
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG = (v0!= 0);
		 /* .line 22 */
		 com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_SKIPUID = (v0!= 0);
		 /* .line 23 */
		 com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_MILLET = (v0!= 0);
		 /* .line 24 */
		 com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_AIDL = (v0!= 0);
		 /* .line 25 */
		 com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_LAUNCH_FROM_HOME = (v0!= 0);
		 return;
	 } // .end method
	 public com.miui.server.greeze.GreezeManagerDebugConfig ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean isEnable ( ) {
		 /* .locals 1 */
		 /* .line 28 */
		 /* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z */
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 int v0 = 1; // const/4 v0, 0x1
			 } // :cond_0
			 int v0 = 0; // const/4 v0, 0x0
		 } // :goto_0
	 } // .end method
