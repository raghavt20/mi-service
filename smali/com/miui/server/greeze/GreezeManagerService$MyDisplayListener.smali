.class Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Landroid/hardware/display/DisplayManager$DisplayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDisplayListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 0

    .line 3334
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V

    return-void
.end method


# virtual methods
.method public onDisplayAdded(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 3337
    return-void
.end method

.method public onDisplayChanged(I)V
    .locals 9
    .param p1, "displayId"    # I

    .line 3345
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    .line 3346
    .local v0, "uids":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;

    move-result-object v1

    .line 3347
    .local v1, "display":Landroid/view/Display;
    if-nez v1, :cond_0

    .line 3348
    return-void

    .line 3350
    :cond_0
    invoke-virtual {v1}, Landroid/view/Display;->getState()I

    move-result v2

    .line 3351
    .local v2, "state":I
    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    if-eqz p1, :cond_4

    .line 3353
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mActivityTaskManager:Landroid/app/IActivityTaskManager;

    invoke-interface {v3, p1}, Landroid/app/IActivityTaskManager;->getAllRootTaskInfosOnDisplay(I)Ljava/util/List;

    move-result-object v3

    .line 3354
    .local v3, "rootTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityTaskManager$RootTaskInfo;

    .line 3355
    .local v5, "rootTaskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
    if-eqz v5, :cond_1

    iget-boolean v6, v5, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z

    if-eqz v6, :cond_1

    iget-object v6, v5, Landroid/app/ActivityTaskManager$RootTaskInfo;->childTaskNames:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_1

    .line 3356
    iget-object v6, v5, Landroid/app/ActivityTaskManager$RootTaskInfo;->childTaskNames:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-static {v6}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v6

    .line 3357
    .local v6, "componentName":Landroid/content/ComponentName;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 3358
    iget-object v7, p0, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/miui/server/greeze/GreezeManagerService;->getUidByPackageName(Ljava/lang/String;)I

    move-result v7

    .line 3359
    .local v7, "uid":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3362
    .end local v5    # "rootTaskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
    .end local v6    # "componentName":Landroid/content/ComponentName;
    .end local v7    # "uid":I
    :cond_1
    goto :goto_0

    .line 3365
    .end local v3    # "rootTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
    :cond_2
    goto :goto_1

    .line 3363
    :catch_0
    move-exception v3

    .line 3364
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "GreezeManager"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3366
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 3368
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3369
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 3370
    .local v4, "needUnFreezeUid":I
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v5, v4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3371
    iget-object v5, p0, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/16 v6, 0x3e8

    const-string v7, "Display_On"

    invoke-virtual {v5, v4, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    goto :goto_2

    .line 3375
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v4    # "needUnFreezeUid":I
    :cond_4
    return-void
.end method

.method public onDisplayRemoved(I)V
    .locals 0
    .param p1, "displayId"    # I

    .line 3341
    return-void
.end method
