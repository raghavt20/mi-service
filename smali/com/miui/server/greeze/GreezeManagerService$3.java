class com.miui.server.greeze.GreezeManagerService$3 implements java.lang.Runnable {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService;->reportSignal(IIJ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
final Integer val$pid; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$3 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 430 */
this.this$0 = p1;
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
/* iput p3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 8 */
/* .line 432 */
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
v0 = android.os.Process .isIsolated ( v0 );
/* .line 433 */
/* .local v0, "isIsoUid":Z */
int v1 = -1; // const/4 v1, -0x1
/* .line 434 */
/* .local v1, "infoUid":I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 435 */
v2 = this.this$0;
/* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
v1 = com.miui.server.greeze.GreezeManagerService .-$$Nest$mgetInfoUid ( v2,v3 );
/* .line 436 */
/* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 437 */
	 final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "reportSig infoUid = "; // const-string v4, "reportSig infoUid = "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v2,v3 );
	 /* .line 439 */
} // :cond_0
v2 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmFrozenPids ( v2 );
/* monitor-enter v2 */
/* .line 440 */
try { // :try_start_0
	 v3 = this.this$0;
	 com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmFrozenPids ( v3 );
	 /* iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
	 java.lang.Integer .valueOf ( v4 );
	 v4 = 	 (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
	 (( android.util.SparseArray ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
	 /* if-nez v3, :cond_1 */
	 /* .line 441 */
	 final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
	 /* new-instance v4, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = " report signal uid = "; // const-string v5, " report signal uid = "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v5 = " pid = "; // const-string v5, " pid = "
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v5 = "is not frozen."; // const-string v5, "is not frozen."
	 (( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v3,v4 );
	 /* .line 442 */
	 /* monitor-exit v2 */
	 return;
	 /* .line 445 */
} // :cond_1
/* const/16 v3, 0x3e8 */
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* if-lez v1, :cond_2 */
	 /* .line 446 */
	 v4 = this.this$0;
	 final String v5 = "Signal iso"; // const-string v5, "Signal iso"
	 (( com.miui.server.greeze.GreezeManagerService ) v4 ).thawUidAsync ( v1, v3, v5 ); // invoke-virtual {v4, v1, v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
	 /* .line 449 */
} // :cond_2
v4 = this.this$0;
/* iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
v4 = (( com.miui.server.greeze.GreezeManagerService ) v4 ).readPidStatus ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->readPidStatus(I)Z
if ( v4 != null) { // if-eqz v4, :cond_3
	 /* .line 450 */
	 final String v4 = "GreezeManager"; // const-string v4, "GreezeManager"
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = " report signal uid = "; // const-string v6, " report signal uid = "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v6 = " pid = "; // const-string v6, " pid = "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v6 = "is not died."; // const-string v6, "is not died."
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v4,v5 );
	 /* .line 455 */
	 v4 = this.this$0;
	 /* iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
	 final String v6 = "Signal other"; // const-string v6, "Signal other"
	 (( com.miui.server.greeze.GreezeManagerService ) v4 ).thawUidAsync ( v5, v3, v6 ); // invoke-virtual {v4, v5, v3, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
	 /* .line 456 */
	 /* monitor-exit v2 */
	 return;
	 /* .line 459 */
} // :cond_3
v3 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmFrozenPids ( v3 );
/* iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
java.lang.Integer .valueOf ( v4 );
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
(( android.util.SparseArray ) v3 ).remove ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/SparseArray;->remove(I)V
/* .line 460 */
v3 = this.this$0;
/* iget v4, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
com.miui.server.greeze.GreezeManagerService .-$$Nest$mgetFrozenInfo ( v3,v4 );
/* .line 461 */
/* .local v3, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* if-nez v3, :cond_6 */
/* .line 462 */
final String v4 = ""; // const-string v4, ""
/* .line 463 */
/* .local v4, "isolatedPid":Ljava/lang/String; */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
	 /* iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
	 v5 = 	 android.os.UserHandle .isApp ( v5 );
	 if ( v5 != null) { // if-eqz v5, :cond_4
		 /* .line 464 */
		 /* iget v5, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
		 com.miui.server.greeze.FreezeUtils .thawPid ( v5 );
		 /* .line 465 */
		 /* new-instance v5, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v6 = " isolatedPid = "; // const-string v6, " isolatedPid = "
		 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$pid:I */
		 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* move-object v4, v5 */
		 /* .line 467 */
	 } // :cond_4
	 /* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
	 if ( v5 != null) { // if-eqz v5, :cond_5
		 final String v5 = "GreezeManager"; // const-string v5, "GreezeManager"
		 /* new-instance v6, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v7 = "reportSignal null uid = "; // const-string v7, "reportSignal null uid = "
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
		 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .w ( v5,v6 );
		 /* .line 468 */
	 } // :cond_5
	 v5 = this.this$0;
	 /* iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
	 (( com.miui.server.greeze.GreezeManagerService ) v5 ).removePendingBroadcast ( v6 ); // invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->removePendingBroadcast(I)V
	 /* .line 470 */
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 /* .line 471 */
	 /* .local v5, "log":Ljava/lang/StringBuilder; */
	 /* new-instance v6, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v7 = "Died uid = "; // const-string v7, "Died uid = "
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$3;->val$uid:I */
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 472 */
	 v6 = this.this$0;
	 com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmHistoryLog ( v6 );
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( android.util.LocalLog ) v6 ).log ( v7 ); // invoke-virtual {v6, v7}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
	 /* .line 474 */
} // .end local v3 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v4 # "isolatedPid":Ljava/lang/String;
} // .end local v5 # "log":Ljava/lang/StringBuilder;
} // :cond_6
/* monitor-exit v2 */
/* .line 475 */
return;
/* .line 474 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
