.class public final Lcom/miui/server/greeze/GreezeManagerService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/GreezeManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/greeze/GreezeManagerService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 266
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 267
    new-instance v0, Lcom/miui/server/greeze/GreezeManagerService;

    invoke-direct {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$Lifecycle;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    .line 268
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 272
    const-string v0, "greezer"

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$Lifecycle;->mService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 274
    invoke-static {}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$smstartService()V

    .line 275
    return-void
.end method
