class com.miui.server.greeze.GreezeManagerService$9 extends android.app.IProcessObserver$Stub {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$9 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .line 1798 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/app/IProcessObserver$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundActivitiesChanged ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "foregroundActivities" # Z */
/* .line 1801 */
v0 = this.this$0;
v0 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmInited ( v0 );
/* if-nez v0, :cond_0 */
return;
/* .line 1802 */
} // :cond_0
final String v0 = " uid = "; // const-string v0, " uid = "
final String v1 = "Aurogon"; // const-string v1, "Aurogon"
if ( p3 != null) { // if-eqz p3, :cond_2
/* .line 1803 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " switch to FG"; // const-string v2, " switch to FG"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1804 */
v0 = this.this$0;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isUidFrozen ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 1805 */
	 v0 = this.this$0;
	 /* const/16 v1, 0x3e8 */
	 final String v2 = "FG activity"; // const-string v2, "FG activity"
	 (( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUid ( p2, v1, v2 ); // invoke-virtual {v0, p2, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
	 /* .line 1809 */
} // :cond_1
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$9$1; */
/* invoke-direct {v1, p0, p2}, Lcom/miui/server/greeze/GreezeManagerService$9$1;-><init>(Lcom/miui/server/greeze/GreezeManagerService$9;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1837 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " switch to BG"; // const-string v2, " switch to BG"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1838 */
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$9$2; */
/* invoke-direct {v1, p0, p2}, Lcom/miui/server/greeze/GreezeManagerService$9$2;-><init>(Lcom/miui/server/greeze/GreezeManagerService$9;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1850 */
} // :goto_0
return;
} // .end method
public void onForegroundServicesChanged ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "serviceTypes" # I */
/* .line 1856 */
v0 = this.this$0;
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$9$3; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$9$3;-><init>(Lcom/miui/server/greeze/GreezeManagerService$9;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1861 */
return;
} // .end method
public void onProcessDied ( Integer p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 1865 */
return;
} // .end method
