class com.miui.server.greeze.GreezeManagerService$LocalService extends com.miui.server.greeze.GreezeManagerInternal {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "LocalService" */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.greeze.GreezeManagerService$LocalService ( ) {
/* .locals 0 */
/* .line 3173 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerInternal;-><init>()V */
return;
} // .end method
 com.miui.server.greeze.GreezeManagerService$LocalService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$LocalService;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
return;
} // .end method
/* # virtual methods */
public Boolean checkAurogonIntentDenyList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 3315 */
v0 = this.this$0;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).checkAurogonIntentDenyList ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->checkAurogonIntentDenyList(Ljava/lang/String;)Z
} // .end method
public void finishLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 3279 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).finishLaunchMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->finishLaunchMode(Ljava/lang/String;I)V
/* .line 3280 */
return;
} // .end method
public Boolean freezePid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 3289 */
v0 = com.miui.server.greeze.FreezeUtils .freezePid ( p1 );
} // .end method
public Boolean freezePid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 3294 */
v0 = com.miui.server.greeze.FreezeUtils .freezePid ( p1,p2 );
} // .end method
public java.util.List freezePids ( Integer[] p0, Long p1, Integer p2, java.lang.String p3 ) {
/* .locals 6 */
/* .param p1, "pids" # [I */
/* .param p2, "timeout" # J */
/* .param p4, "fromWho" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([IJI", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 3269 */
v0 = this.this$0;
/* move-object v1, p1 */
/* move-wide v2, p2 */
/* move v4, p4 */
/* move-object v5, p5 */
/* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/greeze/GreezeManagerService;->freezePids([IJILjava/lang/String;)Ljava/util/List; */
} // .end method
public java.util.List freezeUids ( Integer[] p0, Long p1, Integer p2, java.lang.String p3, Boolean p4 ) {
/* .locals 7 */
/* .param p1, "uids" # [I */
/* .param p2, "timeout" # J */
/* .param p4, "fromWho" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .param p6, "checkAudioGps" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([IJI", */
/* "Ljava/lang/String;", */
/* "Z)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 3264 */
v0 = this.this$0;
/* move-object v1, p1 */
/* move-wide v2, p2 */
/* move v4, p4 */
/* move-object v5, p5 */
/* move v6, p6 */
/* invoke-virtual/range {v0 ..v6}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List; */
} // .end method
public getFrozenPids ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "module" # I */
/* .line 3194 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getFrozenPids ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenPids(I)[I
} // .end method
public getFrozenUids ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "module" # I */
/* .line 3189 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getFrozenUids ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I
} // .end method
public Long getLastThawedTime ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "module" # I */
/* .line 3219 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getLastThawedTime ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->getLastThawedTime(II)J
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public void handleAppZygoteStart ( android.content.pm.ApplicationInfo p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "start" # Z */
/* .line 3330 */
v0 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$mhandleAppZygoteStart ( v0,p1,p2 );
/* .line 3331 */
return;
} // .end method
public Boolean isNeedCachedBroadcast ( android.content.Intent p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .param p4, "force" # Z */
/* .line 3320 */
v0 = this.this$0;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isNeedCachedBroadcast ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;)Z
} // .end method
public Boolean isRestrictBackgroundAction ( java.lang.String p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 6 */
/* .param p1, "localhost" # Ljava/lang/String; */
/* .param p2, "callerUid" # I */
/* .param p3, "callerPkgName" # Ljava/lang/String; */
/* .param p4, "calleeUid" # I */
/* .param p5, "calleePkgName" # Ljava/lang/String; */
/* .line 3178 */
v0 = this.this$0;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
/* move-object v5, p5 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/greeze/GreezeManagerService;->isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z */
} // .end method
public Boolean isRestrictReceiver ( android.content.Intent p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 6 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callerUid" # I */
/* .param p3, "callerPkgName" # Ljava/lang/String; */
/* .param p4, "calleeUid" # I */
/* .param p5, "calleePkgName" # Ljava/lang/String; */
/* .line 3325 */
v0 = this.this$0;
/* move-object v1, p1 */
/* move v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
/* move-object v5, p5 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/greeze/GreezeManagerService;->isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z */
} // .end method
public Boolean isUidFrozen ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 3184 */
v0 = this.this$0;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isUidFrozen ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
} // .end method
public void notifyBackup ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "start" # Z */
/* .line 3204 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyBackup ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyBackup(IZ)V
/* .line 3205 */
return;
} // .end method
public void notifyDumpAllInfo ( ) {
/* .locals 1 */
/* .line 3214 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyDumpAllInfo ( ); // invoke-virtual {v0}, Lcom/miui/server/greeze/GreezeManagerService;->notifyDumpAllInfo()V
/* .line 3215 */
return;
} // .end method
public void notifyDumpAppInfo ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 3209 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyDumpAppInfo ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyDumpAppInfo(II)V
/* .line 3210 */
return;
} // .end method
public void notifyExcuteServices ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 3199 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyExcuteServices ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->notifyExcuteServices(I)V
/* .line 3200 */
return;
} // .end method
public void notifyFreeformModeFocus ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "mode" # I */
/* .line 3239 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyFreeformModeFocus ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyFreeformModeFocus(Ljava/lang/String;I)V
/* .line 3240 */
return;
} // .end method
public void notifyMovetoFront ( Integer p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "inFreeformSmallWinMode" # Z */
/* .line 3229 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyMovetoFront ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyMovetoFront(IZ)V
/* .line 3230 */
return;
} // .end method
public void notifyMultitaskLaunch ( Integer p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 3234 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyMultitaskLaunch ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->notifyMultitaskLaunch(ILjava/lang/String;)V
/* .line 3235 */
return;
} // .end method
public void notifyResumeTopActivity ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "inMultiWindowMode" # Z */
/* .line 3224 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).notifyResumeTopActivity ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->notifyResumeTopActivity(ILjava/lang/String;Z)V
/* .line 3225 */
return;
} // .end method
public void queryBinderState ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 3274 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).queryBinderState ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V
/* .line 3275 */
return;
} // .end method
public Boolean registerCallback ( miui.greeze.IGreezeCallback p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "callback" # Lmiui/greeze/IGreezeCallback; */
/* .param p2, "module" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 3244 */
v0 = this.this$0;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).registerCallback ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
} // .end method
public Boolean thawPid ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .line 3299 */
v0 = com.miui.server.greeze.FreezeUtils .thawPid ( p1 );
} // .end method
public Boolean thawPid ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 3304 */
v0 = com.miui.server.greeze.FreezeUtils .thawPid ( p1,p2 );
} // .end method
public java.util.List thawPids ( Integer[] p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "pids" # [I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 3259 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawPids ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;
} // .end method
public Boolean thawUid ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 3249 */
v0 = this.this$0;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUid ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
} // .end method
public java.util.List thawUids ( Integer[] p0, Integer p1, java.lang.String p2 ) {
/* .locals 1 */
/* .param p1, "uids" # [I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 3254 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUids ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
} // .end method
public void triggerLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 3284 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).triggerLaunchMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->triggerLaunchMode(Ljava/lang/String;I)V
/* .line 3285 */
return;
} // .end method
public void updateOrderBCStatus ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3 ) {
/* .locals 1 */
/* .param p1, "intentAction" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "isforeground" # Z */
/* .param p4, "allow" # Z */
/* .line 3310 */
v0 = this.this$0;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).updateOrderBCStatus ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/greeze/GreezeManagerService;->updateOrderBCStatus(Ljava/lang/String;IZZ)V
/* .line 3311 */
return;
} // .end method
