.class public Lcom/miui/server/greeze/FreezeUtils;
.super Ljava/lang/Object;
.source "FreezeUtils.java"


# static fields
.field static DEBUG_CHECK_FREEZE:Z = false

.field static DEBUG_CHECK_THAW:Z = false

.field private static final FREEAER_FREEAE:Ljava/lang/String; = "/sys/fs/cgroup/frozen/cgroup.freeze"

.field private static final FREEZER_CGROUP_FROZEN:Ljava/lang/String; = "/sys/fs/cgroup/frozen"

.field private static final FREEZER_CGROUP_THAWED:Ljava/lang/String; = "/sys/fs/cgroup/unfrozen"

.field private static final FREEZER_FROZEN_PORCS:Ljava/lang/String; = "/sys/fs/cgroup/frozen/cgroup.procs"

.field private static final FREEZER_ROOT_PATH:Ljava/lang/String; = "/sys/fs/cgroup"

.field private static final FREEZE_ACTION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "FreezeUtils"

.field private static final UNFREEZE_ACTION:I = 0x0

.field private static final V1_FREEZER_CGROUP_FROZEN:Ljava/lang/String; = "/sys/fs/cgroup/freezer/perf/frozen"

.field private static final V1_FREEZER_CGROUP_THAWED:Ljava/lang/String; = "/sys/fs/cgroup/freezer/perf/thawed"

.field private static final V1_FREEZER_FROZEN_PORCS:Ljava/lang/String; = "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"

.field private static final V1_FREEZER_FROZEN_TASKS:Ljava/lang/String; = "/sys/fs/cgroup/freezer/perf/frozen/tasks"

.field private static final V1_FREEZER_ROOT_PATH:Ljava/lang/String; = "/sys/fs/cgroup/freezer"

.field private static final V1_FREEZER_THAWED_PORCS:Ljava/lang/String; = "/sys/fs/cgroup/freezer/perf/thawed/cgroup.procs"

.field private static final V1_FREEZER_THAWED_TASKS:Ljava/lang/String; = "/sys/fs/cgroup/freezer/perf/thawed/tasks"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    const/4 v0, 0x1

    sput-boolean v0, Lcom/miui/server/greeze/FreezeUtils;->DEBUG_CHECK_FREEZE:Z

    .line 24
    sput-boolean v0, Lcom/miui/server/greeze/FreezeUtils;->DEBUG_CHECK_THAW:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static freezePid(I)Z
    .locals 4
    .param p0, "pid"    # I

    .line 126
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    const-string v1, "FreezeUtils"

    if-eqz v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Freeze pid "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_0
    const-string v0, "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"

    invoke-static {v0, p0}, Lcom/miui/server/greeze/FreezeUtils;->writeNode(Ljava/lang/String;I)Z

    move-result v0

    .line 130
    .local v0, "done":Z
    sget-boolean v2, Lcom/miui/server/greeze/FreezeUtils;->DEBUG_CHECK_FREEZE:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/miui/server/greeze/FreezeUtils;->isFrozonPid(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to thaw pid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", it\'s still thawed!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/4 v0, 0x0

    .line 134
    :cond_1
    return v0
.end method

.method public static freezePid(II)Z
    .locals 2
    .param p0, "pid"    # I
    .param p1, "uid"    # I

    .line 101
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Freeze pid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FreezeUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/miui/server/greeze/FreezeUtils;->setFreezeAction(IIZ)Z

    move-result v0

    .line 106
    .local v0, "done":Z
    return v0
.end method

.method public static freezeTid(I)Z
    .locals 2
    .param p0, "tid"    # I

    .line 139
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Freeze tid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FreezeUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_0
    const-string v0, "/sys/fs/cgroup/freezer/perf/frozen/tasks"

    invoke-static {v0, p0}, Lcom/miui/server/greeze/FreezeUtils;->writeNode(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static getFrozenPids()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 45
    const-string v0, "FreezeUtils"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v1, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v2, :cond_0

    const-string v2, "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"

    goto :goto_0

    :cond_0
    const-string v2, "/sys/fs/cgroup/frozen/cgroup.procs"

    .line 47
    .local v2, "path":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 49
    .local v3, "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v5, v4

    .local v5, "line":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 51
    :try_start_2
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    goto :goto_1

    .line 52
    :catch_0
    move-exception v4

    .line 53
    .local v4, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to parse "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " line: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 54
    nop

    .end local v4    # "e":Ljava/lang/NumberFormatException;
    goto :goto_1

    .line 56
    .end local v5    # "line":Ljava/lang/String;
    :cond_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 58
    .end local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 47
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v4

    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v5

    :try_start_6
    invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "path":Ljava/lang/String;
    :goto_2
    throw v4
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 56
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v2    # "path":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 57
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "Failed to get frozen pids"

    invoke-static {v0, v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 59
    .end local v3    # "e":Ljava/io/IOException;
    :goto_3
    return-object v1
.end method

.method public static getFrozonTids()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 64
    const-string v0, "FreezeUtils"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v1, "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    const-string v4, "/sys/fs/cgroup/freezer/perf/frozen/tasks"

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 67
    .local v2, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v4, v3

    .local v4, "line":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 69
    :try_start_2
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    goto :goto_0

    .line 70
    :catch_0
    move-exception v3

    .line 71
    .local v3, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to parse /sys/fs/cgroup/freezer/perf/frozen/tasks line: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    nop

    .end local v3    # "e":Ljava/lang/NumberFormatException;
    goto :goto_0

    .line 74
    .end local v4    # "line":Ljava/lang/String;
    :cond_0
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 76
    .end local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 65
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_6
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_1
    throw v3
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 74
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catch_1
    move-exception v2

    .line 75
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "Failed to get frozen tids"

    invoke-static {v0, v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 77
    .end local v2    # "e":Ljava/io/IOException;
    :goto_2
    return-object v1
.end method

.method public static isAllFrozon([I)Z
    .locals 6
    .param p0, "pids"    # [I

    .line 86
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v0

    .line 87
    .local v0, "frozen":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    array-length v1, p0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget v4, p0, v3

    .line 88
    .local v4, "pid":I
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 89
    return v2

    .line 87
    .end local v4    # "pid":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 92
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method public static isFreezerEnable()Z
    .locals 3

    .line 39
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/fs/cgroup/frozen"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "groupFrozen":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/fs/cgroup/unfrozen"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 41
    .local v1, "groupThawed":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public static isFrozonPid(I)Z
    .locals 2
    .param p0, "pid"    # I

    .line 82
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozenPids()Ljava/util/List;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static isFrozonTid(I)Z
    .locals 2
    .param p0, "tid"    # I

    .line 96
    invoke-static {}, Lcom/miui/server/greeze/FreezeUtils;->getFrozonTids()Ljava/util/List;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static setFreezeAction(IIZ)Z
    .locals 5
    .param p0, "pid"    # I
    .param p1, "uid"    # I
    .param p2, "allow"    # Z

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/sys/fs/cgroup/uid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/pid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/cgroup.freeze"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "path":Ljava/lang/String;
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    .local v2, "writer":Ljava/io/PrintWriter;
    const/4 v3, 0x1

    if-eqz p2, :cond_0

    .line 113
    :try_start_1
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    :goto_0
    nop

    .line 118
    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 117
    return v3

    .line 111
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "path":Ljava/lang/String;
    .end local p0    # "pid":I
    .end local p1    # "uid":I
    .end local p2    # "allow":Z
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 118
    .end local v2    # "writer":Ljava/io/PrintWriter;
    .restart local v0    # "path":Ljava/lang/String;
    .restart local p0    # "pid":I
    .restart local p1    # "uid":I
    .restart local p2    # "allow":Z
    :catch_0
    move-exception v2

    .line 119
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to write to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "FreezeUtils"

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    .end local v2    # "e":Ljava/io/IOException;
    return v1
.end method

.method public static thawPid(I)Z
    .locals 4
    .param p0, "pid"    # I

    .line 157
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    const-string v1, "FreezeUtils"

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Thaw pid "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    const-string v0, "/sys/fs/cgroup/freezer/perf/thawed/cgroup.procs"

    invoke-static {v0, p0}, Lcom/miui/server/greeze/FreezeUtils;->writeNode(Ljava/lang/String;I)Z

    move-result v0

    .line 161
    .local v0, "done":Z
    sget-boolean v2, Lcom/miui/server/greeze/FreezeUtils;->DEBUG_CHECK_THAW:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/miui/server/greeze/FreezeUtils;->isFrozonPid(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 162
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to thaw pid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", it\'s still frozen!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v0, 0x0

    .line 165
    :cond_1
    return v0
.end method

.method public static thawPid(II)Z
    .locals 2
    .param p0, "pid"    # I
    .param p1, "uid"    # I

    .line 147
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thaw pid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FreezeUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/miui/server/greeze/FreezeUtils;->setFreezeAction(IIZ)Z

    move-result v0

    .line 152
    .local v0, "done":Z
    return v0
.end method

.method public static thawTid(I)Z
    .locals 2
    .param p0, "tid"    # I

    .line 170
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thaw tid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FreezeUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    const-string v0, "/sys/fs/cgroup/freezer/perf/thawed/tasks"

    invoke-static {v0, p0}, Lcom/miui/server/greeze/FreezeUtils;->writeNode(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static writeFreezeValue()Z
    .locals 2

    .line 177
    const-string v0, "/sys/fs/cgroup/frozen/cgroup.freeze"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/greeze/FreezeUtils;->writeNode(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method private static writeNode(Ljava/lang/String;I)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "val"    # I

    .line 181
    const-string v0, " with value "

    const-string v1, "FreezeUtils"

    :try_start_0
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, p0}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .local v2, "writer":Ljava/io/PrintWriter;
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 183
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 184
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wrote to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :cond_0
    nop

    .line 187
    :try_start_2
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 186
    const/4 v0, 0x1

    return v0

    .line 181
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local p0    # "path":Ljava/lang/String;
    .end local p1    # "val":I
    :goto_0
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 187
    .end local v2    # "writer":Ljava/io/PrintWriter;
    .restart local p0    # "path":Ljava/lang/String;
    .restart local p1    # "val":I
    :catch_0
    move-exception v2

    .line 188
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to write to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 190
    .end local v2    # "e":Ljava/io/IOException;
    const/4 v0, 0x0

    return v0
.end method
