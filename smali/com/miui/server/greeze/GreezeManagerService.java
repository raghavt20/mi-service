public class com.miui.server.greeze.GreezeManagerService extends miui.greeze.IGreezeManager$Stub {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$H;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$LocalService;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;, */
	 /* Lcom/miui/server/greeze/GreezeManagerService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BINDER_STATE_IN_BUSY;
public static final Integer BINDER_STATE_IN_IDLE;
public static final Integer BINDER_STATE_IN_TRANSACTION;
public static final Integer BINDER_STATE_PROC_IN_BUSY;
public static final Integer BINDER_STATE_THREAD_IN_BUSY;
private static final java.lang.String CLOUD_AUROGON_ALARM_ALLOW_LIST;
private static final java.lang.String CLOUD_GREEZER_ENABLE;
private static final Integer CMD_ADJ_ADD;
private static final Integer CMD_ADJ_REMOVE;
private static final Integer CMD_ADJ_THAWALL;
private static final Integer DUMPSYS_HISTORY_DURATION;
private static final Integer HISTORY_SIZE;
private static final android.util.Singleton IGreezeManagerSingleton;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Singleton<", */
/* "Lmiui/greeze/IGreezeManager;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final Long LOOPONCE_DELAY_TIME;
private static final Integer MAX_HISTORY_ITEMS;
private static final Long MILLET_DELAY_CEILING;
private static final Long MILLET_DELAY_THRASHOLD;
private static final Integer MILLET_MONITOR_ALL;
private static final Integer MILLET_MONITOR_BINDER;
private static final Integer MILLET_MONITOR_NET;
private static final Integer MILLET_MONITOR_SIGNAL;
public static final java.lang.String SERVICE_NAME;
public static final java.lang.String TAG;
private static final java.lang.String TIME_FORMAT_PATTERN;
static java.util.HashMap callbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/Integer;", */
/* "Lmiui/greeze/IGreezeCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mAllowBroadcast;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mCNDeferBroadcast;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mMiuiDeferBroadcast;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List mNeedCachedBroadcast;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
public java.util.List DISABLE_IMMOB_MODE_DEVICE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List ENABLE_LAUNCH_MODE_DEVICE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List ENABLE_VIDEO_MODE_DEVICE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.app.AlarmManager am;
private android.net.ConnectivityManager cm;
private Boolean isBarExpand;
private android.util.SparseArray isoPids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mActivityCtrlBlackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.app.ActivityManager mActivityManager;
private final com.android.server.am.ActivityManagerService mActivityManagerService;
android.app.IActivityTaskManager mActivityTaskManager;
private com.miui.server.greeze.GreezeManagerService$AlarmListener mAlarmLoop;
android.app.IAlarmManager mAlarmManager;
private java.lang.String mAudioZeroPkgs;
private java.util.List mAurogonAlarmAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mAurogonAlarmForbidList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.lang.Object mAurogonLock;
public java.lang.String mBGOrderBroadcastAction;
public Integer mBGOrderBroadcastAppUid;
private java.util.Map mBroadcastCallerWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private Boolean mBroadcastCtrlCloud;
private java.util.Map mBroadcastCtrlMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List mBroadcastIntentDenyList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mBroadcastTargetWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
public java.util.Map mCachedBCList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/List<", */
/* "Landroid/content/Intent;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
final android.database.ContentObserver mCloudAurogonAlarmListObserver;
private android.content.Context mContext;
android.hardware.display.DisplayManager mDisplayManager;
android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
public java.util.List mExcuteServiceList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.lang.String mFGOrderBroadcastAction;
public Integer mFGOrderBroadcastAppUid;
public java.util.List mFreeformSmallWinList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.greeze.GreezeManagerService$FrozenInfo mFrozenHistory;
private final android.util.SparseArray mFrozenPids;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mFsgNavBar;
private java.lang.reflect.Method mGetCastPid;
public android.os.Handler mHandler;
private Integer mHistoryIndexNext;
private android.util.LocalLog mHistoryLog;
public com.miui.server.greeze.AurogonImmobulusMode mImmobulusMode;
private java.util.Map mImmobulusModeWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
public Boolean mInFreeformSmallWinMode;
private Boolean mInitCtsStatused;
private Boolean mInited;
private Integer mLoopCount;
private final android.util.SparseArray mMonitorTokens;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lmiui/greeze/IMonitorToken;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.os.INetworkManagementService mNms;
private android.content.pm.PackageManager mPm;
private final android.app.IProcessObserver mProcessObserver;
public java.util.List mRecentLaunchAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Integer mRegisteredMonitor;
public Boolean mScreenOnOff;
private Integer mSystemUiPid;
private com.android.server.ServiceThread mThread;
public java.lang.String mTopAppPackageName;
public Integer mTopAppUid;
private java.util.Set mUidAdjs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.app.IUidObserver mUidObserver;
public android.view.IWindowManager mWindowManager;
private java.lang.Object powerManagerServiceImpl;
/* # direct methods */
static java.util.List -$$Nest$fgetENABLE_VIDEO_MODE_DEVICE ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.ENABLE_VIDEO_MODE_DEVICE;
} // .end method
static android.util.SparseArray -$$Nest$fgetisoPids ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.isoPids;
} // .end method
static com.android.server.am.ActivityManagerService -$$Nest$fgetmActivityManagerService ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mActivityManagerService;
} // .end method
static java.lang.String -$$Nest$fgetmAudioZeroPkgs ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAudioZeroPkgs;
} // .end method
static java.util.List -$$Nest$fgetmAurogonAlarmAllowList ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAurogonAlarmAllowList;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static android.util.SparseArray -$$Nest$fgetmFrozenPids ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFrozenPids;
} // .end method
static android.util.LocalLog -$$Nest$fgetmHistoryLog ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHistoryLog;
} // .end method
static Boolean -$$Nest$fgetmInited ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z */
} // .end method
static Integer -$$Nest$fgetmLoopCount ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I */
} // .end method
static android.util.SparseArray -$$Nest$fgetmMonitorTokens ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMonitorTokens;
} // .end method
static Integer -$$Nest$fgetmRegisteredMonitor ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
} // .end method
static java.util.Set -$$Nest$fgetmUidAdjs ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mUidAdjs;
} // .end method
static void -$$Nest$fputisBarExpand ( com.miui.server.greeze.GreezeManagerService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->isBarExpand:Z */
return;
} // .end method
static void -$$Nest$fputmAudioZeroPkgs ( com.miui.server.greeze.GreezeManagerService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mAudioZeroPkgs = p1;
return;
} // .end method
static void -$$Nest$fputmLoopCount ( com.miui.server.greeze.GreezeManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I */
return;
} // .end method
static void -$$Nest$fputmRegisteredMonitor ( com.miui.server.greeze.GreezeManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
return;
} // .end method
static void -$$Nest$fputmSystemUiPid ( com.miui.server.greeze.GreezeManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I */
return;
} // .end method
static void -$$Nest$mcheckPermission ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
return;
} // .end method
static void -$$Nest$mgetAlarmManagerService ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getAlarmManagerService()V */
return;
} // .end method
static com.miui.server.greeze.GreezeManagerService$FrozenInfo -$$Nest$mgetFrozenInfo ( com.miui.server.greeze.GreezeManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
} // .end method
static Integer -$$Nest$mgetInfoUid ( com.miui.server.greeze.GreezeManagerService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getInfoUid(I)I */
} // .end method
static void -$$Nest$mgetNavBarInfo ( com.miui.server.greeze.GreezeManagerService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getNavBarInfo(Landroid/content/Context;)V */
return;
} // .end method
static Integer -$$Nest$mgetSystemUiPid ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getSystemUiPid()I */
} // .end method
static void -$$Nest$mhandleAppZygoteStart ( com.miui.server.greeze.GreezeManagerService p0, android.content.pm.ApplicationInfo p1, Boolean p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V */
return;
} // .end method
static void -$$Nest$msetLoopAlarm ( com.miui.server.greeze.GreezeManagerService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->setLoopAlarm()V */
return;
} // .end method
static void -$$Nest$smnLoopOnce ( ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.greeze.GreezeManagerService .nLoopOnce ( );
return;
} // .end method
static void -$$Nest$smstartService ( ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.greeze.GreezeManagerService .startService ( );
return;
} // .end method
static com.miui.server.greeze.GreezeManagerService ( ) {
/* .locals 12 */
/* .line 85 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MONKEY:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* const/16 v0, 0x4000 */
} // :cond_0
/* const/16 v0, 0x1000 */
} // :goto_0
/* .line 99 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
final String v2 = "android.intent.action.SCREEN_ON"; // const-string v2, "android.intent.action.SCREEN_ON"
final String v3 = "android.intent.action.ACTION_POWER_DISCONNECTED"; // const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"
final String v4 = "android.intent.action.ACTION_POWER_CONNECTED"; // const-string v4, "android.intent.action.ACTION_POWER_CONNECTED"
final String v5 = "android.os.action.DEVICE_IDLE_MODE_CHANGED"; // const-string v5, "android.os.action.DEVICE_IDLE_MODE_CHANGED"
final String v6 = "android.os.action.LIGHT_DEVICE_IDLE_MODE_CHANGED"; // const-string v6, "android.os.action.LIGHT_DEVICE_IDLE_MODE_CHANGED"
final String v7 = "com.ss.android.ugc.aweme.search.widget.APPWIDGET_RESET_ALARM"; // const-string v7, "com.ss.android.ugc.aweme.search.widget.APPWIDGET_RESET_ALARM"
final String v8 = "com.tencent.mm.TrafficStatsReceiver"; // const-string v8, "com.tencent.mm.TrafficStatsReceiver"
final String v9 = "com.xiaomi.mipush.MESSAGE_ARRIVED"; // const-string v9, "com.xiaomi.mipush.MESSAGE_ARRIVED"
final String v10 = "com.tencent.mm.plugin.report.service.KVCommCrossProcessReceiver"; // const-string v10, "com.tencent.mm.plugin.report.service.KVCommCrossProcessReceiver"
final String v11 = "android.intent.action.wutao"; // const-string v11, "android.intent.action.wutao"
/* filled-new-array/range {v1 ..v11}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 104 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "android.intent.action.MY_PACKAGE_REPLACED"; // const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"
/* filled-new-array {v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 107 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "android.intent.action.BATTERY_CHANGED"; // const-string v1, "android.intent.action.BATTERY_CHANGED"
/* filled-new-array {v1}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 108 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.google.android.gcm.CONNECTED"; // const-string v1, "com.google.android.gcm.CONNECTED"
final String v2 = "com.google.android.gms.gcm.HEARTBEAT_ALARM"; // const-string v2, "com.google.android.gms.gcm.HEARTBEAT_ALARM"
final String v3 = "com.google.android.intent.action.GCM_RECONNECT"; // const-string v3, "com.google.android.intent.action.GCM_RECONNECT"
final String v4 = "com.google.android.gcm.DISCONNECTED"; // const-string v4, "com.google.android.gcm.DISCONNECTED"
/* filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 237 */
/* new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$2; */
/* invoke-direct {v0}, Lcom/miui/server/greeze/GreezeManagerService$2;-><init>()V */
/* .line 668 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
public com.miui.server.greeze.GreezeManagerService ( ) {
/* .locals 49 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 176 */
/* move-object/from16 v1, p0 */
/* invoke-direct/range {p0 ..p0}, Lmiui/greeze/IGreezeManager$Stub;-><init>()V */
/* .line 88 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const-string/jumbo v2, "thor" */
final String v3 = "dagu"; // const-string v3, "dagu"
/* const-string/jumbo v4, "zizhan" */
/* const-string/jumbo v5, "unicorn" */
final String v6 = "mayfly"; // const-string v6, "mayfly"
final String v7 = "cupid"; // const-string v7, "cupid"
/* const-string/jumbo v8, "zeus" */
final String v9 = "nuwa"; // const-string v9, "nuwa"
final String v10 = "fuxi"; // const-string v10, "fuxi"
/* const-string/jumbo v11, "socrates" */
final String v12 = "marble"; // const-string v12, "marble"
final String v13 = "liuqin"; // const-string v13, "liuqin"
final String v14 = "pipa"; // const-string v14, "pipa"
final String v15 = "ishtar"; // const-string v15, "ishtar"
final String v16 = "daumier"; // const-string v16, "daumier"
final String v17 = "matisse"; // const-string v17, "matisse"
final String v18 = "rubens"; // const-string v18, "rubens"
/* const-string/jumbo v19, "xaga" */
/* const-string/jumbo v20, "yuechu" */
/* const-string/jumbo v21, "sky" */
final String v22 = "pearl"; // const-string v22, "pearl"
final String v23 = "babylon"; // const-string v23, "babylon"
/* const-string/jumbo v24, "selene" */
/* const-string/jumbo v25, "veux" */
final String v26 = "earth"; // const-string v26, "earth"
final String v27 = "evergo"; // const-string v27, "evergo"
final String v28 = "corot"; // const-string v28, "corot"
/* const-string/jumbo v29, "yudi" */
/* const-string/jumbo v30, "xun" */
final String v31 = "river"; // const-string v31, "river"
/* const-string/jumbo v32, "shennong" */
final String v33 = "houji"; // const-string v33, "houji"
final String v34 = "manet"; // const-string v34, "manet"
/* const-string/jumbo v35, "vermeer" */
final String v36 = "duchamp"; // const-string v36, "duchamp"
final String v37 = "mondrian"; // const-string v37, "mondrian"
/* const-string/jumbo v38, "sheng" */
final String v39 = "aurora"; // const-string v39, "aurora"
final String v40 = "ruyi"; // const-string v40, "ruyi"
final String v41 = "chenfeng"; // const-string v41, "chenfeng"
final String v42 = "garnet"; // const-string v42, "garnet"
/* const-string/jumbo v43, "zircon" */
final String v44 = "dizi"; // const-string v44, "dizi"
final String v45 = "goku"; // const-string v45, "goku"
final String v46 = "peridot"; // const-string v46, "peridot"
final String v47 = "breeze"; // const-string v47, "breeze"
final String v48 = "ruan"; // const-string v48, "ruan"
/* filled-new-array/range {v2 ..v48}, [Ljava/lang/String; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.ENABLE_LAUNCH_MODE_DEVICE = v0;
/* .line 89 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v2 = "river"; // const-string v2, "river"
final String v3 = "breeze"; // const-string v3, "breeze"
/* const-string/jumbo v4, "sky" */
/* filled-new-array {v4, v2, v3}, [Ljava/lang/String; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.DISABLE_IMMOB_MODE_DEVICE = v0;
/* .line 90 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v2 = "liuqin"; // const-string v2, "liuqin"
final String v3 = "pipa"; // const-string v3, "pipa"
final String v4 = "fuxi"; // const-string v4, "fuxi"
/* const-string/jumbo v5, "yuechu" */
/* const-string/jumbo v6, "yudi" */
/* const-string/jumbo v7, "shennong" */
final String v8 = "houji"; // const-string v8, "houji"
/* const-string/jumbo v9, "sheng" */
final String v10 = "aurora"; // const-string v10, "aurora"
final String v11 = "chenfeng"; // const-string v11, "chenfeng"
/* filled-new-array/range {v2 ..v11}, [Ljava/lang/String; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.ENABLE_VIDEO_MODE_DEVICE = v0;
/* .line 97 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mFrozenPids = v0;
/* .line 98 */
final String v0 = "com.tencent.lolm, com.netease.dwrg, com.tencent.tmgp.pubgm, com.tencent.tmgp.dwrg, com.tencent.tmgp.pubgmhd, com.tencent.tmgp.sgame, com.netease.dwrg.mi, com.duowan.kiwi"; // const-string v0, "com.tencent.lolm, com.netease.dwrg, com.tencent.tmgp.pubgm, com.tencent.tmgp.dwrg, com.tencent.tmgp.pubgmhd, com.tencent.tmgp.sgame, com.netease.dwrg.mi, com.duowan.kiwi"
this.mAudioZeroPkgs = v0;
/* .line 105 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mImmobulusModeWhiteList = v0;
/* .line 106 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v2 = "com.tencent.android.qqdownloader"; // const-string v2, "com.tencent.android.qqdownloader"
/* filled-new-array {v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v2 );
/* invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mActivityCtrlBlackList = v0;
/* .line 109 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mBroadcastTargetWhiteList = v0;
/* .line 110 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mBroadcastCallerWhiteList = v0;
/* .line 112 */
/* new-array v0, v0, [Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
this.mFrozenHistory = v0;
/* .line 116 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I */
/* .line 118 */
/* new-instance v2, Ljava/lang/Object; */
/* invoke-direct {v2}, Ljava/lang/Object;-><init>()V */
this.mAurogonLock = v2;
/* .line 119 */
int v2 = 0; // const/4 v2, 0x0
this.mImmobulusMode = v2;
/* .line 120 */
this.cm = v2;
/* .line 121 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z */
/* .line 122 */
/* iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z */
/* .line 288 */
/* new-instance v4, Landroid/util/SparseArray; */
/* invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V */
this.mMonitorTokens = v4;
/* .line 335 */
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener; */
/* invoke-direct {v4, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$AlarmListener-IA;)V */
this.mAlarmLoop = v4;
/* .line 336 */
this.am = v2;
/* .line 337 */
/* iput v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I */
/* .line 403 */
/* new-instance v4, Landroid/util/SparseArray; */
/* invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V */
this.isoPids = v4;
/* .line 642 */
/* iput-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mInitCtsStatused:Z */
/* .line 1078 */
/* new-instance v4, Ljava/util/HashSet; */
/* invoke-direct {v4}, Ljava/util/HashSet;-><init>()V */
this.mUidAdjs = v4;
/* .line 1725 */
this.mNms = v2;
/* .line 1765 */
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$8; */
/* invoke-direct {v4, v1}, Lcom/miui/server/greeze/GreezeManagerService$8;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
this.mUidObserver = v4;
/* .line 1798 */
/* new-instance v5, Lcom/miui/server/greeze/GreezeManagerService$9; */
/* invoke-direct {v5, v1}, Lcom/miui/server/greeze/GreezeManagerService$9;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
this.mProcessObserver = v5;
/* .line 1868 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
this.mFreeformSmallWinList = v6;
/* .line 1869 */
/* iput-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z */
/* .line 2011 */
final String v6 = ""; // const-string v6, ""
this.mFGOrderBroadcastAction = v6;
/* .line 2012 */
this.mBGOrderBroadcastAction = v6;
/* .line 2013 */
int v7 = -1; // const/4 v7, -0x1
/* iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I */
/* .line 2014 */
/* iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I */
/* .line 2057 */
/* new-instance v8, Landroid/util/LocalLog; */
/* const/16 v9, 0x1000 */
/* invoke-direct {v8, v9}, Landroid/util/LocalLog;-><init>(I)V */
this.mHistoryLog = v8;
/* .line 2071 */
/* iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
/* .line 2072 */
this.mTopAppPackageName = v6;
/* .line 2074 */
this.mPm = v2;
/* .line 2076 */
/* new-instance v6, Ljava/util/ArrayList; */
final String v8 = "com.tencent.mm"; // const-string v8, "com.tencent.mm"
final String v9 = "com.tencent.mobileqq"; // const-string v9, "com.tencent.mobileqq"
/* filled-new-array {v8, v9}, [Ljava/lang/String; */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mAurogonAlarmAllowList = v6;
/* .line 2077 */
/* new-instance v6, Ljava/util/ArrayList; */
final String v8 = "com.miui.player"; // const-string v8, "com.miui.player"
/* filled-new-array {v8}, [Ljava/lang/String; */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mAurogonAlarmForbidList = v6;
/* .line 2078 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
this.mRecentLaunchAppList = v6;
/* .line 2079 */
this.mAlarmManager = v2;
/* .line 2080 */
this.mWindowManager = v2;
/* .line 2081 */
/* iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
/* .line 2082 */
this.mDisplayManagerInternal = v2;
/* .line 2084 */
/* new-instance v6, Ljava/util/ArrayList; */
final String v8 = "android.intent.action.BATTERY_CHANGED"; // const-string v8, "android.intent.action.BATTERY_CHANGED"
final String v9 = "android.net.wifi.STATE_CHANGE"; // const-string v9, "android.net.wifi.STATE_CHANGE"
final String v10 = "android.intent.action.DROPBOX_ENTRY_ADDED"; // const-string v10, "android.intent.action.DROPBOX_ENTRY_ADDED"
final String v11 = "android.net.wifi.RSSI_CHANGED"; // const-string v11, "android.net.wifi.RSSI_CHANGED"
final String v12 = "android.net.wifi.supplicant.STATE_CHANGE"; // const-string v12, "android.net.wifi.supplicant.STATE_CHANGE"
final String v13 = "com.android.server.action.NETWORK_STATS_UPDATED"; // const-string v13, "com.android.server.action.NETWORK_STATS_UPDATED"
final String v14 = "android.intent.action.CLOSE_SYSTEM_DIALOGS"; // const-string v14, "android.intent.action.CLOSE_SYSTEM_DIALOGS"
final String v15 = "android.intent.action.TIME_TICK"; // const-string v15, "android.intent.action.TIME_TICK"
final String v16 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v16, "android.net.conn.CONNECTIVITY_CHANGE"
final String v17 = "android.net.wifi.WIFI_STATE_CHANGED"; // const-string v17, "android.net.wifi.WIFI_STATE_CHANGED"
final String v18 = "android.net.wifi.SCAN_RESULTS"; // const-string v18, "android.net.wifi.SCAN_RESULTS"
/* filled-new-array/range {v8 ..v18}, [Ljava/lang/String; */
java.util.Arrays .asList ( v8 );
/* invoke-direct {v6, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mBroadcastIntentDenyList = v6;
/* .line 2154 */
/* new-instance v6, Lcom/miui/server/greeze/GreezeManagerService$13; */
v8 = this.mHandler;
/* invoke-direct {v6, v1, v8}, Lcom/miui/server/greeze/GreezeManagerService$13;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Handler;)V */
this.mCloudAurogonAlarmListObserver = v6;
/* .line 2181 */
/* new-instance v6, Ljava/util/ArrayList; */
/* invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V */
this.mExcuteServiceList = v6;
/* .line 2244 */
/* iput-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->isBarExpand:Z */
/* .line 2245 */
/* iput v7, v1, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I */
/* .line 2314 */
/* iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z */
/* .line 2315 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mBroadcastCtrlMap = v0;
/* .line 2540 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCachedBCList = v0;
/* .line 177 */
/* move-object/from16 v3, p1 */
this.mContext = v3;
/* .line 178 */
com.miui.server.greeze.GreezeManagerService$GreezeThread .getInstance ( );
this.mThread = v0;
/* .line 179 */
/* new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$H; */
v6 = this.mThread;
(( com.android.server.ServiceThread ) v6 ).getLooper ( ); // invoke-virtual {v6}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1, v6}, Lcom/miui/server/greeze/GreezeManagerService$H;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 180 */
int v6 = 3; // const/4 v6, 0x3
(( android.os.Handler ) v0 ).sendEmptyMessage ( v6 ); // invoke-virtual {v0, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 181 */
android.app.ActivityManager .getService ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mActivityManagerService = v0;
/* .line 182 */
v6 = this.mContext;
final String v8 = "activity"; // const-string v8, "activity"
(( android.content.Context ) v6 ).getSystemService ( v8 ); // invoke-virtual {v6, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v6, Landroid/app/ActivityManager; */
this.mActivityManager = v6;
/* .line 183 */
/* sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 184 */
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->registerCloudObserver(Landroid/content/Context;)V */
/* .line 185 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
final String v8 = "cloud_greezer_enable"; // const-string v8, "cloud_greezer_enable"
int v9 = -2; // const/4 v9, -0x2
android.provider.Settings$System .getStringForUser ( v6,v8,v9 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 186 */
/* invoke-virtual/range {p1 ..p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver; */
android.provider.Settings$System .getStringForUser ( v6,v8,v9 );
v6 = java.lang.Boolean .parseBoolean ( v6 );
com.miui.server.greeze.GreezeManagerDebugConfig.sEnable = (v6!= 0);
/* .line 189 */
} // :cond_0
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->registerObserverForAurogon()V */
/* .line 190 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->getWindowManagerService()V */
/* .line 191 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->getAlarmManagerService()V */
/* .line 193 */
try { // :try_start_0
(( com.android.server.am.ActivityManagerService ) v0 ).registerProcessObserver ( v5 ); // invoke-virtual {v0, v5}, Lcom/android/server/am/ActivityManagerService;->registerProcessObserver(Landroid/app/IProcessObserver;)V
/* .line 194 */
/* const/16 v5, 0xe */
(( com.android.server.am.ActivityManagerService ) v0 ).registerUidObserver ( v4, v5, v7, v2 ); // invoke-virtual {v0, v4, v5, v7, v2}, Lcom/android/server/am/ActivityManagerService;->registerUidObserver(Landroid/app/IUidObserver;IILjava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 198 */
/* .line 196 */
/* :catch_0 */
/* move-exception v0 */
/* .line 199 */
} // :goto_0
/* const-class v0, Landroid/hardware/display/DisplayManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Landroid/hardware/display/DisplayManagerInternal; */
this.mDisplayManagerInternal = v0;
/* .line 200 */
/* new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver; */
/* invoke-direct {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService$AurogonBroadcastReceiver;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
/* .line 202 */
/* new-instance v0, Lcom/miui/server/greeze/AurogonImmobulusMode; */
v4 = this.mContext;
v5 = this.mThread;
/* invoke-direct {v0, v4, v5, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;-><init>(Landroid/content/Context;Landroid/os/HandlerThread;Lcom/miui/server/greeze/GreezeManagerService;)V */
this.mImmobulusMode = v0;
/* .line 204 */
v0 = this.mContext;
/* const-class v4, Landroid/net/ConnectivityManager; */
(( android.content.Context ) v0 ).getSystemService ( v4 ); // invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.cm = v0;
/* .line 205 */
/* const-class v0, Lcom/miui/server/greeze/GreezeManagerInternal; */
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$LocalService; */
/* invoke-direct {v4, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$LocalService;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$LocalService-IA;)V */
com.android.server.LocalServices .addService ( v0,v4 );
/* .line 206 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->initArgs()V */
/* .line 207 */
v0 = this.mContext;
final String v2 = "alarm"; // const-string v2, "alarm"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/AlarmManager; */
this.am = v0;
/* .line 208 */
android.app.ActivityTaskManager .getService ( );
this.mActivityTaskManager = v0;
/* .line 209 */
v0 = this.mContext;
final String v2 = "display"; // const-string v2, "display"
(( android.content.Context ) v0 ).getSystemService ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/display/DisplayManager; */
this.mDisplayManager = v0;
/* .line 210 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->registerDisplayChanageListener()V */
/* .line 211 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 212 */
final String v0 = "millet_binder"; // const-string v0, "millet_binder"
final String v2 = "ctl.start"; // const-string v2, "ctl.start"
android.os.SystemProperties .set ( v2,v0 );
/* .line 213 */
final String v0 = "millet_pkg"; // const-string v0, "millet_pkg"
android.os.SystemProperties .set ( v2,v0 );
/* .line 214 */
final String v0 = "millet_sig"; // const-string v0, "millet_sig"
android.os.SystemProperties .set ( v2,v0 );
/* .line 216 */
} // :cond_1
return;
} // .end method
private void addHistoryInfo ( com.miui.server.greeze.GreezeManagerService$FrozenInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 2819 */
v0 = this.mFrozenHistory;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I */
/* aput-object p1, v0, v1 */
/* .line 2820 */
int v0 = 1; // const/4 v0, 0x1
v0 = com.miui.server.greeze.GreezeManagerService .ringAdvance ( v1,v0,v2 );
/* iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I */
/* .line 2821 */
return;
} // .end method
private void checkAndFreezeIsolated ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "freeze" # Z */
/* .line 1366 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1367 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$5; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$5;-><init>(Lcom/miui/server/greeze/GreezeManagerService;IZ)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1401 */
return;
} // .end method
private void checkPermission ( ) {
/* .locals 4 */
/* .line 279 */
v0 = android.os.Binder .getCallingUid ( );
/* .line 280 */
/* .local v0, "uid":I */
v1 = android.os.UserHandle .isApp ( v0 );
/* if-nez v1, :cond_0 */
/* .line 281 */
return;
/* .line 283 */
} // :cond_0
/* new-instance v1, Ljava/lang/SecurityException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Uid "; // const-string v3, "Uid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " does not have permission to greezer"; // const-string v3, " does not have permission to greezer"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private Boolean checkStateForScrOff ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1104 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1105 */
/* .line 1108 */
} // :cond_0
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* const/16 v0, 0x4e1f */
/* if-gt p1, v0, :cond_1 */
/* .line 1109 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1110 */
} // :cond_1
} // .end method
private Boolean deferBroadcastForMiui ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 2448 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
v0 = v0 = com.miui.server.greeze.GreezeManagerService.mMiuiDeferBroadcast;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2449 */
/* .line 2450 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
/* if-nez v0, :cond_1 */
/* sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = v0 = com.miui.server.greeze.GreezeManagerService.mCNDeferBroadcast;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2451 */
/* .line 2452 */
} // :cond_1
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isRunningImmobulusMode ( ); // invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningImmobulusMode()Z
if ( v0 != null) { // if-eqz v0, :cond_2
v0 = v0 = com.miui.server.greeze.GreezeManagerService.mMiuiDeferBroadcast;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2453 */
/* .line 2454 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void dumpFreezeAction ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 2060 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
final String v1 = "Frozen processes:"; // const-string v1, "Frozen processes:"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2061 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2063 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x270f */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getFrozenPids ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenPids(I)[I
java.util.Arrays .toString ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2064 */
} // :goto_0
final String v0 = "Greezer History : "; // const-string v0, "Greezer History : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2065 */
v0 = this.mHistoryLog;
(( android.util.LocalLog ) v0 ).dump ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Landroid/util/LocalLog;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 2068 */
v0 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).dump ( p1, p2, p3 ); // invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
/* .line 2069 */
return;
} // .end method
private void getAlarmManagerService ( ) {
/* .locals 1 */
/* .line 2091 */
v0 = this.mAlarmManager;
/* if-nez v0, :cond_0 */
/* .line 2092 */
final String v0 = "alarm"; // const-string v0, "alarm"
android.os.ServiceManager .getService ( v0 );
android.app.IAlarmManager$Stub .asInterface ( v0 );
this.mAlarmManager = v0;
/* .line 2094 */
} // :cond_0
return;
} // .end method
private com.miui.server.greeze.GreezeManagerService$FrozenInfo getFrozenInfo ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 390 */
int v0 = 0; // const/4 v0, 0x0
/* .line 391 */
/* .local v0, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 392 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 393 */
v3 = this.mFrozenPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 394 */
/* .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* if-ne v4, p1, :cond_0 */
/* .line 395 */
/* move-object v0, v3 */
/* .line 396 */
/* .line 392 */
} // .end local v3 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 399 */
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
/* monitor-exit v1 */
/* .line 400 */
/* .line 399 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private java.util.List getFrozenNewPids ( ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1581 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1582 */
/* .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 1583 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_0 */
/* .line 1584 */
v3 = this.mFrozenPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1585 */
/* .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
java.lang.Integer .valueOf ( v4 );
/* .line 1583 */
/* nop */
} // .end local v3 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1587 */
} // .end local v2 # "i":I
} // :cond_0
/* monitor-exit v1 */
/* .line 1588 */
/* .line 1587 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private java.util.List getHistoryInfos ( Long p0 ) {
/* .locals 7 */
/* .param p1, "sinceUptime" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(J)", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 2824 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 2825 */
/* .local v0, "ret":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;" */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mHistoryIndexNext:I */
int v3 = -1; // const/4 v3, -0x1
v1 = com.miui.server.greeze.GreezeManagerService .ringAdvance ( v1,v3,v2 );
/* .line 2827 */
/* .local v1, "index":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
/* if-ge v2, v4, :cond_1 */
/* .line 2828 */
v5 = this.mFrozenHistory;
/* aget-object v5, v5, v1 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* iget-wide v5, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J */
/* cmp-long v5, v5, p1 */
/* if-gez v5, :cond_0 */
/* .line 2830 */
/* .line 2832 */
} // :cond_0
v5 = this.mFrozenHistory;
/* aget-object v5, v5, v1 */
/* .line 2833 */
v1 = com.miui.server.greeze.GreezeManagerService .ringAdvance ( v1,v3,v4 );
/* .line 2827 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2835 */
} // .end local v2 # "i":I
} // :cond_1
} // :goto_1
} // .end method
private Integer getInfoUid ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .line 405 */
int v0 = -1; // const/4 v0, -0x1
/* .line 406 */
/* .local v0, "infoUid":I */
v1 = this.isoPids;
/* monitor-enter v1 */
/* .line 407 */
try { // :try_start_0
v2 = this.isoPids;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* add-int/lit8 v2, v2, -0x1 */
/* .local v2, "i":I */
} // :goto_0
/* if-ltz v2, :cond_2 */
/* .line 408 */
v3 = this.isoPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/util/List; */
/* .line 409 */
/* .local v3, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* if-nez v3, :cond_0 */
/* .line 410 */
} // :cond_0
v4 = java.lang.Integer .valueOf ( p1 );
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 411 */
v4 = this.isoPids;
v4 = (( android.util.SparseArray ) v4 ).keyAt ( v2 ); // invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I
/* move v0, v4 */
/* .line 412 */
/* .line 407 */
} // .end local v3 # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_1
} // :goto_1
/* add-int/lit8 v2, v2, -0x1 */
/* .line 415 */
} // .end local v2 # "i":I
} // :cond_2
} // :goto_2
/* monitor-exit v1 */
/* .line 416 */
/* .line 415 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
private void getNavBarInfo ( android.content.Context p0 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 167 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "force_fsg_nav_bar"; // const-string v1, "force_fsg_nav_bar"
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$Global .getStringForUser ( v0,v1,v2 );
/* .line 169 */
/* .local v0, "val":Ljava/lang/String; */
final String v1 = "0"; // const-string v1, "0"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 170 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z */
/* .line 171 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mFsgNavBar :"; // const-string v2, "mFsgNavBar :"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
android.util.Slog .w ( v2,v1 );
/* .line 172 */
v1 = /* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getSystemUiPid()I */
/* iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I */
/* .line 174 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z */
/* .line 175 */
} // :goto_0
return;
} // .end method
private android.os.INetworkManagementService getNmsService ( ) {
/* .locals 1 */
/* .line 1728 */
v0 = this.mNms;
/* if-nez v0, :cond_0 */
/* .line 1729 */
/* nop */
/* .line 1730 */
final String v0 = "network_management"; // const-string v0, "network_management"
android.os.ServiceManager .getService ( v0 );
/* .line 1729 */
android.os.INetworkManagementService$Stub .asInterface ( v0 );
this.mNms = v0;
/* .line 1732 */
} // :cond_0
v0 = this.mNms;
} // .end method
public static com.miui.server.greeze.GreezeManagerService getService ( ) {
/* .locals 1 */
/* .line 252 */
v0 = com.miui.server.greeze.GreezeManagerService.IGreezeManagerSingleton;
(( android.util.Singleton ) v0 ).get ( ); // invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/greeze/GreezeManagerService; */
} // .end method
private Integer getSystemUiPid ( ) {
/* .locals 5 */
/* .line 2328 */
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 2329 */
/* .local v0, "procList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/RunningProcess; */
/* .line 2330 */
/* .local v2, "proc":Lcom/miui/server/greeze/RunningProcess; */
if ( v2 != null) { // if-eqz v2, :cond_0
final String v3 = "com.android.systemui"; // const-string v3, "com.android.systemui"
v4 = this.processName;
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 2331 */
/* iget v1, v2, Lcom/miui/server/greeze/RunningProcess;->pid:I */
/* .line 2332 */
} // .end local v2 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // :cond_0
/* .line 2333 */
} // :cond_1
int v1 = -1; // const/4 v1, -0x1
} // .end method
private void getWindowManagerService ( ) {
/* .locals 1 */
/* .line 2097 */
v0 = this.mWindowManager;
/* if-nez v0, :cond_0 */
/* .line 2098 */
android.view.WindowManagerGlobal .getWindowManagerService ( );
this.mWindowManager = v0;
/* .line 2100 */
} // :cond_0
return;
} // .end method
private void handleAppZygoteStart ( android.content.pm.ApplicationInfo p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "info" # Landroid/content/pm/ApplicationInfo; */
/* .param p2, "start" # Z */
/* .line 2520 */
/* if-nez p1, :cond_0 */
/* .line 2521 */
return;
/* .line 2523 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 2524 */
/* iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I */
int v1 = 1; // const/4 v1, 0x1
final String v2 = "AppZygote"; // const-string v2, "AppZygote"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 2526 */
} // :cond_1
return;
} // .end method
private void initArgs ( ) {
/* .locals 5 */
/* .line 219 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 220 */
/* .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v1 = "android.net.wifi.STATE_CHANGE"; // const-string v1, "android.net.wifi.STATE_CHANGE"
/* .line 221 */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
/* .line 222 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
v2 = v2 = com.miui.server.greeze.AurogonImmobulusMode.mMessageApp;
/* if-ge v1, v2, :cond_0 */
/* .line 223 */
v2 = com.miui.server.greeze.AurogonImmobulusMode.mMessageApp;
/* check-cast v2, Ljava/lang/String; */
/* .line 224 */
/* .local v2, "pkgName":Ljava/lang/String; */
v3 = this.mBroadcastTargetWhiteList;
/* .line 222 */
} // .end local v2 # "pkgName":Ljava/lang/String;
/* add-int/lit8 v1, v1, 0x1 */
/* .line 226 */
} // .end local v1 # "i":I
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 227 */
/* .local v1, "callerAction":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v2 = "android.appwidget.action.APPWIDGET_UPDATE"; // const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"
/* .line 228 */
final String v2 = "android.appwidget.action.APPWIDGET_ENABLED"; // const-string v2, "android.appwidget.action.APPWIDGET_ENABLED"
/* .line 229 */
v2 = this.mBroadcastCallerWhiteList;
final String v3 = "android"; // const-string v3, "android"
/* .line 231 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 232 */
/* .local v2, "immobulusModeActions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v3 = "*"; // const-string v3, "*"
/* .line 233 */
v3 = this.mImmobulusModeWhiteList;
final String v4 = "com.xiaomi.metoknlp"; // const-string v4, "com.xiaomi.metoknlp"
/* .line 234 */
v3 = this.mContext;
/* invoke-direct {p0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->getNavBarInfo(Landroid/content/Context;)V */
/* .line 235 */
return;
} // .end method
private static native void nAddConcernedUid ( Integer p0 ) {
} // .end method
private static native void nClearConcernedUid ( ) {
} // .end method
private static native void nDelConcernedUid ( Integer p0 ) {
} // .end method
private static native void nLoopOnce ( ) {
} // .end method
private static native void nQueryBinder ( Integer p0 ) {
} // .end method
private void registerCloudObserver ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 133 */
/* new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$1; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1, p1}, Lcom/miui/server/greeze/GreezeManagerService$1;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Handler;Landroid/content/Context;)V */
/* .line 151 */
/* .local v0, "observer":Landroid/database/ContentObserver; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 152 */
final String v2 = "cloud_greezer_enable"; // const-string v2, "cloud_greezer_enable"
android.provider.Settings$System .getUriFor ( v2 );
/* .line 151 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v2, v3, v0, v4 ); // invoke-virtual {v1, v2, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 154 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 155 */
/* const-string/jumbo v2, "zeropkgs" */
android.provider.Settings$Global .getUriFor ( v2 );
/* .line 154 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v5, v3, v0, v4 ); // invoke-virtual {v1, v5, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 157 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 158 */
final String v5 = "force_fsg_nav_bar"; // const-string v5, "force_fsg_nav_bar"
android.provider.Settings$Global .getUriFor ( v5 );
/* .line 157 */
(( android.content.ContentResolver ) v1 ).registerContentObserver ( v5, v3, v0, v4 ); // invoke-virtual {v1, v5, v3, v0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 160 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .getStringForUser ( v1,v2,v4 );
/* .line 162 */
/* .local v1, "data":Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
v2 = (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
int v3 = 3; // const/4 v3, 0x3
/* if-le v2, v3, :cond_0 */
/* .line 163 */
this.mAudioZeroPkgs = v1;
/* .line 164 */
} // :cond_0
return;
} // .end method
private void registerDisplayChanageListener ( ) {
/* .locals 3 */
/* .line 3379 */
v0 = this.mDisplayManager;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener-IA;)V */
v2 = this.mHandler;
(( android.hardware.display.DisplayManager ) v0 ).registerDisplayListener ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
/* .line 3380 */
return;
} // .end method
private void registerObserverForAurogon ( ) {
/* .locals 5 */
/* .line 2175 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 2176 */
final String v1 = "cloud_aurogon_alarm_allow_list"; // const-string v1, "cloud_aurogon_alarm_allow_list"
android.provider.Settings$System .getUriFor ( v1 );
v2 = this.mCloudAurogonAlarmListObserver;
/* .line 2175 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -2; // const/4 v4, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v3, v2, v4 ); // invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 2178 */
return;
} // .end method
private static Integer ringAdvance ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p0, "origin" # I */
/* .param p1, "increment" # I */
/* .param p2, "size" # I */
/* .line 2814 */
/* add-int v0, p0, p1 */
/* rem-int/2addr v0, p2 */
/* .line 2815 */
/* .local v0, "index":I */
/* if-gez v0, :cond_0 */
/* add-int v1, v0, p2 */
} // :cond_0
/* move v1, v0 */
} // :goto_0
} // .end method
private void sendMilletLoop ( ) {
/* .locals 4 */
/* .line 248 */
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
/* const-wide/16 v2, 0x1388 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 249 */
return;
} // .end method
private void setLoopAlarm ( ) {
/* .locals 9 */
/* .line 339 */
v0 = this.am;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
/* if-nez v0, :cond_0 */
/* .line 340 */
/* const-string/jumbo v0, "setLoopAlarm am == null" */
android.util.Slog .d ( v1,v0 );
/* .line 341 */
return;
/* .line 343 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "setLoopAlarm am cnt = " */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 344 */
v2 = this.am;
int v3 = 3; // const/4 v3, 0x3
/* .line 345 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* const-wide/32 v4, 0x493e0 */
/* add-long/2addr v4, v0 */
final String v6 = "monitorloop"; // const-string v6, "monitorloop"
v7 = this.mAlarmLoop;
int v8 = 0; // const/4 v8, 0x0
/* .line 344 */
/* invoke-virtual/range {v2 ..v8}, Landroid/app/AlarmManager;->setExact(IJLjava/lang/String;Landroid/app/AlarmManager$OnAlarmListener;Landroid/os/Handler;)V */
/* .line 347 */
return;
} // .end method
private void setWakeLockState ( java.util.List p0, Boolean p1 ) {
/* .locals 6 */
/* .param p2, "disable" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 755 */
/* .local p1, "ls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/Integer; */
/* .line 756 */
/* .local v1, "uid":Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
v2 = android.os.Process .isIsolated ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 760 */
} // :cond_0
try { // :try_start_0
com.android.server.power.PowerManagerServiceStub .get ( );
/* .line 761 */
v3 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
final String v4 = "greeze"; // const-string v4, "greeze"
/* .line 760 */
int v5 = 0; // const/4 v5, 0x0
(( com.android.server.power.PowerManagerServiceStub ) v2 ).setUidPartialWakeLockDisabledState ( v3, v5, p2, v4 ); // invoke-virtual {v2, v3, v5, p2, v4}, Lcom/android/server/power/PowerManagerServiceStub;->setUidPartialWakeLockDisabledState(ILjava/lang/String;ZLjava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 765 */
/* .line 763 */
/* :catch_0 */
/* move-exception v2 */
/* .line 764 */
/* .local v2, "e":Ljava/lang/Exception; */
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
/* const-string/jumbo v4, "updateWakelockBlockedUid" */
android.util.Log .e ( v3,v4,v2 );
/* .line 766 */
} // .end local v1 # "uid":Ljava/lang/Integer;
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_1
/* .line 767 */
} // :cond_1
return;
} // .end method
private static void startService ( ) {
/* .locals 2 */
/* .line 256 */
com.miui.server.greeze.GreezeManagerService .getService ( );
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "persist.sys.millet.handshake"; // const-string v0, "persist.sys.millet.handshake"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 257 */
com.miui.server.greeze.GreezeManagerService .getService ( );
/* invoke-direct {v0}, Lcom/miui/server/greeze/GreezeManagerService;->sendMilletLoop()V */
/* .line 258 */
final String v0 = "persist.sys.millet.cgroup"; // const-string v0, "persist.sys.millet.cgroup"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.greeze.GreezeManagerDebugConfig.mCgroupV1Flag = (v0!= 0);
/* .line 260 */
} // :cond_0
return;
} // .end method
public static java.lang.String stateToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "state" # I */
/* .line 742 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 748 */
java.lang.Integer .toString ( p0 );
/* .line 747 */
/* :pswitch_0 */
final String v0 = "BINDER_IN_TRANSACTION"; // const-string v0, "BINDER_IN_TRANSACTION"
/* .line 746 */
/* :pswitch_1 */
final String v0 = "BINDER_PROC_IN_BUSY"; // const-string v0, "BINDER_PROC_IN_BUSY"
/* .line 745 */
/* :pswitch_2 */
final String v0 = "BINDER_THREAD_IN_BUSY"; // const-string v0, "BINDER_THREAD_IN_BUSY"
/* .line 744 */
/* :pswitch_3 */
final String v0 = "BINDER_IN_BUSY"; // const-string v0, "BINDER_IN_BUSY"
/* .line 743 */
/* :pswitch_4 */
final String v0 = "BINDER_IN_IDLE"; // const-string v0, "BINDER_IN_IDLE"
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
static toArray ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;)[I" */
/* } */
} // .end annotation
/* .line 2599 */
/* .local p0, "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* if-nez p0, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [I */
/* .line 2600 */
v0 = } // :cond_0
/* new-array v0, v0, [I */
/* .line 2601 */
/* .local v0, "arr":[I */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* array-length v2, v0 */
/* if-ge v1, v2, :cond_1 */
/* .line 2602 */
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* aput v2, v0, v1 */
/* .line 2601 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 2604 */
} // .end local v1 # "i":I
} // :cond_1
} // .end method
/* # virtual methods */
public void CachedBroadcasForAurogon ( android.content.Intent p0, Integer p1 ) {
/* .locals 6 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "uid" # I */
/* .line 2543 */
v0 = this.mCachedBCList;
/* monitor-enter v0 */
/* .line 2544 */
try { // :try_start_0
v1 = this.mCachedBCList;
java.lang.Integer .valueOf ( p2 );
/* check-cast v1, Ljava/util/List; */
/* .line 2545 */
/* .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;" */
/* if-nez v1, :cond_0 */
/* .line 2546 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 2547 */
v2 = this.mCachedBCList;
java.lang.Integer .valueOf ( p2 );
/* .line 2549 */
} // :cond_0
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Landroid/content/Intent; */
/* .line 2550 */
/* .local v3, "old":Landroid/content/Intent; */
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 2551 */
/* monitor-exit v0 */
return;
/* .line 2553 */
} // .end local v3 # "old":Landroid/content/Intent;
} // :cond_1
/* .line 2554 */
} // :cond_2
/* .line 2555 */
/* nop */
} // .end local v1 # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
/* monitor-exit v0 */
/* .line 2556 */
return;
/* .line 2555 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void addToDumpHistory ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "log" # Ljava/lang/String; */
/* .line 1355 */
v0 = this.mHistoryLog;
(( android.util.LocalLog ) v0 ).log ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1356 */
return;
} // .end method
public Boolean checkAurogonIntentDenyList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 2591 */
v0 = v0 = this.mBroadcastIntentDenyList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2592 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2594 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean checkFreeformSmallWin ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1937 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getPackageNameFromUid ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 1938 */
/* .local v0, "packageName":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 1939 */
int v1 = 0; // const/4 v1, 0x0
/* .line 1941 */
} // :cond_0
v1 = this.mFreeformSmallWinList;
/* monitor-enter v1 */
/* .line 1942 */
try { // :try_start_0
v2 = v2 = this.mFreeformSmallWinList;
/* monitor-exit v1 */
/* .line 1943 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public Boolean checkFreeformSmallWin ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1931 */
v0 = this.mFreeformSmallWinList;
/* monitor-enter v0 */
/* .line 1932 */
try { // :try_start_0
v1 = v1 = this.mFreeformSmallWinList;
/* monitor-exit v0 */
/* .line 1933 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
Boolean checkImmobulusModeRestrict ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "targetPkgName" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 2529 */
v0 = this.mImmobulusModeWhiteList;
/* monitor-enter v0 */
/* .line 2530 */
try { // :try_start_0
v1 = v1 = this.mImmobulusModeWhiteList;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2531 */
v1 = this.mImmobulusModeWhiteList;
/* check-cast v1, Ljava/util/List; */
/* .line 2532 */
v2 = /* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v2, :cond_0 */
v2 = final String v2 = "*"; // const-string v2, "*"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 2533 */
} // :cond_0
/* monitor-exit v0 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2536 */
} // .end local v1 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2537 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean checkOrderBCRecivingApp ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 2043 */
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I */
int v1 = 1; // const/4 v1, 0x1
int v2 = -1; // const/4 v2, -0x1
final String v3 = ""; // const-string v3, ""
/* if-ne p1, v0, :cond_0 */
/* .line 2044 */
this.mFGOrderBroadcastAction = v3;
/* .line 2045 */
/* iput v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I */
/* .line 2046 */
/* .line 2047 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I */
/* if-ne p1, v0, :cond_1 */
/* .line 2048 */
this.mBGOrderBroadcastAction = v3;
/* .line 2049 */
/* iput v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I */
/* .line 2050 */
/* .line 2052 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean checkRecentLuanchedApp ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1954 */
v0 = this.mRecentLaunchAppList;
/* monitor-enter v0 */
/* .line 1955 */
try { // :try_start_0
v1 = this.mRecentLaunchAppList;
v1 = java.lang.Integer .valueOf ( p1 );
/* monitor-exit v0 */
/* .line 1956 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void clearMonitorNet ( ) {
/* .locals 0 */
/* .line 780 */
com.miui.server.greeze.GreezeManagerService .nClearConcernedUid ( );
/* .line 781 */
return;
} // .end method
public void clearMonitorNet ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 776 */
com.miui.server.greeze.GreezeManagerService .nDelConcernedUid ( p1 );
/* .line 777 */
return;
} // .end method
public void closeSocketForAurogon ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1718 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1719 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "closeSocketForAurogon uid = "; // const-string v1, "closeSocketForAurogon uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AurogonImmobulusMode"; // const-string v1, "AurogonImmobulusMode"
android.util.Slog .d ( v1,v0 );
/* .line 1720 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* new-array v0, v0, [I */
/* .line 1721 */
/* .local v0, "uids":[I */
int v1 = 0; // const/4 v1, 0x0
/* aput p1, v0, v1 */
/* .line 1722 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).closeSocketForAurogon ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->closeSocketForAurogon([I)V
/* .line 1723 */
return;
} // .end method
public void closeSocketForAurogon ( Integer[] p0 ) {
/* .locals 3 */
/* .param p1, "uids" # [I */
/* .line 1708 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getNmsService()Landroid/os/INetworkManagementService; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1710 */
v0 = this.mNms;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Landroid/os/ServiceSpecificException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1714 */
} // :cond_0
/* .line 1712 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1713 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
final String v2 = "failed to close socket for aurogon!"; // const-string v2, "failed to close socket for aurogon!"
android.util.Slog .d ( v1,v2 );
/* .line 1715 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void dealAdjSet ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "cmd" # I */
/* .line 1083 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$4; */
/* invoke-direct {v1, p0, p2, p1}, Lcom/miui/server/greeze/GreezeManagerService$4;-><init>(Lcom/miui/server/greeze/GreezeManagerService;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1102 */
return;
} // .end method
protected void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 4 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 2904 */
v0 = this.mContext;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
return;
/* .line 2905 */
} // :cond_0
final String v0 = ""; // const-string v0, ""
(( com.miui.server.greeze.GreezeManagerService ) p0 ).dumpSettings ( v0, p1, p2 ); // invoke-virtual {p0, v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->dumpSettings(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
/* .line 2907 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->dumpFreezeAction(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V */
/* .line 2908 */
/* array-length v1, p3 */
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, p3, v1 */
final String v2 = "old"; // const-string v2, "old"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2909 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).dumpHistory ( v0, p1, p2 ); // invoke-virtual {p0, v0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->dumpHistory(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
/* .line 2910 */
} // :cond_1
v0 = this.mFreeformSmallWinList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/lang/String; */
/* .line 2911 */
/* .local v1, "temp":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " FreeformSmallWin uid = "; // const-string v3, " FreeformSmallWin uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2912 */
} // .end local v1 # "temp":Ljava/lang/String;
/* .line 2913 */
} // :cond_2
return;
} // .end method
void dumpFrozen ( java.lang.String p0, java.io.FileDescriptor p1, java.io.PrintWriter p2 ) {
/* .locals 10 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "fd" # Ljava/io/FileDescriptor; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 2876 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2877 */
com.miui.server.greeze.FreezeUtils .getFrozonTids ( );
/* .line 2878 */
/* .local v0, "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "Frozen tids: "; // const-string v2, "Frozen tids: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2879 */
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
/* .line 2880 */
/* .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .line 2881 */
} // .end local v0 # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenNewPids()Ljava/util/List; */
/* .line 2882 */
/* .restart local v0 # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "Frozen pids: "; // const-string v2, "Frozen pids: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2884 */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
final String v2 = "HH:mm:ss.SSS"; // const-string v2, "HH:mm:ss.SSS"
/* invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 2885 */
/* .local v1, "formater":Ljava/text/SimpleDateFormat; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "Frozen processes:"; // const-string v3, "Frozen processes:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v2 ); // invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2886 */
v2 = this.mFrozenPids;
/* monitor-enter v2 */
/* .line 2887 */
try { // :try_start_0
v3 = this.mFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* .line 2888 */
/* .local v3, "n":I */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_1
/* if-ge v4, v3, :cond_2 */
/* .line 2889 */
v5 = this.mFrozenPids;
(( android.util.SparseArray ) v5 ).valueAt ( v4 ); // invoke-virtual {v5, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 2890 */
/* .local v5, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( p1 ); // invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v6 ); // invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "#"; // const-string v7, "#"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v7, v4, 0x1 */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v6 ); // invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2891 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " pid="; // const-string v7, " pid="
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v5, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v6 ); // invoke-virtual {p3, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2892 */
int v6 = 0; // const/4 v6, 0x0
/* .local v6, "index":I */
} // :goto_2
v7 = v7 = this.mFreezeTimes;
/* if-ge v6, v7, :cond_1 */
/* .line 2893 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = " "; // const-string v8, " "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v7 = "fz: "; // const-string v7, "fz: "
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2894 */
/* new-instance v7, Ljava/util/Date; */
v8 = this.mFreezeTimes;
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v1 ).format ( v7 ); // invoke-virtual {v1, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2895 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = " "; // const-string v8, " "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mFreezeReasons;
/* check-cast v8, Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2896 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = " from "; // const-string v8, " from "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mFromWho;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2892 */
/* add-int/lit8 v6, v6, 0x1 */
/* .line 2888 */
} // .end local v5 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v6 # "index":I
} // :cond_1
/* add-int/lit8 v4, v4, 0x1 */
/* goto/16 :goto_1 */
/* .line 2899 */
} // .end local v3 # "n":I
} // .end local v4 # "i":I
} // :cond_2
/* monitor-exit v2 */
/* .line 2900 */
return;
/* .line 2899 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
} // .end method
void dumpHistory ( java.lang.String p0, java.io.FileDescriptor p1, java.io.PrintWriter p2 ) {
/* .locals 10 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "fd" # Ljava/io/FileDescriptor; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 2839 */
final String v0 = "Frozen processes in history:"; // const-string v0, "Frozen processes in history:"
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2840 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* const-wide/32 v2, 0xdbba00 */
/* sub-long/2addr v0, v2 */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getHistoryInfos(J)Ljava/util/List; */
/* .line 2841 */
/* .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;" */
int v1 = 1; // const/4 v1, 0x1
/* .line 2842 */
/* .local v1, "index":I */
/* new-instance v2, Ljava/text/SimpleDateFormat; */
final String v3 = "HH:mm:ss.SSS"; // const-string v3, "HH:mm:ss.SSS"
/* invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 2843 */
/* .local v2, "formater":Ljava/text/SimpleDateFormat; */
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 2844 */
/* .local v4, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v5 ).append ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v5 ); // invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "#"; // const-string v6, "#"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* add-int/lit8 v6, v1, 0x1 */
} // .end local v1 # "index":I
/* .local v6, "index":I */
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2845 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v7, Ljava/util/Date; */
/* iget-wide v8, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J */
/* invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v7 ); // invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2846 */
/* iget v1, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2847 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2848 */
v1 = this.processName;
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_1 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = this.processName;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2849 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v4 ).getFrozenDuration ( ); // invoke-virtual {v4}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getFrozenDuration()J
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v1 ).append ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v7 = "ms"; // const-string v7, "ms"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2850 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_1
v7 = v7 = this.mFreezeTimes;
final String v8 = " "; // const-string v8, " "
/* if-ge v1, v7, :cond_2 */
/* .line 2851 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
final String v7 = "fz: "; // const-string v7, "fz: "
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2852 */
/* new-instance v7, Ljava/util/Date; */
v8 = this.mFreezeTimes;
/* check-cast v8, Ljava/lang/Long; */
(( java.lang.Long ) v8 ).longValue ( ); // invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
/* move-result-wide v8 */
/* invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v7 ); // invoke-virtual {v2, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2853 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mFreezeReasons;
/* check-cast v8, Ljava/lang/String; */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2854 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = " from "; // const-string v8, " from "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v8 = this.mFromWho;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v7 ); // invoke-virtual {p3, v7}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2850 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 2856 */
} // .end local v1 # "i":I
} // :cond_2
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* const-string/jumbo v1, "th: " */
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2857 */
/* new-instance v1, Ljava/util/Date; */
/* iget-wide v7, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J */
/* invoke-direct {v1, v7, v8}, Ljava/util/Date;-><init>(J)V */
(( java.text.SimpleDateFormat ) v2 ).format ( v1 ); // invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).print ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 2858 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mThawReason;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2859 */
} // .end local v4 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* move v1, v6 */
/* goto/16 :goto_0 */
/* .line 2860 */
} // .end local v6 # "index":I
/* .local v1, "index":I */
} // :cond_3
return;
} // .end method
void dumpSettings ( java.lang.String p0, java.io.FileDescriptor p1, java.io.PrintWriter p2 ) {
/* .locals 3 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "fd" # Ljava/io/FileDescriptor; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 2863 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "Settings:"; // const-string v1, "Settings:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2864 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " enable="; // const-string v1, " enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " ("; // const-string v1, " ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "persist.sys.powmillet.enable"; // const-string v1, "persist.sys.powmillet.enable"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2865 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " debug="; // const-string v1, " debug="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " ("; // const-string v1, " ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "persist.sys.gz.debug"; // const-string v1, "persist.sys.gz.debug"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2866 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " monkey="; // const-string v1, " monkey="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MONKEY:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " ("; // const-string v1, " ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "persist.sys.gz.monkey"; // const-string v1, "persist.sys.gz.monkey"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2867 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " fz_timeout="; // const-string v1, " fz_timeout="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " ("; // const-string v1, " ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "persist.sys.gz.fztimeout"; // const-string v1, "persist.sys.gz.fztimeout"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2868 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " monitor="; // const-string v1, " monitor="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " ("; // const-string v1, " ("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v0 ); // invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2869 */
v0 = this.mBroadcastTargetWhiteList;
/* monitor-enter v0 */
/* .line 2870 */
try { // :try_start_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " mBroadcastTargetWhiteList="; // const-string v2, " mBroadcastTargetWhiteList="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mBroadcastTargetWhiteList;
(( java.lang.Object ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p3 ).println ( v1 ); // invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2871 */
/* monitor-exit v0 */
/* .line 2872 */
return;
/* .line 2871 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void finishLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 1352 */
return;
} // .end method
public void forceStopPackage ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "userId" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 1763 */
return;
} // .end method
public Boolean freezeAction ( Integer p0, Integer p1, java.lang.String p2, Boolean p3 ) {
/* .locals 17 */
/* .param p1, "uid" # I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .param p4, "isNeedCompact" # Z */
/* .line 1117 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move/from16 v3, p2 */
/* move-object/from16 v4, p3 */
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isModeReason ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z
int v5 = 0; // const/4 v5, 0x0
/* if-nez v0, :cond_0 */
/* .line 1118 */
v0 = /* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppShowOnWindows(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1119 */
/* .line 1131 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v6, v0 */
/* .line 1132 */
/* .local v6, "log":Ljava/lang/StringBuilder; */
int v7 = 0; // const/4 v7, 0x0
/* .line 1133 */
/* .local v7, "done":Z */
v8 = this.mAurogonLock;
/* monitor-enter v8 */
/* .line 1135 */
try { // :try_start_0
v0 = /* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1136 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1137 */
final String v0 = "Greeze"; // const-string v0, "Greeze"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v10, "uid = " */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = "has be freeze"; // const-string v10, "has be freeze"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v9 );
/* .line 1138 */
} // :cond_1
/* monitor-exit v8 */
/* .line 1141 */
} // :cond_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "FZ uid = "; // const-string v9, "FZ uid = "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1142 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->readPidFromCgroup(I)Ljava/util/List; */
/* move-object v9, v0 */
/* .line 1143 */
v0 = /* .local v9, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* if-nez v0, :cond_3 */
/* monitor-exit v8 */
/* .line 1144 */
} // :cond_3
com.miui.server.greeze.GreezeServiceUtils .getGameUids ( );
/* move-object v10, v0 */
/* .line 1145 */
/* .local v10, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v0 = /* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1146 */
v0 = this.mHandler;
int v11 = 7; // const/4 v11, 0x7
(( android.os.Handler ) v0 ).obtainMessage ( v11, v5, v2 ); // invoke-virtual {v0, v11, v5, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v5 ); // invoke-virtual {v0, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1147 */
} // :cond_4
final String v0 = " pid = [ "; // const-string v0, " pid = [ "
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1148 */
int v5 = 1; // const/4 v5, 0x1
/* .line 1149 */
/* .local v5, "isCgroupPidError":Z */
v11 = this.mFrozenPids;
/* monitor-enter v11 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_3 */
/* .line 1150 */
try { // :try_start_1
v12 = } // :goto_0
if ( v12 != null) { // if-eqz v12, :cond_b
/* check-cast v12, Ljava/lang/Integer; */
v12 = (( java.lang.Integer ) v12 ).intValue ( ); // invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I
/* .line 1152 */
/* .local v12, "pid":I */
v14 = (( com.miui.server.greeze.GreezeManagerService ) v1 ).readPidStatus ( v2, v12 ); // invoke-virtual {v1, v2, v12}, Lcom/miui/server/greeze/GreezeManagerService;->readPidStatus(II)Z
/* if-nez v14, :cond_5 */
/* .line 1154 */
/* .line 1156 */
} // :cond_5
int v5 = 0; // const/4 v5, 0x0
/* .line 1157 */
v14 = com.miui.server.greeze.FreezeUtils .freezePid ( v12,v2 );
/* move v7, v14 */
/* .line 1158 */
if ( v7 != null) { // if-eqz v7, :cond_a
/* .line 1159 */
v14 = this.mFrozenPids;
(( android.util.SparseArray ) v14 ).get ( v12 ); // invoke-virtual {v14, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 1160 */
/* .local v14, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* if-nez v14, :cond_6 */
/* .line 1161 */
try { // :try_start_2
/* new-instance v15, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* invoke-direct {v15, v2, v12}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;-><init>(II)V */
/* move-object v14, v15 */
/* .line 1162 */
v15 = this.mFrozenPids;
(( android.util.SparseArray ) v15 ).put ( v12, v14 ); // invoke-virtual {v15, v12, v14}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* move-object/from16 v16, v0 */
/* .line 1189 */
} // .end local v12 # "pid":I
} // .end local v14 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* :catchall_0 */
/* move-exception v0 */
/* move-object v13, v9 */
/* move-object v15, v10 */
/* goto/16 :goto_5 */
/* .line 1163 */
/* .restart local v12 # "pid":I */
/* .restart local v14 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
} // :cond_6
try { // :try_start_3
/* iget v15, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* if-eq v2, v15, :cond_7 */
/* .line 1164 */
try { // :try_start_4
final String v15 = "GreezeManager"; // const-string v15, "GreezeManager"
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v16, v0 */
/* const-string/jumbo v0, "uid-pid mismatch old uid = " */
(( java.lang.StringBuilder ) v13 ).append ( v0 ); // invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v13, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v13 = " pid = "; // const-string v13, " pid = "
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v13 = " new uid = "; // const-string v13, " new uid = "
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v15,v0 );
/* .line 1165 */
/* iput v2, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 1163 */
} // :cond_7
/* move-object/from16 v16, v0 */
/* .line 1167 */
} // :goto_1
/* move-object v13, v9 */
/* move-object v15, v10 */
} // .end local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v13, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local v15, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
try { // :try_start_5
java.lang.System .currentTimeMillis ( );
/* move-result-wide v9 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v14 ).addFreezeInfo ( v9, v10, v3, v4 ); // invoke-virtual {v14, v9, v10, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V
/* .line 1168 */
v0 = this.mImmobulusMode;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 1169 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z */
/* .line 1170 */
} // :cond_8
int v0 = 1; // const/4 v0, 0x1
/* and-int/lit8 v9, v3, 0x10 */
if ( v9 != null) { // if-eqz v9, :cond_9
/* .line 1171 */
/* iput-boolean v0, v14, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
/* .line 1173 */
} // :cond_9
} // :goto_2
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v9 = " "; // const-string v9, " "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1184 */
/* nop */
} // .end local v14 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* .line 1186 */
} // .end local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // :cond_a
/* move-object/from16 v16, v0 */
/* move-object v13, v9 */
/* move-object v15, v10 */
} // .end local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
final String v0 = "AurogonImmobulusMode"; // const-string v0, "AurogonImmobulusMode"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " Freeze uid = "; // const-string v10, " Freeze uid = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " pid = "; // const-string v10, " pid = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v12 ); // invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " error !"; // const-string v10, " error !"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v9 );
/* .line 1188 */
} // .end local v12 # "pid":I
} // :goto_3
/* move-object v9, v13 */
/* move-object v10, v15 */
/* move-object/from16 v0, v16 */
/* goto/16 :goto_0 */
/* .line 1189 */
} // .end local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // :cond_b
/* move-object v13, v9 */
/* move-object v15, v10 */
} // .end local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* monitor-exit v11 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* .line 1190 */
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 1192 */
try { // :try_start_6
final String v0 = "AurogonImmobulusMode"; // const-string v0, "AurogonImmobulusMode"
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = " Freeze uid = "; // const-string v10, " Freeze uid = "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " error due pid-uid mismatch"; // const-string v10, " error due pid-uid mismatch"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v9 );
/* .line 1193 */
/* monitor-exit v8 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1196 */
} // :cond_c
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = "] reason : "; // const-string v9, "] reason : "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " caller : "; // const-string v9, " caller : "
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1197 */
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isModeReason ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_d
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->PID_DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 1198 */
} // :cond_d
v0 = this.mHistoryLog;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v0 ).log ( v9 ); // invoke-virtual {v0, v9}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1200 */
} // .end local v5 # "isCgroupPidError":Z
} // .end local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_e
/* monitor-exit v8 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_3 */
/* .line 1203 */
v0 = this.mImmobulusMode;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1204 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->closeSocketForAurogon(I)V */
/* .line 1205 */
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.greeze.GreezeManagerService ) v1 ).updateAurogonUidRule ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V
/* .line 1206 */
} // :cond_f
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isNeedRestictNetworkPolicy ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isNeedRestictNetworkPolicy(I)Z
if ( v0 != null) { // if-eqz v0, :cond_10
/* .line 1208 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V */
/* .line 1211 */
} // :cond_10
} // :goto_4
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V */
/* .line 1212 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {v1, v2, v0}, Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V */
/* .line 1213 */
/* .line 1189 */
/* .restart local v5 # "isCgroupPidError":Z */
/* .restart local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* :catchall_1 */
/* move-exception v0 */
/* move-object v13, v9 */
/* move-object v15, v10 */
} // .end local v9 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v10 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // :goto_5
try { // :try_start_7
/* monitor-exit v11 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
} // .end local v6 # "log":Ljava/lang/StringBuilder;
} // .end local v7 # "done":Z
} // .end local p0 # "this":Lcom/miui/server/greeze/GreezeManagerService;
} // .end local p1 # "uid":I
} // .end local p2 # "fromWho":I
} // .end local p3 # "reason":Ljava/lang/String;
} // .end local p4 # "isNeedCompact":Z
try { // :try_start_8
/* throw v0 */
/* .restart local v6 # "log":Ljava/lang/StringBuilder; */
/* .restart local v7 # "done":Z */
/* .restart local p0 # "this":Lcom/miui/server/greeze/GreezeManagerService; */
/* .restart local p1 # "uid":I */
/* .restart local p2 # "fromWho":I */
/* .restart local p3 # "reason":Ljava/lang/String; */
/* .restart local p4 # "isNeedCompact":Z */
/* :catchall_2 */
/* move-exception v0 */
/* .line 1200 */
} // .end local v5 # "isCgroupPidError":Z
} // .end local v13 # "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v15 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* :catchall_3 */
/* move-exception v0 */
/* monitor-exit v8 */
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_3 */
/* throw v0 */
} // .end method
public java.util.List freezePids ( Integer[] p0, Long p1, Integer p2, java.lang.String p3 ) {
/* .locals 18 */
/* .param p1, "pids" # [I */
/* .param p2, "timeout" # J */
/* .param p4, "fromWho" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([IJI", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 893 */
/* move-object/from16 v0, p1 */
/* move-wide/from16 v7, p2 */
/* move/from16 v9, p4 */
/* move-object/from16 v10, p5 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 894 */
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
final String v11 = "GreezeManager"; // const-string v11, "GreezeManager"
final String v12 = ", "; // const-string v12, ", "
if ( v1 != null) { // if-eqz v1, :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "AIDL freezePids("; // const-string v2, "AIDL freezePids("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-static/range {p1 ..p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ")"; // const-string v2, ")"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v1 );
/* .line 896 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 897 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 899 */
} // :cond_1
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 900 */
/* .local v13, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* move-object v14, v1 */
/* .line 901 */
/* .local v14, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* array-length v15, v0 */
int v1 = 0; // const/4 v1, 0x0
/* move v6, v1 */
} // :goto_0
/* if-ge v6, v15, :cond_7 */
/* aget v5, v0, v6 */
/* .line 902 */
/* .local v5, "pid":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 903 */
/* .local v1, "target":Lcom/miui/server/greeze/RunningProcess; */
/* move-object v3, v1 */
} // .end local v1 # "target":Lcom/miui/server/greeze/RunningProcess;
/* .local v3, "target":Lcom/miui/server/greeze/RunningProcess; */
v1 = } // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Lcom/miui/server/greeze/RunningProcess; */
/* .line 904 */
/* .local v1, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* iget v4, v1, Lcom/miui/server/greeze/RunningProcess;->pid:I */
/* if-ne v5, v4, :cond_2 */
/* .line 905 */
/* move-object v3, v1 */
/* .line 907 */
} // .end local v1 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // :cond_2
/* .line 908 */
} // :cond_3
/* if-nez v3, :cond_4 */
/* .line 909 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to freeze invalid pid "; // const-string v2, "Failed to freeze invalid pid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v11,v1 );
/* .line 910 */
/* move/from16 v17, v6 */
/* move-object/from16 v16, v13 */
/* .line 912 */
} // :cond_4
/* move-object/from16 v1, p0 */
/* move-object v2, v3 */
/* move-object v0, v3 */
} // .end local v3 # "target":Lcom/miui/server/greeze/RunningProcess;
/* .local v0, "target":Lcom/miui/server/greeze/RunningProcess; */
/* move-wide/from16 v3, p2 */
/* move-object/from16 v16, v13 */
/* move v13, v5 */
} // .end local v5 # "pid":I
/* .local v13, "pid":I */
/* .local v16, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* move/from16 v5, p4 */
/* move/from16 v17, v6 */
/* move-object/from16 v6, p5 */
v1 = /* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/greeze/GreezeManagerService;->freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z */
/* if-nez v1, :cond_5 */
/* .line 914 */
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "AIDL freezePid("; // const-string v2, "AIDL freezePid("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v13 ); // invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7, v8 ); // invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v12 ); // invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ") failed!"; // const-string v2, ") failed!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v1 );
/* .line 917 */
} // :cond_5
/* iget v1, v0, Lcom/miui/server/greeze/RunningProcess;->pid:I */
java.lang.Integer .valueOf ( v1 );
/* .line 901 */
} // .end local v0 # "target":Lcom/miui/server/greeze/RunningProcess;
} // .end local v13 # "pid":I
} // :cond_6
} // :goto_2
/* add-int/lit8 v6, v17, 0x1 */
/* move-object/from16 v0, p1 */
/* move-object/from16 v13, v16 */
/* goto/16 :goto_0 */
/* .line 921 */
} // .end local v16 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
/* .local v13, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
} // :cond_7
/* move-object/from16 v16, v13 */
} // .end local v13 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
/* .restart local v16 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AIDL freezePids result: frozen "; // const-string v1, "AIDL freezePids result: frozen "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v0 );
/* .line 922 */
} // :cond_8
} // .end method
public Boolean freezeProcess ( com.miui.server.greeze.RunningProcess p0, Long p1, Integer p2, java.lang.String p3 ) {
/* .locals 8 */
/* .param p1, "proc" # Lcom/miui/server/greeze/RunningProcess; */
/* .param p2, "timeout" # J */
/* .param p4, "fromWho" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .line 859 */
/* iget v0, p1, Lcom/miui/server/greeze/RunningProcess;->pid:I */
/* .line 860 */
/* .local v0, "pid":I */
/* if-lez v0, :cond_5 */
v1 = android.os.Process .myPid ( );
/* if-eq v1, v0, :cond_5 */
v1 = android.os.Process .getUidForPid ( v0 );
/* iget v2, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I */
/* if-eq v1, v2, :cond_0 */
/* goto/16 :goto_1 */
/* .line 863 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 864 */
/* .local v1, "done":Z */
/* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 865 */
v1 = com.miui.server.greeze.FreezeUtils .freezePid ( v0 );
/* .line 867 */
} // :cond_1
/* iget v2, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I */
v1 = com.miui.server.greeze.FreezeUtils .freezePid ( v0,v2 );
/* .line 868 */
} // :goto_0
/* const-class v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v2 );
/* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* iget v3, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I */
/* .line 869 */
v2 = this.mFrozenPids;
/* monitor-enter v2 */
/* .line 870 */
try { // :try_start_0
v3 = this.mFrozenPids;
(( android.util.SparseArray ) v3 ).get ( v0 ); // invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 871 */
/* .local v3, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
if ( v3 != null) { // if-eqz v3, :cond_2
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* iget v5, p1, Lcom/miui/server/greeze/RunningProcess;->uid:I */
/* if-eq v4, v5, :cond_2 */
/* .line 872 */
final String v4 = "GreezeManager"; // const-string v4, "GreezeManager"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "freeze uid-pid mismatch "; // const-string v6, "freeze uid-pid mismatch "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 873 */
v4 = this.mFrozenPids;
(( android.util.SparseArray ) v4 ).remove ( v0 ); // invoke-virtual {v4, v0}, Landroid/util/SparseArray;->remove(I)V
/* .line 875 */
} // :cond_2
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* invoke-direct {v4, p1}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;-><init>(Lcom/miui/server/greeze/RunningProcess;)V */
/* move-object v3, v4 */
/* .line 876 */
v4 = this.mFrozenPids;
(( android.util.SparseArray ) v4 ).put ( v0, v3 ); // invoke-virtual {v4, v0, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 877 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v3 ).addFreezeInfo ( v4, v5, p4, p5 ); // invoke-virtual {v3, v4, v5, p4, p5}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V
/* .line 878 */
v4 = this.mHandler;
int v5 = 1; // const/4 v5, 0x1
v4 = (( android.os.Handler ) v4 ).hasMessages ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 879 */
v4 = this.mHandler;
(( android.os.Handler ) v4 ).removeMessages ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 881 */
} // :cond_3
/* const-wide/16 v6, 0x0 */
/* cmp-long v4, p2, v6 */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 882 */
v4 = this.mHandler;
(( android.os.Handler ) v4 ).obtainMessage ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 883 */
/* .local v4, "msg":Landroid/os/Message; */
/* iput v0, v4, Landroid/os/Message;->arg1:I */
/* .line 884 */
/* iput p4, v4, Landroid/os/Message;->arg2:I */
/* .line 885 */
v5 = this.mHandler;
(( android.os.Handler ) v5 ).sendMessageDelayed ( v4, p2, p3 ); // invoke-virtual {v5, v4, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 887 */
} // .end local v3 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v4 # "msg":Landroid/os/Message;
} // :cond_4
/* monitor-exit v2 */
/* .line 888 */
/* .line 887 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 861 */
} // .end local v1 # "done":Z
} // :cond_5
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void freezeThread ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "tid" # I */
/* .line 854 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 855 */
com.miui.server.greeze.FreezeUtils .freezeTid ( p1 );
/* .line 856 */
} // :cond_0
return;
} // .end method
public java.util.List freezeUids ( Integer[] p0, Long p1, Integer p2, java.lang.String p3, Boolean p4 ) {
/* .locals 25 */
/* .param p1, "uids" # [I */
/* .param p2, "timeout" # J */
/* .param p4, "fromWho" # I */
/* .param p5, "reason" # Ljava/lang/String; */
/* .param p6, "checkAudioGps" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([IJI", */
/* "Ljava/lang/String;", */
/* "Z)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 927 */
/* move-object/from16 v7, p0 */
/* move-object/from16 v8, p1 */
/* move/from16 v9, p4 */
/* move-object/from16 v10, p5 */
/* move/from16 v11, p6 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 928 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "AIDL freezeUids("; // const-string v2, "AIDL freezeUids("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-static/range {p1 ..p1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-wide/from16 v12, p2 */
(( java.lang.StringBuilder ) v1 ).append ( v12, v13 ); // invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", "; // const-string v2, ", "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v11 ); // invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ")"; // const-string v2, ")"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
} // :cond_0
/* move-wide/from16 v12, p2 */
/* .line 930 */
} // :goto_0
if ( v8 != null) { // if-eqz v8, :cond_23
v0 = this.mImmobulusMode;
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* move-object v2, v7 */
/* goto/16 :goto_12 */
/* .line 933 */
} // :cond_1
com.miui.server.greeze.GreezeServiceUtils .getUidMap ( );
/* .line 934 */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v15, v0 */
/* .line 935 */
/* .local v15, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
com.miui.server.greeze.GreezeServiceUtils .getGameUids ( );
/* .line 936 */
/* .local v6, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v5, v0 */
/* .line 937 */
/* .local v5, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* array-length v0, v8 */
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* if-ge v1, v0, :cond_2 */
/* aget v2, v8, v1 */
/* .line 938 */
/* .local v2, "u":I */
java.lang.Integer .valueOf ( v2 );
/* .line 937 */
} // .end local v2 # "u":I
/* add-int/lit8 v1, v1, 0x1 */
/* .line 940 */
} // :cond_2
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 941 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_2
try { // :try_start_0
v2 = this.mFrozenPids;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_d */
int v4 = 1; // const/4 v4, 0x1
/* if-ge v0, v2, :cond_6 */
/* .line 942 */
try { // :try_start_1
v2 = this.mFrozenPids;
(( android.util.SparseArray ) v2 ).valueAt ( v0 ); // invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 943 */
/* .local v2, "inf":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* if-nez v2, :cond_3 */
/* .line 944 */
} // :cond_3
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
v3 = java.lang.Integer .valueOf ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_4
/* if-ne v9, v4, :cond_4 */
/* .line 945 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v2 ).addFreezeInfo ( v3, v4, v9, v10 ); // invoke-virtual {v2, v3, v4, v9, v10}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V
/* .line 946 */
/* .line 948 */
} // :cond_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
v3 = java.lang.Integer .valueOf ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_5
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
v3 = java.lang.Integer .valueOf ( v3 );
/* if-nez v3, :cond_5 */
/* .line 949 */
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
java.lang.Integer .valueOf ( v3 );
/* .line 950 */
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
java.lang.Integer .valueOf ( v3 );
/* .line 951 */
/* if-ne v9, v4, :cond_5 */
/* .line 952 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
(( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v2 ).addFreezeInfo ( v3, v4, v9, v10 ); // invoke-virtual {v2, v3, v4, v9, v10}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->addFreezeInfo(JILjava/lang/String;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 941 */
} // .end local v2 # "inf":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_5
} // :goto_3
/* add-int/lit8 v0, v0, 0x1 */
/* .line 955 */
} // .end local v0 # "i":I
/* :catchall_0 */
/* move-exception v0 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
/* goto/16 :goto_11 */
} // :cond_6
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_d */
/* .line 956 */
} // :goto_4
v0 = /* invoke-interface/range {v17 ..v17}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_21
/* invoke-interface/range {v17 ..v17}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v0, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 957 */
/* .local v3, "uid":I */
(( android.util.SparseArray ) v14 ).get ( v3 ); // invoke-virtual {v14, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* move-object/from16 v18, v0 */
/* check-cast v18, Ljava/util/List; */
/* .line 958 */
/* .local v18, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* if-nez v18, :cond_7 */
/* .line 959 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Failed to freeze invalid uid "; // const-string v2, "Failed to freeze invalid uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 960 */
/* .line 963 */
} // :cond_7
v0 = (( com.miui.server.greeze.GreezeManagerService ) v7 ).isAppRunningInFg ( v3 ); // invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* iget-boolean v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 964 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Skit uid = "; // const-string v2, "Skit uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " reason : FgApp"; // const-string v2, " reason : FgApp"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 965 */
/* .line 968 */
} // :cond_8
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isIMEApp ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isIMEApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 969 */
final String v0 = "Aurogon"; // const-string v0, "Aurogon"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Uid "; // const-string v2, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " was IME app, skip it"; // const-string v2, " was IME app, skip it"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 970 */
/* goto/16 :goto_4 */
/* .line 974 */
} // :cond_9
/* iget-boolean v0, v7, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
if ( v0 != null) { // if-eqz v0, :cond_a
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isWidgetApp ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWidgetApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 975 */
/* goto/16 :goto_4 */
/* .line 978 */
} // :cond_a
v0 = (( com.miui.server.greeze.GreezeManagerService ) v7 ).checkFreeformSmallWin ( v3 ); // invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkFreeformSmallWin(I)Z
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 979 */
v0 = /* invoke-direct {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkStateForScrOff(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 980 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "add small win uid = "; // const-string v2, "add small win uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 982 */
} // :cond_b
final String v0 = "Aurogon"; // const-string v0, "Aurogon"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Uid "; // const-string v2, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " was Freeform small window, skip it"; // const-string v2, " was Freeform small window, skip it"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 983 */
/* goto/16 :goto_4 */
/* .line 987 */
} // :cond_c
} // :goto_5
if ( v11 != null) { // if-eqz v11, :cond_d
v0 = (( com.miui.server.greeze.GreezeManagerService ) v7 ).isUidActive ( v3 ); // invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isUidActive(I)Z
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 988 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Uid "; // const-string v2, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " is using audio or GPS or vibrator, won\'t freeze it, skip it"; // const-string v2, " is using audio or GPS or vibrator, won\'t freeze it, skip it"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 989 */
/* goto/16 :goto_4 */
/* .line 991 */
} // :cond_d
com.android.server.am.ActivityManagerServiceStub .get ( );
v0 = (( com.android.server.am.ActivityManagerServiceStub ) v0 ).isBackuping ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->isBackuping(I)Z
/* if-nez v0, :cond_20 */
/* .line 992 */
com.android.server.am.ActivityManagerServiceStub .get ( );
v0 = (( com.android.server.am.ActivityManagerServiceStub ) v0 ).isActiveInstruUid ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->isActiveInstruUid(I)Z
/* if-nez v0, :cond_1f */
/* .line 993 */
com.android.server.am.ActivityManagerServiceStub .get ( );
v0 = (( com.android.server.am.ActivityManagerServiceStub ) v0 ).isVibratorActive ( v3 ); // invoke-virtual {v0, v3}, Lcom/android/server/am/ActivityManagerServiceStub;->isVibratorActive(I)Z
if ( v0 != null) { // if-eqz v0, :cond_e
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
/* const/16 v19, 0x0 */
/* move v14, v3 */
/* move v3, v4 */
/* goto/16 :goto_10 */
/* .line 997 */
} // :cond_e
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).getActivityControllerUid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getActivityControllerUid()I
/* if-ne v0, v3, :cond_f */
/* .line 998 */
(( com.miui.server.greeze.GreezeManagerService ) v7 ).getPackageNameFromUid ( v3 ); // invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 999 */
/* .local v0, "packageName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_f
v1 = v1 = this.mActivityCtrlBlackList;
/* if-nez v1, :cond_f */
/* .line 1000 */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Uid "; // const-string v4, "Uid "
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " is activityc"; // const-string v4, " is activityc"
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1001 */
int v4 = 1; // const/4 v4, 0x1
/* goto/16 :goto_4 */
/* .line 1005 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_f
v0 = this.mWindowManager;
if ( v0 != null) { // if-eqz v0, :cond_11
/* .line 1007 */
v0 = try { // :try_start_3
if ( v0 != null) { // if-eqz v0, :cond_10
/* .line 1008 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Uid "; // const-string v2, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " was show on screen, skip it"; // const-string v2, " was show on screen, skip it"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* :try_end_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 1009 */
int v4 = 1; // const/4 v4, 0x1
/* goto/16 :goto_4 */
/* .line 1013 */
} // :cond_10
/* .line 1011 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1017 */
} // :cond_11
} // :goto_6
v0 = this.mDisplayManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 1018 */
v0 = (( android.hardware.display.DisplayManagerInternal ) v0 ).isUsingVirtualDisplay ( v3 ); // invoke-virtual {v0, v3}, Landroid/hardware/display/DisplayManagerInternal;->isUsingVirtualDisplay(I)Z
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 1019 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Uid "; // const-string v2, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " was using virtual display, skip it"; // const-string v2, " was using virtual display, skip it"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1020 */
int v4 = 1; // const/4 v4, 0x1
/* goto/16 :goto_4 */
/* .line 1024 */
} // :cond_12
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v4, v0 */
/* .line 1025 */
/* .local v4, "success":Ljava/lang/StringBuilder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v2, v0 */
/* .line 1026 */
/* .local v2, "failed":Ljava/lang/StringBuilder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v1, v0 */
/* .line 1027 */
/* .local v1, "log":Ljava/lang/StringBuilder; */
v8 = this.mAurogonLock;
/* monitor-enter v8 */
/* .line 1028 */
try { // :try_start_4
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_b */
/* move-object/from16 v20, v2 */
} // .end local v2 # "failed":Ljava/lang/StringBuilder;
/* .local v20, "failed":Ljava/lang/StringBuilder; */
try { // :try_start_5
final String v2 = "FZ uid = "; // const-string v2, "FZ uid = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1029 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1030 */
/* .local v0, "skipAdj":Z */
/* invoke-interface/range {v18 ..v18}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
v21 = } // :goto_7
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_a */
if ( v21 != null) { // if-eqz v21, :cond_16
try { // :try_start_6
/* check-cast v21, Lcom/miui/server/greeze/RunningProcess; */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* move-object/from16 v22, v21 */
/* .line 1031 */
/* .local v22, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* move/from16 v21, v0 */
/* move-object/from16 v0, v22 */
/* move-object/from16 v22, v1 */
} // .end local v1 # "log":Ljava/lang/StringBuilder;
/* .local v0, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* .local v21, "skipAdj":Z */
/* .local v22, "log":Ljava/lang/StringBuilder; */
try { // :try_start_7
/* iget v1, v0, Lcom/miui/server/greeze/RunningProcess;->adj:I */
/* if-nez v1, :cond_15 */
/* .line 1032 */
/* iget-boolean v1, v7, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
if ( v1 != null) { // if-eqz v1, :cond_13
v1 = (( com.miui.server.greeze.GreezeManagerService ) v7 ).isAppRunningInFg ( v3 ); // invoke-virtual {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
/* if-nez v1, :cond_13 */
/* .line 1033 */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v23, v0 */
} // .end local v0 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* .local v23, "proc":Lcom/miui/server/greeze/RunningProcess; */
final String v0 = "FZ uid = "; // const-string v0, "FZ uid = "
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " adj"; // const-string v2, " adj"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1034 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1035 */
} // .end local v21 # "skipAdj":Z
/* .local v0, "skipAdj":Z */
/* .line 1032 */
} // .end local v23 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* .local v0, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* .restart local v21 # "skipAdj":Z */
} // :cond_13
/* move-object/from16 v23, v0 */
/* .line 1036 */
} // .end local v0 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* .restart local v23 # "proc":Lcom/miui/server/greeze/RunningProcess; */
v0 = /* invoke-direct {v7, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkStateForScrOff(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_14
/* .line 1037 */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.greeze.GreezeManagerService ) v7 ).dealAdjSet ( v3, v1 ); // invoke-virtual {v7, v3, v1}, Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V
/* .line 1038 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v24, v2 */
final String v2 = "add uid = "; // const-string v2, "add uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " adj"; // const-string v2, " adj"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1036 */
} // :cond_14
/* move-object/from16 v24, v2 */
/* .line 1031 */
} // .end local v23 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* .restart local v0 # "proc":Lcom/miui/server/greeze/RunningProcess; */
} // :cond_15
/* move-object/from16 v23, v0 */
/* move-object/from16 v24, v2 */
/* .line 1041 */
} // .end local v0 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // :goto_8
/* move/from16 v0, v21 */
/* move-object/from16 v1, v22 */
/* move-object/from16 v2, v24 */
/* .line 1067 */
} // .end local v21 # "skipAdj":Z
} // .end local v22 # "log":Ljava/lang/StringBuilder;
/* .restart local v1 # "log":Ljava/lang/StringBuilder; */
/* :catchall_1 */
/* move-exception v0 */
/* move-object v11, v1 */
/* move-object v1, v4 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v1 # "log":Ljava/lang/StringBuilder;
/* .restart local v22 # "log":Ljava/lang/StringBuilder; */
/* goto/16 :goto_f */
/* .line 1030 */
} // .end local v22 # "log":Ljava/lang/StringBuilder;
/* .local v0, "skipAdj":Z */
/* .restart local v1 # "log":Ljava/lang/StringBuilder; */
} // :cond_16
/* move/from16 v21, v0 */
/* move-object/from16 v22, v1 */
/* .line 1042 */
} // .end local v1 # "log":Ljava/lang/StringBuilder;
/* .restart local v22 # "log":Ljava/lang/StringBuilder; */
} // :goto_9
if ( v0 != null) { // if-eqz v0, :cond_17
/* monitor-exit v8 */
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_2 */
/* move-object/from16 v8, p1 */
int v4 = 1; // const/4 v4, 0x1
/* goto/16 :goto_4 */
/* .line 1067 */
} // .end local v0 # "skipAdj":Z
/* :catchall_2 */
/* move-exception v0 */
/* move-object v1, v4 */
/* move-object/from16 v21, v5 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
/* goto/16 :goto_f */
/* .line 1043 */
/* .restart local v0 # "skipAdj":Z */
} // :cond_17
try { // :try_start_8
v1 = java.lang.Integer .valueOf ( v3 );
/* :try_end_8 */
/* .catchall {:try_start_8 ..:try_end_8} :catchall_9 */
if ( v1 != null) { // if-eqz v1, :cond_18
/* .line 1044 */
try { // :try_start_9
v1 = this.mHandler;
/* :try_end_9 */
/* .catchall {:try_start_9 ..:try_end_9} :catchall_4 */
int v2 = 7; // const/4 v2, 0x7
/* move-object/from16 v21, v4 */
int v4 = 0; // const/4 v4, 0x0
} // .end local v4 # "success":Ljava/lang/StringBuilder;
/* .local v21, "success":Ljava/lang/StringBuilder; */
try { // :try_start_a
(( android.os.Handler ) v1 ).obtainMessage ( v2, v4, v3 ); // invoke-virtual {v1, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v1 ).sendMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* :try_end_a */
/* .catchall {:try_start_a ..:try_end_a} :catchall_3 */
/* .line 1067 */
} // .end local v0 # "skipAdj":Z
/* :catchall_3 */
/* move-exception v0 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v1, v21 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
/* goto/16 :goto_f */
} // .end local v21 # "success":Ljava/lang/StringBuilder;
/* .restart local v4 # "success":Ljava/lang/StringBuilder; */
/* :catchall_4 */
/* move-exception v0 */
/* move-object v1, v4 */
/* move-object/from16 v21, v5 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v4 # "success":Ljava/lang/StringBuilder;
/* .restart local v21 # "success":Ljava/lang/StringBuilder; */
/* goto/16 :goto_f */
/* .line 1043 */
} // .end local v21 # "success":Ljava/lang/StringBuilder;
/* .restart local v0 # "skipAdj":Z */
/* .restart local v4 # "success":Ljava/lang/StringBuilder; */
} // :cond_18
/* move-object/from16 v21, v4 */
int v4 = 0; // const/4 v4, 0x0
/* .line 1045 */
} // .end local v4 # "success":Ljava/lang/StringBuilder;
/* .restart local v21 # "success":Ljava/lang/StringBuilder; */
} // :goto_a
try { // :try_start_b
/* invoke-interface/range {v18 ..v18}, Ljava/util/List;->iterator()Ljava/util/Iterator; */
} // :goto_b
v1 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
/* :try_end_b */
/* .catchall {:try_start_b ..:try_end_b} :catchall_8 */
if ( v1 != null) { // if-eqz v1, :cond_1b
try { // :try_start_c
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* check-cast v1, Lcom/miui/server/greeze/RunningProcess; */
/* :try_end_c */
/* .catchall {:try_start_c ..:try_end_c} :catchall_6 */
/* move-object v2, v1 */
/* .line 1046 */
/* .local v2, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* move-object/from16 v11, v22 */
} // .end local v22 # "log":Ljava/lang/StringBuilder;
/* .local v11, "log":Ljava/lang/StringBuilder; */
/* move-object/from16 v1, p0 */
/* move-object v13, v2 */
/* move-object/from16 v12, v20 */
} // .end local v2 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // .end local v20 # "failed":Ljava/lang/StringBuilder;
/* .local v12, "failed":Ljava/lang/StringBuilder; */
/* .local v13, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* move/from16 v19, v4 */
/* move-object/from16 v20, v14 */
/* move-object/from16 v7, v21 */
/* move v14, v3 */
} // .end local v3 # "uid":I
} // .end local v21 # "success":Ljava/lang/StringBuilder;
/* .local v7, "success":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* move-wide/from16 v3, p2 */
/* move-object/from16 v21, v5 */
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* move/from16 v5, p4 */
/* move-object/from16 v22, v6 */
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* move-object/from16 v6, p5 */
try { // :try_start_d
v1 = /* invoke-virtual/range {v1 ..v6}, Lcom/miui/server/greeze/GreezeManagerService;->freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z */
/* if-nez v1, :cond_19 */
/* .line 1047 */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "FZ uid = "; // const-string v3, "FZ uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " pid ="; // const-string v3, " pid ="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " failed!"; // const-string v3, " failed!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1048 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v2, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v12 ).append ( v1 ); // invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1050 */
} // :cond_19
v1 = java.lang.Integer .valueOf ( v14 );
/* if-nez v1, :cond_1a */
/* .line 1051 */
java.lang.Integer .valueOf ( v14 );
/* .line 1052 */
} // :cond_1a
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v2, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_d */
/* .catchall {:try_start_d ..:try_end_d} :catchall_5 */
/* .line 1054 */
} // .end local v13 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // :goto_c
/* move v3, v14 */
/* move/from16 v4, v19 */
/* move-object/from16 v14, v20 */
/* move-object/from16 v5, v21 */
/* move-object/from16 v6, v22 */
/* move-object/from16 v21, v7 */
/* move-object/from16 v22, v11 */
/* move-object/from16 v20, v12 */
/* move-object/from16 v7, p0 */
/* move-wide/from16 v12, p2 */
/* move/from16 v11, p6 */
/* goto/16 :goto_b */
/* .line 1067 */
} // .end local v0 # "skipAdj":Z
/* :catchall_5 */
/* move-exception v0 */
/* move-object/from16 v2, p0 */
/* move-object v1, v7 */
/* goto/16 :goto_f */
} // .end local v7 # "success":Ljava/lang/StringBuilder;
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
/* .restart local v3 # "uid":I */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v20, "failed":Ljava/lang/StringBuilder; */
/* .local v21, "success":Ljava/lang/StringBuilder; */
/* .local v22, "log":Ljava/lang/StringBuilder; */
/* :catchall_6 */
/* move-exception v0 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v7, v21 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
/* move-object/from16 v2, p0 */
/* move-object v1, v7 */
} // .end local v3 # "uid":I
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v7 # "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* goto/16 :goto_f */
/* .line 1055 */
} // .end local v7 # "success":Ljava/lang/StringBuilder;
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
/* .restart local v0 # "skipAdj":Z */
/* .restart local v3 # "uid":I */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v20, "failed":Ljava/lang/StringBuilder; */
/* .local v21, "success":Ljava/lang/StringBuilder; */
/* .local v22, "log":Ljava/lang/StringBuilder; */
} // :cond_1b
/* move/from16 v19, v4 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v7, v21 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v3 # "uid":I
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v7 # "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
try { // :try_start_e
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " pid = [ "; // const-string v2, " pid = [ "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "] "; // const-string v2, "] "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v1 ); // invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1056 */
final String v1 = ""; // const-string v1, ""
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_e */
/* .catchall {:try_start_e ..:try_end_e} :catchall_7 */
/* if-nez v1, :cond_1c */
/* .line 1057 */
try { // :try_start_f
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "failed = [ "; // const-string v2, "failed = [ "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v1 ); // invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_f */
/* .catchall {:try_start_f ..:try_end_f} :catchall_5 */
/* .line 1059 */
} // :cond_1c
try { // :try_start_10
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "FZ uid = "; // const-string v3, "FZ uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " reason ="; // const-string v3, " reason ="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v10 ); // invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " success !"; // const-string v3, " success !"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1061 */
} // :goto_d
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " reason : "; // const-string v2, " reason : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " caller : "; // const-string v2, " caller : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v1 ); // invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_10 */
/* .catchall {:try_start_10 ..:try_end_10} :catchall_7 */
/* .line 1062 */
/* move-object/from16 v2, p0 */
/* move-object v1, v7 */
} // .end local v7 # "success":Ljava/lang/StringBuilder;
/* .local v1, "success":Ljava/lang/StringBuilder; */
try { // :try_start_11
v3 = this.mImmobulusMode;
v3 = (( com.miui.server.greeze.AurogonImmobulusMode ) v3 ).isModeReason ( v10 ); // invoke-virtual {v3, v10}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z
/* if-nez v3, :cond_1d */
/* .line 1063 */
v3 = this.mHistoryLog;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v3 ).log ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1065 */
} // :cond_1d
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* .line 1067 */
} // .end local v0 # "skipAdj":Z
} // :goto_e
/* monitor-exit v8 */
/* :try_end_11 */
/* .catchall {:try_start_11 ..:try_end_11} :catchall_c */
/* .line 1068 */
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isNeedRestictNetworkPolicy ( v14 ); // invoke-virtual {v0, v14}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isNeedRestictNetworkPolicy(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1e
/* .line 1069 */
(( com.miui.server.greeze.GreezeManagerService ) v2 ).monitorNet ( v14 ); // invoke-virtual {v2, v14}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V
/* .line 1071 */
} // :cond_1e
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {v2, v14, v3}, Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V */
/* .line 1072 */
} // .end local v1 # "success":Ljava/lang/StringBuilder;
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
} // .end local v14 # "uid":I
} // .end local v18 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
/* move-object/from16 v8, p1 */
/* move-wide/from16 v12, p2 */
/* move/from16 v11, p6 */
/* move-object v7, v2 */
/* move v4, v3 */
/* move-object/from16 v14, v20 */
/* move-object/from16 v5, v21 */
/* move-object/from16 v6, v22 */
/* goto/16 :goto_4 */
/* .line 1067 */
/* .restart local v7 # "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .restart local v14 # "uid":I */
/* .restart local v18 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* :catchall_7 */
/* move-exception v0 */
/* move-object/from16 v2, p0 */
/* move-object v1, v7 */
} // .end local v7 # "success":Ljava/lang/StringBuilder;
/* .restart local v1 # "success":Ljava/lang/StringBuilder; */
} // .end local v1 # "success":Ljava/lang/StringBuilder;
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
/* .restart local v3 # "uid":I */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v20, "failed":Ljava/lang/StringBuilder; */
/* .local v21, "success":Ljava/lang/StringBuilder; */
/* .local v22, "log":Ljava/lang/StringBuilder; */
/* :catchall_8 */
/* move-exception v0 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v1, v21 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v3 # "uid":I
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v1 # "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v21, "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // .end local v1 # "success":Ljava/lang/StringBuilder;
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* .restart local v3 # "uid":I */
/* .restart local v4 # "success":Ljava/lang/StringBuilder; */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v20, "failed":Ljava/lang/StringBuilder; */
/* .local v22, "log":Ljava/lang/StringBuilder; */
/* :catchall_9 */
/* move-exception v0 */
/* move-object v1, v4 */
/* move-object/from16 v21, v5 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v11, v22 */
/* move-object/from16 v22, v6 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v3 # "uid":I
} // .end local v4 # "success":Ljava/lang/StringBuilder;
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v1 # "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .local v22, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v1, "log":Ljava/lang/StringBuilder; */
/* .restart local v3 # "uid":I */
/* .restart local v4 # "success":Ljava/lang/StringBuilder; */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .local v20, "failed":Ljava/lang/StringBuilder; */
/* :catchall_a */
/* move-exception v0 */
/* move-object v11, v1 */
/* move-object v1, v4 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v12, v20 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v3 # "uid":I
} // .end local v4 # "success":Ljava/lang/StringBuilder;
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v1, "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .local v20, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
} // .end local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v1, "log":Ljava/lang/StringBuilder; */
/* .local v2, "failed":Ljava/lang/StringBuilder; */
/* .restart local v3 # "uid":I */
/* .restart local v4 # "success":Ljava/lang/StringBuilder; */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* :catchall_b */
/* move-exception v0 */
/* move-object v11, v1 */
/* move-object v12, v2 */
/* move-object v1, v4 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
/* move v14, v3 */
} // .end local v2 # "failed":Ljava/lang/StringBuilder;
} // .end local v3 # "uid":I
} // .end local v4 # "success":Ljava/lang/StringBuilder;
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v1, "success":Ljava/lang/StringBuilder; */
/* .restart local v11 # "log":Ljava/lang/StringBuilder; */
/* .restart local v12 # "failed":Ljava/lang/StringBuilder; */
/* .local v14, "uid":I */
/* .restart local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // :goto_f
try { // :try_start_12
/* monitor-exit v8 */
/* :try_end_12 */
/* .catchall {:try_start_12 ..:try_end_12} :catchall_c */
/* throw v0 */
/* :catchall_c */
/* move-exception v0 */
/* .line 992 */
} // .end local v1 # "success":Ljava/lang/StringBuilder;
} // .end local v11 # "log":Ljava/lang/StringBuilder;
} // .end local v12 # "failed":Ljava/lang/StringBuilder;
} // .end local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v3 # "uid":I */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
} // :cond_1f
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
/* const/16 v19, 0x0 */
/* move v14, v3 */
/* move v3, v4 */
} // .end local v3 # "uid":I
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v14, "uid":I */
/* .restart local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .line 991 */
} // .end local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v3 # "uid":I */
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
} // :cond_20
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
/* const/16 v19, 0x0 */
/* move v14, v3 */
/* move v3, v4 */
/* .line 994 */
} // .end local v3 # "uid":I
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v14, "uid":I */
/* .restart local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // :goto_10
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Uid "; // const-string v4, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v14 ); // invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " is backing, instr or vibrator"; // const-string v4, " is backing, instr or vibrator"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 995 */
/* move-object/from16 v8, p1 */
/* move-wide/from16 v12, p2 */
/* move/from16 v11, p6 */
/* move-object v7, v2 */
/* move v4, v3 */
/* move-object/from16 v14, v20 */
/* move-object/from16 v5, v21 */
/* move-object/from16 v6, v22 */
/* goto/16 :goto_4 */
/* .line 1073 */
} // .end local v18 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
} // .end local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .local v14, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
} // :cond_21
/* move v3, v4 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v14 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
/* .restart local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* if-ne v9, v3, :cond_22 */
/* .line 1074 */
/* invoke-direct {v2, v15, v3}, Lcom/miui/server/greeze/GreezeManagerService;->setWakeLockState(Ljava/util/List;Z)V */
/* .line 1075 */
} // :cond_22
/* .line 955 */
} // .end local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .restart local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .restart local v14 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* :catchall_d */
/* move-exception v0 */
/* move-object/from16 v21, v5 */
/* move-object/from16 v22, v6 */
/* move-object v2, v7 */
/* move-object/from16 v20, v14 */
} // .end local v5 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v6 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v14 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
/* .restart local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
} // :goto_11
try { // :try_start_13
/* monitor-exit v1 */
/* :try_end_13 */
/* .catchall {:try_start_13 ..:try_end_13} :catchall_e */
/* throw v0 */
/* :catchall_e */
/* move-exception v0 */
/* .line 930 */
} // .end local v15 # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v20 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v21 # "tmp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v22 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_23
/* move-object v2, v7 */
/* .line 931 */
} // :goto_12
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
} // .end method
public java.util.Map getBroadcastConfig ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 2324 */
v0 = this.mBroadcastCtrlMap;
} // .end method
public Boolean getBroadcastCtrl ( ) {
/* .locals 1 */
/* .line 2317 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z */
} // .end method
public getFrozenPids ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "module" # I */
/* .line 1630 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "AIDL getFrozenPids("; // const-string v2, "AIDL getFrozenPids("
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ")"; // const-string v2, ")"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1631 */
} // :cond_0
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1654 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [I */
/* .line 1634 */
/* :sswitch_0 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1635 */
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
/* .local v0, "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .line 1637 */
} // .end local v0 # "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenNewPids()Ljava/util/List; */
/* .line 1638 */
/* .restart local v0 # "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
} // :goto_0
com.miui.server.greeze.GreezeManagerService .toArray ( v0 );
/* .line 1642 */
} // .end local v0 # "frozens":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
/* :sswitch_1 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1643 */
/* .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 1644 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_1
try { // :try_start_0
v3 = this.mFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_3 */
/* .line 1645 */
v3 = this.mFrozenPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1646 */
/* .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
v4 = v4 = this.mFromWho;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 1647 */
v4 = (( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v3 ).getOwner ( ); // invoke-virtual {v3}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I
/* if-ne v4, p1, :cond_2 */
/* .line 1648 */
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
java.lang.Integer .valueOf ( v4 );
/* .line 1644 */
} // .end local v3 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1651 */
} // .end local v2 # "i":I
} // :cond_3
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1652 */
com.miui.server.greeze.GreezeManagerService .toArray ( v0 );
/* .line 1651 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_1 */
/* 0x1 -> :sswitch_1 */
/* 0x2 -> :sswitch_1 */
/* 0x270f -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public getFrozenUids ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "module" # I */
/* .line 1659 */
/* sparse-switch p1, :sswitch_data_0 */
/* .line 1683 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [I */
/* .line 1664 */
/* :sswitch_0 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 1665 */
/* .local v0, "uids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 1666 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_2 */
/* .line 1667 */
v3 = this.mFrozenPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1668 */
/* .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* const/16 v4, 0x270f */
/* if-eq p1, v4, :cond_0 */
v4 = v4 = this.mFromWho;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 1669 */
v4 = (( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v3 ).getOwner ( ); // invoke-virtual {v3}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I
/* if-ne v4, p1, :cond_1 */
/* .line 1670 */
} // :cond_0
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
java.lang.Integer .valueOf ( v4 );
(( java.util.HashSet ) v0 ).add ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1666 */
} // .end local v3 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1673 */
} // .end local v2 # "i":I
} // :cond_2
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1674 */
v1 = (( java.util.HashSet ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->size()I
/* new-array v1, v1, [I */
/* .line 1675 */
/* .local v1, "rst":[I */
int v2 = 0; // const/4 v2, 0x0
/* .line 1676 */
/* .local v2, "index":I */
(( java.util.HashSet ) v0 ).iterator ( ); // invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;
/* .line 1677 */
/* .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1678 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* aput v4, v1, v2 */
/* .line 1679 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1681 */
} // :cond_3
/* .line 1673 */
} // .end local v1 # "rst":[I
} // .end local v2 # "index":I
} // .end local v3 # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_0 */
/* 0x1 -> :sswitch_0 */
/* 0x2 -> :sswitch_0 */
/* 0x270f -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public Long getLastThawedTime ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "module" # I */
/* .line 687 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* const-wide/32 v2, 0xdbba00 */
/* sub-long/2addr v0, v2 */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getHistoryInfos(J)Ljava/util/List; */
/* .line 688 */
/* .local v0, "historyInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 689 */
/* .local v2, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 690 */
/* iget-wide v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawUptime:J */
/* return-wide v3 */
/* .line 691 */
} // .end local v2 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_0
/* .line 692 */
} // :cond_1
/* const-wide/16 v1, -0x1 */
/* return-wide v1 */
} // .end method
java.lang.String getPackageNameFromUid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 2138 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2140 */
/* .local v0, "packName":Ljava/lang/String; */
v1 = this.mPm;
/* if-nez v1, :cond_0 */
/* .line 2141 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPm = v1;
/* .line 2144 */
} // :cond_0
v1 = this.mPm;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2145 */
(( android.content.pm.PackageManager ) v1 ).getNameForUid ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 2148 */
} // :cond_1
/* if-nez v0, :cond_2 */
/* .line 2149 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get caller pkgname failed uid = "; // const-string v2, "get caller pkgname failed uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
android.util.Slog .d ( v2,v1 );
/* .line 2151 */
} // :cond_2
} // .end method
com.android.internal.app.ProcessMap getPkgMap ( ) {
/* .locals 11 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Lcom/android/internal/app/ProcessMap<", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/RunningProcess;", */
/* ">;>;" */
/* } */
} // .end annotation
/* .line 814 */
/* new-instance v0, Lcom/android/internal/app/ProcessMap; */
/* invoke-direct {v0}, Lcom/android/internal/app/ProcessMap;-><init>()V */
/* .line 815 */
/* .local v0, "map":Lcom/android/internal/app/ProcessMap;, "Lcom/android/internal/app/ProcessMap<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 816 */
/* .local v1, "procList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Lcom/miui/server/greeze/RunningProcess; */
/* .line 817 */
/* .local v3, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* iget v4, v3, Lcom/miui/server/greeze/RunningProcess;->uid:I */
/* .line 818 */
/* .local v4, "uid":I */
v5 = this.pkgList;
/* if-nez v5, :cond_0 */
/* .line 819 */
} // :cond_0
v5 = this.pkgList;
/* array-length v6, v5 */
int v7 = 0; // const/4 v7, 0x0
} // :goto_1
/* if-ge v7, v6, :cond_2 */
/* aget-object v8, v5, v7 */
/* .line 820 */
/* .local v8, "packageName":Ljava/lang/String; */
(( com.android.internal.app.ProcessMap ) v0 ).get ( v8, v4 ); // invoke-virtual {v0, v8, v4}, Lcom/android/internal/app/ProcessMap;->get(Ljava/lang/String;I)Ljava/lang/Object;
/* check-cast v9, Ljava/util/List; */
/* .line 821 */
/* .local v9, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* if-nez v9, :cond_1 */
/* .line 822 */
/* new-instance v10, Ljava/util/ArrayList; */
/* invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V */
/* move-object v9, v10 */
/* .line 823 */
(( com.android.internal.app.ProcessMap ) v0 ).put ( v8, v4, v9 ); // invoke-virtual {v0, v8, v4, v9}, Lcom/android/internal/app/ProcessMap;->put(Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/Object;
/* .line 825 */
} // :cond_1
/* .line 819 */
} // .end local v8 # "packageName":Ljava/lang/String;
} // .end local v9 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
/* add-int/lit8 v7, v7, 0x1 */
/* .line 827 */
} // .end local v3 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // .end local v4 # "uid":I
} // :cond_2
/* .line 828 */
} // :cond_3
} // .end method
com.miui.server.greeze.RunningProcess getProcessByPid ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "pid" # I */
/* .line 799 */
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 800 */
/* .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/RunningProcess; */
/* .line 801 */
/* .local v2, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* iget v3, v2, Lcom/miui/server/greeze/RunningProcess;->pid:I */
/* if-ne p1, v3, :cond_0 */
/* .line 802 */
/* .line 804 */
} // .end local v2 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // :cond_0
/* .line 805 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
java.util.List getProcessByUid ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/RunningProcess;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 790 */
com.miui.server.greeze.GreezeServiceUtils .getUidMap ( );
(( android.util.SparseArray ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v0, Ljava/util/List; */
/* .line 791 */
/* .local v0, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 792 */
/* .line 794 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
} // .end method
public Integer getUidByPackageName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 3383 */
v0 = this.mContext;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* if-nez v0, :cond_0 */
/* .line 3387 */
} // :cond_0
try { // :try_start_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getPackageManager ( ); // invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
v0 = (( android.content.pm.PackageManager ) v0 ).getPackageUid ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageUid(Ljava/lang/String;I)I
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 3388 */
/* .local v0, "uid":I */
/* .line 3389 */
} // .end local v0 # "uid":I
/* :catch_0 */
/* move-exception v0 */
/* .line 3390 */
/* .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "can not found the app for "; // const-string v3, "can not found the app for "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "#0"; // const-string v3, "#0"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
android.util.Log .e ( v3,v2 );
/* .line 3391 */
/* .line 3384 */
} // .end local v0 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :cond_1
} // :goto_0
} // .end method
Boolean isAllowBroadcast ( Integer p0, java.lang.String p1, Integer p2, java.lang.String p3, java.lang.String p4 ) {
/* .locals 4 */
/* .param p1, "callerUid" # I */
/* .param p2, "callerPkgName" # Ljava/lang/String; */
/* .param p3, "calleeUid" # I */
/* .param p4, "calleePkgName" # Ljava/lang/String; */
/* .param p5, "action" # Ljava/lang/String; */
/* .line 2458 */
v0 = this.mBroadcastTargetWhiteList;
/* monitor-enter v0 */
/* .line 2459 */
try { // :try_start_0
v1 = v1 = this.mBroadcastTargetWhiteList;
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2460 */
v1 = this.mBroadcastTargetWhiteList;
/* check-cast v1, Ljava/util/List; */
/* .line 2461 */
v3 = /* .local v1, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v3, :cond_0 */
v3 = final String v3 = "*"; // const-string v3, "*"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 2462 */
} // :cond_0
/* monitor-exit v0 */
/* .line 2465 */
} // .end local v1 # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* .line 2467 */
v1 = this.mBroadcastCallerWhiteList;
/* monitor-enter v1 */
/* .line 2468 */
try { // :try_start_1
v0 = v0 = this.mBroadcastCallerWhiteList;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 2469 */
v0 = this.mBroadcastCallerWhiteList;
/* check-cast v0, Ljava/util/List; */
/* .line 2470 */
v3 = /* .local v0, "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* if-nez v3, :cond_2 */
v3 = final String v3 = "*"; // const-string v3, "*"
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 2471 */
} // :cond_2
/* monitor-exit v1 */
/* .line 2474 */
} // .end local v0 # "actions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_3
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* .line 2476 */
v0 = this.mBroadcastCtrlMap;
/* monitor-enter v0 */
/* .line 2477 */
try { // :try_start_2
v1 = v1 = this.mBroadcastCtrlMap;
if ( v1 != null) { // if-eqz v1, :cond_5
v1 = this.mBroadcastCtrlMap;
/* check-cast v1, Ljava/lang/String; */
final String v3 = "ALL"; // const-string v3, "ALL"
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_4 */
v1 = this.mBroadcastCtrlMap;
/* .line 2478 */
/* check-cast v1, Ljava/lang/String; */
v1 = (( java.lang.String ) v1 ).contains ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 2479 */
} // :cond_4
/* monitor-exit v0 */
/* .line 2481 */
} // :cond_5
/* monitor-exit v0 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 2483 */
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isSystemOrMiuiImportantApp ( p4 ); // invoke-virtual {v0, p4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_d */
v0 = com.miui.server.greeze.GreezeManagerService.mAllowBroadcast;
v0 = /* .line 2484 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 2494 */
} // :cond_6
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).checkAurogonIntentDenyList ( p5 ); // invoke-virtual {p0, p5}, Lcom/miui/server/greeze/GreezeManagerService;->checkAurogonIntentDenyList(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 2495 */
/* .line 2498 */
} // :cond_7
/* const/16 v0, 0x403 */
/* if-eq p1, v0, :cond_c */
/* const/16 v0, 0x3ea */
/* if-ne p1, v0, :cond_8 */
/* .line 2502 */
} // :cond_8
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 2504 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isAppRunningInFg ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 2505 */
/* .line 2508 */
} // :cond_9
/* if-ne p1, p3, :cond_a */
/* .line 2509 */
/* .line 2512 */
} // :cond_a
final String v0 = "com.xiaomi.xmsf"; // const-string v0, "com.xiaomi.xmsf"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 2513 */
/* .line 2516 */
} // :cond_b
/* .line 2499 */
} // :cond_c
} // :goto_0
/* .line 2485 */
} // :cond_d
} // :goto_1
final String v0 = "com.google.android.gms"; // const-string v0, "com.google.android.gms"
v0 = (( java.lang.String ) p4 ).contains ( v0 ); // invoke-virtual {p4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_e
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
/* if-nez v0, :cond_e */
/* .line 2486 */
/* .line 2489 */
} // :cond_e
v0 = /* invoke-direct {p0, p5}, Lcom/miui/server/greeze/GreezeManagerService;->deferBroadcastForMiui(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 2490 */
/* .line 2491 */
} // :cond_f
/* .line 2481 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v1 */
/* .line 2474 */
/* :catchall_1 */
/* move-exception v0 */
try { // :try_start_4
/* monitor-exit v1 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* throw v0 */
/* .line 2465 */
/* :catchall_2 */
/* move-exception v1 */
try { // :try_start_5
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_2 */
/* throw v1 */
} // .end method
public Boolean isAllowWakeUpList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 708 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getPackageNameFromUid ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 709 */
/* .local v0, "packageName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 710 */
v1 = this.mImmobulusMode;
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).isAllowWakeUpList ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isAllowWakeUpList(Ljava/lang/String;)Z
/* .line 712 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isAppRunning ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 1742 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1744 */
} // :cond_0
(( com.miui.server.greeze.GreezeManagerService ) p0 ).readPidFromCgroup ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->readPidFromCgroup(I)Ljava/util/List;
/* .line 1745 */
v2 = /* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_1 */
/* .line 1746 */
} // :cond_1
v4 = } // :goto_0
if ( v4 != null) { // if-eqz v4, :cond_3
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 1747 */
/* .local v4, "pid":I */
v5 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).readPidStatus ( p1, v4 ); // invoke-virtual {p0, p1, v4}, Lcom/miui/server/greeze/GreezeManagerService;->readPidStatus(II)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1748 */
/* .line 1750 */
} // .end local v4 # "pid":I
} // :cond_2
/* .line 1751 */
} // :cond_3
} // .end method
public Boolean isAppRunningInFg ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 1756 */
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isAppShowOnWindows ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1230 */
v0 = this.mWindowManager;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1232 */
v0 = try { // :try_start_0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1233 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Uid "; // const-string v2, "Uid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " was show on screen, skip it"; // const-string v2, " was show on screen, skip it"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1234 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1237 */
} // :cond_0
/* .line 1236 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1239 */
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
Boolean isNeedAllowRequest ( Integer p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "callerUid" # I */
/* .param p2, "callerPkgName" # Ljava/lang/String; */
/* .param p3, "calleeUid" # I */
/* .line 2374 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_1 */
/* .line 2375 */
/* const/16 v0, 0x403 */
/* if-eq p1, v0, :cond_0 */
/* const/16 v0, 0x3ea */
/* if-ne p1, v0, :cond_4 */
/* .line 2376 */
} // :cond_0
/* .line 2380 */
} // :cond_1
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isAppRunningInFg ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2381 */
/* .line 2384 */
} // :cond_2
/* if-ne p1, p3, :cond_3 */
/* .line 2385 */
/* .line 2388 */
} // :cond_3
final String v0 = "com.xiaomi.xmsf"; // const-string v0, "com.xiaomi.xmsf"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 2389 */
/* .line 2392 */
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isNeedCachedAlarmForAurogon ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 1737 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isNeedCachedAlarmForAurogonInner ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedCachedAlarmForAurogonInner(I)Z
} // .end method
public Boolean isNeedCachedAlarmForAurogonInner ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 2103 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isUidFrozen ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 2105 */
} // :cond_0
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getPackageNameFromUid ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 2106 */
/* .local v0, "packageName":Ljava/lang/String; */
/* if-nez v0, :cond_1 */
/* .line 2108 */
} // :cond_1
v2 = android.os.UserHandle .isApp ( p1 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* const-string/jumbo v2, "xiaomi" */
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_4 */
final String v2 = "miui"; // const-string v2, "miui"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = v2 = this.mAurogonAlarmForbidList;
/* if-nez v2, :cond_2 */
/* .line 2112 */
} // :cond_2
v2 = v2 = this.mAurogonAlarmAllowList;
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 2113 */
/* .line 2115 */
} // :cond_3
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
final String v2 = "cached alarm!"; // const-string v2, "cached alarm!"
android.util.Slog .d ( v1,v2 );
/* .line 2116 */
int v1 = 1; // const/4 v1, 0x1
/* .line 2109 */
} // :cond_4
} // :goto_0
} // .end method
public Boolean isNeedCachedBroadcast ( android.content.Intent p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "uid" # I */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 2396 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isUidFrozen ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2397 */
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).checkAurogonIntentDenyList ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->checkAurogonIntentDenyList(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2398 */
/* .line 2400 */
} // :cond_0
(( android.content.Intent ) p1 ).getSelector ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 2401 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "drop bc, selector:"; // const-string v2, "drop bc, selector:"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( android.content.Intent ) p1 ).getSelector ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getSelector()Landroid/content/Intent;
(( android.content.Intent ) v2 ).toString ( ); // invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
android.util.Slog .w ( v2,v0 );
/* .line 2402 */
/* .line 2404 */
} // :cond_1
v0 = com.miui.server.greeze.GreezeManagerService.mNeedCachedBroadcast;
v0 = (( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 2405 */
(( android.content.Intent ) p1 ).setPackage ( p3 ); // invoke-virtual {p1, p3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 2406 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).CachedBroadcasForAurogon ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->CachedBroadcasForAurogon(Landroid/content/Intent;I)V
/* .line 2407 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2410 */
} // :cond_2
} // .end method
public Boolean isRestrictBackgroundAction ( java.lang.String p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 4 */
/* .param p1, "localhost" # Ljava/lang/String; */
/* .param p2, "callerUid" # I */
/* .param p3, "callerPkgName" # Ljava/lang/String; */
/* .param p4, "calleeUid" # I */
/* .param p5, "calleePkgName" # Ljava/lang/String; */
/* .line 2339 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2340 */
/* .local v0, "ret":Z */
v1 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isUidFrozen ( p4 ); // invoke-virtual {p0, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_0 */
/* .line 2341 */
/* .line 2343 */
} // :cond_0
/* sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 2344 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "localhost = "; // const-string v3, "localhost = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " callerUid = "; // const-string v3, " callerUid = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " callerPkgName = "; // const-string v3, " callerPkgName = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " calleeUid = "; // const-string v3, " calleeUid = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " calleePkgName = "; // const-string v3, " calleePkgName = "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p5 ); // invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
android.util.Slog .d ( v3,v1 );
/* .line 2347 */
} // :cond_1
v1 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
final String v3 = "provider"; // const-string v3, "provider"
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_2
/* :sswitch_0 */
/* const-string/jumbo v1, "startservice" */
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
int v2 = 3; // const/4 v2, 0x3
/* :sswitch_1 */
final String v1 = "bindservice"; // const-string v1, "bindservice"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
int v2 = 2; // const/4 v2, 0x2
/* :sswitch_2 */
v1 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* :sswitch_3 */
final String v1 = "broadcast"; // const-string v1, "broadcast"
v1 = (( java.lang.String ) p1 ).equals ( v1 ); // invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
int v2 = -1; // const/4 v2, -0x1
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 2356 */
/* :pswitch_0 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isNeedAllowRequest ( p2, p3, p4 ); // invoke-virtual {p0, p2, p3, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedAllowRequest(ILjava/lang/String;I)Z
/* .line 2357 */
/* .line 2352 */
/* :pswitch_1 */
int v0 = 1; // const/4 v0, 0x1
/* .line 2353 */
/* .line 2349 */
/* :pswitch_2 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isNeedAllowRequest ( p2, p3, p4 ); // invoke-virtual {p0, p2, p3, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedAllowRequest(ILjava/lang/String;I)Z
/* .line 2350 */
/* nop */
/* .line 2362 */
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 2363 */
v1 = this.mImmobulusMode;
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).isRunningLaunchMode ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
if ( v1 != null) { // if-eqz v1, :cond_3
v1 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
/* .line 2364 */
v1 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).addLaunchModeQiutList ( p4 ); // invoke-virtual {v1, p4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V
/* .line 2365 */
/* .line 2367 */
} // :cond_3
/* const/16 v1, 0x3e8 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p4, v1, p1 ); // invoke-virtual {p0, p4, v1, p1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 2370 */
} // :cond_4
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x607e173f -> :sswitch_3 */
/* -0x3adbfa0f -> :sswitch_2 */
/* -0xeb32c28 -> :sswitch_1 */
/* 0xb07b013 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public Boolean isRestrictReceiver ( android.content.Intent p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 10 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "callerUid" # I */
/* .param p3, "callerPkgName" # Ljava/lang/String; */
/* .param p4, "calleeUid" # I */
/* .param p5, "calleePkgName" # Ljava/lang/String; */
/* .line 2415 */
int v0 = 0; // const/4 v0, 0x0
/* .line 2416 */
/* .local v0, "ret":Z */
/* iget-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z */
int v2 = 0; // const/4 v2, 0x0
/* const/16 v3, 0x3e8 */
/* if-nez v1, :cond_0 */
/* .line 2417 */
final String v1 = "broadcast_cl"; // const-string v1, "broadcast_cl"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p4, v3, v1 ); // invoke-virtual {p0, p4, v3, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 2418 */
/* .line 2420 */
} // :cond_0
v1 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isUidFrozen ( p4 ); // invoke-virtual {p0, p4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
/* if-nez v1, :cond_1 */
/* .line 2421 */
/* .line 2423 */
} // :cond_1
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 2424 */
/* .local v1, "action":Ljava/lang/String; */
final String v4 = "GreezeManager"; // const-string v4, "GreezeManager"
/* if-nez v1, :cond_2 */
/* .line 2425 */
final String v5 = "bc_action"; // const-string v5, "bc_action"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p4, v3, v5 ); // invoke-virtual {p0, p4, v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 2426 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "isNeedCachedBroadcast white action ="; // const-string v5, "isNeedCachedBroadcast white action ="
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 2427 */
/* .line 2430 */
} // :cond_2
/* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 2431 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Broadcast callerUid = "; // const-string v5, "Broadcast callerUid = "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " callerPkgName = "; // const-string v5, " callerPkgName = "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p3 ); // invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " calleeUid = "; // const-string v5, " calleeUid = "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p4 ); // invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " calleePkgName = "; // const-string v5, " calleePkgName = "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p5 ); // invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " action = "; // const-string v5, " action = "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v2 );
/* .line 2434 */
} // :cond_3
/* move-object v4, p0 */
/* move v5, p2 */
/* move-object v6, p3 */
/* move v7, p4 */
/* move-object v8, p5 */
/* move-object v9, v1 */
v2 = /* invoke-virtual/range {v4 ..v9}, Lcom/miui/server/greeze/GreezeManagerService;->isAllowBroadcast(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z */
/* xor-int/lit8 v2, v2, 0x1 */
/* move v0, v2 */
/* .line 2435 */
/* if-nez v0, :cond_5 */
/* .line 2436 */
v2 = this.mImmobulusMode;
v2 = (( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).isRunningLaunchMode ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
if ( v2 != null) { // if-eqz v2, :cond_4
v2 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).checkImmobulusModeRestrict ( p5, v1 ); // invoke-virtual {p0, p5, v1}, Lcom/miui/server/greeze/GreezeManagerService;->checkImmobulusModeRestrict(Ljava/lang/String;Ljava/lang/String;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 2437 */
v2 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).addLaunchModeQiutList ( p4 ); // invoke-virtual {v2, p4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V
/* .line 2439 */
} // :cond_4
final String v2 = "broadcast"; // const-string v2, "broadcast"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p4, v3, v2 ); // invoke-virtual {p0, p4, v3, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 2442 */
} // :cond_5
(( com.miui.server.greeze.GreezeManagerService ) p0 ).isNeedCachedBroadcast ( p1, p4, p5 ); // invoke-virtual {p0, p1, p4, p5}, Lcom/miui/server/greeze/GreezeManagerService;->isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;)Z
/* .line 2444 */
} // :goto_0
} // .end method
public Boolean isUidActive ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 837 */
int v0 = 3; // const/4 v0, 0x3
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
miui.process.ProcessManager .getActiveUidInfo ( v0 );
/* .line 838 */
/* .local v0, "infos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Lmiui/process/ActiveUidInfo; */
/* .line 839 */
/* .local v3, "info":Lmiui/process/ActiveUidInfo; */
/* iget v4, v3, Lmiui/process/ActiveUidInfo;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* if-ne v4, p1, :cond_0 */
/* .line 840 */
/* .line 842 */
} // .end local v3 # "info":Lmiui/process/ActiveUidInfo;
} // :cond_0
/* .line 847 */
} // .end local v0 # "infos":Ljava/util/List;, "Ljava/util/List<Lmiui/process/ActiveUidInfo;>;"
} // :cond_1
/* nop */
/* .line 848 */
int v0 = 0; // const/4 v0, 0x0
/* .line 843 */
/* :catch_0 */
/* move-exception v0 */
/* .line 844 */
/* .local v0, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to get active audio info.Going to freeze uid"; // const-string v3, "Failed to get active audio info.Going to freeze uid"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " regardless of whether it using audio"; // const-string v3, " regardless of whether it using audio"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
android.util.Slog .w ( v3,v2,v0 );
/* .line 846 */
} // .end method
public Boolean isUidFrozen ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 697 */
v0 = this.mFrozenPids;
/* monitor-enter v0 */
/* .line 698 */
/* const/16 v1, 0x270f */
try { // :try_start_0
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getFrozenUids ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I
/* .line 699 */
/* .local v1, "frozenUids":[I */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_1 */
/* aget v5, v1, v4 */
/* .line 700 */
/* .local v5, "frozenuid":I */
/* if-ne v5, p1, :cond_0 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 699 */
} // .end local v5 # "frozenuid":I
} // :cond_0
/* add-int/lit8 v4, v4, 0x1 */
/* .line 702 */
} // :cond_1
/* monitor-exit v0 */
/* .line 703 */
} // .end local v1 # "frozenUids":[I
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void monitorNet ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 772 */
com.miui.server.greeze.GreezeManagerService .nAddConcernedUid ( p1 );
/* .line 773 */
return;
} // .end method
public void notifyBackup ( Integer p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "start" # Z */
/* .line 2610 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1, p2 ); // invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 2611 */
return;
} // .end method
public void notifyDumpAllInfo ( ) {
/* .locals 2 */
/* .line 2214 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
final String v1 = "notify dump all info"; // const-string v1, "notify dump all info"
android.util.Slog .d ( v0,v1 );
/* .line 2215 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$16; */
/* invoke-direct {v1, p0}, Lcom/miui/server/greeze/GreezeManagerService$16;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2220 */
return;
} // .end method
public void notifyDumpAppInfo ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 2203 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notify dump uid "; // const-string v1, "notify dump uid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " pid "; // const-string v1, " pid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
android.util.Slog .d ( v1,v0 );
/* .line 2204 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isUidFrozen ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 2205 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$15; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$15;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2211 */
} // :cond_0
return;
} // .end method
public void notifyExcuteServices ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 2183 */
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* const/16 v0, 0x4e1f */
/* if-le p1, v0, :cond_0 */
/* .line 2186 */
} // :cond_0
v0 = this.mExcuteServiceList;
/* monitor-enter v0 */
/* .line 2187 */
try { // :try_start_0
v1 = this.mExcuteServiceList;
v1 = java.lang.Integer .valueOf ( p1 );
/* if-nez v1, :cond_1 */
v1 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isAppRunningInFg ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
/* if-nez v1, :cond_1 */
/* .line 2188 */
v1 = this.mExcuteServiceList;
java.lang.Integer .valueOf ( p1 );
/* .line 2190 */
} // :cond_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2191 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$14; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$14;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2200 */
return;
/* .line 2190 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 2184 */
} // :cond_2
} // :goto_0
return;
} // .end method
public void notifyFreeformModeFocus ( java.lang.String p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "string" # Ljava/lang/String; */
/* .param p2, "mode" # I */
/* .line 1910 */
final String v0 = ":"; // const-string v0, ":"
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1913 */
/* .local v0, "str":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_4 */
/* .line 1914 */
int v1 = 1; // const/4 v1, 0x1
/* aget-object v2, v0, v1 */
/* .line 1915 */
/* .local v2, "packageName":Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v4, v0, v3 */
v4 = java.lang.Integer .parseInt ( v4 );
/* .line 1919 */
/* .local v4, "uid":I */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1920 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " notifyFreeformModeFocus packageName = "; // const-string v6, " notifyFreeformModeFocus packageName = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " mode = "; // const-string v6, " mode = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( p2 ); // invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v6 = "Aurogon"; // const-string v6, "Aurogon"
android.util.Slog .d ( v6,v5 );
/* .line 1922 */
} // :cond_0
/* if-nez p2, :cond_1 */
/* move v5, v1 */
} // :cond_1
/* move v5, v3 */
} // :goto_0
/* if-ne p2, v1, :cond_2 */
/* move v6, v1 */
} // :cond_2
/* move v6, v3 */
} // :goto_1
/* or-int/2addr v5, v6 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 1923 */
/* const/16 v3, 0x3e8 */
final String v5 = "FreeformMode"; // const-string v5, "FreeformMode"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( v4, v3, v5 ); // invoke-virtual {p0, v4, v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 1924 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).updateFreeformSmallWinList ( v2, v1 ); // invoke-virtual {p0, v2, v1}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V
/* .line 1926 */
} // :cond_3
(( com.miui.server.greeze.GreezeManagerService ) p0 ).updateFreeformSmallWinList ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V
/* .line 1928 */
} // :goto_2
return;
/* .line 1917 */
} // .end local v2 # "packageName":Ljava/lang/String;
} // .end local v4 # "uid":I
} // :cond_4
return;
} // .end method
public void notifyMovetoFront ( Integer p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "inFreeformSmallWinMode" # Z */
/* .line 1871 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1872 */
final String v0 = "Aurogon"; // const-string v0, "Aurogon"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyMovetoFront uid = "; // const-string v2, "notifyMovetoFront uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " inFreeformSmallWinMode = "; // const-string v2, " inFreeformSmallWinMode = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1874 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 1875 */
/* iput-boolean p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z */
/* .line 1876 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getPackageNameFromUid ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 1877 */
/* .local v0, "packageName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1878 */
v1 = this.mImmobulusMode;
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).isRunningLaunchMode ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
/* if-nez v1, :cond_1 */
v1 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).checkFreeformSmallWin ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->checkFreeformSmallWin(Ljava/lang/String;)Z
/* if-nez v1, :cond_1 */
/* .line 1879 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).triggerLaunchMode ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->triggerLaunchMode(Ljava/lang/String;I)V
/* .line 1881 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.greeze.GreezeManagerService ) p0 ).updateFreeformSmallWinList ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V
/* .line 1883 */
} // :cond_2
v1 = this.mImmobulusMode;
/* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1884 */
v1 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).quitImmobulusMode ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 1887 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // :cond_3
/* const/16 v0, 0x3e8 */
final String v1 = "Activity Front"; // const-string v1, "Activity Front"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 1888 */
v0 = this.mRecentLaunchAppList;
/* monitor-enter v0 */
/* .line 1889 */
try { // :try_start_0
v1 = this.mRecentLaunchAppList;
v1 = java.lang.Integer .valueOf ( p1 );
/* if-nez v1, :cond_4 */
/* .line 1890 */
v1 = this.mRecentLaunchAppList;
java.lang.Integer .valueOf ( p1 );
/* .line 1891 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).removetLaunchAppListDelay ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->removetLaunchAppListDelay(I)V
/* .line 1893 */
} // :cond_4
/* monitor-exit v0 */
/* .line 1895 */
return;
/* .line 1893 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyMultitaskLaunch ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 1947 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1948 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notifyMultitaskLaunch uid = "; // const-string v1, "notifyMultitaskLaunch uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " packageName = "; // const-string v1, " packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Aurogon"; // const-string v1, "Aurogon"
android.util.Slog .d ( v1,v0 );
/* .line 1950 */
} // :cond_0
/* const/16 v0, 0x3e8 */
final String v1 = "Multitask"; // const-string v1, "Multitask"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 1951 */
return;
} // .end method
public void notifyOtherModule ( com.miui.server.greeze.GreezeManagerService$FrozenInfo p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "info" # Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .param p2, "fromWho" # I */
/* .line 1404 */
/* iget v0, p1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I */
/* .line 1405 */
/* .local v0, "state":I */
/* iget v1, p1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* .line 1406 */
/* .local v1, "uid":I */
/* iget v2, p1, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
/* .line 1407 */
/* .local v2, "pid":I */
v3 = android.os.UserHandle .isApp ( v1 );
/* if-nez v3, :cond_0 */
/* .line 1408 */
return;
/* .line 1409 */
} // :cond_0
v3 = this.mHandler;
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$6; */
/* invoke-direct {v4, p0, v1, v2, p2}, Lcom/miui/server/greeze/GreezeManagerService$6;-><init>(Lcom/miui/server/greeze/GreezeManagerService;III)V */
(( android.os.Handler ) v3 ).post ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1426 */
return;
} // .end method
public void notifyResumeTopActivity ( Integer p0, java.lang.String p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "inMultiWindowMode" # Z */
/* .line 1970 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1971 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "notifyResumeTopActivity uid = "; // const-string v2, "notifyResumeTopActivity uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " packageName = "; // const-string v2, " packageName = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " inMultiWindowMode = "; // const-string v2, " inMultiWindowMode = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 1973 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
/* if-eq p1, v0, :cond_7 */
v0 = android.os.UserHandle .isApp ( p1 );
/* if-nez v0, :cond_1 */
/* .line 1974 */
} // :cond_1
/* const/16 v0, 0x3e8 */
final String v1 = "Activity Resume"; // const-string v1, "Activity Resume"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 1975 */
v0 = this.mRecentLaunchAppList;
/* monitor-enter v0 */
/* .line 1976 */
try { // :try_start_0
v1 = this.mRecentLaunchAppList;
v1 = java.lang.Integer .valueOf ( p1 );
/* if-nez v1, :cond_2 */
/* .line 1977 */
v1 = this.mRecentLaunchAppList;
java.lang.Integer .valueOf ( p1 );
/* .line 1978 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).removetLaunchAppListDelay ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->removetLaunchAppListDelay(I)V
/* .line 1980 */
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1981 */
v0 = this.mTopAppPackageName;
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_6 */
/* .line 1982 */
/* iget v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
v0 = android.os.UserHandle .isApp ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1983 */
v0 = this.mImmobulusMode;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
(( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).reOrderTargetList ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->reOrderTargetList(I)V
/* .line 1985 */
} // :cond_3
this.mTopAppPackageName = p2;
/* .line 1986 */
/* iput p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
/* .line 1987 */
if ( p3 != null) { // if-eqz p3, :cond_4
/* .line 1988 */
v0 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).addTempMutiWindowsApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addTempMutiWindowsApp(I)V
/* .line 1989 */
return;
/* .line 1993 */
} // :cond_4
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1994 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z */
/* .line 1995 */
return;
/* .line 1997 */
} // :cond_5
(( com.miui.server.greeze.GreezeManagerService ) p0 ).triggerLaunchMode ( p2, p1 ); // invoke-virtual {p0, p2, p1}, Lcom/miui/server/greeze/GreezeManagerService;->triggerLaunchMode(Ljava/lang/String;I)V
/* .line 1999 */
} // :cond_6
return;
/* .line 1980 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
/* .line 1973 */
} // :cond_7
} // :goto_0
return;
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .line 2920 */
/* new-instance v0, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand; */
/* invoke-direct {v0, p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
/* .line 2921 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 2922 */
return;
} // .end method
public void queryBinderState ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 784 */
com.miui.server.greeze.GreezeManagerService .nQueryBinder ( p1 );
/* .line 785 */
return;
} // .end method
public java.util.List readPidFromCgroup ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1244 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/sys/fs/cgroup/uid_"; // const-string v1, "/sys/fs/cgroup/uid_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1245 */
/* .local v0, "path":Ljava/lang/String; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1246 */
/* .local v1, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1248 */
/* .local v2, "file":Ljava/io/File; */
(( java.io.File ) v2 ).listFiles ( ); // invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 1250 */
/* .local v3, "tempList":[Ljava/io/File; */
/* if-nez v3, :cond_0 */
/* .line 1252 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
/* array-length v5, v3 */
/* if-ge v4, v5, :cond_3 */
/* .line 1253 */
/* aget-object v5, v3, v4 */
(( java.io.File ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;
/* .line 1254 */
/* .local v5, "temp":Ljava/lang/String; */
final String v6 = "pid_"; // const-string v6, "pid_"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v6, :cond_1 */
/* .line 1255 */
/* .line 1258 */
} // :cond_1
/* aget-object v6, v3, v4 */
(( java.io.File ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;
final String v7 = "_"; // const-string v7, "_"
(( java.lang.String ) v6 ).split ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1259 */
/* .local v6, "Str":[Ljava/lang/String; */
/* array-length v7, v6 */
int v8 = 2; // const/4 v8, 0x2
/* if-le v7, v8, :cond_2 */
if ( v6 != null) { // if-eqz v6, :cond_2
/* aget-object v7, v6, v8 */
if ( v7 != null) { // if-eqz v7, :cond_2
/* .line 1260 */
/* aget-object v7, v6, v8 */
v7 = java.lang.Integer .parseInt ( v7 );
java.lang.Integer .valueOf ( v7 );
/* .line 1252 */
} // .end local v5 # "temp":Ljava/lang/String;
} // .end local v6 # "Str":[Ljava/lang/String;
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1263 */
} // .end local v4 # "i":I
} // :cond_3
} // .end method
public Boolean readPidStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 1272 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/proc/"; // const-string v1, "/proc/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "/status"; // const-string v1, "/status"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 1273 */
/* .local v0, "path":Ljava/lang/String; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 1274 */
/* .local v1, "file":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1275 */
int v2 = 1; // const/4 v2, 0x1
/* .line 1277 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
public Boolean readPidStatus ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .line 1267 */
v0 = android.os.Process .getUidForPid ( p2 );
/* .line 1268 */
/* .local v0, "tempUid":I */
/* if-ne v0, p1, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
public Boolean registerCallback ( miui.greeze.IGreezeCallback p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "callback" # Lmiui/greeze/IGreezeCallback; */
/* .param p2, "module" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 672 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 673 */
v0 = android.os.Binder .getCallingUid ( );
/* const/16 v1, 0x3e8 */
int v2 = 0; // const/4 v2, 0x0
/* if-eq v0, v1, :cond_0 */
/* .line 674 */
/* .line 675 */
} // :cond_0
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
java.lang.Integer .valueOf ( p2 );
int v3 = 0; // const/4 v3, 0x0
(( java.util.HashMap ) v0 ).getOrDefault ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 676 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Already registed callback for module: "; // const-string v1, "Already registed callback for module: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
android.util.Slog .i ( v1,v0 );
/* .line 677 */
} // :cond_1
int v0 = 4; // const/4 v0, 0x4
/* if-eq p2, v0, :cond_3 */
int v0 = 3; // const/4 v0, 0x3
/* if-ne p2, v0, :cond_2 */
/* .line 679 */
} // :cond_2
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
java.lang.Integer .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 680 */
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient; */
/* invoke-direct {v1, p0, p2}, Lcom/miui/server/greeze/GreezeManagerService$CallbackDeathRecipient;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V */
/* .line 681 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
/* .line 682 */
int v0 = 1; // const/4 v0, 0x1
/* .line 678 */
} // :cond_3
} // :goto_0
} // .end method
public Boolean registerMonitor ( miui.greeze.IMonitorToken p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "token" # Lmiui/greeze/IMonitorToken; */
/* .param p2, "type" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 366 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 367 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Monitor registered, type "; // const-string v2, "Monitor registered, type "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " pid "; // const-string v2, " pid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = com.miui.server.greeze.GreezeManagerService .getCallingPid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 368 */
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 369 */
v0 = this.mMonitorTokens;
/* monitor-enter v0 */
/* .line 370 */
try { // :try_start_0
v2 = this.mMonitorTokens;
(( android.util.SparseArray ) v2 ).put ( p2, p1 ); // invoke-virtual {v2, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 371 */
/* new-instance v3, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient; */
/* invoke-direct {v3, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$MonitorDeathRecipient;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Lmiui/greeze/IMonitorToken;I)V */
int v4 = 0; // const/4 v4, 0x0
/* .line 372 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 373 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p2, v0, :cond_0 */
/* .line 374 */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
/* or-int/2addr v1, v0 */
/* iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
/* .line 375 */
} // :cond_0
int v2 = 2; // const/4 v2, 0x2
/* if-ne p2, v2, :cond_1 */
/* .line 376 */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
/* or-int/2addr v1, v2 */
/* iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
/* .line 377 */
} // :cond_1
/* if-ne p2, v1, :cond_2 */
/* .line 378 */
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
/* or-int/lit8 v1, v1, 0x4 */
/* iput v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
/* .line 380 */
} // :cond_2
} // :goto_0
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
int v2 = 7; // const/4 v2, 0x7
/* and-int/2addr v1, v2 */
/* if-ne v1, v2, :cond_4 */
/* .line 381 */
v1 = this.am;
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 382 */
v2 = this.mAlarmLoop;
(( android.app.AlarmManager ) v1 ).cancel ( v2 ); // invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/AlarmManager$OnAlarmListener;)V
/* .line 383 */
} // :cond_3
/* iput v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mLoopCount:I */
/* .line 384 */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
final String v2 = "All monitors registered, about to loop once"; // const-string v2, "All monitors registered, about to loop once"
android.util.Slog .i ( v1,v2 );
/* .line 386 */
} // :cond_4
/* .line 372 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void removePendingBroadcast ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 2585 */
v0 = this.mCachedBCList;
/* monitor-enter v0 */
/* .line 2586 */
try { // :try_start_0
v1 = this.mCachedBCList;
java.lang.Integer .valueOf ( p1 );
/* .line 2587 */
/* monitor-exit v0 */
/* .line 2588 */
return;
/* .line 2587 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void removetLaunchAppListDelay ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 1960 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$10; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$10;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V */
/* const-wide/16 v2, 0x3e8 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 1967 */
return;
} // .end method
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 18 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .line 600 */
/* move-object/from16 v1, p0 */
/* move/from16 v9, p1 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 601 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* move/from16 v3, p1 */
/* move/from16 v4, p2 */
/* move/from16 v5, p3 */
/* move/from16 v6, p4 */
/* move-wide/from16 v7, p5 */
/* invoke-interface/range {v2 ..v8}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportBinderState(IIIIJ)V */
/* .line 602 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long v10, v2, p5 */
/* .line 603 */
/* .local v10, "delay":J */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive binder state: uid="; // const-string v2, "Receive binder state: uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " pid="; // const-string v2, " pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v12, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " tid="; // const-string v2, " tid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v13, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " delay="; // const-string v2, " delay="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v10, v11 ); // invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "ms binderState="; // const-string v2, "ms binderState="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 605 */
/* invoke-static/range {p4 ..p4}, Lcom/miui/server/greeze/GreezeManagerService;->stateToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 606 */
/* .local v14, "msg":Ljava/lang/String; */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
android.util.Slog .i ( v0,v14 );
/* .line 607 */
} // :cond_0
/* const-wide/16 v2, 0x32 */
/* cmp-long v0, v10, v2 */
/* if-lez v0, :cond_1 */
/* const-wide/16 v2, 0x2710 */
/* cmp-long v0, v10, v2 */
/* if-gez v0, :cond_1 */
/* .line 608 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow Greezer: "; // const-string v3, "Slow Greezer: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v14 ); // invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 610 */
} // :cond_1
int v2 = -1; // const/4 v2, -0x1
/* .line 611 */
/* .local v2, "owner":I */
int v3 = 0; // const/4 v3, 0x0
/* .line 612 */
/* .local v3, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
v4 = this.mFrozenPids;
/* monitor-enter v4 */
/* .line 613 */
try { // :try_start_0
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_2 */
/* move-object v15, v0 */
/* .line 614 */
} // .end local v3 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* .local v15, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* if-nez v15, :cond_3 */
/* .line 615 */
try { // :try_start_1
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "reportBinderState null uid = "; // const-string v5, "reportBinderState null uid = "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v3 );
/* .line 616 */
} // :cond_2
/* monitor-exit v4 */
return;
/* .line 619 */
} // :cond_3
v0 = (( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v15 ).getOwner ( ); // invoke-virtual {v15}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move/from16 v16, v0 */
/* .line 620 */
} // .end local v2 # "owner":I
/* .local v16, "owner":I */
try { // :try_start_2
/* monitor-exit v4 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 621 */
/* iget v0, v15, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->state:I */
/* and-int/lit8 v0, v0, 0x2 */
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_4
/* move v0, v2 */
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* move/from16 v17, v0 */
/* .line 622 */
/* .local v17, "state":Z */
/* move/from16 v7, p4 */
/* if-ne v7, v2, :cond_7 */
/* .line 623 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 624 */
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isRunningLaunchMode ( ); // invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
if ( v0 != null) { // if-eqz v0, :cond_6
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isSystemOrMiuiImportantApp ( v9 ); // invoke-virtual {v0, v9}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 625 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 626 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "addLaunchModeQiutList uid = "; // const-string v4, "addLaunchModeQiutList uid = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "reason = check binder"; // const-string v4, "reason = check binder"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v3 );
/* .line 627 */
} // :cond_5
v0 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).addLaunchModeQiutList ( v9 ); // invoke-virtual {v0, v9}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V
/* .line 629 */
} // :cond_6
/* const/16 v0, 0x3e8 */
final String v3 = "Check Binder"; // const-string v3, "Check Binder"
(( com.miui.server.greeze.GreezeManagerService ) v1 ).thawUid ( v9, v0, v3 ); // invoke-virtual {v1, v9, v0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 635 */
} // :cond_7
} // :goto_1
try { // :try_start_3
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
java.lang.Integer .valueOf ( v2 );
v0 = (( java.util.HashMap ) v0 ).containsKey ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_8
if ( v17 != null) { // if-eqz v17, :cond_8
/* .line 636 */
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
java.lang.Integer .valueOf ( v2 );
(( java.util.HashMap ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lmiui/greeze/IGreezeCallback; */
/* move/from16 v3, p1 */
/* move/from16 v4, p2 */
/* move/from16 v5, p3 */
/* move/from16 v6, p4 */
/* move-wide/from16 v7, p5 */
/* invoke-interface/range {v2 ..v8}, Lmiui/greeze/IGreezeCallback;->reportBinderState(IIIIJ)V */
/* :try_end_3 */
/* .catch Landroid/os/RemoteException; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 639 */
} // :cond_8
/* .line 638 */
/* :catch_0 */
/* move-exception v0 */
/* .line 640 */
} // :goto_2
return;
/* .line 620 */
} // .end local v17 # "state":Z
/* :catchall_0 */
/* move-exception v0 */
/* move-object v3, v15 */
/* move/from16 v2, v16 */
} // .end local v16 # "owner":I
/* .restart local v2 # "owner":I */
/* :catchall_1 */
/* move-exception v0 */
/* move-object v3, v15 */
} // .end local v15 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* .restart local v3 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* :catchall_2 */
/* move-exception v0 */
} // :goto_3
try { // :try_start_4
/* monitor-exit v4 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_2 */
/* throw v0 */
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Integer p7 ) {
/* .locals 23 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 539 */
/* move-object/from16 v1, p0 */
/* move/from16 v13, p1 */
/* move/from16 v14, p4 */
/* move/from16 v15, p6 */
/* move/from16 v11, p9 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 540 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* int-to-long v9, v11 */
/* move/from16 v3, p1 */
/* move/from16 v4, p2 */
/* move/from16 v5, p3 */
/* move/from16 v6, p4 */
/* move/from16 v7, p5 */
/* move/from16 v8, p6 */
/* move-wide/from16 v16, v9 */
/* move-wide/from16 v9, p7 */
/* move-wide/from16 v11, v16 */
/* invoke-interface/range {v2 ..v12}, Lcom/miui/app/smartpower/SmartPowerServiceInternal;->reportBinderTrans(IIIIIZJJ)V */
/* .line 542 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long v11, v2, p7 */
/* .line 543 */
/* .local v11, "delay":J */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Receive frozen binder trans: dstUid="; // const-string v2, "Receive frozen binder trans: dstUid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " dstPid="; // const-string v2, " dstPid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v9, p2 */
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " callerUid="; // const-string v2, " callerUid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v10, p3 */
(( java.lang.StringBuilder ) v0 ).append ( v10 ); // invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " callerPid="; // const-string v2, " callerPid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " callerTid="; // const-string v2, " callerTid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v8, p5 */
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " delay="; // const-string v2, " delay="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v11, v12 ); // invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "ms oneway="; // const-string v2, "ms oneway="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 551 */
/* .local v7, "msg":Ljava/lang/String; */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
android.util.Slog .i ( v0,v7 );
/* .line 552 */
} // :cond_0
/* const-wide/16 v2, 0x32 */
/* cmp-long v0, v11, v2 */
/* if-lez v0, :cond_1 */
/* const-wide/16 v2, 0x2710 */
/* cmp-long v0, v11, v2 */
/* if-gez v0, :cond_1 */
/* .line 553 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Slow Greezer: "; // const-string v3, "Slow Greezer: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 555 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* .line 556 */
/* .local v2, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
int v3 = -1; // const/4 v3, -0x1
/* .line 557 */
/* .local v3, "owner":I */
v4 = this.mFrozenPids;
/* monitor-enter v4 */
/* .line 558 */
try { // :try_start_0
/* invoke-direct/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_3 */
/* move-object v6, v0 */
/* .line 559 */
} // .end local v2 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* .local v6, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* if-nez v6, :cond_3 */
/* .line 560 */
try { // :try_start_1
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "reportBinderTrans null uid = "; // const-string v5, "reportBinderTrans null uid = "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v13 ); // invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 561 */
} // :cond_2
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
return;
/* .line 564 */
/* :catchall_0 */
/* move-exception v0 */
/* move-object v2, v6 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
/* goto/16 :goto_3 */
/* .line 563 */
} // :cond_3
try { // :try_start_2
v0 = (( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v6 ).getOwner ( ); // invoke-virtual {v6}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_2 */
/* move v5, v0 */
/* .line 564 */
} // .end local v3 # "owner":I
/* .local v5, "owner":I */
try { // :try_start_3
/* monitor-exit v4 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 566 */
/* iget v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mSystemUiPid:I */
/* const/16 v2, 0x3e8 */
/* if-ne v14, v0, :cond_5 */
/* iget-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->isBarExpand:Z */
/* if-nez v0, :cond_4 */
/* iget-boolean v0, v1, Lcom/miui/server/greeze/GreezeManagerService;->mFsgNavBar:Z */
/* if-nez v0, :cond_5 */
/* .line 567 */
} // :cond_4
/* iget v0, v6, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
final String v3 = "UI bar"; // const-string v3, "UI bar"
(( com.miui.server.greeze.GreezeManagerService ) v1 ).thawUid ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 568 */
return;
/* .line 571 */
} // :cond_5
/* move/from16 v4, p9 */
/* if-nez v4, :cond_6 */
/* .line 572 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 573 */
/* iget v0, v6, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
final String v3 = "BF"; // const-string v3, "BF"
(( com.miui.server.greeze.GreezeManagerService ) v1 ).thawUid ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 574 */
return;
/* .line 578 */
} // :cond_6
/* if-nez v15, :cond_7 */
/* .line 579 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 580 */
/* iget v0, v6, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
final String v3 = "Sync Binder"; // const-string v3, "Sync Binder"
(( com.miui.server.greeze.GreezeManagerService ) v1 ).thawUid ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 581 */
return;
/* .line 585 */
} // :cond_7
v0 = com.miui.server.greeze.GreezeManagerService.callbacks;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
} // :goto_0
v0 = /* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->hasNext()Z */
if ( v0 != null) { // if-eqz v0, :cond_9
/* invoke-interface/range {v16 ..v16}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* move-object/from16 v17, v0 */
/* check-cast v17, Ljava/util/Map$Entry; */
/* .line 587 */
/* .local v17, "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lmiui/greeze/IGreezeCallback;>;" */
try { // :try_start_4
/* invoke-interface/range {v17 ..v17}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object; */
/* check-cast v0, Ljava/lang/Integer; */
v0 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* if-ne v0, v5, :cond_8 */
/* .line 588 */
/* invoke-interface/range {v17 ..v17}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object; */
/* move-object v2, v0 */
/* check-cast v2, Lmiui/greeze/IGreezeCallback; */
/* :try_end_4 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_1 */
/* int-to-long v0, v4 */
/* move/from16 v3, p1 */
/* move/from16 v4, p2 */
/* move/from16 v18, v5 */
} // .end local v5 # "owner":I
/* .local v18, "owner":I */
/* move/from16 v5, p3 */
/* move-object/from16 v19, v6 */
} // .end local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
/* .local v19, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* move/from16 v6, p4 */
/* move-object/from16 v20, v7 */
} // .end local v7 # "msg":Ljava/lang/String;
/* .local v20, "msg":Ljava/lang/String; */
/* move/from16 v7, p5 */
/* move/from16 v8, p6 */
/* move-wide/from16 v9, p7 */
/* move-wide/from16 v21, v11 */
} // .end local v11 # "delay":J
/* .local v21, "delay":J */
/* move-wide v11, v0 */
try { // :try_start_5
/* invoke-interface/range {v2 ..v12}, Lmiui/greeze/IGreezeCallback;->reportBinderTrans(IIIIIZJJ)V */
/* :try_end_5 */
/* .catch Landroid/os/RemoteException; {:try_start_5 ..:try_end_5} :catch_0 */
/* .line 590 */
/* .line 592 */
/* :catch_0 */
/* move-exception v0 */
/* .line 594 */
} // .end local v18 # "owner":I
} // .end local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v20 # "msg":Ljava/lang/String;
} // .end local v21 # "delay":J
/* .restart local v5 # "owner":I */
/* .restart local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v7 # "msg":Ljava/lang/String; */
/* .restart local v11 # "delay":J */
} // :cond_8
/* move/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
} // .end local v5 # "owner":I
} // .end local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v7 # "msg":Ljava/lang/String;
} // .end local v11 # "delay":J
/* .restart local v18 # "owner":I */
/* .restart local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v20 # "msg":Ljava/lang/String; */
/* .restart local v21 # "delay":J */
/* .line 592 */
} // .end local v18 # "owner":I
} // .end local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v20 # "msg":Ljava/lang/String;
} // .end local v21 # "delay":J
/* .restart local v5 # "owner":I */
/* .restart local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v7 # "msg":Ljava/lang/String; */
/* .restart local v11 # "delay":J */
/* :catch_1 */
/* move-exception v0 */
/* move/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
/* .line 595 */
} // .end local v5 # "owner":I
} // .end local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v7 # "msg":Ljava/lang/String;
} // .end local v11 # "delay":J
} // .end local v17 # "item":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lmiui/greeze/IGreezeCallback;>;"
/* .restart local v18 # "owner":I */
/* .restart local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v20 # "msg":Ljava/lang/String; */
/* .restart local v21 # "delay":J */
} // :goto_1
/* move-object/from16 v1, p0 */
/* move/from16 v9, p2 */
/* move/from16 v10, p3 */
/* move/from16 v8, p5 */
/* move/from16 v4, p9 */
/* move/from16 v5, v18 */
/* move-object/from16 v6, v19 */
/* move-object/from16 v7, v20 */
/* move-wide/from16 v11, v21 */
/* .line 585 */
} // .end local v18 # "owner":I
} // .end local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v20 # "msg":Ljava/lang/String;
} // .end local v21 # "delay":J
/* .restart local v5 # "owner":I */
/* .restart local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v7 # "msg":Ljava/lang/String; */
/* .restart local v11 # "delay":J */
} // :cond_9
/* move/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
/* .line 596 */
} // .end local v5 # "owner":I
} // .end local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v7 # "msg":Ljava/lang/String;
} // .end local v11 # "delay":J
/* .restart local v18 # "owner":I */
/* .restart local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v20 # "msg":Ljava/lang/String; */
/* .restart local v21 # "delay":J */
} // :goto_2
return;
/* .line 564 */
} // .end local v18 # "owner":I
} // .end local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v20 # "msg":Ljava/lang/String;
} // .end local v21 # "delay":J
/* .restart local v5 # "owner":I */
/* .restart local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v7 # "msg":Ljava/lang/String; */
/* .restart local v11 # "delay":J */
/* :catchall_1 */
/* move-exception v0 */
/* move/from16 v18, v5 */
/* move-object/from16 v19, v6 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
/* move/from16 v3, v18 */
/* move-object/from16 v2, v19 */
} // .end local v5 # "owner":I
} // .end local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v7 # "msg":Ljava/lang/String;
} // .end local v11 # "delay":J
/* .restart local v18 # "owner":I */
/* .restart local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v20 # "msg":Ljava/lang/String; */
/* .restart local v21 # "delay":J */
} // .end local v18 # "owner":I
} // .end local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v20 # "msg":Ljava/lang/String;
} // .end local v21 # "delay":J
/* .restart local v3 # "owner":I */
/* .restart local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v7 # "msg":Ljava/lang/String; */
/* .restart local v11 # "delay":J */
/* :catchall_2 */
/* move-exception v0 */
/* move-object/from16 v19, v6 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
/* move-object/from16 v2, v19 */
} // .end local v6 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v7 # "msg":Ljava/lang/String;
} // .end local v11 # "delay":J
/* .restart local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v20 # "msg":Ljava/lang/String; */
/* .restart local v21 # "delay":J */
} // .end local v19 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v20 # "msg":Ljava/lang/String;
} // .end local v21 # "delay":J
/* .restart local v2 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .restart local v7 # "msg":Ljava/lang/String; */
/* .restart local v11 # "delay":J */
/* :catchall_3 */
/* move-exception v0 */
/* move-object/from16 v20, v7 */
/* move-wide/from16 v21, v11 */
} // .end local v7 # "msg":Ljava/lang/String;
} // .end local v11 # "delay":J
/* .restart local v20 # "msg":Ljava/lang/String; */
/* .restart local v21 # "delay":J */
} // :goto_3
try { // :try_start_6
/* monitor-exit v4 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_4 */
/* throw v0 */
/* :catchall_4 */
/* move-exception v0 */
} // .end method
public void reportLoopOnce ( ) {
/* .locals 5 */
/* .line 645 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 646 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
int v1 = 1; // const/4 v1, 0x1
/* .line 647 */
final String v0 = "Receive millet loop once msg"; // const-string v0, "Receive millet loop once msg"
/* .line 648 */
/* .local v0, "msg":Ljava/lang/String; */
/* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z */
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
if ( v2 != null) { // if-eqz v2, :cond_0
android.util.Slog .i ( v3,v0 );
/* .line 649 */
} // :cond_0
/* iget v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mRegisteredMonitor:I */
int v4 = 7; // const/4 v4, 0x7
/* and-int/2addr v2, v4 */
/* if-ne v2, v4, :cond_3 */
/* .line 650 */
/* iget-boolean v2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInitCtsStatused:Z */
/* if-nez v2, :cond_1 */
/* .line 651 */
v2 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).initCtsStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->initCtsStatus()V
/* .line 652 */
/* iput-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInitCtsStatused:Z */
/* .line 654 */
} // :cond_1
com.miui.server.greeze.GreezeManagerDebugConfig.milletEnable = (v1!= 0);
/* .line 655 */
final String v1 = "Receive millet loop once, gz begin to work"; // const-string v1, "Receive millet loop once, gz begin to work"
android.util.Slog .i ( v3,v1 );
/* .line 656 */
v1 = com.miui.server.greeze.GreezeManagerService.callbacks;
(( java.util.HashMap ) v1 ).values ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lmiui/greeze/IGreezeCallback; */
/* .line 658 */
/* .local v2, "callback":Lmiui/greeze/IGreezeCallback; */
try { // :try_start_0
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 661 */
/* .line 659 */
/* :catch_0 */
/* move-exception v3 */
/* .line 662 */
} // .end local v2 # "callback":Lmiui/greeze/IGreezeCallback;
} // :goto_1
} // :cond_2
/* .line 664 */
} // :cond_3
final String v1 = "Receive millet loop once, but monitor not ready"; // const-string v1, "Receive millet loop once, but monitor not ready"
android.util.Slog .i ( v3,v1 );
/* .line 666 */
} // :goto_2
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .line 492 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 493 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* .line 494 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p2 */
/* .line 495 */
/* .local v0, "delay":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Receive frozen pkg net: uid="; // const-string v3, "Receive frozen pkg net: uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " delay="; // const-string v3, " delay="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms"; // const-string v3, "ms"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 496 */
/* .local v2, "msg":Ljava/lang/String; */
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
android.util.Slog .i ( v3,v2 );
/* .line 497 */
} // :cond_0
/* const-wide/16 v3, 0x32 */
/* cmp-long v3, v0, v3 */
/* if-lez v3, :cond_1 */
/* const-wide/16 v3, 0x2710 */
/* cmp-long v3, v0, v3 */
/* if-gez v3, :cond_1 */
/* .line 498 */
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Slow Greezer: "; // const-string v5, "Slow Greezer: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 500 */
} // :cond_1
int v3 = -1; // const/4 v3, -0x1
/* .line 501 */
/* .local v3, "owner":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 502 */
/* .local v4, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
v5 = this.mFrozenPids;
/* monitor-enter v5 */
/* .line 503 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenInfo(I)Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v4, v6 */
/* .line 504 */
/* if-nez v4, :cond_4 */
/* .line 506 */
try { // :try_start_1
/* iget v6, p0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
/* if-ne v6, p1, :cond_2 */
/* .line 507 */
v6 = com.miui.server.greeze.GreezeManagerService.callbacks;
int v7 = 1; // const/4 v7, 0x1
java.lang.Integer .valueOf ( v7 );
(( java.util.HashMap ) v6 ).get ( v7 ); // invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v6, Lmiui/greeze/IGreezeCallback; */
/* :try_end_1 */
/* .catch Landroid/os/RemoteException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 508 */
/* :catch_0 */
/* move-exception v6 */
} // :cond_2
} // :goto_0
/* nop */
/* .line 509 */
try { // :try_start_2
/* sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v6 != null) { // if-eqz v6, :cond_3
final String v6 = "GreezeManager"; // const-string v6, "GreezeManager"
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "reportNet null uid = "; // const-string v8, "reportNet null uid = "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( p1 ); // invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v6,v7 );
/* .line 510 */
} // :cond_3
/* monitor-exit v5 */
return;
/* .line 512 */
} // :cond_4
v6 = (( com.miui.server.greeze.GreezeManagerService$FrozenInfo ) v4 ).getOwner ( ); // invoke-virtual {v4}, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->getOwner()I
/* move v3, v6 */
/* .line 513 */
/* monitor-exit v5 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 515 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 516 */
/* iget-boolean v5, v4, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 517 */
v5 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v5 ).addLaunchModeQiutList ( p1 ); // invoke-virtual {v5, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(I)V
/* .line 519 */
} // :cond_5
/* const/16 v5, 0x3e8 */
final String v6 = "PACKET"; // const-string v6, "PACKET"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUid ( p1, v5, v6 ); // invoke-virtual {p0, p1, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 533 */
} // :cond_6
} // :goto_1
return;
/* .line 513 */
/* :catchall_0 */
/* move-exception v6 */
try { // :try_start_3
/* monitor-exit v5 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* throw v6 */
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .line 421 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 422 */
/* const-class v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* .line 423 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* sub-long/2addr v0, p3 */
/* .line 424 */
/* .local v0, "delay":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Receive frozen signal: uid="; // const-string v3, "Receive frozen signal: uid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " pid="; // const-string v3, " pid="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " delay="; // const-string v3, " delay="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v3 = "ms"; // const-string v3, "ms"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 426 */
/* .local v2, "msg":Ljava/lang/String; */
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z */
final String v4 = "GreezeManager"; // const-string v4, "GreezeManager"
if ( v3 != null) { // if-eqz v3, :cond_0
android.util.Slog .i ( v4,v2 );
/* .line 427 */
} // :cond_0
/* const-wide/16 v5, 0x32 */
/* cmp-long v3, v0, v5 */
/* if-lez v3, :cond_1 */
/* const-wide/16 v5, 0x2710 */
/* cmp-long v3, v0, v5 */
/* if-gez v3, :cond_1 */
/* .line 428 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Slow Greezer: "; // const-string v5, "Slow Greezer: "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v3 );
/* .line 430 */
} // :cond_1
v3 = this.mHandler;
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$3; */
/* invoke-direct {v4, p0, p1, p2}, Lcom/miui/server/greeze/GreezeManagerService$3;-><init>(Lcom/miui/server/greeze/GreezeManagerService;II)V */
/* const-wide/16 v5, 0xc8 */
(( android.os.Handler ) v3 ).postDelayed ( v4, v5, v6 ); // invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 488 */
return;
} // .end method
public void resetCgroupUidStatus ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 1217 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 1218 */
} // :cond_0
(( com.miui.server.greeze.GreezeManagerService ) p0 ).readPidFromCgroup ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->readPidFromCgroup(I)Ljava/util/List;
/* .line 1219 */
/* .local v0, "pidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1220 */
/* .local v1, "log":Ljava/lang/StringBuilder; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Reset uid = "; // const-string v3, "Reset uid = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " pid = [ "; // const-string v3, " pid = [ "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1221 */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 1222 */
/* .local v3, "pid":I */
com.miui.server.greeze.FreezeUtils .thawPid ( v3,p1 );
/* .line 1223 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1224 */
} // .end local v3 # "pid":I
/* .line 1225 */
} // :cond_1
final String v2 = "]"; // const-string v2, "]"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1226 */
final String v2 = "GreezeManager"; // const-string v2, "GreezeManager"
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1227 */
return;
} // .end method
public void resetStatusForImmobulus ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "type" # I */
/* .line 1297 */
v0 = this.mFrozenPids;
/* monitor-enter v0 */
/* .line 1298 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mFrozenPids;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_2 */
/* .line 1299 */
v2 = this.mFrozenPids;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1300 */
/* .local v2, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* and-int/lit8 v3, p1, 0x8 */
int v4 = 0; // const/4 v4, 0x0
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1301 */
/* iget-boolean v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1302 */
/* iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z */
/* .line 1304 */
} // :cond_0
/* and-int/lit8 v3, p1, 0x10 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1305 */
/* iget-boolean v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1306 */
/* iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
/* .line 1298 */
} // .end local v2 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1310 */
} // .end local v1 # "i":I
} // :cond_2
/* monitor-exit v0 */
/* .line 1311 */
return;
/* .line 1310 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void sendPendingAlarmForAurogon ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 2120 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$12; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$12;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V */
/* const-wide/16 v2, 0x1f4 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 2135 */
return;
} // .end method
public void sendPendingBroadcastForAurogon ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 2559 */
v0 = this.mCachedBCList;
/* monitor-enter v0 */
/* .line 2560 */
try { // :try_start_0
v1 = this.mCachedBCList;
java.lang.Integer .valueOf ( p1 );
/* check-cast v1, Ljava/util/List; */
/* .line 2561 */
/* .local v1, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;" */
v2 = if ( v1 != null) { // if-eqz v1, :cond_6
/* if-nez v2, :cond_0 */
/* .line 2562 */
} // :cond_0
} // :cond_1
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Landroid/content/Intent; */
/* .line 2563 */
/* .local v3, "intent":Landroid/content/Intent; */
/* iget-boolean v4, p0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 2564 */
final String v4 = "android.intent.action.SCREEN_OFF"; // const-string v4, "android.intent.action.SCREEN_OFF"
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 2565 */
/* .line 2567 */
} // :cond_2
final String v4 = "android.intent.action.SCREEN_ON"; // const-string v4, "android.intent.action.SCREEN_ON"
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 2568 */
/* .line 2570 */
} // :cond_3
final String v4 = "android.intent.action.PACKAGE_REPLACED"; // const-string v4, "android.intent.action.PACKAGE_REPLACED"
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_1 */
final String v4 = "android.intent.action.PACKAGE_REMOVED"; // const-string v4, "android.intent.action.PACKAGE_REMOVED"
/* .line 2571 */
(( android.content.Intent ) v3 ).getAction ( ); // invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v4 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 2572 */
/* .line 2574 */
} // :cond_4
v4 = this.mHandler;
/* new-instance v5, Lcom/miui/server/greeze/GreezeManagerService$17; */
/* invoke-direct {v5, p0, v3}, Lcom/miui/server/greeze/GreezeManagerService$17;-><init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/content/Intent;)V */
(( android.os.Handler ) v4 ).post ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2579 */
/* nop */
} // .end local v3 # "intent":Landroid/content/Intent;
/* .line 2580 */
} // :cond_5
v2 = this.mCachedBCList;
java.lang.Integer .valueOf ( p1 );
/* .line 2581 */
/* nop */
} // .end local v1 # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
/* monitor-exit v0 */
/* .line 2582 */
return;
/* .line 2561 */
/* .restart local v1 # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;" */
} // :cond_6
} // :goto_1
/* monitor-exit v0 */
return;
/* .line 2581 */
} // .end local v1 # "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setBroadcastCtrl ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "val" # Z */
/* .line 2320 */
/* iput-boolean p1, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBroadcastCtrlCloud:Z */
/* .line 2321 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "setBroadcastCtrl " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
android.util.Slog .d ( v1,v0 );
/* .line 2322 */
return;
} // .end method
public java.util.List thawAll ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "module" # I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1622 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 1623 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "AIDL thawAll("; // const-string v1, "AIDL thawAll("
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ")"; // const-string v1, ")"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
android.util.Slog .d ( v1,v0 );
/* .line 1625 */
} // :cond_0
/* const/16 v0, 0x270f */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getFrozenUids ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "-thawAll from "; // const-string v2, "-thawAll from "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUids ( v0, p2, v1 ); // invoke-virtual {p0, v0, p2, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
} // .end method
public Boolean thawAll ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1592 */
/* const/16 v0, 0x270f */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).getFrozenUids ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "-thawAll"; // const-string v2, "-thawAll"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* const/16 v2, 0x3e8 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUids ( v0, v2, v1 ); // invoke-virtual {p0, v0, v2, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 1593 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 1594 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1596 */
com.miui.server.greeze.FreezeUtils .getFrozonTids ( );
/* .line 1597 */
/* .local v0, "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 1598 */
/* .local v3, "tid":I */
com.miui.server.greeze.FreezeUtils .thawTid ( v3 );
/* .line 1599 */
} // .end local v3 # "tid":I
/* .line 1600 */
} // :cond_0
v2 = com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
/* if-nez v2, :cond_1 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_1
/* .line 1602 */
} // .end local v0 # "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_2
} // .end method
public java.util.List thawPids ( Integer[] p0, Integer p1, java.lang.String p2 ) {
/* .locals 8 */
/* .param p1, "pids" # [I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1459 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 1460 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
final String v2 = ", "; // const-string v2, ", "
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "AIDL thawPids("; // const-string v3, "AIDL thawPids("
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1462 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1463 */
/* .local v0, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* array-length v3, p1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_3 */
/* aget v5, p1, v4 */
/* .line 1464 */
/* .local v5, "pid":I */
v6 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).thawProcess ( v5, p2, p3 ); // invoke-virtual {p0, v5, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawProcess(IILjava/lang/String;)Z
/* if-nez v6, :cond_1 */
/* .line 1465 */
/* sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "AIDL thawPid("; // const-string v7, "AIDL thawPid("
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p3 ); // invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ") failed"; // const-string v7, ") failed"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v6 );
/* .line 1468 */
} // :cond_1
java.lang.Integer .valueOf ( v5 );
/* .line 1463 */
} // .end local v5 # "pid":I
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1471 */
} // :cond_3
} // .end method
public Boolean thawProcess ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 8 */
/* .param p1, "pid" # I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 1429 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1430 */
/* .local v0, "done":Z */
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 1431 */
try { // :try_start_0
v2 = this.mFrozenPids;
(( android.util.SparseArray ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1432 */
/* .local v2, "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* if-nez v2, :cond_1 */
/* .line 1433 */
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "GreezeManager"; // const-string v3, "GreezeManager"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Thawing a non-frozen process (pid="; // const-string v5, "Thawing a non-frozen process (pid="
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "), won\'t add into history, reason "; // const-string v5, "), won\'t add into history, reason "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p3 ); // invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 1434 */
} // :cond_0
/* monitor-exit v1 */
/* .line 1437 */
} // :cond_1
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 1438 */
v3 = com.miui.server.greeze.FreezeUtils .thawPid ( p1 );
/* move v0, v3 */
/* .line 1440 */
} // :cond_2
/* iget v3, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
v3 = com.miui.server.greeze.FreezeUtils .thawPid ( p1,v3 );
/* move v0, v3 */
/* .line 1441 */
} // :goto_0
com.miui.server.greeze.GreezeServiceUtils .getGameUids ( );
/* .line 1442 */
/* .local v3, "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* iget v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
v4 = java.lang.Integer .valueOf ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 1443 */
v4 = this.mHandler;
/* iget v5, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* const/16 v6, 0x8 */
int v7 = 0; // const/4 v7, 0x0
(( android.os.Handler ) v4 ).obtainMessage ( v6, v7, v5 ); // invoke-virtual {v4, v6, v7, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
(( android.os.Handler ) v4 ).sendMessage ( v5 ); // invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 1444 */
} // :cond_3
/* const-class v4, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
com.android.server.LocalServices .getService ( v4 );
/* check-cast v4, Lcom/miui/app/smartpower/SmartPowerServiceInternal; */
/* iget v5, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* .line 1445 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawTime:J */
/* .line 1446 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v4 */
/* iput-wide v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->mThawUptime:J */
/* .line 1447 */
this.mThawReason = p3;
/* .line 1448 */
v4 = this.mFrozenPids;
(( android.util.SparseArray ) v4 ).remove ( p1 ); // invoke-virtual {v4, p1}, Landroid/util/SparseArray;->remove(I)V
/* .line 1449 */
/* invoke-direct {p0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->addHistoryInfo(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;)V */
/* .line 1450 */
v4 = this.mHandler;
int v5 = 1; // const/4 v5, 0x1
v4 = (( android.os.Handler ) v4 ).hasMessages ( v5, v2 ); // invoke-virtual {v4, v5, v2}, Landroid/os/Handler;->hasMessages(ILjava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1451 */
v4 = this.mHandler;
(( android.os.Handler ) v4 ).removeMessages ( v5, v2 ); // invoke-virtual {v4, v5, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
/* .line 1453 */
} // .end local v2 # "info":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // .end local v3 # "gameUids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // :cond_4
/* monitor-exit v1 */
/* .line 1454 */
/* .line 1453 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void thawThread ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "tid" # I */
/* .line 1361 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1362 */
com.miui.server.greeze.FreezeUtils .thawTid ( p1 );
/* .line 1363 */
} // :cond_0
return;
} // .end method
public Boolean thawUid ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 17 */
/* .param p1, "uid" # I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 1476 */
/* move-object/from16 v1, p0 */
/* move/from16 v2, p1 */
/* move/from16 v3, p2 */
/* move-object/from16 v4, p3 */
v0 = this.mImmobulusMode;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
int v5 = 0; // const/4 v5, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1477 */
final String v0 = "alarm"; // const-string v0, "alarm"
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1478 */
/* .line 1482 */
} // :cond_0
int v6 = 1; // const/4 v6, 0x1
/* .line 1483 */
/* .local v6, "allDone":Z */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v7, v0 */
/* .line 1484 */
/* .local v7, "toThaw":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;" */
v8 = this.mAurogonLock;
/* monitor-enter v8 */
/* .line 1485 */
try { // :try_start_0
v9 = this.mFrozenPids;
/* monitor-enter v9 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1486 */
try { // :try_start_1
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 1487 */
/* .local v0, "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
int v10 = 0; // const/4 v10, 0x0
/* .local v10, "i":I */
} // :goto_0
v11 = this.mFrozenPids;
v11 = (( android.util.SparseArray ) v11 ).size ( ); // invoke-virtual {v11}, Landroid/util/SparseArray;->size()I
/* if-ge v10, v11, :cond_2 */
/* .line 1488 */
v11 = this.mFrozenPids;
(( android.util.SparseArray ) v11 ).valueAt ( v10 ); // invoke-virtual {v11, v10}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v11, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1489 */
/* .local v11, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* iget v12, v11, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
/* if-ne v12, v2, :cond_1 */
/* .line 1490 */
/* .line 1491 */
/* iget v12, v11, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
java.lang.Integer .valueOf ( v12 );
(( java.util.HashSet ) v0 ).add ( v12 ); // invoke-virtual {v0, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
/* .line 1487 */
} // .end local v11 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_1
/* add-int/lit8 v10, v10, 0x1 */
/* .line 1495 */
} // .end local v10 # "i":I
v10 = } // :cond_2
int v11 = 1; // const/4 v11, 0x1
/* if-nez v10, :cond_3 */
/* .line 1496 */
/* monitor-exit v9 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
try { // :try_start_2
/* monitor-exit v8 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 1499 */
} // :cond_3
try { // :try_start_3
/* sget-boolean v10, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 1500 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenNewPids()Ljava/util/List; */
/* .line 1501 */
/* .local v10, "curPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
/* .line 1502 */
/* .local v12, "allPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .line 1503 */
v14 = } // :goto_1
if ( v14 != null) { // if-eqz v14, :cond_5
/* check-cast v14, Ljava/lang/Integer; */
/* .line 1504 */
/* .local v14, "item":Ljava/lang/Integer; */
v15 = (( java.lang.Integer ) v14 ).intValue ( ); // invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I
v15 = android.os.Process .getParentPid ( v15 );
/* .line 1505 */
/* .local v15, "ppid":I */
java.lang.Integer .valueOf ( v15 );
v5 = (( java.util.HashSet ) v0 ).contains ( v5 ); // invoke-virtual {v0, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1506 */
v5 = (( java.lang.Integer ) v14 ).intValue ( ); // invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I
com.miui.server.greeze.FreezeUtils .thawPid ( v5 );
/* .line 1507 */
final String v5 = "GreezeManager"; // const-string v5, "GreezeManager"
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object/from16 v16, v0 */
} // .end local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
/* .local v16, "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
final String v0 = "isolated pid: "; // const-string v0, "isolated pid: "
(( java.lang.StringBuilder ) v11 ).append ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v11 = ", ppid: "; // const-string v11, ", ppid: "
(( java.lang.StringBuilder ) v0 ).append ( v11 ); // invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v0 );
/* .line 1505 */
} // .end local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
/* .restart local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
} // :cond_4
/* move-object/from16 v16, v0 */
/* .line 1509 */
} // .end local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
} // .end local v14 # "item":Ljava/lang/Integer;
} // .end local v15 # "ppid":I
/* .restart local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
} // :goto_2
/* move-object/from16 v0, v16 */
int v5 = 0; // const/4 v5, 0x0
int v11 = 1; // const/4 v11, 0x1
/* .line 1503 */
} // .end local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
/* .restart local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
} // :cond_5
/* move-object/from16 v16, v0 */
} // .end local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
/* .restart local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
/* .line 1499 */
} // .end local v10 # "curPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v12 # "allPids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
/* .restart local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
} // :cond_6
/* move-object/from16 v16, v0 */
/* .line 1512 */
} // .end local v0 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
/* .restart local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;" */
} // :goto_3
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1513 */
/* .local v0, "success":Ljava/lang/StringBuilder; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1514 */
/* .local v5, "failed":Ljava/lang/StringBuilder; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 1515 */
/* .local v10, "log":Ljava/lang/StringBuilder; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "THAW uid = "; // const-string v12, "THAW uid = "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v2 ); // invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1516 */
v12 = } // :goto_4
if ( v12 != null) { // if-eqz v12, :cond_8
/* check-cast v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1517 */
/* .local v12, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* iget v13, v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
v13 = (( com.miui.server.greeze.GreezeManagerService ) v1 ).thawProcess ( v13, v3, v4 ); // invoke-virtual {v1, v13, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawProcess(IILjava/lang/String;)Z
/* if-nez v13, :cond_7 */
/* .line 1518 */
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v14, v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = " "; // const-string v14, " "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v13 ); // invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1519 */
int v6 = 0; // const/4 v6, 0x0
/* .line 1521 */
} // :cond_7
/* new-instance v13, Ljava/lang/StringBuilder; */
/* invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v14, v12, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->pid:I */
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v14 = " "; // const-string v14, " "
(( java.lang.StringBuilder ) v13 ).append ( v14 ); // invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v13 ).toString ( ); // invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v13 ); // invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1523 */
} // .end local v12 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :goto_5
/* .line 1524 */
} // :cond_8
int v11 = 1; // const/4 v11, 0x1
(( com.miui.server.greeze.GreezeManagerService ) v1 ).dealAdjSet ( v2, v11 ); // invoke-virtual {v1, v2, v11}, Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V
/* .line 1525 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = " pid = [ "; // const-string v12, " pid = [ "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = "] "; // const-string v12, "] "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1526 */
final String v11 = ""; // const-string v11, ""
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v11 = (( java.lang.String ) v11 ).equals ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v11, :cond_9 */
/* .line 1527 */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = "failed = [ "; // const-string v12, "failed = [ "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = "]"; // const-string v12, "]"
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1529 */
} // :cond_9
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
final String v12 = " reason : "; // const-string v12, " reason : "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " caller : "; // const-string v12, " caller : "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v3 ); // invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1530 */
v11 = this.mImmobulusMode;
v11 = (( com.miui.server.greeze.AurogonImmobulusMode ) v11 ).isModeReason ( v4 ); // invoke-virtual {v11, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z
if ( v11 != null) { // if-eqz v11, :cond_a
/* if-nez v6, :cond_b */
/* .line 1531 */
} // :cond_a
final String v11 = "GreezeManager"; // const-string v11, "GreezeManager"
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v12 );
/* .line 1532 */
v11 = this.mHistoryLog;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( android.util.LocalLog ) v11 ).log ( v12 ); // invoke-virtual {v11, v12}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 1534 */
} // .end local v0 # "success":Ljava/lang/StringBuilder;
} // .end local v5 # "failed":Ljava/lang/StringBuilder;
} // .end local v10 # "log":Ljava/lang/StringBuilder;
} // .end local v16 # "toThawPids":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
} // :cond_b
/* monitor-exit v9 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 1535 */
try { // :try_start_4
/* monitor-exit v8 */
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 1537 */
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isModeReason ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isModeReason(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_d
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isNeedNotifyAppStatus ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isNeedNotifyAppStatus(I)Z
if ( v0 != null) { // if-eqz v0, :cond_c
} // :cond_c
int v0 = 1; // const/4 v0, 0x1
/* .line 1538 */
} // :cond_d
} // :goto_6
int v0 = 1; // const/4 v0, 0x1
/* if-eq v3, v0, :cond_e */
/* .line 1539 */
int v5 = 0; // const/4 v5, 0x0
/* check-cast v8, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
(( com.miui.server.greeze.GreezeManagerService ) v1 ).notifyOtherModule ( v8, v3 ); // invoke-virtual {v1, v8, v3}, Lcom/miui/server/greeze/GreezeManagerService;->notifyOtherModule(Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;I)V
/* .line 1542 */
} // :cond_e
} // :goto_7
/* if-eq v3, v0, :cond_10 */
/* const/16 v0, 0x3e8 */
/* if-ne v3, v0, :cond_f */
} // :cond_f
int v5 = 0; // const/4 v5, 0x0
/* .line 1543 */
} // :cond_10
} // :goto_8
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1544 */
/* .local v0, "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-static/range {p1 ..p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer; */
/* .line 1545 */
int v5 = 0; // const/4 v5, 0x0
/* invoke-direct {v1, v0, v5}, Lcom/miui/server/greeze/GreezeManagerService;->setWakeLockState(Ljava/util/List;Z)V */
/* .line 1547 */
} // .end local v0 # "uids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :goto_9
/* invoke-direct {v1, v2, v5}, Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V */
/* .line 1548 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->sendPendingAlarmForAurogon(I)V */
/* .line 1549 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/GreezeManagerService;->sendPendingBroadcastForAurogon(I)V */
/* .line 1550 */
v0 = this.mHandler;
/* new-instance v5, Lcom/miui/server/greeze/GreezeManagerService$7; */
/* invoke-direct {v5, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService$7;-><init>(Lcom/miui/server/greeze/GreezeManagerService;I)V */
(( android.os.Handler ) v0 ).post ( v5 ); // invoke-virtual {v0, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1560 */
/* .line 1534 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_5
/* monitor-exit v9 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_0 */
} // .end local v6 # "allDone":Z
} // .end local v7 # "toThaw":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;"
} // .end local p0 # "this":Lcom/miui/server/greeze/GreezeManagerService;
} // .end local p1 # "uid":I
} // .end local p2 # "fromWho":I
} // .end local p3 # "reason":Ljava/lang/String;
try { // :try_start_6
/* throw v0 */
/* .line 1535 */
/* .restart local v6 # "allDone":Z */
/* .restart local v7 # "toThaw":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;>;" */
/* .restart local p0 # "this":Lcom/miui/server/greeze/GreezeManagerService; */
/* .restart local p1 # "uid":I */
/* .restart local p2 # "fromWho":I */
/* .restart local p3 # "reason":Ljava/lang/String; */
/* :catchall_1 */
/* move-exception v0 */
/* monitor-exit v8 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_1 */
/* throw v0 */
} // .end method
public void thawUidAsync ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "caller" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .line 2002 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/GreezeManagerService$11; */
/* invoke-direct {v1, p0, p1, p3}, Lcom/miui/server/greeze/GreezeManagerService$11;-><init>(Lcom/miui/server/greeze/GreezeManagerService;ILjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 2009 */
return;
} // .end method
public java.util.List thawUids ( Integer[] p0, Integer p1, java.lang.String p2 ) {
/* .locals 8 */
/* .param p1, "uids" # [I */
/* .param p2, "fromWho" # I */
/* .param p3, "reason" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 1565 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService;->checkPermission()V */
/* .line 1566 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
final String v2 = ", "; // const-string v2, ", "
if ( v0 != null) { // if-eqz v0, :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "AIDL thawUids("; // const-string v3, "AIDL thawUids("
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ")"; // const-string v3, ")"
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 1568 */
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1569 */
/* .local v0, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* array-length v3, p1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_3 */
/* aget v5, p1, v4 */
/* .line 1570 */
/* .local v5, "uid":I */
v6 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUid ( v5, p2, p3 ); // invoke-virtual {p0, v5, p2, p3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* if-nez v6, :cond_1 */
/* .line 1571 */
/* sget-boolean v6, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z */
if ( v6 != null) { // if-eqz v6, :cond_2
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "AIDL thawUid("; // const-string v7, "AIDL thawUid("
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p2 ); // invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( p3 ); // invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = ") failed"; // const-string v7, ") failed"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v6 );
/* .line 1574 */
} // :cond_1
java.lang.Integer .valueOf ( v5 );
/* .line 1569 */
} // .end local v5 # "uid":I
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1577 */
} // :cond_3
} // .end method
public void thawuidsAll ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1606 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 1607 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v1 = this.mFrozenPids;
/* monitor-enter v1 */
/* .line 1608 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
try { // :try_start_0
v3 = this.mFrozenPids;
v3 = (( android.util.SparseArray ) v3 ).size ( ); // invoke-virtual {v3}, Landroid/util/SparseArray;->size()I
/* if-ge v2, v3, :cond_1 */
/* .line 1609 */
v3 = this.mFrozenPids;
(( android.util.SparseArray ) v3 ).valueAt ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1610 */
/* .local v3, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
v4 = java.lang.Integer .valueOf ( v4 );
/* if-nez v4, :cond_0 */
/* .line 1611 */
/* iget v4, v3, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->uid:I */
java.lang.Integer .valueOf ( v4 );
/* .line 1608 */
} // .end local v3 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_0
/* add-int/lit8 v2, v2, 0x1 */
/* .line 1614 */
} // .end local v2 # "i":I
} // :cond_1
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1615 */
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 1616 */
/* .local v2, "uid":I */
/* const/16 v3, 0x3e8 */
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUid ( v2, v3, p1 ); // invoke-virtual {p0, v2, v3, p1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 1617 */
} // .end local v2 # "uid":I
/* .line 1618 */
} // :cond_2
return;
/* .line 1614 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void triggerLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 1318 */
v0 = this.mImmobulusMode;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_7
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_0 */
/* .line 1321 */
} // :cond_0
final String v0 = "com.miui.home"; // const-string v0, "com.miui.home"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1322 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z */
/* if-nez v0, :cond_1 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mInited:Z */
/* .line 1323 */
} // :cond_1
v0 = this.mImmobulusMode;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).isRunningLaunchMode ( ); // invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1324 */
return;
/* .line 1328 */
} // :cond_2
v0 = this.ENABLE_LAUNCH_MODE_DEVICE;
v0 = v1 = android.os.Build.DEVICE;
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
/* if-nez v0, :cond_3 */
/* .line 1329 */
v0 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
v0 = this.mImmobulusMode;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnabledLMCamera:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1330 */
return;
/* .line 1334 */
} // :cond_3
v0 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
v0 = this.mImmobulusMode;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnabledLMCamera:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 1335 */
v0 = this.mImmobulusMode;
final String v1 = "Camera"; // const-string v1, "Camera"
(( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).triggerImmobulusMode ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V
/* .line 1338 */
} // :cond_4
v0 = android.os.UserHandle .isApp ( p2 );
/* if-nez v0, :cond_5 */
/* .line 1339 */
return;
/* .line 1342 */
} // :cond_5
v0 = (( com.miui.server.greeze.GreezeManagerService ) p0 ).isUidFrozen ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 1343 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Thaw uid = "; // const-string v1, "Thaw uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " Activity Start!"; // const-string v1, " Activity Start!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Aurogon"; // const-string v1, "Aurogon"
android.util.Slog .d ( v1,v0 );
/* .line 1344 */
/* const/16 v0, 0x3e8 */
final String v1 = "Activity Start"; // const-string v1, "Activity Start"
(( com.miui.server.greeze.GreezeManagerService ) p0 ).thawUidAsync ( p2, v0, v1 ); // invoke-virtual {p0, p2, v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawUidAsync(IILjava/lang/String;)V
/* .line 1347 */
} // :cond_6
v0 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v0 ).triggerLaunchMode ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerLaunchMode(Ljava/lang/String;I)V
/* .line 1348 */
return;
/* .line 1318 */
} // :cond_7
} // :goto_0
return;
} // .end method
public void updateAurogonUidRule ( Integer p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "allow" # Z */
/* .line 1689 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 1690 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1691 */
return;
/* .line 1693 */
} // :cond_1
try { // :try_start_0
v0 = this.cm;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1695 */
final String v0 = "android.net.ConnectivityManager"; // const-string v0, "android.net.ConnectivityManager"
java.lang.Class .forName ( v0 );
/* .line 1696 */
/* .local v0, "clazz":Ljava/lang/Class; */
/* const-string/jumbo v1, "updateAurogonUidRule" */
int v2 = 2; // const/4 v2, 0x2
/* new-array v3, v2, [Ljava/lang/Class; */
v4 = java.lang.Integer.TYPE;
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
v4 = java.lang.Boolean.TYPE;
int v6 = 1; // const/4 v6, 0x1
/* aput-object v4, v3, v6 */
(( java.lang.Class ) v0 ).getDeclaredMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
/* .line 1697 */
/* .local v1, "method":Ljava/lang/reflect/Method; */
(( java.lang.reflect.Method ) v1 ).setAccessible ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V
/* .line 1698 */
v3 = this.cm;
/* new-array v2, v2, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
/* aput-object v4, v2, v5 */
java.lang.Boolean .valueOf ( p2 );
/* aput-object v4, v2, v6 */
(( java.lang.reflect.Method ) v1 ).invoke ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1702 */
} // .end local v0 # "clazz":Ljava/lang/Class;
} // .end local v1 # "method":Ljava/lang/reflect/Method;
} // :cond_2
/* .line 1700 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1701 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "GreezeManager"; // const-string v1, "GreezeManager"
/* const-string/jumbo v2, "updateAurogonUidRule error " */
android.util.Log .e ( v1,v2,v0 );
/* .line 1703 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public void updateFreeformSmallWinList ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "allow" # Z */
/* .line 1898 */
v0 = this.mFreeformSmallWinList;
/* monitor-enter v0 */
/* .line 1899 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 1900 */
try { // :try_start_0
v1 = v1 = this.mFreeformSmallWinList;
/* if-nez v1, :cond_1 */
/* .line 1901 */
v1 = this.mFreeformSmallWinList;
/* .line 1904 */
} // :cond_0
v1 = this.mFreeformSmallWinList;
/* .line 1906 */
} // :cond_1
} // :goto_0
/* monitor-exit v0 */
/* .line 1907 */
return;
/* .line 1906 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateFrozenInfoForImmobulus ( Integer p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "type" # I */
/* .line 1283 */
v0 = this.mFrozenPids;
/* monitor-enter v0 */
/* .line 1284 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
try { // :try_start_0
v2 = this.mFrozenPids;
v2 = (( android.util.SparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/SparseArray;->size()I
/* if-ge v1, v2, :cond_2 */
/* .line 1285 */
v2 = this.mFrozenPids;
(( android.util.SparseArray ) v2 ).valueAt ( v1 ); // invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* .line 1286 */
/* .local v2, "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo; */
/* and-int/lit8 v3, p2, 0x8 */
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 1287 */
/* iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByImmobulus:Z */
/* .line 1288 */
} // :cond_0
/* and-int/lit8 v3, p2, 0x10 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 1289 */
/* iput-boolean v4, v2, Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;->isFrozenByLaunchMode:Z */
/* .line 1284 */
} // .end local v2 # "frozen":Lcom/miui/server/greeze/GreezeManagerService$FrozenInfo;
} // :cond_1
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 1292 */
} // .end local v1 # "i":I
} // :cond_2
/* monitor-exit v0 */
/* .line 1293 */
return;
/* .line 1292 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateOrderBCStatus ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "intentAction" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .param p3, "isforeground" # Z */
/* .param p4, "allow" # Z */
/* .line 2017 */
/* if-nez p1, :cond_0 */
return;
/* .line 2018 */
} // :cond_0
if ( p4 != null) { // if-eqz p4, :cond_2
/* .line 2019 */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 2020 */
this.mFGOrderBroadcastAction = p1;
/* .line 2021 */
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I */
/* .line 2023 */
} // :cond_1
this.mBGOrderBroadcastAction = p1;
/* .line 2024 */
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I */
/* .line 2027 */
} // :cond_2
int v0 = -1; // const/4 v0, -0x1
final String v1 = ""; // const-string v1, ""
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 2028 */
v2 = this.mFGOrderBroadcastAction;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 2029 */
this.mFGOrderBroadcastAction = v1;
/* .line 2030 */
/* iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mFGOrderBroadcastAppUid:I */
/* .line 2033 */
} // :cond_3
v2 = this.mBGOrderBroadcastAction;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 2034 */
this.mBGOrderBroadcastAction = v1;
/* .line 2035 */
/* iput v0, p0, Lcom/miui/server/greeze/GreezeManagerService;->mBGOrderBroadcastAppUid:I */
/* .line 2039 */
} // :cond_4
} // :goto_0
return;
} // .end method
