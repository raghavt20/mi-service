.class public Lcom/miui/server/greeze/GreezeManagerDebugConfig;
.super Ljava/lang/Object;
.source "GreezeManagerDebugConfig.java"


# static fields
.field public static DEBUG:Z = false

.field static DEBUG_AIDL:Z = false

.field public static DEBUG_LAUNCH_FROM_HOME:Z = false

.field static DEBUG_MILLET:Z = false

.field static final DEBUG_MONKEY:Z

.field public static DEBUG_SKIPUID:Z = false

.field public static final LAUNCH_FZ_TIMEOUT:J

.field public static PID_DEBUG:Z = false

.field static final PROPERTY_GZ_CGROUPV1:Ljava/lang/String; = "persist.sys.millet.cgroup"

.field static final PROPERTY_GZ_DEBUG:Ljava/lang/String; = "persist.sys.gz.debug"

.field private static final PROPERTY_GZ_ENABLE:Ljava/lang/String; = "persist.sys.gz.enable"

.field static final PROPERTY_GZ_FZTIMEOUT:Ljava/lang/String; = "persist.sys.gz.fztimeout"

.field static final PROPERTY_GZ_HANDSHAKE:Ljava/lang/String; = "persist.sys.millet.handshake"

.field static final PROPERTY_GZ_MONKEY:Ljava/lang/String; = "persist.sys.gz.monkey"

.field static final PROPERTY_PID_DEBUG:Ljava/lang/String; = "persist.sys.pid.debug"

.field static final PROP_POWERMILLET_ENABLE:Ljava/lang/String; = "persist.sys.powmillet.enable"

.field public static mCgroupV1Flag:Z

.field public static final mPowerMilletEnable:Z

.field protected static milletEnable:Z

.field protected static sEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 7
    const-string v0, "persist.sys.gz.monkey"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MONKEY:Z

    .line 9
    const-string v0, "persist.sys.gz.fztimeout"

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Landroid/os/SystemProperties;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    sput-wide v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J

    .line 13
    const-string v0, "persist.sys.pid.debug"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->PID_DEBUG:Z

    .line 16
    const-string v0, "persist.sys.powmillet.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    .line 17
    sput-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    .line 18
    const-string v0, "persist.sys.gz.enable"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    .line 19
    sput-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    .line 21
    const-string v0, "persist.sys.gz.debug"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    .line 22
    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_SKIPUID:Z

    .line 23
    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_MILLET:Z

    .line 24
    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_AIDL:Z

    .line 25
    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_LAUNCH_FROM_HOME:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isEnable()Z
    .locals 1

    .line 28
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->milletEnable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
