public abstract class com.miui.server.greeze.GreezeManagerInternal {
	 /* .source "GreezeManagerInternal.java" */
	 /* # static fields */
	 public static final Integer BINDER_STATE_IN_BUSY;
	 public static final Integer BINDER_STATE_IN_IDLE;
	 public static final Integer BINDER_STATE_IN_TRANSACTION;
	 public static final Integer BINDER_STATE_PROC_IN_BUSY;
	 public static final Integer BINDER_STATE_THREAD_IN_BUSY;
	 public static Integer GREEZER_MODULE_ALL;
	 public static Integer GREEZER_MODULE_GAME;
	 public static Integer GREEZER_MODULE_PERFORMANCE;
	 public static Integer GREEZER_MODULE_POWER;
	 public static Integer GREEZER_MODULE_PRELOAD;
	 public static Integer GREEZER_MODULE_UNKNOWN;
	 private static com.miui.server.greeze.GreezeManagerInternal sInstance;
	 /* # direct methods */
	 static com.miui.server.greeze.GreezeManagerInternal ( ) {
		 /* .locals 1 */
		 /* .line 13 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 14 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* .line 15 */
		 int v0 = 2; // const/4 v0, 0x2
		 /* .line 16 */
		 int v0 = 3; // const/4 v0, 0x3
		 /* .line 17 */
		 int v0 = 4; // const/4 v0, 0x4
		 /* .line 18 */
		 /* const/16 v0, 0x270f */
		 /* .line 25 */
		 int v0 = 0; // const/4 v0, 0x0
		 return;
	 } // .end method
	 public com.miui.server.greeze.GreezeManagerInternal ( ) {
		 /* .locals 0 */
		 /* .line 11 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static com.miui.server.greeze.GreezeManagerInternal getInstance ( ) {
		 /* .locals 1 */
		 /* .line 29 */
		 v0 = com.miui.server.greeze.GreezeManagerInternal.sInstance;
		 /* if-nez v0, :cond_0 */
		 /* .line 30 */
		 /* const-class v0, Lcom/miui/server/greeze/GreezeManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/miui/server/greeze/GreezeManagerInternal; */
		 /* .line 32 */
	 } // :cond_0
	 v0 = com.miui.server.greeze.GreezeManagerInternal.sInstance;
} // .end method
/* # virtual methods */
public abstract Boolean checkAurogonIntentDenyList ( java.lang.String p0 ) {
} // .end method
public abstract void finishLaunchMode ( java.lang.String p0, Integer p1 ) {
} // .end method
public abstract Boolean freezePid ( Integer p0 ) {
} // .end method
public abstract Boolean freezePid ( Integer p0, Integer p1 ) {
} // .end method
public abstract java.util.List freezePids ( Integer[] p0, Long p1, Integer p2, java.lang.String p3 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "([IJI", */
	 /* "Ljava/lang/String;", */
	 /* ")", */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end method
public abstract java.util.List freezeUids ( Integer[] p0, Long p1, Integer p2, java.lang.String p3, Boolean p4 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([IJI", */
/* "Ljava/lang/String;", */
/* "Z)", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract getFrozenPids ( Integer p0 ) {
} // .end method
public abstract getFrozenUids ( Integer p0 ) {
} // .end method
public abstract Long getLastThawedTime ( Integer p0, Integer p1 ) {
} // .end method
public abstract void handleAppZygoteStart ( android.content.pm.ApplicationInfo p0, Boolean p1 ) {
} // .end method
public abstract Boolean isNeedCachedBroadcast ( android.content.Intent p0, Integer p1, java.lang.String p2, Boolean p3 ) {
} // .end method
public abstract Boolean isRestrictBackgroundAction ( java.lang.String p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
} // .end method
public abstract Boolean isRestrictReceiver ( android.content.Intent p0, Integer p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
} // .end method
public abstract Boolean isUidFrozen ( Integer p0 ) {
} // .end method
public abstract void notifyBackup ( Integer p0, Boolean p1 ) {
} // .end method
public abstract void notifyDumpAllInfo ( ) {
} // .end method
public abstract void notifyDumpAppInfo ( Integer p0, Integer p1 ) {
} // .end method
public abstract void notifyExcuteServices ( Integer p0 ) {
} // .end method
public abstract void notifyFreeformModeFocus ( java.lang.String p0, Integer p1 ) {
} // .end method
public abstract void notifyMovetoFront ( Integer p0, Boolean p1 ) {
} // .end method
public abstract void notifyMultitaskLaunch ( Integer p0, java.lang.String p1 ) {
} // .end method
public abstract void notifyResumeTopActivity ( Integer p0, java.lang.String p1, Boolean p2 ) {
} // .end method
public abstract void queryBinderState ( Integer p0 ) {
} // .end method
public abstract Boolean registerCallback ( miui.greeze.IGreezeCallback p0, Integer p1 ) {
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
} // .end method
public abstract Boolean thawPid ( Integer p0 ) {
} // .end method
public abstract Boolean thawPid ( Integer p0, Integer p1 ) {
} // .end method
public abstract java.util.List thawPids ( Integer[] p0, Integer p1, java.lang.String p2 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract Boolean thawUid ( Integer p0, Integer p1, java.lang.String p2 ) {
} // .end method
public abstract java.util.List thawUids ( Integer[] p0, Integer p1, java.lang.String p2 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "([II", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
public abstract void triggerLaunchMode ( java.lang.String p0, Integer p1 ) {
} // .end method
public abstract void updateOrderBCStatus ( java.lang.String p0, Integer p1, Boolean p2, Boolean p3 ) {
} // .end method
