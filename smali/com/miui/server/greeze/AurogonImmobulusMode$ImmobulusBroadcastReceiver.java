public class com.miui.server.greeze.AurogonImmobulusMode$ImmobulusBroadcastReceiver extends android.content.BroadcastReceiver {
	 /* .source "AurogonImmobulusMode.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/AurogonImmobulusMode; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "ImmobulusBroadcastReceiver" */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.AurogonImmobulusMode this$0; //synthetic
/* # direct methods */
public com.miui.server.greeze.AurogonImmobulusMode$ImmobulusBroadcastReceiver ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/AurogonImmobulusMode; */
/* .line 1363 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
/* .line 1364 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 1365 */
/* .local v0, "intent":Landroid/content/IntentFilter; */
final String v1 = "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"; // const-string v1, "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1366 */
final String v1 = "android.intent.action.WALLPAPER_CHANGED"; // const-string v1, "android.intent.action.WALLPAPER_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1367 */
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1368 */
final String v1 = "com.android.app.action.SATELLITE_STATE_CHANGE"; // const-string v1, "com.android.app.action.SATELLITE_STATE_CHANGE"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 1369 */
v1 = this.mContext;
(( android.content.Context ) v1 ).registerReceiver ( p0, v0 ); // invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 1370 */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 8 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 1375 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 1376 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"; // const-string v1, "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_6
	 /* .line 1377 */
	 v1 = this.this$0;
	 /* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
	 /* if-nez v1, :cond_0 */
	 return;
	 /* .line 1378 */
} // :cond_0
final String v1 = "com.xiaomi.joyose.key.GAME_STATUS"; // const-string v1, "com.xiaomi.joyose.key.GAME_STATUS"
v1 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v3 ); // invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 1379 */
/* .local v1, "type":I */
final String v4 = "com.xiaomi.joyose.key.BACKGROUND_FREEZE_WHITELIST"; // const-string v4, "com.xiaomi.joyose.key.BACKGROUND_FREEZE_WHITELIST"
(( android.content.Intent ) p2 ).getStringArrayExtra ( v4 ); // invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1380 */
/* .local v4, "allowList":[Ljava/lang/String; */
/* if-nez v4, :cond_1 */
return;
/* .line 1381 */
} // :cond_1
/* array-length v5, v4 */
} // :goto_0
/* if-ge v3, v5, :cond_3 */
/* aget-object v6, v4, v3 */
/* .line 1382 */
/* .local v6, "str":Ljava/lang/String; */
v7 = this.this$0;
v7 = v7 = this.mImmobulusAllowList;
/* if-nez v7, :cond_2 */
/* .line 1383 */
v7 = this.this$0;
v7 = this.mImmobulusAllowList;
/* .line 1381 */
} // .end local v6 # "str":Ljava/lang/String;
} // :cond_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1387 */
} // :cond_3
/* if-ne v1, v2, :cond_4 */
/* .line 1388 */
v2 = this.this$0;
/* iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* if-nez v2, :cond_5 */
/* .line 1389 */
v2 = this.this$0;
v3 = this.mGreezeService;
/* iget v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
final String v5 = "Game"; // const-string v5, "Game"
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).triggerImmobulusMode ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V
/* .line 1393 */
} // :cond_4
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_5 */
v2 = this.this$0;
/* iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 1396 */
v2 = this.this$0;
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).quitImmobulusMode ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 1398 */
} // .end local v1 # "type":I
} // .end local v4 # "allowList":[Ljava/lang/String;
} // :cond_5
} // :goto_1
/* goto/16 :goto_3 */
} // :cond_6
final String v1 = "android.intent.action.WALLPAPER_CHANGED"; // const-string v1, "android.intent.action.WALLPAPER_CHANGED"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 1399 */
v1 = this.this$0;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).getWallpaperPackageName ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getWallpaperPackageName()V
/* goto/16 :goto_3 */
/* .line 1400 */
} // :cond_7
final String v1 = "android.net.conn.CONNECTIVITY_CHANGE"; // const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 1401 */
v1 = this.this$0;
v1 = this.mConnMgr;
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 1402 */
v1 = this.this$0;
v1 = this.mConnMgr;
/* const/16 v2, 0x11 */
(( android.net.ConnectivityManager ) v1 ).getNetworkInfo ( v2 ); // invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;
/* .line 1403 */
/* .local v1, "networkInfo":Landroid/net/NetworkInfo; */
v2 = this.this$0;
/* if-nez v1, :cond_8 */
} // :cond_8
v3 = (( android.net.NetworkInfo ) v1 ).isConnected ( ); // invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z
} // :goto_2
/* iput-boolean v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z */
/* .line 1404 */
v2 = this.this$0;
/* iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z */
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 1405 */
v2 = this.this$0;
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).updateVpnStatus ( v1 ); // invoke-virtual {v2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateVpnStatus(Landroid/net/NetworkInfo;)V
/* .line 1408 */
} // .end local v1 # "networkInfo":Landroid/net/NetworkInfo;
} // :cond_9
v1 = this.this$0;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).handleMessageAppStatus ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->handleMessageAppStatus()V
/* .line 1409 */
} // :cond_a
final String v1 = "com.android.app.action.SATELLITE_STATE_CHANGE"; // const-string v1, "com.android.app.action.SATELLITE_STATE_CHANGE"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_c
/* .line 1410 */
final String v1 = "is_enable"; // const-string v1, "is_enable"
v1 = (( android.content.Intent ) p2 ).getBooleanExtra ( v1, v3 ); // invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 1411 */
/* .local v1, "state":Z */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " BROADCAST_SATELLITE state = "; // const-string v5, " BROADCAST_SATELLITE state = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "Aurogon"; // const-string v5, "Aurogon"
android.util.Slog .d ( v5,v4 );
/* .line 1412 */
if ( v1 != null) { // if-eqz v1, :cond_b
/* .line 1413 */
v3 = this.this$0;
/* iput-boolean v2, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* .line 1414 */
v2 = this.this$0;
/* const/16 v3, 0x3e8 */
final String v4 = "ExtremeM_SATELLITE"; // const-string v4, "ExtremeM_SATELLITE"
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).triggerImmobulusMode ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V
/* .line 1416 */
} // :cond_b
v2 = this.this$0;
/* iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 1417 */
v2 = this.this$0;
/* iput-boolean v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* .line 1418 */
v2 = this.this$0;
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).quitImmobulusMode ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 1419 */
v2 = this.this$0;
v2 = this.mGreezeService;
final String v3 = "SATELLITE"; // const-string v3, "SATELLITE"
(( com.miui.server.greeze.GreezeManagerService ) v2 ).thawuidsAll ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawuidsAll(Ljava/lang/String;)V
/* .line 1420 */
v2 = this.this$0;
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).simulateNetChange ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->simulateNetChange()V
/* .line 1424 */
} // .end local v1 # "state":Z
} // :cond_c
} // :goto_3
return;
} // .end method
