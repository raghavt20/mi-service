.class Lcom/miui/server/greeze/GreezeManagerService$9$1;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService$9;->onForegroundActivitiesChanged(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService$9;I)V
    .locals 0
    .param p1, "this$1"    # Lcom/miui/server/greeze/GreezeManagerService$9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1809
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1811
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->updateAurogonUidRule(IZ)V

    .line 1813
    invoke-static {}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getInstance()Lcom/miui/server/greeze/AurogonDownloadFilter;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->setMoveToFgApp(I)V

    .line 1814
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1815
    .local v0, "packageName":Ljava/lang/String;
    if-nez v0, :cond_0

    return-void

    .line 1816
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "com.xiaomi.market"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1817
    :cond_1
    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-static {v1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1818
    invoke-static {}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getInstance()Lcom/miui/server/greeze/AurogonDownloadFilter;

    move-result-object v1

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-virtual {v1, v3, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->addAppNetCheckList(ILjava/lang/String;)V

    .line 1821
    :cond_2
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    iget-object v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v3, v3, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v3, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I

    if-eq v1, v3, :cond_3

    .line 1822
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1823
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v2, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    .line 1826
    :cond_3
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetENABLE_VIDEO_MODE_DEVICE(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/util/List;

    move-result-object v1

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1827
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z

    if-eqz v1, :cond_4

    .line 1828
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerVideoMode(ZLjava/lang/String;I)V

    .line 1830
    :cond_4
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVideoAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1831
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const/4 v2, 0x1

    iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$9$1;->val$uid:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerVideoMode(ZLjava/lang/String;I)V

    .line 1834
    :cond_5
    return-void
.end method
