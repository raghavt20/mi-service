.class Lcom/miui/server/greeze/AurogonImmobulusMode$6;
.super Ljava/lang/Object;
.source "AurogonImmobulusMode.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/AurogonImmobulusMode;->handleMessageAppStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/AurogonImmobulusMode;

    .line 1459
    iput-object p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$6;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1461
    sget-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMessageApp:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1462
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$6;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v2

    .line 1463
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    if-nez v2, :cond_0

    goto :goto_0

    .line 1464
    :cond_0
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$6;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v3, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v4, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1465
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$6;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v3, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v4, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const/16 v5, 0x3e8

    const-string v6, "Net Change"

    invoke-virtual {v3, v4, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    .line 1467
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_1
    goto :goto_0

    .line 1468
    :cond_2
    return-void
.end method
