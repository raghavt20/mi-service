.class public Lcom/miui/server/greeze/AurogonImmobulusMode;
.super Ljava/lang/Object;
.source "AurogonImmobulusMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;,
        Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;,
        Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final AUROGON_ENABLE:Ljava/lang/String; = "aurogon_enable"

.field private static final AUROGON_IMMOBULUS_SWITCH_PROPERTY:Ljava/lang/String; = "persist.sys.aurogon.immobulus"

.field private static final BLUETOOTH_GLOBAL_SETTING:Ljava/lang/String; = "RECORD_BLE_APPNAME"

.field public static final BROADCAST_SATELLITE:Ljava/lang/String; = "com.android.app.action.SATELLITE_STATE_CHANGE"

.field public static final CN_MODEL:Z

.field private static ENABLE_DOUBLE_APP:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final IMMOBULUS_ENABLED:Z

.field private static final IMMOBULUS_GAME_CONTROLLER:Ljava/lang/String; = "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"

.field private static final IMMOBULUS_GAME_KEY_ALLOW_LIST:Ljava/lang/String; = "com.xiaomi.joyose.key.BACKGROUND_FREEZE_WHITELIST"

.field private static final IMMOBULUS_GAME_KEY_STATUS:Ljava/lang/String; = "com.xiaomi.joyose.key.GAME_STATUS"

.field private static final IMMOBULUS_GAME_VALUE_QUIT:I = 0x2

.field private static final IMMOBULUS_GAME_VALUE_TRIGGER:I = 0x1

.field public static final IMMOBULUS_LAUNCH_MODE_REPEAT_TIME:I = 0x1f4

.field private static final IMMOBULUS_LEVEL_FORCESTOP:I = 0x3

.field public static final IMMOBULUS_REPEAT_TIME:I = 0x7d0

.field private static final IMMOBULUS_SWITCH_SECURE_SETTING:Ljava/lang/String; = "immobulus_mode_switch_restrict"

.field private static KEY_NO_RESTRICT_APP:Ljava/lang/String; = null

.field public static final MSG_IMMOBULUS_MODE_QUIT_ACTION:I = 0x66

.field public static final MSG_IMMOBULUS_MODE_TRIGGER_ACTION:I = 0x65

.field public static final MSG_LAUNCH_MODE_QUIT_ACTION:I = 0x6a

.field public static final MSG_LAUNCH_MODE_TRIGGER_ACTION:I = 0x69

.field public static final MSG_REMOVE_ALL_MESSAGE:I = 0x68

.field public static final MSG_REPEAT_FREEZE_APP:I = 0x67

.field private static final PC_SECURITY_CENTER_EXTREME_MODE:Ljava/lang/String; = "pc_security_center_extreme_mode_enter"

.field private static REASON_FREEZE:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static REASON_UNFREEZE:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SATELLITE_STATE:Ljava/lang/String; = "satellite_state"

.field public static final TAG:Ljava/lang/String; = "AurogonImmobulusMode"

.field private static final TIME_FREEZE_DURATION_UNUSEFUL:J = 0x4e20L

.field private static final TIME_FREEZE_DURATION_USEFUL:J = 0xea60L

.field private static final TIME_LAUNCH_MODE_TIMEOUT:J = 0xbb8L

.field public static final TYPE_MODE_IMMOBULUS:I = 0x8

.field public static final TYPE_MODE_LAUNCH:I = 0x10

.field public static mMessageApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private CTS_NAME:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mAllowWakeUpPackageNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mBluetoothUsingList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mBroadcastCtrlCloud:Z

.field public mCameraUid:I

.field private mCloudAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mConnMgr:Landroid/net/ConnectivityManager;

.field public mContext:Landroid/content/Context;

.field public mCtsModeOn:Z

.field public mCurrentIMEPacageName:Ljava/lang/String;

.field public mCurrentIMEUid:I

.field public mCurrentVideoPacageName:Ljava/lang/String;

.field public mCurrentWidgetPackages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDoubleAppCtrlCloud:Z

.field public mEnabledLMCamera:Z

.field public mEnterIMCamera:Z

.field public mEnterIMVideo:Z

.field private mEnterIMVideoCloud:Z

.field public mEnterImmobulusMode:Z

.field public mExtremeMode:Z

.field private mExtremeModeCloud:Z

.field public mFgServiceAppList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

.field public mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

.field public mImmobulusAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mImmobulusModeEnabled:Z

.field public mImmobulusTargetList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/AurogonAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mImportantAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mLastBarExpandIMStatus:Z

.field public mLastPackageName:Ljava/lang/String;

.field public mLaunchModeEnabled:Z

.field private mLocalAllowList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mMutiWindowsApp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoRestrictAppSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mOnWindowsAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPm:Landroid/content/pm/PackageManager;

.field public mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

.field public mQuitImmobulusList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/AurogonAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mQuitLaunchModeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/greeze/AurogonAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

.field public mVPNAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mVideoAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mVpnConnect:Z

.field public mWallPaperPackageName:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmExtremeModeCloud(Lcom/miui/server/greeze/AurogonImmobulusMode;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmNoRestrictAppSet(Lcom/miui/server/greeze/AurogonImmobulusMode;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mgetNoRestrictApps(Lcom/miui/server/greeze/AurogonImmobulusMode;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getNoRestrictApps()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mparseAurogonEnable(Lcom/miui/server/greeze/AurogonImmobulusMode;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->parseAurogonEnable()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudAllowList(Lcom/miui/server/greeze/AurogonImmobulusMode;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCloudAllowList()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateIMEAppStatus(Lcom/miui/server/greeze/AurogonImmobulusMode;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetKEY_NO_RESTRICT_APP()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->KEY_NO_RESTRICT_APP:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 8

    .line 88
    const-string v0, "MILLET_NO_RESTRICT_APP"

    sput-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->KEY_NO_RESTRICT_APP:Ljava/lang/String;

    .line 94
    const-string v0, "persist.sys.aurogon.immobulus"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z

    .line 95
    const-string v0, "ro.miui.region"

    const-string/jumbo v1, "unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "CN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "IMMOBULUS"

    const-string v2, "LAUNCH_MODE"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->REASON_FREEZE:Ljava/util/List;

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "SYNC_BINDER"

    const-string v2, "ASYNC_BINDER"

    const-string v3, "PACKET"

    const-string v4, "BINDER_SERVICE"

    const-string v5, "SIGNAL"

    const-string v6, "BROADCAST"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->REASON_UNFREEZE:Ljava/util/List;

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "babylon"

    const-string v2, "fuxi"

    const-string v3, "nuwa"

    const-string v4, "ishtar"

    filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->ENABLE_DOUBLE_APP:Ljava/util/List;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "com.tencent.mobileqq"

    const-string v2, "com.tencent.mm"

    const-string v3, "com.tencent.tim"

    const-string v4, "com.alibaba.android.rimet"

    const-string v5, "com.ss.android.lark"

    const-string v6, "com.ss.android.lark.kami"

    const-string v7, "com.tencent.wework"

    filled-new-array/range {v1 .. v7}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMessageApp:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/HandlerThread;Lcom/miui/server/greeze/GreezeManagerService;)V
    .locals 46
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ht"    # Landroid/os/HandlerThread;
    .param p3, "service"    # Lcom/miui/server/greeze/GreezeManagerService;

    .line 185
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    .line 98
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    .line 99
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    .line 100
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    .line 102
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitImmobulusList:Ljava/util/List;

    .line 103
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitLaunchModeList:Ljava/util/List;

    .line 104
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBluetoothUsingList:Ljava/util/List;

    .line 105
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    .line 106
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    .line 107
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    .line 108
    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    .line 109
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z

    .line 110
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z

    .line 112
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnabledLMCamera:Z

    .line 113
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    .line 115
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 116
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z

    .line 117
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z

    .line 118
    const/4 v4, -0x1

    iput v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I

    .line 120
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z

    .line 121
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z

    .line 122
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentVideoPacageName:Ljava/lang/String;

    .line 124
    iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBroadcastCtrlCloud:Z

    .line 125
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z

    .line 126
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    .line 127
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    .line 129
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    .line 131
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mPm:Landroid/content/pm/PackageManager;

    .line 133
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 134
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    .line 136
    const-string v2, ""

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    .line 137
    iput v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEUid:I

    .line 138
    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastPackageName:Ljava/lang/String;

    .line 139
    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mWallPaperPackageName:Ljava/lang/String;

    .line 141
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentWidgetPackages:Ljava/util/List;

    .line 153
    new-instance v2, Ljava/util/ArrayList;

    const-string v5, "android.uid.shared"

    const-string v6, "com.miui.systemAdSolution"

    const-string v7, "com.android.providers.media.module"

    const-string v8, "com.google.android.webview"

    const-string v9, "com.miui.voicetrigger"

    const-string v10, "com.miui.voiceassist"

    const-string v11, "com.dewmobile.kuaiya"

    const-string v12, "com.android.permissioncontroller"

    const-string v13, "com.android.htmlviewer"

    const-string v14, "com.google.android.providers.media.module"

    const-string v15, "com.android.incallui"

    const-string v16, "org.codeaurora.ims"

    const-string v17, "com.android.providers.contacts"

    const-string v18, "com.xiaomi.xaee"

    const-string v19, "com.android.calllogbackup"

    const-string v20, "com.android.providers.blockednumber"

    const-string v21, "com.android.providers.userdictionary"

    const-string v22, "com.xiaomi.aireco"

    const-string v23, "com.miui.securityinputmethod"

    const-string v24, "com.miui.home"

    const-string v25, "com.miui.newhome"

    const-string v26, "com.miui.screenshot"

    const-string v27, "com.lbe.security.miui"

    const-string v28, "org.ifaa.aidl.manager"

    const-string v29, "com.xiaomi.macro"

    const-string v30, "com.miui.rom"

    const-string v31, "com.miui.personalassistant"

    const-string v32, "com.miui.analytics"

    const-string v33, "com.miui.mediaviewer"

    const-string v34, "com.xiaomi.gamecenter.sdk.service"

    const-string v35, "com.xiaomi.scanner"

    const-string v36, "com.android.cameraextensions"

    const-string v37, "com.xiaomi.xmsf"

    const-string v38, "com.xiaomi.account"

    const-string v39, "com.xiaomi.metoknlp"

    const-string v40, "com.duokan.reader"

    const-string v41, "com.android.mms"

    const-string v42, "com.xiaomi.mibrain.speech"

    const-string v43, "com.baidu.carlife.xiaomi"

    const-string v44, "com.xiaomi.miralink"

    const-string v45, "com.miui.core"

    filled-new-array/range {v5 .. v45}, [Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLocalAllowList:Ljava/util/List;

    .line 170
    new-instance v2, Ljava/util/ArrayList;

    const-string v5, "com.ss.android.ugc.aweme.lite"

    const-string v6, "com.ss.android.ugc.aweme"

    const-string v7, "com.kuaishou.nebula"

    const-string v8, "com.smile.gifmaker"

    const-string/jumbo v9, "tv.danmaku.bili"

    const-string v10, "com.tencent.qqlive"

    const-string v11, "com.ss.android.article.video"

    const-string v12, "com.qiyi.video"

    const-string v13, "com.youku.phone"

    const-string v14, "com.miui.video"

    const-string v15, "com.duowan.kiwi"

    const-string v16, "air.tv.douyu.android"

    const-string v17, "com.le123.ysdq"

    const-string v18, "com.hunantv.imgo.activity"

    filled-new-array/range {v5 .. v18}, [Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVideoAppList:Ljava/util/List;

    .line 175
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    .line 176
    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCloudAllowList:Ljava/util/List;

    .line 177
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    .line 180
    new-instance v2, Ljava/util/ArrayList;

    const-string v5, "com.goodix.fingerprint.setting"

    const-string v6, "com.miui.gallery"

    const-string v7, "com.miui.home"

    const-string v8, "com.android.camera"

    filled-new-array {v7, v8, v5, v6}, [Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImportantAppList:Ljava/util/List;

    .line 183
    new-instance v2, Ljava/util/ArrayList;

    const-string v5, "android.net.cts"

    const-string v6, "com.android.cts.verifier"

    filled-new-array {v5, v6}, [Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CTS_NAME:Ljava/util/List;

    .line 790
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mOnWindowsAppList:Ljava/util/List;

    .line 1261
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    .line 1472
    new-instance v2, Ljava/util/ArrayList;

    const-string v5, "com.tencent.mm"

    const-string v6, "com.google.android.gms"

    filled-new-array {v5, v6}, [Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowWakeUpPackageNameList:Ljava/util/List;

    .line 186
    move-object/from16 v2, p1

    iput-object v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    .line 187
    sget-boolean v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z

    if-eqz v5, :cond_0

    .line 188
    new-instance v5, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual/range {p2 .. p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v0, v6, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Looper;Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler-IA;)V

    iput-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    .line 189
    iget-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mPm:Landroid/content/pm/PackageManager;

    .line 190
    move-object/from16 v1, p3

    iput-object v1, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    .line 191
    new-instance v5, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    iget-object v6, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-direct {v5, v0, v6}, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Handler;)V

    iput-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    .line 192
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    iget-object v6, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLocalAllowList:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 193
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->init()V

    .line 194
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "RECORD_BLE_APPNAME"

    invoke-static {v6}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 196
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "immobulus_mode_switch_restrict"

    invoke-static {v6}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 198
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "default_input_method"

    invoke-static {v6}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    const/4 v8, -0x2

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 200
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    invoke-static {v6}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 202
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "aurogon_enable"

    invoke-static {v6}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 204
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/miui/server/greeze/AurogonImmobulusMode;->KEY_NO_RESTRICT_APP:Ljava/lang/String;

    invoke-static {v6}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 207
    iget-object v5, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "enabled_widgets"

    invoke-static {v6}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 210
    iget-object v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "pc_security_center_extreme_mode_enter"

    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v4, v5, v3, v6, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 213
    iget-object v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "satellite_state"

    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mSettingsObserver:Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;

    invoke-virtual {v4, v5, v3, v6, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 216
    const-class v4, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v4}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/process/ProcessManagerInternal;

    iput-object v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 217
    new-instance v4, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;

    invoke-direct {v4, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    .line 218
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCloudAllowList()V

    .line 219
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V

    .line 220
    invoke-virtual/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateWidgetPackages()V

    .line 222
    invoke-virtual/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getConnectivityManager()V

    .line 223
    iget-object v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v4, v4, Lcom/miui/server/greeze/GreezeManagerService;->DISABLE_IMMOB_MODE_DEVICE:Ljava/util/List;

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 224
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    goto :goto_0

    .line 226
    :cond_0
    move-object/from16 v1, p3

    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    .line 227
    iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    .line 229
    :cond_1
    :goto_0
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getNoRestrictApps()V

    .line 230
    return-void
.end method

.method private InStatusBarScene(Ljava/lang/String;)Z
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .line 274
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppPackageName:Ljava/lang/String;

    const-string v1, "com.milink.service"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "com.xiaomi.smarthome"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    const/4 v0, 0x1

    return v0

    .line 277
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private checkFgServicesList()V
    .locals 9

    .line 757
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    return-void

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    monitor-enter v0

    .line 760
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 761
    .local v1, "tempUidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 762
    .local v3, "uid":I
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 763
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v4, :cond_4

    .line 764
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 765
    .local v5, "tempList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 766
    .local v7, "pid":I
    iget-object v8, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v8, v7}, Lcom/miui/server/process/ProcessManagerInternal;->checkAppFgServices(I)Z

    move-result v8

    if-nez v8, :cond_1

    .line 767
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 769
    .end local v7    # "pid":I
    :cond_1
    goto :goto_1

    .line 770
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 771
    .local v7, "tempPid":I
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 772
    nop

    .end local v7    # "tempPid":I
    goto :goto_2

    .line 773
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_4

    .line 774
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 777
    .end local v3    # "uid":I
    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v5    # "tempList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_4
    goto :goto_0

    .line 778
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 779
    .local v3, "tempUid":I
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 780
    nop

    .end local v3    # "tempUid":I
    goto :goto_3

    .line 781
    .end local v1    # "tempUidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_6
    monitor-exit v0

    .line 782
    return-void

    .line 781
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getNoRestrictApps()V
    .locals 7

    .line 1665
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->KEY_NO_RESTRICT_APP:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1666
    .local v0, "str":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getNoRestrictApps result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AurogonImmobulusMode"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 1671
    :cond_0
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1672
    .local v1, "apps":[Ljava/lang/String;
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-object v5, v1, v4

    .line 1673
    .local v5, "app":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 1674
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 1675
    goto :goto_1

    .line 1676
    :cond_1
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1677
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1672
    .end local v5    # "app":Ljava/lang/String;
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1679
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mNoRestrictAppSet="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1680
    return-void

    .line 1668
    .end local v1    # "apps":[Ljava/lang/String;
    :cond_4
    :goto_2
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1669
    return-void
.end method

.method private getPackageNameFromUid(I)Ljava/lang/String;
    .locals 3
    .param p1, "uid"    # I

    .line 984
    const/4 v0, 0x0

    .line 985
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mPm:Landroid/content/pm/PackageManager;

    if-eqz v1, :cond_0

    .line 986
    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 988
    :cond_0
    if-nez v0, :cond_1

    .line 989
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get caller pkgname failed uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AurogonImmobulusMode"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    :cond_1
    return-object v0
.end method

.method private init()V
    .locals 1

    .line 263
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    .line 265
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBluetoothUsingList:Ljava/util/List;

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitImmobulusList:Ljava/util/List;

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitLaunchModeList:Ljava/util/List;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCloudAllowList:Ljava/util/List;

    .line 269
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    .line 271
    return-void
.end method

.method private isSystemApp(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 930
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 931
    .local v1, "info":Landroid/content/pm/ApplicationInfo;
    if-eqz v1, :cond_0

    iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    move v0, v3

    :cond_0
    return v0

    .line 932
    .end local v1    # "info":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v1

    .line 934
    return v0
.end method

.method private parseAurogonEnable()V
    .locals 15

    .line 1683
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "aurogon_enable"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1684
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_4

    .line 1685
    :cond_0
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1686
    .local v1, "cfs":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_9

    aget-object v5, v1, v4

    .line 1687
    .local v5, "cf":Ljava/lang/String;
    const-string v6, "extrememode:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    const-string/jumbo v7, "true"

    if-eqz v6, :cond_1

    .line 1688
    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z

    goto :goto_3

    .line 1689
    :cond_1
    const-string v6, "enterimvideo:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1690
    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z

    goto :goto_3

    .line 1691
    :cond_2
    const-string v6, "broadcastctrl:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v8, 0x1

    if-eqz v6, :cond_7

    .line 1692
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    if-nez v6, :cond_3

    return-void

    .line 1693
    :cond_3
    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1694
    .local v6, "ls":[Ljava/lang/String;
    array-length v9, v6

    if-lez v9, :cond_4

    .line 1695
    iget-object v9, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    aget-object v10, v6, v3

    invoke-virtual {v10, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    invoke-virtual {v9, v7}, Lcom/miui/server/greeze/GreezeManagerService;->setBroadcastCtrl(Z)V

    .line 1696
    :cond_4
    array-length v7, v6

    move v9, v3

    :goto_1
    if-ge v9, v7, :cond_6

    aget-object v10, v6, v9

    .line 1697
    .local v10, "ua":Ljava/lang/String;
    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1698
    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 1699
    .local v11, "pkgAction":[Ljava/lang/String;
    array-length v12, v11

    if-le v12, v8, :cond_5

    .line 1700
    iget-object v12, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v12}, Lcom/miui/server/greeze/GreezeManagerService;->getBroadcastConfig()Ljava/util/Map;

    move-result-object v12

    aget-object v13, v11, v3

    aget-object v14, v11, v8

    invoke-interface {v12, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1696
    .end local v10    # "ua":Ljava/lang/String;
    .end local v11    # "pkgAction":[Ljava/lang/String;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .end local v6    # "ls":[Ljava/lang/String;
    :cond_6
    goto :goto_2

    .line 1703
    :cond_7
    const-string v6, "doubleapp:"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1704
    const-string v6, "false"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    xor-int/2addr v6, v8

    iput-boolean v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z

    goto :goto_3

    .line 1703
    :cond_8
    :goto_2
    nop

    .line 1686
    .end local v5    # "cf":Ljava/lang/String;
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1707
    :cond_9
    return-void

    .line 1684
    .end local v1    # "cfs":[Ljava/lang/String;
    :cond_a
    :goto_4
    return-void
.end method

.method private updateCloudAllowList()V
    .locals 8

    .line 1482
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z

    if-nez v0, :cond_b

    sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z

    if-nez v0, :cond_0

    goto/16 :goto_6

    .line 1490
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "immobulus_mode_switch_restrict"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1492
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 1493
    const-string v2, "AurogonImmobulusMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clound setting str = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1495
    .local v2, "temp":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x3

    if-ge v3, v4, :cond_1

    return-void

    .line 1496
    :cond_1
    const-string v3, "enable"

    aget-object v4, v2, v1

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_5

    .line 1497
    aget-object v3, v2, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1498
    .local v3, "enable":I
    and-int/lit8 v5, v3, 0x8

    if-eqz v5, :cond_2

    .line 1499
    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v5, v5, Lcom/miui/server/greeze/GreezeManagerService;->DISABLE_IMMOB_MODE_DEVICE:Ljava/util/List;

    sget-object v6, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 1500
    iput-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    goto :goto_0

    .line 1502
    :cond_2
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    .line 1505
    :cond_3
    :goto_0
    and-int/lit8 v5, v3, 0x10

    if-eqz v5, :cond_4

    .line 1506
    iput-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    goto :goto_1

    .line 1508
    :cond_4
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    .line 1512
    .end local v3    # "enable":I
    :cond_5
    :goto_1
    const-string v1, "allowlist"

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1513
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    monitor-enter v1

    .line 1514
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1515
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLocalAllowList:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1516
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCloudAllowList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1517
    const/16 v3, 0x270f

    .line 1518
    .local v3, "flag":I
    const/4 v5, 0x3

    .local v5, "i":I
    :goto_2
    array-length v6, v2

    if-ge v5, v6, :cond_7

    .line 1519
    const-string/jumbo v6, "wakeuplist"

    aget-object v7, v2, v5

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1520
    move v3, v5

    .line 1521
    goto :goto_3

    .line 1523
    :cond_6
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCloudAllowList:Ljava/util/List;

    aget-object v7, v2, v5

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1518
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1525
    .end local v5    # "i":I
    :cond_7
    :goto_3
    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCloudAllowList:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1526
    array-length v5, v2

    sub-int/2addr v5, v4

    if-ge v3, v5, :cond_8

    .line 1527
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowWakeUpPackageNameList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1529
    :cond_8
    add-int/lit8 v4, v3, 0x1

    .local v4, "j":I
    :goto_4
    array-length v5, v2

    if-ge v4, v5, :cond_9

    .line 1530
    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowWakeUpPackageNameList:Ljava/util/List;

    aget-object v6, v2, v4

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1529
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1532
    .end local v3    # "flag":I
    .end local v4    # "j":I
    :cond_9
    monitor-exit v1

    goto :goto_5

    :catchall_0
    move-exception v3

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1535
    .end local v2    # "temp":[Ljava/lang/String;
    :cond_a
    :goto_5
    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->parseAurogonEnable()V

    .line 1536
    return-void

    .line 1483
    .end local v0    # "str":Ljava/lang/String;
    :cond_b
    :goto_6
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    .line 1484
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    .line 1485
    sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z

    if-nez v0, :cond_c

    .line 1486
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->setBroadcastCtrl(Z)V

    .line 1487
    :cond_c
    return-void
.end method

.method private updateCtsStatus()V
    .locals 8

    .line 233
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "cached_apps_freezer"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    .local v0, "freezeEnable":Ljava/lang/String;
    const-string/jumbo v2, "use_freezer"

    .line 236
    .local v2, "KEY_USE_FREEZER":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 237
    .local v3, "DEFAULT_USE_FREEZER":Ljava/lang/Boolean;
    iget-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z

    const-string v5, "disabled"

    const-string v6, "enabled"

    if-eqz v4, :cond_1

    .line 238
    const-string v4, "persist.sys.powmillet.enable"

    const-string v7, "false"

    invoke-static {v4, v7}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 240
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v6}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 248
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string/jumbo v4, "tsMode"

    invoke-virtual {v1, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(Ljava/lang/String;)Z

    goto :goto_0

    .line 249
    :cond_1
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 251
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 250
    const-string v6, "activity_manager_native_boot"

    const-string/jumbo v7, "use_freezer"

    invoke-static {v6, v7, v4}, Landroid/provider/DeviceConfig;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 252
    :cond_2
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v5}, Landroid/provider/Settings$Global;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 255
    :cond_3
    :goto_0
    return-void
.end method

.method private updateIMEAppStatus()V
    .locals 5

    .line 1539
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1540
    .local v0, "curImeId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1541
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1542
    .local v1, "str":[Ljava/lang/String;
    array-length v2, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    .line 1543
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v4, v1, v3

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1544
    aget-object v2, v1, v3

    iput-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    .line 1545
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " updateIMEAppStatus mCurrentIMEPacageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AurogonImmobulusMode"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1546
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->getUidByPackageName(Ljava/lang/String;)I

    move-result v2

    .line 1547
    .local v2, "uid":I
    iput v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEUid:I

    .line 1551
    .end local v1    # "str":[Ljava/lang/String;
    .end local v2    # "uid":I
    :cond_0
    return-void
.end method


# virtual methods
.method public QuitImmobulusModeAction()V
    .locals 6

    .line 406
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    .line 407
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 408
    .local v0, "log":Ljava/lang/StringBuilder;
    const-string v1, "IM finish THAW uid = ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitImmobulusList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 410
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    const-string v3, "IMMOBULUS"

    invoke-virtual {p0, v2, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->unFreezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z

    move-result v3

    .line 411
    .local v3, "success":Z
    if-eqz v3, :cond_0

    .line 412
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    .end local v3    # "success":Z
    :cond_0
    goto :goto_0

    .line 415
    :cond_1
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V

    .line 417
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->resetStatusForImmobulus(I)V

    .line 418
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->resetImmobulusModeStatus()V

    .line 419
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->removeAllMsg()V

    .line 420
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitImmobulusList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 421
    return-void
.end method

.method public QuitLaunchModeAction(Z)V
    .locals 9
    .param p1, "timeout"    # Z

    .line 1129
    const/4 v0, 0x0

    .line 1130
    .local v0, "reason":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 1131
    const-string v0, "LM timeout"

    move-object v1, v0

    goto :goto_0

    .line 1133
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v2, 0x6a

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1134
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1138
    const-string v0, "LM finish"

    move-object v1, v0

    .line 1140
    .end local v0    # "reason":Ljava/lang/String;
    .local v1, "reason":Ljava/lang/String;
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v2, v0

    .line 1141
    .local v2, "log":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " THAW uid = ["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1142
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitLaunchModeList:Ljava/util/List;

    monitor-enter v3

    .line 1143
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitLaunchModeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/16 v5, 0x10

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 1144
    .local v4, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const-string v8, "LAUNCH_MODE"

    invoke-virtual {v6, v7, v5, v8}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    move-result v5

    .line 1145
    .local v5, "success":Z
    if-eqz v5, :cond_1

    .line 1146
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1148
    :cond_1
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->resetCgroupUidStatus(I)V

    .line 1150
    .end local v4    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    .end local v5    # "success":Z
    :goto_2
    goto :goto_1

    .line 1151
    :cond_2
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mQuitLaunchModeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1152
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1153
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1154
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V

    .line 1156
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, v5}, Lcom/miui/server/greeze/GreezeManagerService;->resetStatusForImmobulus(I)V

    .line 1157
    const-string v0, ""

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastPackageName:Ljava/lang/String;

    .line 1158
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->removeTempMutiWindowsApp()V

    .line 1159
    return-void

    .line 1152
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1136
    .end local v1    # "reason":Ljava/lang/String;
    .end local v2    # "log":Ljava/lang/StringBuilder;
    .restart local v0    # "reason":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public TriggerImmobulusModeAction(ILjava/lang/String;)V
    .locals 8
    .param p1, "uid"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 293
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    const/16 v2, 0x65

    const/4 v4, -0x1

    const-wide/16 v6, 0xdac

    move-object v1, p0

    move v3, p1

    move-object v5, p2

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 295
    return-void

    .line 297
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    .local v0, "log":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IM "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FZ ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] uid = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateAppsOnWindowsStatus()V

    .line 300
    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V

    .line 302
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v1

    .line 303
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 304
    .local v3, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    iget-object v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 305
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 306
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is in ImmobulusAllowList, skip it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 312
    :cond_2
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    iget-object v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 313
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 314
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " in Allowlist, skip it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 319
    :cond_3
    iget-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    iget-object v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 320
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 321
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is in NoRestrictAppSet, skip it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 326
    :cond_4
    iget-object v4, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->InStatusBarScene(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 327
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 328
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is in statusbar, skip it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 333
    :cond_5
    iget-object v4, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWallPaperApp(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 334
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 335
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is WallPaperApp app, skip check!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 341
    :cond_6
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunning(I)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-boolean v4, v4, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v4, :cond_7

    .line 342
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 343
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is not running, skip check!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 349
    :cond_7
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 350
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "is running in FG, skip check!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    goto/16 :goto_0

    .line 354
    :cond_8
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 355
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->updateFrozenInfoForImmobulus(II)V

    .line 356
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 357
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "updateFrozenInfoForImmobulus!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 362
    :cond_9
    iget-object v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mOnWindowsAppList:Ljava/util/List;

    if-eqz v4, :cond_a

    iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 363
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 364
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is show on screen, skip it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 383
    :cond_a
    iget-boolean v4, v3, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    if-nez v4, :cond_b

    .line 384
    sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v4, :cond_1

    .line 385
    const-string v4, "AurogonImmobulusMode"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " not has icon, skip it!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 390
    :cond_b
    invoke-virtual {p0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 391
    const/16 v4, 0x7d0

    invoke-virtual {p0, v3, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 392
    goto/16 :goto_0

    .line 395
    :cond_c
    const-string v4, "IMMOBULUS"

    invoke-virtual {p0, v3, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->freezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z

    move-result v4

    .line 396
    .local v4, "success":Z
    if-eqz v4, :cond_d

    .line 397
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    .end local v3    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    .end local v4    # "success":Z
    :cond_d
    goto/16 :goto_0

    .line 400
    :cond_e
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V

    .line 403
    return-void

    .line 400
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public addImmobulusModeQuitList(Lcom/miui/server/greeze/AurogonAppInfo;)V
    .locals 2
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;

    .line 898
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$4;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$4;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Lcom/miui/server/greeze/AurogonAppInfo;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z

    .line 908
    return-void
.end method

.method public addLaunchModeQiutList(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 866
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$2;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$2;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;I)V

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z

    .line 880
    return-void
.end method

.method public addLaunchModeQiutList(Lcom/miui/server/greeze/AurogonAppInfo;)V
    .locals 2
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;

    .line 884
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$3;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$3;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Lcom/miui/server/greeze/AurogonAppInfo;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z

    .line 895
    return-void
.end method

.method public addTempMutiWindowsApp(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 1263
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    monitor-enter v0

    .line 1264
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1265
    monitor-exit v0

    .line 1266
    return-void

    .line 1265
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public checkAppFgService(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 662
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    monitor-enter v0

    .line 663
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 664
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 665
    monitor-exit v0

    const/4 v0, 0x1

    return v0

    .line 667
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    monitor-exit v0

    .line 668
    const/4 v0, 0x0

    return v0

    .line 667
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public checkAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;)V
    .locals 2
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;

    .line 424
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-nez v0, :cond_0

    return-void

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    iget-object v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 430
    return-void

    .line 433
    :cond_2
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunning(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 434
    return-void

    .line 437
    :cond_3
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 438
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 439
    const/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    goto :goto_0

    .line 441
    :cond_4
    const/16 v0, 0x7d0

    invoke-virtual {p0, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 443
    :goto_0
    return-void

    .line 446
    :cond_5
    const-string v0, "repeat"

    invoke-virtual {p0, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->freezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z

    .line 447
    return-void

    .line 426
    :cond_6
    :goto_1
    return-void
.end method

.method public checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z
    .locals 5
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;

    .line 463
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z

    move-result v0

    const-string v1, " uid = "

    const-string v2, "AurogonImmobulusMode"

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    .line 464
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 465
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is running in FG, skip check!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    :cond_0
    return v3

    .line 478
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidActive(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 479
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_2

    .line 480
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is using GPS/Audio/Vibrator!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_2
    return v3

    .line 485
    :cond_3
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerService;->checkFreeformSmallWin(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 486
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_4

    .line 487
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Uid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was Freeform small window, skip it"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_4
    return v3

    .line 493
    :cond_5
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBluetoothUsingList:Ljava/util/List;

    iget-object v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 494
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_6

    .line 495
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is using BT!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    :cond_6
    return v3

    .line 499
    :cond_7
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->isBackuping(I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 500
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->isActiveInstruUid(I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 501
    invoke-static {}, Lcom/android/server/am/ActivityManagerServiceStub;->get()Lcom/android/server/am/ActivityManagerServiceStub;

    move-result-object v0

    iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->isVibratorActive(I)Z

    move-result v0

    if-eqz v0, :cond_8

    goto/16 :goto_0

    .line 507
    :cond_8
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->get()Lcom/android/server/wm/ActivityTaskManagerServiceStub;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getActivityControllerUid()I

    move-result v0

    iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    if-ne v0, v4, :cond_9

    .line 508
    return v3

    .line 512
    :cond_9
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    iget-object v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 513
    return v3

    .line 517
    :cond_a
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    const-string/jumbo v4, "uid = "

    if-eqz v0, :cond_c

    iget-object v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWidgetApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 518
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_b

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is widget app!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    :cond_b
    return v3

    .line 525
    :cond_c
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z

    if-eqz v0, :cond_e

    iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isVpnApp(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 526
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_d

    .line 527
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is vpn app!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    :cond_d
    return v3

    .line 532
    :cond_e
    iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkMutiWindowsApp(I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 533
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_f

    .line 534
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is in MutiWindows!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_f
    return v3

    .line 540
    :cond_10
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerService;->checkOrderBCRecivingApp(I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 541
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_11

    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "freeze app was reciving broadcast! mUid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :cond_11
    return v3

    .line 548
    :cond_12
    iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    iget-object v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {p0, v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isDownloadApp(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 549
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_13

    .line 550
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is downloading."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_13
    return v3

    .line 554
    :cond_14
    const/4 v0, 0x1

    return v0

    .line 502
    :cond_15
    :goto_0
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_16

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is using backingg, activeIns or vibrator"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    :cond_16
    return v3
.end method

.method public checkMutiWindowsApp(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 1274
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    monitor-enter v0

    .line 1275
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1276
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 1710
    const-string v0, "AurogonImmobulusMode : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1711
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mImmobulusModeEnabled : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1712
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 1713
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExtremeMode enabled : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cloud:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1714
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoMode enabled : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cloud:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1715
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Camera enabled : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " doubleapp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1718
    :cond_0
    const-string v0, "AurogonImmobulusMode LM : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LM enabled : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1720
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LM mCurrentIMEPacageName : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEPacageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1722
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 1723
    const-string v0, "mImmobulusAllowList : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1724
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1725
    .local v1, "str":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1726
    .end local v1    # "str":Ljava/lang/String;
    goto :goto_0

    .line 1728
    :cond_1
    const-string v0, "mImmobulusTargetList : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1729
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v0

    .line 1730
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 1731
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonAppInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1732
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    goto :goto_1

    .line 1733
    :cond_2
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1735
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    monitor-enter v1

    .line 1736
    :try_start_1
    const-string v0, "mFgServiceAppList : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1737
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1738
    .local v2, "uid":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " uid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1739
    .end local v2    # "uid":I
    goto :goto_2

    .line 1740
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1742
    const-string v0, "mAllowList : "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1743
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1744
    .local v1, "str1":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1745
    .end local v1    # "str1":Ljava/lang/String;
    goto :goto_3

    .line 1747
    :cond_4
    const-string v0, "mWallPaperPackageName"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1748
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mWallPaperPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1750
    const-string v0, "mAllowWakeUpPackageNameList"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1751
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowWakeUpPackageNameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1752
    .local v1, "str":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "--"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1753
    .end local v1    # "str":Ljava/lang/String;
    goto :goto_4

    .line 1740
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1733
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 1756
    :cond_5
    array-length v0, p3

    if-nez v0, :cond_6

    return-void

    .line 1757
    :cond_6
    const-string v0, "Immobulus"

    const/4 v1, 0x0

    aget-object v2, p3, v1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_8

    .line 1758
    const-string v0, "enable"

    aget-object v3, p3, v2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1759
    iput-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    .line 1760
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v3, 0x65

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendEmptyMessage(I)Z

    goto :goto_5

    .line 1761
    :cond_7
    const-string v0, "disabled"

    aget-object v3, p3, v2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1762
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    .line 1763
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v3, 0x66

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendEmptyMessage(I)Z

    .line 1767
    :cond_8
    :goto_5
    array-length v0, p3

    const/4 v3, 0x3

    if-ne v0, v3, :cond_f

    .line 1768
    const-string v0, "LM"

    aget-object v3, p3, v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v3, 0x2

    if-eqz v0, :cond_d

    .line 1769
    const-string v0, "add"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1770
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    aget-object v1, p3, v3

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 1771
    :cond_9
    const-string v0, "remove"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1772
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    aget-object v1, p3, v3

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1773
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    aget-object v1, p3, v3

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 1775
    :cond_a
    const-string/jumbo v0, "set"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1776
    const-string/jumbo v0, "true"

    aget-object v4, p3, v3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1777
    iput-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    goto :goto_6

    .line 1778
    :cond_b
    const-string v0, "false"

    aget-object v2, p3, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1779
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    goto :goto_6

    .line 1781
    :cond_c
    const-string/jumbo v0, "window"

    aget-object v1, p3, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1782
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateAppsOnWindowsStatus()V

    goto :goto_6

    .line 1784
    :cond_d
    const-string v0, "IM"

    aget-object v4, p3, v1

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1785
    const-string v0, "ExtremeMode"

    aget-object v4, p3, v2

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1786
    const-string/jumbo v0, "true"

    aget-object v4, p3, v3

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1787
    iput-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1788
    const/16 v0, 0x3e8

    const-string v1, "ExtremeM"

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    goto :goto_6

    .line 1789
    :cond_e
    const-string v0, "false"

    aget-object v2, p3, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1790
    iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1791
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1792
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v1, "Sleep Mode"

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(Ljava/lang/String;)Z

    .line 1797
    :cond_f
    :goto_6
    return-void
.end method

.method public finishLaunchMode()V
    .locals 8

    .line 1191
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 1192
    :cond_0
    const/16 v2, 0x6a

    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 1193
    return-void
.end method

.method public finishLaunchMode(Ljava/lang/String;I)V
    .locals 8
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1186
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastPackageName:Ljava/lang/String;

    if-eq p1, v0, :cond_0

    return-void

    .line 1187
    :cond_0
    const/16 v2, 0x6a

    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 1188
    return-void
.end method

.method public forceStopAppForImmobulus(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .line 601
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V

    .line 602
    return-void
.end method

.method public freezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z
    .locals 4
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;
    .param p2, "reason"    # Ljava/lang/String;

    .line 558
    iget-boolean v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z

    if-nez v0, :cond_0

    iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppFgService(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addImmobulusModeQuitList(Lcom/miui/server/greeze/AurogonAppInfo;)V

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->freezeAction(IILjava/lang/String;Z)Z

    move-result v0

    .line 563
    .local v0, "success":Z
    if-eqz v0, :cond_2

    .line 564
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->freezeTime:J

    goto :goto_0

    .line 566
    :cond_2
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v2, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 567
    const/16 v1, 0x7d0

    invoke-virtual {p0, p1, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 570
    :cond_3
    :goto_0
    return v0
.end method

.method public freezeActionForLaunchMode(Lcom/miui/server/greeze/AurogonAppInfo;)Z
    .locals 5
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;

    .line 1162
    const/4 v0, 0x0

    .line 1163
    .local v0, "isNeedCompact":Z
    iget-boolean v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z

    if-nez v1, :cond_1

    iget-boolean v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 1166
    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    .line 1164
    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(Lcom/miui/server/greeze/AurogonAppInfo;)V

    .line 1169
    :goto_1
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v2, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const/16 v3, 0x10

    const-string v4, "LAUNCH_MODE"

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/miui/server/greeze/GreezeManagerService;->freezeAction(IILjava/lang/String;Z)Z

    move-result v1

    .line 1170
    .local v1, "success":Z
    return v1
.end method

.method public getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
    .locals 4
    .param p1, "uid"    # I

    .line 672
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v0

    .line 673
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 674
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    if-ne v3, p1, :cond_0

    .line 675
    monitor-exit v0

    return-object v2

    .line 677
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_0
    goto :goto_0

    .line 678
    :cond_1
    monitor-exit v0

    .line 679
    const/4 v0, 0x0

    return-object v0

    .line 678
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAurogonAppInfo(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonAppInfo;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 683
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v0

    .line 684
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 685
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget-object v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    if-ne v3, p1, :cond_0

    .line 686
    monitor-exit v0

    return-object v2

    .line 688
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_0
    goto :goto_0

    .line 689
    :cond_1
    monitor-exit v0

    .line 690
    const/4 v0, 0x0

    return-object v0

    .line 689
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getConnectivityManager()V
    .locals 2

    .line 1006
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 1007
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    .line 1009
    :cond_0
    return-void
.end method

.method public getProcessManagerInternal()Lcom/miui/server/process/ProcessManagerInternal;
    .locals 2

    .line 785
    const/4 v0, 0x0

    .line 786
    .local v0, "pmi":Lcom/miui/server/process/ProcessManagerInternal;
    const-class v1, Lcom/miui/server/process/ProcessManagerInternal;

    invoke-static {v1}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/miui/server/process/ProcessManagerInternal;

    .line 787
    return-object v0
.end method

.method public getWallpaperPackageName()V
    .locals 4

    .line 1295
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "wallpaper"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/WallpaperManager;

    .line 1296
    .local v0, "wm":Landroid/app/WallpaperManager;
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v1

    .line 1297
    .local v1, "info":Landroid/app/WallpaperInfo;
    if-eqz v1, :cond_0

    .line 1298
    invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mWallPaperPackageName:Ljava/lang/String;

    goto :goto_0

    .line 1300
    :cond_0
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/WallpaperManager;->getDefaultWallpaperComponent(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v2

    .line 1301
    .local v2, "componentName":Landroid/content/ComponentName;
    if-eqz v2, :cond_1

    .line 1302
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mWallPaperPackageName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1307
    .end local v0    # "wm":Landroid/app/WallpaperManager;
    .end local v1    # "info":Landroid/app/WallpaperInfo;
    .end local v2    # "componentName":Landroid/content/ComponentName;
    :cond_1
    :goto_0
    goto :goto_1

    .line 1305
    :catch_0
    move-exception v0

    .line 1308
    :goto_1
    return-void
.end method

.method public handleMessageAppStatus()V
    .locals 2

    .line 1459
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$6;

    invoke-direct {v1, p0}, Lcom/miui/server/greeze/AurogonImmobulusMode$6;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z

    .line 1470
    return-void
.end method

.method public initCtsStatus()V
    .locals 1

    .line 258
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCtsStatus()V

    .line 261
    :cond_0
    return-void
.end method

.method public isAllowWakeUpList(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1474
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowWakeUpPackageNameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " packageName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isAllowWakeUpList "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Aurogon"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1476
    const/4 v0, 0x1

    return v0

    .line 1478
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isAppHasIcon(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 938
    const/4 v0, 0x0

    .line 939
    .local v0, "ret":Z
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 940
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 941
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 942
    if-eqz p1, :cond_0

    .line 943
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 945
    :cond_0
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mPm:Landroid/content/pm/PackageManager;

    const/high16 v3, 0x20000

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 946
    .local v2, "resolveInfolist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 947
    const/4 v0, 0x1

    .line 949
    :cond_1
    return v0
.end method

.method public isCtsApp(Ljava/lang/String;)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .line 911
    const-string v0, "\\."

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 912
    .local v0, "str":[Ljava/lang/String;
    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_5

    .line 913
    const/4 v3, 0x0

    .line 914
    .local v3, "val":I
    array-length v4, v0

    move v5, v1

    :goto_0
    if-ge v5, v4, :cond_5

    aget-object v6, v0, v5

    .line 915
    .local v6, "temp":Ljava/lang/String;
    const-string v7, "cts"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "gts"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    goto :goto_1

    .line 917
    :cond_0
    const-string v7, "android"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "google"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 918
    :cond_1
    or-int/lit8 v3, v3, 0x2

    goto :goto_2

    .line 916
    :cond_2
    :goto_1
    or-int/lit8 v3, v3, 0x1

    .line 920
    :cond_3
    :goto_2
    const/4 v7, 0x3

    if-ne v3, v7, :cond_4

    return v2

    .line 914
    .end local v6    # "temp":Ljava/lang/String;
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 924
    .end local v3    # "val":I
    :cond_5
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CTS_NAME:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    return v2

    .line 925
    :cond_6
    return v1
.end method

.method public isDownloadApp(ILjava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pacakgeName"    # Ljava/lang/String;

    .line 995
    invoke-static {}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getInstance()Lcom/miui/server/greeze/AurogonDownloadFilter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->isDownloadApp(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 996
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppFgService(I)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 997
    return v1

    .line 998
    :cond_0
    sget-object v0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mImportantDownloadApp:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 999
    return v1

    .line 1002
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isIMEApp(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 1554
    iget v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEUid:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isModeReason(Ljava/lang/String;)Z
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 1255
    sget-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->REASON_FREEZE:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1256
    const/4 v0, 0x1

    return v0

    .line 1258
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isNeedNotifyAppStatus(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 953
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v0

    .line 954
    .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 955
    iget-boolean v2, v0, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 957
    :cond_1
    return v1
.end method

.method public isNeedRestictNetworkPolicy(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 1012
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v0

    .line 1013
    .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    if-eqz v0, :cond_0

    sget-object v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMessageApp:Ljava/util/List;

    iget-object v2, v0, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1014
    const/4 v1, 0x1

    return v1

    .line 1016
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isRunningImmobulusMode()Z
    .locals 1

    .line 636
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v0, :cond_1

    .line 637
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 639
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public isRunningLaunchMode()Z
    .locals 2

    .line 632
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z

    move-result v0

    return v0
.end method

.method public isSystemOrMiuiImportantApp(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 605
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 606
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 607
    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 609
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public isSystemOrMiuiImportantApp(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 614
    const-string v0, ".miui"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_3

    const-string v0, ".xiaomi"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 615
    const-string v0, ".google"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.android"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 616
    const-string v0, "com.mi."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 620
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImportantAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 621
    return v1

    .line 624
    :cond_1
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemApp(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 625
    return v1

    .line 628
    :cond_2
    const/4 v0, 0x0

    return v0

    .line 617
    :cond_3
    :goto_0
    return v1
.end method

.method public isUidValid(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isUidValid"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    const/16 v0, 0x2710

    const/4 v1, 0x0

    if-ge p1, v0, :cond_0

    .line 811
    return v1

    .line 813
    :cond_0
    invoke-static {p1}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 814
    const/16 v0, 0x4e1f

    if-le p1, v0, :cond_2

    .line 815
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z

    if-nez v0, :cond_1

    .line 816
    return v1

    .line 817
    :cond_1
    sget-object v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->ENABLE_DOUBLE_APP:Ljava/util/List;

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 818
    return v1

    .line 820
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 822
    :cond_3
    return v1
.end method

.method public isVpnApp(I)Z
    .locals 3
    .param p1, "uid"    # I

    .line 1288
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    monitor-enter v0

    .line 1289
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    .line 1290
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isWallPaperApp(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 1280
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mWallPaperPackageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getWallpaperPackageName()V

    .line 1284
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mWallPaperPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isWidgetApp(I)Z
    .locals 2
    .param p1, "uid"    # I

    .line 1574
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1575
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWidgetApp(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public isWidgetApp(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pacakgeName"    # Ljava/lang/String;

    .line 1579
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentWidgetPackages:Ljava/util/List;

    monitor-enter v0

    .line 1580
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentWidgetPackages:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 1581
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public notifyAppActive(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 724
    sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z

    if-nez v0, :cond_0

    return-void

    .line 725
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateTargetList(I)V

    .line 726
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v0

    .line 727
    .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    if-nez v0, :cond_1

    return-void

    .line 729
    :cond_1
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    iget-object v2, v0, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 730
    return-void

    .line 733
    :cond_2
    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v1, :cond_3

    .line 734
    const/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 736
    :cond_3
    return-void
.end method

.method public notifyAppSwitchToBg(I)V
    .locals 3
    .param p1, "uid"    # I

    .line 695
    sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z

    if-nez v0, :cond_0

    return-void

    .line 697
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 698
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "com.android.camera"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 699
    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v1, :cond_1

    .line 700
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 704
    :cond_1
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v1

    .line 709
    .local v1, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateTargetList(I)V

    .line 711
    iget-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-eqz v2, :cond_2

    .line 712
    if-eqz v1, :cond_2

    .line 713
    const/16 v2, 0x7d0

    invoke-virtual {p0, v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 721
    :cond_2
    return-void
.end method

.method public notifyFgServicesChanged(II)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .line 739
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    if-nez v0, :cond_0

    .line 740
    const-string v0, "AurogonImmobulusMode"

    const-string v1, " mProcessManagerInternal = null"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getProcessManagerInternal()Lcom/miui/server/process/ProcessManagerInternal;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    .line 745
    :cond_0
    invoke-virtual {p0, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    .line 747
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkFgServicesList()V

    .line 749
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mProcessManagerInternal:Lcom/miui/server/process/ProcessManagerInternal;

    invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->checkAppFgServices(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 750
    const/4 v0, 0x1

    invoke-virtual {p0, p2, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateFgServicesList(IIZ)V

    goto :goto_0

    .line 752
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateFgServicesList(IIZ)V

    .line 754
    :goto_0
    return-void
.end method

.method public quitImmobulusMode()V
    .locals 2

    .line 1213
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v0, :cond_1

    .line 1215
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    .line 1217
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1218
    return-void

    .line 1220
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendEmptyMessage(I)Z

    .line 1222
    :cond_1
    return-void
.end method

.method public reOrderTargetList(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 281
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$1;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;I)V

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z

    .line 291
    return-void
.end method

.method public removeAllMsg()V
    .locals 2

    .line 1354
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1355
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1356
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1357
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1358
    return-void
.end method

.method public removeTempMutiWindowsApp()V
    .locals 2

    .line 1268
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    monitor-enter v0

    .line 1269
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mMutiWindowsApp:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1270
    monitor-exit v0

    .line 1271
    return-void

    .line 1270
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public repeatCheckAppForImmobulusMode(II)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "time"    # I

    .line 450
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v0

    .line 451
    .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    if-eqz v0, :cond_0

    .line 452
    invoke-virtual {p0, v0, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 454
    :cond_0
    return-void
.end method

.method public repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
    .locals 8
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;
    .param p2, "time"    # I

    .line 457
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 458
    :cond_0
    const/16 v2, 0x67

    const/4 v3, -0x1

    const/4 v4, -0x1

    int-to-long v6, p2

    move-object v1, p0

    move-object v5, p1

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 459
    return-void

    .line 457
    :cond_1
    :goto_0
    return-void
.end method

.method public resetImmobulusModeStatus()V
    .locals 4

    .line 1247
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v0

    .line 1248
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 1249
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    const/4 v3, 0x0

    iput v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    .line 1250
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    goto :goto_0

    .line 1251
    :cond_0
    monitor-exit v0

    .line 1252
    return-void

    .line 1251
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public sendMeesage(IIILjava/lang/Object;J)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "args1"    # I
    .param p3, "args2"    # I
    .param p4, "obj"    # Ljava/lang/Object;
    .param p5, "delayTime"    # J

    .line 644
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual {v0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 645
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    .line 646
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 648
    :cond_0
    if-eq p3, v1, :cond_1

    .line 649
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 651
    :cond_1
    if-eqz p4, :cond_2

    .line 652
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 654
    :cond_2
    const-wide/16 v1, -0x1

    cmp-long v1, p5, v1

    if-nez v1, :cond_3

    .line 655
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual {v1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 657
    :cond_3
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual {v1, v0, p5, p6}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 659
    :goto_0
    return-void
.end method

.method public simulateNetChange()V
    .locals 2

    .line 1429
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$5;

    invoke-direct {v1, p0}, Lcom/miui/server/greeze/AurogonImmobulusMode$5;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z

    .line 1436
    return-void
.end method

.method public triggerImmobulusMode(ILjava/lang/String;)V
    .locals 8
    .param p1, "uid"    # I
    .param p2, "mode"    # Ljava/lang/String;

    .line 1197
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-nez v0, :cond_0

    return-void

    .line 1199
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-nez v0, :cond_2

    const-string v0, "ExtremeM"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1200
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 1201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " triggerImmobulusMode uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " reject enter because of screen off!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AurogonImmobulusMode"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1203
    :cond_1
    return-void

    .line 1206
    :cond_2
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-nez v0, :cond_3

    .line 1207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    .line 1208
    const/16 v2, 0x65

    const/4 v4, -0x1

    const-wide/16 v6, 0xdac

    move-object v1, p0

    move v3, p1

    move-object v5, p2

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 1210
    :cond_3
    return-void
.end method

.method public triggerLaunchMode(Ljava/lang/String;I)V
    .locals 8
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 1174
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    if-nez v0, :cond_0

    return-void

    .line 1175
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "launch app processName = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AurogonImmobulusMode"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1176
    iput-object p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastPackageName:Ljava/lang/String;

    .line 1178
    const-string v0, "com.android.camera"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z

    .line 1182
    :cond_1
    const/16 v2, 0x69

    const/4 v4, -0x1

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    move-object v1, p0

    move v3, p2

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 1183
    return-void
.end method

.method public triggerLaunchModeAction(I)V
    .locals 18
    .param p1, "uid"    # I

    .line 1021
    move-object/from16 v8, p0

    move/from16 v9, p1

    invoke-virtual/range {p0 .. p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v10

    .line 1022
    .local v10, "tempApp":Lcom/miui/server/greeze/AurogonAppInfo;
    if-eqz v10, :cond_0

    iget-boolean v0, v10, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    if-nez v0, :cond_0

    return-void

    .line 1023
    :cond_0
    iget-object v0, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1025
    iget-object v0, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mHandler:Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V

    .line 1026
    const/16 v2, 0x6a

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const-wide/16 v6, 0x1388

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    goto :goto_0

    .line 1029
    :cond_1
    const/16 v2, 0x6a

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const-wide/16 v6, 0xbb8

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V

    .line 1032
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object v1, v0

    .line 1033
    .local v1, "log":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LM FZ ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] uid = ["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1034
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v2, v0

    .line 1035
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateAppsOnWindowsStatus()V

    .line 1036
    invoke-direct/range {p0 .. p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V

    .line 1038
    iget-object v3, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v3

    .line 1039
    :try_start_0
    iget-object v0, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 1040
    .local v4, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget v5, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    if-ne v9, v5, :cond_3

    goto :goto_1

    .line 1042
    :cond_3
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mAllowList:Ljava/util/List;

    iget-object v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1043
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 1044
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is allow list app, skip check!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1049
    :cond_4
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mNoRestrictAppSet:Ljava/util/Set;

    iget-object v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1050
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 1051
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is mNoRestrictAppSet list app, skip check!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1056
    :cond_5
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->checkRecentLuanchedApp(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1057
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 1058
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is launched at same time with topp app, skip check!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1064
    :cond_6
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunning(I)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1065
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 1066
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not running app, skip check!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1071
    :cond_7
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mOnWindowsAppList:Ljava/util/List;

    if-eqz v5, :cond_8

    iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1072
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 1073
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is show on screen, skip it!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1078
    :cond_8
    iget-object v5, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v8, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWallPaperApp(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1079
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 1080
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is WallPaperApp app, skip check!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1085
    :cond_9
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1086
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v5, :cond_a

    .line 1087
    const-string v5, "AurogonImmobulusMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " uid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is frozen app, skip check!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    :cond_a
    iget-object v5, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const/16 v7, 0x10

    invoke-virtual {v5, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->updateFrozenInfoForImmobulus(II)V

    .line 1090
    goto/16 :goto_1

    .line 1093
    :cond_b
    invoke-virtual {v8, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 1094
    goto/16 :goto_1

    .line 1097
    :cond_c
    sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v5, :cond_d

    .line 1098
    iget v5, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1099
    goto/16 :goto_1

    .line 1101
    :cond_d
    invoke-virtual {v8, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->freezeActionForLaunchMode(Lcom/miui/server/greeze/AurogonAppInfo;)Z

    move-result v5

    .line 1102
    .local v5, "success":Z
    if-eqz v5, :cond_e

    .line 1103
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1105
    .end local v4    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    .end local v5    # "success":Z
    :cond_e
    goto/16 :goto_1

    .line 1106
    :cond_f
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1107
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z

    if-eqz v0, :cond_14

    .line 1108
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_14

    .line 1109
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [I

    .line 1110
    .local v0, "uids":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_10

    .line 1111
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v0, v3

    .line 1110
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1113
    .end local v3    # "i":I
    :cond_10
    iget-object v11, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    const-wide/16 v13, 0x0

    const/16 v15, 0x10

    const-string v16, "LAUNCH_MODE"

    const/16 v17, 0x0

    move-object v12, v0

    invoke-virtual/range {v11 .. v17}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    .line 1114
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_14

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1115
    .local v5, "temp":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1116
    invoke-virtual {v8, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v6

    .line 1117
    .local v6, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    if-nez v6, :cond_11

    goto :goto_3

    .line 1118
    :cond_11
    iget-boolean v7, v6, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z

    if-nez v7, :cond_12

    iget-boolean v7, v6, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    if-nez v7, :cond_13

    .line 1119
    :cond_12
    invoke-virtual {v8, v6}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(Lcom/miui/server/greeze/AurogonAppInfo;)V

    .line 1121
    .end local v5    # "temp":I
    .end local v6    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_13
    goto :goto_3

    .line 1124
    .end local v0    # "uids":[I
    .end local v3    # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_14
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1125
    iget-object v0, v8, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V

    .line 1126
    return-void

    .line 1106
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public triggerVideoMode(ZLjava/lang/String;I)V
    .locals 2
    .param p1, "allow"    # Z
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uid"    # I

    .line 1225
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-nez v0, :cond_0

    return-void

    .line 1226
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z

    if-eqz v0, :cond_2

    .line 1227
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 1228
    const-string v0, "AurogonImmobulusMode"

    const-string v1, "SmallWindowMode skip videoMode!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    :cond_1
    return-void

    .line 1232
    :cond_2
    if-eqz p1, :cond_3

    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z

    if-eqz v0, :cond_3

    .line 1233
    const-string v0, "Video"

    invoke-virtual {p0, p3, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    .line 1234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z

    .line 1235
    iput-object p2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentVideoPacageName:Ljava/lang/String;

    goto :goto_0

    .line 1237
    :cond_3
    iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z

    if-eqz v0, :cond_5

    if-eqz p2, :cond_5

    .line 1238
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentVideoPacageName:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    .line 1239
    :cond_4
    invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z

    .line 1241
    const-string v0, ""

    iput-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentVideoPacageName:Ljava/lang/String;

    .line 1244
    :cond_5
    :goto_0
    return-void
.end method

.method public unFreezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z
    .locals 7
    .param p1, "app"    # Lcom/miui/server/greeze/AurogonAppInfo;
    .param p2, "reason"    # Ljava/lang/String;

    .line 575
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z

    move-result v0

    .line 576
    .local v0, "success":Z
    if-eqz v0, :cond_3

    .line 577
    iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v1, :cond_4

    .line 578
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 579
    .local v1, "currentTime":J
    iget-wide v3, p1, Lcom/miui/server/greeze/AurogonAppInfo;->freezeTime:J

    sub-long v3, v1, v3

    .line 580
    .local v3, "durration":J
    const-wide/16 v5, 0x4e20

    cmp-long v5, v3, v5

    if-gez v5, :cond_0

    .line 581
    iget v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    goto :goto_0

    .line 582
    :cond_0
    const-wide/32 v5, 0xea60

    cmp-long v5, v3, v5

    if-lez v5, :cond_1

    .line 583
    iget v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    .line 585
    :cond_1
    :goto_0
    iput-wide v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->unFreezeTime:J

    .line 586
    iget v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    const/4 v6, 0x3

    if-lt v5, v6, :cond_2

    .line 587
    iget-object v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mPackageName:Ljava/lang/String;

    const-string v6, "frequent wakeup"

    invoke-virtual {p0, v5, v6}, Lcom/miui/server/greeze/AurogonImmobulusMode;->forceStopAppForImmobulus(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    const/4 v5, 0x0

    iput v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I

    .line 589
    return v0

    .line 591
    :cond_2
    const/16 v5, 0x7d0

    invoke-virtual {p0, p1, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V

    .line 592
    .end local v1    # "currentTime":J
    .end local v3    # "durration":J
    goto :goto_1

    .line 594
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " unFreeze uid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reason : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AurogonImmobulusMode"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_4
    :goto_1
    return v0
.end method

.method public updateAppsOnWindowsStatus()V
    .locals 6

    .line 792
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    if-eqz v0, :cond_2

    .line 794
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mWindowManager:Landroid/view/IWindowManager;

    invoke-interface {v0}, Landroid/view/IWindowManager;->getAppsOnWindowsStatus()Ljava/util/List;

    move-result-object v0

    .line 795
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    .line 796
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mOnWindowsAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 797
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 798
    .local v2, "uid":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mOnWindowsAppList:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 799
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 800
    const-string v3, "AurogonImmobulusMode"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Integer.parseInt(uid) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 801
    .end local v2    # "uid":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 804
    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    goto :goto_1

    .line 803
    :catch_0
    move-exception v0

    .line 806
    :cond_2
    :goto_1
    return-void
.end method

.method public updateFgServicesList(IIZ)V
    .locals 4
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "allow"    # Z

    .line 961
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    monitor-enter v0

    .line 962
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 963
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz p3, :cond_1

    .line 964
    if-nez v1, :cond_0

    .line 965
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v1, v2

    .line 966
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 970
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 973
    :cond_1
    if-eqz v1, :cond_2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 974
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 975
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 976
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mFgServiceAppList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_2
    :goto_0
    monitor-exit v0

    .line 981
    return-void

    .line 980
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateTargetList(I)V
    .locals 6
    .param p1, "uid"    # I

    .line 826
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    monitor-enter v0

    .line 828
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isUidValid(I)Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    .line 829
    :cond_0
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo;

    .line 830
    .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    iget v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    if-ne v3, p1, :cond_1

    .line 831
    monitor-exit v0

    return-void

    .line 833
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_1
    goto :goto_0

    .line 835
    :cond_2
    invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v1

    .line 836
    .local v1, "packageName":Ljava/lang/String;
    if-nez v1, :cond_3

    monitor-exit v0

    return-void

    .line 838
    :cond_3
    const-string v2, "com.android.camera"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 839
    iput p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I

    .line 842
    :cond_4
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object v1, v2

    .line 843
    const-string v2, ".qualcomm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "com.qti"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto :goto_1

    .line 844
    :cond_5
    invoke-virtual {p0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isCtsApp(Ljava/lang/String;)Z

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_6

    const-string v2, "ctsshim"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 845
    invoke-virtual {p0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->QuitLaunchModeAction(Z)V

    .line 846
    iput-boolean v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    .line 847
    iput-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z

    .line 848
    const-string v2, "AurogonImmobulusMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "ts "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCtsStatus()V

    .line 852
    :cond_6
    new-instance v2, Lcom/miui/server/greeze/AurogonAppInfo;

    invoke-direct {v2, p1, v1}, Lcom/miui/server/greeze/AurogonAppInfo;-><init>(ILjava/lang/String;)V

    .line 853
    .restart local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    invoke-virtual {p0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isAppHasIcon(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 854
    iput-boolean v4, v2, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z

    .line 857
    :cond_7
    invoke-virtual {p0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 858
    iput-boolean v4, v2, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z

    .line 860
    :cond_8
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusTargetList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 861
    nop

    .end local v1    # "packageName":Ljava/lang/String;
    .end local v2    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    monitor-exit v0

    .line 862
    return-void

    .line 843
    .restart local v1    # "packageName":Ljava/lang/String;
    :cond_9
    :goto_1
    monitor-exit v0

    return-void

    .line 861
    .end local v1    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public updateVpnStatus(Landroid/net/NetworkInfo;)V
    .locals 14
    .param p1, "networkInfo"    # Landroid/net/NetworkInfo;

    .line 1439
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;

    move-result-object v0

    .line 1440
    .local v0, "net":[Landroid/net/Network;
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1441
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, v0, v3

    .line 1442
    .local v4, "temp":Landroid/net/Network;
    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    invoke-virtual {v5, v4}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v5

    .line 1443
    .local v5, "nc":Landroid/net/NetworkCapabilities;
    if-eqz v5, :cond_2

    .line 1444
    invoke-virtual {v5}, Landroid/net/NetworkCapabilities;->getAdministratorUids()[I

    move-result-object v6

    .line 1445
    .local v6, "uids":[I
    iget-object v7, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    monitor-enter v7

    .line 1446
    :try_start_0
    array-length v8, v6

    move v9, v2

    :goto_1
    if-ge v9, v8, :cond_1

    aget v10, v6, v9

    .line 1447
    .local v10, "uid":I
    sget-boolean v11, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v11, :cond_0

    .line 1448
    const-string v11, "AurogonImmobulusMode"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, " vpn uid = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    :cond_0
    iget-object v11, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVPNAppList:Ljava/util/List;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446
    nop

    .end local v10    # "uid":I
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1452
    :cond_1
    monitor-exit v7

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1441
    .end local v4    # "temp":Landroid/net/Network;
    .end local v5    # "nc":Landroid/net/NetworkCapabilities;
    .end local v6    # "uids":[I
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1455
    :cond_3
    return-void
.end method

.method public updateWidgetPackages()V
    .locals 7

    .line 1558
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_widgets"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1559
    .local v0, "widgetPackages":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1560
    const-string v1, "AurogonImmobulusMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " widgetPackages = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1561
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1562
    .local v1, "strs":[Ljava/lang/String;
    array-length v2, v1

    if-lez v2, :cond_1

    .line 1563
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentWidgetPackages:Ljava/util/List;

    monitor-enter v2

    .line 1564
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentWidgetPackages:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1565
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, v1, v4

    .line 1566
    .local v5, "str":Ljava/lang/String;
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentWidgetPackages:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1565
    nop

    .end local v5    # "str":Ljava/lang/String;
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1568
    :cond_0
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1571
    .end local v1    # "strs":[Ljava/lang/String;
    :cond_1
    :goto_1
    return-void
.end method
