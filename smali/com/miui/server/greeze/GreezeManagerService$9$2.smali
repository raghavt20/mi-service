.class Lcom/miui/server/greeze/GreezeManagerService$9$2;
.super Ljava/lang/Object;
.source "GreezeManagerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService$9;->onForegroundActivitiesChanged(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

.field final synthetic val$uid:I


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService$9;I)V
    .locals 0
    .param p1, "this$1"    # Lcom/miui/server/greeze/GreezeManagerService$9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1838
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->val$uid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1840
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z

    if-eqz v0, :cond_0

    .line 1841
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->val$uid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;

    move-result-object v0

    .line 1842
    .local v0, "packageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1843
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v1, v1, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/miui/server/greeze/GreezeManagerService;->updateFreeformSmallWinList(Ljava/lang/String;Z)V

    .line 1846
    .end local v0    # "packageName":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->this$1:Lcom/miui/server/greeze/GreezeManagerService$9;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService$9;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mImmobulusMode:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$9$2;->val$uid:I

    invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->notifyAppSwitchToBg(I)V

    .line 1847
    return-void
.end method
