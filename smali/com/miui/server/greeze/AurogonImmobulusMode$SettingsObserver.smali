.class final Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "AurogonImmobulusMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/AurogonImmobulusMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;


# direct methods
.method public constructor <init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 1585
    iput-object p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    .line 1586
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1587
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 1591
    if-nez p2, :cond_0

    return-void

    .line 1592
    :cond_0
    const-string v0, "RECORD_BLE_APPNAME"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    .line 1593
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1595
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1596
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBluetoothUsingList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1597
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1598
    .local v1, "names":[Ljava/lang/String;
    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v1, v2

    .line 1599
    .local v4, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v5, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBluetoothUsingList:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1600
    iget-object v5, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v5, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonAppInfo;

    move-result-object v5

    .line 1601
    .local v5, "app":Lcom/miui/server/greeze/AurogonAppInfo;
    if-eqz v5, :cond_1

    .line 1602
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v6, v6, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v7, v5, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I

    invoke-virtual {v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1603
    iget-object v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const-string v7, "BT connect"

    invoke-virtual {v6, v5, v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->unFreezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z

    .line 1598
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "app":Lcom/miui/server/greeze/AurogonAppInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1608
    .end local v0    # "str":Ljava/lang/String;
    .end local v1    # "names":[Ljava/lang/String;
    :cond_2
    goto/16 :goto_5

    :cond_3
    const-string v0, "immobulus_mode_switch_restrict"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1609
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-static {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$mupdateCloudAllowList(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    goto/16 :goto_5

    .line 1610
    :cond_4
    const-string v0, "default_input_method"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1611
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-static {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$mupdateIMEAppStatus(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    goto/16 :goto_5

    .line 1612
    :cond_5
    sget-object v0, Landroid/provider/MiuiSettings$Secure;->MIUI_OPTIMIZATION:Ljava/lang/String;

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto/16 :goto_5

    .line 1619
    :cond_6
    const-string v0, "pc_security_center_extreme_mode_enter"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/16 v3, 0x3e8

    const/4 v4, -0x1

    const/4 v5, 0x1

    if-eqz v1, :cond_a

    .line 1620
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-eqz v1, :cond_12

    .line 1621
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_7

    move v0, v5

    goto :goto_1

    :cond_7
    move v0, v2

    .line 1622
    .local v0, "temp":Z
    :goto_1
    const-string v1, "AurogonImmobulusMode"

    if-eqz v0, :cond_9

    .line 1623
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-static {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$fgetmExtremeModeCloud(Lcom/miui/server/greeze/AurogonImmobulusMode;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1624
    const-string v2, " enter Extreme Mode mode!"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1625
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v5, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1626
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const-string v2, "ExtremeM"

    invoke-virtual {v1, v3, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    goto :goto_2

    .line 1628
    :cond_8
    const-string v2, "Extreme Mode mode has enter!"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1631
    :cond_9
    const-string v3, " quit Extreme Mode mode!"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v2, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1633
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1635
    .end local v0    # "temp":Z
    :goto_2
    goto/16 :goto_5

    .line 1637
    :cond_a
    const-string v0, "aurogon_enable"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1638
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-static {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$mparseAurogonEnable(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    goto/16 :goto_5

    .line 1639
    :cond_b
    invoke-static {}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$sfgetKEY_NO_RESTRICT_APP()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1640
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v0, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-eqz v0, :cond_12

    .line 1641
    :cond_c
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-static {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$fgetmNoRestrictAppSet(Lcom/miui/server/greeze/AurogonImmobulusMode;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1642
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-static {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->-$$Nest$mgetNoRestrictApps(Lcom/miui/server/greeze/AurogonImmobulusMode;)V

    goto/16 :goto_5

    .line 1644
    :cond_d
    const-string v0, "satellite_state"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1645
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_e

    move v0, v5

    goto :goto_3

    :cond_e
    move v0, v2

    .line 1646
    .local v0, "state":Z
    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " SATELLITE_STATE state = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "Aurogon"

    invoke-static {v4, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    if-eqz v0, :cond_f

    .line 1648
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v5, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1649
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const-string v2, "ExtremeM_SATELLITE"

    invoke-virtual {v1, v3, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    goto :goto_4

    .line 1651
    :cond_f
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-eqz v1, :cond_11

    .line 1652
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v2, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1653
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1654
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v2, "SATELLITE"

    invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->thawuidsAll(Ljava/lang/String;)V

    .line 1655
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->simulateNetChange()V

    goto :goto_4

    .line 1658
    .end local v0    # "state":Z
    :cond_10
    const-string v0, "enabled_widgets"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1659
    iget-object v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateWidgetPackages()V

    goto :goto_5

    .line 1658
    :cond_11
    :goto_4
    nop

    .line 1661
    :cond_12
    :goto_5
    return-void
.end method
