public class com.miui.server.greeze.AurogonDownloadFilter {
	 /* .source "AurogonDownloadFilter.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;, */
	 /* Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DOWNLOAD_SPEED_COMMEN_LIMIT;
private static final Integer DOWNLOAD_SPEED_HIGH_LIMIT;
private static final Integer DOWNLOAD_SPEED_LOW_LIMIT;
private static final Integer MSG_AUROGON_DOWNLOAD_SPEED_UPDATE;
private static final Integer TIME_NET_DETECT_CYCLE;
public static java.util.List mImportantDownloadApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final com.miui.server.greeze.AurogonDownloadFilter sInstance;
/* # instance fields */
private java.util.List mAppNetCheckList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.greeze.AurogonDownloadFilter$DownloadHandler mHandler;
private Integer mLastLaunchedAppUid;
/* # direct methods */
static com.miui.server.greeze.AurogonDownloadFilter ( ) {
/* .locals 13 */
/* .line 23 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.tencent.tmgp.pubgmhd"; // const-string v1, "com.tencent.tmgp.pubgmhd"
final String v2 = "com.tencent.tmgp.sgame"; // const-string v2, "com.tencent.tmgp.sgame"
final String v3 = "com.miHoYo.ys.mi"; // const-string v3, "com.miHoYo.ys.mi"
final String v4 = "com.miHoYo.Yuanshen"; // const-string v4, "com.miHoYo.Yuanshen"
final String v5 = "com.miHoYo.ys.bilibili"; // const-string v5, "com.miHoYo.ys.bilibili"
final String v6 = "com.tencent.lolm"; // const-string v6, "com.tencent.lolm"
final String v7 = "com.tencent.jkchess"; // const-string v7, "com.tencent.jkchess"
final String v8 = "com.tencent.android.qqdownloader"; // const-string v8, "com.tencent.android.qqdownloader"
final String v9 = "com.hunantv.imgo.activity"; // const-string v9, "com.hunantv.imgo.activity"
final String v10 = "com.youku.phone"; // const-string v10, "com.youku.phone"
final String v11 = "com.cmcc.cmvideo"; // const-string v11, "com.cmcc.cmvideo"
final String v12 = "com.letv.android.client"; // const-string v12, "com.letv.android.client"
/* filled-new-array/range {v1 ..v12}, [Ljava/lang/String; */
/* .line 24 */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 29 */
/* new-instance v0, Lcom/miui/server/greeze/AurogonDownloadFilter; */
/* invoke-direct {v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;-><init>()V */
return;
} // .end method
private com.miui.server.greeze.AurogonDownloadFilter ( ) {
/* .locals 3 */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 30 */
int v0 = 0; // const/4 v0, 0x0
this.mHandler = v0;
/* .line 32 */
this.mAppNetCheckList = v0;
/* .line 33 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mLastLaunchedAppUid:I */
/* .line 36 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mAppNetCheckList = v1;
/* .line 37 */
/* new-instance v1, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler; */
com.miui.server.greeze.GreezeManagerService$GreezeThread .getInstance ( );
(( com.miui.server.greeze.GreezeManagerService$GreezeThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/GreezeManagerService$GreezeThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, p0, v2, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;-><init>(Lcom/miui/server/greeze/AurogonDownloadFilter;Landroid/os/Looper;Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler-IA;)V */
this.mHandler = v1;
/* .line 38 */
return;
} // .end method
private com.miui.server.greeze.AurogonDownloadFilter$AppNetStatus getAppNetStatus ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 166 */
v0 = this.mAppNetCheckList;
/* monitor-enter v0 */
/* .line 167 */
try { // :try_start_0
v1 = this.mAppNetCheckList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 168 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* iget v3, v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 169 */
/* monitor-exit v0 */
/* .line 171 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
} // :cond_0
/* .line 172 */
} // :cond_1
/* monitor-exit v0 */
/* .line 173 */
int v0 = 0; // const/4 v0, 0x0
/* .line 172 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private com.miui.server.greeze.AurogonDownloadFilter$AppNetStatus getAppNetStatus ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 155 */
v0 = this.mAppNetCheckList;
/* monitor-enter v0 */
/* .line 156 */
try { // :try_start_0
v1 = this.mAppNetCheckList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 157 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
v3 = this.mPacakgeName;
v3 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 158 */
/* monitor-exit v0 */
/* .line 160 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
} // :cond_0
/* .line 161 */
} // :cond_1
/* monitor-exit v0 */
/* .line 162 */
int v0 = 0; // const/4 v0, 0x0
/* .line 161 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public static com.miui.server.greeze.AurogonDownloadFilter getInstance ( ) {
/* .locals 1 */
/* .line 41 */
v0 = com.miui.server.greeze.AurogonDownloadFilter.sInstance;
} // .end method
/* # virtual methods */
public void addAppNetCheckList ( Integer p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 121 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
if ( v0 != null) { // if-eqz v0, :cond_0
return;
/* .line 122 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0x3e9 */
v0 = (( com.miui.server.greeze.AurogonDownloadFilter$DownloadHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;->hasMessages(I)Z
/* if-nez v0, :cond_1 */
/* .line 123 */
v0 = this.mHandler;
/* const-wide/16 v2, 0x1388 */
(( com.miui.server.greeze.AurogonDownloadFilter$DownloadHandler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 125 */
} // :cond_1
/* new-instance v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* invoke-direct {v0, p0, p1, p2}, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;-><init>(Lcom/miui/server/greeze/AurogonDownloadFilter;ILjava/lang/String;)V */
/* .line 126 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).getUidTxBytes ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidTxBytes(I)J
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastTxBytes:J */
/* .line 127 */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).getUidRxBytes ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidRxBytes(I)J
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastRxBytes:J */
/* .line 128 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->time:J */
/* .line 129 */
v1 = this.mAppNetCheckList;
/* monitor-enter v1 */
/* .line 130 */
try { // :try_start_0
v2 = this.mAppNetCheckList;
/* .line 131 */
/* monitor-exit v1 */
/* .line 132 */
return;
/* .line 131 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
} // .end method
public void calcAppSPeed ( com.miui.server.greeze.AurogonDownloadFilter$AppNetStatus p0 ) {
/* .locals 14 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 90 */
/* iget v0, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).getUidTxBytes ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidTxBytes(I)J
/* move-result-wide v0 */
/* .line 91 */
/* .local v0, "txBytes":J */
/* iget v2, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).getUidRxBytes ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getUidRxBytes(I)J
/* move-result-wide v2 */
/* .line 92 */
/* .local v2, "rxBytes":J */
/* add-long v4, v0, v2 */
/* .line 93 */
/* .local v4, "totalBytes":J */
/* iget-wide v6, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastRxBytes:J */
/* iget-wide v8, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastTxBytes:J */
/* add-long/2addr v6, v8 */
/* sub-long v6, v4, v6 */
/* .line 94 */
/* .local v6, "diffTotalBytes":J */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v8 */
/* .line 95 */
/* .local v8, "now":J */
/* iget-wide v10, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->time:J */
/* sub-long v10, v8, v10 */
/* const-wide/16 v12, 0x3e8 */
/* div-long/2addr v10, v12 */
/* .line 96 */
/* .local v10, "time":J */
/* const-wide/16 v12, 0x0 */
/* cmp-long v12, v10, v12 */
if ( v12 != null) { // if-eqz v12, :cond_0
/* .line 97 */
/* const-wide/16 v12, 0x400 */
/* div-long v12, v6, v12 */
/* div-long/2addr v12, v10 */
/* long-to-float v12, v12 */
/* iput v12, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F */
/* .line 99 */
} // :cond_0
/* iput-wide v2, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastRxBytes:J */
/* .line 100 */
/* iput-wide v0, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mLastTxBytes:J */
/* .line 101 */
/* iput-wide v8, p1, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->time:J */
/* .line 102 */
return;
} // .end method
public Long getTotalRxBytes ( ) {
/* .locals 2 */
/* .line 58 */
android.net.TrafficStats .getTotalRxBytes ( );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long getTotalTxBytes ( ) {
/* .locals 2 */
/* .line 54 */
android.net.TrafficStats .getTotalTxBytes ( );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long getUidRxBytes ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 50 */
android.net.TrafficStats .getUidRxBytes ( p1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Long getUidTxBytes ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 46 */
android.net.TrafficStats .getUidTxBytes ( p1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
public Boolean isDownloadApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 105 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 106 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 107 */
/* iget-boolean v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z */
/* .line 109 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isDownloadApp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pacakgeName" # Ljava/lang/String; */
/* .line 113 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 114 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 115 */
/* iget-boolean v1, v0, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z */
/* .line 117 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public void removeAppNetCheckList ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 144 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(I)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 145 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).removeAppNetCheckList ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V
/* .line 146 */
return;
} // .end method
public void removeAppNetCheckList ( com.miui.server.greeze.AurogonDownloadFilter$AppNetStatus p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 149 */
v0 = this.mAppNetCheckList;
/* monitor-enter v0 */
/* .line 150 */
try { // :try_start_0
v1 = this.mAppNetCheckList;
/* .line 151 */
/* monitor-exit v0 */
/* .line 152 */
return;
/* .line 151 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void removeAppNetCheckList ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 139 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->getAppNetStatus(Ljava/lang/String;)Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 140 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).removeAppNetCheckList ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonDownloadFilter;->removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V
/* .line 141 */
return;
} // .end method
public void setMoveToFgApp ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 135 */
/* iput p1, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mLastLaunchedAppUid:I */
/* .line 136 */
return;
} // .end method
public void updateAppSpeed ( ) {
/* .locals 7 */
/* .line 62 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 63 */
/* .local v0, "removeList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;>;" */
v1 = this.mAppNetCheckList;
/* monitor-enter v1 */
/* .line 64 */
try { // :try_start_0
v2 = this.mAppNetCheckList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_4
/* check-cast v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 65 */
/* .local v3, "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).calcAppSPeed ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/greeze/AurogonDownloadFilter;->calcAppSPeed(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V
/* .line 66 */
/* iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F */
/* const/high16 v5, 0x447a0000 # 1000.0f */
/* cmpl-float v4, v4, v5 */
/* if-lez v4, :cond_1 */
/* .line 67 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 68 */
final String v4 = "Aurogon isDownload "; // const-string v4, "Aurogon isDownload "
(( com.miui.server.greeze.AurogonDownloadFilter$AppNetStatus ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 69 */
} // :cond_0
int v4 = 1; // const/4 v4, 0x1
/* iput-boolean v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z */
/* .line 70 */
} // :cond_1
/* iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F */
/* const/high16 v5, 0x43fa0000 # 500.0f */
/* cmpl-float v4, v4, v5 */
int v5 = 0; // const/4 v5, 0x0
/* if-lez v4, :cond_2 */
/* .line 71 */
/* iput-boolean v5, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z */
/* .line 72 */
} // :cond_2
/* iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->downloadSpeed:F */
/* const/high16 v6, 0x42c80000 # 100.0f */
/* cmpg-float v4, v4, v6 */
/* if-gez v4, :cond_3 */
/* .line 73 */
/* iput-boolean v5, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->isDownload:Z */
/* .line 74 */
/* iget v4, v3, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;->mUid:I */
/* iget v5, p0, Lcom/miui/server/greeze/AurogonDownloadFilter;->mLastLaunchedAppUid:I */
/* if-eq v4, v5, :cond_3 */
/* .line 75 */
/* .line 78 */
} // .end local v3 # "app":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
} // :cond_3
} // :goto_1
/* .line 79 */
} // :cond_4
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 80 */
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_5
/* check-cast v2, Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
/* .line 81 */
/* .local v2, "removeapp":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus; */
(( com.miui.server.greeze.AurogonDownloadFilter ) p0 ).removeAppNetCheckList ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/greeze/AurogonDownloadFilter;->removeAppNetCheckList(Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;)V
/* .line 82 */
} // .end local v2 # "removeapp":Lcom/miui/server/greeze/AurogonDownloadFilter$AppNetStatus;
/* .line 84 */
} // :cond_5
v1 = v1 = this.mAppNetCheckList;
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 85 */
v1 = this.mHandler;
/* const/16 v2, 0x3e9 */
/* const-wide/16 v3, 0x1388 */
(( com.miui.server.greeze.AurogonDownloadFilter$DownloadHandler ) v1 ).sendEmptyMessageDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Lcom/miui/server/greeze/AurogonDownloadFilter$DownloadHandler;->sendEmptyMessageDelayed(IJ)Z
/* .line 87 */
} // :cond_6
return;
/* .line 79 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
