.class Lcom/miui/server/greeze/GreezeManagerService$1;
.super Landroid/database/ContentObserver;
.source "GreezeManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/greeze/GreezeManagerService;->registerCloudObserver(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/GreezeManagerService;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/miui/server/greeze/GreezeManagerService;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/GreezeManagerService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 133
    iput-object p1, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iput-object p3, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->val$context:Landroid/content/Context;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 136
    if-nez p2, :cond_0

    return-void

    .line 137
    :cond_0
    const-string v0, "cloud_greezer_enable"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "GreezeManager"

    const/4 v3, -0x2

    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cloud control set received :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->sEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 141
    :cond_1
    const-string/jumbo v0, "zeropkgs"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 142
    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v4, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->val$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0, v3}, Landroid/provider/Settings$Global;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fputmAudioZeroPkgs(Lcom/miui/server/greeze/GreezeManagerService;Ljava/lang/String;)V

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAudioZeroPkgs :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    invoke-static {v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$fgetmAudioZeroPkgs(Lcom/miui/server/greeze/GreezeManagerService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    :cond_2
    const-string v0, "force_fsg_nav_bar"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    iget-object v0, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->this$0:Lcom/miui/server/greeze/GreezeManagerService;

    iget-object v1, p0, Lcom/miui/server/greeze/GreezeManagerService$1;->val$context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->-$$Nest$mgetNavBarInfo(Lcom/miui/server/greeze/GreezeManagerService;Landroid/content/Context;)V

    .line 148
    :cond_3
    :goto_0
    return-void
.end method
