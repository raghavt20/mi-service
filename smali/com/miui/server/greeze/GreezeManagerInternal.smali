.class public abstract Lcom/miui/server/greeze/GreezeManagerInternal;
.super Ljava/lang/Object;
.source "GreezeManagerInternal.java"


# static fields
.field public static final BINDER_STATE_IN_BUSY:I = 0x1

.field public static final BINDER_STATE_IN_IDLE:I = 0x0

.field public static final BINDER_STATE_IN_TRANSACTION:I = 0x4

.field public static final BINDER_STATE_PROC_IN_BUSY:I = 0x3

.field public static final BINDER_STATE_THREAD_IN_BUSY:I = 0x2

.field public static GREEZER_MODULE_ALL:I

.field public static GREEZER_MODULE_GAME:I

.field public static GREEZER_MODULE_PERFORMANCE:I

.field public static GREEZER_MODULE_POWER:I

.field public static GREEZER_MODULE_PRELOAD:I

.field public static GREEZER_MODULE_UNKNOWN:I

.field private static sInstance:Lcom/miui/server/greeze/GreezeManagerInternal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    const/4 v0, 0x0

    sput v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_UNKNOWN:I

    .line 14
    const/4 v0, 0x1

    sput v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_POWER:I

    .line 15
    const/4 v0, 0x2

    sput v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PERFORMANCE:I

    .line 16
    const/4 v0, 0x3

    sput v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_GAME:I

    .line 17
    const/4 v0, 0x4

    sput v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_PRELOAD:I

    .line 18
    const/16 v0, 0x270f

    sput v0, Lcom/miui/server/greeze/GreezeManagerInternal;->GREEZER_MODULE_ALL:I

    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerInternal;->sInstance:Lcom/miui/server/greeze/GreezeManagerInternal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/server/greeze/GreezeManagerInternal;
    .locals 1

    .line 29
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerInternal;->sInstance:Lcom/miui/server/greeze/GreezeManagerInternal;

    if-nez v0, :cond_0

    .line 30
    const-class v0, Lcom/miui/server/greeze/GreezeManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/greeze/GreezeManagerInternal;

    sput-object v0, Lcom/miui/server/greeze/GreezeManagerInternal;->sInstance:Lcom/miui/server/greeze/GreezeManagerInternal;

    .line 32
    :cond_0
    sget-object v0, Lcom/miui/server/greeze/GreezeManagerInternal;->sInstance:Lcom/miui/server/greeze/GreezeManagerInternal;

    return-object v0
.end method


# virtual methods
.method public abstract checkAurogonIntentDenyList(Ljava/lang/String;)Z
.end method

.method public abstract finishLaunchMode(Ljava/lang/String;I)V
.end method

.method public abstract freezePid(I)Z
.end method

.method public abstract freezePid(II)Z
.end method

.method public abstract freezePids([IJILjava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJI",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract freezeUids([IJILjava/lang/String;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([IJI",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFrozenPids(I)[I
.end method

.method public abstract getFrozenUids(I)[I
.end method

.method public abstract getLastThawedTime(II)J
.end method

.method public abstract handleAppZygoteStart(Landroid/content/pm/ApplicationInfo;Z)V
.end method

.method public abstract isNeedCachedBroadcast(Landroid/content/Intent;ILjava/lang/String;Z)Z
.end method

.method public abstract isRestrictBackgroundAction(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)Z
.end method

.method public abstract isRestrictReceiver(Landroid/content/Intent;ILjava/lang/String;ILjava/lang/String;)Z
.end method

.method public abstract isUidFrozen(I)Z
.end method

.method public abstract notifyBackup(IZ)V
.end method

.method public abstract notifyDumpAllInfo()V
.end method

.method public abstract notifyDumpAppInfo(II)V
.end method

.method public abstract notifyExcuteServices(I)V
.end method

.method public abstract notifyFreeformModeFocus(Ljava/lang/String;I)V
.end method

.method public abstract notifyMovetoFront(IZ)V
.end method

.method public abstract notifyMultitaskLaunch(ILjava/lang/String;)V
.end method

.method public abstract notifyResumeTopActivity(ILjava/lang/String;Z)V
.end method

.method public abstract queryBinderState(I)V
.end method

.method public abstract registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract thawPid(I)Z
.end method

.method public abstract thawPid(II)Z
.end method

.method public abstract thawPids([IILjava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract thawUid(IILjava/lang/String;)Z
.end method

.method public abstract thawUids([IILjava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract triggerLaunchMode(Ljava/lang/String;I)V
.end method

.method public abstract updateOrderBCStatus(Ljava/lang/String;IZZ)V
.end method
