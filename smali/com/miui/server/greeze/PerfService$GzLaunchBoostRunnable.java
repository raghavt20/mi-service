class com.miui.server.greeze.PerfService$GzLaunchBoostRunnable implements java.lang.Runnable {
	 /* .source "PerfService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/PerfService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "GzLaunchBoostRunnable" */
} // .end annotation
/* # instance fields */
android.os.Bundle bundle;
final com.miui.server.greeze.PerfService this$0; //synthetic
/* # direct methods */
public com.miui.server.greeze.PerfService$GzLaunchBoostRunnable ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/PerfService; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 70 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 71 */
this.bundle = p2;
/* .line 72 */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 35 */
/* .line 76 */
/* move-object/from16 v1, p0 */
final String v2 = "Failed to get cast pid"; // const-string v2, "Failed to get cast pid"
v0 = this.bundle;
/* const-string/jumbo v3, "uid" */
v3 = (( android.os.Bundle ) v0 ).getInt ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 77 */
/* .local v3, "launchinguid":I */
v0 = this.bundle;
final String v4 = "launchingActivity"; // const-string v4, "launchingActivity"
(( android.os.Bundle ) v0 ).getString ( v4 ); // invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 78 */
/* .local v4, "launchingActivity":Ljava/lang/String; */
v0 = this.bundle;
final String v5 = "fromUid"; // const-string v5, "fromUid"
v5 = (( android.os.Bundle ) v0 ).getInt ( v5 ); // invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 79 */
/* .local v5, "fromUid":I */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v6 */
/* .line 80 */
/* .local v6, "startTime":J */
final String v0 = "GzBoost"; // const-string v0, "GzBoost"
/* const-wide/16 v8, 0x40 */
android.os.Trace .traceBegin ( v8,v9,v0 );
/* .line 82 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
final String v10 = "GzBoost "; // const-string v10, "GzBoost "
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 83 */
	 com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
	 /* new-instance v11, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
	 (( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v11 ).append ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v12 = ", start"; // const-string v12, ", start"
	 (( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v0,v11 );
	 /* .line 88 */
} // :cond_0
int v11 = -1; // const/4 v11, -0x1
/* .line 90 */
/* .local v11, "castPid":I */
int v12 = 0; // const/4 v12, 0x0
try { // :try_start_0
	 v0 = this.this$0;
	 com.miui.server.greeze.PerfService .-$$Nest$fgetmGetCastPid ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 /* .line 91 */
		 v0 = this.this$0;
		 com.miui.server.greeze.PerfService .-$$Nest$fgetmGetCastPid ( v0 );
		 v13 = this.this$0;
		 com.miui.server.greeze.PerfService .-$$Nest$fgetmAms ( v13 );
		 /* new-array v14, v12, [Ljava/lang/Object; */
		 (( java.lang.reflect.Method ) v0 ).invoke ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
		 /* check-cast v0, Ljava/lang/Integer; */
		 v0 = 		 (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
		 /* :try_end_0 */
		 /* .catch Ljava/lang/IllegalAccessException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* move v11, v0 */
		 /* .line 97 */
	 } // :cond_1
} // :goto_0
/* .line 95 */
/* :catch_0 */
/* move-exception v0 */
/* .line 96 */
/* .local v0, "e":Ljava/lang/reflect/InvocationTargetException; */
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
android.util.Slog .w ( v13,v2,v0 );
/* .line 93 */
} // .end local v0 # "e":Ljava/lang/reflect/InvocationTargetException;
/* :catch_1 */
/* move-exception v0 */
/* .line 94 */
/* .local v0, "e":Ljava/lang/IllegalAccessException; */
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
android.util.Slog .w ( v13,v2,v0 );
} // .end local v0 # "e":Ljava/lang/IllegalAccessException;
/* .line 100 */
} // :goto_1
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* move-object v2, v0 */
/* .line 101 */
/* .local v2, "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
com.miui.server.greeze.GreezeServiceUtils .getAudioUid ( );
/* .line 102 */
com.miui.server.greeze.GreezeServiceUtils .getIMEUid ( );
/* .line 106 */
try { // :try_start_1
miui.process.ProcessManager .getForegroundInfo ( );
/* .line 107 */
/* .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo; */
/* iget v13, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
java.lang.Integer .valueOf ( v13 );
/* .line 108 */
/* iget v13, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
java.lang.Integer .valueOf ( v13 );
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_2 */
/* .line 111 */
/* nop */
} // .end local v0 # "foregroundInfo":Lmiui/process/ForegroundInfo;
/* .line 109 */
/* :catch_2 */
/* move-exception v0 */
/* .line 110 */
/* .local v0, "e":Ljava/lang/Exception; */
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
final String v14 = "Failed to get foreground info from ProcessManager"; // const-string v14, "Failed to get foreground info from ProcessManager"
android.util.Slog .w ( v13,v14,v0 );
/* .line 114 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
/* .line 115 */
/* .local v0, "fzCount":I */
com.miui.server.greeze.GreezeServiceUtils .getUidMap ( );
/* .line 116 */
/* .local v13, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
int v14 = 0; // const/4 v14, 0x0
/* .local v14, "i":I */
} // :goto_3
v15 = (( android.util.SparseArray ) v13 ).size ( ); // invoke-virtual {v13}, Landroid/util/SparseArray;->size()I
final String v12 = "ms"; // const-string v12, "ms"
final String v8 = " from "; // const-string v8, " from "
/* if-ge v14, v15, :cond_13 */
/* .line 117 */
v9 = (( android.util.SparseArray ) v13 ).keyAt ( v14 ); // invoke-virtual {v13, v14}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 118 */
/* .local v9, "uid":I */
v15 = android.os.UserHandle .isApp ( v9 );
if ( v15 != null) { // if-eqz v15, :cond_11
/* if-eq v9, v3, :cond_11 */
/* if-ne v9, v5, :cond_2 */
/* .line 119 */
/* move/from16 v20, v0 */
/* move-object/from16 v16, v2 */
/* move/from16 v21, v3 */
/* move-wide/from16 v23, v6 */
/* move/from16 v27, v11 */
/* move-object/from16 v22, v13 */
/* move/from16 v28, v14 */
/* goto/16 :goto_b */
/* .line 122 */
} // :cond_2
v15 = java.lang.Integer .valueOf ( v9 );
/* move-object/from16 v16, v2 */
} // .end local v2 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
/* .local v16, "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
final String v2 = "GzBoost skip uid "; // const-string v2, "GzBoost skip uid "
if ( v15 != null) { // if-eqz v15, :cond_4
/* .line 123 */
/* sget-boolean v8, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_SKIPUID:Z */
if ( v8 != null) { // if-eqz v8, :cond_3
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v12 ).append ( v2 ); // invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v12 = " for dynamic white list"; // const-string v12, " for dynamic white list"
(( java.lang.StringBuilder ) v2 ).append ( v12 ); // invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v8,v2 );
} // :cond_3
/* move/from16 v20, v0 */
/* move/from16 v21, v3 */
/* move-wide/from16 v23, v6 */
/* move/from16 v27, v11 */
/* move-object/from16 v22, v13 */
/* move/from16 v28, v14 */
/* goto/16 :goto_b */
/* .line 127 */
} // :cond_4
(( android.util.SparseArray ) v13 ).valueAt ( v14 ); // invoke-virtual {v13, v14}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v15, Ljava/util/List; */
/* .line 129 */
/* .local v15, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* const/16 v17, 0x0 */
/* .line 130 */
/* .local v17, "skipUid":Z */
final String v18 = ""; // const-string v18, ""
/* .line 131 */
/* .local v18, "skipReason":Ljava/lang/String; */
} // :goto_4
v20 = /* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->hasNext()Z */
/* move/from16 v21, v3 */
} // .end local v3 # "launchinguid":I
/* .local v21, "launchinguid":I */
final String v3 = " "; // const-string v3, " "
if ( v20 != null) { // if-eqz v20, :cond_d
/* invoke-interface/range {v19 ..v19}, Ljava/util/Iterator;->next()Ljava/lang/Object; */
/* move-object/from16 v22, v13 */
} // .end local v13 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
/* .local v22, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* move-object/from16 v13, v20 */
/* check-cast v13, Lcom/miui/server/greeze/RunningProcess; */
/* .line 132 */
/* .local v13, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* move/from16 v20, v0 */
} // .end local v0 # "fzCount":I
/* .local v20, "fzCount":I */
/* iget-boolean v0, v13, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 133 */
/* const/16 v17, 0x1 */
/* .line 134 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-wide/from16 v23, v6 */
} // .end local v6 # "startTime":J
/* .local v23, "startTime":J */
/* iget v6, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " has foreground activity"; // const-string v6, " has foreground activity"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 135 */
/* move/from16 v27, v11 */
/* move/from16 v28, v14 */
/* move-object/from16 v0, v18 */
/* goto/16 :goto_9 */
/* .line 137 */
} // .end local v23 # "startTime":J
/* .restart local v6 # "startTime":J */
} // :cond_5
/* move-wide/from16 v23, v6 */
} // .end local v6 # "startTime":J
/* .restart local v23 # "startTime":J */
/* iget-boolean v0, v13, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 138 */
/* const/16 v17, 0x1 */
/* .line 139 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v6, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " has foreground service"; // const-string v6, " has foreground service"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 140 */
/* move/from16 v27, v11 */
/* move/from16 v28, v14 */
/* move-object/from16 v0, v18 */
/* goto/16 :goto_9 */
/* .line 142 */
} // :cond_6
/* iget v0, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
/* if-ne v0, v11, :cond_7 */
/* .line 143 */
/* const/16 v17, 0x1 */
/* .line 144 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* iget v6, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v6 = " has cast activity"; // const-string v6, " has cast activity"
(( java.lang.StringBuilder ) v0 ).append ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 145 */
/* move/from16 v27, v11 */
/* move/from16 v28, v14 */
/* move-object/from16 v0, v18 */
/* goto/16 :goto_9 */
/* .line 147 */
} // :cond_7
v0 = this.pkgList;
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 148 */
v0 = this.pkgList;
/* array-length v6, v0 */
int v7 = 0; // const/4 v7, 0x0
} // :goto_5
/* if-ge v7, v6, :cond_b */
/* move/from16 v25, v6 */
/* aget-object v6, v0, v7 */
/* .line 149 */
/* .local v6, "pkg":Ljava/lang/String; */
/* move-object/from16 v26, v0 */
com.miui.server.greeze.PerfService .-$$Nest$sfgetWHITELIST_PKG ( );
/* move/from16 v27, v11 */
} // .end local v11 # "castPid":I
/* .local v27, "castPid":I */
/* array-length v11, v0 */
/* move/from16 v28, v14 */
int v14 = 0; // const/4 v14, 0x0
} // .end local v14 # "i":I
/* .local v28, "i":I */
} // :goto_6
/* if-ge v14, v11, :cond_9 */
/* move/from16 v29, v11 */
/* aget-object v11, v0, v14 */
/* .line 150 */
/* .local v11, "whitePkg":Ljava/lang/String; */
v30 = android.text.TextUtils .equals ( v6,v11 );
if ( v30 != null) { // if-eqz v30, :cond_8
/* .line 151 */
int v0 = 1; // const/4 v0, 0x1
/* .line 152 */
} // .end local v17 # "skipUid":Z
/* .local v0, "skipUid":Z */
/* new-instance v14, Ljava/lang/StringBuilder; */
/* invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V */
/* move/from16 v17, v0 */
} // .end local v0 # "skipUid":Z
/* .restart local v17 # "skipUid":Z */
/* iget v0, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v14 ).append ( v0 ); // invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v14 = this.processName;
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v14 = " in whitelist"; // const-string v14, " in whitelist"
(( java.lang.StringBuilder ) v0 ).append ( v14 ); // invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 153 */
} // .end local v18 # "skipReason":Ljava/lang/String;
/* .local v0, "skipReason":Ljava/lang/String; */
/* move-object/from16 v18, v0 */
/* .line 149 */
} // .end local v0 # "skipReason":Ljava/lang/String;
} // .end local v11 # "whitePkg":Ljava/lang/String;
/* .restart local v18 # "skipReason":Ljava/lang/String; */
} // :cond_8
/* add-int/lit8 v14, v14, 0x1 */
/* move/from16 v11, v29 */
/* .line 156 */
} // :cond_9
} // :goto_7
if ( v17 != null) { // if-eqz v17, :cond_a
/* .line 148 */
} // .end local v6 # "pkg":Ljava/lang/String;
} // :cond_a
/* add-int/lit8 v7, v7, 0x1 */
/* move/from16 v6, v25 */
/* move-object/from16 v0, v26 */
/* move/from16 v11, v27 */
/* move/from16 v14, v28 */
} // .end local v27 # "castPid":I
} // .end local v28 # "i":I
/* .local v11, "castPid":I */
/* .restart local v14 # "i":I */
} // :cond_b
/* move/from16 v27, v11 */
/* move/from16 v28, v14 */
} // .end local v11 # "castPid":I
} // .end local v14 # "i":I
/* .restart local v27 # "castPid":I */
/* .restart local v28 # "i":I */
/* .line 147 */
} // .end local v27 # "castPid":I
} // .end local v28 # "i":I
/* .restart local v11 # "castPid":I */
/* .restart local v14 # "i":I */
} // :cond_c
/* move/from16 v27, v11 */
/* move/from16 v28, v14 */
/* .line 159 */
} // .end local v11 # "castPid":I
} // .end local v13 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // .end local v14 # "i":I
/* .restart local v27 # "castPid":I */
/* .restart local v28 # "i":I */
} // :goto_8
/* move/from16 v0, v20 */
/* move/from16 v3, v21 */
/* move-object/from16 v13, v22 */
/* move-wide/from16 v6, v23 */
/* move/from16 v11, v27 */
/* move/from16 v14, v28 */
/* goto/16 :goto_4 */
/* .line 131 */
} // .end local v20 # "fzCount":I
} // .end local v22 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v23 # "startTime":J
} // .end local v27 # "castPid":I
} // .end local v28 # "i":I
/* .local v0, "fzCount":I */
/* .local v6, "startTime":J */
/* .restart local v11 # "castPid":I */
/* .local v13, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v14 # "i":I */
} // :cond_d
/* move/from16 v20, v0 */
/* move-wide/from16 v23, v6 */
/* move/from16 v27, v11 */
/* move-object/from16 v22, v13 */
/* move/from16 v28, v14 */
} // .end local v0 # "fzCount":I
} // .end local v6 # "startTime":J
} // .end local v11 # "castPid":I
} // .end local v13 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v14 # "i":I
/* .restart local v20 # "fzCount":I */
/* .restart local v22 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v23 # "startTime":J */
/* .restart local v27 # "castPid":I */
/* .restart local v28 # "i":I */
/* move-object/from16 v0, v18 */
/* .line 160 */
} // .end local v18 # "skipReason":Ljava/lang/String;
/* .local v0, "skipReason":Ljava/lang/String; */
} // :goto_9
if ( v17 != null) { // if-eqz v17, :cond_e
/* .line 161 */
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_SKIPUID:Z */
if ( v3 != null) { // if-eqz v3, :cond_12
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v9 ); // invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = ", because "; // const-string v6, ", because "
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* goto/16 :goto_b */
/* .line 166 */
} // :cond_e
v6 = } // :goto_a
if ( v6 != null) { // if-eqz v6, :cond_10
/* check-cast v6, Lcom/miui/server/greeze/RunningProcess; */
/* .line 167 */
/* .local v6, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v4 ); // invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v11 = ", freezing "; // const-string v11, ", freezing "
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, v6, Lcom/miui/server/greeze/RunningProcess;->uid:I */
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v11, v6, Lcom/miui/server/greeze/RunningProcess;->pid:I */
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v11 = this.processName;
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v11 = " timeout="; // const-string v11, " timeout="
(( java.lang.StringBuilder ) v7 ).append ( v11 ); // invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-wide v13, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J */
(( java.lang.StringBuilder ) v7 ).append ( v13, v14 ); // invoke-virtual {v7, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 170 */
/* .local v7, "msg":Ljava/lang/String; */
/* sget-boolean v11, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v11 != null) { // if-eqz v11, :cond_f
/* .line 171 */
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
android.util.Slog .d ( v11,v7 );
/* .line 173 */
} // :cond_f
v11 = this.this$0;
com.miui.server.greeze.PerfService .-$$Nest$fgetmService ( v11 );
/* sget-wide v31, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J */
/* move-object/from16 v30, v6 */
/* move-object/from16 v34, v7 */
/* invoke-virtual/range {v29 ..v34}, Lcom/miui/server/greeze/GreezeManagerService;->freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z */
/* .line 174 */
/* nop */
} // .end local v6 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // .end local v7 # "msg":Ljava/lang/String;
/* add-int/lit8 v20, v20, 0x1 */
/* .line 175 */
/* .line 176 */
} // :cond_10
v2 = this.this$0;
com.miui.server.greeze.PerfService .-$$Nest$fgetmService ( v2 );
(( com.miui.server.greeze.GreezeManagerService ) v2 ).queryBinderState ( v9 ); // invoke-virtual {v2, v9}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V
/* .line 177 */
v2 = this.this$0;
com.miui.server.greeze.PerfService .-$$Nest$fgetmService ( v2 );
(( com.miui.server.greeze.GreezeManagerService ) v2 ).monitorNet ( v9 ); // invoke-virtual {v2, v9}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V
/* move/from16 v0, v20 */
/* .line 118 */
} // .end local v15 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
} // .end local v16 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v17 # "skipUid":Z
} // .end local v20 # "fzCount":I
} // .end local v21 # "launchinguid":I
} // .end local v22 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v23 # "startTime":J
} // .end local v27 # "castPid":I
} // .end local v28 # "i":I
/* .local v0, "fzCount":I */
/* .restart local v2 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .restart local v3 # "launchinguid":I */
/* .local v6, "startTime":J */
/* .restart local v11 # "castPid":I */
/* .restart local v13 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v14 # "i":I */
} // :cond_11
/* move/from16 v20, v0 */
/* move-object/from16 v16, v2 */
/* move/from16 v21, v3 */
/* move-wide/from16 v23, v6 */
/* move/from16 v27, v11 */
/* move-object/from16 v22, v13 */
/* move/from16 v28, v14 */
/* .line 116 */
} // .end local v0 # "fzCount":I
} // .end local v2 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v3 # "launchinguid":I
} // .end local v6 # "startTime":J
} // .end local v9 # "uid":I
} // .end local v11 # "castPid":I
} // .end local v13 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v14 # "i":I
/* .restart local v16 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .restart local v20 # "fzCount":I */
/* .restart local v21 # "launchinguid":I */
/* .restart local v22 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
/* .restart local v23 # "startTime":J */
/* .restart local v27 # "castPid":I */
/* .restart local v28 # "i":I */
} // :cond_12
} // :goto_b
/* move/from16 v0, v20 */
} // .end local v20 # "fzCount":I
/* .restart local v0 # "fzCount":I */
} // :goto_c
/* add-int/lit8 v14, v28, 0x1 */
/* move-object/from16 v2, v16 */
/* move/from16 v3, v21 */
/* move-object/from16 v13, v22 */
/* move-wide/from16 v6, v23 */
/* move/from16 v11, v27 */
/* const-wide/16 v8, 0x40 */
int v12 = 0; // const/4 v12, 0x0
} // .end local v28 # "i":I
/* .restart local v14 # "i":I */
/* goto/16 :goto_3 */
} // .end local v16 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v21 # "launchinguid":I
} // .end local v22 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v23 # "startTime":J
} // .end local v27 # "castPid":I
/* .restart local v2 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .restart local v3 # "launchinguid":I */
/* .restart local v6 # "startTime":J */
/* .restart local v11 # "castPid":I */
/* .restart local v13 # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
} // :cond_13
/* move/from16 v20, v0 */
/* move-object/from16 v16, v2 */
/* move-wide/from16 v23, v6 */
/* .line 180 */
} // .end local v0 # "fzCount":I
} // .end local v2 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
} // .end local v6 # "startTime":J
} // .end local v14 # "i":I
/* .restart local v16 # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* .restart local v20 # "fzCount":I */
/* .restart local v23 # "startTime":J */
/* const-wide/16 v2, 0x40 */
android.os.Trace .traceEnd ( v2,v3 );
/* .line 181 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
/* sub-long v2, v2, v23 */
/* .line 182 */
/* .local v2, "duration":J */
com.miui.server.greeze.PerfService .-$$Nest$sfgetTAG ( );
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", froze "; // const-string v7, ", froze "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move/from16 v7, v20 */
} // .end local v20 # "fzCount":I
/* .local v7, "fzCount":I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = " processes, took "; // const-string v8, " processes, took "
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2, v3 ); // invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v12 ); // invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v6 );
/* .line 184 */
return;
} // .end method
