.class public Lcom/miui/server/greeze/RunningProcess;
.super Ljava/lang/Object;
.source "RunningProcess.java"


# instance fields
.field adj:I

.field hasForegroundActivities:Z

.field hasForegroundServices:Z

.field pid:I

.field pkgList:[Ljava/lang/String;

.field procState:I

.field processName:Ljava/lang/String;

.field uid:I


# direct methods
.method constructor <init>(Landroid/app/ActivityManager$RunningAppProcessInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->pid:I

    .line 31
    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->uid:I

    .line 32
    iget-object v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    .line 33
    iget-object v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    .line 34
    return-void
.end method

.method constructor <init>(Lmiui/process/RunningProcessInfo;)V
    .locals 1
    .param p1, "info"    # Lmiui/process/RunningProcessInfo;

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iget v0, p1, Lmiui/process/RunningProcessInfo;->mPid:I

    iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->pid:I

    .line 20
    iget v0, p1, Lmiui/process/RunningProcessInfo;->mUid:I

    iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->uid:I

    .line 21
    iget-object v0, p1, Lmiui/process/RunningProcessInfo;->mProcessName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    .line 22
    iget-object v0, p1, Lmiui/process/RunningProcessInfo;->mPkgList:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiui/process/RunningProcessInfo;->mPkgList:[Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    .line 23
    iget v0, p1, Lmiui/process/RunningProcessInfo;->mAdj:I

    iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->adj:I

    .line 24
    iget v0, p1, Lmiui/process/RunningProcessInfo;->mProcState:I

    iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->procState:I

    .line 25
    iget-boolean v0, p1, Lmiui/process/RunningProcessInfo;->mHasForegroundActivities:Z

    iput-boolean v0, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z

    .line 26
    iget-boolean v0, p1, Lmiui/process/RunningProcessInfo;->mHasForegroundServices:Z

    iput-boolean v0, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z

    .line 27
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/miui/server/greeze/RunningProcess;->uid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/miui/server/greeze/RunningProcess;->adj:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/greeze/RunningProcess;->procState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 40
    iget-boolean v1, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z

    const-string v2, ""

    if-eqz v1, :cond_0

    const-string v1, " FA"

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 41
    iget-boolean v1, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z

    if-eqz v1, :cond_1

    const-string v2, " FS"

    :cond_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    return-object v0
.end method
