class com.miui.server.greeze.GreezeManagerService$5 implements java.lang.Runnable {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService;->checkAndFreezeIsolated(IZ)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
final Boolean val$freeze; //synthetic
final Integer val$uid; //synthetic
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$5 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 1367 */
this.this$0 = p1;
/* iput p2, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I */
/* iput-boolean p3, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 11 */
/* .line 1370 */
v0 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmActivityManagerService ( v0 );
v0 = this.mInternal;
/* iget v1, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I */
(( android.app.ActivityManagerInternal ) v0 ).getIsolatedProcesses ( v1 ); // invoke-virtual {v0, v1}, Landroid/app/ActivityManagerInternal;->getIsolatedProcesses(I)Ljava/util/List;
/* .line 1371 */
/* .local v0, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* iget-boolean v1, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z */
/* if-nez v1, :cond_0 */
/* .line 1372 */
v1 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v1 );
/* monitor-enter v1 */
/* .line 1373 */
try { // :try_start_0
v2 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v2 );
/* iget v3, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I */
java.lang.Integer .valueOf ( v3 );
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
(( android.util.SparseArray ) v2 ).remove ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/SparseArray;->remove(I)V
/* .line 1374 */
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* .line 1376 */
} // :cond_0
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 1377 */
return;
/* .line 1378 */
} // :cond_1
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 1379 */
/* .local v1, "rr":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
int v2 = 0; // const/4 v2, 0x0
/* .line 1380 */
/* .local v2, "index":I */
/* .line 1381 */
/* .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
} // :cond_2
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 1382 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 1383 */
/* .local v4, "p":I */
v5 = android.os.Process .getUidForPid ( v4 );
/* .line 1384 */
/* .local v5, "isouid":I */
int v6 = -1; // const/4 v6, -0x1
/* if-eq v5, v6, :cond_2 */
v6 = android.os.Process .isIsolated ( v5 );
/* if-nez v6, :cond_3 */
/* .line 1385 */
/* .line 1386 */
} // :cond_3
java.lang.Integer .valueOf ( v4 );
/* .line 1387 */
} // .end local v4 # "p":I
} // .end local v5 # "isouid":I
/* .line 1388 */
v4 = } // :cond_4
/* if-nez v4, :cond_5 */
return;
/* .line 1389 */
} // :cond_5
/* new-instance v5, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v5}, Lcom/android/server/location/mnlutils/MnlConfigUtils$$ExternalSyntheticLambda0;-><init>()V */
/* .line 1390 */
/* .local v4, "rst":[I */
/* iget-boolean v5, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z */
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 1391 */
v5 = this.this$0;
/* const-wide/16 v7, 0x0 */
/* const/16 v9, 0x3e8 */
final String v10 = "iso"; // const-string v10, "iso"
/* move-object v6, v4 */
/* invoke-virtual/range {v5 ..v10}, Lcom/miui/server/greeze/GreezeManagerService;->freezePids([IJILjava/lang/String;)Ljava/util/List; */
/* .line 1392 */
v5 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v5 );
/* monitor-enter v5 */
/* .line 1393 */
try { // :try_start_1
v6 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v6 );
/* iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I */
(( android.util.SparseArray ) v6 ).put ( v7, v1 ); // invoke-virtual {v6, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 1394 */
/* monitor-exit v5 */
/* :catchall_1 */
/* move-exception v6 */
/* monitor-exit v5 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* throw v6 */
/* .line 1396 */
} // :cond_6
v5 = this.this$0;
/* const/16 v6, 0x3e8 */
final String v7 = "iso"; // const-string v7, "iso"
(( com.miui.server.greeze.GreezeManagerService ) v5 ).thawPids ( v4, v6, v7 ); // invoke-virtual {v5, v4, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;
/* .line 1398 */
} // :goto_2
final String v5 = "GreezeManager"; // const-string v5, "GreezeManager"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "iso uid:"; // const-string v7, "iso uid:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$uid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " p:"; // const-string v7, " p:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v7 = " :"; // const-string v7, " :"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v7, p0, Lcom/miui/server/greeze/GreezeManagerService$5;->val$freeze:Z */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1399 */
return;
} // .end method
