.class public Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AurogonImmobulusMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/AurogonImmobulusMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImmobulusBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;


# direct methods
.method public constructor <init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V
    .locals 2
    .param p1, "this$0"    # Lcom/miui/server/greeze/AurogonImmobulusMode;

    .line 1363
    iput-object p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 1364
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1365
    .local v0, "intent":Landroid/content/IntentFilter;
    const-string v1, "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1366
    const-string v1, "android.intent.action.WALLPAPER_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1367
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1368
    const-string v1, "com.android.app.action.SATELLITE_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1369
    iget-object v1, p1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1370
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1375
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1376
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.xiaomi.joyose.action.GAME_STATUS_UPDATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_6

    .line 1377
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z

    if-nez v1, :cond_0

    return-void

    .line 1378
    :cond_0
    const-string v1, "com.xiaomi.joyose.key.GAME_STATUS"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1379
    .local v1, "type":I
    const-string v4, "com.xiaomi.joyose.key.BACKGROUND_FREEZE_WHITELIST"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1380
    .local v4, "allowList":[Ljava/lang/String;
    if-nez v4, :cond_1

    return-void

    .line 1381
    :cond_1
    array-length v5, v4

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 1382
    .local v6, "str":Ljava/lang/String;
    iget-object v7, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v7, v7, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1383
    iget-object v7, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v7, v7, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusAllowList:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1381
    .end local v6    # "str":Ljava/lang/String;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1387
    :cond_3
    if-ne v1, v2, :cond_4

    .line 1388
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-nez v2, :cond_5

    .line 1389
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    iget v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I

    const-string v5, "Game"

    invoke-virtual {v2, v3, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    goto :goto_1

    .line 1393
    :cond_4
    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z

    if-eqz v2, :cond_5

    .line 1396
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1398
    .end local v1    # "type":I
    .end local v4    # "allowList":[Ljava/lang/String;
    :cond_5
    :goto_1
    goto/16 :goto_3

    :cond_6
    const-string v1, "android.intent.action.WALLPAPER_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1399
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getWallpaperPackageName()V

    goto/16 :goto_3

    .line 1400
    :cond_7
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1401
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_9

    .line 1402
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mConnMgr:Landroid/net/ConnectivityManager;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1403
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    if-nez v1, :cond_8

    goto :goto_2

    :cond_8
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    :goto_2
    iput-boolean v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z

    .line 1404
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z

    if-eqz v2, :cond_9

    .line 1405
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateVpnStatus(Landroid/net/NetworkInfo;)V

    .line 1408
    .end local v1    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_9
    iget-object v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->handleMessageAppStatus()V

    goto :goto_3

    .line 1409
    :cond_a
    const-string v1, "com.android.app.action.SATELLITE_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1410
    const-string v1, "is_enable"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1411
    .local v1, "state":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " BROADCAST_SATELLITE state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Aurogon"

    invoke-static {v5, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    if-eqz v1, :cond_b

    .line 1413
    iget-object v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v2, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1414
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    const/16 v3, 0x3e8

    const-string v4, "ExtremeM_SATELLITE"

    invoke-virtual {v2, v3, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V

    goto :goto_3

    .line 1416
    :cond_b
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    if-eqz v2, :cond_c

    .line 1417
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iput-boolean v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z

    .line 1418
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V

    .line 1419
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    iget-object v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mGreezeService:Lcom/miui/server/greeze/GreezeManagerService;

    const-string v3, "SATELLITE"

    invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawuidsAll(Ljava/lang/String;)V

    .line 1420
    iget-object v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;->this$0:Lcom/miui/server/greeze/AurogonImmobulusMode;

    invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->simulateNetChange()V

    .line 1424
    .end local v1    # "state":Z
    :cond_c
    :goto_3
    return-void
.end method
