public class com.miui.server.greeze.RunningProcess {
	 /* .source "RunningProcess.java" */
	 /* # instance fields */
	 Integer adj;
	 Boolean hasForegroundActivities;
	 Boolean hasForegroundServices;
	 Integer pid;
	 java.lang.String pkgList;
	 Integer procState;
	 java.lang.String processName;
	 Integer uid;
	 /* # direct methods */
	 com.miui.server.greeze.RunningProcess ( ) {
		 /* .locals 1 */
		 /* .param p1, "info" # Landroid/app/ActivityManager$RunningAppProcessInfo; */
		 /* .line 29 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 30 */
		 /* iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
		 /* iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->pid:I */
		 /* .line 31 */
		 /* iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I */
		 /* iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->uid:I */
		 /* .line 32 */
		 v0 = this.processName;
		 this.processName = v0;
		 /* .line 33 */
		 v0 = this.pkgList;
		 this.pkgList = v0;
		 /* .line 34 */
		 return;
	 } // .end method
	 com.miui.server.greeze.RunningProcess ( ) {
		 /* .locals 1 */
		 /* .param p1, "info" # Lmiui/process/RunningProcessInfo; */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 19 */
		 /* iget v0, p1, Lmiui/process/RunningProcessInfo;->mPid:I */
		 /* iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->pid:I */
		 /* .line 20 */
		 /* iget v0, p1, Lmiui/process/RunningProcessInfo;->mUid:I */
		 /* iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->uid:I */
		 /* .line 21 */
		 v0 = this.mProcessName;
		 this.processName = v0;
		 /* .line 22 */
		 v0 = this.mPkgList;
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 v0 = this.mPkgList;
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
		 /* new-array v0, v0, [Ljava/lang/String; */
	 } // :goto_0
	 this.pkgList = v0;
	 /* .line 23 */
	 /* iget v0, p1, Lmiui/process/RunningProcessInfo;->mAdj:I */
	 /* iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->adj:I */
	 /* .line 24 */
	 /* iget v0, p1, Lmiui/process/RunningProcessInfo;->mProcState:I */
	 /* iput v0, p0, Lcom/miui/server/greeze/RunningProcess;->procState:I */
	 /* .line 25 */
	 /* iget-boolean v0, p1, Lmiui/process/RunningProcessInfo;->mHasForegroundActivities:Z */
	 /* iput-boolean v0, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z */
	 /* .line 26 */
	 /* iget-boolean v0, p1, Lmiui/process/RunningProcessInfo;->mHasForegroundServices:Z */
	 /* iput-boolean v0, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z */
	 /* .line 27 */
	 return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
	 /* .locals 3 */
	 /* .line 38 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 /* iget v1, p0, Lcom/miui/server/greeze/RunningProcess;->uid:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v1 = " "; // const-string v1, " "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v2, p0, Lcom/miui/server/greeze/RunningProcess;->pid:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.processName;
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v2 = this.pkgList;
	 java.util.Arrays .toString ( v2 );
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v2, p0, Lcom/miui/server/greeze/RunningProcess;->adj:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/miui/server/greeze/RunningProcess;->procState:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 /* .line 40 */
	 /* iget-boolean v1, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z */
	 final String v2 = ""; // const-string v2, ""
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 final String v1 = " FA"; // const-string v1, " FA"
	 } // :cond_0
	 /* move-object v1, v2 */
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 41 */
/* iget-boolean v1, p0, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
	 final String v2 = " FS"; // const-string v2, " FS"
} // :cond_1
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 38 */
} // .end method
