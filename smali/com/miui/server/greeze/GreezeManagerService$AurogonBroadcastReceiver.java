public class com.miui.server.greeze.GreezeManagerService$AurogonBroadcastReceiver extends android.content.BroadcastReceiver {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "AurogonBroadcastReceiver" */
} // .end annotation
/* # instance fields */
public java.lang.String actionUI;
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
public com.miui.server.greeze.GreezeManagerService$AurogonBroadcastReceiver ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .line 2249 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
/* .line 2248 */
final String v0 = "com.android.systemui.fsgesture"; // const-string v0, "com.android.systemui.fsgesture"
this.actionUI = v0;
/* .line 2250 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 2251 */
/* .local v0, "intent":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2252 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2253 */
v1 = this.actionUI;
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 2254 */
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmContext ( p1 );
(( android.content.Context ) v1 ).registerReceiver ( p0, v0 ); // invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 2255 */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 2259 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 2260 */
/* .local v0, "action":Ljava/lang/String; */
final String v1 = "android.intent.action.SCREEN_ON"; // const-string v1, "android.intent.action.SCREEN_ON"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
int v3 = 0; // const/4 v3, 0x0
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 2261 */
	 v1 = this.this$0;
	 /* iput-boolean v2, v1, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
	 /* .line 2262 */
	 v1 = this.this$0;
	 v1 = 	 v1 = this.mFreeformSmallWinList;
	 /* new-array v1, v1, [I */
	 /* .line 2263 */
	 /* .local v1, "uids":[I */
	 v2 = this.this$0;
	 v4 = this.mFreeformSmallWinList;
	 /* monitor-enter v4 */
	 /* .line 2264 */
	 try { // :try_start_0
		 v2 = this.this$0;
		 v2 = 		 v2 = this.mFreeformSmallWinList;
		 /* if-lez v2, :cond_0 */
		 /* .line 2265 */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .line 2266 */
		 /* .local v2, "i":I */
		 v5 = this.this$0;
		 v5 = this.mFreeformSmallWinList;
	 v6 = 	 } // :goto_0
	 if ( v6 != null) { // if-eqz v6, :cond_0
		 /* check-cast v6, Ljava/lang/String; */
		 /* .line 2267 */
		 /* .local v6, "name":Ljava/lang/String; */
		 v7 = this.this$0;
		 v7 = 		 (( com.miui.server.greeze.GreezeManagerService ) v7 ).getUidByPackageName ( v6 ); // invoke-virtual {v7, v6}, Lcom/miui/server/greeze/GreezeManagerService;->getUidByPackageName(Ljava/lang/String;)I
		 /* .line 2268 */
		 /* .local v7, "uid":I */
		 /* add-int/lit8 v8, v2, 0x1 */
	 } // .end local v2 # "i":I
	 /* .local v8, "i":I */
	 /* aput v7, v1, v2 */
	 /* .line 2269 */
} // .end local v6 # "name":Ljava/lang/String;
} // .end local v7 # "uid":I
/* move v2, v8 */
/* .line 2271 */
} // .end local v8 # "i":I
} // :cond_0
/* monitor-exit v4 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 2272 */
/* array-length v2, v1 */
/* if-lez v2, :cond_1 */
/* .line 2273 */
v2 = this.this$0;
/* const/16 v4, 0x3e8 */
/* const-string/jumbo v5, "smallw" */
(( com.miui.server.greeze.GreezeManagerService ) v2 ).thawUids ( v1, v4, v5 ); // invoke-virtual {v2, v1, v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 2274 */
} // :cond_1
v2 = this.this$0;
int v4 = 2; // const/4 v4, 0x2
(( com.miui.server.greeze.GreezeManagerService ) v2 ).dealAdjSet ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/miui/server/greeze/GreezeManagerService;->dealAdjSet(II)V
/* .line 2275 */
v2 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmHistoryLog ( v2 );
final String v3 = "SCREEN ON!"; // const-string v3, "SCREEN ON!"
(( android.util.LocalLog ) v2 ).log ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 2276 */
} // .end local v1 # "uids":[I
/* goto/16 :goto_1 */
/* .line 2271 */
/* .restart local v1 # "uids":[I */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v4 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
/* .line 2276 */
} // .end local v1 # "uids":[I
} // :cond_2
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 2277 */
v1 = this.this$0;
/* iput-boolean v3, v1, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
/* .line 2278 */
v1 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmHistoryLog ( v1 );
final String v2 = "SCREEN OFF!"; // const-string v2, "SCREEN OFF!"
(( android.util.LocalLog ) v1 ).log ( v2 ); // invoke-virtual {v1, v2}, Landroid/util/LocalLog;->log(Ljava/lang/String;)V
/* .line 2279 */
v1 = this.this$0;
v1 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).finishLaunchMode ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->finishLaunchMode()V
/* .line 2280 */
v1 = this.this$0;
v1 = this.mImmobulusMode;
/* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_9
v1 = this.this$0;
v1 = this.mImmobulusMode;
/* iget-boolean v1, v1, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* if-nez v1, :cond_9 */
/* .line 2281 */
v1 = this.this$0;
v1 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v1 ).quitImmobulusMode ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* goto/16 :goto_1 */
/* .line 2283 */
} // :cond_3
v1 = this.actionUI;
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 2284 */
/* const-string/jumbo v1, "typeFrom" */
(( android.content.Intent ) p2 ).getStringExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;
/* .line 2285 */
/* .local v1, "type":Ljava/lang/String; */
final String v4 = "isEnter"; // const-string v4, "isEnter"
v4 = (( android.content.Intent ) p2 ).getBooleanExtra ( v4, v3 ); // invoke-virtual {p2, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z
/* .line 2286 */
/* .local v4, "isEnter":Z */
if ( v1 != null) { // if-eqz v1, :cond_9
/* const-string/jumbo v5, "typefrom_status_bar_expansion" */
v5 = (( java.lang.String ) v5 ).equals ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 2287 */
v5 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fputisBarExpand ( v5,v4 );
/* .line 2288 */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 2289 */
v5 = this.this$0;
v5 = this.mHandler;
int v6 = 5; // const/4 v6, 0x5
(( android.os.Handler ) v5 ).sendEmptyMessage ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 2290 */
v5 = this.this$0;
v5 = this.mImmobulusMode;
/* iget-boolean v5, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* if-nez v5, :cond_4 */
v5 = this.this$0;
v5 = this.mImmobulusMode;
/* iget-boolean v5, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 2291 */
} // :cond_4
v5 = this.this$0;
v5 = this.mImmobulusMode;
(( com.miui.server.greeze.AurogonImmobulusMode ) v5 ).quitImmobulusMode ( ); // invoke-virtual {v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 2292 */
v5 = this.this$0;
v5 = this.mImmobulusMode;
/* iput-boolean v3, v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* .line 2293 */
v3 = this.this$0;
v3 = this.mImmobulusMode;
/* iput-boolean v2, v3, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z */
/* .line 2296 */
} // :cond_5
final String v5 = ""; // const-string v5, ""
/* .line 2298 */
/* .local v5, "ret":Ljava/lang/String; */
v6 = this.this$0;
/* iget v6, v6, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
v7 = this.this$0;
v7 = this.mImmobulusMode;
/* iget v7, v7, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I */
/* if-ne v6, v7, :cond_6 */
/* .line 2299 */
v6 = this.this$0;
v6 = this.mImmobulusMode;
/* iput-boolean v2, v6, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* .line 2300 */
final String v5 = "Camera"; // const-string v5, "Camera"
/* .line 2302 */
} // :cond_6
v2 = this.this$0;
v2 = this.mImmobulusMode;
/* iget-boolean v2, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z */
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 2303 */
v2 = this.this$0;
v2 = this.mImmobulusMode;
/* iput-boolean v3, v2, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z */
/* .line 2304 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "-BarExpand"; // const-string v3, "-BarExpand"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 2306 */
} // :cond_7
final String v2 = "Camera"; // const-string v2, "Camera"
v2 = (( java.lang.String ) v5 ).contains ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_8 */
final String v2 = "BarExpand"; // const-string v2, "BarExpand"
v2 = (( java.lang.String ) v5 ).contains ( v2 ); // invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_9
/* .line 2307 */
} // :cond_8
v2 = this.this$0;
v2 = this.mImmobulusMode;
v3 = this.this$0;
/* iget v3, v3, Lcom/miui/server/greeze/GreezeManagerService;->mTopAppUid:I */
(( com.miui.server.greeze.AurogonImmobulusMode ) v2 ).triggerImmobulusMode ( v3, v5 ); // invoke-virtual {v2, v3, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V
/* .line 2311 */
} // .end local v1 # "type":Ljava/lang/String;
} // .end local v4 # "isEnter":Z
} // .end local v5 # "ret":Ljava/lang/String;
} // :cond_9
} // :goto_1
return;
} // .end method
