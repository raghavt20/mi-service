public class com.miui.server.greeze.FreezeUtils {
	 /* .source "FreezeUtils.java" */
	 /* # static fields */
	 static Boolean DEBUG_CHECK_FREEZE;
	 static Boolean DEBUG_CHECK_THAW;
	 private static final java.lang.String FREEAER_FREEAE;
	 private static final java.lang.String FREEZER_CGROUP_FROZEN;
	 private static final java.lang.String FREEZER_CGROUP_THAWED;
	 private static final java.lang.String FREEZER_FROZEN_PORCS;
	 private static final java.lang.String FREEZER_ROOT_PATH;
	 private static final Integer FREEZE_ACTION;
	 private static final java.lang.String TAG;
	 private static final Integer UNFREEZE_ACTION;
	 private static final java.lang.String V1_FREEZER_CGROUP_FROZEN;
	 private static final java.lang.String V1_FREEZER_CGROUP_THAWED;
	 private static final java.lang.String V1_FREEZER_FROZEN_PORCS;
	 private static final java.lang.String V1_FREEZER_FROZEN_TASKS;
	 private static final java.lang.String V1_FREEZER_ROOT_PATH;
	 private static final java.lang.String V1_FREEZER_THAWED_PORCS;
	 private static final java.lang.String V1_FREEZER_THAWED_TASKS;
	 /* # direct methods */
	 static com.miui.server.greeze.FreezeUtils ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 int v0 = 1; // const/4 v0, 0x1
		 com.miui.server.greeze.FreezeUtils.DEBUG_CHECK_FREEZE = (v0!= 0);
		 /* .line 24 */
		 com.miui.server.greeze.FreezeUtils.DEBUG_CHECK_THAW = (v0!= 0);
		 return;
	 } // .end method
	 public com.miui.server.greeze.FreezeUtils ( ) {
		 /* .locals 0 */
		 /* .line 14 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Boolean freezePid ( Integer p0 ) {
		 /* .locals 4 */
		 /* .param p0, "pid" # I */
		 /* .line 126 */
		 /* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
		 final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 127 */
			 /* new-instance v0, Ljava/lang/StringBuilder; */
			 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
			 final String v2 = "Freeze pid "; // const-string v2, "Freeze pid "
			 (( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
			 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
			 android.util.Slog .d ( v1,v0 );
			 /* .line 129 */
		 } // :cond_0
		 final String v0 = "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"; // const-string v0, "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"
		 v0 = 		 com.miui.server.greeze.FreezeUtils .writeNode ( v0,p0 );
		 /* .line 130 */
		 /* .local v0, "done":Z */
		 /* sget-boolean v2, Lcom/miui/server/greeze/FreezeUtils;->DEBUG_CHECK_FREEZE:Z */
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 v2 = 				 com.miui.server.greeze.FreezeUtils .isFrozonPid ( p0 );
				 /* if-nez v2, :cond_1 */
				 /* .line 131 */
				 /* new-instance v2, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "Failed to thaw pid "; // const-string v3, "Failed to thaw pid "
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 final String v3 = ", it\'s still thawed!"; // const-string v3, ", it\'s still thawed!"
				 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .w ( v1,v2 );
				 /* .line 132 */
				 int v0 = 0; // const/4 v0, 0x0
				 /* .line 134 */
			 } // :cond_1
		 } // .end method
		 public static Boolean freezePid ( Integer p0, Integer p1 ) {
			 /* .locals 2 */
			 /* .param p0, "pid" # I */
			 /* .param p1, "uid" # I */
			 /* .line 101 */
			 /* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 102 */
				 /* new-instance v0, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v1 = "Freeze pid "; // const-string v1, "Freeze pid "
				 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
				 android.util.Slog .d ( v1,v0 );
				 /* .line 104 */
			 } // :cond_0
			 int v0 = 1; // const/4 v0, 0x1
			 v0 = 			 com.miui.server.greeze.FreezeUtils .setFreezeAction ( p0,p1,v0 );
			 /* .line 106 */
			 /* .local v0, "done":Z */
		 } // .end method
		 public static Boolean freezeTid ( Integer p0 ) {
			 /* .locals 2 */
			 /* .param p0, "tid" # I */
			 /* .line 139 */
			 /* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* .line 140 */
				 /* new-instance v0, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v1 = "Freeze tid "; // const-string v1, "Freeze tid "
				 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
				 android.util.Slog .d ( v1,v0 );
				 /* .line 142 */
			 } // :cond_0
			 final String v0 = "/sys/fs/cgroup/freezer/perf/frozen/tasks"; // const-string v0, "/sys/fs/cgroup/freezer/perf/frozen/tasks"
			 v0 = 			 com.miui.server.greeze.FreezeUtils .writeNode ( v0,p0 );
		 } // .end method
		 public static java.util.List getFrozenPids ( ) {
			 /* .locals 8 */
			 /* .annotation system Ldalvik/annotation/Signature; */
			 /* value = { */
			 /* "()", */
			 /* "Ljava/util/List<", */
			 /* "Ljava/lang/Integer;", */
			 /* ">;" */
			 /* } */
		 } // .end annotation
		 /* .line 45 */
		 final String v0 = "FreezeUtils"; // const-string v0, "FreezeUtils"
		 /* new-instance v1, Ljava/util/ArrayList; */
		 /* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
		 /* .line 46 */
		 /* .local v1, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
		 /* sget-boolean v2, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 final String v2 = "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"; // const-string v2, "/sys/fs/cgroup/freezer/perf/frozen/cgroup.procs"
		 } // :cond_0
		 final String v2 = "/sys/fs/cgroup/frozen/cgroup.procs"; // const-string v2, "/sys/fs/cgroup/frozen/cgroup.procs"
		 /* .line 47 */
		 /* .local v2, "path":Ljava/lang/String; */
	 } // :goto_0
	 try { // :try_start_0
		 /* new-instance v3, Ljava/io/BufferedReader; */
		 /* new-instance v4, Ljava/io/FileReader; */
		 /* invoke-direct {v4, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
		 /* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
		 /* :try_end_0 */
		 /* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
		 /* .line 49 */
		 /* .local v3, "reader":Ljava/io/BufferedReader; */
	 } // :goto_1
	 try { // :try_start_1
		 (( java.io.BufferedReader ) v3 ).readLine ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* move-object v5, v4 */
		 /* .local v5, "line":Ljava/lang/String; */
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* .line 51 */
			 try { // :try_start_2
				 v4 = 				 java.lang.Integer .parseInt ( v5 );
				 java.lang.Integer .valueOf ( v4 );
				 /* :try_end_2 */
				 /* .catch Ljava/lang/NumberFormatException; {:try_start_2 ..:try_end_2} :catch_0 */
				 /* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
				 /* .line 54 */
				 /* .line 52 */
				 /* :catch_0 */
				 /* move-exception v4 */
				 /* .line 53 */
				 /* .local v4, "e":Ljava/lang/NumberFormatException; */
				 try { // :try_start_3
					 /* new-instance v6, Ljava/lang/StringBuilder; */
					 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
					 final String v7 = "Failed to parse "; // const-string v7, "Failed to parse "
					 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 final String v7 = " line: "; // const-string v7, " line: "
					 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
					 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
					 android.util.Slog .w ( v0,v6,v4 );
					 /* :try_end_3 */
					 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
					 /* .line 54 */
					 /* nop */
				 } // .end local v4 # "e":Ljava/lang/NumberFormatException;
				 /* .line 56 */
			 } // .end local v5 # "line":Ljava/lang/String;
		 } // :cond_1
		 try { // :try_start_4
			 (( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
			 /* :try_end_4 */
			 /* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
			 /* .line 58 */
		 } // .end local v3 # "reader":Ljava/io/BufferedReader;
		 /* .line 47 */
		 /* .restart local v3 # "reader":Ljava/io/BufferedReader; */
		 /* :catchall_0 */
		 /* move-exception v4 */
		 try { // :try_start_5
			 (( java.io.BufferedReader ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
			 /* :try_end_5 */
			 /* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
			 /* :catchall_1 */
			 /* move-exception v5 */
			 try { // :try_start_6
				 (( java.lang.Throwable ) v4 ).addSuppressed ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
			 } // .end local v1 # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
		 } // .end local v2 # "path":Ljava/lang/String;
	 } // :goto_2
	 /* throw v4 */
	 /* :try_end_6 */
	 /* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_1 */
	 /* .line 56 */
} // .end local v3 # "reader":Ljava/io/BufferedReader;
/* .restart local v1 # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* .restart local v2 # "path":Ljava/lang/String; */
/* :catch_1 */
/* move-exception v3 */
/* .line 57 */
/* .local v3, "e":Ljava/io/IOException; */
final String v4 = "Failed to get frozen pids"; // const-string v4, "Failed to get frozen pids"
android.util.Slog .w ( v0,v4,v3 );
/* .line 59 */
} // .end local v3 # "e":Ljava/io/IOException;
} // :goto_3
} // .end method
public static java.util.List getFrozonTids ( ) {
/* .locals 7 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 64 */
final String v0 = "FreezeUtils"; // const-string v0, "FreezeUtils"
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 65 */
/* .local v1, "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
final String v4 = "/sys/fs/cgroup/freezer/perf/frozen/tasks"; // const-string v4, "/sys/fs/cgroup/freezer/perf/frozen/tasks"
/* invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .line 67 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 69 */
try { // :try_start_2
v3 = java.lang.Integer .parseInt ( v4 );
java.lang.Integer .valueOf ( v3 );
/* :try_end_2 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 72 */
/* .line 70 */
/* :catch_0 */
/* move-exception v3 */
/* .line 71 */
/* .local v3, "e":Ljava/lang/NumberFormatException; */
try { // :try_start_3
	 /* new-instance v5, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v6 = "Failed to parse /sys/fs/cgroup/freezer/perf/frozen/tasks line: "; // const-string v6, "Failed to parse /sys/fs/cgroup/freezer/perf/frozen/tasks line: "
	 (( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .w ( v0,v5,v3 );
	 /* :try_end_3 */
	 /* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
	 /* .line 72 */
	 /* nop */
} // .end local v3 # "e":Ljava/lang/NumberFormatException;
/* .line 74 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_4
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .line 76 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .line 65 */
/* .restart local v2 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_5
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_6
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :goto_1
/* throw v3 */
/* :try_end_6 */
/* .catch Ljava/io/IOException; {:try_start_6 ..:try_end_6} :catch_1 */
/* .line 74 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .restart local v1 # "tids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* :catch_1 */
/* move-exception v2 */
/* .line 75 */
/* .local v2, "e":Ljava/io/IOException; */
final String v3 = "Failed to get frozen tids"; // const-string v3, "Failed to get frozen tids"
android.util.Slog .w ( v0,v3,v2 );
/* .line 77 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
} // .end method
public static Boolean isAllFrozon ( Integer[] p0 ) {
/* .locals 6 */
/* .param p0, "pids" # [I */
/* .line 86 */
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
/* .line 87 */
/* .local v0, "frozen":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* array-length v1, p0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_1 */
/* aget v4, p0, v3 */
/* .line 88 */
/* .local v4, "pid":I */
v5 = java.lang.Integer .valueOf ( v4 );
/* if-nez v5, :cond_0 */
/* .line 89 */
/* .line 87 */
} // .end local v4 # "pid":I
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 92 */
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // .end method
public static Boolean isFreezerEnable ( ) {
/* .locals 3 */
/* .line 39 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/fs/cgroup/frozen"; // const-string v1, "/sys/fs/cgroup/frozen"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 40 */
/* .local v0, "groupFrozen":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/fs/cgroup/unfrozen"; // const-string v2, "/sys/fs/cgroup/unfrozen"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 41 */
/* .local v1, "groupThawed":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
public static Boolean isFrozonPid ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "pid" # I */
/* .line 82 */
com.miui.server.greeze.FreezeUtils .getFrozenPids ( );
v0 = java.lang.Integer .valueOf ( p0 );
} // .end method
static Boolean isFrozonTid ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "tid" # I */
/* .line 96 */
com.miui.server.greeze.FreezeUtils .getFrozonTids ( );
v0 = java.lang.Integer .valueOf ( p0 );
} // .end method
private static Boolean setFreezeAction ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 5 */
/* .param p0, "pid" # I */
/* .param p1, "uid" # I */
/* .param p2, "allow" # Z */
/* .line 110 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/sys/fs/cgroup/uid_"; // const-string v1, "/sys/fs/cgroup/uid_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "/pid_"; // const-string v1, "/pid_"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "/cgroup.freeze"; // const-string v1, "/cgroup.freeze"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 111 */
/* .local v0, "path":Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* new-instance v2, Ljava/io/PrintWriter; */
/* invoke-direct {v2, v0}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 112 */
/* .local v2, "writer":Ljava/io/PrintWriter; */
int v3 = 1; // const/4 v3, 0x1
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 113 */
try { // :try_start_1
java.lang.Integer .toString ( v3 );
(( java.io.PrintWriter ) v2 ).write ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
/* .line 115 */
} // :cond_0
java.lang.Integer .toString ( v1 );
(( java.io.PrintWriter ) v2 ).write ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 117 */
} // :goto_0
/* nop */
/* .line 118 */
try { // :try_start_2
(( java.io.PrintWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 117 */
/* .line 111 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.PrintWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "path":Ljava/lang/String;
} // .end local p0 # "pid":I
} // .end local p1 # "uid":I
} // .end local p2 # "allow":Z
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 118 */
} // .end local v2 # "writer":Ljava/io/PrintWriter;
/* .restart local v0 # "path":Ljava/lang/String; */
/* .restart local p0 # "pid":I */
/* .restart local p1 # "uid":I */
/* .restart local p2 # "allow":Z */
/* :catch_0 */
/* move-exception v2 */
/* .line 119 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to write to "; // const-string v4, "Failed to write to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " type = "; // const-string v4, " type = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "FreezeUtils"; // const-string v4, "FreezeUtils"
android.util.Slog .d ( v4,v3 );
/* .line 121 */
} // .end local v2 # "e":Ljava/io/IOException;
} // .end method
public static Boolean thawPid ( Integer p0 ) {
/* .locals 4 */
/* .param p0, "pid" # I */
/* .line 157 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 158 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Thaw pid "; // const-string v2, "Thaw pid "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v0 );
/* .line 160 */
} // :cond_0
final String v0 = "/sys/fs/cgroup/freezer/perf/thawed/cgroup.procs"; // const-string v0, "/sys/fs/cgroup/freezer/perf/thawed/cgroup.procs"
v0 = com.miui.server.greeze.FreezeUtils .writeNode ( v0,p0 );
/* .line 161 */
/* .local v0, "done":Z */
/* sget-boolean v2, Lcom/miui/server/greeze/FreezeUtils;->DEBUG_CHECK_THAW:Z */
if ( v2 != null) { // if-eqz v2, :cond_1
if ( v0 != null) { // if-eqz v0, :cond_1
v2 = com.miui.server.greeze.FreezeUtils .isFrozonPid ( p0 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 162 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to thaw pid "; // const-string v3, "Failed to thaw pid "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", it\'s still frozen!"; // const-string v3, ", it\'s still frozen!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 163 */
int v0 = 0; // const/4 v0, 0x0
/* .line 165 */
} // :cond_1
} // .end method
public static Boolean thawPid ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p0, "pid" # I */
/* .param p1, "uid" # I */
/* .line 147 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 148 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Thaw pid "; // const-string v1, "Thaw pid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
android.util.Slog .d ( v1,v0 );
/* .line 150 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
v0 = com.miui.server.greeze.FreezeUtils .setFreezeAction ( p0,p1,v0 );
/* .line 152 */
/* .local v0, "done":Z */
} // .end method
public static Boolean thawTid ( Integer p0 ) {
/* .locals 2 */
/* .param p0, "tid" # I */
/* .line 170 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 171 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Thaw tid "; // const-string v1, "Thaw tid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
android.util.Slog .d ( v1,v0 );
/* .line 173 */
} // :cond_0
final String v0 = "/sys/fs/cgroup/freezer/perf/thawed/tasks"; // const-string v0, "/sys/fs/cgroup/freezer/perf/thawed/tasks"
v0 = com.miui.server.greeze.FreezeUtils .writeNode ( v0,p0 );
} // .end method
public static Boolean writeFreezeValue ( ) {
/* .locals 2 */
/* .line 177 */
final String v0 = "/sys/fs/cgroup/frozen/cgroup.freeze"; // const-string v0, "/sys/fs/cgroup/frozen/cgroup.freeze"
int v1 = 1; // const/4 v1, 0x1
v0 = com.miui.server.greeze.FreezeUtils .writeNode ( v0,v1 );
} // .end method
private static Boolean writeNode ( java.lang.String p0, Integer p1 ) {
/* .locals 5 */
/* .param p0, "path" # Ljava/lang/String; */
/* .param p1, "val" # I */
/* .line 181 */
final String v0 = " with value "; // const-string v0, " with value "
final String v1 = "FreezeUtils"; // const-string v1, "FreezeUtils"
try { // :try_start_0
/* new-instance v2, Ljava/io/PrintWriter; */
/* invoke-direct {v2, p0}, Ljava/io/PrintWriter;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 182 */
/* .local v2, "writer":Ljava/io/PrintWriter; */
try { // :try_start_1
java.lang.Integer .toString ( p1 );
(( java.io.PrintWriter ) v2 ).write ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V
/* .line 183 */
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 184 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Wrote to "; // const-string v4, "Wrote to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 186 */
} // :cond_0
/* nop */
/* .line 187 */
try { // :try_start_2
(( java.io.PrintWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 186 */
int v0 = 1; // const/4 v0, 0x1
/* .line 181 */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.PrintWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local p0 # "path":Ljava/lang/String;
} // .end local p1 # "val":I
} // :goto_0
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 187 */
} // .end local v2 # "writer":Ljava/io/PrintWriter;
/* .restart local p0 # "path":Ljava/lang/String; */
/* .restart local p1 # "val":I */
/* :catch_0 */
/* move-exception v2 */
/* .line 188 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Failed to write to "; // const-string v4, "Failed to write to "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0,v2 );
/* .line 190 */
} // .end local v2 # "e":Ljava/io/IOException;
int v0 = 0; // const/4 v0, 0x0
} // .end method
