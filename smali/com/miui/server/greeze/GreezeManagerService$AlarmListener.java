class com.miui.server.greeze.GreezeManagerService$AlarmListener implements android.app.AlarmManager$OnAlarmListener {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AlarmListener" */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.greeze.GreezeManagerService$AlarmListener ( ) {
/* .locals 0 */
/* .line 348 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.greeze.GreezeManagerService$AlarmListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$AlarmListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
return;
} // .end method
/* # virtual methods */
public void onAlarm ( ) {
/* .locals 2 */
/* .line 351 */
v0 = this.this$0;
v0 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmRegisteredMonitor ( v0 );
int v1 = 7; // const/4 v1, 0x7
/* and-int/2addr v0, v1 */
/* if-ne v0, v1, :cond_0 */
/* .line 352 */
final String v0 = "GreezeManager"; // const-string v0, "GreezeManager"
final String v1 = "loop handshake restore"; // const-string v1, "loop handshake restore"
android.util.Slog .d ( v0,v1 );
/* .line 353 */
return;
/* .line 355 */
} // :cond_0
v0 = this.this$0;
v1 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmLoopCount ( v0 );
/* add-int/lit8 v1, v1, 0x1 */
com.miui.server.greeze.GreezeManagerService .-$$Nest$fputmLoopCount ( v0,v1 );
/* .line 356 */
v0 = this.this$0;
v0 = com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetmLoopCount ( v0 );
/* rem-int/lit8 v0, v0, 0x2 */
/* if-nez v0, :cond_1 */
/* .line 357 */
/* const-string/jumbo v0, "sys.millet.monitor" */
final String v1 = "1"; // const-string v1, "1"
android.os.SystemProperties .set ( v0,v1 );
/* .line 359 */
} // :cond_1
v0 = this.this$0;
v0 = this.mHandler;
int v1 = 4; // const/4 v1, 0x4
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 360 */
} // :goto_0
v0 = this.this$0;
com.miui.server.greeze.GreezeManagerService .-$$Nest$msetLoopAlarm ( v0 );
/* .line 361 */
return;
} // .end method
