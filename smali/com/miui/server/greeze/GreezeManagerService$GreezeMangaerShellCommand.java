class com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand extends android.os.ShellCommand {
	 /* .source "GreezeManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "GreezeMangaerShellCommand" */
} // .end annotation
/* # instance fields */
com.miui.server.greeze.GreezeManagerService mService;
/* # direct methods */
 com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ( ) {
/* .locals 0 */
/* .param p1, "service" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .line 2928 */
/* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
/* .line 2929 */
this.mService = p1;
/* .line 2930 */
return;
} // .end method
private void dumpSkipUid ( ) {
/* .locals 7 */
/* .line 2982 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 2983 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "audio uid: "; // const-string v2, "audio uid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.greeze.GreezeServiceUtils .getAudioUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2984 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "ime uid: "; // const-string v2, "ime uid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
com.miui.server.greeze.GreezeServiceUtils .getIMEUid ( );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2986 */
try { // :try_start_0
	 miui.process.ProcessManager .getForegroundInfo ( );
	 /* .line 2987 */
	 /* .local v1, "foregroundInfo":Lmiui/process/ForegroundInfo; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "foreground uid: "; // const-string v3, "foreground uid: "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, v1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* .line 2988 */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "multi window uid: "; // const-string v3, "multi window uid: "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, v1, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I */
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 2991 */
} // .end local v1 # "foregroundInfo":Lmiui/process/ForegroundInfo;
/* .line 2989 */
/* :catch_0 */
/* move-exception v1 */
/* .line 2990 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "ShellCommand"; // const-string v2, "ShellCommand"
final String v3 = "Failed to get foreground info from ProcessManager"; // const-string v3, "Failed to get foreground info from ProcessManager"
android.util.Slog .w ( v2,v3,v1 );
/* .line 2992 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 2993 */
/* .local v1, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* new-instance v2, Landroid/util/ArraySet; */
/* invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V */
/* .line 2994 */
/* .local v2, "foreActs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
/* new-instance v3, Landroid/util/ArraySet; */
/* invoke-direct {v3}, Landroid/util/ArraySet;-><init>()V */
/* .line 2995 */
/* .local v3, "foreSvcs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;" */
v5 = } // :goto_1
if ( v5 != null) { // if-eqz v5, :cond_2
/* check-cast v5, Lcom/miui/server/greeze/RunningProcess; */
/* .line 2996 */
/* .local v5, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* iget-boolean v6, v5, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z */
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 2997 */
/* iget v6, v5, Lcom/miui/server/greeze/RunningProcess;->uid:I */
java.lang.Integer .valueOf ( v6 );
/* .line 2999 */
} // :cond_0
/* iget-boolean v6, v5, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 3000 */
/* iget v6, v5, Lcom/miui/server/greeze/RunningProcess;->uid:I */
java.lang.Integer .valueOf ( v6 );
/* .line 3002 */
} // .end local v5 # "proc":Lcom/miui/server/greeze/RunningProcess;
} // :cond_1
/* .line 3003 */
} // :cond_2
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fore act uid: "; // const-string v5, "fore act uid: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3004 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "fore svc uid: "; // const-string v5, "fore svc uid: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3005 */
return;
} // .end method
private void runDumpHistory ( ) {
/* .locals 4 */
/* .line 2933 */
v0 = this.mService;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutFileDescriptor ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
final String v3 = ""; // const-string v3, ""
(( com.miui.server.greeze.GreezeManagerService ) v0 ).dumpHistory ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->dumpHistory(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
/* .line 2934 */
return;
} // .end method
private void runDumpPackages ( ) {
/* .locals 12 */
/* .line 2947 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 2948 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
v1 = this.mService;
(( com.miui.server.greeze.GreezeManagerService ) v1 ).getPkgMap ( ); // invoke-virtual {v1}, Lcom/miui/server/greeze/GreezeManagerService;->getPkgMap()Lcom/android/internal/app/ProcessMap;
/* .line 2949 */
/* .local v1, "procMap":Lcom/android/internal/app/ProcessMap;, "Lcom/android/internal/app/ProcessMap<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
(( com.android.internal.app.ProcessMap ) v1 ).getMap ( ); // invoke-virtual {v1}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v2 ).keySet ( ); // invoke-virtual {v2}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_3
/* check-cast v3, Ljava/lang/String; */
/* .line 2950 */
/* .local v3, "pkgName":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "pkg "; // const-string v5, "pkg "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2951 */
(( com.android.internal.app.ProcessMap ) v1 ).getMap ( ); // invoke-virtual {v1}, Lcom/android/internal/app/ProcessMap;->getMap()Landroid/util/ArrayMap;
(( android.util.ArrayMap ) v4 ).get ( v3 ); // invoke-virtual {v4, v3}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v4, Landroid/util/SparseArray; */
/* .line 2952 */
/* .local v4, "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_1
v6 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* if-ge v5, v6, :cond_2 */
/* .line 2953 */
v6 = (( android.util.SparseArray ) v4 ).keyAt ( v5 ); // invoke-virtual {v4, v5}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 2954 */
/* .local v6, "uid":I */
(( android.util.SparseArray ) v4 ).valueAt ( v5 ); // invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v7, Ljava/util/List; */
/* .line 2955 */
/* .local v7, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* if-nez v7, :cond_0 */
/* .line 2956 */
/* .line 2958 */
} // :cond_0
v9 = } // :goto_2
if ( v9 != null) { // if-eqz v9, :cond_1
/* check-cast v9, Lcom/miui/server/greeze/RunningProcess; */
/* .line 2959 */
/* .local v9, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* new-instance v10, Ljava/lang/StringBuilder; */
/* invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V */
final String v11 = " "; // const-string v11, " "
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.greeze.RunningProcess ) v9 ).toString ( ); // invoke-virtual {v9}, Lcom/miui/server/greeze/RunningProcess;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v10 ).append ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v10 ).toString ( ); // invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v10 ); // invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2960 */
} // .end local v9 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* .line 2952 */
} // .end local v6 # "uid":I
} // .end local v7 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
} // :cond_1
} // :goto_3
/* add-int/lit8 v5, v5, 0x1 */
/* .line 2962 */
} // .end local v3 # "pkgName":Ljava/lang/String;
} // .end local v4 # "uids":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
} // .end local v5 # "i":I
} // :cond_2
/* .line 2963 */
} // :cond_3
return;
} // .end method
private void runDumpUids ( ) {
/* .locals 10 */
/* .line 2966 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 2967 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
com.miui.server.greeze.GreezeServiceUtils .getUidMap ( );
/* .line 2968 */
/* .local v1, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;" */
v2 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* .line 2969 */
/* .local v2, "N":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "uid total " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2970 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* .line 2971 */
v4 = (( android.util.SparseArray ) v1 ).keyAt ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/SparseArray;->keyAt(I)I
/* .line 2972 */
/* .local v4, "uid":I */
/* add-int/lit8 v5, v3, 0x1 */
java.lang.Integer .valueOf ( v5 );
java.lang.Integer .valueOf ( v4 );
/* filled-new-array {v5, v6}, [Ljava/lang/Object; */
final String v6 = "#%d uid %d"; // const-string v6, "#%d uid %d"
(( java.io.PrintWriter ) v0 ).printf ( v6, v5 ); // invoke-virtual {v0, v6, v5}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
/* .line 2973 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 2974 */
(( android.util.SparseArray ) v1 ).valueAt ( v3 ); // invoke-virtual {v1, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v5, Ljava/util/List; */
/* .line 2975 */
/* .local v5, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_0
/* check-cast v7, Lcom/miui/server/greeze/RunningProcess; */
/* .line 2976 */
/* .local v7, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
final String v9 = " "; // const-string v9, " "
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.greeze.RunningProcess ) v7 ).toString ( ); // invoke-virtual {v7}, Lcom/miui/server/greeze/RunningProcess;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v8 ); // invoke-virtual {v0, v8}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2977 */
} // .end local v7 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* .line 2970 */
} // .end local v4 # "uid":I
} // .end local v5 # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 2979 */
} // .end local v3 # "i":I
} // :cond_1
return;
} // .end method
private void runListProcesses ( ) {
/* .locals 6 */
/* .line 2937 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 2938 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
com.miui.server.greeze.GreezeServiceUtils .getProcessList ( );
/* .line 2939 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "process total "; // const-string v3, "process total "
v3 = (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 2940 */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
v3 = } // :goto_0
/* if-ge v2, v3, :cond_0 */
/* .line 2941 */
/* check-cast v3, Lcom/miui/server/greeze/RunningProcess; */
/* .line 2942 */
/* .local v3, "proc":Lcom/miui/server/greeze/RunningProcess; */
/* add-int/lit8 v4, v2, 0x1 */
java.lang.Integer .valueOf ( v4 );
(( com.miui.server.greeze.RunningProcess ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/miui/server/greeze/RunningProcess;->toString()Ljava/lang/String;
/* filled-new-array {v4, v5}, [Ljava/lang/Object; */
final String v5 = " #%d %s"; // const-string v5, " #%d %s"
(( java.io.PrintWriter ) v0 ).printf ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 2940 */
} // .end local v3 # "proc":Lcom/miui/server/greeze/RunningProcess;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 2944 */
} // .end local v2 # "i":I
} // :cond_0
return;
} // .end method
/* # virtual methods */
public Integer onCommand ( java.lang.String p0 ) {
/* .locals 12 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .line 3009 */
v0 = this.mService;
com.miui.server.greeze.GreezeManagerService .-$$Nest$mcheckPermission ( v0 );
/* .line 3010 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 3011 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
/* if-nez p1, :cond_0 */
/* .line 3012 */
v1 = (( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
/* .line 3015 */
} // :cond_0
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
v2 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* sparse-switch v2, :sswitch_data_0 */
} // :cond_1
/* goto/16 :goto_0 */
/* :sswitch_0 */
final String v2 = "monitor"; // const-string v2, "monitor"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 5; // const/4 v2, 0x5
/* goto/16 :goto_1 */
/* :sswitch_1 */
final String v2 = "history"; // const-string v2, "history"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x11 */
/* goto/16 :goto_1 */
/* :sswitch_2 */
/* const-string/jumbo v2, "thuid" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 2; // const/4 v2, 0x2
/* goto/16 :goto_1 */
/* :sswitch_3 */
/* const-string/jumbo v2, "thpid" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 3; // const/4 v2, 0x3
/* goto/16 :goto_1 */
/* :sswitch_4 */
final String v2 = "query"; // const-string v2, "query"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x8 */
/* goto/16 :goto_1 */
/* :sswitch_5 */
final String v2 = "fzuid"; // const-string v2, "fzuid"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v4 */
/* goto/16 :goto_1 */
/* :sswitch_6 */
final String v2 = "fzpid"; // const-string v2, "fzpid"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* move v2, v3 */
/* goto/16 :goto_1 */
/* :sswitch_7 */
final String v2 = "debug"; // const-string v2, "debug"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x10 */
/* goto/16 :goto_1 */
/* :sswitch_8 */
/* const-string/jumbo v2, "thaw" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 4; // const/4 v2, 0x4
/* goto/16 :goto_1 */
/* :sswitch_9 */
/* const-string/jumbo v2, "skip" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0xf */
/* goto/16 :goto_1 */
/* :sswitch_a */
final String v2 = "lsfz"; // const-string v2, "lsfz"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0xa */
/* goto/16 :goto_1 */
/* :sswitch_b */
final String v2 = "loop"; // const-string v2, "loop"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x12 */
/* goto/16 :goto_1 */
/* :sswitch_c */
/* const-string/jumbo v2, "uid" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0xb */
/* goto/16 :goto_1 */
/* :sswitch_d */
final String v2 = "pkg"; // const-string v2, "pkg"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0xd */
/* :sswitch_e */
final String v2 = "iso"; // const-string v2, "iso"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x15 */
/* :sswitch_f */
final String v2 = "ps"; // const-string v2, "ps"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0xc */
/* :sswitch_10 */
final String v2 = "ls"; // const-string v2, "ls"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x9 */
/* :sswitch_11 */
final String v2 = "getUids"; // const-string v2, "getUids"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x13 */
/* :sswitch_12 */
final String v2 = "callback"; // const-string v2, "callback"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0x14 */
/* :sswitch_13 */
final String v2 = "clearmonitor"; // const-string v2, "clearmonitor"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 7; // const/4 v2, 0x7
/* :sswitch_14 */
final String v2 = "enable"; // const-string v2, "enable"
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* const/16 v2, 0xe */
/* :sswitch_15 */
/* const-string/jumbo v2, "unmonitor" */
v2 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
int v2 = 6; // const/4 v2, 0x6
} // :goto_0
/* move v2, v1 */
} // :goto_1
/* packed-switch v2, :pswitch_data_0 */
/* .line 3104 */
v1 = (( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
/* goto/16 :goto_5 */
/* .line 3094 */
/* :pswitch_0 */
v2 = this.mService;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v2 );
/* monitor-enter v2 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 3095 */
try { // :try_start_1
v4 = this.mService;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v4 );
v4 = (( android.util.SparseArray ) v4 ).size ( ); // invoke-virtual {v4}, Landroid/util/SparseArray;->size()I
/* sub-int/2addr v4, v3 */
/* .local v4, "i":I */
} // :goto_2
/* if-ltz v4, :cond_3 */
/* .line 3096 */
v3 = this.mService;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v3 );
(( android.util.SparseArray ) v3 ).valueAt ( v4 ); // invoke-virtual {v3, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v3, Ljava/util/List; */
/* .line 3097 */
/* .local v3, "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* if-nez v3, :cond_2 */
/* .line 3098 */
} // :cond_2
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid= "; // const-string v6, " uid= "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = this.mService;
com.miui.server.greeze.GreezeManagerService .-$$Nest$fgetisoPids ( v6 );
v6 = (( android.util.SparseArray ) v6 ).keyAt ( v4 ); // invoke-virtual {v6, v4}, Landroid/util/SparseArray;->keyAt(I)I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " pids:"; // const-string v6, " pids:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v5 ); // invoke-virtual {v0, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3095 */
} // .end local v3 # "pids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :goto_3
/* add-int/lit8 v4, v4, -0x1 */
/* .line 3101 */
} // .end local v4 # "i":I
} // :cond_3
/* monitor-exit v2 */
/* .line 3102 */
/* .line 3101 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
} // .end local v0 # "pw":Ljava/io/PrintWriter;
} // .end local p0 # "this":Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;
} // .end local p1 # "cmd":Ljava/lang/String;
try { // :try_start_2
/* throw v3 */
/* .line 3090 */
/* .restart local v0 # "pw":Ljava/io/PrintWriter; */
/* .restart local p0 # "this":Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand; */
/* .restart local p1 # "cmd":Ljava/lang/String; */
/* :pswitch_1 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 3091 */
/* .local v2, "tmp_module":I */
v3 = this.mService;
/* new-instance v4, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback; */
/* invoke-direct {v4, v2}, Lcom/miui/server/greeze/GreezeManagerService$TmpCallback;-><init>(I)V */
(( com.miui.server.greeze.GreezeManagerService ) v3 ).registerCallback ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lcom/miui/server/greeze/GreezeManagerService;->registerCallback(Lmiui/greeze/IGreezeCallback;I)Z
/* .line 3092 */
/* .line 3085 */
} // .end local v2 # "tmp_module":I
/* :pswitch_2 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* .line 3086 */
/* .local v2, "module":I */
v3 = this.mService;
(( com.miui.server.greeze.GreezeManagerService ) v3 ).getFrozenUids ( v2 ); // invoke-virtual {v3, v2}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenUids(I)[I
/* .line 3087 */
/* .local v3, "rst":[I */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Frozen uids : "; // const-string v5, "Frozen uids : "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.util.Arrays .toString ( v3 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3088 */
/* nop */
/* .line 3108 */
} // .end local v2 # "module":I
} // .end local v3 # "rst":[I
} // :goto_4
/* goto/16 :goto_6 */
/* .line 3082 */
/* :pswitch_3 */
com.miui.server.greeze.GreezeManagerService .-$$Nest$smnLoopOnce ( );
/* .line 3083 */
/* .line 3079 */
/* :pswitch_4 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runDumpHistory()V */
/* .line 3080 */
/* .line 3073 */
/* :pswitch_5 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 3074 */
/* .local v2, "debug":Z */
com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_MILLET = (v2!= 0);
com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_LAUNCH_FROM_HOME = (v2!= 0);
com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG_AIDL = (v2!= 0);
com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG = (v2!= 0);
/* .line 3075 */
com.miui.server.greeze.GreezeManagerDebugConfig.DEBUG = (v2!= 0);
/* .line 3076 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "launch debug log enabled "; // const-string v5, "launch debug log enabled "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3077 */
/* .line 3070 */
} // .end local v2 # "debug":Z
/* :pswitch_6 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->dumpSkipUid()V */
/* .line 3071 */
/* .line 3065 */
/* :pswitch_7 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Boolean .parseBoolean ( v2 );
/* .line 3066 */
/* .local v2, "enable":Z */
com.miui.server.greeze.GreezeManagerDebugConfig.sEnable = (v2!= 0);
/* .line 3067 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "launch freeze enabled "; // const-string v5, "launch freeze enabled "
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) v0 ).println ( v3 ); // invoke-virtual {v0, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3068 */
/* .line 3062 */
} // .end local v2 # "enable":Z
/* :pswitch_8 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runDumpPackages()V */
/* .line 3063 */
/* .line 3059 */
/* :pswitch_9 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runListProcesses()V */
/* .line 3060 */
/* .line 3056 */
/* :pswitch_a */
/* invoke-direct {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->runDumpUids()V */
/* .line 3057 */
/* .line 3052 */
/* :pswitch_b */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
v3 = this.mService;
/* .line 3053 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
(( com.miui.server.greeze.GreezeManagerService ) v3 ).getFrozenPids ( v5 ); // invoke-virtual {v3, v5}, Lcom/miui/server/greeze/GreezeManagerService;->getFrozenPids(I)[I
/* .line 3052 */
java.util.Arrays .toString ( v3 );
(( java.io.PrintWriter ) v2 ).println ( v3 ); // invoke-virtual {v2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3054 */
/* .line 3048 */
/* :pswitch_c */
v2 = this.mService;
final String v3 = ""; // const-string v3, ""
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutFileDescriptor ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
(( com.miui.server.greeze.GreezeManagerService ) v2 ).dumpSettings ( v3, v5, v6 ); // invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->dumpSettings(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
/* .line 3049 */
v2 = this.mService;
final String v3 = ""; // const-string v3, ""
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutFileDescriptor ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutFileDescriptor()Ljava/io/FileDescriptor;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
(( com.miui.server.greeze.GreezeManagerService ) v2 ).dumpFrozen ( v3, v5, v6 ); // invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->dumpFrozen(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
/* .line 3050 */
/* .line 3045 */
/* :pswitch_d */
v2 = this.mService;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v3 = java.lang.Integer .parseInt ( v3 );
(( com.miui.server.greeze.GreezeManagerService ) v2 ).queryBinderState ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V
/* .line 3046 */
/* .line 3042 */
/* :pswitch_e */
v2 = this.mService;
(( com.miui.server.greeze.GreezeManagerService ) v2 ).clearMonitorNet ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/GreezeManagerService;->clearMonitorNet()V
/* .line 3043 */
/* .line 3039 */
/* :pswitch_f */
v2 = this.mService;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v3 = java.lang.Integer .parseInt ( v3 );
(( com.miui.server.greeze.GreezeManagerService ) v2 ).clearMonitorNet ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->clearMonitorNet(I)V
/* .line 3040 */
/* .line 3036 */
/* :pswitch_10 */
v2 = this.mService;
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v3 = java.lang.Integer .parseInt ( v3 );
(( com.miui.server.greeze.GreezeManagerService ) v2 ).monitorNet ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V
/* .line 3037 */
/* .line 3033 */
/* :pswitch_11 */
v2 = this.mService;
final String v3 = "ShellCommand: thaw all"; // const-string v3, "ShellCommand: thaw all"
/* const/16 v5, 0x270f */
(( com.miui.server.greeze.GreezeManagerService ) v2 ).thawAll ( v5, v5, v3 ); // invoke-virtual {v2, v5, v5, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(IILjava/lang/String;)Ljava/util/List;
/* .line 3034 */
/* .line 3029 */
/* :pswitch_12 */
v2 = this.mService;
/* new-array v3, v3, [I */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
/* aput v5, v3, v4 */
/* .line 3030 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
final String v6 = "cmd: thpid"; // const-string v6, "cmd: thpid"
/* .line 3029 */
(( com.miui.server.greeze.GreezeManagerService ) v2 ).thawPids ( v3, v5, v6 ); // invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;
/* .line 3031 */
/* .line 3025 */
/* :pswitch_13 */
v2 = this.mService;
/* new-array v3, v3, [I */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
/* aput v5, v3, v4 */
/* .line 3026 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v5 = java.lang.Integer .parseInt ( v5 );
final String v6 = "cmd: thuid"; // const-string v6, "cmd: thuid"
/* .line 3025 */
(( com.miui.server.greeze.GreezeManagerService ) v2 ).thawUids ( v3, v5, v6 ); // invoke-virtual {v2, v3, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 3027 */
/* .line 3021 */
/* :pswitch_14 */
v5 = this.mService;
/* new-array v6, v3, [I */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* aput v2, v6, v4 */
/* const-wide/16 v7, 0x0 */
/* .line 3022 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v9 = java.lang.Integer .parseInt ( v2 );
final String v10 = "cmd: fzpid"; // const-string v10, "cmd: fzpid"
/* .line 3021 */
/* invoke-virtual/range {v5 ..v10}, Lcom/miui/server/greeze/GreezeManagerService;->freezePids([IJILjava/lang/String;)Ljava/util/List; */
/* .line 3023 */
/* .line 3017 */
/* :pswitch_15 */
v5 = this.mService;
/* new-array v6, v3, [I */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v2 = java.lang.Integer .parseInt ( v2 );
/* aput v2, v6, v4 */
/* const-wide/16 v7, 0x0 */
/* .line 3018 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getNextArgRequired()Ljava/lang/String;
v9 = java.lang.Integer .parseInt ( v2 );
final String v10 = "cmd: fzuid"; // const-string v10, "cmd: fzuid"
int v11 = 0; // const/4 v11, 0x0
/* .line 3017 */
/* invoke-virtual/range {v5 ..v11}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List; */
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 3019 */
/* .line 3104 */
} // :goto_5
/* .line 3106 */
/* :catch_0 */
/* move-exception v2 */
/* .line 3107 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.io.PrintWriter ) v0 ).println ( v2 ); // invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 3109 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_6
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x50473f5f -> :sswitch_15 */
/* -0x4d6ada7d -> :sswitch_14 */
/* -0x1af33993 -> :sswitch_13 */
/* -0xa43dfbb -> :sswitch_12 */
/* -0x479d167 -> :sswitch_11 */
/* 0xd87 -> :sswitch_10 */
/* 0xe03 -> :sswitch_f */
/* 0x19885 -> :sswitch_e */
/* 0x1b1cc -> :sswitch_d */
/* 0x1c450 -> :sswitch_c */
/* 0x32c6a4 -> :sswitch_b */
/* 0x32d49b -> :sswitch_a */
/* 0x35e57f -> :sswitch_9 */
/* 0x364daa -> :sswitch_8 */
/* 0x5b09653 -> :sswitch_7 */
/* 0x5d68437 -> :sswitch_6 */
/* 0x5d696fc -> :sswitch_5 */
/* 0x66f18c8 -> :sswitch_4 */
/* 0x6939e97 -> :sswitch_3 */
/* 0x693b15c -> :sswitch_2 */
/* 0x373fe494 -> :sswitch_1 */
/* 0x49b0bd5a -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_15 */
/* :pswitch_14 */
/* :pswitch_13 */
/* :pswitch_12 */
/* :pswitch_11 */
/* :pswitch_10 */
/* :pswitch_f */
/* :pswitch_e */
/* :pswitch_d */
/* :pswitch_c */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 3114 */
(( com.miui.server.greeze.GreezeManagerService$GreezeMangaerShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/GreezeManagerService$GreezeMangaerShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 3115 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "Greeze manager (greezer) commands:"; // const-string v1, "Greeze manager (greezer) commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3116 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3117 */
final String v1 = " ls lsfz"; // const-string v1, " ls lsfz"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3118 */
final String v1 = " history"; // const-string v1, " history"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3119 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3120 */
final String v1 = " fzpid PID"; // const-string v1, " fzpid PID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3121 */
final String v1 = " fzuid UID"; // const-string v1, " fzuid UID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3122 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3123 */
final String v1 = " thpid PID"; // const-string v1, " thpid PID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3124 */
final String v1 = " thuid UID"; // const-string v1, " thuid UID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3125 */
final String v1 = " thaw"; // const-string v1, " thaw"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3126 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3127 */
final String v1 = " monitor/unmonitor UID"; // const-string v1, " monitor/unmonitor UID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3128 */
final String v1 = " clearmonitor"; // const-string v1, " clearmonitor"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3129 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3130 */
final String v1 = " query UID"; // const-string v1, " query UID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3131 */
final String v1 = " Query binder state in all processes of UID"; // const-string v1, " Query binder state in all processes of UID"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3132 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3133 */
final String v1 = " uid pkg ps"; // const-string v1, " uid pkg ps"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3134 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 3135 */
final String v1 = " enable true/false"; // const-string v1, " enable true/false"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 3136 */
return;
} // .end method
