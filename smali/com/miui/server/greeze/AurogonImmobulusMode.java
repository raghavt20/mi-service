public class com.miui.server.greeze.AurogonImmobulusMode {
	 /* .source "AurogonImmobulusMode.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;, */
	 /* Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;, */
	 /* Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String AUROGON_ENABLE;
private static final java.lang.String AUROGON_IMMOBULUS_SWITCH_PROPERTY;
private static final java.lang.String BLUETOOTH_GLOBAL_SETTING;
public static final java.lang.String BROADCAST_SATELLITE;
public static final Boolean CN_MODEL;
private static java.util.List ENABLE_DOUBLE_APP;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Boolean IMMOBULUS_ENABLED;
private static final java.lang.String IMMOBULUS_GAME_CONTROLLER;
private static final java.lang.String IMMOBULUS_GAME_KEY_ALLOW_LIST;
private static final java.lang.String IMMOBULUS_GAME_KEY_STATUS;
private static final Integer IMMOBULUS_GAME_VALUE_QUIT;
private static final Integer IMMOBULUS_GAME_VALUE_TRIGGER;
public static final Integer IMMOBULUS_LAUNCH_MODE_REPEAT_TIME;
private static final Integer IMMOBULUS_LEVEL_FORCESTOP;
public static final Integer IMMOBULUS_REPEAT_TIME;
private static final java.lang.String IMMOBULUS_SWITCH_SECURE_SETTING;
private static java.lang.String KEY_NO_RESTRICT_APP;
public static final Integer MSG_IMMOBULUS_MODE_QUIT_ACTION;
public static final Integer MSG_IMMOBULUS_MODE_TRIGGER_ACTION;
public static final Integer MSG_LAUNCH_MODE_QUIT_ACTION;
public static final Integer MSG_LAUNCH_MODE_TRIGGER_ACTION;
public static final Integer MSG_REMOVE_ALL_MESSAGE;
public static final Integer MSG_REPEAT_FREEZE_APP;
private static final java.lang.String PC_SECURITY_CENTER_EXTREME_MODE;
private static java.util.List REASON_FREEZE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static java.util.List REASON_UNFREEZE;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String SATELLITE_STATE;
public static final java.lang.String TAG;
private static final Long TIME_FREEZE_DURATION_UNUSEFUL;
private static final Long TIME_FREEZE_DURATION_USEFUL;
private static final Long TIME_LAUNCH_MODE_TIMEOUT;
public static final Integer TYPE_MODE_IMMOBULUS;
public static final Integer TYPE_MODE_LAUNCH;
public static java.util.List mMessageApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private java.util.List CTS_NAME;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
java.util.List mAllowWakeUpPackageNameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List mBluetoothUsingList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mBroadcastCtrlCloud;
public Integer mCameraUid;
private java.util.List mCloudAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public android.net.ConnectivityManager mConnMgr;
public android.content.Context mContext;
public Boolean mCtsModeOn;
public java.lang.String mCurrentIMEPacageName;
public Integer mCurrentIMEUid;
public java.lang.String mCurrentVideoPacageName;
public java.util.List mCurrentWidgetPackages;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mDoubleAppCtrlCloud;
public Boolean mEnabledLMCamera;
public Boolean mEnterIMCamera;
public Boolean mEnterIMVideo;
private Boolean mEnterIMVideoCloud;
public Boolean mEnterImmobulusMode;
public Boolean mExtremeMode;
private Boolean mExtremeModeCloud;
public java.util.Map mFgServiceAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
public com.miui.server.greeze.GreezeManagerService mGreezeService;
public com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler mHandler;
public java.util.List mImmobulusAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mImmobulusModeEnabled;
public java.util.List mImmobulusTargetList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/AurogonAppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mImportantAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mLastBarExpandIMStatus;
public java.lang.String mLastPackageName;
public Boolean mLaunchModeEnabled;
private java.util.List mLocalAllowList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List mMutiWindowsApp;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Set mNoRestrictAppSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List mOnWindowsAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.pm.PackageManager mPm;
public com.miui.server.process.ProcessManagerInternal mProcessManagerInternal;
public java.util.List mQuitImmobulusList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/AurogonAppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List mQuitLaunchModeList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/greeze/AurogonAppInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.miui.server.greeze.AurogonImmobulusMode$SettingsObserver mSettingsObserver;
public java.util.List mVPNAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public java.util.List mVideoAppList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public Boolean mVpnConnect;
public java.lang.String mWallPaperPackageName;
/* # direct methods */
static Boolean -$$Nest$fgetmExtremeModeCloud ( com.miui.server.greeze.AurogonImmobulusMode p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z */
} // .end method
static java.util.Set -$$Nest$fgetmNoRestrictAppSet ( com.miui.server.greeze.AurogonImmobulusMode p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mNoRestrictAppSet;
} // .end method
static void -$$Nest$mgetNoRestrictApps ( com.miui.server.greeze.AurogonImmobulusMode p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getNoRestrictApps()V */
return;
} // .end method
static void -$$Nest$mparseAurogonEnable ( com.miui.server.greeze.AurogonImmobulusMode p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->parseAurogonEnable()V */
return;
} // .end method
static void -$$Nest$mupdateCloudAllowList ( com.miui.server.greeze.AurogonImmobulusMode p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCloudAllowList()V */
return;
} // .end method
static void -$$Nest$mupdateIMEAppStatus ( com.miui.server.greeze.AurogonImmobulusMode p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V */
return;
} // .end method
static java.lang.String -$$Nest$sfgetKEY_NO_RESTRICT_APP ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.greeze.AurogonImmobulusMode.KEY_NO_RESTRICT_APP;
} // .end method
static com.miui.server.greeze.AurogonImmobulusMode ( ) {
/* .locals 8 */
/* .line 88 */
final String v0 = "MILLET_NO_RESTRICT_APP"; // const-string v0, "MILLET_NO_RESTRICT_APP"
/* .line 94 */
final String v0 = "persist.sys.aurogon.immobulus"; // const-string v0, "persist.sys.aurogon.immobulus"
int v1 = 1; // const/4 v1, 0x1
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.greeze.AurogonImmobulusMode.IMMOBULUS_ENABLED = (v0!= 0);
/* .line 95 */
final String v0 = "ro.miui.region"; // const-string v0, "ro.miui.region"
/* const-string/jumbo v1, "unknown" */
android.os.SystemProperties .get ( v0,v1 );
final String v1 = "CN"; // const-string v1, "CN"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.miui.server.greeze.AurogonImmobulusMode.CN_MODEL = (v0!= 0);
/* .line 143 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "IMMOBULUS"; // const-string v1, "IMMOBULUS"
final String v2 = "LAUNCH_MODE"; // const-string v2, "LAUNCH_MODE"
/* filled-new-array {v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 144 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "SYNC_BINDER"; // const-string v1, "SYNC_BINDER"
final String v2 = "ASYNC_BINDER"; // const-string v2, "ASYNC_BINDER"
final String v3 = "PACKET"; // const-string v3, "PACKET"
final String v4 = "BINDER_SERVICE"; // const-string v4, "BINDER_SERVICE"
final String v5 = "SIGNAL"; // const-string v5, "SIGNAL"
final String v6 = "BROADCAST"; // const-string v6, "BROADCAST"
/* filled-new-array/range {v1 ..v6}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 146 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "babylon"; // const-string v1, "babylon"
final String v2 = "fuxi"; // const-string v2, "fuxi"
final String v3 = "nuwa"; // const-string v3, "nuwa"
final String v4 = "ishtar"; // const-string v4, "ishtar"
/* filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 147 */
/* new-instance v0, Ljava/util/ArrayList; */
final String v1 = "com.tencent.mobileqq"; // const-string v1, "com.tencent.mobileqq"
final String v2 = "com.tencent.mm"; // const-string v2, "com.tencent.mm"
final String v3 = "com.tencent.tim"; // const-string v3, "com.tencent.tim"
final String v4 = "com.alibaba.android.rimet"; // const-string v4, "com.alibaba.android.rimet"
final String v5 = "com.ss.android.lark"; // const-string v5, "com.ss.android.lark"
final String v6 = "com.ss.android.lark.kami"; // const-string v6, "com.ss.android.lark.kami"
final String v7 = "com.tencent.wework"; // const-string v7, "com.tencent.wework"
/* filled-new-array/range {v1 ..v7}, [Ljava/lang/String; */
java.util.Arrays .asList ( v1 );
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
return;
} // .end method
public com.miui.server.greeze.AurogonImmobulusMode ( ) {
/* .locals 46 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "ht" # Landroid/os/HandlerThread; */
/* .param p3, "service" # Lcom/miui/server/greeze/GreezeManagerService; */
/* .line 185 */
/* move-object/from16 v0, p0 */
/* invoke-direct/range {p0 ..p0}, Ljava/lang/Object;-><init>()V */
/* .line 97 */
int v1 = 0; // const/4 v1, 0x0
this.mImmobulusAllowList = v1;
/* .line 98 */
this.mImmobulusTargetList = v1;
/* .line 99 */
this.mFgServiceAppList = v1;
/* .line 100 */
this.mVPNAppList = v1;
/* .line 102 */
this.mQuitImmobulusList = v1;
/* .line 103 */
this.mQuitLaunchModeList = v1;
/* .line 104 */
this.mBluetoothUsingList = v1;
/* .line 105 */
this.mContext = v1;
/* .line 106 */
int v2 = 1; // const/4 v2, 0x1
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 107 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 108 */
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* .line 109 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLastBarExpandIMStatus:Z */
/* .line 110 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z */
/* .line 112 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnabledLMCamera:Z */
/* .line 113 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* .line 115 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* .line 116 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z */
/* .line 117 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z */
/* .line 118 */
int v4 = -1; // const/4 v4, -0x1
/* iput v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I */
/* .line 120 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z */
/* .line 121 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z */
/* .line 122 */
this.mCurrentVideoPacageName = v1;
/* .line 124 */
/* iput-boolean v2, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mBroadcastCtrlCloud:Z */
/* .line 125 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z */
/* .line 126 */
this.mHandler = v1;
/* .line 127 */
this.mGreezeService = v1;
/* .line 129 */
this.mSettingsObserver = v1;
/* .line 131 */
this.mPm = v1;
/* .line 133 */
this.mProcessManagerInternal = v1;
/* .line 134 */
this.mConnMgr = v1;
/* .line 136 */
final String v2 = ""; // const-string v2, ""
this.mCurrentIMEPacageName = v2;
/* .line 137 */
/* iput v4, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEUid:I */
/* .line 138 */
this.mLastPackageName = v2;
/* .line 139 */
this.mWallPaperPackageName = v2;
/* .line 141 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
this.mCurrentWidgetPackages = v2;
/* .line 153 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v5 = "android.uid.shared"; // const-string v5, "android.uid.shared"
final String v6 = "com.miui.systemAdSolution"; // const-string v6, "com.miui.systemAdSolution"
final String v7 = "com.android.providers.media.module"; // const-string v7, "com.android.providers.media.module"
final String v8 = "com.google.android.webview"; // const-string v8, "com.google.android.webview"
final String v9 = "com.miui.voicetrigger"; // const-string v9, "com.miui.voicetrigger"
final String v10 = "com.miui.voiceassist"; // const-string v10, "com.miui.voiceassist"
final String v11 = "com.dewmobile.kuaiya"; // const-string v11, "com.dewmobile.kuaiya"
final String v12 = "com.android.permissioncontroller"; // const-string v12, "com.android.permissioncontroller"
final String v13 = "com.android.htmlviewer"; // const-string v13, "com.android.htmlviewer"
final String v14 = "com.google.android.providers.media.module"; // const-string v14, "com.google.android.providers.media.module"
final String v15 = "com.android.incallui"; // const-string v15, "com.android.incallui"
final String v16 = "org.codeaurora.ims"; // const-string v16, "org.codeaurora.ims"
final String v17 = "com.android.providers.contacts"; // const-string v17, "com.android.providers.contacts"
final String v18 = "com.xiaomi.xaee"; // const-string v18, "com.xiaomi.xaee"
final String v19 = "com.android.calllogbackup"; // const-string v19, "com.android.calllogbackup"
final String v20 = "com.android.providers.blockednumber"; // const-string v20, "com.android.providers.blockednumber"
final String v21 = "com.android.providers.userdictionary"; // const-string v21, "com.android.providers.userdictionary"
final String v22 = "com.xiaomi.aireco"; // const-string v22, "com.xiaomi.aireco"
final String v23 = "com.miui.securityinputmethod"; // const-string v23, "com.miui.securityinputmethod"
final String v24 = "com.miui.home"; // const-string v24, "com.miui.home"
final String v25 = "com.miui.newhome"; // const-string v25, "com.miui.newhome"
final String v26 = "com.miui.screenshot"; // const-string v26, "com.miui.screenshot"
final String v27 = "com.lbe.security.miui"; // const-string v27, "com.lbe.security.miui"
final String v28 = "org.ifaa.aidl.manager"; // const-string v28, "org.ifaa.aidl.manager"
final String v29 = "com.xiaomi.macro"; // const-string v29, "com.xiaomi.macro"
final String v30 = "com.miui.rom"; // const-string v30, "com.miui.rom"
final String v31 = "com.miui.personalassistant"; // const-string v31, "com.miui.personalassistant"
final String v32 = "com.miui.analytics"; // const-string v32, "com.miui.analytics"
final String v33 = "com.miui.mediaviewer"; // const-string v33, "com.miui.mediaviewer"
final String v34 = "com.xiaomi.gamecenter.sdk.service"; // const-string v34, "com.xiaomi.gamecenter.sdk.service"
final String v35 = "com.xiaomi.scanner"; // const-string v35, "com.xiaomi.scanner"
final String v36 = "com.android.cameraextensions"; // const-string v36, "com.android.cameraextensions"
final String v37 = "com.xiaomi.xmsf"; // const-string v37, "com.xiaomi.xmsf"
final String v38 = "com.xiaomi.account"; // const-string v38, "com.xiaomi.account"
final String v39 = "com.xiaomi.metoknlp"; // const-string v39, "com.xiaomi.metoknlp"
final String v40 = "com.duokan.reader"; // const-string v40, "com.duokan.reader"
final String v41 = "com.android.mms"; // const-string v41, "com.android.mms"
final String v42 = "com.xiaomi.mibrain.speech"; // const-string v42, "com.xiaomi.mibrain.speech"
final String v43 = "com.baidu.carlife.xiaomi"; // const-string v43, "com.baidu.carlife.xiaomi"
final String v44 = "com.xiaomi.miralink"; // const-string v44, "com.xiaomi.miralink"
final String v45 = "com.miui.core"; // const-string v45, "com.miui.core"
/* filled-new-array/range {v5 ..v45}, [Ljava/lang/String; */
java.util.Arrays .asList ( v5 );
/* invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mLocalAllowList = v2;
/* .line 170 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v5 = "com.ss.android.ugc.aweme.lite"; // const-string v5, "com.ss.android.ugc.aweme.lite"
final String v6 = "com.ss.android.ugc.aweme"; // const-string v6, "com.ss.android.ugc.aweme"
final String v7 = "com.kuaishou.nebula"; // const-string v7, "com.kuaishou.nebula"
final String v8 = "com.smile.gifmaker"; // const-string v8, "com.smile.gifmaker"
/* const-string/jumbo v9, "tv.danmaku.bili" */
final String v10 = "com.tencent.qqlive"; // const-string v10, "com.tencent.qqlive"
final String v11 = "com.ss.android.article.video"; // const-string v11, "com.ss.android.article.video"
final String v12 = "com.qiyi.video"; // const-string v12, "com.qiyi.video"
final String v13 = "com.youku.phone"; // const-string v13, "com.youku.phone"
final String v14 = "com.miui.video"; // const-string v14, "com.miui.video"
final String v15 = "com.duowan.kiwi"; // const-string v15, "com.duowan.kiwi"
final String v16 = "air.tv.douyu.android"; // const-string v16, "air.tv.douyu.android"
final String v17 = "com.le123.ysdq"; // const-string v17, "com.le123.ysdq"
final String v18 = "com.hunantv.imgo.activity"; // const-string v18, "com.hunantv.imgo.activity"
/* filled-new-array/range {v5 ..v18}, [Ljava/lang/String; */
java.util.Arrays .asList ( v5 );
/* invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mVideoAppList = v2;
/* .line 175 */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
this.mNoRestrictAppSet = v2;
/* .line 176 */
this.mCloudAllowList = v1;
/* .line 177 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
this.mAllowList = v2;
/* .line 180 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v5 = "com.goodix.fingerprint.setting"; // const-string v5, "com.goodix.fingerprint.setting"
final String v6 = "com.miui.gallery"; // const-string v6, "com.miui.gallery"
final String v7 = "com.miui.home"; // const-string v7, "com.miui.home"
final String v8 = "com.android.camera"; // const-string v8, "com.android.camera"
/* filled-new-array {v7, v8, v5, v6}, [Ljava/lang/String; */
java.util.Arrays .asList ( v5 );
/* invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mImportantAppList = v2;
/* .line 183 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v5 = "android.net.cts"; // const-string v5, "android.net.cts"
final String v6 = "com.android.cts.verifier"; // const-string v6, "com.android.cts.verifier"
/* filled-new-array {v5, v6}, [Ljava/lang/String; */
java.util.Arrays .asList ( v5 );
/* invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.CTS_NAME = v2;
/* .line 790 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
this.mOnWindowsAppList = v2;
/* .line 1261 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
this.mMutiWindowsApp = v2;
/* .line 1472 */
/* new-instance v2, Ljava/util/ArrayList; */
final String v5 = "com.tencent.mm"; // const-string v5, "com.tencent.mm"
final String v6 = "com.google.android.gms"; // const-string v6, "com.google.android.gms"
/* filled-new-array {v5, v6}, [Ljava/lang/String; */
java.util.Arrays .asList ( v5 );
/* invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mAllowWakeUpPackageNameList = v2;
/* .line 186 */
/* move-object/from16 v2, p1 */
this.mContext = v2;
/* .line 187 */
/* sget-boolean v5, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 188 */
/* new-instance v5, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler; */
/* invoke-virtual/range {p2 ..p2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper; */
/* invoke-direct {v5, v0, v6, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Looper;Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler-IA;)V */
this.mHandler = v5;
/* .line 189 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
this.mPm = v1;
/* .line 190 */
/* move-object/from16 v1, p3 */
this.mGreezeService = v1;
/* .line 191 */
/* new-instance v5, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver; */
v6 = this.mHandler;
/* invoke-direct {v5, v0, v6}, Lcom/miui/server/greeze/AurogonImmobulusMode$SettingsObserver;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Landroid/os/Handler;)V */
this.mSettingsObserver = v5;
/* .line 192 */
v5 = this.mAllowList;
v6 = this.mLocalAllowList;
/* .line 193 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->init()V */
/* .line 194 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "RECORD_BLE_APPNAME"; // const-string v6, "RECORD_BLE_APPNAME"
android.provider.Settings$Global .getUriFor ( v6 );
v7 = this.mSettingsObserver;
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v4 ); // invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 196 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "immobulus_mode_switch_restrict"; // const-string v6, "immobulus_mode_switch_restrict"
android.provider.Settings$Secure .getUriFor ( v6 );
v7 = this.mSettingsObserver;
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v4 ); // invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 198 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "default_input_method"; // const-string v6, "default_input_method"
android.provider.Settings$Secure .getUriFor ( v6 );
v7 = this.mSettingsObserver;
int v8 = -2; // const/4 v8, -0x2
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v8 ); // invoke-virtual {v5, v6, v3, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 200 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v6 = android.provider.MiuiSettings$Secure.MIUI_OPTIMIZATION;
android.provider.Settings$Secure .getUriFor ( v6 );
v7 = this.mSettingsObserver;
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v8 ); // invoke-virtual {v5, v6, v3, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 202 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "aurogon_enable"; // const-string v6, "aurogon_enable"
android.provider.Settings$Global .getUriFor ( v6 );
v7 = this.mSettingsObserver;
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v4 ); // invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 204 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v6 = com.miui.server.greeze.AurogonImmobulusMode.KEY_NO_RESTRICT_APP;
android.provider.Settings$System .getUriFor ( v6 );
v7 = this.mSettingsObserver;
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v4 ); // invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 207 */
v5 = this.mContext;
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "enabled_widgets"; // const-string v6, "enabled_widgets"
android.provider.Settings$Secure .getUriFor ( v6 );
v7 = this.mSettingsObserver;
(( android.content.ContentResolver ) v5 ).registerContentObserver ( v6, v3, v7, v4 ); // invoke-virtual {v5, v6, v3, v7, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 210 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "pc_security_center_extreme_mode_enter"; // const-string v5, "pc_security_center_extreme_mode_enter"
android.provider.Settings$Secure .getUriFor ( v5 );
v6 = this.mSettingsObserver;
(( android.content.ContentResolver ) v4 ).registerContentObserver ( v5, v3, v6, v8 ); // invoke-virtual {v4, v5, v3, v6, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 213 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v5 = "satellite_state"; // const-string v5, "satellite_state"
android.provider.Settings$System .getUriFor ( v5 );
v6 = this.mSettingsObserver;
(( android.content.ContentResolver ) v4 ).registerContentObserver ( v5, v3, v6, v8 ); // invoke-virtual {v4, v5, v3, v6, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 216 */
/* const-class v4, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v4 );
/* check-cast v4, Lcom/miui/server/process/ProcessManagerInternal; */
this.mProcessManagerInternal = v4;
/* .line 217 */
/* new-instance v4, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver; */
/* invoke-direct {v4, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusBroadcastReceiver;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V */
/* .line 218 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCloudAllowList()V */
/* .line 219 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V */
/* .line 220 */
/* invoke-virtual/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateWidgetPackages()V */
/* .line 222 */
/* invoke-virtual/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getConnectivityManager()V */
/* .line 223 */
v4 = this.mGreezeService;
v4 = this.DISABLE_IMMOB_MODE_DEVICE;
v4 = v5 = android.os.Build.DEVICE;
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 224 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 226 */
} // :cond_0
/* move-object/from16 v1, p3 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 227 */
/* iput-boolean v3, v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 229 */
} // :cond_1
} // :goto_0
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getNoRestrictApps()V */
/* .line 230 */
return;
} // .end method
private Boolean InStatusBarScene ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 274 */
v0 = this.mGreezeService;
v0 = this.mTopAppPackageName;
final String v1 = "com.milink.service"; // const-string v1, "com.milink.service"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 275 */
final String v0 = "com.xiaomi.smarthome"; // const-string v0, "com.xiaomi.smarthome"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 276 */
int v0 = 1; // const/4 v0, 0x1
/* .line 277 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void checkFgServicesList ( ) {
/* .locals 9 */
/* .line 757 */
v0 = this.mProcessManagerInternal;
/* if-nez v0, :cond_0 */
return;
/* .line 759 */
} // :cond_0
v0 = this.mFgServiceAppList;
/* monitor-enter v0 */
/* .line 760 */
try { // :try_start_0
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 761 */
/* .local v1, "tempUidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v2 = this.mFgServiceAppList;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 762 */
/* .local v3, "uid":I */
v4 = this.mFgServiceAppList;
java.lang.Integer .valueOf ( v3 );
/* check-cast v4, Ljava/util/List; */
/* .line 763 */
/* .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 764 */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 765 */
/* .local v5, "tempList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v7 = } // :goto_1
if ( v7 != null) { // if-eqz v7, :cond_2
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 766 */
/* .local v7, "pid":I */
v8 = this.mProcessManagerInternal;
v8 = (( com.miui.server.process.ProcessManagerInternal ) v8 ).checkAppFgServices ( v7 ); // invoke-virtual {v8, v7}, Lcom/miui/server/process/ProcessManagerInternal;->checkAppFgServices(I)Z
/* if-nez v8, :cond_1 */
/* .line 767 */
java.lang.Integer .valueOf ( v7 );
/* .line 769 */
} // .end local v7 # "pid":I
} // :cond_1
/* .line 770 */
} // :cond_2
v7 = } // :goto_2
if ( v7 != null) { // if-eqz v7, :cond_3
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* .line 771 */
/* .local v7, "tempPid":I */
java.lang.Integer .valueOf ( v7 );
/* .line 772 */
/* nop */
} // .end local v7 # "tempPid":I
/* .line 773 */
v6 = } // :cond_3
/* if-nez v6, :cond_4 */
/* .line 774 */
java.lang.Integer .valueOf ( v3 );
/* .line 777 */
} // .end local v3 # "uid":I
} // .end local v4 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // .end local v5 # "tempList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_4
/* .line 778 */
} // :cond_5
v3 = } // :goto_3
if ( v3 != null) { // if-eqz v3, :cond_6
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
/* .line 779 */
/* .local v3, "tempUid":I */
v4 = this.mFgServiceAppList;
java.lang.Integer .valueOf ( v3 );
/* .line 780 */
/* nop */
} // .end local v3 # "tempUid":I
/* .line 781 */
} // .end local v1 # "tempUidList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_6
/* monitor-exit v0 */
/* .line 782 */
return;
/* .line 781 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void getNoRestrictApps ( ) {
/* .locals 7 */
/* .line 1665 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = com.miui.server.greeze.AurogonImmobulusMode.KEY_NO_RESTRICT_APP;
android.provider.Settings$System .getString ( v0,v1 );
/* .line 1666 */
/* .local v0, "str":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getNoRestrictApps result="; // const-string v2, "getNoRestrictApps result="
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AurogonImmobulusMode"; // const-string v2, "AurogonImmobulusMode"
android.util.Slog .d ( v2,v1 );
/* .line 1667 */
if ( v0 != null) { // if-eqz v0, :cond_4
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-nez v1, :cond_0 */
/* .line 1671 */
} // :cond_0
final String v1 = ","; // const-string v1, ","
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1672 */
/* .local v1, "apps":[Ljava/lang/String; */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_3 */
/* aget-object v5, v1, v4 */
/* .line 1673 */
/* .local v5, "app":Ljava/lang/String; */
(( java.lang.String ) v5 ).trim ( ); // invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
/* .line 1674 */
v6 = (( java.lang.String ) v5 ).length ( ); // invoke-virtual {v5}, Ljava/lang/String;->length()I
/* if-nez v6, :cond_1 */
/* .line 1675 */
/* .line 1676 */
} // :cond_1
v6 = v6 = this.mNoRestrictAppSet;
/* if-nez v6, :cond_2 */
/* .line 1677 */
v6 = this.mNoRestrictAppSet;
/* .line 1672 */
} // .end local v5 # "app":Ljava/lang/String;
} // :cond_2
} // :goto_1
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1679 */
} // :cond_3
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "mNoRestrictAppSet="; // const-string v4, "mNoRestrictAppSet="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mNoRestrictAppSet;
(( java.lang.Object ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1680 */
return;
/* .line 1668 */
} // .end local v1 # "apps":[Ljava/lang/String;
} // :cond_4
} // :goto_2
v1 = this.mNoRestrictAppSet;
/* .line 1669 */
return;
} // .end method
private java.lang.String getPackageNameFromUid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 984 */
int v0 = 0; // const/4 v0, 0x0
/* .line 985 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.mPm;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 986 */
(( android.content.pm.PackageManager ) v1 ).getNameForUid ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;
/* .line 988 */
} // :cond_0
/* if-nez v0, :cond_1 */
/* .line 989 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "get caller pkgname failed uid = "; // const-string v2, "get caller pkgname failed uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AurogonImmobulusMode"; // const-string v2, "AurogonImmobulusMode"
android.util.Slog .d ( v2,v1 );
/* .line 991 */
} // :cond_1
} // .end method
private void init ( ) {
/* .locals 1 */
/* .line 263 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mImmobulusAllowList = v0;
/* .line 264 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mImmobulusTargetList = v0;
/* .line 265 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mBluetoothUsingList = v0;
/* .line 266 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mQuitImmobulusList = v0;
/* .line 267 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mQuitLaunchModeList = v0;
/* .line 268 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCloudAllowList = v0;
/* .line 269 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mFgServiceAppList = v0;
/* .line 270 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mVPNAppList = v0;
/* .line 271 */
return;
} // .end method
private Boolean isSystemApp ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 930 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mPm;
(( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 931 */
/* .local v1, "info":Landroid/content/pm/ApplicationInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* iget v2, v1, Landroid/content/pm/ApplicationInfo;->flags:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
int v3 = 1; // const/4 v3, 0x1
/* and-int/2addr v2, v3 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* move v0, v3 */
} // :cond_0
/* .line 932 */
} // .end local v1 # "info":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 934 */
} // .end method
private void parseAurogonEnable ( ) {
/* .locals 15 */
/* .line 1683 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "aurogon_enable"; // const-string v1, "aurogon_enable"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 1684 */
/* .local v0, "str":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_a
v1 = (( java.lang.String ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* goto/16 :goto_4 */
/* .line 1685 */
} // :cond_0
final String v1 = ";"; // const-string v1, ";"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1686 */
/* .local v1, "cfs":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
/* move v4, v3 */
} // :goto_0
/* if-ge v4, v2, :cond_9 */
/* aget-object v5, v1, v4 */
/* .line 1687 */
/* .local v5, "cf":Ljava/lang/String; */
final String v6 = "extrememode:"; // const-string v6, "extrememode:"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* const-string/jumbo v7, "true" */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 1688 */
v6 = (( java.lang.String ) v5 ).contains ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* iput-boolean v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z */
/* .line 1689 */
} // :cond_1
final String v6 = "enterimvideo:"; // const-string v6, "enterimvideo:"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 1690 */
v6 = (( java.lang.String ) v5 ).contains ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* iput-boolean v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z */
/* .line 1691 */
} // :cond_2
final String v6 = "broadcastctrl:"; // const-string v6, "broadcastctrl:"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
int v8 = 1; // const/4 v8, 0x1
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 1692 */
v6 = this.mGreezeService;
/* if-nez v6, :cond_3 */
return;
/* .line 1693 */
} // :cond_3
final String v6 = "_"; // const-string v6, "_"
(( java.lang.String ) v5 ).split ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1694 */
/* .local v6, "ls":[Ljava/lang/String; */
/* array-length v9, v6 */
/* if-lez v9, :cond_4 */
/* .line 1695 */
v9 = this.mGreezeService;
/* aget-object v10, v6, v3 */
v7 = (( java.lang.String ) v10 ).contains ( v7 ); // invoke-virtual {v10, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
(( com.miui.server.greeze.GreezeManagerService ) v9 ).setBroadcastCtrl ( v7 ); // invoke-virtual {v9, v7}, Lcom/miui/server/greeze/GreezeManagerService;->setBroadcastCtrl(Z)V
/* .line 1696 */
} // :cond_4
/* array-length v7, v6 */
/* move v9, v3 */
} // :goto_1
/* if-ge v9, v7, :cond_6 */
/* aget-object v10, v6, v9 */
/* .line 1697 */
/* .local v10, "ua":Ljava/lang/String; */
final String v11 = "/"; // const-string v11, "/"
v12 = (( java.lang.String ) v10 ).contains ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v12 != null) { // if-eqz v12, :cond_5
/* .line 1698 */
(( java.lang.String ) v10 ).split ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1699 */
/* .local v11, "pkgAction":[Ljava/lang/String; */
/* array-length v12, v11 */
/* if-le v12, v8, :cond_5 */
/* .line 1700 */
v12 = this.mGreezeService;
(( com.miui.server.greeze.GreezeManagerService ) v12 ).getBroadcastConfig ( ); // invoke-virtual {v12}, Lcom/miui/server/greeze/GreezeManagerService;->getBroadcastConfig()Ljava/util/Map;
/* aget-object v13, v11, v3 */
/* aget-object v14, v11, v8 */
/* .line 1696 */
} // .end local v10 # "ua":Ljava/lang/String;
} // .end local v11 # "pkgAction":[Ljava/lang/String;
} // :cond_5
/* add-int/lit8 v9, v9, 0x1 */
} // .end local v6 # "ls":[Ljava/lang/String;
} // :cond_6
/* .line 1703 */
} // :cond_7
final String v6 = "doubleapp:"; // const-string v6, "doubleapp:"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v6 != null) { // if-eqz v6, :cond_8
/* .line 1704 */
final String v6 = "false"; // const-string v6, "false"
v6 = (( java.lang.String ) v5 ).contains ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* xor-int/2addr v6, v8 */
/* iput-boolean v6, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z */
/* .line 1703 */
} // :cond_8
} // :goto_2
/* nop */
/* .line 1686 */
} // .end local v5 # "cf":Ljava/lang/String;
} // :goto_3
/* add-int/lit8 v4, v4, 0x1 */
/* goto/16 :goto_0 */
/* .line 1707 */
} // :cond_9
return;
/* .line 1684 */
} // .end local v1 # "cfs":[Ljava/lang/String;
} // :cond_a
} // :goto_4
return;
} // .end method
private void updateCloudAllowList ( ) {
/* .locals 8 */
/* .line 1482 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_b
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z */
/* if-nez v0, :cond_b */
/* sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z */
/* if-nez v0, :cond_0 */
/* goto/16 :goto_6 */
/* .line 1490 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v2 = "immobulus_mode_switch_restrict"; // const-string v2, "immobulus_mode_switch_restrict"
android.provider.Settings$Secure .getString ( v0,v2 );
/* .line 1492 */
/* .local v0, "str":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 1493 */
final String v2 = "AurogonImmobulusMode"; // const-string v2, "AurogonImmobulusMode"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "clound setting str = "; // const-string v4, "clound setting str = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 1494 */
final String v2 = "_"; // const-string v2, "_"
(( java.lang.String ) v0 ).split ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1495 */
/* .local v2, "temp":[Ljava/lang/String; */
/* array-length v3, v2 */
int v4 = 3; // const/4 v4, 0x3
/* if-ge v3, v4, :cond_1 */
return;
/* .line 1496 */
} // :cond_1
final String v3 = "enable"; // const-string v3, "enable"
/* aget-object v4, v2, v1 */
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v4 = 1; // const/4 v4, 0x1
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 1497 */
/* aget-object v3, v2, v4 */
v3 = java.lang.Integer .parseInt ( v3 );
/* .line 1498 */
/* .local v3, "enable":I */
/* and-int/lit8 v5, v3, 0x8 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1499 */
v5 = this.mGreezeService;
v5 = this.DISABLE_IMMOB_MODE_DEVICE;
v5 = v6 = android.os.Build.DEVICE;
/* if-nez v5, :cond_3 */
/* .line 1500 */
/* iput-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 1502 */
} // :cond_2
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 1505 */
} // :cond_3
} // :goto_0
/* and-int/lit8 v5, v3, 0x10 */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1506 */
/* iput-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 1508 */
} // :cond_4
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 1512 */
} // .end local v3 # "enable":I
} // :cond_5
} // :goto_1
final String v1 = "allowlist"; // const-string v1, "allowlist"
int v3 = 2; // const/4 v3, 0x2
/* aget-object v3, v2, v3 */
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_a
/* .line 1513 */
v1 = this.mAllowList;
/* monitor-enter v1 */
/* .line 1514 */
try { // :try_start_0
v3 = this.mAllowList;
/* .line 1515 */
v3 = this.mAllowList;
v5 = this.mLocalAllowList;
/* .line 1516 */
v3 = this.mCloudAllowList;
/* .line 1517 */
/* const/16 v3, 0x270f */
/* .line 1518 */
/* .local v3, "flag":I */
int v5 = 3; // const/4 v5, 0x3
/* .local v5, "i":I */
} // :goto_2
/* array-length v6, v2 */
/* if-ge v5, v6, :cond_7 */
/* .line 1519 */
/* const-string/jumbo v6, "wakeuplist" */
/* aget-object v7, v2, v5 */
v6 = (( java.lang.String ) v6 ).equals ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 1520 */
/* move v3, v5 */
/* .line 1521 */
/* .line 1523 */
} // :cond_6
v6 = this.mCloudAllowList;
/* aget-object v7, v2, v5 */
/* .line 1518 */
/* add-int/lit8 v5, v5, 0x1 */
/* .line 1525 */
} // .end local v5 # "i":I
} // :cond_7
} // :goto_3
v5 = this.mAllowList;
v6 = this.mCloudAllowList;
/* .line 1526 */
/* array-length v5, v2 */
/* sub-int/2addr v5, v4 */
/* if-ge v3, v5, :cond_8 */
/* .line 1527 */
v4 = this.mAllowWakeUpPackageNameList;
/* .line 1529 */
} // :cond_8
/* add-int/lit8 v4, v3, 0x1 */
/* .local v4, "j":I */
} // :goto_4
/* array-length v5, v2 */
/* if-ge v4, v5, :cond_9 */
/* .line 1530 */
v5 = this.mAllowWakeUpPackageNameList;
/* aget-object v6, v2, v4 */
/* .line 1529 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1532 */
} // .end local v3 # "flag":I
} // .end local v4 # "j":I
} // :cond_9
/* monitor-exit v1 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 1535 */
} // .end local v2 # "temp":[Ljava/lang/String;
} // :cond_a
} // :goto_5
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->parseAurogonEnable()V */
/* .line 1536 */
return;
/* .line 1483 */
} // .end local v0 # "str":Ljava/lang/String;
} // :cond_b
} // :goto_6
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 1484 */
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 1485 */
/* sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->CN_MODEL:Z */
/* if-nez v0, :cond_c */
/* .line 1486 */
v0 = this.mGreezeService;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).setBroadcastCtrl ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->setBroadcastCtrl(Z)V
/* .line 1487 */
} // :cond_c
return;
} // .end method
private void updateCtsStatus ( ) {
/* .locals 8 */
/* .line 233 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "cached_apps_freezer"; // const-string v1, "cached_apps_freezer"
android.provider.Settings$Global .getString ( v0,v1 );
/* .line 235 */
/* .local v0, "freezeEnable":Ljava/lang/String; */
/* const-string/jumbo v2, "use_freezer" */
/* .line 236 */
/* .local v2, "KEY_USE_FREEZER":Ljava/lang/String; */
int v3 = 1; // const/4 v3, 0x1
java.lang.Boolean .valueOf ( v3 );
/* .line 237 */
/* .local v3, "DEFAULT_USE_FREEZER":Ljava/lang/Boolean; */
/* iget-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z */
final String v5 = "disabled"; // const-string v5, "disabled"
final String v6 = "enabled"; // const-string v6, "enabled"
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 238 */
final String v4 = "persist.sys.powmillet.enable"; // const-string v4, "persist.sys.powmillet.enable"
final String v7 = "false"; // const-string v7, "false"
android.os.SystemProperties .set ( v4,v7 );
/* .line 239 */
v4 = (( java.lang.String ) v5 ).equals ( v0 ); // invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 240 */
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putString ( v4,v1,v6 );
/* .line 248 */
} // :cond_0
v1 = this.mGreezeService;
/* const-string/jumbo v4, "tsMode" */
(( com.miui.server.greeze.GreezeManagerService ) v1 ).thawAll ( v4 ); // invoke-virtual {v1, v4}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(Ljava/lang/String;)Z
/* .line 249 */
} // :cond_1
v4 = (( java.lang.String ) v6 ).equals ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_2 */
/* .line 251 */
v4 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 250 */
final String v6 = "activity_manager_native_boot"; // const-string v6, "activity_manager_native_boot"
/* const-string/jumbo v7, "use_freezer" */
v4 = android.provider.DeviceConfig .getBoolean ( v6,v7,v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 252 */
} // :cond_2
v4 = this.mContext;
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putString ( v4,v1,v5 );
/* .line 255 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void updateIMEAppStatus ( ) {
/* .locals 5 */
/* .line 1539 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "default_input_method"; // const-string v1, "default_input_method"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 1540 */
/* .local v0, "curImeId":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1541 */
final String v1 = "/"; // const-string v1, "/"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1542 */
/* .local v1, "str":[Ljava/lang/String; */
/* array-length v2, v1 */
int v3 = 1; // const/4 v3, 0x1
/* if-le v2, v3, :cond_0 */
/* .line 1543 */
v2 = this.mCurrentIMEPacageName;
int v3 = 0; // const/4 v3, 0x0
/* aget-object v4, v1, v3 */
v2 = (( java.lang.String ) v2 ).equals ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v2, :cond_0 */
/* .line 1544 */
/* aget-object v2, v1, v3 */
this.mCurrentIMEPacageName = v2;
/* .line 1545 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " updateIMEAppStatus mCurrentIMEPacageName = "; // const-string v3, " updateIMEAppStatus mCurrentIMEPacageName = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mCurrentIMEPacageName;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AurogonImmobulusMode"; // const-string v3, "AurogonImmobulusMode"
android.util.Slog .d ( v3,v2 );
/* .line 1546 */
v2 = this.mGreezeService;
v3 = this.mCurrentIMEPacageName;
v2 = (( com.miui.server.greeze.GreezeManagerService ) v2 ).getUidByPackageName ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->getUidByPackageName(Ljava/lang/String;)I
/* .line 1547 */
/* .local v2, "uid":I */
/* iput v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEUid:I */
/* .line 1551 */
} // .end local v1 # "str":[Ljava/lang/String;
} // .end local v2 # "uid":I
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void QuitImmobulusModeAction ( ) {
/* .locals 6 */
/* .line 406 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* .line 407 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 408 */
/* .local v0, "log":Ljava/lang/StringBuilder; */
final String v1 = "IM finish THAW uid = ["; // const-string v1, "IM finish THAW uid = ["
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 409 */
v1 = this.mQuitImmobulusList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 410 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
final String v3 = "IMMOBULUS"; // const-string v3, "IMMOBULUS"
v3 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).unFreezeActionForImmobulus ( v2, v3 ); // invoke-virtual {p0, v2, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->unFreezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z
/* .line 411 */
/* .local v3, "success":Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 412 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = " "; // const-string v5, " "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 414 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // .end local v3 # "success":Z
} // :cond_0
/* .line 415 */
} // :cond_1
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 416 */
v1 = this.mGreezeService;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v1 ).addToDumpHistory ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V
/* .line 417 */
v1 = this.mGreezeService;
/* const/16 v2, 0x8 */
(( com.miui.server.greeze.GreezeManagerService ) v1 ).resetStatusForImmobulus ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->resetStatusForImmobulus(I)V
/* .line 418 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).resetImmobulusModeStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->resetImmobulusModeStatus()V
/* .line 419 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).removeAllMsg ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->removeAllMsg()V
/* .line 420 */
v1 = this.mQuitImmobulusList;
/* .line 421 */
return;
} // .end method
public void QuitLaunchModeAction ( Boolean p0 ) {
/* .locals 9 */
/* .param p1, "timeout" # Z */
/* .line 1129 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1130 */
/* .local v0, "reason":Ljava/lang/String; */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 1131 */
final String v0 = "LM timeout"; // const-string v0, "LM timeout"
/* move-object v1, v0 */
/* .line 1133 */
} // :cond_0
v1 = this.mHandler;
/* const/16 v2, 0x6a */
v1 = (( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v1 ).hasMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 1134 */
v1 = this.mHandler;
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1138 */
final String v0 = "LM finish"; // const-string v0, "LM finish"
/* move-object v1, v0 */
/* .line 1140 */
} // .end local v0 # "reason":Ljava/lang/String;
/* .local v1, "reason":Ljava/lang/String; */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v2, v0 */
/* .line 1141 */
/* .local v2, "log":Ljava/lang/StringBuilder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " THAW uid = ["; // const-string v3, " THAW uid = ["
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1142 */
v3 = this.mQuitLaunchModeList;
/* monitor-enter v3 */
/* .line 1143 */
try { // :try_start_0
v0 = this.mQuitLaunchModeList;
v4 = } // :goto_1
/* const/16 v5, 0x10 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* check-cast v4, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 1144 */
/* .local v4, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
v6 = this.mGreezeService;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
final String v8 = "LAUNCH_MODE"; // const-string v8, "LAUNCH_MODE"
v5 = (( com.miui.server.greeze.GreezeManagerService ) v6 ).thawUid ( v7, v5, v8 ); // invoke-virtual {v6, v7, v5, v8}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 1145 */
/* .local v5, "success":Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 1146 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v6 ); // invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1148 */
} // :cond_1
v6 = this.mGreezeService;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( com.miui.server.greeze.GreezeManagerService ) v6 ).resetCgroupUidStatus ( v7 ); // invoke-virtual {v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->resetCgroupUidStatus(I)V
/* .line 1150 */
} // .end local v4 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // .end local v5 # "success":Z
} // :goto_2
/* .line 1151 */
} // :cond_2
v0 = this.mQuitLaunchModeList;
/* .line 1152 */
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1153 */
final String v0 = "]"; // const-string v0, "]"
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1154 */
v0 = this.mGreezeService;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).addToDumpHistory ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V
/* .line 1156 */
v0 = this.mGreezeService;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).resetStatusForImmobulus ( v5 ); // invoke-virtual {v0, v5}, Lcom/miui/server/greeze/GreezeManagerService;->resetStatusForImmobulus(I)V
/* .line 1157 */
final String v0 = ""; // const-string v0, ""
this.mLastPackageName = v0;
/* .line 1158 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).removeTempMutiWindowsApp ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->removeTempMutiWindowsApp()V
/* .line 1159 */
return;
/* .line 1152 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
/* .line 1136 */
} // .end local v1 # "reason":Ljava/lang/String;
} // .end local v2 # "log":Ljava/lang/StringBuilder;
/* .restart local v0 # "reason":Ljava/lang/String; */
} // :cond_3
return;
} // .end method
public void TriggerImmobulusModeAction ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 293 */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isRunningLaunchMode ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 294 */
/* const/16 v2, 0x65 */
int v4 = -1; // const/4 v4, -0x1
/* const-wide/16 v6, 0xdac */
/* move-object v1, p0 */
/* move v3, p1 */
/* move-object v5, p2 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 295 */
return;
/* .line 297 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 298 */
/* .local v0, "log":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "IM "; // const-string v2, "IM "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " FZ ["; // const-string v2, " FZ ["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "] uid = ["; // const-string v2, "] uid = ["
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 299 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).updateAppsOnWindowsStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateAppsOnWindowsStatus()V
/* .line 300 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V */
/* .line 302 */
v1 = this.mImmobulusTargetList;
/* monitor-enter v1 */
/* .line 303 */
try { // :try_start_0
v2 = this.mImmobulusTargetList;
} // :cond_1
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_e
/* check-cast v3, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 304 */
/* .local v3, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
v4 = this.mImmobulusAllowList;
v4 = v5 = this.mPackageName;
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 305 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 306 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is in ImmobulusAllowList, skip it!"; // const-string v6, " is in ImmobulusAllowList, skip it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 312 */
} // :cond_2
v4 = this.mAllowList;
v4 = v5 = this.mPackageName;
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 313 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 314 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " in Allowlist, skip it!"; // const-string v6, " in Allowlist, skip it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 319 */
} // :cond_3
/* iget-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* if-nez v4, :cond_4 */
v4 = this.mNoRestrictAppSet;
v4 = v5 = this.mPackageName;
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 320 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 321 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is in NoRestrictAppSet, skip it!"; // const-string v6, " is in NoRestrictAppSet, skip it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 326 */
} // :cond_4
v4 = this.mPackageName;
v4 = /* invoke-direct {p0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->InStatusBarScene(Ljava/lang/String;)Z */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 327 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 328 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is in statusbar, skip it!"; // const-string v6, " is in statusbar, skip it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 333 */
} // :cond_5
v4 = this.mPackageName;
v4 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isWallPaperApp ( v4 ); // invoke-virtual {p0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWallPaperApp(Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_6
/* .line 334 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 335 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is WallPaperApp app, skip check!"; // const-string v6, " is WallPaperApp app, skip check!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 341 */
} // :cond_6
v4 = this.mGreezeService;
/* iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v4 = (( com.miui.server.greeze.GreezeManagerService ) v4 ).isAppRunning ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunning(I)Z
/* if-nez v4, :cond_7 */
v4 = this.mGreezeService;
/* iget-boolean v4, v4, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 342 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 343 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is not running, skip check!"; // const-string v6, " is not running, skip check!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 349 */
} // :cond_7
v4 = this.mGreezeService;
/* iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v4 = (( com.miui.server.greeze.GreezeManagerService ) v4 ).isAppRunningInFg ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
if ( v4 != null) { // if-eqz v4, :cond_8
/* .line 350 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "is running in FG, skip check!"; // const-string v6, "is running in FG, skip check!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* .line 351 */
/* goto/16 :goto_0 */
/* .line 354 */
} // :cond_8
v4 = this.mGreezeService;
/* iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v4 = (( com.miui.server.greeze.GreezeManagerService ) v4 ).isUidFrozen ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v4 != null) { // if-eqz v4, :cond_9
/* .line 355 */
v4 = this.mGreezeService;
/* iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* const/16 v6, 0x8 */
(( com.miui.server.greeze.GreezeManagerService ) v4 ).updateFrozenInfoForImmobulus ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->updateFrozenInfoForImmobulus(II)V
/* .line 356 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 357 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v6, "updateFrozenInfoForImmobulus!" */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 362 */
} // :cond_9
v4 = this.mOnWindowsAppList;
if ( v4 != null) { // if-eqz v4, :cond_a
/* iget v5, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v4 = java.lang.Integer .valueOf ( v5 );
if ( v4 != null) { // if-eqz v4, :cond_a
/* .line 363 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 364 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " is show on screen, skip it!"; // const-string v6, " is show on screen, skip it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 383 */
} // :cond_a
/* iget-boolean v4, v3, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
/* if-nez v4, :cond_b */
/* .line 384 */
/* sget-boolean v4, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 385 */
final String v4 = "AurogonImmobulusMode"; // const-string v4, "AurogonImmobulusMode"
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " uid = "; // const-string v6, " uid = "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " not has icon, skip it!"; // const-string v6, " not has icon, skip it!"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v5 );
/* goto/16 :goto_0 */
/* .line 390 */
} // :cond_b
v4 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).checkAppStatusForFreeze ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z
/* if-nez v4, :cond_c */
/* .line 391 */
/* const/16 v4, 0x7d0 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( v3, v4 ); // invoke-virtual {p0, v3, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 392 */
/* goto/16 :goto_0 */
/* .line 395 */
} // :cond_c
final String v4 = "IMMOBULUS"; // const-string v4, "IMMOBULUS"
v4 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).freezeActionForImmobulus ( v3, v4 ); // invoke-virtual {p0, v3, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->freezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z
/* .line 396 */
/* .local v4, "success":Z */
if ( v4 != null) { // if-eqz v4, :cond_d
/* .line 397 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = " "; // const-string v6, " "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, v3, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 399 */
} // .end local v3 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // .end local v4 # "success":Z
} // :cond_d
/* goto/16 :goto_0 */
/* .line 400 */
} // :cond_e
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 401 */
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 402 */
v1 = this.mGreezeService;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v1 ).addToDumpHistory ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V
/* .line 403 */
return;
/* .line 400 */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
public void addImmobulusModeQuitList ( com.miui.server.greeze.AurogonAppInfo p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 898 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$4; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$4;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Lcom/miui/server/greeze/AurogonAppInfo;)V */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z
/* .line 908 */
return;
} // .end method
public void addLaunchModeQiutList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 866 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$2; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$2;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;I)V */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z
/* .line 880 */
return;
} // .end method
public void addLaunchModeQiutList ( com.miui.server.greeze.AurogonAppInfo p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 884 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$3; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$3;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;Lcom/miui/server/greeze/AurogonAppInfo;)V */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z
/* .line 895 */
return;
} // .end method
public void addTempMutiWindowsApp ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1263 */
v0 = this.mMutiWindowsApp;
/* monitor-enter v0 */
/* .line 1264 */
try { // :try_start_0
v1 = this.mMutiWindowsApp;
java.lang.Integer .valueOf ( p1 );
/* .line 1265 */
/* monitor-exit v0 */
/* .line 1266 */
return;
/* .line 1265 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean checkAppFgService ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 662 */
v0 = this.mFgServiceAppList;
/* monitor-enter v0 */
/* .line 663 */
try { // :try_start_0
v1 = this.mFgServiceAppList;
java.lang.Integer .valueOf ( p1 );
/* check-cast v1, Ljava/util/List; */
/* .line 664 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v2 = if ( v1 != null) { // if-eqz v1, :cond_0
/* if-lez v2, :cond_0 */
/* .line 665 */
/* monitor-exit v0 */
int v0 = 1; // const/4 v0, 0x1
/* .line 667 */
} // .end local v1 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_0
/* monitor-exit v0 */
/* .line 668 */
int v0 = 0; // const/4 v0, 0x0
/* .line 667 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void checkAppForImmobulusMode ( com.miui.server.greeze.AurogonAppInfo p0 ) {
/* .locals 2 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 424 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 425 */
} // :cond_0
v0 = this.mGreezeService;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isAppRunningInFg ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
/* if-nez v0, :cond_6 */
v0 = this.mGreezeService;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isUidFrozen ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 429 */
} // :cond_1
v0 = this.mImmobulusAllowList;
v0 = v1 = this.mPackageName;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 430 */
return;
/* .line 433 */
} // :cond_2
v0 = this.mGreezeService;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isAppRunning ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunning(I)Z
/* if-nez v0, :cond_3 */
/* .line 434 */
return;
/* .line 437 */
} // :cond_3
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).checkAppStatusForFreeze ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z
/* if-nez v0, :cond_5 */
/* .line 438 */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isRunningLaunchMode ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isRunningLaunchMode()Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 439 */
/* const/16 v0, 0x1f4 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 441 */
} // :cond_4
/* const/16 v0, 0x7d0 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 443 */
} // :goto_0
return;
/* .line 446 */
} // :cond_5
final String v0 = "repeat"; // const-string v0, "repeat"
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).freezeActionForImmobulus ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->freezeActionForImmobulus(Lcom/miui/server/greeze/AurogonAppInfo;Ljava/lang/String;)Z
/* .line 447 */
return;
/* .line 426 */
} // :cond_6
} // :goto_1
return;
} // .end method
public Boolean checkAppStatusForFreeze ( com.miui.server.greeze.AurogonAppInfo p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 463 */
v0 = this.mGreezeService;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isAppRunningInFg ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunningInFg(I)Z
final String v1 = " uid = "; // const-string v1, " uid = "
final String v2 = "AurogonImmobulusMode"; // const-string v2, "AurogonImmobulusMode"
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 464 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 465 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "is running in FG, skip check!"; // const-string v1, "is running in FG, skip check!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 467 */
} // :cond_0
/* .line 478 */
} // :cond_1
v0 = this.mGreezeService;
/* iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).isUidActive ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidActive(I)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 479 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 480 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is using GPS/Audio/Vibrator!"; // const-string v1, " is using GPS/Audio/Vibrator!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 482 */
} // :cond_2
/* .line 485 */
} // :cond_3
v0 = this.mGreezeService;
v4 = this.mPackageName;
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).checkFreeformSmallWin ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerService;->checkFreeformSmallWin(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 486 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 487 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Uid "; // const-string v1, "Uid "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " was Freeform small window, skip it"; // const-string v1, " was Freeform small window, skip it"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 489 */
} // :cond_4
/* .line 493 */
} // :cond_5
v0 = this.mBluetoothUsingList;
v0 = v4 = this.mPackageName;
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 494 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 495 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is using BT!"; // const-string v1, " is using BT!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 497 */
} // :cond_6
/* .line 499 */
} // :cond_7
com.android.server.am.ActivityManagerServiceStub .get ( );
/* iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.android.server.am.ActivityManagerServiceStub ) v0 ).isBackuping ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->isBackuping(I)Z
/* if-nez v0, :cond_15 */
/* .line 500 */
com.android.server.am.ActivityManagerServiceStub .get ( );
/* iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.android.server.am.ActivityManagerServiceStub ) v0 ).isActiveInstruUid ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->isActiveInstruUid(I)Z
/* if-nez v0, :cond_15 */
/* .line 501 */
com.android.server.am.ActivityManagerServiceStub .get ( );
/* iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.android.server.am.ActivityManagerServiceStub ) v0 ).isVibratorActive ( v4 ); // invoke-virtual {v0, v4}, Lcom/android/server/am/ActivityManagerServiceStub;->isVibratorActive(I)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* goto/16 :goto_0 */
/* .line 507 */
} // :cond_8
com.android.server.wm.ActivityTaskManagerServiceStub .get ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceStub ) v0 ).getActivityControllerUid ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerServiceStub;->getActivityControllerUid()I
/* iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* if-ne v0, v4, :cond_9 */
/* .line 508 */
/* .line 512 */
} // :cond_9
v0 = this.mCurrentIMEPacageName;
v4 = this.mPackageName;
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 513 */
/* .line 517 */
} // :cond_a
v0 = this.mGreezeService;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
/* const-string/jumbo v4, "uid = " */
if ( v0 != null) { // if-eqz v0, :cond_c
v0 = this.mPackageName;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isWidgetApp ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWidgetApp(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 518 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 519 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is widget app!"; // const-string v1, " is widget app!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 521 */
} // :cond_b
/* .line 525 */
} // :cond_c
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mVpnConnect:Z */
if ( v0 != null) { // if-eqz v0, :cond_e
/* iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isVpnApp ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isVpnApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 526 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 527 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is vpn app!"; // const-string v1, " is vpn app!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 529 */
} // :cond_d
/* .line 532 */
} // :cond_e
/* iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).checkMutiWindowsApp ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkMutiWindowsApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_10
/* .line 533 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 534 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is in MutiWindows!"; // const-string v1, " is in MutiWindows!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 536 */
} // :cond_f
/* .line 540 */
} // :cond_10
v0 = this.mGreezeService;
/* iget v4, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).checkOrderBCRecivingApp ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/greeze/GreezeManagerService;->checkOrderBCRecivingApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_12
/* .line 541 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_11
/* .line 542 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "freeze app was reciving broadcast! mUid = "; // const-string v1, "freeze app was reciving broadcast! mUid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 544 */
} // :cond_11
/* .line 548 */
} // :cond_12
/* iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v4 = this.mPackageName;
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isDownloadApp ( v0, v4 ); // invoke-virtual {p0, v0, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isDownloadApp(ILjava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_14
/* .line 549 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_13
/* .line 550 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is downloading."; // const-string v1, " is downloading."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 552 */
} // :cond_13
/* .line 554 */
} // :cond_14
int v0 = 1; // const/4 v0, 0x1
/* .line 502 */
} // :cond_15
} // :goto_0
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_16
/* .line 503 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " is using backingg, activeIns or vibrator"; // const-string v1, " is using backingg, activeIns or vibrator"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 505 */
} // :cond_16
} // .end method
public Boolean checkMutiWindowsApp ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1274 */
v0 = this.mMutiWindowsApp;
/* monitor-enter v0 */
/* .line 1275 */
try { // :try_start_0
v1 = this.mMutiWindowsApp;
v1 = java.lang.Integer .valueOf ( p1 );
/* monitor-exit v0 */
/* .line 1276 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 5 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 1710 */
final String v0 = "AurogonImmobulusMode : "; // const-string v0, "AurogonImmobulusMode : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1711 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "mImmobulusModeEnabled : "; // const-string v1, "mImmobulusModeEnabled : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1712 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1713 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ExtremeMode enabled : "; // const-string v1, "ExtremeMode enabled : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " cloud:"; // const-string v1, " cloud:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeModeCloud:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1714 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "VideoMode enabled : "; // const-string v1, "VideoMode enabled : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " cloud:"; // const-string v1, " cloud:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1715 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Camera enabled : "; // const-string v1, "Camera enabled : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " doubleapp:"; // const-string v1, " doubleapp:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1718 */
} // :cond_0
final String v0 = "AurogonImmobulusMode LM : "; // const-string v0, "AurogonImmobulusMode LM : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1719 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "LM enabled : "; // const-string v1, "LM enabled : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1720 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "LM mCurrentIMEPacageName : "; // const-string v1, "LM mCurrentIMEPacageName : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mCurrentIMEPacageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1722 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 1723 */
final String v0 = "mImmobulusAllowList : "; // const-string v0, "mImmobulusAllowList : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1724 */
v0 = this.mImmobulusAllowList;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/lang/String; */
/* .line 1725 */
/* .local v1, "str":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1726 */
} // .end local v1 # "str":Ljava/lang/String;
/* .line 1728 */
} // :cond_1
final String v0 = "mImmobulusTargetList : "; // const-string v0, "mImmobulusTargetList : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1729 */
v0 = this.mImmobulusTargetList;
/* monitor-enter v0 */
/* .line 1730 */
try { // :try_start_0
v1 = this.mImmobulusTargetList;
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 1731 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " "; // const-string v4, " "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.greeze.AurogonAppInfo ) v2 ).toString ( ); // invoke-virtual {v2}, Lcom/miui/server/greeze/AurogonAppInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v3 ); // invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1732 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 1733 */
} // :cond_2
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_1 */
/* .line 1735 */
v1 = this.mFgServiceAppList;
/* monitor-enter v1 */
/* .line 1736 */
try { // :try_start_1
final String v0 = "mFgServiceAppList : "; // const-string v0, "mFgServiceAppList : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1737 */
v0 = this.mFgServiceAppList;
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* .line 1738 */
/* .local v2, "uid":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = " uid = "; // const-string v4, " uid = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v3 ); // invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1739 */
} // .end local v2 # "uid":I
/* .line 1740 */
} // :cond_3
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 1742 */
final String v0 = "mAllowList : "; // const-string v0, "mAllowList : "
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1743 */
v0 = this.mAllowList;
v1 = } // :goto_3
if ( v1 != null) { // if-eqz v1, :cond_4
/* check-cast v1, Ljava/lang/String; */
/* .line 1744 */
/* .local v1, "str1":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " "; // const-string v3, " "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1745 */
} // .end local v1 # "str1":Ljava/lang/String;
/* .line 1747 */
} // :cond_4
final String v0 = "mWallPaperPackageName"; // const-string v0, "mWallPaperPackageName"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1748 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mWallPaperPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1750 */
final String v0 = "mAllowWakeUpPackageNameList"; // const-string v0, "mAllowWakeUpPackageNameList"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1751 */
v0 = this.mAllowWakeUpPackageNameList;
v1 = } // :goto_4
if ( v1 != null) { // if-eqz v1, :cond_5
/* check-cast v1, Ljava/lang/String; */
/* .line 1752 */
/* .local v1, "str":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "--"; // const-string v3, "--"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "--"; // const-string v3, "--"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v2 ); // invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 1753 */
} // .end local v1 # "str":Ljava/lang/String;
/* .line 1740 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_2
/* monitor-exit v1 */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* throw v0 */
/* .line 1733 */
/* :catchall_1 */
/* move-exception v1 */
try { // :try_start_3
/* monitor-exit v0 */
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* throw v1 */
/* .line 1756 */
} // :cond_5
/* array-length v0, p3 */
/* if-nez v0, :cond_6 */
return;
/* .line 1757 */
} // :cond_6
final String v0 = "Immobulus"; // const-string v0, "Immobulus"
int v1 = 0; // const/4 v1, 0x0
/* aget-object v2, p3, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 1758 */
final String v0 = "enable"; // const-string v0, "enable"
/* aget-object v3, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 1759 */
/* iput-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 1760 */
v0 = this.mHandler;
/* const/16 v3, 0x65 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).sendEmptyMessage ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendEmptyMessage(I)Z
/* .line 1761 */
} // :cond_7
final String v0 = "disabled"; // const-string v0, "disabled"
/* aget-object v3, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 1762 */
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* .line 1763 */
v0 = this.mHandler;
/* const/16 v3, 0x66 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).sendEmptyMessage ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendEmptyMessage(I)Z
/* .line 1767 */
} // :cond_8
} // :goto_5
/* array-length v0, p3 */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v0, v3, :cond_f */
/* .line 1768 */
final String v0 = "LM"; // const-string v0, "LM"
/* aget-object v3, p3, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 2; // const/4 v3, 0x2
if ( v0 != null) { // if-eqz v0, :cond_d
/* .line 1769 */
final String v0 = "add"; // const-string v0, "add"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_9
/* .line 1770 */
v0 = this.mAllowList;
/* aget-object v1, p3, v3 */
/* goto/16 :goto_6 */
/* .line 1771 */
} // :cond_9
final String v0 = "remove"; // const-string v0, "remove"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_a
/* .line 1772 */
v0 = this.mAllowList;
v0 = /* aget-object v1, p3, v3 */
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1773 */
v0 = this.mAllowList;
/* aget-object v1, p3, v3 */
/* goto/16 :goto_6 */
/* .line 1775 */
} // :cond_a
/* const-string/jumbo v0, "set" */
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_c
/* .line 1776 */
/* const-string/jumbo v0, "true" */
/* aget-object v4, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_b
/* .line 1777 */
/* iput-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 1778 */
} // :cond_b
final String v0 = "false"; // const-string v0, "false"
/* aget-object v2, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1779 */
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 1781 */
} // :cond_c
/* const-string/jumbo v0, "window" */
/* aget-object v1, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1782 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).updateAppsOnWindowsStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateAppsOnWindowsStatus()V
/* .line 1784 */
} // :cond_d
final String v0 = "IM"; // const-string v0, "IM"
/* aget-object v4, p3, v1 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1785 */
final String v0 = "ExtremeMode"; // const-string v0, "ExtremeMode"
/* aget-object v4, p3, v2 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1786 */
/* const-string/jumbo v0, "true" */
/* aget-object v4, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_e
/* .line 1787 */
/* iput-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* .line 1788 */
/* const/16 v0, 0x3e8 */
final String v1 = "ExtremeM"; // const-string v1, "ExtremeM"
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).triggerImmobulusMode ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V
/* .line 1789 */
} // :cond_e
final String v0 = "false"; // const-string v0, "false"
/* aget-object v2, p3, v3 */
v0 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_f
/* .line 1790 */
/* iput-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* .line 1791 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).quitImmobulusMode ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 1792 */
v0 = this.mGreezeService;
final String v1 = "Sleep Mode"; // const-string v1, "Sleep Mode"
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawAll ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/GreezeManagerService;->thawAll(Ljava/lang/String;)Z
/* .line 1797 */
} // :cond_f
} // :goto_6
return;
} // .end method
public void finishLaunchMode ( ) {
/* .locals 8 */
/* .line 1191 */
v0 = this.mHandler;
/* const/16 v1, 0x6a */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z
/* if-nez v0, :cond_0 */
return;
/* .line 1192 */
} // :cond_0
/* const/16 v2, 0x6a */
int v3 = 1; // const/4 v3, 0x1
int v4 = -1; // const/4 v4, -0x1
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, -0x1 */
/* move-object v1, p0 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 1193 */
return;
} // .end method
public void finishLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 1186 */
v0 = this.mLastPackageName;
/* if-eq p1, v0, :cond_0 */
return;
/* .line 1187 */
} // :cond_0
/* const/16 v2, 0x6a */
int v3 = 1; // const/4 v3, 0x1
int v4 = -1; // const/4 v4, -0x1
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, -0x1 */
/* move-object v1, p0 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 1188 */
return;
} // .end method
public void forceStopAppForImmobulus ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 601 */
v0 = this.mGreezeService;
v1 = this.mContext;
v1 = (( android.content.Context ) v1 ).getUserId ( ); // invoke-virtual {v1}, Landroid/content/Context;->getUserId()I
(( com.miui.server.greeze.GreezeManagerService ) v0 ).forceStopPackage ( p1, v1, p2 ); // invoke-virtual {v0, p1, v1, p2}, Lcom/miui/server/greeze/GreezeManagerService;->forceStopPackage(Ljava/lang/String;ILjava/lang/String;)V
/* .line 602 */
return;
} // .end method
public Boolean freezeActionForImmobulus ( com.miui.server.greeze.AurogonAppInfo p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 558 */
/* iget-boolean v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z */
/* if-nez v0, :cond_0 */
/* iget v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).checkAppFgService ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppFgService(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 559 */
} // :cond_0
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).addImmobulusModeQuitList ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addImmobulusModeQuitList(Lcom/miui/server/greeze/AurogonAppInfo;)V
/* .line 562 */
} // :cond_1
v0 = this.mGreezeService;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* const/16 v2, 0x8 */
int v3 = 0; // const/4 v3, 0x0
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).freezeAction ( v1, v2, p2, v3 ); // invoke-virtual {v0, v1, v2, p2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->freezeAction(IILjava/lang/String;Z)Z
/* .line 563 */
/* .local v0, "success":Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 564 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* iput-wide v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->freezeTime:J */
/* .line 566 */
} // :cond_2
v1 = this.mGreezeService;
/* iget v2, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v1 = (( com.miui.server.greeze.GreezeManagerService ) v1 ).isUidFrozen ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
/* if-nez v1, :cond_3 */
/* .line 567 */
/* const/16 v1, 0x7d0 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( p1, v1 ); // invoke-virtual {p0, p1, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 570 */
} // :cond_3
} // :goto_0
} // .end method
public Boolean freezeActionForLaunchMode ( com.miui.server.greeze.AurogonAppInfo p0 ) {
/* .locals 5 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 1162 */
int v0 = 0; // const/4 v0, 0x0
/* .line 1163 */
/* .local v0, "isNeedCompact":Z */
/* iget-boolean v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z */
/* if-nez v1, :cond_1 */
/* iget-boolean v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
/* if-nez v1, :cond_0 */
/* .line 1166 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 1164 */
} // :cond_1
} // :goto_0
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).addLaunchModeQiutList ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(Lcom/miui/server/greeze/AurogonAppInfo;)V
/* .line 1169 */
} // :goto_1
v1 = this.mGreezeService;
/* iget v2, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* const/16 v3, 0x10 */
final String v4 = "LAUNCH_MODE"; // const-string v4, "LAUNCH_MODE"
v1 = (( com.miui.server.greeze.GreezeManagerService ) v1 ).freezeAction ( v2, v3, v4, v0 ); // invoke-virtual {v1, v2, v3, v4, v0}, Lcom/miui/server/greeze/GreezeManagerService;->freezeAction(IILjava/lang/String;Z)Z
/* .line 1170 */
/* .local v1, "success":Z */
} // .end method
public com.miui.server.greeze.AurogonAppInfo getAurogonAppInfo ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .line 672 */
v0 = this.mImmobulusTargetList;
/* monitor-enter v0 */
/* .line 673 */
try { // :try_start_0
v1 = this.mImmobulusTargetList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 674 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
/* iget v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* if-ne v3, p1, :cond_0 */
/* .line 675 */
/* monitor-exit v0 */
/* .line 677 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // :cond_0
/* .line 678 */
} // :cond_1
/* monitor-exit v0 */
/* .line 679 */
int v0 = 0; // const/4 v0, 0x0
/* .line 678 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public com.miui.server.greeze.AurogonAppInfo getAurogonAppInfo ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 683 */
v0 = this.mImmobulusTargetList;
/* monitor-enter v0 */
/* .line 684 */
try { // :try_start_0
v1 = this.mImmobulusTargetList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 685 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
v3 = this.mPackageName;
/* if-ne v3, p1, :cond_0 */
/* .line 686 */
/* monitor-exit v0 */
/* .line 688 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // :cond_0
/* .line 689 */
} // :cond_1
/* monitor-exit v0 */
/* .line 690 */
int v0 = 0; // const/4 v0, 0x0
/* .line 689 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void getConnectivityManager ( ) {
/* .locals 2 */
/* .line 1006 */
v0 = this.mConnMgr;
/* if-nez v0, :cond_0 */
/* .line 1007 */
v0 = this.mContext;
final String v1 = "connectivity"; // const-string v1, "connectivity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/net/ConnectivityManager; */
this.mConnMgr = v0;
/* .line 1009 */
} // :cond_0
return;
} // .end method
public com.miui.server.process.ProcessManagerInternal getProcessManagerInternal ( ) {
/* .locals 2 */
/* .line 785 */
int v0 = 0; // const/4 v0, 0x0
/* .line 786 */
/* .local v0, "pmi":Lcom/miui/server/process/ProcessManagerInternal; */
/* const-class v1, Lcom/miui/server/process/ProcessManagerInternal; */
com.android.server.LocalServices .getService ( v1 );
/* move-object v0, v1 */
/* check-cast v0, Lcom/miui/server/process/ProcessManagerInternal; */
/* .line 787 */
} // .end method
public void getWallpaperPackageName ( ) {
/* .locals 4 */
/* .line 1295 */
try { // :try_start_0
v0 = this.mContext;
/* const-string/jumbo v1, "wallpaper" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/WallpaperManager; */
/* .line 1296 */
/* .local v0, "wm":Landroid/app/WallpaperManager; */
(( android.app.WallpaperManager ) v0 ).getWallpaperInfo ( ); // invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;
/* .line 1297 */
/* .local v1, "info":Landroid/app/WallpaperInfo; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1298 */
(( android.app.WallpaperInfo ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/app/WallpaperInfo;->getPackageName()Ljava/lang/String;
this.mWallPaperPackageName = v2;
/* .line 1300 */
} // :cond_0
v2 = this.mContext;
android.app.WallpaperManager .getDefaultWallpaperComponent ( v2 );
/* .line 1301 */
/* .local v2, "componentName":Landroid/content/ComponentName; */
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 1302 */
(( android.content.ComponentName ) v2 ).getPackageName ( ); // invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
this.mWallPaperPackageName = v3;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1307 */
} // .end local v0 # "wm":Landroid/app/WallpaperManager;
} // .end local v1 # "info":Landroid/app/WallpaperInfo;
} // .end local v2 # "componentName":Landroid/content/ComponentName;
} // :cond_1
} // :goto_0
/* .line 1305 */
/* :catch_0 */
/* move-exception v0 */
/* .line 1308 */
} // :goto_1
return;
} // .end method
public void handleMessageAppStatus ( ) {
/* .locals 2 */
/* .line 1459 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$6; */
/* invoke-direct {v1, p0}, Lcom/miui/server/greeze/AurogonImmobulusMode$6;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1470 */
return;
} // .end method
public void initCtsStatus ( ) {
/* .locals 1 */
/* .line 258 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mPowerMilletEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCtsStatus()V */
/* .line 261 */
} // :cond_0
return;
} // .end method
public Boolean isAllowWakeUpList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1474 */
v0 = v0 = this.mAllowWakeUpPackageNameList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1475 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " packageName = "; // const-string v1, " packageName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " isAllowWakeUpList "; // const-string v1, " isAllowWakeUpList "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "Aurogon"; // const-string v1, "Aurogon"
android.util.Slog .d ( v1,v0 );
/* .line 1476 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1478 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isAppHasIcon ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 938 */
int v0 = 0; // const/4 v0, 0x0
/* .line 939 */
/* .local v0, "ret":Z */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 940 */
/* .local v1, "intent":Landroid/content/Intent; */
final String v2 = "android.intent.category.LAUNCHER"; // const-string v2, "android.intent.category.LAUNCHER"
(( android.content.Intent ) v1 ).addCategory ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 941 */
final String v2 = "android.intent.action.MAIN"; // const-string v2, "android.intent.action.MAIN"
(( android.content.Intent ) v1 ).setAction ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 942 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 943 */
(( android.content.Intent ) v1 ).setPackage ( p1 ); // invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 945 */
} // :cond_0
v2 = this.mPm;
/* const/high16 v3, 0x20000 */
(( android.content.pm.PackageManager ) v2 ).queryIntentActivities ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;
/* .line 946 */
/* .local v2, "resolveInfolist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v3 = if ( v2 != null) { // if-eqz v2, :cond_1
/* if-lez v3, :cond_1 */
/* .line 947 */
int v0 = 1; // const/4 v0, 0x1
/* .line 949 */
} // :cond_1
} // .end method
public Boolean isCtsApp ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 911 */
final String v0 = "\\."; // const-string v0, "\\."
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 912 */
/* .local v0, "str":[Ljava/lang/String; */
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 913 */
int v3 = 0; // const/4 v3, 0x0
/* .line 914 */
/* .local v3, "val":I */
/* array-length v4, v0 */
/* move v5, v1 */
} // :goto_0
/* if-ge v5, v4, :cond_5 */
/* aget-object v6, v0, v5 */
/* .line 915 */
/* .local v6, "temp":Ljava/lang/String; */
final String v7 = "cts"; // const-string v7, "cts"
v7 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_2 */
final String v7 = "gts"; // const-string v7, "gts"
v7 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_0
/* .line 917 */
} // :cond_0
final String v7 = "android"; // const-string v7, "android"
v7 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v7, :cond_1 */
final String v7 = "google"; // const-string v7, "google"
v7 = (( java.lang.String ) v7 ).equals ( v6 ); // invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 918 */
} // :cond_1
/* or-int/lit8 v3, v3, 0x2 */
/* .line 916 */
} // :cond_2
} // :goto_1
/* or-int/lit8 v3, v3, 0x1 */
/* .line 920 */
} // :cond_3
} // :goto_2
int v7 = 3; // const/4 v7, 0x3
/* if-ne v3, v7, :cond_4 */
/* .line 914 */
} // .end local v6 # "temp":Ljava/lang/String;
} // :cond_4
/* add-int/lit8 v5, v5, 0x1 */
/* .line 924 */
} // .end local v3 # "val":I
} // :cond_5
v3 = v3 = this.CTS_NAME;
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 925 */
} // :cond_6
} // .end method
public Boolean isDownloadApp ( Integer p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pacakgeName" # Ljava/lang/String; */
/* .line 995 */
com.miui.server.greeze.AurogonDownloadFilter .getInstance ( );
v0 = (( com.miui.server.greeze.AurogonDownloadFilter ) v0 ).isDownloadApp ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/AurogonDownloadFilter;->isDownloadApp(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 996 */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).checkAppFgService ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppFgService(I)Z
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 997 */
/* .line 998 */
} // :cond_0
v0 = v0 = com.miui.server.greeze.AurogonDownloadFilter.mImportantDownloadApp;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 999 */
/* .line 1002 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isIMEApp ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 1554 */
/* iget v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCurrentIMEUid:I */
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public Boolean isModeReason ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 1255 */
v0 = v0 = com.miui.server.greeze.AurogonImmobulusMode.REASON_FREEZE;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1256 */
int v0 = 1; // const/4 v0, 0x1
/* .line 1258 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isNeedNotifyAppStatus ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 953 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getAurogonAppInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 954 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 955 */
/* iget-boolean v2, v0, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z */
/* if-nez v2, :cond_0 */
/* iget-boolean v2, v0, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 957 */
} // :cond_1
} // .end method
public Boolean isNeedRestictNetworkPolicy ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1012 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getAurogonAppInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 1013 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = com.miui.server.greeze.AurogonImmobulusMode.mMessageApp;
v1 = v2 = this.mPackageName;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1014 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1016 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isRunningImmobulusMode ( ) {
/* .locals 1 */
/* .line 636 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 637 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 639 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isRunningLaunchMode ( ) {
/* .locals 2 */
/* .line 632 */
v0 = this.mHandler;
/* const/16 v1, 0x6a */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z
} // .end method
public Boolean isSystemOrMiuiImportantApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 605 */
v0 = this.mGreezeService;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getPackageNameFromUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 606 */
/* .local v0, "packageName":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 607 */
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isSystemOrMiuiImportantApp ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z
/* .line 609 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public Boolean isSystemOrMiuiImportantApp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 614 */
final String v0 = ".miui"; // const-string v0, ".miui"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_3 */
final String v0 = ".xiaomi"; // const-string v0, ".xiaomi"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_3 */
/* .line 615 */
final String v0 = ".google"; // const-string v0, ".google"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_3 */
final String v0 = "com.android"; // const-string v0, "com.android"
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v0, :cond_3 */
/* .line 616 */
final String v0 = "com.mi."; // const-string v0, "com.mi."
v0 = (( java.lang.String ) p1 ).contains ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 620 */
} // :cond_0
v0 = v0 = this.mImportantAppList;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 621 */
/* .line 624 */
} // :cond_1
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemApp(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 625 */
/* .line 628 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .line 617 */
} // :cond_3
} // :goto_0
} // .end method
public Boolean isUidValid ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 809 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "uid = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "isUidValid"; // const-string v1, "isUidValid"
android.util.Slog .d ( v1,v0 );
/* .line 810 */
/* const/16 v0, 0x2710 */
int v1 = 0; // const/4 v1, 0x0
/* if-ge p1, v0, :cond_0 */
/* .line 811 */
/* .line 813 */
} // :cond_0
v0 = android.os.UserHandle .isApp ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 814 */
/* const/16 v0, 0x4e1f */
/* if-le p1, v0, :cond_2 */
/* .line 815 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mDoubleAppCtrlCloud:Z */
/* if-nez v0, :cond_1 */
/* .line 816 */
/* .line 817 */
} // :cond_1
v0 = com.miui.server.greeze.AurogonImmobulusMode.ENABLE_DOUBLE_APP;
v0 = v2 = android.os.Build.DEVICE;
/* if-nez v0, :cond_2 */
/* .line 818 */
/* .line 820 */
} // :cond_2
int v0 = 1; // const/4 v0, 0x1
/* .line 822 */
} // :cond_3
} // .end method
public Boolean isVpnApp ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 1288 */
v0 = this.mVPNAppList;
/* monitor-enter v0 */
/* .line 1289 */
try { // :try_start_0
v1 = this.mVPNAppList;
v1 = java.lang.Integer .valueOf ( p1 );
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* monitor-exit v0 */
/* .line 1290 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean isWallPaperApp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 1280 */
v0 = this.mWallPaperPackageName;
final String v1 = ""; // const-string v1, ""
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1281 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getWallpaperPackageName ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getWallpaperPackageName()V
/* .line 1284 */
} // :cond_0
v0 = this.mWallPaperPackageName;
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
} // .end method
public Boolean isWidgetApp ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 1574 */
v0 = this.mGreezeService;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).getPackageNameFromUid ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/GreezeManagerService;->getPackageNameFromUid(I)Ljava/lang/String;
/* .line 1575 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isWidgetApp ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWidgetApp(Ljava/lang/String;)Z
} // .end method
public Boolean isWidgetApp ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pacakgeName" # Ljava/lang/String; */
/* .line 1579 */
v0 = this.mCurrentWidgetPackages;
/* monitor-enter v0 */
/* .line 1580 */
try { // :try_start_0
v1 = v1 = this.mCurrentWidgetPackages;
/* monitor-exit v0 */
/* .line 1581 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void notifyAppActive ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 724 */
/* sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 725 */
} // :cond_0
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).updateTargetList ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateTargetList(I)V
/* .line 726 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getAurogonAppInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 727 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
/* if-nez v0, :cond_1 */
return;
/* .line 729 */
} // :cond_1
v1 = this.mAllowList;
v1 = v2 = this.mPackageName;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 730 */
return;
/* .line 733 */
} // :cond_2
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 734 */
/* const/16 v1, 0x7d0 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 736 */
} // :cond_3
return;
} // .end method
public void notifyAppSwitchToBg ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .line 695 */
/* sget-boolean v0, Lcom/miui/server/greeze/AurogonImmobulusMode;->IMMOBULUS_ENABLED:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 697 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getPackageNameFromUid(I)Ljava/lang/String; */
/* .line 698 */
/* .local v0, "packageName":Ljava/lang/String; */
final String v1 = "com.android.camera"; // const-string v1, "com.android.camera"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 699 */
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 700 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).quitImmobulusMode ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 704 */
} // :cond_1
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getAurogonAppInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 709 */
/* .local v1, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).updateTargetList ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateTargetList(I)V
/* .line 711 */
/* iget-boolean v2, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mExtremeMode:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 712 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 713 */
/* const/16 v2, 0x7d0 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 721 */
} // :cond_2
return;
} // .end method
public void notifyFgServicesChanged ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .line 739 */
v0 = this.mProcessManagerInternal;
/* if-nez v0, :cond_0 */
/* .line 740 */
final String v0 = "AurogonImmobulusMode"; // const-string v0, "AurogonImmobulusMode"
final String v1 = " mProcessManagerInternal = null"; // const-string v1, " mProcessManagerInternal = null"
android.util.Slog .d ( v0,v1 );
/* .line 741 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getProcessManagerInternal ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getProcessManagerInternal()Lcom/miui/server/process/ProcessManagerInternal;
this.mProcessManagerInternal = v0;
/* .line 745 */
} // :cond_0
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getAurogonAppInfo ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* if-nez v0, :cond_1 */
return;
/* .line 747 */
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkFgServicesList()V */
/* .line 749 */
v0 = this.mProcessManagerInternal;
v0 = (( com.miui.server.process.ProcessManagerInternal ) v0 ).checkAppFgServices ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/process/ProcessManagerInternal;->checkAppFgServices(I)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 750 */
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).updateFgServicesList ( p2, p1, v0 ); // invoke-virtual {p0, p2, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateFgServicesList(IIZ)V
/* .line 752 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).updateFgServicesList ( p2, p1, v0 ); // invoke-virtual {p0, p2, p1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateFgServicesList(IIZ)V
/* .line 754 */
} // :goto_0
return;
} // .end method
public void quitImmobulusMode ( ) {
/* .locals 2 */
/* .line 1213 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1215 */
v0 = this.mHandler;
/* const/16 v1, 0x65 */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1216 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* .line 1217 */
v0 = this.mHandler;
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1218 */
return;
/* .line 1220 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0x66 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendEmptyMessage(I)Z
/* .line 1222 */
} // :cond_1
return;
} // .end method
public void reOrderTargetList ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 281 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$1;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;I)V */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z
/* .line 291 */
return;
} // .end method
public void removeAllMsg ( ) {
/* .locals 2 */
/* .line 1354 */
v0 = this.mHandler;
/* const/16 v1, 0x65 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1355 */
v0 = this.mHandler;
/* const/16 v1, 0x66 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1356 */
v0 = this.mHandler;
/* const/16 v1, 0x67 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1357 */
v0 = this.mHandler;
/* const/16 v1, 0x68 */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1358 */
return;
} // .end method
public void removeTempMutiWindowsApp ( ) {
/* .locals 2 */
/* .line 1268 */
v0 = this.mMutiWindowsApp;
/* monitor-enter v0 */
/* .line 1269 */
try { // :try_start_0
v1 = this.mMutiWindowsApp;
/* .line 1270 */
/* monitor-exit v0 */
/* .line 1271 */
return;
/* .line 1270 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void repeatCheckAppForImmobulusMode ( Integer p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .param p2, "time" # I */
/* .line 450 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).getAurogonAppInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 451 */
/* .local v0, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 452 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( v0, p2 ); // invoke-virtual {p0, v0, p2}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 454 */
} // :cond_0
return;
} // .end method
public void repeatCheckAppForImmobulusMode ( com.miui.server.greeze.AurogonAppInfo p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .param p2, "time" # I */
/* .line 457 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mHandler;
/* const/16 v1, 0x67 */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).hasMessages ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(ILjava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p1, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
/* if-nez v0, :cond_0 */
/* .line 458 */
} // :cond_0
/* const/16 v2, 0x67 */
int v3 = -1; // const/4 v3, -0x1
int v4 = -1; // const/4 v4, -0x1
/* int-to-long v6, p2 */
/* move-object v1, p0 */
/* move-object v5, p1 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 459 */
return;
/* .line 457 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void resetImmobulusModeStatus ( ) {
/* .locals 4 */
/* .line 1247 */
v0 = this.mImmobulusTargetList;
/* monitor-enter v0 */
/* .line 1248 */
try { // :try_start_0
v1 = this.mImmobulusTargetList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 1249 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
int v3 = 0; // const/4 v3, 0x0
/* iput v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
/* .line 1250 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 1251 */
} // :cond_0
/* monitor-exit v0 */
/* .line 1252 */
return;
/* .line 1251 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void sendMeesage ( Integer p0, Integer p1, Integer p2, java.lang.Object p3, Long p4 ) {
/* .locals 3 */
/* .param p1, "what" # I */
/* .param p2, "args1" # I */
/* .param p3, "args2" # I */
/* .param p4, "obj" # Ljava/lang/Object; */
/* .param p5, "delayTime" # J */
/* .line 644 */
v0 = this.mHandler;
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).obtainMessage ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->obtainMessage(I)Landroid/os/Message;
/* .line 645 */
/* .local v0, "msg":Landroid/os/Message; */
int v1 = -1; // const/4 v1, -0x1
/* if-eq p2, v1, :cond_0 */
/* .line 646 */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 648 */
} // :cond_0
/* if-eq p3, v1, :cond_1 */
/* .line 649 */
/* iput p3, v0, Landroid/os/Message;->arg2:I */
/* .line 651 */
} // :cond_1
if ( p4 != null) { // if-eqz p4, :cond_2
/* .line 652 */
this.obj = p4;
/* .line 654 */
} // :cond_2
/* const-wide/16 v1, -0x1 */
/* cmp-long v1, p5, v1 */
/* if-nez v1, :cond_3 */
/* .line 655 */
v1 = this.mHandler;
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 657 */
} // :cond_3
v1 = this.mHandler;
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v1 ).sendMessageDelayed ( v0, p5, p6 ); // invoke-virtual {v1, v0, p5, p6}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 659 */
} // :goto_0
return;
} // .end method
public void simulateNetChange ( ) {
/* .locals 2 */
/* .line 1429 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/greeze/AurogonImmobulusMode$5; */
/* invoke-direct {v1, p0}, Lcom/miui/server/greeze/AurogonImmobulusMode$5;-><init>(Lcom/miui/server/greeze/AurogonImmobulusMode;)V */
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->post(Ljava/lang/Runnable;)Z
/* .line 1436 */
return;
} // .end method
public void triggerImmobulusMode ( Integer p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p1, "uid" # I */
/* .param p2, "mode" # Ljava/lang/String; */
/* .line 1197 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 1199 */
} // :cond_0
v0 = this.mGreezeService;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mScreenOnOff:Z */
/* if-nez v0, :cond_2 */
final String v0 = "ExtremeM"; // const-string v0, "ExtremeM"
v0 = (( java.lang.String ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
/* .line 1200 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1201 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = " triggerImmobulusMode uid = "; // const-string v1, " triggerImmobulusMode uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " mode = "; // const-string v1, " mode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " reject enter because of screen off!"; // const-string v1, " reject enter because of screen off!"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AurogonImmobulusMode"; // const-string v1, "AurogonImmobulusMode"
android.util.Slog .d ( v1,v0 );
/* .line 1203 */
} // :cond_1
return;
/* .line 1206 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* if-nez v0, :cond_3 */
/* .line 1207 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
/* .line 1208 */
/* const/16 v2, 0x65 */
int v4 = -1; // const/4 v4, -0x1
/* const-wide/16 v6, 0xdac */
/* move-object v1, p0 */
/* move v3, p1 */
/* move-object v5, p2 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 1210 */
} // :cond_3
return;
} // .end method
public void triggerLaunchMode ( java.lang.String p0, Integer p1 ) {
/* .locals 8 */
/* .param p1, "processName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 1174 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 1175 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "launch app processName = "; // const-string v1, "launch app processName = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " uid = "; // const-string v1, " uid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "AurogonImmobulusMode"; // const-string v1, "AurogonImmobulusMode"
android.util.Slog .d ( v1,v0 );
/* .line 1176 */
this.mLastPackageName = p1;
/* .line 1178 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1179 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMCamera:Z */
/* .line 1182 */
} // :cond_1
/* const/16 v2, 0x69 */
int v4 = -1; // const/4 v4, -0x1
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, -0x1 */
/* move-object v1, p0 */
/* move v3, p2 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 1183 */
return;
} // .end method
public void triggerLaunchModeAction ( Integer p0 ) {
/* .locals 18 */
/* .param p1, "uid" # I */
/* .line 1021 */
/* move-object/from16 v8, p0 */
/* move/from16 v9, p1 */
/* invoke-virtual/range {p0 ..p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 1022 */
/* .local v10, "tempApp":Lcom/miui/server/greeze/AurogonAppInfo; */
if ( v10 != null) { // if-eqz v10, :cond_0
/* iget-boolean v0, v10, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 1023 */
} // :cond_0
v0 = this.mHandler;
/* const/16 v1, 0x6a */
v0 = (( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).hasMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1025 */
v0 = this.mHandler;
(( com.miui.server.greeze.AurogonImmobulusMode$ImmobulusHandler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode$ImmobulusHandler;->removeMessages(I)V
/* .line 1026 */
/* const/16 v2, 0x6a */
int v3 = -1; // const/4 v3, -0x1
int v4 = -1; // const/4 v4, -0x1
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, 0x1388 */
/* move-object/from16 v1, p0 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 1029 */
} // :cond_1
/* const/16 v2, 0x6a */
int v3 = -1; // const/4 v3, -0x1
int v4 = -1; // const/4 v4, -0x1
int v5 = 0; // const/4 v5, 0x0
/* const-wide/16 v6, 0xbb8 */
/* move-object/from16 v1, p0 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/greeze/AurogonImmobulusMode;->sendMeesage(IIILjava/lang/Object;J)V */
/* .line 1032 */
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* move-object v1, v0 */
/* .line 1033 */
/* .local v1, "log":Ljava/lang/StringBuilder; */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "LM FZ ["; // const-string v2, "LM FZ ["
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9 ); // invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "] uid = ["; // const-string v2, "] uid = ["
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1034 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* move-object v2, v0 */
/* .line 1035 */
/* .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
/* invoke-virtual/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateAppsOnWindowsStatus()V */
/* .line 1036 */
/* invoke-direct/range {p0 ..p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateIMEAppStatus()V */
/* .line 1038 */
v3 = this.mImmobulusTargetList;
/* monitor-enter v3 */
/* .line 1039 */
try { // :try_start_0
v0 = this.mImmobulusTargetList;
} // :cond_2
v4 = } // :goto_1
if ( v4 != null) { // if-eqz v4, :cond_f
/* check-cast v4, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 1040 */
/* .local v4, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
/* iget v5, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* if-ne v9, v5, :cond_3 */
/* .line 1042 */
} // :cond_3
v5 = this.mAllowList;
v5 = v6 = this.mPackageName;
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 1043 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1044 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is allow list app, skip check!"; // const-string v7, " is allow list app, skip check!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1049 */
} // :cond_4
v5 = this.mNoRestrictAppSet;
v5 = v6 = this.mPackageName;
if ( v5 != null) { // if-eqz v5, :cond_5
/* .line 1050 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1051 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is mNoRestrictAppSet list app, skip check!"; // const-string v7, " is mNoRestrictAppSet list app, skip check!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1056 */
} // :cond_5
v5 = this.mGreezeService;
/* iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v5 = (( com.miui.server.greeze.GreezeManagerService ) v5 ).checkRecentLuanchedApp ( v6 ); // invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->checkRecentLuanchedApp(I)Z
if ( v5 != null) { // if-eqz v5, :cond_6
/* .line 1057 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1058 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is launched at same time with topp app, skip check!"; // const-string v7, " is launched at same time with topp app, skip check!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* goto/16 :goto_1 */
/* .line 1064 */
} // :cond_6
v5 = this.mGreezeService;
/* iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v5 = (( com.miui.server.greeze.GreezeManagerService ) v5 ).isAppRunning ( v6 ); // invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->isAppRunning(I)Z
/* if-nez v5, :cond_7 */
/* .line 1065 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1066 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is not running app, skip check!"; // const-string v7, " is not running app, skip check!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* goto/16 :goto_1 */
/* .line 1071 */
} // :cond_7
v5 = this.mOnWindowsAppList;
if ( v5 != null) { // if-eqz v5, :cond_8
/* iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v5 = java.lang.Integer .valueOf ( v6 );
if ( v5 != null) { // if-eqz v5, :cond_8
/* .line 1072 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1073 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is show on screen, skip it!"; // const-string v7, " is show on screen, skip it!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* goto/16 :goto_1 */
/* .line 1078 */
} // :cond_8
v5 = this.mPackageName;
v5 = (( com.miui.server.greeze.AurogonImmobulusMode ) v8 ).isWallPaperApp ( v5 ); // invoke-virtual {v8, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isWallPaperApp(Ljava/lang/String;)Z
if ( v5 != null) { // if-eqz v5, :cond_9
/* .line 1079 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1080 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is WallPaperApp app, skip check!"; // const-string v7, " is WallPaperApp app, skip check!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* goto/16 :goto_1 */
/* .line 1085 */
} // :cond_9
v5 = this.mGreezeService;
/* iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
v5 = (( com.miui.server.greeze.GreezeManagerService ) v5 ).isUidFrozen ( v6 ); // invoke-virtual {v5, v6}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v5 != null) { // if-eqz v5, :cond_b
/* .line 1086 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v5 != null) { // if-eqz v5, :cond_a
/* .line 1087 */
final String v5 = "AurogonImmobulusMode"; // const-string v5, "AurogonImmobulusMode"
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " uid = "; // const-string v7, " uid = "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = " is frozen app, skip check!"; // const-string v7, " is frozen app, skip check!"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 1089 */
} // :cond_a
v5 = this.mGreezeService;
/* iget v6, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* const/16 v7, 0x10 */
(( com.miui.server.greeze.GreezeManagerService ) v5 ).updateFrozenInfoForImmobulus ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->updateFrozenInfoForImmobulus(II)V
/* .line 1090 */
/* goto/16 :goto_1 */
/* .line 1093 */
} // :cond_b
v5 = (( com.miui.server.greeze.AurogonImmobulusMode ) v8 ).checkAppStatusForFreeze ( v4 ); // invoke-virtual {v8, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->checkAppStatusForFreeze(Lcom/miui/server/greeze/AurogonAppInfo;)Z
/* if-nez v5, :cond_c */
/* .line 1094 */
/* goto/16 :goto_1 */
/* .line 1097 */
} // :cond_c
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v5 != null) { // if-eqz v5, :cond_d
/* .line 1098 */
/* iget v5, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
java.lang.Integer .valueOf ( v5 );
/* .line 1099 */
/* goto/16 :goto_1 */
/* .line 1101 */
} // :cond_d
v5 = (( com.miui.server.greeze.AurogonImmobulusMode ) v8 ).freezeActionForLaunchMode ( v4 ); // invoke-virtual {v8, v4}, Lcom/miui/server/greeze/AurogonImmobulusMode;->freezeActionForLaunchMode(Lcom/miui/server/greeze/AurogonAppInfo;)Z
/* .line 1102 */
/* .local v5, "success":Z */
if ( v5 != null) { // if-eqz v5, :cond_e
/* .line 1103 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v7, v4, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1105 */
} // .end local v4 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // .end local v5 # "success":Z
} // :cond_e
/* goto/16 :goto_1 */
/* .line 1106 */
} // :cond_f
/* monitor-exit v3 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 1107 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->mCgroupV1Flag:Z */
if ( v0 != null) { // if-eqz v0, :cond_14
v0 = /* .line 1108 */
/* if-lez v0, :cond_14 */
v0 = /* .line 1109 */
/* new-array v0, v0, [I */
/* .line 1110 */
/* .local v0, "uids":[I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
v4 = } // :goto_2
/* if-ge v3, v4, :cond_10 */
/* .line 1111 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* aput v4, v0, v3 */
/* .line 1110 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1113 */
} // .end local v3 # "i":I
} // :cond_10
v11 = this.mGreezeService;
/* const-wide/16 v13, 0x0 */
/* const/16 v15, 0x10 */
final String v16 = "LAUNCH_MODE"; // const-string v16, "LAUNCH_MODE"
/* const/16 v17, 0x0 */
/* move-object v12, v0 */
/* invoke-virtual/range {v11 ..v17}, Lcom/miui/server/greeze/GreezeManagerService;->freezeUids([IJILjava/lang/String;Z)Ljava/util/List; */
/* .line 1114 */
/* .local v3, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_14
/* check-cast v5, Ljava/lang/Integer; */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
/* .line 1115 */
/* .local v5, "temp":I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = " "; // const-string v7, " "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1116 */
(( com.miui.server.greeze.AurogonImmobulusMode ) v8 ).getAurogonAppInfo ( v5 ); // invoke-virtual {v8, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getAurogonAppInfo(I)Lcom/miui/server/greeze/AurogonAppInfo;
/* .line 1117 */
/* .local v6, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
/* if-nez v6, :cond_11 */
/* .line 1118 */
} // :cond_11
/* iget-boolean v7, v6, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z */
/* if-nez v7, :cond_12 */
/* iget-boolean v7, v6, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
/* if-nez v7, :cond_13 */
/* .line 1119 */
} // :cond_12
(( com.miui.server.greeze.AurogonImmobulusMode ) v8 ).addLaunchModeQiutList ( v6 ); // invoke-virtual {v8, v6}, Lcom/miui/server/greeze/AurogonImmobulusMode;->addLaunchModeQiutList(Lcom/miui/server/greeze/AurogonAppInfo;)V
/* .line 1121 */
} // .end local v5 # "temp":I
} // .end local v6 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // :cond_13
/* .line 1124 */
} // .end local v0 # "uids":[I
} // .end local v3 # "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_14
final String v0 = "]"; // const-string v0, "]"
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 1125 */
v0 = this.mGreezeService;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).addToDumpHistory ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/greeze/GreezeManagerService;->addToDumpHistory(Ljava/lang/String;)V
/* .line 1126 */
return;
/* .line 1106 */
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_1
/* monitor-exit v3 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v0 */
} // .end method
public void triggerVideoMode ( Boolean p0, java.lang.String p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "allow" # Z */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .param p3, "uid" # I */
/* .line 1225 */
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mImmobulusModeEnabled:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 1226 */
} // :cond_0
v0 = this.mGreezeService;
/* iget-boolean v0, v0, Lcom/miui/server/greeze/GreezeManagerService;->mInFreeformSmallWinMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 1227 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1228 */
final String v0 = "AurogonImmobulusMode"; // const-string v0, "AurogonImmobulusMode"
final String v1 = "SmallWindowMode skip videoMode!"; // const-string v1, "SmallWindowMode skip videoMode!"
android.util.Slog .d ( v0,v1 );
/* .line 1230 */
} // :cond_1
return;
/* .line 1232 */
} // :cond_2
if ( p1 != null) { // if-eqz p1, :cond_3
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideoCloud:Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 1233 */
final String v0 = "Video"; // const-string v0, "Video"
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).triggerImmobulusMode ( p3, v0 ); // invoke-virtual {p0, p3, v0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->triggerImmobulusMode(ILjava/lang/String;)V
/* .line 1234 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z */
/* .line 1235 */
this.mCurrentVideoPacageName = p2;
/* .line 1237 */
} // :cond_3
/* iget-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
if ( p2 != null) { // if-eqz p2, :cond_5
/* .line 1238 */
v0 = this.mCurrentVideoPacageName;
v0 = (( java.lang.String ) p2 ).equals ( v0 ); // invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
return;
/* .line 1239 */
} // :cond_4
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).quitImmobulusMode ( ); // invoke-virtual {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->quitImmobulusMode()V
/* .line 1240 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterIMVideo:Z */
/* .line 1241 */
final String v0 = ""; // const-string v0, ""
this.mCurrentVideoPacageName = v0;
/* .line 1244 */
} // :cond_5
} // :goto_0
return;
} // .end method
public Boolean unFreezeActionForImmobulus ( com.miui.server.greeze.AurogonAppInfo p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "app" # Lcom/miui/server/greeze/AurogonAppInfo; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 575 */
v0 = this.mGreezeService;
/* iget v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* const/16 v2, 0x8 */
v0 = (( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUid ( v1, v2, p2 ); // invoke-virtual {v0, v1, v2, p2}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 576 */
/* .local v0, "success":Z */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 577 */
/* iget-boolean v1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mEnterImmobulusMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 578 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 579 */
/* .local v1, "currentTime":J */
/* iget-wide v3, p1, Lcom/miui/server/greeze/AurogonAppInfo;->freezeTime:J */
/* sub-long v3, v1, v3 */
/* .line 580 */
/* .local v3, "durration":J */
/* const-wide/16 v5, 0x4e20 */
/* cmp-long v5, v3, v5 */
/* if-gez v5, :cond_0 */
/* .line 581 */
/* iget v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
/* add-int/lit8 v5, v5, 0x1 */
/* iput v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
/* .line 582 */
} // :cond_0
/* const-wide/32 v5, 0xea60 */
/* cmp-long v5, v3, v5 */
/* if-lez v5, :cond_1 */
/* .line 583 */
/* iget v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
/* add-int/lit8 v5, v5, -0x1 */
/* iput v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
/* .line 585 */
} // :cond_1
} // :goto_0
/* iput-wide v1, p1, Lcom/miui/server/greeze/AurogonAppInfo;->unFreezeTime:J */
/* .line 586 */
/* iget v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
int v6 = 3; // const/4 v6, 0x3
/* if-lt v5, v6, :cond_2 */
/* .line 587 */
v5 = this.mPackageName;
final String v6 = "frequent wakeup"; // const-string v6, "frequent wakeup"
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).forceStopAppForImmobulus ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/miui/server/greeze/AurogonImmobulusMode;->forceStopAppForImmobulus(Ljava/lang/String;Ljava/lang/String;)V
/* .line 588 */
int v5 = 0; // const/4 v5, 0x0
/* iput v5, p1, Lcom/miui/server/greeze/AurogonAppInfo;->level:I */
/* .line 589 */
/* .line 591 */
} // :cond_2
/* const/16 v5, 0x7d0 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).repeatCheckAppForImmobulusMode ( p1, v5 ); // invoke-virtual {p0, p1, v5}, Lcom/miui/server/greeze/AurogonImmobulusMode;->repeatCheckAppForImmobulusMode(Lcom/miui/server/greeze/AurogonAppInfo;I)V
/* .line 592 */
} // .end local v1 # "currentTime":J
} // .end local v3 # "durration":J
/* .line 594 */
} // :cond_3
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = " unFreeze uid = "; // const-string v2, " unFreeze uid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " reason : "; // const-string v2, " reason : "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " failed!"; // const-string v2, " failed!"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "AurogonImmobulusMode"; // const-string v2, "AurogonImmobulusMode"
android.util.Slog .d ( v2,v1 );
/* .line 596 */
} // :cond_4
} // :goto_1
} // .end method
public void updateAppsOnWindowsStatus ( ) {
/* .locals 6 */
/* .line 792 */
v0 = this.mGreezeService;
v0 = this.mWindowManager;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 794 */
try { // :try_start_0
v0 = this.mGreezeService;
v0 = this.mWindowManager;
/* .line 795 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 796 */
v1 = this.mOnWindowsAppList;
/* .line 797 */
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/lang/String; */
/* .line 798 */
/* .local v2, "uid":Ljava/lang/String; */
v3 = this.mOnWindowsAppList;
v4 = java.lang.Integer .parseInt ( v2 );
java.lang.Integer .valueOf ( v4 );
/* .line 799 */
/* sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 800 */
final String v3 = "AurogonImmobulusMode"; // const-string v3, "AurogonImmobulusMode"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Integer.parseInt(uid) = "; // const-string v5, "Integer.parseInt(uid) = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = java.lang.Integer .parseInt ( v2 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v4 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 801 */
} // .end local v2 # "uid":Ljava/lang/String;
} // :cond_0
/* .line 804 */
} // .end local v0 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_1
/* .line 803 */
/* :catch_0 */
/* move-exception v0 */
/* .line 806 */
} // :cond_2
} // :goto_1
return;
} // .end method
public void updateFgServicesList ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 4 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "allow" # Z */
/* .line 961 */
v0 = this.mFgServiceAppList;
/* monitor-enter v0 */
/* .line 962 */
try { // :try_start_0
v1 = this.mFgServiceAppList;
java.lang.Integer .valueOf ( p1 );
/* check-cast v1, Ljava/util/List; */
/* .line 963 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;" */
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 964 */
/* if-nez v1, :cond_0 */
/* .line 965 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* move-object v1, v2 */
/* .line 966 */
v2 = this.mFgServiceAppList;
java.lang.Integer .valueOf ( p1 );
/* .line 969 */
} // :cond_0
v2 = java.lang.Integer .valueOf ( p2 );
/* if-nez v2, :cond_2 */
/* .line 970 */
java.lang.Integer .valueOf ( p2 );
/* .line 973 */
} // :cond_1
if ( v1 != null) { // if-eqz v1, :cond_2
v2 = java.lang.Integer .valueOf ( p2 );
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 974 */
java.lang.Integer .valueOf ( p2 );
v2 = /* .line 975 */
/* if-nez v2, :cond_2 */
/* .line 976 */
v2 = this.mFgServiceAppList;
java.lang.Integer .valueOf ( p1 );
/* .line 980 */
} // .end local v1 # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
/* .line 981 */
return;
/* .line 980 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateTargetList ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "uid" # I */
/* .line 826 */
v0 = this.mImmobulusTargetList;
/* monitor-enter v0 */
/* .line 828 */
try { // :try_start_0
v1 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isUidValid ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isUidValid(I)Z
/* if-nez v1, :cond_0 */
/* monitor-exit v0 */
return;
/* .line 829 */
} // :cond_0
v1 = this.mImmobulusTargetList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* .line 830 */
/* .local v2, "app":Lcom/miui/server/greeze/AurogonAppInfo; */
/* iget v3, v2, Lcom/miui/server/greeze/AurogonAppInfo;->mUid:I */
/* if-ne v3, p1, :cond_1 */
/* .line 831 */
/* monitor-exit v0 */
return;
/* .line 833 */
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
} // :cond_1
/* .line 835 */
} // :cond_2
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->getPackageNameFromUid(I)Ljava/lang/String; */
/* .line 836 */
/* .local v1, "packageName":Ljava/lang/String; */
/* if-nez v1, :cond_3 */
/* monitor-exit v0 */
return;
/* .line 838 */
} // :cond_3
final String v2 = "com.android.camera"; // const-string v2, "com.android.camera"
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 839 */
/* iput p1, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCameraUid:I */
/* .line 842 */
} // :cond_4
final String v2 = ":"; // const-string v2, ":"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
int v3 = 0; // const/4 v3, 0x0
/* aget-object v2, v2, v3 */
/* move-object v1, v2 */
/* .line 843 */
final String v2 = ".qualcomm"; // const-string v2, ".qualcomm"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_9 */
final String v2 = "com.qti"; // const-string v2, "com.qti"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 844 */
} // :cond_5
v2 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isCtsApp ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isCtsApp(Ljava/lang/String;)Z
int v4 = 1; // const/4 v4, 0x1
if ( v2 != null) { // if-eqz v2, :cond_6
final String v2 = "ctsshim"; // const-string v2, "ctsshim"
v2 = (( java.lang.String ) v1 ).contains ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
/* if-nez v2, :cond_6 */
/* .line 845 */
(( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).QuitLaunchModeAction ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/greeze/AurogonImmobulusMode;->QuitLaunchModeAction(Z)V
/* .line 846 */
/* iput-boolean v3, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mLaunchModeEnabled:Z */
/* .line 847 */
/* iput-boolean v4, p0, Lcom/miui/server/greeze/AurogonImmobulusMode;->mCtsModeOn:Z */
/* .line 848 */
final String v2 = "AurogonImmobulusMode"; // const-string v2, "AurogonImmobulusMode"
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "ts " */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 849 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/AurogonImmobulusMode;->updateCtsStatus()V */
/* .line 852 */
} // :cond_6
/* new-instance v2, Lcom/miui/server/greeze/AurogonAppInfo; */
/* invoke-direct {v2, p1, v1}, Lcom/miui/server/greeze/AurogonAppInfo;-><init>(ILjava/lang/String;)V */
/* .line 853 */
/* .restart local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo; */
v3 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isAppHasIcon ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isAppHasIcon(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 854 */
/* iput-boolean v4, v2, Lcom/miui/server/greeze/AurogonAppInfo;->hasIcon:Z */
/* .line 857 */
} // :cond_7
v3 = (( com.miui.server.greeze.AurogonImmobulusMode ) p0 ).isSystemOrMiuiImportantApp ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/greeze/AurogonImmobulusMode;->isSystemOrMiuiImportantApp(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_8
/* .line 858 */
/* iput-boolean v4, v2, Lcom/miui/server/greeze/AurogonAppInfo;->isSystemApp:Z */
/* .line 860 */
} // :cond_8
v3 = this.mImmobulusTargetList;
/* .line 861 */
/* nop */
} // .end local v1 # "packageName":Ljava/lang/String;
} // .end local v2 # "app":Lcom/miui/server/greeze/AurogonAppInfo;
/* monitor-exit v0 */
/* .line 862 */
return;
/* .line 843 */
/* .restart local v1 # "packageName":Ljava/lang/String; */
} // :cond_9
} // :goto_1
/* monitor-exit v0 */
return;
/* .line 861 */
} // .end local v1 # "packageName":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void updateVpnStatus ( android.net.NetworkInfo p0 ) {
/* .locals 14 */
/* .param p1, "networkInfo" # Landroid/net/NetworkInfo; */
/* .line 1439 */
v0 = this.mConnMgr;
(( android.net.ConnectivityManager ) v0 ).getAllNetworks ( ); // invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getAllNetworks()[Landroid/net/Network;
/* .line 1440 */
/* .local v0, "net":[Landroid/net/Network; */
v1 = this.mVPNAppList;
/* .line 1441 */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
/* move v3, v2 */
} // :goto_0
/* if-ge v3, v1, :cond_3 */
/* aget-object v4, v0, v3 */
/* .line 1442 */
/* .local v4, "temp":Landroid/net/Network; */
v5 = this.mConnMgr;
(( android.net.ConnectivityManager ) v5 ).getNetworkCapabilities ( v4 ); // invoke-virtual {v5, v4}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;
/* .line 1443 */
/* .local v5, "nc":Landroid/net/NetworkCapabilities; */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 1444 */
(( android.net.NetworkCapabilities ) v5 ).getAdministratorUids ( ); // invoke-virtual {v5}, Landroid/net/NetworkCapabilities;->getAdministratorUids()[I
/* .line 1445 */
/* .local v6, "uids":[I */
v7 = this.mVPNAppList;
/* monitor-enter v7 */
/* .line 1446 */
try { // :try_start_0
/* array-length v8, v6 */
/* move v9, v2 */
} // :goto_1
/* if-ge v9, v8, :cond_1 */
/* aget v10, v6, v9 */
/* .line 1447 */
/* .local v10, "uid":I */
/* sget-boolean v11, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v11 != null) { // if-eqz v11, :cond_0
/* .line 1448 */
final String v11 = "AurogonImmobulusMode"; // const-string v11, "AurogonImmobulusMode"
/* new-instance v12, Ljava/lang/StringBuilder; */
/* invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V */
final String v13 = " vpn uid = "; // const-string v13, " vpn uid = "
(( java.lang.StringBuilder ) v12 ).append ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).append ( v10 ); // invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v12 ).toString ( ); // invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v11,v12 );
/* .line 1450 */
} // :cond_0
v11 = this.mVPNAppList;
java.lang.Integer .valueOf ( v10 );
/* .line 1446 */
/* nop */
} // .end local v10 # "uid":I
/* add-int/lit8 v9, v9, 0x1 */
/* .line 1452 */
} // :cond_1
/* monitor-exit v7 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 1441 */
} // .end local v4 # "temp":Landroid/net/Network;
} // .end local v5 # "nc":Landroid/net/NetworkCapabilities;
} // .end local v6 # "uids":[I
} // :cond_2
} // :goto_2
/* add-int/lit8 v3, v3, 0x1 */
/* .line 1455 */
} // :cond_3
return;
} // .end method
public void updateWidgetPackages ( ) {
/* .locals 7 */
/* .line 1558 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "enabled_widgets"; // const-string v1, "enabled_widgets"
android.provider.Settings$Secure .getString ( v0,v1 );
/* .line 1559 */
/* .local v0, "widgetPackages":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1560 */
final String v1 = "AurogonImmobulusMode"; // const-string v1, "AurogonImmobulusMode"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = " widgetPackages = "; // const-string v3, " widgetPackages = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 1561 */
final String v1 = ":"; // const-string v1, ":"
(( java.lang.String ) v0 ).split ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 1562 */
/* .local v1, "strs":[Ljava/lang/String; */
/* array-length v2, v1 */
/* if-lez v2, :cond_1 */
/* .line 1563 */
v2 = this.mCurrentWidgetPackages;
/* monitor-enter v2 */
/* .line 1564 */
try { // :try_start_0
v3 = this.mCurrentWidgetPackages;
/* .line 1565 */
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_0 */
/* aget-object v5, v1, v4 */
/* .line 1566 */
/* .local v5, "str":Ljava/lang/String; */
v6 = this.mCurrentWidgetPackages;
/* .line 1565 */
/* nop */
} // .end local v5 # "str":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 1568 */
} // :cond_0
/* monitor-exit v2 */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v2 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 1571 */
} // .end local v1 # "strs":[Ljava/lang/String;
} // :cond_1
} // :goto_1
return;
} // .end method
