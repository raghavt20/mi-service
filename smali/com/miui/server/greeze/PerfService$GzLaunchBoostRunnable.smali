.class Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;
.super Ljava/lang/Object;
.source "PerfService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/greeze/PerfService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GzLaunchBoostRunnable"
.end annotation


# instance fields
.field bundle:Landroid/os/Bundle;

.field final synthetic this$0:Lcom/miui/server/greeze/PerfService;


# direct methods
.method public constructor <init>(Lcom/miui/server/greeze/PerfService;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/greeze/PerfService;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 70
    iput-object p1, p0, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p2, p0, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->bundle:Landroid/os/Bundle;

    .line 72
    return-void
.end method


# virtual methods
.method public run()V
    .locals 35

    .line 76
    move-object/from16 v1, p0

    const-string v2, "Failed to get cast pid"

    iget-object v0, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->bundle:Landroid/os/Bundle;

    const-string/jumbo v3, "uid"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 77
    .local v3, "launchinguid":I
    iget-object v0, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->bundle:Landroid/os/Bundle;

    const-string v4, "launchingActivity"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 78
    .local v4, "launchingActivity":Ljava/lang/String;
    iget-object v0, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->bundle:Landroid/os/Bundle;

    const-string v5, "fromUid"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 79
    .local v5, "fromUid":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 80
    .local v6, "startTime":J
    const-string v0, "GzBoost"

    const-wide/16 v8, 0x40

    invoke-static {v8, v9, v0}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 82
    sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    const-string v10, "GzBoost "

    if-eqz v0, :cond_0

    .line 83
    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", start"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v11}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    const/4 v11, -0x1

    .line 90
    .local v11, "castPid":I
    const/4 v12, 0x0

    :try_start_0
    iget-object v0, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-static {v0}, Lcom/miui/server/greeze/PerfService;->-$$Nest$fgetmGetCastPid(Lcom/miui/server/greeze/PerfService;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 91
    iget-object v0, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-static {v0}, Lcom/miui/server/greeze/PerfService;->-$$Nest$fgetmGetCastPid(Lcom/miui/server/greeze/PerfService;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v13, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-static {v13}, Lcom/miui/server/greeze/PerfService;->-$$Nest$fgetmAms(Lcom/miui/server/greeze/PerfService;)Lcom/android/server/am/ActivityManagerService;

    move-result-object v13

    new-array v14, v12, [Ljava/lang/Object;

    invoke-virtual {v0, v13, v14}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move v11, v0

    .line 97
    :cond_1
    :goto_0
    goto :goto_1

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 93
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    goto :goto_0

    .line 100
    :goto_1
    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    move-object v2, v0

    .line 101
    .local v2, "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getAudioUid()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 102
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getIMEUid()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 106
    :try_start_1
    invoke-static {}, Lmiui/process/ProcessManager;->getForegroundInfo()Lmiui/process/ForegroundInfo;

    move-result-object v0

    .line 107
    .local v0, "foregroundInfo":Lmiui/process/ForegroundInfo;
    iget v13, v0, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v2, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 108
    iget v13, v0, Lmiui/process/ForegroundInfo;->mMultiWindowForegroundUid:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v2, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 111
    nop

    .end local v0    # "foregroundInfo":Lmiui/process/ForegroundInfo;
    goto :goto_2

    .line 109
    :catch_2
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v13

    const-string v14, "Failed to get foreground info from ProcessManager"

    invoke-static {v13, v14, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 114
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    const/4 v0, 0x0

    .line 115
    .local v0, "fzCount":I
    invoke-static {}, Lcom/miui/server/greeze/GreezeServiceUtils;->getUidMap()Landroid/util/SparseArray;

    move-result-object v13

    .line 116
    .local v13, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_3
    invoke-virtual {v13}, Landroid/util/SparseArray;->size()I

    move-result v15

    const-string v12, "ms"

    const-string v8, " from "

    if-ge v14, v15, :cond_13

    .line 117
    invoke-virtual {v13, v14}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v9

    .line 118
    .local v9, "uid":I
    invoke-static {v9}, Landroid/os/UserHandle;->isApp(I)Z

    move-result v15

    if-eqz v15, :cond_11

    if-eq v9, v3, :cond_11

    if-ne v9, v5, :cond_2

    .line 119
    move/from16 v20, v0

    move-object/from16 v16, v2

    move/from16 v21, v3

    move-wide/from16 v23, v6

    move/from16 v27, v11

    move-object/from16 v22, v13

    move/from16 v28, v14

    goto/16 :goto_b

    .line 122
    :cond_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v2, v15}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v15

    move-object/from16 v16, v2

    .end local v2    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local v16, "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const-string v2, "GzBoost skip uid "

    if-eqz v15, :cond_4

    .line 123
    sget-boolean v8, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_SKIPUID:Z

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v8

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v12, " for dynamic white list"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move/from16 v20, v0

    move/from16 v21, v3

    move-wide/from16 v23, v6

    move/from16 v27, v11

    move-object/from16 v22, v13

    move/from16 v28, v14

    goto/16 :goto_b

    .line 127
    :cond_4
    invoke-virtual {v13, v14}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/List;

    .line 129
    .local v15, "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    const/16 v17, 0x0

    .line 130
    .local v17, "skipUid":Z
    const-string v18, ""

    .line 131
    .local v18, "skipReason":Ljava/lang/String;
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    move/from16 v21, v3

    .end local v3    # "launchinguid":I
    .local v21, "launchinguid":I
    const-string v3, " "

    if-eqz v20, :cond_d

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v22, v13

    .end local v13    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .local v22, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    move-object/from16 v13, v20

    check-cast v13, Lcom/miui/server/greeze/RunningProcess;

    .line 132
    .local v13, "proc":Lcom/miui/server/greeze/RunningProcess;
    move/from16 v20, v0

    .end local v0    # "fzCount":I
    .local v20, "fzCount":I
    iget-boolean v0, v13, Lcom/miui/server/greeze/RunningProcess;->hasForegroundActivities:Z

    if-eqz v0, :cond_5

    .line 133
    const/16 v17, 0x1

    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v23, v6

    .end local v6    # "startTime":J
    .local v23, "startTime":J
    iget v6, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, v13, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " has foreground activity"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 135
    move/from16 v27, v11

    move/from16 v28, v14

    move-object/from16 v0, v18

    goto/16 :goto_9

    .line 137
    .end local v23    # "startTime":J
    .restart local v6    # "startTime":J
    :cond_5
    move-wide/from16 v23, v6

    .end local v6    # "startTime":J
    .restart local v23    # "startTime":J
    iget-boolean v0, v13, Lcom/miui/server/greeze/RunningProcess;->hasForegroundServices:Z

    if-eqz v0, :cond_6

    .line 138
    const/16 v17, 0x1

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, v13, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " has foreground service"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 140
    move/from16 v27, v11

    move/from16 v28, v14

    move-object/from16 v0, v18

    goto/16 :goto_9

    .line 142
    :cond_6
    iget v0, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    if-ne v0, v11, :cond_7

    .line 143
    const/16 v17, 0x1

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, v13, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " has cast activity"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 145
    move/from16 v27, v11

    move/from16 v28, v14

    move-object/from16 v0, v18

    goto/16 :goto_9

    .line 147
    :cond_7
    iget-object v0, v13, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 148
    iget-object v0, v13, Lcom/miui/server/greeze/RunningProcess;->pkgList:[Ljava/lang/String;

    array-length v6, v0

    const/4 v7, 0x0

    :goto_5
    if-ge v7, v6, :cond_b

    move/from16 v25, v6

    aget-object v6, v0, v7

    .line 149
    .local v6, "pkg":Ljava/lang/String;
    move-object/from16 v26, v0

    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetWHITELIST_PKG()[Ljava/lang/String;

    move-result-object v0

    move/from16 v27, v11

    .end local v11    # "castPid":I
    .local v27, "castPid":I
    array-length v11, v0

    move/from16 v28, v14

    const/4 v14, 0x0

    .end local v14    # "i":I
    .local v28, "i":I
    :goto_6
    if-ge v14, v11, :cond_9

    move/from16 v29, v11

    aget-object v11, v0, v14

    .line 150
    .local v11, "whitePkg":Ljava/lang/String;
    invoke-static {v6, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 151
    const/4 v0, 0x1

    .line 152
    .end local v17    # "skipUid":Z
    .local v0, "skipUid":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v17, v0

    .end local v0    # "skipUid":Z
    .restart local v17    # "skipUid":Z
    iget v0, v13, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v14, v13, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v14, " in whitelist"

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    .end local v18    # "skipReason":Ljava/lang/String;
    .local v0, "skipReason":Ljava/lang/String;
    move-object/from16 v18, v0

    goto :goto_7

    .line 149
    .end local v0    # "skipReason":Ljava/lang/String;
    .end local v11    # "whitePkg":Ljava/lang/String;
    .restart local v18    # "skipReason":Ljava/lang/String;
    :cond_8
    add-int/lit8 v14, v14, 0x1

    move/from16 v11, v29

    goto :goto_6

    .line 156
    :cond_9
    :goto_7
    if-eqz v17, :cond_a

    goto :goto_8

    .line 148
    .end local v6    # "pkg":Ljava/lang/String;
    :cond_a
    add-int/lit8 v7, v7, 0x1

    move/from16 v6, v25

    move-object/from16 v0, v26

    move/from16 v11, v27

    move/from16 v14, v28

    goto :goto_5

    .end local v27    # "castPid":I
    .end local v28    # "i":I
    .local v11, "castPid":I
    .restart local v14    # "i":I
    :cond_b
    move/from16 v27, v11

    move/from16 v28, v14

    .end local v11    # "castPid":I
    .end local v14    # "i":I
    .restart local v27    # "castPid":I
    .restart local v28    # "i":I
    goto :goto_8

    .line 147
    .end local v27    # "castPid":I
    .end local v28    # "i":I
    .restart local v11    # "castPid":I
    .restart local v14    # "i":I
    :cond_c
    move/from16 v27, v11

    move/from16 v28, v14

    .line 159
    .end local v11    # "castPid":I
    .end local v13    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .end local v14    # "i":I
    .restart local v27    # "castPid":I
    .restart local v28    # "i":I
    :goto_8
    move/from16 v0, v20

    move/from16 v3, v21

    move-object/from16 v13, v22

    move-wide/from16 v6, v23

    move/from16 v11, v27

    move/from16 v14, v28

    goto/16 :goto_4

    .line 131
    .end local v20    # "fzCount":I
    .end local v22    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v23    # "startTime":J
    .end local v27    # "castPid":I
    .end local v28    # "i":I
    .local v0, "fzCount":I
    .local v6, "startTime":J
    .restart local v11    # "castPid":I
    .local v13, "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v14    # "i":I
    :cond_d
    move/from16 v20, v0

    move-wide/from16 v23, v6

    move/from16 v27, v11

    move-object/from16 v22, v13

    move/from16 v28, v14

    .end local v0    # "fzCount":I
    .end local v6    # "startTime":J
    .end local v11    # "castPid":I
    .end local v13    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v14    # "i":I
    .restart local v20    # "fzCount":I
    .restart local v22    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v23    # "startTime":J
    .restart local v27    # "castPid":I
    .restart local v28    # "i":I
    move-object/from16 v0, v18

    .line 160
    .end local v18    # "skipReason":Ljava/lang/String;
    .local v0, "skipReason":Ljava/lang/String;
    :goto_9
    if-eqz v17, :cond_e

    .line 161
    sget-boolean v3, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_SKIPUID:Z

    if-eqz v3, :cond_12

    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", because "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 166
    :cond_e
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/miui/server/greeze/RunningProcess;

    .line 167
    .local v6, "proc":Lcom/miui/server/greeze/RunningProcess;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", freezing "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v11, v6, Lcom/miui/server/greeze/RunningProcess;->uid:I

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v11, v6, Lcom/miui/server/greeze/RunningProcess;->pid:I

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v11, v6, Lcom/miui/server/greeze/RunningProcess;->processName:Ljava/lang/String;

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, " timeout="

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-wide v13, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J

    invoke-virtual {v7, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 170
    .local v7, "msg":Ljava/lang/String;
    sget-boolean v11, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z

    if-eqz v11, :cond_f

    .line 171
    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_f
    iget-object v11, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-static {v11}, Lcom/miui/server/greeze/PerfService;->-$$Nest$fgetmService(Lcom/miui/server/greeze/PerfService;)Lcom/miui/server/greeze/GreezeManagerService;

    move-result-object v29

    sget-wide v31, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->LAUNCH_FZ_TIMEOUT:J

    sget v33, Lcom/miui/server/greeze/GreezeServiceUtils;->GREEZER_MODULE_PERFORMANCE:I

    move-object/from16 v30, v6

    move-object/from16 v34, v7

    invoke-virtual/range {v29 .. v34}, Lcom/miui/server/greeze/GreezeManagerService;->freezeProcess(Lcom/miui/server/greeze/RunningProcess;JILjava/lang/String;)Z

    .line 174
    nop

    .end local v6    # "proc":Lcom/miui/server/greeze/RunningProcess;
    .end local v7    # "msg":Ljava/lang/String;
    add-int/lit8 v20, v20, 0x1

    .line 175
    goto :goto_a

    .line 176
    :cond_10
    iget-object v2, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-static {v2}, Lcom/miui/server/greeze/PerfService;->-$$Nest$fgetmService(Lcom/miui/server/greeze/PerfService;)Lcom/miui/server/greeze/GreezeManagerService;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/miui/server/greeze/GreezeManagerService;->queryBinderState(I)V

    .line 177
    iget-object v2, v1, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;->this$0:Lcom/miui/server/greeze/PerfService;

    invoke-static {v2}, Lcom/miui/server/greeze/PerfService;->-$$Nest$fgetmService(Lcom/miui/server/greeze/PerfService;)Lcom/miui/server/greeze/GreezeManagerService;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/miui/server/greeze/GreezeManagerService;->monitorNet(I)V

    move/from16 v0, v20

    goto :goto_c

    .line 118
    .end local v15    # "procs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;"
    .end local v16    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v17    # "skipUid":Z
    .end local v20    # "fzCount":I
    .end local v21    # "launchinguid":I
    .end local v22    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v23    # "startTime":J
    .end local v27    # "castPid":I
    .end local v28    # "i":I
    .local v0, "fzCount":I
    .restart local v2    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v3    # "launchinguid":I
    .local v6, "startTime":J
    .restart local v11    # "castPid":I
    .restart local v13    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v14    # "i":I
    :cond_11
    move/from16 v20, v0

    move-object/from16 v16, v2

    move/from16 v21, v3

    move-wide/from16 v23, v6

    move/from16 v27, v11

    move-object/from16 v22, v13

    move/from16 v28, v14

    .line 116
    .end local v0    # "fzCount":I
    .end local v2    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v3    # "launchinguid":I
    .end local v6    # "startTime":J
    .end local v9    # "uid":I
    .end local v11    # "castPid":I
    .end local v13    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v14    # "i":I
    .restart local v16    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v20    # "fzCount":I
    .restart local v21    # "launchinguid":I
    .restart local v22    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .restart local v23    # "startTime":J
    .restart local v27    # "castPid":I
    .restart local v28    # "i":I
    :cond_12
    :goto_b
    move/from16 v0, v20

    .end local v20    # "fzCount":I
    .restart local v0    # "fzCount":I
    :goto_c
    add-int/lit8 v14, v28, 0x1

    move-object/from16 v2, v16

    move/from16 v3, v21

    move-object/from16 v13, v22

    move-wide/from16 v6, v23

    move/from16 v11, v27

    const-wide/16 v8, 0x40

    const/4 v12, 0x0

    .end local v28    # "i":I
    .restart local v14    # "i":I
    goto/16 :goto_3

    .end local v16    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v21    # "launchinguid":I
    .end local v22    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    .end local v23    # "startTime":J
    .end local v27    # "castPid":I
    .restart local v2    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v3    # "launchinguid":I
    .restart local v6    # "startTime":J
    .restart local v11    # "castPid":I
    .restart local v13    # "uidMap":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/miui/server/greeze/RunningProcess;>;>;"
    :cond_13
    move/from16 v20, v0

    move-object/from16 v16, v2

    move-wide/from16 v23, v6

    .line 180
    .end local v0    # "fzCount":I
    .end local v2    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v6    # "startTime":J
    .end local v14    # "i":I
    .restart local v16    # "dynamicWhiteList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v20    # "fzCount":I
    .restart local v23    # "startTime":J
    const-wide/16 v2, 0x40

    invoke-static {v2, v3}, Landroid/os/Trace;->traceEnd(J)V

    .line 181
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v23

    .line 182
    .local v2, "duration":J
    invoke-static {}, Lcom/miui/server/greeze/PerfService;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", froze "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v7, v20

    .end local v20    # "fzCount":I
    .local v7, "fzCount":I
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " processes, took "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return-void
.end method
