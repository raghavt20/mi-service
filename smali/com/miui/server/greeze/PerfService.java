public class com.miui.server.greeze.PerfService extends miui.greeze.IGreezeCallback$Stub {
	 /* .source "PerfService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer BINDER_STATE_IN_BUSY;
public static final Integer BINDER_STATE_IN_IDLE;
public static final Integer BINDER_STATE_IN_TRANSACTION;
public static final Integer BINDER_STATE_PROC_IN_BUSY;
public static final Integer BINDER_STATE_THREAD_IN_BUSY;
private static final java.lang.String SERVICE_NAME;
private static java.lang.String TAG;
private static final java.lang.String WHITELIST_PKG;
private static final android.util.Singleton singleton;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Singleton<", */
/* "Lcom/miui/server/greeze/PerfService;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.android.server.am.ActivityManagerService mAms;
private java.lang.reflect.Method mGetCastPid;
private com.miui.server.greeze.GreezeManagerService mService;
/* # direct methods */
static com.android.server.am.ActivityManagerService -$$Nest$fgetmAms ( com.miui.server.greeze.PerfService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAms;
} // .end method
static java.lang.reflect.Method -$$Nest$fgetmGetCastPid ( com.miui.server.greeze.PerfService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mGetCastPid;
} // .end method
static com.miui.server.greeze.GreezeManagerService -$$Nest$fgetmService ( com.miui.server.greeze.PerfService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mService;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.greeze.PerfService.TAG;
} // .end method
static java.lang.String -$$Nest$sfgetWHITELIST_PKG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.greeze.PerfService.WHITELIST_PKG;
} // .end method
static com.miui.server.greeze.PerfService ( ) {
/* .locals 7 */
/* .line 30 */
final String v0 = "GreezeManager:PerfService"; // const-string v0, "GreezeManager:PerfService"
/* .line 40 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
final String v2 = "com.miui.systemAdSolution"; // const-string v2, "com.miui.systemAdSolution"
final String v3 = "com.xiaomi.xmsf"; // const-string v3, "com.xiaomi.xmsf"
final String v4 = "com.miui.hybrid"; // const-string v4, "com.miui.hybrid"
final String v5 = "com.android.providers.media.module"; // const-string v5, "com.android.providers.media.module"
final String v6 = "com.google.android.providers.media.module"; // const-string v6, "com.google.android.providers.media.module"
/* filled-new-array/range {v1 ..v6}, [Ljava/lang/String; */
/* .line 49 */
/* new-instance v0, Lcom/miui/server/greeze/PerfService$1; */
/* invoke-direct {v0}, Lcom/miui/server/greeze/PerfService$1;-><init>()V */
return;
} // .end method
private com.miui.server.greeze.PerfService ( ) {
/* .locals 1 */
/* .line 57 */
/* invoke-direct {p0}, Lmiui/greeze/IGreezeCallback$Stub;-><init>()V */
/* .line 58 */
com.miui.server.greeze.GreezeManagerService .getService ( );
this.mService = v0;
/* .line 59 */
android.app.ActivityManager .getService ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mAms = v0;
/* .line 60 */
return;
} // .end method
 com.miui.server.greeze.PerfService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/greeze/PerfService;-><init>()V */
return;
} // .end method
public static com.miui.server.greeze.PerfService getInstance ( ) {
/* .locals 1 */
/* .line 63 */
v0 = com.miui.server.greeze.PerfService.singleton;
(( android.util.Singleton ) v0 ).get ( ); // invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/greeze/PerfService; */
} // .end method
/* # virtual methods */
public void reportBinderState ( Integer p0, Integer p1, Integer p2, Integer p3, Long p4 ) {
/* .locals 7 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "tid" # I */
/* .param p4, "binderState" # I */
/* .param p5, "now" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 238 */
final String v0 = " tid="; // const-string v0, " tid="
final String v1 = " pid="; // const-string v1, " pid="
final String v2 = "Receive binder state: uid="; // const-string v2, "Receive binder state: uid="
/* packed-switch p4, :pswitch_data_0 */
/* .line 248 */
/* :pswitch_0 */
v3 = this.mService;
/* filled-new-array {p2}, [I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v3 ).thawPids ( v4, v5, v0 ); // invoke-virtual {v3, v4, v5, v0}, Lcom/miui/server/greeze/GreezeManagerService;->thawPids([IILjava/lang/String;)Ljava/util/List;
/* .line 250 */
/* .line 242 */
/* :pswitch_1 */
v3 = this.mService;
/* filled-new-array {p1}, [I */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v3 ).thawUids ( v4, v5, v0 ); // invoke-virtual {v3, v4, v5, v0}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 244 */
/* .line 240 */
/* :pswitch_2 */
/* nop */
/* .line 254 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void reportBinderTrans ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Boolean p5, Long p6, Long p7 ) {
/* .locals 5 */
/* .param p1, "dstUid" # I */
/* .param p2, "dstPid" # I */
/* .param p3, "callerUid" # I */
/* .param p4, "callerPid" # I */
/* .param p5, "callerTid" # I */
/* .param p6, "isOneway" # Z */
/* .param p7, "now" # J */
/* .param p9, "buffer" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 222 */
v0 = this.mService;
/* filled-new-array {p1}, [I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Receive frozen binder trans: dstUid="; // const-string v4, "Receive frozen binder trans: dstUid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " dstPid="; // const-string v4, " dstPid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " callerUid="; // const-string v4, " callerUid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p3 ); // invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " callerPid="; // const-string v4, " callerPid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p4 ); // invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " callerTid="; // const-string v4, " callerTid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p5 ); // invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " oneway="; // const-string v4, " oneway="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p6 ); // invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUids ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 229 */
return;
} // .end method
public void reportNet ( Integer p0, Long p1 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "now" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 217 */
v0 = this.mService;
/* filled-new-array {p1}, [I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Receive frozen pkg net: uid="; // const-string v4, "Receive frozen pkg net: uid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUids ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 218 */
return;
} // .end method
public void reportSignal ( Integer p0, Integer p1, Long p2 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "now" # J */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 212 */
v0 = this.mService;
/* filled-new-array {p1}, [I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Receive frozen signal: uid="; // const-string v4, "Receive frozen signal: uid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " pid="; // const-string v4, " pid="
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p2 ); // invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v0 ).thawUids ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 213 */
return;
} // .end method
public void serviceReady ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "ready" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 234 */
return;
} // .end method
public void startLaunchBoost ( android.os.Bundle p0 ) {
/* .locals 11 */
/* .param p1, "bundle" # Landroid/os/Bundle; */
/* .line 191 */
v0 = com.miui.server.greeze.GreezeManagerDebugConfig .isEnable ( );
/* if-nez v0, :cond_0 */
return;
/* .line 192 */
} // :cond_0
/* const-string/jumbo v0, "uid" */
v0 = (( android.os.Bundle ) p1 ).getInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 193 */
/* .local v0, "launchinguid":I */
final String v1 = "launchingActivity"; // const-string v1, "launchingActivity"
(( android.os.Bundle ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 194 */
/* .local v1, "launchingActivity":Ljava/lang/String; */
final String v2 = "fromUid"; // const-string v2, "fromUid"
v2 = (( android.os.Bundle ) p1 ).getInt ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 195 */
/* .local v2, "fromUid":I */
final String v3 = "fromPkg"; // const-string v3, "fromPkg"
(( android.os.Bundle ) p1 ).getString ( v3 ); // invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 196 */
/* .local v3, "fromPkg":Ljava/lang/String; */
v4 = this.mService;
/* filled-new-array {v0}, [I */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "Launching "; // const-string v8, "Launching "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v1 ); // invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v9 = " from "; // const-string v9, " from "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2 ); // invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v10 = " "; // const-string v10, " "
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.greeze.GreezeManagerService ) v4 ).thawUids ( v5, v6, v7 ); // invoke-virtual {v4, v5, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->thawUids([IILjava/lang/String;)Ljava/util/List;
/* .line 198 */
final String v4 = "com.miui.home"; // const-string v4, "com.miui.home"
v4 = android.text.TextUtils .equals ( v4,v3 );
/* .line 200 */
/* .local v4, "isFromHome":Z */
/* if-nez v4, :cond_2 */
/* .line 201 */
/* sget-boolean v5, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG_LAUNCH_FROM_HOME:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
v5 = com.miui.server.greeze.PerfService.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2 ); // invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v10 ); // invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v5,v6 );
/* .line 203 */
} // :cond_1
return;
/* .line 206 */
} // :cond_2
v5 = this.mService;
v5 = this.mHandler;
/* new-instance v6, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable; */
/* invoke-direct {v6, p0, p1}, Lcom/miui/server/greeze/PerfService$GzLaunchBoostRunnable;-><init>(Lcom/miui/server/greeze/PerfService;Landroid/os/Bundle;)V */
(( android.os.Handler ) v5 ).post ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 208 */
return;
} // .end method
public void thawedByOther ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "module" # I */
/* .line 258 */
/* sget-boolean v0, Lcom/miui/server/greeze/GreezeManagerDebugConfig;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 259 */
v0 = com.miui.server.greeze.PerfService.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "thawed uid:" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " by:"; // const-string v2, " by:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Log .e ( v0,v1 );
/* .line 260 */
} // :cond_0
return;
} // .end method
