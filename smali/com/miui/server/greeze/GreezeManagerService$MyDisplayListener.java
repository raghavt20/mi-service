class com.miui.server.greeze.GreezeManagerService$MyDisplayListener implements android.hardware.display.DisplayManager$DisplayListener {
	 /* .source "GreezeManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/greeze/GreezeManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MyDisplayListener" */
} // .end annotation
/* # instance fields */
final com.miui.server.greeze.GreezeManagerService this$0; //synthetic
/* # direct methods */
private com.miui.server.greeze.GreezeManagerService$MyDisplayListener ( ) {
/* .locals 0 */
/* .line 3334 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.greeze.GreezeManagerService$MyDisplayListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/greeze/GreezeManagerService$MyDisplayListener;-><init>(Lcom/miui/server/greeze/GreezeManagerService;)V */
return;
} // .end method
/* # virtual methods */
public void onDisplayAdded ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 3337 */
return;
} // .end method
public void onDisplayChanged ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "displayId" # I */
/* .line 3345 */
/* new-instance v0, Landroid/util/ArraySet; */
/* invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V */
/* .line 3346 */
/* .local v0, "uids":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/Integer;>;" */
v1 = this.this$0;
v1 = this.mDisplayManager;
(( android.hardware.display.DisplayManager ) v1 ).getDisplay ( p1 ); // invoke-virtual {v1, p1}, Landroid/hardware/display/DisplayManager;->getDisplay(I)Landroid/view/Display;
/* .line 3347 */
/* .local v1, "display":Landroid/view/Display; */
/* if-nez v1, :cond_0 */
/* .line 3348 */
return;
/* .line 3350 */
} // :cond_0
v2 = (( android.view.Display ) v1 ).getState ( ); // invoke-virtual {v1}, Landroid/view/Display;->getState()I
/* .line 3351 */
/* .local v2, "state":I */
int v3 = 2; // const/4 v3, 0x2
/* if-ne v2, v3, :cond_4 */
if ( p1 != null) { // if-eqz p1, :cond_4
/* .line 3353 */
try { // :try_start_0
	 v3 = this.this$0;
	 v3 = this.mActivityTaskManager;
	 /* .line 3354 */
	 /* .local v3, "rootTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;" */
v5 = } // :goto_0
if ( v5 != null) { // if-eqz v5, :cond_2
	 /* check-cast v5, Landroid/app/ActivityTaskManager$RootTaskInfo; */
	 /* .line 3355 */
	 /* .local v5, "rootTaskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo; */
	 if ( v5 != null) { // if-eqz v5, :cond_1
		 /* iget-boolean v6, v5, Landroid/app/ActivityTaskManager$RootTaskInfo;->visible:Z */
		 if ( v6 != null) { // if-eqz v6, :cond_1
			 v6 = this.childTaskNames;
			 /* array-length v6, v6 */
			 /* if-lez v6, :cond_1 */
			 /* .line 3356 */
			 v6 = this.childTaskNames;
			 int v7 = 0; // const/4 v7, 0x0
			 /* aget-object v6, v6, v7 */
			 android.content.ComponentName .unflattenFromString ( v6 );
			 /* .line 3357 */
			 /* .local v6, "componentName":Landroid/content/ComponentName; */
			 if ( v6 != null) { // if-eqz v6, :cond_1
				 (( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
				 if ( v7 != null) { // if-eqz v7, :cond_1
					 /* .line 3358 */
					 v7 = this.this$0;
					 (( android.content.ComponentName ) v6 ).getPackageName ( ); // invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
					 v7 = 					 (( com.miui.server.greeze.GreezeManagerService ) v7 ).getUidByPackageName ( v8 ); // invoke-virtual {v7, v8}, Lcom/miui/server/greeze/GreezeManagerService;->getUidByPackageName(Ljava/lang/String;)I
					 /* .line 3359 */
					 /* .local v7, "uid":I */
					 java.lang.Integer .valueOf ( v7 );
					 (( android.util.ArraySet ) v0 ).add ( v8 ); // invoke-virtual {v0, v8}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z
					 /* :try_end_0 */
					 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
					 /* .line 3362 */
				 } // .end local v5 # "rootTaskInfo":Landroid/app/ActivityTaskManager$RootTaskInfo;
			 } // .end local v6 # "componentName":Landroid/content/ComponentName;
		 } // .end local v7 # "uid":I
	 } // :cond_1
	 /* .line 3365 */
} // .end local v3 # "rootTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityTaskManager$RootTaskInfo;>;"
} // :cond_2
/* .line 3363 */
/* :catch_0 */
/* move-exception v3 */
/* .line 3364 */
/* .local v3, "e":Ljava/lang/Exception; */
final String v4 = "GreezeManager"; // const-string v4, "GreezeManager"
(( java.lang.Exception ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v4,v5 );
/* .line 3366 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
(( android.util.ArraySet ) v0 ).iterator ( ); // invoke-virtual {v0}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;
/* .line 3368 */
/* .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;" */
} // :cond_3
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 3369 */
/* check-cast v4, Ljava/lang/Integer; */
v4 = (( java.lang.Integer ) v4 ).intValue ( ); // invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
/* .line 3370 */
/* .local v4, "needUnFreezeUid":I */
v5 = this.this$0;
v5 = (( com.miui.server.greeze.GreezeManagerService ) v5 ).isUidFrozen ( v4 ); // invoke-virtual {v5, v4}, Lcom/miui/server/greeze/GreezeManagerService;->isUidFrozen(I)Z
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 3371 */
v5 = this.this$0;
/* const/16 v6, 0x3e8 */
final String v7 = "Display_On"; // const-string v7, "Display_On"
(( com.miui.server.greeze.GreezeManagerService ) v5 ).thawUid ( v4, v6, v7 ); // invoke-virtual {v5, v4, v6, v7}, Lcom/miui/server/greeze/GreezeManagerService;->thawUid(IILjava/lang/String;)Z
/* .line 3375 */
} // .end local v3 # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
} // .end local v4 # "needUnFreezeUid":I
} // :cond_4
return;
} // .end method
public void onDisplayRemoved ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "displayId" # I */
/* .line 3341 */
return;
} // .end method
