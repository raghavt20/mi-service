class com.miui.server.BackupManagerService$4 implements android.content.pm.IPackageStatsObserver {
	 /* .source "BackupManagerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/BackupManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.BackupManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.BackupManagerService$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/BackupManagerService; */
/* .line 908 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public android.os.IBinder asBinder ( ) {
/* .locals 1 */
/* .line 926 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onGetStatsCompleted ( android.content.pm.PackageStats p0, Boolean p1 ) {
/* .locals 7 */
/* .param p1, "pStats" # Landroid/content/pm/PackageStats; */
/* .param p2, "succeeded" # Z */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 911 */
v0 = this.packageName;
/* .line 913 */
/* .local v0, "pkg":Ljava/lang/String; */
try { // :try_start_0
v1 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmPackageManager ( v1 );
v2 = this.this$0;
v2 = com.miui.server.BackupManagerService .-$$Nest$fgetmAppUserId ( v2 );
int v3 = 0; // const/4 v3, 0x0
(( android.content.pm.PackageManager ) v1 ).getPackageInfoAsUser ( v0, v3, v2 ); // invoke-virtual {v1, v0, v3, v2}, Landroid/content/pm/PackageManager;->getPackageInfoAsUser(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
/* .line 914 */
/* .local v1, "pi":Landroid/content/pm/PackageInfo; */
v2 = this.this$0;
com.miui.server.BackupManagerService .-$$Nest$fgetmContext ( v2 );
v2 = miui.app.backup.BackupManager .isSysAppForBackup ( v2,v0 );
if ( v2 != null) { // if-eqz v2, :cond_0
	 /* .line 915 */
	 v2 = this.this$0;
	 /* iget-wide v3, p1, Landroid/content/pm/PackageStats;->dataSize:J */
	 com.miui.server.BackupManagerService .-$$Nest$fputmCurrentTotalSize ( v2,v3,v4 );
	 /* .line 917 */
} // :cond_0
v2 = this.this$0;
/* new-instance v3, Ljava/io/File; */
v4 = this.applicationInfo;
v4 = this.sourceDir;
/* invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
(( java.io.File ) v3 ).length ( ); // invoke-virtual {v3}, Ljava/io/File;->length()J
/* move-result-wide v3 */
/* iget-wide v5, p1, Landroid/content/pm/PackageStats;->dataSize:J */
/* add-long/2addr v3, v5 */
com.miui.server.BackupManagerService .-$$Nest$fputmCurrentTotalSize ( v2,v3,v4 );
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 921 */
} // .end local v1 # "pi":Landroid/content/pm/PackageInfo;
} // :goto_0
/* .line 919 */
/* :catch_0 */
/* move-exception v1 */
/* .line 920 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
(( android.content.pm.PackageManager$NameNotFoundException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
/* .line 922 */
} // .end local v1 # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
} // :goto_1
return;
} // .end method
