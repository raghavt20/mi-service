.class public Lcom/miui/server/MiuiFreeDragService;
.super Lmiui/freedrag/IMiuiFreeDragService$Stub;
.source "MiuiFreeDragService.java"

# interfaces
.implements Lcom/miui/app/MiuiFreeDragServiceInternal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiFreeDragService$Lifecycle;
    }
.end annotation


# static fields
.field public static final SERVICE_NAME:Ljava/lang/String; = "MiuiFreeDragService"

.field private static instance:Lcom/miui/server/MiuiFreeDragService;


# instance fields
.field private final mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Landroid/util/ArraySet<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 41
    invoke-direct {p0}, Lmiui/freedrag/IMiuiFreeDragService$Stub;-><init>()V

    .line 52
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    .line 42
    const-class v0, Lcom/miui/app/MiuiFreeDragServiceInternal;

    invoke-static {v0, p0}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/miui/server/MiuiFreeDragService;
    .locals 2

    const-class v0, Lcom/miui/server/MiuiFreeDragService;

    monitor-enter v0

    .line 46
    :try_start_0
    sget-object v1, Lcom/miui/server/MiuiFreeDragService;->instance:Lcom/miui/server/MiuiFreeDragService;

    if-nez v1, :cond_0

    .line 47
    new-instance v1, Lcom/miui/server/MiuiFreeDragService;

    invoke-direct {v1}, Lcom/miui/server/MiuiFreeDragService;-><init>()V

    sput-object v1, Lcom/miui/server/MiuiFreeDragService;->instance:Lcom/miui/server/MiuiFreeDragService;

    .line 49
    :cond_0
    sget-object v1, Lcom/miui/server/MiuiFreeDragService;->instance:Lcom/miui/server/MiuiFreeDragService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 45
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public getFilterRules()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lmiui/freedrag/FilterRule;",
            ">;"
        }
    .end annotation

    .line 68
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto/16 :goto_2

    .line 74
    :cond_0
    invoke-static {}, Lcom/android/server/MiuiCommonCloudServiceStub;->getInstance()Lcom/android/server/MiuiCommonCloudServiceStub;

    move-result-object v2

    const-string v3, "miui_free_drag"

    invoke-virtual {v2, v3}, Lcom/android/server/MiuiCommonCloudServiceStub;->getDataByModuleName(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiui/freedrag/FilterRuleData;

    .line 75
    .local v2, "filterRuleData":Lmiui/freedrag/FilterRuleData;
    if-nez v2, :cond_1

    .line 76
    const-string v3, "MiuiFreeDragService"

    const-string v4, "getFilterRules filterRuleData = null, can\'t drag!"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-object v1

    .line 80
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v3, v1

    .line 81
    .local v3, "rules":Ljava/util/List;, "Ljava/util/List<Lmiui/freedrag/FilterRule;>;"
    invoke-virtual {v2}, Lmiui/freedrag/FilterRuleData;->getRules()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiui/freedrag/FilterRule;

    .line 82
    .local v4, "rule":Lmiui/freedrag/FilterRule;
    invoke-virtual {v4}, Lmiui/freedrag/FilterRule;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v4}, Lmiui/freedrag/FilterRule;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 83
    :cond_2
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    .end local v4    # "rule":Lmiui/freedrag/FilterRule;
    :cond_3
    goto :goto_0

    .line 87
    :cond_4
    iget-object v4, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    monitor-enter v4

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 89
    iget-object v1, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    invoke-virtual {v1, v0}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/ArraySet;

    .line 90
    .local v1, "activityClassSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    invoke-virtual {v1}, Landroid/util/ArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 91
    .local v6, "activityClass":Ljava/lang/String;
    new-instance v7, Lmiui/freedrag/FilterRule;

    const-string v8, ""

    invoke-direct {v7, v0, v6, v8}, Lmiui/freedrag/FilterRule;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .local v7, "filterRule":Lmiui/freedrag/FilterRule;
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    nop

    .end local v6    # "activityClass":Ljava/lang/String;
    .end local v7    # "filterRule":Lmiui/freedrag/FilterRule;
    goto :goto_1

    .line 95
    .end local v1    # "activityClassSet":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    :cond_5
    monitor-exit v4

    .line 97
    return-object v3

    .line 95
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 70
    .end local v2    # "filterRuleData":Lmiui/freedrag/FilterRuleData;
    .end local v3    # "rules":Ljava/util/List;, "Ljava/util/List<Lmiui/freedrag/FilterRule;>;"
    :cond_6
    :goto_2
    const-string v2, "MiuiFreeDragService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFilterRules packageName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", can\'t drag!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return-object v1
.end method

.method public notifyHasMiuiDragAndDropMetaData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    monitor-enter v0

    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    invoke-virtual {v1, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/ArraySet;

    .line 58
    .local v1, "set":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    if-nez v1, :cond_0

    .line 59
    new-instance v2, Landroid/util/ArraySet;

    invoke-direct {v2}, Landroid/util/ArraySet;-><init>()V

    move-object v1, v2

    .line 61
    :cond_0
    invoke-virtual {v1, p2}, Landroid/util/ArraySet;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v2, p0, Lcom/miui/server/MiuiFreeDragService;->mPackagesWithMiuiDragAndDropMetaData:Landroid/util/ArrayMap;

    invoke-virtual {v2, p1, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    nop

    .end local v1    # "set":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    monitor-exit v0

    .line 64
    return-void

    .line 63
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
