public class com.miui.server.MiuiUsbService extends miui.usb.IMiuiUsbManager$Stub {
	 /* .source "MiuiUsbService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/MiuiUsbService$UsbDebuggingManager; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private final android.content.BroadcastReceiver mBootCompletedReceiver;
private android.content.Context mContext;
private com.miui.server.MiuiUsbService$UsbDebuggingManager mUsbDebuggingManager;
/* # direct methods */
static com.miui.server.MiuiUsbService$UsbDebuggingManager -$$Nest$fgetmUsbDebuggingManager ( com.miui.server.MiuiUsbService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mUsbDebuggingManager;
} // .end method
static Boolean -$$Nest$smcontainsFunction ( java.lang.String p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 com.miui.server.MiuiUsbService .containsFunction ( p0,p1 );
} // .end method
public com.miui.server.MiuiUsbService ( ) {
	 /* .locals 3 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 57 */
	 /* invoke-direct {p0}, Lmiui/usb/IMiuiUsbManager$Stub;-><init>()V */
	 /* .line 46 */
	 /* new-instance v0, Lcom/miui/server/MiuiUsbService$1; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/MiuiUsbService$1;-><init>(Lcom/miui/server/MiuiUsbService;)V */
	 this.mBootCompletedReceiver = v0;
	 /* .line 58 */
	 this.mContext = p1;
	 /* .line 59 */
	 final String v1 = "ro.adb.secure"; // const-string v1, "ro.adb.secure"
	 android.os.SystemProperties .get ( v1 );
	 final String v2 = "1"; // const-string v2, "1"
	 v1 = 	 (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 60 */
		 /* new-instance v1, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager; */
		 /* invoke-direct {v1, p0, p1}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;-><init>(Lcom/miui/server/MiuiUsbService;Landroid/content/Context;)V */
		 this.mUsbDebuggingManager = v1;
		 /* .line 63 */
	 } // :cond_0
	 /* new-instance v1, Landroid/content/IntentFilter; */
	 final String v2 = "android.intent.action.BOOT_COMPLETED"; // const-string v2, "android.intent.action.BOOT_COMPLETED"
	 /* invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
	 (( android.content.Context ) p1 ).registerReceiver ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
	 /* .line 65 */
	 return;
} // .end method
private static Boolean containsFunction ( java.lang.String p0, java.lang.String p1 ) {
	 /* .locals 5 */
	 /* .param p0, "functions" # Ljava/lang/String; */
	 /* .param p1, "function" # Ljava/lang/String; */
	 /* .line 68 */
	 v0 = 	 (( java.lang.String ) p0 ).indexOf ( p1 ); // invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
	 /* .line 69 */
	 /* .local v0, "index":I */
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-gez v0, :cond_0 */
	 /* .line 70 */
	 /* .line 71 */
} // :cond_0
/* const/16 v2, 0x2c */
/* if-lez v0, :cond_1 */
/* add-int/lit8 v3, v0, -0x1 */
v3 = (( java.lang.String ) p0 ).charAt ( v3 ); // invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C
/* if-eq v3, v2, :cond_1 */
/* .line 72 */
/* .line 73 */
} // :cond_1
v3 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* add-int/2addr v3, v0 */
/* .line 74 */
/* .local v3, "charAfter":I */
v4 = (( java.lang.String ) p0 ).length ( ); // invoke-virtual {p0}, Ljava/lang/String;->length()I
/* if-ge v3, v4, :cond_2 */
v4 = (( java.lang.String ) p0 ).charAt ( v3 ); // invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C
/* if-eq v4, v2, :cond_2 */
/* .line 75 */
/* .line 76 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
} // .end method
/* # virtual methods */
public void acceptMdbRestore ( ) {
/* .locals 3 */
/* .line 81 */
v0 = this.mContext;
final String v1 = "android.permission.MANAGE_USB"; // const-string v1, "android.permission.MANAGE_USB"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 82 */
return;
} // .end method
public void allowUsbDebugging ( Boolean p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "alwaysAllow" # Z */
/* .param p2, "publicKey" # Ljava/lang/String; */
/* .line 91 */
v0 = this.mContext;
final String v1 = "android.permission.MANAGE_USB"; // const-string v1, "android.permission.MANAGE_USB"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 92 */
v0 = this.mUsbDebuggingManager;
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v0 ).allowUsbDebugging ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->allowUsbDebugging(ZLjava/lang/String;)V
/* .line 93 */
return;
} // .end method
public void cancelMdbRestore ( ) {
/* .locals 3 */
/* .line 86 */
v0 = this.mContext;
final String v1 = "android.permission.MANAGE_USB"; // const-string v1, "android.permission.MANAGE_USB"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 87 */
return;
} // .end method
public void denyUsbDebugging ( ) {
/* .locals 3 */
/* .line 97 */
v0 = this.mContext;
final String v1 = "android.permission.MANAGE_USB"; // const-string v1, "android.permission.MANAGE_USB"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Context ) v0 ).enforceCallingOrSelfPermission ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
/* .line 98 */
v0 = this.mUsbDebuggingManager;
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v0 ).denyUsbDebugging ( ); // invoke-virtual {v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->denyUsbDebugging()V
/* .line 99 */
return;
} // .end method
public void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1 ) {
/* .locals 1 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 102 */
v0 = this.mUsbDebuggingManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 103 */
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v0 ).dump ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
/* .line 105 */
} // :cond_0
return;
} // .end method
