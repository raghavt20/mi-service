public class com.miui.server.AppRunningControlService extends miui.security.IAppRunningControlManager$Stub {
	 /* .source "AppRunningControlService.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 private static final java.util.ArrayList sNotDisallow;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private final java.util.List mAppsDisallowRunning;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.Context mContext;
private android.content.Intent mDisallowRunningAppIntent;
private Boolean mIsBlackListEnable;
/* # direct methods */
static com.miui.server.AppRunningControlService ( ) {
/* .locals 2 */
/* .line 20 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 24 */
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 25 */
final String v1 = "com.miui.securitycenter"; // const-string v1, "com.miui.securitycenter"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 26 */
final String v1 = "com.android.updater"; // const-string v1, "com.android.updater"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 27 */
final String v1 = "com.xiaomi.market"; // const-string v1, "com.xiaomi.market"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 28 */
final String v1 = "com.xiaomi.finddevice"; // const-string v1, "com.xiaomi.finddevice"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 29 */
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 30 */
return;
} // .end method
public com.miui.server.AppRunningControlService ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 32 */
/* invoke-direct {p0}, Lmiui/security/IAppRunningControlManager$Stub;-><init>()V */
/* .line 16 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAppsDisallowRunning = v0;
/* .line 33 */
this.mContext = p1;
/* .line 34 */
return;
} // .end method
private void checkPermission ( ) {
/* .locals 3 */
/* .line 117 */
v0 = this.mContext;
final String v1 = "android.permission.FORCE_STOP_PACKAGES"; // const-string v1, "android.permission.FORCE_STOP_PACKAGES"
v0 = (( android.content.Context ) v0 ).checkCallingOrSelfPermission ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I
/* if-nez v0, :cond_0 */
/* .line 126 */
return;
/* .line 119 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Permission Denial from pid="; // const-string v2, "Permission Denial from pid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 120 */
v2 = android.os.Binder .getCallingPid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", uid="; // const-string v2, ", uid="
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 121 */
v2 = android.os.Binder .getCallingUid ( );
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " requires "; // const-string v2, " requires "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 123 */
/* .local v0, "msg":Ljava/lang/String; */
final String v1 = "AppRunningControlService"; // const-string v1, "AppRunningControlService"
android.util.Slog .w ( v1,v0 );
/* .line 124 */
/* new-instance v1, Ljava/lang/SecurityException; */
/* invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V */
/* throw v1 */
} // .end method
private Boolean matchRuleInner ( java.lang.String p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "wakeType" # I */
/* .line 98 */
/* iget-boolean v0, p0, Lcom/miui/server/AppRunningControlService;->mIsBlackListEnable:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 99 */
/* .line 102 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* if-ne p2, v0, :cond_1 */
/* .line 103 */
/* .line 105 */
} // :cond_1
v0 = v0 = this.mAppsDisallowRunning;
} // .end method
/* # virtual methods */
public android.content.Intent getBlockActivityIntent ( java.lang.String p0, android.content.Intent p1, Boolean p2, Integer p3 ) {
/* .locals 3 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .param p3, "fromActivity" # Z */
/* .param p4, "requestCode" # I */
/* .line 65 */
/* iget-boolean v0, p0, Lcom/miui/server/AppRunningControlService;->mIsBlackListEnable:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 66 */
/* .line 68 */
} // :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 69 */
final String v0 = "AppRunningControlService"; // const-string v0, "AppRunningControlService"
final String v2 = "getBlockActivityIntent packageName can\'t be null"; // const-string v2, "getBlockActivityIntent packageName can\'t be null"
android.util.Slog .w ( v0,v2 );
/* .line 70 */
/* .line 72 */
} // :cond_1
v0 = v0 = this.mAppsDisallowRunning;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 73 */
v0 = this.mDisallowRunningAppIntent;
(( android.content.Intent ) v0 ).clone ( ); // invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;
/* check-cast v0, Landroid/content/Intent; */
/* .line 74 */
/* .local v0, "result":Landroid/content/Intent; */
final String v1 = "packageName"; // const-string v1, "packageName"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 75 */
if ( p2 != null) { // if-eqz p2, :cond_4
/* .line 78 */
v1 = (( android.content.Intent ) p2 ).getFlags ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getFlags()I
/* const/high16 v2, 0x2000000 */
/* and-int/2addr v1, v2 */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 79 */
(( android.content.Intent ) v0 ).addFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 81 */
} // :cond_2
/* const/high16 v1, 0x1000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 82 */
if ( p3 != null) { // if-eqz p3, :cond_3
/* .line 84 */
/* if-ltz p4, :cond_4 */
/* .line 85 */
(( android.content.Intent ) v0 ).addFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 89 */
} // :cond_3
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 92 */
} // :cond_4
} // :goto_0
/* .line 94 */
} // .end local v0 # "result":Landroid/content/Intent;
} // :cond_5
} // .end method
public java.util.List getNotDisallowList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 142 */
v0 = com.miui.server.AppRunningControlService.sNotDisallow;
} // .end method
public Boolean isBlockActivity ( android.content.Intent p0 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 109 */
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = this.mDisallowRunningAppIntent;
/* if-nez v0, :cond_0 */
/* .line 112 */
} // :cond_0
(( android.content.Intent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 113 */
/* .local v0, "action":Ljava/lang/String; */
v1 = this.mDisallowRunningAppIntent;
(( android.content.Intent ) v1 ).getAction ( ); // invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = android.text.TextUtils .equals ( v0,v1 );
/* .line 110 */
} // .end local v0 # "action":Ljava/lang/String;
} // :cond_1
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean matchRule ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "pkgName" # Ljava/lang/String; */
/* .param p2, "wakeType" # I */
/* .line 137 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/AppRunningControlService;->matchRuleInner(Ljava/lang/String;I)Z */
} // .end method
public void setBlackListEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isEnable" # Z */
/* .line 59 */
/* invoke-direct {p0}, Lcom/miui/server/AppRunningControlService;->checkPermission()V */
/* .line 60 */
/* iput-boolean p1, p0, Lcom/miui/server/AppRunningControlService;->mIsBlackListEnable:Z */
/* .line 61 */
return;
} // .end method
public void setDisallowRunningList ( java.util.List p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;", */
/* "Landroid/content/Intent;", */
/* ")V" */
/* } */
} // .end annotation
/* .line 38 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
/* invoke-direct {p0}, Lcom/miui/server/AppRunningControlService;->checkPermission()V */
/* .line 39 */
final String v0 = "AppRunningControlService"; // const-string v0, "AppRunningControlService"
/* if-nez p2, :cond_0 */
/* .line 40 */
/* const-string/jumbo v1, "setDisallowRunningList intent can\'t be null" */
android.util.Slog .w ( v0,v1 );
/* .line 41 */
return;
/* .line 43 */
} // :cond_0
this.mDisallowRunningAppIntent = p2;
/* .line 44 */
v1 = this.mAppsDisallowRunning;
/* .line 45 */
v1 = if ( p1 != null) { // if-eqz p1, :cond_4
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 49 */
} // :cond_1
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_3
/* check-cast v1, Ljava/lang/String; */
/* .line 50 */
/* .local v1, "pkgName":Ljava/lang/String; */
v2 = com.miui.server.AppRunningControlService.sNotDisallow;
v2 = (( java.util.ArrayList ) v2 ).contains ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 51 */
/* .line 53 */
} // :cond_2
v2 = this.mAppsDisallowRunning;
/* .line 54 */
} // .end local v1 # "pkgName":Ljava/lang/String;
/* .line 55 */
} // :cond_3
return;
/* .line 46 */
} // :cond_4
} // :goto_1
/* const-string/jumbo v1, "setDisallowRunningList clear list." */
android.util.Slog .d ( v0,v1 );
/* .line 47 */
return;
} // .end method
