class com.miui.server.input.MiuiBackTapGestureService$H extends android.os.Handler {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
public static final Integer MSG_BACKTAP_FUNCTION;
/* # instance fields */
final com.miui.server.input.MiuiBackTapGestureService this$0; //synthetic
/* # direct methods */
 com.miui.server.input.MiuiBackTapGestureService$H ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiBackTapGestureService; */
/* .line 234 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/Handler;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 5 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 238 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 239 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
/* const-string/jumbo v1, "shortcut" */
(( android.os.Bundle ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 240 */
/* .local v0, "shortcut":Ljava/lang/String; */
v1 = this.obj;
/* check-cast v1, Ljava/lang/String; */
/* .line 241 */
/* .local v1, "function":Ljava/lang/String; */
/* if-nez v1, :cond_0 */
/* .line 242 */
return;
/* .line 244 */
} // :cond_0
v2 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v2 );
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v2 );
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v2 ).triggerFunction ( v1, v0, v3, v4 ); // invoke-virtual {v2, v1, v0, v3, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 247 */
} // .end local v0 # "shortcut":Ljava/lang/String;
} // .end local v1 # "function":Ljava/lang/String;
} // :cond_1
return;
} // .end method
