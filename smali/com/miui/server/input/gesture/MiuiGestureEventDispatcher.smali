.class public Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;
.super Landroid/view/InputEventReceiver;
.source "MiuiGestureEventDispatcher.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiGestureEventDispatcher"


# instance fields
.field private final mInputChannel:Landroid/view/InputChannel;

.field private final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/input/gesture/MiuiGestureListener;",
            ">;"
        }
    .end annotation
.end field

.field private mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;


# direct methods
.method public constructor <init>(Landroid/view/InputChannel;Landroid/os/Looper;)V
    .locals 1
    .param p1, "inputChannel"    # Landroid/view/InputChannel;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    .line 17
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/miui/server/input/gesture/MiuiGestureListener;

    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 21
    iput-object p1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mInputChannel:Landroid/view/InputChannel;

    .line 22
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 92
    invoke-super {p0}, Landroid/view/InputEventReceiver;->dispose()V

    .line 93
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mInputChannel:Landroid/view/InputChannel;

    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    .line 94
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    monitor-enter v0

    .line 95
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 96
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 97
    monitor-exit v0

    .line 98
    return-void

    .line 97
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getGestureListenerCount()I
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    monitor-enter v0

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    monitor-exit v0

    return v1

    .line 84
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onInputEvent(Landroid/view/InputEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/InputEvent;

    .line 26
    instance-of v0, p1, Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    .line 27
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 28
    move-object v0, p1

    check-cast v0, Landroid/view/MotionEvent;

    .line 30
    .local v0, "motionEvent":Landroid/view/MotionEvent;
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    monitor-enter v1

    .line 31
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    if-nez v2, :cond_0

    .line 32
    iget-object v2, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/miui/server/input/gesture/MiuiGestureListener;

    iput-object v2, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 33
    iget-object v3, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 35
    :cond_0
    iget-object v2, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 36
    .local v2, "listeners":[Lcom/miui/server/input/gesture/MiuiGestureListener;
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 38
    aget-object v3, v2, v1

    invoke-interface {v3, v0}, Lcom/miui/server/input/gesture/MiuiGestureListener;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 36
    .end local v1    # "i":I
    .end local v2    # "listeners":[Lcom/miui/server/input/gesture/MiuiGestureListener;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 41
    .end local v0    # "motionEvent":Landroid/view/MotionEvent;
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->finishInputEvent(Landroid/view/InputEvent;Z)V

    .line 42
    return-void
.end method

.method public registerInputEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 50
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    monitor-enter v0

    .line 51
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    const-string v1, "MiuiGestureEventDispatcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerInputEventListener trying to register"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " twice. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    monitor-exit v0

    return-void

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 58
    monitor-exit v0

    .line 59
    return-void

    .line 58
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterInputEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 67
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    monitor-enter v0

    .line 68
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    const-string v1, "MiuiGestureEventDispatcher"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unregisterInputEventListener"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "not registered."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    monitor-exit v0

    return-void

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 73
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->mListenersArray:[Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 74
    monitor-exit v0

    .line 75
    return-void

    .line 74
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
