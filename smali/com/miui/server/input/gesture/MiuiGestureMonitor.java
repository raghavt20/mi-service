public class com.miui.server.input.gesture.MiuiGestureMonitor {
	 /* .source "MiuiGestureMonitor.java" */
	 /* # static fields */
	 private static com.miui.server.input.gesture.MiuiGestureMonitor sMiuiGestureMonitor;
	 /* # instance fields */
	 private final Integer mDisplayId;
	 private android.view.InputMonitor mGestureInputMonitor;
	 private com.miui.server.input.gesture.MiuiGestureEventDispatcher mGesturePointerEventDispatcher;
	 private final android.os.HandlerThread mHandlerThread;
	 /* # direct methods */
	 private com.miui.server.input.gesture.MiuiGestureMonitor ( ) {
		 /* .locals 3 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 18 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 15 */
		 /* new-instance v0, Landroid/os/HandlerThread; */
		 final String v1 = "miui-gesture"; // const-string v1, "miui-gesture"
		 int v2 = -4; // const/4 v2, -0x4
		 /* invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V */
		 this.mHandlerThread = v0;
		 /* .line 19 */
		 v1 = 		 (( android.content.Context ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/content/Context;->getDisplayId()I
		 /* iput v1, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mDisplayId:I */
		 /* .line 20 */
		 (( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
		 /* .line 21 */
		 return;
	 } // .end method
	 public static synchronized com.miui.server.input.gesture.MiuiGestureMonitor getInstance ( android.content.Context p0 ) {
		 /* .locals 2 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* const-class v0, Lcom/miui/server/input/gesture/MiuiGestureMonitor; */
		 /* monitor-enter v0 */
		 /* .line 24 */
		 try { // :try_start_0
			 v1 = com.miui.server.input.gesture.MiuiGestureMonitor.sMiuiGestureMonitor;
			 /* if-nez v1, :cond_0 */
			 /* .line 25 */
			 /* new-instance v1, Lcom/miui/server/input/gesture/MiuiGestureMonitor; */
			 /* invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;-><init>(Landroid/content/Context;)V */
			 /* .line 27 */
		 } // :cond_0
		 v1 = com.miui.server.input.gesture.MiuiGestureMonitor.sMiuiGestureMonitor;
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* monitor-exit v0 */
		 /* .line 23 */
	 } // .end local p0 # "context":Landroid/content/Context;
	 /* :catchall_0 */
	 /* move-exception p0 */
	 /* monitor-exit v0 */
	 /* throw p0 */
} // .end method
private void registerGestureMonitor ( ) {
	 /* .locals 3 */
	 /* .line 31 */
	 android.hardware.input.InputManager .getInstance ( );
	 final String v1 = "miui-gesture"; // const-string v1, "miui-gesture"
	 /* iget v2, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mDisplayId:I */
	 (( android.hardware.input.InputManager ) v0 ).monitorGestureInput ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/hardware/input/InputManager;->monitorGestureInput(Ljava/lang/String;I)Landroid/view/InputMonitor;
	 this.mGestureInputMonitor = v0;
	 /* .line 32 */
	 /* new-instance v0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher; */
	 v1 = this.mGestureInputMonitor;
	 /* .line 33 */
	 (( android.view.InputMonitor ) v1 ).getInputChannel ( ); // invoke-virtual {v1}, Landroid/view/InputMonitor;->getInputChannel()Landroid/view/InputChannel;
	 v2 = this.mHandlerThread;
	 (( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V */
	 this.mGesturePointerEventDispatcher = v0;
	 /* .line 34 */
	 return;
} // .end method
private void unregisterGestureMonitor ( ) {
	 /* .locals 1 */
	 /* .line 37 */
	 v0 = this.mGestureInputMonitor;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 38 */
		 (( android.view.InputMonitor ) v0 ).dispose ( ); // invoke-virtual {v0}, Landroid/view/InputMonitor;->dispose()V
		 /* .line 40 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 this.mGestureInputMonitor = v0;
	 /* .line 41 */
	 this.mGesturePointerEventDispatcher = v0;
	 /* .line 42 */
	 return;
} // .end method
/* # virtual methods */
public void pilferPointers ( ) {
	 /* .locals 1 */
	 /* .line 63 */
	 v0 = this.mGestureInputMonitor;
	 (( android.view.InputMonitor ) v0 ).pilferPointers ( ); // invoke-virtual {v0}, Landroid/view/InputMonitor;->pilferPointers()V
	 /* .line 64 */
	 return;
} // .end method
public void registerPointerEventListener ( com.miui.server.input.gesture.MiuiGestureListener p0 ) {
	 /* .locals 1 */
	 /* .param p1, "listener" # Lcom/miui/server/input/gesture/MiuiGestureListener; */
	 /* .line 45 */
	 v0 = this.mGesturePointerEventDispatcher;
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 46 */
		 v0 = 		 (( com.miui.server.input.gesture.MiuiGestureEventDispatcher ) v0 ).getGestureListenerCount ( ); // invoke-virtual {v0}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->getGestureListenerCount()I
		 /* if-nez v0, :cond_1 */
		 /* .line 47 */
	 } // :cond_0
	 /* invoke-direct {p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerGestureMonitor()V */
	 /* .line 49 */
} // :cond_1
v0 = this.mGesturePointerEventDispatcher;
(( com.miui.server.input.gesture.MiuiGestureEventDispatcher ) v0 ).registerInputEventListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->registerInputEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 50 */
return;
} // .end method
public void unregisterPointerEventListener ( com.miui.server.input.gesture.MiuiGestureListener p0 ) {
/* .locals 1 */
/* .param p1, "listener" # Lcom/miui/server/input/gesture/MiuiGestureListener; */
/* .line 53 */
v0 = this.mGesturePointerEventDispatcher;
/* if-nez v0, :cond_0 */
/* .line 54 */
return;
/* .line 56 */
} // :cond_0
(( com.miui.server.input.gesture.MiuiGestureEventDispatcher ) v0 ).unregisterInputEventListener ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->unregisterInputEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 57 */
v0 = this.mGesturePointerEventDispatcher;
v0 = (( com.miui.server.input.gesture.MiuiGestureEventDispatcher ) v0 ).getGestureListenerCount ( ); // invoke-virtual {v0}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->getGestureListenerCount()I
/* if-nez v0, :cond_1 */
/* .line 58 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterGestureMonitor()V */
/* .line 60 */
} // :cond_1
return;
} // .end method
