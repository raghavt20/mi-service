public class com.miui.server.input.gesture.MiuiGestureEventDispatcher extends android.view.InputEventReceiver {
	 /* .source "MiuiGestureEventDispatcher.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.view.InputChannel mInputChannel;
	 private final java.util.ArrayList mListeners;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/input/gesture/MiuiGestureListener;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private com.miui.server.input.gesture.MiuiGestureListener mListenersArray;
/* # direct methods */
public com.miui.server.input.gesture.MiuiGestureEventDispatcher ( ) {
/* .locals 1 */
/* .param p1, "inputChannel" # Landroid/view/InputChannel; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 20 */
/* invoke-direct {p0, p1, p2}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V */
/* .line 16 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mListeners = v0;
/* .line 17 */
int v0 = 0; // const/4 v0, 0x0
/* new-array v0, v0, [Lcom/miui/server/input/gesture/MiuiGestureListener; */
this.mListenersArray = v0;
/* .line 21 */
this.mInputChannel = p1;
/* .line 22 */
return;
} // .end method
/* # virtual methods */
public void dispose ( ) {
/* .locals 2 */
/* .line 92 */
/* invoke-super {p0}, Landroid/view/InputEventReceiver;->dispose()V */
/* .line 93 */
v0 = this.mInputChannel;
(( android.view.InputChannel ) v0 ).dispose ( ); // invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V
/* .line 94 */
v0 = this.mListeners;
/* monitor-enter v0 */
/* .line 95 */
try { // :try_start_0
	 v1 = this.mListeners;
	 (( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
	 /* .line 96 */
	 int v1 = 0; // const/4 v1, 0x0
	 this.mListenersArray = v1;
	 /* .line 97 */
	 /* monitor-exit v0 */
	 /* .line 98 */
	 return;
	 /* .line 97 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
public Integer getGestureListenerCount ( ) {
	 /* .locals 2 */
	 /* .line 82 */
	 v0 = this.mListeners;
	 /* monitor-enter v0 */
	 /* .line 83 */
	 try { // :try_start_0
		 v1 = this.mListeners;
		 v1 = 		 (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
		 /* monitor-exit v0 */
		 /* .line 84 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
	 } // .end method
	 public void onInputEvent ( android.view.InputEvent p0 ) {
		 /* .locals 4 */
		 /* .param p1, "event" # Landroid/view/InputEvent; */
		 /* .line 26 */
		 /* instance-of v0, p1, Landroid/view/MotionEvent; */
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 27 */
			 v0 = 			 (( android.view.InputEvent ) p1 ).getSource ( ); // invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I
			 /* and-int/lit8 v0, v0, 0x2 */
			 if ( v0 != null) { // if-eqz v0, :cond_1
				 /* .line 28 */
				 /* move-object v0, p1 */
				 /* check-cast v0, Landroid/view/MotionEvent; */
				 /* .line 30 */
				 /* .local v0, "motionEvent":Landroid/view/MotionEvent; */
				 v1 = this.mListeners;
				 /* monitor-enter v1 */
				 /* .line 31 */
				 try { // :try_start_0
					 v2 = this.mListenersArray;
					 /* if-nez v2, :cond_0 */
					 /* .line 32 */
					 v2 = this.mListeners;
					 v2 = 					 (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
					 /* new-array v2, v2, [Lcom/miui/server/input/gesture/MiuiGestureListener; */
					 this.mListenersArray = v2;
					 /* .line 33 */
					 v3 = this.mListeners;
					 (( java.util.ArrayList ) v3 ).toArray ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
					 /* .line 35 */
				 } // :cond_0
				 v2 = this.mListenersArray;
				 /* .line 36 */
				 /* .local v2, "listeners":[Lcom/miui/server/input/gesture/MiuiGestureListener; */
				 /* monitor-exit v1 */
				 /* :try_end_0 */
				 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
				 /* .line 37 */
				 int v1 = 0; // const/4 v1, 0x0
				 /* .local v1, "i":I */
			 } // :goto_0
			 /* array-length v3, v2 */
			 /* if-ge v1, v3, :cond_1 */
			 /* .line 38 */
			 /* aget-object v3, v2, v1 */
			 /* .line 37 */
			 /* add-int/lit8 v1, v1, 0x1 */
			 /* .line 36 */
		 } // .end local v1 # "i":I
	 } // .end local v2 # "listeners":[Lcom/miui/server/input/gesture/MiuiGestureListener;
	 /* :catchall_0 */
	 /* move-exception v2 */
	 try { // :try_start_1
		 /* monitor-exit v1 */
		 /* :try_end_1 */
		 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
		 /* throw v2 */
		 /* .line 41 */
	 } // .end local v0 # "motionEvent":Landroid/view/MotionEvent;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.gesture.MiuiGestureEventDispatcher ) p0 ).finishInputEvent ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->finishInputEvent(Landroid/view/InputEvent;Z)V
/* .line 42 */
return;
} // .end method
public void registerInputEventListener ( com.miui.server.input.gesture.MiuiGestureListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Lcom/miui/server/input/gesture/MiuiGestureListener; */
/* .line 50 */
v0 = this.mListeners;
/* monitor-enter v0 */
/* .line 51 */
try { // :try_start_0
	 v1 = this.mListeners;
	 v1 = 	 (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 52 */
		 final String v1 = "MiuiGestureEventDispatcher"; // const-string v1, "MiuiGestureEventDispatcher"
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "registerInputEventListener trying to register"; // const-string v3, "registerInputEventListener trying to register"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 final String v3 = " twice."; // const-string v3, " twice."
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v1,v2 );
		 /* .line 54 */
		 /* monitor-exit v0 */
		 return;
		 /* .line 56 */
	 } // :cond_0
	 v1 = this.mListeners;
	 (( java.util.ArrayList ) v1 ).add ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
	 /* .line 57 */
	 int v1 = 0; // const/4 v1, 0x0
	 this.mListenersArray = v1;
	 /* .line 58 */
	 /* monitor-exit v0 */
	 /* .line 59 */
	 return;
	 /* .line 58 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
public void unregisterInputEventListener ( com.miui.server.input.gesture.MiuiGestureListener p0 ) {
	 /* .locals 4 */
	 /* .param p1, "listener" # Lcom/miui/server/input/gesture/MiuiGestureListener; */
	 /* .line 67 */
	 v0 = this.mListeners;
	 /* monitor-enter v0 */
	 /* .line 68 */
	 try { // :try_start_0
		 v1 = this.mListeners;
		 v1 = 		 (( java.util.ArrayList ) v1 ).contains ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
		 /* if-nez v1, :cond_0 */
		 /* .line 69 */
		 final String v1 = "MiuiGestureEventDispatcher"; // const-string v1, "MiuiGestureEventDispatcher"
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 /* const-string/jumbo v3, "unregisterInputEventListener" */
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
		 final String v3 = "not registered."; // const-string v3, "not registered."
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 android.util.Slog .i ( v1,v2 );
		 /* .line 70 */
		 /* monitor-exit v0 */
		 return;
		 /* .line 72 */
	 } // :cond_0
	 v1 = this.mListeners;
	 (( java.util.ArrayList ) v1 ).remove ( p1 ); // invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
	 /* .line 73 */
	 int v1 = 0; // const/4 v1, 0x0
	 this.mListenersArray = v1;
	 /* .line 74 */
	 /* monitor-exit v0 */
	 /* .line 75 */
	 return;
	 /* .line 74 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
