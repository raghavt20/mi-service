.class public Lcom/miui/server/input/gesture/MiuiGestureMonitor;
.super Ljava/lang/Object;
.source "MiuiGestureMonitor.java"


# static fields
.field private static sMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;


# instance fields
.field private final mDisplayId:I

.field private mGestureInputMonitor:Landroid/view/InputMonitor;

.field private mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

.field private final mHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "miui-gesture"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mHandlerThread:Landroid/os/HandlerThread;

    .line 19
    invoke-virtual {p1}, Landroid/content/Context;->getDisplayId()I

    move-result v1

    iput v1, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mDisplayId:I

    .line 20
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 21
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    const-class v0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    monitor-enter v0

    .line 24
    :try_start_0
    sget-object v1, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->sMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    if-nez v1, :cond_0

    .line 25
    new-instance v1, Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->sMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    .line 27
    :cond_0
    sget-object v1, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->sMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 23
    .end local p0    # "context":Landroid/content/Context;
    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private registerGestureMonitor()V
    .locals 3

    .line 31
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v0

    const-string v1, "miui-gesture"

    iget v2, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mDisplayId:I

    invoke-virtual {v0, v1, v2}, Landroid/hardware/input/InputManager;->monitorGestureInput(Ljava/lang/String;I)Landroid/view/InputMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGestureInputMonitor:Landroid/view/InputMonitor;

    .line 32
    new-instance v0, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    iget-object v1, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGestureInputMonitor:Landroid/view/InputMonitor;

    .line 33
    invoke-virtual {v1}, Landroid/view/InputMonitor;->getInputChannel()Landroid/view/InputChannel;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    .line 34
    return-void
.end method

.method private unregisterGestureMonitor()V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGestureInputMonitor:Landroid/view/InputMonitor;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0}, Landroid/view/InputMonitor;->dispose()V

    .line 40
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGestureInputMonitor:Landroid/view/InputMonitor;

    .line 41
    iput-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    .line 42
    return-void
.end method


# virtual methods
.method public pilferPointers()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGestureInputMonitor:Landroid/view/InputMonitor;

    invoke-virtual {v0}, Landroid/view/InputMonitor;->pilferPointers()V

    .line 64
    return-void
.end method

.method public registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 45
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->getGestureListenerCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 47
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerGestureMonitor()V

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->registerInputEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 50
    return-void
.end method

.method public unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/miui/server/input/gesture/MiuiGestureListener;

    .line 53
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    if-nez v0, :cond_0

    .line 54
    return-void

    .line 56
    :cond_0
    invoke-virtual {v0, p1}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->unregisterInputEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 57
    iget-object v0, p0, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->mGesturePointerEventDispatcher:Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;

    invoke-virtual {v0}, Lcom/miui/server/input/gesture/MiuiGestureEventDispatcher;->getGestureListenerCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 58
    invoke-direct {p0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterGestureMonitor()V

    .line 60
    :cond_1
    return-void
.end method
