.class public Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;
.super Ljava/lang/Object;
.source "MiuiMultiFingerGestureRect.java"


# instance fields
.field private mHeight:I

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mWidth:I

    .line 12
    iput p2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mHeight:I

    .line 13
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mHeight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mWidth:I

    return v0
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .line 28
    iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mHeight:I

    .line 29
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .line 20
    iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mWidth:I

    .line 21
    return-void
.end method
