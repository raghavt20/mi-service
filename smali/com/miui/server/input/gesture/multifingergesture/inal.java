public class inal extends java.lang.Enum {
	 /* .source "MiuiMultiFingerGestureStatus.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/lang/Enum<", */
	 /* "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* # static fields */
private static final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus $VALUES; //synthetic
public static final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus DETECTING;
public static final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus FAIL;
public static final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus NONE;
public static final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus READY;
public static final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus SUCCESS;
/* # direct methods */
private static com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus $values ( ) { //synthethic
	 /* .locals 5 */
	 /* .line 7 */
	 v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.NONE;
	 v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.READY;
	 v2 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.DETECTING;
	 v3 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.SUCCESS;
	 v4 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
	 /* filled-new-array {v0, v1, v2, v3, v4}, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
} // .end method
static inal ( ) {
	 /* .locals 3 */
	 /* .line 12 */
	 /* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
	 final String v1 = "NONE"; // const-string v1, "NONE"
	 int v2 = 0; // const/4 v2, 0x0
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V */
	 /* .line 16 */
	 /* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
	 final String v1 = "READY"; // const-string v1, "READY"
	 int v2 = 1; // const/4 v2, 0x1
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V */
	 /* .line 20 */
	 /* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
	 final String v1 = "DETECTING"; // const-string v1, "DETECTING"
	 int v2 = 2; // const/4 v2, 0x2
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V */
	 /* .line 24 */
	 /* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
	 final String v1 = "SUCCESS"; // const-string v1, "SUCCESS"
	 int v2 = 3; // const/4 v2, 0x3
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V */
	 /* .line 28 */
	 /* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
	 final String v1 = "FAIL"; // const-string v1, "FAIL"
	 int v2 = 4; // const/4 v2, 0x4
	 /* invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V */
	 /* .line 7 */
	 com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus .$values ( );
	 return;
} // .end method
private inal ( ) {
	 /* .locals 0 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()V" */
	 /* } */
} // .end annotation
/* .line 7 */
/* invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V */
return;
} // .end method
public static com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus valueOf ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "name" # Ljava/lang/String; */
/* .line 7 */
/* const-class v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
java.lang.Enum .valueOf ( v0,p0 );
/* check-cast v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
} // .end method
public static com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus values ( ) {
/* .locals 1 */
/* .line 7 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.$VALUES;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus ) v0 ).clone ( ); // invoke-virtual {v0}, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->clone()Ljava/lang/Object;
/* check-cast v0, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
} // .end method
