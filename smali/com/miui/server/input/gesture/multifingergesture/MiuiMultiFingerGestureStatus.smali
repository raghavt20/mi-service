.class public final enum Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
.super Ljava/lang/Enum;
.source "MiuiMultiFingerGestureStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

.field public static final enum DETECTING:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

.field public static final enum FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

.field public static final enum NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

.field public static final enum READY:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

.field public static final enum SUCCESS:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;


# direct methods
.method private static synthetic $values()[Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    .locals 5

    .line 7
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    sget-object v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->READY:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    sget-object v2, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->DETECTING:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    sget-object v3, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->SUCCESS:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    sget-object v4, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    filled-new-array {v0, v1, v2, v3, v4}, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 12
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 16
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    const-string v1, "READY"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->READY:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 20
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    const-string v1, "DETECTING"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->DETECTING:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 24
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    const-string v1, "SUCCESS"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->SUCCESS:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 28
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    const-string v1, "FAIL"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 7
    invoke-static {}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->$values()[Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->$VALUES:[Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 7
    const-class v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    return-object v0
.end method

.method public static values()[Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    .locals 1

    .line 7
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->$VALUES:[Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {v0}, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    return-object v0
.end method
