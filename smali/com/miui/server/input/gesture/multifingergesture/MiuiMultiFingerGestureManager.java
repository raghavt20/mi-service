public class com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager implements com.miui.server.input.gesture.MiuiGestureListener {
	 /* .source "MiuiMultiFingerGestureManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.util.Set DISABLE_GESTURE_APPS;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String TAG;
/* # instance fields */
private final java.util.List mAllMultiFingerGestureList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mBootCompleted;
private final android.content.Context mContext;
private Boolean mContinueSendEvent;
private com.android.server.policy.WindowManagerPolicy$WindowState mCurrentFocusedWindow;
private Integer mCurrentUserId;
private Boolean mDeviceProvisioned;
private Boolean mEnable;
private java.util.function.BooleanSupplier mGetKeyguardActiveFunction;
private final android.os.Handler mHandler;
private Boolean mIsFolded;
private Boolean mIsRegister;
private Boolean mIsScreenOn;
private final com.miui.server.input.gesture.MiuiGestureMonitor mMiuiGestureMonitor;
private final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver mMiuiSettingsObserver;
private Boolean mNeedCancel;
private final java.util.Set mNotNeedHapticFeedbackFunction;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$dcEVTpx3eF5CghUa_RZEYGeX3KI ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->lambda$updateSettings$2(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
return;
} // .end method
static java.util.List -$$Nest$fgetmAllMultiFingerGestureList ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mAllMultiFingerGestureList;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmEnable ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z */
} // .end method
static Boolean -$$Nest$fgetmIsRegister ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsRegister:Z */
} // .end method
static com.miui.server.input.gesture.MiuiGestureMonitor -$$Nest$fgetmMiuiGestureMonitor ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMiuiGestureMonitor;
} // .end method
static void -$$Nest$fputmDeviceProvisioned ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mDeviceProvisioned:Z */
return;
} // .end method
static void -$$Nest$fputmEnable ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z */
return;
} // .end method
static void -$$Nest$fputmIsRegister ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsRegister:Z */
return;
} // .end method
static Boolean -$$Nest$misEmpty ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->isEmpty(Ljava/lang/String;)Z */
} // .end method
static void -$$Nest$monConfigChange ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->onConfigChange()V */
return;
} // .end method
static com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ( ) {
/* .locals 1 */
/* .line 43 */
final String v0 = "com.android.cts.verifier"; // const-string v0, "com.android.cts.verifier"
java.util.Set .of ( v0 );
return;
} // .end method
public com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "mHandler" # Landroid/os/Handler; */
/* .line 90 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 58 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
this.mNotNeedHapticFeedbackFunction = v0;
/* .line 91 */
this.mContext = p1;
/* .line 92 */
this.mHandler = p2;
/* .line 93 */
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( p1 );
this.mMiuiGestureMonitor = v0;
/* .line 94 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mAllMultiFingerGestureList = v0;
/* .line 95 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initAllGesture()V */
/* .line 96 */
/* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver; */
/* invoke-direct {v0, p0, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Landroid/os/Handler;)V */
this.mMiuiSettingsObserver = v0;
/* .line 97 */
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->observe()V
/* .line 98 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateSettings()V */
/* .line 99 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->registerOnConfigBroadcast()V */
/* .line 100 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initNotNeedHapticFeedbackFunction()V */
/* .line 101 */
return;
} // .end method
private Boolean canPerformGesture ( android.view.MotionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 187 */
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mDeviceProvisioned:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsScreenOn:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
/* move v0, v2 */
/* .line 188 */
/* .local v0, "isGestureAvailable":Z */
} // :goto_0
/* if-nez v0, :cond_1 */
/* .line 189 */
/* .line 191 */
} // :cond_1
v3 = (( android.view.MotionEvent ) p1 ).getDeviceId ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I
int v4 = -1; // const/4 v4, -0x1
/* if-ne v3, v4, :cond_2 */
/* .line 192 */
/* .line 195 */
} // :cond_2
v2 = this.mCurrentFocusedWindow;
/* if-nez v2, :cond_3 */
/* .line 196 */
/* .line 199 */
} // :cond_3
v2 = v3 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager.DISABLE_GESTURE_APPS;
/* xor-int/2addr v1, v2 */
} // .end method
private Boolean checkBootCompleted ( ) {
/* .locals 2 */
/* .line 278 */
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mBootCompleted:Z */
/* if-nez v0, :cond_0 */
/* .line 279 */
/* const-string/jumbo v0, "sys.boot_completed" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mBootCompleted:Z */
/* .line 280 */
/* .line 282 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void gestureStatusIsDetecting ( android.view.MotionEvent p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 249 */
v0 = (( android.view.MotionEvent ) p1 ).getPointerCount ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I
v1 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* if-ne v0, v1, :cond_0 */
/* .line 250 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).onTouchEvent ( p1 ); // invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onTouchEvent(Landroid/view/MotionEvent;)V
/* .line 253 */
} // :cond_0
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).changeStatus ( v0 ); // invoke-virtual {p2, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 255 */
} // :goto_0
return;
} // .end method
private void gestureStatusIsReady ( android.view.MotionEvent p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .param p2, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 258 */
v0 = (( android.view.MotionEvent ) p1 ).getPointerCount ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I
v1 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* if-eq v0, v1, :cond_0 */
/* .line 260 */
return;
/* .line 262 */
} // :cond_0
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).preCondition ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->preCondition()Z
/* if-nez v0, :cond_1 */
/* .line 264 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).changeStatus ( v0 ); // invoke-virtual {p2, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 265 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getGestureKey ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " init fail, because pre condition."; // const-string v1, " init fail, because pre condition."
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiFingerGestureManager"; // const-string v1, "MiuiMultiFingerGestureManager"
android.util.Slog .i ( v1,v0 );
/* .line 266 */
return;
/* .line 268 */
} // :cond_1
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).initGesture ( p1 ); // invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->initGesture(Landroid/view/MotionEvent;)V
/* .line 269 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getStatus ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.DETECTING;
/* if-ne v0, v1, :cond_2 */
/* .line 271 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->pilferPointers()V */
/* .line 273 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).onTouchEvent ( p1 ); // invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onTouchEvent(Landroid/view/MotionEvent;)V
/* .line 275 */
} // :cond_2
return;
} // .end method
private void initAllGesture ( ) {
/* .locals 4 */
/* .line 120 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture; */
v2 = this.mContext;
v3 = this.mHandler;
/* invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 121 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture; */
v2 = this.mContext;
v3 = this.mHandler;
/* invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 122 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture; */
v2 = this.mContext;
v3 = this.mHandler;
/* invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 123 */
v0 = this.mContext;
v0 = android.view.animation.DeviceHelper .isTablet ( v0 );
/* if-nez v0, :cond_0 */
v0 = android.view.animation.DeviceHelper .isFoldDevice ( );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 124 */
} // :cond_0
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture; */
v2 = this.mContext;
v3 = this.mHandler;
/* invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 126 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalRTLGesture; */
v2 = this.mContext;
v3 = this.mHandler;
/* invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalRTLGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 129 */
} // :cond_1
return;
} // .end method
private void initAllGestureStatus ( ) {
/* .locals 2 */
/* .line 152 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda3; */
/* invoke-direct {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda3;-><init>()V */
/* .line 159 */
return;
} // .end method
private void initNotNeedHapticFeedbackFunction ( ) {
/* .locals 2 */
/* .line 104 */
v0 = this.mNotNeedHapticFeedbackFunction;
/* const-string/jumbo v1, "screen_shot" */
/* .line 105 */
v0 = this.mNotNeedHapticFeedbackFunction;
final String v1 = "partial_screen_shot"; // const-string v1, "partial_screen_shot"
/* .line 106 */
v0 = this.mNotNeedHapticFeedbackFunction;
final String v1 = "dump_log"; // const-string v1, "dump_log"
/* .line 107 */
return;
} // .end method
private Boolean isEmpty ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 326 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
final String v0 = "none"; // const-string v0, "none"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
static void lambda$dump$3 ( java.io.PrintWriter p0, java.lang.String p1, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "pw" # Ljava/io/PrintWriter; */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 348 */
(( java.io.PrintWriter ) p0 ).print ( p1 ); // invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 349 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gestureName="; // const-string v1, "gestureName="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) p2 ).getClass ( ); // invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 350 */
(( java.io.PrintWriter ) p0 ).print ( p1 ); // invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 351 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gestureKey="; // const-string v1, "gestureKey="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getGestureKey ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 352 */
(( java.io.PrintWriter ) p0 ).print ( p1 ); // invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 353 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "gestureFunction="; // const-string v1, "gestureFunction="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getGestureFunction ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p0 ).println ( v0 ); // invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 354 */
(( java.io.PrintWriter ) p0 ).println ( ); // invoke-virtual {p0}, Ljava/io/PrintWriter;->println()V
/* .line 355 */
return;
} // .end method
static void lambda$initAllGestureStatus$1 ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0 ) { //synthethic
/* .locals 2 */
/* .param p0, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 153 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getStatus ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
/* .line 154 */
/* .local v0, "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.NONE;
/* if-ne v0, v1, :cond_0 */
/* .line 155 */
return;
/* .line 157 */
} // :cond_0
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.READY;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).changeStatus ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 158 */
return;
} // .end method
static void lambda$updateAllGestureStatus$0 ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0, com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus p1, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p2 ) { //synthethic
/* .locals 2 */
/* .param p0, "except" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .param p1, "newStatus" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
/* .param p2, "multiFingerGesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 139 */
/* if-ne p2, p0, :cond_0 */
/* .line 140 */
return;
/* .line 142 */
} // :cond_0
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getStatus ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
/* .line 143 */
/* .local v0, "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
/* if-eq v0, p1, :cond_2 */
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.NONE;
/* if-ne v0, v1, :cond_1 */
/* .line 147 */
} // :cond_1
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).changeStatus ( p1 ); // invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 148 */
return;
/* .line 145 */
} // :cond_2
} // :goto_0
return;
} // .end method
private void lambda$updateSettings$2 ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 340 */
v0 = this.mMiuiSettingsObserver;
/* .line 341 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureKey ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 340 */
return;
} // .end method
private void onConfigChange ( ) {
/* .locals 2 */
/* .line 364 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda2;-><init>()V */
/* .line 365 */
return;
} // .end method
private void pilferPointers ( ) {
/* .locals 2 */
/* .line 292 */
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNeedCancel:Z */
/* if-nez v0, :cond_0 */
/* .line 293 */
return;
/* .line 295 */
} // :cond_0
v0 = this.mMiuiGestureMonitor;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).pilferPointers ( ); // invoke-virtual {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->pilferPointers()V
/* .line 296 */
final String v0 = "MiuiMultiFingerGestureManager"; // const-string v0, "MiuiMultiFingerGestureManager"
final String v1 = "Pilfer pointers because gesture detected"; // const-string v1, "Pilfer pointers because gesture detected"
android.util.Slog .d ( v0,v1 );
/* .line 297 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNeedCancel:Z */
/* .line 298 */
return;
} // .end method
private void registerOnConfigBroadcast ( ) {
/* .locals 4 */
/* .line 110 */
v0 = this.mContext;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$1;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.CONFIGURATION_CHANGED"; // const-string v3, "android.intent.action.CONFIGURATION_CHANGED"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v0 ).registerReceiver ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 116 */
return;
} // .end method
private void sendEventToGestures ( android.view.MotionEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 203 */
int v0 = 0; // const/4 v0, 0x0
/* .line 204 */
/* .local v0, "count":I */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNeedCancel:Z */
/* .line 205 */
v1 = this.mAllMultiFingerGestureList;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 206 */
/* .local v2, "gesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) v2 ).getStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
/* .line 207 */
/* .local v3, "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
v4 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$2.$SwitchMap$com$miui$server$input$gesture$multifingergesture$MiuiMultiFingerGestureStatus;
v5 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus ) v3 ).ordinal ( ); // invoke-virtual {v3}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->ordinal()I
/* aget v4, v4, v5 */
/* packed-switch v4, :pswitch_data_0 */
/* .line 212 */
/* :pswitch_0 */
/* invoke-direct {p0, p1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->gestureStatusIsDetecting(Landroid/view/MotionEvent;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
/* .line 213 */
/* .line 209 */
/* :pswitch_1 */
/* invoke-direct {p0, p1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->gestureStatusIsReady(Landroid/view/MotionEvent;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
/* .line 210 */
/* nop */
/* .line 219 */
} // :goto_1
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) v2 ).getStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
/* .line 220 */
v4 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.SUCCESS;
/* if-ne v3, v4, :cond_0 */
/* .line 221 */
return;
/* .line 223 */
} // :cond_0
v4 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
/* if-eq v3, v4, :cond_1 */
v4 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.NONE;
/* if-ne v3, v4, :cond_2 */
/* .line 225 */
} // :cond_1
/* add-int/lit8 v0, v0, 0x1 */
/* .line 227 */
} // .end local v2 # "gesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
} // .end local v3 # "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
} // :cond_2
/* .line 228 */
} // :cond_3
v1 = v1 = this.mAllMultiFingerGestureList;
/* if-ne v0, v1, :cond_4 */
/* .line 229 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z */
/* .line 231 */
} // :cond_4
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void triggerFunction ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0 ) {
/* .locals 6 */
/* .param p1, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 286 */
v0 = this.mNotNeedHapticFeedbackFunction;
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureFunction ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;
/* .line 287 */
/* .local v0, "notNeedHapticFeedback":Z */
v1 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v1 );
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureFunction ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;
/* .line 288 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureKey ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
/* .line 287 */
/* xor-int/lit8 v4, v0, 0x1 */
int v5 = 0; // const/4 v5, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v1 ).triggerFunction ( v2, v3, v5, v4 ); // invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 289 */
return;
} // .end method
private void updateAllGestureStatus ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) {
/* .locals 2 */
/* .param p1, "newStatus" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
/* .param p2, "except" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 138 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V */
/* .line 149 */
return;
} // .end method
private void updateSettings ( ) {
/* .locals 3 */
/* .line 331 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 1; // const/4 v1, 0x1
final String v2 = "enable_three_gesture"; // const-string v2, "enable_three_gesture"
android.provider.Settings$System .putInt ( v0,v2,v1 );
/* .line 333 */
v0 = this.mMiuiSettingsObserver;
/* if-nez v0, :cond_0 */
/* .line 334 */
return;
/* .line 336 */
} // :cond_0
/* nop */
/* .line 337 */
android.provider.Settings$System .getUriFor ( v2 );
/* .line 336 */
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 338 */
v0 = this.mMiuiSettingsObserver;
/* .line 339 */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
android.provider.Settings$Global .getUriFor ( v1 );
/* .line 338 */
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 340 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda4;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 342 */
return;
} // .end method
/* # virtual methods */
public void checkSuccess ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0 ) {
/* .locals 1 */
/* .param p1, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 240 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z */
/* .line 241 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateAllGestureStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
/* .line 242 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->checkBootCompleted()Z */
/* if-nez v0, :cond_0 */
/* .line 243 */
return;
/* .line 245 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->triggerFunction(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
/* .line 246 */
return;
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 345 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 346 */
final String v0 = "MiuiMultiFingerGestureManager"; // const-string v0, "MiuiMultiFingerGestureManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 347 */
v0 = this.mAllMultiFingerGestureList;
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda1;-><init>(Ljava/io/PrintWriter;Ljava/lang/String;)V */
/* .line 356 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mBootCompleted = "; // const-string v1, "mBootCompleted = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mBootCompleted:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 357 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mDeviceProvisioned = "; // const-string v1, "mDeviceProvisioned = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mDeviceProvisioned:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 358 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mIsScreenOn = "; // const-string v1, "mIsScreenOn = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsScreenOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 359 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mEnable = "; // const-string v1, "mEnable = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 360 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mIsFolded = "; // const-string v1, "mIsFolded = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsFolded:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 361 */
return;
} // .end method
public Boolean getIsFolded ( ) {
/* .locals 1 */
/* .line 87 */
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsFolded:Z */
} // .end method
public void initKeyguardActiveFunction ( java.util.function.BooleanSupplier p0 ) {
/* .locals 0 */
/* .param p1, "getKeyguardActiveFunction" # Ljava/util/function/BooleanSupplier; */
/* .line 312 */
this.mGetKeyguardActiveFunction = p1;
/* .line 313 */
return;
} // .end method
public Boolean isKeyguardActive ( ) {
/* .locals 1 */
/* .line 319 */
v0 = this.mGetKeyguardActiveFunction;
/* if-nez v0, :cond_0 */
/* .line 320 */
int v0 = 0; // const/4 v0, 0x0
/* .line 322 */
v0 = } // :cond_0
} // .end method
public void notifyFoldStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "folded" # Z */
/* .line 372 */
/* iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsFolded:Z */
/* .line 373 */
return;
} // .end method
public void onFocusedWindowChanged ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 0 */
/* .param p1, "newFocus" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 368 */
this.mCurrentFocusedWindow = p1;
/* .line 369 */
return;
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 163 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* sparse-switch v0, :sswitch_data_0 */
/* .line 173 */
/* :sswitch_0 */
final String v0 = "MiuiMultiFingerGestureManager"; // const-string v0, "MiuiMultiFingerGestureManager"
final String v1 = "Receive a cancel event, all gesture will fail."; // const-string v1, "Receive a cancel event, all gesture will fail."
android.util.Slog .d ( v0,v1 );
/* .line 174 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateAllGestureStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
/* .line 175 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z */
/* .line 176 */
return;
/* .line 167 */
/* :sswitch_1 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->canPerformGesture(Landroid/view/MotionEvent;)Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z */
/* .line 168 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 169 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initAllGestureStatus()V */
/* .line 179 */
} // :cond_0
} // :goto_0
/* iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z */
/* if-nez v0, :cond_1 */
/* .line 180 */
return;
/* .line 182 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->sendEventToGestures(Landroid/view/MotionEvent;)V */
/* .line 183 */
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x0 -> :sswitch_1 */
/* 0x3 -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newUserId" # I */
/* .line 305 */
/* iget v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mCurrentUserId:I */
/* if-eq v0, p1, :cond_0 */
/* .line 306 */
/* iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mCurrentUserId:I */
/* .line 307 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateSettings()V */
/* .line 309 */
} // :cond_0
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "screenOn" # Z */
/* .line 301 */
/* iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsScreenOn:Z */
/* .line 302 */
return;
} // .end method
