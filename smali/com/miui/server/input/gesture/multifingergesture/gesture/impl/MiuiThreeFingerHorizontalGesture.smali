.class public Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;
.super Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
.source "MiuiThreeFingerHorizontalGesture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalRTLGesture;,
        Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;
    }
.end annotation


# instance fields
.field protected mThreshold:F

.field private final mValidRangeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "manager"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    .line 31
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    invoke-direct {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;-><init>()V

    .line 32
    .local v0, "rect":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    filled-new-array {v1, v0}, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mValidRangeList:Ljava/util/List;

    .line 33
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->updateConfig()V

    .line 34
    return-void
.end method

.method private updateConfig()V
    .locals 3

    .line 73
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->getFunctionNeedFingerNum()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mContext:Landroid/content/Context;

    .line 74
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v0, v1

    .line 75
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mThreshold:F

    .line 76
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mValidRangeList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    .line 77
    .local v0, "rect":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    invoke-virtual {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setHeight(I)V

    .line 78
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    invoke-virtual {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setWidth(I)V

    .line 79
    return-void
.end method


# virtual methods
.method public getGestureKey()Ljava/lang/String;
    .locals 1

    .line 43
    const-string/jumbo v0, "three_gesture_horizontal"

    return-object v0
.end method

.method protected getValidRange()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mValidRangeList:Ljava/util/List;

    return-object v0
.end method

.method protected handleEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 48
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 51
    .local v0, "distanceX":F
    const/4 v1, 0x0

    .line 52
    .local v1, "distanceY":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->getFunctionNeedFingerNum()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 53
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mInitX:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v0, v3

    .line 54
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mInitY:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v1, v3

    .line 52
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "i":I
    :cond_1
    iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mThreshold:F

    cmpl-float v3, v0, v2

    if-ltz v3, :cond_2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_2

    .line 57
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->checkSuccess()V

    .line 59
    :cond_2
    return-void
.end method

.method protected isEventFromTouchScreen(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 82
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InputDevice;->getSources()I

    move-result v1

    const/16 v2, 0x1002

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_1

    .line 85
    const/4 v0, 0x1

    return v0

    .line 87
    :cond_1
    return v0

    .line 82
    :cond_2
    :goto_0
    return v0
.end method

.method public onConfigChange()V
    .locals 0

    .line 68
    invoke-super {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onConfigChange()V

    .line 69
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->updateConfig()V

    .line 70
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 38
    invoke-virtual {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->handleEvent(Landroid/view/MotionEvent;)V

    .line 39
    return-void
.end method
