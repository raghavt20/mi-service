.class public Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;
.super Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
.source "MiuiThreeFingerDownGesture.java"


# static fields
.field private static final HORIZONTAL_THRESHOLD:I = 0x18


# instance fields
.field private mThreshold:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "manager"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    .line 30
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->updateConfig()V

    .line 31
    return-void
.end method

.method private handleEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 45
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    .line 48
    .local v0, "distanceX":F
    const/4 v1, 0x0

    .line 49
    .local v1, "distanceY":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->getFunctionNeedFingerNum()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 50
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mInitY:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    add-float/2addr v1, v3

    .line 51
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mInitX:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v0, v3

    .line 49
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    .end local v2    # "i":I
    :cond_1
    iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F

    neg-float v2, v2

    cmpg-float v2, v1, v2

    const-string v3, "Gesture fail, because "

    if-gez v2, :cond_2

    .line 54
    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F

    neg-float v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->checkFail()V

    .line 56
    return-void

    .line 58
    :cond_2
    sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v2, :cond_3

    const/high16 v2, 0x41c00000    # 24.0f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    cmpl-float v2, v0, v1

    if-lez v2, :cond_3

    .line 59
    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->checkFail()V

    .line 61
    return-void

    .line 63
    :cond_3
    iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_4

    .line 64
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->checkSuccess()V

    .line 66
    :cond_4
    return-void
.end method

.method private updateConfig()V
    .locals 2

    .line 75
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->getFunctionNeedFingerNum()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mContext:Landroid/content/Context;

    .line 76
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v0, v1

    .line 77
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F

    .line 78
    return-void
.end method


# virtual methods
.method public getGestureKey()Ljava/lang/String;
    .locals 1

    .line 40
    const-string/jumbo v0, "three_gesture_down"

    return-object v0
.end method

.method public onConfigChange()V
    .locals 0

    .line 70
    invoke-super {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onConfigChange()V

    .line 71
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->updateConfig()V

    .line 72
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 35
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->handleEvent(Landroid/view/MotionEvent;)V

    .line 36
    return-void
.end method
