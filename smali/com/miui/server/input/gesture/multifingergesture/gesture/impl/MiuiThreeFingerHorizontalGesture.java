public class com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture extends com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture {
	 /* .source "MiuiThreeFingerHorizontalGesture.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalRTLGesture;, */
	 /* Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture; */
	 /* } */
} // .end annotation
/* # instance fields */
protected Float mThreshold;
private final java.util.List mValidRangeList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "manager" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
/* .line 30 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 31 */
/* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
/* invoke-direct {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;-><init>()V */
/* .line 32 */
/* .local v0, "rect":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
v1 = this.mDefaultRage;
/* filled-new-array {v1, v0}, [Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
java.util.Arrays .asList ( v1 );
this.mValidRangeList = v1;
/* .line 33 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->updateConfig()V */
/* .line 34 */
return;
} // .end method
private void updateConfig ( ) {
/* .locals 3 */
/* .line 73 */
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->getFunctionNeedFingerNum()I
/* int-to-float v0, v0 */
v1 = this.mContext;
/* .line 74 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getDisplayMetrics ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v1, v1, Landroid/util/DisplayMetrics;->density:F */
/* mul-float/2addr v0, v1 */
/* const/high16 v1, 0x42480000 # 50.0f */
/* mul-float/2addr v0, v1 */
/* .line 75 */
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
int v2 = 1; // const/4 v2, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :cond_0
/* move v1, v2 */
} // :goto_0
/* int-to-float v1, v1 */
/* mul-float/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mThreshold:F */
/* .line 76 */
v0 = this.mValidRangeList;
/* check-cast v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
/* .line 77 */
/* .local v0, "rect":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
v1 = this.mDefaultRage;
v1 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) v1 ).getWidth ( ); // invoke-virtual {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getWidth()I
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) v0 ).setHeight ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setHeight(I)V
/* .line 78 */
v1 = this.mDefaultRage;
v1 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) v1 ).getHeight ( ); // invoke-virtual {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getHeight()I
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) v0 ).setWidth ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setWidth(I)V
/* .line 79 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getGestureKey ( ) {
/* .locals 1 */
/* .line 43 */
/* const-string/jumbo v0, "three_gesture_horizontal" */
} // .end method
protected java.util.List getValidRange ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 63 */
v0 = this.mValidRangeList;
} // .end method
protected void handleEvent ( android.view.MotionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 47 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_0 */
/* .line 48 */
return;
/* .line 50 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 51 */
/* .local v0, "distanceX":F */
int v1 = 0; // const/4 v1, 0x0
/* .line 52 */
/* .local v1, "distanceY":F */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->getFunctionNeedFingerNum()I
/* if-ge v2, v3, :cond_1 */
/* .line 53 */
v3 = (( android.view.MotionEvent ) p1 ).getX ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F
v4 = this.mInitX;
/* aget v4, v4, v2 */
/* sub-float/2addr v3, v4 */
v3 = java.lang.Math .abs ( v3 );
/* add-float/2addr v0, v3 */
/* .line 54 */
v3 = (( android.view.MotionEvent ) p1 ).getY ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F
v4 = this.mInitY;
/* aget v4, v4, v2 */
/* sub-float/2addr v3, v4 */
v3 = java.lang.Math .abs ( v3 );
/* add-float/2addr v1, v3 */
/* .line 52 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 56 */
} // .end local v2 # "i":I
} // :cond_1
/* iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->mThreshold:F */
/* cmpl-float v3, v0, v2 */
/* if-ltz v3, :cond_2 */
/* cmpg-float v2, v1, v2 */
/* if-gtz v2, :cond_2 */
/* .line 57 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture ) p0 ).checkSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->checkSuccess()V
/* .line 59 */
} // :cond_2
return;
} // .end method
protected Boolean isEventFromTouchScreen ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 82 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
(( android.view.MotionEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;
/* if-nez v1, :cond_0 */
/* .line 83 */
} // :cond_0
(( android.view.MotionEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;
v1 = (( android.view.InputDevice ) v1 ).getSources ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->getSources()I
/* const/16 v2, 0x1002 */
/* and-int/2addr v1, v2 */
/* if-ne v1, v2, :cond_1 */
/* .line 85 */
int v0 = 1; // const/4 v0, 0x1
/* .line 87 */
} // :cond_1
/* .line 82 */
} // :cond_2
} // :goto_0
} // .end method
public void onConfigChange ( ) {
/* .locals 0 */
/* .line 68 */
/* invoke-super {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onConfigChange()V */
/* .line 69 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->updateConfig()V */
/* .line 70 */
return;
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 0 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 38 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture ) p0 ).handleEvent ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;->handleEvent(Landroid/view/MotionEvent;)V
/* .line 39 */
return;
} // .end method
