public abstract class com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture {
	 /* .source "BaseMiuiMultiFingerGesture.java" */
	 /* # static fields */
	 private static final Integer DEFAULT_FINGER_NUM;
	 private static final Integer DEFAULT_FIRST_AND_LAST_FINGER_DOWN_TIME_DIFF;
	 private static final Integer DEFAULT_MAX_HEIGHT_DIFF;
	 private static final Integer FIXED_WIDTH;
	 /* # instance fields */
	 protected final java.lang.String TAG;
	 protected android.content.Context mContext;
	 protected com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect mDefaultRage;
	 private java.util.List mDefaultRageList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
protected java.lang.String mFunction;
protected android.os.Handler mHandler;
protected mInitX;
protected mInitY;
protected final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager mMiuiMultiFingerGestureManager;
private com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus mStatus;
/* # direct methods */
static com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ( ) {
/* .locals 2 */
/* .line 32 */
/* const-string/jumbo v0, "support_fix_width_device" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getInteger ( v0,v1 );
/* .line 33 */
return;
} // .end method
protected com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "manager" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
/* .line 64 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
(( java.lang.Object ) p0 ).getClass ( ); // invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
this.TAG = v0;
/* .line 45 */
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* new-array v0, v0, [F */
this.mInitX = v0;
/* .line 49 */
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* new-array v0, v0, [F */
this.mInitY = v0;
/* .line 65 */
this.mContext = p1;
/* .line 66 */
this.mHandler = p2;
/* .line 67 */
this.mMiuiMultiFingerGestureManager = p3;
/* .line 68 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->initDefaultRage()V */
/* .line 69 */
return;
} // .end method
private Boolean checkIsInValidRange ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 3 */
/* .param p1, "minX" # F */
/* .param p2, "minY" # F */
/* .param p3, "maxX" # F */
/* .param p4, "maxY" # F */
/* .line 124 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getValidRange ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getValidRange()Ljava/util/List;
/* .line 125 */
/* .local v0, "validRange":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;>;" */
/* if-nez v0, :cond_0 */
/* .line 126 */
int v1 = 0; // const/4 v1, 0x0
/* .line 128 */
} // :cond_0
/* new-instance v2, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture$$ExternalSyntheticLambda0; */
v1 = /* invoke-direct {v2, p3, p1, p4, p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture$$ExternalSyntheticLambda0;-><init>(FFFF)V */
} // .end method
private Boolean hasEventFromShoulderKey ( android.view.MotionEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 184 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* if-ge v0, v1, :cond_1 */
/* .line 185 */
/* const/16 v1, 0x20 */
v1 = (( android.view.MotionEvent ) p1 ).getAxisValue ( v1, v0 ); // invoke-virtual {p1, v1, v0}, Landroid/view/MotionEvent;->getAxisValue(II)F
/* const/high16 v2, 0x41800000 # 16.0f */
/* cmpl-float v1, v1, v2 */
/* if-ltz v1, :cond_0 */
/* .line 186 */
v1 = this.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Gesture "; // const-string v3, "Gesture "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getGestureKey ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " init fail, because pointer "; // const-string v3, " init fail, because pointer "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " from shoulder key."; // const-string v3, " from shoulder key."
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 188 */
int v1 = 1; // const/4 v1, 0x1
/* .line 184 */
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 191 */
} // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void initDefaultRage ( ) {
/* .locals 1 */
/* .line 72 */
/* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
/* invoke-direct {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;-><init>()V */
this.mDefaultRage = v0;
/* .line 73 */
java.util.Collections .singletonList ( v0 );
this.mDefaultRageList = v0;
/* .line 74 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->updateRage()V */
/* .line 75 */
return;
} // .end method
static Boolean lambda$checkIsInValidRange$0 ( Float p0, Float p1, Float p2, Float p3, com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect p4 ) { //synthethic
/* .locals 2 */
/* .param p0, "maxX" # F */
/* .param p1, "minX" # F */
/* .param p2, "maxY" # F */
/* .param p3, "minY" # F */
/* .param p4, "rect" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect; */
/* .line 128 */
/* sub-float v0, p0, p1 */
v1 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) p4 ).getWidth ( ); // invoke-virtual {p4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getWidth()I
/* int-to-float v1, v1 */
/* cmpg-float v0, v0, v1 */
/* if-gtz v0, :cond_0 */
/* sub-float v0, p2, p3 */
/* .line 129 */
v1 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) p4 ).getHeight ( ); // invoke-virtual {p4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getHeight()I
/* int-to-float v1, v1 */
/* cmpg-float v0, v0, v1 */
/* if-gtz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 128 */
} // :goto_0
} // .end method
private void updateRage ( ) {
/* .locals 6 */
/* .line 78 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* .line 79 */
/* .local v0, "displayMetrics":Landroid/util/DisplayMetrics; */
/* iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I */
/* .line 80 */
/* .local v1, "height":I */
/* iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I */
/* .line 85 */
/* .local v2, "width":I */
v3 = this.mDefaultRage;
/* if-lez v4, :cond_0 */
/* .line 86 */
/* int-to-float v4, v4 */
/* iget v5, v0, Landroid/util/DisplayMetrics;->density:F */
/* mul-float/2addr v4, v5 */
/* float-to-int v4, v4 */
} // :cond_0
v4 = java.lang.Math .min ( v2,v1 );
/* .line 85 */
} // :goto_0
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) v3 ).setWidth ( v4 ); // invoke-virtual {v3, v4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setWidth(I)V
/* .line 87 */
v3 = this.mDefaultRage;
/* iget v4, v0, Landroid/util/DisplayMetrics;->density:F */
/* const/high16 v5, 0x43480000 # 200.0f */
/* mul-float/2addr v4, v5 */
/* float-to-int v4, v4 */
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ) v3 ).setHeight ( v4 ); // invoke-virtual {v3, v4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setHeight(I)V
/* .line 88 */
return;
} // .end method
/* # virtual methods */
public final void changeStatus ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus p0 ) {
/* .locals 1 */
/* .param p1, "newStatus" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus; */
/* .line 150 */
this.mStatus = p1;
/* .line 151 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
/* if-ne p1, v0, :cond_0 */
/* .line 152 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).onFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onFail()V
/* .line 153 */
return;
/* .line 155 */
} // :cond_0
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.SUCCESS;
/* if-ne p1, v0, :cond_1 */
/* .line 156 */
v0 = this.mMiuiMultiFingerGestureManager;
(( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).checkSuccess ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->checkSuccess(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
/* .line 158 */
} // :cond_1
return;
} // .end method
protected final void checkFail ( ) {
/* .locals 1 */
/* .line 216 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).changeStatus ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 217 */
return;
} // .end method
protected Boolean checkGestureIsOk ( android.view.MotionEvent p0 ) {
/* .locals 7 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 105 */
(( android.view.MotionEvent ) p1 ).getEventTime ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J
/* move-result-wide v0 */
(( android.view.MotionEvent ) p1 ).getDownTime ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J
/* move-result-wide v2 */
/* sub-long/2addr v0, v2 */
v2 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getFirstAndLastFingerDownTimeDiff ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFirstAndLastFingerDownTimeDiff()I
/* int-to-long v2, v2 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_0 */
/* .line 106 */
int v0 = 0; // const/4 v0, 0x0
/* .line 108 */
} // :cond_0
/* const v0, 0x7f7fffff # Float.MAX_VALUE */
/* .line 109 */
/* .local v0, "minX":F */
/* const v1, 0x7f7fffff # Float.MAX_VALUE */
/* .line 110 */
/* .local v1, "minY":F */
int v2 = 1; // const/4 v2, 0x1
/* .line 111 */
/* .local v2, "maxX":F */
int v3 = 1; // const/4 v3, 0x1
/* .line 112 */
/* .local v3, "maxY":F */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* if-ge v4, v5, :cond_1 */
/* .line 113 */
v5 = (( android.view.MotionEvent ) p1 ).getX ( v4 ); // invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F
/* .line 114 */
/* .local v5, "x":F */
v6 = (( android.view.MotionEvent ) p1 ).getY ( v4 ); // invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F
/* .line 115 */
/* .local v6, "y":F */
v0 = java.lang.Math .min ( v0,v5 );
/* .line 116 */
v2 = java.lang.Math .max ( v2,v5 );
/* .line 117 */
v1 = java.lang.Math .min ( v1,v6 );
/* .line 118 */
v3 = java.lang.Math .max ( v3,v6 );
/* .line 112 */
} // .end local v5 # "x":F
} // .end local v6 # "y":F
/* add-int/lit8 v4, v4, 0x1 */
/* .line 120 */
} // .end local v4 # "i":I
} // :cond_1
v4 = /* invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->checkIsInValidRange(FFFF)Z */
} // .end method
protected final void checkSuccess ( ) {
/* .locals 1 */
/* .line 212 */
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.SUCCESS;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).changeStatus ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 213 */
return;
} // .end method
protected Integer getFirstAndLastFingerDownTimeDiff ( ) {
/* .locals 1 */
/* .line 231 */
/* const/16 v0, 0x118 */
} // .end method
public Integer getFunctionNeedFingerNum ( ) {
/* .locals 1 */
/* .line 177 */
int v0 = 3; // const/4 v0, 0x3
} // .end method
public final java.lang.String getGestureFunction ( ) {
/* .locals 1 */
/* .line 161 */
v0 = this.mFunction;
} // .end method
public abstract java.lang.String getGestureKey ( ) {
} // .end method
public final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus getStatus ( ) {
/* .locals 1 */
/* .line 143 */
v0 = this.mStatus;
} // .end method
protected java.util.List getValidRange ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 136 */
v0 = this.mDefaultRageList;
} // .end method
public void initGesture ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 198 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->hasEventFromShoulderKey(Landroid/view/MotionEvent;)Z */
/* if-nez v0, :cond_2 */
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).checkGestureIsOk ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->checkGestureIsOk(Landroid/view/MotionEvent;)Z
/* if-nez v0, :cond_0 */
/* .line 203 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I
/* if-ge v0, v1, :cond_1 */
/* .line 204 */
v1 = this.mInitX;
v2 = (( android.view.MotionEvent ) p1 ).getX ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F
/* aput v2, v1, v0 */
/* .line 205 */
v1 = this.mInitY;
v2 = (( android.view.MotionEvent ) p1 ).getY ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F
/* aput v2, v1, v0 */
/* .line 203 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 208 */
} // .end local v0 # "i":I
} // :cond_1
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.DETECTING;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).changeStatus ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 209 */
return;
/* .line 199 */
} // :cond_2
} // :goto_1
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.FAIL;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p0 ).changeStatus ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 200 */
return;
} // .end method
public void onConfigChange ( ) {
/* .locals 0 */
/* .line 235 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->updateRage()V */
/* .line 236 */
return;
} // .end method
protected void onFail ( ) {
/* .locals 0 */
/* .line 225 */
return;
} // .end method
public abstract void onTouchEvent ( android.view.MotionEvent p0 ) {
} // .end method
public Boolean preCondition ( ) {
/* .locals 1 */
/* .line 239 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public final void setGestureFunction ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 170 */
this.mFunction = p1;
/* .line 171 */
return;
} // .end method
