public class com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture extends com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture {
	 /* .source "MiuiThreeFingerDownGesture.java" */
	 /* # static fields */
	 private static final Integer HORIZONTAL_THRESHOLD;
	 /* # instance fields */
	 private Float mThreshold;
	 /* # direct methods */
	 public com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "manager" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
		 /* .line 29 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
		 /* .line 30 */
		 /* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->updateConfig()V */
		 /* .line 31 */
		 return;
	 } // .end method
	 private void handleEvent ( android.view.MotionEvent p0 ) {
		 /* .locals 5 */
		 /* .param p1, "event" # Landroid/view/MotionEvent; */
		 /* .line 44 */
		 v0 = 		 (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
		 int v1 = 2; // const/4 v1, 0x2
		 /* if-eq v0, v1, :cond_0 */
		 /* .line 45 */
		 return;
		 /* .line 47 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 48 */
	 /* .local v0, "distanceX":F */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 49 */
	 /* .local v1, "distanceY":F */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .local v2, "i":I */
} // :goto_0
v3 = (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->getFunctionNeedFingerNum()I
/* if-ge v2, v3, :cond_1 */
/* .line 50 */
v3 = (( android.view.MotionEvent ) p1 ).getY ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F
v4 = this.mInitY;
/* aget v4, v4, v2 */
/* sub-float/2addr v3, v4 */
/* add-float/2addr v1, v3 */
/* .line 51 */
v3 = (( android.view.MotionEvent ) p1 ).getX ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F
v4 = this.mInitX;
/* aget v4, v4, v2 */
/* sub-float/2addr v3, v4 */
v3 = java.lang.Math .abs ( v3 );
/* add-float/2addr v0, v3 */
/* .line 49 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 53 */
} // .end local v2 # "i":I
} // :cond_1
/* iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F */
/* neg-float v2, v2 */
/* cmpg-float v2, v1, v2 */
final String v3 = "Gesture fail, because "; // const-string v3, "Gesture fail, because "
/* if-gez v2, :cond_2 */
/* .line 54 */
v2 = this.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = " < "; // const-string v4, " < "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F */
/* neg-float v4, v4 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 55 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture ) p0 ).checkFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->checkFail()V
/* .line 56 */
return;
/* .line 58 */
} // :cond_2
/* sget-boolean v2, Lmiui/os/Build;->IS_TABLET:Z */
if ( v2 != null) { // if-eqz v2, :cond_3
/* const/high16 v2, 0x41c00000 # 24.0f */
/* cmpl-float v2, v0, v2 */
/* if-lez v2, :cond_3 */
/* cmpl-float v2, v0, v1 */
/* if-lez v2, :cond_3 */
/* .line 59 */
v2 = this.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v3 ); // invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v4 = " > "; // const-string v4, " > "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 60 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture ) p0 ).checkFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->checkFail()V
/* .line 61 */
return;
/* .line 63 */
} // :cond_3
/* iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F */
/* cmpl-float v2, v1, v2 */
/* if-ltz v2, :cond_4 */
/* .line 64 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture ) p0 ).checkSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->checkSuccess()V
/* .line 66 */
} // :cond_4
return;
} // .end method
private void updateConfig ( ) {
/* .locals 2 */
/* .line 75 */
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerDownGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->getFunctionNeedFingerNum()I
/* int-to-float v0, v0 */
v1 = this.mContext;
/* .line 76 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getDisplayMetrics ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v1, v1, Landroid/util/DisplayMetrics;->density:F */
/* mul-float/2addr v0, v1 */
/* const/high16 v1, 0x42480000 # 50.0f */
/* mul-float/2addr v0, v1 */
/* .line 77 */
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_0
/* int-to-float v1, v1 */
/* mul-float/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->mThreshold:F */
/* .line 78 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getGestureKey ( ) {
/* .locals 1 */
/* .line 40 */
/* const-string/jumbo v0, "three_gesture_down" */
} // .end method
public void onConfigChange ( ) {
/* .locals 0 */
/* .line 70 */
/* invoke-super {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onConfigChange()V */
/* .line 71 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->updateConfig()V */
/* .line 72 */
return;
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 0 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 35 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;->handleEvent(Landroid/view/MotionEvent;)V */
/* .line 36 */
return;
} // .end method
