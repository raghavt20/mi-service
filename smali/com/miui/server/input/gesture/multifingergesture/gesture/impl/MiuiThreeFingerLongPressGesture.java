public class com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture extends com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture {
	 /* .source "MiuiThreeFingerLongPressGesture.java" */
	 /* # static fields */
	 private static final Boolean IS_FLIP_DEVICE;
	 private static final Integer THREE_LONG_PRESS_TIME_OUT;
	 private static final Integer XIAOMI_TOUCHPAD_PRODUCT_ID;
	 private static final Integer XIAOMI_TOUCHPAD_VENDOR_ID;
	 /* # instance fields */
	 private Float mMoveThreshold;
	 private final java.lang.Runnable mThreeLongPressRunnable;
	 /* # direct methods */
	 static com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture ( ) {
		 /* .locals 1 */
		 /* .line 25 */
		 v0 = 		 miui.util.MiuiMultiDisplayTypeInfo .isFlipDevice ( );
		 com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture.IS_FLIP_DEVICE = (v0!= 0);
		 return;
	 } // .end method
	 public com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "handler" # Landroid/os/Handler; */
		 /* .param p3, "manager" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
		 /* .line 34 */
		 /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
		 /* .line 26 */
		 /* new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture$$ExternalSyntheticLambda0; */
		 /* invoke-direct {v0, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;)V */
		 this.mThreeLongPressRunnable = v0;
		 /* .line 35 */
		 /* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->updateConfig()V */
		 /* .line 36 */
		 return;
	 } // .end method
	 private void handleEvent ( android.view.MotionEvent p0 ) {
		 /* .locals 5 */
		 /* .param p1, "event" # Landroid/view/MotionEvent; */
		 /* .line 49 */
		 v0 = 		 (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
		 int v1 = 2; // const/4 v1, 0x2
		 /* if-ne v0, v1, :cond_3 */
		 /* .line 50 */
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 51 */
		 /* .local v0, "distanceX":F */
		 int v1 = 0; // const/4 v1, 0x0
		 /* .line 52 */
		 /* .local v1, "distanceY":F */
		 int v2 = 0; // const/4 v2, 0x0
		 /* .local v2, "i":I */
	 } // :goto_0
	 v3 = 	 (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->getFunctionNeedFingerNum()I
	 /* if-ge v2, v3, :cond_0 */
	 /* .line 53 */
	 v3 = 	 (( android.view.MotionEvent ) p1 ).getY ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F
	 v4 = this.mInitY;
	 /* aget v4, v4, v2 */
	 /* sub-float/2addr v3, v4 */
	 v3 = 	 java.lang.Math .abs ( v3 );
	 /* add-float/2addr v1, v3 */
	 /* .line 54 */
	 v3 = 	 (( android.view.MotionEvent ) p1 ).getX ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F
	 v4 = this.mInitX;
	 /* aget v4, v4, v2 */
	 /* sub-float/2addr v3, v4 */
	 v3 = 	 java.lang.Math .abs ( v3 );
	 /* add-float/2addr v0, v3 */
	 /* .line 52 */
	 /* add-int/lit8 v2, v2, 0x1 */
	 /* .line 56 */
} // .end local v2 # "i":I
} // :cond_0
/* iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mMoveThreshold:F */
/* cmpl-float v3, v0, v2 */
/* if-gez v3, :cond_1 */
/* cmpl-float v2, v1, v2 */
/* if-ltz v2, :cond_2 */
/* .line 57 */
} // :cond_1
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture ) p0 ).checkFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->checkFail()V
/* .line 59 */
} // :cond_2
return;
/* .line 61 */
} // .end local v0 # "distanceX":F
} // .end local v1 # "distanceY":F
} // :cond_3
v0 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_4 */
/* .line 62 */
v0 = this.mHandler;
v1 = this.mThreeLongPressRunnable;
/* const-wide/16 v2, 0x258 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 64 */
} // :cond_4
return;
} // .end method
static void lambda$new$0 ( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture p0 ) { //synthethic
/* .locals 0 */
/* .param p0, "rec$" # Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture; */
/* .line 26 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture ) p0 ).checkSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->checkSuccess()V
return;
} // .end method
private void updateConfig ( ) {
/* .locals 2 */
/* .line 72 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v0, v0, Landroid/util/DisplayMetrics;->density:F */
/* const/high16 v1, 0x42480000 # 50.0f */
/* mul-float/2addr v0, v1 */
/* .line 73 */
/* sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :goto_0
/* int-to-float v1, v1 */
/* mul-float/2addr v0, v1 */
/* const v1, 0x3f19999a # 0.6f */
/* mul-float/2addr v0, v1 */
/* iput v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mMoveThreshold:F */
/* .line 74 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getGestureKey ( ) {
/* .locals 1 */
/* .line 45 */
/* const-string/jumbo v0, "three_gesture_long_press" */
} // .end method
public void initGesture ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 96 */
(( android.view.MotionEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;
/* .line 97 */
/* .local v0, "device":Landroid/view/InputDevice; */
/* if-nez v0, :cond_0 */
/* .line 98 */
v1 = this.TAG;
final String v2 = "This motion event device is null"; // const-string v2, "This motion event device is null"
android.util.Slog .d ( v1,v2 );
/* .line 99 */
} // :cond_0
v1 = (( android.view.InputDevice ) v0 ).getVendorId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I
/* const/16 v2, 0x15d9 */
/* if-ne v1, v2, :cond_1 */
/* .line 100 */
v1 = (( android.view.InputDevice ) v0 ).getProductId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v2, 0xa1 */
/* if-ne v1, v2, :cond_1 */
/* .line 101 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerLongPressGesture ) p0 ).checkFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->checkFail()V
/* .line 102 */
return;
/* .line 104 */
} // :cond_1
} // :goto_0
/* invoke-super {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->initGesture(Landroid/view/MotionEvent;)V */
/* .line 105 */
return;
} // .end method
public void onConfigChange ( ) {
/* .locals 0 */
/* .line 78 */
/* invoke-super {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onConfigChange()V */
/* .line 79 */
/* invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->updateConfig()V */
/* .line 80 */
return;
} // .end method
protected void onFail ( ) {
/* .locals 2 */
/* .line 68 */
v0 = this.mHandler;
v1 = this.mThreeLongPressRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 69 */
return;
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 0 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 40 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->handleEvent(Landroid/view/MotionEvent;)V */
/* .line 41 */
return;
} // .end method
public Boolean preCondition ( ) {
/* .locals 2 */
/* .line 85 */
v0 = this.mMiuiMultiFingerGestureManager;
v0 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).isKeyguardActive ( ); // invoke-virtual {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->isKeyguardActive()Z
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 86 */
/* .line 88 */
} // :cond_0
/* sget-boolean v0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->IS_FLIP_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mMiuiMultiFingerGestureManager;
v0 = (( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager ) v0 ).getIsFolded ( ); // invoke-virtual {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->getIsFolded()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 89 */
/* .line 91 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
} // .end method
