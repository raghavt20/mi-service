.class public Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;
.super Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
.source "MiuiThreeFingerLongPressGesture.java"


# static fields
.field private static final IS_FLIP_DEVICE:Z

.field private static final THREE_LONG_PRESS_TIME_OUT:I = 0x258

.field private static final XIAOMI_TOUCHPAD_PRODUCT_ID:I = 0xa1

.field private static final XIAOMI_TOUCHPAD_VENDOR_ID:I = 0x15d9


# instance fields
.field private mMoveThreshold:F

.field private final mThreeLongPressRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    invoke-static {}, Lmiui/util/MiuiMultiDisplayTypeInfo;->isFlipDevice()Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->IS_FLIP_DEVICE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "manager"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    .line 26
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;)V

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mThreeLongPressRunnable:Ljava/lang/Runnable;

    .line 35
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->updateConfig()V

    .line 36
    return-void
.end method

.method private handleEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 49
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 50
    const/4 v0, 0x0

    .line 51
    .local v0, "distanceX":F
    const/4 v1, 0x0

    .line 52
    .local v1, "distanceY":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->getFunctionNeedFingerNum()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 53
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mInitY:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v1, v3

    .line 54
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mInitX:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    add-float/2addr v0, v3

    .line 52
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "i":I
    :cond_0
    iget v2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mMoveThreshold:F

    cmpl-float v3, v0, v2

    if-gez v3, :cond_1

    cmpl-float v2, v1, v2

    if-ltz v2, :cond_2

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->checkFail()V

    .line 59
    :cond_2
    return-void

    .line 61
    .end local v0    # "distanceX":F
    .end local v1    # "distanceY":F
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 62
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mThreeLongPressRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 64
    :cond_4
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;)V
    .locals 0
    .param p0, "rec$"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;

    .line 26
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->checkSuccess()V

    return-void
.end method

.method private updateConfig()V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v0, v1

    .line 73
    sget-boolean v1, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    int-to-float v1, v1

    mul-float/2addr v0, v1

    const v1, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mMoveThreshold:F

    .line 74
    return-void
.end method


# virtual methods
.method public getGestureKey()Ljava/lang/String;
    .locals 1

    .line 45
    const-string/jumbo v0, "three_gesture_long_press"

    return-object v0
.end method

.method public initGesture(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 96
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 97
    .local v0, "device":Landroid/view/InputDevice;
    if-nez v0, :cond_0

    .line 98
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->TAG:Ljava/lang/String;

    const-string v2, "This motion event device is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I

    move-result v1

    const/16 v2, 0x15d9

    if-ne v1, v2, :cond_1

    .line 100
    invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I

    move-result v1

    const/16 v2, 0xa1

    if-ne v1, v2, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->checkFail()V

    .line 102
    return-void

    .line 104
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->initGesture(Landroid/view/MotionEvent;)V

    .line 105
    return-void
.end method

.method public onConfigChange()V
    .locals 0

    .line 78
    invoke-super {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onConfigChange()V

    .line 79
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->updateConfig()V

    .line 80
    return-void
.end method

.method protected onFail()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mThreeLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 69
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 40
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->handleEvent(Landroid/view/MotionEvent;)V

    .line 41
    return-void
.end method

.method public preCondition()Z
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mMiuiMultiFingerGestureManager:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->isKeyguardActive()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 86
    return v1

    .line 88
    :cond_0
    sget-boolean v0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->IS_FLIP_DEVICE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;->mMiuiMultiFingerGestureManager:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->getIsFolded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    return v1

    .line 91
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
