public class com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture extends com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture {
	 /* .source "MiuiThreeFingerHorizontalGesture.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "MiuiThreeFingerHorizontalLTRGesture" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture ( ) {
/* .locals 0 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .param p3, "manager" # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
/* .line 96 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V */
/* .line 97 */
return;
} // .end method
/* # virtual methods */
public java.lang.String getGestureKey ( ) {
/* .locals 1 */
/* .line 101 */
/* const-string/jumbo v0, "three_gesture_horizontal_ltr" */
} // .end method
protected void handleEvent ( android.view.MotionEvent p0 ) {
/* .locals 7 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 106 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_3 */
v0 = (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture ) p0 ).isEventFromTouchScreen ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;->isEventFromTouchScreen(Landroid/view/MotionEvent;)Z
/* if-nez v0, :cond_0 */
/* .line 109 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 110 */
/* .local v0, "distanceX":F */
int v1 = 0; // const/4 v1, 0x0
/* .line 111 */
/* .local v1, "distanceY":F */
int v2 = 0; // const/4 v2, 0x0
/* .line 112 */
/* .local v2, "direction":F */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture ) p0 ).getFunctionNeedFingerNum ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;->getFunctionNeedFingerNum()I
/* if-ge v3, v4, :cond_1 */
/* .line 113 */
v4 = (( android.view.MotionEvent ) p1 ).getX ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F
v5 = this.mInitX;
/* aget v5, v5, v3 */
/* sub-float/2addr v4, v5 */
v4 = java.lang.Math .abs ( v4 );
/* add-float/2addr v0, v4 */
/* .line 114 */
v4 = (( android.view.MotionEvent ) p1 ).getY ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F
v5 = this.mInitY;
/* aget v5, v5, v3 */
/* sub-float/2addr v4, v5 */
v4 = java.lang.Math .abs ( v4 );
/* add-float/2addr v1, v4 */
/* .line 115 */
v4 = (( android.view.MotionEvent ) p1 ).getX ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F
v5 = this.mInitX;
/* aget v5, v5, v3 */
/* sub-float/2addr v4, v5 */
v5 = (( android.view.MotionEvent ) p1 ).getY ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F
v6 = this.mInitY;
/* aget v6, v6, v3 */
/* sub-float/2addr v5, v6 */
/* add-float/2addr v4, v5 */
/* add-float/2addr v2, v4 */
/* .line 112 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 117 */
} // .end local v3 # "i":I
} // :cond_1
/* iget v3, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;->mThreshold:F */
/* cmpl-float v3, v0, v3 */
/* if-ltz v3, :cond_2 */
/* iget v3, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;->mThreshold:F */
/* cmpg-float v3, v1, v3 */
/* if-gtz v3, :cond_2 */
int v3 = 0; // const/4 v3, 0x0
/* cmpl-float v3, v2, v3 */
/* if-lez v3, :cond_2 */
/* .line 118 */
(( com.miui.server.input.gesture.multifingergesture.gesture.impl.MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture ) p0 ).checkSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;->checkSuccess()V
/* .line 120 */
} // :cond_2
return;
/* .line 107 */
} // .end local v0 # "distanceX":F
} // .end local v1 # "distanceY":F
} // .end local v2 # "direction":F
} // :cond_3
} // :goto_1
return;
} // .end method
