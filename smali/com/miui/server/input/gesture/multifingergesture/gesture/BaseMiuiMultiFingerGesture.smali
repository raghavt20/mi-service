.class public abstract Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
.super Ljava/lang/Object;
.source "BaseMiuiMultiFingerGesture.java"


# static fields
.field private static final DEFAULT_FINGER_NUM:I = 0x3

.field private static final DEFAULT_FIRST_AND_LAST_FINGER_DOWN_TIME_DIFF:I = 0x118

.field private static final DEFAULT_MAX_HEIGHT_DIFF:I = 0xc8

.field private static final FIXED_WIDTH:I


# instance fields
.field protected final TAG:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field protected mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

.field private mDefaultRageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;",
            ">;"
        }
    .end annotation
.end field

.field protected mFunction:Ljava/lang/String;

.field protected mHandler:Landroid/os/Handler;

.field protected mInitX:[F

.field protected mInitY:[F

.field protected final mMiuiMultiFingerGestureManager:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

.field private mStatus:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    const-string/jumbo v0, "support_fix_width_device"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getInteger(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->FIXED_WIDTH:I

    .line 33
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "manager"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->TAG:Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mInitX:[F

    .line 49
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mInitY:[F

    .line 65
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mContext:Landroid/content/Context;

    .line 66
    iput-object p2, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mHandler:Landroid/os/Handler;

    .line 67
    iput-object p3, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mMiuiMultiFingerGestureManager:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 68
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->initDefaultRage()V

    .line 69
    return-void
.end method

.method private checkIsInValidRange(FFFF)Z
    .locals 3
    .param p1, "minX"    # F
    .param p2, "minY"    # F
    .param p3, "maxX"    # F
    .param p4, "maxY"    # F

    .line 124
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getValidRange()Ljava/util/List;

    move-result-object v0

    .line 125
    .local v0, "validRange":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;>;"
    if-nez v0, :cond_0

    .line 126
    const/4 v1, 0x0

    return v1

    .line 128
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture$$ExternalSyntheticLambda0;

    invoke-direct {v2, p3, p1, p4, p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture$$ExternalSyntheticLambda0;-><init>(FFFF)V

    invoke-interface {v1, v2}, Ljava/util/stream/Stream;->anyMatch(Ljava/util/function/Predicate;)Z

    move-result v1

    return v1
.end method

.method private hasEventFromShoulderKey(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 184
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 185
    const/16 v1, 0x20

    invoke-virtual {p1, v1, v0}, Landroid/view/MotionEvent;->getAxisValue(II)F

    move-result v1

    const/high16 v2, 0x41800000    # 16.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 186
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Gesture "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " init fail, because pointer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from shoulder key."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    const/4 v1, 0x1

    return v1

    .line 184
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private initDefaultRage()V
    .locals 1

    .line 72
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    invoke-direct {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    .line 73
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mDefaultRageList:Ljava/util/List;

    .line 74
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->updateRage()V

    .line 75
    return-void
.end method

.method static synthetic lambda$checkIsInValidRange$0(FFFFLcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;)Z
    .locals 2
    .param p0, "maxX"    # F
    .param p1, "minX"    # F
    .param p2, "maxY"    # F
    .param p3, "minY"    # F
    .param p4, "rect"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    .line 128
    sub-float v0, p0, p1

    invoke-virtual {p4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    sub-float v0, p2, p3

    .line 129
    invoke-virtual {p4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->getHeight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 128
    :goto_0
    return v0
.end method

.method private updateRage()V
    .locals 6

    .line 78
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 79
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 80
    .local v1, "height":I
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 85
    .local v2, "width":I
    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    sget v4, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->FIXED_WIDTH:I

    if-lez v4, :cond_0

    .line 86
    int-to-float v4, v4

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    goto :goto_0

    :cond_0
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 85
    :goto_0
    invoke-virtual {v3, v4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setWidth(I)V

    .line 87
    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mDefaultRage:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x43480000    # 200.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->setHeight(I)V

    .line 88
    return-void
.end method


# virtual methods
.method public final changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
    .locals 1
    .param p1, "newStatus"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 150
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mStatus:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 151
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne p1, v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onFail()V

    .line 153
    return-void

    .line 155
    :cond_0
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->SUCCESS:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne p1, v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mMiuiMultiFingerGestureManager:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v0, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->checkSuccess(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 158
    :cond_1
    return-void
.end method

.method protected final checkFail()V
    .locals 1

    .line 216
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 217
    return-void
.end method

.method protected checkGestureIsOk(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFirstAndLastFingerDownTimeDiff()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    return v0

    .line 108
    :cond_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 109
    .local v0, "minX":F
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 110
    .local v1, "minY":F
    const/4 v2, 0x1

    .line 111
    .local v2, "maxX":F
    const/4 v3, 0x1

    .line 112
    .local v3, "maxY":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 113
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    .line 114
    .local v5, "x":F
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 115
    .local v6, "y":F
    invoke-static {v0, v5}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 116
    invoke-static {v2, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 117
    invoke-static {v1, v6}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 118
    invoke-static {v3, v6}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 112
    .end local v5    # "x":F
    .end local v6    # "y":F
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 120
    .end local v4    # "i":I
    :cond_1
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->checkIsInValidRange(FFFF)Z

    move-result v4

    return v4
.end method

.method protected final checkSuccess()V
    .locals 1

    .line 212
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->SUCCESS:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 213
    return-void
.end method

.method protected getFirstAndLastFingerDownTimeDiff()I
    .locals 1

    .line 231
    const/16 v0, 0x118

    return v0
.end method

.method public getFunctionNeedFingerNum()I
    .locals 1

    .line 177
    const/4 v0, 0x3

    return v0
.end method

.method public final getGestureFunction()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mFunction:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getGestureKey()Ljava/lang/String;
.end method

.method public final getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mStatus:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    return-object v0
.end method

.method protected getValidRange()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mDefaultRageList:Ljava/util/List;

    return-object v0
.end method

.method public initGesture(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 198
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->hasEventFromShoulderKey(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->checkGestureIsOk(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 203
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 204
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mInitX:[F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    aput v2, v1, v0

    .line 205
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mInitY:[F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    aput v2, v1, v0

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    .end local v0    # "i":I
    :cond_1
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->DETECTING:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 209
    return-void

    .line 199
    :cond_2
    :goto_1
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 200
    return-void
.end method

.method public onConfigChange()V
    .locals 0

    .line 235
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->updateRage()V

    .line 236
    return-void
.end method

.method protected onFail()V
    .locals 0

    .line 225
    return-void
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)V
.end method

.method public preCondition()Z
    .locals 1

    .line 239
    const/4 v0, 0x1

    return v0
.end method

.method public final setGestureFunction(Ljava/lang/String;)V
    .locals 0
    .param p1, "function"    # Ljava/lang/String;

    .line 170
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->mFunction:Ljava/lang/String;

    .line 171
    return-void
.end method
