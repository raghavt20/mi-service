class com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiMultiFingerGestureManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$8STm1bP2blkaKObIlnEyDqYzHFI ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->lambda$onChange$2(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
return;
} // .end method
public static void $r8$lambda$8bXBvrIyfY21aI8moxsOYozNG2M ( com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver p0, android.content.ContentResolver p1, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->lambda$observe$0(Landroid/content/ContentResolver;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
return;
} // .end method
 com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 377 */
this.this$0 = p1;
/* .line 378 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 379 */
return;
} // .end method
private void lambda$observe$0 ( android.content.ContentResolver p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) { //synthethic
/* .locals 3 */
/* .param p1, "resolver" # Landroid/content/ContentResolver; */
/* .param p2, "baseMiuiMultiFingerGesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 388 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p2 ).getGestureKey ( ); // invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
android.provider.Settings$System .getUriFor ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = -1; // const/4 v2, -0x1
(( android.content.ContentResolver ) p1 ).registerContentObserver ( v0, v1, p0, v2 ); // invoke-virtual {p1, v0, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
return;
} // .end method
static Boolean lambda$onChange$1 ( android.net.Uri p0, com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "uri" # Landroid/net/Uri; */
/* .param p1, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 408 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureKey ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
android.provider.Settings$System .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p0 ); // invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
} // .end method
private void lambda$onChange$2 ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0 ) { //synthethic
/* .locals 3 */
/* .param p1, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 411 */
v0 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmContext ( v0 );
/* .line 412 */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureKey ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
/* .line 411 */
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
/* .line 413 */
/* .local v0, "function":Ljava/lang/String; */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).setGestureFunction ( v0 ); // invoke-virtual {p1, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->setGestureFunction(Ljava/lang/String;)V
/* .line 414 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureKey ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = " :"; // const-string v2, " :"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiMultiFingerGestureManager"; // const-string v2, "MiuiMultiFingerGestureManager"
android.util.Slog .d ( v2,v1 );
/* .line 415 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->updateGestures(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V */
/* .line 416 */
return;
} // .end method
private void updateGestures ( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture p0 ) {
/* .locals 5 */
/* .param p1, "gesture" # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 420 */
v0 = this.this$0;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).getGestureFunction ( ); // invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$misEmpty ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 421 */
	 v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.NONE;
} // :cond_0
v0 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureStatus.READY;
/* .line 420 */
} // :goto_0
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) p1 ).changeStatus ( v0 ); // invoke-virtual {p1, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V
/* .line 422 */
int v0 = 0; // const/4 v0, 0x0
/* .line 423 */
/* .local v0, "num":B */
v1 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmAllMultiFingerGestureList ( v1 );
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
/* .line 424 */
/* .local v2, "baseMiuiMultiFingerGesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture; */
v3 = this.this$0;
(( com.miui.server.input.gesture.multifingergesture.gesture.BaseMiuiMultiFingerGesture ) v2 ).getGestureFunction ( ); // invoke-virtual {v2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;
v3 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$misEmpty ( v3,v4 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 425 */
/* .line 427 */
} // :cond_1
/* add-int/lit8 v3, v0, 0x1 */
/* int-to-byte v0, v3 */
/* .line 428 */
} // .end local v2 # "baseMiuiMultiFingerGesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
/* .line 429 */
} // :cond_2
v1 = this.this$0;
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmIsRegister ( v1 );
final String v2 = "MiuiMultiFingerGestureManager"; // const-string v2, "MiuiMultiFingerGestureManager"
/* if-nez v1, :cond_3 */
/* if-lez v0, :cond_3 */
/* .line 431 */
final String v1 = "The gesture has been add,register pointer event listener."; // const-string v1, "The gesture has been add,register pointer event listener."
android.util.Slog .d ( v2,v1 );
/* .line 432 */
v1 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmMiuiGestureMonitor ( v1 );
v2 = this.this$0;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v1 ).registerPointerEventListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 433 */
v1 = this.this$0;
int v2 = 1; // const/4 v2, 0x1
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fputmIsRegister ( v1,v2 );
/* .line 434 */
return;
/* .line 436 */
} // :cond_3
v1 = this.this$0;
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmIsRegister ( v1 );
if ( v1 != null) { // if-eqz v1, :cond_4
/* if-nez v0, :cond_4 */
/* .line 438 */
final String v1 = "The gestures has been all removed, unregister pointer event listener."; // const-string v1, "The gestures has been all removed, unregister pointer event listener."
android.util.Slog .d ( v2,v1 );
/* .line 439 */
v1 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmMiuiGestureMonitor ( v1 );
v2 = this.this$0;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v1 ).unregisterPointerEventListener ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 440 */
v1 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fputmIsRegister ( v1,v2 );
/* .line 442 */
} // :cond_4
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 382 */
v0 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 383 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "enable_three_gesture"; // const-string v1, "enable_three_gesture"
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 385 */
final String v1 = "device_provisioned"; // const-string v1, "device_provisioned"
android.provider.Settings$Global .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 387 */
v1 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmAllMultiFingerGestureList ( v1 );
/* new-instance v2, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;Landroid/content/ContentResolver;)V */
/* .line 390 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 394 */
final String v0 = "enable_three_gesture"; // const-string v0, "enable_three_gesture"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 396 */
v1 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$System .getInt ( v4,v0,v3 );
/* if-ne v0, v3, :cond_0 */
/* move v2, v3 */
} // :cond_0
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fputmEnable ( v1,v2 );
/* .line 399 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "enable_three_gesture_key :"; // const-string v1, "enable_three_gesture_key :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmEnable ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiMultiFingerGestureManager"; // const-string v1, "MiuiMultiFingerGestureManager"
android.util.Slog .d ( v1,v0 );
/* .line 400 */
return;
/* .line 402 */
} // :cond_1
final String v0 = "device_provisioned"; // const-string v0, "device_provisioned"
android.provider.Settings$Global .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 403 */
v1 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v4 ).getContentResolver ( ); // invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$Global .getInt ( v4,v0,v2 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* move v2, v3 */
} // :cond_2
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fputmDeviceProvisioned ( v1,v2 );
/* .line 405 */
return;
/* .line 407 */
} // :cond_3
v0 = this.this$0;
com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureManager .-$$Nest$fgetmAllMultiFingerGestureList ( v0 );
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;-><init>(Landroid/net/Uri;)V */
/* .line 408 */
/* .line 409 */
/* new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;)V */
/* .line 410 */
(( java.util.Optional ) v0 ).ifPresent ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V
/* .line 417 */
return;
} // .end method
