public class com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect {
	 /* .source "MiuiMultiFingerGestureRect.java" */
	 /* # instance fields */
	 private Integer mHeight;
	 private Integer mWidth;
	 /* # direct methods */
	 public com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ( ) {
		 /* .locals 0 */
		 /* .line 7 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 8 */
		 return;
	 } // .end method
	 public com.miui.server.input.gesture.multifingergesture.MiuiMultiFingerGestureRect ( ) {
		 /* .locals 0 */
		 /* .param p1, "width" # I */
		 /* .param p2, "height" # I */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 11 */
		 /* iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mWidth:I */
		 /* .line 12 */
		 /* iput p2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mHeight:I */
		 /* .line 13 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getHeight ( ) {
		 /* .locals 1 */
		 /* .line 24 */
		 /* iget v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mHeight:I */
	 } // .end method
	 public Integer getWidth ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 /* iget v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mWidth:I */
	 } // .end method
	 public void setHeight ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "height" # I */
		 /* .line 28 */
		 /* iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mHeight:I */
		 /* .line 29 */
		 return;
	 } // .end method
	 public void setWidth ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "width" # I */
		 /* .line 20 */
		 /* iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureRect;->mWidth:I */
		 /* .line 21 */
		 return;
	 } // .end method
