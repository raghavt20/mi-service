.class Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiMultiFingerGestureManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;


# direct methods
.method public static synthetic $r8$lambda$8STm1bP2blkaKObIlnEyDqYzHFI(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->lambda$onChange$2(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    return-void
.end method

.method public static synthetic $r8$lambda$8bXBvrIyfY21aI8moxsOYozNG2M(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;Landroid/content/ContentResolver;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->lambda$observe$0(Landroid/content/ContentResolver;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    return-void
.end method

.method constructor <init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 377
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    .line 378
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 379
    return-void
.end method

.method private synthetic lambda$observe$0(Landroid/content/ContentResolver;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 3
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "baseMiuiMultiFingerGesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 388
    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v1, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method static synthetic lambda$onChange$1(Landroid/net/Uri;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)Z
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 408
    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private synthetic lambda$onChange$2(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 3
    .param p1, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 411
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Landroid/content/Context;

    move-result-object v0

    .line 412
    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v1

    .line 411
    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    .local v0, "function":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->setGestureFunction(Ljava/lang/String;)V

    .line 414
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiMultiFingerGestureManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->updateGestures(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 416
    return-void
.end method

.method private updateGestures(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 5
    .param p1, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 420
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$misEmpty(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->READY:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    .line 420
    :goto_0
    invoke-virtual {p1, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 422
    const/4 v0, 0x0

    .line 423
    .local v0, "num":B
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmAllMultiFingerGestureList(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 424
    .local v2, "baseMiuiMultiFingerGesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$misEmpty(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 425
    goto :goto_1

    .line 427
    :cond_1
    add-int/lit8 v3, v0, 0x1

    int-to-byte v0, v3

    .line 428
    .end local v2    # "baseMiuiMultiFingerGesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
    goto :goto_1

    .line 429
    :cond_2
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmIsRegister(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Z

    move-result v1

    const-string v2, "MiuiMultiFingerGestureManager"

    if-nez v1, :cond_3

    if-lez v0, :cond_3

    .line 431
    const-string v1, "The gesture has been add,register pointer event listener."

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmMiuiGestureMonitor(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 433
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fputmIsRegister(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V

    .line 434
    return-void

    .line 436
    :cond_3
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmIsRegister(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_4

    .line 438
    const-string v1, "The gestures has been all removed, unregister pointer event listener."

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmMiuiGestureMonitor(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-virtual {v1, v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 440
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fputmIsRegister(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V

    .line 442
    :cond_4
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 382
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 383
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "enable_three_gesture"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 385
    const-string v1, "device_provisioned"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 387
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmAllMultiFingerGestureList(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0, v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;Landroid/content/ContentResolver;)V

    invoke-interface {v1, v2}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 390
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 394
    const-string v0, "enable_three_gesture"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_0

    move v2, v3

    :cond_0
    invoke-static {v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fputmEnable(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enable_three_gesture_key :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmEnable(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiFingerGestureManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    return-void

    .line 402
    :cond_1
    const-string v0, "device_provisioned"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 403
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    move v2, v3

    :cond_2
    invoke-static {v1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fputmDeviceProvisioned(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V

    .line 405
    return-void

    .line 407
    :cond_3
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;

    invoke-static {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->-$$Nest$fgetmAllMultiFingerGestureList(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;

    invoke-direct {v1, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;-><init>(Landroid/net/Uri;)V

    .line 408
    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 409
    invoke-interface {v0}, Ljava/util/stream/Stream;->findFirst()Ljava/util/Optional;

    move-result-object v0

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;)V

    .line 410
    invoke-virtual {v0, v1}, Ljava/util/Optional;->ifPresent(Ljava/util/function/Consumer;)V

    .line 417
    return-void
.end method
