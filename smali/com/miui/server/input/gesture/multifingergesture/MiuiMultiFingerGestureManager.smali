.class public Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;
.super Ljava/lang/Object;
.source "MiuiMultiFingerGestureManager.java"

# interfaces
.implements Lcom/miui/server/input/gesture/MiuiGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field private static final DISABLE_GESTURE_APPS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "MiuiMultiFingerGestureManager"


# instance fields
.field private final mAllMultiFingerGestureList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;",
            ">;"
        }
    .end annotation
.end field

.field private mBootCompleted:Z

.field private final mContext:Landroid/content/Context;

.field private mContinueSendEvent:Z

.field private mCurrentFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

.field private mCurrentUserId:I

.field private mDeviceProvisioned:Z

.field private mEnable:Z

.field private mGetKeyguardActiveFunction:Ljava/util/function/BooleanSupplier;

.field private final mHandler:Landroid/os/Handler;

.field private mIsFolded:Z

.field private mIsRegister:Z

.field private mIsScreenOn:Z

.field private final mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

.field private final mMiuiSettingsObserver:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;

.field private mNeedCancel:Z

.field private final mNotNeedHapticFeedbackFunction:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$dcEVTpx3eF5CghUa_RZEYGeX3KI(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->lambda$updateSettings$2(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmAllMultiFingerGestureList(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmEnable(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsRegister(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsRegister:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiGestureMonitor(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDeviceProvisioned(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mDeviceProvisioned:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmEnable(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsRegister(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsRegister:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$misEmpty(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->isEmpty(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$monConfigChange(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->onConfigChange()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 43
    const-string v0, "com.android.cts.verifier"

    invoke-static {v0}, Ljava/util/Set;->of(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->DISABLE_GESTURE_APPS:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mHandler"    # Landroid/os/Handler;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNotNeedHapticFeedbackFunction:Ljava/util/Set;

    .line 91
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    .line 92
    iput-object p2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mHandler:Landroid/os/Handler;

    .line 93
    invoke-static {p1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    .line 95
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initAllGesture()V

    .line 96
    new-instance v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;

    invoke-direct {v0, p0, p2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiSettingsObserver:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;

    .line 97
    invoke-virtual {v0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->observe()V

    .line 98
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateSettings()V

    .line 99
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->registerOnConfigBroadcast()V

    .line 100
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initNotNeedHapticFeedbackFunction()V

    .line 101
    return-void
.end method

.method private canPerformGesture(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 187
    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mDeviceProvisioned:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsScreenOn:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    .line 188
    .local v0, "isGestureAvailable":Z
    :goto_0
    if-nez v0, :cond_1

    .line 189
    return v2

    .line 191
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 192
    return v2

    .line 195
    :cond_2
    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mCurrentFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-nez v2, :cond_3

    .line 196
    return v1

    .line 199
    :cond_3
    sget-object v3, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->DISABLE_GESTURE_APPS:Ljava/util/Set;

    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v1, v2

    return v1
.end method

.method private checkBootCompleted()Z
    .locals 2

    .line 278
    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mBootCompleted:Z

    if-nez v0, :cond_0

    .line 279
    const-string/jumbo v0, "sys.boot_completed"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mBootCompleted:Z

    .line 280
    return v0

    .line 282
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private gestureStatusIsDetecting(Landroid/view/MotionEvent;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 250
    invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onTouchEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 253
    :cond_0
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p2, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 255
    :goto_0
    return-void
.end method

.method private gestureStatusIsReady(Landroid/view/MotionEvent;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getFunctionNeedFingerNum()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 260
    return-void

    .line 262
    :cond_0
    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->preCondition()Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p2, v0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " init fail, because pre condition."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiMultiFingerGestureManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    return-void

    .line 268
    :cond_1
    invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->initGesture(Landroid/view/MotionEvent;)V

    .line 269
    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v0

    sget-object v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->DETECTING:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne v0, v1, :cond_2

    .line 271
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->pilferPointers()V

    .line 273
    invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 275
    :cond_2
    return-void
.end method

.method private initAllGesture()V
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerDownGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerLongPressGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/animation/DeviceHelper;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/animation/DeviceHelper;->isFoldDevice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalLTRGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalRTLGesture;

    iget-object v2, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/impl/MiuiThreeFingerHorizontalGesture$MiuiThreeFingerHorizontalRTLGesture;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_1
    return-void
.end method

.method private initAllGestureStatus()V
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda3;

    invoke-direct {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda3;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 159
    return-void
.end method

.method private initNotNeedHapticFeedbackFunction()V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNotNeedHapticFeedbackFunction:Ljava/util/Set;

    const-string/jumbo v1, "screen_shot"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNotNeedHapticFeedbackFunction:Ljava/util/Set;

    const-string v1, "partial_screen_shot"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNotNeedHapticFeedbackFunction:Ljava/util/Set;

    const-string v1, "dump_log"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method private isEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "function"    # Ljava/lang/String;

    .line 326
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method static synthetic lambda$dump$3(Ljava/io/PrintWriter;Ljava/lang/String;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 2
    .param p0, "pw"    # Ljava/io/PrintWriter;
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 348
    invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gestureName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gestureKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p0, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gestureFunction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Ljava/io/PrintWriter;->println()V

    .line 355
    return-void
.end method

.method static synthetic lambda$initAllGestureStatus$1(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 2
    .param p0, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 153
    invoke-virtual {p0}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v0

    .line 154
    .local v0, "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    sget-object v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne v0, v1, :cond_0

    .line 155
    return-void

    .line 157
    :cond_0
    sget-object v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->READY:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-virtual {p0, v1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 158
    return-void
.end method

.method static synthetic lambda$updateAllGestureStatus$0(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 2
    .param p0, "except"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
    .param p1, "newStatus"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    .param p2, "multiFingerGesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 139
    if-ne p2, p0, :cond_0

    .line 140
    return-void

    .line 142
    :cond_0
    invoke-virtual {p2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v0

    .line 143
    .local v0, "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    if-eq v0, p1, :cond_2

    sget-object v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 147
    :cond_1
    invoke-virtual {p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->changeStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    .line 148
    return-void

    .line 145
    :cond_2
    :goto_0
    return-void
.end method

.method private synthetic lambda$updateSettings$2(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 3
    .param p1, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 340
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiSettingsObserver:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;

    .line 341
    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 340
    return-void
.end method

.method private onConfigChange()V
    .locals 2

    .line 364
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda2;

    invoke-direct {v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda2;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 365
    return-void
.end method

.method private pilferPointers()V
    .locals 2

    .line 292
    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNeedCancel:Z

    if-nez v0, :cond_0

    .line 293
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    invoke-virtual {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->pilferPointers()V

    .line 296
    const-string v0, "MiuiMultiFingerGestureManager"

    const-string v1, "Pilfer pointers because gesture detected"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNeedCancel:Z

    .line 298
    return-void
.end method

.method private registerOnConfigBroadcast()V
    .locals 4

    .line 110
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$1;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 116
    return-void
.end method

.method private sendEventToGestures(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "count":I
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNeedCancel:Z

    .line 205
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 206
    .local v2, "gesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
    invoke-virtual {v2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v3

    .line 207
    .local v3, "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    sget-object v4, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$2;->$SwitchMap$com$miui$server$input$gesture$multifingergesture$MiuiMultiFingerGestureStatus:[I

    invoke-virtual {v3}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 212
    :pswitch_0
    invoke-direct {p0, p1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->gestureStatusIsDetecting(Landroid/view/MotionEvent;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 213
    goto :goto_1

    .line 209
    :pswitch_1
    invoke-direct {p0, p1, v2}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->gestureStatusIsReady(Landroid/view/MotionEvent;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 210
    nop

    .line 219
    :goto_1
    invoke-virtual {v2}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getStatus()Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    move-result-object v3

    .line 220
    sget-object v4, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->SUCCESS:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne v3, v4, :cond_0

    .line 221
    return-void

    .line 223
    :cond_0
    sget-object v4, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-eq v3, v4, :cond_1

    sget-object v4, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->NONE:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    if-ne v3, v4, :cond_2

    .line 225
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 227
    .end local v2    # "gesture":Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;
    .end local v3    # "status":Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    :cond_2
    goto :goto_0

    .line 228
    :cond_3
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 229
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z

    .line 231
    :cond_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private triggerFunction(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 6
    .param p1, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 286
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mNotNeedHapticFeedbackFunction:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 287
    .local v0, "notNeedHapticFeedback":Z
    iget-object v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureFunction()Ljava/lang/String;

    move-result-object v2

    .line 288
    invoke-virtual {p1}, Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;->getGestureKey()Ljava/lang/String;

    move-result-object v3

    .line 287
    xor-int/lit8 v4, v0, 0x1

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v5, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 289
    return-void
.end method

.method private updateAllGestureStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 2
    .param p1, "newStatus"    # Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;
    .param p2, "except"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 138
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;)V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 149
    return-void
.end method

.method private updateSettings()V
    .locals 3

    .line 331
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "enable_three_gesture"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 333
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiSettingsObserver:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;

    if-nez v0, :cond_0

    .line 334
    return-void

    .line 336
    :cond_0
    nop

    .line 337
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 336
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 338
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mMiuiSettingsObserver:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;

    .line 339
    const-string v1, "device_provisioned"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 338
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 340
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda4;-><init>(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;)V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 342
    return-void
.end method


# virtual methods
.method public checkSuccess(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V
    .locals 1
    .param p1, "gesture"    # Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z

    .line 241
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    invoke-direct {p0, v0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateAllGestureStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 242
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->checkBootCompleted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    return-void

    .line 245
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->triggerFunction(Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 246
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 345
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 346
    const-string v0, "MiuiMultiFingerGestureManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mAllMultiFingerGestureList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p2, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager$$ExternalSyntheticLambda1;-><init>(Ljava/io/PrintWriter;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mBootCompleted = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mBootCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mDeviceProvisioned = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mDeviceProvisioned:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mIsScreenOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsScreenOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mEnable = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mIsFolded = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsFolded:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 361
    return-void
.end method

.method public getIsFolded()Z
    .locals 1

    .line 87
    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsFolded:Z

    return v0
.end method

.method public initKeyguardActiveFunction(Ljava/util/function/BooleanSupplier;)V
    .locals 0
    .param p1, "getKeyguardActiveFunction"    # Ljava/util/function/BooleanSupplier;

    .line 312
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mGetKeyguardActiveFunction:Ljava/util/function/BooleanSupplier;

    .line 313
    return-void
.end method

.method public isKeyguardActive()Z
    .locals 1

    .line 319
    iget-object v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mGetKeyguardActiveFunction:Ljava/util/function/BooleanSupplier;

    if-nez v0, :cond_0

    .line 320
    const/4 v0, 0x0

    return v0

    .line 322
    :cond_0
    invoke-interface {v0}, Ljava/util/function/BooleanSupplier;->getAsBoolean()Z

    move-result v0

    return v0
.end method

.method public notifyFoldStatus(Z)V
    .locals 0
    .param p1, "folded"    # Z

    .line 372
    iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsFolded:Z

    .line 373
    return-void
.end method

.method public onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
    .locals 0
    .param p1, "newFocus"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 368
    iput-object p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mCurrentFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 369
    return-void
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 173
    :sswitch_0
    const-string v0, "MiuiMultiFingerGestureManager"

    const-string v1, "Receive a cancel event, all gesture will fail."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    sget-object v0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;->FAIL:Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateAllGestureStatus(Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureStatus;Lcom/miui/server/input/gesture/multifingergesture/gesture/BaseMiuiMultiFingerGesture;)V

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z

    .line 176
    return-void

    .line 167
    :sswitch_1
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->canPerformGesture(Landroid/view/MotionEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z

    .line 168
    if-eqz v0, :cond_0

    .line 169
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->initAllGestureStatus()V

    .line 179
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mContinueSendEvent:Z

    if-nez v0, :cond_1

    .line 180
    return-void

    .line 182
    :cond_1
    invoke-direct {p0, p1}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->sendEventToGestures(Landroid/view/MotionEvent;)V

    .line 183
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x3 -> :sswitch_0
    .end sparse-switch
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "newUserId"    # I

    .line 305
    iget v0, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mCurrentUserId:I

    if-eq v0, p1, :cond_0

    .line 306
    iput p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mCurrentUserId:I

    .line 307
    invoke-direct {p0}, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->updateSettings()V

    .line 309
    :cond_0
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 0
    .param p1, "screenOn"    # Z

    .line 301
    iput-boolean p1, p0, Lcom/miui/server/input/gesture/multifingergesture/MiuiMultiFingerGestureManager;->mIsScreenOn:Z

    .line 302
    return-void
.end method
