.class public Lcom/miui/server/input/MiuiBackTapGestureService;
.super Ljava/lang/Object;
.source "MiuiBackTapGestureService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/MiuiBackTapGestureService$H;,
        Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;,
        Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;
    }
.end annotation


# static fields
.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final TAG:Ljava/lang/String; = "MiuiBackTapGestureService"


# instance fields
.field private mBackDoubleTapFunction:Ljava/lang/String;

.field private final mBackTapSensorChangeListener:Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;

.field private mBackTapSensorWrapper:Lmiui/util/BackTapSensorWrapper;

.field private mBackTapTouchHelper:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

.field private mBackTripleTapFunction:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mHandler:Landroid/os/Handler;

.field private mIsBackTapSensorListenerRegistered:Z

.field private volatile mIsGameMode:Z

.field private mIsSupportBackTapSensor:Z

.field private mIsUnFolded:Z

.field private mMiuiSettingsObserver:Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;

.field private mScreenOn:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackTapSensorChangeListener(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapSensorChangeListener:Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackTapSensorWrapper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapSensorWrapper:Lmiui/util/BackTapSensorWrapper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackTapTouchHelper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapTouchHelper:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsGameMode(Lcom/miui/server/input/MiuiBackTapGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsUnFolded(Lcom/miui/server/input/MiuiBackTapGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsUnFolded:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenOn(Lcom/miui/server/input/MiuiBackTapGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsGameMode(Lcom/miui/server/input/MiuiBackTapGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckEmpty(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService;->checkEmpty(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mpostShortcutFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/MiuiBackTapGestureService;->postShortcutFunction(Ljava/lang/String;ILjava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateBackTapFeatureState(Lcom/miui/server/input/MiuiBackTapGestureService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapSensorWrapper:Lmiui/util/BackTapSensorWrapper;

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z

    .line 54
    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z

    .line 57
    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z

    .line 59
    const/4 v1, -0x2

    iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mCurrentUserId:I

    .line 60
    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z

    .line 61
    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsUnFolded:Z

    .line 63
    new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$1;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapSensorChangeListener:Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;

    .line 81
    invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService;->init(Landroid/content/Context;)V

    .line 82
    return-void
.end method

.method private backTapSettingsInit()V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    const-string v1, "back_double_tap"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    const-string v1, "back_triple_tap"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    .line 100
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V

    .line 101
    new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;

    .line 102
    invoke-virtual {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->observe()V

    .line 103
    new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapTouchHelper:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    .line 104
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->removeUnsupportedFunction()V

    .line 105
    return-void
.end method

.method private checkEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "feature"    # Ljava/lang/String;

    .line 186
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 189
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 187
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 85
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    .line 86
    const-string/jumbo v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    .line 87
    const v1, 0x1fa2665

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z

    .line 88
    if-eqz v0, :cond_1

    .line 89
    new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$H;

    invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$H;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mHandler:Landroid/os/Handler;

    .line 90
    new-instance v0, Lmiui/util/BackTapSensorWrapper;

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiui/util/BackTapSensorWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapSensorWrapper:Lmiui/util/BackTapSensorWrapper;

    .line 91
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->backTapSettingsInit()V

    .line 93
    :cond_1
    return-void
.end method

.method private postShortcutFunction(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 6
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "delay"    # I
    .param p3, "shortcut"    # Ljava/lang/String;

    .line 193
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const/4 v0, 0x0

    return v0

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 197
    .local v0, "message":Landroid/os/Message;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 198
    .local v2, "bundle":Landroid/os/Bundle;
    const-string/jumbo v3, "shortcut"

    invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-virtual {v0, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 200
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mHandler:Landroid/os/Handler;

    int-to-long v4, p2

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 201
    return v1
.end method

.method private removeUnsupportedFunction()V
    .locals 7

    .line 108
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    const-string/jumbo v1, "turn_on_torch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "launch_ai_shortcut"

    const-string v3, "launch_alipay_health_code"

    const/4 v4, -0x2

    const-string v5, "none"

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    .line 109
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    .line 110
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v6, "back_double_tap"

    invoke-static {v0, v6, v5, v4}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    .line 116
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    .line 117
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "back_triple_tap"

    invoke-static {v0, v1, v5, v4}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 122
    :cond_3
    return-void
.end method

.method private updateBackTapFeatureState()V
    .locals 2

    .line 152
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z

    if-nez v0, :cond_0

    return-void

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/MiuiBackTapGestureService$2;

    invoke-direct {v1, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$2;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 183
    return-void
.end method

.method private updateSettings()V
    .locals 3

    .line 143
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z

    if-nez v0, :cond_0

    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;

    .line 145
    const-string v1, "back_double_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 144
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 146
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;

    .line 147
    const-string v1, "back_triple_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 146
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 148
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->removeUnsupportedFunction()V

    .line 149
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 251
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 252
    const-string v0, "MiuiBackTapGestureService"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 253
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 254
    const-string v0, "mIsSupportBackTapSensor="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 255
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 256
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 257
    const-string v0, "mIsBackTapSensorListenerRegistered="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 258
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 259
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 260
    const-string v0, "mBackDoubleTapFunction="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackDoubleTapFunction:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 263
    const-string v0, "mBackTripleTapFunction="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTripleTapFunction:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 266
    const-string v0, "mIsGameMode="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 267
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 268
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mBackTapTouchHelper:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    if-eqz v0, :cond_0

    .line 269
    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$misDebuggable(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$mdump(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 270
    :cond_0
    return-void
.end method

.method public notifyFoldStatus(Z)V
    .locals 1
    .param p1, "folded"    # Z

    .line 133
    xor-int/lit8 v0, p1, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsUnFolded:Z

    .line 134
    return-void
.end method

.method public notifyScreenOff()V
    .locals 1

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z

    .line 130
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V

    .line 131
    return-void
.end method

.method public notifyScreenOn()V
    .locals 1

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z

    .line 126
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V

    .line 127
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "newUserId"    # I

    .line 136
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mCurrentUserId:I

    if-eq v0, p1, :cond_0

    .line 137
    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mCurrentUserId:I

    .line 138
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateSettings()V

    .line 140
    :cond_0
    return-void
.end method
