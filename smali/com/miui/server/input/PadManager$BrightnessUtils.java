class com.miui.server.input.PadManager$BrightnessUtils {
	 /* .source "PadManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/PadManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BrightnessUtils" */
} // .end annotation
/* # static fields */
private static final Float A;
private static final Float B;
private static final Float C;
private static final Float R;
/* # direct methods */
private com.miui.server.input.PadManager$BrightnessUtils ( ) {
/* .locals 0 */
/* .line 221 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static final Float convertGammaToLinear ( Float p0 ) {
/* .locals 3 */
/* .param p0, "val" # F */
/* .line 237 */
/* const/high16 v0, 0x3f000000 # 0.5f */
/* cmpg-float v1, p0, v0 */
/* if-gtz v1, :cond_0 */
/* .line 238 */
/* div-float v0, p0, v0 */
v0 = android.util.MathUtils .sq ( v0 );
/* .local v0, "ret":F */
/* .line 240 */
} // .end local v0 # "ret":F
} // :cond_0
/* const v0, 0x3f0f564f */
/* sub-float v0, p0, v0 */
/* const v1, 0x3e371ff0 */
/* div-float/2addr v0, v1 */
v0 = android.util.MathUtils .exp ( v0 );
/* const v1, 0x3e91c020 */
/* add-float/2addr v0, v1 */
/* .line 246 */
/* .restart local v0 # "ret":F */
} // :goto_0
int v1 = 0; // const/4 v1, 0x0
/* const/high16 v2, 0x41400000 # 12.0f */
v1 = android.util.MathUtils .constrain ( v0,v1,v2 );
/* .line 250 */
/* .local v1, "normalizedRet":F */
/* div-float v2, v1, v2 */
} // .end method
public static final Float convertLinearToGamma ( Float p0 ) {
/* .locals 3 */
/* .param p0, "val" # F */
/* .line 261 */
/* const/high16 v0, 0x41400000 # 12.0f */
/* mul-float/2addr v0, p0 */
/* .line 263 */
/* .local v0, "normalizedVal":F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpg-float v1, v0, v1 */
/* if-gtz v1, :cond_0 */
/* .line 264 */
v1 = android.util.MathUtils .sqrt ( v0 );
/* const/high16 v2, 0x3f000000 # 0.5f */
/* mul-float/2addr v1, v2 */
/* .local v1, "ret":F */
/* .line 266 */
} // .end local v1 # "ret":F
} // :cond_0
/* const v1, 0x3e91c020 */
/* sub-float v1, v0, v1 */
v1 = android.util.MathUtils .log ( v1 );
/* const v2, 0x3e371ff0 */
/* mul-float/2addr v1, v2 */
/* const v2, 0x3f0f564f */
/* add-float/2addr v1, v2 */
/* .line 268 */
/* .restart local v1 # "ret":F */
} // :goto_0
} // .end method
