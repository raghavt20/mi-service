.class Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;
.super Landroid/database/ContentObserver;
.source "AutoDisableScreenButtonsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/AutoDisableScreenButtonsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisableButtonsSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 339
    iput-object p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    .line 340
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 341
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 349
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 350
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "screen_buttons_state"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 352
    const-string v1, "auto_disable_screen_button"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 354
    const-string v1, "auto_disable_screen_button_cloud_setting"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 357
    invoke-virtual {p0, v2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->onChange(Z)V

    .line 358
    return-void
.end method

.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 345
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$mupdateSettings(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V

    .line 346
    return-void
.end method
