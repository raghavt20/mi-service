.class public Lcom/miui/server/input/FlipScreenManager;
.super Ljava/lang/Object;
.source "FlipScreenManager.java"


# static fields
.field private static volatile sIntance:Lcom/miui/server/input/FlipScreenManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "systemContext"    # Landroid/content/Context;

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/server/input/FlipScreenManager;
    .locals 2
    .param p0, "systemContext"    # Landroid/content/Context;

    .line 18
    sget-object v0, Lcom/miui/server/input/FlipScreenManager;->sIntance:Lcom/miui/server/input/FlipScreenManager;

    if-nez v0, :cond_1

    .line 19
    const-class v0, Lcom/miui/server/input/FlipScreenManager;

    monitor-enter v0

    .line 20
    :try_start_0
    sget-object v1, Lcom/miui/server/input/FlipScreenManager;->sIntance:Lcom/miui/server/input/FlipScreenManager;

    if-nez v1, :cond_0

    .line 21
    new-instance v1, Lcom/miui/server/input/FlipScreenManager;

    invoke-direct {v1, p0}, Lcom/miui/server/input/FlipScreenManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/miui/server/input/FlipScreenManager;->sIntance:Lcom/miui/server/input/FlipScreenManager;

    .line 23
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 25
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/FlipScreenManager;->sIntance:Lcom/miui/server/input/FlipScreenManager;

    return-object v0
.end method


# virtual methods
.method public startService()V
    .locals 0

    .line 29
    return-void
.end method
