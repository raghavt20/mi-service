.class Lcom/miui/server/input/MiuiBackTapGestureService$H;
.super Landroid/os/Handler;
.source "MiuiBackTapGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiBackTapGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field public static final MSG_BACKTAP_FUNCTION:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiBackTapGestureService;


# direct methods
.method constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiBackTapGestureService;

    .line 234
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$H;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .line 238
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 239
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "shortcut"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "shortcut":Ljava/lang/String;
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 241
    .local v1, "function":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 242
    return-void

    .line 244
    :cond_0
    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$H;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 247
    .end local v0    # "shortcut":Ljava/lang/String;
    .end local v1    # "function":Ljava/lang/String;
    :cond_1
    return-void
.end method
