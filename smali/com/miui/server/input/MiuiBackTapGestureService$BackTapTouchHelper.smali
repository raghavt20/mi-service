.class Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;
.super Ljava/lang/Object;
.source "MiuiBackTapGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiBackTapGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackTapTouchHelper"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;,
        Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;
    }
.end annotation


# instance fields
.field private final BACK_TAP_TOUCH_DEBUG:Ljava/lang/String;

.field private DEBUG:Z

.field private SCREEN_HEIGHT:I

.field private SCREEN_WIDTH:I

.field private final TAG:Ljava/lang/String;

.field private TOUCH_BOTTOM:F

.field private final TOUCH_EVENT_DEBOUNCE:I

.field private TOUCH_LEFT:F

.field private TOUCH_RIGHT:F

.field private TOUCH_TOP:F

.field private mRotation:I

.field private mRotationWatcher:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;

.field private mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

.field private mTouchTrackingEnabled:Z

.field private mWms:Lcom/android/server/wm/WindowManagerService;

.field private final spFoldStatus:Z

.field final synthetic this$0:Lcom/miui/server/input/MiuiBackTapGestureService;


# direct methods
.method static bridge synthetic -$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->DEBUG:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetSCREEN_HEIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetSCREEN_WIDTH(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetTOUCH_BOTTOM(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_BOTTOM:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetTOUCH_LEFT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_LEFT:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetTOUCH_RIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_RIGHT:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetTOUCH_TOP(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_TOP:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmRotation(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotation:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmRotation(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotation:I

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckTouchStatus(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->checkTouchStatus()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mdump(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$misDebuggable(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->isDebuggable()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mnotifyRotationChanged(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->notifyRotationChanged(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetTouchTrackingEnabled(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->setTouchTrackingEnabled(Z)V

    return-void
.end method

.method public constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V
    .locals 1

    .line 300
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 273
    const-string p1, "BackTapTouchHelper"

    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TAG:Ljava/lang/String;

    .line 274
    const-string/jumbo p1, "sys.sensor.backtap.dbg"

    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->BACK_TAP_TOUCH_DEBUG:Ljava/lang/String;

    .line 276
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->DEBUG:Z

    .line 280
    iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z

    .line 284
    const/high16 p1, 0x42c80000    # 100.0f

    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_LEFT:F

    .line 285
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_RIGHT:F

    .line 286
    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_TOP:F

    .line 287
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I

    int-to-float v0, v0

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_BOTTOM:F

    .line 289
    const/4 p1, -0x1

    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotation:I

    .line 293
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x110b0011

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_EVENT_DEBOUNCE:I

    .line 297
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x1105000d

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z

    .line 301
    const-string/jumbo p1, "window"

    invoke-static {p1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    check-cast p1, Lcom/android/server/wm/WindowManagerService;

    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mWms:Lcom/android/server/wm/WindowManagerService;

    .line 302
    new-instance p1, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-direct {p1, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)V

    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    .line 303
    invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->initialize()V

    .line 304
    return-void
.end method

.method private checkTouchStatus()Z
    .locals 9

    .line 323
    const-wide/16 v0, 0x0

    .line 325
    .local v0, "mNowTime":J
    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-virtual {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->getTouchStatus()I

    move-result v2

    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-static {v3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "BackTapTouchHelper"

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 326
    const-string v2, "getTouchStatus: TOUCH_POSITIVE, sensor event will be ignored"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    return v4

    .line 330
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsUnFolded is "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v5}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsUnFolded(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", spFoldStatus: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-boolean v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsUnFolded(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 332
    return v4

    .line 333
    :cond_1
    iget-boolean v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsUnFolded(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 334
    return v4

    .line 337
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    .line 338
    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-virtual {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->getTouchStatus()I

    move-result v2

    iget-object v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-static {v5}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->-$$Nest$fgetmLastObservedTouchTime(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;)J

    move-result-wide v5

    sub-long v5, v0, v5

    iget v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_EVENT_DEBOUNCE:I

    const v7, 0xf4240

    mul-int/2addr v2, v7

    int-to-long v7, v2

    cmp-long v2, v5, v7

    if-gez v2, :cond_3

    .line 340
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkTouchStatus: { mow time: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " last positive time: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-static {v5}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->-$$Nest$fgetmLastObservedTouchTime(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;)J

    move-result-wide v5

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "}"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const-string/jumbo v2, "sensor event will be ignored due to touch event timeout not reached!"

    invoke-static {v3, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return v4

    .line 346
    :cond_3
    const/4 v2, 0x0

    return v2
.end method

.method private dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 3
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "enableDebug"    # Z

    .line 350
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 351
    const-string v0, "BackTapTouchHelper"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 353
    const-string v0, "SCREEN_WIDTH="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 354
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 355
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 356
    const-string v0, "SCREEN_HEIGHT="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 357
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 358
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 359
    const-string v0, "TOUCH_REGION=["

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 360
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    iget v1, v1, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    iget v2, v2, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    iget v2, v2, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    iget v1, v1, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 363
    const-string v0, "TOUCH_EVENT_DEBOUNCE="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 364
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_EVENT_DEBOUNCE:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 365
    iput-boolean p3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->DEBUG:Z

    .line 366
    return-void
.end method

.method private initialize()V
    .locals 7

    .line 307
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 308
    .local v0, "wm":Landroid/view/WindowManager;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 309
    .local v1, "outSize":Landroid/graphics/Point;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 310
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I

    .line 311
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I

    .line 312
    const/high16 v3, 0x42480000    # 50.0f

    iput v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_LEFT:F

    .line 313
    iget v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I

    int-to-float v4, v4

    sub-float/2addr v4, v3

    iput v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_RIGHT:F

    .line 314
    const/high16 v5, 0x42c80000    # 100.0f

    iput v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_TOP:F

    .line 315
    int-to-float v2, v2

    iput v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_BOTTOM:F

    .line 316
    iget-object v6, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-virtual {v6, v3, v4, v5, v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V

    .line 317
    new-instance v2, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher-IA;)V

    iput-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotationWatcher:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;

    .line 318
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v4}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getDisplayId()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;I)I

    .line 319
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBackTapTouchHelper initialize!! SCREEN_WIDTH: = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", SCREEN_HEIGHT = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "BackTapTouchHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    return-void
.end method

.method private isDebuggable()Z
    .locals 2

    .line 385
    const-string/jumbo v0, "sys.sensor.backtap.dbg"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private notifyRotationChanged(I)V
    .locals 1
    .param p1, "rotation"    # I

    .line 389
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    if-eqz v0, :cond_0

    .line 390
    invoke-virtual {v0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(I)V

    .line 392
    :cond_0
    return-void
.end method

.method private setTouchTrackingEnabled(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .line 369
    const-string v0, "BackTapTouchHelper"

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    .line 370
    iget-boolean v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z

    if-nez v3, :cond_1

    .line 371
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mWms:Lcom/android/server/wm/WindowManagerService;

    iget-object v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 372
    iput-boolean v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z

    .line 373
    const-string/jumbo v1, "touch pointer listener has registered!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 376
    :cond_0
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchPositionTracker:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;

    if-eqz v3, :cond_1

    iget-boolean v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z

    if-ne v4, v1, :cond_1

    .line 377
    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mWms:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 378
    iput-boolean v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z

    .line 379
    const-string/jumbo v1, "touch pointer listener has unregistered!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_1
    :goto_0
    return-void
.end method
