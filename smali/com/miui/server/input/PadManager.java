public class com.miui.server.input.PadManager {
	 /* .source "PadManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/PadManager$H;, */
	 /* Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;, */
	 /* Lcom/miui/server/input/PadManager$TrackMotionListener;, */
	 /* Lcom/miui/server/input/PadManager$BrightnessUtils; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String IIC_MI_MEDIA_KEYBOARD_NAME;
private static final java.lang.String TAG;
private static volatile com.miui.server.input.PadManager sIntance;
/* # instance fields */
private final android.media.AudioManager mAudioManager;
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private volatile Boolean mIsCapsLock;
private volatile Boolean mIsLidOpen;
private volatile Boolean mIsTabletOpen;
private Boolean mIsUserSetup;
private final java.lang.Object mLock;
private com.miui.server.input.PadManager$MiuiPadSettingsObserver mMiuiPadSettingsObserver;
private Boolean mRunning;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.PadManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.input.PadManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmRunning ( com.miui.server.input.PadManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/PadManager;->mRunning:Z */
} // .end method
static void -$$Nest$fputmIsUserSetup ( com.miui.server.input.PadManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsUserSetup:Z */
	 return;
} // .end method
private com.miui.server.input.PadManager ( ) {
	 /* .locals 3 */
	 /* .line 53 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 43 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsLidOpen:Z */
	 /* .line 44 */
	 /* iput-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsTabletOpen:Z */
	 /* .line 46 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mLock = v0;
	 /* .line 54 */
	 android.app.ActivityThread .currentActivityThread ( );
	 (( android.app.ActivityThread ) v0 ).getSystemContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
	 this.mContext = v0;
	 /* .line 55 */
	 /* new-instance v1, Lcom/miui/server/input/PadManager$H; */
	 com.android.server.input.MiuiInputThread .getHandler ( );
	 (( android.os.Handler ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, p0, v2}, Lcom/miui/server/input/PadManager$H;-><init>(Lcom/miui/server/input/PadManager;Landroid/os/Looper;)V */
	 this.mHandler = v1;
	 /* .line 56 */
	 final String v1 = "audio"; // const-string v1, "audio"
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/media/AudioManager; */
	 this.mAudioManager = v0;
	 /* .line 57 */
	 return;
} // .end method
public static com.miui.server.input.PadManager getInstance ( ) {
	 /* .locals 2 */
	 /* .line 60 */
	 v0 = com.miui.server.input.PadManager.sIntance;
	 /* if-nez v0, :cond_1 */
	 /* .line 61 */
	 /* const-class v0, Lcom/miui/server/input/PadManager; */
	 /* monitor-enter v0 */
	 /* .line 62 */
	 try { // :try_start_0
		 v1 = com.miui.server.input.PadManager.sIntance;
		 /* if-nez v1, :cond_0 */
		 /* .line 63 */
		 /* new-instance v1, Lcom/miui/server/input/PadManager; */
		 /* invoke-direct {v1}, Lcom/miui/server/input/PadManager;-><init>()V */
		 /* .line 65 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
	 /* .line 67 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.PadManager.sIntance;
} // .end method
private Boolean isKeyFromKeyboard ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 92 */
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 93 */
/* .local v0, "device":Landroid/view/InputDevice; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( android.view.InputDevice ) v0 ).getProductId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I
/* .line 94 */
v2 = (( android.view.InputDevice ) v0 ).getVendorId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I
/* .line 93 */
v1 = miui.hardware.input.MiuiKeyboardHelper .isXiaomiWakeUpDevice ( v1,v2 );
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
/* # virtual methods */
public Boolean adjustBrightnessFromKeycode ( android.view.KeyEvent p0 ) {
/* .locals 7 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 100 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v1, 0xdc */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* if-eq v0, v1, :cond_1 */
/* .line 101 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v1, 0xdd */
/* if-ne v0, v1, :cond_0 */
/* .line 121 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/miui/server/input/PadManager;->mRunning:Z */
/* .line 122 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 124 */
/* .line 102 */
} // :cond_1
} // :goto_0
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_3 */
/* .line 103 */
/* iput-boolean v1, p0, Lcom/miui/server/input/PadManager;->mRunning:Z */
/* .line 104 */
v0 = this.mHandler;
/* const/16 v3, 0x63 */
(( android.os.Handler ) v0 ).obtainMessage ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 105 */
/* .local v0, "msg":Landroid/os/Message; */
/* new-instance v3, Landroid/os/Bundle; */
/* invoke-direct {v3}, Landroid/os/Bundle;-><init>()V */
/* .line 106 */
/* .local v3, "bundle":Landroid/os/Bundle; */
final String v4 = "brightness_key"; // const-string v4, "brightness_key"
(( android.os.Bundle ) v3 ).putParcelable ( v4, p1 ); // invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 107 */
final String v4 = "delay_adjust"; // const-string v4, "delay_adjust"
/* const/16 v5, 0xc8 */
(( android.os.Bundle ) v3 ).putInt ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 108 */
(( android.os.Message ) v0 ).setData ( v3 ); // invoke-virtual {v0, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 109 */
v4 = this.mHandler;
(( android.os.Handler ) v4 ).sendMessage ( v0 ); // invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 110 */
/* iget-boolean v4, p0, Lcom/miui/server/input/PadManager;->mIsUserSetup:Z */
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 111 */
v4 = this.mContext;
/* new-instance v5, Landroid/content/Intent; */
final String v6 = "com.android.intent.action.SHOW_BRIGHTNESS_DIALOG"; // const-string v6, "com.android.intent.action.SHOW_BRIGHTNESS_DIALOG"
/* invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
v6 = android.os.UserHandle.CURRENT_OR_SELF;
(( android.content.Context ) v4 ).startActivityAsUser ( v5, v2, v6 ); // invoke-virtual {v4, v5, v2, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
/* .line 114 */
} // .end local v0 # "msg":Landroid/os/Message;
} // .end local v3 # "bundle":Landroid/os/Bundle;
} // :cond_2
/* .line 115 */
} // :cond_3
/* iput-boolean v3, p0, Lcom/miui/server/input/PadManager;->mRunning:Z */
/* .line 116 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeCallbacksAndMessages ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
/* .line 118 */
} // :goto_1
final String v0 = "PadManager"; // const-string v0, "PadManager"
final String v2 = "handle brightness key for miui"; // const-string v2, "handle brightness key for miui"
android.util.Slog .i ( v0,v2 );
/* .line 119 */
} // .end method
public synchronized Boolean getCapsLockStatus ( ) {
/* .locals 1 */
/* monitor-enter p0 */
/* .line 134 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsCapsLock:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* monitor-exit p0 */
/* .line 134 */
} // .end local p0 # "this":Lcom/miui/server/input/PadManager;
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
public Boolean getMuteStatus ( ) {
/* .locals 1 */
/* .line 142 */
v0 = this.mAudioManager;
v0 = (( android.media.AudioManager ) v0 ).isMicrophoneMute ( ); // invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z
} // .end method
public Boolean isPad ( ) {
/* .locals 1 */
/* .line 71 */
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
} // .end method
public void notifySystemBooted ( ) {
/* .locals 1 */
/* .line 128 */
v0 = com.android.server.input.padkeyboard.MiuiIICKeyboardManager .supportPadKeyboard ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 129 */
(( com.miui.server.input.PadManager ) p0 ).registerPadSettingsObserver ( ); // invoke-virtual {p0}, Lcom/miui/server/input/PadManager;->registerPadSettingsObserver()V
/* .line 131 */
} // :cond_0
return;
} // .end method
public Boolean padLidInterceptWakeKey ( android.view.KeyEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 88 */
v0 = (( com.miui.server.input.PadManager ) p0 ).isPad ( ); // invoke-virtual {p0}, Lcom/miui/server/input/PadManager;->isPad()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsLidOpen:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsTabletOpen:Z */
/* if-nez v0, :cond_1 */
} // :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/PadManager;->isKeyFromKeyboard(Landroid/view/KeyEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void registerPadSettingsObserver ( ) {
/* .locals 2 */
/* .line 75 */
/* new-instance v0, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver; */
com.android.server.input.MiuiInputThread .getHandler ( );
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;-><init>(Lcom/miui/server/input/PadManager;Landroid/os/Handler;)V */
this.mMiuiPadSettingsObserver = v0;
/* .line 76 */
(( com.miui.server.input.PadManager$MiuiPadSettingsObserver ) v0 ).observer ( ); // invoke-virtual {v0}, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->observer()V
/* .line 77 */
return;
} // .end method
public void registerPointerEventListener ( ) {
/* .locals 2 */
/* .line 276 */
v0 = this.mContext;
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( v0 );
/* new-instance v1, Lcom/miui/server/input/PadManager$TrackMotionListener; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/PadManager$TrackMotionListener;-><init>(Lcom/miui/server/input/PadManager;)V */
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).registerPointerEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 277 */
return;
} // .end method
public synchronized void setCapsLockStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isCapsLock" # Z */
/* monitor-enter p0 */
/* .line 138 */
try { // :try_start_0
/* iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsCapsLock:Z */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 139 */
/* monitor-exit p0 */
return;
/* .line 137 */
} // .end local p0 # "this":Lcom/miui/server/input/PadManager;
} // .end local p1 # "isCapsLock":Z
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void setIsLidOpen ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isLidOpen" # Z */
/* .line 80 */
/* iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsLidOpen:Z */
/* .line 81 */
return;
} // .end method
public void setIsTableOpen ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "isTabletOpen" # Z */
/* .line 84 */
/* iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsTabletOpen:Z */
/* .line 85 */
return;
} // .end method
