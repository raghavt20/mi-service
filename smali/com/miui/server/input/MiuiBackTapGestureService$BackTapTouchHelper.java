class com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "BackTapTouchHelper" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;, */
/* Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher; */
/* } */
} // .end annotation
/* # instance fields */
private final java.lang.String BACK_TAP_TOUCH_DEBUG;
private Boolean DEBUG;
private Integer SCREEN_HEIGHT;
private Integer SCREEN_WIDTH;
private final java.lang.String TAG;
private Float TOUCH_BOTTOM;
private final Integer TOUCH_EVENT_DEBOUNCE;
private Float TOUCH_LEFT;
private Float TOUCH_RIGHT;
private Float TOUCH_TOP;
private Integer mRotation;
private com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher mRotationWatcher;
private com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker mTouchPositionTracker;
private Boolean mTouchTrackingEnabled;
private com.android.server.wm.WindowManagerService mWms;
private final Boolean spFoldStatus;
final com.miui.server.input.MiuiBackTapGestureService this$0; //synthetic
/* # direct methods */
static Boolean -$$Nest$fgetDEBUG ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->DEBUG:Z */
} // .end method
static Integer -$$Nest$fgetSCREEN_HEIGHT ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I */
} // .end method
static Integer -$$Nest$fgetSCREEN_WIDTH ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I */
} // .end method
static Float -$$Nest$fgetTOUCH_BOTTOM ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_BOTTOM:F */
} // .end method
static Float -$$Nest$fgetTOUCH_LEFT ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_LEFT:F */
} // .end method
static Float -$$Nest$fgetTOUCH_RIGHT ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_RIGHT:F */
} // .end method
static Float -$$Nest$fgetTOUCH_TOP ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_TOP:F */
} // .end method
static Integer -$$Nest$fgetmRotation ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotation:I */
} // .end method
static void -$$Nest$fputmRotation ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotation:I */
return;
} // .end method
static Boolean -$$Nest$mcheckTouchStatus ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->checkTouchStatus()Z */
} // .end method
static void -$$Nest$mdump ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0, java.lang.String p1, java.io.PrintWriter p2, Boolean p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->dump(Ljava/lang/String;Ljava/io/PrintWriter;Z)V */
return;
} // .end method
static Boolean -$$Nest$misDebuggable ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->isDebuggable()Z */
} // .end method
static void -$$Nest$mnotifyRotationChanged ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->notifyRotationChanged(I)V */
return;
} // .end method
static void -$$Nest$msetTouchTrackingEnabled ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->setTouchTrackingEnabled(Z)V */
return;
} // .end method
public com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper ( ) {
/* .locals 1 */
/* .line 300 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 273 */
final String p1 = "BackTapTouchHelper"; // const-string p1, "BackTapTouchHelper"
this.TAG = p1;
/* .line 274 */
/* const-string/jumbo p1, "sys.sensor.backtap.dbg" */
this.BACK_TAP_TOUCH_DEBUG = p1;
/* .line 276 */
int p1 = 0; // const/4 p1, 0x0
/* iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->DEBUG:Z */
/* .line 280 */
/* iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z */
/* .line 284 */
/* const/high16 p1, 0x42c80000 # 100.0f */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_LEFT:F */
/* .line 285 */
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I */
/* int-to-float v0, v0 */
/* sub-float/2addr v0, p1 */
/* iput v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_RIGHT:F */
/* .line 286 */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_TOP:F */
/* .line 287 */
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I */
/* int-to-float v0, v0 */
/* sub-float/2addr v0, p1 */
/* iput v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_BOTTOM:F */
/* .line 289 */
int p1 = -1; // const/4 p1, -0x1
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mRotation:I */
/* .line 293 */
android.content.res.Resources .getSystem ( );
/* const v0, 0x110b0011 */
p1 = (( android.content.res.Resources ) p1 ).getInteger ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_EVENT_DEBOUNCE:I */
/* .line 297 */
android.content.res.Resources .getSystem ( );
/* const v0, 0x1105000d */
p1 = (( android.content.res.Resources ) p1 ).getBoolean ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z */
/* .line 301 */
/* const-string/jumbo p1, "window" */
android.os.ServiceManager .getService ( p1 );
/* check-cast p1, Lcom/android/server/wm/WindowManagerService; */
this.mWms = p1;
/* .line 302 */
/* new-instance p1, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker; */
/* invoke-direct {p1, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)V */
this.mTouchPositionTracker = p1;
/* .line 303 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->initialize()V */
/* .line 304 */
return;
} // .end method
private Boolean checkTouchStatus ( ) {
/* .locals 9 */
/* .line 323 */
/* const-wide/16 v0, 0x0 */
/* .line 325 */
/* .local v0, "mNowTime":J */
v2 = this.mTouchPositionTracker;
v2 = (( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) v2 ).getTouchStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->getTouchStatus()I
v3 = this.mTouchPositionTracker;
java.util.Objects .requireNonNull ( v3 );
final String v3 = "BackTapTouchHelper"; // const-string v3, "BackTapTouchHelper"
int v4 = 1; // const/4 v4, 0x1
/* if-ne v2, v4, :cond_0 */
/* .line 326 */
final String v2 = "getTouchStatus: TOUCH_POSITIVE, sensor event will be ignored"; // const-string v2, "getTouchStatus: TOUCH_POSITIVE, sensor event will be ignored"
android.util.Slog .d ( v3,v2 );
/* .line 327 */
/* .line 330 */
} // :cond_0
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "mIsUnFolded is "; // const-string v5, "mIsUnFolded is "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.this$0;
v5 = com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsUnFolded ( v5 );
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v5 = ", spFoldStatus: "; // const-string v5, ", spFoldStatus: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 331 */
/* iget-boolean v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z */
/* if-nez v2, :cond_1 */
v2 = this.this$0;
v2 = com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsUnFolded ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 332 */
/* .line 333 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->spFoldStatus:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.this$0;
v2 = com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsUnFolded ( v2 );
/* if-nez v2, :cond_2 */
/* .line 334 */
/* .line 337 */
} // :cond_2
android.os.SystemClock .elapsedRealtimeNanos ( );
/* move-result-wide v0 */
/* .line 338 */
v2 = this.mTouchPositionTracker;
v2 = (( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) v2 ).getTouchStatus ( ); // invoke-virtual {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->getTouchStatus()I
v5 = this.mTouchPositionTracker;
java.util.Objects .requireNonNull ( v5 );
/* if-nez v2, :cond_3 */
v2 = this.mTouchPositionTracker;
com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker .-$$Nest$fgetmLastObservedTouchTime ( v2 );
/* move-result-wide v5 */
/* sub-long v5, v0, v5 */
/* iget v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_EVENT_DEBOUNCE:I */
/* const v7, 0xf4240 */
/* mul-int/2addr v2, v7 */
/* int-to-long v7, v2 */
/* cmp-long v2, v5, v7 */
/* if-gez v2, :cond_3 */
/* .line 340 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "checkTouchStatus: { mow time: "; // const-string v5, "checkTouchStatus: { mow time: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v5 = " last positive time: "; // const-string v5, " last positive time: "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = this.mTouchPositionTracker;
com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker .-$$Nest$fgetmLastObservedTouchTime ( v5 );
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v2 ).append ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* const-string/jumbo v5, "}" */
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v3,v2 );
/* .line 342 */
/* const-string/jumbo v2, "sensor event will be ignored due to touch event timeout not reached!" */
android.util.Slog .w ( v3,v2 );
/* .line 343 */
/* .line 346 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
} // .end method
private void dump ( java.lang.String p0, java.io.PrintWriter p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "enableDebug" # Z */
/* .line 350 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 351 */
final String v0 = "BackTapTouchHelper"; // const-string v0, "BackTapTouchHelper"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 352 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 353 */
final String v0 = "SCREEN_WIDTH="; // const-string v0, "SCREEN_WIDTH="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 354 */
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 355 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 356 */
final String v0 = "SCREEN_HEIGHT="; // const-string v0, "SCREEN_HEIGHT="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 357 */
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 358 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 359 */
final String v0 = "TOUCH_REGION=["; // const-string v0, "TOUCH_REGION=["
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 360 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mTouchPositionTracker;
/* iget v1, v1, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = ", "; // const-string v1, ", "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTouchPositionTracker;
/* iget v2, v2, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mTouchPositionTracker;
/* iget v2, v2, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mTouchPositionTracker;
/* iget v1, v1, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 362 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 363 */
final String v0 = "TOUCH_EVENT_DEBOUNCE="; // const-string v0, "TOUCH_EVENT_DEBOUNCE="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 364 */
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_EVENT_DEBOUNCE:I */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 365 */
/* iput-boolean p3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->DEBUG:Z */
/* .line 366 */
return;
} // .end method
private void initialize ( ) {
/* .locals 7 */
/* .line 307 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v0 );
/* const-string/jumbo v1, "window" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/WindowManager; */
/* .line 308 */
/* .local v0, "wm":Landroid/view/WindowManager; */
/* new-instance v1, Landroid/graphics/Point; */
/* invoke-direct {v1}, Landroid/graphics/Point;-><init>()V */
/* .line 309 */
/* .local v1, "outSize":Landroid/graphics/Point; */
(( android.view.Display ) v2 ).getRealSize ( v1 ); // invoke-virtual {v2, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V
/* .line 310 */
/* iget v2, v1, Landroid/graphics/Point;->x:I */
/* iput v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I */
/* .line 311 */
/* iget v2, v1, Landroid/graphics/Point;->y:I */
/* iput v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I */
/* .line 312 */
/* const/high16 v3, 0x42480000 # 50.0f */
/* iput v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_LEFT:F */
/* .line 313 */
/* iget v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I */
/* int-to-float v4, v4 */
/* sub-float/2addr v4, v3 */
/* iput v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_RIGHT:F */
/* .line 314 */
/* const/high16 v5, 0x42c80000 # 100.0f */
/* iput v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_TOP:F */
/* .line 315 */
/* int-to-float v2, v2 */
/* iput v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->TOUCH_BOTTOM:F */
/* .line 316 */
v6 = this.mTouchPositionTracker;
(( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) v6 ).updateTouchBorder ( v3, v4, v5, v2 ); // invoke-virtual {v6, v3, v4, v5, v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V
/* .line 317 */
/* new-instance v2, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher-IA;)V */
this.mRotationWatcher = v2;
/* .line 318 */
v3 = this.mWms;
v4 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v4 );
(( android.content.Context ) v4 ).getDisplay ( ); // invoke-virtual {v4}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
v4 = (( android.view.Display ) v4 ).getDisplayId ( ); // invoke-virtual {v4}, Landroid/view/Display;->getDisplayId()I
(( com.android.server.wm.WindowManagerService ) v3 ).watchRotation ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Lcom/android/server/wm/WindowManagerService;->watchRotation(Landroid/view/IRotationWatcher;I)I
/* .line 319 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "mBackTapTouchHelper initialize!! SCREEN_WIDTH: = "; // const-string v3, "mBackTapTouchHelper initialize!! SCREEN_WIDTH: = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_WIDTH:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ", SCREEN_HEIGHT = "; // const-string v3, ", SCREEN_HEIGHT = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->SCREEN_HEIGHT:I */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "BackTapTouchHelper"; // const-string v3, "BackTapTouchHelper"
android.util.Slog .d ( v3,v2 );
/* .line 320 */
return;
} // .end method
private Boolean isDebuggable ( ) {
/* .locals 2 */
/* .line 385 */
/* const-string/jumbo v0, "sys.sensor.backtap.dbg" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
private void notifyRotationChanged ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "rotation" # I */
/* .line 389 */
v0 = this.mTouchPositionTracker;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 390 */
(( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) v0 ).updateTouchBorder ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(I)V
/* .line 392 */
} // :cond_0
return;
} // .end method
private void setTouchTrackingEnabled ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "enable" # Z */
/* .line 369 */
final String v0 = "BackTapTouchHelper"; // const-string v0, "BackTapTouchHelper"
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 370 */
/* iget-boolean v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z */
/* if-nez v3, :cond_1 */
/* .line 371 */
v3 = this.mWms;
v4 = this.mTouchPositionTracker;
(( com.android.server.wm.WindowManagerService ) v3 ).registerPointerEventListener ( v4, v2 ); // invoke-virtual {v3, v4, v2}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 372 */
/* iput-boolean v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z */
/* .line 373 */
/* const-string/jumbo v1, "touch pointer listener has registered!" */
android.util.Slog .d ( v0,v1 );
/* .line 376 */
} // :cond_0
v3 = this.mTouchPositionTracker;
if ( v3 != null) { // if-eqz v3, :cond_1
/* iget-boolean v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z */
/* if-ne v4, v1, :cond_1 */
/* .line 377 */
v1 = this.mWms;
(( com.android.server.wm.WindowManagerService ) v1 ).unregisterPointerEventListener ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 378 */
/* iput-boolean v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->mTouchTrackingEnabled:Z */
/* .line 379 */
/* const-string/jumbo v1, "touch pointer listener has unregistered!" */
android.util.Slog .d ( v0,v1 );
/* .line 382 */
} // :cond_1
} // :goto_0
return;
} // .end method
