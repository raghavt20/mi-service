.class Lcom/miui/server/input/MiuiBackTapGestureService$1;
.super Ljava/lang/Object;
.source "MiuiBackTapGestureService.java"

# interfaces
.implements Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiBackTapGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiBackTapGestureService;


# direct methods
.method constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiBackTapGestureService;

    .line 63
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackDoubleTap()V
    .locals 4

    .line 66
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapTouchHelper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    move-result-object v0

    .line 67
    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$mcheckTouchStatus(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsGameMode(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "back_double_tap"

    invoke-static {v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mpostShortcutFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;ILjava/lang/String;)Z

    .line 70
    :cond_0
    return-void
.end method

.method public onBackTripleTap()V
    .locals 4

    .line 73
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapTouchHelper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$mcheckTouchStatus(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsGameMode(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$1;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "back_triple_tap"

    invoke-static {v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mpostShortcutFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;ILjava/lang/String;)Z

    .line 77
    :cond_0
    return-void
.end method
