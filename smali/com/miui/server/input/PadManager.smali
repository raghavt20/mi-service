.class public Lcom/miui/server/input/PadManager;
.super Ljava/lang/Object;
.source "PadManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/PadManager$H;,
        Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;,
        Lcom/miui/server/input/PadManager$TrackMotionListener;,
        Lcom/miui/server/input/PadManager$BrightnessUtils;
    }
.end annotation


# static fields
.field private static final IIC_MI_MEDIA_KEYBOARD_NAME:Ljava/lang/String; = "Xiaomi Consumer"

.field private static final TAG:Ljava/lang/String; = "PadManager"

.field private static volatile sIntance:Lcom/miui/server/input/PadManager;


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private volatile mIsCapsLock:Z

.field private volatile mIsLidOpen:Z

.field private volatile mIsTabletOpen:Z

.field private mIsUserSetup:Z

.field private final mLock:Ljava/lang/Object;

.field private mMiuiPadSettingsObserver:Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;

.field private mRunning:Z


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/PadManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/input/PadManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/PadManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmRunning(Lcom/miui/server/input/PadManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/PadManager;->mRunning:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmIsUserSetup(Lcom/miui/server/input/PadManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsUserSetup:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsLidOpen:Z

    .line 44
    iput-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsTabletOpen:Z

    .line 46
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/PadManager;->mLock:Ljava/lang/Object;

    .line 54
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/PadManager;->mContext:Landroid/content/Context;

    .line 55
    new-instance v1, Lcom/miui/server/input/PadManager$H;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/miui/server/input/PadManager$H;-><init>(Lcom/miui/server/input/PadManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/input/PadManager;->mHandler:Landroid/os/Handler;

    .line 56
    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/miui/server/input/PadManager;->mAudioManager:Landroid/media/AudioManager;

    .line 57
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/PadManager;
    .locals 2

    .line 60
    sget-object v0, Lcom/miui/server/input/PadManager;->sIntance:Lcom/miui/server/input/PadManager;

    if-nez v0, :cond_1

    .line 61
    const-class v0, Lcom/miui/server/input/PadManager;

    monitor-enter v0

    .line 62
    :try_start_0
    sget-object v1, Lcom/miui/server/input/PadManager;->sIntance:Lcom/miui/server/input/PadManager;

    if-nez v1, :cond_0

    .line 63
    new-instance v1, Lcom/miui/server/input/PadManager;

    invoke-direct {v1}, Lcom/miui/server/input/PadManager;-><init>()V

    sput-object v1, Lcom/miui/server/input/PadManager;->sIntance:Lcom/miui/server/input/PadManager;

    .line 65
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 67
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/PadManager;->sIntance:Lcom/miui/server/input/PadManager;

    return-object v0
.end method

.method private isKeyFromKeyboard(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 92
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 93
    .local v0, "device":Landroid/view/InputDevice;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I

    move-result v1

    .line 94
    invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I

    move-result v2

    .line 93
    invoke-static {v1, v2}, Lmiui/hardware/input/MiuiKeyboardHelper;->isXiaomiWakeUpDevice(II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method public adjustBrightnessFromKeycode(Landroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 100
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0xdc

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eq v0, v1, :cond_1

    .line 101
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0xdd

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 121
    :cond_0
    iput-boolean v3, p0, Lcom/miui/server/input/PadManager;->mRunning:Z

    .line 122
    iget-object v0, p0, Lcom/miui/server/input/PadManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 124
    return v3

    .line 102
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_3

    .line 103
    iput-boolean v1, p0, Lcom/miui/server/input/PadManager;->mRunning:Z

    .line 104
    iget-object v0, p0, Lcom/miui/server/input/PadManager;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x63

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 105
    .local v0, "msg":Landroid/os/Message;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v4, "brightness_key"

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 107
    const-string v4, "delay_adjust"

    const/16 v5, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    invoke-virtual {v0, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 109
    iget-object v4, p0, Lcom/miui/server/input/PadManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 110
    iget-boolean v4, p0, Lcom/miui/server/input/PadManager;->mIsUserSetup:Z

    if-eqz v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/miui/server/input/PadManager;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.intent.action.SHOW_BRIGHTNESS_DIALOG"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v6, Landroid/os/UserHandle;->CURRENT_OR_SELF:Landroid/os/UserHandle;

    invoke-virtual {v4, v5, v2, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    .line 114
    .end local v0    # "msg":Landroid/os/Message;
    .end local v3    # "bundle":Landroid/os/Bundle;
    :cond_2
    goto :goto_1

    .line 115
    :cond_3
    iput-boolean v3, p0, Lcom/miui/server/input/PadManager;->mRunning:Z

    .line 116
    iget-object v0, p0, Lcom/miui/server/input/PadManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 118
    :goto_1
    const-string v0, "PadManager"

    const-string v2, "handle brightness key for miui"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    return v1
.end method

.method public declared-synchronized getCapsLockStatus()Z
    .locals 1

    monitor-enter p0

    .line 134
    :try_start_0
    iget-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsCapsLock:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 134
    .end local p0    # "this":Lcom/miui/server/input/PadManager;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMuteStatus()Z
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/miui/server/input/PadManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v0

    return v0
.end method

.method public isPad()Z
    .locals 1

    .line 71
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    return v0
.end method

.method public notifySystemBooted()V
    .locals 1

    .line 128
    invoke-static {}, Lcom/android/server/input/padkeyboard/MiuiIICKeyboardManager;->supportPadKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/miui/server/input/PadManager;->registerPadSettingsObserver()V

    .line 131
    :cond_0
    return-void
.end method

.method public padLidInterceptWakeKey(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 88
    invoke-virtual {p0}, Lcom/miui/server/input/PadManager;->isPad()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsLidOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/PadManager;->mIsTabletOpen:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/input/PadManager;->isKeyFromKeyboard(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public registerPadSettingsObserver()V
    .locals 2

    .line 75
    new-instance v0, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;-><init>(Lcom/miui/server/input/PadManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/PadManager;->mMiuiPadSettingsObserver:Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;

    .line 76
    invoke-virtual {v0}, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->observer()V

    .line 77
    return-void
.end method

.method public registerPointerEventListener()V
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/miui/server/input/PadManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    new-instance v1, Lcom/miui/server/input/PadManager$TrackMotionListener;

    invoke-direct {v1, p0}, Lcom/miui/server/input/PadManager$TrackMotionListener;-><init>(Lcom/miui/server/input/PadManager;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 277
    return-void
.end method

.method public declared-synchronized setCapsLockStatus(Z)V
    .locals 0
    .param p1, "isCapsLock"    # Z

    monitor-enter p0

    .line 138
    :try_start_0
    iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsCapsLock:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-void

    .line 137
    .end local p0    # "this":Lcom/miui/server/input/PadManager;
    .end local p1    # "isCapsLock":Z
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setIsLidOpen(Z)V
    .locals 0
    .param p1, "isLidOpen"    # Z

    .line 80
    iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsLidOpen:Z

    .line 81
    return-void
.end method

.method public setIsTableOpen(Z)V
    .locals 0
    .param p1, "isTabletOpen"    # Z

    .line 84
    iput-boolean p1, p0, Lcom/miui/server/input/PadManager;->mIsTabletOpen:Z

    .line 85
    return-void
.end method
