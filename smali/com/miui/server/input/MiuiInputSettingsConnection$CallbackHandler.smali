.class public Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;
.super Landroid/os/Handler;
.source "MiuiInputSettingsConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiInputSettingsConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CallbackHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/MiuiInputSettingsConnection;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiInputSettingsConnection;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 322
    iput-object p1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    .line 323
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 324
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 328
    const-string v0, "MiuiInputSettingsConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive message from settings "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$smmessageWhatClientToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$mresetTimer(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    .line 331
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$fgetmCallbackMap(Lcom/miui/server/input/MiuiInputSettingsConnection;)Ljava/util/Map;

    move-result-object v0

    monitor-enter v0

    .line 332
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$fgetmCallbackMap(Lcom/miui/server/input/MiuiInputSettingsConnection;)Ljava/util/Map;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/function/Consumer;

    .line 333
    .local v1, "messageConsumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Landroid/os/Message;>;"
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    if-nez v1, :cond_0

    .line 335
    const-string v0, "MiuiInputSettingsConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not found callback for what = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    .line 336
    invoke-static {v3}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$smmessageWhatClientToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 335
    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    return-void

    .line 339
    :cond_0
    invoke-interface {v1, p1}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    .line 340
    return-void

    .line 333
    .end local v1    # "messageConsumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Landroid/os/Message;>;"
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
