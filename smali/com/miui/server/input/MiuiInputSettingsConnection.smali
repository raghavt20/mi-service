.class public Lcom/miui/server/input/MiuiInputSettingsConnection;
.super Ljava/lang/Object;
.source "MiuiInputSettingsConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;,
        Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;,
        Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;,
        Lcom/miui/server/input/MiuiInputSettingsConnection$MessageWhatClient;,
        Lcom/miui/server/input/MiuiInputSettingsConnection$MessageWhatServer;
    }
.end annotation


# static fields
.field private static final INPUT_SETTINGS_COMPONENT:Ljava/lang/String; = "com.miui.securitycore/com.miui.miinput.service.MiuiInputSettingsService"

.field public static final MSG_CLIENT_TRIGGER_QUICK_NOTE:I = 0x1

.field public static final MSG_SERVER_ADD_STYLUS_MASK:I = 0x1

.field public static final MSG_SERVER_REMOVE_STYLUS_MASK:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MiuiInputSettingsConnection"

.field private static final TIMEOUT_TIME:I = 0x3a98

.field private static volatile sInstance:Lcom/miui/server/input/MiuiInputSettingsConnection;


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mCachedMessageList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Landroid/os/Message;",
            "Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbackMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/function/Consumer<",
            "Landroid/os/Message;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mCallbackMessenger:Landroid/os/Messenger;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mMessenger:Landroid/os/Messenger;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private final mTimerRunnable:Ljava/lang/Runnable;


# direct methods
.method public static synthetic $r8$lambda$HazgjCEfaUwoTCtcu7Fa4YKwfwQ(Lcom/miui/server/input/MiuiInputSettingsConnection;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetConnection()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmCachedMessageList(Lcom/miui/server/input/MiuiInputSettingsConnection;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCachedMessageList:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCallbackMap(Lcom/miui/server/input/MiuiInputSettingsConnection;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMessenger(Lcom/miui/server/input/MiuiInputSettingsConnection;)Landroid/os/Messenger;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mMessenger:Landroid/os/Messenger;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmMessenger(Lcom/miui/server/input/MiuiInputSettingsConnection;Landroid/os/Messenger;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mMessenger:Landroid/os/Messenger;

    return-void
.end method

.method static bridge synthetic -$$Nest$mexecuteCallback(Lcom/miui/server/input/MiuiInputSettingsConnection;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetConnection(Lcom/miui/server/input/MiuiInputSettingsConnection;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetConnection()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetTimer(Lcom/miui/server/input/MiuiInputSettingsConnection;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetTimer()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smmessageWhatClientToString(I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->messageWhatClientToString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$smmessageWhatServerToString(I)Ljava/lang/String;
    .locals 0

    invoke-static {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->messageWhatServerToString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private constructor <init>()V
    .locals 5

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Lcom/miui/server/input/MiuiInputSettingsConnection$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiInputSettingsConnection$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mTimerRunnable:Ljava/lang/Runnable;

    .line 89
    new-instance v0, Lcom/miui/server/input/MiuiInputSettingsConnection$1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiInputSettingsConnection$1;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 104
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mContext:Landroid/content/Context;

    .line 105
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getThread()Lcom/android/server/input/MiuiInputThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mHandler:Landroid/os/Handler;

    .line 106
    new-instance v2, Landroid/os/Messenger;

    new-instance v3, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;

    .line 107
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getThread()Lcom/android/server/input/MiuiInputThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;Landroid/os/Looper;)V

    invoke-direct {v2, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMessenger:Landroid/os/Messenger;

    .line 108
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCachedMessageList:Ljava/util/Map;

    .line 109
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    .line 110
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 111
    .local v2, "filter":Landroid/content/IntentFilter;
    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    .line 112
    return-void
.end method

.method private executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V
    .locals 0
    .param p1, "callback"    # Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;
    .param p2, "status"    # I

    .line 315
    if-nez p1, :cond_0

    .line 316
    return-void

    .line 318
    :cond_0
    invoke-interface {p1, p2}, Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;->onMessageSendFinish(I)V

    .line 319
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/MiuiInputSettingsConnection;
    .locals 2

    .line 118
    sget-object v0, Lcom/miui/server/input/MiuiInputSettingsConnection;->sInstance:Lcom/miui/server/input/MiuiInputSettingsConnection;

    if-nez v0, :cond_1

    .line 119
    const-class v0, Lcom/miui/server/input/MiuiInputSettingsConnection;

    monitor-enter v0

    .line 120
    :try_start_0
    sget-object v1, Lcom/miui/server/input/MiuiInputSettingsConnection;->sInstance:Lcom/miui/server/input/MiuiInputSettingsConnection;

    if-nez v1, :cond_0

    .line 121
    new-instance v1, Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-direct {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;-><init>()V

    sput-object v1, Lcom/miui/server/input/MiuiInputSettingsConnection;->sInstance:Lcom/miui/server/input/MiuiInputSettingsConnection;

    .line 123
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 125
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/MiuiInputSettingsConnection;->sInstance:Lcom/miui/server/input/MiuiInputSettingsConnection;

    return-object v0
.end method

.method private static messageWhatClientToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "messageWhatClient"    # I

    .line 389
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    .line 390
    const-string v0, "MSG_CLIENT_TRIGGER_QUICK_NOTE"

    return-object v0

    .line 392
    :cond_0
    const-string v0, "NONE"

    return-object v0
.end method

.method private static messageWhatServerToString(I)Ljava/lang/String;
    .locals 1
    .param p0, "messageWhatServer"    # I

    .line 378
    packed-switch p0, :pswitch_data_0

    .line 384
    const-string v0, "NONE"

    return-object v0

    .line 382
    :pswitch_0
    const-string v0, "MSG_SERVER_REMOVE_STYLUS_MASK"

    return-object v0

    .line 380
    :pswitch_1
    const-string v0, "MSG_SERVER_ADD_STYLUS_MASK"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private removeTimer()V
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mTimerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 265
    return-void
.end method

.method private resetConnection()V
    .locals 2

    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request reset connection, connect = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiInputSettingsConnection"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    if-nez v0, :cond_1

    .line 273
    return-void

    .line 275
    :cond_1
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 277
    iput-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mMessenger:Landroid/os/Messenger;

    .line 278
    invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->removeTimer()V

    .line 279
    return-void
.end method

.method private resetTimer()V
    .locals 4

    .line 256
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mTimerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 257
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mTimerRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 258
    return-void
.end method


# virtual methods
.method public isBindToInputSettings()Z
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public registerCallbackListener(ILjava/util/function/Consumer;)V
    .locals 4
    .param p1, "messageWhatClient"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/function/Consumer<",
            "Landroid/os/Message;",
            ">;)V"
        }
    .end annotation

    .line 218
    .local p2, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Landroid/os/Message;>;"
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    monitor-enter v0

    .line 219
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const-string v1, "MiuiInputSettingsConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callback for what = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 221
    invoke-static {p1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->messageWhatClientToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", already registered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 220
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    monitor-exit v0

    return-void

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    monitor-exit v0

    .line 226
    return-void

    .line 225
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public sendMessageToInputSettings(I)V
    .locals 2
    .param p1, "messageWhatServer"    # I

    .line 135
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 136
    .local v0, "message":Landroid/os/Message;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(ILandroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V

    .line 137
    return-void
.end method

.method public sendMessageToInputSettings(II)V
    .locals 2
    .param p1, "messageWhatServer"    # I
    .param p2, "arg1"    # I

    .line 147
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 148
    .local v0, "message":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 149
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(ILandroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V

    .line 150
    return-void
.end method

.method public declared-synchronized sendMessageToInputSettings(ILandroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V
    .locals 6
    .param p1, "messageWhatServer"    # I
    .param p2, "message"    # Landroid/os/Message;
    .param p3, "callback"    # Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;

    monitor-enter p0

    .line 167
    :try_start_0
    iput p1, p2, Landroid/os/Message;->what:I

    .line 168
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMessenger:Landroid/os/Messenger;

    iput-object v0, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 169
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mMessenger:Landroid/os/Messenger;

    if-nez v0, :cond_1

    .line 171
    const-string v0, "com.miui.securitycore/com.miui.miinput.service.MiuiInputSettingsService"

    .line 172
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 173
    .local v0, "serviceComponent":Landroid/content/ComponentName;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 174
    .local v1, "serviceIntent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 175
    new-instance v2, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection-IA;)V

    .line 176
    .local v2, "connection":Landroid/content/ServiceConnection;
    iget-object v3, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mContext:Landroid/content/Context;

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const v5, 0x44000001    # 512.00006f

    invoke-virtual {v3, v1, v2, v5, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    const-string v3, "MiuiInputSettingsConnection"

    const-string/jumbo v4, "service bind success"

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iput-object v2, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    goto :goto_0

    .line 182
    .end local p0    # "this":Lcom/miui/server/input/MiuiInputSettingsConnection;
    :cond_0
    const-string v3, "MiuiInputSettingsConnection"

    const-string/jumbo v4, "service bind fail"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v3, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 186
    .end local v0    # "serviceComponent":Landroid/content/ComponentName;
    .end local v1    # "serviceIntent":Landroid/content/Intent;
    .end local v2    # "connection":Landroid/content/ServiceConnection;
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v1, 0x1

    if-nez v0, :cond_2

    .line 187
    invoke-direct {p0, p3, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 191
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mMessenger:Landroid/os/Messenger;

    if-nez v0, :cond_3

    .line 193
    const-string v0, "MiuiInputSettingsConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cache message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/os/Message;->what:I

    invoke-static {v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->messageWhatServerToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCachedMessageList:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    monitor-exit p0

    return-void

    .line 198
    :cond_3
    :try_start_2
    const-string v0, "MiuiInputSettingsConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "send message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p2, Landroid/os/Message;->what:I

    .line 199
    invoke-static {v3}, Lcom/miui/server/input/MiuiInputSettingsConnection;->messageWhatServerToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to settings"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 198
    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V

    .line 202
    invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetTimer()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 206
    goto :goto_1

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-direct {p0, p3, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V

    .line 205
    const-string v1, "MiuiInputSettingsConnection"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 207
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    monitor-exit p0

    return-void

    .line 166
    .end local p1    # "messageWhatServer":I
    .end local p2    # "message":Landroid/os/Message;
    .end local p3    # "callback":Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public unRegisterCallbackListener(I)V
    .locals 4
    .param p1, "messageWhatClient"    # I

    .line 235
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    monitor-enter v0

    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 237
    const-string v1, "MiuiInputSettingsConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callback for what = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 238
    invoke-static {p1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->messageWhatClientToString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", not registered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 237
    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    monitor-exit v0

    return-void

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection;->mCallbackMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    monitor-exit v0

    .line 243
    return-void

    .line 242
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
