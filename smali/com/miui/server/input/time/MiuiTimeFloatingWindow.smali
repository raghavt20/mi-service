.class public Lcom/miui/server/input/time/MiuiTimeFloatingWindow;
.super Ljava/lang/Object;
.source "MiuiTimeFloatingWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;,
        Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;
    }
.end annotation


# static fields
.field private static final LOG_PRINT_TIME_DIFF:J = 0x493e0L

.field private static final REASON_SCREEN_STATE_CHANGE:I = 0x0

.field private static final REASON_SETTINGS_CHANGE:I = 0x1

.field private static final SETTINGS_PACKAGE_NAME:Ljava/lang/String; = "com.android.settings"

.field private static final TAG:Ljava/lang/String; = "MiuiTimeFloatingWindow"


# instance fields
.field private mChoreographer:Landroid/view/Choreographer;

.field private final mContext:Landroid/content/Context;

.field private mDateTimeFormatter:Ljava/time/format/DateTimeFormatter;

.field private final mDrawCallBack:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mIsFirstScreenOn:Z

.field private mIsInit:Z

.field private mIsScreenOn:Z

.field private mIsTimeFloatingWindowOn:Z

.field private mLastTimeText:Ljava/lang/String;

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

.field private mRootView:Landroid/view/View;

.field private volatile mShowTime:Z

.field private mTimeTextView:Landroid/widget/TextView;

.field private mTimerForLog:J

.field private mTimezoneChangedIntentFilter:Landroid/content/IntentFilter;

.field private mTimezoneChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public static synthetic $r8$lambda$082IOALXwCxk0dYxMUH8cDowrZY(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->lambda$init$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$7d-1Bl5R4fatAr5-jYZB26galSE(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->initView()V

    return-void
.end method

.method public static synthetic $r8$lambda$NAPwmDa5tcXjuGdbkzdIH8Nr3WQ(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->lambda$showTimeFloatWindow$1(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$_2y-0NLewpnGIuY7XD9gmRzZJCE(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->lambda$hideTimeFloatWindow$2(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$mt6fKDEPqFBj2zIe084emgS1lEE(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->initLayoutParams()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmChoreographer(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Landroid/view/Choreographer;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mChoreographer:Landroid/view/Choreographer;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDrawCallBack(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mDrawCallBack:Ljava/lang/Runnable;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsTimeFloatingWindowOn(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmShowTime(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$mupdateDateTimeFormatter(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateDateTimeFormatter()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateLocation(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateLocation(II)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateTime(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateTime()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J

    .line 67
    new-instance v0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$1;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mDrawCallBack:Ljava/lang/Runnable;

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLastTimeText:Ljava/lang/String;

    .line 91
    iput-object p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    .line 92
    new-instance v0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;Landroid/os/Handler;)V

    .line 93
    .local v0, "miuiSettingsObserver":Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;
    invoke-virtual {v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;->observe()V

    .line 94
    return-void
.end method

.method private addRootViewToWindow(I)V
    .locals 5
    .param p1, "reason"    # I

    .line 159
    const-string v0, "MiuiTimeFloatingWindow"

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 160
    const-string v2, "Because settings state change, add window"

    invoke-static {v0, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    iget-object v4, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    :cond_0
    iget-boolean v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z

    if-nez v2, :cond_1

    goto :goto_0

    .line 166
    :cond_1
    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iput-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    .line 168
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mChoreographer:Landroid/view/Choreographer;

    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mDrawCallBack:Ljava/lang/Runnable;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v1, v4, v2, v3}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V

    .line 169
    const-string v1, "Time floating window show"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    return-void

    .line 164
    :cond_2
    :goto_0
    return-void
.end method

.method private getLayoutParamsY()I
    .locals 8

    .line 97
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 98
    .local v0, "textPaint":Landroid/graphics/Paint;
    new-instance v1, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v1}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    .line 99
    .local v1, "textMetrics":Landroid/graphics/Paint$FontMetricsInt;
    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 100
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    .line 101
    iget v2, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v3, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x2

    .line 102
    .local v2, "textMetricsHeight":I
    iget-object v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getMaximumWindowMetrics()Landroid/view/WindowMetrics;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/WindowMetrics;->getWindowInsets()Landroid/view/WindowInsets;

    move-result-object v3

    .line 103
    .local v3, "insets":Landroid/view/WindowInsets;
    if-nez v3, :cond_0

    .line 104
    return v2

    .line 107
    :cond_0
    const/4 v4, 0x0

    .line 108
    .local v4, "headerPaddingTop":I
    nop

    .line 109
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/WindowInsets;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v5

    .line 110
    .local v5, "topLeftRounded":Landroid/view/RoundedCorner;
    if-eqz v5, :cond_1

    .line 111
    invoke-virtual {v5}, Landroid/view/RoundedCorner;->getRadius()I

    move-result v4

    .line 114
    :cond_1
    nop

    .line 115
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/view/WindowInsets;->getRoundedCorner(I)Landroid/view/RoundedCorner;

    move-result-object v6

    .line 116
    .local v6, "topRightRounded":Landroid/view/RoundedCorner;
    if-eqz v6, :cond_2

    .line 117
    invoke-virtual {v6}, Landroid/view/RoundedCorner;->getRadius()I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 120
    :cond_2
    invoke-virtual {v3}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 121
    nop

    .line 122
    invoke-virtual {v3}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/DisplayCutout;->getSafeInsetTop()I

    move-result v7

    invoke-static {v4, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 125
    :cond_3
    add-int v7, v4, v2

    return v7
.end method

.method private init()V
    .locals 3

    .line 191
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    const-class v1, Landroid/view/WindowManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 192
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 193
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "TimeFloatingWindow"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 194
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 195
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mHandler:Landroid/os/Handler;

    .line 196
    new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 197
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 198
    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateDateTimeFormatter()V

    .line 199
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda4;

    invoke-direct {v2, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda4;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsInit:Z

    .line 201
    new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;

    invoke-direct {v1, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    iput-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    .line 213
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimezoneChangedIntentFilter:Landroid/content/IntentFilter;

    .line 214
    new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$3;

    invoke-direct {v1, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$3;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    iput-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimezoneChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 220
    return-void
.end method

.method private initLayoutParams()V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x128

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 132
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 134
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x7e2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 135
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->setTrustedOverlay()V

    .line 136
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 137
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 138
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 139
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->getLayoutParamsY()I

    move-result v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 140
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 141
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 142
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "MiuiTimeFloatingWindow"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 143
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x1000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 145
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 148
    :cond_0
    return-void
.end method

.method private initView()V
    .locals 4

    .line 151
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    const-class v1, Landroid/view/LayoutInflater;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 152
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v1, 0x110c0041

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    .line 153
    const v3, 0x110a004c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimeTextView:Landroid/widget/TextView;

    .line 154
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    new-instance v3, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;

    invoke-direct {v3, p0, v2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener-IA;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 155
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setForceDarkAllowed(Z)V

    .line 156
    return-void
.end method

.method private synthetic lambda$hideTimeFloatWindow$2(I)V
    .locals 0
    .param p1, "reason"    # I

    .line 240
    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->removeRootViewFromWindow(I)V

    return-void
.end method

.method private synthetic lambda$init$0()V
    .locals 1

    .line 199
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mChoreographer:Landroid/view/Choreographer;

    return-void
.end method

.method private synthetic lambda$showTimeFloatWindow$1(I)V
    .locals 0
    .param p1, "reason"    # I

    .line 231
    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->addRootViewToWindow(I)V

    return-void
.end method

.method private logForIsRunning()V
    .locals 4

    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 255
    .local v0, "now":J
    iget-wide v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 256
    const-string v2, "MiuiTimeFloatingWindow"

    const-string v3, "Time floating window is running."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-wide/32 v2, 0x493e0

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J

    .line 259
    :cond_0
    return-void
.end method

.method private reasonToString(I)Ljava/lang/String;
    .locals 1
    .param p1, "reason"    # I

    .line 441
    packed-switch p1, :pswitch_data_0

    .line 447
    const-string v0, "UNKNOWN"

    return-object v0

    .line 445
    :pswitch_0
    const-string v0, "SETTINGS_CHANGE"

    return-object v0

    .line 443
    :pswitch_1
    const-string v0, "SCREEN_STATE_CHANGE"

    return-object v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private registerReceivers()V
    .locals 6

    .line 290
    const-string v0, "MiuiTimeFloatingWindow"

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimezoneChangedReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimezoneChangedIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 291
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V

    .line 292
    const-string v1, "Register time zone and package monitor."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    goto :goto_0

    .line 293
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private removeRootViewFromWindow(I)V
    .locals 4
    .param p1, "reason"    # I

    .line 173
    const/4 v0, 0x1

    const-string v1, "MiuiTimeFloatingWindow"

    if-ne p1, v0, :cond_0

    .line 174
    const-string v0, "Because settings state change, remove window"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    invoke-interface {v0, v2}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 177
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    if-nez v0, :cond_1

    .line 178
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    .line 183
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J

    .line 184
    const-string v0, "Time floating window hide"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    return-void
.end method

.method private unregisterReceivers()V
    .locals 3

    .line 300
    const-string v0, "MiuiTimeFloatingWindow"

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimezoneChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 301
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mPackageMonitor:Lcom/android/internal/content/PackageMonitor;

    invoke-virtual {v1}, Lcom/android/internal/content/PackageMonitor;->unregister()V

    .line 302
    const-string v1, "Unregister time zone and package monitor."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    goto :goto_0

    .line 303
    :catch_0
    move-exception v1

    .line 304
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method private updateDateTimeFormatter()V
    .locals 2

    .line 283
    nop

    .line 284
    const-string v0, "HH:mm:ss"

    invoke-static {v0}, Ljava/time/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Ljava/time/format/DateTimeFormatter;

    move-result-object v0

    .line 285
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->toZoneId()Ljava/time/ZoneId;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/time/format/DateTimeFormatter;->withZone(Ljava/time/ZoneId;)Ljava/time/format/DateTimeFormatter;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mDateTimeFormatter:Ljava/time/format/DateTimeFormatter;

    .line 286
    return-void
.end method

.method private updateLocation(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .line 428
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    if-ne v0, p2, :cond_0

    .line 430
    return-void

    .line 432
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    if-nez v0, :cond_1

    .line 433
    return-void

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 436
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 437
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mRootView:Landroid/view/View;

    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 438
    return-void
.end method

.method private updateTime()V
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mDateTimeFormatter:Ljava/time/format/DateTimeFormatter;

    invoke-static {}, Ljava/time/Instant;->now()Ljava/time/Instant;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/time/format/DateTimeFormatter;->format(Ljava/time/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLastTimeText:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 247
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iput-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mLastTimeText:Ljava/lang/String;

    .line 250
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->logForIsRunning()V

    .line 251
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 452
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 453
    const-string v0, "MiuiTimeFloatingWindow"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mShowTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 455
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mIsTimeFloatingWindowOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mIsScreenOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 457
    return-void
.end method

.method public hideTimeFloatWindow(I)V
    .locals 2
    .param p1, "reason"    # I

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request hide time floating window because "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->reasonToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mShowTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsScreenOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiTimeFloatingWindow"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 241
    return-void
.end method

.method public showTimeFloatWindow(I)V
    .locals 2
    .param p1, "reason"    # I

    .line 226
    iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsInit:Z

    if-nez v0, :cond_0

    .line 227
    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->init()V

    .line 229
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request show time floating window because "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->reasonToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mShowTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsScreenOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiTimeFloatingWindow"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 232
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 3
    .param p1, "isScreenOn"    # Z

    .line 309
    iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z

    const-string v1, "MiuiTimeFloatingWindow"

    if-ne v0, p1, :cond_0

    .line 310
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The screen state not change, but receive the notify, now isScreenOn = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return-void

    .line 314
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z

    .line 315
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsFirstScreenOn:Z

    if-nez v0, :cond_1

    .line 317
    const-string v0, "First screen on let\'s read setting value"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-virtual {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateTimeFloatWindowState()V

    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsFirstScreenOn:Z

    .line 320
    return-void

    .line 322
    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z

    if-nez v0, :cond_2

    .line 324
    return-void

    .line 326
    :cond_2
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 327
    invoke-virtual {p0, v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->showTimeFloatWindow(I)V

    .line 328
    return-void

    .line 330
    :cond_3
    invoke-virtual {p0, v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->hideTimeFloatWindow(I)V

    .line 331
    return-void
.end method

.method public updateTimeFloatWindowState()V
    .locals 4

    .line 262
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "miui_time_floating_window"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 264
    .local v0, "newValue":I
    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v3, v1

    :cond_0
    move v2, v3

    .line 265
    .local v2, "newState":Z
    iget-boolean v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z

    if-ne v3, v2, :cond_1

    .line 266
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The setting value was not change, but receive the notify, new state is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "MiuiTimeFloatingWindow"

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    return-void

    .line 270
    :cond_1
    iput-boolean v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z

    .line 271
    if-eqz v2, :cond_2

    .line 272
    invoke-virtual {p0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->showTimeFloatWindow(I)V

    .line 274
    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->registerReceivers()V

    goto :goto_0

    .line 276
    :cond_2
    invoke-virtual {p0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->hideTimeFloatWindow(I)V

    .line 278
    invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->unregisterReceivers()V

    .line 280
    :goto_0
    return-void
.end method
