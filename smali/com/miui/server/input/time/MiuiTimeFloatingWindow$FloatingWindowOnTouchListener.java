class com.miui.server.input.time.MiuiTimeFloatingWindow$FloatingWindowOnTouchListener implements android.view.View$OnTouchListener {
	 /* .source "MiuiTimeFloatingWindow.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/time/MiuiTimeFloatingWindow; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "FloatingWindowOnTouchListener" */
} // .end annotation
/* # static fields */
private static final Integer MOVE_THRESHOLD;
/* # instance fields */
private final mLocationTemp;
protected Float mStartRawX;
protected Float mStartRawY;
protected Float mStartX;
protected Float mStartY;
final com.miui.server.input.time.MiuiTimeFloatingWindow this$0; //synthetic
/* # direct methods */
private com.miui.server.input.time.MiuiTimeFloatingWindow$FloatingWindowOnTouchListener ( ) {
/* .locals 0 */
/* .line 355 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 363 */
int p1 = 0; // const/4 p1, 0x0
/* iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F */
/* .line 367 */
/* iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F */
/* .line 371 */
/* iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartX:F */
/* .line 375 */
/* iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartY:F */
/* .line 379 */
int p1 = 2; // const/4 p1, 0x2
/* new-array p1, p1, [I */
this.mLocationTemp = p1;
return;
} // .end method
 com.miui.server.input.time.MiuiTimeFloatingWindow$FloatingWindowOnTouchListener ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
return;
} // .end method
private Boolean isClick ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 422 */
/* iget v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F */
v1 = (( android.view.MotionEvent ) p1 ).getRawX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F
/* sub-float/2addr v0, v1 */
v0 = java.lang.Math .abs ( v0 );
/* const/high16 v1, 0x40400000 # 3.0f */
/* cmpl-float v0, v0, v1 */
/* if-gtz v0, :cond_0 */
/* iget v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F */
/* .line 423 */
v2 = (( android.view.MotionEvent ) p1 ).getRawY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F
/* sub-float/2addr v0, v2 */
v0 = java.lang.Math .abs ( v0 );
/* cmpl-float v0, v0, v1 */
/* if-gtz v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 422 */
} // :goto_0
} // .end method
/* # virtual methods */
protected void actionMoveEvent ( android.view.MotionEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 413 */
/* iget v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartX:F */
v1 = (( android.view.MotionEvent ) p1 ).getRawX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F
/* iget v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F */
/* sub-float/2addr v1, v2 */
/* add-float/2addr v0, v1 */
/* float-to-int v0, v0 */
/* .line 414 */
/* .local v0, "newX":I */
/* iget v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartY:F */
v2 = (( android.view.MotionEvent ) p1 ).getRawY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F
/* iget v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F */
/* sub-float/2addr v2, v3 */
/* add-float/2addr v1, v2 */
/* float-to-int v1, v1 */
/* .line 415 */
/* .local v1, "newY":I */
v2 = this.this$0;
com.miui.server.input.time.MiuiTimeFloatingWindow .-$$Nest$mupdateLocation ( v2,v0,v1 );
/* .line 416 */
return;
} // .end method
public Boolean onTouch ( android.view.View p0, android.view.MotionEvent p1 ) {
/* .locals 3 */
/* .param p1, "v" # Landroid/view/View; */
/* .param p2, "event" # Landroid/view/MotionEvent; */
/* .line 383 */
v0 = (( android.view.MotionEvent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* .line 395 */
/* :pswitch_0 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->isClick(Landroid/view/MotionEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 396 */
/* .line 399 */
} // :cond_0
(( com.miui.server.input.time.MiuiTimeFloatingWindow$FloatingWindowOnTouchListener ) p0 ).actionMoveEvent ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->actionMoveEvent(Landroid/view/MotionEvent;)V
/* .line 400 */
/* .line 402 */
/* :pswitch_1 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->isClick(Landroid/view/MotionEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 403 */
(( android.view.View ) p1 ).performClick ( ); // invoke-virtual {p1}, Landroid/view/View;->performClick()Z
/* .line 386 */
/* :pswitch_2 */
v0 = (( android.view.MotionEvent ) p2 ).getRawX ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F
/* iput v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F */
/* .line 387 */
v0 = (( android.view.MotionEvent ) p2 ).getRawY ( ); // invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F
/* iput v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F */
/* .line 389 */
v0 = this.mLocationTemp;
(( android.view.View ) p1 ).getLocationOnScreen ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V
/* .line 390 */
v0 = this.mLocationTemp;
int v2 = 0; // const/4 v2, 0x0
/* aget v2, v0, v2 */
/* int-to-float v2, v2 */
/* iput v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartX:F */
/* .line 391 */
/* aget v0, v0, v1 */
/* int-to-float v0, v0 */
/* iput v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartY:F */
/* .line 392 */
/* nop */
/* .line 409 */
} // :cond_1
} // :goto_0
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
