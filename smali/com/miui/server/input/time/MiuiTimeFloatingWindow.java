public class com.miui.server.input.time.MiuiTimeFloatingWindow {
	 /* .source "MiuiTimeFloatingWindow.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;, */
	 /* Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Long LOG_PRINT_TIME_DIFF;
private static final Integer REASON_SCREEN_STATE_CHANGE;
private static final Integer REASON_SETTINGS_CHANGE;
private static final java.lang.String SETTINGS_PACKAGE_NAME;
private static final java.lang.String TAG;
/* # instance fields */
private android.view.Choreographer mChoreographer;
private final android.content.Context mContext;
private java.time.format.DateTimeFormatter mDateTimeFormatter;
private final java.lang.Runnable mDrawCallBack;
private android.os.Handler mHandler;
private Boolean mIsFirstScreenOn;
private Boolean mIsInit;
private Boolean mIsScreenOn;
private Boolean mIsTimeFloatingWindowOn;
private java.lang.String mLastTimeText;
private android.view.WindowManager$LayoutParams mLayoutParams;
private com.android.internal.content.PackageMonitor mPackageMonitor;
private android.view.View mRootView;
private volatile Boolean mShowTime;
private android.widget.TextView mTimeTextView;
private Long mTimerForLog;
private android.content.IntentFilter mTimezoneChangedIntentFilter;
private android.content.BroadcastReceiver mTimezoneChangedReceiver;
private android.view.WindowManager mWindowManager;
/* # direct methods */
public static void $r8$lambda$082IOALXwCxk0dYxMUH8cDowrZY ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->lambda$init$0()V */
	 return;
} // .end method
public static void $r8$lambda$7d-1Bl5R4fatAr5-jYZB26galSE ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->initView()V */
	 return;
} // .end method
public static void $r8$lambda$NAPwmDa5tcXjuGdbkzdIH8Nr3WQ ( com.miui.server.input.time.MiuiTimeFloatingWindow p0, Integer p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->lambda$showTimeFloatWindow$1(I)V */
	 return;
} // .end method
public static void $r8$lambda$_2y-0NLewpnGIuY7XD9gmRzZJCE ( com.miui.server.input.time.MiuiTimeFloatingWindow p0, Integer p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->lambda$hideTimeFloatWindow$2(I)V */
	 return;
} // .end method
public static void $r8$lambda$mt6fKDEPqFBj2zIe084emgS1lEE ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->initLayoutParams()V */
	 return;
} // .end method
static android.view.Choreographer -$$Nest$fgetmChoreographer ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mChoreographer;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static java.lang.Runnable -$$Nest$fgetmDrawCallBack ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDrawCallBack;
} // .end method
static Boolean -$$Nest$fgetmIsTimeFloatingWindowOn ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z */
} // .end method
static Boolean -$$Nest$fgetmShowTime ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
} // .end method
static void -$$Nest$mupdateDateTimeFormatter ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateDateTimeFormatter()V */
	 return;
} // .end method
static void -$$Nest$mupdateLocation ( com.miui.server.input.time.MiuiTimeFloatingWindow p0, Integer p1, Integer p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateLocation(II)V */
	 return;
} // .end method
static void -$$Nest$mupdateTime ( com.miui.server.input.time.MiuiTimeFloatingWindow p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateTime()V */
	 return;
} // .end method
public com.miui.server.input.time.MiuiTimeFloatingWindow ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 90 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 66 */
	 /* const-wide/16 v0, 0x0 */
	 /* iput-wide v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J */
	 /* .line 67 */
	 /* new-instance v0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$1; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$1;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
	 this.mDrawCallBack = v0;
	 /* .line 87 */
	 final String v0 = ""; // const-string v0, ""
	 this.mLastTimeText = v0;
	 /* .line 91 */
	 this.mContext = p1;
	 /* .line 92 */
	 /* new-instance v0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver; */
	 com.android.server.input.MiuiInputThread .getHandler ( );
	 /* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;Landroid/os/Handler;)V */
	 /* .line 93 */
	 /* .local v0, "miuiSettingsObserver":Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver; */
	 (( com.miui.server.input.time.MiuiTimeFloatingWindow$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;->observe()V
	 /* .line 94 */
	 return;
} // .end method
private void addRootViewToWindow ( Integer p0 ) {
	 /* .locals 5 */
	 /* .param p1, "reason" # I */
	 /* .line 159 */
	 final String v0 = "MiuiTimeFloatingWindow"; // const-string v0, "MiuiTimeFloatingWindow"
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-ne p1, v1, :cond_0 */
	 /* .line 160 */
	 final String v2 = "Because settings state change, add window"; // const-string v2, "Because settings state change, add window"
	 android.util.Slog .i ( v0,v2 );
	 /* .line 161 */
	 v2 = this.mWindowManager;
	 v3 = this.mRootView;
	 v4 = this.mLayoutParams;
	 /* .line 163 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
/* if-nez v2, :cond_2 */
/* iget-boolean v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z */
/* if-nez v2, :cond_1 */
/* .line 166 */
} // :cond_1
v2 = this.mRootView;
int v3 = 0; // const/4 v3, 0x0
(( android.view.View ) v2 ).setVisibility ( v3 ); // invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V
/* .line 167 */
/* iput-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
/* .line 168 */
v1 = this.mChoreographer;
v2 = this.mDrawCallBack;
int v3 = 0; // const/4 v3, 0x0
int v4 = 3; // const/4 v4, 0x3
(( android.view.Choreographer ) v1 ).postCallback ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Landroid/view/Choreographer;->postCallback(ILjava/lang/Runnable;Ljava/lang/Object;)V
/* .line 169 */
final String v1 = "Time floating window show"; // const-string v1, "Time floating window show"
android.util.Slog .i ( v0,v1 );
/* .line 170 */
return;
/* .line 164 */
} // :cond_2
} // :goto_0
return;
} // .end method
private Integer getLayoutParamsY ( ) {
/* .locals 8 */
/* .line 97 */
/* new-instance v0, Landroid/graphics/Paint; */
/* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
/* .line 98 */
/* .local v0, "textPaint":Landroid/graphics/Paint; */
/* new-instance v1, Landroid/graphics/Paint$FontMetricsInt; */
/* invoke-direct {v1}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V */
/* .line 99 */
/* .local v1, "textMetrics":Landroid/graphics/Paint$FontMetricsInt; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getDisplayMetrics ( ); // invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v2, v2, Landroid/util/DisplayMetrics;->density:F */
/* const/high16 v3, 0x41200000 # 10.0f */
/* mul-float/2addr v2, v3 */
(( android.graphics.Paint ) v0 ).setTextSize ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V
/* .line 100 */
(( android.graphics.Paint ) v0 ).getFontMetricsInt ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I
/* .line 101 */
/* iget v2, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I */
/* iget v3, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I */
/* sub-int/2addr v2, v3 */
/* add-int/lit8 v2, v2, 0x2 */
/* .line 102 */
/* .local v2, "textMetricsHeight":I */
v3 = this.mWindowManager;
(( android.view.WindowMetrics ) v3 ).getWindowInsets ( ); // invoke-virtual {v3}, Landroid/view/WindowMetrics;->getWindowInsets()Landroid/view/WindowInsets;
/* .line 103 */
/* .local v3, "insets":Landroid/view/WindowInsets; */
/* if-nez v3, :cond_0 */
/* .line 104 */
/* .line 107 */
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .line 108 */
/* .local v4, "headerPaddingTop":I */
/* nop */
/* .line 109 */
int v5 = 0; // const/4 v5, 0x0
(( android.view.WindowInsets ) v3 ).getRoundedCorner ( v5 ); // invoke-virtual {v3, v5}, Landroid/view/WindowInsets;->getRoundedCorner(I)Landroid/view/RoundedCorner;
/* .line 110 */
/* .local v5, "topLeftRounded":Landroid/view/RoundedCorner; */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 111 */
v4 = (( android.view.RoundedCorner ) v5 ).getRadius ( ); // invoke-virtual {v5}, Landroid/view/RoundedCorner;->getRadius()I
/* .line 114 */
} // :cond_1
/* nop */
/* .line 115 */
int v6 = 1; // const/4 v6, 0x1
(( android.view.WindowInsets ) v3 ).getRoundedCorner ( v6 ); // invoke-virtual {v3, v6}, Landroid/view/WindowInsets;->getRoundedCorner(I)Landroid/view/RoundedCorner;
/* .line 116 */
/* .local v6, "topRightRounded":Landroid/view/RoundedCorner; */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 117 */
v7 = (( android.view.RoundedCorner ) v6 ).getRadius ( ); // invoke-virtual {v6}, Landroid/view/RoundedCorner;->getRadius()I
v4 = java.lang.Math .max ( v4,v7 );
/* .line 120 */
} // :cond_2
(( android.view.WindowInsets ) v3 ).getDisplayCutout ( ); // invoke-virtual {v3}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;
if ( v7 != null) { // if-eqz v7, :cond_3
/* .line 121 */
/* nop */
/* .line 122 */
(( android.view.WindowInsets ) v3 ).getDisplayCutout ( ); // invoke-virtual {v3}, Landroid/view/WindowInsets;->getDisplayCutout()Landroid/view/DisplayCutout;
v7 = (( android.view.DisplayCutout ) v7 ).getSafeInsetTop ( ); // invoke-virtual {v7}, Landroid/view/DisplayCutout;->getSafeInsetTop()I
v4 = java.lang.Math .max ( v4,v7 );
/* .line 125 */
} // :cond_3
/* add-int v7, v4, v2 */
} // .end method
private void init ( ) {
/* .locals 3 */
/* .line 191 */
v0 = this.mContext;
/* const-class v1, Landroid/view/WindowManager; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/WindowManager; */
this.mWindowManager = v0;
/* .line 192 */
/* new-instance v0, Landroid/view/WindowManager$LayoutParams; */
/* invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V */
this.mLayoutParams = v0;
/* .line 193 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "TimeFloatingWindow"; // const-string v1, "TimeFloatingWindow"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
/* .line 194 */
/* .local v0, "handlerThread":Landroid/os/HandlerThread; */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 195 */
/* new-instance v1, Landroid/os/Handler; */
(( android.os.HandlerThread ) v0 ).getLooper ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 196 */
/* new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 197 */
v1 = this.mHandler;
/* new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda3; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 198 */
/* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateDateTimeFormatter()V */
/* .line 199 */
v1 = this.mHandler;
/* new-instance v2, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda4; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda4;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 200 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsInit:Z */
/* .line 201 */
/* new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
this.mPackageMonitor = v1;
/* .line 213 */
/* new-instance v1, Landroid/content/IntentFilter; */
final String v2 = "android.intent.action.TIMEZONE_CHANGED"; // const-string v2, "android.intent.action.TIMEZONE_CHANGED"
/* invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
this.mTimezoneChangedIntentFilter = v1;
/* .line 214 */
/* new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$3; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$3;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V */
this.mTimezoneChangedReceiver = v1;
/* .line 220 */
return;
} // .end method
private void initLayoutParams ( ) {
/* .locals 3 */
/* .line 129 */
v0 = this.mLayoutParams;
/* const/16 v1, 0x128 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 132 */
v0 = this.mLayoutParams;
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 134 */
v0 = this.mLayoutParams;
/* const/16 v1, 0x7e2 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 135 */
v0 = this.mLayoutParams;
(( android.view.WindowManager$LayoutParams ) v0 ).setTrustedOverlay ( ); // invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->setTrustedOverlay()V
/* .line 136 */
v0 = this.mLayoutParams;
int v1 = -3; // const/4 v1, -0x3
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I */
/* .line 137 */
v0 = this.mLayoutParams;
/* const v1, 0x800033 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 138 */
v0 = this.mLayoutParams;
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I */
/* .line 139 */
v0 = this.mLayoutParams;
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->getLayoutParamsY()I */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I */
/* .line 140 */
v0 = this.mLayoutParams;
int v1 = -2; // const/4 v1, -0x2
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I */
/* .line 141 */
v0 = this.mLayoutParams;
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I */
/* .line 142 */
v0 = this.mLayoutParams;
final String v1 = "MiuiTimeFloatingWindow"; // const-string v1, "MiuiTimeFloatingWindow"
(( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 143 */
v0 = android.app.ActivityManager .isHighEndGfx ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 144 */
v0 = this.mLayoutParams;
/* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v2, 0x1000000 */
/* or-int/2addr v1, v2 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 145 */
v0 = this.mLayoutParams;
/* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit8 v1, v1, 0x2 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 148 */
} // :cond_0
return;
} // .end method
private void initView ( ) {
/* .locals 4 */
/* .line 151 */
v0 = this.mContext;
/* const-class v1, Landroid/view/LayoutInflater; */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/LayoutInflater; */
/* .line 152 */
/* .local v0, "layoutInflater":Landroid/view/LayoutInflater; */
/* const v1, 0x110c0041 */
int v2 = 0; // const/4 v2, 0x0
(( android.view.LayoutInflater ) v0 ).inflate ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
this.mRootView = v1;
/* .line 153 */
/* const v3, 0x110a004c */
(( android.view.View ) v1 ).findViewById ( v3 ); // invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;
/* check-cast v1, Landroid/widget/TextView; */
this.mTimeTextView = v1;
/* .line 154 */
v1 = this.mRootView;
/* new-instance v3, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener; */
/* invoke-direct {v3, p0, v2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener-IA;)V */
(( android.view.View ) v1 ).setOnTouchListener ( v3 ); // invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V
/* .line 155 */
v1 = this.mRootView;
int v2 = 0; // const/4 v2, 0x0
(( android.view.View ) v1 ).setForceDarkAllowed ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/View;->setForceDarkAllowed(Z)V
/* .line 156 */
return;
} // .end method
private void lambda$hideTimeFloatWindow$2 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "reason" # I */
/* .line 240 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->removeRootViewFromWindow(I)V */
return;
} // .end method
private void lambda$init$0 ( ) { //synthethic
/* .locals 1 */
/* .line 199 */
android.view.Choreographer .getInstance ( );
this.mChoreographer = v0;
return;
} // .end method
private void lambda$showTimeFloatWindow$1 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "reason" # I */
/* .line 231 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->addRootViewToWindow(I)V */
return;
} // .end method
private void logForIsRunning ( ) {
/* .locals 4 */
/* .line 254 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* .line 255 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J */
/* cmp-long v2, v0, v2 */
/* if-lez v2, :cond_0 */
/* .line 256 */
final String v2 = "MiuiTimeFloatingWindow"; // const-string v2, "MiuiTimeFloatingWindow"
final String v3 = "Time floating window is running."; // const-string v3, "Time floating window is running."
android.util.Slog .i ( v2,v3 );
/* .line 257 */
/* const-wide/32 v2, 0x493e0 */
/* add-long/2addr v2, v0 */
/* iput-wide v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J */
/* .line 259 */
} // :cond_0
return;
} // .end method
private java.lang.String reasonToString ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "reason" # I */
/* .line 441 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 447 */
final String v0 = "UNKNOWN"; // const-string v0, "UNKNOWN"
/* .line 445 */
/* :pswitch_0 */
final String v0 = "SETTINGS_CHANGE"; // const-string v0, "SETTINGS_CHANGE"
/* .line 443 */
/* :pswitch_1 */
final String v0 = "SCREEN_STATE_CHANGE"; // const-string v0, "SCREEN_STATE_CHANGE"
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void registerReceivers ( ) {
/* .locals 6 */
/* .line 290 */
final String v0 = "MiuiTimeFloatingWindow"; // const-string v0, "MiuiTimeFloatingWindow"
try { // :try_start_0
v1 = this.mContext;
v2 = this.mTimezoneChangedReceiver;
v3 = this.mTimezoneChangedIntentFilter;
(( android.content.Context ) v1 ).registerReceiver ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 291 */
v1 = this.mPackageMonitor;
v2 = this.mContext;
v3 = this.mHandler;
(( android.os.Handler ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
v4 = android.os.UserHandle.CURRENT;
int v5 = 1; // const/4 v5, 0x1
(( com.android.internal.content.PackageMonitor ) v1 ).register ( v2, v3, v4, v5 ); // invoke-virtual {v1, v2, v3, v4, v5}, Lcom/android/internal/content/PackageMonitor;->register(Landroid/content/Context;Landroid/os/Looper;Landroid/os/UserHandle;Z)V
/* .line 292 */
final String v1 = "Register time zone and package monitor."; // const-string v1, "Register time zone and package monitor."
android.util.Slog .i ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 295 */
/* .line 293 */
/* :catch_0 */
/* move-exception v1 */
/* .line 294 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 296 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void removeRootViewFromWindow ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "reason" # I */
/* .line 173 */
int v0 = 1; // const/4 v0, 0x1
final String v1 = "MiuiTimeFloatingWindow"; // const-string v1, "MiuiTimeFloatingWindow"
/* if-ne p1, v0, :cond_0 */
/* .line 174 */
final String v0 = "Because settings state change, remove window"; // const-string v0, "Because settings state change, remove window"
android.util.Slog .i ( v1,v0 );
/* .line 175 */
v0 = this.mWindowManager;
v2 = this.mRootView;
/* .line 177 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
/* if-nez v0, :cond_1 */
/* .line 178 */
return;
/* .line 180 */
} // :cond_1
v0 = this.mRootView;
/* const/16 v2, 0x8 */
(( android.view.View ) v0 ).setVisibility ( v2 ); // invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V
/* .line 181 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
/* .line 183 */
/* const-wide/16 v2, 0x0 */
/* iput-wide v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mTimerForLog:J */
/* .line 184 */
final String v0 = "Time floating window hide"; // const-string v0, "Time floating window hide"
android.util.Slog .i ( v1,v0 );
/* .line 185 */
return;
} // .end method
private void unregisterReceivers ( ) {
/* .locals 3 */
/* .line 300 */
final String v0 = "MiuiTimeFloatingWindow"; // const-string v0, "MiuiTimeFloatingWindow"
try { // :try_start_0
v1 = this.mContext;
v2 = this.mTimezoneChangedReceiver;
(( android.content.Context ) v1 ).unregisterReceiver ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
/* .line 301 */
v1 = this.mPackageMonitor;
(( com.android.internal.content.PackageMonitor ) v1 ).unregister ( ); // invoke-virtual {v1}, Lcom/android/internal/content/PackageMonitor;->unregister()V
/* .line 302 */
final String v1 = "Unregister time zone and package monitor."; // const-string v1, "Unregister time zone and package monitor."
android.util.Slog .i ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 305 */
/* .line 303 */
/* :catch_0 */
/* move-exception v1 */
/* .line 304 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;
android.util.Slog .w ( v0,v2 );
/* .line 306 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
private void updateDateTimeFormatter ( ) {
/* .locals 2 */
/* .line 283 */
/* nop */
/* .line 284 */
final String v0 = "HH:mm:ss"; // const-string v0, "HH:mm:ss"
java.time.format.DateTimeFormatter .ofPattern ( v0 );
/* .line 285 */
java.util.TimeZone .getDefault ( );
(( java.util.TimeZone ) v1 ).toZoneId ( ); // invoke-virtual {v1}, Ljava/util/TimeZone;->toZoneId()Ljava/time/ZoneId;
(( java.time.format.DateTimeFormatter ) v0 ).withZone ( v1 ); // invoke-virtual {v0, v1}, Ljava/time/format/DateTimeFormatter;->withZone(Ljava/time/ZoneId;)Ljava/time/format/DateTimeFormatter;
this.mDateTimeFormatter = v0;
/* .line 286 */
return;
} // .end method
private void updateLocation ( Integer p0, Integer p1 ) {
/* .locals 3 */
/* .param p1, "x" # I */
/* .param p2, "y" # I */
/* .line 428 */
v0 = this.mLayoutParams;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I */
/* if-ne v0, p1, :cond_0 */
v0 = this.mLayoutParams;
/* iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I */
/* if-ne v0, p2, :cond_0 */
/* .line 430 */
return;
/* .line 432 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
/* if-nez v0, :cond_1 */
/* .line 433 */
return;
/* .line 435 */
} // :cond_1
v0 = this.mLayoutParams;
/* iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I */
/* .line 436 */
v0 = this.mLayoutParams;
/* iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I */
/* .line 437 */
v0 = this.mWindowManager;
v1 = this.mRootView;
v2 = this.mLayoutParams;
/* .line 438 */
return;
} // .end method
private void updateTime ( ) {
/* .locals 2 */
/* .line 245 */
v0 = this.mDateTimeFormatter;
java.time.Instant .now ( );
(( java.time.format.DateTimeFormatter ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/time/format/DateTimeFormatter;->format(Ljava/time/temporal/TemporalAccessor;)Ljava/lang/String;
/* .line 246 */
/* .local v0, "time":Ljava/lang/String; */
v1 = this.mLastTimeText;
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_0 */
/* .line 247 */
v1 = this.mTimeTextView;
(( android.widget.TextView ) v1 ).setText ( v0 ); // invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
/* .line 248 */
this.mLastTimeText = v0;
/* .line 250 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->logForIsRunning()V */
/* .line 251 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 452 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 453 */
final String v0 = "MiuiTimeFloatingWindow"; // const-string v0, "MiuiTimeFloatingWindow"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 454 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mShowTime = "; // const-string v1, "mShowTime = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 455 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mIsTimeFloatingWindowOn = "; // const-string v1, "mIsTimeFloatingWindowOn = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 456 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "mIsScreenOn = "; // const-string v1, "mIsScreenOn = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 457 */
return;
} // .end method
public void hideTimeFloatWindow ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "reason" # I */
/* .line 238 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Request hide time floating window because "; // const-string v1, "Request hide time floating window because "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->reasonToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mShowTime = "; // const-string v1, " mShowTime = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mIsScreenOn = "; // const-string v1, " mIsScreenOn = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiTimeFloatingWindow"; // const-string v1, "MiuiTimeFloatingWindow"
android.util.Slog .i ( v1,v0 );
/* .line 240 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 241 */
return;
} // .end method
public void showTimeFloatWindow ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "reason" # I */
/* .line 226 */
/* iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsInit:Z */
/* if-nez v0, :cond_0 */
/* .line 227 */
/* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->init()V */
/* .line 229 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Request show time floating window because "; // const-string v1, "Request show time floating window because "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->reasonToString(I)Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " mShowTime = "; // const-string v1, " mShowTime = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mShowTime:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = " mIsScreenOn = "; // const-string v1, " mIsScreenOn = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiTimeFloatingWindow"; // const-string v1, "MiuiTimeFloatingWindow"
android.util.Slog .i ( v1,v0 );
/* .line 231 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 232 */
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "isScreenOn" # Z */
/* .line 309 */
/* iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z */
final String v1 = "MiuiTimeFloatingWindow"; // const-string v1, "MiuiTimeFloatingWindow"
/* if-ne v0, p1, :cond_0 */
/* .line 310 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "The screen state not change, but receive the notify, now isScreenOn = "; // const-string v2, "The screen state not change, but receive the notify, now isScreenOn = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 312 */
return;
/* .line 314 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsScreenOn:Z */
/* .line 315 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsFirstScreenOn:Z */
/* if-nez v0, :cond_1 */
/* .line 317 */
final String v0 = "First screen on let\'s read setting value"; // const-string v0, "First screen on let\'s read setting value"
android.util.Slog .i ( v1,v0 );
/* .line 318 */
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) p0 ).updateTimeFloatWindowState ( ); // invoke-virtual {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateTimeFloatWindowState()V
/* .line 319 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsFirstScreenOn:Z */
/* .line 320 */
return;
/* .line 322 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z */
/* if-nez v0, :cond_2 */
/* .line 324 */
return;
/* .line 326 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_3
/* .line 327 */
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) p0 ).showTimeFloatWindow ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->showTimeFloatWindow(I)V
/* .line 328 */
return;
/* .line 330 */
} // :cond_3
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) p0 ).hideTimeFloatWindow ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->hideTimeFloatWindow(I)V
/* .line 331 */
return;
} // .end method
public void updateTimeFloatWindowState ( ) {
/* .locals 4 */
/* .line 262 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "miui_time_floating_window"; // const-string v2, "miui_time_floating_window"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$System .getIntForUser ( v0,v2,v3,v1 );
/* .line 264 */
/* .local v0, "newValue":I */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v3, v1 */
} // :cond_0
/* move v2, v3 */
/* .line 265 */
/* .local v2, "newState":Z */
/* iget-boolean v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z */
/* if-ne v3, v2, :cond_1 */
/* .line 266 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "The setting value was not change, but receive the notify, new state is "; // const-string v3, "The setting value was not change, but receive the notify, new state is "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiTimeFloatingWindow"; // const-string v3, "MiuiTimeFloatingWindow"
android.util.Slog .w ( v3,v1 );
/* .line 268 */
return;
/* .line 270 */
} // :cond_1
/* iput-boolean v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->mIsTimeFloatingWindowOn:Z */
/* .line 271 */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 272 */
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) p0 ).showTimeFloatWindow ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->showTimeFloatWindow(I)V
/* .line 274 */
/* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->registerReceivers()V */
/* .line 276 */
} // :cond_2
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) p0 ).hideTimeFloatWindow ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->hideTimeFloatWindow(I)V
/* .line 278 */
/* invoke-direct {p0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->unregisterReceivers()V */
/* .line 280 */
} // :goto_0
return;
} // .end method
