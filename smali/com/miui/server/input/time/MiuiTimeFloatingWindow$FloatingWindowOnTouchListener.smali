.class Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;
.super Ljava/lang/Object;
.source "MiuiTimeFloatingWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/time/MiuiTimeFloatingWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FloatingWindowOnTouchListener"
.end annotation


# static fields
.field private static final MOVE_THRESHOLD:I = 0x3


# instance fields
.field private final mLocationTemp:[I

.field protected mStartRawX:F

.field protected mStartRawY:F

.field protected mStartX:F

.field protected mStartY:F

.field final synthetic this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;


# direct methods
.method private constructor <init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0

    .line 355
    iput-object p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363
    const/4 p1, 0x0

    iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F

    .line 367
    iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F

    .line 371
    iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartX:F

    .line 375
    iput p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartY:F

    .line 379
    const/4 p1, 0x2

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mLocationTemp:[I

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;-><init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V

    return-void
.end method

.method private isClick(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 422
    iget v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F

    .line 423
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 422
    :goto_0
    return v0
.end method


# virtual methods
.method protected actionMoveEvent(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 413
    iget v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    iget v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 414
    .local v0, "newX":I
    iget v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget v3, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F

    sub-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 415
    .local v1, "newY":I
    iget-object v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-static {v2, v0, v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->-$$Nest$mupdateLocation(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;II)V

    .line 416
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 383
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 395
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->isClick(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    goto :goto_0

    .line 399
    :cond_0
    invoke-virtual {p0, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->actionMoveEvent(Landroid/view/MotionEvent;)V

    .line 400
    goto :goto_0

    .line 402
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->isClick(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 403
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    goto :goto_0

    .line 386
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawX:F

    .line 387
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartRawY:F

    .line 389
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mLocationTemp:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 390
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mLocationTemp:[I

    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartX:F

    .line 391
    aget v0, v0, v1

    int-to-float v0, v0

    iput v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$FloatingWindowOnTouchListener;->mStartY:F

    .line 392
    nop

    .line 409
    :cond_1
    :goto_0
    return v1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
