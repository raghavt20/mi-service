class com.miui.server.input.time.MiuiTimeFloatingWindow$2 extends com.android.internal.content.PackageMonitor {
	 /* .source "MiuiTimeFloatingWindow.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->init()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.time.MiuiTimeFloatingWindow this$0; //synthetic
/* # direct methods */
 com.miui.server.input.time.MiuiTimeFloatingWindow$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/time/MiuiTimeFloatingWindow; */
/* .line 201 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onPackageDataCleared ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 204 */
final String v0 = "com.android.settings"; // const-string v0, "com.android.settings"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 205 */
return;
/* .line 207 */
} // :cond_0
final String v0 = "MiuiTimeFloatingWindow"; // const-string v0, "MiuiTimeFloatingWindow"
/* const-string/jumbo v1, "settings data was cleared, write current value." */
android.util.Slog .i ( v0,v1 );
/* .line 208 */
v0 = this.this$0;
com.miui.server.input.time.MiuiTimeFloatingWindow .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 210 */
v1 = this.this$0;
v1 = com.miui.server.input.time.MiuiTimeFloatingWindow .-$$Nest$fgetmIsTimeFloatingWindowOn ( v1 );
/* .line 208 */
final String v2 = "miui_time_floating_window"; // const-string v2, "miui_time_floating_window"
int v3 = -2; // const/4 v3, -0x2
android.provider.Settings$System .putIntForUser ( v0,v2,v1,v3 );
/* .line 211 */
return;
} // .end method
