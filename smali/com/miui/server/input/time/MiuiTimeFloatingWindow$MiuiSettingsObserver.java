class com.miui.server.input.time.MiuiTimeFloatingWindow$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiTimeFloatingWindow.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/time/MiuiTimeFloatingWindow; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.time.MiuiTimeFloatingWindow this$0; //synthetic
/* # direct methods */
public com.miui.server.input.time.MiuiTimeFloatingWindow$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 335 */
this.this$0 = p1;
/* .line 336 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 337 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 340 */
v0 = this.this$0;
com.miui.server.input.time.MiuiTimeFloatingWindow .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 341 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* nop */
/* .line 342 */
final String v1 = "miui_time_floating_window"; // const-string v1, "miui_time_floating_window"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 341 */
int v2 = 0; // const/4 v2, 0x0
int v3 = -2; // const/4 v3, -0x2
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 344 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 348 */
v0 = this.this$0;
(( com.miui.server.input.time.MiuiTimeFloatingWindow ) v0 ).updateTimeFloatWindowState ( ); // invoke-virtual {v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateTimeFloatWindowState()V
/* .line 349 */
return;
} // .end method
