.class Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiTimeFloatingWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/time/MiuiTimeFloatingWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 335
    iput-object p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    .line 336
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 337
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 340
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-static {v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->-$$Nest$fgetmContext(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 341
    .local v0, "resolver":Landroid/content/ContentResolver;
    nop

    .line 342
    const-string v1, "miui_time_floating_window"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 341
    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 344
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 348
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-virtual {v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->updateTimeFloatWindowState()V

    .line 349
    return-void
.end method
