.class Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;
.super Lcom/android/internal/content/PackageMonitor;
.source "MiuiTimeFloatingWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;


# direct methods
.method constructor <init>(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    .line 201
    iput-object p1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-direct {p0}, Lcom/android/internal/content/PackageMonitor;-><init>()V

    return-void
.end method


# virtual methods
.method public onPackageDataCleared(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 204
    const-string v0, "com.android.settings"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    return-void

    .line 207
    :cond_0
    const-string v0, "MiuiTimeFloatingWindow"

    const-string/jumbo v1, "settings data was cleared, write current value."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v0, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-static {v0}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->-$$Nest$fgetmContext(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/miui/server/input/time/MiuiTimeFloatingWindow$2;->this$0:Lcom/miui/server/input/time/MiuiTimeFloatingWindow;

    invoke-static {v1}, Lcom/miui/server/input/time/MiuiTimeFloatingWindow;->-$$Nest$fgetmIsTimeFloatingWindowOn(Lcom/miui/server/input/time/MiuiTimeFloatingWindow;)Z

    move-result v1

    .line 208
    const-string v2, "miui_time_floating_window"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 211
    return-void
.end method
