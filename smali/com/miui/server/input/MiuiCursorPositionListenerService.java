public class com.miui.server.input.MiuiCursorPositionListenerService implements android.view.WindowManagerPolicyConstants$PointerEventListener {
	 /* .source "MiuiCursorPositionListenerService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static volatile com.miui.server.input.MiuiCursorPositionListenerService sInstance;
/* # instance fields */
private final java.util.List mOnCursorPositionChangedListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.List mTempOnCursorPositionChangedListeners;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final com.android.server.wm.WindowManagerService mWindowManagerService;
/* # direct methods */
static com.miui.server.input.MiuiCursorPositionListenerService ( ) {
/* .locals 1 */
/* .line 17 */
/* const-class v0, Lcom/miui/server/input/MiuiCursorPositionListenerService; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
return;
} // .end method
private com.miui.server.input.MiuiCursorPositionListenerService ( ) {
/* .locals 1 */
/* .line 23 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 24 */
/* nop */
/* .line 25 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v0, Lcom/android/server/wm/WindowManagerService; */
this.mWindowManagerService = v0;
/* .line 26 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mOnCursorPositionChangedListeners = v0;
/* .line 27 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mTempOnCursorPositionChangedListeners = v0;
/* .line 28 */
return;
} // .end method
private void deliverCursorPositionChangedEvent ( Integer p0, Float p1, Float p2 ) {
/* .locals 3 */
/* .param p1, "deviceId" # I */
/* .param p2, "x" # F */
/* .param p3, "y" # F */
/* .line 88 */
v0 = this.mTempOnCursorPositionChangedListeners;
/* .line 89 */
v0 = this.mOnCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 90 */
try { // :try_start_0
v1 = this.mTempOnCursorPositionChangedListeners;
v2 = this.mOnCursorPositionChangedListeners;
/* .line 91 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 92 */
v0 = this.mTempOnCursorPositionChangedListeners;
/* new-instance v1, Lcom/miui/server/input/MiuiCursorPositionListenerService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1, p2, p3}, Lcom/miui/server/input/MiuiCursorPositionListenerService$$ExternalSyntheticLambda0;-><init>(IFF)V */
/* .line 94 */
return;
/* .line 91 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public static com.miui.server.input.MiuiCursorPositionListenerService getInstance ( ) {
/* .locals 2 */
/* .line 31 */
v0 = com.miui.server.input.MiuiCursorPositionListenerService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 32 */
/* const-class v0, Lcom/miui/server/input/MiuiCursorPositionListenerService; */
/* monitor-enter v0 */
/* .line 33 */
try { // :try_start_0
v1 = com.miui.server.input.MiuiCursorPositionListenerService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 34 */
/* new-instance v1, Lcom/miui/server/input/MiuiCursorPositionListenerService; */
/* invoke-direct {v1}, Lcom/miui/server/input/MiuiCursorPositionListenerService;-><init>()V */
/* .line 36 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 38 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.MiuiCursorPositionListenerService.sInstance;
} // .end method
static void lambda$deliverCursorPositionChangedEvent$0 ( Integer p0, Float p1, Float p2, com.miui.server.input.MiuiCursorPositionListenerService$OnCursorPositionChangedListener p3 ) { //synthethic
/* .locals 0 */
/* .param p0, "deviceId" # I */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .param p3, "listener" # Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener; */
/* .line 93 */
return;
} // .end method
private void populatePointerListenerLocked ( ) {
/* .locals 2 */
/* .line 72 */
v0 = v0 = this.mOnCursorPositionChangedListeners;
int v1 = 1; // const/4 v1, 0x1
/* if-eq v0, v1, :cond_0 */
/* .line 73 */
return;
/* .line 75 */
} // :cond_0
v0 = com.miui.server.input.MiuiCursorPositionListenerService.TAG;
final String v1 = "register pointer event listener to wms"; // const-string v1, "register pointer event listener to wms"
android.util.Slog .d ( v0,v1 );
/* .line 76 */
v0 = this.mWindowManagerService;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.WindowManagerService ) v0 ).registerPointerEventListener ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 77 */
return;
} // .end method
private void unPopulatePointerListenerLocked ( ) {
/* .locals 2 */
/* .line 80 */
v0 = v0 = this.mOnCursorPositionChangedListeners;
/* if-nez v0, :cond_0 */
/* .line 81 */
return;
/* .line 83 */
} // :cond_0
v0 = com.miui.server.input.MiuiCursorPositionListenerService.TAG;
/* const-string/jumbo v1, "unregister pointer event listener to wms" */
android.util.Slog .d ( v0,v1 );
/* .line 84 */
v0 = this.mWindowManagerService;
int v1 = 0; // const/4 v1, 0x0
(( com.android.server.wm.WindowManagerService ) v0 ).unregisterPointerEventListener ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 85 */
return;
} // .end method
/* # virtual methods */
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 4 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 98 */
int v0 = 0; // const/4 v0, 0x0
v0 = (( android.view.MotionEvent ) p1 ).getToolType ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_0 */
/* .line 100 */
return;
/* .line 102 */
} // :cond_0
v0 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
/* .line 103 */
/* .local v0, "action":I */
int v1 = 2; // const/4 v1, 0x2
/* if-eq v0, v1, :cond_1 */
int v1 = 7; // const/4 v1, 0x7
/* if-ne v0, v1, :cond_2 */
/* .line 104 */
} // :cond_1
v1 = (( android.view.MotionEvent ) p1 ).getDeviceId ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I
/* .line 105 */
v2 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
v3 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* .line 104 */
/* invoke-direct {p0, v1, v2, v3}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->deliverCursorPositionChangedEvent(IFF)V */
/* .line 107 */
} // :cond_2
return;
} // .end method
public void registerOnCursorPositionChangedListener ( com.miui.server.input.MiuiCursorPositionListenerService$OnCursorPositionChangedListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener; */
/* .line 42 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 45 */
v0 = this.mOnCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 46 */
try { // :try_start_0
v1 = v1 = this.mOnCursorPositionChangedListeners;
/* if-nez v1, :cond_0 */
/* .line 49 */
v1 = this.mOnCursorPositionChangedListeners;
/* .line 50 */
v1 = com.miui.server.input.MiuiCursorPositionListenerService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "a cursor position listener added, current size = "; // const-string v3, "a cursor position listener added, current size = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mOnCursorPositionChangedListeners;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 52 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->populatePointerListenerLocked()V */
/* .line 53 */
/* monitor-exit v0 */
/* .line 54 */
return;
/* .line 47 */
} // :cond_0
/* new-instance v1, Ljava/lang/RuntimeException; */
final String v2 = "The listener already registered"; // const-string v2, "The listener already registered"
/* invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService;
} // .end local p1 # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
/* throw v1 */
/* .line 53 */
/* .restart local p0 # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService; */
/* .restart local p1 # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 43 */
} // :cond_1
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "The listener can not be null!"; // const-string v1, "The listener can not be null!"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
public void unregisterOnCursorPositionChangedListener ( com.miui.server.input.MiuiCursorPositionListenerService$OnCursorPositionChangedListener p0 ) {
/* .locals 4 */
/* .param p1, "listener" # Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener; */
/* .line 57 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 60 */
v0 = this.mOnCursorPositionChangedListeners;
/* monitor-enter v0 */
/* .line 61 */
try { // :try_start_0
v1 = v1 = this.mOnCursorPositionChangedListeners;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 64 */
v1 = this.mOnCursorPositionChangedListeners;
/* .line 65 */
v1 = com.miui.server.input.MiuiCursorPositionListenerService.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "a cursor position listener removed, current size = "; // const-string v3, "a cursor position listener removed, current size = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mOnCursorPositionChangedListeners;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 67 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->unPopulatePointerListenerLocked()V */
/* .line 68 */
/* monitor-exit v0 */
/* .line 69 */
return;
/* .line 62 */
} // :cond_0
/* new-instance v1, Ljava/lang/RuntimeException; */
final String v2 = "The listener not registered"; // const-string v2, "The listener not registered"
/* invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
} // .end local p0 # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService;
} // .end local p1 # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
/* throw v1 */
/* .line 68 */
/* .restart local p0 # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService; */
/* .restart local p1 # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener; */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 58 */
} // :cond_1
/* new-instance v0, Ljava/lang/RuntimeException; */
final String v1 = "The listener can not be null!"; // const-string v1, "The listener can not be null!"
/* invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v0 */
} // .end method
