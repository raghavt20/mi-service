public class com.miui.server.input.custom.InputMiuiDesktopMode {
	 /* .source "InputMiuiDesktopMode.java" */
	 /* # static fields */
	 public static final java.lang.String SHORTCUT_ALT_ON_TAB;
	 public static final java.lang.String SHORTCUT_META_ON_D;
	 public static final java.lang.String SHORTCUT_META_ON_TAB;
	 /* # direct methods */
	 public com.miui.server.input.custom.InputMiuiDesktopMode ( ) {
		 /* .locals 0 */
		 /* .line 23 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Integer getKeyInterceptType ( com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE p0, android.content.Context p1, android.view.KeyEvent p2 ) {
		 /* .locals 2 */
		 /* .param p0, "interceptStage" # Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE; */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .param p2, "event" # Landroid/view/KeyEvent; */
		 /* .line 100 */
		 v0 = 		 com.android.server.wm.MiuiDesktopModeUtils .isActive ( p1 );
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 101 */
			 v0 = 			 (( android.view.KeyEvent ) p2 ).getKeyCode ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
			 /* const/16 v1, 0x3d */
			 /* if-ne v0, v1, :cond_1 */
			 /* .line 102 */
			 v0 = 			 (( android.view.KeyEvent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I
			 /* if-nez v0, :cond_1 */
			 v0 = 			 (( android.view.KeyEvent ) p2 ).getRepeatCount ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I
			 /* if-nez v0, :cond_1 */
			 /* .line 103 */
			 /* nop */
			 /* .line 104 */
			 v0 = 			 (( android.view.KeyEvent ) p2 ).getModifiers ( ); // invoke-virtual {p2}, Landroid/view/KeyEvent;->getModifiers()I
			 /* and-int/lit16 v0, v0, -0xc2 */
			 /* .line 105 */
			 /* .local v0, "shiftlessModifiers":I */
			 int v1 = 2; // const/4 v1, 0x2
			 v1 = 			 android.view.KeyEvent .metaStateHasModifiers ( v0,v1 );
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 /* .line 107 */
				 v1 = com.android.server.policy.MiuiKeyInterceptExtend$INTERCEPT_STAGE.BEFORE_QUEUEING;
				 /* if-ne p0, v1, :cond_0 */
				 /* .line 109 */
				 final String v1 = "2+61"; // const-string v1, "2+61"
				 com.miui.server.input.custom.InputMiuiDesktopMode .launchRecents ( p1,v1 );
				 /* .line 111 */
			 } // :cond_0
			 int v1 = 4; // const/4 v1, 0x4
			 /* .line 116 */
		 } // .end local v0 # "shiftlessModifiers":I
	 } // :cond_1
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public static java.util.List getMuiDeskModeKeyboardShortcutInfo ( android.content.Context p0, java.util.List p1 ) {
	 /* .locals 2 */
	 /* .param p0, "context" # Landroid/content/Context; */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Landroid/content/Context;", */
	 /* "Ljava/util/List<", */
	 /* "Landroid/view/KeyboardShortcutInfo;", */
	 /* ">;)", */
	 /* "Ljava/util/List<", */
	 /* "Landroid/view/KeyboardShortcutInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 130 */
/* .local p1, "keyboardShortcutInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/KeyboardShortcutInfo;>;" */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 131 */
	 /* new-instance v1, Lcom/miui/server/input/custom/InputMiuiDesktopMode$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode$$ExternalSyntheticLambda0;-><init>()V */
	 /* .line 142 */
	 java.util.stream.Collectors .toList ( );
	 /* check-cast v0, Ljava/util/List; */
	 /* .line 131 */
	 /* .line 144 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
static Boolean lambda$getMuiDeskModeKeyboardShortcutInfo$0 ( android.view.KeyboardShortcutInfo p0 ) { //synthethic
/* .locals 6 */
/* .param p0, "keyboardShortcutInfo" # Landroid/view/KeyboardShortcutInfo; */
/* .line 133 */
(( android.view.KeyboardShortcutInfo ) p0 ).getShortcutKeyCode ( ); // invoke-virtual {p0}, Landroid/view/KeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v0 */
/* .line 134 */
/* .local v0, "keyCode":J */
/* const-wide/high16 v2, 0x1000000000000L */
/* and-long/2addr v2, v0 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 135 */
	 /* const-wide v2, -0x1000000000001L */
	 /* and-long/2addr v0, v2 */
	 /* .line 136 */
	 /* const-wide/16 v2, 0x16 */
	 /* cmp-long v2, v0, v2 */
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* const-wide/16 v2, 0x15 */
		 /* cmp-long v2, v0, v2 */
		 /* if-nez v2, :cond_1 */
		 /* .line 138 */
	 } // :cond_0
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 141 */
} // :cond_1
int v2 = 1; // const/4 v2, 0x1
} // .end method
public static Boolean launchHome ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 58 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 59 */
	 final String v0 = "65536+32"; // const-string v0, "65536+32"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 60 */
		 /* new-instance v0, Landroid/content/Intent; */
		 final String v1 = "com.miui.home.action.shortcut"; // const-string v1, "com.miui.home.action.shortcut"
		 /* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 61 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 final String v1 = "keyevent1"; // const-string v1, "keyevent1"
		 /* const/high16 v2, 0x10000 */
		 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 62 */
		 final String v1 = "keyevent2"; // const-string v1, "keyevent2"
		 /* const/16 v2, 0x20 */
		 (( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 63 */
		 final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
		 (( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 64 */
		 v1 = android.os.UserHandle.CURRENT;
		 final String v2 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"
		 (( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
		 /* .line 66 */
		 final String v1 = "miuidesktopmode launchHome"; // const-string v1, "miuidesktopmode launchHome"
		 com.android.server.policy.MiuiInputLog .major ( v1 );
		 /* .line 67 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* .line 70 */
	 } // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean launchRecents ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 8 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 34 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 35 */
	 final String v0 = "65536+61"; // const-string v0, "65536+61"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 final String v1 = "miuidesktopmode launchRecents"; // const-string v1, "miuidesktopmode launchRecents"
	 final String v2 = "miui.permission.USE_INTERNAL_GENERAL_API"; // const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"
	 final String v3 = "com.miui.home"; // const-string v3, "com.miui.home"
	 /* const/16 v4, 0x3d */
	 final String v5 = "keyevent2"; // const-string v5, "keyevent2"
	 final String v6 = "keyevent1"; // const-string v6, "keyevent1"
	 final String v7 = "com.miui.home.action.shortcut"; // const-string v7, "com.miui.home.action.shortcut"
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 36 */
		 /* new-instance v0, Landroid/content/Intent; */
		 /* invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
		 /* .line 37 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 /* const/high16 v7, 0x10000 */
		 (( android.content.Intent ) v0 ).putExtra ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 38 */
		 (( android.content.Intent ) v0 ).putExtra ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
		 /* .line 39 */
		 (( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
		 /* .line 40 */
		 v3 = android.os.UserHandle.CURRENT;
		 (( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v3, v2 ); // invoke-virtual {p0, v0, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
		 /* .line 42 */
		 com.android.server.policy.MiuiInputLog .major ( v1 );
		 /* .line 43 */
		 int v1 = 1; // const/4 v1, 0x1
		 /* .line 44 */
	 } // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_0
final String v0 = "2+61"; // const-string v0, "2+61"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 45 */
	 /* new-instance v0, Landroid/content/Intent; */
	 /* invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
	 /* .line 46 */
	 /* .restart local v0 # "intent":Landroid/content/Intent; */
	 int v7 = 2; // const/4 v7, 0x2
	 (( android.content.Intent ) v0 ).putExtra ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
	 /* .line 47 */
	 (( android.content.Intent ) v0 ).putExtra ( v5, v4 ); // invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
	 /* .line 48 */
	 (( android.content.Intent ) v0 ).setPackage ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 49 */
	 v3 = android.os.UserHandle.CURRENT;
	 (( android.content.Context ) p0 ).sendBroadcastAsUser ( v0, v3, v2 ); // invoke-virtual {p0, v0, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V
	 /* .line 51 */
	 com.android.server.policy.MiuiInputLog .major ( v1 );
	 /* .line 54 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean shouldInterceptKeyboardCombinationRule ( android.content.Context p0, Integer p1, Integer p2 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "metaState" # I */
/* .param p2, "keyCode" # I */
/* .line 87 */
v0 = com.android.server.wm.MiuiDesktopModeUtils .isActive ( p0 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 88 */
/* const/high16 v0, 0x10000 */
/* if-ne p1, v0, :cond_1 */
/* const/16 v0, 0x16 */
/* if-eq p2, v0, :cond_0 */
/* const/16 v0, 0x15 */
/* if-ne p2, v0, :cond_1 */
/* .line 91 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 94 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
