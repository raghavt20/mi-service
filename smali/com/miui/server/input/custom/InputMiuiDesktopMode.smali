.class public Lcom/miui/server/input/custom/InputMiuiDesktopMode;
.super Ljava/lang/Object;
.source "InputMiuiDesktopMode.java"


# static fields
.field public static final SHORTCUT_ALT_ON_TAB:Ljava/lang/String; = "2+61"

.field public static final SHORTCUT_META_ON_D:Ljava/lang/String; = "65536+32"

.field public static final SHORTCUT_META_ON_TAB:Ljava/lang/String; = "65536+61"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getKeyInterceptType(Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;Landroid/content/Context;Landroid/view/KeyEvent;)I
    .locals 2
    .param p0, "interceptStage"    # Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 100
    invoke-static {p1}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x3d

    if-ne v0, v1, :cond_1

    .line 102
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 103
    nop

    .line 104
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getModifiers()I

    move-result v0

    and-int/lit16 v0, v0, -0xc2

    .line 105
    .local v0, "shiftlessModifiers":I
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    sget-object v1, Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;->BEFORE_QUEUEING:Lcom/android/server/policy/MiuiKeyInterceptExtend$INTERCEPT_STAGE;

    if-ne p0, v1, :cond_0

    .line 109
    const-string v1, "2+61"

    invoke-static {p1, v1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode;->launchRecents(Landroid/content/Context;Ljava/lang/String;)Z

    .line 111
    :cond_0
    const/4 v1, 0x4

    return v1

    .line 116
    .end local v0    # "shiftlessModifiers":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static getMuiDeskModeKeyboardShortcutInfo(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;)",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation

    .line 130
    .local p1, "keyboardShortcutInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/KeyboardShortcutInfo;>;"
    invoke-static {p0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-interface {p1}, Ljava/util/List;->stream()Ljava/util/stream/Stream;

    move-result-object v0

    new-instance v1, Lcom/miui/server/input/custom/InputMiuiDesktopMode$$ExternalSyntheticLambda0;

    invoke-direct {v1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode$$ExternalSyntheticLambda0;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->filter(Ljava/util/function/Predicate;)Ljava/util/stream/Stream;

    move-result-object v0

    .line 142
    invoke-static {}, Ljava/util/stream/Collectors;->toList()Ljava/util/stream/Collector;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/stream/Stream;->collect(Ljava/util/stream/Collector;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 131
    return-object v0

    .line 144
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic lambda$getMuiDeskModeKeyboardShortcutInfo$0(Landroid/view/KeyboardShortcutInfo;)Z
    .locals 6
    .param p0, "keyboardShortcutInfo"    # Landroid/view/KeyboardShortcutInfo;

    .line 133
    invoke-virtual {p0}, Landroid/view/KeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v0

    .line 134
    .local v0, "keyCode":J
    const-wide/high16 v2, 0x1000000000000L

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 135
    const-wide v2, -0x1000000000001L

    and-long/2addr v0, v2

    .line 136
    const-wide/16 v2, 0x16

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x15

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 138
    :cond_0
    const/4 v2, 0x0

    return v2

    .line 141
    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public static launchHome(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 58
    invoke-static {p0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "65536+32"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.home.action.shortcut"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "keyevent1"

    const/high16 v2, 0x10000

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    const-string v1, "keyevent2"

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 63
    const-string v1, "com.miui.home"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 66
    const-string v1, "miuidesktopmode launchHome"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 67
    const/4 v1, 0x1

    return v1

    .line 70
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static launchRecents(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 34
    invoke-static {p0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35
    const-string v0, "65536+61"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "miuidesktopmode launchRecents"

    const-string v2, "miui.permission.USE_INTERNAL_GENERAL_API"

    const-string v3, "com.miui.home"

    const/16 v4, 0x3d

    const-string v5, "keyevent2"

    const-string v6, "keyevent1"

    const-string v7, "com.miui.home.action.shortcut"

    if-eqz v0, :cond_0

    .line 36
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 37
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v7, 0x10000

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 38
    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 39
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 42
    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 43
    const/4 v1, 0x1

    return v1

    .line 44
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v0, "2+61"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    .restart local v0    # "intent":Landroid/content/Intent;
    const/4 v7, 0x2

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 47
    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 48
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v3, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    .line 51
    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 54
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public static shouldInterceptKeyboardCombinationRule(Landroid/content/Context;II)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "metaState"    # I
    .param p2, "keyCode"    # I

    .line 87
    invoke-static {p0}, Lcom/android/server/wm/MiuiDesktopModeUtils;->isActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const/high16 v0, 0x10000

    if-ne p1, v0, :cond_1

    const/16 v0, 0x16

    if-eq p2, v0, :cond_0

    const/16 v0, 0x15

    if-ne p2, v0, :cond_1

    .line 91
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 94
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
