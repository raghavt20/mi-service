.class Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;
.super Ljava/lang/Object;
.source "MiuiFingerPrintTapListener.java"

# interfaces
.implements Lcom/miui/server/input/gesture/MiuiGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiFingerPrintTapListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MiuiFingerPrintTapGestureListener"
.end annotation


# instance fields
.field private mPointerCount:I


# direct methods
.method constructor <init>()V
    .locals 0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isFingerPressing()Z
    .locals 1

    .line 163
    iget v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->mPointerCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 151
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 157
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->mPointerCount:I

    goto :goto_0

    .line 154
    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->mPointerCount:I

    .line 155
    nop

    .line 160
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
