.class Lcom/miui/server/input/PadManager$TrackMotionListener;
.super Ljava/lang/Object;
.source "PadManager.java"

# interfaces
.implements Lcom/miui/server/input/gesture/MiuiGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/PadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TrackMotionListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/PadManager;


# direct methods
.method constructor <init>(Lcom/miui/server/input/PadManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/PadManager;

    .line 280
    iput-object p1, p0, Lcom/miui/server/input/PadManager$TrackMotionListener;->this$0:Lcom/miui/server/input/PadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 283
    invoke-static {p1}, Lcom/android/server/input/InputOneTrackUtil;->shouldCountDevice(Landroid/view/InputEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/miui/server/input/PadManager$TrackMotionListener;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v0}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/input/InputOneTrackUtil;->trackExternalDevice(Landroid/view/InputEvent;)V

    .line 286
    :cond_0
    return-void
.end method
