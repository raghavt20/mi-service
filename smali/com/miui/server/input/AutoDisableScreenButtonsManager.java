public class com.miui.server.input.AutoDisableScreenButtonsManager {
	 /* .source "AutoDisableScreenButtonsManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer ENABLE_KEY_PRESS_INTERVAL;
private static final java.lang.String NAVIGATION_BAR_HEIGHT;
private static final java.lang.String PREF_ADSB_NOT_SHOW_PROMPTS;
private static final android.content.ComponentName SettingsActionComponent;
private static final java.lang.String TAG;
private static final Integer TMP_DISABLE_BUTTON;
private static com.miui.server.input.AutoDisableScreenButtonsManager sAutoDisableScreenButtonsManager;
/* # instance fields */
private java.lang.String mCloudConfig;
private android.content.Context mContext;
private Integer mCurUserId;
private miui.view.AutoDisableScreenbuttonsFloatView mFloatView;
private android.os.Handler mHandler;
private final java.lang.Object mLock;
private Integer mScreenButtonPressedKeyCode;
private Long mScreenButtonPressedTime;
private Boolean mScreenButtonsDisabled;
private Boolean mScreenButtonsTmpDisabled;
private Boolean mStatusBarVisibleOld;
private Long mToastShowTime;
private Boolean mTwice;
private java.lang.String mUserSetting;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static miui.view.AutoDisableScreenbuttonsFloatView -$$Nest$fgetmFloatView ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mFloatView;
} // .end method
static Boolean -$$Nest$fgetmScreenButtonsTmpDisabled ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z */
} // .end method
static void -$$Nest$fputmStatusBarVisibleOld ( com.miui.server.input.AutoDisableScreenButtonsManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mStatusBarVisibleOld:Z */
	 return;
} // .end method
static void -$$Nest$msaveTmpDisableButtonsStatus ( com.miui.server.input.AutoDisableScreenButtonsManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->saveTmpDisableButtonsStatus(Z)V */
	 return;
} // .end method
static void -$$Nest$mshowFloat ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showFloat()V */
	 return;
} // .end method
static Boolean -$$Nest$mshowPromptsIfNeeds ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showPromptsIfNeeds()Z */
} // .end method
static void -$$Nest$mshowSettings ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showSettings()V */
	 return;
} // .end method
static void -$$Nest$mshowToastInner ( com.miui.server.input.AutoDisableScreenButtonsManager p0, java.lang.CharSequence p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToastInner(Ljava/lang/CharSequence;)V */
	 return;
} // .end method
static void -$$Nest$mupdateSettings ( com.miui.server.input.AutoDisableScreenButtonsManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->updateSettings()V */
	 return;
} // .end method
static java.lang.String -$$Nest$smgetRunningTopActivity ( ) { //bridge//synthethic
	 /* .locals 1 */
	 com.miui.server.input.AutoDisableScreenButtonsManager .getRunningTopActivity ( );
} // .end method
static com.miui.server.input.AutoDisableScreenButtonsManager ( ) {
	 /* .locals 1 */
	 /* .line 60 */
	 /* nop */
	 /* .line 61 */
	 final String v0 = "com.android.settings/.AutoDisableScreenButtonsAppListSettingsActivity"; // const-string v0, "com.android.settings/.AutoDisableScreenButtonsAppListSettingsActivity"
	 android.content.ComponentName .unflattenFromString ( v0 );
	 /* .line 60 */
	 return;
} // .end method
public com.miui.server.input.AutoDisableScreenButtonsManager ( ) {
	 /* .locals 1 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "handler" # Landroid/os/Handler; */
	 /* .line 69 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 43 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
	 /* .line 50 */
	 /* iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z */
	 /* .line 55 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mStatusBarVisibleOld:Z */
	 /* .line 57 */
	 /* new-instance v0, Landroid/os/Handler; */
	 /* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
	 this.mHandler = v0;
	 /* .line 65 */
	 /* new-instance v0, Ljava/lang/Object; */
	 /* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
	 this.mLock = v0;
	 /* .line 70 */
	 this.mContext = p1;
	 /* .line 71 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->resetButtonsStatus()V */
	 /* .line 72 */
	 /* new-instance v0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver; */
	 /* invoke-direct {v0, p0, p2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Landroid/os/Handler;)V */
	 /* .line 73 */
	 /* .local v0, "observer":Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver; */
	 (( com.miui.server.input.AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->observe()V
	 /* .line 74 */
	 /* .line 75 */
	 return;
} // .end method
private Integer getAndroidDimen ( android.content.Context p0, java.lang.String p1 ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .param p2, "name" # Ljava/lang/String; */
	 /* .line 255 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* if-nez p1, :cond_0 */
	 /* .line 256 */
	 /* .line 258 */
} // :cond_0
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v2 = "dimen"; // const-string v2, "dimen"
final String v3 = "android"; // const-string v3, "android"
v1 = (( android.content.res.Resources ) v1 ).getIdentifier ( p2, v2, v3 ); // invoke-virtual {v1, p2, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 259 */
/* .local v1, "heightResId":I */
/* if-lez v1, :cond_1 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
v0 = (( android.content.res.Resources ) v0 ).getDimensionPixelOffset ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I
} // :cond_1
} // .end method
private Integer getNavigationBarHeight ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 251 */
final String v0 = "navigation_bar_height"; // const-string v0, "navigation_bar_height"
v0 = /* invoke-direct {p0, p1, v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->getAndroidDimen(Landroid/content/Context;Ljava/lang/String;)I */
} // .end method
private static java.lang.String getRunningTopActivity ( ) {
/* .locals 2 */
/* .line 330 */
com.android.server.wm.WindowProcessUtils .getTopRunningActivityInfo ( );
/* .line 331 */
/* .local v0, "topActivityInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;" */
/* if-nez v0, :cond_0 */
/* .line 332 */
int v1 = 0; // const/4 v1, 0x0
/* .line 334 */
} // :cond_0
final String v1 = "packageName"; // const-string v1, "packageName"
(( java.util.HashMap ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
} // .end method
public static void onStatusBarVisibilityChangeStatic ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "visible" # Z */
/* .line 104 */
v0 = com.miui.server.input.AutoDisableScreenButtonsManager.sAutoDisableScreenButtonsManager;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 105 */
(( com.miui.server.input.AutoDisableScreenButtonsManager ) v0 ).onStatusBarVisibilityChange ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onStatusBarVisibilityChange(Z)V
/* .line 107 */
} // :cond_0
return;
} // .end method
private void resetButtonsStatus ( ) {
/* .locals 1 */
/* .line 321 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->saveDisableButtonsStatus(Z)V */
/* .line 322 */
/* iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z */
/* .line 323 */
return;
} // .end method
private void saveDisableButtonsStatus ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "disable" # Z */
/* .line 306 */
/* iput-boolean p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z */
/* .line 307 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 308 */
/* iget v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
/* .line 307 */
final String v2 = "screen_buttons_state"; // const-string v2, "screen_buttons_state"
android.provider.Settings$Secure .putIntForUser ( v0,v2,p1,v1 );
/* .line 309 */
return;
} // .end method
private void saveTmpDisableButtonsStatus ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "tmp" # Z */
/* .line 312 */
/* iput-boolean p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z */
/* .line 313 */
/* iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 314 */
return;
/* .line 316 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 317 */
if ( p1 != null) { // if-eqz p1, :cond_1
int v1 = 2; // const/4 v1, 0x2
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
/* iget v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
/* .line 316 */
final String v3 = "screen_buttons_state"; // const-string v3, "screen_buttons_state"
android.provider.Settings$Secure .putIntForUser ( v0,v3,v1,v2 );
/* .line 318 */
return;
} // .end method
private void setLinearLayoutMarginBottom ( ) {
/* .locals 4 */
/* .line 241 */
v0 = this.mFloatView;
/* .line 242 */
/* const v1, 0x110a0019 */
(( miui.view.AutoDisableScreenbuttonsFloatView ) v0 ).findViewById ( v1 ); // invoke-virtual {v0, v1}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->findViewById(I)Landroid/view/View;
/* check-cast v0, Landroid/widget/LinearLayout; */
/* .line 243 */
/* .local v0, "linearLayout":Landroid/widget/LinearLayout; */
/* nop */
/* .line 244 */
(( android.widget.LinearLayout ) v0 ).getLayoutParams ( ); // invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
/* check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams; */
/* .line 245 */
/* .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams; */
v2 = this.mContext;
v2 = /* invoke-direct {p0, v2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->getNavigationBarHeight(Landroid/content/Context;)I */
/* .line 246 */
/* .local v2, "navigationBarHeight":I */
int v3 = 0; // const/4 v3, 0x0
(( android.view.ViewGroup$MarginLayoutParams ) v1 ).setMargins ( v3, v3, v3, v2 ); // invoke-virtual {v1, v3, v3, v3, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V
/* .line 247 */
(( android.widget.LinearLayout ) v0 ).setLayoutParams ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
/* .line 248 */
return;
} // .end method
private void showFloat ( ) {
/* .locals 2 */
/* .line 214 */
final String v0 = "AutoDisableScreenButtonsManager"; // const-string v0, "AutoDisableScreenButtonsManager"
/* const-string/jumbo v1, "showing auto disable screen buttons float window..." */
android.util.Log .d ( v0,v1 );
/* .line 216 */
v0 = this.mFloatView;
/* if-nez v0, :cond_0 */
/* .line 217 */
v0 = this.mContext;
miui.view.AutoDisableScreenbuttonsFloatView .inflate ( v0 );
this.mFloatView = v0;
/* .line 218 */
/* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->setLinearLayoutMarginBottom()V */
/* .line 220 */
v0 = this.mFloatView;
/* new-instance v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager$3; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$3;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V */
(( miui.view.AutoDisableScreenbuttonsFloatView ) v0 ).setOnClickListener ( v1 ); // invoke-virtual {v0, v1}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
/* .line 229 */
v0 = this.mFloatView;
/* new-instance v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager$4; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$4;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V */
(( miui.view.AutoDisableScreenbuttonsFloatView ) v0 ).setOnLongClickListener ( v1 ); // invoke-virtual {v0, v1}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
/* .line 237 */
} // :cond_0
v0 = this.mFloatView;
(( miui.view.AutoDisableScreenbuttonsFloatView ) v0 ).show ( ); // invoke-virtual {v0}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->show()V
/* .line 238 */
return;
} // .end method
private Boolean showPromptsIfNeeds ( ) {
/* .locals 7 */
/* .line 265 */
v0 = this.mContext;
final String v1 = "ADSB_NOT_SHOW_PROMPTS"; // const-string v1, "ADSB_NOT_SHOW_PROMPTS"
com.miui.server.input.util.AutoDisableScreenButtonsHelper .getValue ( v0,v1 );
/* .line 266 */
/* .local v0, "obj":Ljava/lang/Object; */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* move-object v2, v0 */
/* check-cast v2, Ljava/lang/Boolean; */
v2 = (( java.lang.Boolean ) v2 ).booleanValue ( ); // invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
/* .line 267 */
/* .local v2, "notShow":Z */
} // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 268 */
/* .line 271 */
} // :cond_1
/* new-instance v1, Landroid/app/AlertDialog$Builder; */
v3 = this.mContext;
/* invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V */
/* .line 272 */
/* .local v1, "builder":Landroid/app/AlertDialog$Builder; */
/* const v3, 0x110f0060 */
(( android.app.AlertDialog$Builder ) v1 ).setTitle ( v3 ); // invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;
/* .line 273 */
/* const v4, 0x110f005d */
(( android.app.AlertDialog$Builder ) v3 ).setMessage ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;
/* .line 277 */
int v4 = 1; // const/4 v4, 0x1
(( android.app.AlertDialog$Builder ) v3 ).setCancelable ( v4 ); // invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;
/* new-instance v5, Lcom/miui/server/input/AutoDisableScreenButtonsManager$5; */
/* invoke-direct {v5, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$5;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V */
/* .line 278 */
/* const v6, 0x110f005f */
(( android.app.AlertDialog$Builder ) v3 ).setPositiveButton ( v6, v5 ); // invoke-virtual {v3, v6, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
/* .line 287 */
(( android.app.AlertDialog$Builder ) v3 ).create ( ); // invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;
/* .line 288 */
/* .local v3, "adlg":Landroid/app/AlertDialog; */
(( android.app.AlertDialog ) v3 ).getWindow ( ); // invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;
/* const/16 v6, 0x7d3 */
(( android.view.Window ) v5 ).setType ( v6 ); // invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V
/* .line 289 */
(( android.app.AlertDialog ) v3 ).show ( ); // invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V
/* .line 290 */
} // .end method
private void showSettings ( ) {
/* .locals 4 */
/* .line 294 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.MAIN"; // const-string v1, "android.intent.action.MAIN"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 295 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = com.miui.server.input.AutoDisableScreenButtonsManager.SettingsActionComponent;
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 296 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 299 */
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).startActivity ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
/* :try_end_0 */
/* .catch Landroid/content/ActivityNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 302 */
/* .line 300 */
/* :catch_0 */
/* move-exception v1 */
/* .line 301 */
/* .local v1, "e":Landroid/content/ActivityNotFoundException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "start activity exception, component = " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = com.miui.server.input.AutoDisableScreenButtonsManager.SettingsActionComponent;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "AutoDisableScreenButtonsManager"; // const-string v3, "AutoDisableScreenButtonsManager"
android.util.Log .e ( v3,v2 );
/* .line 303 */
} // .end local v1 # "e":Landroid/content/ActivityNotFoundException;
} // :goto_0
return;
} // .end method
private void showToast ( java.lang.CharSequence p0, android.os.Handler p1 ) {
/* .locals 1 */
/* .param p1, "text" # Ljava/lang/CharSequence; */
/* .param p2, "h" # Landroid/os/Handler; */
/* .line 196 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 197 */
/* new-instance v0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$2; */
/* invoke-direct {v0, p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$2;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Ljava/lang/CharSequence;)V */
(( android.os.Handler ) p2 ).post ( v0 ); // invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 203 */
} // :cond_0
/* invoke-direct {p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToastInner(Ljava/lang/CharSequence;)V */
/* .line 205 */
} // :goto_0
return;
} // .end method
private void showToast ( Boolean p0, android.os.Handler p1 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .param p2, "h" # Landroid/os/Handler; */
/* .line 191 */
v0 = this.mContext;
if ( p1 != null) { // if-eqz p1, :cond_0
/* const v1, 0x110f005b */
/* .line 192 */
} // :cond_0
/* const v1, 0x110f005a */
/* .line 191 */
} // :goto_0
(( android.content.Context ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* invoke-direct {p0, v0, p2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToast(Ljava/lang/CharSequence;Landroid/os/Handler;)V */
/* .line 193 */
return;
} // .end method
private void showToastInner ( java.lang.CharSequence p0 ) {
/* .locals 2 */
/* .param p1, "text" # Ljava/lang/CharSequence; */
/* .line 208 */
v0 = this.mContext;
int v1 = 0; // const/4 v1, 0x0
android.widget.Toast .makeText ( v0,p1,v1 );
/* .line 209 */
/* .local v0, "toast":Landroid/widget/Toast; */
/* const/16 v1, 0x7d6 */
(( android.widget.Toast ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Landroid/widget/Toast;->setType(I)V
/* .line 210 */
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 211 */
return;
} // .end method
private void updateSettings ( ) {
/* .locals 6 */
/* .line 362 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 363 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
v1 = this.mLock;
/* monitor-enter v1 */
/* .line 364 */
try { // :try_start_0
final String v2 = "screen_buttons_state"; // const-string v2, "screen_buttons_state"
/* iget v3, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
int v4 = 0; // const/4 v4, 0x0
v2 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v4,v3 );
/* .line 366 */
/* .local v2, "btnState":I */
/* packed-switch v2, :pswitch_data_0 */
/* .line 371 */
/* :pswitch_0 */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z */
/* .line 368 */
/* :pswitch_1 */
/* iput-boolean v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z */
/* .line 369 */
/* nop */
/* .line 375 */
} // :goto_0
final String v3 = "auto_disable_screen_button"; // const-string v3, "auto_disable_screen_button"
/* iget v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
android.provider.MiuiSettings$System .getStringForUser ( v0,v3,v4 );
/* .line 377 */
/* .local v3, "userSetting":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
v4 = this.mUserSetting;
v4 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v4, :cond_0 */
/* .line 378 */
this.mUserSetting = v3;
/* .line 379 */
com.miui.server.input.util.AutoDisableScreenButtonsHelper .updateUserJson ( v3 );
/* .line 381 */
} // :cond_0
final String v4 = "auto_disable_screen_button_cloud_setting"; // const-string v4, "auto_disable_screen_button_cloud_setting"
android.provider.Settings$System .getString ( v0,v4 );
/* .line 383 */
/* .local v4, "cloudConfig":Ljava/lang/String; */
if ( v4 != null) { // if-eqz v4, :cond_1
v5 = this.mCloudConfig;
v5 = (( java.lang.String ) v4 ).equals ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_1 */
/* .line 384 */
this.mCloudConfig = v4;
/* .line 385 */
com.miui.server.input.util.AutoDisableScreenButtonsHelper .updateCloudJson ( v4 );
/* .line 387 */
} // .end local v2 # "btnState":I
} // .end local v3 # "userSetting":Ljava/lang/String;
} // .end local v4 # "cloudConfig":Ljava/lang/String;
} // :cond_1
/* monitor-exit v1 */
/* .line 388 */
return;
/* .line 387 */
/* :catchall_0 */
/* move-exception v2 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v2 */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public Boolean handleDisableButtons ( Integer p0, Boolean p1, Boolean p2, Boolean p3, android.view.KeyEvent p4 ) {
/* .locals 7 */
/* .param p1, "keyCode" # I */
/* .param p2, "down" # Z */
/* .param p3, "disableForSingleKey" # Z */
/* .param p4, "disableForLidClose" # Z */
/* .param p5, "event" # Landroid/view/KeyEvent; */
/* .line 118 */
int v0 = 0; // const/4 v0, 0x0
/* .line 119 */
/* .local v0, "disable":Z */
(( android.view.KeyEvent ) p5 ).getDevice ( ); // invoke-virtual {p5}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_1
(( android.view.KeyEvent ) p5 ).getDevice ( ); // invoke-virtual {p5}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
v1 = (( android.view.InputDevice ) v1 ).isVirtual ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->isVirtual()Z
if ( v1 != null) { // if-eqz v1, :cond_0
} // :cond_0
/* move v1, v2 */
} // :cond_1
} // :goto_0
/* move v1, v3 */
/* .line 120 */
/* .local v1, "isVirtual":Z */
} // :goto_1
v4 = (( android.view.KeyEvent ) p5 ).getFlags ( ); // invoke-virtual {p5}, Landroid/view/KeyEvent;->getFlags()I
/* and-int/lit8 v4, v4, 0x40 */
if ( v4 != null) { // if-eqz v4, :cond_2
/* move v4, v3 */
} // :cond_2
/* move v4, v2 */
/* .line 122 */
/* .local v4, "isVirtualHardKey":Z */
} // :goto_2
v5 = this.mContext;
/* iget v6, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
v5 = com.miui.enterprise.RestrictionsHelper .hasKeyCodeRestriction ( v5,p1,v6 );
/* if-nez v5, :cond_8 */
/* .line 124 */
miui.enterprise.RestrictionsHelperStub .getInstance ( );
v5 = /* iget v6, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 129 */
} // :cond_3
final String v3 = "AutoDisableScreenButtonsManager"; // const-string v3, "AutoDisableScreenButtonsManager"
/* sparse-switch p1, :sswitch_data_0 */
/* .line 151 */
/* .line 133 */
/* :sswitch_0 */
if ( p3 != null) { // if-eqz p3, :cond_4
/* if-nez v1, :cond_4 */
/* .line 134 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "disableForSingleKey keyCode:"; // const-string v5, "disableForSingleKey keyCode:"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 135 */
int v0 = 1; // const/4 v0, 0x1
/* .line 136 */
/* .line 140 */
} // :cond_4
/* :sswitch_1 */
if ( v1 != null) { // if-eqz v1, :cond_5
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 141 */
} // :cond_5
if ( p4 != null) { // if-eqz p4, :cond_6
v2 = miui.util.SmartCoverManager .deviceDisableKeysWhenLidClose ( );
if ( v2 != null) { // if-eqz v2, :cond_6
/* .line 142 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "disableForLidClose keyCode:"; // const-string v5, "disableForLidClose keyCode:"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 143 */
int v0 = 1; // const/4 v0, 0x1
/* .line 144 */
} // :cond_6
v2 = (( com.miui.server.input.AutoDisableScreenButtonsManager ) p0 ).screenButtonsInterceptKey ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->screenButtonsInterceptKey(IZ)Z
if ( v2 != null) { // if-eqz v2, :cond_7
/* .line 145 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "screenButtonsDisabled keyCode:"; // const-string v5, "screenButtonsDisabled keyCode:"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v2 );
/* .line 146 */
int v0 = 1; // const/4 v0, 0x1
/* .line 153 */
} // :cond_7
} // :goto_3
/* .line 126 */
} // :cond_8
} // :goto_4
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x3 -> :sswitch_1 */
/* 0x4 -> :sswitch_0 */
/* 0x52 -> :sswitch_0 */
/* 0x54 -> :sswitch_1 */
/* 0xbb -> :sswitch_0 */
} // .end sparse-switch
} // .end method
public Boolean isScreenButtonsDisabled ( ) {
/* .locals 1 */
/* .line 187 */
/* iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z */
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public void onStatusBarVisibilityChange ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "visible" # Z */
/* .line 78 */
/* iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mStatusBarVisibleOld:Z */
/* if-eq p1, v0, :cond_0 */
/* .line 79 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 101 */
} // :cond_0
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 180 */
/* iget v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
/* if-eq v0, p1, :cond_0 */
/* .line 181 */
/* iput p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I */
/* .line 182 */
/* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->updateSettings()V */
/* .line 184 */
} // :cond_0
return;
} // .end method
public void resetTmpButtonsStatus ( ) {
/* .locals 1 */
/* .line 326 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z */
/* .line 327 */
return;
} // .end method
public Boolean screenButtonsInterceptKey ( Integer p0, Boolean p1 ) {
/* .locals 8 */
/* .param p1, "keycode" # I */
/* .param p2, "down" # Z */
/* .line 157 */
v0 = (( com.miui.server.input.AutoDisableScreenButtonsManager ) p0 ).isScreenButtonsDisabled ( ); // invoke-virtual {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->isScreenButtonsDisabled()Z
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 158 */
/* .line 160 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
if ( p2 != null) { // if-eqz p2, :cond_2
/* .line 161 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v2 */
/* .line 162 */
/* .local v2, "time":J */
/* iget-wide v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedTime:J */
/* sub-long v4, v2, v4 */
/* const-wide/16 v6, 0x7d0 */
/* cmp-long v4, v4, v6 */
/* if-gez v4, :cond_1 */
/* iget v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedKeyCode:I */
/* if-ne v4, p1, :cond_1 */
/* iget-boolean v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 164 */
/* iput-boolean v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z */
/* .line 165 */
/* invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->resetButtonsStatus()V */
/* .line 166 */
/* .line 168 */
} // :cond_1
/* iput-wide v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedTime:J */
/* .line 169 */
/* iput p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedKeyCode:I */
/* .line 170 */
/* iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z */
/* .line 171 */
/* iget-wide v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mToastShowTime:J */
/* sub-long v4, v2, v4 */
/* cmp-long v1, v4, v6 */
/* if-lez v1, :cond_2 */
/* .line 172 */
/* iput-wide v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mToastShowTime:J */
/* .line 173 */
v1 = this.mContext;
/* const v4, 0x110f0061 */
(( android.content.Context ) v1 ).getString ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
v4 = this.mHandler;
/* invoke-direct {p0, v1, v4}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToast(Ljava/lang/CharSequence;Landroid/os/Handler;)V */
/* .line 176 */
} // .end local v2 # "time":J
} // :cond_2
} // .end method
