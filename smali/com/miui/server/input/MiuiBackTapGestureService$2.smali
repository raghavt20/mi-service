.class Lcom/miui/server/input/MiuiBackTapGestureService$2;
.super Ljava/lang/Object;
.source "MiuiBackTapGestureService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiBackTapGestureService;


# direct methods
.method constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiBackTapGestureService;

    .line 153
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 156
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapSensorWrapper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmScreenOn(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mcheckEmpty(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-static {v0, v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mcheckEmpty(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapSensorWrapper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapSensorChangeListener(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiui/util/BackTapSensorWrapper;->registerListener(Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;)V

    .line 162
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fputmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;Z)V

    .line 163
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapTouchHelper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$msetTouchTrackingEnabled(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Z)V

    goto :goto_0

    .line 165
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mcheckEmpty(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;)Ljava/lang/String;

    move-result-object v2

    .line 166
    invoke-static {v0, v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mcheckEmpty(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapSensorWrapper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapSensorChangeListener(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmiui/util/BackTapSensorWrapper;->unregisterListener(Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;)V

    .line 168
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fputmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;Z)V

    .line 169
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapTouchHelper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$msetTouchTrackingEnabled(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Z)V

    goto :goto_0

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 174
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapSensorWrapper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lmiui/util/BackTapSensorWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/BackTapSensorWrapper;->unregisterAllListeners()V

    .line 175
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fputmIsBackTapSensorListenerRegistered(Lcom/miui/server/input/MiuiBackTapGestureService;Z)V

    .line 176
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$2;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmBackTapTouchHelper(Lcom/miui/server/input/MiuiBackTapGestureService;)Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$msetTouchTrackingEnabled(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Z)V

    .line 180
    :cond_3
    :goto_0
    return-void
.end method
