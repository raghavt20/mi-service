class com.miui.server.input.MiuiFingerPrintTapListener$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiFingerPrintTapListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiFingerPrintTapListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiFingerPrintTapListener this$0; //synthetic
/* # direct methods */
 com.miui.server.input.MiuiFingerPrintTapListener$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiFingerPrintTapListener; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 180 */
this.this$0 = p1;
/* .line 181 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 182 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 185 */
v0 = this.this$0;
com.miui.server.input.MiuiFingerPrintTapListener .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 186 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "fingerprint_double_tap"; // const-string v1, "fingerprint_double_tap"
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 188 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 192 */
final String v0 = "fingerprint_double_tap"; // const-string v0, "fingerprint_double_tap"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 193 */
	 v1 = this.this$0;
	 com.miui.server.input.MiuiFingerPrintTapListener .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 int v3 = -2; // const/4 v3, -0x2
	 android.provider.Settings$System .getStringForUser ( v2,v0,v3 );
	 com.miui.server.input.MiuiFingerPrintTapListener .-$$Nest$fputmDoubleTapSideFp ( v1,v0 );
	 /* .line 195 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "fingerprint_drop_down changed, mDoubleTapSideFp = "; // const-string v1, "fingerprint_drop_down changed, mDoubleTapSideFp = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.miui.server.input.MiuiFingerPrintTapListener .-$$Nest$fgetmDoubleTapSideFp ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "MiuiFingerPrintTapListener"; // const-string v1, "MiuiFingerPrintTapListener"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 197 */
} // :cond_0
v0 = this.this$0;
com.miui.server.input.MiuiFingerPrintTapListener .-$$Nest$mupdatePrintTapState ( v0 );
/* .line 198 */
return;
} // .end method
