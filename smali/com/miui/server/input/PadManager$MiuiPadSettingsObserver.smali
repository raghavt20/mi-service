.class Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;
.super Landroid/database/ContentObserver;
.source "PadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/PadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiPadSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/PadManager;


# direct methods
.method constructor <init>(Lcom/miui/server/input/PadManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/PadManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 146
    iput-object p1, p0, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->this$0:Lcom/miui/server/input/PadManager;

    .line 147
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 148
    return-void
.end method


# virtual methods
.method observer()V
    .locals 5

    .line 151
    iget-object v0, p0, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v0}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 152
    .local v0, "resolver":Landroid/content/ContentResolver;
    nop

    .line 153
    const-string/jumbo v1, "user_setup_complete"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 152
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v3, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 155
    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 156
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 160
    iget-object v0, p0, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v0}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 161
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string/jumbo v1, "user_setup_complete"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 162
    iget-object v2, p0, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->this$0:Lcom/miui/server/input/PadManager;

    .line 163
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 162
    const/4 v4, 0x0

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v4, 0x1

    :cond_0
    invoke-static {v2, v4}, Lcom/miui/server/input/PadManager;->-$$Nest$fputmIsUserSetup(Lcom/miui/server/input/PadManager;Z)V

    .line 165
    :cond_1
    return-void
.end method
