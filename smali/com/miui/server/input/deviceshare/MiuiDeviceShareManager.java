public class com.miui.server.input.deviceshare.MiuiDeviceShareManager {
	 /* .source "MiuiDeviceShareManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;, */
	 /* Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$MiuiDeviceShareManagerHolder; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
/* # instance fields */
private final android.os.Handler mHandler;
private final java.lang.Object mLock;
private final com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private final android.util.SparseArray mRecords;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/SparseArray<", */
/* "Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$monListenerBinderDied ( com.miui.server.input.deviceshare.MiuiDeviceShareManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->onListenerBinderDied(I)V */
return;
} // .end method
private com.miui.server.input.deviceshare.MiuiDeviceShareManager ( ) {
/* .locals 2 */
/* .line 33 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 34 */
/* new-instance v0, Landroid/util/SparseArray; */
/* invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V */
this.mRecords = v0;
/* .line 35 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.input.MiuiInputThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 36 */
/* new-instance v0, Ljava/lang/Object; */
/* invoke-direct {v0}, Ljava/lang/Object;-><init>()V */
this.mLock = v0;
/* .line 37 */
/* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
this.mMiuiInputManagerInternal = v0;
/* .line 38 */
return;
} // .end method
 com.miui.server.input.deviceshare.MiuiDeviceShareManager ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;-><init>()V */
return;
} // .end method
private void closeSocket ( android.os.ParcelFileDescriptor p0 ) {
/* .locals 3 */
/* .param p1, "parcelFileDescriptor" # Landroid/os/ParcelFileDescriptor; */
/* .line 145 */
final String v0 = "MiuiDeviceShareManager"; // const-string v0, "MiuiDeviceShareManager"
/* if-nez p1, :cond_0 */
/* .line 146 */
return;
/* .line 149 */
} // :cond_0
try { // :try_start_0
(( android.os.ParcelFileDescriptor ) p1 ).close ( ); // invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V
/* .line 150 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "closeSocket "; // const-string v2, "closeSocket "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 153 */
/* .line 151 */
/* :catch_0 */
/* move-exception v1 */
/* .line 152 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).getMessage ( ); // invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v0,v2,v1 );
/* .line 154 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_0
return;
} // .end method
public static com.miui.server.input.deviceshare.MiuiDeviceShareManager getInstance ( ) {
/* .locals 1 */
/* .line 210 */
com.miui.server.input.deviceshare.MiuiDeviceShareManager$MiuiDeviceShareManagerHolder .-$$Nest$sfgetsInstance ( );
} // .end method
private void onListenerBinderDied ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 119 */
final String v0 = "MiuiDeviceShareManager"; // const-string v0, "MiuiDeviceShareManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " binder died"; // const-string v2, " binder died"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 121 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 122 */
try { // :try_start_0
v1 = this.mRecords;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V */
/* .line 123 */
/* monitor-exit v0 */
/* .line 124 */
return;
/* .line 123 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void removeRecordLocked ( com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord p0 ) {
/* .locals 5 */
/* .param p1, "record" # Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* .line 128 */
/* if-nez p1, :cond_0 */
/* .line 129 */
return;
/* .line 132 */
} // :cond_0
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fgetmListener ( p1 );
/* .line 133 */
/* .local v0, "binder":Landroid/os/IBinder; */
int v1 = 0; // const/4 v1, 0x0
/* .line 134 */
v2 = com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fgetmPid ( p1 );
/* .line 136 */
/* .local v2, "pid":I */
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fgetmParcelFileDescriptor ( p1 );
/* invoke-direct {p0, v3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->closeSocket(Landroid/os/ParcelFileDescriptor;)V */
/* .line 138 */
v3 = this.mRecords;
(( android.util.SparseArray ) v3 ).remove ( v2 ); // invoke-virtual {v3, v2}, Landroid/util/SparseArray;->remove(I)V
/* .line 140 */
v3 = this.mMiuiInputManagerInternal;
int v4 = 0; // const/4 v4, 0x0
(( com.android.server.input.MiuiInputManagerInternal ) v3 ).setDeviceShareListener ( v2, v4, v1 ); // invoke-virtual {v3, v2, v4, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V
/* .line 141 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 3 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 157 */
final String v0 = "MiuiDeviceShareManager(Java) State:"; // const-string v0, "MiuiDeviceShareManager(Java) State:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 158 */
final String v0 = " "; // const-string v0, " "
/* .line 159 */
/* .local v0, "prefix":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "mRecords: "; // const-string v2, "mRecords: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).print ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 160 */
v1 = this.mRecords;
v1 = (( android.util.SparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/SparseArray;->size()I
/* if-nez v1, :cond_0 */
/* .line 161 */
final String v1 = "<EMPTY>"; // const-string v1, "<EMPTY>"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 163 */
} // :cond_0
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 164 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mRecords;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 166 */
} // :goto_0
return;
} // .end method
public void onSocketBroken ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "pid" # I */
/* .line 103 */
final String v0 = "MiuiDeviceShareManager"; // const-string v0, "MiuiDeviceShareManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " socket broken"; // const-string v2, " socket broken"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v0,v1 );
/* .line 105 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 106 */
try { // :try_start_0
v1 = this.mRecords;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* .line 107 */
/* .local v1, "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* if-nez v1, :cond_0 */
/* .line 109 */
/* monitor-exit v0 */
return;
/* .line 112 */
} // :cond_0
/* invoke-direct {p0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V */
/* .line 113 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 115 */
v0 = this.mHandler;
java.util.Objects .requireNonNull ( v1 );
/* new-instance v2, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 116 */
return;
/* .line 113 */
} // .end local v1 # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
public void setDeviceShareListener ( Integer p0, android.os.ParcelFileDescriptor p1, Integer p2, miui.hardware.input.IDeviceShareStateChangedListener p3 ) {
/* .locals 10 */
/* .param p1, "pid" # I */
/* .param p2, "fd" # Landroid/os/ParcelFileDescriptor; */
/* .param p3, "flags" # I */
/* .param p4, "listener" # Lmiui/hardware/input/IDeviceShareStateChangedListener; */
/* .line 43 */
final String v0 = "MiuiDeviceShareManager"; // const-string v0, "MiuiDeviceShareManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "pid "; // const-string v2, "pid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " request set device share listener, flag = "; // const-string v2, " request set device share listener, flag = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 44 */
miui.hardware.input.MiuiInputManager .deviceShareFlagToString ( p3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 43 */
android.util.Slog .w ( v0,v1 );
/* .line 45 */
/* if-nez p3, :cond_0 */
/* .line 47 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 48 */
try { // :try_start_0
v1 = this.mRecords;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V */
/* .line 49 */
/* monitor-exit v0 */
/* .line 50 */
return;
/* .line 49 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 52 */
} // :cond_0
if ( p2 != null) { // if-eqz p2, :cond_3
/* if-nez p4, :cond_1 */
/* goto/16 :goto_0 */
/* .line 57 */
} // :cond_1
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 58 */
try { // :try_start_1
v1 = this.mRecords;
(( android.util.SparseArray ) v1 ).get ( p1 ); // invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* .line 59 */
/* .local v1, "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_2 */
/* .line 61 */
/* new-instance v9, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* move-object v3, v9 */
/* move-object v4, p0 */
/* move v5, p1 */
/* move-object v6, p4 */
/* move-object v7, p2 */
/* move v8, p3 */
/* invoke-direct/range {v3 ..v8}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;-><init>(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;ILmiui/hardware/input/IDeviceShareStateChangedListener;Landroid/os/ParcelFileDescriptor;I)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_1 */
/* move-object v3, v9 */
/* .line 64 */
/* .local v3, "newRecord":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
try { // :try_start_2
/* .line 65 */
/* .local v4, "binder":Landroid/os/IBinder; */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_1 */
/* .line 69 */
} // .end local v4 # "binder":Landroid/os/IBinder;
/* nop */
/* .line 70 */
try { // :try_start_3
v2 = this.mRecords;
(( android.util.SparseArray ) v2 ).put ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
/* .line 72 */
v2 = this.mMiuiInputManagerInternal;
/* .line 73 */
(( android.os.ParcelFileDescriptor ) p2 ).getFileDescriptor ( ); // invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
/* .line 72 */
(( com.android.server.input.MiuiInputManagerInternal ) v2 ).setDeviceShareListener ( p1, v4, p3 ); // invoke-virtual {v2, p1, v4, p3}, Lcom/android/server/input/MiuiInputManagerInternal;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V
/* .line 74 */
/* monitor-exit v0 */
return;
/* .line 66 */
/* :catch_0 */
/* move-exception v2 */
/* .line 68 */
/* .local v2, "e":Landroid/os/RemoteException; */
/* new-instance v4, Ljava/lang/RuntimeException; */
/* invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
} // .end local p0 # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
} // .end local p1 # "pid":I
} // .end local p2 # "fd":Landroid/os/ParcelFileDescriptor;
} // .end local p3 # "flags":I
} // .end local p4 # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener;
/* throw v4 */
/* .line 77 */
} // .end local v2 # "e":Landroid/os/RemoteException;
} // .end local v3 # "newRecord":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
/* .restart local p0 # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "fd":Landroid/os/ParcelFileDescriptor; */
/* .restart local p3 # "flags":I */
/* .restart local p4 # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener; */
} // :cond_2
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fgetmParcelFileDescriptor ( v1 );
/* .line 78 */
/* .local v3, "oldFd":Landroid/os/ParcelFileDescriptor; */
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fgetmListener ( v1 );
/* .line 80 */
/* .local v4, "oldBinder":Landroid/os/IBinder; */
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fputmParcelFileDescriptor ( v1,p2 );
/* .line 81 */
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fputmListener ( v1,p4 );
/* .line 82 */
com.miui.server.input.deviceshare.MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord .-$$Nest$fputmFlags ( v1,p3 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* .line 85 */
try { // :try_start_4
/* .line 86 */
/* .local v5, "binder":Landroid/os/IBinder; */
/* :try_end_4 */
/* .catch Landroid/os/RemoteException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_1 */
/* .line 91 */
} // .end local v5 # "binder":Landroid/os/IBinder;
/* nop */
/* .line 93 */
try { // :try_start_5
v5 = this.mMiuiInputManagerInternal;
(( android.os.ParcelFileDescriptor ) p2 ).getFileDescriptor ( ); // invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;
(( com.android.server.input.MiuiInputManagerInternal ) v5 ).setDeviceShareListener ( p1, v6, p3 ); // invoke-virtual {v5, p1, v6, p3}, Lcom/android/server/input/MiuiInputManagerInternal;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V
/* .line 96 */
/* invoke-direct {p0, v3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->closeSocket(Landroid/os/ParcelFileDescriptor;)V */
/* .line 98 */
/* .line 99 */
/* nop */
} // .end local v1 # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
} // .end local v3 # "oldFd":Landroid/os/ParcelFileDescriptor;
} // .end local v4 # "oldBinder":Landroid/os/IBinder;
/* monitor-exit v0 */
/* .line 100 */
return;
/* .line 87 */
/* .restart local v1 # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord; */
/* .restart local v3 # "oldFd":Landroid/os/ParcelFileDescriptor; */
/* .restart local v4 # "oldBinder":Landroid/os/IBinder; */
/* :catch_1 */
/* move-exception v2 */
/* .line 89 */
/* .restart local v2 # "e":Landroid/os/RemoteException; */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->onListenerBinderDied(I)V */
/* .line 90 */
/* new-instance v5, Ljava/lang/RuntimeException; */
/* invoke-direct {v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V */
} // .end local p0 # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
} // .end local p1 # "pid":I
} // .end local p2 # "fd":Landroid/os/ParcelFileDescriptor;
} // .end local p3 # "flags":I
} // .end local p4 # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener;
/* throw v5 */
/* .line 99 */
} // .end local v1 # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
} // .end local v2 # "e":Landroid/os/RemoteException;
} // .end local v3 # "oldFd":Landroid/os/ParcelFileDescriptor;
} // .end local v4 # "oldBinder":Landroid/os/IBinder;
/* .restart local p0 # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager; */
/* .restart local p1 # "pid":I */
/* .restart local p2 # "fd":Landroid/os/ParcelFileDescriptor; */
/* .restart local p3 # "flags":I */
/* .restart local p4 # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener; */
/* :catchall_1 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* throw v1 */
/* .line 53 */
} // :cond_3
} // :goto_0
final String v0 = "MiuiDeviceShareManager"; // const-string v0, "MiuiDeviceShareManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "pid "; // const-string v2, "pid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " setDeviceShareListener fail, because fd or listener is null"; // const-string v2, " setDeviceShareListener fail, because fd or listener is null"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v1 );
/* .line 55 */
return;
} // .end method
public void setTouchpadButtonState ( Integer p0, Integer p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "deviceId" # I */
/* .param p3, "isDown" # Z */
/* .line 169 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "process "; // const-string v1, "process "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " request device"; // const-string v1, " request device"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "button state, isDown = "; // const-string v1, "button state, isDown = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiDeviceShareManager"; // const-string v1, "MiuiDeviceShareManager"
android.util.Slog .w ( v1,v0 );
/* .line 171 */
v0 = this.mMiuiInputManagerInternal;
(( com.android.server.input.MiuiInputManagerInternal ) v0 ).setTouchpadButtonState ( p2, p3 ); // invoke-virtual {v0, p2, p3}, Lcom/android/server/input/MiuiInputManagerInternal;->setTouchpadButtonState(IZ)V
/* .line 172 */
return;
} // .end method
