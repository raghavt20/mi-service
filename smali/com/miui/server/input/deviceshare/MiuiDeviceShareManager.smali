.class public Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
.super Ljava/lang/Object;
.source "MiuiDeviceShareManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;,
        Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$MiuiDeviceShareManagerHolder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiDeviceShareManager"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private final mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private final mRecords:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$monListenerBinderDied(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->onListenerBinderDied(I)V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mHandler:Landroid/os/Handler;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mLock:Ljava/lang/Object;

    .line 37
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager-IA;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;-><init>()V

    return-void
.end method

.method private closeSocket(Landroid/os/ParcelFileDescriptor;)V
    .locals 3
    .param p1, "parcelFileDescriptor"    # Landroid/os/ParcelFileDescriptor;

    .line 145
    const-string v0, "MiuiDeviceShareManager"

    if-nez p1, :cond_0

    .line 146
    return-void

    .line 149
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeSocket "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    goto :goto_0

    .line 151
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 154
    .end local v1    # "e":Ljava/io/IOException;
    :goto_0
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
    .locals 1

    .line 210
    invoke-static {}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$MiuiDeviceShareManagerHolder;->-$$Nest$sfgetsInstance()Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    move-result-object v0

    return-object v0
.end method

.method private onListenerBinderDied(I)V
    .locals 3
    .param p1, "pid"    # I

    .line 119
    const-string v0, "MiuiDeviceShareManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " binder died"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 122
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;

    invoke-direct {p0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V

    .line 123
    monitor-exit v0

    .line 124
    return-void

    .line 123
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V
    .locals 5
    .param p1, "record"    # Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;

    .line 128
    if-nez p1, :cond_0

    .line 129
    return-void

    .line 132
    :cond_0
    invoke-static {p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fgetmListener(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)Lmiui/hardware/input/IDeviceShareStateChangedListener;

    move-result-object v0

    invoke-interface {v0}, Lmiui/hardware/input/IDeviceShareStateChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 133
    .local v0, "binder":Landroid/os/IBinder;
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 134
    invoke-static {p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fgetmPid(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)I

    move-result v2

    .line 136
    .local v2, "pid":I
    invoke-static {p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fgetmParcelFileDescriptor(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->closeSocket(Landroid/os/ParcelFileDescriptor;)V

    .line 138
    iget-object v3, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->remove(I)V

    .line 140
    iget-object v3, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V

    .line 141
    return-void
.end method


# virtual methods
.method public dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 157
    const-string v0, "MiuiDeviceShareManager(Java) State:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 158
    const-string v0, "  "

    .line 159
    .local v0, "prefix":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mRecords: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 161
    const-string v1, "<EMPTY>"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void
.end method

.method public onSocketBroken(I)V
    .locals 3
    .param p1, "pid"    # I

    .line 103
    const-string v0, "MiuiDeviceShareManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " socket broken"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 106
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;

    .line 107
    .local v1, "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    if-nez v1, :cond_0

    .line 109
    monitor-exit v0

    return-void

    .line 112
    :cond_0
    invoke-direct {p0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V

    .line 113
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mHandler:Landroid/os/Handler;

    invoke-static {v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 116
    return-void

    .line 113
    .end local v1    # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setDeviceShareListener(ILandroid/os/ParcelFileDescriptor;ILmiui/hardware/input/IDeviceShareStateChangedListener;)V
    .locals 10
    .param p1, "pid"    # I
    .param p2, "fd"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "flags"    # I
    .param p4, "listener"    # Lmiui/hardware/input/IDeviceShareStateChangedListener;

    .line 43
    const-string v0, "MiuiDeviceShareManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " request set device share listener, flag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 44
    invoke-static {p3}, Lmiui/hardware/input/MiuiInputManager;->deviceShareFlagToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    if-nez p3, :cond_0

    .line 47
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;

    invoke-direct {p0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->removeRecordLocked(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)V

    .line 49
    monitor-exit v0

    .line 50
    return-void

    .line 49
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 52
    :cond_0
    if-eqz p2, :cond_3

    if-nez p4, :cond_1

    goto/16 :goto_0

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 58
    :try_start_1
    iget-object v1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;

    .line 59
    .local v1, "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 61
    new-instance v9, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;

    move-object v3, v9

    move-object v4, p0

    move v5, p1

    move-object v6, p4

    move-object v7, p2

    move v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;-><init>(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;ILmiui/hardware/input/IDeviceShareStateChangedListener;Landroid/os/ParcelFileDescriptor;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v3, v9

    .line 64
    .local v3, "newRecord":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    :try_start_2
    invoke-interface {p4}, Lmiui/hardware/input/IDeviceShareStateChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    .line 65
    .local v4, "binder":Landroid/os/IBinder;
    invoke-interface {v4, v3, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 69
    .end local v4    # "binder":Landroid/os/IBinder;
    nop

    .line 70
    :try_start_3
    iget-object v2, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mRecords:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 72
    iget-object v2, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 73
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v4

    .line 72
    invoke-virtual {v2, p1, v4, p3}, Lcom/android/server/input/MiuiInputManagerInternal;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V

    .line 74
    monitor-exit v0

    return-void

    .line 66
    :catch_0
    move-exception v2

    .line 68
    .local v2, "e":Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
    .end local p1    # "pid":I
    .end local p2    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local p3    # "flags":I
    .end local p4    # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener;
    throw v4

    .line 77
    .end local v2    # "e":Landroid/os/RemoteException;
    .end local v3    # "newRecord":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    .restart local p0    # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
    .restart local p1    # "pid":I
    .restart local p2    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local p3    # "flags":I
    .restart local p4    # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener;
    :cond_2
    invoke-static {v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fgetmParcelFileDescriptor(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    .line 78
    .local v3, "oldFd":Landroid/os/ParcelFileDescriptor;
    invoke-static {v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fgetmListener(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)Lmiui/hardware/input/IDeviceShareStateChangedListener;

    move-result-object v4

    invoke-interface {v4}, Lmiui/hardware/input/IDeviceShareStateChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    .line 80
    .local v4, "oldBinder":Landroid/os/IBinder;
    invoke-static {v1, p2}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fputmParcelFileDescriptor(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;Landroid/os/ParcelFileDescriptor;)V

    .line 81
    invoke-static {v1, p4}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fputmListener(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;Lmiui/hardware/input/IDeviceShareStateChangedListener;)V

    .line 82
    invoke-static {v1, p3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->-$$Nest$fputmFlags(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 85
    :try_start_4
    invoke-interface {p4}, Lmiui/hardware/input/IDeviceShareStateChangedListener;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    .line 86
    .local v5, "binder":Landroid/os/IBinder;
    invoke-interface {v5, v1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 91
    .end local v5    # "binder":Landroid/os/IBinder;
    nop

    .line 93
    :try_start_5
    iget-object v5, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v5, p1, v6, p3}, Lcom/android/server/input/MiuiInputManagerInternal;->setDeviceShareListener(ILjava/io/FileDescriptor;I)V

    .line 96
    invoke-direct {p0, v3}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->closeSocket(Landroid/os/ParcelFileDescriptor;)V

    .line 98
    invoke-interface {v4, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 99
    nop

    .end local v1    # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    .end local v3    # "oldFd":Landroid/os/ParcelFileDescriptor;
    .end local v4    # "oldBinder":Landroid/os/IBinder;
    monitor-exit v0

    .line 100
    return-void

    .line 87
    .restart local v1    # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    .restart local v3    # "oldFd":Landroid/os/ParcelFileDescriptor;
    .restart local v4    # "oldBinder":Landroid/os/IBinder;
    :catch_1
    move-exception v2

    .line 89
    .restart local v2    # "e":Landroid/os/RemoteException;
    invoke-direct {p0, p1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->onListenerBinderDied(I)V

    .line 90
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    .end local p0    # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
    .end local p1    # "pid":I
    .end local p2    # "fd":Landroid/os/ParcelFileDescriptor;
    .end local p3    # "flags":I
    .end local p4    # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener;
    throw v5

    .line 99
    .end local v1    # "record":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
    .end local v2    # "e":Landroid/os/RemoteException;
    .end local v3    # "oldFd":Landroid/os/ParcelFileDescriptor;
    .end local v4    # "oldBinder":Landroid/os/IBinder;
    .restart local p0    # "this":Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
    .restart local p1    # "pid":I
    .restart local p2    # "fd":Landroid/os/ParcelFileDescriptor;
    .restart local p3    # "flags":I
    .restart local p4    # "listener":Lmiui/hardware/input/IDeviceShareStateChangedListener;
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1

    .line 53
    :cond_3
    :goto_0
    const-string v0, "MiuiDeviceShareManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setDeviceShareListener fail, because fd or listener is null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method

.method public setTouchpadButtonState(IIZ)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "deviceId"    # I
    .param p3, "isDown"    # Z

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "process "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " request device"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "button state, isDown = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiDeviceShareManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v0, p2, p3}, Lcom/android/server/input/MiuiInputManagerInternal;->setTouchpadButtonState(IZ)V

    .line 172
    return-void
.end method
