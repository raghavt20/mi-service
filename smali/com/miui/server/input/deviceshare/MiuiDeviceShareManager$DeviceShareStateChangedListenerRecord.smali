.class final Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;
.super Ljava/lang/Object;
.source "MiuiDeviceShareManager.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DeviceShareStateChangedListenerRecord"
.end annotation


# instance fields
.field private mFlags:I

.field private mListener:Lmiui/hardware/input/IDeviceShareStateChangedListener;

.field private mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

.field private final mPid:I

.field final synthetic this$0:Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;


# direct methods
.method static bridge synthetic -$$Nest$fgetmListener(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)Lmiui/hardware/input/IDeviceShareStateChangedListener;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mListener:Lmiui/hardware/input/IDeviceShareStateChangedListener;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmParcelFileDescriptor(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)Landroid/os/ParcelFileDescriptor;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPid(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mPid:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmFlags(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mFlags:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmListener(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;Lmiui/hardware/input/IDeviceShareStateChangedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mListener:Lmiui/hardware/input/IDeviceShareStateChangedListener;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmParcelFileDescriptor(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    return-void
.end method

.method public constructor <init>(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;ILmiui/hardware/input/IDeviceShareStateChangedListener;Landroid/os/ParcelFileDescriptor;I)V
    .locals 0
    .param p2, "mPid"    # I
    .param p3, "mListener"    # Lmiui/hardware/input/IDeviceShareStateChangedListener;
    .param p4, "parcelFileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p5, "flags"    # I

    .line 183
    iput-object p1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->this$0:Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput p2, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mPid:I

    .line 186
    iput-object p3, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mListener:Lmiui/hardware/input/IDeviceShareStateChangedListener;

    .line 187
    iput-object p4, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mParcelFileDescriptor:Landroid/os/ParcelFileDescriptor;

    .line 188
    iput p5, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mFlags:I

    .line 189
    return-void
.end method


# virtual methods
.method public binderDied()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->this$0:Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;

    iget v1, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mPid:I

    invoke-static {v0, v1}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;->-$$Nest$monListenerBinderDied(Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager;I)V

    .line 194
    return-void
.end method

.method public notifySocketBroken()V
    .locals 1

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->mListener:Lmiui/hardware/input/IDeviceShareStateChangedListener;

    invoke-interface {v0}, Lmiui/hardware/input/IDeviceShareStateChangedListener;->onSocketBroken()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0}, Lcom/miui/server/input/deviceshare/MiuiDeviceShareManager$DeviceShareStateChangedListenerRecord;->binderDied()V

    .line 202
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method
