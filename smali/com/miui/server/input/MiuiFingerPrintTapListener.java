public class com.miui.server.input.MiuiFingerPrintTapListener {
	 /* .source "MiuiFingerPrintTapListener.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/MiuiFingerPrintTapListener$H;, */
	 /* Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;, */
	 /* Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer FINGERPRINT_PRODUCT;
private static final Integer FINGERPRINT_VENDOR;
private static final java.lang.String TAG;
/* # instance fields */
private android.content.Context mContext;
private Integer mCurrentUserId;
private java.lang.String mDoubleTapSideFp;
private com.miui.server.input.MiuiFingerPrintTapListener$H mHandler;
private Boolean mIsFeatureSupport;
private Boolean mIsRegisterListener;
private Boolean mIsSetupComplete;
private com.miui.server.input.MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener mMiuiFingerPrintTapGestureListener;
private com.miui.server.input.gesture.MiuiGestureMonitor mMiuiGestureMonitor;
private com.miui.server.input.MiuiFingerPrintTapListener$MiuiSettingsObserver mMiuiSettingsObserver;
/* # direct methods */
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.MiuiFingerPrintTapListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static java.lang.String -$$Nest$fgetmDoubleTapSideFp ( com.miui.server.input.MiuiFingerPrintTapListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDoubleTapSideFp;
} // .end method
static void -$$Nest$fputmDoubleTapSideFp ( com.miui.server.input.MiuiFingerPrintTapListener p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mDoubleTapSideFp = p1;
	 return;
} // .end method
static void -$$Nest$mtoggleFpDoubleTap ( com.miui.server.input.MiuiFingerPrintTapListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->toggleFpDoubleTap()V */
	 return;
} // .end method
static void -$$Nest$mupdatePrintTapState ( com.miui.server.input.MiuiFingerPrintTapListener p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->updatePrintTapState()V */
	 return;
} // .end method
public com.miui.server.input.MiuiFingerPrintTapListener ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 41 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 29 */
	 int v0 = -2; // const/4 v0, -0x2
	 /* iput v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mCurrentUserId:I */
	 /* .line 42 */
	 final String v0 = "is_support_fingerprint_tap"; // const-string v0, "is_support_fingerprint_tap"
	 int v1 = 0; // const/4 v1, 0x0
	 v0 = 	 miui.util.FeatureParser .getBoolean ( v0,v1 );
	 /* iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsFeatureSupport:Z */
	 /* .line 43 */
	 this.mContext = p1;
	 /* .line 44 */
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 45 */
		 /* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->initialize()V */
		 /* .line 47 */
	 } // :cond_0
	 return;
} // .end method
private Boolean checkEmpty ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "feature" # Ljava/lang/String; */
	 /* .line 126 */
	 v0 = 	 android.text.TextUtils .isEmpty ( p1 );
	 /* if-nez v0, :cond_1 */
	 final String v0 = "none"; // const-string v0, "none"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private void initialize ( ) {
/* .locals 2 */
/* .line 50 */
/* new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener$H; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;-><init>(Lcom/miui/server/input/MiuiFingerPrintTapListener;Lcom/miui/server/input/MiuiFingerPrintTapListener$H-IA;)V */
this.mHandler = v0;
/* .line 51 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->isUserSetUp()Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z */
/* .line 52 */
/* new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener; */
/* invoke-direct {v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;-><init>()V */
this.mMiuiFingerPrintTapGestureListener = v0;
/* .line 53 */
v0 = this.mContext;
com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( v0 );
this.mMiuiGestureMonitor = v0;
/* .line 54 */
/* new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/MiuiFingerPrintTapListener;Landroid/os/Handler;)V */
this.mMiuiSettingsObserver = v0;
/* .line 55 */
(( com.miui.server.input.MiuiFingerPrintTapListener$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->observe()V
/* .line 56 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->updateSettings()V */
/* .line 57 */
return;
} // .end method
private Boolean isUserSetUp ( ) {
/* .locals 4 */
/* .line 130 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "user_setup_complete" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
} // .end method
private void removeUnsupportedFunction ( ) {
/* .locals 4 */
/* .line 107 */
final String v0 = "launch_alipay_health_code"; // const-string v0, "launch_alipay_health_code"
v1 = this.mDoubleTapSideFp;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = this.mDoubleTapSideFp;
/* .line 108 */
final String v1 = "launch_ai_shortcut"; // const-string v1, "launch_ai_shortcut"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 109 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "none"; // const-string v1, "none"
int v2 = -2; // const/4 v2, -0x2
final String v3 = "fingerprint_double_tap"; // const-string v3, "fingerprint_double_tap"
android.provider.Settings$System .putStringForUser ( v0,v3,v1,v2 );
/* .line 113 */
} // :cond_1
return;
} // .end method
private void toggleFpDoubleTap ( ) {
/* .locals 5 */
/* .line 87 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
v1 = this.mDoubleTapSideFp;
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
final String v4 = "fingerprint_double_tap"; // const-string v4, "fingerprint_double_tap"
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 89 */
return;
} // .end method
private void updatePrintTapState ( ) {
/* .locals 2 */
/* .line 116 */
v0 = this.mDoubleTapSideFp;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->checkEmpty(Ljava/lang/String;)Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z */
/* if-nez v0, :cond_0 */
/* .line 117 */
v0 = this.mMiuiGestureMonitor;
v1 = this.mMiuiFingerPrintTapGestureListener;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).registerPointerEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 118 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z */
/* .line 119 */
} // :cond_0
v0 = this.mDoubleTapSideFp;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->checkEmpty(Ljava/lang/String;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 120 */
v0 = this.mMiuiGestureMonitor;
v1 = this.mMiuiFingerPrintTapGestureListener;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v0 ).unregisterPointerEventListener ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V
/* .line 121 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z */
/* .line 123 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void updateSettings ( ) {
/* .locals 3 */
/* .line 99 */
v0 = this.mMiuiSettingsObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 100 */
/* nop */
/* .line 101 */
final String v1 = "fingerprint_double_tap"; // const-string v1, "fingerprint_double_tap"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 100 */
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.MiuiFingerPrintTapListener$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 102 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->removeUnsupportedFunction()V */
/* .line 104 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 1 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 135 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 136 */
final String v0 = "MiuiFingerPrintTapListener"; // const-string v0, "MiuiFingerPrintTapListener"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 137 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 138 */
final String v0 = "mIsSetupComplete="; // const-string v0, "mIsSetupComplete="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 139 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 140 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 141 */
final String v0 = "mDoubleTapSideFp="; // const-string v0, "mDoubleTapSideFp="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 142 */
v0 = this.mDoubleTapSideFp;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 144 */
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newUserId" # I */
/* .line 92 */
/* iget v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mCurrentUserId:I */
/* if-eq v0, p1, :cond_0 */
/* .line 93 */
/* iput p1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mCurrentUserId:I */
/* .line 94 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->updateSettings()V */
/* .line 96 */
} // :cond_0
return;
} // .end method
public Boolean shouldInterceptSlideFpTapKey ( android.view.KeyEvent p0, Boolean p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .param p2, "isScreenOn" # Z */
/* .param p3, "keyguard" # Z */
/* .line 60 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* const/16 v1, 0x62 */
/* if-ne v0, v1, :cond_4 */
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
v0 = (( android.view.InputDevice ) v0 ).getVendorId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I
/* const/16 v1, 0x666 */
/* if-ne v0, v1, :cond_4 */
/* .line 61 */
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
v0 = (( android.view.InputDevice ) v0 ).getProductId ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I
/* const/16 v1, 0x888 */
/* if-ne v0, v1, :cond_4 */
/* .line 62 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_3 */
if ( p2 != null) { // if-eqz p2, :cond_3
/* .line 63 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z */
/* if-nez v0, :cond_0 */
/* .line 64 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->isUserSetUp()Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z */
/* .line 65 */
/* if-nez v0, :cond_0 */
/* .line 67 */
} // :cond_0
final String v0 = "MiuiFingerPrintTapListener"; // const-string v0, "MiuiFingerPrintTapListener"
if ( p3 != null) { // if-eqz p3, :cond_1
/* .line 68 */
final String v2 = "Keyguard active, so fingerprint double tap can\'t trigger function"; // const-string v2, "Keyguard active, so fingerprint double tap can\'t trigger function"
android.util.Slog .d ( v0,v2 );
/* .line 69 */
/* .line 71 */
} // :cond_1
v2 = this.mMiuiFingerPrintTapGestureListener;
v2 = (( com.miui.server.input.MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener ) v2 ).isFingerPressing ( ); // invoke-virtual {v2}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->isFingerPressing()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 72 */
final String v2 = "finger pressing, so fingerprint double tap can\'t trigger function"; // const-string v2, "finger pressing, so fingerprint double tap can\'t trigger function"
android.util.Slog .d ( v0,v2 );
/* .line 73 */
/* .line 75 */
} // :cond_2
v0 = this.mDoubleTapSideFp;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->checkEmpty(Ljava/lang/String;)Z */
/* if-nez v0, :cond_3 */
/* .line 76 */
v0 = this.mHandler;
final String v2 = "fingerprint_double_tap"; // const-string v2, "fingerprint_double_tap"
(( com.miui.server.input.MiuiFingerPrintTapListener$H ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
(( com.miui.server.input.MiuiFingerPrintTapListener$H ) v0 ).sendMessage ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;->sendMessage(Landroid/os/Message;)Z
/* .line 80 */
} // :cond_3
/* .line 82 */
} // :cond_4
int v0 = 0; // const/4 v0, 0x0
} // .end method
