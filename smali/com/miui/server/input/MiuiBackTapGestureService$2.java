class com.miui.server.input.MiuiBackTapGestureService$2 implements java.lang.Runnable {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiBackTapGestureService this$0; //synthetic
/* # direct methods */
 com.miui.server.input.MiuiBackTapGestureService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiBackTapGestureService; */
/* .line 153 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 3 */
/* .line 156 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapSensorWrapper ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 157 */
	 v0 = this.this$0;
	 v0 = 	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmScreenOn ( v0 );
	 int v1 = 0; // const/4 v1, 0x0
	 if ( v0 != null) { // if-eqz v0, :cond_2
		 /* .line 159 */
		 v0 = this.this$0;
		 v0 = 		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsBackTapSensorListenerRegistered ( v0 );
		 /* if-nez v0, :cond_1 */
		 v0 = this.this$0;
		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackDoubleTapFunction ( v0 );
		 v0 = 		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mcheckEmpty ( v0,v2 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 v0 = this.this$0;
			 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTripleTapFunction ( v0 );
			 /* .line 160 */
			 v0 = 			 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mcheckEmpty ( v0,v2 );
			 /* if-nez v0, :cond_1 */
			 /* .line 161 */
		 } // :cond_0
		 v0 = this.this$0;
		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapSensorWrapper ( v0 );
		 v1 = this.this$0;
		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapSensorChangeListener ( v1 );
		 (( miui.util.BackTapSensorWrapper ) v0 ).registerListener ( v1 ); // invoke-virtual {v0, v1}, Lmiui/util/BackTapSensorWrapper;->registerListener(Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;)V
		 /* .line 162 */
		 v0 = this.this$0;
		 int v1 = 1; // const/4 v1, 0x1
		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fputmIsBackTapSensorListenerRegistered ( v0,v1 );
		 /* .line 163 */
		 v0 = this.this$0;
		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapTouchHelper ( v0 );
		 com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$msetTouchTrackingEnabled ( v0,v1 );
		 /* .line 165 */
	 } // :cond_1
	 v0 = this.this$0;
	 v0 = 	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsBackTapSensorListenerRegistered ( v0 );
	 if ( v0 != null) { // if-eqz v0, :cond_3
		 v0 = this.this$0;
		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackDoubleTapFunction ( v0 );
		 v0 = 		 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mcheckEmpty ( v0,v2 );
		 if ( v0 != null) { // if-eqz v0, :cond_3
			 v0 = this.this$0;
			 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTripleTapFunction ( v0 );
			 /* .line 166 */
			 v0 = 			 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mcheckEmpty ( v0,v2 );
			 if ( v0 != null) { // if-eqz v0, :cond_3
				 /* .line 167 */
				 v0 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapSensorWrapper ( v0 );
				 v2 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapSensorChangeListener ( v2 );
				 (( miui.util.BackTapSensorWrapper ) v0 ).unregisterListener ( v2 ); // invoke-virtual {v0, v2}, Lmiui/util/BackTapSensorWrapper;->unregisterListener(Lmiui/util/BackTapSensorWrapper$BackTapSensorChangeListener;)V
				 /* .line 168 */
				 v0 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fputmIsBackTapSensorListenerRegistered ( v0,v1 );
				 /* .line 169 */
				 v0 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapTouchHelper ( v0 );
				 com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$msetTouchTrackingEnabled ( v0,v1 );
				 /* .line 173 */
			 } // :cond_2
			 v0 = this.this$0;
			 v0 = 			 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsBackTapSensorListenerRegistered ( v0 );
			 if ( v0 != null) { // if-eqz v0, :cond_3
				 /* .line 174 */
				 v0 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapSensorWrapper ( v0 );
				 (( miui.util.BackTapSensorWrapper ) v0 ).unregisterAllListeners ( ); // invoke-virtual {v0}, Lmiui/util/BackTapSensorWrapper;->unregisterAllListeners()V
				 /* .line 175 */
				 v0 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fputmIsBackTapSensorListenerRegistered ( v0,v1 );
				 /* .line 176 */
				 v0 = this.this$0;
				 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapTouchHelper ( v0 );
				 com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$msetTouchTrackingEnabled ( v0,v1 );
				 /* .line 180 */
			 } // :cond_3
		 } // :goto_0
		 return;
	 } // .end method
