.class public Lcom/miui/server/input/edgesuppression/SuppressionRect;
.super Ljava/lang/Object;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;,
        Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;
    }
.end annotation


# static fields
.field private static final NODE_DEFAULT:I

.field private static final TIME_DEFAULT:I


# instance fields
.field private bottomRightX:I

.field private bottomRightY:I

.field private list:Ljava/util/ArrayList;

.field private node:I

.field private position:I

.field private time:I

.field private topLeftX:I

.field private topLeftY:I

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    .line 21
    iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    .line 22
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "position"    # I

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    .line 25
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I

    .line 26
    iput p2, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    .line 28
    iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    .line 29
    return-void
.end method


# virtual methods
.method public getBottomRightX()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I

    return v0
.end method

.method public getBottomRightY()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I

    return v0
.end method

.method public getList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNode()I
    .locals 1

    .line 88
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I

    return v0
.end method

.method public getTime()I
    .locals 1

    .line 80
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    return v0
.end method

.method public getTopLeftX()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I

    return v0
.end method

.method public getTopLeftY()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I

    return v0
.end method

.method public getType()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I

    return v0
.end method

.method public setBottomRightX(I)V
    .locals 0
    .param p1, "bottomRightX"    # I

    .line 68
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I

    .line 69
    return-void
.end method

.method public setBottomRightY(I)V
    .locals 0
    .param p1, "bottomRightY"    # I

    .line 76
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I

    .line 77
    return-void
.end method

.method public setEmpty()V
    .locals 1

    .line 108
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setTopLeftX(I)V

    .line 109
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setBottomRightY(I)V

    .line 110
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setBottomRightX(I)V

    .line 111
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setBottomRightY(I)V

    .line 112
    return-void
.end method

.method public setNode(I)V
    .locals 0
    .param p1, "node"    # I

    .line 92
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    .line 93
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .line 44
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I

    .line 45
    return-void
.end method

.method public setTime(I)V
    .locals 0
    .param p1, "time"    # I

    .line 84
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    .line 85
    return-void
.end method

.method public setTopLeftX(I)V
    .locals 0
    .param p1, "topLeftX"    # I

    .line 52
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I

    .line 53
    return-void
.end method

.method public setTopLeftY(I)V
    .locals 0
    .param p1, "topLeftY"    # I

    .line 60
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I

    .line 61
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .line 36
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I

    .line 37
    return-void
.end method

.method public setValue(IIIIII)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "position"    # I
    .param p3, "topLeftX"    # I
    .param p4, "topLeftY"    # I
    .param p5, "bottomRightX"    # I
    .param p6, "bottomRightY"    # I

    .line 97
    iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I

    .line 98
    iput p2, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I

    .line 99
    iput p3, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I

    .line 100
    iput p4, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I

    .line 101
    iput p5, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I

    .line 102
    iput p6, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    .line 104
    iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    .line 105
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SuppressionRect{list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->list:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", position="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", topLeftX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", topLeftY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bottomRightX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bottomRightY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", node="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
