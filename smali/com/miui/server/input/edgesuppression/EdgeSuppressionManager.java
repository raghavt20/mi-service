public class com.miui.server.input.edgesuppression.EdgeSuppressionManager {
	 /* .source "EdgeSuppressionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;, */
	 /* Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;, */
	 /* Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver; */
	 /* } */
} // .end annotation
/* # static fields */
public static final java.lang.String HORIZONTAL_EDGE_SUPPRESSION_SIZE;
private static final Integer INDEX_OF_DEFAULT;
private static final Integer INDEX_OF_MAX;
private static final Integer INDEX_OF_MIN;
private static final Integer INDEX_OF_STRONG;
private static final Integer INDEX_OF_WAKE;
public static final Boolean IS_SUPPORT_EDGE_MODE;
private static final java.lang.String KEY_INPUTMETHOD_SIZE;
private static final java.lang.String KEY_SCREEN_EDGE_MODE_CUSTOM;
private static final java.lang.String KEY_SCREEN_EDGE_MODE_DEFAULT;
private static final java.lang.String KEY_SCREEN_EDGE_MODE_DIY;
private static final java.lang.String KEY_SCREEN_EDGE_MODE_STRONG;
private static final java.lang.String KEY_SCREEN_EDGE_MODE_WAKE;
public static final java.lang.String REASON_OF_CONFIGURATION;
public static final java.lang.String REASON_OF_GAMEBOOSTER;
public static final java.lang.String REASON_OF_HOLDSENSOR;
public static final java.lang.String REASON_OF_LAYSENSOR;
public static final java.lang.String REASON_OF_ROTATION;
public static final java.lang.String REASON_OF_SCREENON;
public static final java.lang.String REASON_OF_SETTINGS;
public static final Boolean SHOULD_REMOVE_EDGE_SETTINGS;
private static final java.lang.String SUPPORT_SENSOR;
private static final java.lang.String TAG;
private static final java.lang.String TRACK_EDGE_TYPE_DEFAULT;
private static final java.lang.String TRACK_EVENT_TIME;
public static final java.lang.String VERTICAL_EDGE_SUPPRESSION_SIZE;
private static volatile com.miui.server.input.edgesuppression.EdgeSuppressionManager sInstance;
/* # instance fields */
private Integer mAbsoluteSize;
private com.miui.server.input.edgesuppression.BaseEdgeSuppression mBaseEdgeSuppression;
private Integer mConditionSize;
private final com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader mConfigLoader;
private final android.content.Context mContext;
private Float mEdgeModeSize;
public java.lang.String mEdgeModeType;
private final com.miui.server.input.edgesuppression.EdgeSuppressionInfo mEdgeSuppressionInfoList;
private Boolean mFolded;
private final android.os.Handler mHandler;
final miui.util.HoldSensorWrapper$HoldSensorChangeListener mHoldListener;
private volatile Integer mHoldSensorState;
private miui.util.HoldSensorWrapper mHoldSensorWrapper;
private Boolean mIsUserSetted;
private Integer mLastEdgeModeSize;
private java.lang.String mLastEdgeModeType;
final miui.util.LaySensorWrapper$LaySensorChangeListener mLayListener;
private volatile Integer mLaySensorState;
private miui.util.LaySensorWrapper mLaySensorWrapper;
private Integer mMaxAdjustValue;
private java.util.concurrent.ScheduledExecutorService mScheduledExecutorService;
private Integer mScreenHeight;
private Integer mScreenWidth;
private java.text.SimpleDateFormat mSimpleDateFormat;
private final com.miui.server.input.edgesuppression.EdgeSuppressionInfo mSubScreenEdgeSuppressionList;
private Boolean mSupportSensor;
private java.lang.String mTrackEventTime;
/* # direct methods */
public static void $r8$lambda$SGmOlD3MzBOyGeD6hDYNmd5x7G4 ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, Integer p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->lambda$new$1(I)V */
	 return;
} // .end method
public static void $r8$lambda$gxYSBlipA664OV0f0LDcHjqRwvM ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->lambda$new$2()V */
	 return;
} // .end method
public static void $r8$lambda$sIartofm1hh7XpB3-cTYCZ_V5Sw ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, Integer p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->lambda$new$0(I)V */
	 return;
} // .end method
static com.miui.server.input.edgesuppression.BaseEdgeSuppression -$$Nest$fgetmBaseEdgeSuppression ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBaseEdgeSuppression;
} // .end method
static com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader -$$Nest$fgetmConfigLoader ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mConfigLoader;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Float -$$Nest$fgetmEdgeModeSize ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F */
} // .end method
static com.miui.server.input.edgesuppression.EdgeSuppressionInfo -$$Nest$fgetmEdgeSuppressionInfoList ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mEdgeSuppressionInfoList;
} // .end method
static Integer -$$Nest$fgetmHoldSensorState ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I */
} // .end method
static Integer -$$Nest$fgetmLaySensorState ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
} // .end method
static Integer -$$Nest$fgetmMaxAdjustValue ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mMaxAdjustValue:I */
} // .end method
static com.miui.server.input.edgesuppression.EdgeSuppressionInfo -$$Nest$fgetmSubScreenEdgeSuppressionList ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mSubScreenEdgeSuppressionList;
} // .end method
static Boolean -$$Nest$fgetmSupportSensor ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z */
} // .end method
static void -$$Nest$fputmBaseEdgeSuppression ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, com.miui.server.input.edgesuppression.BaseEdgeSuppression p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mBaseEdgeSuppression = p1;
	 return;
} // .end method
static void -$$Nest$fputmEdgeModeSize ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, Float p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F */
	 return;
} // .end method
static void -$$Nest$fputmIsUserSetted ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mIsUserSetted:Z */
	 return;
} // .end method
static void -$$Nest$fputmLastEdgeModeSize ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeSize:I */
	 return;
} // .end method
static void -$$Nest$fputmLastEdgeModeType ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mLastEdgeModeType = p1;
	 return;
} // .end method
static void -$$Nest$fputmTrackEventTime ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mTrackEventTime = p1;
	 return;
} // .end method
static void -$$Nest$mhandleEdgeSuppressionData ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeSuppressionData()V */
	 return;
} // .end method
static void -$$Nest$mnotifyEdgeSuppressionChanged ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0, Integer p1, java.lang.String p2 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V */
	 return;
} // .end method
static void -$$Nest$mresetUserSetted ( com.miui.server.input.edgesuppression.EdgeSuppressionManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->resetUserSetted()V */
	 return;
} // .end method
static com.miui.server.input.edgesuppression.EdgeSuppressionManager ( ) {
	 /* .locals 1 */
	 /* .line 60 */
	 miui.util.ITouchFeature .getInstance ( );
	 v0 = 	 (( miui.util.ITouchFeature ) v0 ).hasSupportEdgeMode ( ); // invoke-virtual {v0}, Lmiui/util/ITouchFeature;->hasSupportEdgeMode()Z
	 com.miui.server.input.edgesuppression.EdgeSuppressionManager.IS_SUPPORT_EDGE_MODE = (v0!= 0);
	 /* .line 62 */
	 miui.util.ITouchFeature .getInstance ( );
	 v0 = 	 (( miui.util.ITouchFeature ) v0 ).shouldRemoveEdgeSettings ( ); // invoke-virtual {v0}, Lmiui/util/ITouchFeature;->shouldRemoveEdgeSettings()Z
	 com.miui.server.input.edgesuppression.EdgeSuppressionManager.SHOULD_REMOVE_EDGE_SETTINGS = (v0!= 0);
	 /* .line 61 */
	 return;
} // .end method
private com.miui.server.input.edgesuppression.EdgeSuppressionManager ( ) {
	 /* .locals 11 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 113 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 75 */
	 final String v0 = "default_suppression"; // const-string v0, "default_suppression"
	 this.mEdgeModeType = v0;
	 /* .line 82 */
	 /* const/16 v0, 0x8 */
	 /* new-array v0, v0, [Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
	 this.mEdgeSuppressionInfoList = v0;
	 /* .line 83 */
	 int v0 = 2; // const/4 v0, 0x2
	 /* new-array v0, v0, [Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
	 this.mSubScreenEdgeSuppressionList = v0;
	 /* .line 86 */
	 int v0 = -1; // const/4 v0, -0x1
	 /* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I */
	 /* .line 89 */
	 int v1 = 1; // const/4 v1, 0x1
	 /* iput-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mIsUserSetted:Z */
	 /* .line 90 */
	 final String v2 = "1970-01-01 00:00:00"; // const-string v2, "1970-01-01 00:00:00"
	 this.mTrackEventTime = v2;
	 /* .line 91 */
	 /* new-instance v2, Ljava/text/SimpleDateFormat; */
	 /* const-string/jumbo v3, "yyyy-MM-dd HH:mm:ss" */
	 /* invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
	 this.mSimpleDateFormat = v2;
	 /* .line 92 */
	 java.util.concurrent.Executors .newScheduledThreadPool ( v1 );
	 this.mScheduledExecutorService = v2;
	 /* .line 93 */
	 /* new-instance v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda0; */
	 /* invoke-direct {v2, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V */
	 this.mHoldListener = v2;
	 /* .line 102 */
	 /* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
	 /* .line 103 */
	 /* new-instance v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda1; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V */
	 this.mLayListener = v0;
	 /* .line 114 */
	 /* nop */
	 /* .line 115 */
	 /* const-string/jumbo v0, "window" */
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/view/WindowManager; */
	 /* .line 116 */
	 /* .local v0, "windowManager":Landroid/view/WindowManager; */
	 /* new-instance v2, Landroid/util/DisplayMetrics; */
	 /* invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V */
	 /* .line 117 */
	 /* .local v2, "metrics":Landroid/util/DisplayMetrics; */
	 (( android.view.Display ) v3 ).getRealMetrics ( v2 ); // invoke-virtual {v3, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V
	 /* .line 118 */
	 /* iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I */
	 /* sub-int/2addr v3, v1 */
	 /* iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I */
	 /* sub-int/2addr v4, v1 */
	 v3 = 	 java.lang.Math .min ( v3,v4 );
	 /* iput v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I */
	 /* .line 119 */
	 /* iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I */
	 /* sub-int/2addr v3, v1 */
	 /* iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I */
	 /* sub-int/2addr v4, v1 */
	 v1 = 	 java.lang.Math .max ( v3,v4 );
	 /* iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I */
	 /* .line 120 */
	 this.mContext = p1;
	 /* .line 121 */
	 /* new-instance v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader; */
	 /* invoke-direct {v1, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V */
	 this.mConfigLoader = v1;
	 /* .line 122 */
	 com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader .-$$Nest$minitEdgeSuppressionConfig ( v1 );
	 /* .line 123 */
	 /* const-string/jumbo v1, "support_edgesuppression_with_sensor" */
	 int v3 = 0; // const/4 v3, 0x0
	 v1 = 	 miui.util.FeatureParser .getBoolean ( v1,v3 );
	 /* iput-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z */
	 /* .line 124 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->initParamForOldSettings()V */
	 /* .line 125 */
	 /* nop */
	 /* .line 126 */
	 final String v1 = "normal"; // const-string v1, "normal"
	 com.miui.server.input.edgesuppression.EdgeSuppressionFactory .getEdgeSuppressionMode ( v1 );
	 this.mBaseEdgeSuppression = v1;
	 /* .line 127 */
	 /* iget v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F */
	 /* float-to-int v3, v3 */
	 (( com.miui.server.input.edgesuppression.BaseEdgeSuppression ) v1 ).initInputMethodData ( p1, v3 ); // invoke-virtual {v1, p1, v3}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->initInputMethodData(Landroid/content/Context;I)V
	 /* .line 128 */
	 /* new-instance v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler; */
	 com.android.server.input.MiuiInputThread .getThread ( );
	 (( com.android.server.input.MiuiInputThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v1, p0, v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Landroid/os/Looper;)V */
	 this.mHandler = v1;
	 /* .line 129 */
	 /* new-instance v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver; */
	 /* invoke-direct {v3, p0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Landroid/os/Handler;)V */
	 /* move-object v1, v3 */
	 /* .line 131 */
	 /* .local v1, "mMiuiEdgeSuppressionObserver":Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver; */
	 (( com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ) v1 ).registerObserver ( ); // invoke-virtual {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->registerObserver()V
	 /* .line 132 */
	 /* sget-boolean v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 133 */
		 v4 = this.mScheduledExecutorService;
		 /* new-instance v5, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda2; */
		 /* invoke-direct {v5, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V */
		 /* const-wide/16 v6, 0x5 */
		 v3 = java.util.concurrent.TimeUnit.DAYS;
		 /* .line 135 */
		 /* const-wide/16 v8, 0x1 */
		 (( java.util.concurrent.TimeUnit ) v3 ).toMinutes ( v8, v9 ); // invoke-virtual {v3, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J
		 /* move-result-wide v8 */
		 v10 = java.util.concurrent.TimeUnit.MINUTES;
		 /* .line 133 */
		 /* invoke-interface/range {v4 ..v10}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture; */
		 /* .line 137 */
	 } // :cond_0
	 return;
} // .end method
private Integer compareTimeOfNow ( java.lang.String p0, java.util.Date p1 ) {
	 /* .locals 5 */
	 /* .param p1, "dateOldString" # Ljava/lang/String; */
	 /* .param p2, "dateNow" # Ljava/util/Date; */
	 /* .line 307 */
	 v0 = 	 android.text.TextUtils .isEmpty ( p1 );
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 308 */
		 final String p1 = "1970-01-01 00:00:00"; // const-string p1, "1970-01-01 00:00:00"
		 /* .line 310 */
	 } // :cond_0
	 /* new-instance v0, Ljava/util/Date; */
	 /* invoke-direct {v0}, Ljava/util/Date;-><init>()V */
	 /* .line 312 */
	 /* .local v0, "dateOld":Ljava/util/Date; */
	 try { // :try_start_0
		 v1 = this.mSimpleDateFormat;
		 (( java.text.SimpleDateFormat ) v1 ).parse ( p1 ); // invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
		 /* :try_end_0 */
		 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* move-object v0, v1 */
		 /* .line 315 */
		 /* .line 313 */
		 /* :catch_0 */
		 /* move-exception v1 */
		 /* .line 314 */
		 /* .local v1, "e":Ljava/lang/Exception; */
		 (( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
		 /* .line 316 */
	 } // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
(( java.util.Date ) p2 ).getTime ( ); // invoke-virtual {p2}, Ljava/util/Date;->getTime()J
/* move-result-wide v1 */
(( java.util.Date ) v0 ).getTime ( ); // invoke-virtual {v0}, Ljava/util/Date;->getTime()J
/* move-result-wide v3 */
/* sub-long/2addr v1, v3 */
/* const-wide/32 v3, 0x5265c00 */
/* div-long/2addr v1, v3 */
/* long-to-int v1, v1 */
} // .end method
private Integer getAbsoluteSize ( Float p0 ) {
/* .locals 4 */
/* .param p1, "conditionSize" # F */
/* .line 205 */
v0 = this.mConfigLoader;
v0 = this.mAbsoluteLevel;
int v1 = 0; // const/4 v1, 0x0
/* aget v0, v0, v1 */
/* .line 206 */
/* .local v0, "result":I */
v2 = this.mConfigLoader;
v2 = this.mConditionLevel;
/* aget v1, v2, v1 */
/* int-to-float v1, v1 */
v1 = java.lang.Float .compare ( p1,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_0 */
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
/* aget v1, v1, v2 */
/* int-to-float v1, v1 */
/* .line 207 */
v1 = java.lang.Float .compare ( p1,v1 );
/* if-eq v1, v2, :cond_0 */
/* .line 208 */
v1 = this.mConfigLoader;
v1 = this.mAbsoluteLevel;
/* aget v0, v1, v2 */
/* .line 209 */
} // :cond_0
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
/* aget v1, v1, v2 */
/* int-to-float v1, v1 */
v1 = java.lang.Float .compare ( p1,v1 );
int v3 = 2; // const/4 v3, 0x2
/* if-ne v1, v2, :cond_1 */
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
/* aget v1, v1, v3 */
/* int-to-float v1, v1 */
/* .line 210 */
v1 = java.lang.Float .compare ( p1,v1 );
/* if-eq v1, v2, :cond_1 */
/* .line 211 */
v1 = this.mConfigLoader;
v1 = this.mAbsoluteLevel;
/* aget v0, v1, v3 */
/* .line 212 */
} // :cond_1
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
/* aget v1, v1, v3 */
/* int-to-float v1, v1 */
v1 = java.lang.Float .compare ( p1,v1 );
int v3 = 3; // const/4 v3, 0x3
/* if-ne v1, v2, :cond_2 */
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
/* aget v1, v1, v3 */
/* int-to-float v1, v1 */
/* .line 213 */
v1 = java.lang.Float .compare ( p1,v1 );
/* if-eq v1, v2, :cond_2 */
/* .line 214 */
v1 = this.mConfigLoader;
v1 = this.mAbsoluteLevel;
/* aget v0, v1, v3 */
/* .line 215 */
} // :cond_2
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
/* aget v1, v1, v3 */
/* int-to-float v1, v1 */
v1 = java.lang.Float .compare ( p1,v1 );
/* if-ne v1, v2, :cond_3 */
v1 = this.mConfigLoader;
v1 = this.mConditionLevel;
int v3 = 4; // const/4 v3, 0x4
/* aget v1, v1, v3 */
/* int-to-float v1, v1 */
/* .line 216 */
v1 = java.lang.Float .compare ( p1,v1 );
/* if-eq v1, v2, :cond_3 */
/* .line 217 */
v1 = this.mConfigLoader;
v1 = this.mAbsoluteLevel;
/* aget v0, v1, v3 */
/* .line 219 */
} // :cond_3
} // :goto_0
} // .end method
private com.miui.server.input.edgesuppression.EdgeSuppressionInfo getEdgeSuppressionInfo ( Float p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "edgeModeSize" # F */
/* .param p2, "rotation" # I */
/* .line 167 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
int v1 = -1; // const/4 v1, -0x1
int v2 = 2; // const/4 v2, 0x2
/* if-eq v0, v1, :cond_0 */
/* .line 168 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 176 */
/* :pswitch_0 */
int p1 = 0; // const/4 p1, 0x0
/* .line 177 */
/* .line 173 */
/* :pswitch_1 */
v0 = this.mConfigLoader;
v0 = this.mConditionLevel;
int v1 = 3; // const/4 v1, 0x3
/* aget v0, v0, v1 */
/* int-to-float p1, v0 */
/* .line 174 */
/* .line 170 */
/* :pswitch_2 */
v0 = this.mConfigLoader;
v0 = this.mConditionLevel;
/* aget v0, v0, v2 */
/* int-to-float p1, v0 */
/* .line 171 */
/* nop */
/* .line 182 */
} // :cond_0
} // :goto_0
/* float-to-int v0, p1 */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConditionSize:I */
/* .line 183 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getAbsoluteSize(F)I */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mAbsoluteSize:I */
/* .line 185 */
v0 = this.mConfigLoader;
v1 = this.mEdgeSuppressionInfoList;
v3 = this.mEdgeModeType;
com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader .-$$Nest$mgetInfo ( v0,v1,v3,p2 );
/* .line 188 */
/* .local v0, "info":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* iget-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z */
/* if-nez v1, :cond_1 */
final String v1 = "custom_suppression"; // const-string v1, "custom_suppression"
v3 = this.mEdgeModeType;
v1 = (( java.lang.String ) v1 ).equals ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 189 */
} // :cond_1
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mAbsoluteSize:I */
(( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) v0 ).setAbsoluteSize ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->setAbsoluteSize(I)V
/* .line 190 */
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConditionSize:I */
(( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) v0 ).setConditionSize ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->setConditionSize(I)V
/* .line 194 */
} // :cond_2
/* sget-boolean v1, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* iget-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mFolded:Z */
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 196 */
if ( p2 != null) { // if-eqz p2, :cond_4
/* if-ne p2, v2, :cond_3 */
} // :cond_3
int v1 = 1; // const/4 v1, 0x1
} // :cond_4
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
/* .line 197 */
/* .local v1, "screenState":I */
} // :goto_2
v2 = this.mSubScreenEdgeSuppressionList;
/* aget-object v2, v2, v1 */
/* if-nez v2, :cond_5 */
/* move-object v2, v0 */
/* .line 198 */
} // :cond_5
/* nop */
} // :goto_3
/* move-object v0, v2 */
/* .line 200 */
} // .end local v1 # "screenState":I
} // :cond_6
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mScreenWidth = "; // const-string v2, "mScreenWidth = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " mScreenHeight = "; // const-string v2, " mScreenHeight = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "EdgeSuppressionManager"; // const-string v2, "EdgeSuppressionManager"
android.util.Slog .i ( v2,v1 );
/* .line 201 */
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static com.miui.server.input.edgesuppression.EdgeSuppressionManager getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 140 */
v0 = com.miui.server.input.edgesuppression.EdgeSuppressionManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 141 */
/* const-class v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
/* monitor-enter v0 */
/* .line 142 */
try { // :try_start_0
v1 = com.miui.server.input.edgesuppression.EdgeSuppressionManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 143 */
/* new-instance v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;-><init>(Landroid/content/Context;)V */
/* .line 145 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 147 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.edgesuppression.EdgeSuppressionManager.sInstance;
} // .end method
private void handleEdgeSuppressionData ( ) {
/* .locals 10 */
/* .line 152 */
/* sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 153 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getDisplay ( ); // invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
v0 = (( android.view.Display ) v0 ).getRotation ( ); // invoke-virtual {v0}, Landroid/view/Display;->getRotation()I
/* .line 154 */
/* .local v0, "rotation":I */
int v1 = 0; // const/4 v1, 0x0
/* .line 155 */
/* .local v1, "targetId":I */
/* iget v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F */
/* invoke-direct {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getEdgeSuppressionInfo(FI)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .line 156 */
/* .local v8, "info":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* sget-boolean v2, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* iget-boolean v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mFolded:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 157 */
int v1 = 1; // const/4 v1, 0x1
/* move v9, v1 */
/* .line 159 */
} // :cond_0
/* move v9, v1 */
} // .end local v1 # "targetId":I
/* .local v9, "targetId":I */
} // :goto_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "handleEdgeSuppressionData:targetId = "; // const-string v2, "handleEdgeSuppressionData:targetId = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " "; // const-string v2, " "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) v8 ).toString ( ); // invoke-virtual {v8}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "EdgeSuppressionManager"; // const-string v2, "EdgeSuppressionManager"
android.util.Slog .i ( v2,v1 );
/* .line 160 */
v1 = this.mBaseEdgeSuppression;
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I */
/* iget v6, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I */
/* iget v7, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I */
/* move-object v2, v8 */
/* move v4, v0 */
/* move v5, v9 */
/* invoke-virtual/range {v1 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->updateInterNalParam(Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;IIIII)V */
/* .line 162 */
v1 = this.mBaseEdgeSuppression;
(( com.miui.server.input.edgesuppression.BaseEdgeSuppression ) v1 ).syncDataToKernel ( ); // invoke-virtual {v1}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->syncDataToKernel()V
/* .line 164 */
} // .end local v0 # "rotation":I
} // .end local v8 # "info":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
} // .end local v9 # "targetId":I
} // :cond_1
return;
} // .end method
private void initParamForOldSettings ( ) {
/* .locals 2 */
/* .line 247 */
final String v0 = "edge_suppresson_condition"; // const-string v0, "edge_suppresson_condition"
miui.util.FeatureParser .getIntArray ( v0 );
/* .line 248 */
/* .local v0, "intArray":[I */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 249 */
int v1 = 0; // const/4 v1, 0x0
/* aget v1, v0, v1 */
/* iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mMaxAdjustValue:I */
/* .line 251 */
} // :cond_0
return;
} // .end method
private void lambda$new$0 ( Integer p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "status" # I */
/* .line 95 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I */
/* if-eq p1, v0, :cond_0 */
/* .line 96 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Hold status is "; // const-string v1, "Hold status is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "EdgeSuppressionManager"; // const-string v1, "EdgeSuppressionManager"
android.util.Slog .i ( v1,v0 );
/* .line 97 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I */
/* .line 98 */
int v0 = 1; // const/4 v0, 0x1
final String v1 = "holdSensor"; // const-string v1, "holdSensor"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V */
/* .line 101 */
} // :cond_0
return;
} // .end method
private void lambda$new$1 ( Integer p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "status" # I */
/* .line 105 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
/* if-eq p1, v0, :cond_0 */
/* .line 106 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Lay status is "; // const-string v1, "Lay status is "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "EdgeSuppressionManager"; // const-string v1, "EdgeSuppressionManager"
android.util.Slog .i ( v1,v0 );
/* .line 107 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
/* .line 108 */
int v0 = 1; // const/4 v0, 0x1
final String v1 = "laySensor"; // const-string v1, "laySensor"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V */
/* .line 111 */
} // :cond_0
return;
} // .end method
private void lambda$new$2 ( ) { //synthethic
/* .locals 0 */
/* .line 134 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->trackEdgeSuppressionAndUpdateTrackTime()V */
/* .line 135 */
return;
} // .end method
private void notifyEdgeSuppressionChanged ( Integer p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "action" # I */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 235 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).obtainMessage ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 236 */
/* .local v0, "msg":Landroid/os/Message; */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 237 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "reason"; // const-string v2, "reason"
(( android.os.Bundle ) v1 ).putString ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 238 */
(( android.os.Message ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 239 */
v2 = this.mHandler;
v2 = (( android.os.Handler ) v2 ).hasMessages ( p1 ); // invoke-virtual {v2, p1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 240 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).removeMessages ( p1 ); // invoke-virtual {v2, p1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 242 */
} // :cond_0
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendMessage ( v0 ); // invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 244 */
} // :goto_0
return;
} // .end method
private void resetUserSetted ( ) {
/* .locals 1 */
/* .line 332 */
final String v0 = "none"; // const-string v0, "none"
this.mLastEdgeModeType = v0;
/* .line 333 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeSize:I */
/* .line 334 */
return;
} // .end method
private void trackEdgeSuppressionAndUpdateTrackTime ( ) {
/* .locals 8 */
/* .line 320 */
/* new-instance v0, Ljava/util/Date; */
/* invoke-direct {v0}, Ljava/util/Date;-><init>()V */
/* .line 321 */
/* .local v0, "dateNow":Ljava/util/Date; */
v1 = this.mTrackEventTime;
v1 = /* invoke-direct {p0, v1, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->compareTimeOfNow(Ljava/lang/String;Ljava/util/Date;)I */
int v2 = 1; // const/4 v2, 0x1
/* if-lt v1, v2, :cond_0 */
/* .line 322 */
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
/* iget-boolean v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mIsUserSetted:Z */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeSize:I */
v5 = this.mLastEdgeModeType;
/* iget v6, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConditionSize:I */
v7 = this.mEdgeModeType;
/* invoke-virtual/range {v2 ..v7}, Lcom/android/server/input/InputOneTrackUtil;->trackEdgeSuppressionEvent(ZILjava/lang/String;ILjava/lang/String;)V */
/* .line 325 */
v1 = this.mSimpleDateFormat;
(( java.text.SimpleDateFormat ) v1 ).format ( v0 ); // invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 326 */
/* .local v1, "updateTime":Ljava/lang/String; */
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 327 */
v3 = android.os.UserHandle .myUserId ( );
/* .line 326 */
final String v4 = "edge_suppression_track_time"; // const-string v4, "edge_suppression_track_time"
android.provider.Settings$System .putStringForUser ( v2,v4,v1,v3 );
/* .line 329 */
} // .end local v1 # "updateTime":Ljava/lang/String;
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void finishedGoingToSleep ( ) {
/* .locals 2 */
/* .line 285 */
/* iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "default_suppression"; // const-string v0, "default_suppression"
v1 = this.mEdgeModeType;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 286 */
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) p0 ).unRegisterSensors ( ); // invoke-virtual {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->unRegisterSensors()V
/* .line 288 */
} // :cond_0
return;
} // .end method
public void finishedWakingUp ( ) {
/* .locals 2 */
/* .line 276 */
/* sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 277 */
/* iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
final String v0 = "default_suppression"; // const-string v0, "default_suppression"
v1 = this.mEdgeModeType;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 278 */
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) p0 ).registerSensors ( ); // invoke-virtual {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->registerSensors()V
/* .line 280 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
final String v1 = "screenOn"; // const-string v1, "screenOn"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V */
/* .line 282 */
} // :cond_1
return;
} // .end method
public getAbsoluteLevel ( ) {
/* .locals 1 */
/* .line 295 */
v0 = this.mConfigLoader;
v0 = this.mAbsoluteLevel;
} // .end method
public getConditionLevel ( ) {
/* .locals 1 */
/* .line 291 */
v0 = this.mConfigLoader;
v0 = this.mConditionLevel;
} // .end method
public getInputMethodSizeScope ( ) {
/* .locals 2 */
/* .line 299 */
v0 = this.mConfigLoader;
/* iget v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I */
v1 = this.mConfigLoader;
/* iget v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I */
/* filled-new-array {v0, v1}, [I */
} // .end method
public Integer getScreenWidth ( ) {
/* .locals 1 */
/* .line 303 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I */
} // .end method
public void handleEdgeModeChange ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .line 224 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V */
/* .line 225 */
return;
} // .end method
public void handleEdgeModeChange ( java.lang.String p0, Boolean p1, com.android.server.wm.DisplayFrames p2 ) {
/* .locals 3 */
/* .param p1, "reason" # Ljava/lang/String; */
/* .param p2, "folded" # Z */
/* .param p3, "displayFrames" # Lcom/android/server/wm/DisplayFrames; */
/* .line 228 */
/* iput-boolean p2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mFolded:Z */
/* .line 229 */
/* iget v0, p3, Lcom/android/server/wm/DisplayFrames;->mWidth:I */
int v1 = 1; // const/4 v1, 0x1
/* sub-int/2addr v0, v1 */
/* iget v2, p3, Lcom/android/server/wm/DisplayFrames;->mHeight:I */
/* sub-int/2addr v2, v1 */
v0 = java.lang.Math .min ( v0,v2 );
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I */
/* .line 230 */
/* iget v0, p3, Lcom/android/server/wm/DisplayFrames;->mWidth:I */
/* sub-int/2addr v0, v1 */
/* iget v2, p3, Lcom/android/server/wm/DisplayFrames;->mHeight:I */
/* sub-int/2addr v2, v1 */
v0 = java.lang.Math .max ( v0,v2 );
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I */
/* .line 231 */
/* invoke-direct {p0, v1, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V */
/* .line 232 */
return;
} // .end method
public void registerSensors ( ) {
/* .locals 2 */
/* .line 254 */
v0 = this.mHoldSensorWrapper;
/* if-nez v0, :cond_0 */
/* .line 255 */
/* new-instance v0, Lmiui/util/HoldSensorWrapper; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lmiui/util/HoldSensorWrapper;-><init>(Landroid/content/Context;)V */
this.mHoldSensorWrapper = v0;
/* .line 257 */
} // :cond_0
v0 = this.mLaySensorWrapper;
/* if-nez v0, :cond_1 */
/* .line 258 */
/* new-instance v0, Lmiui/util/LaySensorWrapper; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lmiui/util/LaySensorWrapper;-><init>(Landroid/content/Context;)V */
this.mLaySensorWrapper = v0;
/* .line 260 */
} // :cond_1
v0 = this.mHoldSensorWrapper;
v1 = this.mHoldListener;
(( miui.util.HoldSensorWrapper ) v0 ).registerListener ( v1 ); // invoke-virtual {v0, v1}, Lmiui/util/HoldSensorWrapper;->registerListener(Lmiui/util/HoldSensorWrapper$HoldSensorChangeListener;)V
/* .line 261 */
v0 = this.mLaySensorWrapper;
v1 = this.mLayListener;
(( miui.util.LaySensorWrapper ) v0 ).registerListener ( v1 ); // invoke-virtual {v0, v1}, Lmiui/util/LaySensorWrapper;->registerListener(Lmiui/util/LaySensorWrapper$LaySensorChangeListener;)V
/* .line 262 */
return;
} // .end method
public void unRegisterSensors ( ) {
/* .locals 2 */
/* .line 265 */
v0 = this.mHoldSensorWrapper;
int v1 = -1; // const/4 v1, -0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 266 */
/* iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I */
/* .line 267 */
v0 = this.mHoldSensorWrapper;
(( miui.util.HoldSensorWrapper ) v0 ).unregisterAllListener ( ); // invoke-virtual {v0}, Lmiui/util/HoldSensorWrapper;->unregisterAllListener()V
/* .line 269 */
} // :cond_0
v0 = this.mLaySensorWrapper;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 270 */
/* iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I */
/* .line 271 */
v0 = this.mLaySensorWrapper;
(( miui.util.LaySensorWrapper ) v0 ).unregisterAllListener ( ); // invoke-virtual {v0}, Lmiui/util/LaySensorWrapper;->unregisterAllListener()V
/* .line 273 */
} // :cond_1
return;
} // .end method
