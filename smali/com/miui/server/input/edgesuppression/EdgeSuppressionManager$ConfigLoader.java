class com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader {
	 /* .source "EdgeSuppressionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ConfigLoader" */
} // .end annotation
/* # static fields */
private static final java.lang.String ATTRIBUTE_ABSOLUTE_SIZE;
private static final java.lang.String ATTRIBUTE_CONDITION_SIZE;
private static final java.lang.String ATTRIBUTE_CONNER_HEIGHT;
private static final java.lang.String ATTRIBUTE_CONNER_WIDTH;
private static final java.lang.String ATTRIBUTE_DISPLAY_ID;
private static final java.lang.String ATTRIBUTE_IS_HORIZONTAL;
private static final java.lang.String ATTRIBUTE_MAX_ABSOLUTE_SIZE;
private static final java.lang.String ATTRIBUTE_MAX_CONDITION_SIZE;
private static final java.lang.String ATTRIBUTE_MAX_INPUTMETHOD_SIZE;
private static final java.lang.String ATTRIBUTE_MIN_ABSOLUTE_SIZE;
private static final java.lang.String ATTRIBUTE_MIN_CONDITION_SIZE;
private static final java.lang.String ATTRIBUTE_MIN_INPUTMETHOD_SIZE;
private static final java.lang.String ATTRIBUTE_TYPE;
private static final java.lang.String CONFIG_FILE;
private static final java.lang.String TAG_CONFIG_ITEM;
private static final java.lang.String TAG_FILE;
/* # instance fields */
 mAbsoluteLevel;
 mConditionLevel;
Integer mMaxInputMethodSize;
Integer mMinInputMethodSize;
final com.miui.server.input.edgesuppression.EdgeSuppressionManager this$0; //synthetic
/* # direct methods */
static com.miui.server.input.edgesuppression.EdgeSuppressionInfo -$$Nest$mgetInfo ( com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader p0, com.miui.server.input.edgesuppression.EdgeSuppressionInfo[] p1, java.lang.String p2, Integer p3 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->getInfo([Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;Ljava/lang/String;I)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
} // .end method
static void -$$Nest$minitEdgeSuppressionConfig ( com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->initEdgeSuppressionConfig()V */
return;
} // .end method
 com.miui.server.input.edgesuppression.EdgeSuppressionManager$ConfigLoader ( ) {
/* .locals 2 */
/* .param p1, "this$0" # Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
/* .line 474 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 491 */
int v0 = 0; // const/4 v0, 0x0
/* filled-new-array {v0, v0, v0, v0, v0}, [I */
this.mConditionLevel = v1;
/* .line 492 */
/* filled-new-array {v0, v0, v0, v0, v0}, [I */
this.mAbsoluteLevel = v0;
/* .line 493 */
/* const/16 v0, 0xa */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I */
/* .line 494 */
/* const/16 v0, 0x2d */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I */
return;
} // .end method
private com.miui.server.input.edgesuppression.EdgeSuppressionInfo getInfo ( com.miui.server.input.edgesuppression.EdgeSuppressionInfo[] p0, java.lang.String p1, Integer p2 ) {
/* .locals 9 */
/* .param p1, "list" # [Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .param p2, "type" # Ljava/lang/String; */
/* .param p3, "rotation" # I */
/* .line 604 */
if ( p2 != null) { // if-eqz p2, :cond_3
	 /* array-length v0, p1 */
	 /* if-lez v0, :cond_3 */
	 /* .line 605 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 606 */
	 /* .local v0, "isHorizontal":Z */
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-eq p3, v1, :cond_0 */
	 int v1 = 3; // const/4 v1, 0x3
	 /* if-ne p3, v1, :cond_1 */
	 /* .line 607 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 609 */
} // :cond_1
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_3 */
/* aget-object v3, p1, v2 */
/* .line 610 */
/* .local v3, "target":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
(( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) v3 ).getType ( ); // invoke-virtual {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getType()Ljava/lang/String;
v4 = (( java.lang.String ) p2 ).equals ( v4 ); // invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_2
v4 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) v3 ).isHorizontal ( ); // invoke-virtual {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->isHorizontal()Z
/* if-ne v0, v4, :cond_2 */
/* .line 611 */
/* .line 609 */
} // .end local v3 # "target":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 615 */
} // .end local v0 # "isHorizontal":Z
} // :cond_3
final String v0 = "EdgeSuppressionManager"; // const-string v0, "EdgeSuppressionManager"
final String v1 = "There is not available info"; // const-string v1, "There is not available info"
android.util.Slog .i ( v0,v1 );
/* .line 616 */
/* new-instance v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
final String v8 = "default_suppression"; // const-string v8, "default_suppression"
/* move-object v2, v0 */
/* invoke-direct/range {v2 ..v8}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V */
} // .end method
private void initEdgeSuppressionConfig ( ) {
/* .locals 32 */
/* .line 497 */
/* move-object/from16 v1, p0 */
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const-string/jumbo v2, "xml" */
final String v3 = "android.miui"; // const-string v3, "android.miui"
final String v4 = "edge_suppression_config"; // const-string v4, "edge_suppression_config"
v2 = (( android.content.res.Resources ) v0 ).getIdentifier ( v4, v2, v3 ); // invoke-virtual {v0, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 499 */
/* .local v2, "resId":I */
/* if-nez v2, :cond_0 */
return;
/* .line 500 */
} // :cond_0
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getXml ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
/* .line 501 */
/* .local v3, "xmlResourceParser":Landroid/content/res/XmlResourceParser; */
/* if-nez v3, :cond_1 */
return;
/* .line 502 */
} // :cond_1
/* const-string/jumbo v0, "xmlResourceParser is not null, file = edge_suppression_config" */
final String v4 = "EdgeSuppressionManager"; // const-string v4, "EdgeSuppressionManager"
android.util.Slog .i ( v4,v0 );
/* .line 504 */
try { // :try_start_0
final String v0 = "edgesuppressions"; // const-string v0, "edgesuppressions"
com.android.internal.util.XmlUtils .beginDocument ( v3,v0 );
/* .line 508 */
int v0 = 0; // const/4 v0, 0x0
/* .line 509 */
/* .local v0, "index":I */
com.android.internal.util.XmlUtils .nextElement ( v3 );
/* .line 510 */
v5 = } // :goto_0
int v6 = 3; // const/4 v6, 0x3
/* if-eq v5, v6, :cond_d */
final String v5 = "edgesuppressionitem"; // const-string v5, "edgesuppressionitem"
/* .line 511 */
v5 = (( java.lang.String ) v5 ).equals ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_c
/* .line 512 */
final String v5 = "absoluteSize"; // const-string v5, "absoluteSize"
int v7 = 0; // const/4 v7, 0x0
/* .line 514 */
/* .local v5, "absoluteSize":Ljava/lang/String; */
final String v8 = "conditionSize"; // const-string v8, "conditionSize"
/* .line 516 */
/* .local v8, "conditionSize":Ljava/lang/String; */
final String v9 = "connerWidth"; // const-string v9, "connerWidth"
/* .line 518 */
/* .local v9, "connerWidth":Ljava/lang/String; */
final String v10 = "connerHeight"; // const-string v10, "connerHeight"
/* .line 520 */
/* .local v10, "connerHeight":Ljava/lang/String; */
final String v11 = "isHorizontal"; // const-string v11, "isHorizontal"
/* .line 522 */
/* .local v11, "isHorizontal":Ljava/lang/String; */
/* const-string/jumbo v12, "type" */
/* .line 523 */
/* .local v12, "type":Ljava/lang/String; */
final String v13 = "maxAbsoluteSize"; // const-string v13, "maxAbsoluteSize"
/* move-object/from16 v20, v13 */
/* .line 525 */
/* .local v20, "maxAbsoluteSize":Ljava/lang/String; */
final String v13 = "minAbsoluteSize"; // const-string v13, "minAbsoluteSize"
/* move-object/from16 v21, v13 */
/* .line 527 */
/* .local v21, "minAbsoluteSize":Ljava/lang/String; */
final String v13 = "maxConditionSize"; // const-string v13, "maxConditionSize"
/* move-object/from16 v22, v13 */
/* .line 529 */
/* .local v22, "maxConditionSize":Ljava/lang/String; */
final String v13 = "minConditionSize"; // const-string v13, "minConditionSize"
/* move-object/from16 v23, v13 */
/* .line 531 */
/* .local v23, "minConditionSize":Ljava/lang/String; */
final String v13 = "minInputMethodSize"; // const-string v13, "minInputMethodSize"
/* move-object/from16 v24, v13 */
/* .line 533 */
/* .local v24, "minInputMethodSize":Ljava/lang/String; */
final String v13 = "maxInputMethodSize"; // const-string v13, "maxInputMethodSize"
/* move-object/from16 v25, v13 */
/* .line 535 */
/* .local v25, "maxInputMethodSize":Ljava/lang/String; */
final String v13 = "displayId"; // const-string v13, "displayId"
/* .line 536 */
/* .local v7, "displayId":Ljava/lang/String; */
final String v13 = "inputmethod_size"; // const-string v13, "inputmethod_size"
v13 = (( java.lang.String ) v13 ).equals ( v12 ); // invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_2 */
if ( v13 != null) { // if-eqz v13, :cond_2
/* .line 537 */
try { // :try_start_1
v6 = /* invoke-static/range {v25 ..v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* iput v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I */
/* .line 538 */
v6 = /* invoke-static/range {v24 ..v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* iput v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I */
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 539 */
/* move/from16 v31, v2 */
/* goto/16 :goto_b */
/* .line 598 */
} // .end local v0 # "index":I
} // .end local v5 # "absoluteSize":Ljava/lang/String;
} // .end local v7 # "displayId":Ljava/lang/String;
} // .end local v8 # "conditionSize":Ljava/lang/String;
} // .end local v9 # "connerWidth":Ljava/lang/String;
} // .end local v10 # "connerHeight":Ljava/lang/String;
} // .end local v11 # "isHorizontal":Ljava/lang/String;
} // .end local v12 # "type":Ljava/lang/String;
} // .end local v20 # "maxAbsoluteSize":Ljava/lang/String;
} // .end local v21 # "minAbsoluteSize":Ljava/lang/String;
} // .end local v22 # "maxConditionSize":Ljava/lang/String;
} // .end local v23 # "minConditionSize":Ljava/lang/String;
} // .end local v24 # "minInputMethodSize":Ljava/lang/String;
} // .end local v25 # "maxInputMethodSize":Ljava/lang/String;
/* :catch_0 */
/* move-exception v0 */
/* move/from16 v31, v2 */
/* goto/16 :goto_c */
/* .line 542 */
/* .restart local v0 # "index":I */
/* .restart local v5 # "absoluteSize":Ljava/lang/String; */
/* .restart local v7 # "displayId":Ljava/lang/String; */
/* .restart local v8 # "conditionSize":Ljava/lang/String; */
/* .restart local v9 # "connerWidth":Ljava/lang/String; */
/* .restart local v10 # "connerHeight":Ljava/lang/String; */
/* .restart local v11 # "isHorizontal":Ljava/lang/String; */
/* .restart local v12 # "type":Ljava/lang/String; */
/* .restart local v20 # "maxAbsoluteSize":Ljava/lang/String; */
/* .restart local v21 # "minAbsoluteSize":Ljava/lang/String; */
/* .restart local v22 # "maxConditionSize":Ljava/lang/String; */
/* .restart local v23 # "minConditionSize":Ljava/lang/String; */
/* .restart local v24 # "minInputMethodSize":Ljava/lang/String; */
/* .restart local v25 # "maxInputMethodSize":Ljava/lang/String; */
} // :cond_2
try { // :try_start_2
/* sget-boolean v13, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
/* const/16 v26, 0x2 */
/* const/16 v27, 0x0 */
int v15 = 1; // const/4 v15, 0x1
/* if-nez v13, :cond_7 */
/* .line 543 */
v13 = (( java.lang.String ) v12 ).hashCode ( ); // invoke-virtual {v12}, Ljava/lang/String;->hashCode()I
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_2 */
final String v14 = "custom_suppression"; // const-string v14, "custom_suppression"
/* sparse-switch v13, :sswitch_data_0 */
} // :cond_3
/* :sswitch_0 */
try { // :try_start_3
v13 = (( java.lang.String ) v12 ).equals ( v14 ); // invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* move v13, v6 */
/* :sswitch_1 */
/* const-string/jumbo v13, "wake_suppression" */
v13 = (( java.lang.String ) v12 ).equals ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* move v13, v15 */
/* :sswitch_2 */
/* const-string/jumbo v13, "strong_suppression" */
v13 = (( java.lang.String ) v12 ).equals ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* move/from16 v13, v26 */
/* :sswitch_3 */
final String v13 = "default_suppression"; // const-string v13, "default_suppression"
v13 = (( java.lang.String ) v12 ).equals ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_3
/* move/from16 v13, v27 */
} // :goto_1
int v13 = -1; // const/4 v13, -0x1
} // :goto_2
/* packed-switch v13, :pswitch_data_0 */
/* .line 558 */
/* :pswitch_0 */
v6 = this.mAbsoluteLevel;
v13 = /* invoke-static/range {v21 ..v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* aput v13, v6, v27 */
/* .line 559 */
v6 = this.mAbsoluteLevel;
v13 = /* invoke-static/range {v20 ..v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* const/16 v16, 0x4 */
/* aput v13, v6, v16 */
/* .line 560 */
v6 = this.mConditionLevel;
v13 = /* invoke-static/range {v23 ..v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* aput v13, v6, v27 */
/* .line 561 */
v6 = this.mConditionLevel;
v13 = /* invoke-static/range {v22 ..v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* aput v13, v6, v16 */
/* .line 562 */
/* .line 554 */
/* :pswitch_1 */
v13 = this.mAbsoluteLevel;
v16 = java.lang.Integer .parseInt ( v5 );
/* aput v16, v13, v6 */
/* .line 555 */
v13 = this.mConditionLevel;
v16 = java.lang.Integer .parseInt ( v8 );
/* aput v16, v13, v6 */
/* .line 556 */
/* .line 550 */
/* :pswitch_2 */
v6 = this.mAbsoluteLevel;
v13 = java.lang.Integer .parseInt ( v5 );
/* aput v13, v6, v15 */
/* .line 551 */
v6 = this.mConditionLevel;
v13 = java.lang.Integer .parseInt ( v8 );
/* aput v13, v6, v15 */
/* .line 552 */
/* .line 545 */
/* :pswitch_3 */
v6 = this.mAbsoluteLevel;
v13 = java.lang.Integer .parseInt ( v5 );
/* aput v13, v6, v26 */
/* .line 546 */
v6 = this.mConditionLevel;
v13 = java.lang.Integer .parseInt ( v8 );
/* aput v13, v6, v26 */
/* .line 547 */
v6 = this.this$0;
v13 = this.mConditionLevel;
/* aget v13, v13, v26 */
/* int-to-float v13, v13 */
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmEdgeModeSize ( v6,v13 );
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 548 */
/* nop */
/* .line 566 */
} // :goto_3
try { // :try_start_4
v6 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmEdgeSuppressionInfoList ( v6 );
/* add-int/lit8 v28, v0, 0x1 */
} // .end local v0 # "index":I
/* .local v28, "index":I */
/* new-instance v29, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .line 567 */
v13 = (( java.lang.String ) v14 ).equals ( v12 ); // invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_4
/* .line 568 */
/* move/from16 v16, v27 */
} // :cond_4
v13 = java.lang.Integer .parseInt ( v5 );
/* move/from16 v16, v13 */
/* .line 569 */
} // :goto_4
v13 = (( java.lang.String ) v14 ).equals ( v12 ); // invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v13 != null) { // if-eqz v13, :cond_5
/* move/from16 v17, v27 */
/* .line 570 */
} // :cond_5
v13 = java.lang.Integer .parseInt ( v8 );
/* move/from16 v17, v13 */
/* .line 571 */
} // :goto_5
v18 = java.lang.Integer .parseInt ( v9 );
/* .line 572 */
v19 = java.lang.Integer .parseInt ( v10 );
/* .line 573 */
v13 = java.lang.Integer .parseInt ( v11 );
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* if-ne v13, v15, :cond_6 */
/* move/from16 v30, v15 */
} // :cond_6
/* move/from16 v30, v27 */
} // :goto_6
/* move-object/from16 v13, v29 */
/* move/from16 v14, v16 */
/* move/from16 v31, v2 */
/* move v2, v15 */
} // .end local v2 # "resId":I
/* .local v31, "resId":I */
/* move/from16 v15, v17 */
/* move/from16 v16, v18 */
/* move/from16 v17, v19 */
/* move/from16 v18, v30 */
/* move-object/from16 v19, v12 */
try { // :try_start_5
/* invoke-direct/range {v13 ..v19}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V */
/* aput-object v29, v6, v0 */
/* move/from16 v0, v28 */
/* .line 542 */
} // .end local v28 # "index":I
} // .end local v31 # "resId":I
/* .restart local v0 # "index":I */
/* .restart local v2 # "resId":I */
} // :cond_7
/* move/from16 v31, v2 */
/* move v2, v15 */
/* .line 576 */
} // .end local v2 # "resId":I
/* .restart local v31 # "resId":I */
} // :goto_7
/* sget-boolean v6, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v6 != null) { // if-eqz v6, :cond_b
/* .line 577 */
v6 = android.text.TextUtils .isEmpty ( v7 );
if ( v6 != null) { // if-eqz v6, :cond_9
/* .line 578 */
v6 = this.mAbsoluteLevel;
v13 = java.lang.Integer .parseInt ( v5 );
/* aput v13, v6, v26 */
/* .line 579 */
v6 = this.mConditionLevel;
v13 = java.lang.Integer .parseInt ( v8 );
/* aput v13, v6, v26 */
/* .line 580 */
v6 = this.this$0;
v13 = this.mConditionLevel;
/* aget v13, v13, v26 */
/* int-to-float v13, v13 */
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmEdgeModeSize ( v6,v13 );
/* .line 581 */
v6 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmEdgeSuppressionInfoList ( v6 );
/* add-int/lit8 v26, v0, 0x1 */
} // .end local v0 # "index":I
/* .local v26, "index":I */
/* new-instance v28, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .line 582 */
v14 = java.lang.Integer .parseInt ( v5 );
/* .line 583 */
v15 = java.lang.Integer .parseInt ( v8 );
/* .line 584 */
v16 = java.lang.Integer .parseInt ( v9 );
/* .line 585 */
v17 = java.lang.Integer .parseInt ( v10 );
/* .line 586 */
v13 = java.lang.Integer .parseInt ( v11 );
/* if-ne v13, v2, :cond_8 */
/* move/from16 v18, v2 */
} // :cond_8
/* move/from16 v18, v27 */
} // :goto_8
/* move-object/from16 v13, v28 */
/* move-object/from16 v19, v12 */
/* invoke-direct/range {v13 ..v19}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V */
/* aput-object v28, v6, v0 */
/* move/from16 v0, v26 */
/* .line 588 */
} // .end local v26 # "index":I
/* .restart local v0 # "index":I */
} // :cond_9
v6 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmSubScreenEdgeSuppressionList ( v6 );
v26 = java.lang.Integer .parseInt ( v11 );
/* new-instance v28, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .line 589 */
v14 = java.lang.Integer .parseInt ( v5 );
/* .line 590 */
v15 = java.lang.Integer .parseInt ( v8 );
/* .line 591 */
v16 = java.lang.Integer .parseInt ( v9 );
/* .line 592 */
v17 = java.lang.Integer .parseInt ( v10 );
/* .line 593 */
v13 = java.lang.Integer .parseInt ( v11 );
/* if-ne v13, v2, :cond_a */
/* move/from16 v18, v2 */
} // :cond_a
/* move/from16 v18, v27 */
} // :goto_9
/* move-object/from16 v13, v28 */
/* move-object/from16 v19, v12 */
/* invoke-direct/range {v13 ..v19}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V */
/* aput-object v28, v6, v26 */
/* .line 596 */
} // :cond_b
} // :goto_a
com.android.internal.util.XmlUtils .nextElement ( v3 );
/* :try_end_5 */
/* .catch Ljava/lang/Exception; {:try_start_5 ..:try_end_5} :catch_1 */
/* move/from16 v2, v31 */
/* goto/16 :goto_0 */
/* .line 598 */
} // .end local v0 # "index":I
} // .end local v5 # "absoluteSize":Ljava/lang/String;
} // .end local v7 # "displayId":Ljava/lang/String;
} // .end local v8 # "conditionSize":Ljava/lang/String;
} // .end local v9 # "connerWidth":Ljava/lang/String;
} // .end local v10 # "connerHeight":Ljava/lang/String;
} // .end local v11 # "isHorizontal":Ljava/lang/String;
} // .end local v12 # "type":Ljava/lang/String;
} // .end local v20 # "maxAbsoluteSize":Ljava/lang/String;
} // .end local v21 # "minAbsoluteSize":Ljava/lang/String;
} // .end local v22 # "maxConditionSize":Ljava/lang/String;
} // .end local v23 # "minConditionSize":Ljava/lang/String;
} // .end local v24 # "minInputMethodSize":Ljava/lang/String;
} // .end local v25 # "maxInputMethodSize":Ljava/lang/String;
/* :catch_1 */
/* move-exception v0 */
/* .line 511 */
} // .end local v31 # "resId":I
/* .restart local v0 # "index":I */
/* .restart local v2 # "resId":I */
} // :cond_c
/* move/from16 v31, v2 */
} // .end local v2 # "resId":I
/* .restart local v31 # "resId":I */
/* .line 510 */
} // .end local v31 # "resId":I
/* .restart local v2 # "resId":I */
} // :cond_d
/* move/from16 v31, v2 */
/* .line 600 */
} // .end local v0 # "index":I
} // .end local v2 # "resId":I
/* .restart local v31 # "resId":I */
} // :goto_b
/* .line 598 */
} // .end local v31 # "resId":I
/* .restart local v2 # "resId":I */
/* :catch_2 */
/* move-exception v0 */
/* move/from16 v31, v2 */
/* .line 599 */
} // .end local v2 # "resId":I
/* .local v0, "e":Ljava/lang/Exception; */
/* .restart local v31 # "resId":I */
} // :goto_c
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "init EdgeSuppression Config fail!"; // const-string v5, "init EdgeSuppression Config fail!"
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v2 );
/* .line 601 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_d
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x1ca0abcb -> :sswitch_3 */
/* 0x3cb5238b -> :sswitch_2 */
/* 0x5d6e1318 -> :sswitch_1 */
/* 0x600bc165 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
