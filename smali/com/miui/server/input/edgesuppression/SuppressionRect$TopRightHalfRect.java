public class com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "TopRightHalfRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ( ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "widthOfScreen" # I */
/* .param p3, "widthOfRect" # I */
/* .line 201 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 202 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;->setType(I)V
/* .line 203 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;->setPosition(I)V
/* .line 204 */
/* div-int/lit8 v1, p2, 0x2 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ) p0 ).setTopLeftX ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;->setTopLeftX(I)V
/* .line 205 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;->setTopLeftY(I)V
/* .line 206 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ) p0 ).setBottomRightX ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;->setBottomRightX(I)V
/* .line 207 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRightHalfRect ) p0 ).setBottomRightY ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;->setBottomRightY(I)V
/* .line 208 */
return;
} // .end method
