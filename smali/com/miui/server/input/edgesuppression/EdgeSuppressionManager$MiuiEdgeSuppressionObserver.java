class com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver extends android.database.ContentObserver {
	 /* .source "EdgeSuppressionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiEdgeSuppressionObserver" */
} // .end annotation
/* # instance fields */
android.content.ContentResolver contentResolver;
Boolean isTypeChanged;
final com.miui.server.input.edgesuppression.EdgeSuppressionManager this$0; //synthetic
/* # direct methods */
public com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 344 */
this.this$0 = p1;
/* .line 345 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 337 */
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmContext ( p1 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.contentResolver = v0;
/* .line 338 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z */
/* .line 346 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 8 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 375 */
final String v0 = "edge_size"; // const-string v0, "edge_size"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "custom_suppression"; // const-string v2, "custom_suppression"
int v3 = -2; // const/4 v3, -0x2
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 376 */
	 v1 = this.this$0;
	 v6 = 	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmEdgeModeSize ( v1 );
	 /* float-to-int v6, v6 */
	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmLastEdgeModeSize ( v1,v6 );
	 /* .line 377 */
	 v1 = this.this$0;
	 v6 = this.contentResolver;
	 v7 = 	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmEdgeModeSize ( v1 );
	 v0 = 	 android.provider.Settings$System .getFloatForUser ( v6,v0,v7,v3 );
	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmEdgeModeSize ( v1,v0 );
	 /* .line 380 */
	 v0 = this.this$0;
	 v0 = 	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmEdgeModeSize ( v0 );
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* cmpg-float v0, v0, v1 */
	 /* if-gtz v0, :cond_0 */
	 /* .line 381 */
	 v0 = this.this$0;
	 v1 = 	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmEdgeModeSize ( v0 );
	 v3 = this.this$0;
	 v3 = 	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmMaxAdjustValue ( v3 );
	 /* int-to-float v3, v3 */
	 /* mul-float/2addr v1, v3 */
	 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmEdgeModeSize ( v0,v1 );
	 /* .line 384 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z */
/* if-nez v0, :cond_1 */
/* .line 385 */
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmLastEdgeModeType ( v0,v2 );
/* .line 387 */
} // :cond_1
/* iput-boolean v4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z */
/* .line 389 */
} // :goto_0
v0 = this.this$0;
/* const-string/jumbo v1, "settings" */
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mnotifyEdgeSuppressionChanged ( v0,v5,v1 );
/* goto/16 :goto_2 */
/* .line 391 */
} // :cond_2
final String v0 = "edge_type"; // const-string v0, "edge_type"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 392 */
/* iput-boolean v5, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z */
/* .line 393 */
v1 = this.this$0;
v5 = this.mEdgeModeType;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmLastEdgeModeType ( v1,v5 );
/* .line 394 */
v1 = this.this$0;
v5 = this.contentResolver;
android.provider.Settings$System .getStringForUser ( v5,v0,v3 );
this.mEdgeModeType = v0;
/* .line 396 */
v0 = this.this$0;
v0 = this.mEdgeModeType;
final String v1 = "default_suppression"; // const-string v1, "default_suppression"
/* if-nez v0, :cond_3 */
/* .line 397 */
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmIsUserSetted ( v0,v4 );
/* .line 398 */
v0 = this.this$0;
this.mEdgeModeType = v1;
/* .line 399 */
} // :cond_3
v0 = this.this$0;
v0 = this.mEdgeModeType;
final String v3 = "diy_suppression"; // const-string v3, "diy_suppression"
v0 = (( java.lang.String ) v0 ).equals ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 400 */
v0 = this.this$0;
this.mEdgeModeType = v2;
/* .line 402 */
} // :cond_4
} // :goto_1
v0 = this.this$0;
v0 = com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmSupportSensor ( v0 );
final String v2 = "EdgeSuppressionManager"; // const-string v2, "EdgeSuppressionManager"
if ( v0 != null) { // if-eqz v0, :cond_5
v0 = this.this$0;
v0 = this.mEdgeModeType;
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 403 */
v0 = this.this$0;
final String v1 = "intelligent"; // const-string v1, "intelligent"
com.miui.server.input.edgesuppression.EdgeSuppressionFactory .getEdgeSuppressionMode ( v1 );
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmBaseEdgeSuppression ( v0,v1 );
/* .line 405 */
v0 = this.this$0;
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).registerSensors ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->registerSensors()V
/* .line 406 */
final String v0 = "mBaseEdgeSuppression: intelligent"; // const-string v0, "mBaseEdgeSuppression: intelligent"
android.util.Slog .i ( v2,v0 );
/* goto/16 :goto_2 */
/* .line 408 */
} // :cond_5
v0 = this.this$0;
final String v1 = "normal"; // const-string v1, "normal"
com.miui.server.input.edgesuppression.EdgeSuppressionFactory .getEdgeSuppressionMode ( v1 );
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmBaseEdgeSuppression ( v0,v1 );
/* .line 410 */
final String v0 = "mBaseEdgeSuppression: normal"; // const-string v0, "mBaseEdgeSuppression: normal"
android.util.Slog .i ( v2,v0 );
/* .line 411 */
v0 = this.this$0;
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager ) v0 ).unRegisterSensors ( ); // invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->unRegisterSensors()V
/* .line 413 */
} // :cond_6
/* const-string/jumbo v0, "vertical_edge_suppression_size" */
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 414 */
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmBaseEdgeSuppression ( v0 );
v1 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmContext ( v1 );
v2 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmConfigLoader ( v2 );
/* iget v2, v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I */
v3 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmConfigLoader ( v3 );
/* iget v3, v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I */
(( com.miui.server.input.edgesuppression.BaseEdgeSuppression ) v0 ).setInputMethodSize ( v1, v2, v3, v5 ); // invoke-virtual {v0, v1, v2, v3, v5}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setInputMethodSize(Landroid/content/Context;IIZ)V
/* .line 416 */
} // :cond_7
final String v0 = "horizontal_edge_suppression_size"; // const-string v0, "horizontal_edge_suppression_size"
android.provider.Settings$Global .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 417 */
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmBaseEdgeSuppression ( v0 );
v1 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmContext ( v1 );
v2 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmConfigLoader ( v2 );
/* iget v2, v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I */
v3 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmConfigLoader ( v3 );
/* iget v3, v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I */
(( com.miui.server.input.edgesuppression.BaseEdgeSuppression ) v0 ).setInputMethodSize ( v1, v2, v3, v4 ); // invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setInputMethodSize(Landroid/content/Context;IIZ)V
/* .line 419 */
} // :cond_8
final String v0 = "edge_suppression_track_time"; // const-string v0, "edge_suppression_track_time"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 420 */
v1 = this.this$0;
v2 = this.contentResolver;
/* .line 421 */
v3 = android.os.UserHandle .myUserId ( );
/* .line 420 */
android.provider.Settings$System .getStringForUser ( v2,v0,v3 );
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fputmTrackEventTime ( v1,v0 );
/* .line 423 */
} // :cond_9
} // :goto_2
return;
} // .end method
void registerObserver ( ) {
/* .locals 6 */
/* .line 349 */
/* sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
int v1 = -1; // const/4 v1, -0x1
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 350 */
v0 = this.contentResolver;
final String v3 = "edge_type"; // const-string v3, "edge_type"
android.provider.Settings$System .getUriFor ( v3 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v1 ); // invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 353 */
android.provider.Settings$System .getUriFor ( v3 );
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 354 */
v0 = this.contentResolver;
final String v3 = "edge_size"; // const-string v3, "edge_size"
android.provider.Settings$System .getUriFor ( v3 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v1 ); // invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 357 */
android.provider.Settings$System .getUriFor ( v3 );
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 358 */
v0 = this.contentResolver;
final String v3 = "edge_suppression_track_time"; // const-string v3, "edge_suppression_track_time"
android.provider.Settings$System .getUriFor ( v3 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v1 ); // invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 360 */
android.provider.Settings$System .getUriFor ( v3 );
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 361 */
v0 = this.this$0;
com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mresetUserSetted ( v0 );
/* .line 364 */
} // :cond_0
v0 = this.contentResolver;
/* const-string/jumbo v3, "vertical_edge_suppression_size" */
android.provider.Settings$Global .getUriFor ( v3 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v4, v2, p0, v1 ); // invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 367 */
v0 = this.contentResolver;
final String v4 = "horizontal_edge_suppression_size"; // const-string v4, "horizontal_edge_suppression_size"
android.provider.Settings$Global .getUriFor ( v4 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v5, v2, p0, v1 ); // invoke-virtual {v0, v5, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 370 */
android.provider.Settings$Global .getUriFor ( v3 );
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 371 */
android.provider.Settings$Global .getUriFor ( v4 );
(( com.miui.server.input.edgesuppression.EdgeSuppressionManager$MiuiEdgeSuppressionObserver ) p0 ).onChange ( v2, v0 ); // invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 372 */
return;
} // .end method
