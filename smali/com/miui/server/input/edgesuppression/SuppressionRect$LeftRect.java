public class com.miui.server.input.edgesuppression.SuppressionRect$LeftRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "LeftRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "heightOfScreen" # I */
/* .param p3, "widthOfRect" # I */
/* .line 273 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 274 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;->setType(I)V
/* .line 275 */
int v0 = 2; // const/4 v0, 0x2
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;->setPosition(I)V
/* .line 276 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;->setTopLeftX(I)V
/* .line 277 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;->setTopLeftY(I)V
/* .line 278 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;->setBottomRightX(I)V
/* .line 279 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftRect ) p0 ).setBottomRightY ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;->setBottomRightY(I)V
/* .line 280 */
return;
} // .end method
