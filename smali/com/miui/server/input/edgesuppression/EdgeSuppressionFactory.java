public class com.miui.server.input.edgesuppression.EdgeSuppressionFactory {
	 /* .source "EdgeSuppressionFactory.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 public static final java.lang.String TYPE_INTELLIGENT;
	 public static final java.lang.String TYPE_NORMAL;
	 /* # direct methods */
	 private com.miui.server.input.edgesuppression.EdgeSuppressionFactory ( ) {
		 /* .locals 0 */
		 /* .line 9 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 10 */
		 return;
	 } // .end method
	 public static com.miui.server.input.edgesuppression.BaseEdgeSuppression getEdgeSuppressionMode ( java.lang.String p0 ) {
		 /* .locals 2 */
		 /* .param p0, "type" # Ljava/lang/String; */
		 /* .line 15 */
		 v0 = 		 (( java.lang.String ) p0 ).hashCode ( ); // invoke-virtual {p0}, Ljava/lang/String;->hashCode()I
		 /* sparse-switch v0, :sswitch_data_0 */
	 } // :cond_0
	 /* :sswitch_0 */
	 final String v0 = "intelligent"; // const-string v0, "intelligent"
	 v0 = 	 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 int v0 = 0; // const/4 v0, 0x0
		 /* :sswitch_1 */
		 final String v0 = "normal"; // const-string v0, "normal"
		 v0 = 		 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 int v0 = 1; // const/4 v0, 0x1
		 } // :goto_0
		 int v0 = -1; // const/4 v0, -0x1
	 } // :goto_1
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 21 */
	 final String v0 = "EdgeSuppressionManager"; // const-string v0, "EdgeSuppressionManager"
	 final String v1 = "Wrong type parameter for getEdgeSuppressionMode"; // const-string v1, "Wrong type parameter for getEdgeSuppressionMode"
	 android.util.Slog .e ( v0,v1 );
	 /* .line 22 */
	 com.miui.server.input.edgesuppression.NormalHelper .getInstance ( );
	 /* .line 19 */
	 /* :pswitch_0 */
	 com.miui.server.input.edgesuppression.NormalHelper .getInstance ( );
	 /* .line 17 */
	 /* :pswitch_1 */
	 com.miui.server.input.edgesuppression.IntelligentHelper .getInstance ( );
	 /* :sswitch_data_0 */
	 /* .sparse-switch */
	 /* -0x3df94319 -> :sswitch_1 */
	 /* 0x43994e77 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
