.class public Lcom/miui/server/input/edgesuppression/IntelligentHelper;
.super Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;
.source "IntelligentHelper.java"


# static fields
.field private static final INDEX_ABSOLUTE_FOURTH:I = 0x3

.field private static final INDEX_ABSOLUTE_ONE:I = 0x0

.field private static final INDEX_ABSOLUTE_THREE:I = 0x2

.field private static final INDEX_ABSOLUTE_TWO:I = 0x1

.field private static final INDEX_CONDITION_FOURTH:I = 0x7

.field private static final INDEX_CONDITION_ONE:I = 0x4

.field private static final INDEX_CONDITION_THREE:I = 0x6

.field private static final INDEX_CONDITION_TWO:I = 0x5

.field private static volatile mInstance:Lcom/miui/server/input/edgesuppression/IntelligentHelper;


# instance fields
.field private final mRectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/input/edgesuppression/SuppressionRect;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 18
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;-><init>()V

    .line 8
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    .line 19
    return-void
.end method

.method private getArrayList(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/input/edgesuppression/SuppressionRect;",
            ">;)V"
        }
    .end annotation

    .line 178
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    .line 179
    .local v1, "suppressionRect":Lcom/miui/server/input/edgesuppression/SuppressionRect;
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mSendList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->getList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 180
    .end local v1    # "suppressionRect":Lcom/miui/server/input/edgesuppression/SuppressionRect;
    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/edgesuppression/IntelligentHelper;
    .locals 2

    .line 22
    sget-object v0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mInstance:Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    if-nez v0, :cond_1

    .line 23
    const-class v0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    monitor-enter v0

    .line 24
    :try_start_0
    sget-object v1, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mInstance:Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    if-nez v1, :cond_0

    .line 25
    new-instance v1, Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    invoke-direct {v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;-><init>()V

    sput-object v1, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mInstance:Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    .line 27
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 29
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mInstance:Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    return-object v0
.end method

.method private getRectPosition(II)I
    .locals 2
    .param p1, "type"    # I
    .param p2, "position"    # I

    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "result":I
    const/4 v1, 0x2

    packed-switch p2, :pswitch_data_0

    goto :goto_4

    .line 163
    :pswitch_0
    if-ne p1, v1, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :cond_0
    const/4 v1, 0x7

    :goto_0
    move v0, v1

    .line 164
    goto :goto_4

    .line 160
    :pswitch_1
    if-ne p1, v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x6

    :goto_1
    move v0, v1

    .line 161
    goto :goto_4

    .line 157
    :pswitch_2
    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x5

    :goto_2
    move v0, v1

    .line 158
    goto :goto_4

    .line 154
    :pswitch_3
    if-ne p1, v1, :cond_3

    const/4 v1, 0x0

    goto :goto_3

    :cond_3
    const/4 v1, 0x4

    :goto_3
    move v0, v1

    .line 155
    nop

    .line 168
    :goto_4
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private initArrayList()V
    .locals 3

    .line 172
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    invoke-direct {v2}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private setHorizontalDataWithoutSensor(II)V
    .locals 9
    .param p1, "size"    # I
    .param p2, "type"    # I

    .line 95
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    move-object v1, p0

    move v3, p2

    move v8, p1

    invoke-virtual/range {v1 .. v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 97
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v4, 0x1

    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    sub-int v6, v0, p1

    iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    iget v8, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 99
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v4, 0x2

    const/4 v6, 0x0

    iget v8, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    move-object v1, p0

    move v7, p1

    invoke-virtual/range {v1 .. v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 101
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v4, 0x3

    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    sub-int v5, v0, p1

    iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    iget v8, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 103
    return-void
.end method

.method private setHorizontalRectData(II)V
    .locals 12
    .param p1, "size"    # I
    .param p2, "type"    # I

    .line 59
    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mHoldSensorState:I

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 77
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v4}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v4, p0

    move v6, p2

    invoke-virtual/range {v4 .. v11}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 79
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v3}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v6, 0x1

    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    sub-int v8, v0, p1

    iget v9, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    iget v10, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    move-object v3, p0

    move v5, p2

    invoke-virtual/range {v3 .. v10}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 82
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;-><init>(III)V

    invoke-virtual {v0, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 84
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 87
    goto :goto_0

    .line 66
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v4}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v10, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    move-object v4, p0

    move v6, p2

    move v11, p1

    invoke-virtual/range {v4 .. v11}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 68
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v3}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v6, 0x1

    const/4 v10, 0x0

    move-object v3, p0

    move v5, p2

    invoke-virtual/range {v3 .. v10}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 70
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;-><init>(III)V

    invoke-virtual {v0, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 75
    goto :goto_0

    .line 63
    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setHorizontalDataWithoutSensor(II)V

    .line 64
    nop

    .line 91
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setPortraitRectData(II)V
    .locals 10
    .param p1, "size"    # I
    .param p2, "type"    # I

    .line 111
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move v3, p2

    invoke-virtual/range {v1 .. v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 113
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v5, 0x1

    const/4 v9, 0x0

    move-object v2, p0

    move v4, p2

    invoke-virtual/range {v2 .. v9}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 115
    const/4 v0, 0x3

    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 116
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v0

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;-><init>(IIII)V

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 120
    :cond_0
    if-ne p2, v1, :cond_1

    .line 121
    iget v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mHoldSensorState:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 138
    :pswitch_0
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v0

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;-><init>(IIII)V

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 143
    goto :goto_0

    .line 131
    :pswitch_1
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v0

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;-><init>(IIII)V

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 136
    goto :goto_0

    .line 125
    :pswitch_2
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v2

    new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 127
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I

    move-result v0

    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I

    iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I

    invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;-><init>(IIII)V

    invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 129
    nop

    .line 148
    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getEdgeSuppressionData(III)Ljava/util/ArrayList;
    .locals 8
    .param p1, "rotation"    # I
    .param p2, "widthOfScreen"    # I
    .param p3, "heightOfScreen"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->initArrayList()V

    .line 38
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mIsHorizontal:Z

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eqz v0, :cond_1

    .line 39
    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mAbsoluteSize:I

    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setHorizontalRectData(II)V

    .line 40
    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConditionSize:I

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setHorizontalRectData(II)V

    goto :goto_0

    .line 42
    :cond_1
    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mAbsoluteSize:I

    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setPortraitRectData(II)V

    .line 43
    iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConditionSize:I

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setPortraitRectData(II)V

    .line 45
    :goto_0
    iget v6, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConnerWidth:I

    iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConnerHeight:I

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getCornerData(IIIII)Ljava/util/ArrayList;

    move-result-object v0

    .line 47
    .local v0, "cornerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;"
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mSendList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 48
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mRectList:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getArrayList(Ljava/util/ArrayList;)V

    .line 49
    invoke-direct {p0, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getArrayList(Ljava/util/ArrayList;)V

    .line 50
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mSendList:Ljava/util/ArrayList;

    return-object v1
.end method
