public class com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "RightBottomHalfRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "heightOfScreen" # I */
/* .param p3, "widthOfScreen" # I */
/* .param p4, "widthOfRect" # I */
/* .line 179 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 180 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setType(I)V
/* .line 181 */
int v0 = 3; // const/4 v0, 0x3
(( com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setPosition(I)V
/* .line 182 */
/* sub-int v0, p3, p4 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setTopLeftX(I)V
/* .line 183 */
/* div-int/lit8 v0, p2, 0x2 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setTopLeftY(I)V
/* .line 184 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setBottomRightX(I)V
/* .line 185 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightBottomHalfRect ) p0 ).setBottomRightY ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setBottomRightY(I)V
/* .line 186 */
return;
} // .end method
