.class Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;
.super Ljava/lang/Object;
.source "EdgeSuppressionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConfigLoader"
.end annotation


# static fields
.field private static final ATTRIBUTE_ABSOLUTE_SIZE:Ljava/lang/String; = "absoluteSize"

.field private static final ATTRIBUTE_CONDITION_SIZE:Ljava/lang/String; = "conditionSize"

.field private static final ATTRIBUTE_CONNER_HEIGHT:Ljava/lang/String; = "connerHeight"

.field private static final ATTRIBUTE_CONNER_WIDTH:Ljava/lang/String; = "connerWidth"

.field private static final ATTRIBUTE_DISPLAY_ID:Ljava/lang/String; = "displayId"

.field private static final ATTRIBUTE_IS_HORIZONTAL:Ljava/lang/String; = "isHorizontal"

.field private static final ATTRIBUTE_MAX_ABSOLUTE_SIZE:Ljava/lang/String; = "maxAbsoluteSize"

.field private static final ATTRIBUTE_MAX_CONDITION_SIZE:Ljava/lang/String; = "maxConditionSize"

.field private static final ATTRIBUTE_MAX_INPUTMETHOD_SIZE:Ljava/lang/String; = "maxInputMethodSize"

.field private static final ATTRIBUTE_MIN_ABSOLUTE_SIZE:Ljava/lang/String; = "minAbsoluteSize"

.field private static final ATTRIBUTE_MIN_CONDITION_SIZE:Ljava/lang/String; = "minConditionSize"

.field private static final ATTRIBUTE_MIN_INPUTMETHOD_SIZE:Ljava/lang/String; = "minInputMethodSize"

.field private static final ATTRIBUTE_TYPE:Ljava/lang/String; = "type"

.field private static final CONFIG_FILE:Ljava/lang/String; = "edge_suppression_config"

.field private static final TAG_CONFIG_ITEM:Ljava/lang/String; = "edgesuppressionitem"

.field private static final TAG_FILE:Ljava/lang/String; = "edgesuppressions"


# instance fields
.field mAbsoluteLevel:[I

.field mConditionLevel:[I

.field mMaxInputMethodSize:I

.field mMinInputMethodSize:I

.field final synthetic this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;


# direct methods
.method static bridge synthetic -$$Nest$mgetInfo(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;Ljava/lang/String;I)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->getInfo([Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;Ljava/lang/String;I)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$minitEdgeSuppressionConfig(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->initEdgeSuppressionConfig()V

    return-void
.end method

.method constructor <init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V
    .locals 2
    .param p1, "this$0"    # Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 474
    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    const/4 v0, 0x0

    filled-new-array {v0, v0, v0, v0, v0}, [I

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    .line 492
    filled-new-array {v0, v0, v0, v0, v0}, [I

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    .line 493
    const/16 v0, 0xa

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I

    .line 494
    const/16 v0, 0x2d

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I

    return-void
.end method

.method private getInfo([Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;Ljava/lang/String;I)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .locals 9
    .param p1, "list"    # [Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "rotation"    # I

    .line 604
    if-eqz p2, :cond_3

    array-length v0, p1

    if-lez v0, :cond_3

    .line 605
    const/4 v0, 0x0

    .line 606
    .local v0, "isHorizontal":Z
    const/4 v1, 0x1

    if-eq p3, v1, :cond_0

    const/4 v1, 0x3

    if-ne p3, v1, :cond_1

    .line 607
    :cond_0
    const/4 v0, 0x1

    .line 609
    :cond_1
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, p1, v2

    .line 610
    .local v3, "target":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    invoke-virtual {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->isHorizontal()Z

    move-result v4

    if-ne v0, v4, :cond_2

    .line 611
    return-object v3

    .line 609
    .end local v3    # "target":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 615
    .end local v0    # "isHorizontal":Z
    :cond_3
    const-string v0, "EdgeSuppressionManager"

    const-string v1, "There is not available info"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    new-instance v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "default_suppression"

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V

    return-object v0
.end method

.method private initEdgeSuppressionConfig()V
    .locals 32

    .line 497
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v2, "xml"

    const-string v3, "android.miui"

    const-string v4, "edge_suppression_config"

    invoke-virtual {v0, v4, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 499
    .local v2, "resId":I
    if-nez v2, :cond_0

    return-void

    .line 500
    :cond_0
    iget-object v0, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    .line 501
    .local v3, "xmlResourceParser":Landroid/content/res/XmlResourceParser;
    if-nez v3, :cond_1

    return-void

    .line 502
    :cond_1
    const-string/jumbo v0, "xmlResourceParser is not null, file = edge_suppression_config"

    const-string v4, "EdgeSuppressionManager"

    invoke-static {v4, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :try_start_0
    const-string v0, "edgesuppressions"

    invoke-static {v3, v0}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 508
    const/4 v0, 0x0

    .line 509
    .local v0, "index":I
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 510
    :goto_0
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_d

    const-string v5, "edgesuppressionitem"

    .line 511
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 512
    const-string v5, "absoluteSize"

    const/4 v7, 0x0

    invoke-interface {v3, v7, v5}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 514
    .local v5, "absoluteSize":Ljava/lang/String;
    const-string v8, "conditionSize"

    invoke-interface {v3, v7, v8}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 516
    .local v8, "conditionSize":Ljava/lang/String;
    const-string v9, "connerWidth"

    invoke-interface {v3, v7, v9}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 518
    .local v9, "connerWidth":Ljava/lang/String;
    const-string v10, "connerHeight"

    invoke-interface {v3, v7, v10}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 520
    .local v10, "connerHeight":Ljava/lang/String;
    const-string v11, "isHorizontal"

    invoke-interface {v3, v7, v11}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 522
    .local v11, "isHorizontal":Ljava/lang/String;
    const-string/jumbo v12, "type"

    invoke-interface {v3, v7, v12}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 523
    .local v12, "type":Ljava/lang/String;
    const-string v13, "maxAbsoluteSize"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v20, v13

    .line 525
    .local v20, "maxAbsoluteSize":Ljava/lang/String;
    const-string v13, "minAbsoluteSize"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v21, v13

    .line 527
    .local v21, "minAbsoluteSize":Ljava/lang/String;
    const-string v13, "maxConditionSize"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v22, v13

    .line 529
    .local v22, "maxConditionSize":Ljava/lang/String;
    const-string v13, "minConditionSize"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v23, v13

    .line 531
    .local v23, "minConditionSize":Ljava/lang/String;
    const-string v13, "minInputMethodSize"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v24, v13

    .line 533
    .local v24, "minInputMethodSize":Ljava/lang/String;
    const-string v13, "maxInputMethodSize"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v25, v13

    .line 535
    .local v25, "maxInputMethodSize":Ljava/lang/String;
    const-string v13, "displayId"

    invoke-interface {v3, v7, v13}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 536
    .local v7, "displayId":Ljava/lang/String;
    const-string v13, "inputmethod_size"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v13, :cond_2

    .line 537
    :try_start_1
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I

    .line 538
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 539
    move/from16 v31, v2

    goto/16 :goto_b

    .line 598
    .end local v0    # "index":I
    .end local v5    # "absoluteSize":Ljava/lang/String;
    .end local v7    # "displayId":Ljava/lang/String;
    .end local v8    # "conditionSize":Ljava/lang/String;
    .end local v9    # "connerWidth":Ljava/lang/String;
    .end local v10    # "connerHeight":Ljava/lang/String;
    .end local v11    # "isHorizontal":Ljava/lang/String;
    .end local v12    # "type":Ljava/lang/String;
    .end local v20    # "maxAbsoluteSize":Ljava/lang/String;
    .end local v21    # "minAbsoluteSize":Ljava/lang/String;
    .end local v22    # "maxConditionSize":Ljava/lang/String;
    .end local v23    # "minConditionSize":Ljava/lang/String;
    .end local v24    # "minInputMethodSize":Ljava/lang/String;
    .end local v25    # "maxInputMethodSize":Ljava/lang/String;
    :catch_0
    move-exception v0

    move/from16 v31, v2

    goto/16 :goto_c

    .line 542
    .restart local v0    # "index":I
    .restart local v5    # "absoluteSize":Ljava/lang/String;
    .restart local v7    # "displayId":Ljava/lang/String;
    .restart local v8    # "conditionSize":Ljava/lang/String;
    .restart local v9    # "connerWidth":Ljava/lang/String;
    .restart local v10    # "connerHeight":Ljava/lang/String;
    .restart local v11    # "isHorizontal":Ljava/lang/String;
    .restart local v12    # "type":Ljava/lang/String;
    .restart local v20    # "maxAbsoluteSize":Ljava/lang/String;
    .restart local v21    # "minAbsoluteSize":Ljava/lang/String;
    .restart local v22    # "maxConditionSize":Ljava/lang/String;
    .restart local v23    # "minConditionSize":Ljava/lang/String;
    .restart local v24    # "minInputMethodSize":Ljava/lang/String;
    .restart local v25    # "maxInputMethodSize":Ljava/lang/String;
    :cond_2
    :try_start_2
    sget-boolean v13, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    const/16 v26, 0x2

    const/16 v27, 0x0

    const/4 v15, 0x1

    if-nez v13, :cond_7

    .line 543
    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v13
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    const-string v14, "custom_suppression"

    sparse-switch v13, :sswitch_data_0

    :cond_3
    goto :goto_1

    :sswitch_0
    :try_start_3
    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    move v13, v6

    goto :goto_2

    :sswitch_1
    const-string/jumbo v13, "wake_suppression"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    move v13, v15

    goto :goto_2

    :sswitch_2
    const-string/jumbo v13, "strong_suppression"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    move/from16 v13, v26

    goto :goto_2

    :sswitch_3
    const-string v13, "default_suppression"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    move/from16 v13, v27

    goto :goto_2

    :goto_1
    const/4 v13, -0x1

    :goto_2
    packed-switch v13, :pswitch_data_0

    goto :goto_3

    .line 558
    :pswitch_0
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v27

    .line 559
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    const/16 v16, 0x4

    aput v13, v6, v16

    .line 560
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v27

    .line 561
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v16

    .line 562
    goto :goto_3

    .line 554
    :pswitch_1
    iget-object v13, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    aput v16, v13, v6

    .line 555
    iget-object v13, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    aput v16, v13, v6

    .line 556
    goto :goto_3

    .line 550
    :pswitch_2
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v15

    .line 551
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v15

    .line 552
    goto :goto_3

    .line 545
    :pswitch_3
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v26

    .line 546
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v26

    .line 547
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v13, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v13, v13, v26

    int-to-float v13, v13

    invoke-static {v6, v13}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;F)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 548
    nop

    .line 566
    :goto_3
    :try_start_4
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v6}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmEdgeSuppressionInfoList(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    move-result-object v6

    add-int/lit8 v28, v0, 0x1

    .end local v0    # "index":I
    .local v28, "index":I
    new-instance v29, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    .line 567
    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 568
    move/from16 v16, v27

    goto :goto_4

    :cond_4
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    move/from16 v16, v13

    .line 569
    :goto_4
    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    move/from16 v17, v27

    goto :goto_5

    .line 570
    :cond_5
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    move/from16 v17, v13

    .line 571
    :goto_5
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    .line 572
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    .line 573
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    if-ne v13, v15, :cond_6

    move/from16 v30, v15

    goto :goto_6

    :cond_6
    move/from16 v30, v27

    :goto_6
    move-object/from16 v13, v29

    move/from16 v14, v16

    move/from16 v31, v2

    move v2, v15

    .end local v2    # "resId":I
    .local v31, "resId":I
    move/from16 v15, v17

    move/from16 v16, v18

    move/from16 v17, v19

    move/from16 v18, v30

    move-object/from16 v19, v12

    :try_start_5
    invoke-direct/range {v13 .. v19}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V

    aput-object v29, v6, v0

    move/from16 v0, v28

    goto :goto_7

    .line 542
    .end local v28    # "index":I
    .end local v31    # "resId":I
    .restart local v0    # "index":I
    .restart local v2    # "resId":I
    :cond_7
    move/from16 v31, v2

    move v2, v15

    .line 576
    .end local v2    # "resId":I
    .restart local v31    # "resId":I
    :goto_7
    sget-boolean v6, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v6, :cond_b

    .line 577
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 578
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v26

    .line 579
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    aput v13, v6, v26

    .line 580
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v13, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v13, v13, v26

    int-to-float v13, v13

    invoke-static {v6, v13}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;F)V

    .line 581
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v6}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmEdgeSuppressionInfoList(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    move-result-object v6

    add-int/lit8 v26, v0, 0x1

    .end local v0    # "index":I
    .local v26, "index":I
    new-instance v28, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    .line 582
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 583
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 584
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 585
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 586
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-ne v13, v2, :cond_8

    move/from16 v18, v2

    goto :goto_8

    :cond_8
    move/from16 v18, v27

    :goto_8
    move-object/from16 v13, v28

    move-object/from16 v19, v12

    invoke-direct/range {v13 .. v19}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V

    aput-object v28, v6, v0

    move/from16 v0, v26

    goto :goto_a

    .line 588
    .end local v26    # "index":I
    .restart local v0    # "index":I
    :cond_9
    iget-object v6, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v6}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmSubScreenEdgeSuppressionList(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    move-result-object v6

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    new-instance v28, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    .line 589
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 590
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    .line 591
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 592
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 593
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    if-ne v13, v2, :cond_a

    move/from16 v18, v2

    goto :goto_9

    :cond_a
    move/from16 v18, v27

    :goto_9
    move-object/from16 v13, v28

    move-object/from16 v19, v12

    invoke-direct/range {v13 .. v19}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;-><init>(IIIIZLjava/lang/String;)V

    aput-object v28, v6, v26

    .line 596
    :cond_b
    :goto_a
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move/from16 v2, v31

    goto/16 :goto_0

    .line 598
    .end local v0    # "index":I
    .end local v5    # "absoluteSize":Ljava/lang/String;
    .end local v7    # "displayId":Ljava/lang/String;
    .end local v8    # "conditionSize":Ljava/lang/String;
    .end local v9    # "connerWidth":Ljava/lang/String;
    .end local v10    # "connerHeight":Ljava/lang/String;
    .end local v11    # "isHorizontal":Ljava/lang/String;
    .end local v12    # "type":Ljava/lang/String;
    .end local v20    # "maxAbsoluteSize":Ljava/lang/String;
    .end local v21    # "minAbsoluteSize":Ljava/lang/String;
    .end local v22    # "maxConditionSize":Ljava/lang/String;
    .end local v23    # "minConditionSize":Ljava/lang/String;
    .end local v24    # "minInputMethodSize":Ljava/lang/String;
    .end local v25    # "maxInputMethodSize":Ljava/lang/String;
    :catch_1
    move-exception v0

    goto :goto_c

    .line 511
    .end local v31    # "resId":I
    .restart local v0    # "index":I
    .restart local v2    # "resId":I
    :cond_c
    move/from16 v31, v2

    .end local v2    # "resId":I
    .restart local v31    # "resId":I
    goto :goto_b

    .line 510
    .end local v31    # "resId":I
    .restart local v2    # "resId":I
    :cond_d
    move/from16 v31, v2

    .line 600
    .end local v0    # "index":I
    .end local v2    # "resId":I
    .restart local v31    # "resId":I
    :goto_b
    goto :goto_d

    .line 598
    .end local v31    # "resId":I
    .restart local v2    # "resId":I
    :catch_2
    move-exception v0

    move/from16 v31, v2

    .line 599
    .end local v2    # "resId":I
    .local v0, "e":Ljava/lang/Exception;
    .restart local v31    # "resId":I
    :goto_c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "init EdgeSuppression Config fail!"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_d
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1ca0abcb -> :sswitch_3
        0x3cb5238b -> :sswitch_2
        0x5d6e1318 -> :sswitch_1
        0x600bc165 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
