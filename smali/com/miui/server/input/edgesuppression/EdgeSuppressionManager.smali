.class public Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
.super Ljava/lang/Object;
.source "EdgeSuppressionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;,
        Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;,
        Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;
    }
.end annotation


# static fields
.field public static final HORIZONTAL_EDGE_SUPPRESSION_SIZE:Ljava/lang/String; = "horizontal_edge_suppression_size"

.field private static final INDEX_OF_DEFAULT:I = 0x2

.field private static final INDEX_OF_MAX:I = 0x4

.field private static final INDEX_OF_MIN:I = 0x0

.field private static final INDEX_OF_STRONG:I = 0x3

.field private static final INDEX_OF_WAKE:I = 0x1

.field public static final IS_SUPPORT_EDGE_MODE:Z

.field private static final KEY_INPUTMETHOD_SIZE:Ljava/lang/String; = "inputmethod_size"

.field private static final KEY_SCREEN_EDGE_MODE_CUSTOM:Ljava/lang/String; = "custom_suppression"

.field private static final KEY_SCREEN_EDGE_MODE_DEFAULT:Ljava/lang/String; = "default_suppression"

.field private static final KEY_SCREEN_EDGE_MODE_DIY:Ljava/lang/String; = "diy_suppression"

.field private static final KEY_SCREEN_EDGE_MODE_STRONG:Ljava/lang/String; = "strong_suppression"

.field private static final KEY_SCREEN_EDGE_MODE_WAKE:Ljava/lang/String; = "wake_suppression"

.field public static final REASON_OF_CONFIGURATION:Ljava/lang/String; = "configuration"

.field public static final REASON_OF_GAMEBOOSTER:Ljava/lang/String; = "gameBooster"

.field public static final REASON_OF_HOLDSENSOR:Ljava/lang/String; = "holdSensor"

.field public static final REASON_OF_LAYSENSOR:Ljava/lang/String; = "laySensor"

.field public static final REASON_OF_ROTATION:Ljava/lang/String; = "rotation"

.field public static final REASON_OF_SCREENON:Ljava/lang/String; = "screenOn"

.field public static final REASON_OF_SETTINGS:Ljava/lang/String; = "settings"

.field public static final SHOULD_REMOVE_EDGE_SETTINGS:Z

.field private static final SUPPORT_SENSOR:Ljava/lang/String; = "support_edgesuppression_with_sensor"

.field private static final TAG:Ljava/lang/String; = "EdgeSuppressionManager"

.field private static final TRACK_EDGE_TYPE_DEFAULT:Ljava/lang/String; = "none"

.field private static final TRACK_EVENT_TIME:Ljava/lang/String; = "edge_suppression_track_time"

.field public static final VERTICAL_EDGE_SUPPRESSION_SIZE:Ljava/lang/String; = "vertical_edge_suppression_size"

.field private static volatile sInstance:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;


# instance fields
.field private mAbsoluteSize:I

.field private mBaseEdgeSuppression:Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

.field private mConditionSize:I

.field private final mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

.field private final mContext:Landroid/content/Context;

.field private mEdgeModeSize:F

.field public mEdgeModeType:Ljava/lang/String;

.field private final mEdgeSuppressionInfoList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

.field private mFolded:Z

.field private final mHandler:Landroid/os/Handler;

.field final mHoldListener:Lmiui/util/HoldSensorWrapper$HoldSensorChangeListener;

.field private volatile mHoldSensorState:I

.field private mHoldSensorWrapper:Lmiui/util/HoldSensorWrapper;

.field private mIsUserSetted:Z

.field private mLastEdgeModeSize:I

.field private mLastEdgeModeType:Ljava/lang/String;

.field final mLayListener:Lmiui/util/LaySensorWrapper$LaySensorChangeListener;

.field private volatile mLaySensorState:I

.field private mLaySensorWrapper:Lmiui/util/LaySensorWrapper;

.field private mMaxAdjustValue:I

.field private mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSimpleDateFormat:Ljava/text/SimpleDateFormat;

.field private final mSubScreenEdgeSuppressionList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

.field private mSupportSensor:Z

.field private mTrackEventTime:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$SGmOlD3MzBOyGeD6hDYNmd5x7G4(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->lambda$new$1(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$gxYSBlipA664OV0f0LDcHjqRwvM(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->lambda$new$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$sIartofm1hh7XpB3-cTYCZ_V5Sw(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->lambda$new$0(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmBaseEdgeSuppression(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mBaseEdgeSuppression:Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmConfigLoader(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)F
    .locals 0

    iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmEdgeSuppressionInfoList(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeSuppressionInfoList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHoldSensorState(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmLaySensorState(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmMaxAdjustValue(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mMaxAdjustValue:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSubScreenEdgeSuppressionList(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSubScreenEdgeSuppressionList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmSupportSensor(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmBaseEdgeSuppression(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mBaseEdgeSuppression:Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;F)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsUserSetted(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mIsUserSetted:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeSize:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastEdgeModeType(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeType:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmTrackEventTime(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mTrackEventTime:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->handleEdgeSuppressionData()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mnotifyEdgeSuppressionChanged(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mresetUserSetted(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->resetUserSetted()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 60
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/ITouchFeature;->hasSupportEdgeMode()Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    .line 62
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/util/ITouchFeature;->shouldRemoveEdgeSettings()Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->SHOULD_REMOVE_EDGE_SETTINGS:Z

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const-string v0, "default_suppression"

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    .line 82
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeSuppressionInfoList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    .line 83
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSubScreenEdgeSuppressionList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I

    .line 89
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mIsUserSetted:Z

    .line 90
    const-string v2, "1970-01-01 00:00:00"

    iput-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mTrackEventTime:Ljava/lang/String;

    .line 91
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;

    .line 92
    invoke-static {v1}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 93
    new-instance v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    iput-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldListener:Lmiui/util/HoldSensorWrapper$HoldSensorChangeListener;

    .line 102
    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    .line 103
    new-instance v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLayListener:Lmiui/util/LaySensorWrapper$LaySensorChangeListener;

    .line 114
    nop

    .line 115
    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 116
    .local v0, "windowManager":Landroid/view/WindowManager;
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 117
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 118
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v3, v1

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v4, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    .line 119
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v3, v1

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v4, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    .line 120
    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    .line 121
    new-instance v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    invoke-direct {v1, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    iput-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    .line 122
    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->-$$Nest$minitEdgeSuppressionConfig(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;)V

    .line 123
    const-string/jumbo v1, "support_edgesuppression_with_sensor"

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z

    .line 124
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->initParamForOldSettings()V

    .line 125
    nop

    .line 126
    const-string v1, "normal"

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionFactory;->getEdgeSuppressionMode(Ljava/lang/String;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mBaseEdgeSuppression:Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    .line 127
    iget v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F

    float-to-int v3, v3

    invoke-virtual {v1, p1, v3}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->initInputMethodData(Landroid/content/Context;I)V

    .line 128
    new-instance v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getThread()Lcom/android/server/input/MiuiInputThread;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHandler:Landroid/os/Handler;

    .line 129
    new-instance v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;

    invoke-direct {v3, p0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Landroid/os/Handler;)V

    move-object v1, v3

    .line 131
    .local v1, "mMiuiEdgeSuppressionObserver":Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;
    invoke-virtual {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->registerObserver()V

    .line 132
    sget-boolean v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v3, :cond_0

    .line 133
    iget-object v4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScheduledExecutorService:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda2;

    invoke-direct {v5, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    const-wide/16 v6, 0x5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    .line 135
    const-wide/16 v8, 0x1

    invoke-virtual {v3, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v8

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    .line 133
    invoke-interface/range {v4 .. v10}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 137
    :cond_0
    return-void
.end method

.method private compareTimeOfNow(Ljava/lang/String;Ljava/util/Date;)I
    .locals 5
    .param p1, "dateOldString"    # Ljava/lang/String;
    .param p2, "dateNow"    # Ljava/util/Date;

    .line 307
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    const-string p1, "1970-01-01 00:00:00"

    .line 310
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 312
    .local v0, "dateOld":Ljava/util/Date;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 315
    goto :goto_0

    .line 313
    :catch_0
    move-exception v1

    .line 314
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 316
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x5265c00

    div-long/2addr v1, v3

    long-to-int v1, v1

    return v1
.end method

.method private getAbsoluteSize(F)I
    .locals 4
    .param p1, "conditionSize"    # F

    .line 205
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 206
    .local v0, "result":I
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v2, v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v2, v1

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v1, v2

    int-to-float v1, v1

    .line 207
    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 208
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    aget v0, v1, v2

    goto :goto_0

    .line 209
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v1, v2

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v1, v3

    int-to-float v1, v1

    .line 210
    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eq v1, v2, :cond_1

    .line 211
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    aget v0, v1, v3

    goto :goto_0

    .line 212
    :cond_1
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v1, v3

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v1, v3

    int-to-float v1, v1

    .line 213
    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eq v1, v2, :cond_2

    .line 214
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    aget v0, v1, v3

    goto :goto_0

    .line 215
    :cond_2
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v1, v1, v3

    int-to-float v1, v1

    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    const/4 v3, 0x4

    aget v1, v1, v3

    int-to-float v1, v1

    .line 216
    invoke-static {p1, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-eq v1, v2, :cond_3

    .line 217
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    aget v0, v1, v3

    .line 219
    :cond_3
    :goto_0
    return v0
.end method

.method private getEdgeSuppressionInfo(FI)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .locals 4
    .param p1, "edgeModeSize"    # F
    .param p2, "rotation"    # I

    .line 167
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    const/4 v1, -0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_0

    .line 168
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 176
    :pswitch_0
    const/4 p1, 0x0

    .line 177
    goto :goto_0

    .line 173
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    int-to-float p1, v0

    .line 174
    goto :goto_0

    .line 170
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    aget v0, v0, v2

    int-to-float p1, v0

    .line 171
    nop

    .line 182
    :cond_0
    :goto_0
    float-to-int v0, p1

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConditionSize:I

    .line 183
    invoke-direct {p0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getAbsoluteSize(F)I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mAbsoluteSize:I

    .line 185
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeSuppressionInfoList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-static {v0, v1, v3, p2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->-$$Nest$mgetInfo(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;Ljava/lang/String;I)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    move-result-object v0

    .line 188
    .local v0, "info":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    iget-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z

    if-nez v1, :cond_1

    const-string v1, "custom_suppression"

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    :cond_1
    iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mAbsoluteSize:I

    invoke-virtual {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->setAbsoluteSize(I)V

    .line 190
    iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConditionSize:I

    invoke-virtual {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->setConditionSize(I)V

    .line 194
    :cond_2
    sget-boolean v1, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mFolded:Z

    if-eqz v1, :cond_6

    .line 196
    if-eqz p2, :cond_4

    if-ne p2, v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v1, 0x0

    .line 197
    .local v1, "screenState":I
    :goto_2
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSubScreenEdgeSuppressionList:[Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    aget-object v2, v2, v1

    if-nez v2, :cond_5

    move-object v2, v0

    goto :goto_3

    .line 198
    :cond_5
    nop

    :goto_3
    move-object v0, v2

    .line 200
    .end local v1    # "screenState":I
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mScreenWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mScreenHeight = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EdgeSuppressionManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 140
    sget-object v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->sInstance:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    if-nez v0, :cond_1

    .line 141
    const-class v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    monitor-enter v0

    .line 142
    :try_start_0
    sget-object v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->sInstance:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    if-nez v1, :cond_0

    .line 143
    new-instance v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-direct {v1, p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->sInstance:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 145
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 147
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->sInstance:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    return-object v0
.end method

.method private handleEdgeSuppressionData()V
    .locals 10

    .line 152
    sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 154
    .local v0, "rotation":I
    const/4 v1, 0x0

    .line 155
    .local v1, "targetId":I
    iget v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeSize:F

    invoke-direct {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->getEdgeSuppressionInfo(FI)Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;

    move-result-object v8

    .line 156
    .local v8, "info":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    sget-boolean v2, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mFolded:Z

    if-eqz v2, :cond_0

    .line 157
    const/4 v1, 0x1

    move v9, v1

    goto :goto_0

    .line 159
    :cond_0
    move v9, v1

    .end local v1    # "targetId":I
    .local v9, "targetId":I
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleEdgeSuppressionData:targetId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EdgeSuppressionManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mBaseEdgeSuppression:Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    iget v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I

    iget v6, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    iget v7, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    move-object v2, v8

    move v4, v0

    move v5, v9

    invoke-virtual/range {v1 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->updateInterNalParam(Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;IIIII)V

    .line 162
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mBaseEdgeSuppression:Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    invoke-virtual {v1}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->syncDataToKernel()V

    .line 164
    .end local v0    # "rotation":I
    .end local v8    # "info":Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .end local v9    # "targetId":I
    :cond_1
    return-void
.end method

.method private initParamForOldSettings()V
    .locals 2

    .line 247
    const-string v0, "edge_suppresson_condition"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 248
    .local v0, "intArray":[I
    if-eqz v0, :cond_0

    .line 249
    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mMaxAdjustValue:I

    .line 251
    :cond_0
    return-void
.end method

.method private synthetic lambda$new$0(I)V
    .locals 2
    .param p1, "status"    # I

    .line 95
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I

    if-eq p1, v0, :cond_0

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Hold status is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EdgeSuppressionManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I

    .line 98
    const/4 v0, 0x1

    const-string v1, "holdSensor"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V

    .line 101
    :cond_0
    return-void
.end method

.method private synthetic lambda$new$1(I)V
    .locals 2
    .param p1, "status"    # I

    .line 105
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    if-eq p1, v0, :cond_0

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Lay status is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EdgeSuppressionManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    .line 108
    const/4 v0, 0x1

    const-string v1, "laySensor"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V

    .line 111
    :cond_0
    return-void
.end method

.method private synthetic lambda$new$2()V
    .locals 0

    .line 134
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->trackEdgeSuppressionAndUpdateTrackTime()V

    .line 135
    return-void
.end method

.method private notifyEdgeSuppressionChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "reason"    # Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 236
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 237
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "reason"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 239
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, p1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 244
    :goto_0
    return-void
.end method

.method private resetUserSetted()V
    .locals 1

    .line 332
    const-string v0, "none"

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeType:Ljava/lang/String;

    .line 333
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeSize:I

    .line 334
    return-void
.end method

.method private trackEdgeSuppressionAndUpdateTrackTime()V
    .locals 8

    .line 320
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 321
    .local v0, "dateNow":Ljava/util/Date;
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mTrackEventTime:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->compareTimeOfNow(Ljava/lang/String;Ljava/util/Date;)I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    .line 322
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mIsUserSetted:Z

    iget v4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeSize:I

    iget-object v5, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLastEdgeModeType:Ljava/lang/String;

    iget v6, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConditionSize:I

    iget-object v7, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lcom/android/server/input/InputOneTrackUtil;->trackEdgeSuppressionEvent(ZILjava/lang/String;ILjava/lang/String;)V

    .line 325
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSimpleDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "updateTime":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 327
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 326
    const-string v4, "edge_suppression_track_time"

    invoke-static {v2, v4, v1, v3}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 329
    .end local v1    # "updateTime":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public finishedGoingToSleep()V
    .locals 2

    .line 285
    iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z

    if-eqz v0, :cond_0

    const-string v0, "default_suppression"

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->unRegisterSensors()V

    .line 288
    :cond_0
    return-void
.end method

.method public finishedWakingUp()V
    .locals 2

    .line 276
    sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v0, :cond_1

    .line 277
    iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mSupportSensor:Z

    if-eqz v0, :cond_0

    const-string v0, "default_suppression"

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->registerSensors()V

    .line 280
    :cond_0
    const/4 v0, 0x1

    const-string v1, "screenOn"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V

    .line 282
    :cond_1
    return-void
.end method

.method public getAbsoluteLevel()[I
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mAbsoluteLevel:[I

    return-object v0
.end method

.method public getConditionLevel()[I
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mConditionLevel:[I

    return-object v0
.end method

.method public getInputMethodSizeScope()[I
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mConfigLoader:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    iget v1, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    return-object v0
.end method

.method public getScreenWidth()I
    .locals 1

    .line 303
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    return v0
.end method

.method public handleEdgeModeChange(Ljava/lang/String;)V
    .locals 1
    .param p1, "reason"    # Ljava/lang/String;

    .line 224
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V

    .line 225
    return-void
.end method

.method public handleEdgeModeChange(Ljava/lang/String;ZLcom/android/server/wm/DisplayFrames;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;
    .param p2, "folded"    # Z
    .param p3, "displayFrames"    # Lcom/android/server/wm/DisplayFrames;

    .line 228
    iput-boolean p2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mFolded:Z

    .line 229
    iget v0, p3, Lcom/android/server/wm/DisplayFrames;->mWidth:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iget v2, p3, Lcom/android/server/wm/DisplayFrames;->mHeight:I

    sub-int/2addr v2, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenWidth:I

    .line 230
    iget v0, p3, Lcom/android/server/wm/DisplayFrames;->mWidth:I

    sub-int/2addr v0, v1

    iget v2, p3, Lcom/android/server/wm/DisplayFrames;->mHeight:I

    sub-int/2addr v2, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mScreenHeight:I

    .line 231
    invoke-direct {p0, v1, p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->notifyEdgeSuppressionChanged(ILjava/lang/String;)V

    .line 232
    return-void
.end method

.method public registerSensors()V
    .locals 2

    .line 254
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorWrapper:Lmiui/util/HoldSensorWrapper;

    if-nez v0, :cond_0

    .line 255
    new-instance v0, Lmiui/util/HoldSensorWrapper;

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiui/util/HoldSensorWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorWrapper:Lmiui/util/HoldSensorWrapper;

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorWrapper:Lmiui/util/LaySensorWrapper;

    if-nez v0, :cond_1

    .line 258
    new-instance v0, Lmiui/util/LaySensorWrapper;

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lmiui/util/LaySensorWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorWrapper:Lmiui/util/LaySensorWrapper;

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorWrapper:Lmiui/util/HoldSensorWrapper;

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldListener:Lmiui/util/HoldSensorWrapper$HoldSensorChangeListener;

    invoke-virtual {v0, v1}, Lmiui/util/HoldSensorWrapper;->registerListener(Lmiui/util/HoldSensorWrapper$HoldSensorChangeListener;)V

    .line 261
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorWrapper:Lmiui/util/LaySensorWrapper;

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLayListener:Lmiui/util/LaySensorWrapper$LaySensorChangeListener;

    invoke-virtual {v0, v1}, Lmiui/util/LaySensorWrapper;->registerListener(Lmiui/util/LaySensorWrapper$LaySensorChangeListener;)V

    .line 262
    return-void
.end method

.method public unRegisterSensors()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorWrapper:Lmiui/util/HoldSensorWrapper;

    const/4 v1, -0x1

    if-eqz v0, :cond_0

    .line 266
    iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorState:I

    .line 267
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mHoldSensorWrapper:Lmiui/util/HoldSensorWrapper;

    invoke-virtual {v0}, Lmiui/util/HoldSensorWrapper;->unregisterAllListener()V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorWrapper:Lmiui/util/LaySensorWrapper;

    if-eqz v0, :cond_1

    .line 270
    iput v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorState:I

    .line 271
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mLaySensorWrapper:Lmiui/util/LaySensorWrapper;

    invoke-virtual {v0}, Lmiui/util/LaySensorWrapper;->unregisterAllListener()V

    .line 273
    :cond_1
    return-void
.end method
