.class public Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
.super Ljava/lang/Object;
.source "EdgeSuppressionInfo.java"


# instance fields
.field private mAbsoluteSize:I

.field private mConditionSize:I

.field private mConnerHeight:I

.field private mConnerWidth:I

.field private mIsHorizontal:Z

.field private mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIIIZLjava/lang/String;)V
    .locals 0
    .param p1, "absoluteSize"    # I
    .param p2, "conditionSize"    # I
    .param p3, "connerWidth"    # I
    .param p4, "connerHeight"    # I
    .param p5, "isHorizontal"    # Z
    .param p6, "type"    # Ljava/lang/String;

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I

    .line 15
    iput p2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I

    .line 16
    iput p3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I

    .line 17
    iput p4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I

    .line 18
    iput-boolean p5, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z

    .line 19
    iput-object p6, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mType:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getAbsoluteSize()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I

    return v0
.end method

.method public getConditionSize()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I

    return v0
.end method

.method public getConnerHeight()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I

    return v0
.end method

.method public getConnerWidth()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public isHorizontal()Z
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z

    return v0
.end method

.method public setAbsoluteSize(I)V
    .locals 0
    .param p1, "absoluteSize"    # I

    .line 27
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I

    .line 28
    return-void
.end method

.method public setConditionSize(I)V
    .locals 0
    .param p1, "conditionSize"    # I

    .line 35
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I

    .line 36
    return-void
.end method

.method public setConnerHeight(I)V
    .locals 0
    .param p1, "connerHeight"    # I

    .line 51
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I

    .line 52
    return-void
.end method

.method public setConnerWidth(I)V
    .locals 0
    .param p1, "connerWidth"    # I

    .line 43
    iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I

    .line 44
    return-void
.end method

.method public setIsHorizontal(Z)V
    .locals 0
    .param p1, "isHorizontal"    # Z

    .line 59
    iput-boolean p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z

    .line 60
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .line 67
    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mType:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EdgeSuppressionInfo{mAbsoluteSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mConditionSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mConnerWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mConnerHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsHorizontal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
