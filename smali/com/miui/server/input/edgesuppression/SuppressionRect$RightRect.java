public class com.miui.server.input.edgesuppression.SuppressionRect$RightRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "RightRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$RightRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "heightOfScreen" # I */
/* .param p3, "widthOfScreen" # I */
/* .param p4, "widthOfRect" # I */
/* .line 284 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 285 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;->setType(I)V
/* .line 286 */
int v0 = 3; // const/4 v0, 0x3
(( com.miui.server.input.edgesuppression.SuppressionRect$RightRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;->setPosition(I)V
/* .line 287 */
/* sub-int v0, p3, p4 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;->setTopLeftX(I)V
/* .line 288 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$RightRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;->setTopLeftY(I)V
/* .line 289 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;->setBottomRightX(I)V
/* .line 290 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightRect ) p0 ).setBottomRightY ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;->setBottomRightY(I)V
/* .line 291 */
return;
} // .end method
