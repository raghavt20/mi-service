public class com.miui.server.input.edgesuppression.SuppressionRect$BottomRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "BottomRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ( ) {
/* .locals 1 */
/* .param p1, "isHorizontal" # Z */
/* .param p2, "type" # I */
/* .param p3, "heightOfScreen" # I */
/* .param p4, "widthOfScreen" # I */
/* .param p5, "widthOfRect" # I */
/* .line 255 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 256 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setType ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setType(I)V
/* .line 257 */
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setPosition(I)V
/* .line 258 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 259 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftX(I)V
	 /* .line 260 */
	 /* sub-int v0, p3, p5 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftY(I)V
	 /* .line 261 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setBottomRightX ( p4 ); // invoke-virtual {p0, p4}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightX(I)V
	 /* .line 262 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setBottomRightY ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightY(I)V
	 /* .line 264 */
} // :cond_0
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftX(I)V
/* .line 265 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftY(I)V
/* .line 266 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setBottomRightX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightX(I)V
/* .line 267 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRect ) p0 ).setBottomRightY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightY(I)V
/* .line 269 */
} // :goto_0
return;
} // .end method
