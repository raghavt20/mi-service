.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RightTopHalfRect"
.end annotation


# direct methods
.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "heightOfScreen"    # I
    .param p3, "widthOfScreen"    # I
    .param p4, "widthOfRect"    # I

    .line 156
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 157
    invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setType(I)V

    .line 158
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setPosition(I)V

    .line 159
    sub-int v0, p3, p4

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setTopLeftX(I)V

    .line 160
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setTopLeftY(I)V

    .line 161
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setBottomRightX(I)V

    .line 162
    div-int/lit8 v0, p2, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setBottomRightY(I)V

    .line 163
    return-void
.end method
