.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BottomRect"
.end annotation


# direct methods
.method public constructor <init>(ZIIII)V
    .locals 1
    .param p1, "isHorizontal"    # Z
    .param p2, "type"    # I
    .param p3, "heightOfScreen"    # I
    .param p4, "widthOfScreen"    # I
    .param p5, "widthOfRect"    # I

    .line 255
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 256
    invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setType(I)V

    .line 257
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setPosition(I)V

    .line 258
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 259
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftX(I)V

    .line 260
    sub-int v0, p3, p5

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftY(I)V

    .line 261
    invoke-virtual {p0, p4}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightX(I)V

    .line 262
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightY(I)V

    goto :goto_0

    .line 264
    :cond_0
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftX(I)V

    .line 265
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setTopLeftY(I)V

    .line 266
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightX(I)V

    .line 267
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;->setBottomRightY(I)V

    .line 269
    :goto_0
    return-void
.end method
