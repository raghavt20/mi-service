public class com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "LeftTopHalfRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "heightOfScreen" # I */
/* .param p3, "widthOfRect" # I */
/* .line 145 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 146 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setType(I)V
/* .line 147 */
int v0 = 2; // const/4 v0, 0x2
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setPosition(I)V
/* .line 148 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setTopLeftX(I)V
/* .line 149 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setTopLeftY(I)V
/* .line 150 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setBottomRightX(I)V
/* .line 151 */
/* div-int/lit8 v0, p2, 0x2 */
(( com.miui.server.input.edgesuppression.SuppressionRect$LeftTopHalfRect ) p0 ).setBottomRightY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setBottomRightY(I)V
/* .line 152 */
return;
} // .end method
