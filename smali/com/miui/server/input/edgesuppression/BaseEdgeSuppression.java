public abstract class com.miui.server.input.edgesuppression.BaseEdgeSuppression {
	 /* .source "BaseEdgeSuppression.java" */
	 /* # static fields */
	 protected static final Integer ABSOLUTE;
	 protected static final Integer CONDITION;
	 private static final Integer CORNER;
	 protected static final Integer EMPTY_POINT;
	 protected static final Integer RECT_FIRST;
	 protected static final Integer RECT_FOURTH;
	 protected static final Integer RECT_SECOND;
	 protected static final Integer RECT_THIRD;
	 protected static final java.lang.String TAG;
	 /* # instance fields */
	 protected Integer mAbsoluteSize;
	 protected Integer mConditionSize;
	 protected Integer mConnerHeight;
	 protected Integer mConnerWidth;
	 protected Integer mHoldSensorState;
	 protected Boolean mIsHorizontal;
	 protected Integer mRotation;
	 protected Integer mScreenHeight;
	 protected Integer mScreenWidth;
	 protected java.util.ArrayList mSendList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
protected Integer mTargetId;
/* # direct methods */
public com.miui.server.input.edgesuppression.BaseEdgeSuppression ( ) {
/* .locals 1 */
/* .line 14 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 25 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mSendList = v0;
/* .line 27 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConditionSize:I */
/* .line 28 */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerWidth:I */
/* .line 29 */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I */
return;
} // .end method
private Boolean hasParamChanged ( com.miui.server.input.edgesuppression.EdgeSuppressionInfo p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "info" # Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .param p2, "widthOfScreen" # I */
/* .param p3, "heightOfScreen" # I */
/* .line 42 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConditionSize:I */
v1 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getConditionSize ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConditionSize()I
/* if-ne v0, v1, :cond_1 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerWidth:I */
v1 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getConnerWidth ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerWidth()I
/* if-ne v0, v1, :cond_1 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I */
/* .line 43 */
v1 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getConnerHeight ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerHeight()I
/* if-ne v0, v1, :cond_1 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mScreenWidth:I */
/* if-ne v0, p2, :cond_1 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I */
/* if-eq v0, p3, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
/* .line 42 */
} // :goto_1
} // .end method
/* # virtual methods */
java.util.ArrayList getCornerData ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 18 */
/* .param p1, "rotation" # I */
/* .param p2, "widthOfScreen" # I */
/* .param p3, "heightOfScreen" # I */
/* .param p4, "connerWidth" # I */
/* .param p5, "connerHeight" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(IIIII)", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/input/edgesuppression/SuppressionRect;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 73 */
/* new-instance v0, Ljava/util/ArrayList; */
int v1 = 4; // const/4 v1, 0x4
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
/* move-object v8, v0 */
/* .line 74 */
/* .local v8, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;" */
/* const/16 v17, 0x0 */
/* .line 75 */
/* .local v17, "type":I */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* if-ge v0, v1, :cond_0 */
/* .line 76 */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
/* invoke-direct {v2}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
(( java.util.ArrayList ) v8 ).add ( v2 ); // invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 75 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 78 */
} // .end local v0 # "i":I
} // :cond_0
int v7 = 3; // const/4 v7, 0x3
int v6 = 2; // const/4 v6, 0x2
int v0 = 0; // const/4 v0, 0x0
int v5 = 1; // const/4 v5, 0x1
/* packed-switch p1, :pswitch_data_0 */
/* goto/16 :goto_1 */
/* .line 111 */
/* :pswitch_0 */
(( java.util.ArrayList ) v8 ).get ( v0 ); // invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* const/16 v16, 0x0 */
/* move-object/from16 v9, p0 */
/* move/from16 v11, v17 */
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 113 */
(( java.util.ArrayList ) v8 ).get ( v5 ); // invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 1; // const/4 v3, 0x1
/* sub-int v4, p2, p4 */
int v5 = 0; // const/4 v5, 0x0
/* move-object/from16 v0, p0 */
/* move/from16 v2, v17 */
/* move v15, v6 */
/* move/from16 v6, p2 */
/* move v14, v7 */
/* move/from16 v7, p5 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 115 */
(( java.util.ArrayList ) v8 ).get ( v15 ); // invoke-virtual {v8, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 2; // const/4 v12, 0x2
int v0 = 0; // const/4 v0, 0x0
int v15 = 0; // const/4 v15, 0x0
/* move v7, v14 */
/* move v14, v0 */
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 117 */
(( java.util.ArrayList ) v8 ).get ( v7 ); // invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 3; // const/4 v3, 0x3
/* sub-int v4, p2, p4 */
/* sub-int v5, p3, p5 */
/* move-object/from16 v0, p0 */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 120 */
/* goto/16 :goto_1 */
/* .line 101 */
/* :pswitch_1 */
/* move v15, v6 */
(( java.util.ArrayList ) v8 ).get ( v0 ); // invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v6 = 0; // const/4 v6, 0x0
/* move-object/from16 v0, p0 */
/* move/from16 v2, v17 */
/* move v14, v5 */
/* move v5, v6 */
/* move/from16 v6, p4 */
/* move v13, v7 */
/* move/from16 v7, p5 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 103 */
(( java.util.ArrayList ) v8 ).get ( v14 ); // invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 1; // const/4 v3, 0x1
/* sub-int v4, p2, p4 */
int v5 = 0; // const/4 v5, 0x0
/* move-object/from16 v0, p0 */
/* move/from16 v6, p2 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 105 */
(( java.util.ArrayList ) v8 ).get ( v15 ); // invoke-virtual {v8, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 2; // const/4 v12, 0x2
int v0 = 0; // const/4 v0, 0x0
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* const/16 v16, 0x0 */
/* move-object/from16 v9, p0 */
/* move/from16 v11, v17 */
/* move v7, v13 */
/* move v13, v0 */
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 107 */
(( java.util.ArrayList ) v8 ).get ( v7 ); // invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 3; // const/4 v12, 0x3
int v13 = 0; // const/4 v13, 0x0
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 109 */
/* goto/16 :goto_1 */
/* .line 91 */
/* :pswitch_2 */
/* move v14, v5 */
/* move v15, v6 */
(( java.util.ArrayList ) v8 ).get ( v0 ); // invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object/from16 v0, p0 */
/* move/from16 v2, v17 */
/* move/from16 v6, p4 */
/* move v13, v7 */
/* move/from16 v7, p5 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 93 */
(( java.util.ArrayList ) v8 ).get ( v14 ); // invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 1; // const/4 v12, 0x1
int v0 = 0; // const/4 v0, 0x0
int v14 = 0; // const/4 v14, 0x0
int v1 = 0; // const/4 v1, 0x0
/* const/16 v16, 0x0 */
/* move-object/from16 v9, p0 */
/* move/from16 v11, v17 */
/* move v7, v13 */
/* move v13, v0 */
/* move v2, v15 */
/* move v15, v1 */
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 95 */
(( java.util.ArrayList ) v8 ).get ( v2 ); // invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 2; // const/4 v3, 0x2
/* sub-int v5, p3, p5 */
/* move-object/from16 v0, p0 */
/* move/from16 v2, v17 */
/* move v15, v7 */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 97 */
(( java.util.ArrayList ) v8 ).get ( v15 ); // invoke-virtual {v8, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 3; // const/4 v12, 0x3
int v13 = 0; // const/4 v13, 0x0
int v15 = 0; // const/4 v15, 0x0
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 99 */
/* .line 80 */
/* :pswitch_3 */
/* move v14, v5 */
/* move v2, v6 */
/* move v15, v7 */
(( java.util.ArrayList ) v8 ).get ( v0 ); // invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 0; // const/4 v12, 0x0
int v13 = 0; // const/4 v13, 0x0
int v0 = 0; // const/4 v0, 0x0
int v1 = 0; // const/4 v1, 0x0
/* const/16 v16, 0x0 */
/* move-object/from16 v9, p0 */
/* move/from16 v11, v17 */
/* move v3, v14 */
/* move v14, v0 */
/* move v15, v1 */
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 82 */
(( java.util.ArrayList ) v8 ).get ( v3 ); // invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v10, v0 */
/* check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v12 = 1; // const/4 v12, 0x1
int v14 = 0; // const/4 v14, 0x0
int v15 = 0; // const/4 v15, 0x0
/* invoke-virtual/range {v9 ..v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 84 */
(( java.util.ArrayList ) v8 ).get ( v2 ); // invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
/* sub-int v5, p3, p5 */
/* move-object/from16 v0, p0 */
/* move/from16 v2, v17 */
/* move/from16 v6, p4 */
/* move v9, v7 */
/* move/from16 v7, p3 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 86 */
(( java.util.ArrayList ) v8 ).get ( v9 ); // invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v1, v0 */
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v3 = 3; // const/4 v3, 0x3
/* sub-int v4, p2, p4 */
/* sub-int v5, p3, p5 */
/* move-object/from16 v0, p0 */
/* move/from16 v6, p2 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 89 */
/* nop */
/* .line 124 */
} // :goto_1
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
abstract java.util.ArrayList getEdgeSuppressionData ( Integer p0, Integer p1, Integer p2 ) {
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(III)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end method
void initInputMethodData ( android.content.Context p0, Integer p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "size" # I */
/* .line 133 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "vertical_edge_suppression_size" */
int v2 = -1; // const/4 v2, -0x1
v0 = android.provider.Settings$Global .getInt ( v0,v1,v2 );
/* .line 135 */
/* .local v0, "verticalSize":I */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v4 = "horizontal_edge_suppression_size"; // const-string v4, "horizontal_edge_suppression_size"
v3 = android.provider.Settings$Global .getInt ( v3,v4,v2 );
/* .line 138 */
/* .local v3, "horizontalSize":I */
/* if-ne v0, v2, :cond_0 */
/* if-ne v3, v2, :cond_0 */
/* sget-boolean v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* sget-boolean v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->SHOULD_REMOVE_EDGE_SETTINGS:Z */
/* if-nez v2, :cond_0 */
/* .line 141 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putInt ( v2,v1,p2 );
/* .line 143 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$Global .putInt ( v1,v4,p2 );
/* .line 146 */
} // :cond_0
return;
} // .end method
void setInputMethodSize ( android.content.Context p0, Integer p1, Integer p2, Boolean p3 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "minInputMethodSize" # I */
/* .param p3, "maxInputMethodSize" # I */
/* .param p4, "isVertical" # Z */
/* .line 151 */
int v0 = 1; // const/4 v0, 0x1
/* .line 152 */
/* .local v0, "shouldOverWrite":Z */
/* const-string/jumbo v1, "vertical_edge_suppression_size" */
final String v2 = "horizontal_edge_suppression_size"; // const-string v2, "horizontal_edge_suppression_size"
int v3 = -1; // const/4 v3, -0x1
if ( p4 != null) { // if-eqz p4, :cond_0
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v4 = android.provider.Settings$Global .getInt ( v4,v1,v3 );
/* .line 153 */
} // :cond_0
/* nop */
/* .line 154 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 153 */
v4 = android.provider.Settings$Global .getInt ( v4,v2,v3 );
} // :goto_0
/* nop */
/* .line 157 */
/* .local v4, "result":I */
/* sget-boolean v5, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* sget-boolean v5, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->SHOULD_REMOVE_EDGE_SETTINGS:Z */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 165 */
} // :cond_1
/* if-le v4, p3, :cond_2 */
/* .line 166 */
/* move v4, p3 */
/* .line 167 */
} // :cond_2
/* if-ge v4, p2, :cond_3 */
/* .line 168 */
/* move v4, p2 */
/* .line 170 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
/* .line 159 */
} // :cond_4
} // :goto_1
/* if-eq v4, v3, :cond_5 */
/* .line 160 */
int v4 = -1; // const/4 v4, -0x1
/* .line 162 */
} // :cond_5
int v0 = 0; // const/4 v0, 0x0
/* .line 174 */
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 175 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 176 */
if ( p4 != null) { // if-eqz p4, :cond_6
/* .line 177 */
} // :cond_6
/* move-object v1, v2 */
/* .line 175 */
} // :goto_3
android.provider.Settings$Global .putInt ( v3,v1,v4 );
/* .line 179 */
} // :cond_7
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "InputMethod ShrinkSize shouldOverWrite = "; // const-string v2, "InputMethod ShrinkSize shouldOverWrite = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = " result = "; // const-string v2, " result = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " isVertical = "; // const-string v2, " isVertical = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p4 ); // invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "EdgeSuppressionManager"; // const-string v2, "EdgeSuppressionManager"
android.util.Slog .i ( v2,v1 );
/* .line 181 */
return;
} // .end method
void setRectValue ( com.miui.server.input.edgesuppression.SuppressionRect p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5, Integer p6 ) {
/* .locals 0 */
/* .param p1, "suppressionRect" # Lcom/miui/server/input/edgesuppression/SuppressionRect; */
/* .param p2, "type" # I */
/* .param p3, "position" # I */
/* .param p4, "startX" # I */
/* .param p5, "startY" # I */
/* .param p6, "endX" # I */
/* .param p7, "endY" # I */
/* .line 129 */
/* invoke-virtual/range {p1 ..p7}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setValue(IIIIII)V */
/* .line 130 */
return;
} // .end method
void syncDataToKernel ( ) {
/* .locals 5 */
/* .line 66 */
v0 = this.mSendList;
(( java.util.ArrayList ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;
final String v1 = "EdgeSuppressionManager"; // const-string v1, "EdgeSuppressionManager"
android.util.Slog .i ( v1,v0 );
/* .line 67 */
miui.util.ITouchFeature .getInstance ( );
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mTargetId:I */
v2 = this.mSendList;
/* .line 68 */
v3 = (( java.util.ArrayList ) v2 ).size ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->size()I
/* .line 67 */
/* const/16 v4, 0xf */
(( miui.util.ITouchFeature ) v0 ).setEdgeMode ( v1, v4, v2, v3 ); // invoke-virtual {v0, v1, v4, v2, v3}, Lmiui/util/ITouchFeature;->setEdgeMode(IILjava/util/ArrayList;I)Z
/* .line 69 */
return;
} // .end method
void updateInterNalParam ( com.miui.server.input.edgesuppression.EdgeSuppressionInfo p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) {
/* .locals 1 */
/* .param p1, "info" # Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo; */
/* .param p2, "holdSensorState" # I */
/* .param p3, "rotation" # I */
/* .param p4, "targetId" # I */
/* .param p5, "widthOfScreen" # I */
/* .param p6, "heightOfScreen" # I */
/* .line 50 */
/* iput p3, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mRotation:I */
/* .line 51 */
/* iput p4, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mTargetId:I */
/* .line 52 */
/* iput p2, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mHoldSensorState:I */
/* .line 53 */
v0 = /* invoke-direct {p0, p1, p5, p6}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->hasParamChanged(Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;II)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 54 */
v0 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getConditionSize ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConditionSize()I
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConditionSize:I */
/* .line 55 */
v0 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getAbsoluteSize ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getAbsoluteSize()I
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mAbsoluteSize:I */
/* .line 56 */
v0 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getConnerWidth ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerWidth()I
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerWidth:I */
/* .line 57 */
v0 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).getConnerHeight ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerHeight()I
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I */
/* .line 58 */
v0 = (( com.miui.server.input.edgesuppression.EdgeSuppressionInfo ) p1 ).isHorizontal ( ); // invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->isHorizontal()Z
/* iput-boolean v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mIsHorizontal:Z */
/* .line 59 */
/* iput p5, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mScreenWidth:I */
/* .line 60 */
/* iput p6, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mScreenHeight:I */
/* .line 61 */
(( com.miui.server.input.edgesuppression.BaseEdgeSuppression ) p0 ).getEdgeSuppressionData ( p3, p5, p6 ); // invoke-virtual {p0, p3, p5, p6}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->getEdgeSuppressionData(III)Ljava/util/ArrayList;
this.mSendList = v0;
/* .line 63 */
} // :cond_0
return;
} // .end method
