.class public Lcom/miui/server/input/edgesuppression/NormalHelper;
.super Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;
.source "NormalHelper.java"


# static fields
.field private static volatile mInstance:Lcom/miui/server/input/edgesuppression/NormalHelper;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;-><init>()V

    .line 8
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/edgesuppression/NormalHelper;
    .locals 2

    .line 11
    sget-object v0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mInstance:Lcom/miui/server/input/edgesuppression/NormalHelper;

    if-nez v0, :cond_1

    .line 12
    const-class v0, Lcom/miui/server/input/edgesuppression/NormalHelper;

    monitor-enter v0

    .line 13
    :try_start_0
    sget-object v1, Lcom/miui/server/input/edgesuppression/NormalHelper;->mInstance:Lcom/miui/server/input/edgesuppression/NormalHelper;

    if-nez v1, :cond_0

    .line 14
    new-instance v1, Lcom/miui/server/input/edgesuppression/NormalHelper;

    invoke-direct {v1}, Lcom/miui/server/input/edgesuppression/NormalHelper;-><init>()V

    sput-object v1, Lcom/miui/server/input/edgesuppression/NormalHelper;->mInstance:Lcom/miui/server/input/edgesuppression/NormalHelper;

    .line 16
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 18
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mInstance:Lcom/miui/server/input/edgesuppression/NormalHelper;

    return-object v0
.end method


# virtual methods
.method public getEdgeSuppressionData(III)Ljava/util/ArrayList;
    .locals 8
    .param p1, "rotation"    # I
    .param p2, "widthOfScreen"    # I
    .param p3, "heightOfScreen"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 25
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;"
    const/4 v1, 0x2

    iget v2, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mAbsoluteSize:I

    invoke-virtual {p0, v1, v2, p2, p3}, Lcom/miui/server/input/edgesuppression/NormalHelper;->getEdgeSuppressionRectData(IIII)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 27
    const/4 v1, 0x1

    iget v2, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mConditionSize:I

    invoke-virtual {p0, v1, v2, p2, p3}, Lcom/miui/server/input/edgesuppression/NormalHelper;->getEdgeSuppressionRectData(IIII)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 29
    iget v6, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mConnerWidth:I

    iget v7, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mConnerHeight:I

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v2 .. v7}, Lcom/miui/server/input/edgesuppression/NormalHelper;->getCornerData(IIIII)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 30
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mSendList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 31
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    .line 32
    .local v2, "target":Lcom/miui/server/input/edgesuppression/SuppressionRect;
    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mSendList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->getList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 33
    .end local v2    # "target":Lcom/miui/server/input/edgesuppression/SuppressionRect;
    goto :goto_0

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mSendList:Ljava/util/ArrayList;

    return-object v1
.end method

.method public getEdgeSuppressionRectData(IIII)Ljava/util/ArrayList;
    .locals 9
    .param p1, "type"    # I
    .param p2, "size"    # I
    .param p3, "widthOfScreen"    # I
    .param p4, "heightOfScreen"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/input/edgesuppression/SuppressionRect;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 41
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;"
    new-instance v1, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;

    iget-boolean v2, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mIsHorizontal:Z

    invoke-direct {v1, v2, p1, p3, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;-><init>(ZIII)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v1, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;

    iget-boolean v4, p0, Lcom/miui/server/input/edgesuppression/NormalHelper;->mIsHorizontal:Z

    move-object v3, v1

    move v5, p1

    move v6, p4

    move v7, p3

    move v8, p2

    invoke-direct/range {v3 .. v8}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;-><init>(ZIIII)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v1, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;

    invoke-direct {v1, p1, p4, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;-><init>(III)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    new-instance v1, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;

    invoke-direct {v1, p1, p4, p3, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;-><init>(IIII)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v0
.end method
