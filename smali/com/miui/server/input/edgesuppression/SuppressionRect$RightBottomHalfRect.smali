.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RightBottomHalfRect"
.end annotation


# direct methods
.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "heightOfScreen"    # I
    .param p3, "widthOfScreen"    # I
    .param p4, "widthOfRect"    # I

    .line 179
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 180
    invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setType(I)V

    .line 181
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setPosition(I)V

    .line 182
    sub-int v0, p3, p4

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setTopLeftX(I)V

    .line 183
    div-int/lit8 v0, p2, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setTopLeftY(I)V

    .line 184
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setBottomRightX(I)V

    .line 185
    invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;->setBottomRightY(I)V

    .line 186
    return-void
.end method
