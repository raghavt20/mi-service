public class com.miui.server.input.edgesuppression.EdgeSuppressionInfo {
	 /* .source "EdgeSuppressionInfo.java" */
	 /* # instance fields */
	 private Integer mAbsoluteSize;
	 private Integer mConditionSize;
	 private Integer mConnerHeight;
	 private Integer mConnerWidth;
	 private Boolean mIsHorizontal;
	 private java.lang.String mType;
	 /* # direct methods */
	 public com.miui.server.input.edgesuppression.EdgeSuppressionInfo ( ) {
		 /* .locals 0 */
		 /* .param p1, "absoluteSize" # I */
		 /* .param p2, "conditionSize" # I */
		 /* .param p3, "connerWidth" # I */
		 /* .param p4, "connerHeight" # I */
		 /* .param p5, "isHorizontal" # Z */
		 /* .param p6, "type" # Ljava/lang/String; */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 14 */
		 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I */
		 /* .line 15 */
		 /* iput p2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I */
		 /* .line 16 */
		 /* iput p3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I */
		 /* .line 17 */
		 /* iput p4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I */
		 /* .line 18 */
		 /* iput-boolean p5, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z */
		 /* .line 19 */
		 this.mType = p6;
		 /* .line 20 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getAbsoluteSize ( ) {
		 /* .locals 1 */
		 /* .line 23 */
		 /* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I */
	 } // .end method
	 public Integer getConditionSize ( ) {
		 /* .locals 1 */
		 /* .line 31 */
		 /* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I */
	 } // .end method
	 public Integer getConnerHeight ( ) {
		 /* .locals 1 */
		 /* .line 47 */
		 /* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I */
	 } // .end method
	 public Integer getConnerWidth ( ) {
		 /* .locals 1 */
		 /* .line 39 */
		 /* iget v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I */
	 } // .end method
	 public java.lang.String getType ( ) {
		 /* .locals 1 */
		 /* .line 63 */
		 v0 = this.mType;
	 } // .end method
	 public Boolean isHorizontal ( ) {
		 /* .locals 1 */
		 /* .line 55 */
		 /* iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z */
	 } // .end method
	 public void setAbsoluteSize ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "absoluteSize" # I */
		 /* .line 27 */
		 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I */
		 /* .line 28 */
		 return;
	 } // .end method
	 public void setConditionSize ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "conditionSize" # I */
		 /* .line 35 */
		 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I */
		 /* .line 36 */
		 return;
	 } // .end method
	 public void setConnerHeight ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "connerHeight" # I */
		 /* .line 51 */
		 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I */
		 /* .line 52 */
		 return;
	 } // .end method
	 public void setConnerWidth ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "connerWidth" # I */
		 /* .line 43 */
		 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I */
		 /* .line 44 */
		 return;
	 } // .end method
	 public void setIsHorizontal ( Boolean p0 ) {
		 /* .locals 0 */
		 /* .param p1, "isHorizontal" # Z */
		 /* .line 59 */
		 /* iput-boolean p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z */
		 /* .line 60 */
		 return;
	 } // .end method
	 public void setType ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "type" # Ljava/lang/String; */
		 /* .line 67 */
		 this.mType = p1;
		 /* .line 68 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 2 */
		 /* .line 72 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v1 = "EdgeSuppressionInfo{mAbsoluteSize="; // const-string v1, "EdgeSuppressionInfo{mAbsoluteSize="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mAbsoluteSize:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mConditionSize="; // const-string v1, ", mConditionSize="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConditionSize:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mConnerWidth="; // const-string v1, ", mConnerWidth="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerWidth:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mConnerHeight="; // const-string v1, ", mConnerHeight="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mConnerHeight:I */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v1 = ", mIsHorizontal="; // const-string v1, ", mIsHorizontal="
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-boolean v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->mIsHorizontal:Z */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
		 final String v1 = ", mType=\'"; // const-string v1, ", mType=\'"
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v1 = this.mType;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x27 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 /* const/16 v1, 0x7d */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
