class com.miui.server.input.edgesuppression.EdgeSuppressionManager$EdgeSuppressionHandler extends android.os.Handler {
	 /* .source "EdgeSuppressionManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "EdgeSuppressionHandler" */
} // .end annotation
/* # static fields */
public static final java.lang.String MSG_DATA_REASON;
public static final Integer MSG_SEND_DATA;
/* # instance fields */
final com.miui.server.input.edgesuppression.EdgeSuppressionManager this$0; //synthetic
/* # direct methods */
public com.miui.server.input.edgesuppression.EdgeSuppressionManager$EdgeSuppressionHandler ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 430 */
this.this$0 = p1;
/* .line 431 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 432 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 435 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v1 = "reason"; // const-string v1, "reason"
/* const-string/jumbo v2, "unKnow" */
(( android.os.Bundle ) v0 ).getString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 436 */
/* .local v0, "reason":Ljava/lang/String; */
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
final String v1 = "laySensor"; // const-string v1, "laySensor"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 2; // const/4 v1, 0x2
/* :sswitch_1 */
final String v1 = "configuration"; // const-string v1, "configuration"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 int v1 = 6; // const/4 v1, 0x6
	 /* :sswitch_2 */
	 /* const-string/jumbo v1, "settings" */
	 v1 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v1 = 4; // const/4 v1, 0x4
		 /* :sswitch_3 */
		 final String v1 = "rotation"; // const-string v1, "rotation"
		 v1 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 int v1 = 0; // const/4 v1, 0x0
			 /* :sswitch_4 */
			 final String v1 = "gameBooster"; // const-string v1, "gameBooster"
			 v1 = 			 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v1 != null) { // if-eqz v1, :cond_0
				 int v1 = 5; // const/4 v1, 0x5
				 /* :sswitch_5 */
				 final String v1 = "screenOn"; // const-string v1, "screenOn"
				 v1 = 				 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v1 != null) { // if-eqz v1, :cond_0
					 int v1 = 3; // const/4 v1, 0x3
					 /* :sswitch_6 */
					 final String v1 = "holdSensor"; // const-string v1, "holdSensor"
					 v1 = 					 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v1 != null) { // if-eqz v1, :cond_0
						 int v1 = 1; // const/4 v1, 0x1
					 } // :goto_0
					 int v1 = -1; // const/4 v1, -0x1
				 } // :goto_1
				 final String v2 = "EdgeSuppressionManager"; // const-string v2, "EdgeSuppressionManager"
				 /* packed-switch v1, :pswitch_data_0 */
				 /* goto/16 :goto_2 */
				 /* .line 464 */
				 /* :pswitch_0 */
				 final String v1 = "Send EdgeSuppression data because configuration"; // const-string v1, "Send EdgeSuppression data because configuration"
				 android.util.Slog .i ( v2,v1 );
				 /* .line 465 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 466 */
				 /* .line 460 */
				 /* :pswitch_1 */
				 final String v1 = "Send EdgeSuppression data because Game Booster"; // const-string v1, "Send EdgeSuppression data because Game Booster"
				 android.util.Slog .i ( v2,v1 );
				 /* .line 461 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 462 */
				 /* .line 456 */
				 /* :pswitch_2 */
				 final String v1 = "Send EdgeSuppression data because User Settings"; // const-string v1, "Send EdgeSuppression data because User Settings"
				 android.util.Slog .i ( v2,v1 );
				 /* .line 457 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 458 */
				 /* .line 452 */
				 /* :pswitch_3 */
				 final String v1 = "Send EdgeSuppression data because screen turn on"; // const-string v1, "Send EdgeSuppression data because screen turn on"
				 android.util.Slog .i ( v2,v1 );
				 /* .line 453 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 454 */
				 /* .line 447 */
				 /* :pswitch_4 */
				 /* new-instance v1, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "Send EdgeSuppression data because laySensor,status = "; // const-string v3, "Send EdgeSuppression data because laySensor,status = "
				 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v3 = this.this$0;
				 v3 = 				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmLaySensorState ( v3 );
				 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .i ( v2,v1 );
				 /* .line 449 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 450 */
				 /* .line 442 */
				 /* :pswitch_5 */
				 /* new-instance v1, Ljava/lang/StringBuilder; */
				 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
				 final String v3 = "Send EdgeSuppression data because holdSensor,status = "; // const-string v3, "Send EdgeSuppression data because holdSensor,status = "
				 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
				 v3 = this.this$0;
				 v3 = 				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$fgetmHoldSensorState ( v3 );
				 (( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
				 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
				 android.util.Slog .i ( v2,v1 );
				 /* .line 444 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 445 */
				 /* .line 438 */
				 /* :pswitch_6 */
				 final String v1 = "Send EdgeSuppression data because rotation"; // const-string v1, "Send EdgeSuppression data because rotation"
				 android.util.Slog .i ( v2,v1 );
				 /* .line 439 */
				 v1 = this.this$0;
				 com.miui.server.input.edgesuppression.EdgeSuppressionManager .-$$Nest$mhandleEdgeSuppressionData ( v1 );
				 /* .line 440 */
				 /* nop */
				 /* .line 470 */
			 } // :goto_2
			 /* invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V */
			 /* .line 471 */
			 return;
			 /* nop */
			 /* :sswitch_data_0 */
			 /* .sparse-switch */
			 /* -0x682a96a7 -> :sswitch_6 */
			 /* -0x1888a095 -> :sswitch_5 */
			 /* -0x416d4c2 -> :sswitch_4 */
			 /* -0x266f082 -> :sswitch_3 */
			 /* 0x5582bc23 -> :sswitch_2 */
			 /* 0x733374f6 -> :sswitch_1 */
			 /* 0x7d68167e -> :sswitch_0 */
		 } // .end sparse-switch
		 /* :pswitch_data_0 */
		 /* .packed-switch 0x0 */
		 /* :pswitch_6 */
		 /* :pswitch_5 */
		 /* :pswitch_4 */
		 /* :pswitch_3 */
		 /* :pswitch_2 */
		 /* :pswitch_1 */
		 /* :pswitch_0 */
	 } // .end packed-switch
} // .end method
