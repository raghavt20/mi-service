.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LeftTopHalfRect"
.end annotation


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "heightOfScreen"    # I
    .param p3, "widthOfRect"    # I

    .line 145
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 146
    invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setType(I)V

    .line 147
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setPosition(I)V

    .line 148
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setTopLeftX(I)V

    .line 149
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setTopLeftY(I)V

    .line 150
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setBottomRightX(I)V

    .line 151
    div-int/lit8 v0, p2, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;->setBottomRightY(I)V

    .line 152
    return-void
.end method
