public class com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "BottomRightHalfRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "heightOfScreen" # I */
/* .param p3, "widthOfScreen" # I */
/* .param p4, "widthOfRect" # I */
/* .line 225 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 226 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setType(I)V
/* .line 227 */
int v0 = 1; // const/4 v0, 0x1
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setPosition(I)V
/* .line 228 */
/* div-int/lit8 v0, p3, 0x2 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setTopLeftX(I)V
/* .line 229 */
/* sub-int v0, p2, p4 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setTopLeftY(I)V
/* .line 230 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setBottomRightX(I)V
/* .line 231 */
(( com.miui.server.input.edgesuppression.SuppressionRect$BottomRightHalfRect ) p0 ).setBottomRightY ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setBottomRightY(I)V
/* .line 232 */
return;
} // .end method
