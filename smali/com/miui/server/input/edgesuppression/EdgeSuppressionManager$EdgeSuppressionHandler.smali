.class Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;
.super Landroid/os/Handler;
.source "EdgeSuppressionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EdgeSuppressionHandler"
.end annotation


# static fields
.field public static final MSG_DATA_REASON:Ljava/lang/String; = "reason"

.field public static final MSG_SEND_DATA:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 430
    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 431
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 432
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 435
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "reason"

    const-string/jumbo v2, "unKnow"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 436
    .local v0, "reason":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v1, "laySensor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_1
    const-string v1, "configuration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x6

    goto :goto_1

    :sswitch_2
    const-string/jumbo v1, "settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    goto :goto_1

    :sswitch_3
    const-string v1, "rotation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_4
    const-string v1, "gameBooster"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    goto :goto_1

    :sswitch_5
    const-string v1, "screenOn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    goto :goto_1

    :sswitch_6
    const-string v1, "holdSensor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    const-string v2, "EdgeSuppressionManager"

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_2

    .line 464
    :pswitch_0
    const-string v1, "Send EdgeSuppression data because configuration"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 466
    goto :goto_2

    .line 460
    :pswitch_1
    const-string v1, "Send EdgeSuppression data because Game Booster"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 462
    goto :goto_2

    .line 456
    :pswitch_2
    const-string v1, "Send EdgeSuppression data because User Settings"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 458
    goto :goto_2

    .line 452
    :pswitch_3
    const-string v1, "Send EdgeSuppression data because screen turn on"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 454
    goto :goto_2

    .line 447
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send EdgeSuppression data because laySensor,status = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmLaySensorState(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 450
    goto :goto_2

    .line 442
    :pswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send EdgeSuppression data because holdSensor,status = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmHoldSensorState(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 445
    goto :goto_2

    .line 438
    :pswitch_6
    const-string v1, "Send EdgeSuppression data because rotation"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$EdgeSuppressionHandler;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mhandleEdgeSuppressionData(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 440
    nop

    .line 470
    :goto_2
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 471
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x682a96a7 -> :sswitch_6
        -0x1888a095 -> :sswitch_5
        -0x416d4c2 -> :sswitch_4
        -0x266f082 -> :sswitch_3
        0x5582bc23 -> :sswitch_2
        0x733374f6 -> :sswitch_1
        0x7d68167e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
