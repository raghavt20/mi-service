.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BottomLeftHalfRect"
.end annotation


# direct methods
.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "heightOfScreen"    # I
    .param p3, "widthOfScreen"    # I
    .param p4, "widthOfRect"    # I

    .line 213
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 214
    invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;->setType(I)V

    .line 215
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;->setPosition(I)V

    .line 216
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;->setTopLeftX(I)V

    .line 217
    sub-int v0, p2, p4

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;->setTopLeftY(I)V

    .line 218
    div-int/lit8 v0, p3, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;->setBottomRightX(I)V

    .line 219
    invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;->setBottomRightY(I)V

    .line 220
    return-void
.end method
