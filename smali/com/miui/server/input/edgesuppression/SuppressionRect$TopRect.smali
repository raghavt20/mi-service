.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TopRect"
.end annotation


# direct methods
.method public constructor <init>(ZIII)V
    .locals 1
    .param p1, "isHorizontal"    # Z
    .param p2, "type"    # I
    .param p3, "widthOfScreen"    # I
    .param p4, "widthOfRect"    # I

    .line 236
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 237
    invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setType(I)V

    .line 238
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setPosition(I)V

    .line 239
    if-eqz p1, :cond_0

    .line 240
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftX(I)V

    .line 241
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftY(I)V

    .line 242
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightX(I)V

    .line 243
    invoke-virtual {p0, p4}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightY(I)V

    goto :goto_0

    .line 245
    :cond_0
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftX(I)V

    .line 246
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftY(I)V

    .line 247
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightX(I)V

    .line 248
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightY(I)V

    .line 250
    :goto_0
    return-void
.end method
