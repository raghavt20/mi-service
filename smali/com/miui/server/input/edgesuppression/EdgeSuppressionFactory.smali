.class public Lcom/miui/server/input/edgesuppression/EdgeSuppressionFactory;
.super Ljava/lang/Object;
.source "EdgeSuppressionFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EdgeSuppressionManager"

.field public static final TYPE_INTELLIGENT:Ljava/lang/String; = "intelligent"

.field public static final TYPE_NORMAL:Ljava/lang/String; = "normal"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method public static getEdgeSuppressionMode(Ljava/lang/String;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .line 15
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    const-string v0, "intelligent"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v0, "normal"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 21
    const-string v0, "EdgeSuppressionManager"

    const-string v1, "Wrong type parameter for getEdgeSuppressionMode"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    invoke-static {}, Lcom/miui/server/input/edgesuppression/NormalHelper;->getInstance()Lcom/miui/server/input/edgesuppression/NormalHelper;

    move-result-object v0

    return-object v0

    .line 19
    :pswitch_0
    invoke-static {}, Lcom/miui/server/input/edgesuppression/NormalHelper;->getInstance()Lcom/miui/server/input/edgesuppression/NormalHelper;

    move-result-object v0

    return-object v0

    .line 17
    :pswitch_1
    invoke-static {}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getInstance()Lcom/miui/server/input/edgesuppression/IntelligentHelper;

    move-result-object v0

    return-object v0

    :sswitch_data_0
    .sparse-switch
        -0x3df94319 -> :sswitch_1
        0x43994e77 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
