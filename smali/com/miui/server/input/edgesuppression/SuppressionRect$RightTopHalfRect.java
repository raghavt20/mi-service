public class com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "RightTopHalfRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "heightOfScreen" # I */
/* .param p3, "widthOfScreen" # I */
/* .param p4, "widthOfRect" # I */
/* .line 156 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 157 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setType(I)V
/* .line 158 */
int v0 = 3; // const/4 v0, 0x3
(( com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setPosition(I)V
/* .line 159 */
/* sub-int v0, p3, p4 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setTopLeftX(I)V
/* .line 160 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setTopLeftY(I)V
/* .line 161 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setBottomRightX(I)V
/* .line 162 */
/* div-int/lit8 v0, p2, 0x2 */
(( com.miui.server.input.edgesuppression.SuppressionRect$RightTopHalfRect ) p0 ).setBottomRightY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;->setBottomRightY(I)V
/* .line 163 */
return;
} // .end method
