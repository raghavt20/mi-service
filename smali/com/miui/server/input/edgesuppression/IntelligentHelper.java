public class com.miui.server.input.edgesuppression.IntelligentHelper extends com.miui.server.input.edgesuppression.BaseEdgeSuppression {
	 /* .source "IntelligentHelper.java" */
	 /* # static fields */
	 private static final Integer INDEX_ABSOLUTE_FOURTH;
	 private static final Integer INDEX_ABSOLUTE_ONE;
	 private static final Integer INDEX_ABSOLUTE_THREE;
	 private static final Integer INDEX_ABSOLUTE_TWO;
	 private static final Integer INDEX_CONDITION_FOURTH;
	 private static final Integer INDEX_CONDITION_ONE;
	 private static final Integer INDEX_CONDITION_THREE;
	 private static final Integer INDEX_CONDITION_TWO;
	 private static volatile com.miui.server.input.edgesuppression.IntelligentHelper mInstance;
	 /* # instance fields */
	 private final java.util.ArrayList mRectList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/input/edgesuppression/SuppressionRect;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # direct methods */
private com.miui.server.input.edgesuppression.IntelligentHelper ( ) {
/* .locals 2 */
/* .line 18 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;-><init>()V */
/* .line 8 */
/* new-instance v0, Ljava/util/ArrayList; */
/* const/16 v1, 0x8 */
/* invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V */
this.mRectList = v0;
/* .line 19 */
return;
} // .end method
private void getArrayList ( java.util.ArrayList p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/input/edgesuppression/SuppressionRect;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 178 */
/* .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;" */
(( java.util.ArrayList ) p1 ).iterator ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
/* .line 179 */
/* .local v1, "suppressionRect":Lcom/miui/server/input/edgesuppression/SuppressionRect; */
v2 = this.mSendList;
(( com.miui.server.input.edgesuppression.SuppressionRect ) v1 ).getList ( ); // invoke-virtual {v1}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->getList()Ljava/util/ArrayList;
(( java.util.ArrayList ) v2 ).addAll ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
/* .line 180 */
} // .end local v1 # "suppressionRect":Lcom/miui/server/input/edgesuppression/SuppressionRect;
/* .line 181 */
} // :cond_0
return;
} // .end method
public static com.miui.server.input.edgesuppression.IntelligentHelper getInstance ( ) {
/* .locals 2 */
/* .line 22 */
v0 = com.miui.server.input.edgesuppression.IntelligentHelper.mInstance;
/* if-nez v0, :cond_1 */
/* .line 23 */
/* const-class v0, Lcom/miui/server/input/edgesuppression/IntelligentHelper; */
/* monitor-enter v0 */
/* .line 24 */
try { // :try_start_0
v1 = com.miui.server.input.edgesuppression.IntelligentHelper.mInstance;
/* if-nez v1, :cond_0 */
/* .line 25 */
/* new-instance v1, Lcom/miui/server/input/edgesuppression/IntelligentHelper; */
/* invoke-direct {v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;-><init>()V */
/* .line 27 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 29 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.edgesuppression.IntelligentHelper.mInstance;
} // .end method
private Integer getRectPosition ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "type" # I */
/* .param p2, "position" # I */
/* .line 151 */
int v0 = 0; // const/4 v0, 0x0
/* .line 152 */
/* .local v0, "result":I */
int v1 = 2; // const/4 v1, 0x2
/* packed-switch p2, :pswitch_data_0 */
/* .line 163 */
/* :pswitch_0 */
/* if-ne p1, v1, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
} // :cond_0
int v1 = 7; // const/4 v1, 0x7
} // :goto_0
/* move v0, v1 */
/* .line 164 */
/* .line 160 */
/* :pswitch_1 */
/* if-ne p1, v1, :cond_1 */
} // :cond_1
int v1 = 6; // const/4 v1, 0x6
} // :goto_1
/* move v0, v1 */
/* .line 161 */
/* .line 157 */
/* :pswitch_2 */
/* if-ne p1, v1, :cond_2 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
int v1 = 5; // const/4 v1, 0x5
} // :goto_2
/* move v0, v1 */
/* .line 158 */
/* .line 154 */
/* :pswitch_3 */
/* if-ne p1, v1, :cond_3 */
int v1 = 0; // const/4 v1, 0x0
} // :cond_3
int v1 = 4; // const/4 v1, 0x4
} // :goto_3
/* move v0, v1 */
/* .line 155 */
/* nop */
/* .line 168 */
} // :goto_4
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void initArrayList ( ) {
/* .locals 3 */
/* .line 172 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
/* const/16 v1, 0x8 */
/* if-ge v0, v1, :cond_0 */
/* .line 173 */
v1 = this.mRectList;
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
/* invoke-direct {v2}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
(( java.util.ArrayList ) v1 ).add ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 172 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 175 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void setHorizontalDataWithoutSensor ( Integer p0, Integer p1 ) {
/* .locals 9 */
/* .param p1, "size" # I */
/* .param p2, "type" # I */
/* .line 95 */
v0 = this.mRectList;
int v1 = 0; // const/4 v1, 0x0
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
/* iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* move-object v1, p0 */
/* move v3, p2 */
/* move v8, p1 */
/* invoke-virtual/range {v1 ..v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 97 */
v0 = this.mRectList;
int v1 = 1; // const/4 v1, 0x1
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v4 = 1; // const/4 v4, 0x1
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* sub-int v6, v0, p1 */
/* iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* iget v8, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* move-object v1, p0 */
/* invoke-virtual/range {v1 ..v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 99 */
v0 = this.mRectList;
int v1 = 2; // const/4 v1, 0x2
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v4 = 2; // const/4 v4, 0x2
int v6 = 0; // const/4 v6, 0x0
/* iget v8, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* move-object v1, p0 */
/* move v7, p1 */
/* invoke-virtual/range {v1 ..v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 101 */
v0 = this.mRectList;
int v1 = 3; // const/4 v1, 0x3
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v4 = 3; // const/4 v4, 0x3
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* sub-int v5, v0, p1 */
/* iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* iget v8, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* move-object v1, p0 */
/* invoke-virtual/range {v1 ..v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 103 */
return;
} // .end method
private void setHorizontalRectData ( Integer p0, Integer p1 ) {
/* .locals 12 */
/* .param p1, "size" # I */
/* .param p2, "type" # I */
/* .line 59 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mHoldSensorState:I */
int v1 = 3; // const/4 v1, 0x3
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
int v4 = 0; // const/4 v4, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* goto/16 :goto_0 */
/* .line 77 */
/* :pswitch_0 */
v0 = this.mRectList;
v4 = /* invoke-direct {p0, p2, v4}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v5, v0 */
/* check-cast v5, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
int v10 = 0; // const/4 v10, 0x0
int v11 = 0; // const/4 v11, 0x0
/* move-object v4, p0 */
/* move v6, p2 */
/* invoke-virtual/range {v4 ..v11}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 79 */
v0 = this.mRectList;
v3 = /* invoke-direct {p0, p2, v3}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v4, v0 */
/* check-cast v4, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v6 = 1; // const/4 v6, 0x1
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* sub-int v8, v0, p1 */
/* iget v9, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* iget v10, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* move-object v3, p0 */
/* move v5, p2 */
/* invoke-virtual/range {v3 ..v10}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 82 */
v0 = this.mRectList;
v2 = /* invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect; */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;-><init>(III)V */
(( java.util.ArrayList ) v0 ).set ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 84 */
v0 = this.mRectList;
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect; */
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;-><init>(IIII)V */
(( java.util.ArrayList ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 87 */
/* .line 66 */
/* :pswitch_1 */
v0 = this.mRectList;
v4 = /* invoke-direct {p0, p2, v4}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v4 ); // invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v5, v0 */
/* check-cast v5, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
int v9 = 0; // const/4 v9, 0x0
/* iget v10, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* move-object v4, p0 */
/* move v6, p2 */
/* move v11, p1 */
/* invoke-virtual/range {v4 ..v11}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 68 */
v0 = this.mRectList;
v3 = /* invoke-direct {p0, p2, v3}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v4, v0 */
/* check-cast v4, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v6 = 1; // const/4 v6, 0x1
int v10 = 0; // const/4 v10, 0x0
/* move-object v3, p0 */
/* move v5, p2 */
/* invoke-virtual/range {v3 ..v10}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 70 */
v0 = this.mRectList;
v2 = /* invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect; */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;-><init>(III)V */
(( java.util.ArrayList ) v0 ).set ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 72 */
v0 = this.mRectList;
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect; */
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;-><init>(IIII)V */
(( java.util.ArrayList ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 75 */
/* .line 63 */
/* :pswitch_2 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setHorizontalDataWithoutSensor(II)V */
/* .line 64 */
/* nop */
/* .line 91 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
private void setPortraitRectData ( Integer p0, Integer p1 ) {
/* .locals 10 */
/* .param p1, "size" # I */
/* .param p2, "type" # I */
/* .line 111 */
v0 = this.mRectList;
int v1 = 0; // const/4 v1, 0x0
v1 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v2, v0 */
/* check-cast v2, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
int v8 = 0; // const/4 v8, 0x0
/* move-object v1, p0 */
/* move v3, p2 */
/* invoke-virtual/range {v1 ..v8}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 113 */
v0 = this.mRectList;
int v1 = 1; // const/4 v1, 0x1
v2 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
(( java.util.ArrayList ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* move-object v3, v0 */
/* check-cast v3, Lcom/miui/server/input/edgesuppression/SuppressionRect; */
int v5 = 1; // const/4 v5, 0x1
int v9 = 0; // const/4 v9, 0x0
/* move-object v2, p0 */
/* move v4, p2 */
/* invoke-virtual/range {v2 ..v9}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V */
/* .line 115 */
int v0 = 3; // const/4 v0, 0x3
int v2 = 2; // const/4 v2, 0x2
/* if-ne p2, v2, :cond_0 */
/* .line 116 */
v1 = this.mRectList;
v2 = /* invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect; */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;-><init>(III)V */
(( java.util.ArrayList ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 118 */
v1 = this.mRectList;
v0 = /* invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect; */
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;-><init>(IIII)V */
(( java.util.ArrayList ) v1 ).set ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 120 */
} // :cond_0
/* if-ne p2, v1, :cond_1 */
/* .line 121 */
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mHoldSensorState:I */
/* packed-switch v1, :pswitch_data_0 */
/* .line 138 */
/* :pswitch_0 */
v1 = this.mRectList;
v2 = /* invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect; */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;-><init>(III)V */
(( java.util.ArrayList ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 140 */
v1 = this.mRectList;
v0 = /* invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect; */
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;-><init>(IIII)V */
(( java.util.ArrayList ) v1 ).set ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 143 */
/* .line 131 */
/* :pswitch_1 */
v1 = this.mRectList;
v2 = /* invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect; */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect;-><init>(III)V */
(( java.util.ArrayList ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 133 */
v1 = this.mRectList;
v0 = /* invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect; */
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;-><init>(IIII)V */
(( java.util.ArrayList ) v1 ).set ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 136 */
/* .line 125 */
/* :pswitch_2 */
v1 = this.mRectList;
v2 = /* invoke-direct {p0, p2, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v3, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect; */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* invoke-direct {v3, p2, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;-><init>(III)V */
(( java.util.ArrayList ) v1 ).set ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 127 */
v1 = this.mRectList;
v0 = /* invoke-direct {p0, p2, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getRectPosition(II)I */
/* new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect; */
/* iget v3, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenHeight:I */
/* iget v4, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mScreenWidth:I */
/* invoke-direct {v2, p2, v3, v4, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;-><init>(IIII)V */
(( java.util.ArrayList ) v1 ).set ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
/* .line 129 */
/* nop */
/* .line 148 */
} // :cond_1
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch -0x1 */
/* :pswitch_2 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
/* # virtual methods */
public java.util.ArrayList getEdgeSuppressionData ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 8 */
/* .param p1, "rotation" # I */
/* .param p2, "widthOfScreen" # I */
/* .param p3, "heightOfScreen" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(III)", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 35 */
v0 = this.mRectList;
v0 = (( java.util.ArrayList ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 36 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->initArrayList()V */
/* .line 38 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mIsHorizontal:Z */
int v1 = 1; // const/4 v1, 0x1
int v2 = 2; // const/4 v2, 0x2
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 39 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mAbsoluteSize:I */
/* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setHorizontalRectData(II)V */
/* .line 40 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConditionSize:I */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setHorizontalRectData(II)V */
/* .line 42 */
} // :cond_1
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mAbsoluteSize:I */
/* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setPortraitRectData(II)V */
/* .line 43 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConditionSize:I */
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->setPortraitRectData(II)V */
/* .line 45 */
} // :goto_0
/* iget v6, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConnerWidth:I */
/* iget v7, p0, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->mConnerHeight:I */
/* move-object v2, p0 */
/* move v3, p1 */
/* move v4, p2 */
/* move v5, p3 */
/* invoke-virtual/range {v2 ..v7}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getCornerData(IIIII)Ljava/util/ArrayList; */
/* .line 47 */
/* .local v0, "cornerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;" */
v1 = this.mSendList;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 48 */
v1 = this.mRectList;
/* invoke-direct {p0, v1}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getArrayList(Ljava/util/ArrayList;)V */
/* .line 49 */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/edgesuppression/IntelligentHelper;->getArrayList(Ljava/util/ArrayList;)V */
/* .line 50 */
v1 = this.mSendList;
} // .end method
