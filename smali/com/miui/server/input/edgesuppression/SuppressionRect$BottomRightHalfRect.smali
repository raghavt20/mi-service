.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BottomRightHalfRect"
.end annotation


# direct methods
.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "heightOfScreen"    # I
    .param p3, "widthOfScreen"    # I
    .param p4, "widthOfRect"    # I

    .line 225
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 226
    invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setType(I)V

    .line 227
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setPosition(I)V

    .line 228
    div-int/lit8 v0, p3, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setTopLeftX(I)V

    .line 229
    sub-int v0, p2, p4

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setTopLeftY(I)V

    .line 230
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setBottomRightX(I)V

    .line 231
    invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;->setBottomRightY(I)V

    .line 232
    return-void
.end method
