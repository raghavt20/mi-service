public class com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "TopLeftHalfRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ( ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "widthOfScreen" # I */
/* .param p3, "widthOfRect" # I */
/* .line 190 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 191 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ) p0 ).setType ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setType(I)V
/* .line 192 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setPosition(I)V
/* .line 193 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setTopLeftX(I)V
/* .line 194 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setTopLeftY(I)V
/* .line 195 */
/* div-int/lit8 v0, p2, 0x2 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ) p0 ).setBottomRightX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setBottomRightX(I)V
/* .line 196 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopLeftHalfRect ) p0 ).setBottomRightY ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setBottomRightY(I)V
/* .line 197 */
return;
} // .end method
