public class com.miui.server.input.edgesuppression.SuppressionRect$TopRect extends com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/edgesuppression/SuppressionRect; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "TopRect" */
} // .end annotation
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect$TopRect ( ) {
/* .locals 1 */
/* .param p1, "isHorizontal" # Z */
/* .param p2, "type" # I */
/* .param p3, "widthOfScreen" # I */
/* .param p4, "widthOfRect" # I */
/* .line 236 */
/* invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V */
/* .line 237 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setType ( p2 ); // invoke-virtual {p0, p2}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setType(I)V
/* .line 238 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setPosition ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setPosition(I)V
/* .line 239 */
if ( p1 != null) { // if-eqz p1, :cond_0
	 /* .line 240 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftX(I)V
	 /* .line 241 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftY(I)V
	 /* .line 242 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setBottomRightX ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightX(I)V
	 /* .line 243 */
	 (( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setBottomRightY ( p4 ); // invoke-virtual {p0, p4}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightY(I)V
	 /* .line 245 */
} // :cond_0
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftX(I)V
/* .line 246 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setTopLeftY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setTopLeftY(I)V
/* .line 247 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setBottomRightX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightX(I)V
/* .line 248 */
(( com.miui.server.input.edgesuppression.SuppressionRect$TopRect ) p0 ).setBottomRightY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;->setBottomRightY(I)V
/* .line 250 */
} // :goto_0
return;
} // .end method
