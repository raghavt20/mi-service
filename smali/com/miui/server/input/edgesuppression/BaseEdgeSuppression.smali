.class public abstract Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;
.super Ljava/lang/Object;
.source "BaseEdgeSuppression.java"


# static fields
.field protected static final ABSOLUTE:I = 0x2

.field protected static final CONDITION:I = 0x1

.field private static final CORNER:I = 0x0

.field protected static final EMPTY_POINT:I = 0x0

.field protected static final RECT_FIRST:I = 0x0

.field protected static final RECT_FOURTH:I = 0x3

.field protected static final RECT_SECOND:I = 0x1

.field protected static final RECT_THIRD:I = 0x2

.field protected static final TAG:Ljava/lang/String; = "EdgeSuppressionManager"


# instance fields
.field protected mAbsoluteSize:I

.field protected mConditionSize:I

.field protected mConnerHeight:I

.field protected mConnerWidth:I

.field protected mHoldSensorState:I

.field protected mIsHorizontal:Z

.field protected mRotation:I

.field protected mScreenHeight:I

.field protected mScreenWidth:I

.field protected mSendList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mTargetId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mSendList:Ljava/util/ArrayList;

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConditionSize:I

    .line 28
    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerWidth:I

    .line 29
    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I

    return-void
.end method

.method private hasParamChanged(Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;II)Z
    .locals 2
    .param p1, "info"    # Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .param p2, "widthOfScreen"    # I
    .param p3, "heightOfScreen"    # I

    .line 42
    iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConditionSize:I

    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConditionSize()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerWidth:I

    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerWidth()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I

    .line 43
    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerHeight()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mScreenWidth:I

    if-ne v0, p2, :cond_1

    iget v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I

    if-eq v0, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 42
    :goto_1
    return v0
.end method


# virtual methods
.method getCornerData(IIIII)Ljava/util/ArrayList;
    .locals 18
    .param p1, "rotation"    # I
    .param p2, "widthOfScreen"    # I
    .param p3, "heightOfScreen"    # I
    .param p4, "connerWidth"    # I
    .param p5, "connerHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIII)",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/input/edgesuppression/SuppressionRect;",
            ">;"
        }
    .end annotation

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v8, v0

    .line 74
    .local v8, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/edgesuppression/SuppressionRect;>;"
    const/16 v17, 0x0

    .line 75
    .local v17, "type":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 76
    new-instance v2, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    invoke-direct {v2}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    .end local v0    # "i":I
    :cond_0
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v5, 0x1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_1

    .line 111
    :pswitch_0
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v9, p0

    move/from16 v11, v17

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 113
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x1

    sub-int v4, p2, p4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v2, v17

    move v15, v6

    move/from16 v6, p2

    move v14, v7

    move/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 115
    invoke-virtual {v8, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x2

    const/4 v0, 0x0

    const/4 v15, 0x0

    move v7, v14

    move v14, v0

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 117
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x3

    sub-int v4, p2, p4

    sub-int v5, p3, p5

    move-object/from16 v0, p0

    move/from16 v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 120
    goto/16 :goto_1

    .line 101
    :pswitch_1
    move v15, v6

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v2, v17

    move v14, v5

    move v5, v6

    move/from16 v6, p4

    move v13, v7

    move/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 103
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x1

    sub-int v4, p2, p4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v6, p2

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 105
    invoke-virtual {v8, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x2

    const/4 v0, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v9, p0

    move/from16 v11, v17

    move v7, v13

    move v13, v0

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 107
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x3

    const/4 v13, 0x0

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 109
    goto/16 :goto_1

    .line 91
    :pswitch_2
    move v14, v5

    move v15, v6

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move/from16 v2, v17

    move/from16 v6, p4

    move v13, v7

    move/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 93
    invoke-virtual {v8, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x1

    const/4 v0, 0x0

    const/4 v14, 0x0

    const/4 v1, 0x0

    const/16 v16, 0x0

    move-object/from16 v9, p0

    move/from16 v11, v17

    move v7, v13

    move v13, v0

    move v2, v15

    move v15, v1

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 95
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x2

    sub-int v5, p3, p5

    move-object/from16 v0, p0

    move/from16 v2, v17

    move v15, v7

    move/from16 v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 97
    invoke-virtual {v8, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x3

    const/4 v13, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 99
    goto :goto_1

    .line 80
    :pswitch_3
    move v14, v5

    move v2, v6

    move v15, v7

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v16, 0x0

    move-object/from16 v9, p0

    move/from16 v11, v17

    move v3, v14

    move v14, v0

    move v15, v1

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 82
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v12, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v9 .. v16}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 84
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x2

    const/4 v4, 0x0

    sub-int v5, p3, p5

    move-object/from16 v0, p0

    move/from16 v2, v17

    move/from16 v6, p4

    move v9, v7

    move/from16 v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 86
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/miui/server/input/edgesuppression/SuppressionRect;

    const/4 v3, 0x3

    sub-int v4, p2, p4

    sub-int v5, p3, p5

    move-object/from16 v0, p0

    move/from16 v6, p2

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V

    .line 89
    nop

    .line 124
    :goto_1
    return-object v8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method abstract getEdgeSuppressionData(III)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method initInputMethodData(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "size"    # I

    .line 133
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "vertical_edge_suppression_size"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 135
    .local v0, "verticalSize":I
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "horizontal_edge_suppression_size"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 138
    .local v3, "horizontalSize":I
    if-ne v0, v2, :cond_0

    if-ne v3, v2, :cond_0

    sget-boolean v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->SHOULD_REMOVE_EDGE_SETTINGS:Z

    if-nez v2, :cond_0

    .line 141
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 143
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 146
    :cond_0
    return-void
.end method

.method setInputMethodSize(Landroid/content/Context;IIZ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "minInputMethodSize"    # I
    .param p3, "maxInputMethodSize"    # I
    .param p4, "isVertical"    # Z

    .line 151
    const/4 v0, 0x1

    .line 152
    .local v0, "shouldOverWrite":Z
    const-string/jumbo v1, "vertical_edge_suppression_size"

    const-string v2, "horizontal_edge_suppression_size"

    const/4 v3, -0x1

    if-eqz p4, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v1, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    goto :goto_0

    .line 153
    :cond_0
    nop

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 153
    invoke-static {v4, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    :goto_0
    nop

    .line 157
    .local v4, "result":I
    sget-boolean v5, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    if-eqz v5, :cond_4

    sget-boolean v5, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->SHOULD_REMOVE_EDGE_SETTINGS:Z

    if-eqz v5, :cond_1

    goto :goto_1

    .line 165
    :cond_1
    if-le v4, p3, :cond_2

    .line 166
    move v4, p3

    goto :goto_2

    .line 167
    :cond_2
    if-ge v4, p2, :cond_3

    .line 168
    move v4, p2

    goto :goto_2

    .line 170
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 159
    :cond_4
    :goto_1
    if-eq v4, v3, :cond_5

    .line 160
    const/4 v4, -0x1

    goto :goto_2

    .line 162
    :cond_5
    const/4 v0, 0x0

    .line 174
    :goto_2
    if-eqz v0, :cond_7

    .line 175
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 176
    if-eqz p4, :cond_6

    goto :goto_3

    .line 177
    :cond_6
    move-object v1, v2

    .line 175
    :goto_3
    invoke-static {v3, v1, v4}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 179
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputMethod ShrinkSize shouldOverWrite = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isVertical = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EdgeSuppressionManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    return-void
.end method

.method setRectValue(Lcom/miui/server/input/edgesuppression/SuppressionRect;IIIIII)V
    .locals 0
    .param p1, "suppressionRect"    # Lcom/miui/server/input/edgesuppression/SuppressionRect;
    .param p2, "type"    # I
    .param p3, "position"    # I
    .param p4, "startX"    # I
    .param p5, "startY"    # I
    .param p6, "endX"    # I
    .param p7, "endY"    # I

    .line 129
    invoke-virtual/range {p1 .. p7}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setValue(IIIIII)V

    .line 130
    return-void
.end method

.method syncDataToKernel()V
    .locals 5

    .line 66
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mSendList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EdgeSuppressionManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mTargetId:I

    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mSendList:Ljava/util/ArrayList;

    .line 68
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 67
    const/16 v4, 0xf

    invoke-virtual {v0, v1, v4, v2, v3}, Lmiui/util/ITouchFeature;->setEdgeMode(IILjava/util/ArrayList;I)Z

    .line 69
    return-void
.end method

.method updateInterNalParam(Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;IIIII)V
    .locals 1
    .param p1, "info"    # Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;
    .param p2, "holdSensorState"    # I
    .param p3, "rotation"    # I
    .param p4, "targetId"    # I
    .param p5, "widthOfScreen"    # I
    .param p6, "heightOfScreen"    # I

    .line 50
    iput p3, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mRotation:I

    .line 51
    iput p4, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mTargetId:I

    .line 52
    iput p2, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mHoldSensorState:I

    .line 53
    invoke-direct {p0, p1, p5, p6}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->hasParamChanged(Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConditionSize()I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConditionSize:I

    .line 55
    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getAbsoluteSize()I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mAbsoluteSize:I

    .line 56
    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerWidth()I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerWidth:I

    .line 57
    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->getConnerHeight()I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mConnerHeight:I

    .line 58
    invoke-virtual {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionInfo;->isHorizontal()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mIsHorizontal:Z

    .line 59
    iput p5, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mScreenWidth:I

    .line 60
    iput p6, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mScreenHeight:I

    .line 61
    invoke-virtual {p0, p3, p5, p6}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->getEdgeSuppressionData(III)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->mSendList:Ljava/util/ArrayList;

    .line 63
    :cond_0
    return-void
.end method
