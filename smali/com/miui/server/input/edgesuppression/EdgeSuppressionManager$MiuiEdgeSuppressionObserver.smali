.class Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;
.super Landroid/database/ContentObserver;
.source "EdgeSuppressionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiEdgeSuppressionObserver"
.end annotation


# instance fields
.field contentResolver:Landroid/content/ContentResolver;

.field isTypeChanged:Z

.field final synthetic this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Landroid/os/Handler;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 344
    iput-object p1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    .line 345
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 337
    invoke-static {p1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    .line 338
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z

    .line 346
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 375
    const-string v0, "edge_size"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "custom_suppression"

    const/4 v3, -0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v1, :cond_2

    .line 376
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)F

    move-result v6

    float-to-int v6, v6

    invoke-static {v1, v6}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmLastEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;I)V

    .line 377
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v6, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)F

    move-result v7

    invoke-static {v6, v0, v7, v3}, Landroid/provider/Settings$System;->getFloatForUser(Landroid/content/ContentResolver;Ljava/lang/String;FI)F

    move-result v0

    invoke-static {v1, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;F)V

    .line 380
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)F

    move-result v1

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmMaxAdjustValue(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    invoke-static {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmEdgeModeSize(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;F)V

    .line 384
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z

    if-nez v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0, v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmLastEdgeModeType(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Ljava/lang/String;)V

    goto :goto_0

    .line 387
    :cond_1
    iput-boolean v4, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z

    .line 389
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    const-string/jumbo v1, "settings"

    invoke-static {v0, v5, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mnotifyEdgeSuppressionChanged(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;ILjava/lang/String;)V

    goto/16 :goto_2

    .line 391
    :cond_2
    const-string v0, "edge_type"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 392
    iput-boolean v5, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->isTypeChanged:Z

    .line 393
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v5, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmLastEdgeModeType(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Ljava/lang/String;)V

    .line 394
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v5, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    invoke-static {v5, v0, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    .line 396
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    const-string v1, "default_suppression"

    if-nez v0, :cond_3

    .line 397
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0, v4}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmIsUserSetted(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Z)V

    .line 398
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iput-object v1, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    goto :goto_1

    .line 399
    :cond_3
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    const-string v3, "diy_suppression"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 400
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iput-object v2, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    .line 402
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmSupportSensor(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Z

    move-result v0

    const-string v2, "EdgeSuppressionManager"

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v0, v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->mEdgeModeType:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 403
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    const-string v1, "intelligent"

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionFactory;->getEdgeSuppressionMode(Ljava/lang/String;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmBaseEdgeSuppression(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;)V

    .line 405
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->registerSensors()V

    .line 406
    const-string v0, "mBaseEdgeSuppression: intelligent"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 408
    :cond_5
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    const-string v1, "normal"

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionFactory;->getEdgeSuppressionMode(Ljava/lang/String;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmBaseEdgeSuppression(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;)V

    .line 410
    const-string v0, "mBaseEdgeSuppression: normal"

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-virtual {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->unRegisterSensors()V

    goto :goto_2

    .line 413
    :cond_6
    const-string/jumbo v0, "vertical_edge_suppression_size"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 414
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmBaseEdgeSuppression(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmConfigLoader(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    move-result-object v2

    iget v2, v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmConfigLoader(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    move-result-object v3

    iget v3, v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setInputMethodSize(Landroid/content/Context;IIZ)V

    goto :goto_2

    .line 416
    :cond_7
    const-string v0, "horizontal_edge_suppression_size"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 417
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmBaseEdgeSuppression(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v1}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v2}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmConfigLoader(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    move-result-object v2

    iget v2, v2, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMinInputMethodSize:I

    iget-object v3, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v3}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fgetmConfigLoader(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;

    move-result-object v3

    iget v3, v3, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$ConfigLoader;->mMaxInputMethodSize:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/miui/server/input/edgesuppression/BaseEdgeSuppression;->setInputMethodSize(Landroid/content/Context;IIZ)V

    goto :goto_2

    .line 419
    :cond_8
    const-string v0, "edge_suppression_track_time"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 420
    iget-object v1, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    iget-object v2, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    .line 421
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 420
    invoke-static {v2, v0, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$fputmTrackEventTime(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;Ljava/lang/String;)V

    .line 423
    :cond_9
    :goto_2
    return-void
.end method

.method registerObserver()V
    .locals 6

    .line 349
    sget-boolean v0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->IS_SUPPORT_EDGE_MODE:Z

    const/4 v1, -0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    const-string v3, "edge_type"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 353
    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V

    .line 354
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    const-string v3, "edge_size"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 357
    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V

    .line 358
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    const-string v3, "edge_suppression_track_time"

    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 360
    invoke-static {v3}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V

    .line 361
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->this$0:Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;

    invoke-static {v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;->-$$Nest$mresetUserSetted(Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager;)V

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v3, "vertical_edge_suppression_size"

    invoke-static {v3}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 367
    iget-object v0, p0, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->contentResolver:Landroid/content/ContentResolver;

    const-string v4, "horizontal_edge_suppression_size"

    invoke-static {v4}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5, v2, p0, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 370
    invoke-static {v3}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V

    .line 371
    invoke-static {v4}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/miui/server/input/edgesuppression/EdgeSuppressionManager$MiuiEdgeSuppressionObserver;->onChange(ZLandroid/net/Uri;)V

    .line 372
    return-void
.end method
