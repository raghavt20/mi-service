.class public Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;
.super Lcom/miui/server/input/edgesuppression/SuppressionRect;
.source "SuppressionRect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/edgesuppression/SuppressionRect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TopLeftHalfRect"
.end annotation


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "widthOfScreen"    # I
    .param p3, "widthOfRect"    # I

    .line 190
    invoke-direct {p0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;-><init>()V

    .line 191
    invoke-virtual {p0, p1}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setType(I)V

    .line 192
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setPosition(I)V

    .line 193
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setTopLeftX(I)V

    .line 194
    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setTopLeftY(I)V

    .line 195
    div-int/lit8 v0, p2, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setBottomRightX(I)V

    .line 196
    invoke-virtual {p0, p3}, Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;->setBottomRightY(I)V

    .line 197
    return-void
.end method
