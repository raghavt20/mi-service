public class com.miui.server.input.edgesuppression.SuppressionRect {
	 /* .source "SuppressionRect.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$RightRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomRightHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$BottomLeftHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$TopRightHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$TopLeftHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$RightBottomHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftBottomHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$RightTopHalfRect;, */
	 /* Lcom/miui/server/input/edgesuppression/SuppressionRect$LeftTopHalfRect; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer NODE_DEFAULT;
private static final Integer TIME_DEFAULT;
/* # instance fields */
private Integer bottomRightX;
private Integer bottomRightY;
private java.util.ArrayList list;
private Integer node;
private Integer position;
private Integer time;
private Integer topLeftX;
private Integer topLeftY;
private Integer type;
/* # direct methods */
public com.miui.server.input.edgesuppression.SuppressionRect ( ) {
	 /* .locals 1 */
	 /* .line 19 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 8 */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 this.list = v0;
	 /* .line 20 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
	 /* .line 21 */
	 /* iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
	 /* .line 22 */
	 return;
} // .end method
public com.miui.server.input.edgesuppression.SuppressionRect ( ) {
	 /* .locals 1 */
	 /* .param p1, "type" # I */
	 /* .param p2, "position" # I */
	 /* .line 24 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 8 */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 this.list = v0;
	 /* .line 25 */
	 /* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I */
	 /* .line 26 */
	 /* iput p2, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I */
	 /* .line 27 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
	 /* .line 28 */
	 /* iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
	 /* .line 29 */
	 return;
} // .end method
/* # virtual methods */
public Integer getBottomRightX ( ) {
	 /* .locals 1 */
	 /* .line 64 */
	 /* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I */
} // .end method
public Integer getBottomRightY ( ) {
	 /* .locals 1 */
	 /* .line 72 */
	 /* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I */
} // .end method
public java.util.ArrayList getList ( ) {
	 /* .locals 2 */
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "()", */
	 /* "Ljava/util/ArrayList<", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
/* .line 130 */
v0 = this.list;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 131 */
	 v0 = this.list;
	 (( java.util.ArrayList ) v0 ).clear ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
	 /* .line 133 */
} // :cond_0
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 134 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 135 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 136 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 137 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 138 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 139 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 140 */
v0 = this.list;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
java.lang.Integer .valueOf ( v1 );
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 141 */
v0 = this.list;
} // .end method
public Integer getNode ( ) {
/* .locals 1 */
/* .line 88 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
} // .end method
public Integer getPosition ( ) {
/* .locals 1 */
/* .line 40 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I */
} // .end method
public Integer getTime ( ) {
/* .locals 1 */
/* .line 80 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
} // .end method
public Integer getTopLeftX ( ) {
/* .locals 1 */
/* .line 48 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I */
} // .end method
public Integer getTopLeftY ( ) {
/* .locals 1 */
/* .line 56 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I */
} // .end method
public Integer getType ( ) {
/* .locals 1 */
/* .line 32 */
/* iget v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I */
} // .end method
public void setBottomRightX ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "bottomRightX" # I */
/* .line 68 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I */
/* .line 69 */
return;
} // .end method
public void setBottomRightY ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "bottomRightY" # I */
/* .line 76 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I */
/* .line 77 */
return;
} // .end method
public void setEmpty ( ) {
/* .locals 1 */
/* .line 108 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.edgesuppression.SuppressionRect ) p0 ).setTopLeftX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setTopLeftX(I)V
/* .line 109 */
(( com.miui.server.input.edgesuppression.SuppressionRect ) p0 ).setBottomRightY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setBottomRightY(I)V
/* .line 110 */
(( com.miui.server.input.edgesuppression.SuppressionRect ) p0 ).setBottomRightX ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setBottomRightX(I)V
/* .line 111 */
(( com.miui.server.input.edgesuppression.SuppressionRect ) p0 ).setBottomRightY ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/edgesuppression/SuppressionRect;->setBottomRightY(I)V
/* .line 112 */
return;
} // .end method
public void setNode ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "node" # I */
/* .line 92 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
/* .line 93 */
return;
} // .end method
public void setPosition ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "position" # I */
/* .line 44 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I */
/* .line 45 */
return;
} // .end method
public void setTime ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "time" # I */
/* .line 84 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
/* .line 85 */
return;
} // .end method
public void setTopLeftX ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "topLeftX" # I */
/* .line 52 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I */
/* .line 53 */
return;
} // .end method
public void setTopLeftY ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "topLeftY" # I */
/* .line 60 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I */
/* .line 61 */
return;
} // .end method
public void setType ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "type" # I */
/* .line 36 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I */
/* .line 37 */
return;
} // .end method
public void setValue ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) {
/* .locals 1 */
/* .param p1, "type" # I */
/* .param p2, "position" # I */
/* .param p3, "topLeftX" # I */
/* .param p4, "topLeftY" # I */
/* .param p5, "bottomRightX" # I */
/* .param p6, "bottomRightY" # I */
/* .line 97 */
/* iput p1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I */
/* .line 98 */
/* iput p2, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I */
/* .line 99 */
/* iput p3, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I */
/* .line 100 */
/* iput p4, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I */
/* .line 101 */
/* iput p5, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I */
/* .line 102 */
/* iput p6, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I */
/* .line 103 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
/* .line 104 */
/* iput v0, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
/* .line 105 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 116 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "SuppressionRect{list="; // const-string v1, "SuppressionRect{list="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.list;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", type="; // const-string v1, ", type="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->type:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", position="; // const-string v1, ", position="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->position:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", topLeftX="; // const-string v1, ", topLeftX="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftX:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", topLeftY="; // const-string v1, ", topLeftY="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->topLeftY:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", bottomRightX="; // const-string v1, ", bottomRightX="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightX:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", bottomRightY="; // const-string v1, ", bottomRightY="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->bottomRightY:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", time="; // const-string v1, ", time="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->time:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", node="; // const-string v1, ", node="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/edgesuppression/SuppressionRect;->node:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
