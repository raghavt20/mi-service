.class Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiKnockGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/MiuiKnockGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;


# direct methods
.method constructor <init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/knock/MiuiKnockGestureService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 486
    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    .line 487
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 488
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 491
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 492
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "double_knock"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 494
    const-string v1, "knock_gesture_v"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 496
    const-string v1, "knock_slide_shape"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 498
    const-string v1, "knock_long_press_horizontal_slid"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 500
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 502
    const-string v1, "pref_open_game_booster"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 504
    const-string v1, "close_knock_all_feature"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 506
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 6
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 510
    const-string v0, "double_knock"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "MiuiKnockGestureService"

    if-eqz v1, :cond_0

    .line 511
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmFunctionDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V

    .line 513
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmCheckerDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGestureDouble;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->setFunction(Ljava/lang/String;)V

    .line 514
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "knock feature change , double_knock_function:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 515
    :cond_0
    const-string v0, "knock_gesture_v"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 516
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmFunctionGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmCheckerKnockGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGestureV;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureV;->setFunction(Ljava/lang/String;)V

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "knock feature change , knock_gesture_v:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 520
    :cond_1
    const-string v0, "knock_slide_shape"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 521
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmFunctionGesturePartial(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V

    .line 523
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmCheckerGesturePartialShot(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionGesturePartial(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setFunction(Ljava/lang/String;)V

    .line 524
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "knock feature change , knock_gesture_partial_screenshot:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionGesturePartial(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 525
    :cond_2
    const-string v0, "knock_long_press_horizontal_slid"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 526
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmFunctionHorizontalSlid(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V

    .line 528
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmCheckerHorizontalBrightness(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionHorizontalSlid(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setFunction(Ljava/lang/String;)V

    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "knock feature change , knock_long_press_horizontal_slid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmFunctionHorizontalSlid(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 530
    :cond_3
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_5

    .line 531
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_4

    move v3, v4

    :cond_4
    invoke-static {v1, v3}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmIsGameMode(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V

    .line 532
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "game_booster changed, isGameMode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmIsGameMode(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 533
    :cond_5
    const-string v0, "pref_open_game_booster"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 534
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v0, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_6

    move v3, v4

    :cond_6
    invoke-static {v1, v3}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmOpenGameBooster(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V

    .line 535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "game_booster_feature changed, mOpenGameBooster = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmOpenGameBooster(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 536
    :cond_7
    const-string v0, "close_knock_all_feature"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 537
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, v0, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_8

    move v3, v4

    :cond_8
    invoke-static {v1, v3}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmCloseKnockAllFeature(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V

    .line 539
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "close all knock feature changed, mOpenGameBooster = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmOpenGameBooster(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :cond_9
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$mupdateKnockFeatureState(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    .line 542
    return-void
.end method
