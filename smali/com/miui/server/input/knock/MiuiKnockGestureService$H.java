class com.miui.server.input.knock.MiuiKnockGestureService$H extends android.os.Handler {
	 /* .source "MiuiKnockGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/knock/MiuiKnockGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
static final Integer MSG_KEY_SCREEN_STATE_CHANGED;
static final Integer MSG_KEY_STATUSBAR_CHANGED;
static final Integer MSG_KEY_UPDATE_FEATURE_STATE;
/* # instance fields */
final com.miui.server.input.knock.MiuiKnockGestureService this$0; //synthetic
/* # direct methods */
private com.miui.server.input.knock.MiuiKnockGestureService$H ( ) {
/* .locals 0 */
/* .line 283 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/Handler;-><init>()V */
return;
} // .end method
 com.miui.server.input.knock.MiuiKnockGestureService$H ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 289 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 298 */
/* :pswitch_0 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmIsScreenOn ( v0,v1 );
/* .line 299 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$mupdateKnockFeatureState ( v0 );
/* .line 300 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmKnockCheckDelegate ( v0 );
v1 = this.this$0;
v1 = com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmIsScreenOn ( v1 );
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).updateScreenState ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->updateScreenState(Z)V
/* .line 301 */
/* .line 295 */
/* :pswitch_1 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$mupdateKnockFeatureState ( v0 );
/* .line 296 */
/* .line 291 */
/* :pswitch_2 */
v0 = this.this$0;
v1 = this.obj;
/* check-cast v1, Ljava/lang/Boolean; */
v1 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmIsStatusBarVisible ( v0,v1 );
/* .line 292 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$mupdateKnockFeatureState ( v0 );
/* .line 293 */
/* nop */
/* .line 305 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
