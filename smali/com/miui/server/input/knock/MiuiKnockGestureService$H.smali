.class Lcom/miui/server/input/knock/MiuiKnockGestureService$H;
.super Landroid/os/Handler;
.source "MiuiKnockGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/MiuiKnockGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field static final MSG_KEY_SCREEN_STATE_CHANGED:I = 0x3

.field static final MSG_KEY_STATUSBAR_CHANGED:I = 0x1

.field static final MSG_KEY_UPDATE_FEATURE_STATE:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;


# direct methods
.method private constructor <init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;Lcom/miui/server/input/knock/MiuiKnockGestureService$H-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 289
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 298
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmIsScreenOn(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V

    .line 299
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$mupdateKnockFeatureState(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    .line 300
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmKnockCheckDelegate(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/KnockCheckDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmIsScreenOn(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->updateScreenState(Z)V

    .line 301
    goto :goto_0

    .line 295
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$mupdateKnockFeatureState(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    .line 296
    goto :goto_0

    .line 291
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmIsStatusBarVisible(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V

    .line 292
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$mupdateKnockFeatureState(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    .line 293
    nop

    .line 305
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
