public class com.miui.server.input.knock.config.KnockConfigHelper {
	 /* .source "KnockConfigHelper.java" */
	 /* # static fields */
	 private static final java.lang.String ConfigFilePath;
	 private static final java.lang.String TAG;
	 private static final java.util.List mConfigList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/miui/server/input/knock/config/KnockConfig;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private static volatile com.miui.server.input.knock.config.KnockConfigHelper sCloudConfigHelper;
/* # direct methods */
static com.miui.server.input.knock.config.KnockConfigHelper ( ) {
/* .locals 1 */
/* .line 23 */
int v0 = 0; // const/4 v0, 0x0
/* .line 25 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
return;
} // .end method
private com.miui.server.input.knock.config.KnockConfigHelper ( ) {
/* .locals 0 */
/* .line 28 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 29 */
return;
} // .end method
public static com.miui.server.input.knock.config.KnockConfigHelper getInstance ( ) {
/* .locals 2 */
/* .line 33 */
v0 = com.miui.server.input.knock.config.KnockConfigHelper.sCloudConfigHelper;
/* if-nez v0, :cond_1 */
/* .line 34 */
/* const-class v0, Lcom/miui/server/input/knock/config/KnockConfigHelper; */
/* monitor-enter v0 */
/* .line 35 */
try { // :try_start_0
	 v1 = com.miui.server.input.knock.config.KnockConfigHelper.sCloudConfigHelper;
	 /* if-nez v1, :cond_0 */
	 /* .line 36 */
	 /* new-instance v1, Lcom/miui/server/input/knock/config/KnockConfigHelper; */
	 /* invoke-direct {v1}, Lcom/miui/server/input/knock/config/KnockConfigHelper;-><init>()V */
	 /* .line 38 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 40 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.knock.config.KnockConfigHelper.sCloudConfigHelper;
} // .end method
private java.lang.StringBuilder parseFile ( java.io.File p0 ) {
/* .locals 5 */
/* .param p1, "configFile" # Ljava/io/File; */
/* .line 49 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
v1 = (( java.io.File ) p1 ).exists ( ); // invoke-virtual {p1}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
/* .line 50 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 51 */
/* .local v1, "stringBuilder":Ljava/lang/StringBuilder; */
int v2 = 0; // const/4 v2, 0x0
/* .line 53 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
try { // :try_start_0
/* new-instance v3, Ljava/io/BufferedReader; */
/* new-instance v4, Ljava/io/FileReader; */
/* invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* move-object v2, v3 */
/* .line 55 */
} // :goto_0
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 56 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 62 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_1
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 63 */
/* nop */
/* .line 64 */
/* .line 62 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 58 */
/* :catch_0 */
/* move-exception v3 */
/* .line 59 */
/* .local v3, "e":Ljava/lang/Exception; */
try { // :try_start_1
final String v4 = "KnockConfigHelper"; // const-string v4, "KnockConfigHelper"
android.util.Slog .w ( v4,v3 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 60 */
/* nop */
/* .line 62 */
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 60 */
/* .line 62 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_1
libcore.io.IoUtils .closeQuietly ( v2 );
/* .line 63 */
/* throw v0 */
/* .line 49 */
} // .end local v1 # "stringBuilder":Ljava/lang/StringBuilder;
} // .end local v2 # "reader":Ljava/io/BufferedReader;
} // :cond_2
} // :goto_2
} // .end method
private void parseKnockConfig ( ) {
/* .locals 18 */
/* .line 68 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/vendor/etc/knock-config.json"; // const-string v1, "/vendor/etc/knock-config.json"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* move-object/from16 v1, p0 */
/* invoke-direct {v1, v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->parseFile(Ljava/io/File;)Ljava/lang/StringBuilder; */
/* .line 69 */
/* .local v2, "stringBuilder":Ljava/lang/StringBuilder; */
/* if-nez v2, :cond_0 */
/* .line 70 */
return;
/* .line 72 */
} // :cond_0
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 73 */
/* .local v3, "fileContent":Ljava/lang/String; */
v0 = android.text.TextUtils .isEmpty ( v3 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 74 */
return;
/* .line 76 */
} // :cond_1
int v4 = 0; // const/4 v4, 0x0
/* .line 78 */
/* .local v4, "knockConfigJson":Lorg/json/JSONObject; */
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* move-object v4, v0 */
/* .line 79 */
final String v0 = "knockConfigs"; // const-string v0, "knockConfigs"
(( org.json.JSONObject ) v4 ).getJSONArray ( v0 ); // invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 81 */
/* .local v0, "knockConfigArray":Lorg/json/JSONArray; */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
v6 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v5, v6, :cond_6 */
/* .line 82 */
(( org.json.JSONArray ) v0 ).get ( v5 ); // invoke-virtual {v0, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;
/* check-cast v6, Lorg/json/JSONObject; */
/* .line 83 */
/* .local v6, "jsonObjectTemp":Lorg/json/JSONObject; */
/* new-instance v7, Lcom/miui/server/input/knock/config/KnockConfig; */
/* invoke-direct {v7}, Lcom/miui/server/input/knock/config/KnockConfig;-><init>()V */
/* .line 84 */
/* .local v7, "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig; */
final String v8 = "displayVersion"; // const-string v8, "displayVersion"
v8 = (( org.json.JSONObject ) v6 ).getInt ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v8, v7, Lcom/miui/server/input/knock/config/KnockConfig;->displayVersion:I */
/* .line 85 */
final String v8 = "deviceProperty"; // const-string v8, "deviceProperty"
v8 = (( org.json.JSONObject ) v6 ).getInt ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v8, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I */
/* .line 86 */
final String v8 = "localAlgorithmPath"; // const-string v8, "localAlgorithmPath"
(( org.json.JSONObject ) v6 ).getString ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
this.localAlgorithmPath = v8;
/* .line 87 */
/* new-instance v8, Landroid/graphics/Rect; */
int v9 = 0; // const/4 v9, 0x0
/* invoke-direct {v8, v9, v9, v9, v9}, Landroid/graphics/Rect;-><init>(IIII)V */
this.knockRegion = v8;
/* .line 88 */
final String v8 = "knockRegion"; // const-string v8, "knockRegion"
(( org.json.JSONObject ) v6 ).getString ( v8 ); // invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 89 */
/* .local v8, "knockRegion":Ljava/lang/String; */
v10 = (( java.lang.String ) v8 ).isEmpty ( ); // invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
int v11 = 4; // const/4 v11, 0x4
int v13 = 2; // const/4 v13, 0x2
final String v14 = ","; // const-string v14, ","
int v15 = 1; // const/4 v15, 0x1
/* if-nez v10, :cond_2 */
/* .line 90 */
try { // :try_start_1
(( java.lang.String ) v8 ).split ( v14 ); // invoke-virtual {v8, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 91 */
/* .local v10, "regionArray":[Ljava/lang/String; */
/* array-length v12, v10 */
/* if-ne v12, v11, :cond_2 */
/* .line 92 */
v12 = this.knockRegion;
/* aget-object v16, v10, v9 */
v11 = /* invoke-static/range {v16 ..v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* iput v11, v12, Landroid/graphics/Rect;->left:I */
/* .line 93 */
v11 = this.knockRegion;
/* aget-object v12, v10, v15 */
v12 = java.lang.Integer .parseInt ( v12 );
/* iput v12, v11, Landroid/graphics/Rect;->top:I */
/* .line 94 */
v11 = this.knockRegion;
/* aget-object v12, v10, v13 */
v12 = java.lang.Integer .parseInt ( v12 );
/* iput v12, v11, Landroid/graphics/Rect;->right:I */
/* .line 95 */
v11 = this.knockRegion;
int v12 = 3; // const/4 v12, 0x3
/* aget-object v16, v10, v12 */
v12 = /* invoke-static/range {v16 ..v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* iput v12, v11, Landroid/graphics/Rect;->bottom:I */
/* .line 98 */
} // .end local v10 # "regionArray":[Ljava/lang/String;
} // :cond_2
final String v10 = "deviceX"; // const-string v10, "deviceX"
(( org.json.JSONObject ) v6 ).getString ( v10 ); // invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 99 */
/* .local v10, "deviceX":Ljava/lang/String; */
/* filled-new-array {v15, v15}, [I */
this.deviceX = v11;
/* .line 100 */
v11 = (( java.lang.String ) v10 ).isEmpty ( ); // invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z
/* if-nez v11, :cond_3 */
/* .line 101 */
(( java.lang.String ) v10 ).split ( v14 ); // invoke-virtual {v10, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 102 */
/* .local v11, "deviceArray":[Ljava/lang/String; */
/* array-length v12, v11 */
/* if-ne v12, v13, :cond_3 */
/* .line 103 */
v12 = this.deviceX;
/* aget-object v16, v11, v9 */
v16 = /* invoke-static/range {v16 ..v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* aput v16, v12, v9 */
/* .line 104 */
v12 = this.deviceX;
/* aget-object v16, v11, v15 */
v16 = /* invoke-static/range {v16 ..v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I */
/* aput v16, v12, v15 */
/* .line 107 */
} // .end local v11 # "deviceArray":[Ljava/lang/String;
} // :cond_3
final String v11 = "knockScoreThreshold"; // const-string v11, "knockScoreThreshold"
(( org.json.JSONObject ) v6 ).getDouble ( v11 ); // invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v11 */
/* double-to-float v11, v11 */
/* iput v11, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockScoreThreshold:F */
/* .line 109 */
final String v11 = "deviceName"; // const-string v11, "deviceName"
(( org.json.JSONObject ) v6 ).getString ( v11 ); // invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 110 */
/* .local v11, "deviceName":Ljava/lang/String; */
v12 = android.text.TextUtils .isEmpty ( v11 );
/* if-nez v12, :cond_4 */
/* .line 111 */
(( java.lang.String ) v11 ).split ( v14 ); // invoke-virtual {v11, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
java.util.Arrays .asList ( v12 );
this.deviceName = v12;
/* .line 113 */
} // :cond_4
/* const-string/jumbo v12, "useFrame" */
v12 = (( org.json.JSONObject ) v6 ).getInt ( v12 ); // invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
/* iput v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->useFrame:I */
/* .line 114 */
final String v12 = "quickMoveSpeed"; // const-string v12, "quickMoveSpeed"
/* move-object/from16 v17, v14 */
(( org.json.JSONObject ) v6 ).getDouble ( v12 ); // invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D
/* move-result-wide v13 */
/* double-to-float v12, v13 */
/* iput v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->quickMoveSpeed:F */
/* .line 115 */
/* const-string/jumbo v12, "sensorThreshold" */
(( org.json.JSONObject ) v6 ).getString ( v12 ); // invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 116 */
/* .local v12, "sensorThreshold":Ljava/lang/String; */
/* filled-new-array {v9, v9, v9, v9}, [I */
this.sensorThreshold = v13;
/* .line 117 */
v13 = (( java.lang.String ) v12 ).isEmpty ( ); // invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z
/* if-nez v13, :cond_5 */
/* .line 118 */
/* move-object/from16 v13, v17 */
(( java.lang.String ) v12 ).split ( v13 ); // invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 119 */
/* .local v13, "sensorThresholdArray":[Ljava/lang/String; */
/* array-length v14, v13 */
int v15 = 4; // const/4 v15, 0x4
/* if-ne v14, v15, :cond_5 */
/* .line 120 */
v14 = this.sensorThreshold;
/* aget-object v15, v13, v9 */
v15 = java.lang.Integer .parseInt ( v15 );
/* aput v15, v14, v9 */
/* .line 121 */
v9 = this.sensorThreshold;
int v14 = 1; // const/4 v14, 0x1
/* aget-object v15, v13, v14 */
v15 = java.lang.Integer .parseInt ( v15 );
/* aput v15, v9, v14 */
/* .line 122 */
v9 = this.sensorThreshold;
int v14 = 2; // const/4 v14, 0x2
/* aget-object v15, v13, v14 */
v15 = java.lang.Integer .parseInt ( v15 );
/* aput v15, v9, v14 */
/* .line 123 */
v9 = this.sensorThreshold;
int v14 = 3; // const/4 v14, 0x3
/* aget-object v15, v13, v14 */
v15 = java.lang.Integer .parseInt ( v15 );
/* aput v15, v9, v14 */
/* .line 126 */
} // .end local v13 # "sensorThresholdArray":[Ljava/lang/String;
} // :cond_5
v9 = com.miui.server.input.knock.config.KnockConfigHelper.mConfigList;
/* :try_end_1 */
/* .catch Lorg/json/JSONException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 81 */
/* nop */
} // .end local v6 # "jsonObjectTemp":Lorg/json/JSONObject;
} // .end local v7 # "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
/* add-int/lit8 v5, v5, 0x1 */
/* goto/16 :goto_0 */
/* .line 130 */
} // .end local v0 # "knockConfigArray":Lorg/json/JSONArray;
} // .end local v5 # "i":I
} // .end local v8 # "knockRegion":Ljava/lang/String;
} // .end local v10 # "deviceX":Ljava/lang/String;
} // .end local v11 # "deviceName":Ljava/lang/String;
} // .end local v12 # "sensorThreshold":Ljava/lang/String;
} // :cond_6
/* .line 128 */
/* :catch_0 */
/* move-exception v0 */
/* .line 129 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 131 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public com.miui.server.input.knock.config.KnockConfig getKnockConfig ( java.util.List p0 ) {
/* .locals 5 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;", */
/* ">;)", */
/* "Lcom/miui/server/input/knock/config/KnockConfig;" */
/* } */
} // .end annotation
/* .line 138 */
/* .local p1, "knockConfigFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;>;" */
v0 = com.miui.server.input.knock.config.KnockConfigHelper.mConfigList;
/* .line 139 */
/* .local v0, "knockConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;" */
v2 = } // :goto_0
int v3 = 0; // const/4 v3, 0x0
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/input/knock/config/filter/KnockConfigFilter; */
/* .line 140 */
/* .local v2, "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter; */
/* .line 141 */
v4 = if ( v0 != null) { // if-eqz v0, :cond_1
/* if-nez v4, :cond_0 */
/* .line 144 */
} // .end local v2 # "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;
} // :cond_0
/* .line 142 */
/* .restart local v2 # "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter; */
} // :cond_1
} // :goto_1
/* .line 146 */
} // .end local v2 # "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;
v1 = } // :cond_2
int v2 = 1; // const/4 v2, 0x1
/* if-ne v1, v2, :cond_3 */
/* .line 147 */
int v1 = 0; // const/4 v1, 0x0
/* check-cast v1, Lcom/miui/server/input/knock/config/KnockConfig; */
/* .line 149 */
} // :cond_3
} // .end method
public void initConfig ( ) {
/* .locals 0 */
/* .line 44 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->parseKnockConfig()V */
/* .line 45 */
return;
} // .end method
public Boolean isDeviceSupport ( ) {
/* .locals 1 */
/* .line 134 */
v0 = v0 = com.miui.server.input.knock.config.KnockConfigHelper.mConfigList;
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
