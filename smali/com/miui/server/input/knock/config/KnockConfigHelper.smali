.class public Lcom/miui/server/input/knock/config/KnockConfigHelper;
.super Ljava/lang/Object;
.source "KnockConfigHelper.java"


# static fields
.field private static final ConfigFilePath:Ljava/lang/String; = "/vendor/etc/knock-config.json"

.field private static final TAG:Ljava/lang/String; = "KnockConfigHelper"

.field private static final mConfigList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sCloudConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;->sCloudConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;->mConfigList:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/knock/config/KnockConfigHelper;
    .locals 2

    .line 33
    sget-object v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;->sCloudConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    if-nez v0, :cond_1

    .line 34
    const-class v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;

    monitor-enter v0

    .line 35
    :try_start_0
    sget-object v1, Lcom/miui/server/input/knock/config/KnockConfigHelper;->sCloudConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    if-nez v1, :cond_0

    .line 36
    new-instance v1, Lcom/miui/server/input/knock/config/KnockConfigHelper;

    invoke-direct {v1}, Lcom/miui/server/input/knock/config/KnockConfigHelper;-><init>()V

    sput-object v1, Lcom/miui/server/input/knock/config/KnockConfigHelper;->sCloudConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    .line 38
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 40
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;->sCloudConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    return-object v0
.end method

.method private parseFile(Ljava/io/File;)Ljava/lang/StringBuilder;
    .locals 5
    .param p1, "configFile"    # Ljava/io/File;

    .line 49
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 50
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .local v1, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 53
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    .line 55
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "line":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 56
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 62
    .end local v4    # "line":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 63
    nop

    .line 64
    return-object v1

    .line 62
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 58
    :catch_0
    move-exception v3

    .line 59
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v4, "KnockConfigHelper"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    nop

    .line 62
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 60
    return-object v0

    .line 62
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v2}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 63
    throw v0

    .line 49
    .end local v1    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_2
    return-object v0
.end method

.method private parseKnockConfig()V
    .locals 18

    .line 68
    new-instance v0, Ljava/io/File;

    const-string v1, "/vendor/etc/knock-config.json"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->parseFile(Ljava/io/File;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 69
    .local v2, "stringBuilder":Ljava/lang/StringBuilder;
    if-nez v2, :cond_0

    .line 70
    return-void

    .line 72
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 73
    .local v3, "fileContent":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    return-void

    .line 76
    :cond_1
    const/4 v4, 0x0

    .line 78
    .local v4, "knockConfigJson":Lorg/json/JSONObject;
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object v4, v0

    .line 79
    const-string v0, "knockConfigs"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 81
    .local v0, "knockConfigArray":Lorg/json/JSONArray;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v5, v6, :cond_6

    .line 82
    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/json/JSONObject;

    .line 83
    .local v6, "jsonObjectTemp":Lorg/json/JSONObject;
    new-instance v7, Lcom/miui/server/input/knock/config/KnockConfig;

    invoke-direct {v7}, Lcom/miui/server/input/knock/config/KnockConfig;-><init>()V

    .line 84
    .local v7, "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
    const-string v8, "displayVersion"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/miui/server/input/knock/config/KnockConfig;->displayVersion:I

    .line 85
    const-string v8, "deviceProperty"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I

    .line 86
    const-string v8, "localAlgorithmPath"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/miui/server/input/knock/config/KnockConfig;->localAlgorithmPath:Ljava/lang/String;

    .line 87
    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    invoke-direct {v8, v9, v9, v9, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v8, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    .line 88
    const-string v8, "knockRegion"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 89
    .local v8, "knockRegion":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v10
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v11, 0x4

    const/4 v13, 0x2

    const-string v14, ","

    const/4 v15, 0x1

    if-nez v10, :cond_2

    .line 90
    :try_start_1
    invoke-virtual {v8, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 91
    .local v10, "regionArray":[Ljava/lang/String;
    array-length v12, v10

    if-ne v12, v11, :cond_2

    .line 92
    iget-object v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    aget-object v16, v10, v9

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    iput v11, v12, Landroid/graphics/Rect;->left:I

    .line 93
    iget-object v11, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    aget-object v12, v10, v15

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->top:I

    .line 94
    iget-object v11, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    aget-object v12, v10, v13

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->right:I

    .line 95
    iget-object v11, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    const/4 v12, 0x3

    aget-object v16, v10, v12

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v11, Landroid/graphics/Rect;->bottom:I

    .line 98
    .end local v10    # "regionArray":[Ljava/lang/String;
    :cond_2
    const-string v10, "deviceX"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 99
    .local v10, "deviceX":Ljava/lang/String;
    filled-new-array {v15, v15}, [I

    move-result-object v11

    iput-object v11, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    .line 100
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 101
    invoke-virtual {v10, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 102
    .local v11, "deviceArray":[Ljava/lang/String;
    array-length v12, v11

    if-ne v12, v13, :cond_3

    .line 103
    iget-object v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    aget-object v16, v11, v9

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    aput v16, v12, v9

    .line 104
    iget-object v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    aget-object v16, v11, v15

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    aput v16, v12, v15

    .line 107
    .end local v11    # "deviceArray":[Ljava/lang/String;
    :cond_3
    const-string v11, "knockScoreThreshold"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v11

    double-to-float v11, v11

    iput v11, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockScoreThreshold:F

    .line 109
    const-string v11, "deviceName"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 110
    .local v11, "deviceName":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 111
    invoke-virtual {v11, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    iput-object v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceName:Ljava/util/List;

    .line 113
    :cond_4
    const-string/jumbo v12, "useFrame"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    iput v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->useFrame:I

    .line 114
    const-string v12, "quickMoveSpeed"

    move-object/from16 v17, v14

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v13

    double-to-float v12, v13

    iput v12, v7, Lcom/miui/server/input/knock/config/KnockConfig;->quickMoveSpeed:F

    .line 115
    const-string/jumbo v12, "sensorThreshold"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 116
    .local v12, "sensorThreshold":Ljava/lang/String;
    filled-new-array {v9, v9, v9, v9}, [I

    move-result-object v13

    iput-object v13, v7, Lcom/miui/server/input/knock/config/KnockConfig;->sensorThreshold:[I

    .line 117
    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_5

    .line 118
    move-object/from16 v13, v17

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 119
    .local v13, "sensorThresholdArray":[Ljava/lang/String;
    array-length v14, v13

    const/4 v15, 0x4

    if-ne v14, v15, :cond_5

    .line 120
    iget-object v14, v7, Lcom/miui/server/input/knock/config/KnockConfig;->sensorThreshold:[I

    aget-object v15, v13, v9

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    aput v15, v14, v9

    .line 121
    iget-object v9, v7, Lcom/miui/server/input/knock/config/KnockConfig;->sensorThreshold:[I

    const/4 v14, 0x1

    aget-object v15, v13, v14

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    aput v15, v9, v14

    .line 122
    iget-object v9, v7, Lcom/miui/server/input/knock/config/KnockConfig;->sensorThreshold:[I

    const/4 v14, 0x2

    aget-object v15, v13, v14

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    aput v15, v9, v14

    .line 123
    iget-object v9, v7, Lcom/miui/server/input/knock/config/KnockConfig;->sensorThreshold:[I

    const/4 v14, 0x3

    aget-object v15, v13, v14

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    aput v15, v9, v14

    .line 126
    .end local v13    # "sensorThresholdArray":[Ljava/lang/String;
    :cond_5
    sget-object v9, Lcom/miui/server/input/knock/config/KnockConfigHelper;->mConfigList:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 81
    nop

    .end local v6    # "jsonObjectTemp":Lorg/json/JSONObject;
    .end local v7    # "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 130
    .end local v0    # "knockConfigArray":Lorg/json/JSONArray;
    .end local v5    # "i":I
    .end local v8    # "knockRegion":Ljava/lang/String;
    .end local v10    # "deviceX":Ljava/lang/String;
    .end local v11    # "deviceName":Ljava/lang/String;
    .end local v12    # "sensorThreshold":Ljava/lang/String;
    :cond_6
    goto :goto_1

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 131
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method


# virtual methods
.method public getKnockConfig(Ljava/util/List;)Lcom/miui/server/input/knock/config/KnockConfig;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;",
            ">;)",
            "Lcom/miui/server/input/knock/config/KnockConfig;"
        }
    .end annotation

    .line 138
    .local p1, "knockConfigFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;>;"
    sget-object v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;->mConfigList:Ljava/util/List;

    .line 139
    .local v0, "knockConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;

    .line 140
    .local v2, "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;
    invoke-interface {v2, v0}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;->knockConfigFilter(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    .line 144
    .end local v2    # "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;
    :cond_0
    goto :goto_0

    .line 142
    .restart local v2    # "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;
    :cond_1
    :goto_1
    return-object v3

    .line 146
    .end local v2    # "knockConfigFilter":Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 147
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/knock/config/KnockConfig;

    return-object v1

    .line 149
    :cond_3
    return-object v3
.end method

.method public initConfig()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->parseKnockConfig()V

    .line 45
    return-void
.end method

.method public isDeviceSupport()Z
    .locals 1

    .line 134
    sget-object v0, Lcom/miui/server/input/knock/config/KnockConfigHelper;->mConfigList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
