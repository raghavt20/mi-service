.class public Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;
.super Ljava/lang/Object;
.source "KnockConfigFilterDeviceName.java"

# interfaces
.implements Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;


# instance fields
.field private final mDeviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->mDeviceName:Ljava/lang/String;

    .line 14
    return-void
.end method

.method private findKnockConfig(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;"
        }
    .end annotation

    .line 27
    .local p1, "knockConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .local v0, "knockConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;"
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->mDeviceName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 29
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/knock/config/KnockConfig;

    .line 30
    .local v2, "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
    invoke-direct {p0, v2}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->findKnockConfigDeviceName(Lcom/miui/server/input/knock/config/KnockConfig;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 31
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .end local v2    # "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
    :cond_0
    goto :goto_0

    .line 35
    :cond_1
    return-object v0
.end method

.method private findKnockConfigDeviceName(Lcom/miui/server/input/knock/config/KnockConfig;)Z
    .locals 3
    .param p1, "knockConfig"    # Lcom/miui/server/input/knock/config/KnockConfig;

    .line 39
    iget-object v0, p1, Lcom/miui/server/input/knock/config/KnockConfig;->deviceName:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/miui/server/input/knock/config/KnockConfig;->deviceName:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 40
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p1, Lcom/miui/server/input/knock/config/KnockConfig;->deviceName:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 41
    iget-object v1, p0, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->mDeviceName:Ljava/lang/String;

    iget-object v2, p1, Lcom/miui/server/input/knock/config/KnockConfig;->deviceName:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    const/4 v1, 0x1

    return v1

    .line 40
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public knockConfigFilter(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;"
        }
    .end annotation

    .line 18
    .local p1, "knockConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;"
    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->findKnockConfig(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
