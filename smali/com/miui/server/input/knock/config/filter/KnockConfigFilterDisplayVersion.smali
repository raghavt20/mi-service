.class public Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;
.super Ljava/lang/Object;
.source "KnockConfigFilterDisplayVersion.java"

# interfaces
.implements Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;


# instance fields
.field private final mDisplayVersion:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "displayVersion"    # Ljava/lang/String;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;->mDisplayVersion:I

    .line 14
    return-void
.end method


# virtual methods
.method public knockConfigFilter(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/config/KnockConfig;",
            ">;"
        }
    .end annotation

    .line 18
    .local p1, "knockConfigsList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .local v0, "knockConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;"
    if-eqz p1, :cond_1

    .line 20
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/input/knock/config/KnockConfig;

    .line 21
    .local v2, "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
    iget v3, p0, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;->mDisplayVersion:I

    iget v4, v2, Lcom/miui/server/input/knock/config/KnockConfig;->displayVersion:I

    if-ne v3, v4, :cond_0

    .line 22
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    .end local v2    # "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
    :cond_0
    goto :goto_0

    .line 26
    :cond_1
    return-object v0
.end method
