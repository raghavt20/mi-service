public class com.miui.server.input.knock.config.filter.KnockConfigFilterDeviceName implements com.miui.server.input.knock.config.filter.KnockConfigFilter {
	 /* .source "KnockConfigFilterDeviceName.java" */
	 /* # interfaces */
	 /* # instance fields */
	 private final java.lang.String mDeviceName;
	 /* # direct methods */
	 public com.miui.server.input.knock.config.filter.KnockConfigFilterDeviceName ( ) {
		 /* .locals 0 */
		 /* .param p1, "deviceName" # Ljava/lang/String; */
		 /* .line 12 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 13 */
		 this.mDeviceName = p1;
		 /* .line 14 */
		 return;
	 } // .end method
	 private java.util.List findKnockConfig ( java.util.List p0 ) {
		 /* .locals 4 */
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Lcom/miui/server/input/knock/config/KnockConfig;", */
		 /* ">;)", */
		 /* "Ljava/util/List<", */
		 /* "Lcom/miui/server/input/knock/config/KnockConfig;", */
		 /* ">;" */
		 /* } */
	 } // .end annotation
	 /* .line 27 */
	 /* .local p1, "knockConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;" */
	 /* new-instance v0, Ljava/util/ArrayList; */
	 /* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
	 /* .line 28 */
	 /* .local v0, "knockConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;" */
	 if ( p1 != null) { // if-eqz p1, :cond_1
		 v1 = this.mDeviceName;
		 if ( v1 != null) { // if-eqz v1, :cond_1
			 /* .line 29 */
		 v2 = 		 } // :goto_0
		 if ( v2 != null) { // if-eqz v2, :cond_1
			 /* check-cast v2, Lcom/miui/server/input/knock/config/KnockConfig; */
			 /* .line 30 */
			 /* .local v2, "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig; */
			 v3 = 			 /* invoke-direct {p0, v2}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->findKnockConfigDeviceName(Lcom/miui/server/input/knock/config/KnockConfig;)Z */
			 if ( v3 != null) { // if-eqz v3, :cond_0
				 /* .line 31 */
				 /* .line 33 */
			 } // .end local v2 # "knockConfig":Lcom/miui/server/input/knock/config/KnockConfig;
		 } // :cond_0
		 /* .line 35 */
	 } // :cond_1
} // .end method
private Boolean findKnockConfigDeviceName ( com.miui.server.input.knock.config.KnockConfig p0 ) {
	 /* .locals 3 */
	 /* .param p1, "knockConfig" # Lcom/miui/server/input/knock/config/KnockConfig; */
	 /* .line 39 */
	 v0 = this.deviceName;
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 v0 = 		 v0 = this.deviceName;
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 /* .line 40 */
			 int v0 = 0; // const/4 v0, 0x0
			 /* .local v0, "i":I */
		 } // :goto_0
		 v1 = 		 v1 = this.deviceName;
		 /* if-ge v0, v1, :cond_1 */
		 /* .line 41 */
		 v1 = this.mDeviceName;
		 v2 = this.deviceName;
		 v1 = 		 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 42 */
			 int v1 = 1; // const/4 v1, 0x1
			 /* .line 40 */
		 } // :cond_0
		 /* add-int/lit8 v0, v0, 0x1 */
		 /* .line 46 */
	 } // .end local v0 # "i":I
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
/* # virtual methods */
public java.util.List knockConfigFilter ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/knock/config/KnockConfig;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/input/knock/config/KnockConfig;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 18 */
/* .local p1, "knockConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/KnockConfig;>;" */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;->findKnockConfig(Ljava/util/List;)Ljava/util/List; */
} // .end method
