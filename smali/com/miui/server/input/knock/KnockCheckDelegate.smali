.class public Lcom/miui/server/input/knock/KnockCheckDelegate;
.super Ljava/lang/Object;
.source "KnockCheckDelegate.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "KnockCheckDelegate"


# instance fields
.field private mCurrentChecker:Lcom/miui/server/input/knock/KnockGestureChecker;

.field private mKnockGesturePathView:Lcom/miui/server/input/knock/view/KnockGesturePathView;

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/knock/KnockGestureChecker;",
            ">;"
        }
    .end annotation
.end field

.field private mMultipleTouch:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    .line 37
    new-instance v0, Lcom/miui/server/input/knock/view/KnockGesturePathView;

    invoke-direct {v0, p1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mKnockGesturePathView:Lcom/miui/server/input/knock/view/KnockGesturePathView;

    .line 38
    return-void
.end method

.method private resetState()V
    .locals 2

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mCurrentChecker:Lcom/miui/server/input/knock/KnockGestureChecker;

    .line 84
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/knock/KnockGestureChecker;

    invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->resetState()V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    .end local v0    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onScreenSizeChanged()V
    .locals 2

    .line 96
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/knock/KnockGestureChecker;

    invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->onScreenSizeChanged()V

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 52
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mKnockGesturePathView:Lcom/miui/server/input/knock/view/KnockGesturePathView;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->onPointerEvent(Landroid/view/MotionEvent;)V

    .line 53
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/miui/server/input/knock/KnockCheckDelegate;->resetState()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z

    .line 60
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z

    if-eqz v0, :cond_2

    .line 61
    return-void

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mCurrentChecker:Lcom/miui/server/input/knock/KnockGestureChecker;

    if-eqz v0, :cond_3

    .line 65
    invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 66
    return-void

    .line 68
    :cond_3
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 69
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 70
    iget-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/knock/KnockGestureChecker;

    .line 71
    .local v1, "knockGestureChecker":Lcom/miui/server/input/knock/KnockGestureChecker;
    invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 72
    invoke-virtual {v1, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 73
    invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->checkOnlyOneGesture()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 74
    iput-object v1, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mCurrentChecker:Lcom/miui/server/input/knock/KnockGestureChecker;

    .line 75
    return-void

    .line 69
    .end local v1    # "knockGestureChecker":Lcom/miui/server/input/knock/KnockGestureChecker;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 80
    .end local v0    # "i":I
    :cond_5
    return-void
.end method

.method public registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V
    .locals 2
    .param p1, "knockGestureChecker"    # Lcom/miui/server/input/knock/KnockGestureChecker;

    .line 42
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "repeat register knock checker :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KnockCheckDelegate"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mKnockGesturePathView:Lcom/miui/server/input/knock/view/KnockGesturePathView;

    invoke-virtual {p1, v0}, Lcom/miui/server/input/knock/KnockGestureChecker;->setKnockPathListener(Lcom/miui/server/input/knock/view/KnockPathListener;)V

    .line 47
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_1
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 1
    .param p1, "screenOn"    # Z

    .line 90
    if-nez p1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mKnockGesturePathView:Lcom/miui/server/input/knock/view/KnockGesturePathView;

    invoke-virtual {v0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->removeViewImmediate()V

    .line 93
    :cond_0
    return-void
.end method
