public class com.miui.server.input.knock.KnockCheckDelegate {
	 /* .source "KnockCheckDelegate.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private com.miui.server.input.knock.KnockGestureChecker mCurrentChecker;
	 private com.miui.server.input.knock.view.KnockGesturePathView mKnockGesturePathView;
	 private java.util.List mListeners;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Lcom/miui/server/input/knock/KnockGestureChecker;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Boolean mMultipleTouch;
/* # direct methods */
public com.miui.server.input.knock.KnockCheckDelegate ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 33 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z */
/* .line 36 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mListeners = v0;
/* .line 37 */
/* new-instance v0, Lcom/miui/server/input/knock/view/KnockGesturePathView; */
/* invoke-direct {v0, p1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;-><init>(Landroid/content/Context;)V */
this.mKnockGesturePathView = v0;
/* .line 38 */
return;
} // .end method
private void resetState ( ) {
/* .locals 2 */
/* .line 83 */
int v0 = 0; // const/4 v0, 0x0
this.mCurrentChecker = v0;
/* .line 84 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = this.mListeners;
/* if-ge v0, v1, :cond_0 */
/* .line 85 */
v1 = this.mListeners;
/* check-cast v1, Lcom/miui/server/input/knock/KnockGestureChecker; */
(( com.miui.server.input.knock.KnockGestureChecker ) v1 ).resetState ( ); // invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->resetState()V
/* .line 84 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 87 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void onScreenSizeChanged ( ) {
/* .locals 2 */
/* .line 96 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = this.mListeners;
/* if-ge v0, v1, :cond_0 */
/* .line 97 */
v1 = this.mListeners;
/* check-cast v1, Lcom/miui/server/input/knock/KnockGestureChecker; */
(( com.miui.server.input.knock.KnockGestureChecker ) v1 ).onScreenSizeChanged ( ); // invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->onScreenSizeChanged()V
/* .line 96 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 99 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 52 */
v0 = this.mKnockGesturePathView;
(( com.miui.server.input.knock.view.KnockGesturePathView ) v0 ).onPointerEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->onPointerEvent(Landroid/view/MotionEvent;)V
/* .line 53 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* if-nez v0, :cond_0 */
/* .line 54 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/KnockCheckDelegate;->resetState()V */
/* .line 55 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z */
/* .line 56 */
} // :cond_0
v0 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
int v1 = 5; // const/4 v1, 0x5
/* if-ne v0, v1, :cond_1 */
/* .line 57 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z */
/* .line 60 */
} // :cond_1
} // :goto_0
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/KnockCheckDelegate;->mMultipleTouch:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 61 */
return;
/* .line 64 */
} // :cond_2
v0 = this.mCurrentChecker;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 65 */
(( com.miui.server.input.knock.KnockGestureChecker ) v0 ).onTouchEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;->onTouchEvent(Landroid/view/MotionEvent;)V
/* .line 66 */
return;
/* .line 68 */
} // :cond_3
v0 = this.mListeners;
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_1
v1 = v1 = this.mListeners;
/* if-ge v0, v1, :cond_5 */
/* .line 70 */
v1 = this.mListeners;
/* check-cast v1, Lcom/miui/server/input/knock/KnockGestureChecker; */
/* .line 71 */
/* .local v1, "knockGestureChecker":Lcom/miui/server/input/knock/KnockGestureChecker; */
v2 = (( com.miui.server.input.knock.KnockGestureChecker ) v1 ).continueCheck ( ); // invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 72 */
(( com.miui.server.input.knock.KnockGestureChecker ) v1 ).onTouchEvent ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;->onTouchEvent(Landroid/view/MotionEvent;)V
/* .line 73 */
v2 = (( com.miui.server.input.knock.KnockGestureChecker ) v1 ).checkOnlyOneGesture ( ); // invoke-virtual {v1}, Lcom/miui/server/input/knock/KnockGestureChecker;->checkOnlyOneGesture()Z
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 74 */
this.mCurrentChecker = v1;
/* .line 75 */
return;
/* .line 69 */
} // .end local v1 # "knockGestureChecker":Lcom/miui/server/input/knock/KnockGestureChecker;
} // :cond_4
/* add-int/lit8 v0, v0, 0x1 */
/* .line 80 */
} // .end local v0 # "i":I
} // :cond_5
return;
} // .end method
public void registerKnockChecker ( com.miui.server.input.knock.KnockGestureChecker p0 ) {
/* .locals 2 */
/* .param p1, "knockGestureChecker" # Lcom/miui/server/input/knock/KnockGestureChecker; */
/* .line 42 */
v0 = v0 = this.mListeners;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 43 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "repeat register knock checker :"; // const-string v1, "repeat register knock checker :"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.Object ) p1 ).getClass ( ); // invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
(( java.lang.Class ) v1 ).getSimpleName ( ); // invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "KnockCheckDelegate"; // const-string v1, "KnockCheckDelegate"
android.util.Slog .w ( v1,v0 );
/* .line 45 */
} // :cond_0
v0 = v0 = this.mListeners;
/* if-nez v0, :cond_1 */
/* .line 46 */
v0 = this.mKnockGesturePathView;
(( com.miui.server.input.knock.KnockGestureChecker ) p1 ).setKnockPathListener ( v0 ); // invoke-virtual {p1, v0}, Lcom/miui/server/input/knock/KnockGestureChecker;->setKnockPathListener(Lcom/miui/server/input/knock/view/KnockPathListener;)V
/* .line 47 */
v0 = this.mListeners;
/* .line 49 */
} // :cond_1
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "screenOn" # Z */
/* .line 90 */
/* if-nez p1, :cond_0 */
/* .line 91 */
v0 = this.mKnockGesturePathView;
(( com.miui.server.input.knock.view.KnockGesturePathView ) v0 ).removeViewImmediate ( ); // invoke-virtual {v0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->removeViewImmediate()V
/* .line 93 */
} // :cond_0
return;
} // .end method
