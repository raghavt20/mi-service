.class Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;
.super Landroid/content/BroadcastReceiver;
.source "KnockGestureHorizontalBrightness.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;


# direct methods
.method constructor <init>(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    .line 49
    iput-object p1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;->this$0:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 52
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;->this$0:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->-$$Nest$fputmDisplayMetrics(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;Landroid/util/DisplayMetrics;)V

    .line 53
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;->this$0:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    invoke-static {v0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->-$$Nest$fgetmDisplayMetrics(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;->this$0:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    invoke-static {v2}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->-$$Nest$fgetmDisplayMetrics(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-double v1, v1

    const-wide v3, 0x3fdded288ce703b0L    # 0.4676

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->-$$Nest$fputmBrightnessViewLength(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;I)V

    .line 55
    return-void
.end method
