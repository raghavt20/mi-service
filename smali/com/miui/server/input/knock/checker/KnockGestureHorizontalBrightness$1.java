class com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$1 extends android.content.BroadcastReceiver {
	 /* .source "KnockGestureHorizontalBrightness.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness this$0; //synthetic
/* # direct methods */
 com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness; */
/* .line 49 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 5 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 52 */
v0 = this.this$0;
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getDisplayMetrics ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness .-$$Nest$fputmDisplayMetrics ( v0,v1 );
/* .line 53 */
v0 = this.this$0;
com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness .-$$Nest$fgetmDisplayMetrics ( v0 );
/* iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I */
v2 = this.this$0;
com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness .-$$Nest$fgetmDisplayMetrics ( v2 );
/* iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I */
v1 = java.lang.Math .min ( v1,v2 );
/* int-to-double v1, v1 */
/* const-wide v3, 0x3fdded288ce703b0L # 0.4676 */
/* mul-double/2addr v1, v3 */
/* double-to-int v1, v1 */
com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness .-$$Nest$fputmBrightnessViewLength ( v0,v1 );
/* .line 55 */
return;
} // .end method
