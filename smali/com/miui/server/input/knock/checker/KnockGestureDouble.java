public class com.miui.server.input.knock.checker.KnockGestureDouble extends com.miui.server.input.knock.KnockGestureChecker {
	 /* .source "KnockGestureDouble.java" */
	 /* # static fields */
	 private static final Integer DOUBLE_KNOCK_MAX_MOVE_DISTANCE;
	 private static final Integer DOUBLE_KNOCK_TIME;
	 private static final Integer DOUBLE_KNOCK_VALID_DISTANCE;
	 public static final java.lang.String TAG;
	 /* # instance fields */
	 private Integer mDoubleKnockValidDistance;
	 private Float mLastKnockDownX;
	 private Float mLastKnockDownY;
	 private Long mLastKnockTime;
	 private Long mLastTriggerTime;
	 private Integer mMaxMoveDistance;
	 /* # direct methods */
	 public com.miui.server.input.knock.checker.KnockGestureDouble ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 36 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V */
		 /* .line 37 */
		 /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->initScreenSize()V */
		 /* .line 38 */
		 return;
	 } // .end method
	 private void initScreenSize ( ) {
		 /* .locals 6 */
		 /* .line 99 */
		 v0 = this.mContext;
		 /* const-string/jumbo v1, "window" */
		 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/view/WindowManager; */
		 /* .line 100 */
		 /* .local v0, "mWindowManager":Landroid/view/WindowManager; */
		 /* .line 101 */
		 /* .local v1, "defaultDisplay":Landroid/view/Display; */
		 /* new-instance v2, Landroid/util/DisplayMetrics; */
		 /* invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V */
		 /* .line 102 */
		 /* .local v2, "dm":Landroid/util/DisplayMetrics; */
		 (( android.view.Display ) v1 ).getRealMetrics ( v2 ); // invoke-virtual {v1, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V
		 /* .line 103 */
		 /* iget v3, v2, Landroid/util/DisplayMetrics;->densityDpi:I */
		 /* int-to-float v3, v3 */
		 /* const v4, 0x40228f5c # 2.54f */
		 /* div-float/2addr v3, v4 */
		 /* .line 104 */
		 /* .local v3, "oneCentimeterPix":F */
		 /* const/high16 v4, 0x40000000 # 2.0f */
		 /* mul-float/2addr v4, v3 */
		 /* float-to-int v4, v4 */
		 /* const/16 v5, 0x96 */
		 v4 = 		 java.lang.Math .max ( v4,v5 );
		 /* iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I */
		 /* .line 105 */
		 /* const/high16 v4, 0x3f800000 # 1.0f */
		 /* mul-float/2addr v4, v3 */
		 /* float-to-int v4, v4 */
		 /* const/16 v5, 0x64 */
		 v4 = 		 java.lang.Math .max ( v4,v5 );
		 /* iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I */
		 /* .line 106 */
		 return;
	 } // .end method
	 private void triggerDoubleKnockFeature ( ) {
		 /* .locals 7 */
		 /* .line 82 */
		 v0 = this.mKnockPathListener;
		 /* .line 83 */
		 (( com.miui.server.input.knock.checker.KnockGestureDouble ) p0 ).setCheckSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->setCheckSuccess()V
		 /* .line 84 */
		 java.lang.System .currentTimeMillis ( );
		 /* move-result-wide v0 */
		 /* .line 85 */
		 /* .local v0, "nowTime":J */
		 /* iget-wide v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastTriggerTime:J */
		 /* sub-long v2, v0, v2 */
		 /* const-wide/16 v4, 0x1f4 */
		 /* cmp-long v2, v2, v4 */
		 /* if-gez v2, :cond_0 */
		 /* .line 86 */
		 return;
		 /* .line 88 */
	 } // :cond_0
	 /* iput-wide v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastTriggerTime:J */
	 /* .line 89 */
	 v2 = this.mContext;
	 com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v2 );
	 v3 = this.mFunction;
	 int v4 = 0; // const/4 v4, 0x0
	 int v5 = 1; // const/4 v5, 0x1
	 final String v6 = "double_knock"; // const-string v6, "double_knock"
	 (( com.miui.server.input.util.ShortCutActionsUtils ) v2 ).triggerFunction ( v3, v6, v4, v5 ); // invoke-virtual {v2, v3, v6, v4, v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
	 /* .line 90 */
	 return;
} // .end method
/* # virtual methods */
public Boolean continueCheck ( ) {
	 /* .locals 1 */
	 /* .line 42 */
	 v0 = this.mFunction;
	 v0 = 	 (( com.miui.server.input.knock.checker.KnockGestureDouble ) p0 ).checkEmpty ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->checkEmpty(Ljava/lang/String;)Z
	 /* if-nez v0, :cond_0 */
	 /* .line 43 */
	 v0 = 	 /* invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z */
	 /* .line 45 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onScreenSizeChanged ( ) {
/* .locals 0 */
/* .line 94 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->initScreenSize()V */
/* .line 95 */
return;
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 13 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 51 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* .line 52 */
/* .local v0, "action":I */
v1 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* .line 53 */
/* .local v1, "nowX":F */
v2 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* .line 55 */
/* .local v2, "nowY":F */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* .line 56 */
/* .local v3, "nowTime":J */
/* const-wide/16 v5, 0x0 */
final String v7 = "KnockGestureDouble"; // const-string v7, "KnockGestureDouble"
int v8 = 0; // const/4 v8, 0x0
/* if-nez v0, :cond_1 */
/* .line 57 */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "knock down, time:"; // const-string v10, "knock down, time:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v3, v4 ); // invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = " nowX:"; // const-string v10, " nowX:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v1 ); // invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v10 = " nowY:"; // const-string v10, " nowY:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v2 ); // invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v10 = " mDoubleKnockValidDistance:"; // const-string v10, " mDoubleKnockValidDistance:"
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I */
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v9 );
/* .line 58 */
/* iget-wide v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J */
/* sub-long v9, v3, v9 */
/* const-wide/16 v11, 0x1f4 */
/* cmp-long v7, v9, v11 */
/* if-gez v7, :cond_0 */
/* iget v7, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F */
/* sub-float v7, v1, v7 */
/* .line 59 */
v7 = java.lang.Math .abs ( v7 );
/* iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I */
/* int-to-float v9, v9 */
/* cmpg-float v7, v7, v9 */
/* if-gez v7, :cond_0 */
/* iget v7, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F */
/* sub-float v7, v2, v7 */
/* .line 60 */
v7 = java.lang.Math .abs ( v7 );
/* iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I */
/* int-to-float v9, v9 */
/* cmpg-float v7, v7, v9 */
/* if-gez v7, :cond_0 */
/* .line 61 */
/* iput-wide v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J */
/* .line 62 */
/* iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F */
/* .line 63 */
/* iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F */
/* .line 64 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->triggerDoubleKnockFeature()V */
/* .line 66 */
} // :cond_0
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F */
/* .line 67 */
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F */
/* .line 68 */
/* iput-wide v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J */
/* .line 70 */
} // :cond_1
int v9 = 2; // const/4 v9, 0x2
/* if-ne v0, v9, :cond_3 */
/* .line 72 */
/* iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F */
/* sub-float v9, v1, v9 */
v9 = java.lang.Math .abs ( v9 );
/* iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I */
/* int-to-float v10, v10 */
/* cmpl-float v9, v9, v10 */
/* if-gtz v9, :cond_2 */
/* iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F */
/* sub-float v9, v2, v9 */
v9 = java.lang.Math .abs ( v9 );
/* iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I */
/* int-to-float v10, v10 */
/* cmpl-float v9, v9, v10 */
/* if-lez v9, :cond_3 */
/* .line 73 */
} // :cond_2
/* iput-wide v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J */
/* .line 74 */
/* iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F */
/* .line 75 */
/* iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F */
/* .line 76 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "knock move more than mMaxMoveDistance:"; // const-string v6, "knock move more than mMaxMoveDistance:"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v6, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I */
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v7,v5 );
/* .line 79 */
} // :cond_3
} // :goto_0
return;
} // .end method
