.class public Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;
.super Lcom/miui/server/input/knock/KnockGestureChecker;
.source "KnockGestureHorizontalBrightness.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;
    }
.end annotation


# static fields
.field private static final BRIGHTNESS_NO_CHANGE:I = -0x1

.field private static final LONG_PRESS_TIME:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "KnockBrightness"


# instance fields
.field private mBrightnessMax:F

.field private mBrightnessMin:F

.field private mBrightnessViewLength:I

.field private mConfigurationReceiver:Landroid/content/BroadcastReceiver;

.field private final mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDownBrightness:F

.field private mDownX:F

.field private mDownY:F

.field private mHandler:Landroid/os/Handler;

.field private mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

.field private mLongPressRunnable:Ljava/lang/Runnable;

.field private mNowBrightness:F

.field private minX:F


# direct methods
.method static bridge synthetic -$$Nest$fgetmDisplayMetrics(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)Landroid/util/DisplayMetrics;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHapticFeedbackUtil(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)Lmiui/util/HapticFeedbackUtil;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmBrightnessViewLength(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDisplayMetrics(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;Landroid/util/DisplayMetrics;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 60
    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V

    .line 36
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    .line 37
    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F

    .line 49
    new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;-><init>(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mConfigurationReceiver:Landroid/content/BroadcastReceiver;

    .line 80
    new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$2;

    invoke-direct {v0, p0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$2;-><init>(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mLongPressRunnable:Ljava/lang/Runnable;

    .line 61
    const-class v0, Landroid/hardware/display/DisplayManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 63
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x3fdded288ce703b0L    # 0.4676

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHandler:Landroid/os/Handler;

    .line 66
    new-instance v0, Lmiui/util/HapticFeedbackUtil;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    .line 67
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mConfigurationReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 68
    return-void
.end method

.method static synthetic access$002(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;I)I
    .locals 0
    .param p0, "x0"    # Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;
    .param p1, "x1"    # I

    .line 30
    iput p1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I

    return p1
.end method

.method static synthetic access$100(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)Lcom/miui/server/input/knock/view/KnockPathListener;
    .locals 1
    .param p0, "x0"    # Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    .line 30
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mKnockPathListener:Lcom/miui/server/input/knock/view/KnockPathListener;

    return-object v0
.end method

.method private updateMinX(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "brightness"    # F

    .line 170
    iget v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F

    iget v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F

    invoke-static {p2, v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->convertLinearToGammaFloat(FFF)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    sget v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 171
    .local v0, "percent":F
    iget v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    sub-float v1, p1, v1

    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->minX:F

    .line 172
    return-void
.end method


# virtual methods
.method public continueCheck()Z
    .locals 2

    .line 73
    const-string v0, "change_brightness"

    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mFunction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z

    move-result v0

    return v0

    .line 76
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 91
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 92
    .local v0, "nowX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 93
    .local v1, "nowY":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x3

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    .line 101
    :pswitch_0
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownX:F

    sub-float v2, v0, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 102
    .local v2, "diffDownX":F
    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownY:F

    sub-float v4, v1, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 104
    .local v4, "diffDownY":F
    const/high16 v5, 0x42480000    # 50.0f

    cmpl-float v6, v2, v5

    if-gtz v6, :cond_0

    cmpl-float v5, v4, v5

    if-lez v5, :cond_1

    .line 105
    :cond_0
    iget-object v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 106
    iget-object v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 107
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setCheckFail()V

    .line 111
    :cond_1
    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4

    .line 113
    const/high16 v5, 0x42c80000    # 100.0f

    cmpl-float v5, v4, v5

    const-string v6, "KnockBrightness"

    if-lez v5, :cond_2

    .line 114
    const-string v3, "knock feature brightness check fail"

    invoke-static {v6, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const/4 v3, 0x4

    iput v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I

    goto/16 :goto_1

    .line 116
    :cond_2
    div-float v5, v2, v4

    const/high16 v7, 0x40a00000    # 5.0f

    cmpl-float v5, v5, v7

    if-lez v5, :cond_7

    const/high16 v5, 0x43160000    # 150.0f

    cmpl-float v5, v2, v5

    if-lez v5, :cond_7

    .line 117
    const-string v5, "knock feature brightness check success"

    invoke-static {v6, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iput v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I

    .line 120
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getBrightnessInfo()Landroid/hardware/display/BrightnessInfo;

    move-result-object v3

    .line 121
    .local v3, "info":Landroid/hardware/display/BrightnessInfo;
    if-eqz v3, :cond_3

    .line 122
    iget v5, v3, Landroid/hardware/display/BrightnessInfo;->brightnessMaximum:F

    iput v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F

    .line 123
    iget v5, v3, Landroid/hardware/display/BrightnessInfo;->brightnessMinimum:F

    iput v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F

    .line 124
    iget v5, v3, Landroid/hardware/display/BrightnessInfo;->brightness:F

    iput v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F

    .line 126
    :cond_3
    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F

    invoke-direct {p0, v0, v5}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->updateMinX(FF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "info":Landroid/hardware/display/BrightnessInfo;
    goto :goto_0

    .line 127
    :catch_0
    move-exception v3

    .line 128
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 129
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_0
    goto :goto_1

    .line 131
    :cond_4
    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I

    if-ne v5, v3, :cond_7

    .line 132
    invoke-virtual {p0, p1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setBrightness(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 136
    .end local v2    # "diffDownX":F
    .end local v4    # "diffDownY":F
    :pswitch_1
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v2, v2, v4

    if-eqz v2, :cond_5

    .line 137
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDisplayId()I

    move-result v5

    iget v6, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    invoke-virtual {v2, v5, v6}, Landroid/hardware/display/DisplayManager;->setBrightness(IF)V

    .line 138
    iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    .line 140
    :cond_5
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I

    if-ne v2, v3, :cond_6

    .line 141
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v2

    const-string v3, "knock_long_press_horizontal_slid"

    iget-object v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mFunction:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_6
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 95
    :pswitch_2
    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownX:F

    .line 96
    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownY:F

    .line 97
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mLongPressRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 98
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mLongPressRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    nop

    .line 147
    :cond_7
    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setBrightness(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 150
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 151
    .local v0, "nowX":F
    iget v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->minX:F

    sub-float v1, v0, v1

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 152
    .local v1, "slideValue":I
    if-gez v1, :cond_0

    .line 153
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    .line 154
    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F

    .line 155
    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->updateMinX(FF)V

    goto :goto_0

    .line 156
    :cond_0
    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    if-le v1, v2, :cond_1

    .line 157
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    .line 158
    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F

    .line 159
    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->updateMinX(FF)V

    goto :goto_0

    .line 161
    :cond_1
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F

    iget v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F

    invoke-static {v1, v2, v3}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->convertGammaToLinearFloat(IFF)F

    move-result v2

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    .line 163
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDisplayId()I

    move-result v3

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F

    invoke-virtual {v2, v3, v4}, Landroid/hardware/display/DisplayManager;->setTemporaryBrightness(IF)V

    .line 164
    return-void
.end method
