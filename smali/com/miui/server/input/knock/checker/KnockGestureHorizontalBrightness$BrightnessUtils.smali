.class Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;
.super Ljava/lang/Object;
.source "KnockGestureHorizontalBrightness.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BrightnessUtils"
.end annotation


# static fields
.field private static final A:F

.field private static final B:F

.field private static final C:F

.field public static final GAMMA_SPACE_MAX:I

.field public static final GAMMA_SPACE_MIN:I

.field private static final R:F

.field private static final resources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 181
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    .line 182
    const v1, 0x10e00f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    .line 189
    invoke-static {}, Landroid/app/ActivityThread;->currentApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->resources:Landroid/content/res/Resources;

    .line 190
    const v1, 0x1107001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    sput v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->R:F

    .line 192
    const v1, 0x1107001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    sput v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->A:F

    .line 194
    const v1, 0x1107001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v1

    sput v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->B:F

    .line 196
    const v1, 0x1107001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    sput v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->C:F

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final convertGammaToLinear(III)I
    .locals 3
    .param p0, "val"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I

    .line 222
    sget v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    int-to-float v0, v0

    int-to-float v1, p0

    const/4 v2, 0x0

    invoke-static {v2, v0, v1}, Landroid/util/MathUtils;->norm(FFF)F

    move-result v0

    .line 224
    .local v0, "normalizedVal":F
    sget v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->R:F

    cmpg-float v2, v0, v1

    if-gtz v2, :cond_0

    .line 225
    div-float v1, v0, v1

    invoke-static {v1}, Landroid/util/MathUtils;->sq(F)F

    move-result v1

    .local v1, "ret":F
    goto :goto_0

    .line 227
    .end local v1    # "ret":F
    :cond_0
    sget v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->C:F

    sub-float v1, v0, v1

    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->A:F

    div-float/2addr v1, v2

    invoke-static {v1}, Landroid/util/MathUtils;->exp(F)F

    move-result v1

    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->B:F

    add-float/2addr v1, v2

    .line 232
    .restart local v1    # "ret":F
    :goto_0
    const/high16 v2, 0x41400000    # 12.0f

    div-float v2, v1, v2

    invoke-static {p1, p2, v2}, Landroid/util/MathUtils;->lerp(IIF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    return v2
.end method

.method public static final convertGammaToLinearFloat(IFF)F
    .locals 4
    .param p0, "val"    # I
    .param p1, "min"    # F
    .param p2, "max"    # F

    .line 245
    sget v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    int-to-float v0, v0

    int-to-float v1, p0

    const/4 v2, 0x0

    invoke-static {v2, v0, v1}, Landroid/util/MathUtils;->norm(FFF)F

    move-result v0

    .line 247
    .local v0, "normalizedVal":F
    sget v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->R:F

    cmpg-float v3, v0, v1

    if-gtz v3, :cond_0

    .line 248
    div-float v1, v0, v1

    invoke-static {v1}, Landroid/util/MathUtils;->sq(F)F

    move-result v1

    .local v1, "ret":F
    goto :goto_0

    .line 250
    .end local v1    # "ret":F
    :cond_0
    sget v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->C:F

    sub-float v1, v0, v1

    sget v3, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->A:F

    div-float/2addr v1, v3

    invoke-static {v1}, Landroid/util/MathUtils;->exp(F)F

    move-result v1

    sget v3, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->B:F

    add-float/2addr v1, v3

    .line 255
    .restart local v1    # "ret":F
    :goto_0
    const/high16 v3, 0x41400000    # 12.0f

    invoke-static {v1, v2, v3}, Landroid/util/MathUtils;->constrain(FFF)F

    move-result v2

    .line 259
    .local v2, "normalizedRet":F
    div-float v3, v2, v3

    invoke-static {p1, p2, v3}, Landroid/util/MathUtils;->lerp(FFF)F

    move-result v3

    return v3
.end method

.method public static final convertLinearToGamma(III)I
    .locals 3
    .param p0, "val"    # I
    .param p1, "min"    # I
    .param p2, "max"    # I

    .line 285
    int-to-float v0, p0

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-static {v0, v1, v2}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->convertLinearToGammaFloat(FFF)I

    move-result v0

    return v0
.end method

.method public static final convertLinearToGammaFloat(FFF)I
    .locals 4
    .param p0, "val"    # F
    .param p1, "min"    # F
    .param p2, "max"    # F

    .line 298
    invoke-static {p1, p2, p0}, Landroid/util/MathUtils;->norm(FFF)F

    move-result v0

    const/high16 v1, 0x41400000    # 12.0f

    mul-float/2addr v0, v1

    .line 300
    .local v0, "normalizedVal":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    .line 301
    invoke-static {v0}, Landroid/util/MathUtils;->sqrt(F)F

    move-result v1

    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->R:F

    mul-float/2addr v1, v2

    .local v1, "ret":F
    goto :goto_0

    .line 303
    .end local v1    # "ret":F
    :cond_0
    sget v1, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->A:F

    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->B:F

    sub-float v2, v0, v2

    invoke-static {v2}, Landroid/util/MathUtils;->log(F)F

    move-result v2

    mul-float/2addr v1, v2

    sget v2, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->C:F

    add-float/2addr v1, v2

    .line 306
    .restart local v1    # "ret":F
    :goto_0
    const/4 v2, 0x0

    sget v3, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils;->GAMMA_SPACE_MAX:I

    invoke-static {v2, v3, v1}, Landroid/util/MathUtils;->lerp(IIF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    return v2
.end method
