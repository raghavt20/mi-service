.class public Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;
.super Lcom/miui/server/input/knock/KnockGestureChecker;
.source "KnockGesturePartialShot.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "KnockGesturePartialShot"


# instance fields
.field private final EPS:F

.field private mBehindNextX:F

.field private mBehindNextY:F

.field private mBehindX:F

.field private mBehindY:F

.field private mCrossoverBehindX:F

.field private mCrossoverBehindY:F

.field private mCrossoverPointCount:I

.field private mCrossoverPreX:F

.field private mCrossoverPreY:F

.field private mIsBegin:Z

.field private mPathX:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mPathY:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mPreNextX:F

.field private mPreNextY:F

.field private mPx:F

.field private mPy:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 21
    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    .line 29
    const v1, 0x358637bd    # 1.0E-6f

    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->EPS:F

    .line 45
    iput-boolean v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z

    .line 22
    return-void
.end method

.method private checkIsValid()Z
    .locals 11

    .line 87
    const-string/jumbo v0, "sys.miui.screenshot.partial"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "KnockGesturePartialShot"

    const-string v2, "cloud not start partial screenshot because of partial screenshot is running"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return v1

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mKnockPathListener:Lcom/miui/server/input/knock/view/KnockPathListener;

    invoke-interface {v0}, Lcom/miui/server/input/knock/view/KnockPathListener;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v0

    .line 92
    .local v0, "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    if-eqz v0, :cond_7

    iget v2, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    const/16 v3, 0xa

    if-lt v2, v3, :cond_7

    iget v2, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    const/16 v3, 0x3e8

    if-le v2, v3, :cond_1

    goto/16 :goto_1

    .line 96
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget v4, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 97
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget v4, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    .line 98
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathRight()F

    move-result v2

    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathLeft()F

    move-result v3

    sub-float/2addr v2, v3

    const/high16 v3, 0x43480000    # 200.0f

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_6

    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathBottom()F

    move-result v2

    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathTop()F

    move-result v4

    sub-float/2addr v2, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    goto/16 :goto_0

    .line 101
    :cond_2
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->judgePath()V

    .line 102
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    const/4 v4, 0x1

    if-eq v2, v4, :cond_3

    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v5

    sub-int/2addr v5, v4

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    sub-float/2addr v2, v5

    float-to-double v5, v2

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v9

    sub-int/2addr v9, v4

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v9, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Float;

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    sub-float/2addr v2, v9

    float-to-double v9, v2

    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    add-double/2addr v5, v7

    const-wide v7, 0x40f5f90000000000L    # 90000.0

    cmpl-double v2, v5, v7

    if-lez v2, :cond_3

    .line 103
    return v1

    .line 105
    :cond_3
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    const/4 v5, 0x2

    if-ge v2, v5, :cond_5

    .line 106
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->cutPath()V

    .line 107
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathRight()F

    move-result v2

    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathLeft()F

    move-result v5

    sub-float/2addr v2, v5

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_4

    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathBottom()F

    move-result v2

    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathTop()F

    move-result v5

    sub-float/2addr v2, v5

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_4

    move v1, v4

    :cond_4
    return v1

    .line 109
    :cond_5
    return v4

    .line 99
    :cond_6
    :goto_0
    return v1

    .line 93
    :cond_7
    :goto_1
    return v1
.end method

.method private cutPath()V
    .locals 15

    .line 149
    move-object v0, p0

    iget v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    iget-object v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 150
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v4, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget-object v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->distance(FFFF)D

    move-result-wide v4

    const-wide/high16 v6, 0x4069000000000000L    # 200.0

    cmpl-double v1, v4, v6

    if-gtz v1, :cond_0

    iget-object v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 151
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v4, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F

    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v8

    sub-int/2addr v8, v3

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget v8, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F

    invoke-direct {p0, v1, v4, v5, v8}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->distance(FFFF)D

    move-result-wide v4

    cmpg-double v1, v4, v6

    if-gez v1, :cond_4

    iget-object v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 152
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v4, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F

    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget v8, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F

    invoke-direct {p0, v1, v4, v5, v8}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->distance(FFFF)D

    move-result-wide v4

    cmpg-double v1, v4, v6

    if-gez v1, :cond_4

    .line 154
    :cond_0
    const/4 v1, 0x0

    .local v1, "from":I
    const/4 v4, 0x0

    .line 155
    .local v4, "to":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v6

    if-ge v5, v6, :cond_3

    .line 156
    iget-object v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_1

    iget-object v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_1

    .line 157
    move v1, v5

    .line 159
    :cond_1
    iget-object v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_2

    iget-object v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_2

    .line 160
    move v4, v5

    .line 155
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 163
    .end local v5    # "i":I
    :cond_3
    if-lez v1, :cond_4

    if-lez v4, :cond_4

    sub-int v5, v4, v1

    const/4 v6, 0x5

    if-le v5, v6, :cond_4

    .line 164
    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v5, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    .line 165
    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v5, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    iput-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 166
    iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    .end local v1    # "from":I
    .end local v4    # "to":I
    :cond_4
    iget v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    if-nez v1, :cond_a

    .line 173
    iget-object v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .local v1, "beginX":F
    iget-object v4, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .local v4, "beginY":F
    iget-object v5, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .local v5, "lastX":F
    iget-object v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v7

    sub-int/2addr v7, v3

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    .line 174
    .local v6, "lastY":F
    const v7, 0x7f7fffff    # Float.MAX_VALUE

    .line 175
    .local v7, "minDistance":F
    const/4 v8, -0x1

    .line 176
    .local v8, "pointMinCutIndex":I
    const/4 v9, 0x1

    .local v9, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    if-ge v9, v10, :cond_6

    .line 177
    iget-object v10, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    sub-float v10, v5, v10

    float-to-double v13, v10

    invoke-static {v13, v14, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    iget-object v10, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    sub-float v10, v6, v10

    float-to-double v2, v10

    invoke-static {v2, v3, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v13, v2

    float-to-double v2, v7

    cmpg-double v2, v13, v2

    if-gez v2, :cond_5

    .line 178
    iget-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float v2, v5, v2

    float-to-double v2, v2

    invoke-static {v2, v3, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget-object v10, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    sub-float v10, v6, v10

    float-to-double v13, v10

    invoke-static {v13, v14, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v2, v10

    double-to-float v2, v2

    .line 179
    .end local v7    # "minDistance":F
    .local v2, "minDistance":F
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z

    .line 180
    move v3, v9

    move v7, v2

    move v8, v3

    .line 176
    .end local v2    # "minDistance":F
    .restart local v7    # "minDistance":F
    :cond_5
    add-int/lit8 v9, v9, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    goto :goto_1

    .line 183
    .end local v9    # "i":I
    :cond_6
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .local v2, "i":I
    :goto_2
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v3

    const/4 v9, 0x1

    sub-int/2addr v3, v9

    if-ge v2, v3, :cond_8

    .line 184
    iget-object v3, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v1, v3

    float-to-double v13, v3

    invoke-static {v13, v14, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    iget-object v3, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v4, v3

    float-to-double v9, v3

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    add-double/2addr v13, v9

    float-to-double v9, v7

    cmpg-double v3, v13, v9

    if-gez v3, :cond_7

    .line 185
    iget-object v3, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v1, v3

    float-to-double v9, v3

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    iget-object v3, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v4, v3

    float-to-double v13, v3

    invoke-static {v13, v14, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    add-double/2addr v9, v13

    double-to-float v3, v9

    .line 186
    .end local v7    # "minDistance":F
    .local v3, "minDistance":F
    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z

    .line 187
    move v7, v2

    move v8, v7

    move v7, v3

    .line 183
    .end local v3    # "minDistance":F
    .restart local v7    # "minDistance":F
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 190
    .end local v2    # "i":I
    :cond_8
    if-lez v8, :cond_a

    .line 191
    iget-boolean v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z

    if-eqz v2, :cond_9

    add-int/lit8 v2, v8, 0x7

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 192
    iget-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    add-int/lit8 v3, v8, 0x2

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v9

    invoke-interface {v2, v3, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 193
    iget-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    add-int/lit8 v3, v8, 0x2

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v9

    invoke-interface {v2, v3, v9}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    goto :goto_3

    .line 194
    :cond_9
    iget-boolean v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z

    if-nez v2, :cond_a

    const/16 v2, 0x8

    if-le v8, v2, :cond_a

    .line 195
    iget-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    add-int/lit8 v3, v8, -0x3

    const/4 v9, 0x0

    invoke-interface {v2, v9, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    .line 196
    iget-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    add-int/lit8 v3, v8, -0x3

    invoke-interface {v2, v9, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    .line 200
    .end local v1    # "beginX":F
    .end local v4    # "beginY":F
    .end local v5    # "lastX":F
    .end local v6    # "lastY":F
    .end local v7    # "minDistance":F
    .end local v8    # "pointMinCutIndex":I
    :cond_a
    :goto_3
    return-void
.end method

.method private distance(FFFF)D
    .locals 6
    .param p1, "x1"    # F
    .param p2, "x2"    # F
    .param p3, "y1"    # F
    .param p4, "y2"    # F

    .line 203
    sub-float v0, p1, p2

    float-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-float v4, p3, p4

    float-to-double v4, v4

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private getPathBottom()F
    .locals 5

    .line 233
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 234
    .local v0, "max":F
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 235
    .local v3, "y":F
    cmpl-float v4, v3, v0

    if-lez v4, :cond_1

    .line 236
    move v0, v3

    .line 238
    .end local v3    # "y":F
    :cond_1
    goto :goto_1

    .line 239
    :cond_2
    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/4 v0, 0x0

    .line 240
    :cond_3
    return v0
.end method

.method private getPathLeft()F
    .locals 5

    .line 222
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 223
    .local v0, "min":F
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 224
    .local v3, "x":F
    cmpg-float v4, v3, v0

    if-gez v4, :cond_1

    .line 225
    move v0, v3

    .line 227
    .end local v3    # "x":F
    :cond_1
    goto :goto_1

    .line 228
    :cond_2
    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/4 v0, 0x0

    .line 229
    :cond_3
    return v0
.end method

.method private getPathRight()F
    .locals 5

    .line 244
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 245
    .local v0, "max":F
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    .line 246
    .local v3, "x":Ljava/lang/Float;
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    cmpl-float v4, v4, v0

    if-lez v4, :cond_1

    .line 247
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 249
    .end local v3    # "x":Ljava/lang/Float;
    :cond_1
    goto :goto_1

    .line 250
    :cond_2
    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/4 v0, 0x0

    .line 251
    :cond_3
    return v0
.end method

.method private getPathTop()F
    .locals 5

    .line 211
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 212
    .local v0, "min":F
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 213
    .local v3, "y":F
    cmpg-float v4, v3, v0

    if-gez v4, :cond_1

    .line 214
    move v0, v3

    .line 216
    .end local v3    # "y":F
    :cond_1
    goto :goto_1

    .line 217
    :cond_2
    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/4 v0, 0x0

    .line 218
    :cond_3
    return v0
.end method

.method private judgePath()V
    .locals 15

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    .line 114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0xb

    if-ge v0, v1, :cond_3

    .line 115
    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F

    .line 116
    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F

    .line 117
    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F

    .line 118
    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    add-int/lit8 v2, v0, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F

    .line 119
    add-int/lit8 v1, v0, 0xa

    .local v1, "j":I
    :goto_1
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 120
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F

    .line 121
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F

    .line 122
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F

    .line 123
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F

    .line 125
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F

    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    cmpl-float v2, v2, v4

    if-gez v2, :cond_1

    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F

    .line 126
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F

    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    cmpl-float v2, v2, v4

    if-gez v2, :cond_1

    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F

    .line 127
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F

    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    cmpl-float v2, v2, v4

    if-gez v2, :cond_1

    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F

    .line 128
    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F

    iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_0

    .line 129
    goto :goto_2

    .line 131
    :cond_0
    iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F

    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F

    sub-float v5, v2, v4

    iget v6, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F

    iget v7, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F

    sub-float v8, v6, v7

    mul-float/2addr v5, v8

    iget v8, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F

    sub-float v9, v8, v7

    iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F

    sub-float v11, v10, v4

    mul-float/2addr v9, v11

    sub-float/2addr v5, v9

    .line 132
    .local v5, "h":F
    sub-float v9, v2, v4

    iget v11, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F

    sub-float v12, v11, v7

    mul-float/2addr v9, v12

    sub-float v12, v8, v7

    iget v13, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F

    sub-float v14, v13, v4

    mul-float/2addr v12, v14

    sub-float/2addr v9, v12

    .line 133
    .local v9, "m":F
    sub-float v12, v13, v10

    sub-float/2addr v7, v6

    mul-float/2addr v12, v7

    sub-float v7, v11, v6

    sub-float/2addr v4, v10

    mul-float/2addr v7, v4

    sub-float/2addr v12, v7

    .line 134
    .local v12, "n":F
    sub-float/2addr v13, v10

    sub-float v4, v8, v6

    mul-float/2addr v13, v4

    sub-float/2addr v11, v6

    sub-float v4, v2, v10

    mul-float/2addr v11, v4

    sub-float/2addr v13, v11

    .line 135
    .local v13, "k":F
    mul-float v4, v5, v9

    const v7, 0x358637bd    # 1.0E-6f

    cmpg-float v4, v4, v7

    if-gtz v4, :cond_1

    mul-float v4, v12, v13

    cmpg-float v4, v4, v7

    if-gtz v4, :cond_1

    .line 136
    iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    add-int/2addr v4, v3

    iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I

    .line 137
    if-ne v4, v3, :cond_1

    .line 138
    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F

    .line 139
    iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F

    .line 140
    iput v10, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F

    .line 141
    iput v6, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F

    .line 119
    .end local v5    # "h":F
    .end local v9    # "m":F
    .end local v12    # "n":F
    .end local v13    # "k":F
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 114
    .end local v1    # "j":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 146
    .end local v0    # "i":I
    :cond_3
    return-void
.end method


# virtual methods
.method public continueCheck()Z
    .locals 2

    .line 49
    const-string v0, "partial_screen_shot"

    iget-object v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mFunction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z

    move-result v0

    return v0

    .line 52
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 58
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 59
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->checkIsValid()Z

    move-result v0

    const-string v1, "KnockGesturePartialShot"

    if-eqz v0, :cond_2

    .line 60
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setCheckSuccess()V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "check success path size is :  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [F

    .line 65
    .local v1, "motionList":[F
    const/4 v2, 0x0

    .line 66
    .local v2, "j":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 67
    iget-object v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathX:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 68
    .local v4, "x":F
    iget-object v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 69
    .local v5, "y":F
    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 70
    goto :goto_1

    .line 72
    :cond_0
    add-int/lit8 v6, v2, 0x1

    .end local v2    # "j":I
    .local v6, "j":I
    aput v4, v1, v2

    .line 73
    add-int/lit8 v2, v6, 0x1

    .end local v6    # "j":I
    .restart local v2    # "j":I
    aput v5, v1, v6

    .line 66
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 75
    .end local v3    # "i":I
    .end local v4    # "x":F
    .end local v5    # "y":F
    :cond_1
    const-string v3, "partial.screenshot.points"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 76
    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mKnockPathListener:Lcom/miui/server/input/knock/view/KnockPathListener;

    invoke-interface {v3}, Lcom/miui/server/input/knock/view/KnockPathListener;->hideView()V

    .line 77
    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mFunction:Ljava/lang/String;

    const-string v5, "knock_slide_shape"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 79
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "motionList":[F
    .end local v2    # "j":I
    goto :goto_2

    .line 80
    :cond_2
    const-string v0, "check fail"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setCheckFail()V

    .line 84
    :cond_3
    :goto_2
    return-void
.end method

.method public size()I
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPathY:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
