public class com.miui.server.input.knock.checker.KnockGestureV extends com.miui.server.input.knock.KnockGestureChecker {
	 /* .source "KnockGestureV.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.miui.server.input.knock.checker.KnockGestureV ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 15 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V */
		 /* .line 16 */
		 return;
	 } // .end method
	 private Double lineSpace ( Float p0, Float p1, Float p2, Float p3 ) {
		 /* .locals 3 */
		 /* .param p1, "x1" # F */
		 /* .param p2, "y1" # F */
		 /* .param p3, "x2" # F */
		 /* .param p4, "y2" # F */
		 /* .line 126 */
		 /* sub-float v0, p1, p3 */
		 /* sub-float v1, p1, p3 */
		 /* mul-float/2addr v0, v1 */
		 /* sub-float v1, p2, p4 */
		 /* sub-float v2, p2, p4 */
		 /* mul-float/2addr v1, v2 */
		 /* add-float/2addr v0, v1 */
		 /* float-to-double v0, v0 */
		 java.lang.Math .sqrt ( v0,v1 );
		 /* move-result-wide v0 */
		 /* return-wide v0 */
	 } // .end method
	 private Double pointToLine ( Float p0, Float p1, Float p2, Float p3, Float p4, Float p5 ) {
		 /* .locals 23 */
		 /* .param p1, "x1" # F */
		 /* .param p2, "y1" # F */
		 /* .param p3, "x2" # F */
		 /* .param p4, "y2" # F */
		 /* .param p5, "x0" # F */
		 /* .param p6, "y0" # F */
		 /* .line 97 */
		 /* move-object/from16 v0, p0 */
		 /* move/from16 v1, p5 */
		 /* move/from16 v2, p6 */
		 /* const-wide/16 v3, 0x0 */
		 /* .line 99 */
		 /* .local v3, "space":D */
		 /* invoke-direct/range {p0 ..p4}, Lcom/miui/server/input/knock/checker/KnockGestureV;->lineSpace(FFFF)D */
		 /* move-result-wide v5 */
		 /* .line 100 */
		 /* .local v5, "a":D */
		 /* move/from16 v7, p1 */
		 /* move/from16 v8, p2 */
		 /* invoke-direct {v0, v7, v8, v1, v2}, Lcom/miui/server/input/knock/checker/KnockGestureV;->lineSpace(FFFF)D */
		 /* move-result-wide v9 */
		 /* .line 101 */
		 /* .local v9, "b":D */
		 /* move/from16 v11, p3 */
		 /* move/from16 v12, p4 */
		 /* invoke-direct {v0, v11, v12, v1, v2}, Lcom/miui/server/input/knock/checker/KnockGestureV;->lineSpace(FFFF)D */
		 /* move-result-wide v13 */
		 /* .line 102 */
		 /* .local v13, "c":D */
		 /* const-wide v15, 0x3eb0c6f7a0b5ed8dL # 1.0E-6 */
		 /* cmpg-double v17, v13, v15 */
		 /* if-lez v17, :cond_4 */
		 /* cmpg-double v17, v9, v15 */
		 /* if-gtz v17, :cond_0 */
		 /* .line 106 */
	 } // :cond_0
	 /* cmpg-double v15, v5, v15 */
	 /* if-gtz v15, :cond_1 */
	 /* .line 107 */
	 /* move-wide v3, v9 */
	 /* .line 108 */
	 /* return-wide v3 */
	 /* .line 110 */
} // :cond_1
/* mul-double v15, v13, v13 */
/* mul-double v17, v5, v5 */
/* mul-double v19, v9, v9 */
/* add-double v17, v17, v19 */
/* cmpl-double v15, v15, v17 */
/* if-ltz v15, :cond_2 */
/* .line 111 */
/* move-wide v3, v9 */
/* .line 112 */
/* return-wide v3 */
/* .line 114 */
} // :cond_2
/* mul-double v15, v9, v9 */
/* mul-double v17, v5, v5 */
/* mul-double v19, v13, v13 */
/* add-double v17, v17, v19 */
/* cmpl-double v15, v15, v17 */
/* if-ltz v15, :cond_3 */
/* .line 115 */
/* move-wide v3, v13 */
/* .line 116 */
/* return-wide v3 */
/* .line 118 */
} // :cond_3
/* add-double v15, v5, v9 */
/* add-double/2addr v15, v13 */
/* const-wide/high16 v17, 0x4000000000000000L # 2.0 */
/* div-double v15, v15, v17 */
/* .line 119 */
/* .local v15, "p":D */
/* sub-double v19, v15, v5 */
/* mul-double v19, v19, v15 */
/* sub-double v21, v15, v9 */
/* mul-double v19, v19, v21 */
/* sub-double v21, v15, v13 */
/* mul-double v19, v19, v21 */
/* invoke-static/range {v19 ..v20}, Ljava/lang/Math;->sqrt(D)D */
/* move-result-wide v19 */
/* .line 120 */
/* .local v19, "s":D */
/* mul-double v17, v17, v19 */
/* div-double v17, v17, v5 */
/* .line 121 */
} // .end local v3 # "space":D
/* .local v17, "space":D */
/* return-wide v17 */
/* .line 103 */
} // .end local v15 # "p":D
} // .end local v17 # "space":D
} // .end local v19 # "s":D
/* .restart local v3 # "space":D */
} // :cond_4
} // :goto_0
/* const-wide/16 v3, 0x0 */
/* .line 104 */
/* return-wide v3 */
} // .end method
/* # virtual methods */
public Boolean continueCheck ( ) {
/* .locals 1 */
/* .line 20 */
v0 = this.mFunction;
v0 = (( com.miui.server.input.knock.checker.KnockGestureV ) p0 ).checkEmpty ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/knock/checker/KnockGestureV;->checkEmpty(Ljava/lang/String;)Z
/* if-nez v0, :cond_0 */
/* .line 21 */
v0 = /* invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z */
/* .line 23 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isVGesture ( ) {
/* .locals 34 */
/* .line 41 */
/* move-object/from16 v7, p0 */
v0 = this.mKnockPathListener;
/* .line 42 */
/* .local v8, "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
int v9 = 0; // const/4 v9, 0x0
if ( v8 != null) { // if-eqz v8, :cond_9
/* iget v0, v8, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* const/16 v1, 0xa */
/* if-gt v0, v1, :cond_0 */
/* move v0, v9 */
/* goto/16 :goto_3 */
/* .line 45 */
} // :cond_0
/* iget v10, v8, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* .line 46 */
/* .local v10, "N":I */
v0 = this.mTraceX;
/* aget-object v0, v0, v9 */
v11 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 47 */
/* .local v11, "downX":F */
v0 = this.mTraceY;
/* aget-object v0, v0, v9 */
v12 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 48 */
/* .local v12, "downY":F */
v0 = this.mTraceX;
/* add-int/lit8 v1, v10, -0x1 */
/* aget-object v0, v0, v1 */
v13 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 49 */
/* .local v13, "upX":F */
v0 = this.mTraceY;
/* add-int/lit8 v1, v10, -0x1 */
/* aget-object v0, v0, v1 */
v14 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 50 */
/* .local v14, "upY":F */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "bottomX":F */
int v1 = 0; // const/4 v1, 0x0
/* .line 51 */
/* .local v1, "bottomY":F */
int v2 = 0; // const/4 v2, 0x0
/* .line 52 */
/* .local v2, "bottomIndex":I */
int v3 = 0; // const/4 v3, 0x0
/* move v15, v0 */
/* move/from16 v16, v1 */
/* move v6, v2 */
} // .end local v0 # "bottomX":F
} // .end local v1 # "bottomY":F
} // .end local v2 # "bottomIndex":I
/* .local v3, "i":I */
/* .local v6, "bottomIndex":I */
/* .local v15, "bottomX":F */
/* .local v16, "bottomY":F */
} // :goto_0
/* if-ge v3, v10, :cond_2 */
/* .line 53 */
v0 = this.mTraceY;
/* aget-object v0, v0, v3 */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 54 */
/* .local v0, "y":F */
/* cmpl-float v1, v0, v16 */
/* if-lez v1, :cond_1 */
/* .line 55 */
/* move v1, v0 */
/* .line 56 */
} // .end local v16 # "bottomY":F
/* .restart local v1 # "bottomY":F */
v2 = this.mTraceX;
/* aget-object v2, v2, v3 */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* .line 57 */
} // .end local v15 # "bottomX":F
/* .local v2, "bottomX":F */
/* move v4, v3 */
/* move/from16 v16, v1 */
/* move v15, v2 */
/* move v6, v4 */
/* .line 52 */
} // .end local v0 # "y":F
} // .end local v1 # "bottomY":F
} // .end local v2 # "bottomX":F
/* .restart local v15 # "bottomX":F */
/* .restart local v16 # "bottomY":F */
} // :cond_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 61 */
} // .end local v3 # "i":I
} // :cond_2
/* cmpl-float v0, v16, v12 */
if ( v0 != null) { // if-eqz v0, :cond_8
/* cmpl-float v0, v16, v14 */
if ( v0 != null) { // if-eqz v0, :cond_8
/* cmpg-float v0, v15, v11 */
/* if-lez v0, :cond_8 */
/* cmpl-float v0, v15, v13 */
/* if-gez v0, :cond_8 */
/* cmpl-float v0, v11, v13 */
/* if-gez v0, :cond_8 */
/* sub-float v0, v16, v12 */
/* const/high16 v1, 0x43af0000 # 350.0f */
/* cmpg-float v0, v0, v1 */
/* if-ltz v0, :cond_8 */
/* sub-float v0, v16, v14 */
/* cmpg-float v0, v0, v1 */
/* if-gez v0, :cond_3 */
/* move/from16 v21, v10 */
/* move/from16 v26, v12 */
/* move v12, v6 */
/* goto/16 :goto_2 */
/* .line 67 */
} // :cond_3
/* sub-float v0, v15, v11 */
/* sub-float v1, v16, v12 */
/* div-float/2addr v0, v1 */
/* float-to-double v4, v0 */
/* .line 68 */
/* .local v4, "vLeft":D */
java.lang.Math .atan ( v4,v5 );
/* move-result-wide v17 */
/* .line 69 */
/* .local v17, "atanLeft":D */
/* invoke-static/range {v17 ..v18}, Ljava/lang/Math;->toDegrees(D)D */
/* move-result-wide v2 */
/* .line 71 */
/* .local v2, "atanDegLeft":D */
/* sub-float v0, v13, v15 */
/* sub-float v1, v16, v14 */
/* div-float/2addr v0, v1 */
/* float-to-double v0, v0 */
/* .line 72 */
/* .local v0, "vRight":D */
java.lang.Math .atan ( v0,v1 );
/* move-result-wide v19 */
/* .line 73 */
/* .local v19, "atanRight":D */
/* move/from16 v21, v10 */
} // .end local v10 # "N":I
/* .local v21, "N":I */
/* invoke-static/range {v19 ..v20}, Ljava/lang/Math;->toDegrees(D)D */
/* move-result-wide v9 */
/* .line 74 */
/* .local v9, "atanDegRight":D */
/* move-wide/from16 v22, v0 */
} // .end local v0 # "vRight":D
/* .local v22, "vRight":D */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "\u89d2\u5ea6 left:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
final String v1 = " right:"; // const-string v1, " right:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v9, v10 ); // invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "KnockGestureV"; // const-string v1, "KnockGestureV"
android.util.Slog .i ( v1,v0 );
/* .line 76 */
int v0 = 0; // const/4 v0, 0x0
/* move v1, v0 */
/* .local v1, "i":I */
} // :goto_1
/* move/from16 v0, v21 */
} // .end local v21 # "N":I
/* .local v0, "N":I */
/* if-ge v1, v0, :cond_6 */
/* .line 77 */
/* const-wide/high16 v24, 0x4069000000000000L # 200.0 */
/* if-ge v1, v6, :cond_4 */
/* .line 78 */
/* move/from16 v21, v0 */
} // .end local v0 # "N":I
/* .restart local v21 # "N":I */
v0 = this.mTraceX;
/* aget-object v0, v0, v6 */
v26 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v0 = this.mTraceY;
/* aget-object v0, v0, v6 */
v27 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v0 = this.mTraceX;
/* aget-object v0, v0, v1 */
/* .line 79 */
v28 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v0 = this.mTraceY;
/* aget-object v0, v0, v1 */
v29 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 78 */
/* move-object/from16 v0, p0 */
/* move v7, v1 */
} // .end local v1 # "i":I
/* .local v7, "i":I */
/* move v1, v11 */
/* move-wide/from16 v30, v2 */
} // .end local v2 # "atanDegLeft":D
/* .local v30, "atanDegLeft":D */
/* move v2, v12 */
/* move/from16 v3, v26 */
/* move-wide/from16 v32, v4 */
} // .end local v4 # "vLeft":D
/* .local v32, "vLeft":D */
/* move/from16 v4, v27 */
/* move/from16 v5, v28 */
/* move/from16 v26, v12 */
/* move v12, v6 */
} // .end local v6 # "bottomIndex":I
/* .local v12, "bottomIndex":I */
/* .local v26, "downY":F */
/* move/from16 v6, v29 */
/* invoke-direct/range {v0 ..v6}, Lcom/miui/server/input/knock/checker/KnockGestureV;->pointToLine(FFFFFF)D */
/* move-result-wide v0 */
/* cmpl-double v0, v0, v24 */
/* if-lez v0, :cond_5 */
/* .line 80 */
int v0 = 0; // const/4 v0, 0x0
/* .line 82 */
} // .end local v7 # "i":I
} // .end local v21 # "N":I
} // .end local v26 # "downY":F
} // .end local v30 # "atanDegLeft":D
} // .end local v32 # "vLeft":D
/* .restart local v0 # "N":I */
/* .restart local v1 # "i":I */
/* .restart local v2 # "atanDegLeft":D */
/* .restart local v4 # "vLeft":D */
/* .restart local v6 # "bottomIndex":I */
/* .local v12, "downY":F */
} // :cond_4
/* move/from16 v21, v0 */
/* move v7, v1 */
/* move-wide/from16 v30, v2 */
/* move-wide/from16 v32, v4 */
/* move/from16 v26, v12 */
/* move v12, v6 */
} // .end local v0 # "N":I
} // .end local v1 # "i":I
} // .end local v2 # "atanDegLeft":D
} // .end local v4 # "vLeft":D
} // .end local v6 # "bottomIndex":I
/* .restart local v7 # "i":I */
/* .local v12, "bottomIndex":I */
/* .restart local v21 # "N":I */
/* .restart local v26 # "downY":F */
/* .restart local v30 # "atanDegLeft":D */
/* .restart local v32 # "vLeft":D */
/* if-le v7, v12, :cond_5 */
/* .line 83 */
v0 = this.mTraceX;
/* aget-object v0, v0, v12 */
v3 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v0 = this.mTraceY;
/* aget-object v0, v0, v12 */
v4 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v0 = this.mTraceX;
/* aget-object v0, v0, v7 */
/* .line 84 */
v5 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
v0 = this.mTraceY;
/* aget-object v0, v0, v7 */
v6 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 83 */
/* move-object/from16 v0, p0 */
/* move v1, v13 */
/* move v2, v14 */
/* invoke-direct/range {v0 ..v6}, Lcom/miui/server/input/knock/checker/KnockGestureV;->pointToLine(FFFFFF)D */
/* move-result-wide v0 */
/* cmpl-double v0, v0, v24 */
/* if-lez v0, :cond_5 */
/* .line 85 */
int v0 = 0; // const/4 v0, 0x0
/* .line 76 */
} // :cond_5
/* add-int/lit8 v1, v7, 0x1 */
/* move-object/from16 v7, p0 */
/* move v6, v12 */
/* move/from16 v12, v26 */
/* move-wide/from16 v2, v30 */
/* move-wide/from16 v4, v32 */
} // .end local v7 # "i":I
/* .restart local v1 # "i":I */
/* goto/16 :goto_1 */
} // .end local v21 # "N":I
} // .end local v26 # "downY":F
} // .end local v30 # "atanDegLeft":D
} // .end local v32 # "vLeft":D
/* .restart local v0 # "N":I */
/* .restart local v2 # "atanDegLeft":D */
/* .restart local v4 # "vLeft":D */
/* .restart local v6 # "bottomIndex":I */
/* .local v12, "downY":F */
} // :cond_6
/* move/from16 v21, v0 */
/* move v7, v1 */
/* move-wide/from16 v30, v2 */
/* move-wide/from16 v32, v4 */
/* move/from16 v26, v12 */
/* move v12, v6 */
/* .line 90 */
} // .end local v0 # "N":I
} // .end local v1 # "i":I
} // .end local v2 # "atanDegLeft":D
} // .end local v4 # "vLeft":D
} // .end local v6 # "bottomIndex":I
/* .local v12, "bottomIndex":I */
/* .restart local v21 # "N":I */
/* .restart local v26 # "downY":F */
/* .restart local v30 # "atanDegLeft":D */
/* .restart local v32 # "vLeft":D */
/* sub-double v2, v30, v9 */
java.lang.Math .abs ( v2,v3 );
/* move-result-wide v0 */
/* const-wide/high16 v2, 0x403e000000000000L # 30.0 */
/* cmpg-double v0, v0, v2 */
/* if-gtz v0, :cond_7 */
/* sub-float v0, v11, v13 */
v0 = java.lang.Math .abs ( v0 );
/* const/high16 v1, 0x43480000 # 200.0f */
/* cmpl-float v0, v0, v1 */
/* if-lez v0, :cond_7 */
/* .line 91 */
int v0 = 1; // const/4 v0, 0x1
/* .line 93 */
} // :cond_7
int v0 = 0; // const/4 v0, 0x0
/* .line 61 */
} // .end local v9 # "atanDegRight":D
} // .end local v17 # "atanLeft":D
} // .end local v19 # "atanRight":D
} // .end local v21 # "N":I
} // .end local v22 # "vRight":D
} // .end local v26 # "downY":F
} // .end local v30 # "atanDegLeft":D
} // .end local v32 # "vLeft":D
/* .restart local v6 # "bottomIndex":I */
/* .restart local v10 # "N":I */
/* .local v12, "downY":F */
} // :cond_8
/* move/from16 v21, v10 */
/* move/from16 v26, v12 */
/* move v12, v6 */
/* .line 63 */
} // .end local v6 # "bottomIndex":I
} // .end local v10 # "N":I
/* .local v12, "bottomIndex":I */
/* .restart local v21 # "N":I */
/* .restart local v26 # "downY":F */
} // :goto_2
int v0 = 0; // const/4 v0, 0x0
/* .line 42 */
} // .end local v11 # "downX":F
} // .end local v12 # "bottomIndex":I
} // .end local v13 # "upX":F
} // .end local v14 # "upY":F
} // .end local v15 # "bottomX":F
} // .end local v16 # "bottomY":F
} // .end local v21 # "N":I
} // .end local v26 # "downY":F
} // :cond_9
/* move v0, v9 */
/* .line 43 */
} // :goto_3
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 29 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_1 */
/* .line 30 */
v0 = (( com.miui.server.input.knock.checker.KnockGestureV ) p0 ).isVGesture ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureV;->isVGesture()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 31 */
(( com.miui.server.input.knock.checker.KnockGestureV ) p0 ).setCheckSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureV;->setCheckSuccess()V
/* .line 32 */
v0 = this.mKnockPathListener;
/* .line 33 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
v2 = this.mFunction;
final String v3 = "knock_gesture_v"; // const-string v3, "knock_gesture_v"
int v4 = 0; // const/4 v4, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v2, v3, v4, v1 ); // invoke-virtual {v0, v2, v3, v4, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 35 */
} // :cond_0
(( com.miui.server.input.knock.checker.KnockGestureV ) p0 ).setCheckFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureV;->setCheckFail()V
/* .line 38 */
} // :cond_1
} // :goto_0
return;
} // .end method
