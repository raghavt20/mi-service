class com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$BrightnessUtils {
	 /* .source "KnockGestureHorizontalBrightness.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "BrightnessUtils" */
} // .end annotation
/* # static fields */
private static final Float A;
private static final Float B;
private static final Float C;
public static final Integer GAMMA_SPACE_MAX;
public static final Integer GAMMA_SPACE_MIN;
private static final Float R;
private static final android.content.res.Resources resources;
/* # direct methods */
static com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$BrightnessUtils ( ) {
/* .locals 2 */
/* .line 181 */
android.content.res.Resources .getSystem ( );
/* .line 182 */
/* const v1, 0x10e00f3 */
v0 = (( android.content.res.Resources ) v0 ).getInteger ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I
/* .line 189 */
android.app.ActivityThread .currentApplication ( );
(( android.app.Application ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;
/* .line 190 */
/* const v1, 0x1107001d */
v1 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 192 */
/* const v1, 0x1107001a */
v1 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 194 */
/* const v1, 0x1107001b */
v1 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* .line 196 */
/* const v1, 0x1107001c */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
return;
} // .end method
private com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$BrightnessUtils ( ) {
/* .locals 0 */
/* .line 177 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
public static final Integer convertGammaToLinear ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "val" # I */
/* .param p1, "min" # I */
/* .param p2, "max" # I */
/* .line 222 */
/* int-to-float v0, v0 */
/* int-to-float v1, p0 */
int v2 = 0; // const/4 v2, 0x0
v0 = android.util.MathUtils .norm ( v2,v0,v1 );
/* .line 224 */
/* .local v0, "normalizedVal":F */
/* cmpg-float v2, v0, v1 */
/* if-gtz v2, :cond_0 */
/* .line 225 */
/* div-float v1, v0, v1 */
v1 = android.util.MathUtils .sq ( v1 );
/* .local v1, "ret":F */
/* .line 227 */
} // .end local v1 # "ret":F
} // :cond_0
/* sub-float v1, v0, v1 */
/* div-float/2addr v1, v2 */
v1 = android.util.MathUtils .exp ( v1 );
/* add-float/2addr v1, v2 */
/* .line 232 */
/* .restart local v1 # "ret":F */
} // :goto_0
/* const/high16 v2, 0x41400000 # 12.0f */
/* div-float v2, v1, v2 */
v2 = android.util.MathUtils .lerp ( p1,p2,v2 );
v2 = java.lang.Math .round ( v2 );
} // .end method
public static final Float convertGammaToLinearFloat ( Integer p0, Float p1, Float p2 ) {
/* .locals 4 */
/* .param p0, "val" # I */
/* .param p1, "min" # F */
/* .param p2, "max" # F */
/* .line 245 */
/* int-to-float v0, v0 */
/* int-to-float v1, p0 */
int v2 = 0; // const/4 v2, 0x0
v0 = android.util.MathUtils .norm ( v2,v0,v1 );
/* .line 247 */
/* .local v0, "normalizedVal":F */
/* cmpg-float v3, v0, v1 */
/* if-gtz v3, :cond_0 */
/* .line 248 */
/* div-float v1, v0, v1 */
v1 = android.util.MathUtils .sq ( v1 );
/* .local v1, "ret":F */
/* .line 250 */
} // .end local v1 # "ret":F
} // :cond_0
/* sub-float v1, v0, v1 */
/* div-float/2addr v1, v3 */
v1 = android.util.MathUtils .exp ( v1 );
/* add-float/2addr v1, v3 */
/* .line 255 */
/* .restart local v1 # "ret":F */
} // :goto_0
/* const/high16 v3, 0x41400000 # 12.0f */
v2 = android.util.MathUtils .constrain ( v1,v2,v3 );
/* .line 259 */
/* .local v2, "normalizedRet":F */
/* div-float v3, v2, v3 */
v3 = android.util.MathUtils .lerp ( p1,p2,v3 );
} // .end method
public static final Integer convertLinearToGamma ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "val" # I */
/* .param p1, "min" # I */
/* .param p2, "max" # I */
/* .line 285 */
/* int-to-float v0, p0 */
/* int-to-float v1, p1 */
/* int-to-float v2, p2 */
v0 = com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$BrightnessUtils .convertLinearToGammaFloat ( v0,v1,v2 );
} // .end method
public static final Integer convertLinearToGammaFloat ( Float p0, Float p1, Float p2 ) {
/* .locals 4 */
/* .param p0, "val" # F */
/* .param p1, "min" # F */
/* .param p2, "max" # F */
/* .line 298 */
v0 = android.util.MathUtils .norm ( p1,p2,p0 );
/* const/high16 v1, 0x41400000 # 12.0f */
/* mul-float/2addr v0, v1 */
/* .line 300 */
/* .local v0, "normalizedVal":F */
/* const/high16 v1, 0x3f800000 # 1.0f */
/* cmpg-float v1, v0, v1 */
/* if-gtz v1, :cond_0 */
/* .line 301 */
v1 = android.util.MathUtils .sqrt ( v0 );
/* mul-float/2addr v1, v2 */
/* .local v1, "ret":F */
/* .line 303 */
} // .end local v1 # "ret":F
} // :cond_0
/* sub-float v2, v0, v2 */
v2 = android.util.MathUtils .log ( v2 );
/* mul-float/2addr v1, v2 */
/* add-float/2addr v1, v2 */
/* .line 306 */
/* .restart local v1 # "ret":F */
} // :goto_0
int v2 = 0; // const/4 v2, 0x0
v2 = android.util.MathUtils .lerp ( v2,v3,v1 );
v2 = java.lang.Math .round ( v2 );
} // .end method
