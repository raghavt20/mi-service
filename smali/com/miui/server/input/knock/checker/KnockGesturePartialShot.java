public class com.miui.server.input.knock.checker.KnockGesturePartialShot extends com.miui.server.input.knock.KnockGestureChecker {
	 /* .source "KnockGesturePartialShot.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Float EPS;
	 private Float mBehindNextX;
	 private Float mBehindNextY;
	 private Float mBehindX;
	 private Float mBehindY;
	 private Float mCrossoverBehindX;
	 private Float mCrossoverBehindY;
	 private Integer mCrossoverPointCount;
	 private Float mCrossoverPreX;
	 private Float mCrossoverPreY;
	 private Boolean mIsBegin;
	 private java.util.List mPathX;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/Float;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.List mPathY;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/Float;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Float mPreNextX;
private Float mPreNextY;
private Float mPx;
private Float mPy;
/* # direct methods */
public com.miui.server.input.knock.checker.KnockGesturePartialShot ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 21 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V */
/* .line 27 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
/* .line 29 */
/* const v1, 0x358637bd # 1.0E-6f */
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->EPS:F */
/* .line 45 */
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z */
/* .line 22 */
return;
} // .end method
private Boolean checkIsValid ( ) {
/* .locals 11 */
/* .line 87 */
/* const-string/jumbo v0, "sys.miui.screenshot.partial" */
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 88 */
final String v0 = "KnockGesturePartialShot"; // const-string v0, "KnockGesturePartialShot"
final String v2 = "cloud not start partial screenshot because of partial screenshot is running"; // const-string v2, "cloud not start partial screenshot because of partial screenshot is running"
android.util.Slog .d ( v0,v2 );
/* .line 89 */
/* .line 91 */
} // :cond_0
v0 = this.mKnockPathListener;
/* .line 92 */
/* .local v0, "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
if ( v0 != null) { // if-eqz v0, :cond_7
/* iget v2, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* const/16 v3, 0xa */
/* if-lt v2, v3, :cond_7 */
/* iget v2, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* const/16 v3, 0x3e8 */
/* if-le v2, v3, :cond_1 */
/* goto/16 :goto_1 */
/* .line 96 */
} // :cond_1
/* new-instance v2, Ljava/util/ArrayList; */
v3 = this.mTraceX;
java.util.Arrays .asList ( v3 );
/* iget v4, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mPathX = v2;
/* .line 97 */
/* new-instance v2, Ljava/util/ArrayList; */
v3 = this.mTraceY;
java.util.Arrays .asList ( v3 );
/* iget v4, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
this.mPathY = v2;
/* .line 98 */
v2 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathRight()F */
v3 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathLeft()F */
/* sub-float/2addr v2, v3 */
/* const/high16 v3, 0x43480000 # 200.0f */
/* cmpg-float v2, v2, v3 */
/* if-ltz v2, :cond_6 */
v2 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathBottom()F */
v4 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathTop()F */
/* sub-float/2addr v2, v4 */
/* cmpg-float v2, v2, v3 */
/* if-gez v2, :cond_2 */
/* goto/16 :goto_0 */
/* .line 101 */
} // :cond_2
/* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->judgePath()V */
/* .line 102 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
int v4 = 1; // const/4 v4, 0x1
/* if-eq v2, v4, :cond_3 */
v2 = this.mPathX;
v5 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v5, v4 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v5 = this.mPathX;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v2, v5 */
/* float-to-double v5, v2 */
/* const-wide/high16 v7, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v5,v6,v7,v8 );
/* move-result-wide v5 */
v2 = this.mPathY;
v9 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v9, v4 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
v9 = this.mPathY;
/* check-cast v9, Ljava/lang/Float; */
v9 = (( java.lang.Float ) v9 ).floatValue ( ); // invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F
/* sub-float/2addr v2, v9 */
/* float-to-double v9, v2 */
java.lang.Math .pow ( v9,v10,v7,v8 );
/* move-result-wide v7 */
/* add-double/2addr v5, v7 */
/* const-wide v7, 0x40f5f90000000000L # 90000.0 */
/* cmpl-double v2, v5, v7 */
/* if-lez v2, :cond_3 */
/* .line 103 */
/* .line 105 */
} // :cond_3
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
int v5 = 2; // const/4 v5, 0x2
/* if-ge v2, v5, :cond_5 */
/* .line 106 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->cutPath()V */
/* .line 107 */
v2 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathRight()F */
v5 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathLeft()F */
/* sub-float/2addr v2, v5 */
/* cmpg-float v2, v2, v3 */
/* if-ltz v2, :cond_4 */
v2 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathBottom()F */
v5 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->getPathTop()F */
/* sub-float/2addr v2, v5 */
/* cmpg-float v2, v2, v3 */
/* if-ltz v2, :cond_4 */
/* move v1, v4 */
} // :cond_4
/* .line 109 */
} // :cond_5
/* .line 99 */
} // :cond_6
} // :goto_0
/* .line 93 */
} // :cond_7
} // :goto_1
} // .end method
private void cutPath ( ) {
/* .locals 15 */
/* .line 149 */
/* move-object v0, p0 */
/* iget v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v3, :cond_4 */
v1 = this.mPathX;
/* .line 150 */
v4 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v4, v3 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
v4 = this.mPathX;
/* check-cast v4, Ljava/lang/Float; */
v4 = (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
v5 = this.mPathY;
v6 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v6, v3 */
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
v6 = this.mPathY;
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* invoke-direct {p0, v1, v4, v5, v6}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->distance(FFFF)D */
/* move-result-wide v4 */
/* const-wide/high16 v6, 0x4069000000000000L # 200.0 */
/* cmpl-double v1, v4, v6 */
/* if-gtz v1, :cond_0 */
v1 = this.mPathX;
/* .line 151 */
v4 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v4, v3 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iget v4, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F */
v5 = this.mPathY;
v8 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v8, v3 */
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* iget v8, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F */
/* invoke-direct {p0, v1, v4, v5, v8}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->distance(FFFF)D */
/* move-result-wide v4 */
/* cmpg-double v1, v4, v6 */
/* if-gez v1, :cond_4 */
v1 = this.mPathX;
/* .line 152 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iget v4, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F */
v5 = this.mPathY;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* iget v8, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F */
/* invoke-direct {p0, v1, v4, v5, v8}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->distance(FFFF)D */
/* move-result-wide v4 */
/* cmpg-double v1, v4, v6 */
/* if-gez v1, :cond_4 */
/* .line 154 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "from":I */
int v4 = 0; // const/4 v4, 0x0
/* .line 155 */
/* .local v4, "to":I */
int v5 = 0; // const/4 v5, 0x0
/* .local v5, "i":I */
} // :goto_0
v6 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* if-ge v5, v6, :cond_3 */
/* .line 156 */
v6 = this.mPathX;
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F */
/* cmpl-float v6, v6, v7 */
/* if-nez v6, :cond_1 */
v6 = this.mPathY;
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F */
/* cmpl-float v6, v6, v7 */
/* if-nez v6, :cond_1 */
/* .line 157 */
/* move v1, v5 */
/* .line 159 */
} // :cond_1
v6 = this.mPathX;
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F */
/* cmpl-float v6, v6, v7 */
/* if-nez v6, :cond_2 */
v6 = this.mPathY;
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* iget v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F */
/* cmpl-float v6, v6, v7 */
/* if-nez v6, :cond_2 */
/* .line 160 */
/* move v4, v5 */
/* .line 155 */
} // :cond_2
/* add-int/lit8 v5, v5, 0x1 */
/* .line 163 */
} // .end local v5 # "i":I
} // :cond_3
/* if-lez v1, :cond_4 */
/* if-lez v4, :cond_4 */
/* sub-int v5, v4, v1 */
int v6 = 5; // const/4 v6, 0x5
/* if-le v5, v6, :cond_4 */
/* .line 164 */
v5 = this.mPathY;
this.mPathY = v5;
/* .line 165 */
v5 = this.mPathX;
this.mPathX = v5;
/* .line 166 */
/* iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F */
java.lang.Float .valueOf ( v6 );
/* .line 167 */
v5 = this.mPathY;
/* iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F */
java.lang.Float .valueOf ( v6 );
/* .line 168 */
v5 = this.mPathX;
/* iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F */
java.lang.Float .valueOf ( v6 );
/* .line 169 */
v5 = this.mPathY;
/* iget v6, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F */
java.lang.Float .valueOf ( v6 );
/* .line 172 */
} // .end local v1 # "from":I
} // .end local v4 # "to":I
} // :cond_4
/* iget v1, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
/* if-nez v1, :cond_a */
/* .line 173 */
v1 = this.mPathX;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* .local v1, "beginX":F */
v4 = this.mPathY;
/* check-cast v4, Ljava/lang/Float; */
v4 = (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
/* .local v4, "beginY":F */
v5 = this.mPathX;
v6 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v6, v3 */
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* .local v5, "lastX":F */
v6 = this.mPathY;
v7 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* sub-int/2addr v7, v3 */
/* check-cast v6, Ljava/lang/Float; */
v6 = (( java.lang.Float ) v6 ).floatValue ( ); // invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F
/* .line 174 */
/* .local v6, "lastY":F */
/* const v7, 0x7f7fffff # Float.MAX_VALUE */
/* .line 175 */
/* .local v7, "minDistance":F */
int v8 = -1; // const/4 v8, -0x1
/* .line 176 */
/* .local v8, "pointMinCutIndex":I */
int v9 = 1; // const/4 v9, 0x1
/* .local v9, "i":I */
} // :goto_1
v10 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* div-int/lit8 v10, v10, 0x2 */
/* const-wide/high16 v11, 0x4000000000000000L # 2.0 */
/* if-ge v9, v10, :cond_6 */
/* .line 177 */
v10 = this.mPathX;
/* check-cast v10, Ljava/lang/Float; */
v10 = (( java.lang.Float ) v10 ).floatValue ( ); // invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F
/* sub-float v10, v5, v10 */
/* float-to-double v13, v10 */
java.lang.Math .pow ( v13,v14,v11,v12 );
/* move-result-wide v13 */
v10 = this.mPathY;
/* check-cast v10, Ljava/lang/Float; */
v10 = (( java.lang.Float ) v10 ).floatValue ( ); // invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F
/* sub-float v10, v6, v10 */
/* float-to-double v2, v10 */
java.lang.Math .pow ( v2,v3,v11,v12 );
/* move-result-wide v2 */
/* add-double/2addr v13, v2 */
/* float-to-double v2, v7 */
/* cmpg-double v2, v13, v2 */
/* if-gez v2, :cond_5 */
/* .line 178 */
v2 = this.mPathX;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* sub-float v2, v5, v2 */
/* float-to-double v2, v2 */
java.lang.Math .pow ( v2,v3,v11,v12 );
/* move-result-wide v2 */
v10 = this.mPathY;
/* check-cast v10, Ljava/lang/Float; */
v10 = (( java.lang.Float ) v10 ).floatValue ( ); // invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F
/* sub-float v10, v6, v10 */
/* float-to-double v13, v10 */
java.lang.Math .pow ( v13,v14,v11,v12 );
/* move-result-wide v10 */
/* add-double/2addr v2, v10 */
/* double-to-float v2, v2 */
/* .line 179 */
} // .end local v7 # "minDistance":F
/* .local v2, "minDistance":F */
int v3 = 1; // const/4 v3, 0x1
/* iput-boolean v3, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z */
/* .line 180 */
/* move v3, v9 */
/* move v7, v2 */
/* move v8, v3 */
/* .line 176 */
} // .end local v2 # "minDistance":F
/* .restart local v7 # "minDistance":F */
} // :cond_5
/* add-int/lit8 v9, v9, 0x1 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 1; // const/4 v3, 0x1
/* .line 183 */
} // .end local v9 # "i":I
} // :cond_6
v2 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* div-int/lit8 v2, v2, 0x2 */
/* .local v2, "i":I */
} // :goto_2
v3 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
int v9 = 1; // const/4 v9, 0x1
/* sub-int/2addr v3, v9 */
/* if-ge v2, v3, :cond_8 */
/* .line 184 */
v3 = this.mPathX;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float v3, v1, v3 */
/* float-to-double v13, v3 */
java.lang.Math .pow ( v13,v14,v11,v12 );
/* move-result-wide v13 */
v3 = this.mPathY;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float v3, v4, v3 */
/* float-to-double v9, v3 */
java.lang.Math .pow ( v9,v10,v11,v12 );
/* move-result-wide v9 */
/* add-double/2addr v13, v9 */
/* float-to-double v9, v7 */
/* cmpg-double v3, v13, v9 */
/* if-gez v3, :cond_7 */
/* .line 185 */
v3 = this.mPathX;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float v3, v1, v3 */
/* float-to-double v9, v3 */
java.lang.Math .pow ( v9,v10,v11,v12 );
/* move-result-wide v9 */
v3 = this.mPathY;
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* sub-float v3, v4, v3 */
/* float-to-double v13, v3 */
java.lang.Math .pow ( v13,v14,v11,v12 );
/* move-result-wide v13 */
/* add-double/2addr v9, v13 */
/* double-to-float v3, v9 */
/* .line 186 */
} // .end local v7 # "minDistance":F
/* .local v3, "minDistance":F */
int v7 = 0; // const/4 v7, 0x0
/* iput-boolean v7, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z */
/* .line 187 */
/* move v7, v2 */
/* move v8, v7 */
/* move v7, v3 */
/* .line 183 */
} // .end local v3 # "minDistance":F
/* .restart local v7 # "minDistance":F */
} // :cond_7
/* add-int/lit8 v2, v2, 0x1 */
/* .line 190 */
} // .end local v2 # "i":I
} // :cond_8
/* if-lez v8, :cond_a */
/* .line 191 */
/* iget-boolean v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z */
if ( v2 != null) { // if-eqz v2, :cond_9
/* add-int/lit8 v2, v8, 0x7 */
v3 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* if-ge v2, v3, :cond_9 */
/* .line 192 */
v2 = this.mPathX;
/* add-int/lit8 v3, v8, 0x2 */
v9 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
this.mPathX = v2;
/* .line 193 */
v2 = this.mPathY;
/* add-int/lit8 v3, v8, 0x2 */
v9 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
this.mPathY = v2;
/* .line 194 */
} // :cond_9
/* iget-boolean v2, v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mIsBegin:Z */
/* if-nez v2, :cond_a */
/* const/16 v2, 0x8 */
/* if-le v8, v2, :cond_a */
/* .line 195 */
v2 = this.mPathX;
/* add-int/lit8 v3, v8, -0x3 */
int v9 = 0; // const/4 v9, 0x0
this.mPathX = v2;
/* .line 196 */
v2 = this.mPathY;
/* add-int/lit8 v3, v8, -0x3 */
this.mPathY = v2;
/* .line 200 */
} // .end local v1 # "beginX":F
} // .end local v4 # "beginY":F
} // .end local v5 # "lastX":F
} // .end local v6 # "lastY":F
} // .end local v7 # "minDistance":F
} // .end local v8 # "pointMinCutIndex":I
} // :cond_a
} // :goto_3
return;
} // .end method
private Double distance ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 6 */
/* .param p1, "x1" # F */
/* .param p2, "x2" # F */
/* .param p3, "y1" # F */
/* .param p4, "y2" # F */
/* .line 203 */
/* sub-float v0, p1, p2 */
/* float-to-double v0, v0 */
/* const-wide/high16 v2, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v0,v1,v2,v3 );
/* move-result-wide v0 */
/* sub-float v4, p3, p4 */
/* float-to-double v4, v4 */
java.lang.Math .pow ( v4,v5,v2,v3 );
/* move-result-wide v2 */
/* add-double/2addr v0, v2 */
java.lang.Math .sqrt ( v0,v1 );
/* move-result-wide v0 */
/* return-wide v0 */
} // .end method
private Float getPathBottom ( ) {
/* .locals 5 */
/* .line 233 */
v0 = v0 = this.mPathY;
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_0 */
v0 = this.mPathY;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // :cond_0
/* move v0, v1 */
/* .line 234 */
/* .local v0, "max":F */
} // :goto_0
v2 = this.mPathY;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* .line 235 */
/* .local v3, "y":F */
/* cmpl-float v4, v3, v0 */
/* if-lez v4, :cond_1 */
/* .line 236 */
/* move v0, v3 */
/* .line 238 */
} // .end local v3 # "y":F
} // :cond_1
/* .line 239 */
} // :cond_2
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 240 */
} // :cond_3
} // .end method
private Float getPathLeft ( ) {
/* .locals 5 */
/* .line 222 */
v0 = v0 = this.mPathX;
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_0 */
v0 = this.mPathX;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // :cond_0
/* move v0, v1 */
/* .line 223 */
/* .local v0, "min":F */
} // :goto_0
v2 = this.mPathX;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* .line 224 */
/* .local v3, "x":F */
/* cmpg-float v4, v3, v0 */
/* if-gez v4, :cond_1 */
/* .line 225 */
/* move v0, v3 */
/* .line 227 */
} // .end local v3 # "x":F
} // :cond_1
/* .line 228 */
} // :cond_2
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 229 */
} // :cond_3
} // .end method
private Float getPathRight ( ) {
/* .locals 5 */
/* .line 244 */
v0 = v0 = this.mPathX;
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_0 */
v0 = this.mPathX;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // :cond_0
/* move v0, v1 */
/* .line 245 */
/* .local v0, "max":F */
} // :goto_0
v2 = this.mPathX;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/Float; */
/* .line 246 */
/* .local v3, "x":Ljava/lang/Float; */
v4 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* cmpl-float v4, v4, v0 */
/* if-lez v4, :cond_1 */
/* .line 247 */
v0 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* .line 249 */
} // .end local v3 # "x":Ljava/lang/Float;
} // :cond_1
/* .line 250 */
} // :cond_2
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 251 */
} // :cond_3
} // .end method
private Float getPathTop ( ) {
/* .locals 5 */
/* .line 211 */
v0 = v0 = this.mPathY;
int v1 = 0; // const/4 v1, 0x0
/* if-lez v0, :cond_0 */
v0 = this.mPathY;
int v2 = 0; // const/4 v2, 0x0
/* check-cast v0, Ljava/lang/Float; */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
} // :cond_0
/* move v0, v1 */
/* .line 212 */
/* .local v0, "min":F */
} // :goto_0
v2 = this.mPathY;
v3 = } // :goto_1
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Ljava/lang/Float; */
v3 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
/* .line 213 */
/* .local v3, "y":F */
/* cmpg-float v4, v3, v0 */
/* if-gez v4, :cond_1 */
/* .line 214 */
/* move v0, v3 */
/* .line 216 */
} // .end local v3 # "y":F
} // :cond_1
/* .line 217 */
} // :cond_2
/* cmpg-float v1, v0, v1 */
/* if-gez v1, :cond_3 */
int v0 = 0; // const/4 v0, 0x0
/* .line 218 */
} // :cond_3
} // .end method
private void judgePath ( ) {
/* .locals 15 */
/* .line 113 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
/* .line 114 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* add-int/lit8 v1, v1, -0xb */
/* if-ge v0, v1, :cond_3 */
/* .line 115 */
v1 = this.mPathX;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F */
/* .line 116 */
v1 = this.mPathY;
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F */
/* .line 117 */
v1 = this.mPathX;
/* add-int/lit8 v2, v0, 0x1 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F */
/* .line 118 */
v1 = this.mPathY;
/* add-int/lit8 v2, v0, 0x1 */
/* check-cast v1, Ljava/lang/Float; */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F */
/* .line 119 */
/* add-int/lit8 v1, v0, 0xa */
/* .local v1, "j":I */
} // :goto_1
v2 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
int v3 = 1; // const/4 v3, 0x1
/* sub-int/2addr v2, v3 */
/* if-ge v1, v2, :cond_2 */
/* .line 120 */
v2 = this.mPathX;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F */
/* .line 121 */
v2 = this.mPathY;
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F */
/* .line 122 */
v2 = this.mPathX;
/* add-int/lit8 v4, v1, 0x1 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F */
/* .line 123 */
v2 = this.mPathY;
/* add-int/lit8 v4, v1, 0x1 */
/* check-cast v2, Ljava/lang/Float; */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F */
/* .line 125 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F */
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F */
v2 = java.lang.Math .min ( v2,v4 );
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F */
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F */
v4 = java.lang.Math .max ( v4,v5 );
/* cmpl-float v2, v2, v4 */
/* if-gez v2, :cond_1 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F */
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F */
/* .line 126 */
v2 = java.lang.Math .min ( v2,v4 );
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F */
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F */
v4 = java.lang.Math .max ( v4,v5 );
/* cmpl-float v2, v2, v4 */
/* if-gez v2, :cond_1 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F */
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F */
/* .line 127 */
v2 = java.lang.Math .min ( v2,v4 );
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F */
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F */
v4 = java.lang.Math .max ( v4,v5 );
/* cmpl-float v2, v2, v4 */
/* if-gez v2, :cond_1 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F */
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F */
/* .line 128 */
v2 = java.lang.Math .min ( v2,v4 );
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F */
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F */
v4 = java.lang.Math .max ( v4,v5 );
/* cmpl-float v2, v2, v4 */
/* if-ltz v2, :cond_0 */
/* .line 129 */
/* .line 131 */
} // :cond_0
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextX:F */
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPx:F */
/* sub-float v5, v2, v4 */
/* iget v6, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindY:F */
/* iget v7, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPy:F */
/* sub-float v8, v6, v7 */
/* mul-float/2addr v5, v8 */
/* iget v8, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mPreNextY:F */
/* sub-float v9, v8, v7 */
/* iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindX:F */
/* sub-float v11, v10, v4 */
/* mul-float/2addr v9, v11 */
/* sub-float/2addr v5, v9 */
/* .line 132 */
/* .local v5, "h":F */
/* sub-float v9, v2, v4 */
/* iget v11, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextY:F */
/* sub-float v12, v11, v7 */
/* mul-float/2addr v9, v12 */
/* sub-float v12, v8, v7 */
/* iget v13, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mBehindNextX:F */
/* sub-float v14, v13, v4 */
/* mul-float/2addr v12, v14 */
/* sub-float/2addr v9, v12 */
/* .line 133 */
/* .local v9, "m":F */
/* sub-float v12, v13, v10 */
/* sub-float/2addr v7, v6 */
/* mul-float/2addr v12, v7 */
/* sub-float v7, v11, v6 */
/* sub-float/2addr v4, v10 */
/* mul-float/2addr v7, v4 */
/* sub-float/2addr v12, v7 */
/* .line 134 */
/* .local v12, "n":F */
/* sub-float/2addr v13, v10 */
/* sub-float v4, v8, v6 */
/* mul-float/2addr v13, v4 */
/* sub-float/2addr v11, v6 */
/* sub-float v4, v2, v10 */
/* mul-float/2addr v11, v4 */
/* sub-float/2addr v13, v11 */
/* .line 135 */
/* .local v13, "k":F */
/* mul-float v4, v5, v9 */
/* const v7, 0x358637bd # 1.0E-6f */
/* cmpg-float v4, v4, v7 */
/* if-gtz v4, :cond_1 */
/* mul-float v4, v12, v13 */
/* cmpg-float v4, v4, v7 */
/* if-gtz v4, :cond_1 */
/* .line 136 */
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
/* add-int/2addr v4, v3 */
/* iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPointCount:I */
/* .line 137 */
/* if-ne v4, v3, :cond_1 */
/* .line 138 */
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreX:F */
/* .line 139 */
/* iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverPreY:F */
/* .line 140 */
/* iput v10, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindX:F */
/* .line 141 */
/* iput v6, p0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->mCrossoverBehindY:F */
/* .line 119 */
} // .end local v5 # "h":F
} // .end local v9 # "m":F
} // .end local v12 # "n":F
} // .end local v13 # "k":F
} // :cond_1
} // :goto_2
/* add-int/lit8 v1, v1, 0x1 */
/* goto/16 :goto_1 */
/* .line 114 */
} // .end local v1 # "j":I
} // :cond_2
/* add-int/lit8 v0, v0, 0x1 */
/* goto/16 :goto_0 */
/* .line 146 */
} // .end local v0 # "i":I
} // :cond_3
return;
} // .end method
/* # virtual methods */
public Boolean continueCheck ( ) {
/* .locals 2 */
/* .line 49 */
final String v0 = "partial_screen_shot"; // const-string v0, "partial_screen_shot"
v1 = this.mFunction;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 50 */
v0 = /* invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z */
/* .line 52 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
/* .locals 7 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 58 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_3 */
/* .line 59 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->checkIsValid()Z */
final String v1 = "KnockGesturePartialShot"; // const-string v1, "KnockGesturePartialShot"
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 60 */
(( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).setCheckSuccess ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setCheckSuccess()V
/* .line 61 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "check success path size is : "; // const-string v2, "check success path size is : "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v0 );
/* .line 62 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 63 */
/* .local v0, "bundle":Landroid/os/Bundle; */
v1 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* mul-int/lit8 v1, v1, 0x2 */
/* new-array v1, v1, [F */
/* .line 65 */
/* .local v1, "motionList":[F */
int v2 = 0; // const/4 v2, 0x0
/* .line 66 */
/* .local v2, "j":I */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).size ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->size()I
/* if-ge v3, v4, :cond_1 */
/* .line 67 */
v4 = this.mPathX;
/* check-cast v4, Ljava/lang/Float; */
v4 = (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
/* .line 68 */
/* .local v4, "x":F */
v5 = this.mPathY;
/* check-cast v5, Ljava/lang/Float; */
v5 = (( java.lang.Float ) v5 ).floatValue ( ); // invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F
/* .line 69 */
/* .local v5, "y":F */
v6 = java.lang.Float .isNaN ( v4 );
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 70 */
/* .line 72 */
} // :cond_0
/* add-int/lit8 v6, v2, 0x1 */
} // .end local v2 # "j":I
/* .local v6, "j":I */
/* aput v4, v1, v2 */
/* .line 73 */
/* add-int/lit8 v2, v6, 0x1 */
} // .end local v6 # "j":I
/* .restart local v2 # "j":I */
/* aput v5, v1, v6 */
/* .line 66 */
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 75 */
} // .end local v3 # "i":I
} // .end local v4 # "x":F
} // .end local v5 # "y":F
} // :cond_1
final String v3 = "partial.screenshot.points"; // const-string v3, "partial.screenshot.points"
(( android.os.Bundle ) v0 ).putFloatArray ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 76 */
v3 = this.mKnockPathListener;
/* .line 77 */
v3 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v3 );
v4 = this.mFunction;
final String v5 = "knock_slide_shape"; // const-string v5, "knock_slide_shape"
int v6 = 0; // const/4 v6, 0x0
(( com.miui.server.input.util.ShortCutActionsUtils ) v3 ).triggerFunction ( v4, v5, v0, v6 ); // invoke-virtual {v3, v4, v5, v0, v6}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 79 */
} // .end local v0 # "bundle":Landroid/os/Bundle;
} // .end local v1 # "motionList":[F
} // .end local v2 # "j":I
/* .line 80 */
} // :cond_2
final String v0 = "check fail"; // const-string v0, "check fail"
android.util.Slog .i ( v1,v0 );
/* .line 81 */
(( com.miui.server.input.knock.checker.KnockGesturePartialShot ) p0 ).setCheckFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setCheckFail()V
/* .line 84 */
} // :cond_3
} // :goto_2
return;
} // .end method
public Integer size ( ) {
/* .locals 1 */
/* .line 207 */
v0 = v0 = this.mPathY;
} // .end method
