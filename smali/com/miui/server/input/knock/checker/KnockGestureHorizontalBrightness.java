public class com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness extends com.miui.server.input.knock.KnockGestureChecker {
	 /* .source "KnockGestureHorizontalBrightness.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$BrightnessUtils; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer BRIGHTNESS_NO_CHANGE;
private static final Integer LONG_PRESS_TIME;
private static final java.lang.String TAG;
/* # instance fields */
private Float mBrightnessMax;
private Float mBrightnessMin;
private Integer mBrightnessViewLength;
private android.content.BroadcastReceiver mConfigurationReceiver;
private final android.hardware.display.DisplayManager mDisplayManager;
private android.util.DisplayMetrics mDisplayMetrics;
private Float mDownBrightness;
private Float mDownX;
private Float mDownY;
private android.os.Handler mHandler;
private miui.util.HapticFeedbackUtil mHapticFeedbackUtil;
private java.lang.Runnable mLongPressRunnable;
private Float mNowBrightness;
private Float minX;
/* # direct methods */
static android.util.DisplayMetrics -$$Nest$fgetmDisplayMetrics ( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mDisplayMetrics;
} // .end method
static miui.util.HapticFeedbackUtil -$$Nest$fgetmHapticFeedbackUtil ( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHapticFeedbackUtil;
} // .end method
static void -$$Nest$fputmBrightnessViewLength ( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput p1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I */
	 return;
} // .end method
static void -$$Nest$fputmDisplayMetrics ( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness p0, android.util.DisplayMetrics p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mDisplayMetrics = p1;
	 return;
} // .end method
public com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness ( ) {
	 /* .locals 4 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 60 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V */
	 /* .line 36 */
	 /* const/high16 v0, -0x40800000 # -1.0f */
	 /* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
	 /* .line 37 */
	 /* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F */
	 /* .line 38 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F */
	 /* .line 39 */
	 /* const/high16 v0, 0x3f800000 # 1.0f */
	 /* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F */
	 /* .line 49 */
	 /* new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$1;-><init>(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)V */
	 this.mConfigurationReceiver = v0;
	 /* .line 80 */
	 /* new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$2; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness$2;-><init>(Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;)V */
	 this.mLongPressRunnable = v0;
	 /* .line 61 */
	 /* const-class v0, Landroid/hardware/display/DisplayManager; */
	 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/hardware/display/DisplayManager; */
	 this.mDisplayManager = v0;
	 /* .line 62 */
	 (( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
	 (( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
	 this.mDisplayMetrics = v0;
	 /* .line 63 */
	 /* iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I */
	 v1 = this.mDisplayMetrics;
	 /* iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I */
	 v0 = 	 java.lang.Math .min ( v0,v1 );
	 /* int-to-double v0, v0 */
	 /* const-wide v2, 0x3fdded288ce703b0L # 0.4676 */
	 /* mul-double/2addr v0, v2 */
	 /* double-to-int v0, v0 */
	 /* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I */
	 /* .line 65 */
	 /* new-instance v0, Landroid/os/Handler; */
	 /* invoke-direct {v0}, Landroid/os/Handler;-><init>()V */
	 this.mHandler = v0;
	 /* .line 66 */
	 /* new-instance v0, Lmiui/util/HapticFeedbackUtil; */
	 int v1 = 0; // const/4 v1, 0x0
	 /* invoke-direct {v0, p1, v1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V */
	 this.mHapticFeedbackUtil = v0;
	 /* .line 67 */
	 v0 = this.mContext;
	 v1 = this.mConfigurationReceiver;
	 /* new-instance v2, Landroid/content/IntentFilter; */
	 final String v3 = "android.intent.action.CONFIGURATION_CHANGED"; // const-string v3, "android.intent.action.CONFIGURATION_CHANGED"
	 /* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
	 (( android.content.Context ) v0 ).registerReceiver ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
	 /* .line 68 */
	 return;
} // .end method
static Integer access$002 ( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness p0, Integer p1 ) { //synthethic
	 /* .locals 0 */
	 /* .param p0, "x0" # Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness; */
	 /* .param p1, "x1" # I */
	 /* .line 30 */
	 /* iput p1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I */
} // .end method
static com.miui.server.input.knock.view.KnockPathListener access$100 ( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness p0 ) { //synthethic
	 /* .locals 1 */
	 /* .param p0, "x0" # Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness; */
	 /* .line 30 */
	 v0 = this.mKnockPathListener;
} // .end method
private void updateMinX ( Float p0, Float p1 ) {
	 /* .locals 2 */
	 /* .param p1, "x" # F */
	 /* .param p2, "brightness" # F */
	 /* .line 170 */
	 /* iget v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F */
	 /* iget v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F */
	 v0 = 	 com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$BrightnessUtils .convertLinearToGammaFloat ( p2,v0,v1 );
	 /* int-to-float v0, v0 */
	 /* const/high16 v1, 0x3f800000 # 1.0f */
	 /* mul-float/2addr v0, v1 */
	 /* int-to-float v1, v1 */
	 /* div-float/2addr v0, v1 */
	 /* .line 171 */
	 /* .local v0, "percent":F */
	 /* iget v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I */
	 /* int-to-float v1, v1 */
	 /* mul-float/2addr v1, v0 */
	 /* sub-float v1, p1, v1 */
	 /* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->minX:F */
	 /* .line 172 */
	 return;
} // .end method
/* # virtual methods */
public Boolean continueCheck ( ) {
	 /* .locals 2 */
	 /* .line 73 */
	 final String v0 = "change_brightness"; // const-string v0, "change_brightness"
	 v1 = this.mFunction;
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 74 */
		 v0 = 		 /* invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z */
		 /* .line 76 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onTouchEvent ( android.view.MotionEvent p0 ) {
	 /* .locals 8 */
	 /* .param p1, "event" # Landroid/view/MotionEvent; */
	 /* .line 91 */
	 v0 = 	 (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
	 /* .line 92 */
	 /* .local v0, "nowX":F */
	 v1 = 	 (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
	 /* .line 93 */
	 /* .local v1, "nowY":F */
	 v2 = 	 (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
	 int v3 = 3; // const/4 v3, 0x3
	 /* packed-switch v2, :pswitch_data_0 */
	 /* goto/16 :goto_1 */
	 /* .line 101 */
	 /* :pswitch_0 */
	 /* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownX:F */
	 /* sub-float v2, v0, v2 */
	 v2 = 	 java.lang.Math .abs ( v2 );
	 /* .line 102 */
	 /* .local v2, "diffDownX":F */
	 /* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownY:F */
	 /* sub-float v4, v1, v4 */
	 v4 = 	 java.lang.Math .abs ( v4 );
	 /* .line 104 */
	 /* .local v4, "diffDownY":F */
	 /* const/high16 v5, 0x42480000 # 50.0f */
	 /* cmpl-float v6, v2, v5 */
	 /* if-gtz v6, :cond_0 */
	 /* cmpl-float v5, v4, v5 */
	 /* if-lez v5, :cond_1 */
	 /* .line 105 */
} // :cond_0
v5 = this.mHandler;
v6 = this.mLongPressRunnable;
v5 = (( android.os.Handler ) v5 ).hasCallbacks ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
	 /* .line 106 */
	 v5 = this.mHandler;
	 v6 = this.mLongPressRunnable;
	 (( android.os.Handler ) v5 ).removeCallbacks ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
	 /* .line 107 */
	 (( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness ) p0 ).setCheckFail ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setCheckFail()V
	 /* .line 111 */
} // :cond_1
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I */
int v6 = 2; // const/4 v6, 0x2
/* if-ne v5, v6, :cond_4 */
/* .line 113 */
/* const/high16 v5, 0x42c80000 # 100.0f */
/* cmpl-float v5, v4, v5 */
final String v6 = "KnockBrightness"; // const-string v6, "KnockBrightness"
/* if-lez v5, :cond_2 */
/* .line 114 */
final String v3 = "knock feature brightness check fail"; // const-string v3, "knock feature brightness check fail"
android.util.Slog .i ( v6,v3 );
/* .line 115 */
int v3 = 4; // const/4 v3, 0x4
/* iput v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I */
/* goto/16 :goto_1 */
/* .line 116 */
} // :cond_2
/* div-float v5, v2, v4 */
/* const/high16 v7, 0x40a00000 # 5.0f */
/* cmpl-float v5, v5, v7 */
/* if-lez v5, :cond_7 */
/* const/high16 v5, 0x43160000 # 150.0f */
/* cmpl-float v5, v2, v5 */
/* if-lez v5, :cond_7 */
/* .line 117 */
final String v5 = "knock feature brightness check success"; // const-string v5, "knock feature brightness check success"
android.util.Slog .i ( v6,v5 );
/* .line 118 */
/* iput v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I */
/* .line 120 */
try { // :try_start_0
v3 = this.mContext;
(( android.content.Context ) v3 ).getDisplay ( ); // invoke-virtual {v3}, Landroid/content/Context;->getDisplay()Landroid/view/Display;
(( android.view.Display ) v3 ).getBrightnessInfo ( ); // invoke-virtual {v3}, Landroid/view/Display;->getBrightnessInfo()Landroid/hardware/display/BrightnessInfo;
/* .line 121 */
/* .local v3, "info":Landroid/hardware/display/BrightnessInfo; */
if ( v3 != null) { // if-eqz v3, :cond_3
	 /* .line 122 */
	 /* iget v5, v3, Landroid/hardware/display/BrightnessInfo;->brightnessMaximum:F */
	 /* iput v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F */
	 /* .line 123 */
	 /* iget v5, v3, Landroid/hardware/display/BrightnessInfo;->brightnessMinimum:F */
	 /* iput v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F */
	 /* .line 124 */
	 /* iget v5, v3, Landroid/hardware/display/BrightnessInfo;->brightness:F */
	 /* iput v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F */
	 /* .line 126 */
} // :cond_3
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F */
/* invoke-direct {p0, v0, v5}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->updateMinX(FF)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // .end local v3 # "info":Landroid/hardware/display/BrightnessInfo;
/* .line 127 */
/* :catch_0 */
/* move-exception v3 */
/* .line 128 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 129 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :goto_0
/* .line 131 */
} // :cond_4
/* iget v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I */
/* if-ne v5, v3, :cond_7 */
/* .line 132 */
(( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness ) p0 ).setBrightness ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setBrightness(Landroid/view/MotionEvent;)V
/* .line 136 */
} // .end local v2 # "diffDownX":F
} // .end local v4 # "diffDownY":F
/* :pswitch_1 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
/* const/high16 v4, -0x40800000 # -1.0f */
/* cmpl-float v2, v2, v4 */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 137 */
v2 = this.mDisplayManager;
v5 = (( android.view.MotionEvent ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDisplayId()I
/* iget v6, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
(( android.hardware.display.DisplayManager ) v2 ).setBrightness ( v5, v6 ); // invoke-virtual {v2, v5, v6}, Landroid/hardware/display/DisplayManager;->setBrightness(IF)V
/* .line 138 */
/* iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
/* .line 140 */
} // :cond_5
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mCheckState:I */
/* if-ne v2, v3, :cond_6 */
/* .line 141 */
v2 = this.mContext;
com.android.server.input.shortcut.ShortcutOneTrackHelper .getInstance ( v2 );
final String v3 = "knock_long_press_horizontal_slid"; // const-string v3, "knock_long_press_horizontal_slid"
v4 = this.mFunction;
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v2 ).trackShortcutEventTrigger ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 144 */
} // :cond_6
v2 = this.mHandler;
v3 = this.mLongPressRunnable;
(( android.os.Handler ) v2 ).removeCallbacks ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 95 */
/* :pswitch_2 */
/* iput v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownX:F */
/* .line 96 */
/* iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownY:F */
/* .line 97 */
v2 = this.mHandler;
v3 = this.mLongPressRunnable;
(( android.os.Handler ) v2 ).removeCallbacks ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 98 */
v2 = this.mHandler;
v3 = this.mLongPressRunnable;
/* const-wide/16 v4, 0x1f4 */
(( android.os.Handler ) v2 ).postDelayed ( v3, v4, v5 ); // invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 99 */
/* nop */
/* .line 147 */
} // :cond_7
} // :goto_1
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void setBrightness ( android.view.MotionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 150 */
v0 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* .line 151 */
/* .local v0, "nowX":F */
/* iget v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->minX:F */
/* sub-float v1, v0, v1 */
/* const/high16 v2, 0x3f800000 # 1.0f */
/* mul-float/2addr v1, v2 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessViewLength:I */
/* int-to-float v2, v2 */
/* div-float/2addr v1, v2 */
/* int-to-float v2, v2 */
/* mul-float/2addr v1, v2 */
/* float-to-int v1, v1 */
/* .line 152 */
/* .local v1, "slideValue":I */
/* if-gez v1, :cond_0 */
/* .line 153 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F */
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
/* .line 154 */
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F */
/* .line 155 */
/* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->updateMinX(FF)V */
/* .line 156 */
} // :cond_0
/* if-le v1, v2, :cond_1 */
/* .line 157 */
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F */
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
/* .line 158 */
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mDownBrightness:F */
/* .line 159 */
/* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->updateMinX(FF)V */
/* .line 161 */
} // :cond_1
/* iget v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMin:F */
/* iget v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mBrightnessMax:F */
v2 = com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness$BrightnessUtils .convertGammaToLinearFloat ( v1,v2,v3 );
/* iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
/* .line 163 */
} // :goto_0
v2 = this.mDisplayManager;
v3 = (( android.view.MotionEvent ) p1 ).getDisplayId ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getDisplayId()I
/* iget v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->mNowBrightness:F */
(( android.hardware.display.DisplayManager ) v2 ).setTemporaryBrightness ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/hardware/display/DisplayManager;->setTemporaryBrightness(IF)V
/* .line 164 */
return;
} // .end method
