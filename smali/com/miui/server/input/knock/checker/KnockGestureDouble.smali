.class public Lcom/miui/server/input/knock/checker/KnockGestureDouble;
.super Lcom/miui/server/input/knock/KnockGestureChecker;
.source "KnockGestureDouble.java"


# static fields
.field private static final DOUBLE_KNOCK_MAX_MOVE_DISTANCE:I = 0x1

.field private static final DOUBLE_KNOCK_TIME:I = 0x1f4

.field private static final DOUBLE_KNOCK_VALID_DISTANCE:I = 0x2

.field public static final TAG:Ljava/lang/String; = "KnockGestureDouble"


# instance fields
.field private mDoubleKnockValidDistance:I

.field private mLastKnockDownX:F

.field private mLastKnockDownY:F

.field private mLastKnockTime:J

.field private mLastTriggerTime:J

.field private mMaxMoveDistance:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/KnockGestureChecker;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->initScreenSize()V

    .line 38
    return-void
.end method

.method private initScreenSize()V
    .locals 6

    .line 99
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 100
    .local v0, "mWindowManager":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 101
    .local v1, "defaultDisplay":Landroid/view/Display;
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 102
    .local v2, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 103
    iget v3, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v3, v3

    const v4, 0x40228f5c    # 2.54f

    div-float/2addr v3, v4

    .line 104
    .local v3, "oneCentimeterPix":F
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v3

    float-to-int v4, v4

    const/16 v5, 0x96

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I

    .line 105
    const/high16 v4, 0x3f800000    # 1.0f

    mul-float/2addr v4, v3

    float-to-int v4, v4

    const/16 v5, 0x64

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I

    .line 106
    return-void
.end method

.method private triggerDoubleKnockFeature()V
    .locals 7

    .line 82
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mKnockPathListener:Lcom/miui/server/input/knock/view/KnockPathListener;

    invoke-interface {v0}, Lcom/miui/server/input/knock/view/KnockPathListener;->hideView()V

    .line 83
    invoke-virtual {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->setCheckSuccess()V

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 85
    .local v0, "nowTime":J
    iget-wide v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastTriggerTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 86
    return-void

    .line 88
    :cond_0
    iput-wide v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastTriggerTime:J

    .line 89
    iget-object v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mFunction:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v6, "double_knock"

    invoke-virtual {v2, v3, v6, v4, v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 90
    return-void
.end method


# virtual methods
.method public continueCheck()Z
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mFunction:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->checkEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    invoke-super {p0}, Lcom/miui/server/input/knock/KnockGestureChecker;->continueCheck()Z

    move-result v0

    return v0

    .line 45
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onScreenSizeChanged()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->initScreenSize()V

    .line 95
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 51
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 52
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 53
    .local v1, "nowX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 55
    .local v2, "nowY":F
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 56
    .local v3, "nowTime":J
    const-wide/16 v5, 0x0

    const-string v7, "KnockGestureDouble"

    const/4 v8, 0x0

    if-nez v0, :cond_1

    .line 57
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "knock down, time:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " nowX:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " nowY:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mDoubleKnockValidDistance:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-wide v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J

    sub-long v9, v3, v9

    const-wide/16 v11, 0x1f4

    cmp-long v7, v9, v11

    if-gez v7, :cond_0

    iget v7, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F

    sub-float v7, v1, v7

    .line 59
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I

    int-to-float v9, v9

    cmpg-float v7, v7, v9

    if-gez v7, :cond_0

    iget v7, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F

    sub-float v7, v2, v7

    .line 60
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mDoubleKnockValidDistance:I

    int-to-float v9, v9

    cmpg-float v7, v7, v9

    if-gez v7, :cond_0

    .line 61
    iput-wide v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J

    .line 62
    iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F

    .line 63
    iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F

    .line 64
    invoke-direct {p0}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->triggerDoubleKnockFeature()V

    goto :goto_0

    .line 66
    :cond_0
    iput v1, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F

    .line 67
    iput v2, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F

    .line 68
    iput-wide v3, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J

    goto :goto_0

    .line 70
    :cond_1
    const/4 v9, 0x2

    if-ne v0, v9, :cond_3

    .line 72
    iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F

    sub-float v9, v1, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-gtz v9, :cond_2

    iget v9, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F

    sub-float v9, v2, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_3

    .line 73
    :cond_2
    iput-wide v5, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockTime:J

    .line 74
    iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownX:F

    .line 75
    iput v8, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mLastKnockDownY:F

    .line 76
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "knock move more than mMaxMoveDistance:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->mMaxMoveDistance:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_3
    :goto_0
    return-void
.end method
