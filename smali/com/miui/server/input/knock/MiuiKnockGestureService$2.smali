.class Lcom/miui/server/input/knock/MiuiKnockGestureService$2;
.super Landroid/content/BroadcastReceiver;
.source "MiuiKnockGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/MiuiKnockGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;


# direct methods
.method constructor <init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/knock/MiuiKnockGestureService;

    .line 116
    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 119
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmWindowManager(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmTempPoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 120
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmSizePoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmTempPoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmSizePoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmTempPoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-eq v0, v1, :cond_0

    .line 121
    const-string v0, "MiuiKnockGestureService"

    const-string v1, "Screen resolution conversion so reset Knock Valid Area"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$msetKnockValidRect(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    .line 123
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmKnockCheckDelegate(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/KnockCheckDelegate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/knock/KnockCheckDelegate;->onScreenSizeChanged()V

    .line 125
    :cond_0
    return-void
.end method
