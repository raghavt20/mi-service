public abstract class com.miui.server.input.knock.KnockGestureChecker {
	 /* .source "KnockGestureChecker.java" */
	 /* # static fields */
	 protected static final Integer STATE_CHECKING_ALL_CHECKER;
	 protected static final Integer STATE_CHECKING_ONLY_CHECKER;
	 protected static final Integer STATE_FAIL;
	 protected static final Integer STATE_SUCCESS;
	 /* # instance fields */
	 protected Integer mCheckState;
	 protected android.content.Context mContext;
	 protected java.lang.String mFunction;
	 protected com.miui.server.input.knock.view.KnockPathListener mKnockPathListener;
	 /* # direct methods */
	 protected com.miui.server.input.knock.KnockGestureChecker ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 24 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 18 */
		 int v0 = 1; // const/4 v0, 0x1
		 /* iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I */
		 /* .line 25 */
		 this.mContext = p1;
		 /* .line 26 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 protected Boolean checkEmpty ( java.lang.String p0 ) {
		 /* .locals 1 */
		 /* .param p1, "feature" # Ljava/lang/String; */
		 /* .line 69 */
		 v0 = 		 android.text.TextUtils .isEmpty ( p1 );
		 /* if-nez v0, :cond_1 */
		 final String v0 = "none"; // const-string v0, "none"
		 v0 = 		 (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 72 */
		 } // :cond_0
		 int v0 = 0; // const/4 v0, 0x0
		 /* .line 70 */
	 } // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean checkOnlyOneGesture ( ) {
/* .locals 2 */
/* .line 36 */
/* iget v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I */
int v1 = 3; // const/4 v1, 0x3
/* if-eq v0, v1, :cond_1 */
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_0 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
public Boolean continueCheck ( ) {
/* .locals 2 */
/* .line 31 */
/* iget v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I */
int v1 = 3; // const/4 v1, 0x3
/* if-ge v0, v1, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void onScreenSizeChanged ( ) {
/* .locals 0 */
/* .line 62 */
return;
} // .end method
public abstract void onTouchEvent ( android.view.MotionEvent p0 ) {
} // .end method
public void resetState ( ) {
/* .locals 1 */
/* .line 53 */
int v0 = 1; // const/4 v0, 0x1
/* iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I */
/* .line 54 */
return;
} // .end method
protected void setCheckFail ( ) {
/* .locals 1 */
/* .line 44 */
int v0 = 4; // const/4 v0, 0x4
/* iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I */
/* .line 45 */
return;
} // .end method
protected void setCheckSuccess ( ) {
/* .locals 1 */
/* .line 40 */
int v0 = 3; // const/4 v0, 0x3
/* iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I */
/* .line 41 */
return;
} // .end method
public void setFunction ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "function" # Ljava/lang/String; */
/* .line 57 */
this.mFunction = p1;
/* .line 58 */
return;
} // .end method
void setKnockPathListener ( com.miui.server.input.knock.view.KnockPathListener p0 ) {
/* .locals 0 */
/* .param p1, "knockPathListener" # Lcom/miui/server/input/knock/view/KnockPathListener; */
/* .line 65 */
this.mKnockPathListener = p1;
/* .line 66 */
return;
} // .end method
