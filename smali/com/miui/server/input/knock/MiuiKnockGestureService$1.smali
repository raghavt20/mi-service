.class Lcom/miui/server/input/knock/MiuiKnockGestureService$1;
.super Ljava/lang/Object;
.source "MiuiKnockGestureService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/MiuiKnockGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;


# direct methods
.method constructor <init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/knock/MiuiKnockGestureService;

    .line 97
    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    const-string/jumbo v1, "vendor.panel.vendor"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmDisplayVersion(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmDisplayVersion(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmDisplayVersion(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isInteger(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$mdisplayVersionSupportCheck(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$mfeatureSupportInit(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    goto :goto_1

    .line 102
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmDelayCheckTimes(Lcom/miui/server/input/knock/MiuiKnockGestureService;)I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2

    .line 103
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmDelayCheckTimes(Lcom/miui/server/input/knock/MiuiKnockGestureService;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fputmDelayCheckTimes(Lcom/miui/server/input/knock/MiuiKnockGestureService;I)V

    .line 104
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;->this$0:Lcom/miui/server/input/knock/MiuiKnockGestureService;

    invoke-static {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->-$$Nest$fgetmHandler(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/server/input/knock/MiuiKnockGestureService$1$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$1$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService$1;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 106
    :cond_2
    const-string v0, "MiuiKnockGestureService"

    const-string v1, "display_version get fail"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_3
    :goto_1
    return-void
.end method
