.class public Lcom/miui/server/input/knock/MiuiKnockGestureService;
.super Ljava/lang/Object;
.source "MiuiKnockGestureService.java"

# interfaces
.implements Landroid/view/WindowManagerPolicyConstants$PointerEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/knock/MiuiKnockGestureService$H;,
        Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field private static final DELAY_CHECK_TIME:I = 0x3e8

.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final KEY_OPEN_GAME_BOOSTER:Ljava/lang/String; = "pref_open_game_booster"

.field private static final LOG_NATIVE_FEATURE_ERROR:Ljava/lang/String; = "native feature not merge"

.field private static final MOTION_EVENT_TOOL_TYPE_KNOCK:I = 0x20

.field private static final PROPERTY_COLLECT_DATA:I = 0x2

.field private static final PROPERTY_DISPLAY_VERSION:Ljava/lang/String; = "vendor.panel.vendor"

.field private static final PROPERTY_OPEN_KNOCK_FEATURE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiuiKnockGestureService"

.field private static sMiuiKnockGestureService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/server/input/knock/MiuiKnockGestureService;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCheckerDoubleKnock:Lcom/miui/server/input/knock/checker/KnockGestureDouble;

.field private mCheckerGesturePartialShot:Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

.field private mCheckerHorizontalBrightness:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

.field private mCheckerKnockGestureV:Lcom/miui/server/input/knock/checker/KnockGestureV;

.field private mCloseKnockAllFeature:Z

.field private mConfigurationReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mDelayCheckTimes:I

.field private mDelayRunnable:Ljava/lang/Runnable;

.field private mDisplayVersion:Ljava/lang/String;

.field private mFunctionDoubleKnock:Ljava/lang/String;

.field private mFunctionGesturePartial:Ljava/lang/String;

.field private mFunctionGestureV:Ljava/lang/String;

.field private mFunctionHorizontalSlid:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHasRegisterListener:Z

.field private mInputManagerService:Lcom/android/server/input/InputManagerService;

.field private mIsGameMode:Z

.field private mIsScreenOn:Z

.field private mIsSetupComplete:Z

.field private mIsStatusBarVisible:Z

.field private mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

.field private mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

.field private mKnockConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

.field private mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

.field private mNativeFeatureState:I

.field private mOpenGameBooster:Z

.field private mSizePoint:Landroid/graphics/Point;

.field private mTempPoint:Landroid/graphics/Point;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCheckerDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGestureDouble;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerDoubleKnock:Lcom/miui/server/input/knock/checker/KnockGestureDouble;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCheckerGesturePartialShot(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerGesturePartialShot:Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCheckerHorizontalBrightness(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerHorizontalBrightness:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCheckerKnockGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/checker/KnockGestureV;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerKnockGestureV:Lcom/miui/server/input/knock/checker/KnockGestureV;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDelayCheckTimes(Lcom/miui/server/input/knock/MiuiKnockGestureService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDelayCheckTimes:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayVersion(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDisplayVersion:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFunctionDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionDoubleKnock:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFunctionGesturePartial(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGesturePartial:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFunctionGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFunctionHorizontalSlid(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionHorizontalSlid:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsGameMode(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsScreenOn(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsScreenOn:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmKnockCheckDelegate(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Lcom/miui/server/input/knock/KnockCheckDelegate;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmOpenGameBooster(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmSizePoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmTempPoint(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/graphics/Point;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mTempPoint:Landroid/graphics/Point;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmWindowManager(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Landroid/view/WindowManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mWindowManager:Landroid/view/WindowManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmCloseKnockAllFeature(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDelayCheckTimes(Lcom/miui/server/input/knock/MiuiKnockGestureService;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDelayCheckTimes:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmDisplayVersion(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDisplayVersion:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFunctionDoubleKnock(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionDoubleKnock:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFunctionGesturePartial(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGesturePartial:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFunctionGestureV(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmFunctionHorizontalSlid(Lcom/miui/server/input/knock/MiuiKnockGestureService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionHorizontalSlid:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsGameMode(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsScreenOn(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsScreenOn:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmIsStatusBarVisible(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsStatusBarVisible:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmOpenGameBooster(Lcom/miui/server/input/knock/MiuiKnockGestureService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$mdisplayVersionSupportCheck(Lcom/miui/server/input/knock/MiuiKnockGestureService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->displayVersionSupportCheck()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mfeatureSupportInit(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->featureSupportInit()V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetKnockValidRect(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockValidRect()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateKnockFeatureState(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateKnockFeatureState()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, -0x2

    iput v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCurrentUserId:I

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z

    .line 94
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    .line 95
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mTempPoint:Landroid/graphics/Point;

    .line 97
    new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDelayRunnable:Ljava/lang/Runnable;

    .line 116
    new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;

    invoke-direct {v0, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mConfigurationReceiver:Landroid/content/BroadcastReceiver;

    .line 162
    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->initialize(Landroid/content/Context;)V

    .line 164
    return-void
.end method

.method private checkEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "feature"    # Ljava/lang/String;

    .line 430
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 433
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 431
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method private displayVersionSupportCheck()Z
    .locals 6

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v0, "knockConfigFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;>;"
    new-instance v1, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;-><init>(Ljava/lang/String;)V

    .line 203
    .local v1, "filterDeviceName":Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;
    new-instance v2, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;

    iget-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDisplayVersion:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;-><init>(Ljava/lang/String;)V

    .line 205
    .local v2, "filterDisplayVersion":Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    invoke-virtual {v3, v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->getKnockConfig(Ljava/util/List;)Lcom/miui/server/input/knock/config/KnockConfig;

    move-result-object v3

    iput-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    .line 208
    const/4 v4, 0x0

    if-nez v3, :cond_0

    .line 209
    return v4

    .line 212
    :cond_0
    const-string v5, "MiuiKnockGestureService"

    invoke-virtual {v3}, Lcom/miui/server/input/knock/config/KnockConfig;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget v3, v3, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I

    const/4 v5, 0x1

    and-int/2addr v3, v5

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget v3, v3, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    goto :goto_0

    .line 218
    :cond_1
    return v4

    .line 216
    :cond_2
    :goto_0
    return v5
.end method

.method private featureSupportInit()V
    .locals 4

    .line 227
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockValidRect()V

    .line 228
    const-string/jumbo v0, "screen_resolution_supported"

    invoke-static {v0}, Lmiui/util/FeatureParser;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mConfigurationReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 231
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockDeviceProperty()V

    .line 232
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->localAlgorithmPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockDlcPath(Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->knockScoreThreshold:F

    invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockScoreThreshold(F)V

    .line 234
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->useFrame:I

    invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockUseFrame(I)V

    .line 235
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->quickMoveSpeed:F

    invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockQuickMoveSpeed(F)V

    .line 236
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->sensorThreshold:[I

    invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockSensorThreshold([I)V

    .line 238
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "support_knock"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    .line 240
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    const-string v1, "double_knock"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionDoubleKnock:Ljava/lang/String;

    .line 242
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    const-string v1, "knock_gesture_v"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    .line 244
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    const-string v1, "knock_slide_shape"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGesturePartial:Ljava/lang/String;

    .line 246
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    const-string v1, "knock_long_press_horizontal_slid"

    invoke-static {v0, v1}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionHorizontalSlid:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pref_open_game_booster"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x0

    if-ne v0, v2, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z

    .line 250
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "gb_boosting"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z

    .line 253
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "close_knock_all_feature"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_3

    goto :goto_2

    :cond_3
    move v2, v1

    :goto_2
    iput-boolean v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z

    .line 256
    new-instance v0, Lcom/miui/server/input/knock/KnockCheckDelegate;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    .line 257
    new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerHorizontalBrightness:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    .line 258
    new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureV;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureV;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerKnockGestureV:Lcom/miui/server/input/knock/checker/KnockGestureV;

    .line 259
    new-instance v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerGesturePartialShot:Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

    .line 260
    new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureDouble;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerDoubleKnock:Lcom/miui/server/input/knock/checker/KnockGestureDouble;

    .line 262
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerKnockGestureV:Lcom/miui/server/input/knock/checker/KnockGestureV;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureV;->setFunction(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerDoubleKnock:Lcom/miui/server/input/knock/checker/KnockGestureDouble;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionDoubleKnock:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->setFunction(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerGesturePartialShot:Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGesturePartial:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setFunction(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerHorizontalBrightness:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionHorizontalSlid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setFunction(Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerDoubleKnock:Lcom/miui/server/input/knock/checker/KnockGestureDouble;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V

    .line 267
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerKnockGestureV:Lcom/miui/server/input/knock/checker/KnockGestureV;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V

    .line 268
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerHorizontalBrightness:Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V

    .line 269
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCheckerGesturePartialShot:Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V

    .line 271
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isUserSetUp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z

    .line 273
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateKnockFeatureState()V

    .line 275
    new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 276
    invoke-virtual {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->observe()V

    .line 277
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->sMiuiKnockGestureService:Ljava/lang/ref/WeakReference;

    .line 279
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->removeUnsupportedFunction()V

    .line 281
    return-void
.end method

.method public static finishPostLayoutPolicyLw(Z)V
    .locals 1
    .param p0, "statusBarVisible"    # Z

    .line 153
    sget-object v0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->sMiuiKnockGestureService:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/knock/MiuiKnockGestureService;

    .line 155
    .local v0, "miuiKnockGestureService":Lcom/miui/server/input/knock/MiuiKnockGestureService;
    if-eqz v0, :cond_0

    .line 156
    invoke-direct {v0, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->onStatusBarVisibilityChangeStatic(Z)V

    .line 159
    .end local v0    # "miuiKnockGestureService":Lcom/miui/server/input/knock/MiuiKnockGestureService;
    :cond_0
    return-void
.end method

.method private initDeviceKnockProperty()V
    .locals 5

    .line 178
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    invoke-virtual {v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->isDeviceSupport()Z

    move-result v0

    .line 179
    .local v0, "deviceSupport":Z
    if-nez v0, :cond_0

    .line 180
    return-void

    .line 184
    :cond_0
    const-string/jumbo v1, "vendor.panel.vendor"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDisplayVersion:Ljava/lang/String;

    .line 185
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDisplayVersion:Ljava/lang/String;

    invoke-static {v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isInteger(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    .line 188
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->displayVersionSupportCheck()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->featureSupportInit()V

    goto :goto_1

    .line 192
    :cond_2
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string/jumbo v2, "support_knock"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/MiuiSettings$Global;->putBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto :goto_1

    .line 186
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDelayRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    :goto_1
    return-void
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 167
    iput-object p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    .line 168
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lcom/android/server/wm/WindowManagerService;

    iput-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 169
    const-string v1, "input"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    check-cast v1, Lcom/android/server/input/InputManagerService;

    iput-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mInputManagerService:Lcom/android/server/input/InputManagerService;

    .line 170
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mWindowManager:Landroid/view/WindowManager;

    .line 171
    new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;Lcom/miui/server/input/knock/MiuiKnockGestureService$H-IA;)V

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    .line 172
    invoke-static {}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->getInstance()Lcom/miui/server/input/knock/config/KnockConfigHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfigHelper:Lcom/miui/server/input/knock/config/KnockConfigHelper;

    .line 173
    invoke-virtual {v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->initConfig()V

    .line 174
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->initDeviceKnockProperty()V

    .line 175
    return-void
.end method

.method private isFeatureInit()Z
    .locals 1

    .line 149
    sget-object v0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->sMiuiKnockGestureService:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isInteger(Ljava/lang/String;)Z
    .locals 2
    .param p0, "displayVersion"    # Ljava/lang/String;

    .line 222
    const-string v0, "^[-\\+]?[\\d]*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 223
    .local v0, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method private isMotionUserSetUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 548
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 549
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isUserSetUp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z

    .line 551
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z

    return v0
.end method

.method private isUserSetUp()Z
    .locals 4

    .line 555
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "user_setup_complete"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method private onStatusBarVisibilityChangeStatic(Z)V
    .locals 3
    .param p1, "statusBarVisible"    # Z

    .line 311
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 312
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 313
    return-void
.end method

.method private removeUnsupportedFunction()V
    .locals 4

    .line 477
    const-string v0, "launch_ai_shortcut"

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "none"

    const/4 v2, -0x2

    const-string v3, "knock_gesture_v"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/MiuiSettings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 482
    :cond_0
    return-void
.end method

.method private setKnockDeviceProperty()V
    .locals 2

    .line 364
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 365
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget v1, v1, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I

    invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockDeviceProperty(I)V

    .line 366
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 367
    return-void
.end method

.method private setKnockDlcPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "dlcPath"    # Ljava/lang/String;

    .line 400
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 401
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->updateAlgorithmPath(Ljava/lang/String;)V

    .line 402
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 403
    return-void
.end method

.method private setKnockInvalidRect(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 394
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 395
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/input/config/InputKnockConfig;->setKnockInValidRect(IIII)V

    .line 396
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 397
    return-void
.end method

.method private setKnockQuickMoveSpeed(F)V
    .locals 1
    .param p1, "quickMoveSpeed"    # F

    .line 418
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 419
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockQuickMoveSpeed(F)V

    .line 420
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 421
    return-void
.end method

.method private setKnockScoreThreshold(F)V
    .locals 1
    .param p1, "score"    # F

    .line 406
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 407
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockScoreThreshold(F)V

    .line 408
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 409
    return-void
.end method

.method private setKnockSensorThreshold([I)V
    .locals 1
    .param p1, "sensorThreshold"    # [I

    .line 424
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 425
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockSensorThreshold([I)V

    .line 426
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 427
    return-void
.end method

.method private setKnockUseFrame(I)V
    .locals 1
    .param p1, "useFrame"    # I

    .line 412
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 413
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockUseFrame(I)V

    .line 414
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 415
    return-void
.end method

.method private setKnockValidRect()V
    .locals 8

    .line 375
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 376
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 377
    .local v0, "width":I
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mSizePoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 378
    .local v1, "height":I
    iget-object v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v2, v2, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    mul-int/2addr v2, v0

    iget-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v3, v3, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    div-int/2addr v2, v3

    .line 379
    .local v2, "left":I
    iget-object v3, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v3, v3, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    mul-int/2addr v3, v1

    iget-object v5, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v5, v5, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    div-int/2addr v3, v5

    .line 380
    .local v3, "top":I
    iget-object v5, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v5, v5, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    aget v5, v5, v4

    iget-object v7, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v7, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v7

    mul-int/2addr v5, v0

    iget-object v7, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v7, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    aget v4, v7, v4

    div-int/2addr v5, v4

    .line 382
    .local v5, "right":I
    iget-object v4, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v4, v4, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    aget v4, v4, v6

    iget-object v7, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v7, v7, Lcom/miui/server/input/knock/config/KnockConfig;->knockRegion:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v7

    mul-int/2addr v4, v1

    iget-object v7, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    iget-object v7, v7, Lcom/miui/server/input/knock/config/KnockConfig;->deviceX:[I

    aget v6, v7, v6

    div-int/2addr v4, v6

    .line 384
    .local v4, "bottom":I
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v6

    .line 385
    .local v6, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v6, v2, v3, v5, v4}, Lcom/android/server/input/config/InputKnockConfig;->setKnockValidRect(IIII)V

    .line 386
    invoke-virtual {v6}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 388
    return-void
.end method

.method private setNativeKnockFeatureState(I)V
    .locals 1
    .param p1, "state"    # I

    .line 355
    iget v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mNativeFeatureState:I

    if-eq p1, v0, :cond_0

    .line 356
    iput p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mNativeFeatureState:I

    .line 357
    invoke-static {}, Lcom/android/server/input/config/InputKnockConfig;->getInstance()Lcom/android/server/input/config/InputKnockConfig;

    move-result-object v0

    .line 358
    .local v0, "config":Lcom/android/server/input/config/InputKnockConfig;
    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockFeatureState(I)V

    .line 359
    invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V

    .line 361
    .end local v0    # "config":Lcom/android/server/input/config/InputKnockConfig;
    :cond_0
    return-void
.end method

.method private updateKnockFeatureState()V
    .locals 3

    .line 317
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z

    const-string v1, "MiuiKnockGestureService"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 318
    const-string v0, "mCloseKnockAllFeature true, close knock"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V

    .line 320
    return-void

    .line 322
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsScreenOn:Z

    if-nez v0, :cond_1

    .line 323
    const-string v0, "mIsScreenOn false, close knock"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V

    .line 325
    return-void

    .line 327
    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z

    if-eqz v0, :cond_2

    .line 328
    const-string v0, "mIsGameMode true, close knock"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V

    .line 330
    return-void

    .line 332
    :cond_2
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z

    if-nez v0, :cond_3

    .line 333
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsStatusBarVisible:Z

    if-nez v0, :cond_3

    .line 334
    const-string v0, "mOpenGameBooster false, mIsStatusBarVisible false, close knock"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V

    .line 336
    return-void

    .line 339
    :cond_3
    const/4 v0, 0x0

    .line 340
    .local v0, "currentFeatureState":Z
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionDoubleKnock:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGesturePartial:Ljava/lang/String;

    .line 341
    invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionHorizontalSlid:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 342
    :cond_4
    const/4 v0, 0x1

    .line 344
    :cond_5
    if-eqz v0, :cond_6

    iget-boolean v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z

    if-nez v1, :cond_6

    .line 345
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1, p0, v2}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 346
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z

    goto :goto_0

    .line 347
    :cond_6
    if-nez v0, :cond_7

    iget-boolean v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z

    if-eqz v1, :cond_7

    .line 348
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    invoke-virtual {v1, p0, v2}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 349
    iput-boolean v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z

    .line 351
    :cond_7
    :goto_0
    invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V

    .line 352
    return-void
.end method

.method private updateSettings()V
    .locals 3

    .line 457
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    if-eqz v0, :cond_0

    .line 458
    nop

    .line 459
    const-string v1, "double_knock"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 458
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 460
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 461
    const-string v1, "knock_gesture_v"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 460
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 462
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 463
    const-string v1, "knock_slide_shape"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 462
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 464
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 465
    const-string v1, "knock_long_press_horizontal_slid"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 464
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 466
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 467
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 466
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 468
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 469
    const-string v1, "pref_open_game_booster"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 468
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 470
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mMiuiSettingsObserver:Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;

    .line 471
    const-string v1, "close_knock_all_feature"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 470
    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 473
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->removeUnsupportedFunction()V

    .line 474
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 561
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 562
    const-string v0, "MiuiKnockGestureService"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 563
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 564
    const-string v0, "mIsSetupComplete="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 565
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 566
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 567
    const-string v0, "mCloseKnockAllFeature="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 568
    iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 569
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 570
    const-string v0, "mKnockConfig="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 571
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockConfig:Lcom/miui/server/input/knock/config/KnockConfig;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 572
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 573
    const-string v0, "mDisplayVersion="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 574
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDisplayVersion:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 575
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 576
    const-string v0, "mFeatureInit="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 577
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isFeatureInit()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 578
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 579
    const-string v0, "mDoubleKnockFunction="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 580
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionDoubleKnock:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 581
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 582
    const-string v0, "mKnockGestureV="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 583
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGestureV:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 584
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 585
    const-string v0, "mGesturePartialFunction="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionGesturePartial:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 587
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 588
    const-string v0, "mHorizontalSlidFunction="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 589
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mFunctionHorizontalSlid:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 591
    return-void
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 438
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 439
    invoke-direct {p0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isMotionUserSetUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mKnockCheckDelegate:Lcom/miui/server/input/knock/KnockCheckDelegate;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->onTouchEvent(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 442
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 443
    const-string v0, "MiuiKnockGestureService"

    const-string/jumbo v1, "user not setup complete, action down"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :cond_1
    :goto_0
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "newUserId"    # I

    .line 450
    iget v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCurrentUserId:I

    if-eq v0, p1, :cond_0

    .line 451
    iput p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCurrentUserId:I

    .line 452
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateSettings()V

    .line 454
    :cond_0
    return-void
.end method

.method public setInputMethodRect(Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "inputMethodRegion"    # Landroid/graphics/Rect;

    .line 129
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isFeatureInit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    return-void

    .line 132
    :cond_0
    if-nez p1, :cond_1

    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockInvalidRect(IIII)V

    goto :goto_0

    .line 135
    :cond_1
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockInvalidRect(IIII)V

    .line 137
    :goto_0
    return-void
.end method

.method public updateScreenState(Z)V
    .locals 3
    .param p1, "screenOn"    # Z

    .line 140
    invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isFeatureInit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 144
    .local v0, "message":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 145
    return-void
.end method
