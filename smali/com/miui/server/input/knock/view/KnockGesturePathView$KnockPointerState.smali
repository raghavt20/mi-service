.class public Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
.super Ljava/lang/Object;
.source "KnockGesturePathView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/knock/view/KnockGesturePathView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KnockPointerState"
.end annotation


# instance fields
.field public mTraceCount:I

.field public mTraceCurrent:[Z

.field public mTraceX:[Ljava/lang/Float;

.field public mTraceY:[Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    const/16 v0, 0x20

    new-array v1, v0, [Ljava/lang/Float;

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    .line 257
    new-array v1, v0, [Ljava/lang/Float;

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    .line 258
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCurrent:[Z

    return-void
.end method


# virtual methods
.method public addTrace(FFZ)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "current"    # Z

    .line 262
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    array-length v1, v0

    .line 263
    .local v1, "traceCapacity":I
    iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    if-ne v2, v1, :cond_0

    .line 264
    mul-int/lit8 v1, v1, 0x2

    .line 265
    new-array v3, v1, [Ljava/lang/Float;

    .line 266
    .local v3, "newTraceX":[Ljava/lang/Float;
    const/4 v4, 0x0

    invoke-static {v0, v4, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 267
    iput-object v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    .line 269
    new-array v0, v1, [Ljava/lang/Float;

    .line 270
    .local v0, "newTraceY":[Ljava/lang/Float;
    iget-object v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    iget v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    invoke-static {v2, v4, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 271
    iput-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    .line 273
    new-array v2, v1, [Z

    .line 274
    .local v2, "newTraceCurrent":[Z
    iget-object v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCurrent:[Z

    iget v6, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    invoke-static {v5, v4, v2, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 275
    iput-object v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCurrent:[Z

    .line 278
    .end local v0    # "newTraceY":[Ljava/lang/Float;
    .end local v2    # "newTraceCurrent":[Z
    .end local v3    # "newTraceX":[Ljava/lang/Float;
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v0, v2

    .line 279
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v0, v2

    .line 280
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCurrent:[Z

    iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    aput-boolean p3, v0, v2

    .line 281
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    .line 282
    return-void
.end method

.method public getTraceCount()I
    .locals 1

    .line 285
    iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I

    return v0
.end method
