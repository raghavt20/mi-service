public class com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState {
	 /* .source "KnockGesturePathView.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/knock/view/KnockGesturePathView; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x9 */
/* name = "KnockPointerState" */
} // .end annotation
/* # instance fields */
public Integer mTraceCount;
public mTraceCurrent;
public java.lang.Float mTraceX;
public java.lang.Float mTraceY;
/* # direct methods */
public com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ( ) {
/* .locals 2 */
/* .line 254 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 256 */
/* const/16 v0, 0x20 */
/* new-array v1, v0, [Ljava/lang/Float; */
this.mTraceX = v1;
/* .line 257 */
/* new-array v1, v0, [Ljava/lang/Float; */
this.mTraceY = v1;
/* .line 258 */
/* new-array v0, v0, [Z */
this.mTraceCurrent = v0;
return;
} // .end method
/* # virtual methods */
public void addTrace ( Float p0, Float p1, Boolean p2 ) {
/* .locals 7 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .param p3, "current" # Z */
/* .line 262 */
v0 = this.mTraceX;
/* array-length v1, v0 */
/* .line 263 */
/* .local v1, "traceCapacity":I */
/* iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* if-ne v2, v1, :cond_0 */
/* .line 264 */
/* mul-int/lit8 v1, v1, 0x2 */
/* .line 265 */
/* new-array v3, v1, [Ljava/lang/Float; */
/* .line 266 */
/* .local v3, "newTraceX":[Ljava/lang/Float; */
int v4 = 0; // const/4 v4, 0x0
java.lang.System .arraycopy ( v0,v4,v3,v4,v2 );
/* .line 267 */
this.mTraceX = v3;
/* .line 269 */
/* new-array v0, v1, [Ljava/lang/Float; */
/* .line 270 */
/* .local v0, "newTraceY":[Ljava/lang/Float; */
v2 = this.mTraceY;
/* iget v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
java.lang.System .arraycopy ( v2,v4,v0,v4,v5 );
/* .line 271 */
this.mTraceY = v0;
/* .line 273 */
/* new-array v2, v1, [Z */
/* .line 274 */
/* .local v2, "newTraceCurrent":[Z */
v5 = this.mTraceCurrent;
/* iget v6, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
java.lang.System .arraycopy ( v5,v4,v2,v4,v6 );
/* .line 275 */
this.mTraceCurrent = v2;
/* .line 278 */
} // .end local v0 # "newTraceY":[Ljava/lang/Float;
} // .end local v2 # "newTraceCurrent":[Z
} // .end local v3 # "newTraceX":[Ljava/lang/Float;
} // :cond_0
v0 = this.mTraceX;
/* iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
java.lang.Float .valueOf ( p1 );
/* aput-object v3, v0, v2 */
/* .line 279 */
v0 = this.mTraceY;
/* iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
java.lang.Float .valueOf ( p2 );
/* aput-object v3, v0, v2 */
/* .line 280 */
v0 = this.mTraceCurrent;
/* iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* aput-boolean p3, v0, v2 */
/* .line 281 */
/* add-int/lit8 v2, v2, 0x1 */
/* iput v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
/* .line 282 */
return;
} // .end method
public Integer getTraceCount ( ) {
/* .locals 1 */
/* .line 285 */
/* iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceCount:I */
} // .end method
