public class com.miui.server.input.knock.view.KnockGesturePathView extends android.view.View implements com.miui.server.input.knock.view.KnockPathListener {
	 /* .source "KnockGesturePathView.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Float BLUR_SIZE;
private static final Integer FLAG_SHOW_FOR_ALL_USERS;
private static final Integer MIN_DISTANCE_SHOW_VIEW;
private static final java.lang.String TAG;
/* # instance fields */
private Integer mDataPointCount;
private volatile Boolean mDistanceShowView;
private Float mDownX;
private Float mDownY;
private final android.os.Handler mHandler;
private volatile Boolean mIsAdded;
private android.view.WindowManager$LayoutParams mLayoutParams;
private final android.graphics.Paint mMaskLine;
private android.graphics.Path mPath;
private final android.graphics.Paint mPloyLine;
private final java.util.ArrayList mPointers;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.Runnable mRemoveWindowRunnable;
private Boolean mVisibility;
private final android.view.WindowManager mWindowManager;
/* # direct methods */
public static void $r8$lambda$wSjAPGlKp8Y213QYl5kGI9vfVcc ( com.miui.server.input.knock.view.KnockGesturePathView p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->lambda$tryAddToWindow$0()V */
return;
} // .end method
static Boolean -$$Nest$fgetmIsAdded ( com.miui.server.input.knock.view.KnockGesturePathView p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z */
} // .end method
static android.view.WindowManager -$$Nest$fgetmWindowManager ( com.miui.server.input.knock.view.KnockGesturePathView p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWindowManager;
} // .end method
static void -$$Nest$fputmIsAdded ( com.miui.server.input.knock.view.KnockGesturePathView p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z */
return;
} // .end method
public com.miui.server.input.knock.view.KnockGesturePathView ( ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 58 */
/* invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V */
/* .line 28 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPointers = v0;
/* .line 34 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z */
/* .line 39 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z */
/* .line 42 */
/* new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView$1;-><init>(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V */
this.mRemoveWindowRunnable = v1;
/* .line 59 */
int v1 = 2; // const/4 v1, 0x2
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).setLayerType ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->setLayerType(ILandroid/graphics/Paint;)V
/* .line 60 */
v1 = this.mContext;
/* const-class v2, Landroid/view/WindowManager; */
(( android.content.Context ) v1 ).getSystemService ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v1, Landroid/view/WindowManager; */
this.mWindowManager = v1;
/* .line 61 */
/* new-instance v1, Landroid/os/Handler; */
com.android.server.input.MiuiInputThread .getThread ( );
(( com.android.server.input.MiuiInputThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v1;
/* .line 62 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->initLayoutParam()V */
/* .line 63 */
/* new-instance v1, Landroid/graphics/Paint; */
/* invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V */
this.mPloyLine = v1;
/* .line 64 */
(( android.graphics.Paint ) v1 ).setFlags ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setFlags(I)V
/* .line 65 */
v2 = android.graphics.Paint$Cap.ROUND;
(( android.graphics.Paint ) v1 ).setStrokeCap ( v2 ); // invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V
/* .line 66 */
v2 = android.graphics.Paint$Join.ROUND;
(( android.graphics.Paint ) v1 ).setStrokeJoin ( v2 ); // invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V
/* .line 67 */
v2 = android.graphics.Paint$Style.STROKE;
(( android.graphics.Paint ) v1 ).setStyle ( v2 ); // invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
/* .line 68 */
/* const/16 v2, 0xff */
/* const/16 v3, 0x22 */
/* const/16 v4, 0xe0 */
(( android.graphics.Paint ) v1 ).setARGB ( v2, v3, v4, v2 ); // invoke-virtual {v1, v2, v3, v4, v2}, Landroid/graphics/Paint;->setARGB(IIII)V
/* .line 69 */
/* const/high16 v5, 0x40900000 # 4.5f */
(( android.graphics.Paint ) v1 ).setStrokeWidth ( v5 ); // invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V
/* .line 70 */
/* new-instance v1, Landroid/graphics/Paint; */
/* invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V */
this.mMaskLine = v1;
/* .line 71 */
(( android.graphics.Paint ) v1 ).setFlags ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setFlags(I)V
/* .line 72 */
v0 = android.graphics.Paint$Style.STROKE;
(( android.graphics.Paint ) v1 ).setStyle ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
/* .line 73 */
v0 = android.graphics.Paint$Cap.ROUND;
(( android.graphics.Paint ) v1 ).setStrokeCap ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V
/* .line 74 */
v0 = android.graphics.Paint$Join.ROUND;
(( android.graphics.Paint ) v1 ).setStrokeJoin ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V
/* .line 75 */
/* new-instance v0, Landroid/graphics/BlurMaskFilter; */
/* const/high16 v5, 0x40a00000 # 5.0f */
v6 = android.graphics.BlurMaskFilter$Blur.SOLID;
/* invoke-direct {v0, v5, v6}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V */
(( android.graphics.Paint ) v1 ).setMaskFilter ( v0 ); // invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;
/* .line 76 */
(( android.graphics.Paint ) v1 ).setARGB ( v2, v3, v4, v2 ); // invoke-virtual {v1, v2, v3, v4, v2}, Landroid/graphics/Paint;->setARGB(IIII)V
/* .line 77 */
/* new-instance v0, Landroid/graphics/Path; */
/* invoke-direct {v0}, Landroid/graphics/Path;-><init>()V */
this.mPath = v0;
/* .line 78 */
return;
} // .end method
private void flush ( ) {
/* .locals 2 */
/* .line 242 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 243 */
return;
} // .end method
private void initLayoutParam ( ) {
/* .locals 4 */
/* .line 81 */
/* new-instance v0, Landroid/view/WindowManager$LayoutParams; */
int v1 = -1; // const/4 v1, -0x1
/* invoke-direct {v0, v1, v1}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V */
this.mLayoutParams = v0;
/* .line 84 */
/* const/16 v1, 0x7df */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 85 */
v0 = this.mLayoutParams;
/* const/16 v1, 0x518 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 89 */
v0 = this.mLayoutParams;
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 91 */
v0 = android.app.ActivityManager .isHighEndGfx ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 92 */
v0 = this.mLayoutParams;
/* iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* const/high16 v3, 0x1000000 */
/* or-int/2addr v2, v3 */
/* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 93 */
v0 = this.mLayoutParams;
/* iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit8 v2, v2, 0x2 */
/* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 96 */
} // :cond_0
v0 = this.mLayoutParams;
int v2 = -3; // const/4 v2, -0x3
/* iput v2, v0, Landroid/view/WindowManager$LayoutParams;->format:I */
/* .line 97 */
v0 = this.mLayoutParams;
/* iget v2, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I */
/* or-int/2addr v1, v2 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I */
/* .line 98 */
v0 = this.mLayoutParams;
/* iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* or-int/lit8 v1, v1, 0x10 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I */
/* .line 99 */
v0 = this.mLayoutParams;
final String v1 = "knock_drawing"; // const-string v1, "knock_drawing"
(( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 100 */
return;
} // .end method
private void lambda$tryAddToWindow$0 ( ) { //synthethic
/* .locals 2 */
/* .line 141 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z */
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 142 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z */
/* .line 144 */
try { // :try_start_0
	 v0 = this.mWindowManager;
	 v1 = this.mLayoutParams;
	 /* :try_end_0 */
	 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 147 */
	 /* .line 145 */
	 /* :catch_0 */
	 /* move-exception v0 */
	 /* .line 146 */
	 /* .local v0, "e":Ljava/lang/Exception; */
	 (( java.lang.Exception ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
	 /* .line 149 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_0
return;
} // .end method
private void pathLink ( android.graphics.Path p0 ) {
/* .locals 6 */
/* .param p1, "path" # Landroid/graphics/Path; */
/* .line 246 */
/* iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
int v1 = 2; // const/4 v1, 0x2
/* if-ge v0, v1, :cond_0 */
return;
/* .line 247 */
} // :cond_0
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v0 = this.mTraceX;
/* iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* sub-int/2addr v2, v1 */
/* aget-object v0, v0, v2 */
v0 = (( java.lang.Float ) v0 ).floatValue ( ); // invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
/* .line 248 */
/* .local v0, "preX":F */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v2 = this.mTraceY;
/* iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* sub-int/2addr v3, v1 */
/* aget-object v1, v2, v3 */
v1 = (( java.lang.Float ) v1 ).floatValue ( ); // invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
/* .line 249 */
/* .local v1, "preY":F */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v2 = this.mTraceX;
/* iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* add-int/lit8 v3, v3, -0x1 */
/* aget-object v2, v2, v3 */
v2 = (( java.lang.Float ) v2 ).floatValue ( ); // invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F
/* add-float/2addr v2, v0 */
/* const/high16 v3, 0x40000000 # 2.0f */
/* div-float/2addr v2, v3 */
/* .line 250 */
/* .local v2, "endX":F */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v4 = this.mTraceY;
/* iget v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* add-int/lit8 v5, v5, -0x1 */
/* aget-object v4, v4, v5 */
v4 = (( java.lang.Float ) v4 ).floatValue ( ); // invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
/* add-float/2addr v4, v1 */
/* div-float/2addr v4, v3 */
/* .line 251 */
/* .local v4, "endY":F */
(( android.graphics.Path ) p1 ).quadTo ( v0, v1, v2, v4 ); // invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Path;->quadTo(FFFF)V
/* .line 252 */
return;
} // .end method
private void tryAddToWindow ( ) {
/* .locals 2 */
/* .line 140 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 150 */
return;
} // .end method
private void tryRemoveWindow ( ) {
/* .locals 4 */
/* .line 153 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).hideView ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->hideView()V
/* .line 154 */
v0 = this.mHandler;
v1 = this.mRemoveWindowRunnable;
/* const-wide/16 v2, 0xbb8 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 155 */
return;
} // .end method
/* # virtual methods */
public com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState getPointerPathData ( ) {
/* .locals 2 */
/* .line 229 */
v0 = this.mPointers;
v0 = (( java.util.ArrayList ) v0 ).size ( ); // invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
/* if-nez v0, :cond_0 */
/* .line 230 */
int v0 = 0; // const/4 v0, 0x0
/* .line 232 */
} // :cond_0
v0 = this.mPointers;
int v1 = 0; // const/4 v1, 0x0
(( java.util.ArrayList ) v0 ).get ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
} // .end method
public void hideView ( ) {
/* .locals 1 */
/* .line 237 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z */
/* .line 238 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->flush()V */
/* .line 239 */
return;
} // .end method
protected void onAttachedToWindow ( ) {
/* .locals 2 */
/* .line 104 */
final String v0 = "KnockGesturePathView -> onAttachedToWindow"; // const-string v0, "KnockGesturePathView -> onAttachedToWindow"
final String v1 = "KnockGesturePathView"; // const-string v1, "KnockGesturePathView"
android.util.Slog .i ( v1,v0 );
/* .line 106 */
v0 = this.mParent;
/* if-nez v0, :cond_0 */
/* .line 107 */
final String v0 = "onAttachedToWindow mParent null"; // const-string v0, "onAttachedToWindow mParent null"
android.util.Slog .i ( v1,v0 );
/* .line 109 */
} // :cond_0
/* invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V */
/* .line 111 */
} // :goto_0
return;
} // .end method
protected void onDetachedFromWindow ( ) {
/* .locals 2 */
/* .line 115 */
final String v0 = "KnockGesturePathView"; // const-string v0, "KnockGesturePathView"
final String v1 = "KnockGesturePathView -> onDetachedFromWindow"; // const-string v1, "KnockGesturePathView -> onDetachedFromWindow"
android.util.Slog .i ( v0,v1 );
/* .line 116 */
/* invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V */
/* .line 117 */
return;
} // .end method
protected void onDraw ( android.graphics.Canvas p0 ) {
/* .locals 10 */
/* .param p1, "canvas" # Landroid/graphics/Canvas; */
/* .line 121 */
/* invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V */
/* .line 123 */
int v0 = 0; // const/4 v0, 0x0
v1 = android.graphics.PorterDuff$Mode.CLEAR;
(( android.graphics.Canvas ) p1 ).drawColor ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V
/* .line 124 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
int v1 = 5; // const/4 v1, 0x5
/* if-le v0, v1, :cond_1 */
/* .line 125 */
v0 = this.mPath;
v1 = this.mPloyLine;
(( android.graphics.Canvas ) p1 ).drawPath ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
/* .line 126 */
/* const/16 v0, 0xf */
/* .line 127 */
/* .local v0, "mLastLightCount":I */
/* iget v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* const/16 v2, 0xf */
/* if-ge v1, v2, :cond_0 */
/* .line 128 */
/* iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* .line 130 */
} // :cond_0
/* const/high16 v2, 0x40900000 # 4.5f */
/* .line 131 */
/* .local v2, "m":F */
/* sub-int/2addr v1, v0 */
/* .local v1, "i":I */
} // :goto_0
/* iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* add-int/lit8 v3, v3, -0x1 */
/* if-ge v1, v3, :cond_1 */
/* .line 132 */
v3 = this.mMaskLine;
/* const v4, 0x3f4ccccd # 0.8f */
/* add-float/2addr v4, v2 */
/* move v2, v4 */
(( android.graphics.Paint ) v3 ).setStrokeWidth ( v4 ); // invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V
/* .line 133 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v3 = this.mTraceX;
/* aget-object v3, v3, v1 */
v5 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v3 = this.mTraceY;
/* aget-object v3, v3, v1 */
v6 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v3 = this.mTraceX;
/* add-int/lit8 v4, v1, 0x1 */
/* aget-object v3, v3, v4 */
v7 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v3 = this.mTraceY;
/* add-int/lit8 v4, v1, 0x1 */
/* aget-object v3, v3, v4 */
v8 = (( java.lang.Float ) v3 ).floatValue ( ); // invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F
v9 = this.mMaskLine;
/* move-object v4, p1 */
/* invoke-virtual/range {v4 ..v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V */
/* .line 131 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 136 */
} // .end local v0 # "mLastLightCount":I
} // .end local v1 # "i":I
} // .end local v2 # "m":F
} // :cond_1
return;
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 11 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 163 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* .line 164 */
/* .local v0, "action":I */
int v1 = 1; // const/4 v1, 0x1
int v2 = 0; // const/4 v2, 0x0
/* packed-switch v0, :pswitch_data_0 */
/* :pswitch_0 */
/* goto/16 :goto_2 */
/* .line 220 */
/* :pswitch_1 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).hideView ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->hideView()V
/* .line 221 */
/* goto/16 :goto_2 */
/* .line 187 */
/* :pswitch_2 */
v3 = this.mPointers;
v3 = (( java.util.ArrayList ) v3 ).size ( ); // invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
/* if-nez v3, :cond_0 */
/* .line 188 */
return;
/* .line 190 */
} // :cond_0
v3 = this.mPointers;
(( java.util.ArrayList ) v3 ).get ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
/* .line 191 */
/* .local v3, "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
v4 = (( android.view.MotionEvent ) p1 ).getHistorySize ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I
/* if-lez v4, :cond_1 */
/* .line 192 */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
} // :goto_0
v5 = (( android.view.MotionEvent ) p1 ).getHistorySize ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I
/* if-ge v4, v5, :cond_1 */
/* .line 193 */
v5 = (( android.view.MotionEvent ) p1 ).getHistoricalAxisValue ( v2, v4 ); // invoke-virtual {p1, v2, v4}, Landroid/view/MotionEvent;->getHistoricalAxisValue(II)F
v6 = (( android.view.MotionEvent ) p1 ).getHistoricalAxisValue ( v1, v4 ); // invoke-virtual {p1, v1, v4}, Landroid/view/MotionEvent;->getHistoricalAxisValue(II)F
(( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v3 ).addTrace ( v5, v6, v2 ); // invoke-virtual {v3, v5, v6, v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->addTrace(FFZ)V
/* .line 194 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v5 = (( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v5 ).getTraceCount ( ); // invoke-virtual {v5}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I
/* iput v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* .line 195 */
v5 = this.mPath;
/* invoke-direct {p0, v5}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->pathLink(Landroid/graphics/Path;)V */
/* .line 192 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 198 */
} // .end local v4 # "i":I
} // :cond_1
v2 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* .line 199 */
/* .local v2, "nowX":F */
v4 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* .line 200 */
/* .local v4, "nowY":F */
/* iget-boolean v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z */
/* if-nez v5, :cond_2 */
/* iget v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownX:F */
/* sub-float v5, v2, v5 */
/* float-to-double v5, v5 */
/* const-wide/high16 v7, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v5,v6,v7,v8 );
/* move-result-wide v5 */
/* iget v9, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownY:F */
/* sub-float v9, v4, v9 */
/* float-to-double v9, v9 */
java.lang.Math .pow ( v9,v10,v7,v8 );
/* move-result-wide v7 */
/* add-double/2addr v5, v7 */
java.lang.Math .sqrt ( v5,v6 );
/* move-result-wide v5 */
/* const-wide/high16 v7, 0x4059000000000000L # 100.0 */
/* cmpl-double v5, v5, v7 */
/* if-lez v5, :cond_2 */
/* .line 201 */
/* iput-boolean v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z */
/* .line 202 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->tryAddToWindow()V */
/* .line 204 */
} // :cond_2
(( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v3 ).addTrace ( v2, v4, v1 ); // invoke-virtual {v3, v2, v4, v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->addTrace(FFZ)V
/* .line 205 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v1 = (( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v1 ).getTraceCount ( ); // invoke-virtual {v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I
/* iput v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* .line 206 */
v1 = this.mPath;
/* invoke-direct {p0, v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->pathLink(Landroid/graphics/Path;)V */
/* .line 207 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->flush()V */
/* .line 208 */
/* goto/16 :goto_2 */
/* .line 211 */
} // .end local v2 # "nowX":F
} // .end local v3 # "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
} // .end local v4 # "nowY":F
/* :pswitch_3 */
v1 = this.mPointers;
v1 = (( java.util.ArrayList ) v1 ).size ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
/* if-nez v1, :cond_3 */
/* .line 212 */
return;
/* .line 214 */
} // :cond_3
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v1 = (( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v1 ).getTraceCount ( ); // invoke-virtual {v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I
/* iput v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* .line 215 */
v1 = this.mPath;
/* invoke-direct {p0, v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->pathLink(Landroid/graphics/Path;)V */
/* .line 216 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).hideView ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->hideView()V
/* .line 217 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->tryRemoveWindow()V */
/* .line 218 */
/* .line 166 */
/* :pswitch_4 */
v3 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* iput v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownX:F */
/* .line 167 */
v3 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* iput v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownY:F */
/* .line 168 */
v3 = this.mHandler;
v4 = this.mRemoveWindowRunnable;
(( android.os.Handler ) v3 ).removeCallbacks ( v4 ); // invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 169 */
/* iput-boolean v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z */
/* .line 170 */
/* iput-boolean v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z */
/* .line 171 */
v1 = this.mPointers;
(( java.util.ArrayList ) v1 ).clear ( ); // invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V
/* .line 172 */
/* new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
/* invoke-direct {v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;-><init>()V */
/* .line 173 */
/* .local v1, "ps":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState; */
v3 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
v4 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
(( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v1 ).addTrace ( v3, v4, v2 ); // invoke-virtual {v1, v3, v4, v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->addTrace(FFZ)V
/* .line 174 */
v2 = this.mPointers;
(( java.util.ArrayList ) v2 ).add ( v1 ); // invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 175 */
v2 = this.mPath;
/* if-nez v2, :cond_4 */
/* .line 176 */
/* new-instance v2, Landroid/graphics/Path; */
/* invoke-direct {v2}, Landroid/graphics/Path;-><init>()V */
this.mPath = v2;
/* .line 178 */
} // :cond_4
(( android.graphics.Path ) v2 ).reset ( ); // invoke-virtual {v2}, Landroid/graphics/Path;->reset()V
/* .line 180 */
} // :goto_1
v2 = this.mPath;
/* iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownX:F */
/* iget v4, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownY:F */
(( android.graphics.Path ) v2 ).moveTo ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V
/* .line 181 */
(( com.miui.server.input.knock.view.KnockGesturePathView ) p0 ).getPointerPathData ( ); // invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
v2 = (( com.miui.server.input.knock.view.KnockGesturePathView$KnockPointerState ) v2 ).getTraceCount ( ); // invoke-virtual {v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I
/* iput v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I */
/* .line 182 */
/* iget-boolean v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z */
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 183 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->flush()V */
/* .line 225 */
} // .end local v1 # "ps":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
} // :cond_5
} // :goto_2
return;
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
public void removeViewImmediate ( ) {
/* .locals 2 */
/* .line 158 */
v0 = this.mHandler;
v1 = this.mRemoveWindowRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 159 */
v0 = this.mHandler;
v1 = this.mRemoveWindowRunnable;
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 160 */
return;
} // .end method
