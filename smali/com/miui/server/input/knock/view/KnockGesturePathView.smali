.class public Lcom/miui/server/input/knock/view/KnockGesturePathView;
.super Landroid/view/View;
.source "KnockGesturePathView.java"

# interfaces
.implements Lcom/miui/server/input/knock/view/KnockPathListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    }
.end annotation


# static fields
.field private static final BLUR_SIZE:F = 5.0f

.field private static final FLAG_SHOW_FOR_ALL_USERS:I = 0x10

.field private static final MIN_DISTANCE_SHOW_VIEW:I = 0x64

.field private static final TAG:Ljava/lang/String; = "KnockGesturePathView"


# instance fields
.field private mDataPointCount:I

.field private volatile mDistanceShowView:Z

.field private mDownX:F

.field private mDownY:F

.field private final mHandler:Landroid/os/Handler;

.field private volatile mIsAdded:Z

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private final mMaskLine:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private final mPloyLine:Landroid/graphics/Paint;

.field private final mPointers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;",
            ">;"
        }
    .end annotation
.end field

.field private final mRemoveWindowRunnable:Ljava/lang/Runnable;

.field private mVisibility:Z

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public static synthetic $r8$lambda$wSjAPGlKp8Y213QYl5kGI9vfVcc(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->lambda$tryAddToWindow$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmIsAdded(Lcom/miui/server/input/knock/view/KnockGesturePathView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmWindowManager(Lcom/miui/server/input/knock/view/KnockGesturePathView;)Landroid/view/WindowManager;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mWindowManager:Landroid/view/WindowManager;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmIsAdded(Lcom/miui/server/input/knock/view/KnockGesturePathView;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .line 58
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z

    .line 39
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z

    .line 42
    new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView$1;-><init>(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mRemoveWindowRunnable:Ljava/lang/Runnable;

    .line 59
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 60
    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mContext:Landroid/content/Context;

    const-class v2, Landroid/view/WindowManager;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mWindowManager:Landroid/view/WindowManager;

    .line 61
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getThread()Lcom/android/server/input/MiuiInputThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    .line 62
    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->initLayoutParam()V

    .line 63
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPloyLine:Landroid/graphics/Paint;

    .line 64
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 65
    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 66
    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 67
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    const/16 v2, 0xff

    const/16 v3, 0x22

    const/16 v4, 0xe0

    invoke-virtual {v1, v2, v3, v4, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 69
    const/high16 v5, 0x40900000    # 4.5f

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 70
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mMaskLine:Landroid/graphics/Paint;

    .line 71
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 72
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 73
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 74
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 75
    new-instance v0, Landroid/graphics/BlurMaskFilter;

    const/high16 v5, 0x40a00000    # 5.0f

    sget-object v6, Landroid/graphics/BlurMaskFilter$Blur;->SOLID:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v0, v5, v6}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 76
    invoke-virtual {v1, v2, v3, v4, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 77
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    .line 78
    return-void
.end method

.method private flush()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 243
    return-void
.end method

.method private initLayoutParam()V
    .locals 4

    .line 81
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 84
    const/16 v1, 0x7df

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 85
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x518

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 89
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 91
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v3, 0x1000000

    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 93
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, -0x3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 97
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    .line 98
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 99
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "knock_drawing"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 100
    return-void
.end method

.method private synthetic lambda$tryAddToWindow$0()V
    .locals 2

    .line 141
    iget-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 149
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    return-void
.end method

.method private pathLink(Landroid/graphics/Path;)V
    .locals 6
    .param p1, "path"    # Landroid/graphics/Path;

    .line 246
    iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v0

    iget-object v0, v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    iget v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    sub-int/2addr v2, v1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 248
    .local v0, "preX":F
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v2

    iget-object v2, v2, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    sub-int/2addr v3, v1

    aget-object v1, v2, v3

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 249
    .local v1, "preY":F
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v2

    iget-object v2, v2, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v2, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 250
    .local v2, "endX":F
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v4

    iget-object v4, v4, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    iget v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v4, v1

    div-float/2addr v4, v3

    .line 251
    .local v4, "endY":F
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 252
    return-void
.end method

.method private tryAddToWindow()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/knock/view/KnockGesturePathView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 150
    return-void
.end method

.method private tryRemoveWindow()V
    .locals 4

    .line 153
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->hideView()V

    .line 154
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mRemoveWindowRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    return-void
.end method


# virtual methods
.method public getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 230
    const/4 v0, 0x0

    return-object v0

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    return-object v0
.end method

.method public hideView()V
    .locals 1

    .line 237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z

    .line 238
    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->flush()V

    .line 239
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 104
    const-string v0, "KnockGesturePathView -> onAttachedToWindow"

    const-string v1, "KnockGesturePathView"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mParent:Landroid/view/ViewParent;

    if-nez v0, :cond_0

    .line 107
    const-string v0, "onAttachedToWindow mParent null"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    :cond_0
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 111
    :goto_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 115
    const-string v0, "KnockGesturePathView"

    const-string v1, "KnockGesturePathView -> onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 117
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 121
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 123
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 124
    iget-boolean v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    .line 125
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPloyLine:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 126
    const/16 v0, 0xf

    .line 127
    .local v0, "mLastLightCount":I
    iget v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    const/16 v2, 0xf

    if-ge v1, v2, :cond_0

    .line 128
    iget v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    .line 130
    :cond_0
    const/high16 v2, 0x40900000    # 4.5f

    .line 131
    .local v2, "m":F
    sub-int/2addr v1, v0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_1

    .line 132
    iget-object v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mMaskLine:Landroid/graphics/Paint;

    const v4, 0x3f4ccccd    # 0.8f

    add-float/2addr v4, v2

    move v2, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 133
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v3

    iget-object v3, v3, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v3

    iget-object v3, v3, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v3

    iget-object v3, v3, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceX:[Ljava/lang/Float;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v3

    iget-object v3, v3, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->mTraceY:[Ljava/lang/Float;

    add-int/lit8 v4, v1, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget-object v9, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mMaskLine:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "mLastLightCount":I
    .end local v1    # "i":I
    .end local v2    # "m":F
    :cond_1
    return-void
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 164
    .local v0, "action":I
    const/4 v1, 0x1

    const/4 v2, 0x0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_2

    .line 220
    :pswitch_1
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->hideView()V

    .line 221
    goto/16 :goto_2

    .line 187
    :pswitch_2
    iget-object v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 188
    return-void

    .line 190
    :cond_0
    iget-object v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    .line 191
    .local v3, "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v4

    if-lez v4, :cond_1

    .line 192
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 193
    invoke-virtual {p1, v2, v4}, Landroid/view/MotionEvent;->getHistoricalAxisValue(II)F

    move-result v5

    invoke-virtual {p1, v1, v4}, Landroid/view/MotionEvent;->getHistoricalAxisValue(II)F

    move-result v6

    invoke-virtual {v3, v5, v6, v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->addTrace(FFZ)V

    .line 194
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I

    move-result v5

    iput v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    .line 195
    iget-object v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    invoke-direct {p0, v5}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->pathLink(Landroid/graphics/Path;)V

    .line 192
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 198
    .end local v4    # "i":I
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 199
    .local v2, "nowX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 200
    .local v4, "nowY":F
    iget-boolean v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z

    if-nez v5, :cond_2

    iget v5, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownX:F

    sub-float v5, v2, v5

    float-to-double v5, v5

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    iget v9, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownY:F

    sub-float v9, v4, v9

    float-to-double v9, v9

    invoke-static {v9, v10, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    add-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    cmpl-double v5, v5, v7

    if-lez v5, :cond_2

    .line 201
    iput-boolean v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z

    .line 202
    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->tryAddToWindow()V

    .line 204
    :cond_2
    invoke-virtual {v3, v2, v4, v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->addTrace(FFZ)V

    .line 205
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I

    move-result v1

    iput v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    .line 206
    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    invoke-direct {p0, v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->pathLink(Landroid/graphics/Path;)V

    .line 207
    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->flush()V

    .line 208
    goto/16 :goto_2

    .line 211
    .end local v2    # "nowX":F
    .end local v3    # "pointerState":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    .end local v4    # "nowY":F
    :pswitch_3
    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 212
    return-void

    .line 214
    :cond_3
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I

    move-result v1

    iput v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    .line 215
    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    invoke-direct {p0, v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->pathLink(Landroid/graphics/Path;)V

    .line 216
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->hideView()V

    .line 217
    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->tryRemoveWindow()V

    .line 218
    goto :goto_2

    .line 166
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownX:F

    .line 167
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownY:F

    .line 168
    iget-object v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mRemoveWindowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 169
    iput-boolean v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDistanceShowView:Z

    .line 170
    iput-boolean v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mVisibility:Z

    .line 171
    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 172
    new-instance v1, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    invoke-direct {v1}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;-><init>()V

    .line 173
    .local v1, "ps":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v1, v3, v4, v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->addTrace(FFZ)V

    .line 174
    iget-object v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPointers:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    if-nez v2, :cond_4

    .line 176
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    goto :goto_1

    .line 178
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 180
    :goto_1
    iget-object v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mPath:Landroid/graphics/Path;

    iget v3, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownX:F

    iget v4, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDownY:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 181
    invoke-virtual {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->getPointerPathData()Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;->getTraceCount()I

    move-result v2

    iput v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mDataPointCount:I

    .line 182
    iget-boolean v2, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mIsAdded:Z

    if-eqz v2, :cond_5

    .line 183
    invoke-direct {p0}, Lcom/miui/server/input/knock/view/KnockGesturePathView;->flush()V

    .line 225
    .end local v1    # "ps":Lcom/miui/server/input/knock/view/KnockGesturePathView$KnockPointerState;
    :cond_5
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public removeViewImmediate()V
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mRemoveWindowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 159
    iget-object v0, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/knock/view/KnockGesturePathView;->mRemoveWindowRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 160
    return-void
.end method
