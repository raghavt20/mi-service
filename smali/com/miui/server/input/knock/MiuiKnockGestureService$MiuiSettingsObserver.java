class com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiKnockGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/knock/MiuiKnockGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.knock.MiuiKnockGestureService this$0; //synthetic
/* # direct methods */
 com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/knock/MiuiKnockGestureService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 486 */
this.this$0 = p1;
/* .line 487 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 488 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 491 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 492 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "double_knock"; // const-string v1, "double_knock"
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 494 */
final String v1 = "knock_gesture_v"; // const-string v1, "knock_gesture_v"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 496 */
final String v1 = "knock_slide_shape"; // const-string v1, "knock_slide_shape"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 498 */
final String v1 = "knock_long_press_horizontal_slid"; // const-string v1, "knock_long_press_horizontal_slid"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 500 */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 502 */
final String v1 = "pref_open_game_booster"; // const-string v1, "pref_open_game_booster"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 504 */
final String v1 = "close_knock_all_feature"; // const-string v1, "close_knock_all_feature"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 506 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 6 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 510 */
final String v0 = "double_knock"; // const-string v0, "double_knock"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
final String v2 = "MiuiKnockGestureService"; // const-string v2, "MiuiKnockGestureService"
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 511 */
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v3,v0 );
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmFunctionDoubleKnock ( v1,v0 );
	 /* .line 513 */
	 v0 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmCheckerDoubleKnock ( v0 );
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionDoubleKnock ( v1 );
	 (( com.miui.server.input.knock.checker.KnockGestureDouble ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->setFunction(Ljava/lang/String;)V
	 /* .line 514 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "knock feature change , double_knock_function:"; // const-string v1, "knock feature change , double_knock_function:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionDoubleKnock ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 515 */
} // :cond_0
final String v0 = "knock_gesture_v"; // const-string v0, "knock_gesture_v"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 516 */
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v3,v0 );
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmFunctionGestureV ( v1,v0 );
	 /* .line 518 */
	 v0 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmCheckerKnockGestureV ( v0 );
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionGestureV ( v1 );
	 (( com.miui.server.input.knock.checker.KnockGestureV ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureV;->setFunction(Ljava/lang/String;)V
	 /* .line 519 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "knock feature change , knock_gesture_v:"; // const-string v1, "knock feature change , knock_gesture_v:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionGestureV ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 520 */
} // :cond_1
final String v0 = "knock_slide_shape"; // const-string v0, "knock_slide_shape"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
	 /* .line 521 */
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v3,v0 );
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmFunctionGesturePartial ( v1,v0 );
	 /* .line 523 */
	 v0 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmCheckerGesturePartialShot ( v0 );
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionGesturePartial ( v1 );
	 (( com.miui.server.input.knock.checker.KnockGesturePartialShot ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setFunction(Ljava/lang/String;)V
	 /* .line 524 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "knock feature change , knock_gesture_partial_screenshot:"; // const-string v1, "knock feature change , knock_gesture_partial_screenshot:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionGesturePartial ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 525 */
} // :cond_2
final String v0 = "knock_long_press_horizontal_slid"; // const-string v0, "knock_long_press_horizontal_slid"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 526 */
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v3,v0 );
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmFunctionHorizontalSlid ( v1,v0 );
	 /* .line 528 */
	 v0 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmCheckerHorizontalBrightness ( v0 );
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionHorizontalSlid ( v1 );
	 (( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setFunction(Ljava/lang/String;)V
	 /* .line 529 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "knock feature change , knock_long_press_horizontal_slid:"; // const-string v1, "knock feature change , knock_long_press_horizontal_slid:"
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmFunctionHorizontalSlid ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .i ( v2,v0 );
	 /* goto/16 :goto_0 */
	 /* .line 530 */
} // :cond_3
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v1 != null) { // if-eqz v1, :cond_5
	 /* .line 531 */
	 v1 = this.this$0;
	 com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 v0 = 	 android.provider.Settings$Secure .getInt ( v5,v0,v3 );
	 /* if-ne v0, v4, :cond_4 */
	 /* move v3, v4 */
} // :cond_4
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmIsGameMode ( v1,v3 );
/* .line 532 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "game_booster changed, isGameMode = "; // const-string v1, "game_booster changed, isGameMode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmIsGameMode ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* goto/16 :goto_0 */
/* .line 533 */
} // :cond_5
final String v0 = "pref_open_game_booster"; // const-string v0, "pref_open_game_booster"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 534 */
v1 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$Secure .getInt ( v5,v0,v4 );
/* if-ne v0, v4, :cond_6 */
/* move v3, v4 */
} // :cond_6
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmOpenGameBooster ( v1,v3 );
/* .line 535 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "game_booster_feature changed, mOpenGameBooster = "; // const-string v1, "game_booster_feature changed, mOpenGameBooster = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmOpenGameBooster ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 536 */
} // :cond_7
final String v0 = "close_knock_all_feature"; // const-string v0, "close_knock_all_feature"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_9
/* .line 537 */
v1 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v5 ).getContentResolver ( ); // invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v0 = android.provider.Settings$System .getInt ( v5,v0,v3 );
/* if-ne v0, v4, :cond_8 */
/* move v3, v4 */
} // :cond_8
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fputmCloseKnockAllFeature ( v1,v3 );
/* .line 539 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "close all knock feature changed, mOpenGameBooster = "; // const-string v1, "close all knock feature changed, mOpenGameBooster = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.this$0;
v1 = com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmOpenGameBooster ( v1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v0 );
/* .line 541 */
} // :cond_9
} // :goto_0
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$mupdateKnockFeatureState ( v0 );
/* .line 542 */
return;
} // .end method
