class com.miui.server.input.knock.MiuiKnockGestureService$2 extends android.content.BroadcastReceiver {
	 /* .source "MiuiKnockGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/knock/MiuiKnockGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.knock.MiuiKnockGestureService this$0; //synthetic
/* # direct methods */
 com.miui.server.input.knock.MiuiKnockGestureService$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/knock/MiuiKnockGestureService; */
/* .line 116 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 119 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmWindowManager ( v0 );
v1 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmTempPoint ( v1 );
(( android.view.Display ) v0 ).getRealSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V
/* .line 120 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmSizePoint ( v0 );
/* iget v0, v0, Landroid/graphics/Point;->x:I */
v1 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmTempPoint ( v1 );
/* iget v1, v1, Landroid/graphics/Point;->x:I */
/* if-eq v0, v1, :cond_0 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmSizePoint ( v0 );
/* iget v0, v0, Landroid/graphics/Point;->x:I */
v1 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmTempPoint ( v1 );
/* iget v1, v1, Landroid/graphics/Point;->y:I */
/* if-eq v0, v1, :cond_0 */
/* .line 121 */
final String v0 = "MiuiKnockGestureService"; // const-string v0, "MiuiKnockGestureService"
final String v1 = "Screen resolution conversion so reset Knock Valid Area"; // const-string v1, "Screen resolution conversion so reset Knock Valid Area"
android.util.Slog .i ( v0,v1 );
/* .line 122 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$msetKnockValidRect ( v0 );
/* .line 123 */
v0 = this.this$0;
com.miui.server.input.knock.MiuiKnockGestureService .-$$Nest$fgetmKnockCheckDelegate ( v0 );
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).onScreenSizeChanged ( ); // invoke-virtual {v0}, Lcom/miui/server/input/knock/KnockCheckDelegate;->onScreenSizeChanged()V
/* .line 125 */
} // :cond_0
return;
} // .end method
