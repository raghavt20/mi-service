.class public abstract Lcom/miui/server/input/knock/KnockGestureChecker;
.super Ljava/lang/Object;
.source "KnockGestureChecker.java"


# static fields
.field protected static final STATE_CHECKING_ALL_CHECKER:I = 0x1

.field protected static final STATE_CHECKING_ONLY_CHECKER:I = 0x2

.field protected static final STATE_FAIL:I = 0x4

.field protected static final STATE_SUCCESS:I = 0x3


# instance fields
.field protected mCheckState:I

.field protected mContext:Landroid/content/Context;

.field protected mFunction:Ljava/lang/String;

.field protected mKnockPathListener:Lcom/miui/server/input/knock/view/KnockPathListener;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I

    .line 25
    iput-object p1, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method


# virtual methods
.method protected checkEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "feature"    # Ljava/lang/String;

    .line 69
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 72
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 70
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public checkOnlyOneGesture()Z
    .locals 2

    .line 36
    iget v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public continueCheck()Z
    .locals 2

    .line 31
    iget v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onScreenSizeChanged()V
    .locals 0

    .line 62
    return-void
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)V
.end method

.method public resetState()V
    .locals 1

    .line 53
    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I

    .line 54
    return-void
.end method

.method protected setCheckFail()V
    .locals 1

    .line 44
    const/4 v0, 0x4

    iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I

    .line 45
    return-void
.end method

.method protected setCheckSuccess()V
    .locals 1

    .line 40
    const/4 v0, 0x3

    iput v0, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mCheckState:I

    .line 41
    return-void
.end method

.method public setFunction(Ljava/lang/String;)V
    .locals 0
    .param p1, "function"    # Ljava/lang/String;

    .line 57
    iput-object p1, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mFunction:Ljava/lang/String;

    .line 58
    return-void
.end method

.method setKnockPathListener(Lcom/miui/server/input/knock/view/KnockPathListener;)V
    .locals 0
    .param p1, "knockPathListener"    # Lcom/miui/server/input/knock/view/KnockPathListener;

    .line 65
    iput-object p1, p0, Lcom/miui/server/input/knock/KnockGestureChecker;->mKnockPathListener:Lcom/miui/server/input/knock/view/KnockPathListener;

    .line 66
    return-void
.end method
