public class com.miui.server.input.knock.MiuiKnockGestureService implements android.view.WindowManagerPolicyConstants$PointerEventListener {
	 /* .source "MiuiKnockGestureService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/knock/MiuiKnockGestureService$H;, */
	 /* Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DELAY_CHECK_TIME;
private static final java.lang.String KEY_GAME_BOOSTER;
private static final java.lang.String KEY_OPEN_GAME_BOOSTER;
private static final java.lang.String LOG_NATIVE_FEATURE_ERROR;
private static final Integer MOTION_EVENT_TOOL_TYPE_KNOCK;
private static final Integer PROPERTY_COLLECT_DATA;
private static final java.lang.String PROPERTY_DISPLAY_VERSION;
private static final Integer PROPERTY_OPEN_KNOCK_FEATURE;
private static final java.lang.String TAG;
private static java.lang.ref.WeakReference sMiuiKnockGestureService;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/ref/WeakReference<", */
/* "Lcom/miui/server/input/knock/MiuiKnockGestureService;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # instance fields */
private com.miui.server.input.knock.checker.KnockGestureDouble mCheckerDoubleKnock;
private com.miui.server.input.knock.checker.KnockGesturePartialShot mCheckerGesturePartialShot;
private com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness mCheckerHorizontalBrightness;
private com.miui.server.input.knock.checker.KnockGestureV mCheckerKnockGestureV;
private Boolean mCloseKnockAllFeature;
private android.content.BroadcastReceiver mConfigurationReceiver;
private android.content.Context mContext;
private Integer mCurrentUserId;
private Integer mDelayCheckTimes;
private java.lang.Runnable mDelayRunnable;
private java.lang.String mDisplayVersion;
private java.lang.String mFunctionDoubleKnock;
private java.lang.String mFunctionGesturePartial;
private java.lang.String mFunctionGestureV;
private java.lang.String mFunctionHorizontalSlid;
private android.os.Handler mHandler;
private Boolean mHasRegisterListener;
private com.android.server.input.InputManagerService mInputManagerService;
private Boolean mIsGameMode;
private Boolean mIsScreenOn;
private Boolean mIsSetupComplete;
private Boolean mIsStatusBarVisible;
private com.miui.server.input.knock.KnockCheckDelegate mKnockCheckDelegate;
private com.miui.server.input.knock.config.KnockConfig mKnockConfig;
private com.miui.server.input.knock.config.KnockConfigHelper mKnockConfigHelper;
private com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver mMiuiSettingsObserver;
private Integer mNativeFeatureState;
private Boolean mOpenGameBooster;
private android.graphics.Point mSizePoint;
private android.graphics.Point mTempPoint;
private android.view.WindowManager mWindowManager;
private com.android.server.wm.WindowManagerService mWindowManagerService;
/* # direct methods */
static com.miui.server.input.knock.checker.KnockGestureDouble -$$Nest$fgetmCheckerDoubleKnock ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCheckerDoubleKnock;
} // .end method
static com.miui.server.input.knock.checker.KnockGesturePartialShot -$$Nest$fgetmCheckerGesturePartialShot ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCheckerGesturePartialShot;
} // .end method
static com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness -$$Nest$fgetmCheckerHorizontalBrightness ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCheckerHorizontalBrightness;
} // .end method
static com.miui.server.input.knock.checker.KnockGestureV -$$Nest$fgetmCheckerKnockGestureV ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCheckerKnockGestureV;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmDelayCheckTimes ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDelayCheckTimes:I */
} // .end method
static java.lang.String -$$Nest$fgetmDisplayVersion ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mDisplayVersion;
} // .end method
static java.lang.String -$$Nest$fgetmFunctionDoubleKnock ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFunctionDoubleKnock;
} // .end method
static java.lang.String -$$Nest$fgetmFunctionGesturePartial ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFunctionGesturePartial;
} // .end method
static java.lang.String -$$Nest$fgetmFunctionGestureV ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFunctionGestureV;
} // .end method
static java.lang.String -$$Nest$fgetmFunctionHorizontalSlid ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mFunctionHorizontalSlid;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsGameMode ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z */
} // .end method
static Boolean -$$Nest$fgetmIsScreenOn ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsScreenOn:Z */
} // .end method
static com.miui.server.input.knock.KnockCheckDelegate -$$Nest$fgetmKnockCheckDelegate ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mKnockCheckDelegate;
} // .end method
static Boolean -$$Nest$fgetmOpenGameBooster ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z */
} // .end method
static android.graphics.Point -$$Nest$fgetmSizePoint ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mSizePoint;
} // .end method
static android.graphics.Point -$$Nest$fgetmTempPoint ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mTempPoint;
} // .end method
static android.view.WindowManager -$$Nest$fgetmWindowManager ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mWindowManager;
} // .end method
static void -$$Nest$fputmCloseKnockAllFeature ( com.miui.server.input.knock.MiuiKnockGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z */
return;
} // .end method
static void -$$Nest$fputmDelayCheckTimes ( com.miui.server.input.knock.MiuiKnockGestureService p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mDelayCheckTimes:I */
return;
} // .end method
static void -$$Nest$fputmDisplayVersion ( com.miui.server.input.knock.MiuiKnockGestureService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mDisplayVersion = p1;
return;
} // .end method
static void -$$Nest$fputmFunctionDoubleKnock ( com.miui.server.input.knock.MiuiKnockGestureService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFunctionDoubleKnock = p1;
return;
} // .end method
static void -$$Nest$fputmFunctionGesturePartial ( com.miui.server.input.knock.MiuiKnockGestureService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFunctionGesturePartial = p1;
return;
} // .end method
static void -$$Nest$fputmFunctionGestureV ( com.miui.server.input.knock.MiuiKnockGestureService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFunctionGestureV = p1;
return;
} // .end method
static void -$$Nest$fputmFunctionHorizontalSlid ( com.miui.server.input.knock.MiuiKnockGestureService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mFunctionHorizontalSlid = p1;
return;
} // .end method
static void -$$Nest$fputmIsGameMode ( com.miui.server.input.knock.MiuiKnockGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z */
return;
} // .end method
static void -$$Nest$fputmIsScreenOn ( com.miui.server.input.knock.MiuiKnockGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsScreenOn:Z */
return;
} // .end method
static void -$$Nest$fputmIsStatusBarVisible ( com.miui.server.input.knock.MiuiKnockGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsStatusBarVisible:Z */
return;
} // .end method
static void -$$Nest$fputmOpenGameBooster ( com.miui.server.input.knock.MiuiKnockGestureService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z */
return;
} // .end method
static Boolean -$$Nest$mdisplayVersionSupportCheck ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->displayVersionSupportCheck()Z */
} // .end method
static void -$$Nest$mfeatureSupportInit ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->featureSupportInit()V */
return;
} // .end method
static void -$$Nest$msetKnockValidRect ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockValidRect()V */
return;
} // .end method
static void -$$Nest$mupdateKnockFeatureState ( com.miui.server.input.knock.MiuiKnockGestureService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateKnockFeatureState()V */
return;
} // .end method
public com.miui.server.input.knock.MiuiKnockGestureService ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 161 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 71 */
int v0 = -2; // const/4 v0, -0x2
/* iput v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCurrentUserId:I */
/* .line 74 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z */
/* .line 94 */
/* new-instance v1, Landroid/graphics/Point; */
/* invoke-direct {v1, v0, v0}, Landroid/graphics/Point;-><init>(II)V */
this.mSizePoint = v1;
/* .line 95 */
/* new-instance v1, Landroid/graphics/Point; */
/* invoke-direct {v1, v0, v0}, Landroid/graphics/Point;-><init>(II)V */
this.mTempPoint = v1;
/* .line 97 */
/* new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$1;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V */
this.mDelayRunnable = v0;
/* .line 116 */
/* new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$2; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$2;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;)V */
this.mConfigurationReceiver = v0;
/* .line 162 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->initialize(Landroid/content/Context;)V */
/* .line 164 */
return;
} // .end method
private Boolean checkEmpty ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "feature" # Ljava/lang/String; */
/* .line 430 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_1 */
final String v0 = "none"; // const-string v0, "none"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 433 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 431 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean displayVersionSupportCheck ( ) {
/* .locals 6 */
/* .line 200 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 201 */
/* .local v0, "knockConfigFilterList":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/input/knock/config/filter/KnockConfigFilter;>;" */
/* new-instance v1, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName; */
v2 = miui.os.Build.DEVICE;
/* invoke-direct {v1, v2}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName;-><init>(Ljava/lang/String;)V */
/* .line 203 */
/* .local v1, "filterDeviceName":Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDeviceName; */
/* new-instance v2, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion; */
v3 = this.mDisplayVersion;
/* invoke-direct {v2, v3}, Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion;-><init>(Ljava/lang/String;)V */
/* .line 205 */
/* .local v2, "filterDisplayVersion":Lcom/miui/server/input/knock/config/filter/KnockConfigFilterDisplayVersion; */
/* .line 206 */
/* .line 207 */
v3 = this.mKnockConfigHelper;
(( com.miui.server.input.knock.config.KnockConfigHelper ) v3 ).getKnockConfig ( v0 ); // invoke-virtual {v3, v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->getKnockConfig(Ljava/util/List;)Lcom/miui/server/input/knock/config/KnockConfig;
this.mKnockConfig = v3;
/* .line 208 */
int v4 = 0; // const/4 v4, 0x0
/* if-nez v3, :cond_0 */
/* .line 209 */
/* .line 212 */
} // :cond_0
final String v5 = "MiuiKnockGestureService"; // const-string v5, "MiuiKnockGestureService"
(( com.miui.server.input.knock.config.KnockConfig ) v3 ).toString ( ); // invoke-virtual {v3}, Lcom/miui/server/input/knock/config/KnockConfig;->toString()Ljava/lang/String;
android.util.Slog .w ( v5,v3 );
/* .line 214 */
v3 = this.mKnockConfig;
/* iget v3, v3, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I */
int v5 = 1; // const/4 v5, 0x1
/* and-int/2addr v3, v5 */
/* if-nez v3, :cond_2 */
v3 = this.mKnockConfig;
/* iget v3, v3, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I */
/* and-int/lit8 v3, v3, 0x2 */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 218 */
} // :cond_1
/* .line 216 */
} // :cond_2
} // :goto_0
} // .end method
private void featureSupportInit ( ) {
/* .locals 4 */
/* .line 227 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockValidRect()V */
/* .line 228 */
/* const-string/jumbo v0, "screen_resolution_supported" */
miui.util.FeatureParser .getIntArray ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 229 */
v0 = this.mContext;
v1 = this.mConfigurationReceiver;
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.CONFIGURATION_CHANGED"; // const-string v3, "android.intent.action.CONFIGURATION_CHANGED"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
(( android.content.Context ) v0 ).registerReceiver ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 231 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockDeviceProperty()V */
/* .line 232 */
v0 = this.mKnockConfig;
v0 = this.localAlgorithmPath;
/* invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockDlcPath(Ljava/lang/String;)V */
/* .line 233 */
v0 = this.mKnockConfig;
/* iget v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->knockScoreThreshold:F */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockScoreThreshold(F)V */
/* .line 234 */
v0 = this.mKnockConfig;
/* iget v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->useFrame:I */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockUseFrame(I)V */
/* .line 235 */
v0 = this.mKnockConfig;
/* iget v0, v0, Lcom/miui/server/input/knock/config/KnockConfig;->quickMoveSpeed:F */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockQuickMoveSpeed(F)V */
/* .line 236 */
v0 = this.mKnockConfig;
v0 = this.sensorThreshold;
/* invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockSensorThreshold([I)V */
/* .line 238 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "support_knock" */
int v2 = 1; // const/4 v2, 0x1
android.provider.MiuiSettings$Global .putBoolean ( v0,v1,v2 );
/* .line 240 */
v0 = this.mContext;
final String v1 = "double_knock"; // const-string v1, "double_knock"
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
this.mFunctionDoubleKnock = v0;
/* .line 242 */
v0 = this.mContext;
final String v1 = "knock_gesture_v"; // const-string v1, "knock_gesture_v"
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
this.mFunctionGestureV = v0;
/* .line 244 */
v0 = this.mContext;
final String v1 = "knock_slide_shape"; // const-string v1, "knock_slide_shape"
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
this.mFunctionGesturePartial = v0;
/* .line 246 */
v0 = this.mContext;
final String v1 = "knock_long_press_horizontal_slid"; // const-string v1, "knock_long_press_horizontal_slid"
android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
this.mFunctionHorizontalSlid = v0;
/* .line 248 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "pref_open_game_booster"; // const-string v1, "pref_open_game_booster"
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
int v1 = 0; // const/4 v1, 0x0
/* if-ne v0, v2, :cond_1 */
/* move v0, v2 */
} // :cond_1
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z */
/* .line 250 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "gb_boosting"; // const-string v3, "gb_boosting"
v0 = android.provider.Settings$Secure .getInt ( v0,v3,v1 );
/* if-ne v0, v2, :cond_2 */
/* move v0, v2 */
} // :cond_2
/* move v0, v1 */
} // :goto_1
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z */
/* .line 253 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v3 = "close_knock_all_feature"; // const-string v3, "close_knock_all_feature"
v0 = android.provider.Settings$System .getInt ( v0,v3,v1 );
/* if-ne v0, v2, :cond_3 */
} // :cond_3
/* move v2, v1 */
} // :goto_2
/* iput-boolean v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z */
/* .line 256 */
/* new-instance v0, Lcom/miui/server/input/knock/KnockCheckDelegate; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;-><init>(Landroid/content/Context;)V */
this.mKnockCheckDelegate = v0;
/* .line 257 */
/* new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;-><init>(Landroid/content/Context;)V */
this.mCheckerHorizontalBrightness = v0;
/* .line 258 */
/* new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureV; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureV;-><init>(Landroid/content/Context;)V */
this.mCheckerKnockGestureV = v0;
/* .line 259 */
/* new-instance v0, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;-><init>(Landroid/content/Context;)V */
this.mCheckerGesturePartialShot = v0;
/* .line 260 */
/* new-instance v0, Lcom/miui/server/input/knock/checker/KnockGestureDouble; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;-><init>(Landroid/content/Context;)V */
this.mCheckerDoubleKnock = v0;
/* .line 262 */
v0 = this.mCheckerKnockGestureV;
v1 = this.mFunctionGestureV;
(( com.miui.server.input.knock.checker.KnockGestureV ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureV;->setFunction(Ljava/lang/String;)V
/* .line 263 */
v0 = this.mCheckerDoubleKnock;
v1 = this.mFunctionDoubleKnock;
(( com.miui.server.input.knock.checker.KnockGestureDouble ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureDouble;->setFunction(Ljava/lang/String;)V
/* .line 264 */
v0 = this.mCheckerGesturePartialShot;
v1 = this.mFunctionGesturePartial;
(( com.miui.server.input.knock.checker.KnockGesturePartialShot ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGesturePartialShot;->setFunction(Ljava/lang/String;)V
/* .line 265 */
v0 = this.mCheckerHorizontalBrightness;
v1 = this.mFunctionHorizontalSlid;
(( com.miui.server.input.knock.checker.KnockGestureHorizontalBrightness ) v0 ).setFunction ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/checker/KnockGestureHorizontalBrightness;->setFunction(Ljava/lang/String;)V
/* .line 266 */
v0 = this.mKnockCheckDelegate;
v1 = this.mCheckerDoubleKnock;
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).registerKnockChecker ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V
/* .line 267 */
v0 = this.mKnockCheckDelegate;
v1 = this.mCheckerKnockGestureV;
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).registerKnockChecker ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V
/* .line 268 */
v0 = this.mKnockCheckDelegate;
v1 = this.mCheckerHorizontalBrightness;
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).registerKnockChecker ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V
/* .line 269 */
v0 = this.mKnockCheckDelegate;
v1 = this.mCheckerGesturePartialShot;
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).registerKnockChecker ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->registerKnockChecker(Lcom/miui/server/input/knock/KnockGestureChecker;)V
/* .line 271 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isUserSetUp()Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z */
/* .line 273 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateKnockFeatureState()V */
/* .line 275 */
/* new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;Landroid/os/Handler;)V */
this.mMiuiSettingsObserver = v0;
/* .line 276 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->observe()V
/* .line 277 */
/* new-instance v0, Ljava/lang/ref/WeakReference; */
/* invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V */
/* .line 279 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->removeUnsupportedFunction()V */
/* .line 281 */
return;
} // .end method
public static void finishPostLayoutPolicyLw ( Boolean p0 ) {
/* .locals 1 */
/* .param p0, "statusBarVisible" # Z */
/* .line 153 */
v0 = com.miui.server.input.knock.MiuiKnockGestureService.sMiuiKnockGestureService;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 154 */
(( java.lang.ref.WeakReference ) v0 ).get ( ); // invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/input/knock/MiuiKnockGestureService; */
/* .line 155 */
/* .local v0, "miuiKnockGestureService":Lcom/miui/server/input/knock/MiuiKnockGestureService; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 156 */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->onStatusBarVisibilityChangeStatic(Z)V */
/* .line 159 */
} // .end local v0 # "miuiKnockGestureService":Lcom/miui/server/input/knock/MiuiKnockGestureService;
} // :cond_0
return;
} // .end method
private void initDeviceKnockProperty ( ) {
/* .locals 5 */
/* .line 178 */
v0 = this.mKnockConfigHelper;
v0 = (( com.miui.server.input.knock.config.KnockConfigHelper ) v0 ).isDeviceSupport ( ); // invoke-virtual {v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->isDeviceSupport()Z
/* .line 179 */
/* .local v0, "deviceSupport":Z */
/* if-nez v0, :cond_0 */
/* .line 180 */
return;
/* .line 184 */
} // :cond_0
/* const-string/jumbo v1, "vendor.panel.vendor" */
android.os.SystemProperties .get ( v1 );
this.mDisplayVersion = v1;
/* .line 185 */
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_3 */
v1 = this.mDisplayVersion;
v1 = com.miui.server.input.knock.MiuiKnockGestureService .isInteger ( v1 );
/* if-nez v1, :cond_1 */
/* .line 188 */
} // :cond_1
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->displayVersionSupportCheck()Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 189 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->featureSupportInit()V */
/* .line 192 */
} // :cond_2
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v2, "support_knock" */
int v3 = 0; // const/4 v3, 0x0
android.provider.MiuiSettings$Global .putBoolean ( v1,v2,v3 );
/* .line 186 */
} // :cond_3
} // :goto_0
v1 = this.mHandler;
v2 = this.mDelayRunnable;
/* const-wide/16 v3, 0x3e8 */
(( android.os.Handler ) v1 ).postDelayed ( v2, v3, v4 ); // invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 197 */
} // :goto_1
return;
} // .end method
private void initialize ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 167 */
this.mContext = p1;
/* .line 168 */
/* const-string/jumbo v0, "window" */
android.os.ServiceManager .getService ( v0 );
/* check-cast v1, Lcom/android/server/wm/WindowManagerService; */
this.mWindowManagerService = v1;
/* .line 169 */
final String v1 = "input"; // const-string v1, "input"
android.os.ServiceManager .getService ( v1 );
/* check-cast v1, Lcom/android/server/input/InputManagerService; */
this.mInputManagerService = v1;
/* .line 170 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getSystemService ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/view/WindowManager; */
this.mWindowManager = v0;
/* .line 171 */
/* new-instance v0, Lcom/miui/server/input/knock/MiuiKnockGestureService$H; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$H;-><init>(Lcom/miui/server/input/knock/MiuiKnockGestureService;Lcom/miui/server/input/knock/MiuiKnockGestureService$H-IA;)V */
this.mHandler = v0;
/* .line 172 */
com.miui.server.input.knock.config.KnockConfigHelper .getInstance ( );
this.mKnockConfigHelper = v0;
/* .line 173 */
(( com.miui.server.input.knock.config.KnockConfigHelper ) v0 ).initConfig ( ); // invoke-virtual {v0}, Lcom/miui/server/input/knock/config/KnockConfigHelper;->initConfig()V
/* .line 174 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->initDeviceKnockProperty()V */
/* .line 175 */
return;
} // .end method
private Boolean isFeatureInit ( ) {
/* .locals 1 */
/* .line 149 */
v0 = com.miui.server.input.knock.MiuiKnockGestureService.sMiuiKnockGestureService;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static Boolean isInteger ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p0, "displayVersion" # Ljava/lang/String; */
/* .line 222 */
final String v0 = "^[-\\+]?[\\d]*$"; // const-string v0, "^[-\\+]?[\\d]*$"
java.util.regex.Pattern .compile ( v0 );
/* .line 223 */
/* .local v0, "pattern":Ljava/util/regex/Pattern; */
(( java.util.regex.Pattern ) v0 ).matcher ( p0 ); // invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
v1 = (( java.util.regex.Matcher ) v1 ).matches ( ); // invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z
} // .end method
private Boolean isMotionUserSetUp ( android.view.MotionEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 548 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z */
/* if-nez v0, :cond_0 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* if-nez v0, :cond_0 */
/* .line 549 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isUserSetUp()Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z */
/* .line 551 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z */
} // .end method
private Boolean isUserSetUp ( ) {
/* .locals 4 */
/* .line 555 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "user_setup_complete" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
} // .end method
private void onStatusBarVisibilityChangeStatic ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "statusBarVisible" # Z */
/* .line 311 */
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 312 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 313 */
return;
} // .end method
private void removeUnsupportedFunction ( ) {
/* .locals 4 */
/* .line 477 */
final String v0 = "launch_ai_shortcut"; // const-string v0, "launch_ai_shortcut"
v1 = this.mFunctionGestureV;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 478 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "none"; // const-string v1, "none"
int v2 = -2; // const/4 v2, -0x2
final String v3 = "knock_gesture_v"; // const-string v3, "knock_gesture_v"
android.provider.MiuiSettings$System .putStringForUser ( v0,v3,v1,v2 );
/* .line 482 */
} // :cond_0
return;
} // .end method
private void setKnockDeviceProperty ( ) {
/* .locals 2 */
/* .line 364 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 365 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
v1 = this.mKnockConfig;
/* iget v1, v1, Lcom/miui/server/input/knock/config/KnockConfig;->deviceProperty:I */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockDeviceProperty ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockDeviceProperty(I)V
/* .line 366 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 367 */
return;
} // .end method
private void setKnockDlcPath ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "dlcPath" # Ljava/lang/String; */
/* .line 400 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 401 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).updateAlgorithmPath ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->updateAlgorithmPath(Ljava/lang/String;)V
/* .line 402 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 403 */
return;
} // .end method
private void setKnockInvalidRect ( Integer p0, Integer p1, Integer p2, Integer p3 ) {
/* .locals 1 */
/* .param p1, "left" # I */
/* .param p2, "top" # I */
/* .param p3, "right" # I */
/* .param p4, "bottom" # I */
/* .line 394 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 395 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockInValidRect ( p1, p2, p3, p4 ); // invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/server/input/config/InputKnockConfig;->setKnockInValidRect(IIII)V
/* .line 396 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 397 */
return;
} // .end method
private void setKnockQuickMoveSpeed ( Float p0 ) {
/* .locals 1 */
/* .param p1, "quickMoveSpeed" # F */
/* .line 418 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 419 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockQuickMoveSpeed ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockQuickMoveSpeed(F)V
/* .line 420 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 421 */
return;
} // .end method
private void setKnockScoreThreshold ( Float p0 ) {
/* .locals 1 */
/* .param p1, "score" # F */
/* .line 406 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 407 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockScoreThreshold ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockScoreThreshold(F)V
/* .line 408 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 409 */
return;
} // .end method
private void setKnockSensorThreshold ( Integer[] p0 ) {
/* .locals 1 */
/* .param p1, "sensorThreshold" # [I */
/* .line 424 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 425 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockSensorThreshold ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockSensorThreshold([I)V
/* .line 426 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 427 */
return;
} // .end method
private void setKnockUseFrame ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "useFrame" # I */
/* .line 412 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 413 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockUseFrame ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockUseFrame(I)V
/* .line 414 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 415 */
return;
} // .end method
private void setKnockValidRect ( ) {
/* .locals 8 */
/* .line 375 */
v0 = this.mWindowManager;
v1 = this.mSizePoint;
(( android.view.Display ) v0 ).getRealSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V
/* .line 376 */
v0 = this.mSizePoint;
/* iget v0, v0, Landroid/graphics/Point;->x:I */
v1 = this.mSizePoint;
/* iget v1, v1, Landroid/graphics/Point;->y:I */
v0 = java.lang.Math .min ( v0,v1 );
/* .line 377 */
/* .local v0, "width":I */
v1 = this.mSizePoint;
/* iget v1, v1, Landroid/graphics/Point;->x:I */
v2 = this.mSizePoint;
/* iget v2, v2, Landroid/graphics/Point;->y:I */
v1 = java.lang.Math .max ( v1,v2 );
/* .line 378 */
/* .local v1, "height":I */
v2 = this.mKnockConfig;
v2 = this.knockRegion;
/* iget v2, v2, Landroid/graphics/Rect;->left:I */
/* mul-int/2addr v2, v0 */
v3 = this.mKnockConfig;
v3 = this.deviceX;
int v4 = 0; // const/4 v4, 0x0
/* aget v3, v3, v4 */
/* div-int/2addr v2, v3 */
/* .line 379 */
/* .local v2, "left":I */
v3 = this.mKnockConfig;
v3 = this.knockRegion;
/* iget v3, v3, Landroid/graphics/Rect;->top:I */
/* mul-int/2addr v3, v1 */
v5 = this.mKnockConfig;
v5 = this.deviceX;
int v6 = 1; // const/4 v6, 0x1
/* aget v5, v5, v6 */
/* div-int/2addr v3, v5 */
/* .line 380 */
/* .local v3, "top":I */
v5 = this.mKnockConfig;
v5 = this.deviceX;
/* aget v5, v5, v4 */
v7 = this.mKnockConfig;
v7 = this.knockRegion;
/* iget v7, v7, Landroid/graphics/Rect;->right:I */
/* sub-int/2addr v5, v7 */
/* mul-int/2addr v5, v0 */
v7 = this.mKnockConfig;
v7 = this.deviceX;
/* aget v4, v7, v4 */
/* div-int/2addr v5, v4 */
/* .line 382 */
/* .local v5, "right":I */
v4 = this.mKnockConfig;
v4 = this.deviceX;
/* aget v4, v4, v6 */
v7 = this.mKnockConfig;
v7 = this.knockRegion;
/* iget v7, v7, Landroid/graphics/Rect;->bottom:I */
/* sub-int/2addr v4, v7 */
/* mul-int/2addr v4, v1 */
v7 = this.mKnockConfig;
v7 = this.deviceX;
/* aget v6, v7, v6 */
/* div-int/2addr v4, v6 */
/* .line 384 */
/* .local v4, "bottom":I */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 385 */
/* .local v6, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v6 ).setKnockValidRect ( v2, v3, v5, v4 ); // invoke-virtual {v6, v2, v3, v5, v4}, Lcom/android/server/input/config/InputKnockConfig;->setKnockValidRect(IIII)V
/* .line 386 */
(( com.android.server.input.config.InputKnockConfig ) v6 ).flushToNative ( ); // invoke-virtual {v6}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 388 */
return;
} // .end method
private void setNativeKnockFeatureState ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "state" # I */
/* .line 355 */
/* iget v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mNativeFeatureState:I */
/* if-eq p1, v0, :cond_0 */
/* .line 356 */
/* iput p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mNativeFeatureState:I */
/* .line 357 */
com.android.server.input.config.InputKnockConfig .getInstance ( );
/* .line 358 */
/* .local v0, "config":Lcom/android/server/input/config/InputKnockConfig; */
(( com.android.server.input.config.InputKnockConfig ) v0 ).setKnockFeatureState ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputKnockConfig;->setKnockFeatureState(I)V
/* .line 359 */
(( com.android.server.input.config.InputKnockConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputKnockConfig;->flushToNative()V
/* .line 361 */
} // .end local v0 # "config":Lcom/android/server/input/config/InputKnockConfig;
} // :cond_0
return;
} // .end method
private void updateKnockFeatureState ( ) {
/* .locals 3 */
/* .line 317 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z */
final String v1 = "MiuiKnockGestureService"; // const-string v1, "MiuiKnockGestureService"
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 318 */
final String v0 = "mCloseKnockAllFeature true, close knock"; // const-string v0, "mCloseKnockAllFeature true, close knock"
android.util.Slog .i ( v1,v0 );
/* .line 319 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V */
/* .line 320 */
return;
/* .line 322 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsScreenOn:Z */
/* if-nez v0, :cond_1 */
/* .line 323 */
final String v0 = "mIsScreenOn false, close knock"; // const-string v0, "mIsScreenOn false, close knock"
android.util.Slog .i ( v1,v0 );
/* .line 324 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V */
/* .line 325 */
return;
/* .line 327 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsGameMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 328 */
final String v0 = "mIsGameMode true, close knock"; // const-string v0, "mIsGameMode true, close knock"
android.util.Slog .i ( v1,v0 );
/* .line 329 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V */
/* .line 330 */
return;
/* .line 332 */
} // :cond_2
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mOpenGameBooster:Z */
/* if-nez v0, :cond_3 */
/* .line 333 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsStatusBarVisible:Z */
/* if-nez v0, :cond_3 */
/* .line 334 */
final String v0 = "mOpenGameBooster false, mIsStatusBarVisible false, close knock"; // const-string v0, "mOpenGameBooster false, mIsStatusBarVisible false, close knock"
android.util.Slog .i ( v1,v0 );
/* .line 335 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V */
/* .line 336 */
return;
/* .line 339 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
/* .line 340 */
/* .local v0, "currentFeatureState":Z */
v1 = this.mFunctionDoubleKnock;
v1 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mFunctionGestureV;
v1 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mFunctionGesturePartial;
/* .line 341 */
v1 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z */
if ( v1 != null) { // if-eqz v1, :cond_4
v1 = this.mFunctionHorizontalSlid;
v1 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->checkEmpty(Ljava/lang/String;)Z */
/* if-nez v1, :cond_5 */
/* .line 342 */
} // :cond_4
int v0 = 1; // const/4 v0, 0x1
/* .line 344 */
} // :cond_5
if ( v0 != null) { // if-eqz v0, :cond_6
/* iget-boolean v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z */
/* if-nez v1, :cond_6 */
/* .line 345 */
v1 = this.mWindowManagerService;
(( com.android.server.wm.WindowManagerService ) v1 ).registerPointerEventListener ( p0, v2 ); // invoke-virtual {v1, p0, v2}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 346 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z */
/* .line 347 */
} // :cond_6
/* if-nez v0, :cond_7 */
/* iget-boolean v1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z */
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 348 */
v1 = this.mWindowManagerService;
(( com.android.server.wm.WindowManagerService ) v1 ).unregisterPointerEventListener ( p0, v2 ); // invoke-virtual {v1, p0, v2}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V
/* .line 349 */
/* iput-boolean v2, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mHasRegisterListener:Z */
/* .line 351 */
} // :cond_7
} // :goto_0
/* invoke-direct {p0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setNativeKnockFeatureState(I)V */
/* .line 352 */
return;
} // .end method
private void updateSettings ( ) {
/* .locals 3 */
/* .line 457 */
v0 = this.mMiuiSettingsObserver;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 458 */
/* nop */
/* .line 459 */
final String v1 = "double_knock"; // const-string v1, "double_knock"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 458 */
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 460 */
v0 = this.mMiuiSettingsObserver;
/* .line 461 */
final String v1 = "knock_gesture_v"; // const-string v1, "knock_gesture_v"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 460 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 462 */
v0 = this.mMiuiSettingsObserver;
/* .line 463 */
final String v1 = "knock_slide_shape"; // const-string v1, "knock_slide_shape"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 462 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 464 */
v0 = this.mMiuiSettingsObserver;
/* .line 465 */
final String v1 = "knock_long_press_horizontal_slid"; // const-string v1, "knock_long_press_horizontal_slid"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 464 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 466 */
v0 = this.mMiuiSettingsObserver;
/* .line 467 */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 466 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 468 */
v0 = this.mMiuiSettingsObserver;
/* .line 469 */
final String v1 = "pref_open_game_booster"; // const-string v1, "pref_open_game_booster"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 468 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 470 */
v0 = this.mMiuiSettingsObserver;
/* .line 471 */
final String v1 = "close_knock_all_feature"; // const-string v1, "close_knock_all_feature"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 470 */
(( com.miui.server.input.knock.MiuiKnockGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/knock/MiuiKnockGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 473 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->removeUnsupportedFunction()V */
/* .line 474 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 1 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 561 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 562 */
final String v0 = "MiuiKnockGestureService"; // const-string v0, "MiuiKnockGestureService"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 563 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 564 */
final String v0 = "mIsSetupComplete="; // const-string v0, "mIsSetupComplete="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 565 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mIsSetupComplete:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 566 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 567 */
final String v0 = "mCloseKnockAllFeature="; // const-string v0, "mCloseKnockAllFeature="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 568 */
/* iget-boolean v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCloseKnockAllFeature:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 569 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 570 */
final String v0 = "mKnockConfig="; // const-string v0, "mKnockConfig="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 571 */
v0 = this.mKnockConfig;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 572 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 573 */
final String v0 = "mDisplayVersion="; // const-string v0, "mDisplayVersion="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 574 */
v0 = this.mDisplayVersion;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 575 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 576 */
final String v0 = "mFeatureInit="; // const-string v0, "mFeatureInit="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 577 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isFeatureInit()Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 578 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 579 */
final String v0 = "mDoubleKnockFunction="; // const-string v0, "mDoubleKnockFunction="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 580 */
v0 = this.mFunctionDoubleKnock;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 581 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 582 */
final String v0 = "mKnockGestureV="; // const-string v0, "mKnockGestureV="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 583 */
v0 = this.mFunctionGestureV;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 584 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 585 */
final String v0 = "mGesturePartialFunction="; // const-string v0, "mGesturePartialFunction="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 586 */
v0 = this.mFunctionGesturePartial;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 587 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 588 */
final String v0 = "mHorizontalSlidFunction="; // const-string v0, "mHorizontalSlidFunction="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 589 */
v0 = this.mFunctionHorizontalSlid;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 591 */
return;
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 438 */
int v0 = 0; // const/4 v0, 0x0
v0 = (( android.view.MotionEvent ) p1 ).getToolType ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I
/* const/16 v1, 0x20 */
/* if-ne v0, v1, :cond_1 */
/* .line 439 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isMotionUserSetUp(Landroid/view/MotionEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 440 */
v0 = this.mKnockCheckDelegate;
(( com.miui.server.input.knock.KnockCheckDelegate ) v0 ).onTouchEvent ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/knock/KnockCheckDelegate;->onTouchEvent(Landroid/view/MotionEvent;)V
/* .line 442 */
} // :cond_0
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* if-nez v0, :cond_1 */
/* .line 443 */
final String v0 = "MiuiKnockGestureService"; // const-string v0, "MiuiKnockGestureService"
/* const-string/jumbo v1, "user not setup complete, action down" */
android.util.Slog .i ( v0,v1 );
/* .line 447 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newUserId" # I */
/* .line 450 */
/* iget v0, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCurrentUserId:I */
/* if-eq v0, p1, :cond_0 */
/* .line 451 */
/* iput p1, p0, Lcom/miui/server/input/knock/MiuiKnockGestureService;->mCurrentUserId:I */
/* .line 452 */
/* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->updateSettings()V */
/* .line 454 */
} // :cond_0
return;
} // .end method
public void setInputMethodRect ( android.graphics.Rect p0 ) {
/* .locals 4 */
/* .param p1, "inputMethodRegion" # Landroid/graphics/Rect; */
/* .line 129 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isFeatureInit()Z */
/* if-nez v0, :cond_0 */
/* .line 130 */
return;
/* .line 132 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 133 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0, v0, v0, v0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockInvalidRect(IIII)V */
/* .line 135 */
} // :cond_1
/* iget v0, p1, Landroid/graphics/Rect;->left:I */
/* iget v1, p1, Landroid/graphics/Rect;->top:I */
/* iget v2, p1, Landroid/graphics/Rect;->right:I */
/* iget v3, p1, Landroid/graphics/Rect;->bottom:I */
/* invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->setKnockInvalidRect(IIII)V */
/* .line 137 */
} // :goto_0
return;
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "screenOn" # Z */
/* .line 140 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/knock/MiuiKnockGestureService;->isFeatureInit()Z */
/* if-nez v0, :cond_0 */
/* .line 141 */
return;
/* .line 143 */
} // :cond_0
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
java.lang.Boolean .valueOf ( p1 );
(( android.os.Handler ) v0 ).obtainMessage ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 144 */
/* .local v0, "message":Landroid/os/Message; */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 145 */
return;
} // .end method
