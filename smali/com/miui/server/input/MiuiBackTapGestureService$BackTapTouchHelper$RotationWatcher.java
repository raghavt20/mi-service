class com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher extends android.view.IRotationWatcher$Stub {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "RotationWatcher" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper this$1; //synthetic
/* # direct methods */
private com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher ( ) {
/* .locals 0 */
/* .line 394 */
this.this$1 = p1;
/* invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V */
return;
} // .end method
 com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)V */
return;
} // .end method
/* # virtual methods */
public void onRotationChanged ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "i" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 398 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "rotation changed = "; // const-string v1, "rotation changed = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "BackTapTouchHelper"; // const-string v1, "BackTapTouchHelper"
android.util.Slog .d ( v1,v0 );
/* .line 399 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetmRotation ( v0 );
/* if-eq v0, p1, :cond_0 */
/* .line 400 */
v0 = this.this$1;
com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fputmRotation ( v0,p1 );
/* .line 401 */
v0 = this.this$1;
v1 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetmRotation ( v0 );
com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$mnotifyRotationChanged ( v0,v1 );
/* .line 403 */
} // :cond_0
return;
} // .end method
