.class public interface abstract Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;
.super Ljava/lang/Object;
.source "MiuiInputSettingsConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiInputSettingsConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnMessageSendFinishCallback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback$MessageSendFinishStatus;
    }
.end annotation


# static fields
.field public static final STATUS_SEND_FAIL_EXCEPTION:I = 0x1

.field public static final STATUS_SEND_FAIL_NOT_FOUND_SERVICE:I = 0x1

.field public static final STATUS_SEND_SUCCESS:I


# virtual methods
.method public abstract onMessageSendFinish(I)V
.end method
