class com.miui.server.input.MiuiInputSettingsConnection$1 extends android.content.BroadcastReceiver {
	 /* .source "MiuiInputSettingsConnection.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiInputSettingsConnection; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiInputSettingsConnection this$0; //synthetic
/* # direct methods */
 com.miui.server.input.MiuiInputSettingsConnection$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiInputSettingsConnection; */
/* .line 89 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 92 */
v0 = this.this$0;
/* monitor-enter v0 */
/* .line 93 */
try { // :try_start_0
	 final String v1 = "android.intent.action.USER_SWITCHED"; // const-string v1, "android.intent.action.USER_SWITCHED"
	 (( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
	 v1 = 	 (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 94 */
		 final String v1 = "MiuiInputSettingsConnection"; // const-string v1, "MiuiInputSettingsConnection"
		 final String v2 = "User switched, request reset connection"; // const-string v2, "User switched, request reset connection"
		 android.util.Slog .w ( v1,v2 );
		 /* .line 95 */
		 v1 = this.this$0;
		 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$mresetConnection ( v1 );
		 /* .line 97 */
	 } // :cond_0
	 /* monitor-exit v0 */
	 /* .line 98 */
	 return;
	 /* .line 97 */
	 /* :catchall_0 */
	 /* move-exception v1 */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* throw v1 */
} // .end method
