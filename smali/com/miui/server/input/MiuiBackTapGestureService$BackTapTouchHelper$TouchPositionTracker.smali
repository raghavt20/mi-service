.class Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;
.super Ljava/lang/Object;
.source "MiuiBackTapGestureService.java"

# interfaces
.implements Landroid/view/WindowManagerPolicyConstants$PointerEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchPositionTracker"
.end annotation


# instance fields
.field private final TOUCH_NEGATIVE:I

.field private final TOUCH_POSITIVE:I

.field private final TOUCH_UNKNOWN:I

.field private mLastObservedTouchTime:J

.field private mPointerIndexTriggerBitMask:S

.field public volatile mTouchBottom:F

.field public volatile mTouchLeft:F

.field public volatile mTouchRight:F

.field mTouchStatus:I

.field public volatile mTouchTop:F

.field final synthetic this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;


# direct methods
.method static bridge synthetic -$$Nest$fgetmLastObservedTouchTime(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J

    return-wide v0
.end method

.method public constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)V
    .locals 3

    .line 418
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 407
    const/4 p1, -0x1

    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->TOUCH_UNKNOWN:I

    .line 408
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->TOUCH_NEGATIVE:I

    .line 409
    const/4 v1, 0x1

    iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->TOUCH_POSITIVE:I

    .line 410
    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    .line 411
    const/4 p1, 0x0

    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F

    .line 412
    const/high16 v1, 0x43fa0000    # 500.0f

    iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F

    .line 413
    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F

    .line 414
    iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F

    .line 415
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J

    .line 416
    iput-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    .line 419
    const-string p1, "BackTapTouchHelper"

    const-string v0, "TouchPositionTracker new obj!"

    invoke-static {p1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    return-void
.end method


# virtual methods
.method getTouchStatus()I
    .locals 2

    .line 474
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "touch status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BackTapTouchHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    return v0
.end method

.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 11
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 482
    invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 483
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 484
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 485
    .local v1, "mPointerCount":I
    new-instance v2, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 487
    .local v2, "mPointerCoords":Landroid/view/MotionEvent$PointerCoords;
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v3

    const-string v4, "BackTapTouchHelper"

    if-eqz v3, :cond_0

    .line 488
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 489
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 490
    .local v5, "mPointerId":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "touch onPointerEvent: index: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", id:  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    .end local v5    # "mPointerId":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 494
    .end local v3    # "i":I
    :cond_0
    and-int/lit16 v3, v0, 0xff

    const/4 v5, 0x0

    const/16 v6, 0x8

    const-string/jumbo v7, "touch onPointerEvent action: "

    const/4 v8, 0x1

    packed-switch v3, :pswitch_data_0

    .line 532
    :pswitch_0
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 533
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 510
    :pswitch_1
    if-eq v0, v8, :cond_1

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    .line 512
    :cond_1
    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 513
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "touch onPointerEvent mTouchStatus = "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v9, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :cond_2
    iget v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    if-ne v3, v8, :cond_3

    .line 517
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v9

    iput-wide v9, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J

    .line 519
    :cond_3
    iput v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    .line 521
    :cond_4
    const v3, 0xff00

    and-int/2addr v3, v0

    shr-int/2addr v3, v6

    .line 523
    .local v3, "index":I
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 524
    .local v5, "pointerId":I
    iget-short v6, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    shl-int v7, v8, v5

    not-int v7, v7

    and-int/2addr v6, v7

    int-to-short v6, v6

    iput-short v6, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    .line 525
    iget-object v6, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v6}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 526
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "touch onPointerEvent ACTION_POINTER_UP: index:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", pointerId:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 497
    .end local v3    # "index":I
    .end local v5    # "pointerId":I
    :pswitch_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_6

    .line 498
    invoke-virtual {p1, v3, v2}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V

    .line 499
    iget-object v7, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v7}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 500
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "touch getPointerCoords: ORIENTATION = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 501
    invoke-virtual {v2, v6}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", x = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 502
    invoke-virtual {v2, v5}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ", y = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 503
    invoke-virtual {v2, v8}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 500
    invoke-static {v4, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_5
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    invoke-virtual {p0, v2, v7}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->verifyMotionEvent(Landroid/view/MotionEvent$PointerCoords;I)V

    .line 497
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 506
    .end local v3    # "i":I
    :cond_6
    nop

    .line 537
    .end local v0    # "action":I
    .end local v1    # "mPointerCount":I
    .end local v2    # "mPointerCoords":Landroid/view/MotionEvent$PointerCoords;
    :cond_7
    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method updateTouchBorder(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "right"    # F
    .param p3, "top"    # F
    .param p4, "bottom"    # F

    .line 437
    iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F

    .line 438
    iput p2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F

    .line 439
    iput p3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F

    .line 440
    iput p4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F

    .line 441
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    const-string v0, "BackTapTouchHelper"

    const-string/jumbo v1, "updateTouchBorder!!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :cond_0
    return-void
.end method

.method updateTouchBorder(I)V
    .locals 5
    .param p1, "rotation"    # I

    .line 423
    packed-switch p1, :pswitch_data_0

    .line 431
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_LEFT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_RIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v1

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_TOP(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v2

    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_BOTTOM(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V

    goto :goto_0

    .line 428
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetSCREEN_HEIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_BOTTOM(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetSCREEN_HEIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_TOP(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_LEFT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v2

    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_RIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V

    .line 429
    goto :goto_0

    .line 425
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_TOP(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_BOTTOM(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v1

    iget-object v2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v2}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetSCREEN_WIDTH(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_RIGHT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetSCREEN_WIDTH(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v4}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetTOUCH_LEFT(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V

    .line 426
    nop

    .line 434
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method verifyMotionEvent(Landroid/view/MotionEvent$PointerCoords;I)V
    .locals 7
    .param p1, "mPointerCoords"    # Landroid/view/MotionEvent$PointerCoords;
    .param p2, "pointerId"    # I

    .line 447
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v0

    const-string/jumbo v1, "}"

    const-string v2, "BackTapTouchHelper"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    .line 448
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "verifyMotionEvent [ID: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", Ori: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 449
    const/16 v5, 0x8

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "],  { "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 450
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 451
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 448
    invoke-static {v2, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v0

    iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    .line 455
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v0

    iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_1

    .line 456
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v0

    iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    .line 457
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F

    move-result v0

    iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F

    cmpg-float v0, v0, v5

    if-gez v0, :cond_1

    .line 458
    iget-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    shl-int v5, v4, p2

    or-int/2addr v0, v5

    int-to-short v0, v0

    iput-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    .line 459
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetDEBUG(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "touch: { time: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 463
    :cond_1
    iget-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    shl-int v1, v4, p2

    not-int v1, v1

    and-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    .line 466
    :cond_2
    :goto_0
    iget-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S

    if-eqz v0, :cond_3

    .line 467
    iput v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    goto :goto_1

    .line 469
    :cond_3
    iput v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I

    .line 471
    :goto_1
    return-void
.end method
