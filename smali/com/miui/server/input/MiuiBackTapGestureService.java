public class com.miui.server.input.MiuiBackTapGestureService {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/MiuiBackTapGestureService$H;, */
	 /* Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;, */
	 /* Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String KEY_GAME_BOOSTER;
private static final java.lang.String TAG;
/* # instance fields */
private java.lang.String mBackDoubleTapFunction;
private final miui.util.BackTapSensorWrapper$BackTapSensorChangeListener mBackTapSensorChangeListener;
private miui.util.BackTapSensorWrapper mBackTapSensorWrapper;
private com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper mBackTapTouchHelper;
private java.lang.String mBackTripleTapFunction;
private android.content.Context mContext;
private Integer mCurrentUserId;
private android.os.Handler mHandler;
private Boolean mIsBackTapSensorListenerRegistered;
private volatile Boolean mIsGameMode;
private Boolean mIsSupportBackTapSensor;
private Boolean mIsUnFolded;
private com.miui.server.input.MiuiBackTapGestureService$MiuiSettingsObserver mMiuiSettingsObserver;
private Boolean mScreenOn;
/* # direct methods */
static java.lang.String -$$Nest$fgetmBackDoubleTapFunction ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBackDoubleTapFunction;
} // .end method
static miui.util.BackTapSensorWrapper$BackTapSensorChangeListener -$$Nest$fgetmBackTapSensorChangeListener ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBackTapSensorChangeListener;
} // .end method
static miui.util.BackTapSensorWrapper -$$Nest$fgetmBackTapSensorWrapper ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBackTapSensorWrapper;
} // .end method
static com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper -$$Nest$fgetmBackTapTouchHelper ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBackTapTouchHelper;
} // .end method
static java.lang.String -$$Nest$fgetmBackTripleTapFunction ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mBackTripleTapFunction;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static Boolean -$$Nest$fgetmIsBackTapSensorListenerRegistered ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z */
} // .end method
static Boolean -$$Nest$fgetmIsGameMode ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z */
} // .end method
static Boolean -$$Nest$fgetmIsUnFolded ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsUnFolded:Z */
} // .end method
static Boolean -$$Nest$fgetmScreenOn ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z */
} // .end method
static void -$$Nest$fputmBackDoubleTapFunction ( com.miui.server.input.MiuiBackTapGestureService p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mBackDoubleTapFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmBackTripleTapFunction ( com.miui.server.input.MiuiBackTapGestureService p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mBackTripleTapFunction = p1;
	 return;
} // .end method
static void -$$Nest$fputmIsBackTapSensorListenerRegistered ( com.miui.server.input.MiuiBackTapGestureService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z */
	 return;
} // .end method
static void -$$Nest$fputmIsGameMode ( com.miui.server.input.MiuiBackTapGestureService p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z */
	 return;
} // .end method
static Boolean -$$Nest$mcheckEmpty ( com.miui.server.input.MiuiBackTapGestureService p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService;->checkEmpty(Ljava/lang/String;)Z */
} // .end method
static Boolean -$$Nest$mpostShortcutFunction ( com.miui.server.input.MiuiBackTapGestureService p0, java.lang.String p1, Integer p2, java.lang.String p3 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = 	 /* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/input/MiuiBackTapGestureService;->postShortcutFunction(Ljava/lang/String;ILjava/lang/String;)Z */
} // .end method
static void -$$Nest$mupdateBackTapFeatureState ( com.miui.server.input.MiuiBackTapGestureService p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V */
	 return;
} // .end method
public com.miui.server.input.MiuiBackTapGestureService ( ) {
	 /* .locals 2 */
	 /* .param p1, "context" # Landroid/content/Context; */
	 /* .line 80 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 52 */
	 int v0 = 0; // const/4 v0, 0x0
	 this.mBackTapSensorWrapper = v0;
	 /* .line 53 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z */
	 /* .line 54 */
	 /* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z */
	 /* .line 57 */
	 /* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z */
	 /* .line 59 */
	 int v1 = -2; // const/4 v1, -0x2
	 /* iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mCurrentUserId:I */
	 /* .line 60 */
	 /* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z */
	 /* .line 61 */
	 /* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsUnFolded:Z */
	 /* .line 63 */
	 /* new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$1; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$1;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V */
	 this.mBackTapSensorChangeListener = v0;
	 /* .line 81 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService;->init(Landroid/content/Context;)V */
	 /* .line 82 */
	 return;
} // .end method
private void backTapSettingsInit ( ) {
	 /* .locals 2 */
	 /* .line 96 */
	 v0 = this.mContext;
	 final String v1 = "back_double_tap"; // const-string v1, "back_double_tap"
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
	 this.mBackDoubleTapFunction = v0;
	 /* .line 98 */
	 v0 = this.mContext;
	 final String v1 = "back_triple_tap"; // const-string v1, "back_triple_tap"
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v0,v1 );
	 this.mBackTripleTapFunction = v0;
	 /* .line 100 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V */
	 /* .line 101 */
	 /* new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver; */
	 v1 = this.mHandler;
	 /* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;Landroid/os/Handler;)V */
	 this.mMiuiSettingsObserver = v0;
	 /* .line 102 */
	 (( com.miui.server.input.MiuiBackTapGestureService$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->observe()V
	 /* .line 103 */
	 /* new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper; */
	 /* invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V */
	 this.mBackTapTouchHelper = v0;
	 /* .line 104 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->removeUnsupportedFunction()V */
	 /* .line 105 */
	 return;
} // .end method
private Boolean checkEmpty ( java.lang.String p0 ) {
	 /* .locals 1 */
	 /* .param p1, "feature" # Ljava/lang/String; */
	 /* .line 186 */
	 v0 = 	 android.text.TextUtils .isEmpty ( p1 );
	 /* if-nez v0, :cond_1 */
	 final String v0 = "none"; // const-string v0, "none"
	 v0 = 	 (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* .line 189 */
	 } // :cond_0
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 187 */
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void init ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 85 */
this.mContext = p1;
/* .line 86 */
/* const-string/jumbo v0, "sensor" */
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/SensorManager; */
/* .line 87 */
/* const v1, 0x1fa2665 */
(( android.hardware.SensorManager ) v0 ).getDefaultSensor ( v1 ); // invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z */
/* .line 88 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 89 */
/* new-instance v0, Lcom/miui/server/input/MiuiBackTapGestureService$H; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$H;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V */
this.mHandler = v0;
/* .line 90 */
/* new-instance v0, Lmiui/util/BackTapSensorWrapper; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lmiui/util/BackTapSensorWrapper;-><init>(Landroid/content/Context;)V */
this.mBackTapSensorWrapper = v0;
/* .line 91 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->backTapSettingsInit()V */
/* .line 93 */
} // :cond_1
return;
} // .end method
private Boolean postShortcutFunction ( java.lang.String p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "delay" # I */
/* .param p3, "shortcut" # Ljava/lang/String; */
/* .line 193 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 194 */
int v0 = 0; // const/4 v0, 0x0
/* .line 196 */
} // :cond_0
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
(( android.os.Handler ) v0 ).obtainMessage ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;
/* .line 197 */
/* .local v0, "message":Landroid/os/Message; */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 198 */
/* .local v2, "bundle":Landroid/os/Bundle; */
/* const-string/jumbo v3, "shortcut" */
(( android.os.Bundle ) v2 ).putString ( v3, p3 ); // invoke-virtual {v2, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 199 */
(( android.os.Message ) v0 ).setData ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 200 */
v3 = this.mHandler;
/* int-to-long v4, p2 */
(( android.os.Handler ) v3 ).sendMessageDelayed ( v0, v4, v5 ); // invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 201 */
} // .end method
private void removeUnsupportedFunction ( ) {
/* .locals 7 */
/* .line 108 */
v0 = this.mBackDoubleTapFunction;
/* const-string/jumbo v1, "turn_on_torch" */
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
final String v2 = "launch_ai_shortcut"; // const-string v2, "launch_ai_shortcut"
final String v3 = "launch_alipay_health_code"; // const-string v3, "launch_alipay_health_code"
int v4 = -2; // const/4 v4, -0x2
final String v5 = "none"; // const-string v5, "none"
/* if-nez v0, :cond_0 */
v0 = this.mBackDoubleTapFunction;
/* .line 109 */
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = this.mBackDoubleTapFunction;
/* .line 110 */
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 111 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v6 = "back_double_tap"; // const-string v6, "back_double_tap"
android.provider.Settings$System .putStringForUser ( v0,v6,v5,v4 );
/* .line 115 */
} // :cond_1
v0 = this.mBackTripleTapFunction;
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
v0 = this.mBackTripleTapFunction;
/* .line 116 */
v0 = (( java.lang.String ) v3 ).equals ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_2 */
v0 = this.mBackTripleTapFunction;
/* .line 117 */
v0 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 118 */
} // :cond_2
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "back_triple_tap"; // const-string v1, "back_triple_tap"
android.provider.Settings$System .putStringForUser ( v0,v1,v5,v4 );
/* .line 122 */
} // :cond_3
return;
} // .end method
private void updateBackTapFeatureState ( ) {
/* .locals 2 */
/* .line 152 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 153 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/MiuiBackTapGestureService$2; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/MiuiBackTapGestureService$2;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 183 */
return;
} // .end method
private void updateSettings ( ) {
/* .locals 3 */
/* .line 143 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z */
/* if-nez v0, :cond_0 */
return;
/* .line 144 */
} // :cond_0
v0 = this.mMiuiSettingsObserver;
/* .line 145 */
final String v1 = "back_double_tap"; // const-string v1, "back_double_tap"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 144 */
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.MiuiBackTapGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 146 */
v0 = this.mMiuiSettingsObserver;
/* .line 147 */
final String v1 = "back_triple_tap"; // const-string v1, "back_triple_tap"
android.provider.Settings$System .getUriFor ( v1 );
/* .line 146 */
(( com.miui.server.input.MiuiBackTapGestureService$MiuiSettingsObserver ) v0 ).onChange ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 148 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->removeUnsupportedFunction()V */
/* .line 149 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 251 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 252 */
final String v0 = "MiuiBackTapGestureService"; // const-string v0, "MiuiBackTapGestureService"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 253 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 254 */
final String v0 = "mIsSupportBackTapSensor="; // const-string v0, "mIsSupportBackTapSensor="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 255 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsSupportBackTapSensor:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 256 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 257 */
final String v0 = "mIsBackTapSensorListenerRegistered="; // const-string v0, "mIsBackTapSensorListenerRegistered="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 258 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsBackTapSensorListenerRegistered:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 259 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 260 */
final String v0 = "mBackDoubleTapFunction="; // const-string v0, "mBackDoubleTapFunction="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 261 */
v0 = this.mBackDoubleTapFunction;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 262 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 263 */
final String v0 = "mBackTripleTapFunction="; // const-string v0, "mBackTripleTapFunction="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 264 */
v0 = this.mBackTripleTapFunction;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 265 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 266 */
final String v0 = "mIsGameMode="; // const-string v0, "mIsGameMode="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 267 */
/* iget-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsGameMode:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 268 */
v0 = this.mBackTapTouchHelper;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 269 */
v1 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$misDebuggable ( v0 );
com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$mdump ( v0,p1,p2,v1 );
/* .line 270 */
} // :cond_0
return;
} // .end method
public void notifyFoldStatus ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "folded" # Z */
/* .line 133 */
/* xor-int/lit8 v0, p1, 0x1 */
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mIsUnFolded:Z */
/* .line 134 */
return;
} // .end method
public void notifyScreenOff ( ) {
/* .locals 1 */
/* .line 129 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z */
/* .line 130 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V */
/* .line 131 */
return;
} // .end method
public void notifyScreenOn ( ) {
/* .locals 1 */
/* .line 125 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mScreenOn:Z */
/* .line 126 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateBackTapFeatureState()V */
/* .line 127 */
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newUserId" # I */
/* .line 136 */
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mCurrentUserId:I */
/* if-eq v0, p1, :cond_0 */
/* .line 137 */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService;->mCurrentUserId:I */
/* .line 138 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiBackTapGestureService;->updateSettings()V */
/* .line 140 */
} // :cond_0
return;
} // .end method
