class com.miui.server.input.MiuiBackTapGestureService$1 implements miui.util.BackTapSensorWrapper$BackTapSensorChangeListener {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiBackTapGestureService this$0; //synthetic
/* # direct methods */
 com.miui.server.input.MiuiBackTapGestureService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiBackTapGestureService; */
/* .line 63 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onBackDoubleTap ( ) {
/* .locals 4 */
/* .line 66 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackDoubleTapFunction ( v0 );
final String v1 = "none"; // const-string v1, "none"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapTouchHelper ( v0 );
/* .line 67 */
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$mcheckTouchStatus ( v0 );
/* if-nez v0, :cond_0 */
v0 = this.this$0;
v0 = com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsGameMode ( v0 );
/* if-nez v0, :cond_0 */
/* .line 68 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackDoubleTapFunction ( v0 );
int v2 = 0; // const/4 v2, 0x0
final String v3 = "back_double_tap"; // const-string v3, "back_double_tap"
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mpostShortcutFunction ( v0,v1,v2,v3 );
/* .line 70 */
} // :cond_0
return;
} // .end method
public void onBackTripleTap ( ) {
/* .locals 4 */
/* .line 73 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTripleTapFunction ( v0 );
final String v1 = "none"; // const-string v1, "none"
v0 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTapTouchHelper ( v0 );
/* .line 74 */
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$mcheckTouchStatus ( v0 );
/* if-nez v0, :cond_0 */
v0 = this.this$0;
v0 = com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmIsGameMode ( v0 );
/* if-nez v0, :cond_0 */
/* .line 75 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmBackTripleTapFunction ( v0 );
int v2 = 0; // const/4 v2, 0x0
final String v3 = "back_triple_tap"; // const-string v3, "back_triple_tap"
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mpostShortcutFunction ( v0,v1,v2,v3 );
/* .line 77 */
} // :cond_0
return;
} // .end method
