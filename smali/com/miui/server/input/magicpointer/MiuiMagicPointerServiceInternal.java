public abstract class com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal {
	 /* .source "MiuiMagicPointerServiceInternal.java" */
	 /* # direct methods */
	 public com.miui.server.input.magicpointer.MiuiMagicPointerServiceInternal ( ) {
		 /* .locals 0 */
		 /* .line 10 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void handleAppDied ( Integer p0, com.android.server.am.ProcessRecord p1 ) {
	 } // .end method
	 public abstract Boolean needInterceptSetCustomPointerIcon ( android.view.PointerIcon p0 ) {
	 } // .end method
	 public abstract Boolean needInterceptSetPointerIconType ( Integer p0 ) {
	 } // .end method
	 public abstract void onFocusedWindowChanged ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
	 } // .end method
	 public abstract void onUserChanged ( Integer p0 ) {
	 } // .end method
	 public abstract void setDisplayViewports ( java.util.List p0 ) {
		 /* .annotation system Ldalvik/annotation/Signature; */
		 /* value = { */
		 /* "(", */
		 /* "Ljava/util/List<", */
		 /* "Landroid/hardware/display/DisplayViewport;", */
		 /* ">;)V" */
		 /* } */
	 } // .end annotation
} // .end method
public abstract void setMagicPointerVisibility ( Boolean p0 ) {
} // .end method
public abstract void systemReady ( ) {
} // .end method
public abstract void updateMagicPointerPosition ( Float p0, Float p1 ) {
} // .end method
public abstract void updatePointerDisplayId ( Integer p0 ) {
} // .end method
