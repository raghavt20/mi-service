.class public abstract Lcom/miui/server/input/magicpointer/MiuiMagicPointerServiceInternal;
.super Ljava/lang/Object;
.source "MiuiMagicPointerServiceInternal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract handleAppDied(ILcom/android/server/am/ProcessRecord;)V
.end method

.method public abstract needInterceptSetCustomPointerIcon(Landroid/view/PointerIcon;)Z
.end method

.method public abstract needInterceptSetPointerIconType(I)Z
.end method

.method public abstract onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
.end method

.method public abstract onUserChanged(I)V
.end method

.method public abstract setDisplayViewports(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setMagicPointerVisibility(Z)V
.end method

.method public abstract systemReady()V
.end method

.method public abstract updateMagicPointerPosition(FF)V
.end method

.method public abstract updatePointerDisplayId(I)V
.end method
