.class public Lcom/miui/server/input/MiuiFingerPrintTapListener;
.super Ljava/lang/Object;
.source "MiuiFingerPrintTapListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/MiuiFingerPrintTapListener$H;,
        Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;,
        Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field private static final FINGERPRINT_PRODUCT:I = 0x888

.field private static final FINGERPRINT_VENDOR:I = 0x666

.field private static final TAG:Ljava/lang/String; = "MiuiFingerPrintTapListener"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentUserId:I

.field private mDoubleTapSideFp:Ljava/lang/String;

.field private mHandler:Lcom/miui/server/input/MiuiFingerPrintTapListener$H;

.field private mIsFeatureSupport:Z

.field private mIsRegisterListener:Z

.field private mIsSetupComplete:Z

.field private mMiuiFingerPrintTapGestureListener:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;

.field private mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

.field private mMiuiSettingsObserver:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/MiuiFingerPrintTapListener;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmDoubleTapSideFp(Lcom/miui/server/input/MiuiFingerPrintTapListener;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDoubleTapSideFp(Lcom/miui/server/input/MiuiFingerPrintTapListener;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$mtoggleFpDoubleTap(Lcom/miui/server/input/MiuiFingerPrintTapListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->toggleFpDoubleTap()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdatePrintTapState(Lcom/miui/server/input/MiuiFingerPrintTapListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->updatePrintTapState()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, -0x2

    iput v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mCurrentUserId:I

    .line 42
    const-string v0, "is_support_fingerprint_tap"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsFeatureSupport:Z

    .line 43
    iput-object p1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mContext:Landroid/content/Context;

    .line 44
    if-eqz v0, :cond_0

    .line 45
    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->initialize()V

    .line 47
    :cond_0
    return-void
.end method

.method private checkEmpty(Ljava/lang/String;)Z
    .locals 1
    .param p1, "feature"    # Ljava/lang/String;

    .line 126
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "none"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private initialize()V
    .locals 2

    .line 50
    new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;-><init>(Lcom/miui/server/input/MiuiFingerPrintTapListener;Lcom/miui/server/input/MiuiFingerPrintTapListener$H-IA;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mHandler:Lcom/miui/server/input/MiuiFingerPrintTapListener$H;

    .line 51
    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->isUserSetUp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z

    .line 52
    new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;

    invoke-direct {v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiFingerPrintTapGestureListener:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;

    .line 53
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    .line 54
    new-instance v0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mHandler:Lcom/miui/server/input/MiuiFingerPrintTapListener$H;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/MiuiFingerPrintTapListener;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiSettingsObserver:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;

    .line 55
    invoke-virtual {v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->observe()V

    .line 56
    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->updateSettings()V

    .line 57
    return-void
.end method

.method private isUserSetUp()Z
    .locals 4

    .line 130
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "user_setup_complete"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method private removeUnsupportedFunction()V
    .locals 4

    .line 107
    const-string v0, "launch_alipay_health_code"

    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    .line 108
    const-string v1, "launch_ai_shortcut"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "none"

    const/4 v2, -0x2

    const-string v3, "fingerprint_double_tap"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 113
    :cond_1
    return-void
.end method

.method private toggleFpDoubleTap()V
    .locals 5

    .line 87
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "fingerprint_double_tap"

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 89
    return-void
.end method

.method private updatePrintTapState()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->checkEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiFingerPrintTapGestureListener:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->registerPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z

    goto :goto_0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->checkEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiFingerPrintTapGestureListener:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->unregisterPointerEventListener(Lcom/miui/server/input/gesture/MiuiGestureListener;)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsRegisterListener:Z

    .line 123
    :cond_1
    :goto_0
    return-void
.end method

.method private updateSettings()V
    .locals 3

    .line 99
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiSettingsObserver:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;

    if-eqz v0, :cond_0

    .line 100
    nop

    .line 101
    const-string v1, "fingerprint_double_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 100
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->onChange(ZLandroid/net/Uri;)V

    .line 102
    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->removeUnsupportedFunction()V

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 135
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 136
    const-string v0, "MiuiFingerPrintTapListener"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 138
    const-string v0, "mIsSetupComplete="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 139
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 140
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 141
    const-string v0, "mDoubleTapSideFp="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "newUserId"    # I

    .line 92
    iget v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mCurrentUserId:I

    if-eq v0, p1, :cond_0

    .line 93
    iput p1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mCurrentUserId:I

    .line 94
    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->updateSettings()V

    .line 96
    :cond_0
    return-void
.end method

.method public shouldInterceptSlideFpTapKey(Landroid/view/KeyEvent;ZZ)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;
    .param p2, "isScreenOn"    # Z
    .param p3, "keyguard"    # Z

    .line 60
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x62

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/InputDevice;->getVendorId()I

    move-result v0

    const/16 v1, 0x666

    if-ne v0, v1, :cond_4

    .line 61
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/InputDevice;->getProductId()I

    move-result v0

    const/16 v1, 0x888

    if-ne v0, v1, :cond_4

    .line 62
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    if-eqz p2, :cond_3

    .line 63
    iget-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z

    if-nez v0, :cond_0

    .line 64
    invoke-direct {p0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->isUserSetUp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mIsSetupComplete:Z

    .line 65
    if-nez v0, :cond_0

    return v1

    .line 67
    :cond_0
    const-string v0, "MiuiFingerPrintTapListener"

    if-eqz p3, :cond_1

    .line 68
    const-string v2, "Keyguard active, so fingerprint double tap can\'t trigger function"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    return v1

    .line 71
    :cond_1
    iget-object v2, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mMiuiFingerPrintTapGestureListener:Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;

    invoke-virtual {v2}, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->isFingerPressing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 72
    const-string v2, "finger pressing, so fingerprint double tap can\'t trigger function"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    return v1

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mDoubleTapSideFp:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->checkEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 76
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener;->mHandler:Lcom/miui/server/input/MiuiFingerPrintTapListener$H;

    const-string v2, "fingerprint_double_tap"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/server/input/MiuiFingerPrintTapListener$H;->sendMessage(Landroid/os/Message;)Z

    .line 80
    :cond_3
    return v1

    .line 82
    :cond_4
    const/4 v0, 0x0

    return v0
.end method
