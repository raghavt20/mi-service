.class public Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
.super Ljava/lang/Object;
.source "MiuiCustomizeShortCutUtils.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MiuiKeyboardShortcutInfo"
.end annotation


# instance fields
.field private mClassName:Ljava/lang/String;

.field private mCustomized:Z

.field private mEnable:Z

.field private mHistoryKeyCode:J

.field private mPackageName:Ljava/lang/String;

.field private mShortcutKeyCode:J

.field private mType:I

.field final synthetic this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;


# direct methods
.method static bridge synthetic -$$Nest$fgetmClassName(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mClassName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmCustomized(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmEnable(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmPackageName(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mPackageName:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmShortcutKeyCode(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fgetmType(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I

    return p0
.end method

.method public constructor <init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 566
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 567
    return-void
.end method

.method public constructor <init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;JZIZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    .param p2, "shortcutKeyCode"    # J
    .param p4, "enable"    # Z
    .param p5, "type"    # I
    .param p6, "customized"    # Z

    .line 570
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 571
    iput-wide p2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J

    .line 572
    iput-boolean p4, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z

    .line 573
    iput p5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I

    .line 574
    iput-boolean p6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z

    .line 575
    return-void
.end method

.method public constructor <init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/view/KeyboardShortcutInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    .param p2, "info"    # Landroid/view/KeyboardShortcutInfo;

    .line 577
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 579
    return-void
.end method

.method private getShortcutLabel()Ljava/lang/String;
    .locals 7

    .line 623
    iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 624
    const-string v0, ""

    return-object v0

    .line 626
    :cond_0
    iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J

    .line 627
    .local v0, "keyCode":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 628
    .local v4, "shortcutLabel":Ljava/lang/StringBuilder;
    const-wide/high16 v5, 0x1000000000000L

    and-long/2addr v5, v0

    cmp-long v5, v5, v2

    if-eqz v5, :cond_1

    .line 629
    const-wide v5, -0x1000000000001L

    and-long/2addr v0, v5

    .line 630
    const-string v5, "meta "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 632
    :cond_1
    const-wide v5, 0x200000000L

    and-long/2addr v5, v0

    cmp-long v5, v5, v2

    if-eqz v5, :cond_2

    .line 633
    const-wide v5, -0x200000001L

    and-long/2addr v0, v5

    .line 634
    const-string v5, "alt "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 636
    :cond_2
    const-wide v5, 0x100000000000L

    and-long/2addr v5, v0

    cmp-long v5, v5, v2

    if-eqz v5, :cond_3

    .line 637
    const-wide v5, -0x100000000001L

    and-long/2addr v0, v5

    .line 638
    const-string v5, "ctrl "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 640
    :cond_3
    const-wide v5, 0x100000000L

    and-long/2addr v5, v0

    cmp-long v2, v5, v2

    if-eqz v2, :cond_4

    .line 641
    const-wide v2, -0x100000001L

    and-long/2addr v0, v2

    .line 642
    const-string/jumbo v2, "shift "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    :cond_4
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 645
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .line 663
    move-object v0, p1

    check-cast v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 664
    .local v0, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    iget v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I

    iget v2, v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I

    sub-int v3, v1, v2

    if-gez v3, :cond_0

    .line 665
    const/4 v1, 0x1

    return v1

    .line 666
    :cond_0
    sub-int/2addr v1, v2

    if-lez v1, :cond_1

    .line 667
    const/4 v1, -0x1

    return v1

    .line 669
    :cond_1
    const/4 v1, 0x0

    return v1
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    .line 615
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getHistoryKeyCode()J
    .locals 2

    .line 619
    iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mHistoryKeyCode:J

    return-wide v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .line 611
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getShortcutKeyCode()J
    .locals 2

    .line 595
    iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .line 607
    iget v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I

    return v0
.end method

.method public isCustomized()Z
    .locals 1

    .line 603
    iget-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z

    return v0
.end method

.method public isEnable()Z
    .locals 1

    .line 599
    iget-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z

    return v0
.end method

.method public setAppInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .line 586
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mPackageName:Ljava/lang/String;

    .line 587
    iput-object p2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mClassName:Ljava/lang/String;

    .line 588
    return-void
.end method

.method public setHistoryKeyCode(J)V
    .locals 0
    .param p1, "historyKeyCode"    # J

    .line 591
    iput-wide p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mHistoryKeyCode:J

    .line 592
    return-void
.end method

.method public setShortcutKeyCode(J)V
    .locals 0
    .param p1, "shortcutKeyCode"    # J

    .line 582
    iput-wide p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J

    .line 583
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 650
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MiuiKeyboardShortcutInfo{mShortcutKeyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 651
    invoke-direct {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEnable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPackageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mClassName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mClassName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHistoryKeyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mHistoryKeyCode:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCustomized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 650
    return-object v0
.end method
