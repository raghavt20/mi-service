public class com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo implements java.lang.Comparable {
	 /* .source "MiuiCustomizeShortCutUtils.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "MiuiKeyboardShortcutInfo" */
} // .end annotation
/* # instance fields */
private java.lang.String mClassName;
private Boolean mCustomized;
private Boolean mEnable;
private Long mHistoryKeyCode;
private java.lang.String mPackageName;
private Long mShortcutKeyCode;
private Integer mType;
final com.miui.server.input.util.MiuiCustomizeShortCutUtils this$0; //synthetic
/* # direct methods */
static java.lang.String -$$Nest$fgetmClassName ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mClassName;
} // .end method
static Boolean -$$Nest$fgetmCustomized ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z */
} // .end method
static Boolean -$$Nest$fgetmEnable ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z */
} // .end method
static java.lang.String -$$Nest$fgetmPackageName ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mPackageName;
} // .end method
static Long -$$Nest$fgetmShortcutKeyCode ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J */
/* return-wide v0 */
} // .end method
static Integer -$$Nest$fgetmType ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I */
} // .end method
public com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* .line 566 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 567 */
return;
} // .end method
public com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* .param p2, "shortcutKeyCode" # J */
/* .param p4, "enable" # Z */
/* .param p5, "type" # I */
/* .param p6, "customized" # Z */
/* .line 570 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 571 */
/* iput-wide p2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J */
/* .line 572 */
/* iput-boolean p4, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z */
/* .line 573 */
/* iput p5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I */
/* .line 574 */
/* iput-boolean p6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z */
/* .line 575 */
return;
} // .end method
public com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* .param p2, "info" # Landroid/view/KeyboardShortcutInfo; */
/* .line 577 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 579 */
return;
} // .end method
private java.lang.String getShortcutLabel ( ) {
/* .locals 7 */
/* .line 623 */
/* iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_0 */
/* .line 624 */
final String v0 = ""; // const-string v0, ""
/* .line 626 */
} // :cond_0
/* iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J */
/* .line 627 */
/* .local v0, "keyCode":J */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 628 */
/* .local v4, "shortcutLabel":Ljava/lang/StringBuilder; */
/* const-wide/high16 v5, 0x1000000000000L */
/* and-long/2addr v5, v0 */
/* cmp-long v5, v5, v2 */
if ( v5 != null) { // if-eqz v5, :cond_1
/* .line 629 */
/* const-wide v5, -0x1000000000001L */
/* and-long/2addr v0, v5 */
/* .line 630 */
final String v5 = "meta "; // const-string v5, "meta "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 632 */
} // :cond_1
/* const-wide v5, 0x200000000L */
/* and-long/2addr v5, v0 */
/* cmp-long v5, v5, v2 */
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 633 */
/* const-wide v5, -0x200000001L */
/* and-long/2addr v0, v5 */
/* .line 634 */
final String v5 = "alt "; // const-string v5, "alt "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 636 */
} // :cond_2
/* const-wide v5, 0x100000000000L */
/* and-long/2addr v5, v0 */
/* cmp-long v5, v5, v2 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 637 */
/* const-wide v5, -0x100000000001L */
/* and-long/2addr v0, v5 */
/* .line 638 */
final String v5 = "ctrl "; // const-string v5, "ctrl "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 640 */
} // :cond_3
/* const-wide v5, 0x100000000L */
/* and-long/2addr v5, v0 */
/* cmp-long v2, v5, v2 */
if ( v2 != null) { // if-eqz v2, :cond_4
/* .line 641 */
/* const-wide v2, -0x100000001L */
/* and-long/2addr v0, v2 */
/* .line 642 */
/* const-string/jumbo v2, "shift " */
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 644 */
} // :cond_4
(( java.lang.StringBuilder ) v4 ).append ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 645 */
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public Integer compareTo ( java.lang.Object p0 ) {
/* .locals 4 */
/* .param p1, "o" # Ljava/lang/Object; */
/* .line 663 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 664 */
/* .local v0, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* iget v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I */
/* iget v2, v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I */
/* sub-int v3, v1, v2 */
/* if-gez v3, :cond_0 */
/* .line 665 */
int v1 = 1; // const/4 v1, 0x1
/* .line 666 */
} // :cond_0
/* sub-int/2addr v1, v2 */
/* if-lez v1, :cond_1 */
/* .line 667 */
int v1 = -1; // const/4 v1, -0x1
/* .line 669 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
} // .end method
public java.lang.String getClassName ( ) {
/* .locals 1 */
/* .line 615 */
v0 = this.mClassName;
} // .end method
public Long getHistoryKeyCode ( ) {
/* .locals 2 */
/* .line 619 */
/* iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mHistoryKeyCode:J */
/* return-wide v0 */
} // .end method
public java.lang.String getPackageName ( ) {
/* .locals 1 */
/* .line 611 */
v0 = this.mPackageName;
} // .end method
public Long getShortcutKeyCode ( ) {
/* .locals 2 */
/* .line 595 */
/* iget-wide v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J */
/* return-wide v0 */
} // .end method
public Integer getType ( ) {
/* .locals 1 */
/* .line 607 */
/* iget v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I */
} // .end method
public Boolean isCustomized ( ) {
/* .locals 1 */
/* .line 603 */
/* iget-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z */
} // .end method
public Boolean isEnable ( ) {
/* .locals 1 */
/* .line 599 */
/* iget-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z */
} // .end method
public void setAppInfo ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "className" # Ljava/lang/String; */
/* .line 586 */
this.mPackageName = p1;
/* .line 587 */
this.mClassName = p2;
/* .line 588 */
return;
} // .end method
public void setHistoryKeyCode ( Long p0 ) {
/* .locals 0 */
/* .param p1, "historyKeyCode" # J */
/* .line 591 */
/* iput-wide p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mHistoryKeyCode:J */
/* .line 592 */
return;
} // .end method
public void setShortcutKeyCode ( Long p0 ) {
/* .locals 0 */
/* .param p1, "shortcutKeyCode" # J */
/* .line 582 */
/* iput-wide p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mShortcutKeyCode:J */
/* .line 583 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 3 */
/* .line 650 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "MiuiKeyboardShortcutInfo{mShortcutKeyCode="; // const-string v1, "MiuiKeyboardShortcutInfo{mShortcutKeyCode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 651 */
/* invoke-direct {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutLabel()Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", mEnable="; // const-string v1, ", mEnable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", mType="; // const-string v1, ", mType="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mType:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", mPackageName=\'"; // const-string v1, ", mPackageName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mPackageName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x27 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v2 = ", mClassName=\'"; // const-string v2, ", mClassName=\'"
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.mClassName;
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
final String v1 = ", mHistoryKeyCode="; // const-string v1, ", mHistoryKeyCode="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mHistoryKeyCode:J */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = ", mCustomized="; // const-string v1, ", mCustomized="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->mCustomized:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 650 */
} // .end method
