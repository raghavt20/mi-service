public class com.miui.server.input.util.MiuiCustomizeShortCutUtils {
	 /* .source "MiuiCustomizeShortCutUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;, */
	 /* Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer ALL_SHORTCUT;
public static final Integer APP_SHORTCUT;
public static final java.lang.String ATTRIBUTE_ALT;
public static final java.lang.String ATTRIBUTE_CLASSNAME;
public static final java.lang.String ATTRIBUTE_CTRL;
public static final java.lang.String ATTRIBUTE_CUSTOMIZED;
public static final java.lang.String ATTRIBUTE_ENABLE;
public static final java.lang.String ATTRIBUTE_KEYCODE;
public static final java.lang.String ATTRIBUTE_LEFT_ALT;
public static final java.lang.String ATTRIBUTE_META;
public static final java.lang.String ATTRIBUTE_PACKAGENAME;
public static final java.lang.String ATTRIBUTE_RIGHT_ALT;
public static final java.lang.String ATTRIBUTE_SHIFT;
public static final java.lang.String ATTRIBUTE_TYPE;
public static final Integer SYSTEM_SHORTCUT;
private static final Integer UPDATE_ADD;
private static final Integer UPDATE_DEL;
private static final Integer UPDATE_MODIFY;
private static final Integer UPDATE_RESET_APP;
private static final Integer UPDATE_RESET_SYSTEM;
private static volatile com.miui.server.input.util.MiuiCustomizeShortCutUtils mInstance;
/* # instance fields */
private final java.lang.String TAG;
private final java.lang.String TAG_1;
private final java.lang.String TAG_2;
private final java.io.File mBackupFile;
private android.content.Context mContext;
private java.util.List mDefaultAppShortcutInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mDefaultShortcutInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mDefaultSystemShortcutInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.io.File mFile;
private volatile Boolean mInterceptShortcutKey;
private java.util.List mKeyboardShortcutInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mNeedLoadInfoForDefaultRes;
private final com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil mReflectionUtil;
private final java.lang.String mShortCutFileName;
private android.util.LongSparseArray mShortcutInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/LongSparseArray<", */
/* "Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
public static void $r8$lambda$1diIpLZS0vOPaYBfDAtNs8fJ1ho ( com.miui.server.input.util.MiuiCustomizeShortCutUtils p0, android.content.IntentFilter p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$enableAutoRemoveShortCutWhenAppRemove$3(Landroid/content/IntentFilter;)V */
return;
} // .end method
public static void $r8$lambda$NYK3NNB8OfMcVHcqqizSmSzdCQ0 ( com.miui.server.input.util.MiuiCustomizeShortCutUtils p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$new$2()V */
return;
} // .end method
public static void $r8$lambda$TOYPeznr90UgOQk0QdV9JiIfrtY ( com.miui.server.input.util.MiuiCustomizeShortCutUtils p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$new$1()V */
return;
} // .end method
public static void $r8$lambda$wvqXSwxxbLhjO8kbuXrrwdsq9QM ( com.miui.server.input.util.MiuiCustomizeShortCutUtils p0, android.content.IntentFilter p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$new$0(Landroid/content/IntentFilter;)V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.util.MiuiCustomizeShortCutUtils p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static void -$$Nest$mdelShortCutByPackage ( com.miui.server.input.util.MiuiCustomizeShortCutUtils p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->delShortCutByPackage(Ljava/lang/String;)V */
return;
} // .end method
private com.miui.server.input.util.MiuiCustomizeShortCutUtils ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 96 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 54 */
final String v0 = "MiuiCustomizeShortCutUtils"; // const-string v0, "MiuiCustomizeShortCutUtils"
this.TAG = v0;
/* .line 55 */
final String v0 = "ShortcutInfos"; // const-string v0, "ShortcutInfos"
this.TAG_1 = v0;
/* .line 56 */
final String v0 = "ShortcutInfo"; // const-string v0, "ShortcutInfo"
this.TAG_2 = v0;
/* .line 81 */
/* new-instance v0, Landroid/util/LongSparseArray; */
/* invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V */
this.mShortcutInfos = v0;
/* .line 82 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mKeyboardShortcutInfos = v0;
/* .line 83 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDefaultShortcutInfos = v0;
/* .line 84 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDefaultSystemShortcutInfos = v0;
/* .line 85 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mDefaultAppShortcutInfos = v0;
/* .line 87 */
final String v0 = "miuishortcuts"; // const-string v0, "miuishortcuts"
this.mShortCutFileName = v0;
/* .line 93 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mInterceptShortcutKey:Z */
/* .line 94 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z */
/* .line 97 */
this.mContext = p1;
/* .line 99 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v2 = "miuishortcuts.xml"; // const-string v2, "miuishortcuts.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mFile = v0;
/* .line 101 */
/* new-instance v0, Ljava/io/File; */
android.os.Environment .getDataSystemDirectory ( );
final String v2 = "miuishortcuts-backup.xml"; // const-string v2, "miuishortcuts-backup.xml"
/* invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
this.mBackupFile = v0;
/* .line 104 */
/* new-instance v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V */
this.mReflectionUtil = v0;
/* .line 105 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 106 */
/* .local v0, "local_switch":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.LOCALE_CHANGED"; // const-string v1, "android.intent.action.LOCALE_CHANGED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 107 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0, v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/content/IntentFilter;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 115 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 116 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda3; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 118 */
return;
} // .end method
private void delShortCutByPackage ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 440 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = v1 = this.mKeyboardShortcutInfos;
/* if-ge v0, v1, :cond_1 */
/* .line 441 */
v1 = this.mKeyboardShortcutInfos;
/* check-cast v1, Landroid/view/KeyboardShortcutInfo; */
/* .line 442 */
/* .local v1, "info":Landroid/view/KeyboardShortcutInfo; */
(( android.view.KeyboardShortcutInfo ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/view/KeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
if ( v2 != null) { // if-eqz v2, :cond_0
(( android.view.KeyboardShortcutInfo ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/view/KeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 443 */
v2 = this.mShortcutInfos;
(( android.view.KeyboardShortcutInfo ) v1 ).getShortcutKeyCode ( ); // invoke-virtual {v1}, Landroid/view/KeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v3 */
(( android.util.LongSparseArray ) v2 ).delete ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/util/LongSparseArray;->delete(J)V
/* .line 444 */
v2 = this.mKeyboardShortcutInfos;
/* .line 445 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).writeShortCuts ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->writeShortCuts()V
/* .line 446 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "delShortCutByPackage: "; // const-string v3, "delShortCutByPackage: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiCustomizeShortCutUtils"; // const-string v3, "MiuiCustomizeShortCutUtils"
android.util.Slog .i ( v3,v2 );
/* .line 447 */
/* .line 440 */
} // .end local v1 # "info":Landroid/view/KeyboardShortcutInfo;
} // :cond_0
/* add-int/lit8 v0, v0, 0x1 */
/* .line 450 */
} // .end local v0 # "i":I
} // :cond_1
} // :goto_1
return;
} // .end method
public static com.miui.server.input.util.MiuiCustomizeShortCutUtils getInstance ( android.content.Context p0 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 121 */
v0 = com.miui.server.input.util.MiuiCustomizeShortCutUtils.mInstance;
/* if-nez v0, :cond_1 */
/* .line 122 */
/* const-class v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* monitor-enter v0 */
/* .line 123 */
try { // :try_start_0
v1 = com.miui.server.input.util.MiuiCustomizeShortCutUtils.mInstance;
/* if-nez v1, :cond_0 */
/* .line 124 */
/* new-instance v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;-><init>(Landroid/content/Context;)V */
/* .line 126 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 128 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.util.MiuiCustomizeShortCutUtils.mInstance;
} // .end method
private void lambda$enableAutoRemoveShortCutWhenAppRemove$3 ( android.content.IntentFilter p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "packageFilter" # Landroid/content/IntentFilter; */
/* .line 190 */
v0 = this.mContext;
/* new-instance v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V */
v2 = android.os.UserHandle.ALL;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v3, p1 */
/* invoke-virtual/range {v0 ..v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
return;
} // .end method
private void lambda$new$0 ( android.content.IntentFilter p0 ) { //synthethic
/* .locals 6 */
/* .param p1, "local_switch" # Landroid/content/IntentFilter; */
/* .line 108 */
v0 = this.mContext;
/* new-instance v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V */
v2 = android.os.UserHandle.ALL;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
/* move-object v3, p1 */
/* invoke-virtual/range {v0 ..v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent; */
return;
} // .end method
private void lambda$new$1 ( ) { //synthethic
/* .locals 0 */
/* .line 115 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).loadShortCuts ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V
return;
} // .end method
private void lambda$new$2 ( ) { //synthethic
/* .locals 1 */
/* .line 116 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadFromDefaultRes(Z)V */
return;
} // .end method
private void loadFromDefaultRes ( Boolean p0 ) {
/* .locals 8 */
/* .param p1, "onlyInitDefault" # Z */
/* .line 207 */
try { // :try_start_0
/* iget-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z */
/* if-nez v0, :cond_0 */
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
v0 = this.mContext;
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
final String v1 = "miuishortcuts"; // const-string v1, "miuishortcuts"
/* const-string/jumbo v2, "xml" */
final String v3 = "android.miui"; // const-string v3, "android.miui"
v0 = (( android.content.res.Resources ) v0 ).getIdentifier ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
/* .line 212 */
/* .local v0, "resId":I */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getXml ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
/* .line 213 */
/* .local v1, "parser":Landroid/content/res/XmlResourceParser; */
final String v2 = "ShortcutInfos"; // const-string v2, "ShortcutInfos"
com.android.internal.util.XmlUtils .beginDocument ( v1,v2 );
/* .line 214 */
/* new-instance v2, Ljava/util/ArrayList; */
/* invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V */
/* .line 216 */
/* .local v2, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
} // :goto_0
com.android.internal.util.XmlUtils .nextElement ( v1 );
v3 = /* .line 217 */
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_1 */
/* .line 218 */
/* .line 220 */
} // :cond_1
final String v3 = "ShortcutInfo"; // const-string v3, "ShortcutInfo"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v3, :cond_5 */
/* .line 221 */
/* nop */
/* .line 235 */
} // :goto_1
v3 = this.mDefaultShortcutInfos;
/* .line 236 */
v3 = this.mDefaultAppShortcutInfos;
/* .line 237 */
v3 = this.mDefaultSystemShortcutInfos;
/* .line 238 */
(( java.util.ArrayList ) v2 ).iterator ( ); // invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_4
/* check-cast v4, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 239 */
/* .local v4, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v5 = this.mReflectionUtil;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ) v5 ).reflectObject ( v4 ); // invoke-virtual {v5, v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;
/* .line 240 */
/* .local v5, "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo; */
v6 = this.mDefaultShortcutInfos;
/* .line 241 */
v6 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v4 ).getType ( ); // invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v6, :cond_2 */
/* .line 242 */
v6 = this.mDefaultAppShortcutInfos;
/* .line 243 */
/* if-nez p1, :cond_3 */
v6 = this.mContext;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
v6 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).checkAppInstalled ( v6, v7 ); // invoke-virtual {p0, v6, v7}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 244 */
v6 = this.mKeyboardShortcutInfos;
/* .line 247 */
} // :cond_2
v6 = this.mDefaultSystemShortcutInfos;
/* .line 248 */
/* if-nez p1, :cond_3 */
/* .line 249 */
v6 = this.mKeyboardShortcutInfos;
/* .line 252 */
} // .end local v4 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
} // .end local v5 # "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
} // :cond_3
} // :goto_3
/* .line 253 */
} // :cond_4
int v3 = 0; // const/4 v3, 0x0
/* iput-boolean v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z */
/* .line 256 */
} // .end local v0 # "resId":I
} // .end local v1 # "parser":Landroid/content/res/XmlResourceParser;
} // .end local v2 # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
/* .line 223 */
/* .restart local v0 # "resId":I */
/* .restart local v1 # "parser":Landroid/content/res/XmlResourceParser; */
/* .restart local v2 # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
} // :cond_5
/* invoke-direct {p0, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->readFromParser(Lorg/xmlpull/v1/XmlPullParser;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 224 */
/* .local v3, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* if-nez p1, :cond_7 */
/* .line 225 */
v4 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v3 ).getType ( ); // invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v4, :cond_6 */
/* .line 226 */
v4 = this.mContext;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v3 ).getPackageName ( ); // invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
v4 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).checkAppInstalled ( v4, v5 ); // invoke-virtual {p0, v4, v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
if ( v4 != null) { // if-eqz v4, :cond_7
/* .line 227 */
v4 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v3 ).getShortcutKeyCode ( ); // invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v5 */
(( android.util.LongSparseArray ) v4 ).put ( v5, v6, v3 ); // invoke-virtual {v4, v5, v6, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 230 */
} // :cond_6
v4 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v3 ).getShortcutKeyCode ( ); // invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v5 */
(( android.util.LongSparseArray ) v4 ).put ( v5, v6, v3 ); // invoke-virtual {v4, v5, v6, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 233 */
} // :cond_7
} // :goto_4
(( java.util.ArrayList ) v2 ).add ( v3 ); // invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 234 */
/* nop */
} // .end local v3 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
/* goto/16 :goto_0 */
/* .line 254 */
} // .end local v0 # "resId":I
} // .end local v1 # "parser":Landroid/content/res/XmlResourceParser;
} // .end local v2 # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
/* :catch_0 */
/* move-exception v0 */
/* .line 255 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiCustomizeShortCutUtils"; // const-string v1, "MiuiCustomizeShortCutUtils"
final String v2 = "Got exception parsing bookmarks."; // const-string v2, "Got exception parsing bookmarks."
android.util.Slog .w ( v1,v2,v0 );
/* .line 257 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_5
return;
} // .end method
private com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo readFromParser ( org.xmlpull.v1.XmlPullParser p0 ) {
/* .locals 23 */
/* .param p1, "in" # Lorg/xmlpull/v1/XmlPullParser; */
/* .line 466 */
/* move-object/from16 v0, p1 */
/* const-string/jumbo v1, "true" */
/* .line 467 */
/* .local v1, "metaStateOn":Ljava/lang/String; */
final String v2 = "meta"; // const-string v2, "meta"
int v3 = 0; // const/4 v3, 0x0
v2 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 468 */
/* .local v2, "meta":Z */
/* const-string/jumbo v4, "shift" */
v4 = (( java.lang.String ) v1 ).equals ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 469 */
/* .local v4, "shift":Z */
final String v5 = "ctrl"; // const-string v5, "ctrl"
v5 = (( java.lang.String ) v1 ).equals ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 470 */
/* .local v5, "ctrl":Z */
final String v6 = "alt"; // const-string v6, "alt"
v6 = (( java.lang.String ) v1 ).equals ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 471 */
/* .local v6, "alt":Z */
final String v7 = "ralt"; // const-string v7, "ralt"
v7 = (( java.lang.String ) v1 ).equals ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 472 */
/* .local v7, "rctrl":Z */
final String v8 = "lalt"; // const-string v8, "lalt"
v8 = (( java.lang.String ) v1 ).equals ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* .line 473 */
/* .local v8, "lctrl":Z */
final String v9 = "keycode"; // const-string v9, "keycode"
v9 = java.lang.Integer .parseInt ( v9 );
/* .line 475 */
/* .local v9, "keyCode":I */
/* const-wide/16 v10, 0x0 */
/* .line 476 */
/* .local v10, "shortcutCode":J */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 477 */
/* const-wide/high16 v12, 0x1000000000000L */
/* or-long/2addr v12, v10 */
} // :cond_0
/* move-wide v12, v10 */
} // :goto_0
/* move-wide v10, v12 */
/* .line 478 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 479 */
/* const-wide v12, 0x100000000L */
/* or-long/2addr v12, v10 */
} // :cond_1
/* move-wide v12, v10 */
} // :goto_1
/* move-wide v10, v12 */
/* .line 480 */
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 481 */
/* const-wide v12, 0x200000000L */
/* or-long/2addr v12, v10 */
} // :cond_2
/* move-wide v12, v10 */
} // :goto_2
/* move-wide v10, v12 */
/* .line 482 */
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 483 */
/* const-wide v12, 0x100000000000L */
/* or-long/2addr v12, v10 */
} // :cond_3
/* move-wide v12, v10 */
} // :goto_3
/* move-wide v10, v12 */
/* .line 484 */
if ( v7 != null) { // if-eqz v7, :cond_4
/* .line 485 */
/* const-wide v12, 0x2000000000L */
/* or-long/2addr v12, v10 */
} // :cond_4
/* move-wide v12, v10 */
} // :goto_4
/* move-wide v10, v12 */
/* .line 486 */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 487 */
/* const-wide v12, 0x1000000000L */
/* or-long/2addr v12, v10 */
} // :cond_5
/* move-wide v12, v10 */
} // :goto_5
/* move-wide v10, v12 */
/* .line 488 */
/* int-to-long v12, v9 */
/* or-long/2addr v10, v12 */
/* .line 490 */
/* const-string/jumbo v12, "type" */
v12 = java.lang.Integer .parseInt ( v12 );
/* .line 491 */
/* .local v12, "type":I */
final String v13 = "enable"; // const-string v13, "enable"
v13 = java.lang.Boolean .parseBoolean ( v13 );
/* .line 492 */
/* .local v13, "enable":Z */
final String v14 = "customized"; // const-string v14, "customized"
v21 = java.lang.Boolean .parseBoolean ( v14 );
/* .line 493 */
/* .local v21, "customized":Z */
/* new-instance v22, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* move-object/from16 v14, v22 */
/* move-object/from16 v15, p0 */
/* move-wide/from16 v16, v10 */
/* move/from16 v18, v13 */
/* move/from16 v19, v12 */
/* move/from16 v20, v21 */
/* invoke-direct/range {v14 ..v20}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;JZIZ)V */
/* .line 494 */
/* .local v14, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* if-nez v12, :cond_6 */
/* .line 495 */
final String v15 = "packageName"; // const-string v15, "packageName"
/* .line 496 */
/* .local v15, "packageName":Ljava/lang/String; */
/* move-object/from16 v16, v1 */
} // .end local v1 # "metaStateOn":Ljava/lang/String;
/* .local v16, "metaStateOn":Ljava/lang/String; */
final String v1 = "className"; // const-string v1, "className"
/* .line 497 */
/* .local v1, "className":Ljava/lang/String; */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v14 ).setAppInfo ( v15, v1 ); // invoke-virtual {v14, v15, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->setAppInfo(Ljava/lang/String;Ljava/lang/String;)V
/* .line 494 */
} // .end local v15 # "packageName":Ljava/lang/String;
} // .end local v16 # "metaStateOn":Ljava/lang/String;
/* .local v1, "metaStateOn":Ljava/lang/String; */
} // :cond_6
/* move-object/from16 v16, v1 */
/* .line 500 */
} // .end local v1 # "metaStateOn":Ljava/lang/String;
/* .restart local v16 # "metaStateOn":Ljava/lang/String; */
} // :goto_6
} // .end method
/* # virtual methods */
public Boolean addShortcut ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 426 */
v0 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v1 */
(( android.util.LongSparseArray ) v0 ).get ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 427 */
int v0 = 0; // const/4 v0, 0x0
/* .line 430 */
} // :cond_0
v0 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v1 */
(( android.util.LongSparseArray ) v0 ).put ( v1, v2, p1 ); // invoke-virtual {v0, v1, v2, p1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 431 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
public Boolean checkAppInstalled ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "pkgName" # Ljava/lang/String; */
/* .line 260 */
v0 = android.text.TextUtils .isEmpty ( p2 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 261 */
/* .line 264 */
} // :cond_0
try { // :try_start_0
(( android.content.Context ) p1 ).getPackageManager ( ); // invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackageInfo ( p2, v1 ); // invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 267 */
/* nop */
/* .line 268 */
int v0 = 1; // const/4 v0, 0x1
/* .line 265 */
/* :catch_0 */
/* move-exception v0 */
/* .line 266 */
/* .local v0, "x":Ljava/lang/Exception; */
} // .end method
public void delShortCutByShortcutCode ( Long p0 ) {
/* .locals 1 */
/* .param p1, "shortcutCode" # J */
/* .line 435 */
v0 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v0 ).delete ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->delete(J)V
/* .line 436 */
return;
} // .end method
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 504 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 505 */
final String v0 = "MiuiCustomizeShortCutUtils"; // const-string v0, "MiuiCustomizeShortCutUtils"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 506 */
(( java.io.PrintWriter ) p2 ).println ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 507 */
final String v0 = "mShortcutInfos ="; // const-string v0, "mShortcutInfos ="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 508 */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = this.mShortcutInfos;
v1 = (( android.util.LongSparseArray ) v1 ).size ( ); // invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I
/* if-ge v0, v1, :cond_0 */
/* .line 509 */
v1 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v1 ).valueAt ( v0 ); // invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v1 ).toString ( ); // invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p2 ).println ( v1 ); // invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 508 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 511 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
public void enableAutoRemoveShortCutWhenAppRemove ( ) {
/* .locals 3 */
/* .line 185 */
final String v0 = "MiuiCustomizeShortCutUtils"; // const-string v0, "MiuiCustomizeShortCutUtils"
final String v1 = "enableAutoRemoveShortCutWhenAppRemove"; // const-string v1, "enableAutoRemoveShortCutWhenAppRemove"
android.util.Slog .i ( v0,v1 );
/* .line 186 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
/* .line 187 */
/* .local v0, "packageFilter":Landroid/content/IntentFilter; */
final String v1 = "android.intent.action.PACKAGE_REMOVED"; // const-string v1, "android.intent.action.PACKAGE_REMOVED"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 188 */
final String v1 = "package"; // const-string v1, "package"
(( android.content.IntentFilter ) v0 ).addDataScheme ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V
/* .line 189 */
com.android.server.MiuiBgThread .getHandler ( );
/* new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0, v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/content/IntentFilter;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 203 */
return;
} // .end method
public java.util.List getDefaultKeyboardShortcutInfos ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 145 */
v0 = this.mDefaultShortcutInfos;
} // .end method
public java.util.List getKeyboardShortcutInfo ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Landroid/view/KeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 132 */
v0 = this.mContext;
v1 = this.mKeyboardShortcutInfos;
/* .line 133 */
com.miui.server.input.custom.InputMiuiDesktopMode .getMuiDeskModeKeyboardShortcutInfo ( v0,v1 );
/* .line 134 */
/* .local v0, "miuiDeskModeKeyboardShortcutInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/KeyboardShortcutInfo;>;" */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 135 */
/* .line 137 */
} // :cond_0
v1 = this.mKeyboardShortcutInfos;
} // .end method
public android.util.LongSparseArray getMiuiKeyboardShortcutInfo ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Landroid/util/LongSparseArray<", */
/* "Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 141 */
v0 = this.mShortcutInfos;
} // .end method
public com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo getShortcut ( Long p0 ) {
/* .locals 1 */
/* .param p1, "shortcutCode" # J */
/* .line 453 */
v0 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v0 ).get ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
} // .end method
public Boolean isShortcutExist ( Long p0 ) {
/* .locals 1 */
/* .param p1, "shortcutCode" # J */
/* .line 422 */
v0 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v0 ).get ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void loadShortCuts ( ) {
/* .locals 9 */
/* .line 272 */
final String v0 = "loadShortCuts"; // const-string v0, "loadShortCuts"
final String v1 = "MiuiCustomizeShortCutUtils"; // const-string v1, "MiuiCustomizeShortCutUtils"
android.util.Slog .i ( v1,v0 );
/* .line 273 */
int v0 = 0; // const/4 v0, 0x0
/* .line 274 */
/* .local v0, "str":Ljava/io/FileInputStream; */
v2 = this.mBackupFile;
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 276 */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
v3 = this.mBackupFile;
/* invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v0, v2 */
/* .line 277 */
final String v2 = "Need to read from backup settings file"; // const-string v2, "Need to read from backup settings file"
android.util.Slog .w ( v1,v2 );
/* .line 278 */
v2 = this.mFile;
v2 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 282 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Cleaning up settings file "; // const-string v3, "Cleaning up settings file "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mFile;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 283 */
v2 = this.mFile;
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 287 */
} // :cond_0
/* .line 285 */
/* :catch_0 */
/* move-exception v2 */
/* .line 290 */
} // :cond_1
} // :goto_0
v2 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v2 ).clear ( ); // invoke-virtual {v2}, Landroid/util/LongSparseArray;->clear()V
/* .line 291 */
v2 = this.mKeyboardShortcutInfos;
/* .line 294 */
int v2 = 1; // const/4 v2, 0x1
/* if-nez v0, :cond_3 */
/* .line 295 */
try { // :try_start_1
v3 = this.mFile;
v3 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_2 */
/* .line 296 */
final String v3 = "No shortcuts xml file found"; // const-string v3, "No shortcuts xml file found"
android.util.Slog .w ( v1,v3 );
/* .line 297 */
/* iput-boolean v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z */
/* .line 298 */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadFromDefaultRes(Z)V */
/* .line 299 */
final String v2 = "load shortcuts from res successfully"; // const-string v2, "load shortcuts from res successfully"
android.util.Slog .i ( v1,v2 );
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 333 */
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 300 */
return;
/* .line 302 */
} // :cond_2
try { // :try_start_2
/* new-instance v3, Ljava/io/FileInputStream; */
v4 = this.mFile;
/* invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v0, v3 */
/* .line 305 */
} // :cond_3
android.util.Xml .newPullParser ( );
/* .line 306 */
/* .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser; */
v4 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v4 ).name ( ); // invoke-virtual {v4}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 308 */
final String v4 = "ShortcutInfos"; // const-string v4, "ShortcutInfos"
com.android.internal.util.XmlUtils .beginDocument ( v3,v4 );
/* .line 309 */
/* new-instance v4, Ljava/util/ArrayList; */
/* invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V */
/* .line 311 */
/* .local v4, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
} // :goto_1
com.android.internal.util.XmlUtils .nextElement ( v3 );
v5 = /* .line 312 */
/* if-ne v5, v2, :cond_4 */
/* .line 313 */
/* .line 316 */
} // :cond_4
final String v6 = "ShortcutInfo"; // const-string v6, "ShortcutInfo"
v5 = (( java.lang.String ) v5 ).equals ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v5, :cond_6 */
/* .line 317 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "read xml tagName error."; // const-string v5, "read xml tagName error."
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 318 */
/* nop */
/* .line 324 */
} // :goto_2
(( java.util.ArrayList ) v4 ).iterator ( ); // invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v5 = } // :goto_3
if ( v5 != null) { // if-eqz v5, :cond_5
/* check-cast v5, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 325 */
/* .local v5, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v6 = this.mReflectionUtil;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ) v6 ).reflectObject ( v5 ); // invoke-virtual {v6, v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;
/* .line 326 */
/* .local v6, "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo; */
v7 = this.mKeyboardShortcutInfos;
/* .line 327 */
/* nop */
} // .end local v5 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
} // .end local v6 # "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
/* .line 328 */
} // :cond_5
final String v2 = "load shortcuts from xml successfully"; // const-string v2, "load shortcuts from xml successfully"
android.util.Slog .i ( v1,v2 );
/* .line 333 */
/* nop */
} // .end local v3 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v4 # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
/* .line 320 */
/* .restart local v3 # "parser":Lorg/xmlpull/v1/XmlPullParser; */
/* .restart local v4 # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
} // :cond_6
/* invoke-direct {p0, v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->readFromParser(Lorg/xmlpull/v1/XmlPullParser;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 321 */
/* .restart local v5 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v6 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v5 ).getShortcutKeyCode ( ); // invoke-virtual {v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v7 */
(( android.util.LongSparseArray ) v6 ).put ( v7, v8, v5 ); // invoke-virtual {v6, v7, v8, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 322 */
(( java.util.ArrayList ) v4 ).add ( v5 ); // invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 ..:try_end_2} :catch_1 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 323 */
/* nop */
} // .end local v5 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
/* .line 333 */
} // .end local v3 # "parser":Lorg/xmlpull/v1/XmlPullParser;
} // .end local v4 # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
/* :catchall_0 */
/* move-exception v1 */
/* .line 329 */
/* :catch_1 */
/* move-exception v2 */
/* .line 330 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_3
v3 = this.mFile;
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 331 */
final String v3 = "Error reading miui shortcuts settings"; // const-string v3, "Error reading miui shortcuts settings"
android.util.Slog .wtf ( v1,v3,v2 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 333 */
/* nop */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_4
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 334 */
/* nop */
/* .line 335 */
return;
/* .line 333 */
} // :goto_5
libcore.io.IoUtils .closeQuietly ( v0 );
/* .line 334 */
/* throw v1 */
} // .end method
public void resetShortCut ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "type" # I */
/* .line 392 */
int v0 = 2; // const/4 v0, 0x2
int v1 = 1; // const/4 v1, 0x1
/* if-eq p1, v1, :cond_0 */
/* if-eq p1, v0, :cond_0 */
/* .line 393 */
return;
/* .line 395 */
} // :cond_0
v2 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v2 ).clone ( ); // invoke-virtual {v2}, Landroid/util/LongSparseArray;->clone()Landroid/util/LongSparseArray;
/* .line 396 */
/* .local v2, "shortcutInfos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;" */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( android.util.LongSparseArray ) v2 ).size ( ); // invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I
/* if-ge v3, v4, :cond_4 */
/* .line 397 */
(( android.util.LongSparseArray ) v2 ).valueAt ( v3 ); // invoke-virtual {v2, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v4, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 398 */
/* .local v4, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* if-ne p1, v1, :cond_1 */
v5 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v4 ).getType ( ); // invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v5, :cond_2 */
} // :cond_1
/* if-ne p1, v0, :cond_3 */
/* .line 399 */
v5 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v4 ).getType ( ); // invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v5, :cond_3 */
/* .line 400 */
} // :cond_2
v5 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v4 ).getShortcutKeyCode ( ); // invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v6 */
(( android.util.LongSparseArray ) v5 ).delete ( v6, v7 ); // invoke-virtual {v5, v6, v7}, Landroid/util/LongSparseArray;->delete(J)V
/* .line 401 */
v5 = this.mReflectionUtil;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ) v5 ).reflectObject ( v4 ); // invoke-virtual {v5, v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;
/* .line 402 */
/* .local v5, "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo; */
v6 = this.mKeyboardShortcutInfos;
/* .line 396 */
} // .end local v4 # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
} // .end local v5 # "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
} // :cond_3
/* add-int/lit8 v3, v3, 0x1 */
/* .line 405 */
} // .end local v3 # "i":I
} // :cond_4
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 406 */
/* .local v1, "defaultShortcutInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/KeyboardShortcutInfo;>;" */
/* if-ne p1, v0, :cond_5 */
/* .line 407 */
v1 = this.mDefaultAppShortcutInfos;
/* .line 409 */
} // :cond_5
v1 = this.mDefaultSystemShortcutInfos;
/* .line 411 */
} // :goto_1
v4 = } // :goto_2
if ( v4 != null) { // if-eqz v4, :cond_7
/* check-cast v4, Landroid/view/KeyboardShortcutInfo; */
/* .line 412 */
/* .local v4, "info":Landroid/view/KeyboardShortcutInfo; */
/* if-ne p1, v0, :cond_6 */
v5 = this.mContext;
(( android.view.KeyboardShortcutInfo ) v4 ).getPackageName ( ); // invoke-virtual {v4}, Landroid/view/KeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
v5 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).checkAppInstalled ( v5, v6 ); // invoke-virtual {p0, v5, v6}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
/* if-nez v5, :cond_6 */
/* .line 413 */
/* .line 415 */
} // :cond_6
v5 = this.mKeyboardShortcutInfos;
/* .line 416 */
v5 = this.mReflectionUtil;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ) v5 ).invokeObject ( v4 ); // invoke-virtual {v5, v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->invokeObject(Landroid/view/KeyboardShortcutInfo;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
/* .line 417 */
/* .local v5, "shortcutInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v6 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v5 ).getShortcutKeyCode ( ); // invoke-virtual {v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v7 */
(( android.util.LongSparseArray ) v6 ).append ( v7, v8, v5 ); // invoke-virtual {v6, v7, v8, v5}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V
/* .line 418 */
} // .end local v4 # "info":Landroid/view/KeyboardShortcutInfo;
} // .end local v5 # "shortcutInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
/* .line 419 */
} // :cond_7
return;
} // .end method
public void updateKeyboardShortcut ( android.view.KeyboardShortcutInfo p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "info" # Landroid/view/KeyboardShortcutInfo; */
/* .param p2, "type" # I */
/* .line 149 */
v0 = this.mReflectionUtil;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ) v0 ).invokeObject ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->invokeObject(Landroid/view/KeyboardShortcutInfo;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
/* .line 150 */
/* .local v0, "shortcutInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).loadShortCuts ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V
/* .line 151 */
/* packed-switch p2, :pswitch_data_0 */
/* .line 174 */
/* :pswitch_0 */
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).resetShortCut ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->resetShortCut(I)V
/* .line 175 */
com.android.server.input.KeyboardCombinationManagerStubImpl .getInstance ( );
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) v1 ).resetRule ( ); // invoke-virtual {v1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->resetRule()V
/* .line 176 */
/* .line 169 */
/* :pswitch_1 */
int v1 = 2; // const/4 v1, 0x2
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).resetShortCut ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->resetShortCut(I)V
/* .line 170 */
com.android.server.input.KeyboardCombinationManagerStubImpl .getInstance ( );
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) v1 ).resetRule ( ); // invoke-virtual {v1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->resetRule()V
/* .line 171 */
/* .line 164 */
/* :pswitch_2 */
v1 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).getShortcutKeyCode ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v2 */
(( android.util.LongSparseArray ) v1 ).delete ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/util/LongSparseArray;->delete(J)V
/* .line 165 */
com.android.server.input.KeyboardCombinationManagerStubImpl .getInstance ( );
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) v1 ).removeRule ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->removeRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
/* .line 166 */
/* .line 159 */
/* :pswitch_3 */
v1 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).getHistoryKeyCode ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getHistoryKeyCode()J
/* move-result-wide v2 */
(( android.util.LongSparseArray ) v1 ).put ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 160 */
com.android.server.input.KeyboardCombinationManagerStubImpl .getInstance ( );
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) v1 ).updateRule ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->updateRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
/* .line 161 */
/* .line 154 */
/* :pswitch_4 */
v1 = this.mShortcutInfos;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).getShortcutKeyCode ( ); // invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v2 */
(( android.util.LongSparseArray ) v1 ).put ( v2, v3, v0 ); // invoke-virtual {v1, v2, v3, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V
/* .line 155 */
com.android.server.input.KeyboardCombinationManagerStubImpl .getInstance ( );
(( com.android.server.input.KeyboardCombinationManagerStubImpl ) v1 ).addRule ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->addRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V
/* .line 156 */
/* nop */
/* .line 180 */
} // :goto_0
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).writeShortCuts ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->writeShortCuts()V
/* .line 181 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).loadShortCuts ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V
/* .line 182 */
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void writeShortCuts ( ) {
/* .locals 10 */
/* .line 338 */
final String v0 = "ShortcutInfos"; // const-string v0, "ShortcutInfos"
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 340 */
/* .local v1, "startTime":J */
v3 = this.mFile;
v3 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
final String v4 = "MiuiCustomizeShortCutUtils"; // const-string v4, "MiuiCustomizeShortCutUtils"
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 341 */
v3 = this.mBackupFile;
v3 = (( java.io.File ) v3 ).exists ( ); // invoke-virtual {v3}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_0 */
/* .line 342 */
v3 = this.mFile;
v5 = this.mBackupFile;
v3 = (( java.io.File ) v3 ).renameTo ( v5 ); // invoke-virtual {v3, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
/* if-nez v3, :cond_1 */
/* .line 343 */
final String v0 = "Unable to backup miui shortcut settings, current changes will be lost at reboot"; // const-string v0, "Unable to backup miui shortcut settings, current changes will be lost at reboot"
android.util.Slog .wtf ( v4,v0 );
/* .line 346 */
return;
/* .line 349 */
} // :cond_0
v3 = this.mFile;
(( java.io.File ) v3 ).delete ( ); // invoke-virtual {v3}, Ljava/io/File;->delete()Z
/* .line 350 */
final String v3 = "Preserving older settings backup"; // const-string v3, "Preserving older settings backup"
android.util.Slog .w ( v4,v3 );
/* .line 354 */
} // :cond_1
try { // :try_start_0
/* new-instance v3, Ljava/io/FileOutputStream; */
v5 = this.mFile;
/* invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 355 */
/* .local v3, "fstr":Ljava/io/FileOutputStream; */
try { // :try_start_1
/* new-instance v5, Ljava/io/BufferedOutputStream; */
/* invoke-direct {v5, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 357 */
/* .local v5, "out":Ljava/io/BufferedOutputStream; */
try { // :try_start_2
/* new-instance v6, Lcom/android/internal/util/FastXmlSerializer; */
/* invoke-direct {v6}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V */
/* .line 358 */
/* .local v6, "serializer":Lorg/xmlpull/v1/XmlSerializer; */
v7 = java.nio.charset.StandardCharsets.UTF_8;
(( java.nio.charset.Charset ) v7 ).name ( ); // invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;
/* .line 359 */
int v7 = 1; // const/4 v7, 0x1
java.lang.Boolean .valueOf ( v7 );
int v9 = 0; // const/4 v9, 0x0
/* .line 360 */
final String v8 = "http://xmlpull.org/v1/doc/features.html#indent-output"; // const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"
/* .line 362 */
/* .line 364 */
int v7 = 0; // const/4 v7, 0x0
/* .local v7, "i":I */
} // :goto_0
v8 = this.mShortcutInfos;
v8 = (( android.util.LongSparseArray ) v8 ).size ( ); // invoke-virtual {v8}, Landroid/util/LongSparseArray;->size()I
/* if-ge v7, v8, :cond_2 */
/* .line 365 */
v8 = this.mShortcutInfos;
(( android.util.LongSparseArray ) v8 ).valueAt ( v7 ); // invoke-virtual {v8, v7}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;
/* check-cast v8, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils ) p0 ).writeToParser ( v8, v6 ); // invoke-virtual {p0, v8, v6}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->writeToParser(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;Lorg/xmlpull/v1/XmlSerializer;)V
/* .line 364 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 368 */
} // .end local v7 # "i":I
} // :cond_2
/* .line 369 */
/* .line 371 */
(( java.io.BufferedOutputStream ) v5 ).flush ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V
/* .line 372 */
android.os.FileUtils .sync ( v3 );
/* .line 373 */
(( java.io.BufferedOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
/* .line 375 */
v0 = this.mBackupFile;
(( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* .line 376 */
final String v0 = "miuishortcuts"; // const-string v0, "miuishortcuts"
/* .line 377 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v7 */
/* sub-long/2addr v7, v1 */
/* .line 376 */
com.android.internal.logging.EventLogTags .writeCommitSysConfigFile ( v0,v7,v8 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 379 */
try { // :try_start_3
(( java.io.BufferedOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
try { // :try_start_4
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 378 */
return;
/* .line 354 */
} // .end local v6 # "serializer":Lorg/xmlpull/v1/XmlSerializer;
/* :catchall_0 */
/* move-exception v0 */
try { // :try_start_5
(( java.io.BufferedOutputStream ) v5 ).close ( ); // invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v6 */
try { // :try_start_6
(( java.lang.Throwable ) v0 ).addSuppressed ( v6 ); // invoke-virtual {v0, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "startTime":J
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
} // .end local p0 # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
} // :goto_1
/* throw v0 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
} // .end local v5 # "out":Ljava/io/BufferedOutputStream;
/* .restart local v1 # "startTime":J */
/* .restart local v3 # "fstr":Ljava/io/FileOutputStream; */
/* .restart local p0 # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* :catchall_2 */
/* move-exception v0 */
try { // :try_start_7
(( java.io.FileOutputStream ) v3 ).close ( ); // invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* :catchall_3 */
/* move-exception v5 */
try { // :try_start_8
(( java.lang.Throwable ) v0 ).addSuppressed ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "startTime":J
} // .end local p0 # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
} // :goto_2
/* throw v0 */
/* :try_end_8 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_0 */
/* .line 379 */
} // .end local v3 # "fstr":Ljava/io/FileOutputStream;
/* .restart local v1 # "startTime":J */
/* .restart local p0 # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* :catch_0 */
/* move-exception v0 */
/* .line 380 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v3 = "Unable to write miui shortcut settings, current changes will be lost at reboot"; // const-string v3, "Unable to write miui shortcut settings, current changes will be lost at reboot"
android.util.Slog .wtf ( v4,v3,v0 );
/* .line 385 */
} // .end local v0 # "e":Ljava/lang/Exception;
v0 = this.mFile;
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mFile;
v0 = (( java.io.File ) v0 ).delete ( ); // invoke-virtual {v0}, Ljava/io/File;->delete()Z
/* if-nez v0, :cond_3 */
/* .line 386 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to clean up mangled file: "; // const-string v3, "Failed to clean up mangled file: "
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mFile;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .wtf ( v4,v0 );
/* .line 389 */
} // :cond_3
return;
} // .end method
public void writeToParser ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0, org.xmlpull.v1.XmlSerializer p1 ) {
/* .locals 12 */
/* .param p1, "info" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .param p2, "out" # Lorg/xmlpull/v1/XmlSerializer; */
/* .line 515 */
final String v0 = "ShortcutInfo"; // const-string v0, "ShortcutInfo"
final String v1 = ""; // const-string v1, ""
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* .line 517 */
final String v3 = "meta"; // const-string v3, "meta"
/* .line 519 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v4 */
/* const/16 v6, 0x20 */
/* shr-long/2addr v4, v6 */
/* const-wide/32 v7, 0x10000 */
/* and-long/2addr v4, v7 */
/* cmp-long v4, v4, v7 */
int v5 = 1; // const/4 v5, 0x1
int v7 = 0; // const/4 v7, 0x0
/* if-nez v4, :cond_0 */
/* move v4, v5 */
} // :cond_0
/* move v4, v7 */
/* .line 518 */
} // :goto_0
java.lang.String .valueOf ( v4 );
/* .line 517 */
/* .line 521 */
/* const-string/jumbo v3, "shift" */
/* .line 523 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v8 */
/* shr-long/2addr v8, v6 */
/* const-wide/16 v10, 0x1 */
/* and-long/2addr v8, v10 */
/* cmp-long v4, v8, v10 */
/* if-nez v4, :cond_1 */
/* move v4, v5 */
} // :cond_1
/* move v4, v7 */
/* .line 522 */
} // :goto_1
java.lang.String .valueOf ( v4 );
/* .line 521 */
/* .line 525 */
final String v3 = "alt"; // const-string v3, "alt"
/* .line 527 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v8 */
/* shr-long/2addr v8, v6 */
/* const-wide/16 v10, 0x2 */
/* and-long/2addr v8, v10 */
/* cmp-long v4, v8, v10 */
/* if-nez v4, :cond_2 */
/* move v4, v5 */
} // :cond_2
/* move v4, v7 */
/* .line 526 */
} // :goto_2
java.lang.String .valueOf ( v4 );
/* .line 525 */
/* .line 529 */
final String v3 = "ctrl"; // const-string v3, "ctrl"
/* .line 531 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v8 */
/* shr-long/2addr v8, v6 */
/* const-wide/16 v10, 0x1000 */
/* and-long/2addr v8, v10 */
/* cmp-long v4, v8, v10 */
/* if-nez v4, :cond_3 */
/* move v4, v5 */
} // :cond_3
/* move v4, v7 */
/* .line 530 */
} // :goto_3
java.lang.String .valueOf ( v4 );
/* .line 529 */
/* .line 533 */
final String v3 = "ralt"; // const-string v3, "ralt"
/* .line 535 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v8 */
/* shr-long/2addr v8, v6 */
/* const-wide/16 v10, 0x20 */
/* and-long/2addr v8, v10 */
/* cmp-long v4, v8, v10 */
/* if-nez v4, :cond_4 */
/* move v4, v5 */
} // :cond_4
/* move v4, v7 */
/* .line 534 */
} // :goto_4
java.lang.String .valueOf ( v4 );
/* .line 533 */
/* .line 537 */
final String v3 = "lalt"; // const-string v3, "lalt"
/* .line 539 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v8 */
/* shr-long/2addr v8, v6 */
/* const-wide/16 v10, 0x10 */
/* and-long/2addr v8, v10 */
/* cmp-long v4, v8, v10 */
/* if-nez v4, :cond_5 */
} // :cond_5
/* move v5, v7 */
/* .line 538 */
} // :goto_5
java.lang.String .valueOf ( v5 );
/* .line 537 */
/* .line 541 */
final String v3 = "keycode"; // const-string v3, "keycode"
/* .line 542 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v4 */
/* const-wide/32 v6, 0xffff */
/* and-long/2addr v4, v6 */
java.lang.String .valueOf ( v4,v5 );
/* .line 541 */
/* .line 544 */
final String v3 = "enable"; // const-string v3, "enable"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).isEnable ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 545 */
/* const-string/jumbo v3, "type" */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 546 */
v3 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v3, :cond_6 */
/* .line 547 */
final String v3 = "packageName"; // const-string v3, "packageName"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 548 */
final String v3 = "className"; // const-string v3, "className"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getClassName ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getClassName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 550 */
} // :cond_6
final String v3 = "customized"; // const-string v3, "customized"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).isCustomized ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isCustomized()Z
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 551 */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 554 */
/* .line 552 */
/* :catch_0 */
/* move-exception v0 */
/* .line 553 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiCustomizeShortCutUtils"; // const-string v1, "MiuiCustomizeShortCutUtils"
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .d ( v1,v2 );
/* .line 555 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_6
return;
} // .end method
