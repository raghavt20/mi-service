public class com.miui.server.input.util.AutoDisableScreenButtonsHelper {
	 /* .source "AutoDisableScreenButtonsHelper.java" */
	 /* # static fields */
	 public static final java.lang.String CLOUD_SETTING;
	 public static final Integer ENABLE_ASK;
	 public static final Integer ENABLE_AUTO;
	 public static final java.lang.String MODULE_AUTO_DIS_NAV_BTN;
	 public static final Integer NO;
	 public static final Integer NONE;
	 private static final java.lang.String TAG;
	 private static org.json.JSONObject mCloudJson;
	 private static org.json.JSONObject mUserJson;
	 /* # direct methods */
	 public com.miui.server.input.util.AutoDisableScreenButtonsHelper ( ) {
		 /* .locals 0 */
		 /* .line 15 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 private static void checkJson ( android.content.Context p0 ) {
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 106 */
		 /* if-nez p0, :cond_0 */
		 /* .line 107 */
		 return;
		 /* .line 109 */
	 } // :cond_0
	 v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mUserJson;
	 /* if-nez v0, :cond_2 */
	 /* .line 110 */
	 (( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 /* .line 112 */
	 v1 = 	 miui.securityspace.CrossUserUtils .getCurrentUserId ( );
	 /* .line 110 */
	 final String v2 = "auto_disable_screen_button"; // const-string v2, "auto_disable_screen_button"
	 android.provider.MiuiSettings$System .getStringForUser ( v0,v2,v1 );
	 /* .line 113 */
	 /* .local v0, "userSetting":Ljava/lang/String; */
	 /* if-nez v0, :cond_1 */
	 /* .line 114 */
	 /* new-instance v1, Lorg/json/JSONObject; */
	 /* invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V */
	 /* .line 116 */
} // :cond_1
com.miui.server.input.util.AutoDisableScreenButtonsHelper .updateUserJson ( v0 );
/* .line 119 */
} // .end local v0 # "userSetting":Ljava/lang/String;
} // :cond_2
} // :goto_0
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mCloudJson;
/* if-nez v0, :cond_3 */
/* .line 120 */
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "auto_disable_screen_button_cloud_setting"; // const-string v1, "auto_disable_screen_button_cloud_setting"
android.provider.Settings$System .getString ( v0,v1 );
/* .line 122 */
/* .local v0, "cloudConfig":Ljava/lang/String; */
com.miui.server.input.util.AutoDisableScreenButtonsHelper .updateCloudJson ( v0 );
/* .line 124 */
} // .end local v0 # "cloudConfig":Ljava/lang/String;
} // :cond_3
return;
} // .end method
public static Integer getAppFlag ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 41 */
com.miui.server.input.util.AutoDisableScreenButtonsHelper .getValue ( p0,p1 );
/* .line 42 */
/* .local v0, "flagObj":Ljava/lang/Object; */
/* if-nez v0, :cond_0 */
int v1 = 3; // const/4 v1, 0x3
} // :cond_0
/* move-object v1, v0 */
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* .line 43 */
/* .local v1, "flag":I */
} // :goto_0
} // .end method
public static java.lang.Object getValue ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "key" # Ljava/lang/String; */
/* .line 47 */
com.miui.server.input.util.AutoDisableScreenButtonsHelper .checkJson ( p0 );
/* .line 49 */
try { // :try_start_0
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mUserJson;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( org.json.JSONObject ) v0 ).has ( p1 ); // invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 50 */
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mUserJson;
(( org.json.JSONObject ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
/* .line 51 */
} // :cond_0
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mCloudJson;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( org.json.JSONObject ) v0 ).has ( p1 ); // invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 52 */
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mCloudJson;
(( org.json.JSONObject ) v0 ).get ( p1 ); // invoke-virtual {v0, p1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 56 */
} // :cond_1
/* .line 54 */
/* :catch_0 */
/* move-exception v0 */
/* .line 55 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 57 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static void setFlag ( android.content.Context p0, java.lang.String p1, Integer p2 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "flag" # I */
/* .line 62 */
java.lang.Integer .valueOf ( p2 );
com.miui.server.input.util.AutoDisableScreenButtonsHelper .setValue ( p0,p1,v0 );
/* .line 63 */
return;
} // .end method
public static void setValue ( android.content.Context p0, java.lang.String p1, java.lang.Object p2 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "key" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/Object; */
/* .line 66 */
com.miui.server.input.util.AutoDisableScreenButtonsHelper .checkJson ( p0 );
/* .line 67 */
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mUserJson;
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p0 != null) { // if-eqz p0, :cond_0
/* .line 69 */
try { // :try_start_0
(( org.json.JSONObject ) v0 ).put ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 72 */
/* .line 70 */
/* :catch_0 */
/* move-exception v0 */
/* .line 71 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 73 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_0
(( android.content.Context ) p0 ).getContentResolver ( ); // invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
v1 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mUserJson;
/* .line 75 */
(( org.json.JSONObject ) v1 ).toString ( ); // invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
v2 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* .line 73 */
final String v3 = "auto_disable_screen_button"; // const-string v3, "auto_disable_screen_button"
android.provider.MiuiSettings$System .putStringForUser ( v0,v3,v1,v2 );
/* .line 77 */
} // :cond_0
return;
} // .end method
public static void updateCloudJson ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "config" # Ljava/lang/String; */
/* .line 92 */
v0 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v0, :cond_1 */
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mCloudJson;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 93 */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 97 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 100 */
/* .line 98 */
/* :catch_0 */
/* move-exception v0 */
/* .line 99 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 101 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_0
return;
/* .line 94 */
} // :cond_1
} // :goto_1
return;
} // .end method
public static void updateUserJson ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p0, "config" # Ljava/lang/String; */
/* .line 80 */
v0 = android.text.TextUtils .isEmpty ( p0 );
/* if-nez v0, :cond_1 */
v0 = com.miui.server.input.util.AutoDisableScreenButtonsHelper.mUserJson;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 81 */
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
v0 = (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 85 */
} // :cond_0
try { // :try_start_0
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 88 */
/* .line 86 */
/* :catch_0 */
/* move-exception v0 */
/* .line 87 */
/* .local v0, "e":Lorg/json/JSONException; */
(( org.json.JSONException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V
/* .line 89 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_0
return;
/* .line 82 */
} // :cond_1
} // :goto_1
return;
} // .end method
