class com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil {
	 /* .source "MiuiCustomizeShortCutUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ReflectionUtil" */
} // .end annotation
/* # instance fields */
private java.lang.reflect.Method mGetClassName;
private java.lang.reflect.Method mGetHistoryKeyCode;
private java.lang.reflect.Method mGetPackageName;
private java.lang.reflect.Method mGetShortcutKeyCode;
private java.lang.reflect.Method mGetType;
private java.lang.reflect.Method mIsCustomized;
private java.lang.reflect.Method mIsEnable;
private java.lang.reflect.Method mSetAppInfo;
private java.lang.reflect.Method mSetCustomized;
private java.lang.reflect.Method mSetEnable;
private java.lang.reflect.Method mSetShortcutKeyCode;
private java.lang.reflect.Method mSetType;
final com.miui.server.input.util.MiuiCustomizeShortCutUtils this$0; //synthetic
/* # direct methods */
 com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* .line 680 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 681 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$ReflectionUtil ) p0 ).init ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->init()V
/* .line 682 */
return;
} // .end method
private java.lang.String getAppName ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 762 */
final String v0 = ""; // const-string v0, ""
v1 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v1 );
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 764 */
/* .local v1, "packageManager":Landroid/content/pm/PackageManager; */
/* const/16 v2, 0x80 */
try { // :try_start_0
	 (( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
	 /* .line 766 */
	 /* .local v2, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
	 /* if-nez v2, :cond_0 */
	 /* .line 767 */
	 /* .line 769 */
} // :cond_0
(( android.content.pm.PackageManager ) v1 ).getApplicationLabel ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
/* check-cast v3, Ljava/lang/String; */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 770 */
} // .end local v2 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
/* :catch_0 */
/* move-exception v2 */
/* .line 771 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 773 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // .end method
private java.lang.String getSystemLabel ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 2 */
/* .param p1, "rawInfo" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 778 */
v0 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 806 */
int v0 = 0; // const/4 v0, 0x0
/* .line 802 */
/* :pswitch_0 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f025b */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 800 */
/* :pswitch_1 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f025a */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 798 */
/* :pswitch_2 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0255 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 796 */
/* :pswitch_3 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0256 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 794 */
/* :pswitch_4 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0254 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 790 */
/* :pswitch_5 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f025d */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 792 */
/* :pswitch_6 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f025e */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 788 */
/* :pswitch_7 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f025c */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 786 */
/* :pswitch_8 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0253 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 784 */
/* :pswitch_9 */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0259 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 782 */
/* :pswitch_a */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0258 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 780 */
/* :pswitch_b */
v0 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f0257 */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_b */
/* :pswitch_a */
/* :pswitch_9 */
/* :pswitch_8 */
/* :pswitch_7 */
/* :pswitch_6 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private android.view.KeyboardShortcutInfo transformKeycode ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 8 */
/* .param p1, "rawInfo" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 738 */
int v0 = 0; // const/4 v0, 0x0
/* .line 739 */
/* .local v0, "metaStaus":I */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v1 */
/* const-wide/32 v3, 0xffff */
/* and-long/2addr v1, v3 */
/* long-to-int v1, v1 */
/* .line 740 */
/* .local v1, "keyCode":I */
int v2 = 0; // const/4 v2, 0x0
/* .line 741 */
/* .local v2, "label":Ljava/lang/String; */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v3 */
/* const/16 v5, 0x20 */
/* shr-long/2addr v3, v5 */
/* const-wide/32 v6, 0x10000 */
/* and-long/2addr v3, v6 */
/* cmp-long v3, v3, v6 */
/* if-nez v3, :cond_0 */
/* .line 742 */
/* const/high16 v3, 0x10000 */
/* or-int/2addr v0, v3 */
/* .line 744 */
} // :cond_0
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v3 */
/* shr-long/2addr v3, v5 */
/* const-wide/16 v6, 0x1 */
/* and-long/2addr v3, v6 */
/* cmp-long v3, v3, v6 */
/* if-nez v3, :cond_1 */
/* .line 745 */
/* or-int/lit8 v0, v0, 0x1 */
/* .line 747 */
} // :cond_1
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v3 */
/* shr-long/2addr v3, v5 */
/* const-wide/16 v6, 0x2 */
/* and-long/2addr v3, v6 */
/* cmp-long v3, v3, v6 */
/* if-nez v3, :cond_2 */
/* .line 748 */
/* or-int/lit8 v0, v0, 0x2 */
/* .line 750 */
} // :cond_2
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getShortcutKeyCode ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J
/* move-result-wide v3 */
/* shr-long/2addr v3, v5 */
/* const-wide/16 v5, 0x1000 */
/* and-long/2addr v3, v5 */
/* cmp-long v3, v3, v5 */
/* if-nez v3, :cond_3 */
/* .line 751 */
/* or-int/lit16 v0, v0, 0x1000 */
/* .line 753 */
} // :cond_3
v3 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v3, :cond_4 */
/* .line 754 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;
/* invoke-direct {p0, v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->getAppName(Ljava/lang/String;)Ljava/lang/String; */
/* .line 756 */
} // :cond_4
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->getSystemLabel(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String; */
/* .line 758 */
} // :goto_0
/* new-instance v3, Landroid/view/KeyboardShortcutInfo; */
/* invoke-direct {v3, v2, v1, v0}, Landroid/view/KeyboardShortcutInfo;-><init>(Ljava/lang/CharSequence;II)V */
} // .end method
/* # virtual methods */
public void init ( ) {
/* .locals 6 */
/* .line 686 */
try { // :try_start_0
/* const-class v0, Landroid/view/KeyboardShortcutInfo; */
/* .line 687 */
/* .local v0, "infoClass":Ljava/lang/Class; */
final String v1 = "getShortcutKeyCode"; // const-string v1, "getShortcutKeyCode"
int v2 = 0; // const/4 v2, 0x0
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetShortcutKeyCode = v1;
/* .line 688 */
final String v1 = "isActive"; // const-string v1, "isActive"
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mIsEnable = v1;
/* .line 689 */
final String v1 = "getType"; // const-string v1, "getType"
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetType = v1;
/* .line 690 */
final String v1 = "getPackageName"; // const-string v1, "getPackageName"
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetPackageName = v1;
/* .line 691 */
final String v1 = "getClassName"; // const-string v1, "getClassName"
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetClassName = v1;
/* .line 692 */
final String v1 = "getHistoryKeyCode"; // const-string v1, "getHistoryKeyCode"
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mGetHistoryKeyCode = v1;
/* .line 693 */
final String v1 = "isCustomized"; // const-string v1, "isCustomized"
/* new-array v3, v2, [Ljava/lang/Class; */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mIsCustomized = v1;
/* .line 695 */
/* const-string/jumbo v1, "setShortcutKeyCode" */
int v3 = 1; // const/4 v3, 0x1
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Long.TYPE;
/* aput-object v5, v4, v2 */
(( java.lang.Class ) v0 ).getMethod ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mSetShortcutKeyCode = v1;
/* .line 696 */
/* const-string/jumbo v1, "setActive" */
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Boolean.TYPE;
/* aput-object v5, v4, v2 */
(( java.lang.Class ) v0 ).getMethod ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mSetEnable = v1;
/* .line 697 */
/* const-string/jumbo v1, "setType" */
/* new-array v4, v3, [Ljava/lang/Class; */
v5 = java.lang.Integer.TYPE;
/* aput-object v5, v4, v2 */
(( java.lang.Class ) v0 ).getMethod ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mSetType = v1;
/* .line 698 */
/* const-string/jumbo v1, "setAppInfo" */
int v4 = 2; // const/4 v4, 0x2
/* new-array v4, v4, [Ljava/lang/Class; */
/* const-class v5, Ljava/lang/String; */
/* aput-object v5, v4, v2 */
/* const-class v5, Ljava/lang/String; */
/* aput-object v5, v4, v3 */
(( java.lang.Class ) v0 ).getMethod ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mSetAppInfo = v1;
/* .line 699 */
/* const-string/jumbo v1, "setCustomized" */
/* new-array v3, v3, [Ljava/lang/Class; */
v4 = java.lang.Boolean.TYPE;
/* aput-object v4, v3, v2 */
(( java.lang.Class ) v0 ).getMethod ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
this.mSetCustomized = v1;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 702 */
} // .end local v0 # "infoClass":Ljava/lang/Class;
/* .line 700 */
/* :catch_0 */
/* move-exception v0 */
/* .line 701 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "MiuiCustomizeShortCutUtils"; // const-string v1, "MiuiCustomizeShortCutUtils"
(( java.lang.Exception ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 703 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
public com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo invokeObject ( android.view.KeyboardShortcutInfo p0 ) {
/* .locals 10 */
/* .param p1, "rawInfo" # Landroid/view/KeyboardShortcutInfo; */
/* .line 706 */
int v0 = 0; // const/4 v0, 0x0
/* .line 708 */
/* .local v0, "resultInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
try { // :try_start_0
/* new-instance v8, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
v2 = this.this$0;
v1 = this.mGetShortcutKeyCode;
int v9 = 0; // const/4 v9, 0x0
/* new-array v3, v9, [Ljava/lang/Object; */
/* .line 709 */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v3 ); // invoke-virtual {v1, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v3 */
v1 = this.mIsEnable;
/* new-array v5, v9, [Ljava/lang/Object; */
/* .line 710 */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v5 ); // invoke-virtual {v1, p1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Boolean; */
v5 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
v1 = this.mGetType;
/* new-array v6, v9, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v6 ); // invoke-virtual {v1, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v6 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
v1 = this.mIsCustomized;
/* new-array v7, v9, [Ljava/lang/Object; */
/* .line 711 */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v7 ); // invoke-virtual {v1, p1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Boolean; */
v7 = (( java.lang.Boolean ) v1 ).booleanValue ( ); // invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
/* move-object v1, v8 */
/* invoke-direct/range {v1 ..v7}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;JZIZ)V */
/* move-object v0, v8 */
/* .line 712 */
v1 = this.mGetPackageName;
/* new-array v2, v9, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
v2 = this.mGetClassName;
/* new-array v3, v9, [Ljava/lang/Object; */
/* .line 713 */
(( java.lang.reflect.Method ) v2 ).invoke ( p1, v3 ); // invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/String; */
/* .line 712 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).setAppInfo ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->setAppInfo(Ljava/lang/String;Ljava/lang/String;)V
/* .line 714 */
v1 = this.mGetHistoryKeyCode;
/* new-array v2, v9, [Ljava/lang/Object; */
(( java.lang.reflect.Method ) v1 ).invoke ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Long; */
(( java.lang.Long ) v1 ).longValue ( ); // invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
/* move-result-wide v1 */
(( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) v0 ).setHistoryKeyCode ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->setHistoryKeyCode(J)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 717 */
/* .line 715 */
/* :catch_0 */
/* move-exception v1 */
/* .line 716 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiCustomizeShortCutUtils"; // const-string v2, "MiuiCustomizeShortCutUtils"
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 718 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
public android.view.KeyboardShortcutInfo reflectObject ( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo p0 ) {
/* .locals 6 */
/* .param p1, "rawInfo" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo; */
/* .line 722 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->transformKeycode(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo; */
/* .line 724 */
/* .local v0, "resultInfo":Landroid/view/KeyboardShortcutInfo; */
try { // :try_start_0
v1 = this.mSetShortcutKeyCode;
int v2 = 1; // const/4 v2, 0x1
/* new-array v3, v2, [Ljava/lang/Object; */
com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo .-$$Nest$fgetmShortcutKeyCode ( p1 );
/* move-result-wide v4 */
java.lang.Long .valueOf ( v4,v5 );
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 725 */
v1 = this.mSetEnable;
/* new-array v3, v2, [Ljava/lang/Object; */
v4 = com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo .-$$Nest$fgetmEnable ( p1 );
java.lang.Boolean .valueOf ( v4 );
/* aput-object v4, v3, v5 */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 726 */
v1 = this.mSetType;
/* new-array v3, v2, [Ljava/lang/Object; */
v4 = com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo .-$$Nest$fgetmType ( p1 );
java.lang.Integer .valueOf ( v4 );
/* aput-object v4, v3, v5 */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 727 */
v1 = (( com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo ) p1 ).getType ( ); // invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I
/* if-nez v1, :cond_0 */
/* .line 728 */
v1 = this.mSetAppInfo;
int v3 = 2; // const/4 v3, 0x2
/* new-array v3, v3, [Ljava/lang/Object; */
com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo .-$$Nest$fgetmPackageName ( p1 );
/* aput-object v4, v3, v5 */
com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo .-$$Nest$fgetmClassName ( p1 );
/* aput-object v4, v3, v2 */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* .line 730 */
} // :cond_0
v1 = this.mSetCustomized;
/* new-array v2, v2, [Ljava/lang/Object; */
v3 = com.miui.server.input.util.MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo .-$$Nest$fgetmCustomized ( p1 );
java.lang.Boolean .valueOf ( v3 );
/* aput-object v3, v2, v5 */
(( java.lang.reflect.Method ) v1 ).invoke ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 733 */
/* .line 731 */
/* :catch_0 */
/* move-exception v1 */
/* .line 732 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "MiuiCustomizeShortCutUtils"; // const-string v2, "MiuiCustomizeShortCutUtils"
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v3 );
/* .line 734 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
