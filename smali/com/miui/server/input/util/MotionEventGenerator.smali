.class public final Lcom/miui/server/input/util/MotionEventGenerator;
.super Ljava/lang/Object;
.source "MotionEventGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;
    }
.end annotation


# static fields
.field private static final DEFAULT_DELAY_MILLIS:I = 0x5


# instance fields
.field private mAllStep:I

.field private mCurrentStep:I

.field private mDoubleTapDuration:I

.field private final mDoubleTapRunnable:Ljava/lang/Runnable;

.field private mDownTime:J

.field private mEndX:F

.field private mEndY:F

.field private mEveryDelayTime:J

.field private mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

.field private final mHandler:Landroid/os/Handler;

.field private volatile mIsRunning:Z

.field private final mLock:Ljava/lang/Object;

.field private mStartX:F

.field private mStartY:F

.field private final mSwipeRunnable:Ljava/lang/Runnable;

.field private final mTapRunnable:Ljava/lang/Runnable;

.field private mTapX:F

.field private mTapY:F


# direct methods
.method public static synthetic $r8$lambda$BP4pdwsKAyJkflulJD49VOLDnm8(Lcom/miui/server/input/util/MotionEventGenerator;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->sendSwipe()V

    return-void
.end method

.method public static synthetic $r8$lambda$Oj_tuhP_r3F3KjiirSKXr7ds-T0(Lcom/miui/server/input/util/MotionEventGenerator;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->sendDoubleTap()V

    return-void
.end method

.method public static synthetic $r8$lambda$t-wYhD6k6IN8nEz2ufABxn2x9Sw(Lcom/miui/server/input/util/MotionEventGenerator;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->sendTap()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2
    .param p1, "looper"    # Landroid/os/Looper;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Lcom/miui/server/input/util/MotionEventGenerator$$ExternalSyntheticLambda0;

    invoke-direct {v0, p0}, Lcom/miui/server/input/util/MotionEventGenerator$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MotionEventGenerator;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mSwipeRunnable:Ljava/lang/Runnable;

    .line 15
    new-instance v0, Lcom/miui/server/input/util/MotionEventGenerator$$ExternalSyntheticLambda1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/util/MotionEventGenerator$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/util/MotionEventGenerator;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapRunnable:Ljava/lang/Runnable;

    .line 16
    new-instance v0, Lcom/miui/server/input/util/MotionEventGenerator$$ExternalSyntheticLambda2;

    invoke-direct {v0, p0}, Lcom/miui/server/input/util/MotionEventGenerator$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/util/MotionEventGenerator;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDoubleTapRunnable:Ljava/lang/Runnable;

    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mLock:Ljava/lang/Object;

    .line 34
    new-instance v0, Landroid/os/Handler;

    if-nez p1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method private generateTapMotionEvent()V
    .locals 12

    .line 82
    invoke-static {}, Lcom/miui/server/input/util/MotionEventGenerator;->now()J

    move-result-wide v8

    .line 83
    .local v8, "currentTime":J
    const/4 v4, 0x0

    iget v5, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapX:F

    iget v6, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapY:F

    const/4 v7, 0x0

    move-wide v0, v8

    move-wide v2, v8

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v10

    .line 85
    .local v10, "down":Landroid/view/MotionEvent;
    const/16 v11, 0x1002

    invoke-virtual {v10, v11}, Landroid/view/MotionEvent;->setSource(I)V

    .line 86
    const/4 v4, 0x1

    iget v5, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapX:F

    iget v6, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapY:F

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 88
    .local v0, "up":Landroid/view/MotionEvent;
    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->setSource(I)V

    .line 89
    iget-object v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    invoke-interface {v1, v10}, Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;->onMotionEvent(Landroid/view/MotionEvent;)V

    .line 90
    iget-object v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    invoke-interface {v1, v0}, Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;->onMotionEvent(Landroid/view/MotionEvent;)V

    .line 91
    return-void
.end method

.method private getRunningStatus()Z
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 168
    :try_start_0
    iget-boolean v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mIsRunning:Z

    monitor-exit v0

    return v1

    .line 169
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static now()J
    .locals 2

    .line 173
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private sendDoubleTap()V
    .locals 4

    .line 77
    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->generateTapMotionEvent()V

    .line 78
    iget-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapRunnable:Ljava/lang/Runnable;

    iget v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDoubleTapDuration:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 79
    return-void
.end method

.method private sendSwipe()V
    .locals 12

    .line 44
    iget v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mCurrentStep:I

    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "currentAction":I
    invoke-static {}, Lcom/miui/server/input/util/MotionEventGenerator;->now()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDownTime:J

    .line 47
    iget v6, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mStartX:F

    iget v7, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mStartY:F

    const/4 v8, 0x0

    move-wide v1, v3

    move v5, v0

    invoke-static/range {v1 .. v8}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v1

    .local v1, "motionEvent":Landroid/view/MotionEvent;
    goto :goto_0

    .line 49
    .end local v0    # "currentAction":I
    .end local v1    # "motionEvent":Landroid/view/MotionEvent;
    :cond_0
    iget v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mAllStep:I

    if-lt v0, v1, :cond_1

    .line 50
    const/4 v0, 0x1

    .line 51
    .restart local v0    # "currentAction":I
    iget-wide v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDownTime:J

    invoke-static {}, Lcom/miui/server/input/util/MotionEventGenerator;->now()J

    move-result-wide v4

    iget v7, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEndX:F

    iget v8, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEndY:F

    const/4 v9, 0x0

    move v6, v0

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v1

    .restart local v1    # "motionEvent":Landroid/view/MotionEvent;
    goto :goto_0

    .line 54
    .end local v0    # "currentAction":I
    .end local v1    # "motionEvent":Landroid/view/MotionEvent;
    :cond_1
    const/4 v10, 0x2

    .line 55
    .local v10, "currentAction":I
    iget v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mStartX:F

    iget v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEndX:F

    sub-float/2addr v3, v2

    int-to-float v4, v1

    div-float/2addr v3, v4

    int-to-float v4, v0

    mul-float/2addr v3, v4

    add-float v11, v2, v3

    .line 56
    .local v11, "x":F
    iget v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mStartY:F

    iget v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEndY:F

    sub-float/2addr v3, v2

    int-to-float v1, v1

    div-float/2addr v3, v1

    int-to-float v0, v0

    mul-float/2addr v3, v0

    add-float v0, v2, v3

    .line 57
    .local v0, "y":F
    iget-wide v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDownTime:J

    invoke-static {}, Lcom/miui/server/input/util/MotionEventGenerator;->now()J

    move-result-wide v4

    const/4 v9, 0x0

    move v6, v10

    move v7, v11

    move v8, v0

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v1

    move v0, v10

    .line 60
    .end local v10    # "currentAction":I
    .end local v11    # "x":F
    .local v0, "currentAction":I
    .restart local v1    # "motionEvent":Landroid/view/MotionEvent;
    :goto_0
    const/16 v2, 0x1002

    invoke-virtual {v1, v2}, Landroid/view/MotionEvent;->setSource(I)V

    .line 61
    iget-object v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    invoke-interface {v2, v1}, Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;->onMotionEvent(Landroid/view/MotionEvent;)V

    .line 62
    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 64
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/miui/server/input/util/MotionEventGenerator;->setRunningStatus(Z)V

    .line 65
    return-void

    .line 67
    :cond_2
    iget v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mCurrentStep:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mCurrentStep:I

    .line 68
    iget-object v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mSwipeRunnable:Ljava/lang/Runnable;

    iget-wide v4, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEveryDelayTime:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 69
    return-void
.end method

.method private sendTap()V
    .locals 1

    .line 72
    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->generateTapMotionEvent()V

    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/input/util/MotionEventGenerator;->setRunningStatus(Z)V

    .line 74
    return-void
.end method

.method private setRunningStatus(Z)V
    .locals 2
    .param p1, "running"    # Z

    .line 160
    iget-object v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 161
    :try_start_0
    iput-boolean p1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mIsRunning:Z

    .line 162
    monitor-exit v0

    .line 163
    return-void

    .line 162
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public generateDoubleTapMotionEvent(FFILcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "duration"    # I
    .param p4, "callback"    # Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 143
    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->getRunningStatus()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 144
    return v1

    .line 146
    :cond_0
    if-gez p3, :cond_1

    .line 147
    return v1

    .line 149
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/input/util/MotionEventGenerator;->setRunningStatus(Z)V

    .line 150
    iput p1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapX:F

    .line 151
    iput p2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapY:F

    .line 152
    iput p3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDoubleTapDuration:I

    .line 153
    iput-object p4, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 155
    iget-object v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDoubleTapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    return v0
.end method

.method public generateSwipeMotionEvent(FFFFIJLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z
    .locals 5
    .param p1, "startX"    # F
    .param p2, "startY"    # F
    .param p3, "endX"    # F
    .param p4, "endY"    # F
    .param p5, "duration"    # I
    .param p6, "everyDelayTime"    # J
    .param p8, "callback"    # Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 102
    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->getRunningStatus()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 103
    return v1

    .line 105
    :cond_0
    if-ltz p5, :cond_3

    const-wide/16 v2, 0x0

    cmp-long v0, p6, v2

    if-gez v0, :cond_1

    goto :goto_0

    .line 108
    :cond_1
    int-to-long v2, p5

    div-long/2addr v2, p6

    long-to-int v0, v2

    .line 109
    .local v0, "step":I
    const/4 v2, 0x1

    if-ge v0, v2, :cond_2

    .line 110
    return v1

    .line 112
    :cond_2
    invoke-direct {p0, v2}, Lcom/miui/server/input/util/MotionEventGenerator;->setRunningStatus(Z)V

    .line 113
    iput p1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mStartX:F

    .line 114
    iput p2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mStartY:F

    .line 115
    iput p3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEndX:F

    .line 116
    iput p4, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEndY:F

    .line 117
    iput v0, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mAllStep:I

    .line 118
    iput v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mCurrentStep:I

    .line 119
    iput-wide p6, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mEveryDelayTime:J

    .line 120
    iput-object p8, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 121
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mDownTime:J

    .line 123
    iget-object v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mSwipeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 124
    return v2

    .line 106
    .end local v0    # "step":I
    :cond_3
    :goto_0
    return v1
.end method

.method public generateSwipeMotionEvent(FFFFILcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z
    .locals 9
    .param p1, "startX"    # F
    .param p2, "startY"    # F
    .param p3, "endX"    # F
    .param p4, "endY"    # F
    .param p5, "duration"    # I
    .param p6, "callback"    # Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 95
    const-wide/16 v6, 0x5

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/miui/server/input/util/MotionEventGenerator;->generateSwipeMotionEvent(FFFFIJLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z

    move-result v0

    return v0
.end method

.method public generateTapMotionEvent(FFLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "callback"    # Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 129
    invoke-direct {p0}, Lcom/miui/server/input/util/MotionEventGenerator;->getRunningStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    const/4 v0, 0x0

    return v0

    .line 132
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/input/util/MotionEventGenerator;->setRunningStatus(Z)V

    .line 133
    iput p1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapX:F

    .line 134
    iput p2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapY:F

    .line 135
    iput-object p3, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mGenerateResultCallback:Lcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;

    .line 137
    iget-object v1, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/miui/server/input/util/MotionEventGenerator;->mTapRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 138
    return v0
.end method

.method public isGenerating()Z
    .locals 1

    .line 38
    const/4 v0, 0x0

    return v0
.end method
