.class Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;
.super Ljava/lang/Object;
.source "MiuiCustomizeShortCutUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ReflectionUtil"
.end annotation


# instance fields
.field private mGetClassName:Ljava/lang/reflect/Method;

.field private mGetHistoryKeyCode:Ljava/lang/reflect/Method;

.field private mGetPackageName:Ljava/lang/reflect/Method;

.field private mGetShortcutKeyCode:Ljava/lang/reflect/Method;

.field private mGetType:Ljava/lang/reflect/Method;

.field private mIsCustomized:Ljava/lang/reflect/Method;

.field private mIsEnable:Ljava/lang/reflect/Method;

.field private mSetAppInfo:Ljava/lang/reflect/Method;

.field private mSetCustomized:Ljava/lang/reflect/Method;

.field private mSetEnable:Ljava/lang/reflect/Method;

.field private mSetShortcutKeyCode:Ljava/lang/reflect/Method;

.field private mSetType:Ljava/lang/reflect/Method;

.field final synthetic this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;


# direct methods
.method constructor <init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 680
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 681
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->init()V

    .line 682
    return-void
.end method

.method private getAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .line 762
    const-string v0, ""

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 764
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 766
    .local v2, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    if-nez v2, :cond_0

    .line 767
    return-object v0

    .line 769
    :cond_0
    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    .line 770
    .end local v2    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v2

    .line 771
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 773
    .end local v2    # "e":Ljava/lang/Exception;
    return-object v0
.end method

.method private getSystemLabel(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;
    .locals 2
    .param p1, "rawInfo"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 778
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 806
    const/4 v0, 0x0

    return-object v0

    .line 802
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f025b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 800
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f025a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 798
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0255

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 796
    :pswitch_3
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0256

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 794
    :pswitch_4
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0254

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 790
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f025d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 792
    :pswitch_6
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f025e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 788
    :pswitch_7
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f025c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 786
    :pswitch_8
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0253

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 784
    :pswitch_9
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0259

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 782
    :pswitch_a
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0258

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 780
    :pswitch_b
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f0257

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private transformKeycode(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;
    .locals 8
    .param p1, "rawInfo"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 738
    const/4 v0, 0x0

    .line 739
    .local v0, "metaStaus":I
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v1

    const-wide/32 v3, 0xffff

    and-long/2addr v1, v3

    long-to-int v1, v1

    .line 740
    .local v1, "keyCode":I
    const/4 v2, 0x0

    .line 741
    .local v2, "label":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v3

    const/16 v5, 0x20

    shr-long/2addr v3, v5

    const-wide/32 v6, 0x10000

    and-long/2addr v3, v6

    cmp-long v3, v3, v6

    if-nez v3, :cond_0

    .line 742
    const/high16 v3, 0x10000

    or-int/2addr v0, v3

    .line 744
    :cond_0
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v3

    shr-long/2addr v3, v5

    const-wide/16 v6, 0x1

    and-long/2addr v3, v6

    cmp-long v3, v3, v6

    if-nez v3, :cond_1

    .line 745
    or-int/lit8 v0, v0, 0x1

    .line 747
    :cond_1
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v3

    shr-long/2addr v3, v5

    const-wide/16 v6, 0x2

    and-long/2addr v3, v6

    cmp-long v3, v3, v6

    if-nez v3, :cond_2

    .line 748
    or-int/lit8 v0, v0, 0x2

    .line 750
    :cond_2
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v3

    shr-long/2addr v3, v5

    const-wide/16 v5, 0x1000

    and-long/2addr v3, v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    .line 751
    or-int/lit16 v0, v0, 0x1000

    .line 753
    :cond_3
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v3

    if-nez v3, :cond_4

    .line 754
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 756
    :cond_4
    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->getSystemLabel(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;

    move-result-object v2

    .line 758
    :goto_0
    new-instance v3, Landroid/view/KeyboardShortcutInfo;

    invoke-direct {v3, v2, v1, v0}, Landroid/view/KeyboardShortcutInfo;-><init>(Ljava/lang/CharSequence;II)V

    return-object v3
.end method


# virtual methods
.method public init()V
    .locals 6

    .line 686
    :try_start_0
    const-class v0, Landroid/view/KeyboardShortcutInfo;

    .line 687
    .local v0, "infoClass":Ljava/lang/Class;
    const-string v1, "getShortcutKeyCode"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetShortcutKeyCode:Ljava/lang/reflect/Method;

    .line 688
    const-string v1, "isActive"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mIsEnable:Ljava/lang/reflect/Method;

    .line 689
    const-string v1, "getType"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetType:Ljava/lang/reflect/Method;

    .line 690
    const-string v1, "getPackageName"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetPackageName:Ljava/lang/reflect/Method;

    .line 691
    const-string v1, "getClassName"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetClassName:Ljava/lang/reflect/Method;

    .line 692
    const-string v1, "getHistoryKeyCode"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetHistoryKeyCode:Ljava/lang/reflect/Method;

    .line 693
    const-string v1, "isCustomized"

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mIsCustomized:Ljava/lang/reflect/Method;

    .line 695
    const-string/jumbo v1, "setShortcutKeyCode"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v2

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetShortcutKeyCode:Ljava/lang/reflect/Method;

    .line 696
    const-string/jumbo v1, "setActive"

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v2

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetEnable:Ljava/lang/reflect/Method;

    .line 697
    const-string/jumbo v1, "setType"

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v2

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetType:Ljava/lang/reflect/Method;

    .line 698
    const-string/jumbo v1, "setAppInfo"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const-class v5, Ljava/lang/String;

    aput-object v5, v4, v2

    const-class v5, Ljava/lang/String;

    aput-object v5, v4, v3

    invoke-virtual {v0, v1, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetAppInfo:Ljava/lang/reflect/Method;

    .line 699
    const-string/jumbo v1, "setCustomized"

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetCustomized:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 702
    .end local v0    # "infoClass":Ljava/lang/Class;
    goto :goto_0

    .line 700
    :catch_0
    move-exception v0

    .line 701
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiCustomizeShortCutUtils"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public invokeObject(Landroid/view/KeyboardShortcutInfo;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .locals 10
    .param p1, "rawInfo"    # Landroid/view/KeyboardShortcutInfo;

    .line 706
    const/4 v0, 0x0

    .line 708
    .local v0, "resultInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    :try_start_0
    new-instance v8, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetShortcutKeyCode:Ljava/lang/reflect/Method;

    const/4 v9, 0x0

    new-array v3, v9, [Ljava/lang/Object;

    .line 709
    invoke-virtual {v1, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mIsEnable:Ljava/lang/reflect/Method;

    new-array v5, v9, [Ljava/lang/Object;

    .line 710
    invoke-virtual {v1, p1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetType:Ljava/lang/reflect/Method;

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mIsCustomized:Ljava/lang/reflect/Method;

    new-array v7, v9, [Ljava/lang/Object;

    .line 711
    invoke-virtual {v1, p1, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;JZIZ)V

    move-object v0, v8

    .line 712
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetPackageName:Ljava/lang/reflect/Method;

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetClassName:Ljava/lang/reflect/Method;

    new-array v3, v9, [Ljava/lang/Object;

    .line 713
    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 712
    invoke-virtual {v0, v1, v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->setAppInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mGetHistoryKeyCode:Ljava/lang/reflect/Method;

    new-array v2, v9, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->setHistoryKeyCode(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    goto :goto_0

    .line 715
    :catch_0
    move-exception v1

    .line 716
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiCustomizeShortCutUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method

.method public reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;
    .locals 6
    .param p1, "rawInfo"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 722
    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->transformKeycode(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;

    move-result-object v0

    .line 724
    .local v0, "resultInfo":Landroid/view/KeyboardShortcutInfo;
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetShortcutKeyCode:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->-$$Nest$fgetmShortcutKeyCode(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetEnable:Ljava/lang/reflect/Method;

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->-$$Nest$fgetmEnable(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetType:Ljava/lang/reflect/Method;

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->-$$Nest$fgetmType(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 727
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v1

    if-nez v1, :cond_0

    .line 728
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetAppInfo:Ljava/lang/reflect/Method;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->-$$Nest$fgetmPackageName(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->-$$Nest$fgetmClassName(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->mSetCustomized:Ljava/lang/reflect/Method;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->-$$Nest$fgetmCustomized(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 733
    goto :goto_0

    .line 731
    :catch_0
    move-exception v1

    .line 732
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "MiuiCustomizeShortCutUtils"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return-object v0
.end method
