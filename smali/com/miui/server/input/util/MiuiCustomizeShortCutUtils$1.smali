.class Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiCustomizeShortCutUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;


# direct methods
.method constructor <init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 108
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 111
    const-string v0, "MiuiCustomizeShortCutUtils"

    const-string/jumbo v1, "update all shortcuts because languages is changed"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V

    .line 113
    return-void
.end method
