class com.miui.server.input.util.MiuiCustomizeShortCutUtils$2 extends android.content.BroadcastReceiver {
	 /* .source "MiuiCustomizeShortCutUtils.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->enableAutoRemoveShortCutWhenAppRemove()V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.util.MiuiCustomizeShortCutUtils this$0; //synthetic
/* # direct methods */
 com.miui.server.input.util.MiuiCustomizeShortCutUtils$2 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils; */
/* .line 190 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 193 */
final String v0 = "enableAutoRemoveShortCutWhenAppRemove: onReceive"; // const-string v0, "enableAutoRemoveShortCutWhenAppRemove: onReceive"
final String v1 = "MiuiCustomizeShortCutUtils"; // const-string v1, "MiuiCustomizeShortCutUtils"
android.util.Slog .d ( v1,v0 );
/* .line 194 */
(( android.content.Intent ) p2 ).getData ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;
/* .line 195 */
/* .local v0, "data":Landroid/net/Uri; */
/* if-nez v0, :cond_0 */
/* .line 196 */
final String v2 = "Cannot handle package broadcast with null data"; // const-string v2, "Cannot handle package broadcast with null data"
android.util.Slog .w ( v1,v2 );
/* .line 197 */
return;
/* .line 199 */
} // :cond_0
(( android.net.Uri ) v0 ).getSchemeSpecificPart ( ); // invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;
/* .line 200 */
/* .local v1, "packageName":Ljava/lang/String; */
v2 = this.this$0;
com.miui.server.input.util.MiuiCustomizeShortCutUtils .-$$Nest$mdelShortCutByPackage ( v2,v1 );
/* .line 201 */
return;
} // .end method
