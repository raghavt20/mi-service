.class Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2;
.super Landroid/content/BroadcastReceiver;
.source "MiuiCustomizeShortCutUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->enableAutoRemoveShortCutWhenAppRemove()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;


# direct methods
.method constructor <init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 190
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 193
    const-string v0, "enableAutoRemoveShortCutWhenAppRemove: onReceive"

    const-string v1, "MiuiCustomizeShortCutUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 195
    .local v0, "data":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 196
    const-string v2, "Cannot handle package broadcast with null data"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    return-void

    .line 199
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2;->this$0:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-static {v2, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->-$$Nest$mdelShortCutByPackage(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Ljava/lang/String;)V

    .line 201
    return-void
.end method
