public class com.miui.server.input.util.MiuiInputShellCommand {
	 /* .source "MiuiInputShellCommand.java" */
	 /* # static fields */
	 private static final Boolean DEBUG;
	 private static final Integer MODE_INJECT;
	 private static final java.lang.String TAG;
	 private static volatile com.miui.server.input.util.MiuiInputShellCommand sInstance;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 private android.view.DisplayInfo mDisplayInfo;
	 private final android.hardware.display.DisplayManager$DisplayListener mDisplayListener;
	 private final android.hardware.display.DisplayManagerInternal mDisplayManagerInternal;
	 private final com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
	 private final com.miui.server.input.util.MotionEventGenerator mMotionEventGenerator;
	 /* # direct methods */
	 public static void $r8$lambda$GlekGVcdi-zo8AW92rsoCT38W2o ( com.miui.server.input.util.MiuiInputShellCommand p0, android.view.MotionEvent p1 ) { //synthethic
		 /* .locals 0 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiInputShellCommand;->injectEvent(Landroid/view/MotionEvent;)V */
		 return;
	 } // .end method
	 static android.hardware.display.DisplayManagerInternal -$$Nest$fgetmDisplayManagerInternal ( com.miui.server.input.util.MiuiInputShellCommand p0 ) { //bridge//synthethic
		 /* .locals 0 */
		 p0 = this.mDisplayManagerInternal;
	 } // .end method
	 static void -$$Nest$fputmDisplayInfo ( com.miui.server.input.util.MiuiInputShellCommand p0, android.view.DisplayInfo p1 ) { //bridge//synthethic
		 /* .locals 0 */
		 this.mDisplayInfo = p1;
		 return;
	 } // .end method
	 static com.miui.server.input.util.MiuiInputShellCommand ( ) {
		 /* .locals 1 */
		 /* .line 18 */
		 /* const-class v0, Lcom/miui/server/input/util/MiuiInputShellCommand; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 private com.miui.server.input.util.MiuiInputShellCommand ( ) {
		 /* .locals 5 */
		 /* .line 49 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 29 */
		 /* new-instance v0, Lcom/miui/server/input/util/MiuiInputShellCommand$1; */
		 /* invoke-direct {v0, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$1;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V */
		 this.mDisplayListener = v0;
		 /* .line 50 */
		 android.app.ActivityThread .currentActivityThread ( );
		 (( android.app.ActivityThread ) v1 ).getSystemContext ( ); // invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
		 this.mContext = v1;
		 /* .line 51 */
		 /* new-instance v2, Landroid/os/HandlerThread; */
		 final String v3 = "input_inject"; // const-string v3, "input_inject"
		 /* invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
		 /* .line 52 */
		 /* .local v2, "handlerThread":Landroid/os/HandlerThread; */
		 (( android.os.HandlerThread ) v2 ).start ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V
		 /* .line 53 */
		 /* new-instance v3, Lcom/miui/server/input/util/MotionEventGenerator; */
		 (( android.os.HandlerThread ) v2 ).getLooper ( ); // invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
		 /* invoke-direct {v3, v4}, Lcom/miui/server/input/util/MotionEventGenerator;-><init>(Landroid/os/Looper;)V */
		 this.mMotionEventGenerator = v3;
		 /* .line 54 */
		 /* const-class v3, Landroid/hardware/display/DisplayManager; */
		 (( android.content.Context ) v1 ).getSystemService ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
		 /* check-cast v1, Landroid/hardware/display/DisplayManager; */
		 /* .line 55 */
		 /* .local v1, "displayManager":Landroid/hardware/display/DisplayManager; */
		 int v3 = 0; // const/4 v3, 0x0
		 (( android.hardware.display.DisplayManager ) v1 ).registerDisplayListener ( v0, v3 ); // invoke-virtual {v1, v0, v3}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V
		 /* .line 56 */
		 /* const-class v0, Landroid/hardware/display/DisplayManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Landroid/hardware/display/DisplayManagerInternal; */
		 this.mDisplayManagerInternal = v0;
		 /* .line 57 */
		 int v3 = 0; // const/4 v3, 0x0
		 (( android.hardware.display.DisplayManagerInternal ) v0 ).getDisplayInfo ( v3 ); // invoke-virtual {v0, v3}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;
		 this.mDisplayInfo = v0;
		 /* .line 59 */
		 /* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
		 com.android.server.LocalServices .getService ( v0 );
		 /* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
		 this.mMiuiInputManagerInternal = v0;
		 /* .line 60 */
		 return;
	 } // .end method
	 public static com.miui.server.input.util.MiuiInputShellCommand getInstance ( ) {
		 /* .locals 2 */
		 /* .line 63 */
		 v0 = com.miui.server.input.util.MiuiInputShellCommand.sInstance;
		 /* if-nez v0, :cond_1 */
		 /* .line 64 */
		 /* const-class v0, Lcom/miui/server/input/util/MiuiInputShellCommand; */
		 /* monitor-enter v0 */
		 /* .line 65 */
		 try { // :try_start_0
			 v1 = com.miui.server.input.util.MiuiInputShellCommand.sInstance;
			 /* if-nez v1, :cond_0 */
			 /* .line 66 */
			 /* new-instance v1, Lcom/miui/server/input/util/MiuiInputShellCommand; */
			 /* invoke-direct {v1}, Lcom/miui/server/input/util/MiuiInputShellCommand;-><init>()V */
			 /* .line 68 */
		 } // :cond_0
		 /* monitor-exit v0 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
		 /* .line 70 */
	 } // :cond_1
} // :goto_0
v0 = com.miui.server.input.util.MiuiInputShellCommand.sInstance;
} // .end method
private void injectEvent ( android.view.MotionEvent p0 ) {
/* .locals 2 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 118 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiInputShellCommand;->transformMotionEventForInjection(Landroid/view/MotionEvent;)V */
/* .line 119 */
v0 = this.mMiuiInputManagerInternal;
int v1 = 3; // const/4 v1, 0x3
(( com.android.server.input.MiuiInputManagerInternal ) v0 ).injectMotionEvent ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->injectMotionEvent(Landroid/view/MotionEvent;I)V
/* .line 120 */
return;
} // .end method
private void transformMotionEventForInjection ( android.view.MotionEvent p0 ) {
/* .locals 6 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 99 */
v0 = this.mDisplayInfo;
/* iget v0, v0, Landroid/view/DisplayInfo;->rotation:I */
/* .line 100 */
/* .local v0, "rotation":I */
v1 = this.mDisplayInfo;
/* iget v1, v1, Landroid/view/DisplayInfo;->logicalWidth:I */
/* .line 101 */
/* .local v1, "width":I */
v2 = this.mDisplayInfo;
/* iget v2, v2, Landroid/view/DisplayInfo;->logicalHeight:I */
/* .line 102 */
/* .local v2, "height":I */
int v3 = 3; // const/4 v3, 0x3
int v4 = 1; // const/4 v4, 0x1
/* if-ne v0, v4, :cond_0 */
/* .line 103 */
android.view.MotionEvent .createRotateMatrix ( v3,v2,v1 );
(( android.view.MotionEvent ) p1 ).applyTransform ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V
/* .line 105 */
} // :cond_0
int v5 = 2; // const/4 v5, 0x2
/* if-ne v0, v5, :cond_1 */
/* .line 106 */
android.view.MotionEvent .createRotateMatrix ( v5,v1,v2 );
(( android.view.MotionEvent ) p1 ).applyTransform ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V
/* .line 108 */
} // :cond_1
/* if-ne v0, v3, :cond_2 */
/* .line 109 */
android.view.MotionEvent .createRotateMatrix ( v4,v2,v1 );
(( android.view.MotionEvent ) p1 ).applyTransform ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V
/* .line 115 */
} // :cond_2
} // :goto_0
return;
} // .end method
/* # virtual methods */
public Boolean doubleTapGenerator ( Integer p0, Integer p1, Integer p2 ) {
/* .locals 4 */
/* .param p1, "tapX" # I */
/* .param p2, "tapY" # I */
/* .param p3, "duration" # I */
/* .line 89 */
v0 = this.mMotionEventGenerator;
/* int-to-float v1, p1 */
/* int-to-float v2, p2 */
/* new-instance v3, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V */
v0 = (( com.miui.server.input.util.MotionEventGenerator ) v0 ).generateDoubleTapMotionEvent ( v1, v2, p3, v3 ); // invoke-virtual {v0, v1, v2, p3, v3}, Lcom/miui/server/input/util/MotionEventGenerator;->generateDoubleTapMotionEvent(FFILcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z
} // .end method
public Boolean swipeGenerator ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4 ) {
/* .locals 7 */
/* .param p1, "downX" # I */
/* .param p2, "downY" # I */
/* .param p3, "upX" # I */
/* .param p4, "upY" # I */
/* .param p5, "duration" # I */
/* .line 74 */
v0 = this.mMotionEventGenerator;
/* int-to-float v1, p1 */
/* int-to-float v2, p2 */
/* int-to-float v3, p3 */
/* int-to-float v4, p4 */
/* new-instance v6, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0; */
/* invoke-direct {v6, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V */
/* move v5, p5 */
v0 = /* invoke-virtual/range {v0 ..v6}, Lcom/miui/server/input/util/MotionEventGenerator;->generateSwipeMotionEvent(FFFFILcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z */
} // .end method
public Boolean swipeGenerator ( Integer p0, Integer p1, Integer p2, Integer p3, Integer p4, Integer p5 ) {
/* .locals 9 */
/* .param p1, "downX" # I */
/* .param p2, "downY" # I */
/* .param p3, "upX" # I */
/* .param p4, "upY" # I */
/* .param p5, "duration" # I */
/* .param p6, "everyDelayTime" # I */
/* .line 80 */
v0 = this.mMotionEventGenerator;
/* int-to-float v1, p1 */
/* int-to-float v2, p2 */
/* int-to-float v3, p3 */
/* int-to-float v4, p4 */
/* int-to-long v6, p6 */
/* new-instance v8, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0; */
/* invoke-direct {v8, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V */
/* move v5, p5 */
v0 = /* invoke-virtual/range {v0 ..v8}, Lcom/miui/server/input/util/MotionEventGenerator;->generateSwipeMotionEvent(FFFFIJLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z */
} // .end method
public Boolean tapGenerator ( Integer p0, Integer p1 ) {
/* .locals 4 */
/* .param p1, "tapX" # I */
/* .param p2, "tapY" # I */
/* .line 85 */
v0 = this.mMotionEventGenerator;
/* int-to-float v1, p1 */
/* int-to-float v2, p2 */
/* new-instance v3, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V */
v0 = (( com.miui.server.input.util.MotionEventGenerator ) v0 ).generateTapMotionEvent ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/input/util/MotionEventGenerator;->generateTapMotionEvent(FFLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z
} // .end method
