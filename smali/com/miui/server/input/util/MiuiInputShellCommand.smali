.class public Lcom/miui/server/input/util/MiuiInputShellCommand;
.super Ljava/lang/Object;
.source "MiuiInputShellCommand.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final MODE_INJECT:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static volatile sInstance:Lcom/miui/server/input/util/MiuiInputShellCommand;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDisplayInfo:Landroid/view/DisplayInfo;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private final mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

.field private final mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private final mMotionEventGenerator:Lcom/miui/server/input/util/MotionEventGenerator;


# direct methods
.method public static synthetic $r8$lambda$GlekGVcdi-zo8AW92rsoCT38W2o(Lcom/miui/server/input/util/MiuiInputShellCommand;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiInputShellCommand;->injectEvent(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmDisplayManagerInternal(Lcom/miui/server/input/util/MiuiInputShellCommand;)Landroid/hardware/display/DisplayManagerInternal;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmDisplayInfo(Lcom/miui/server/input/util/MiuiInputShellCommand;Landroid/view/DisplayInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayInfo:Landroid/view/DisplayInfo;

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 18
    const-class v0, Lcom/miui/server/input/util/MiuiInputShellCommand;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/util/MiuiInputShellCommand;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/miui/server/input/util/MiuiInputShellCommand$1;

    invoke-direct {v0, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$1;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 50
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mContext:Landroid/content/Context;

    .line 51
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "input_inject"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 52
    .local v2, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 53
    new-instance v3, Lcom/miui/server/input/util/MotionEventGenerator;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/miui/server/input/util/MotionEventGenerator;-><init>(Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMotionEventGenerator:Lcom/miui/server/input/util/MotionEventGenerator;

    .line 54
    const-class v3, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    .line 55
    .local v1, "displayManager":Landroid/hardware/display/DisplayManager;
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 56
    const-class v0, Landroid/hardware/display/DisplayManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManagerInternal;

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayManagerInternal:Landroid/hardware/display/DisplayManagerInternal;

    .line 57
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/hardware/display/DisplayManagerInternal;->getDisplayInfo(I)Landroid/view/DisplayInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayInfo:Landroid/view/DisplayInfo;

    .line 59
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 60
    return-void
.end method

.method public static getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;
    .locals 2

    .line 63
    sget-object v0, Lcom/miui/server/input/util/MiuiInputShellCommand;->sInstance:Lcom/miui/server/input/util/MiuiInputShellCommand;

    if-nez v0, :cond_1

    .line 64
    const-class v0, Lcom/miui/server/input/util/MiuiInputShellCommand;

    monitor-enter v0

    .line 65
    :try_start_0
    sget-object v1, Lcom/miui/server/input/util/MiuiInputShellCommand;->sInstance:Lcom/miui/server/input/util/MiuiInputShellCommand;

    if-nez v1, :cond_0

    .line 66
    new-instance v1, Lcom/miui/server/input/util/MiuiInputShellCommand;

    invoke-direct {v1}, Lcom/miui/server/input/util/MiuiInputShellCommand;-><init>()V

    sput-object v1, Lcom/miui/server/input/util/MiuiInputShellCommand;->sInstance:Lcom/miui/server/input/util/MiuiInputShellCommand;

    .line 68
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 70
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/util/MiuiInputShellCommand;->sInstance:Lcom/miui/server/input/util/MiuiInputShellCommand;

    return-object v0
.end method

.method private injectEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 118
    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiInputShellCommand;->transformMotionEventForInjection(Landroid/view/MotionEvent;)V

    .line 119
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, Lcom/android/server/input/MiuiInputManagerInternal;->injectMotionEvent(Landroid/view/MotionEvent;I)V

    .line 120
    return-void
.end method

.method private transformMotionEventForInjection(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 99
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v0, v0, Landroid/view/DisplayInfo;->rotation:I

    .line 100
    .local v0, "rotation":I
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v1, v1, Landroid/view/DisplayInfo;->logicalWidth:I

    .line 101
    .local v1, "width":I
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mDisplayInfo:Landroid/view/DisplayInfo;

    iget v2, v2, Landroid/view/DisplayInfo;->logicalHeight:I

    .line 102
    .local v2, "height":I
    const/4 v3, 0x3

    const/4 v4, 0x1

    if-ne v0, v4, :cond_0

    .line 103
    invoke-static {v3, v2, v1}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 105
    :cond_0
    const/4 v5, 0x2

    if-ne v0, v5, :cond_1

    .line 106
    invoke-static {v5, v1, v2}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 108
    :cond_1
    if-ne v0, v3, :cond_2

    .line 109
    invoke-static {v4, v2, v1}, Landroid/view/MotionEvent;->createRotateMatrix(III)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->applyTransform(Landroid/graphics/Matrix;)V

    .line 115
    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public doubleTapGenerator(III)Z
    .locals 4
    .param p1, "tapX"    # I
    .param p2, "tapY"    # I
    .param p3, "duration"    # I

    .line 89
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMotionEventGenerator:Lcom/miui/server/input/util/MotionEventGenerator;

    int-to-float v1, p1

    int-to-float v2, p2

    new-instance v3, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V

    invoke-virtual {v0, v1, v2, p3, v3}, Lcom/miui/server/input/util/MotionEventGenerator;->generateDoubleTapMotionEvent(FFILcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z

    move-result v0

    return v0
.end method

.method public swipeGenerator(IIIII)Z
    .locals 7
    .param p1, "downX"    # I
    .param p2, "downY"    # I
    .param p3, "upX"    # I
    .param p4, "upY"    # I
    .param p5, "duration"    # I

    .line 74
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMotionEventGenerator:Lcom/miui/server/input/util/MotionEventGenerator;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    new-instance v6, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;

    invoke-direct {v6, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/miui/server/input/util/MotionEventGenerator;->generateSwipeMotionEvent(FFFFILcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z

    move-result v0

    return v0
.end method

.method public swipeGenerator(IIIIII)Z
    .locals 9
    .param p1, "downX"    # I
    .param p2, "downY"    # I
    .param p3, "upX"    # I
    .param p4, "upY"    # I
    .param p5, "duration"    # I
    .param p6, "everyDelayTime"    # I

    .line 80
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMotionEventGenerator:Lcom/miui/server/input/util/MotionEventGenerator;

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    int-to-long v6, p6

    new-instance v8, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;

    invoke-direct {v8, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V

    move v5, p5

    invoke-virtual/range {v0 .. v8}, Lcom/miui/server/input/util/MotionEventGenerator;->generateSwipeMotionEvent(FFFFIJLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z

    move-result v0

    return v0
.end method

.method public tapGenerator(II)Z
    .locals 4
    .param p1, "tapX"    # I
    .param p2, "tapY"    # I

    .line 85
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiInputShellCommand;->mMotionEventGenerator:Lcom/miui/server/input/util/MotionEventGenerator;

    int-to-float v1, p1

    int-to-float v2, p2

    new-instance v3, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/miui/server/input/util/MiuiInputShellCommand$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiInputShellCommand;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/input/util/MotionEventGenerator;->generateTapMotionEvent(FFLcom/miui/server/input/util/MotionEventGenerator$GenerateResultCallback;)Z

    move-result v0

    return v0
.end method
