.class public Lcom/miui/server/input/util/ShortCutActionsUtils;
.super Ljava/lang/Object;
.source "ShortCutActionsUtils.java"


# static fields
.field private static final ACCESSIBILITY_CLASS_NAME_ENVIRONMENT_SPEECH_RECOGNITION:Ljava/lang/String; = "com.miui.accessibility.environment.sound.recognition.TransparentEsrSettings"

.field private static final ACCESSIBILITY_CLASS_NAME_ENVIRONMENT_SPEECH_RECOGNITION_ENABLED:Ljava/lang/String; = "com.miui.accessibility.environment.sound.recognition.EnvSoundRecognitionService"

.field private static final ACCESSIBILITY_CLASS_NAME_HEAR_SOUND:Ljava/lang/String; = "com.miui.accessibility.asr.component.message.MessageActivity"

.field private static final ACCESSIBILITY_CLASS_NAME_HEAR_SOUND_SUBTITLE:Ljava/lang/String; = "com.miui.accessibility.asr.component.floatwindow.TransparentCaptionActivity"

.field private static final ACCESSIBILITY_CLASS_NAME_TALK_BACK:Ljava/lang/String; = "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

.field private static final ACCESSIBILITY_CLASS_NAME_VOICE_CONTROL:Ljava/lang/String; = "com.miui.accessibility.voiceaccess.settings.TransparentVoiceAccessSettings"

.field private static final ACCESSIBILITY_CLASS_NAME_VOICE_CONTROL_ENABLED:Ljava/lang/String; = "com.miui.accessibility.voiceaccess.VoiceAccessAccessibilityService"

.field private static final ACCESSIBILITY_LIVE_SUBTITLES_WINDOW_TYPE_DEFAULT:Ljava/lang/String; = "startAiSubtitlesWindow"

.field private static final ACCESSIBILITY_LIVE_SUBTITLES_WINDOW_TYPE_FULL_SCREEN:Ljava/lang/String; = "startAiSubtitlesFullscreen"

.field private static final ACCESSIBILITY_PACKAGE_NAME:Ljava/lang/String; = "com.miui.accessibility"

.field public static final ACCESSIBILITY_TALKBACK_STATUS:Ljava/lang/String; = "talkback_status"

.field private static final ACCESSIBLE_MODE_LARGE_DENSITY:I = 0x1cd

.field private static final ACCESSIBLE_MODE_SMALL_DENSITY:I = 0x160

.field private static final ACTION_GLOBAL_POWER_GUIDE:Ljava/lang/String; = "com.miui.miinput.action.GLOBAL_POWER_GUIDE"

.field private static final ACTION_INTENT_CLASS_NAME:Ljava/lang/String; = "className"

.field private static final ACTION_INTENT_CONTENT:Ljava/lang/String; = "content"

.field private static final ACTION_INTENT_DUAL:Ljava/lang/String; = "dual"

.field private static final ACTION_INTENT_KEY:Ljava/lang/String; = "packageName"

.field private static final ACTION_INTENT_SHORTCUT:Ljava/lang/String; = "shortcut"

.field private static final ACTION_INTENT_TITLE:Ljava/lang/String; = "title"

.field private static final ACTION_KDDI_GLOBAL_POWER_GUIDE:Ljava/lang/String; = "com.miui.miinput.action.KDDI_GLOBAL_POWER_GUIDE"

.field public static final ACTION_PANEL_OPERATION:Ljava/lang/String; = "action_panels_operation"

.field public static final ACTION_POWER_UP:Ljava/lang/String; = "power_up"

.field private static final ACTION_ROTATION_FOLLOWS_SENSOR_GLOBAL_POWER_GUIDE:Ljava/lang/String; = "com.miui.miinput.action.ROTATION_FOLLOWS_SENSOR_GLOBAL_POWER_GUIDE"

.field private static final ACTION_SMALL_SCREEN_GLOBAL_POWER_GUIDE:Ljava/lang/String; = "com.miui.miinput.action.SMALL_SCREEN_GLOBAL_POWER_GUIDE"

.field private static final DEVICE_TYPE_FOLD:Ljava/lang/String; = "fold"

.field private static final DEVICE_TYPE_PAD:Ljava/lang/String; = "pad"

.field private static final DEVICE_TYPE_PHONE:Ljava/lang/String; = "phone"

.field private static final DEVICE_TYPE_SMALL_SCREEN:Ljava/lang/String; = "small_screen"

.field static final DIRECTION_LEFT:I = 0x0

.field static final DIRECTION_RIGHT:I = 0x1

.field public static final DISABLE_SHORTCUT_TRACK:Ljava/lang/String; = "disable_shortcut_track"

.field public static final EVENT_LEICA_MOMENT:I = 0x10001

.field public static final EXTRA_ACTION_SOURCE:Ljava/lang/String; = "event_source"

.field public static final EXTRA_KEY_ACTION:Ljava/lang/String; = "extra_key_action"

.field public static final EXTRA_KEY_EVENT_TIME:Ljava/lang/String; = "extra_key_event_time"

.field public static final EXTRA_LONG_PRESS_POWER_FUNCTION:Ljava/lang/String; = "extra_long_press_power_function"

.field public static final EXTRA_POWER_GUIDE:Ljava/lang/String; = "powerGuide"

.field public static final EXTRA_SHORTCUT_TYPE:Ljava/lang/String; = "shortcut_type"

.field public static final EXTRA_SKIP_TELECOM_CHECK:Ljava/lang/String; = "skip_telecom_check"

.field public static final EXTRA_TORCH_ENABLED:Ljava/lang/String; = "extra_torch_enabled"

.field private static final IS_CETUS:Z

.field private static final KEY_ACTION:Ljava/lang/String; = "key_action"

.field public static final KEY_OPERATION:Ljava/lang/String; = "operation"

.field public static final NOTES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/text_note"

.field private static final PACKAGE_INPUT_SETTINGS:Ljava/lang/String; = "com.miui.securitycore"

.field private static final PACKAGE_MI_CREATION:Ljava/lang/String; = "com.miui.creation"

.field private static final PACKAGE_NOTES:Ljava/lang/String; = "com.miui.notes"

.field public static final PACKAGE_SMART_HOME:Ljava/lang/String; = "com.miui.smarthomeplus"

.field public static final PARTIAL_SCREENSHOT_POINTS:Ljava/lang/String; = "partial.screenshot.points"

.field public static final REASON_OF_DOUBLE_CLICK_HOME_KEY:Ljava/lang/String; = "double_click_home"

.field public static final REASON_OF_DOUBLE_CLICK_VOLUME_DOWN:Ljava/lang/String; = "double_click_volume_down"

.field public static final REASON_OF_KEYBOARD:Ljava/lang/String; = "keyboard"

.field public static final REASON_OF_KEYBOARD_FN:Ljava/lang/String; = "keyboard_fn"

.field public static final REASON_OF_LONG_PRESS_CAMERA_KEY:Ljava/lang/String; = "long_press_camera_key"

.field public static final REASON_OF_TRIGGERED_BY_AI_KEY:Ljava/lang/String; = "ai_key"

.field public static final REASON_OF_TRIGGERED_BY_PROXIMITY_SENSOR:Ljava/lang/String; = "proximity_sensor"

.field public static final REASON_OF_TRIGGERED_BY_STABILIZER:Ljava/lang/String; = "stabilizer"

.field public static final REASON_OF_TRIGGERED_TORCH:Ljava/lang/String; = "triggered_by_runnable"

.field public static final REASON_OF_TRIGGERED_TORCH_BY_POWER:Ljava/lang/String; = "triggered_by_power"

.field public static final REVERSE_NOTIFICATION_PANEL:Ljava/lang/String; = "reverse_notifications_panel"

.field public static final REVERSE_QUICK_SETTINGS_PANEL:Ljava/lang/String; = "reverse_quick_settings_panel"

.field private static final SETTINGS_CLASS_NAME_KEY_DOWNLOAD_DIALOG_ACTIVITY:Ljava/lang/String; = "com.android.settings.KeyDownloadDialogActivity"

.field private static final SETTINGS_PACKAGE_NAME:Ljava/lang/String; = "com.android.settings"

.field private static final TAG:Ljava/lang/String; = "ShortCutActionsUtils"

.field public static final TAKE_PARTIAL_SCREENSHOT:I = 0x64

.field public static final TAKE_PARTIAL_SCREENSHOT_WITH_STYLUS:I = 0x62

.field public static final TAKE_SCREENSHOT_WITHOUT_ANIM:I = 0x63

.field public static final TYPE_PHONE_SHORTCUT:Ljava/lang/String; = "phone_shortcut"

.field private static final XIAO_POWER_GUIDE_VERSIONCODE:I = 0x3

.field private static shortCutActionsUtils:Lcom/miui/server/input/util/ShortCutActionsUtils;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGAGuideCustomizedRegionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

.field private mHasCameraFlash:Z

.field private mIsFolded:Z

.field private mIsKidMode:Z

.field private mIsSystemReady:Z

.field private final mIsVoiceCapable:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mScreenshotHelper:Lcom/android/internal/util/ScreenshotHelper;

.field private final mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

.field private mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

.field private mStatusBarManagerInternal:Lcom/android/server/statusbar/StatusBarManagerInternal;

.field private mSupportAccessibilitySubtitle:Z

.field private mWifiOnly:Z

.field private mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;


# direct methods
.method public static synthetic $r8$lambda$ZVuYTQzk9gzCfluI2DfBwYcAcaI(Lcom/miui/server/input/util/ShortCutActionsUtils;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->lambda$new$0(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic $r8$lambda$pp9-S0QktzQOaIhIfq0Hk0rKrXc(Lcom/miui/server/input/util/ShortCutActionsUtils;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->lambda$showToast$1(II)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 122
    const-string v0, "cetus"

    sget-object v1, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/input/util/ShortCutActionsUtils;->IS_CETUS:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mGAGuideCustomizedRegionList:Ljava/util/List;

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsSystemReady:Z

    .line 184
    iput-object p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    .line 185
    new-instance v0, Lcom/android/internal/util/ScreenshotHelper;

    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/internal/util/ScreenshotHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mScreenshotHelper:Lcom/android/internal/util/ScreenshotHelper;

    .line 186
    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHandler:Landroid/os/Handler;

    .line 187
    new-instance v1, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/ShortCutActionsUtils;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 188
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x111023a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsVoiceCapable:Z

    .line 190
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/os/Build;->hasCameraFlash(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHasCameraFlash:Z

    .line 191
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    .line 192
    return-void
.end method

.method private findDeviceLocate()Z
    .locals 2

    .line 751
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const-string v1, "imperceptible_power_press"

    invoke-static {v0, v1}, Lcom/android/server/policy/FindDevicePowerOffLocateManager;->sendFindDeviceLocateBroadcast(Landroid/content/Context;Ljava/lang/String;)V

    .line 753
    const/4 v0, 0x1

    return v0
.end method

.method private getDeviceType()Ljava/lang/String;
    .locals 1

    .line 516
    sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z

    if-eqz v0, :cond_0

    .line 517
    const-string v0, "fold"

    return-object v0

    .line 518
    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z

    if-eqz v0, :cond_1

    .line 519
    const-string v0, "pad"

    return-object v0

    .line 521
    :cond_1
    const-string v0, "phone"

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .line 195
    sget-object v0, Lcom/miui/server/input/util/ShortCutActionsUtils;->shortCutActionsUtils:Lcom/miui/server/input/util/ShortCutActionsUtils;

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Lcom/miui/server/input/util/ShortCutActionsUtils;

    invoke-direct {v0, p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/miui/server/input/util/ShortCutActionsUtils;->shortCutActionsUtils:Lcom/miui/server/input/util/ShortCutActionsUtils;

    .line 198
    :cond_0
    sget-object v0, Lcom/miui/server/input/util/ShortCutActionsUtils;->shortCutActionsUtils:Lcom/miui/server/input/util/ShortCutActionsUtils;

    return-object v0
.end method

.method private getLongPressPowerKeyFunctionIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7
    .param p1, "intentName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .line 780
    const/4 v0, 0x0

    .line 781
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v2, "android.intent.action.ASSIST"

    const-string v3, "com.miui.smarthomeplus"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, -0x1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    goto :goto_0

    :sswitch_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    goto :goto_1

    :sswitch_1
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    goto :goto_1

    :goto_0
    move v1, v6

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 788
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    move-object v0, v1

    .line 789
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.smarthomeplus.UWBEntryService"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 791
    goto :goto_2

    .line 783
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object v0, v1

    .line 784
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 785
    const-string/jumbo v1, "versionCode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 786
    nop

    .line 796
    :goto_2
    if-eqz v0, :cond_2

    .line 797
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    :cond_1
    goto :goto_3

    :sswitch_2
    const-string v1, "power_up"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v6, v5

    goto :goto_3

    :sswitch_3
    const-string v1, "long_press_power_key"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v6, v4

    :goto_3
    const-string v1, "key_action"

    packed-switch v6, :pswitch_data_1

    goto :goto_4

    .line 805
    :pswitch_2
    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 806
    const-string v1, "key_event_time"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 807
    goto :goto_4

    .line 799
    :pswitch_3
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 801
    nop

    .line 802
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 801
    const-string v3, "long_press_event_time"

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 803
    nop

    .line 812
    :cond_2
    :goto_4
    return-object v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11cbb911 -> :sswitch_1
        0x5d5f976e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        -0x4b0ca89a -> :sswitch_3
        0x332c9015 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private getPowerGuideIntent()Landroid/content/Intent;
    .locals 3

    .line 1377
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1378
    .local v0, "powerGuideIntent":Landroid/content/Intent;
    const-string v1, "com.miui.voiceassist"

    const-string v2, "com.xiaomi.voiceassistant.guidePage.PowerGuideDialogActivityV2"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1380
    const-string/jumbo v1, "showSwitchNotice"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1381
    const/high16 v1, 0x30000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1382
    return-object v0
.end method

.method private goToSleep(Ljava/lang/String;)Z
    .locals 4
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1312
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1313
    .local v0, "currentTime":J
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v2, :cond_0

    .line 1314
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "goToSleep, reason = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1315
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v2, v0, v1}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 1316
    const/4 v2, 0x1

    return v2

    .line 1318
    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method private isAccessibilityFunctionEnabled(Ljava/lang/String;)Z
    .locals 5
    .param p1, "componentClassName"    # Ljava/lang/String;

    .line 693
    const/4 v0, 0x0

    .line 694
    .local v0, "enabled":Z
    if-nez p1, :cond_0

    .line 695
    const-string v1, "Accessibility function componentClassName is null"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    .line 696
    const/4 v1, 0x0

    return v1

    .line 698
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    .line 699
    const/4 v3, -0x2

    invoke-static {v2, v3}, Lcom/android/internal/accessibility/util/AccessibilityUtils;->getEnabledServicesFromSettings(Landroid/content/Context;I)Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 700
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    .line 701
    .local v3, "component":Landroid/content/ComponentName;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 702
    const/4 v0, 0x1

    .line 704
    .end local v3    # "component":Landroid/content/ComponentName;
    :cond_1
    goto :goto_0

    .line 705
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Accessibility function componentClassName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " enabled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 707
    return v0
.end method

.method private isAppInstalled(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 954
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 955
    invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 959
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    goto :goto_0

    .line 956
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 957
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    .line 958
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " don\'t install"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    move-object v1, v2

    .line 960
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isLargeScreen()Z
    .locals 3

    .line 526
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 527
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    .line 528
    .local v1, "screenSize":I
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method private isLargeScreen(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 469
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 470
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 471
    .local v1, "smallestScreenWidthDp":I
    iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I

    const/16 v3, 0x160

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne v2, v3, :cond_1

    .line 473
    const/16 v2, 0x190

    if-le v1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v4, v5

    :goto_0
    return v4

    .line 474
    :cond_1
    iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I

    const/16 v3, 0x1cd

    if-ne v2, v3, :cond_3

    .line 476
    const/16 v2, 0x131

    if-le v1, v2, :cond_2

    goto :goto_1

    :cond_2
    move v4, v5

    :goto_1
    return v4

    .line 478
    :cond_3
    const/16 v2, 0x140

    if-le v1, v2, :cond_4

    goto :goto_2

    :cond_4
    move v4, v5

    :goto_2
    return v4
.end method

.method private isSupportSpiltScreen()Z
    .locals 3

    .line 454
    sget-boolean v0, Lcom/miui/server/input/util/ShortCutActionsUtils;->IS_CETUS:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isLargeScreen(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    const-string v0, "Ignore because the current window is the small screen"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 456
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v2, 0x110f01f5

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->makeAllUserToastAndShow(Ljava/lang/String;I)V

    .line 458
    return v1

    .line 460
    :cond_0
    return v1
.end method

.method private synthetic lambda$new$0(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 187
    new-instance v0, Lmiui/util/HapticFeedbackUtil;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    return-void
.end method

.method private synthetic lambda$showToast$1(II)V
    .locals 2
    .param p1, "resourceId"    # I
    .param p2, "length"    # I

    .line 1285
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1286
    return-void
.end method

.method private lauchWeixinPaymentCode(Ljava/lang/String;)Z
    .locals 5
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1182
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1183
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.tencent.mm.plugin.offline.ui.WalletOfflineCoinPurseUI"

    const-string v3, "com.tencent.mm"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1185
    const-string v1, "key_entry_scene"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1186
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1187
    const/4 v1, 0x1

    return v1

    .line 1189
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1190
    .local v1, "dialogIntent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v4, "com.android.settings.KeyDownloadDialogActivity"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1191
    const-string/jumbo v2, "shortcut"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1192
    const-string v2, "packageName"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1193
    const-string v2, "dual"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1194
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f004e

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "title"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1195
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f0050

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "content"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1196
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1197
    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    .line 1198
    return v3
.end method

.method private launchAccessibilityEnvironmentSpeechRecognition()Z
    .locals 3

    .line 648
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 649
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.accessibility"

    const-string v2, "com.miui.accessibility.environment.sound.recognition.TransparentEsrSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 651
    const-string v1, "com.miui.accessibility.environment.sound.recognition.EnvSoundRecognitionService"

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAccessibilityFunctionEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 652
    const-string v1, "open"

    goto :goto_0

    :cond_0
    const-string v1, "close"

    .line 651
    :goto_0
    const-string v2, "OPEN_ESR"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 653
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchAccessibilityHearSound(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 663
    iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mSupportAccessibilitySubtitle:Z

    if-eqz v0, :cond_0

    .line 664
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "redirect accessibility hear sound success, action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 665
    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->redirectAccessibilityHearSoundFunction(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 667
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 668
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.accessibility"

    const-string v2, "com.miui.accessibility.asr.component.message.MessageActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 669
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchAccessibilityHearSoundSubtitle(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 680
    iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mSupportAccessibilitySubtitle:Z

    if-eqz v0, :cond_0

    .line 681
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "redirect accessibility hear sound subtitle success, action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 683
    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->redirectAccessibilityHearSoundFunction(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 685
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 686
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.accessibility"

    const-string v2, "com.miui.accessibility.asr.component.floatwindow.TransparentCaptionActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 688
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "floatingWindowType"    # Ljava/lang/String;

    .line 433
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 434
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.xiaomi.aiasst.vision"

    const-string v3, "com.xiaomi.aiasst.vision.control.translation.AiTranslateService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 436
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 437
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "from"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v2, "floatingWindowType"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 440
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 441
    const/4 v2, 0x1

    return v2
.end method

.method private launchAccessibilityVoiceControl()Z
    .locals 3

    .line 634
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 635
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.accessibility"

    const-string v2, "com.miui.accessibility.voiceaccess.settings.TransparentVoiceAccessSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 636
    const-string v1, "com.miui.accessibility.voiceaccess.VoiceAccessAccessibilityService"

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAccessibilityFunctionEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 637
    const-string v1, "open"

    goto :goto_0

    :cond_0
    const-string v1, "close"

    .line 636
    :goto_0
    const-string v2, "OPEN_VOICE_ACCESS"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 638
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchAlipayHealthCode(Ljava/lang/String;)Z
    .locals 5
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1234
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1235
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1236
    const-string v1, "com.eg.android.AlipayGphone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1237
    const/high16 v2, 0x14800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1239
    const-string v2, "alipays://platformapi/startapp?appId=68687564&chInfo=ch_xiaomi_quick&sceneCode=KF_CHANGSHANG&shareUserId=2088831085791813&partnerId=ch_xiaomi_quick&pikshemo=YES"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1240
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1241
    const/4 v1, 0x1

    return v1

    .line 1243
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1244
    .local v2, "dialogIntent":Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.KeyDownloadDialogActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1245
    const-string/jumbo v3, "shortcut"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1246
    const-string v3, "packageName"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1247
    const-string v1, "dual"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1248
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f0049

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "title"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1249
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f0050

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "content"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1250
    const v1, 0x10008000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1251
    invoke-direct {p0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    .line 1252
    return v3
.end method

.method private launchAlipayPaymentCode(Ljava/lang/String;)Z
    .locals 5
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1207
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1208
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.eg.android.AlipayGphone.FastStartActivity"

    const-string v3, "com.eg.android.AlipayGphone"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1210
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1211
    const/high16 v1, 0x14800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1213
    const-string v1, "alipayss://platformapi/startapp?appId=20000056&source=shortcut"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1214
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1215
    const/4 v1, 0x1

    return v1

    .line 1217
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1218
    .local v1, "dialogIntent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v4, "com.android.settings.KeyDownloadDialogActivity"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1219
    const-string/jumbo v2, "shortcut"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1220
    const-string v2, "packageName"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1221
    const-string v2, "dual"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1222
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f004a    # 1.12808E-28f

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "title"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1223
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f0050

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "content"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1224
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1225
    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    .line 1226
    return v3
.end method

.method private launchAlipayScanner(Ljava/lang/String;)Z
    .locals 9
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1143
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1145
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.alipay.mobile.scan.as.main.MainCaptureActivity"

    const-string v3, "com.eg.android.AlipayGphone"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1146
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1147
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1148
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v4, "app_id"

    const-string v5, "10000007"

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1151
    .local v4, "bundleTemp":Landroid/os/Bundle;
    const-string/jumbo v6, "source"

    const-string/jumbo v7, "shortcut"

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const-string v6, "appId"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const-string v5, "REALLY_STARTAPP"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1154
    const-string/jumbo v5, "showOthers"

    const-string v8, "YES"

    invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    const-string/jumbo v5, "startFromExternal"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1156
    const-string v5, "REALLY_DOSTARTAPP"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1157
    const-string/jumbo v5, "sourceId"

    invoke-virtual {v4, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    const-string v5, "ap_framework_sceneId"

    const-string v8, "20000001"

    invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const-string v5, "mExtras"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1161
    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1162
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1163
    return v6

    .line 1165
    :cond_0
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 1166
    .local v5, "dialogIntent":Landroid/content/Intent;
    const-string v6, "com.android.settings"

    const-string v8, "com.android.settings.KeyDownloadDialogActivity"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1167
    invoke-virtual {v5, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1168
    const-string v6, "packageName"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1169
    const-string v3, "dual"

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1170
    iget-object v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v7, 0x110f004b

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v7, "title"

    invoke-virtual {v5, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1171
    iget-object v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v7, 0x110f0050

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v7, "content"

    invoke-virtual {v5, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1172
    invoke-virtual {v5, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1173
    invoke-direct {p0, v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    .line 1174
    return v6
.end method

.method private launchApp(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1386
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method private launchApp(Landroid/content/Intent;Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 1393
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 1394
    const/high16 v0, 0x14000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    .line 1396
    :cond_0
    const/high16 v0, 0x14800000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1400
    :goto_0
    const/4 v0, 0x0

    .line 1401
    .local v0, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1402
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1403
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1404
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 1403
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1405
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 1407
    :cond_2
    :goto_1
    const/4 v1, 0x0

    if-nez v0, :cond_3

    .line 1408
    const-string v2, "package name is null"

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1409
    return v1

    .line 1411
    :cond_3
    const/high16 v2, 0xd0000

    .line 1413
    .local v2, "flags":I
    iget-object v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1414
    invoke-static {}, Lmiui/securityspace/CrossUserUtils;->getCurrentUserId()I

    move-result v4

    .line 1413
    invoke-virtual {v3, p1, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v3

    .line 1415
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_4

    .line 1417
    :try_start_0
    iget-object v4, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, p1, p2, v5}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1418
    const/4 v1, 0x1

    return v1

    .line 1421
    :catch_0
    move-exception v4

    .line 1422
    .local v4, "e":Ljava/lang/IllegalStateException;
    const-string v5, "IllegalStateException"

    invoke-static {v5, v4}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v4    # "e":Ljava/lang/IllegalStateException;
    goto :goto_2

    .line 1419
    :catch_1
    move-exception v4

    .line 1420
    .local v4, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "ActivityNotFoundException"

    invoke-static {v5, v4}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1423
    .end local v4    # "e":Landroid/content/ActivityNotFoundException;
    :goto_2
    goto :goto_3

    .line 1425
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "launch app fail  package:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1428
    :goto_3
    return v1
.end method

.method private launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .line 965
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, p1, p2, v1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 966
    const/4 v0, 0x1

    return v0

    .line 969
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "launchAppSimple IllegalStateException"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 967
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 968
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v1, "launchAppSimple ActivityNotFoundException"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 971
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    nop

    .line 972
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method private launchAuPay()Z
    .locals 3

    .line 1337
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1338
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1339
    const-string v1, "jp.auone.wallet"

    const-string v2, "jp.auone.wallet.ui.main.DeviceCredentialSchemeActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1340
    const-string/jumbo v1, "shortcut_start"

    const-string v2, "jp.auone.wallet.qr"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1341
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1342
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1343
    const v1, 0x110f004c

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->showToast(II)V

    .line 1344
    return v2

    .line 1346
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method private launchCalculator()Z
    .locals 4

    .line 1260
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1261
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.calculator"

    const-string v3, "com.miui.calculator.cal.CalculatorActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1262
    const/high16 v1, 0x200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1263
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchCamera(Ljava/lang/String;)Z
    .locals 4
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1031
    const-string v0, "double_click_volume_down"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1032
    const-string v0, "launch_camera_and_take_photo"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1033
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V

    .line 1036
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1037
    .local v0, "cameraIntent":Landroid/content/Intent;
    const/high16 v1, 0x200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1038
    const-string v1, "ShowCameraWhenLocked"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1039
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v3, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v3, :cond_4

    .line 1040
    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    .line 1041
    invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyguardActive()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1042
    const-string v1, "power_double_tap"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    nop

    .line 1040
    :goto_1
    const-string v1, "StartActivityWhenLocked"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1044
    :cond_4
    const-string v1, "android.media.action.STILL_IMAGE_CAMERA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1045
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1046
    const v2, 0x110f00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1045
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1047
    .local v1, "mCameraComponentName":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1049
    const-string v2, "com.android.systemui.camera_launch_source"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1050
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    return v2
.end method

.method private launchCamera(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "shortcut"    # Ljava/lang/String;
    .param p2, "mode"    # Ljava/lang/String;

    .line 1057
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1058
    .local v0, "cameraIntent":Landroid/content/Intent;
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1059
    const-string v1, "ShowCameraWhenLocked"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1060
    const-string v1, "StartActivityWhenLocked"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1061
    const-string v1, "android.media.action.VOICE_COMMAND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1062
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    .line 1063
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x110f00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1062
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1064
    .local v1, "mCameraComponentName":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1065
    const-string v2, "android.intent.extra.CAMERA_MODE"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1066
    nop

    .line 1067
    const-string v2, "android-app://com.android.camera"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1066
    const-string v3, "android.intent.extra.REFERRER"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1068
    const-string v2, "com.android.systemui.camera_launch_source"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1069
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    return v2
.end method

.method private launchCameraAndTakePhoto()Z
    .locals 1

    .line 488
    const-string v0, "launch_camera_and_take_photo"

    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private launchDumpLog()Z
    .locals 3

    .line 1270
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1271
    .local v0, "dumpLogIntent":Landroid/content/Intent;
    const-string v1, "com.miui.bugreport"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1272
    const-string v1, "com.miui.bugreport.service.action.DUMPLOG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1273
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1274
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1275
    const v1, 0x110f037c

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->showToast(II)V

    .line 1276
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    if-eqz v1, :cond_0

    .line 1277
    invoke-interface {v1}, Lcom/miui/server/stability/StabilityLocalServiceInternal;->captureDumpLog()V

    .line 1279
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method private launchDumpLogOrContact(Ljava/lang/String;)Z
    .locals 3
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1294
    iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsVoiceCapable:Z

    if-nez v0, :cond_0

    .line 1295
    const-string v0, "mIsVoiceCapable false, so lunch emergencyDialer"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1296
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1297
    .local v0, "intent":Landroid/content/Intent;
    nop

    .line 1298
    const-string v1, "com.android.phone/com.android.phone.EmergencyDialer"

    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 1299
    .local v1, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1300
    const-string/jumbo v2, "shortcut"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1301
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    return v2

    .line 1303
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "componentName":Landroid/content/ComponentName;
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLog()Z

    .line 1305
    const/4 v0, 0x1

    return v0
.end method

.method private launchGlobalPowerGuide()Z
    .locals 3

    .line 492
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 495
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "pad"

    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "fold"

    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 496
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isLargeScreen()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 498
    :cond_0
    const-string v1, "ruyi"

    sget-object v2, Lmiui/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsFolded:Z

    if-eqz v1, :cond_1

    .line 500
    const-string v1, "com.miui.miinput.action.SMALL_SCREEN_GLOBAL_POWER_GUIDE"

    .local v1, "actionName":Ljava/lang/String;
    goto :goto_2

    .line 502
    .end local v1    # "actionName":Ljava/lang/String;
    :cond_1
    sget-boolean v1, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI_POWER_GUIDE:Z

    if-eqz v1, :cond_2

    .line 503
    const-string v1, "com.miui.miinput.action.KDDI_GLOBAL_POWER_GUIDE"

    goto :goto_0

    :cond_2
    const-string v1, "com.miui.miinput.action.GLOBAL_POWER_GUIDE"

    .restart local v1    # "actionName":Ljava/lang/String;
    :goto_0
    goto :goto_2

    .line 497
    .end local v1    # "actionName":Ljava/lang/String;
    :cond_3
    :goto_1
    const-string v1, "com.miui.miinput.action.ROTATION_FOLLOWS_SENSOR_GLOBAL_POWER_GUIDE"

    .line 505
    .restart local v1    # "actionName":Ljava/lang/String;
    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 506
    const-string v2, "com.miui.securitycore"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 507
    const v2, 0x10008000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 508
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    return v2
.end method

.method private launchGooglePay()Z
    .locals 1

    .line 1368
    const/4 v0, 0x0

    return v0
.end method

.method private launchGoogleSearch(Ljava/lang/String;)Z
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .line 711
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    if-nez v0, :cond_0

    .line 712
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 714
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v0, :cond_1

    .line 715
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 716
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "android.intent.extra.ASSIST_INPUT_DEVICE_ID"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 717
    invoke-direct {p0, p1, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->setGoogleSearchInvocationTypeKey(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 718
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchAssistAction(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v1

    return v1

    .line 721
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private launchHome(Ljava/lang/String;)Z
    .locals 2
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 573
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode;->launchHome(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    const/4 v0, 0x1

    return v0

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    if-nez v0, :cond_1

    .line 577
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 579
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v1, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v1, :cond_2

    .line 580
    check-cast v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchHome()Z

    move-result v0

    return v0

    .line 582
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private launchMiNotes(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "shortcut"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .line 909
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.pad.notes.action.INSERT_OR_EDIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 910
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "vnd.android.cursor.item/text_note"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    const-string/jumbo v1, "shortcut"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 912
    const-string v1, "com.miui.notes"

    .line 913
    .local v1, "packageName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 914
    const/4 v2, 0x0

    if-nez p2, :cond_0

    .line 915
    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z

    move-result v2

    return v2

    .line 917
    :cond_0
    const-string v3, "scene"

    invoke-virtual {p2, v3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 918
    .local v4, "scene":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 919
    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z

    move-result v2

    return v2

    .line 921
    :cond_1
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 922
    const-string v3, "com.miui.creation"

    invoke-direct {p0, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAppInstalled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 923
    const-string v1, "com.miui.creation"

    .line 924
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 925
    const-string v3, "com.miui.creation.action.INSERT_CREATION"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 926
    const-string/jumbo v3, "vnd.android.cursor.item/create_creation"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 927
    const-string v3, "com.miui.creation already installed"

    invoke-static {v3}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 929
    :cond_2
    const/high16 v3, 0x14000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 930
    const-string v3, "keyguard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v5, 0x1

    if-nez v3, :cond_5

    .line 931
    const-string v3, "off_screen"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 935
    :cond_3
    const/4 v2, 0x0

    .line 936
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v3, "app"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 937
    iget-object v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v3, v1, v5, v6}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;ZZ)Landroid/app/ActivityOptions;

    move-result-object v3

    .line 939
    .local v3, "activityOptions":Landroid/app/ActivityOptions;
    if-eqz v3, :cond_4

    .line 940
    invoke-virtual {v3}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v2

    .line 943
    .end local v3    # "activityOptions":Landroid/app/ActivityOptions;
    :cond_4
    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z

    move-result v3

    return v3

    .line 932
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_5
    :goto_0
    const-string v3, "StartActivityWhenLocked"

    invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 933
    invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z

    move-result v2

    return v2
.end method

.method private launchMiPay(Ljava/lang/String;)Z
    .locals 3
    .param p1, "eventSource"    # Ljava/lang/String;

    .line 987
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 988
    .local v0, "nfcIntent":Landroid/content/Intent;
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 989
    const-string v1, "StartActivityWhenLocked"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 990
    const-string v1, "com.miui.intent.action.DOUBLE_CLICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 991
    const-string v1, "event_source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 992
    const-string v1, "com.miui.tsmclient"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 993
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchMiuiSoS()Z
    .locals 2

    .line 745
    new-instance v0, Landroid/content/Intent;

    const-string v1, "miui.intent.action.LAUNCH_SOS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 746
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 747
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private launchMusicReader()Z
    .locals 4

    .line 1106
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1107
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.player"

    const-string v3, "com.miui.player.ui.MusicBrowserActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    .local v1, "comp":Landroid/content/ComponentName;
    const-string v2, "miui-music://radar?miref=com.miui.knock"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1110
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1111
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    return v2
.end method

.method private launchRecents(Ljava/lang/String;)Z
    .locals 1
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 586
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode;->launchRecents(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    const/4 v0, 0x1

    return v0

    .line 589
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    if-nez v0, :cond_1

    .line 590
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 592
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mStatusBarManagerInternal:Lcom/android/server/statusbar/StatusBarManagerInternal;

    if-eqz v0, :cond_2

    .line 593
    invoke-interface {v0}, Lcom/android/server/statusbar/StatusBarManagerInternal;->toggleRecentApps()V

    .line 595
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method private launchScreenRecorder()Z
    .locals 3

    .line 1076
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1077
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "miui.intent.screenrecorder.RECORDER_SERVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1078
    const-string v1, "com.miui.screenrecorder"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1079
    const-string v1, "is_start_immediately"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1080
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1081
    const/4 v1, 0x1

    return v1
.end method

.method private launchSmartHomeService(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "shortcut"    # Ljava/lang/String;
    .param p2, "extra"    # Landroid/os/Bundle;

    .line 762
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 763
    .local v0, "smartHomeIntent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.smarthomeplus.UWBEntryService"

    const-string v3, "com.miui.smarthomeplus"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 765
    const-string v1, "long_press_power_key"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 766
    const-string v1, "extra_long_press_power_function"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 767
    nop

    .line 768
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 767
    invoke-direct {p0, v3, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getLongPressPowerKeyFunctionIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 770
    :cond_0
    const-string v1, "event_source"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 772
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 775
    goto :goto_0

    .line 773
    :catch_0
    move-exception v1

    .line 774
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;)V

    .line 776
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x1

    return v1
.end method

.method private launchSoundRecorder()Z
    .locals 4

    .line 1096
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1097
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.soundrecorder"

    const-string v3, "com.android.soundrecorder.SoundRecorder"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    .local v1, "comp":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1099
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v2

    return v2
.end method

.method private launchTorch(Landroid/os/Bundle;)Z
    .locals 6
    .param p1, "extra"    # Landroid/os/Bundle;

    .line 1000
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    const-string/jumbo v2, "skip_telecom_check"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1001
    invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getTelecommService()Landroid/telecom/TelecomManager;

    move-result-object v2

    .line 1002
    .local v2, "telecomManager":Landroid/telecom/TelecomManager;
    iget-boolean v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWifiOnly:Z

    if-nez v3, :cond_1

    if-eqz v2, :cond_0

    .line 1003
    invoke-virtual {v2}, Landroid/telecom/TelecomManager;->getCallState()I

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    move v3, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v3, v0

    .line 1004
    .local v3, "phoneIdle":Z
    :goto_1
    if-nez v3, :cond_2

    .line 1005
    const-string v0, "not launch torch,because telecom"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 1006
    return v1

    .line 1010
    .end local v2    # "telecomManager":Landroid/telecom/TelecomManager;
    .end local v3    # "phoneIdle":Z
    :cond_2
    iget-boolean v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHasCameraFlash:Z

    if-nez v2, :cond_3

    .line 1011
    const-string v0, "not have camera flash"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 1012
    return v1

    .line 1014
    :cond_3
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "torch_state"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    goto :goto_2

    :cond_4
    move v2, v1

    .line 1016
    .local v2, "isOpen":Z
    :goto_2
    new-instance v3, Landroid/content/Intent;

    const-string v4, "miui.intent.action.TOGGLE_TORCH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1017
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1018
    const-string v4, "miui.intent.extra.IS_ENABLE"

    if-eqz p1, :cond_5

    .line 1019
    const-string v5, "extra_torch_enabled"

    invoke-virtual {p1, v5, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_3

    .line 1021
    :cond_5
    if-nez v2, :cond_6

    move v1, v0

    :cond_6
    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1023
    :goto_3
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1024
    return v0
.end method

.method private launchVoiceAssistant(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "shortcut"    # Ljava/lang/String;
    .param p2, "extra"    # Landroid/os/Bundle;

    .line 874
    const-string v0, "powerGuide"

    const-string v1, "extra_long_press_power_function"

    const-string v2, "android.intent.action.ASSIST"

    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 875
    .local v4, "intent":Landroid/content/Intent;
    const-string v5, "keyboard"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string/jumbo v6, "voice_assist_start_from_key"

    if-eqz v5, :cond_0

    .line 876
    :try_start_1
    const-string v0, "external_keyboard_xiaoai_key"

    invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 878
    :cond_0
    const-string v5, "long_press_power_key"

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz p2, :cond_1

    .line 879
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 880
    nop

    .line 881
    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 880
    invoke-direct {p0, v2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getLongPressPowerKeyFunctionIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 882
    .end local v4    # "intent":Landroid/content/Intent;
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object v4, v1

    goto :goto_0

    .line 884
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_1
    if-eqz p2, :cond_2

    const-string v0, "ai_key"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 885
    const-string v0, "key_action"

    const-string v1, "extra_key_action"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 886
    const-string v0, "key_event_time"

    const-string v1, "extra_key_event_time"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 888
    :cond_2
    :goto_0
    invoke-virtual {v4, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 889
    const-string v0, "app.send.wakeup.command"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 892
    :goto_1
    const-string v0, "com.miui.voiceassist"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 893
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    .line 895
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x110f00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 894
    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 896
    .local v0, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 897
    const-string v1, "launchVoiceAssistant startForegroundServiceAsUser"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 898
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->startForegroundServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_3

    const/4 v3, 0x1

    :cond_3
    return v3

    .line 899
    .end local v0    # "componentName":Landroid/content/ComponentName;
    .end local v4    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 900
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Exception"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 902
    .end local v0    # "e":Ljava/lang/Exception;
    return v3
.end method

.method private launchWexinScanner(Ljava/lang/String;)Z
    .locals 5
    .param p1, "shortcut"    # Ljava/lang/String;

    .line 1118
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1119
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1120
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"

    const-string v3, "com.tencent.mm"

    invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1122
    invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1123
    const/4 v1, 0x1

    return v1

    .line 1125
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1126
    .local v1, "dialogIntent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v4, "com.android.settings.KeyDownloadDialogActivity"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1127
    const-string/jumbo v2, "shortcut"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1128
    const-string v2, "packageName"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1129
    const-string v2, "dual"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1130
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f004f

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v4, "title"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1131
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const v4, 0x110f0050

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "content"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1132
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1133
    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    .line 1134
    return v3
.end method

.method private makeAllUserToastAndShow(Ljava/lang/String;I)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .line 464
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 465
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 466
    return-void
.end method

.method private redirectAccessibilityHearSoundFunction(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "function"    # Ljava/lang/String;

    .line 396
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 397
    const-string v0, "redirect accessibility hear sound function fail! because action is null"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 399
    return v1

    .line 402
    :cond_0
    const-string v0, "back_double_tap"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 403
    const-string v0, "back_triple_tap"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 404
    :cond_1
    const/4 v0, 0x0

    .line 405
    .local v0, "redirectFunction":Ljava/lang/String;
    const-string v2, "hear_sound"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 406
    const-string v0, "live_subtitles_full_screen"

    .line 408
    const-string/jumbo v2, "startAiSubtitlesFullscreen"

    invoke-direct {p0, p1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 410
    :cond_2
    const-string v2, "hear_sound_subtitle"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 412
    const-string v0, "live_subtitles"

    .line 413
    const-string/jumbo v2, "startAiSubtitlesWindow"

    invoke-direct {p0, p1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z

    .line 417
    :cond_3
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 418
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, -0x2

    invoke-static {v1, p1, v0, v2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 420
    const/4 v1, 0x1

    return v1

    .line 423
    .end local v0    # "redirectFunction":Ljava/lang/String;
    :cond_4
    return v1
.end method

.method public static sendRecordCountEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "event"    # Ljava/lang/String;

    .line 1436
    const-string v0, "count_event"

    .line 1437
    .local v0, "STAT_TYPE_COUNT_EVENT":Ljava/lang/String;
    const-string v1, "com.miui.gallery"

    .line 1438
    .local v1, "GALLERY_PKG_NAME":Ljava/lang/String;
    const-string v2, "com.miui.gallery.intent.action.SEND_STAT"

    .line 1439
    .local v2, "ACTION_SEND_STAT":Ljava/lang/String;
    const-string/jumbo v3, "stat_type"

    .line 1440
    .local v3, "EXTRA_STAT_TYPE":Ljava/lang/String;
    const-string v4, "category"

    .line 1441
    .local v4, "EXTRA_CATEGORY":Ljava/lang/String;
    const-string v5, "event"

    .line 1443
    .local v5, "EXTRA_EVENT":Ljava/lang/String;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.miui.gallery.intent.action.SEND_STAT"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1444
    .local v6, "intent":Landroid/content/Intent;
    const-string v7, "com.miui.gallery"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1445
    const-string/jumbo v7, "stat_type"

    const-string v8, "count_event"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1446
    const-string v7, "category"

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1447
    const-string v7, "event"

    invoke-virtual {v6, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1448
    invoke-virtual {p0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1449
    return-void
.end method

.method private setAccessibilityTalkBackState(Ljava/lang/String;)Z
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .line 614
    nop

    .line 615
    const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    .line 617
    .local v0, "componentName":Landroid/content/ComponentName;
    const-string v1, "key_combination_volume_up_volume_down"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, -0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 618
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-static {v1, v0, v4, v2}, Lcom/android/internal/accessibility/util/AccessibilityUtils;->setAccessibilityServiceState(Landroid/content/Context;Landroid/content/ComponentName;ZI)V

    .line 619
    return v3

    .line 621
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    .line 622
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAccessibilityFunctionEnabled(Ljava/lang/String;)Z

    move-result v4

    xor-int/2addr v4, v3

    .line 621
    invoke-static {v1, v0, v4, v2}, Lcom/android/internal/accessibility/util/AccessibilityUtils;->setAccessibilityServiceState(Landroid/content/Context;Landroid/content/ComponentName;ZI)V

    .line 624
    return v3
.end method

.method private setGoogleSearchInvocationTypeKey(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "args"    # Landroid/os/Bundle;

    .line 725
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    return-void

    .line 728
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v0, "long_press_home_key"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v0, "long_press_power_key"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-string v1, "invocation_type"

    packed-switch v0, :pswitch_data_0

    .line 738
    const/4 v0, 0x2

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 734
    :pswitch_0
    const/4 v0, 0x5

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 736
    goto :goto_2

    .line 730
    :pswitch_1
    const/4 v0, 0x6

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 732
    nop

    .line 742
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4b0ca89a -> :sswitch_1
        0x5add439e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showMenu(Ljava/lang/String;)Z
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .line 445
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    instance-of v1, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    if-eqz v1, :cond_0

    .line 446
    check-cast v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerShowMenu(Ljava/lang/String;)Z

    .line 448
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private showToast(II)V
    .locals 2
    .param p1, "resourceId"    # I
    .param p2, "length"    # I

    .line 1284
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/util/ShortCutActionsUtils;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1287
    return-void
.end method

.method private takeScreenshot(I)Z
    .locals 4
    .param p1, "type"    # I

    .line 1327
    new-instance v0, Lcom/android/internal/util/ScreenshotRequest$Builder;

    invoke-direct {v0, p1}, Lcom/android/internal/util/ScreenshotRequest$Builder;-><init>(I)V

    .line 1328
    invoke-virtual {v0}, Lcom/android/internal/util/ScreenshotRequest$Builder;->build()Lcom/android/internal/util/ScreenshotRequest;

    move-result-object v0

    .line 1329
    .local v0, "request":Lcom/android/internal/util/ScreenshotRequest;
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mScreenshotHelper:Lcom/android/internal/util/ScreenshotHelper;

    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/ScreenshotHelper;->takeScreenshot(Lcom/android/internal/util/ScreenshotRequest;Landroid/os/Handler;Ljava/util/function/Consumer;)V

    .line 1330
    const/4 v1, 0x1

    return v1
.end method

.method private takeScreenshotWithBundle([F)Z
    .locals 5
    .param p1, "pathList"    # [F

    .line 1355
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1356
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "partial.screenshot.points"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 1357
    new-instance v1, Lcom/android/internal/util/ScreenshotRequest$Builder;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Lcom/android/internal/util/ScreenshotRequest$Builder;-><init>(I)V

    .line 1358
    invoke-virtual {v1, v0}, Lcom/android/internal/util/ScreenshotRequest$Builder;->setExtraBundle(Landroid/os/Bundle;)Lcom/android/internal/util/ScreenshotRequest$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/util/ScreenshotRequest$Builder;->build()Lcom/android/internal/util/ScreenshotRequest;

    move-result-object v1

    .line 1359
    .local v1, "request":Lcom/android/internal/util/ScreenshotRequest;
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mScreenshotHelper:Lcom/android/internal/util/ScreenshotHelper;

    iget-object v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/android/internal/util/ScreenshotHelper;->takeScreenshot(Lcom/android/internal/util/ScreenshotRequest;Landroid/os/Handler;Ljava/util/function/Consumer;)V

    .line 1360
    const/4 v2, 0x1

    return v2
.end method

.method private triggerHapticFeedback(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2
    .param p1, "triggered"    # Z
    .param p2, "shortcut"    # Ljava/lang/String;
    .param p3, "function"    # Ljava/lang/String;
    .param p4, "hapticFeedback"    # Z
    .param p5, "effectKey"    # Ljava/lang/String;

    .line 600
    if-eqz p1, :cond_0

    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHapticFeedbackUtil:Lmiui/util/HapticFeedbackUtil;

    if-eqz v0, :cond_0

    .line 601
    const/4 v1, 0x0

    invoke-virtual {v0, p5, v1}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z

    .line 603
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "shortcut:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " trigger function:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 605
    return-void
.end method


# virtual methods
.method public bootComplete()V
    .locals 1

    .line 210
    invoke-static {}, Lmiui/hardware/input/InputFeature;->supportAccessibilityLiveSubtitles()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mSupportAccessibilitySubtitle:Z

    .line 211
    return-void
.end method

.method public getIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .line 563
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 564
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 565
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 569
    return-object v0
.end method

.method getTelecommService()Landroid/telecom/TelecomManager;
    .locals 2

    .line 976
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "telecom"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/TelecomManager;

    return-object v0
.end method

.method public launchAiShortcut()Z
    .locals 4

    .line 849
    :try_start_0
    const-string v0, "knock launch ai shortcut"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    .line 850
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 851
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "is_show_dialog"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 852
    const-string v1, "from"

    const-string v2, "knock"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 853
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 854
    const-string v1, "com.miui.voiceassist/com.xiaomi.voiceassistant.AiSettings.AiShortcutActivity"

    .line 855
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    .line 856
    .local v1, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 857
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 858
    const/4 v2, 0x1

    return v2

    .line 863
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "componentName":Landroid/content/ComponentName;
    :catch_0
    move-exception v0

    .line 864
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "RuntimeException"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 861
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 862
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "IllegalStateException"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v0    # "e":Ljava/lang/IllegalStateException;
    goto :goto_0

    .line 859
    :catch_2
    move-exception v0

    .line 860
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "SecurityException"

    invoke-static {v1, v0}, Lcom/android/server/policy/MiuiInputLog;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 865
    .end local v0    # "e":Ljava/lang/SecurityException;
    :goto_0
    nop

    .line 866
    :goto_1
    const/4 v0, 0x0

    return v0
.end method

.method public launchAppSmallWindow(Landroid/os/Bundle;)Z
    .locals 8
    .param p1, "extras"    # Landroid/os/Bundle;

    .line 532
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 533
    return v0

    .line 535
    :cond_0
    const-string v1, "packageName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 536
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "className"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 537
    .local v2, "className":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    .line 540
    :cond_1
    invoke-virtual {p0, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 542
    .local v3, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v4

    .line 543
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    .line 542
    invoke-virtual {v4, v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isFreeFormExit(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 544
    return v0

    .line 546
    :cond_2
    iget-object v4, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v1, v5, v0}, Landroid/util/MiuiMultiWindowUtils;->getActivityOptions(Landroid/content/Context;Ljava/lang/String;ZZ)Landroid/app/ActivityOptions;

    move-result-object v4

    .line 549
    .local v4, "options":Landroid/app/ActivityOptions;
    if-eqz v4, :cond_4

    .line 550
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v6

    .line 551
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    .line 550
    invoke-virtual {v6, v1, v7}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isSplitExist(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 552
    return v0

    .line 554
    :cond_3
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v6

    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    goto :goto_0

    .line 557
    :cond_4
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v3, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 559
    :goto_0
    return v5

    .line 538
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "options":Landroid/app/ActivityOptions;
    :cond_5
    :goto_1
    return v0
.end method

.method public launchControlCenter()Z
    .locals 3

    .line 819
    new-instance v0, Landroid/content/Intent;

    const-string v1, "action_panels_operation"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 820
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "operation"

    const-string v2, "reverse_quick_settings_panel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 821
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 822
    const/4 v1, 0x1

    return v1
.end method

.method public launchNotificationCenter()Z
    .locals 3

    .line 829
    new-instance v0, Landroid/content/Intent;

    const-string v1, "action_panels_operation"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 830
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "operation"

    const-string v2, "reverse_notifications_panel"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 831
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 832
    const/4 v1, 0x1

    return v1
.end method

.method public mute()Z
    .locals 3

    .line 839
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/provider/MiuiSettings$SoundMode;->isSilenceModeOn(Landroid/content/Context;)Z

    move-result v0

    .line 840
    .local v0, "isSilenceModeOn":Z
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    xor-int/lit8 v2, v0, 0x1

    invoke-static {v1, v2}, Landroid/provider/MiuiSettings$SoundMode;->setSilenceModeOn(Landroid/content/Context;Z)V

    .line 841
    const/4 v1, 0x1

    return v1
.end method

.method public notifyFoldStatus(Z)V
    .locals 0
    .param p1, "folded"    # Z

    .line 512
    iput-boolean p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsFolded:Z

    .line 513
    return-void
.end method

.method public notifyKidSpaceChanged(Z)V
    .locals 0
    .param p1, "inKidSpace"    # Z

    .line 1432
    iput-boolean p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsKidMode:Z

    .line 1433
    return-void
.end method

.method public setWifiOnly(Z)V
    .locals 0
    .param p1, "wifiOnly"    # Z

    .line 980
    iput-boolean p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWifiOnly:Z

    .line 981
    return-void
.end method

.method public systemReady()V
    .locals 2

    .line 202
    const-class v0, Lcom/android/server/policy/WindowManagerPolicy;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/policy/WindowManagerPolicy;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    .line 203
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mPowerManager:Landroid/os/PowerManager;

    .line 204
    const-class v0, Lcom/android/server/statusbar/StatusBarManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/statusbar/StatusBarManagerInternal;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mStatusBarManagerInternal:Lcom/android/server/statusbar/StatusBarManagerInternal;

    .line 205
    const-class v0, Lcom/miui/server/stability/StabilityLocalServiceInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/stability/StabilityLocalServiceInternal;

    iput-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mStabilityLocalServiceInternal:Lcom/miui/server/stability/StabilityLocalServiceInternal;

    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsSystemReady:Z

    .line 207
    return-void
.end method

.method public triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
    .locals 6
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z

    .line 758
    const-string v5, "mesh_heavy"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZLjava/lang/String;)Z
    .locals 8
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;
    .param p4, "hapticFeedback"    # Z
    .param p5, "effectKey"    # Ljava/lang/String;

    .line 215
    iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsSystemReady:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 216
    const-string v0, "Failed to trigger shortcut function, system is not ready"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 217
    return v1

    .line 221
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsKidMode:Z

    if-eqz v0, :cond_1

    .line 222
    const-string v0, "Children\'s space does not trigger shortcut gestures"

    invoke-static {v0}, Lcom/android/server/policy/MiuiInputLog;->defaults(Ljava/lang/String;)V

    .line 223
    return v1

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    if-eqz v0, :cond_3

    if-eqz p3, :cond_2

    const-string v0, "disable_shortcut_track"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mShortcutOneTrackHelper:Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;

    invoke-virtual {v0, p2, p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_3
    const/4 v0, 0x0

    .line 232
    .local v0, "triggered":Z
    if-nez p5, :cond_4

    .line 233
    const-string/jumbo p5, "virtual_key_longpress"

    .line 237
    :cond_4
    iget-object v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy;->isUserSetupComplete()Z

    move-result v2

    const-string v3, "dump_log_or_secret_code"

    const-string v4, "dump_log"

    if-nez v2, :cond_7

    .line 239
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 240
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsVoiceCapable:Z

    if-eqz v1, :cond_5

    goto :goto_0

    .line 244
    :cond_5
    const-string/jumbo v1, "user setup not complete"

    invoke-static {v1}, Lcom/android/server/policy/MiuiInputLog;->major(Ljava/lang/String;)V

    goto :goto_1

    .line 241
    :cond_6
    :goto_0
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLog()Z

    move-result v0

    .line 242
    move-object v2, p0

    move v3, v0

    move-object v4, p2

    move-object v5, p1

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v2 .. v7}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerHapticFeedback(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 246
    :goto_1
    return v0

    .line 248
    :cond_7
    const-string/jumbo v2, "screen_shot"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v5, 0x1

    if-eqz v2, :cond_8

    .line 249
    invoke-direct {p0, v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z

    move-result v0

    .line 250
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "screenshot"

    const-string v3, "key_shortcut"

    invoke-static {v1, v2, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->sendRecordCountEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 251
    :cond_8
    const-string v2, "partial_screen_shot"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 252
    if-eqz p3, :cond_9

    .line 253
    const-string v1, "partial.screenshot.points"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshotWithBundle([F)Z

    move-result v0

    goto/16 :goto_2

    .line 255
    :cond_9
    const/16 v1, 0x64

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z

    move-result v0

    goto/16 :goto_2

    .line 257
    :cond_a
    const-string/jumbo v2, "screenshot_without_anim"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 258
    const/16 v1, 0x63

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z

    move-result v0

    goto/16 :goto_2

    .line 259
    :cond_b
    const-string/jumbo v2, "stylus_partial_screenshot"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 260
    const/16 v1, 0x62

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z

    move-result v0

    goto/16 :goto_2

    .line 261
    :cond_c
    const-string v2, "launch_voice_assistant"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 262
    invoke-direct {p0, p2, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchVoiceAssistant(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    .line 263
    const-string p5, "screen_button_voice_assist"

    goto/16 :goto_2

    .line 264
    :cond_d
    const-string v2, "launch_ai_shortcut"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 265
    invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAiShortcut()Z

    move-result v0

    goto/16 :goto_2

    .line 266
    :cond_e
    const-string v2, "launch_alipay_scanner"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 267
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAlipayScanner(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 268
    :cond_f
    const-string v2, "launch_alipay_payment_code"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 269
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAlipayPaymentCode(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 270
    :cond_10
    const-string v2, "launch_alipay_health_code"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 271
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAlipayHealthCode(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 272
    :cond_11
    const-string v2, "launch_wechat_scanner"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 273
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchWexinScanner(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 274
    :cond_12
    const-string v2, "launch_wechat_payment_code"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 275
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->lauchWeixinPaymentCode(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 276
    :cond_13
    const-string/jumbo v2, "turn_on_torch"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 277
    invoke-direct {p0, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchTorch(Landroid/os/Bundle;)Z

    move-result v0

    goto/16 :goto_2

    .line 278
    :cond_14
    const-string v2, "launch_calculator"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 279
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCalculator()Z

    move-result v0

    goto/16 :goto_2

    .line 280
    :cond_15
    const-string v2, "launch_camera"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 281
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 282
    :cond_16
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 283
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLog()Z

    move-result v0

    goto/16 :goto_2

    .line 284
    :cond_17
    const-string v2, "launch_control_center"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 285
    invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchControlCenter()Z

    move-result v0

    goto/16 :goto_2

    .line 286
    :cond_18
    const-string v2, "launch_notification_center"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 287
    invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchNotificationCenter()Z

    move-result v0

    goto/16 :goto_2

    .line 288
    :cond_19
    const-string v2, "mute"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 289
    invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->mute()Z

    move-result v0

    goto/16 :goto_2

    .line 290
    :cond_1a
    const-string v2, "launch_google_search"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 291
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchGoogleSearch(Ljava/lang/String;)Z

    move-result v0

    .line 292
    const-string p5, "screen_button_voice_assist"

    goto/16 :goto_2

    .line 293
    :cond_1b
    const-string v2, "go_to_sleep"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 294
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->goToSleep(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 295
    :cond_1c
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 296
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLogOrContact(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 297
    :cond_1d
    const-string v2, "au_pay"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 298
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAuPay()Z

    move-result v0

    goto/16 :goto_2

    .line 299
    :cond_1e
    const-string v2, "google_pay"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 300
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchGooglePay()Z

    move-result v0

    goto/16 :goto_2

    .line 301
    :cond_1f
    const-string v2, "mi_pay"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 302
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchMiPay(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 303
    :cond_20
    const-string v2, "note"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 304
    invoke-direct {p0, p2, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchMiNotes(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    goto/16 :goto_2

    .line 305
    :cond_21
    const-string v2, "launch_smarthome"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 306
    invoke-direct {p0, p2, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchSmartHomeService(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    goto/16 :goto_2

    .line 307
    :cond_22
    const-string v2, "find_device_locate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 308
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->findDeviceLocate()Z

    move-result v0

    goto/16 :goto_2

    .line 309
    :cond_23
    const-string v2, "launch_sound_recorder"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 310
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchSoundRecorder()Z

    move-result v0

    goto/16 :goto_2

    .line 311
    :cond_24
    const-string v2, "launch_camera_capture"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 312
    const-string v1, "CAPTURE"

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 313
    :cond_25
    const-string v2, "launch_camera_video"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 314
    const-string v1, "VIDEO"

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 315
    :cond_26
    const-string/jumbo v2, "vibrate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 316
    invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->vibrate()Z

    move-result v0

    goto/16 :goto_2

    .line 317
    :cond_27
    const-string v2, "launch_screen_recorder"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 318
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchScreenRecorder()Z

    move-result v0

    goto/16 :goto_2

    .line 319
    :cond_28
    const-string v2, "miui_talkback"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 320
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->setAccessibilityTalkBackState(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 321
    :cond_29
    const-string/jumbo v2, "voice_control"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 322
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityVoiceControl()Z

    move-result v0

    goto/16 :goto_2

    .line 323
    :cond_2a
    const-string v2, "environment_speech_recognition"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 324
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityEnvironmentSpeechRecognition()Z

    move-result v0

    goto/16 :goto_2

    .line 325
    :cond_2b
    const-string v2, "hear_sound"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 326
    invoke-direct {p0, p2, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityHearSound(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 327
    :cond_2c
    const-string v2, "hear_sound_subtitle"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 328
    invoke-direct {p0, p2, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityHearSoundSubtitle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 329
    :cond_2d
    const-string v2, "launch_global_power_guide"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 330
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchGlobalPowerGuide()Z

    move-result v0

    goto/16 :goto_2

    .line 331
    :cond_2e
    const-string/jumbo v2, "split_ltr"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 332
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->toggleSplitByGesture(Z)Z

    goto/16 :goto_2

    .line 333
    :cond_2f
    const-string/jumbo v2, "split_rtl"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 334
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->toggleSplitByGesture(Z)Z

    goto/16 :goto_2

    .line 335
    :cond_30
    const-string v2, "launch_home"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 336
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchHome(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 337
    :cond_31
    const-string v2, "launch_recents"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 338
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchRecents(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_2

    .line 339
    :cond_32
    const-string v2, "close_app"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 340
    if-eqz p3, :cond_33

    .line 341
    const-string/jumbo v1, "shortcut_type"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    const-string v2, "phone_shortcut"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 342
    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWindowManagerPolicy:Lcom/android/server/policy/WindowManagerPolicy;

    check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;

    invoke-virtual {v1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerCloseApp(Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 344
    :cond_33
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onMetaKeyCombination()V

    goto/16 :goto_2

    .line 346
    :cond_34
    const-string v2, "launch_app_small_window"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 348
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    .line 349
    invoke-virtual {v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->freeFormAndFullScreenToggleByKeyCombination(Z)Z

    move-result v0

    goto/16 :goto_2

    .line 350
    :cond_35
    const-string v2, "launch_app_full_window"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 352
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    .line 353
    invoke-virtual {v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->freeFormAndFullScreenToggleByKeyCombination(Z)Z

    move-result v0

    goto/16 :goto_2

    .line 354
    :cond_36
    const-string v2, "launch_split_screen_to_left"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 355
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->setSplitScreenDirection(I)Z

    move-result v0

    goto/16 :goto_2

    .line 357
    :cond_37
    const-string v1, "launch_split_screen_to_right"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 358
    invoke-static {}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->getInstance()Lcom/android/server/wm/ActivityTaskManagerServiceImpl;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->setSplitScreenDirection(I)Z

    move-result v0

    goto/16 :goto_2

    .line 360
    :cond_38
    const-string v1, "launch_app"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 361
    invoke-virtual {p0, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSmallWindow(Landroid/os/Bundle;)Z

    move-result v0

    goto/16 :goto_2

    .line 362
    :cond_39
    const-string v1, "launch_camera_and_take_photo"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 363
    const v1, 0x10001

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string/jumbo v2, "sendEvent"

    invoke-static {v2, v1}, Lcom/android/server/camera/CameraOpt;->callMethod(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCameraAndTakePhoto()Z

    move-result v0

    goto :goto_2

    .line 365
    :cond_3a
    const-string v1, "launch_sos"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 366
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchMiuiSoS()Z

    move-result v0

    goto :goto_2

    .line 367
    :cond_3b
    const-string v1, "launch_xiaoai_guide"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 368
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getPowerGuideIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_2

    .line 369
    :cond_3c
    const-string/jumbo v1, "show_menu"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 370
    invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->showMenu(Ljava/lang/String;)Z

    move-result v0

    goto :goto_2

    .line 371
    :cond_3d
    const-string/jumbo v1, "split_screen"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 372
    invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isSupportSpiltScreen()Z

    move-result v1

    if-eqz v1, :cond_40

    iget-object v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mStatusBarManagerInternal:Lcom/android/server/statusbar/StatusBarManagerInternal;

    if-eqz v1, :cond_40

    .line 373
    invoke-interface {v1}, Lcom/android/server/statusbar/StatusBarManagerInternal;->toggleSplitScreen()V

    .line 374
    const/4 v0, 0x1

    goto :goto_2

    .line 376
    :cond_3e
    const-string v1, "live_subtitles_full_screen"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 378
    const-string/jumbo v1, "startAiSubtitlesFullscreen"

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_2

    .line 380
    :cond_3f
    const-string v1, "live_subtitles"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_40

    .line 381
    const-string/jumbo v1, "startAiSubtitlesWindow"

    invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 385
    :cond_40
    :goto_2
    move-object v1, p0

    move v2, v0

    move-object v3, p2

    move-object v4, p1

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerHapticFeedback(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 386
    return v0
.end method

.method public vibrate()Z
    .locals 1

    .line 1088
    iget-object v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/util/AudioManagerHelper;->toggleVibrateSetting(Landroid/content/Context;)V

    .line 1089
    const/4 v0, 0x1

    return v0
.end method
