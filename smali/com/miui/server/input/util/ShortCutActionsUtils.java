public class com.miui.server.input.util.ShortCutActionsUtils {
	 /* .source "ShortCutActionsUtils.java" */
	 /* # static fields */
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_ENVIRONMENT_SPEECH_RECOGNITION;
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_ENVIRONMENT_SPEECH_RECOGNITION_ENABLED;
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_HEAR_SOUND;
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_HEAR_SOUND_SUBTITLE;
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_TALK_BACK;
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_VOICE_CONTROL;
	 private static final java.lang.String ACCESSIBILITY_CLASS_NAME_VOICE_CONTROL_ENABLED;
	 private static final java.lang.String ACCESSIBILITY_LIVE_SUBTITLES_WINDOW_TYPE_DEFAULT;
	 private static final java.lang.String ACCESSIBILITY_LIVE_SUBTITLES_WINDOW_TYPE_FULL_SCREEN;
	 private static final java.lang.String ACCESSIBILITY_PACKAGE_NAME;
	 public static final java.lang.String ACCESSIBILITY_TALKBACK_STATUS;
	 private static final Integer ACCESSIBLE_MODE_LARGE_DENSITY;
	 private static final Integer ACCESSIBLE_MODE_SMALL_DENSITY;
	 private static final java.lang.String ACTION_GLOBAL_POWER_GUIDE;
	 private static final java.lang.String ACTION_INTENT_CLASS_NAME;
	 private static final java.lang.String ACTION_INTENT_CONTENT;
	 private static final java.lang.String ACTION_INTENT_DUAL;
	 private static final java.lang.String ACTION_INTENT_KEY;
	 private static final java.lang.String ACTION_INTENT_SHORTCUT;
	 private static final java.lang.String ACTION_INTENT_TITLE;
	 private static final java.lang.String ACTION_KDDI_GLOBAL_POWER_GUIDE;
	 public static final java.lang.String ACTION_PANEL_OPERATION;
	 public static final java.lang.String ACTION_POWER_UP;
	 private static final java.lang.String ACTION_ROTATION_FOLLOWS_SENSOR_GLOBAL_POWER_GUIDE;
	 private static final java.lang.String ACTION_SMALL_SCREEN_GLOBAL_POWER_GUIDE;
	 private static final java.lang.String DEVICE_TYPE_FOLD;
	 private static final java.lang.String DEVICE_TYPE_PAD;
	 private static final java.lang.String DEVICE_TYPE_PHONE;
	 private static final java.lang.String DEVICE_TYPE_SMALL_SCREEN;
	 static final Integer DIRECTION_LEFT;
	 static final Integer DIRECTION_RIGHT;
	 public static final java.lang.String DISABLE_SHORTCUT_TRACK;
	 public static final Integer EVENT_LEICA_MOMENT;
	 public static final java.lang.String EXTRA_ACTION_SOURCE;
	 public static final java.lang.String EXTRA_KEY_ACTION;
	 public static final java.lang.String EXTRA_KEY_EVENT_TIME;
	 public static final java.lang.String EXTRA_LONG_PRESS_POWER_FUNCTION;
	 public static final java.lang.String EXTRA_POWER_GUIDE;
	 public static final java.lang.String EXTRA_SHORTCUT_TYPE;
	 public static final java.lang.String EXTRA_SKIP_TELECOM_CHECK;
	 public static final java.lang.String EXTRA_TORCH_ENABLED;
	 private static final Boolean IS_CETUS;
	 private static final java.lang.String KEY_ACTION;
	 public static final java.lang.String KEY_OPERATION;
	 public static final java.lang.String NOTES_CONTENT_ITEM_TYPE;
	 private static final java.lang.String PACKAGE_INPUT_SETTINGS;
	 private static final java.lang.String PACKAGE_MI_CREATION;
	 private static final java.lang.String PACKAGE_NOTES;
	 public static final java.lang.String PACKAGE_SMART_HOME;
	 public static final java.lang.String PARTIAL_SCREENSHOT_POINTS;
	 public static final java.lang.String REASON_OF_DOUBLE_CLICK_HOME_KEY;
	 public static final java.lang.String REASON_OF_DOUBLE_CLICK_VOLUME_DOWN;
	 public static final java.lang.String REASON_OF_KEYBOARD;
	 public static final java.lang.String REASON_OF_KEYBOARD_FN;
	 public static final java.lang.String REASON_OF_LONG_PRESS_CAMERA_KEY;
	 public static final java.lang.String REASON_OF_TRIGGERED_BY_AI_KEY;
	 public static final java.lang.String REASON_OF_TRIGGERED_BY_PROXIMITY_SENSOR;
	 public static final java.lang.String REASON_OF_TRIGGERED_BY_STABILIZER;
	 public static final java.lang.String REASON_OF_TRIGGERED_TORCH;
	 public static final java.lang.String REASON_OF_TRIGGERED_TORCH_BY_POWER;
	 public static final java.lang.String REVERSE_NOTIFICATION_PANEL;
	 public static final java.lang.String REVERSE_QUICK_SETTINGS_PANEL;
	 private static final java.lang.String SETTINGS_CLASS_NAME_KEY_DOWNLOAD_DIALOG_ACTIVITY;
	 private static final java.lang.String SETTINGS_PACKAGE_NAME;
	 private static final java.lang.String TAG;
	 public static final Integer TAKE_PARTIAL_SCREENSHOT;
	 public static final Integer TAKE_PARTIAL_SCREENSHOT_WITH_STYLUS;
	 public static final Integer TAKE_SCREENSHOT_WITHOUT_ANIM;
	 public static final java.lang.String TYPE_PHONE_SHORTCUT;
	 private static final Integer XIAO_POWER_GUIDE_VERSIONCODE;
	 private static com.miui.server.input.util.ShortCutActionsUtils shortCutActionsUtils;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private java.util.List mGAGuideCustomizedRegionList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.os.Handler mHandler;
private miui.util.HapticFeedbackUtil mHapticFeedbackUtil;
private Boolean mHasCameraFlash;
private Boolean mIsFolded;
private Boolean mIsKidMode;
private Boolean mIsSystemReady;
private final Boolean mIsVoiceCapable;
private android.os.PowerManager mPowerManager;
private final com.android.internal.util.ScreenshotHelper mScreenshotHelper;
private final com.android.server.input.shortcut.ShortcutOneTrackHelper mShortcutOneTrackHelper;
private com.miui.server.stability.StabilityLocalServiceInternal mStabilityLocalServiceInternal;
private com.android.server.statusbar.StatusBarManagerInternal mStatusBarManagerInternal;
private Boolean mSupportAccessibilitySubtitle;
private Boolean mWifiOnly;
private com.android.server.policy.WindowManagerPolicy mWindowManagerPolicy;
/* # direct methods */
public static void $r8$lambda$ZVuYTQzk9gzCfluI2DfBwYcAcaI ( com.miui.server.input.util.ShortCutActionsUtils p0, android.content.Context p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->lambda$new$0(Landroid/content/Context;)V */
return;
} // .end method
public static void $r8$lambda$pp9-S0QktzQOaIhIfq0Hk0rKrXc ( com.miui.server.input.util.ShortCutActionsUtils p0, Integer p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->lambda$showToast$1(II)V */
return;
} // .end method
static com.miui.server.input.util.ShortCutActionsUtils ( ) {
/* .locals 2 */
/* .line 122 */
final String v0 = "cetus"; // const-string v0, "cetus"
v1 = miui.os.Build.DEVICE;
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
com.miui.server.input.util.ShortCutActionsUtils.IS_CETUS = (v0!= 0);
return;
} // .end method
private com.miui.server.input.util.ShortCutActionsUtils ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 183 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 162 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mGAGuideCustomizedRegionList = v0;
/* .line 178 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsSystemReady:Z */
/* .line 184 */
this.mContext = p1;
/* .line 185 */
/* new-instance v0, Lcom/android/internal/util/ScreenshotHelper; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/android/internal/util/ScreenshotHelper;-><init>(Landroid/content/Context;)V */
this.mScreenshotHelper = v0;
/* .line 186 */
com.android.server.input.MiuiInputThread .getHandler ( );
this.mHandler = v0;
/* .line 187 */
/* new-instance v1, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/ShortCutActionsUtils;Landroid/content/Context;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 188 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x111023a */
v0 = (( android.content.res.Resources ) v0 ).getBoolean ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
/* iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsVoiceCapable:Z */
/* .line 190 */
v0 = this.mContext;
v0 = miui.os.Build .hasCameraFlash ( v0 );
/* iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHasCameraFlash:Z */
/* .line 191 */
v0 = this.mContext;
com.android.server.input.shortcut.ShortcutOneTrackHelper .getInstance ( v0 );
this.mShortcutOneTrackHelper = v0;
/* .line 192 */
return;
} // .end method
private Boolean findDeviceLocate ( ) {
/* .locals 2 */
/* .line 751 */
v0 = this.mContext;
final String v1 = "imperceptible_power_press"; // const-string v1, "imperceptible_power_press"
com.android.server.policy.FindDevicePowerOffLocateManager .sendFindDeviceLocateBroadcast ( v0,v1 );
/* .line 753 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private java.lang.String getDeviceType ( ) {
/* .locals 1 */
/* .line 516 */
/* sget-boolean v0, Landroid/provider/MiuiSettings$System;->IS_FOLD_DEVICE:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 517 */
	 final String v0 = "fold"; // const-string v0, "fold"
	 /* .line 518 */
} // :cond_0
/* sget-boolean v0, Lmiui/os/Build;->IS_TABLET:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 519 */
	 final String v0 = "pad"; // const-string v0, "pad"
	 /* .line 521 */
} // :cond_1
final String v0 = "phone"; // const-string v0, "phone"
} // .end method
public static com.miui.server.input.util.ShortCutActionsUtils getInstance ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p0, "context" # Landroid/content/Context; */
/* .line 195 */
v0 = com.miui.server.input.util.ShortCutActionsUtils.shortCutActionsUtils;
/* if-nez v0, :cond_0 */
/* .line 196 */
/* new-instance v0, Lcom/miui/server/input/util/ShortCutActionsUtils; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;-><init>(Landroid/content/Context;)V */
/* .line 198 */
} // :cond_0
v0 = com.miui.server.input.util.ShortCutActionsUtils.shortCutActionsUtils;
} // .end method
private android.content.Intent getLongPressPowerKeyFunctionIntent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 7 */
/* .param p1, "intentName" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .line 780 */
int v0 = 0; // const/4 v0, 0x0
/* .line 781 */
/* .local v0, "intent":Landroid/content/Intent; */
v1 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
final String v2 = "android.intent.action.ASSIST"; // const-string v2, "android.intent.action.ASSIST"
final String v3 = "com.miui.smarthomeplus"; // const-string v3, "com.miui.smarthomeplus"
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
int v6 = -1; // const/4 v6, -0x1
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_0
/* :sswitch_0 */
v1 = (( java.lang.String ) p1 ).equals ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v4 */
/* :sswitch_1 */
v1 = (( java.lang.String ) p1 ).equals ( v3 ); // invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* move v1, v5 */
} // :goto_0
/* move v1, v6 */
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 788 */
/* :pswitch_0 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* move-object v0, v1 */
/* .line 789 */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.smarthomeplus.UWBEntryService"; // const-string v2, "com.miui.smarthomeplus.UWBEntryService"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 791 */
/* .line 783 */
/* :pswitch_1 */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* move-object v0, v1 */
/* .line 784 */
int v1 = 0; // const/4 v1, 0x0
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 785 */
/* const-string/jumbo v1, "versionCode" */
int v2 = 3; // const/4 v2, 0x3
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 786 */
/* nop */
/* .line 796 */
} // :goto_2
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 797 */
v1 = (( java.lang.String ) p2 ).hashCode ( ); // invoke-virtual {p2}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_1 */
} // :cond_1
/* :sswitch_2 */
final String v1 = "power_up"; // const-string v1, "power_up"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v6, v5 */
/* :sswitch_3 */
final String v1 = "long_press_power_key"; // const-string v1, "long_press_power_key"
v1 = (( java.lang.String ) p2 ).equals ( v1 ); // invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* move v6, v4 */
} // :goto_3
final String v1 = "key_action"; // const-string v1, "key_action"
/* packed-switch v6, :pswitch_data_1 */
/* .line 805 */
/* :pswitch_2 */
(( android.content.Intent ) v0 ).putExtra ( v1, v5 ); // invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 806 */
final String v1 = "key_event_time"; // const-string v1, "key_event_time"
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v2 */
(( android.content.Intent ) v0 ).putExtra ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 807 */
/* .line 799 */
/* :pswitch_3 */
(( android.content.Intent ) v0 ).putExtra ( v1, v4 ); // invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 801 */
/* nop */
/* .line 802 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
/* .line 801 */
final String v3 = "long_press_event_time"; // const-string v3, "long_press_event_time"
(( android.content.Intent ) v0 ).putExtra ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 803 */
/* nop */
/* .line 812 */
} // :cond_2
} // :goto_4
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* 0x11cbb911 -> :sswitch_1 */
/* 0x5d5f976e -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
/* :sswitch_data_1 */
/* .sparse-switch */
/* -0x4b0ca89a -> :sswitch_3 */
/* 0x332c9015 -> :sswitch_2 */
} // .end sparse-switch
/* :pswitch_data_1 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
} // .end packed-switch
} // .end method
private android.content.Intent getPowerGuideIntent ( ) {
/* .locals 3 */
/* .line 1377 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1378 */
/* .local v0, "powerGuideIntent":Landroid/content/Intent; */
final String v1 = "com.miui.voiceassist"; // const-string v1, "com.miui.voiceassist"
final String v2 = "com.xiaomi.voiceassistant.guidePage.PowerGuideDialogActivityV2"; // const-string v2, "com.xiaomi.voiceassistant.guidePage.PowerGuideDialogActivityV2"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1380 */
/* const-string/jumbo v1, "showSwitchNotice" */
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1381 */
/* const/high16 v1, 0x30000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1382 */
} // .end method
private Boolean goToSleep ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1312 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 1313 */
/* .local v0, "currentTime":J */
v2 = this.mPowerManager;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1314 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "goToSleep, reason = "; // const-string v3, "goToSleep, reason = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v2 );
/* .line 1315 */
v2 = this.mPowerManager;
(( android.os.PowerManager ) v2 ).goToSleep ( v0, v1 ); // invoke-virtual {v2, v0, v1}, Landroid/os/PowerManager;->goToSleep(J)V
/* .line 1316 */
int v2 = 1; // const/4 v2, 0x1
/* .line 1318 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // .end method
private Boolean isAccessibilityFunctionEnabled ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "componentClassName" # Ljava/lang/String; */
/* .line 693 */
int v0 = 0; // const/4 v0, 0x0
/* .line 694 */
/* .local v0, "enabled":Z */
/* if-nez p1, :cond_0 */
/* .line 695 */
final String v1 = "Accessibility function componentClassName is null"; // const-string v1, "Accessibility function componentClassName is null"
com.android.server.policy.MiuiInputLog .error ( v1 );
/* .line 696 */
int v1 = 0; // const/4 v1, 0x0
/* .line 698 */
} // :cond_0
/* new-instance v1, Ljava/util/ArrayList; */
v2 = this.mContext;
/* .line 699 */
int v3 = -2; // const/4 v3, -0x2
com.android.internal.accessibility.util.AccessibilityUtils .getEnabledServicesFromSettings ( v2,v3 );
/* invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V */
/* .line 700 */
/* .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Landroid/content/ComponentName; */
/* .line 701 */
/* .local v3, "component":Landroid/content/ComponentName; */
if ( v3 != null) { // if-eqz v3, :cond_1
(( android.content.ComponentName ) v3 ).getClassName ( ); // invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v4 = (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 702 */
int v0 = 1; // const/4 v0, 0x1
/* .line 704 */
} // .end local v3 # "component":Landroid/content/ComponentName;
} // :cond_1
/* .line 705 */
} // :cond_2
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Accessibility function componentClassName:"; // const-string v3, "Accessibility function componentClassName:"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " enabled="; // const-string v3, " enabled="
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v2 );
/* .line 707 */
} // .end method
private Boolean isAppInstalled ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 954 */
int v0 = 0; // const/4 v0, 0x0
try { // :try_start_0
v1 = this.mContext;
(( android.content.Context ) v1 ).getPackageManager ( ); // invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 955 */
(( android.content.pm.PackageManager ) v1 ).getPackageInfo ( p1, v0 ); // invoke-virtual {v1, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
/* :try_end_0 */
/* .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 959 */
/* .local v1, "packageInfo":Landroid/content/pm/PackageInfo; */
/* .line 956 */
} // .end local v1 # "packageInfo":Landroid/content/pm/PackageInfo;
/* :catch_0 */
/* move-exception v1 */
/* .line 957 */
/* .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException; */
int v2 = 0; // const/4 v2, 0x0
/* .line 958 */
/* .local v2, "packageInfo":Landroid/content/pm/PackageInfo; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = " don\'t install"; // const-string v4, " don\'t install"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .error ( v3 );
/* move-object v1, v2 */
/* .line 960 */
} // .end local v2 # "packageInfo":Landroid/content/pm/PackageInfo;
/* .local v1, "packageInfo":Landroid/content/pm/PackageInfo; */
} // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
} // .end method
private Boolean isLargeScreen ( ) {
/* .locals 3 */
/* .line 526 */
android.content.res.Resources .getSystem ( );
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* .line 527 */
/* .local v0, "configuration":Landroid/content/res/Configuration; */
/* iget v1, v0, Landroid/content/res/Configuration;->screenLayout:I */
/* and-int/lit8 v1, v1, 0xf */
/* .line 528 */
/* .local v1, "screenSize":I */
int v2 = 3; // const/4 v2, 0x3
/* if-ne v1, v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
} // .end method
private Boolean isLargeScreen ( android.content.Context p0 ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 469 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v0 ).getConfiguration ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;
/* .line 470 */
/* .local v0, "configuration":Landroid/content/res/Configuration; */
/* iget v1, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I */
/* .line 471 */
/* .local v1, "smallestScreenWidthDp":I */
/* iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I */
/* const/16 v3, 0x160 */
int v4 = 1; // const/4 v4, 0x1
int v5 = 0; // const/4 v5, 0x0
/* if-ne v2, v3, :cond_1 */
/* .line 473 */
/* const/16 v2, 0x190 */
/* if-le v1, v2, :cond_0 */
} // :cond_0
/* move v4, v5 */
} // :goto_0
/* .line 474 */
} // :cond_1
/* iget v2, v0, Landroid/content/res/Configuration;->densityDpi:I */
/* const/16 v3, 0x1cd */
/* if-ne v2, v3, :cond_3 */
/* .line 476 */
/* const/16 v2, 0x131 */
/* if-le v1, v2, :cond_2 */
} // :cond_2
/* move v4, v5 */
} // :goto_1
/* .line 478 */
} // :cond_3
/* const/16 v2, 0x140 */
/* if-le v1, v2, :cond_4 */
} // :cond_4
/* move v4, v5 */
} // :goto_2
} // .end method
private Boolean isSupportSpiltScreen ( ) {
/* .locals 3 */
/* .line 454 */
/* sget-boolean v0, Lcom/miui/server/input/util/ShortCutActionsUtils;->IS_CETUS:Z */
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = this.mContext;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isLargeScreen(Landroid/content/Context;)Z */
/* if-nez v0, :cond_0 */
/* .line 455 */
final String v0 = "Ignore because the current window is the small screen"; // const-string v0, "Ignore because the current window is the small screen"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 456 */
v0 = this.mContext;
/* const v2, 0x110f01f5 */
(( android.content.Context ) v0 ).getString ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->makeAllUserToastAndShow(Ljava/lang/String;I)V */
/* .line 458 */
/* .line 460 */
} // :cond_0
} // .end method
private void lambda$new$0 ( android.content.Context p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 187 */
/* new-instance v0, Lmiui/util/HapticFeedbackUtil; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p1, v1}, Lmiui/util/HapticFeedbackUtil;-><init>(Landroid/content/Context;Z)V */
this.mHapticFeedbackUtil = v0;
return;
} // .end method
private void lambda$showToast$1 ( Integer p0, Integer p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "resourceId" # I */
/* .param p2, "length" # I */
/* .line 1285 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getString ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;
android.widget.Toast .makeText ( v0,v1,p2 );
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 1286 */
return;
} // .end method
private Boolean lauchWeixinPaymentCode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1182 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.VIEW"; // const-string v1, "android.intent.action.VIEW"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1183 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.tencent.mm.plugin.offline.ui.WalletOfflineCoinPurseUI"; // const-string v2, "com.tencent.mm.plugin.offline.ui.WalletOfflineCoinPurseUI"
final String v3 = "com.tencent.mm"; // const-string v3, "com.tencent.mm"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1185 */
final String v1 = "key_entry_scene"; // const-string v1, "key_entry_scene"
int v2 = 2; // const/4 v2, 0x2
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 1186 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1187 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1189 */
} // :cond_0
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 1190 */
/* .local v1, "dialogIntent":Landroid/content/Intent; */
final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
final String v4 = "com.android.settings.KeyDownloadDialogActivity"; // const-string v4, "com.android.settings.KeyDownloadDialogActivity"
(( android.content.Intent ) v1 ).setClassName ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1191 */
/* const-string/jumbo v2, "shortcut" */
(( android.content.Intent ) v1 ).putExtra ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1192 */
final String v2 = "packageName"; // const-string v2, "packageName"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1193 */
final String v2 = "dual"; // const-string v2, "dual"
int v3 = 0; // const/4 v3, 0x0
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1194 */
v2 = this.mContext;
/* const v4, 0x110f004e */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* const-string/jumbo v4, "title" */
(( android.content.Intent ) v1 ).putExtra ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1195 */
v2 = this.mContext;
/* const v4, 0x110f0050 */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
final String v4 = "content"; // const-string v4, "content"
(( android.content.Intent ) v1 ).putExtra ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1196 */
/* const v2, 0x10008000 */
(( android.content.Intent ) v1 ).setFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1197 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 1198 */
} // .end method
private Boolean launchAccessibilityEnvironmentSpeechRecognition ( ) {
/* .locals 3 */
/* .line 648 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 649 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.accessibility"; // const-string v1, "com.miui.accessibility"
final String v2 = "com.miui.accessibility.environment.sound.recognition.TransparentEsrSettings"; // const-string v2, "com.miui.accessibility.environment.sound.recognition.TransparentEsrSettings"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 651 */
final String v1 = "com.miui.accessibility.environment.sound.recognition.EnvSoundRecognitionService"; // const-string v1, "com.miui.accessibility.environment.sound.recognition.EnvSoundRecognitionService"
v1 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAccessibilityFunctionEnabled(Ljava/lang/String;)Z */
/* if-nez v1, :cond_0 */
/* .line 652 */
final String v1 = "open"; // const-string v1, "open"
} // :cond_0
final String v1 = "close"; // const-string v1, "close"
/* .line 651 */
} // :goto_0
final String v2 = "OPEN_ESR"; // const-string v2, "OPEN_ESR"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 653 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchAccessibilityHearSound ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 663 */
/* iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mSupportAccessibilitySubtitle:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 664 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "redirect accessibility hear sound success, action="; // const-string v1, "redirect accessibility hear sound success, action="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 665 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->redirectAccessibilityHearSoundFunction(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 667 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 668 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.accessibility"; // const-string v1, "com.miui.accessibility"
final String v2 = "com.miui.accessibility.asr.component.message.MessageActivity"; // const-string v2, "com.miui.accessibility.asr.component.message.MessageActivity"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 669 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchAccessibilityHearSoundSubtitle ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 680 */
/* iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mSupportAccessibilitySubtitle:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 681 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "redirect accessibility hear sound subtitle success, action="; // const-string v1, "redirect accessibility hear sound subtitle success, action="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 683 */
v0 = /* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->redirectAccessibilityHearSoundFunction(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 685 */
} // :cond_0
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 686 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.accessibility"; // const-string v1, "com.miui.accessibility"
final String v2 = "com.miui.accessibility.asr.component.floatwindow.TransparentCaptionActivity"; // const-string v2, "com.miui.accessibility.asr.component.floatwindow.TransparentCaptionActivity"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 688 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchAccessibilityLiveSubtitle ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "floatingWindowType" # Ljava/lang/String; */
/* .line 433 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 434 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.xiaomi.aiasst.vision"; // const-string v2, "com.xiaomi.aiasst.vision"
final String v3 = "com.xiaomi.aiasst.vision.control.translation.AiTranslateService"; // const-string v3, "com.xiaomi.aiasst.vision.control.translation.AiTranslateService"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 436 */
/* new-instance v1, Landroid/os/Bundle; */
/* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
/* .line 437 */
/* .local v1, "bundle":Landroid/os/Bundle; */
final String v2 = "from"; // const-string v2, "from"
(( android.os.Bundle ) v1 ).putString ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 438 */
final String v2 = "floatingWindowType"; // const-string v2, "floatingWindowType"
(( android.os.Bundle ) v1 ).putString ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 439 */
(( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 440 */
v2 = this.mContext;
(( android.content.Context ) v2 ).startService ( v0 ); // invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 441 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
private Boolean launchAccessibilityVoiceControl ( ) {
/* .locals 3 */
/* .line 634 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 635 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.miui.accessibility"; // const-string v1, "com.miui.accessibility"
final String v2 = "com.miui.accessibility.voiceaccess.settings.TransparentVoiceAccessSettings"; // const-string v2, "com.miui.accessibility.voiceaccess.settings.TransparentVoiceAccessSettings"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 636 */
final String v1 = "com.miui.accessibility.voiceaccess.VoiceAccessAccessibilityService"; // const-string v1, "com.miui.accessibility.voiceaccess.VoiceAccessAccessibilityService"
v1 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAccessibilityFunctionEnabled(Ljava/lang/String;)Z */
/* if-nez v1, :cond_0 */
/* .line 637 */
final String v1 = "open"; // const-string v1, "open"
} // :cond_0
final String v1 = "close"; // const-string v1, "close"
/* .line 636 */
} // :goto_0
final String v2 = "OPEN_VOICE_ACCESS"; // const-string v2, "OPEN_VOICE_ACCESS"
(( android.content.Intent ) v0 ).putExtra ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 638 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchAlipayHealthCode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1234 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1235 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.action.VIEW"; // const-string v1, "android.intent.action.VIEW"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1236 */
final String v1 = "com.eg.android.AlipayGphone"; // const-string v1, "com.eg.android.AlipayGphone"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1237 */
/* const/high16 v2, 0x14800000 */
(( android.content.Intent ) v0 ).setFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1239 */
final String v2 = "alipays://platformapi/startapp?appId=68687564&chInfo=ch_xiaomi_quick&sceneCode=KF_CHANGSHANG&shareUserId=2088831085791813&partnerId=ch_xiaomi_quick&pikshemo=YES"; // const-string v2, "alipays://platformapi/startapp?appId=68687564&chInfo=ch_xiaomi_quick&sceneCode=KF_CHANGSHANG&shareUserId=2088831085791813&partnerId=ch_xiaomi_quick&pikshemo=YES"
android.net.Uri .parse ( v2 );
(( android.content.Intent ) v0 ).setData ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
/* .line 1240 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1241 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1243 */
} // :cond_0
/* new-instance v2, Landroid/content/Intent; */
/* invoke-direct {v2}, Landroid/content/Intent;-><init>()V */
/* .line 1244 */
/* .local v2, "dialogIntent":Landroid/content/Intent; */
final String v3 = "com.android.settings"; // const-string v3, "com.android.settings"
final String v4 = "com.android.settings.KeyDownloadDialogActivity"; // const-string v4, "com.android.settings.KeyDownloadDialogActivity"
(( android.content.Intent ) v2 ).setClassName ( v3, v4 ); // invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1245 */
/* const-string/jumbo v3, "shortcut" */
(( android.content.Intent ) v2 ).putExtra ( v3, p1 ); // invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1246 */
final String v3 = "packageName"; // const-string v3, "packageName"
(( android.content.Intent ) v2 ).putExtra ( v3, v1 ); // invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1247 */
final String v1 = "dual"; // const-string v1, "dual"
int v3 = 0; // const/4 v3, 0x0
(( android.content.Intent ) v2 ).putExtra ( v1, v3 ); // invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1248 */
v1 = this.mContext;
/* const v4, 0x110f0049 */
(( android.content.Context ) v1 ).getString ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* const-string/jumbo v4, "title" */
(( android.content.Intent ) v2 ).putExtra ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1249 */
v1 = this.mContext;
/* const v4, 0x110f0050 */
(( android.content.Context ) v1 ).getString ( v4 ); // invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
final String v4 = "content"; // const-string v4, "content"
(( android.content.Intent ) v2 ).putExtra ( v4, v1 ); // invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1250 */
/* const v1, 0x10008000 */
(( android.content.Intent ) v2 ).setFlags ( v1 ); // invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1251 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 1252 */
} // .end method
private Boolean launchAlipayPaymentCode ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1207 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1208 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.eg.android.AlipayGphone.FastStartActivity"; // const-string v2, "com.eg.android.AlipayGphone.FastStartActivity"
final String v3 = "com.eg.android.AlipayGphone"; // const-string v3, "com.eg.android.AlipayGphone"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1210 */
final String v1 = "android.intent.action.VIEW"; // const-string v1, "android.intent.action.VIEW"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1211 */
/* const/high16 v1, 0x14800000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1213 */
final String v1 = "alipayss://platformapi/startapp?appId=20000056&source=shortcut"; // const-string v1, "alipayss://platformapi/startapp?appId=20000056&source=shortcut"
android.net.Uri .parse ( v1 );
(( android.content.Intent ) v0 ).setData ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
/* .line 1214 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1215 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1217 */
} // :cond_0
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 1218 */
/* .local v1, "dialogIntent":Landroid/content/Intent; */
final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
final String v4 = "com.android.settings.KeyDownloadDialogActivity"; // const-string v4, "com.android.settings.KeyDownloadDialogActivity"
(( android.content.Intent ) v1 ).setClassName ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1219 */
/* const-string/jumbo v2, "shortcut" */
(( android.content.Intent ) v1 ).putExtra ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1220 */
final String v2 = "packageName"; // const-string v2, "packageName"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1221 */
final String v2 = "dual"; // const-string v2, "dual"
int v3 = 0; // const/4 v3, 0x0
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1222 */
v2 = this.mContext;
/* const v4, 0x110f004a # 1.12808E-28f */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* const-string/jumbo v4, "title" */
(( android.content.Intent ) v1 ).putExtra ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1223 */
v2 = this.mContext;
/* const v4, 0x110f0050 */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
final String v4 = "content"; // const-string v4, "content"
(( android.content.Intent ) v1 ).putExtra ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1224 */
/* const v2, 0x10008000 */
(( android.content.Intent ) v1 ).setFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1225 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 1226 */
} // .end method
private Boolean launchAlipayScanner ( java.lang.String p0 ) {
/* .locals 9 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1143 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1144 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.action.VIEW"; // const-string v1, "android.intent.action.VIEW"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1145 */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.alipay.mobile.scan.as.main.MainCaptureActivity"; // const-string v2, "com.alipay.mobile.scan.as.main.MainCaptureActivity"
final String v3 = "com.eg.android.AlipayGphone"; // const-string v3, "com.eg.android.AlipayGphone"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1146 */
/* const v1, 0x10008000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1147 */
/* new-instance v2, Landroid/os/Bundle; */
/* invoke-direct {v2}, Landroid/os/Bundle;-><init>()V */
/* .line 1148 */
/* .local v2, "bundle":Landroid/os/Bundle; */
final String v4 = "app_id"; // const-string v4, "app_id"
final String v5 = "10000007"; // const-string v5, "10000007"
(( android.os.Bundle ) v2 ).putString ( v4, v5 ); // invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1150 */
/* new-instance v4, Landroid/os/Bundle; */
/* invoke-direct {v4}, Landroid/os/Bundle;-><init>()V */
/* .line 1151 */
/* .local v4, "bundleTemp":Landroid/os/Bundle; */
/* const-string/jumbo v6, "source" */
/* const-string/jumbo v7, "shortcut" */
(( android.os.Bundle ) v4 ).putString ( v6, v7 ); // invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1152 */
final String v6 = "appId"; // const-string v6, "appId"
(( android.os.Bundle ) v4 ).putString ( v6, v5 ); // invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1153 */
final String v5 = "REALLY_STARTAPP"; // const-string v5, "REALLY_STARTAPP"
int v6 = 1; // const/4 v6, 0x1
(( android.os.Bundle ) v4 ).putBoolean ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1154 */
/* const-string/jumbo v5, "showOthers" */
final String v8 = "YES"; // const-string v8, "YES"
(( android.os.Bundle ) v4 ).putString ( v5, v8 ); // invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1155 */
/* const-string/jumbo v5, "startFromExternal" */
(( android.os.Bundle ) v4 ).putBoolean ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1156 */
final String v5 = "REALLY_DOSTARTAPP"; // const-string v5, "REALLY_DOSTARTAPP"
(( android.os.Bundle ) v4 ).putBoolean ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
/* .line 1157 */
/* const-string/jumbo v5, "sourceId" */
(( android.os.Bundle ) v4 ).putString ( v5, v7 ); // invoke-virtual {v4, v5, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1158 */
final String v5 = "ap_framework_sceneId"; // const-string v5, "ap_framework_sceneId"
final String v8 = "20000001"; // const-string v8, "20000001"
(( android.os.Bundle ) v4 ).putString ( v5, v8 ); // invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 1160 */
final String v5 = "mExtras"; // const-string v5, "mExtras"
(( android.os.Bundle ) v2 ).putBundle ( v5, v4 ); // invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
/* .line 1161 */
(( android.content.Intent ) v0 ).putExtras ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
/* .line 1162 */
v5 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 1163 */
/* .line 1165 */
} // :cond_0
/* new-instance v5, Landroid/content/Intent; */
/* invoke-direct {v5}, Landroid/content/Intent;-><init>()V */
/* .line 1166 */
/* .local v5, "dialogIntent":Landroid/content/Intent; */
final String v6 = "com.android.settings"; // const-string v6, "com.android.settings"
final String v8 = "com.android.settings.KeyDownloadDialogActivity"; // const-string v8, "com.android.settings.KeyDownloadDialogActivity"
(( android.content.Intent ) v5 ).setClassName ( v6, v8 ); // invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1167 */
(( android.content.Intent ) v5 ).putExtra ( v7, p1 ); // invoke-virtual {v5, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1168 */
final String v6 = "packageName"; // const-string v6, "packageName"
(( android.content.Intent ) v5 ).putExtra ( v6, v3 ); // invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1169 */
final String v3 = "dual"; // const-string v3, "dual"
int v6 = 0; // const/4 v6, 0x0
(( android.content.Intent ) v5 ).putExtra ( v3, v6 ); // invoke-virtual {v5, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1170 */
v3 = this.mContext;
/* const v7, 0x110f004b */
(( android.content.Context ) v3 ).getString ( v7 ); // invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* const-string/jumbo v7, "title" */
(( android.content.Intent ) v5 ).putExtra ( v7, v3 ); // invoke-virtual {v5, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1171 */
v3 = this.mContext;
/* const v7, 0x110f0050 */
(( android.content.Context ) v3 ).getString ( v7 ); // invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;
final String v7 = "content"; // const-string v7, "content"
(( android.content.Intent ) v5 ).putExtra ( v7, v3 ); // invoke-virtual {v5, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1172 */
(( android.content.Intent ) v5 ).setFlags ( v1 ); // invoke-virtual {v5, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1173 */
/* invoke-direct {p0, v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 1174 */
} // .end method
private Boolean launchApp ( android.content.Intent p0 ) {
/* .locals 1 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .line 1386 */
int v0 = 0; // const/4 v0, 0x0
v0 = /* invoke-direct {p0, p1, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;Landroid/os/Bundle;)Z */
} // .end method
private Boolean launchApp ( android.content.Intent p0, android.os.Bundle p1 ) {
/* .locals 6 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 1393 */
v0 = (( android.content.Intent ) p1 ).getFlags ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I
/* const/high16 v1, 0x200000 */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 1394 */
/* const/high16 v0, 0x14000000 */
(( android.content.Intent ) p1 ).addFlags ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1396 */
} // :cond_0
/* const/high16 v0, 0x14800000 */
(( android.content.Intent ) p1 ).addFlags ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1400 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* .line 1401 */
/* .local v0, "packageName":Ljava/lang/String; */
(( android.content.Intent ) p1 ).getPackage ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_1 */
/* .line 1402 */
(( android.content.Intent ) p1 ).getPackage ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;
/* .line 1403 */
} // :cond_1
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 1404 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1403 */
v1 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v1, :cond_2 */
/* .line 1405 */
(( android.content.Intent ) p1 ).getComponent ( ); // invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;
(( android.content.ComponentName ) v1 ).getPackageName ( ); // invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
/* .line 1407 */
} // :cond_2
} // :goto_1
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_3 */
/* .line 1408 */
final String v2 = "package name is null"; // const-string v2, "package name is null"
com.android.server.policy.MiuiInputLog .major ( v2 );
/* .line 1409 */
/* .line 1411 */
} // :cond_3
/* const/high16 v2, 0xd0000 */
/* .line 1413 */
/* .local v2, "flags":I */
v3 = this.mContext;
(( android.content.Context ) v3 ).getPackageManager ( ); // invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 1414 */
v4 = miui.securityspace.CrossUserUtils .getCurrentUserId ( );
/* .line 1413 */
(( android.content.pm.PackageManager ) v3 ).queryIntentActivitiesAsUser ( p1, v2, v4 ); // invoke-virtual {v3, p1, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;
/* .line 1415 */
/* .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;" */
v4 = if ( v3 != null) { // if-eqz v3, :cond_4
/* if-lez v4, :cond_4 */
/* .line 1417 */
try { // :try_start_0
v4 = this.mContext;
v5 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v4 ).startActivityAsUser ( p1, p2, v5 ); // invoke-virtual {v4, p1, p2, v5}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Landroid/content/ActivityNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 1418 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1421 */
/* :catch_0 */
/* move-exception v4 */
/* .line 1422 */
/* .local v4, "e":Ljava/lang/IllegalStateException; */
final String v5 = "IllegalStateException"; // const-string v5, "IllegalStateException"
com.android.server.policy.MiuiInputLog .error ( v5,v4 );
} // .end local v4 # "e":Ljava/lang/IllegalStateException;
/* .line 1419 */
/* :catch_1 */
/* move-exception v4 */
/* .line 1420 */
/* .local v4, "e":Landroid/content/ActivityNotFoundException; */
final String v5 = "ActivityNotFoundException"; // const-string v5, "ActivityNotFoundException"
com.android.server.policy.MiuiInputLog .error ( v5,v4 );
/* .line 1423 */
} // .end local v4 # "e":Landroid/content/ActivityNotFoundException;
} // :goto_2
/* .line 1425 */
} // :cond_4
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "launch app fail package:"; // const-string v5, "launch app fail package:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v0 ); // invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .major ( v4 );
/* .line 1428 */
} // :goto_3
} // .end method
private Boolean launchAppSimple ( android.content.Intent p0, android.os.Bundle p1 ) {
/* .locals 2 */
/* .param p1, "intent" # Landroid/content/Intent; */
/* .param p2, "bundle" # Landroid/os/Bundle; */
/* .line 965 */
try { // :try_start_0
v0 = this.mContext;
v1 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v0 ).startActivityAsUser ( p1, p2, v1 ); // invoke-virtual {v0, p1, p2, v1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Landroid/content/ActivityNotFoundException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 966 */
int v0 = 1; // const/4 v0, 0x1
/* .line 969 */
/* :catch_0 */
/* move-exception v0 */
/* .line 970 */
/* .local v0, "e":Ljava/lang/IllegalStateException; */
final String v1 = "launchAppSimple IllegalStateException"; // const-string v1, "launchAppSimple IllegalStateException"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
/* .line 967 */
} // .end local v0 # "e":Ljava/lang/IllegalStateException;
/* :catch_1 */
/* move-exception v0 */
/* .line 968 */
/* .local v0, "e":Landroid/content/ActivityNotFoundException; */
final String v1 = "launchAppSimple ActivityNotFoundException"; // const-string v1, "launchAppSimple ActivityNotFoundException"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
/* .line 971 */
} // .end local v0 # "e":Landroid/content/ActivityNotFoundException;
/* nop */
/* .line 972 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean launchAuPay ( ) {
/* .locals 3 */
/* .line 1337 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1338 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.action.VIEW"; // const-string v1, "android.intent.action.VIEW"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1339 */
final String v1 = "jp.auone.wallet"; // const-string v1, "jp.auone.wallet"
final String v2 = "jp.auone.wallet.ui.main.DeviceCredentialSchemeActivity"; // const-string v2, "jp.auone.wallet.ui.main.DeviceCredentialSchemeActivity"
(( android.content.Intent ) v0 ).setClassName ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1340 */
/* const-string/jumbo v1, "shortcut_start" */
final String v2 = "jp.auone.wallet.qr"; // const-string v2, "jp.auone.wallet.qr"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1341 */
/* const/high16 v1, 0x20000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1342 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* if-nez v1, :cond_0 */
/* .line 1343 */
/* const v1, 0x110f004c */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->showToast(II)V */
/* .line 1344 */
/* .line 1346 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean launchCalculator ( ) {
/* .locals 4 */
/* .line 1260 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1261 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.calculator"; // const-string v2, "com.miui.calculator"
final String v3 = "com.miui.calculator.cal.CalculatorActivity"; // const-string v3, "com.miui.calculator.cal.CalculatorActivity"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1262 */
/* const/high16 v1, 0x200000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1263 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchCamera ( java.lang.String p0 ) {
/* .locals 4 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1031 */
final String v0 = "double_click_volume_down"; // const-string v0, "double_click_volume_down"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_0 */
/* .line 1032 */
final String v0 = "launch_camera_and_take_photo"; // const-string v0, "launch_camera_and_take_photo"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 1033 */
} // :cond_0
v0 = this.mPowerManager;
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v1 */
int v3 = 5; // const/4 v3, 0x5
(( android.os.PowerManager ) v0 ).wakeUp ( v1, v2, v3, p1 ); // invoke-virtual {v0, v1, v2, v3, p1}, Landroid/os/PowerManager;->wakeUp(JILjava/lang/String;)V
/* .line 1036 */
} // :cond_1
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1037 */
/* .local v0, "cameraIntent":Landroid/content/Intent; */
/* const/high16 v1, 0x200000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1038 */
final String v1 = "ShowCameraWhenLocked"; // const-string v1, "ShowCameraWhenLocked"
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1039 */
v1 = this.mWindowManagerPolicy;
/* instance-of v3, v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 1040 */
/* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
/* .line 1041 */
v1 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).getKeyguardActive ( ); // invoke-virtual {v1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->getKeyguardActive()Z
/* if-nez v1, :cond_3 */
/* .line 1042 */
final String v1 = "power_double_tap"; // const-string v1, "power_double_tap"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
} // :cond_2
int v2 = 0; // const/4 v2, 0x0
} // :cond_3
} // :goto_0
/* nop */
/* .line 1040 */
} // :goto_1
final String v1 = "StartActivityWhenLocked"; // const-string v1, "StartActivityWhenLocked"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1044 */
} // :cond_4
final String v1 = "android.media.action.STILL_IMAGE_CAMERA"; // const-string v1, "android.media.action.STILL_IMAGE_CAMERA"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1045 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 1046 */
/* const v2, 0x110f00a3 */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1045 */
android.content.ComponentName .unflattenFromString ( v1 );
/* .line 1047 */
/* .local v1, "mCameraComponentName":Landroid/content/ComponentName; */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1049 */
final String v2 = "com.android.systemui.camera_launch_source"; // const-string v2, "com.android.systemui.camera_launch_source"
(( android.content.Intent ) v0 ).putExtra ( v2, p1 ); // invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1050 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchCamera ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .param p2, "mode" # Ljava/lang/String; */
/* .line 1057 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1058 */
/* .local v0, "cameraIntent":Landroid/content/Intent; */
/* const/high16 v1, 0x800000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1059 */
final String v1 = "ShowCameraWhenLocked"; // const-string v1, "ShowCameraWhenLocked"
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1060 */
final String v1 = "StartActivityWhenLocked"; // const-string v1, "StartActivityWhenLocked"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1061 */
final String v1 = "android.media.action.VOICE_COMMAND"; // const-string v1, "android.media.action.VOICE_COMMAND"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1062 */
v1 = this.mContext;
/* .line 1063 */
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v2, 0x110f00a3 */
(( android.content.res.Resources ) v1 ).getString ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 1062 */
android.content.ComponentName .unflattenFromString ( v1 );
/* .line 1064 */
/* .local v1, "mCameraComponentName":Landroid/content/ComponentName; */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1065 */
final String v2 = "android.intent.extra.CAMERA_MODE"; // const-string v2, "android.intent.extra.CAMERA_MODE"
(( android.content.Intent ) v0 ).putExtra ( v2, p2 ); // invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1066 */
/* nop */
/* .line 1067 */
final String v2 = "android-app://com.android.camera"; // const-string v2, "android-app://com.android.camera"
android.net.Uri .parse ( v2 );
/* .line 1066 */
final String v3 = "android.intent.extra.REFERRER"; // const-string v3, "android.intent.extra.REFERRER"
(( android.content.Intent ) v0 ).putExtra ( v3, v2 ); // invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
/* .line 1068 */
final String v2 = "com.android.systemui.camera_launch_source"; // const-string v2, "com.android.systemui.camera_launch_source"
(( android.content.Intent ) v0 ).putExtra ( v2, p1 ); // invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1069 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchCameraAndTakePhoto ( ) {
/* .locals 1 */
/* .line 488 */
final String v0 = "launch_camera_and_take_photo"; // const-string v0, "launch_camera_and_take_photo"
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;)Z */
} // .end method
private Boolean launchDumpLog ( ) {
/* .locals 3 */
/* .line 1270 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1271 */
/* .local v0, "dumpLogIntent":Landroid/content/Intent; */
final String v1 = "com.miui.bugreport"; // const-string v1, "com.miui.bugreport"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1272 */
final String v1 = "com.miui.bugreport.service.action.DUMPLOG"; // const-string v1, "com.miui.bugreport.service.action.DUMPLOG"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1273 */
/* const/16 v1, 0x20 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1274 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).sendBroadcastAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 1275 */
/* const v1, 0x110f037c */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {p0, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->showToast(II)V */
/* .line 1276 */
v1 = this.mStabilityLocalServiceInternal;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1277 */
/* .line 1279 */
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean launchDumpLogOrContact ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1294 */
/* iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsVoiceCapable:Z */
/* if-nez v0, :cond_0 */
/* .line 1295 */
final String v0 = "mIsVoiceCapable false, so lunch emergencyDialer"; // const-string v0, "mIsVoiceCapable false, so lunch emergencyDialer"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 1296 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1297 */
/* .local v0, "intent":Landroid/content/Intent; */
/* nop */
/* .line 1298 */
final String v1 = "com.android.phone/com.android.phone.EmergencyDialer"; // const-string v1, "com.android.phone/com.android.phone.EmergencyDialer"
android.content.ComponentName .unflattenFromString ( v1 );
/* .line 1299 */
/* .local v1, "componentName":Landroid/content/ComponentName; */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1300 */
/* const-string/jumbo v2, "shortcut" */
(( android.content.Intent ) v0 ).putExtra ( v2, p1 ); // invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1301 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 1303 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "componentName":Landroid/content/ComponentName;
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLog()Z */
/* .line 1305 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
private Boolean launchGlobalPowerGuide ( ) {
/* .locals 3 */
/* .line 492 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 495 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "pad"; // const-string v1, "pad"
/* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getDeviceType()Ljava/lang/String; */
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_3 */
final String v1 = "fold"; // const-string v1, "fold"
/* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getDeviceType()Ljava/lang/String; */
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 496 */
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isLargeScreen()Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 498 */
} // :cond_0
final String v1 = "ruyi"; // const-string v1, "ruyi"
v2 = miui.os.Build.DEVICE;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* iget-boolean v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsFolded:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 500 */
final String v1 = "com.miui.miinput.action.SMALL_SCREEN_GLOBAL_POWER_GUIDE"; // const-string v1, "com.miui.miinput.action.SMALL_SCREEN_GLOBAL_POWER_GUIDE"
/* .local v1, "actionName":Ljava/lang/String; */
/* .line 502 */
} // .end local v1 # "actionName":Ljava/lang/String;
} // :cond_1
/* sget-boolean v1, Lmiui/hardware/input/InputFeature;->IS_SUPPORT_KDDI_POWER_GUIDE:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 503 */
final String v1 = "com.miui.miinput.action.KDDI_GLOBAL_POWER_GUIDE"; // const-string v1, "com.miui.miinput.action.KDDI_GLOBAL_POWER_GUIDE"
} // :cond_2
final String v1 = "com.miui.miinput.action.GLOBAL_POWER_GUIDE"; // const-string v1, "com.miui.miinput.action.GLOBAL_POWER_GUIDE"
/* .restart local v1 # "actionName":Ljava/lang/String; */
} // :goto_0
/* .line 497 */
} // .end local v1 # "actionName":Ljava/lang/String;
} // :cond_3
} // :goto_1
final String v1 = "com.miui.miinput.action.ROTATION_FOLLOWS_SENSOR_GLOBAL_POWER_GUIDE"; // const-string v1, "com.miui.miinput.action.ROTATION_FOLLOWS_SENSOR_GLOBAL_POWER_GUIDE"
/* .line 505 */
/* .restart local v1 # "actionName":Ljava/lang/String; */
} // :goto_2
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 506 */
final String v2 = "com.miui.securitycore"; // const-string v2, "com.miui.securitycore"
(( android.content.Intent ) v0 ).setPackage ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 507 */
/* const v2, 0x10008000 */
(( android.content.Intent ) v0 ).setFlags ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 508 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchGooglePay ( ) {
/* .locals 1 */
/* .line 1368 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean launchGoogleSearch ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 711 */
v0 = this.mWindowManagerPolicy;
/* if-nez v0, :cond_0 */
/* .line 712 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v0;
/* .line 714 */
} // :cond_0
v0 = this.mWindowManagerPolicy;
/* instance-of v0, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 715 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 716 */
/* .local v0, "args":Landroid/os/Bundle; */
final String v1 = "android.intent.extra.ASSIST_INPUT_DEVICE_ID"; // const-string v1, "android.intent.extra.ASSIST_INPUT_DEVICE_ID"
int v2 = -1; // const/4 v2, -0x1
(( android.os.Bundle ) v0 ).putInt ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 717 */
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->setGoogleSearchInvocationTypeKey(Ljava/lang/String;Landroid/os/Bundle;)V */
/* .line 718 */
v1 = this.mWindowManagerPolicy;
/* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
int v2 = 0; // const/4 v2, 0x0
v1 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).launchAssistAction ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchAssistAction(Ljava/lang/String;Landroid/os/Bundle;)Z
/* .line 721 */
} // .end local v0 # "args":Landroid/os/Bundle;
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean launchHome ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 573 */
v0 = this.mContext;
v0 = com.miui.server.input.custom.InputMiuiDesktopMode .launchHome ( v0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 574 */
int v0 = 1; // const/4 v0, 0x1
/* .line 576 */
} // :cond_0
v0 = this.mWindowManagerPolicy;
/* if-nez v0, :cond_1 */
/* .line 577 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v0;
/* .line 579 */
} // :cond_1
v0 = this.mWindowManagerPolicy;
/* instance-of v1, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 580 */
/* check-cast v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
v0 = (( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).launchHome ( ); // invoke-virtual {v0}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->launchHome()Z
/* .line 582 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean launchMiNotes ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 7 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .param p2, "extras" # Landroid/os/Bundle; */
/* .line 909 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.miui.pad.notes.action.INSERT_OR_EDIT"; // const-string v1, "com.miui.pad.notes.action.INSERT_OR_EDIT"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 910 */
/* .local v0, "intent":Landroid/content/Intent; */
/* const-string/jumbo v1, "vnd.android.cursor.item/text_note" */
(( android.content.Intent ) v0 ).setType ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;
/* .line 911 */
/* const-string/jumbo v1, "shortcut" */
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 912 */
final String v1 = "com.miui.notes"; // const-string v1, "com.miui.notes"
/* .line 913 */
/* .local v1, "packageName":Ljava/lang/String; */
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 914 */
int v2 = 0; // const/4 v2, 0x0
/* if-nez p2, :cond_0 */
/* .line 915 */
v2 = /* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z */
/* .line 917 */
} // :cond_0
final String v3 = "scene"; // const-string v3, "scene"
(( android.os.Bundle ) p2 ).getString ( v3, v2 ); // invoke-virtual {p2, v3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* .line 918 */
/* .local v4, "scene":Ljava/lang/String; */
/* if-nez v4, :cond_1 */
/* .line 919 */
v2 = /* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z */
/* .line 921 */
} // :cond_1
(( android.content.Intent ) v0 ).putExtra ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 922 */
final String v3 = "com.miui.creation"; // const-string v3, "com.miui.creation"
v3 = /* invoke-direct {p0, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAppInstalled(Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 923 */
final String v1 = "com.miui.creation"; // const-string v1, "com.miui.creation"
/* .line 924 */
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 925 */
final String v3 = "com.miui.creation.action.INSERT_CREATION"; // const-string v3, "com.miui.creation.action.INSERT_CREATION"
(( android.content.Intent ) v0 ).setAction ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 926 */
/* const-string/jumbo v3, "vnd.android.cursor.item/create_creation" */
(( android.content.Intent ) v0 ).setType ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;
/* .line 927 */
final String v3 = "com.miui.creation already installed"; // const-string v3, "com.miui.creation already installed"
com.android.server.policy.MiuiInputLog .defaults ( v3 );
/* .line 929 */
} // :cond_2
/* const/high16 v3, 0x14000000 */
(( android.content.Intent ) v0 ).addFlags ( v3 ); // invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 930 */
final String v3 = "keyguard"; // const-string v3, "keyguard"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v5 = 1; // const/4 v5, 0x1
/* if-nez v3, :cond_5 */
/* .line 931 */
final String v3 = "off_screen"; // const-string v3, "off_screen"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_3
/* .line 935 */
} // :cond_3
int v2 = 0; // const/4 v2, 0x0
/* .line 936 */
/* .local v2, "bundle":Landroid/os/Bundle; */
final String v3 = "app"; // const-string v3, "app"
v3 = (( java.lang.String ) v3 ).equals ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 937 */
v3 = this.mContext;
int v6 = 0; // const/4 v6, 0x0
android.util.MiuiMultiWindowUtils .getActivityOptions ( v3,v1,v5,v6 );
/* .line 939 */
/* .local v3, "activityOptions":Landroid/app/ActivityOptions; */
if ( v3 != null) { // if-eqz v3, :cond_4
/* .line 940 */
(( android.app.ActivityOptions ) v3 ).toBundle ( ); // invoke-virtual {v3}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;
/* .line 943 */
} // .end local v3 # "activityOptions":Landroid/app/ActivityOptions;
} // :cond_4
v3 = /* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z */
/* .line 932 */
} // .end local v2 # "bundle":Landroid/os/Bundle;
} // :cond_5
} // :goto_0
final String v3 = "StartActivityWhenLocked"; // const-string v3, "StartActivityWhenLocked"
(( android.content.Intent ) v0 ).putExtra ( v3, v5 ); // invoke-virtual {v0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 933 */
v2 = /* invoke-direct {p0, v0, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSimple(Landroid/content/Intent;Landroid/os/Bundle;)Z */
} // .end method
private Boolean launchMiPay ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "eventSource" # Ljava/lang/String; */
/* .line 987 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 988 */
/* .local v0, "nfcIntent":Landroid/content/Intent; */
/* const/high16 v1, 0x20000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 989 */
final String v1 = "StartActivityWhenLocked"; // const-string v1, "StartActivityWhenLocked"
int v2 = 1; // const/4 v2, 0x1
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 990 */
final String v1 = "com.miui.intent.action.DOUBLE_CLICK"; // const-string v1, "com.miui.intent.action.DOUBLE_CLICK"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 991 */
final String v1 = "event_source"; // const-string v1, "event_source"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 992 */
final String v1 = "com.miui.tsmclient"; // const-string v1, "com.miui.tsmclient"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 993 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchMiuiSoS ( ) {
/* .locals 2 */
/* .line 745 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "miui.intent.action.LAUNCH_SOS"; // const-string v1, "miui.intent.action.LAUNCH_SOS"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 746 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.android.settings"; // const-string v1, "com.android.settings"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 747 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchMusicReader ( ) {
/* .locals 4 */
/* .line 1106 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.VIEW"; // const-string v1, "android.intent.action.VIEW"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1107 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.player"; // const-string v2, "com.miui.player"
final String v3 = "com.miui.player.ui.MusicBrowserActivity"; // const-string v3, "com.miui.player.ui.MusicBrowserActivity"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1109 */
/* .local v1, "comp":Landroid/content/ComponentName; */
final String v2 = "miui-music://radar?miref=com.miui.knock"; // const-string v2, "miui-music://radar?miref=com.miui.knock"
android.net.Uri .parse ( v2 );
(( android.content.Intent ) v0 ).setData ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
/* .line 1110 */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1111 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchRecents ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 586 */
v0 = this.mContext;
v0 = com.miui.server.input.custom.InputMiuiDesktopMode .launchRecents ( v0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 587 */
int v0 = 1; // const/4 v0, 0x1
/* .line 589 */
} // :cond_0
v0 = this.mWindowManagerPolicy;
/* if-nez v0, :cond_1 */
/* .line 590 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v0;
/* .line 592 */
} // :cond_1
v0 = this.mStatusBarManagerInternal;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 593 */
/* .line 595 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean launchScreenRecorder ( ) {
/* .locals 3 */
/* .line 1076 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1077 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "miui.intent.screenrecorder.RECORDER_SERVICE"; // const-string v1, "miui.intent.screenrecorder.RECORDER_SERVICE"
(( android.content.Intent ) v0 ).setAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1078 */
final String v1 = "com.miui.screenrecorder"; // const-string v1, "com.miui.screenrecorder"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1079 */
final String v1 = "is_start_immediately"; // const-string v1, "is_start_immediately"
int v2 = 0; // const/4 v2, 0x0
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1080 */
v1 = this.mContext;
(( android.content.Context ) v1 ).startService ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
/* .line 1081 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean launchSmartHomeService ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 4 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .param p2, "extra" # Landroid/os/Bundle; */
/* .line 762 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 763 */
/* .local v0, "smartHomeIntent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.miui.smarthomeplus.UWBEntryService"; // const-string v2, "com.miui.smarthomeplus.UWBEntryService"
final String v3 = "com.miui.smarthomeplus"; // const-string v3, "com.miui.smarthomeplus"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 765 */
final String v1 = "long_press_power_key"; // const-string v1, "long_press_power_key"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 766 */
final String v1 = "extra_long_press_power_function"; // const-string v1, "extra_long_press_power_function"
(( android.os.Bundle ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
v2 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v2, :cond_0 */
/* .line 767 */
/* nop */
/* .line 768 */
(( android.os.Bundle ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 767 */
/* invoke-direct {p0, v3, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getLongPressPowerKeyFunctionIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent; */
/* .line 770 */
} // :cond_0
final String v1 = "event_source"; // const-string v1, "event_source"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 772 */
try { // :try_start_0
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startServiceAsUser ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 775 */
/* .line 773 */
/* :catch_0 */
/* move-exception v1 */
/* .line 774 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .error ( v2 );
/* .line 776 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean launchSoundRecorder ( ) {
/* .locals 4 */
/* .line 1096 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 1097 */
/* .local v0, "intent":Landroid/content/Intent; */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.android.soundrecorder"; // const-string v2, "com.android.soundrecorder"
final String v3 = "com.android.soundrecorder.SoundRecorder"; // const-string v3, "com.android.soundrecorder.SoundRecorder"
/* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 1098 */
/* .local v1, "comp":Landroid/content/ComponentName; */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1099 */
v2 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
} // .end method
private Boolean launchTorch ( android.os.Bundle p0 ) {
/* .locals 6 */
/* .param p1, "extra" # Landroid/os/Bundle; */
/* .line 1000 */
int v0 = 1; // const/4 v0, 0x1
int v1 = 0; // const/4 v1, 0x0
if ( p1 != null) { // if-eqz p1, :cond_2
/* const-string/jumbo v2, "skip_telecom_check" */
v2 = (( android.os.Bundle ) p1 ).getBoolean ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
/* if-nez v2, :cond_2 */
/* .line 1001 */
(( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).getTelecommService ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getTelecommService()Landroid/telecom/TelecomManager;
/* .line 1002 */
/* .local v2, "telecomManager":Landroid/telecom/TelecomManager; */
/* iget-boolean v3, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWifiOnly:Z */
/* if-nez v3, :cond_1 */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 1003 */
v3 = (( android.telecom.TelecomManager ) v2 ).getCallState ( ); // invoke-virtual {v2}, Landroid/telecom/TelecomManager;->getCallState()I
/* if-nez v3, :cond_0 */
} // :cond_0
/* move v3, v1 */
} // :cond_1
} // :goto_0
/* move v3, v0 */
/* .line 1004 */
/* .local v3, "phoneIdle":Z */
} // :goto_1
/* if-nez v3, :cond_2 */
/* .line 1005 */
final String v0 = "not launch torch,because telecom"; // const-string v0, "not launch torch,because telecom"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 1006 */
/* .line 1010 */
} // .end local v2 # "telecomManager":Landroid/telecom/TelecomManager;
} // .end local v3 # "phoneIdle":Z
} // :cond_2
/* iget-boolean v2, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mHasCameraFlash:Z */
/* if-nez v2, :cond_3 */
/* .line 1011 */
final String v0 = "not have camera flash"; // const-string v0, "not have camera flash"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 1012 */
/* .line 1014 */
} // :cond_3
v2 = this.mContext;
(( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v3, "torch_state" */
v2 = android.provider.Settings$Global .getInt ( v2,v3,v1 );
if ( v2 != null) { // if-eqz v2, :cond_4
/* move v2, v0 */
} // :cond_4
/* move v2, v1 */
/* .line 1016 */
/* .local v2, "isOpen":Z */
} // :goto_2
/* new-instance v3, Landroid/content/Intent; */
final String v4 = "miui.intent.action.TOGGLE_TORCH"; // const-string v4, "miui.intent.action.TOGGLE_TORCH"
/* invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1017 */
/* .local v3, "intent":Landroid/content/Intent; */
/* const/high16 v4, 0x10000000 */
(( android.content.Intent ) v3 ).addFlags ( v4 ); // invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 1018 */
final String v4 = "miui.intent.extra.IS_ENABLE"; // const-string v4, "miui.intent.extra.IS_ENABLE"
if ( p1 != null) { // if-eqz p1, :cond_5
/* .line 1019 */
final String v5 = "extra_torch_enabled"; // const-string v5, "extra_torch_enabled"
v1 = (( android.os.Bundle ) p1 ).getBoolean ( v5, v1 ); // invoke-virtual {p1, v5, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
(( android.content.Intent ) v3 ).putExtra ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1021 */
} // :cond_5
/* if-nez v2, :cond_6 */
/* move v1, v0 */
} // :cond_6
(( android.content.Intent ) v3 ).putExtra ( v4, v1 ); // invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1023 */
} // :goto_3
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v3 ); // invoke-virtual {v1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1024 */
} // .end method
private Boolean launchVoiceAssistant ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 7 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .param p2, "extra" # Landroid/os/Bundle; */
/* .line 874 */
final String v0 = "powerGuide"; // const-string v0, "powerGuide"
final String v1 = "extra_long_press_power_function"; // const-string v1, "extra_long_press_power_function"
final String v2 = "android.intent.action.ASSIST"; // const-string v2, "android.intent.action.ASSIST"
int v3 = 0; // const/4 v3, 0x0
try { // :try_start_0
/* new-instance v4, Landroid/content/Intent; */
/* invoke-direct {v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 875 */
/* .local v4, "intent":Landroid/content/Intent; */
final String v5 = "keyboard"; // const-string v5, "keyboard"
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* const-string/jumbo v6, "voice_assist_start_from_key" */
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 876 */
try { // :try_start_1
final String v0 = "external_keyboard_xiaoai_key"; // const-string v0, "external_keyboard_xiaoai_key"
(( android.content.Intent ) v4 ).putExtra ( v6, v0 ); // invoke-virtual {v4, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 878 */
} // :cond_0
final String v5 = "long_press_power_key"; // const-string v5, "long_press_power_key"
v5 = (( java.lang.String ) v5 ).equals ( p1 ); // invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v5 != null) { // if-eqz v5, :cond_1
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 879 */
(( android.os.Bundle ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
v5 = android.text.TextUtils .isEmpty ( v5 );
/* if-nez v5, :cond_1 */
/* .line 880 */
/* nop */
/* .line 881 */
(( android.os.Bundle ) p2 ).getString ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 880 */
/* invoke-direct {p0, v2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getLongPressPowerKeyFunctionIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent; */
/* .line 882 */
} // .end local v4 # "intent":Landroid/content/Intent;
/* .local v1, "intent":Landroid/content/Intent; */
v2 = (( android.os.Bundle ) p2 ).getBoolean ( v0, v3 ); // invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
(( android.content.Intent ) v1 ).putExtra ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* move-object v4, v1 */
/* .line 884 */
} // .end local v1 # "intent":Landroid/content/Intent;
/* .restart local v4 # "intent":Landroid/content/Intent; */
} // :cond_1
if ( p2 != null) { // if-eqz p2, :cond_2
final String v0 = "ai_key"; // const-string v0, "ai_key"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 885 */
final String v0 = "key_action"; // const-string v0, "key_action"
final String v1 = "extra_key_action"; // const-string v1, "extra_key_action"
v1 = (( android.os.Bundle ) p2 ).getInt ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
(( android.content.Intent ) v4 ).putExtra ( v0, v1 ); // invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 886 */
final String v0 = "key_event_time"; // const-string v0, "key_event_time"
final String v1 = "extra_key_event_time"; // const-string v1, "extra_key_event_time"
(( android.os.Bundle ) p2 ).getLong ( v1 ); // invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J
/* move-result-wide v1 */
(( android.content.Intent ) v4 ).putExtra ( v0, v1, v2 ); // invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 888 */
} // :cond_2
} // :goto_0
(( android.content.Intent ) v4 ).putExtra ( v6, p1 ); // invoke-virtual {v4, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 889 */
final String v0 = "app.send.wakeup.command"; // const-string v0, "app.send.wakeup.command"
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( android.content.Intent ) v4 ).putExtra ( v0, v1, v2 ); // invoke-virtual {v4, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
/* .line 892 */
} // :goto_1
final String v0 = "com.miui.voiceassist"; // const-string v0, "com.miui.voiceassist"
(( android.content.Intent ) v4 ).setPackage ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 893 */
v0 = this.mContext;
/* .line 895 */
(( android.content.Context ) v0 ).getResources ( ); // invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* const v1, 0x110f00ab */
(( android.content.res.Resources ) v0 ).getString ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
/* .line 894 */
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 896 */
/* .local v0, "componentName":Landroid/content/ComponentName; */
(( android.content.Intent ) v4 ).setComponent ( v0 ); // invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 897 */
final String v1 = "launchVoiceAssistant startForegroundServiceAsUser"; // const-string v1, "launchVoiceAssistant startForegroundServiceAsUser"
com.android.server.policy.MiuiInputLog .major ( v1 );
/* .line 898 */
v1 = this.mContext;
v2 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v1 ).startForegroundServiceAsUser ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Context;->startForegroundServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
if ( v1 != null) { // if-eqz v1, :cond_3
int v3 = 1; // const/4 v3, 0x1
} // :cond_3
/* .line 899 */
} // .end local v0 # "componentName":Landroid/content/ComponentName;
} // .end local v4 # "intent":Landroid/content/Intent;
/* :catch_0 */
/* move-exception v0 */
/* .line 900 */
/* .local v0, "e":Ljava/lang/Exception; */
final String v1 = "Exception"; // const-string v1, "Exception"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
/* .line 902 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // .end method
private Boolean launchWexinScanner ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "shortcut" # Ljava/lang/String; */
/* .line 1118 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.MAIN"; // const-string v1, "android.intent.action.MAIN"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1119 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.category.LAUNCHER"; // const-string v1, "android.intent.category.LAUNCHER"
(( android.content.Intent ) v0 ).addCategory ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1120 */
/* new-instance v1, Landroid/content/ComponentName; */
final String v2 = "com.tencent.mm.plugin.scanner.ui.BaseScanUI"; // const-string v2, "com.tencent.mm.plugin.scanner.ui.BaseScanUI"
final String v3 = "com.tencent.mm"; // const-string v3, "com.tencent.mm"
/* invoke-direct {v1, v3, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 1122 */
v1 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 1123 */
int v1 = 1; // const/4 v1, 0x1
/* .line 1125 */
} // :cond_0
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 1126 */
/* .local v1, "dialogIntent":Landroid/content/Intent; */
final String v2 = "com.android.settings"; // const-string v2, "com.android.settings"
final String v4 = "com.android.settings.KeyDownloadDialogActivity"; // const-string v4, "com.android.settings.KeyDownloadDialogActivity"
(( android.content.Intent ) v1 ).setClassName ( v2, v4 ); // invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1127 */
/* const-string/jumbo v2, "shortcut" */
(( android.content.Intent ) v1 ).putExtra ( v2, p1 ); // invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1128 */
final String v2 = "packageName"; // const-string v2, "packageName"
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1129 */
final String v2 = "dual"; // const-string v2, "dual"
int v3 = 0; // const/4 v3, 0x0
(( android.content.Intent ) v1 ).putExtra ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
/* .line 1130 */
v2 = this.mContext;
/* const v4, 0x110f004f */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
/* const-string/jumbo v4, "title" */
(( android.content.Intent ) v1 ).putExtra ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1131 */
v2 = this.mContext;
/* const v4, 0x110f0050 */
(( android.content.Context ) v2 ).getString ( v4 ); // invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;
final String v4 = "content"; // const-string v4, "content"
(( android.content.Intent ) v1 ).putExtra ( v4, v2 ); // invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1132 */
/* const v2, 0x10008000 */
(( android.content.Intent ) v1 ).setFlags ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 1133 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 1134 */
} // .end method
private void makeAllUserToastAndShow ( java.lang.String p0, Integer p1 ) {
/* .locals 1 */
/* .param p1, "text" # Ljava/lang/String; */
/* .param p2, "duration" # I */
/* .line 464 */
v0 = this.mContext;
android.widget.Toast .makeText ( v0,p1,p2 );
/* .line 465 */
/* .local v0, "toast":Landroid/widget/Toast; */
(( android.widget.Toast ) v0 ).show ( ); // invoke-virtual {v0}, Landroid/widget/Toast;->show()V
/* .line 466 */
return;
} // .end method
private Boolean redirectAccessibilityHearSoundFunction ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "function" # Ljava/lang/String; */
/* .line 396 */
v0 = android.text.TextUtils .isEmpty ( p1 );
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 397 */
final String v0 = "redirect accessibility hear sound function fail! because action is null"; // const-string v0, "redirect accessibility hear sound function fail! because action is null"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 399 */
/* .line 402 */
} // :cond_0
final String v0 = "back_double_tap"; // const-string v0, "back_double_tap"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
/* .line 403 */
final String v0 = "back_triple_tap"; // const-string v0, "back_triple_tap"
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 404 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 405 */
/* .local v0, "redirectFunction":Ljava/lang/String; */
final String v2 = "hear_sound"; // const-string v2, "hear_sound"
v2 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 406 */
final String v0 = "live_subtitles_full_screen"; // const-string v0, "live_subtitles_full_screen"
/* .line 408 */
/* const-string/jumbo v2, "startAiSubtitlesFullscreen" */
/* invoke-direct {p0, p1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 410 */
} // :cond_2
final String v2 = "hear_sound_subtitle"; // const-string v2, "hear_sound_subtitle"
v2 = (( java.lang.String ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 412 */
final String v0 = "live_subtitles"; // const-string v0, "live_subtitles"
/* .line 413 */
/* const-string/jumbo v2, "startAiSubtitlesWindow" */
/* invoke-direct {p0, p1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 417 */
} // :cond_3
} // :goto_0
v2 = android.text.TextUtils .isEmpty ( v0 );
/* if-nez v2, :cond_4 */
/* .line 418 */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v2 = -2; // const/4 v2, -0x2
android.provider.Settings$System .putStringForUser ( v1,p1,v0,v2 );
/* .line 420 */
int v1 = 1; // const/4 v1, 0x1
/* .line 423 */
} // .end local v0 # "redirectFunction":Ljava/lang/String;
} // :cond_4
} // .end method
public static void sendRecordCountEvent ( android.content.Context p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 9 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "category" # Ljava/lang/String; */
/* .param p2, "event" # Ljava/lang/String; */
/* .line 1436 */
final String v0 = "count_event"; // const-string v0, "count_event"
/* .line 1437 */
/* .local v0, "STAT_TYPE_COUNT_EVENT":Ljava/lang/String; */
final String v1 = "com.miui.gallery"; // const-string v1, "com.miui.gallery"
/* .line 1438 */
/* .local v1, "GALLERY_PKG_NAME":Ljava/lang/String; */
final String v2 = "com.miui.gallery.intent.action.SEND_STAT"; // const-string v2, "com.miui.gallery.intent.action.SEND_STAT"
/* .line 1439 */
/* .local v2, "ACTION_SEND_STAT":Ljava/lang/String; */
/* const-string/jumbo v3, "stat_type" */
/* .line 1440 */
/* .local v3, "EXTRA_STAT_TYPE":Ljava/lang/String; */
final String v4 = "category"; // const-string v4, "category"
/* .line 1441 */
/* .local v4, "EXTRA_CATEGORY":Ljava/lang/String; */
final String v5 = "event"; // const-string v5, "event"
/* .line 1443 */
/* .local v5, "EXTRA_EVENT":Ljava/lang/String; */
/* new-instance v6, Landroid/content/Intent; */
final String v7 = "com.miui.gallery.intent.action.SEND_STAT"; // const-string v7, "com.miui.gallery.intent.action.SEND_STAT"
/* invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 1444 */
/* .local v6, "intent":Landroid/content/Intent; */
final String v7 = "com.miui.gallery"; // const-string v7, "com.miui.gallery"
(( android.content.Intent ) v6 ).setPackage ( v7 ); // invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 1445 */
/* const-string/jumbo v7, "stat_type" */
final String v8 = "count_event"; // const-string v8, "count_event"
(( android.content.Intent ) v6 ).putExtra ( v7, v8 ); // invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1446 */
final String v7 = "category"; // const-string v7, "category"
(( android.content.Intent ) v6 ).putExtra ( v7, p1 ); // invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1447 */
final String v7 = "event"; // const-string v7, "event"
(( android.content.Intent ) v6 ).putExtra ( v7, p2 ); // invoke-virtual {v6, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 1448 */
(( android.content.Context ) p0 ).sendBroadcast ( v6 ); // invoke-virtual {p0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 1449 */
return;
} // .end method
private Boolean setAccessibilityTalkBackState ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 614 */
/* nop */
/* .line 615 */
final String v0 = "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"; // const-string v0, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 617 */
/* .local v0, "componentName":Landroid/content/ComponentName; */
final String v1 = "key_combination_volume_up_volume_down"; // const-string v1, "key_combination_volume_up_volume_down"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = -2; // const/4 v2, -0x2
int v3 = 1; // const/4 v3, 0x1
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 618 */
v1 = this.mContext;
int v4 = 0; // const/4 v4, 0x0
com.android.internal.accessibility.util.AccessibilityUtils .setAccessibilityServiceState ( v1,v0,v4,v2 );
/* .line 619 */
/* .line 621 */
} // :cond_0
v1 = this.mContext;
/* .line 622 */
(( android.content.ComponentName ) v0 ).getClassName ( ); // invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;
v4 = /* invoke-direct {p0, v4}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isAccessibilityFunctionEnabled(Ljava/lang/String;)Z */
/* xor-int/2addr v4, v3 */
/* .line 621 */
com.android.internal.accessibility.util.AccessibilityUtils .setAccessibilityServiceState ( v1,v0,v4,v2 );
/* .line 624 */
} // .end method
private void setGoogleSearchInvocationTypeKey ( java.lang.String p0, android.os.Bundle p1 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .param p2, "args" # Landroid/os/Bundle; */
/* .line 725 */
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 726 */
return;
/* .line 728 */
} // :cond_0
v0 = (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
/* sparse-switch v0, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v0 = "long_press_home_key"; // const-string v0, "long_press_home_key"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
/* :sswitch_1 */
final String v0 = "long_press_power_key"; // const-string v0, "long_press_power_key"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
int v0 = -1; // const/4 v0, -0x1
} // :goto_1
final String v1 = "invocation_type"; // const-string v1, "invocation_type"
/* packed-switch v0, :pswitch_data_0 */
/* .line 738 */
int v0 = 2; // const/4 v0, 0x2
(( android.os.Bundle ) p2 ).putInt ( v1, v0 ); // invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 734 */
/* :pswitch_0 */
int v0 = 5; // const/4 v0, 0x5
(( android.os.Bundle ) p2 ).putInt ( v1, v0 ); // invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 736 */
/* .line 730 */
/* :pswitch_1 */
int v0 = 6; // const/4 v0, 0x6
(( android.os.Bundle ) p2 ).putInt ( v1, v0 ); // invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 732 */
/* nop */
/* .line 742 */
} // :goto_2
return;
/* nop */
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x4b0ca89a -> :sswitch_1 */
/* 0x5add439e -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private Boolean showMenu ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "action" # Ljava/lang/String; */
/* .line 445 */
v0 = this.mWindowManagerPolicy;
/* instance-of v1, v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 446 */
/* check-cast v0, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v0 ).triggerShowMenu ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerShowMenu(Ljava/lang/String;)Z
/* .line 448 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void showToast ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "resourceId" # I */
/* .param p2, "length" # I */
/* .line 1284 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/util/ShortCutActionsUtils;II)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 1287 */
return;
} // .end method
private Boolean takeScreenshot ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .line 1327 */
/* new-instance v0, Lcom/android/internal/util/ScreenshotRequest$Builder; */
/* invoke-direct {v0, p1}, Lcom/android/internal/util/ScreenshotRequest$Builder;-><init>(I)V */
/* .line 1328 */
(( com.android.internal.util.ScreenshotRequest$Builder ) v0 ).build ( ); // invoke-virtual {v0}, Lcom/android/internal/util/ScreenshotRequest$Builder;->build()Lcom/android/internal/util/ScreenshotRequest;
/* .line 1329 */
/* .local v0, "request":Lcom/android/internal/util/ScreenshotRequest; */
v1 = this.mScreenshotHelper;
v2 = this.mHandler;
int v3 = 0; // const/4 v3, 0x0
(( com.android.internal.util.ScreenshotHelper ) v1 ).takeScreenshot ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Lcom/android/internal/util/ScreenshotHelper;->takeScreenshot(Lcom/android/internal/util/ScreenshotRequest;Landroid/os/Handler;Ljava/util/function/Consumer;)V
/* .line 1330 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
private Boolean takeScreenshotWithBundle ( Float[] p0 ) {
/* .locals 5 */
/* .param p1, "pathList" # [F */
/* .line 1355 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 1356 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "partial.screenshot.points"; // const-string v1, "partial.screenshot.points"
(( android.os.Bundle ) v0 ).putFloatArray ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V
/* .line 1357 */
/* new-instance v1, Lcom/android/internal/util/ScreenshotRequest$Builder; */
/* const/16 v2, 0x64 */
/* invoke-direct {v1, v2}, Lcom/android/internal/util/ScreenshotRequest$Builder;-><init>(I)V */
/* .line 1358 */
(( com.android.internal.util.ScreenshotRequest$Builder ) v1 ).setExtraBundle ( v0 ); // invoke-virtual {v1, v0}, Lcom/android/internal/util/ScreenshotRequest$Builder;->setExtraBundle(Landroid/os/Bundle;)Lcom/android/internal/util/ScreenshotRequest$Builder;
(( com.android.internal.util.ScreenshotRequest$Builder ) v1 ).build ( ); // invoke-virtual {v1}, Lcom/android/internal/util/ScreenshotRequest$Builder;->build()Lcom/android/internal/util/ScreenshotRequest;
/* .line 1359 */
/* .local v1, "request":Lcom/android/internal/util/ScreenshotRequest; */
v2 = this.mScreenshotHelper;
v3 = this.mHandler;
int v4 = 0; // const/4 v4, 0x0
(( com.android.internal.util.ScreenshotHelper ) v2 ).takeScreenshot ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Lcom/android/internal/util/ScreenshotHelper;->takeScreenshot(Lcom/android/internal/util/ScreenshotRequest;Landroid/os/Handler;Ljava/util/function/Consumer;)V
/* .line 1360 */
int v2 = 1; // const/4 v2, 0x1
} // .end method
private void triggerHapticFeedback ( Boolean p0, java.lang.String p1, java.lang.String p2, Boolean p3, java.lang.String p4 ) {
/* .locals 2 */
/* .param p1, "triggered" # Z */
/* .param p2, "shortcut" # Ljava/lang/String; */
/* .param p3, "function" # Ljava/lang/String; */
/* .param p4, "hapticFeedback" # Z */
/* .param p5, "effectKey" # Ljava/lang/String; */
/* .line 600 */
if ( p1 != null) { // if-eqz p1, :cond_0
if ( p4 != null) { // if-eqz p4, :cond_0
v0 = this.mHapticFeedbackUtil;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 601 */
int v1 = 0; // const/4 v1, 0x0
(( miui.util.HapticFeedbackUtil ) v0 ).performHapticFeedback ( p5, v1 ); // invoke-virtual {v0, p5, v1}, Lmiui/util/HapticFeedbackUtil;->performHapticFeedback(Ljava/lang/String;Z)Z
/* .line 603 */
} // :cond_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "shortcut:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " trigger function:"; // const-string v1, " trigger function:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p3 ); // invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " result:"; // const-string v1, " result:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 605 */
return;
} // .end method
/* # virtual methods */
public void bootComplete ( ) {
/* .locals 1 */
/* .line 210 */
v0 = miui.hardware.input.InputFeature .supportAccessibilityLiveSubtitles ( );
/* iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mSupportAccessibilitySubtitle:Z */
/* .line 211 */
return;
} // .end method
public android.content.Intent getIntent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .param p2, "className" # Ljava/lang/String; */
/* .line 563 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "android.intent.action.MAIN"; // const-string v1, "android.intent.action.MAIN"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 564 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "android.intent.category.LAUNCHER"; // const-string v1, "android.intent.category.LAUNCHER"
(( android.content.Intent ) v0 ).addCategory ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;
/* .line 565 */
(( android.content.Intent ) v0 ).setClassName ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 566 */
/* const/high16 v1, 0x10200000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 569 */
} // .end method
android.telecom.TelecomManager getTelecommService ( ) {
/* .locals 2 */
/* .line 976 */
v0 = this.mContext;
/* const-string/jumbo v1, "telecom" */
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/telecom/TelecomManager; */
} // .end method
public Boolean launchAiShortcut ( ) {
/* .locals 4 */
/* .line 849 */
try { // :try_start_0
final String v0 = "knock launch ai shortcut"; // const-string v0, "knock launch ai shortcut"
com.android.server.policy.MiuiInputLog .major ( v0 );
/* .line 850 */
/* new-instance v0, Landroid/content/Intent; */
/* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
/* .line 851 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "is_show_dialog"; // const-string v1, "is_show_dialog"
final String v2 = "false"; // const-string v2, "false"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 852 */
final String v1 = "from"; // const-string v1, "from"
final String v2 = "knock"; // const-string v2, "knock"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 853 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).setFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
/* .line 854 */
final String v1 = "com.miui.voiceassist/com.xiaomi.voiceassistant.AiSettings.AiShortcutActivity"; // const-string v1, "com.miui.voiceassist/com.xiaomi.voiceassistant.AiSettings.AiShortcutActivity"
/* .line 855 */
android.content.ComponentName .unflattenFromString ( v1 );
/* .line 856 */
/* .local v1, "componentName":Landroid/content/ComponentName; */
(( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 857 */
v2 = this.mContext;
v3 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v2 ).startActivityAsUser ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* :try_end_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_2 */
/* .catch Ljava/lang/IllegalStateException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/RuntimeException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 858 */
int v2 = 1; // const/4 v2, 0x1
/* .line 863 */
} // .end local v0 # "intent":Landroid/content/Intent;
} // .end local v1 # "componentName":Landroid/content/ComponentName;
/* :catch_0 */
/* move-exception v0 */
/* .line 864 */
/* .local v0, "e":Ljava/lang/RuntimeException; */
final String v1 = "RuntimeException"; // const-string v1, "RuntimeException"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
/* .line 861 */
} // .end local v0 # "e":Ljava/lang/RuntimeException;
/* :catch_1 */
/* move-exception v0 */
/* .line 862 */
/* .local v0, "e":Ljava/lang/IllegalStateException; */
final String v1 = "IllegalStateException"; // const-string v1, "IllegalStateException"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
} // .end local v0 # "e":Ljava/lang/IllegalStateException;
/* .line 859 */
/* :catch_2 */
/* move-exception v0 */
/* .line 860 */
/* .local v0, "e":Ljava/lang/SecurityException; */
final String v1 = "SecurityException"; // const-string v1, "SecurityException"
com.android.server.policy.MiuiInputLog .error ( v1,v0 );
/* .line 865 */
} // .end local v0 # "e":Ljava/lang/SecurityException;
} // :goto_0
/* nop */
/* .line 866 */
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean launchAppSmallWindow ( android.os.Bundle p0 ) {
/* .locals 8 */
/* .param p1, "extras" # Landroid/os/Bundle; */
/* .line 532 */
int v0 = 0; // const/4 v0, 0x0
/* if-nez p1, :cond_0 */
/* .line 533 */
/* .line 535 */
} // :cond_0
final String v1 = "packageName"; // const-string v1, "packageName"
(( android.os.Bundle ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 536 */
/* .local v1, "packageName":Ljava/lang/String; */
final String v2 = "className"; // const-string v2, "className"
(( android.os.Bundle ) p1 ).getString ( v2 ); // invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 537 */
/* .local v2, "className":Ljava/lang/String; */
v3 = android.text.TextUtils .isEmpty ( v1 );
/* if-nez v3, :cond_5 */
v3 = android.text.TextUtils .isEmpty ( v2 );
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 540 */
} // :cond_1
(( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).getIntent ( v1, v2 ); // invoke-virtual {p0, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 542 */
/* .local v3, "intent":Landroid/content/Intent; */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 543 */
v5 = android.os.UserHandle .myUserId ( );
/* .line 542 */
v4 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v4 ).isFreeFormExit ( v1, v5 ); // invoke-virtual {v4, v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isFreeFormExit(Ljava/lang/String;I)Z
if ( v4 != null) { // if-eqz v4, :cond_2
/* .line 544 */
/* .line 546 */
} // :cond_2
v4 = this.mContext;
int v5 = 1; // const/4 v5, 0x1
android.util.MiuiMultiWindowUtils .getActivityOptions ( v4,v1,v5,v0 );
/* .line 549 */
/* .local v4, "options":Landroid/app/ActivityOptions; */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 550 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 551 */
v7 = android.os.UserHandle .myUserId ( );
/* .line 550 */
v6 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v6 ).isSplitExist ( v1, v7 ); // invoke-virtual {v6, v1, v7}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->isSplitExist(Ljava/lang/String;I)Z
if ( v6 != null) { // if-eqz v6, :cond_3
/* .line 552 */
/* .line 554 */
} // :cond_3
v0 = this.mContext;
(( android.app.ActivityOptions ) v4 ).toBundle ( ); // invoke-virtual {v4}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;
v7 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v0 ).startActivityAsUser ( v3, v6, v7 ); // invoke-virtual {v0, v3, v6, v7}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V
/* .line 557 */
} // :cond_4
v0 = this.mContext;
v6 = android.os.UserHandle.CURRENT;
(( android.content.Context ) v0 ).startActivityAsUser ( v3, v6 ); // invoke-virtual {v0, v3, v6}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
/* .line 559 */
} // :goto_0
/* .line 538 */
} // .end local v3 # "intent":Landroid/content/Intent;
} // .end local v4 # "options":Landroid/app/ActivityOptions;
} // :cond_5
} // :goto_1
} // .end method
public Boolean launchControlCenter ( ) {
/* .locals 3 */
/* .line 819 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "action_panels_operation"; // const-string v1, "action_panels_operation"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 820 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "operation"; // const-string v1, "operation"
final String v2 = "reverse_quick_settings_panel"; // const-string v2, "reverse_quick_settings_panel"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 821 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 822 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean launchNotificationCenter ( ) {
/* .locals 3 */
/* .line 829 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "action_panels_operation"; // const-string v1, "action_panels_operation"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 830 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "operation"; // const-string v1, "operation"
final String v2 = "reverse_notifications_panel"; // const-string v2, "reverse_notifications_panel"
(( android.content.Intent ) v0 ).putExtra ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
/* .line 831 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 832 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public Boolean mute ( ) {
/* .locals 3 */
/* .line 839 */
v0 = this.mContext;
v0 = android.provider.MiuiSettings$SoundMode .isSilenceModeOn ( v0 );
/* .line 840 */
/* .local v0, "isSilenceModeOn":Z */
v1 = this.mContext;
/* xor-int/lit8 v2, v0, 0x1 */
android.provider.MiuiSettings$SoundMode .setSilenceModeOn ( v1,v2 );
/* .line 841 */
int v1 = 1; // const/4 v1, 0x1
} // .end method
public void notifyFoldStatus ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "folded" # Z */
/* .line 512 */
/* iput-boolean p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsFolded:Z */
/* .line 513 */
return;
} // .end method
public void notifyKidSpaceChanged ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "inKidSpace" # Z */
/* .line 1432 */
/* iput-boolean p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsKidMode:Z */
/* .line 1433 */
return;
} // .end method
public void setWifiOnly ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "wifiOnly" # Z */
/* .line 980 */
/* iput-boolean p1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mWifiOnly:Z */
/* .line 981 */
return;
} // .end method
public void systemReady ( ) {
/* .locals 2 */
/* .line 202 */
/* const-class v0, Lcom/android/server/policy/WindowManagerPolicy; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/policy/WindowManagerPolicy; */
this.mWindowManagerPolicy = v0;
/* .line 203 */
v0 = this.mContext;
final String v1 = "power"; // const-string v1, "power"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/os/PowerManager; */
this.mPowerManager = v0;
/* .line 204 */
/* const-class v0, Lcom/android/server/statusbar/StatusBarManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/statusbar/StatusBarManagerInternal; */
this.mStatusBarManagerInternal = v0;
/* .line 205 */
/* const-class v0, Lcom/miui/server/stability/StabilityLocalServiceInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/miui/server/stability/StabilityLocalServiceInternal; */
this.mStabilityLocalServiceInternal = v0;
/* .line 206 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsSystemReady:Z */
/* .line 207 */
return;
} // .end method
public Boolean triggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3 ) {
/* .locals 6 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "extras" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .line 758 */
final String v5 = "mesh_heavy"; // const-string v5, "mesh_heavy"
/* move-object v0, p0 */
/* move-object v1, p1 */
/* move-object v2, p2 */
/* move-object v3, p3 */
/* move v4, p4 */
v0 = /* invoke-virtual/range {v0 ..v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;ZLjava/lang/String;)Z */
} // .end method
public Boolean triggerFunction ( java.lang.String p0, java.lang.String p1, android.os.Bundle p2, Boolean p3, java.lang.String p4 ) {
/* .locals 8 */
/* .param p1, "function" # Ljava/lang/String; */
/* .param p2, "action" # Ljava/lang/String; */
/* .param p3, "extras" # Landroid/os/Bundle; */
/* .param p4, "hapticFeedback" # Z */
/* .param p5, "effectKey" # Ljava/lang/String; */
/* .line 215 */
/* iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsSystemReady:Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 216 */
final String v0 = "Failed to trigger shortcut function, system is not ready"; // const-string v0, "Failed to trigger shortcut function, system is not ready"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 217 */
/* .line 221 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsKidMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 222 */
final String v0 = "Children\'s space does not trigger shortcut gestures"; // const-string v0, "Children\'s space does not trigger shortcut gestures"
com.android.server.policy.MiuiInputLog .defaults ( v0 );
/* .line 223 */
/* .line 225 */
} // :cond_1
v0 = this.mShortcutOneTrackHelper;
if ( v0 != null) { // if-eqz v0, :cond_3
if ( p3 != null) { // if-eqz p3, :cond_2
final String v0 = "disable_shortcut_track"; // const-string v0, "disable_shortcut_track"
v0 = (( android.os.Bundle ) p3 ).getBoolean ( v0 ); // invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
/* if-nez v0, :cond_3 */
/* .line 227 */
} // :cond_2
v0 = this.mShortcutOneTrackHelper;
(( com.android.server.input.shortcut.ShortcutOneTrackHelper ) v0 ).trackShortcutEventTrigger ( p2, p1 ); // invoke-virtual {v0, p2, p1}, Lcom/android/server/input/shortcut/ShortcutOneTrackHelper;->trackShortcutEventTrigger(Ljava/lang/String;Ljava/lang/String;)V
/* .line 230 */
} // :cond_3
int v0 = 0; // const/4 v0, 0x0
/* .line 232 */
/* .local v0, "triggered":Z */
/* if-nez p5, :cond_4 */
/* .line 233 */
/* const-string/jumbo p5, "virtual_key_longpress" */
/* .line 237 */
} // :cond_4
v2 = v2 = this.mWindowManagerPolicy;
final String v3 = "dump_log_or_secret_code"; // const-string v3, "dump_log_or_secret_code"
final String v4 = "dump_log"; // const-string v4, "dump_log"
/* if-nez v2, :cond_7 */
/* .line 239 */
v1 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v1, :cond_6 */
/* .line 240 */
v1 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_5
/* iget-boolean v1, p0, Lcom/miui/server/input/util/ShortCutActionsUtils;->mIsVoiceCapable:Z */
if ( v1 != null) { // if-eqz v1, :cond_5
/* .line 244 */
} // :cond_5
/* const-string/jumbo v1, "user setup not complete" */
com.android.server.policy.MiuiInputLog .major ( v1 );
/* .line 241 */
} // :cond_6
} // :goto_0
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLog()Z */
/* .line 242 */
/* move-object v2, p0 */
/* move v3, v0 */
/* move-object v4, p2 */
/* move-object v5, p1 */
/* move v6, p4 */
/* move-object v7, p5 */
/* invoke-direct/range {v2 ..v7}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerHapticFeedback(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V */
/* .line 246 */
} // :goto_1
/* .line 248 */
} // :cond_7
/* const-string/jumbo v2, "screen_shot" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v5 = 1; // const/4 v5, 0x1
if ( v2 != null) { // if-eqz v2, :cond_8
/* .line 249 */
v0 = /* invoke-direct {p0, v5}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z */
/* .line 250 */
v1 = this.mContext;
/* const-string/jumbo v2, "screenshot" */
final String v3 = "key_shortcut"; // const-string v3, "key_shortcut"
com.miui.server.input.util.ShortCutActionsUtils .sendRecordCountEvent ( v1,v2,v3 );
/* goto/16 :goto_2 */
/* .line 251 */
} // :cond_8
final String v2 = "partial_screen_shot"; // const-string v2, "partial_screen_shot"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_a
/* .line 252 */
if ( p3 != null) { // if-eqz p3, :cond_9
/* .line 253 */
final String v1 = "partial.screenshot.points"; // const-string v1, "partial.screenshot.points"
(( android.os.Bundle ) p3 ).getFloatArray ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F
v0 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshotWithBundle([F)Z */
/* goto/16 :goto_2 */
/* .line 255 */
} // :cond_9
/* const/16 v1, 0x64 */
v0 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z */
/* goto/16 :goto_2 */
/* .line 257 */
} // :cond_a
/* const-string/jumbo v2, "screenshot_without_anim" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_b
/* .line 258 */
/* const/16 v1, 0x63 */
v0 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z */
/* goto/16 :goto_2 */
/* .line 259 */
} // :cond_b
/* const-string/jumbo v2, "stylus_partial_screenshot" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_c
/* .line 260 */
/* const/16 v1, 0x62 */
v0 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->takeScreenshot(I)Z */
/* goto/16 :goto_2 */
/* .line 261 */
} // :cond_c
final String v2 = "launch_voice_assistant"; // const-string v2, "launch_voice_assistant"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_d
/* .line 262 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchVoiceAssistant(Ljava/lang/String;Landroid/os/Bundle;)Z */
/* .line 263 */
final String p5 = "screen_button_voice_assist"; // const-string p5, "screen_button_voice_assist"
/* goto/16 :goto_2 */
/* .line 264 */
} // :cond_d
final String v2 = "launch_ai_shortcut"; // const-string v2, "launch_ai_shortcut"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_e
/* .line 265 */
v0 = (( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).launchAiShortcut ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAiShortcut()Z
/* goto/16 :goto_2 */
/* .line 266 */
} // :cond_e
final String v2 = "launch_alipay_scanner"; // const-string v2, "launch_alipay_scanner"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_f
/* .line 267 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAlipayScanner(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 268 */
} // :cond_f
final String v2 = "launch_alipay_payment_code"; // const-string v2, "launch_alipay_payment_code"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_10
/* .line 269 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAlipayPaymentCode(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 270 */
} // :cond_10
final String v2 = "launch_alipay_health_code"; // const-string v2, "launch_alipay_health_code"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_11
/* .line 271 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAlipayHealthCode(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 272 */
} // :cond_11
final String v2 = "launch_wechat_scanner"; // const-string v2, "launch_wechat_scanner"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_12
/* .line 273 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchWexinScanner(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 274 */
} // :cond_12
final String v2 = "launch_wechat_payment_code"; // const-string v2, "launch_wechat_payment_code"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_13
/* .line 275 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->lauchWeixinPaymentCode(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 276 */
} // :cond_13
/* const-string/jumbo v2, "turn_on_torch" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_14
/* .line 277 */
v0 = /* invoke-direct {p0, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchTorch(Landroid/os/Bundle;)Z */
/* goto/16 :goto_2 */
/* .line 278 */
} // :cond_14
final String v2 = "launch_calculator"; // const-string v2, "launch_calculator"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_15
/* .line 279 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCalculator()Z */
/* goto/16 :goto_2 */
/* .line 280 */
} // :cond_15
final String v2 = "launch_camera"; // const-string v2, "launch_camera"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_16
/* .line 281 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 282 */
} // :cond_16
v2 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_17
/* .line 283 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLog()Z */
/* goto/16 :goto_2 */
/* .line 284 */
} // :cond_17
final String v2 = "launch_control_center"; // const-string v2, "launch_control_center"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_18
/* .line 285 */
v0 = (( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).launchControlCenter ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchControlCenter()Z
/* goto/16 :goto_2 */
/* .line 286 */
} // :cond_18
final String v2 = "launch_notification_center"; // const-string v2, "launch_notification_center"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_19
/* .line 287 */
v0 = (( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).launchNotificationCenter ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchNotificationCenter()Z
/* goto/16 :goto_2 */
/* .line 288 */
} // :cond_19
final String v2 = "mute"; // const-string v2, "mute"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1a
/* .line 289 */
v0 = (( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).mute ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->mute()Z
/* goto/16 :goto_2 */
/* .line 290 */
} // :cond_1a
final String v2 = "launch_google_search"; // const-string v2, "launch_google_search"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1b
/* .line 291 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchGoogleSearch(Ljava/lang/String;)Z */
/* .line 292 */
final String p5 = "screen_button_voice_assist"; // const-string p5, "screen_button_voice_assist"
/* goto/16 :goto_2 */
/* .line 293 */
} // :cond_1b
final String v2 = "go_to_sleep"; // const-string v2, "go_to_sleep"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1c
/* .line 294 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->goToSleep(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 295 */
} // :cond_1c
v2 = (( java.lang.String ) v3 ).equals ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1d
/* .line 296 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchDumpLogOrContact(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 297 */
} // :cond_1d
final String v2 = "au_pay"; // const-string v2, "au_pay"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1e
/* .line 298 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAuPay()Z */
/* goto/16 :goto_2 */
/* .line 299 */
} // :cond_1e
final String v2 = "google_pay"; // const-string v2, "google_pay"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1f
/* .line 300 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchGooglePay()Z */
/* goto/16 :goto_2 */
/* .line 301 */
} // :cond_1f
final String v2 = "mi_pay"; // const-string v2, "mi_pay"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_20
/* .line 302 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchMiPay(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 303 */
} // :cond_20
final String v2 = "note"; // const-string v2, "note"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_21
/* .line 304 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchMiNotes(Ljava/lang/String;Landroid/os/Bundle;)Z */
/* goto/16 :goto_2 */
/* .line 305 */
} // :cond_21
final String v2 = "launch_smarthome"; // const-string v2, "launch_smarthome"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_22
/* .line 306 */
v0 = /* invoke-direct {p0, p2, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchSmartHomeService(Ljava/lang/String;Landroid/os/Bundle;)Z */
/* goto/16 :goto_2 */
/* .line 307 */
} // :cond_22
final String v2 = "find_device_locate"; // const-string v2, "find_device_locate"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_23
/* .line 308 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->findDeviceLocate()Z */
/* goto/16 :goto_2 */
/* .line 309 */
} // :cond_23
final String v2 = "launch_sound_recorder"; // const-string v2, "launch_sound_recorder"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_24
/* .line 310 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchSoundRecorder()Z */
/* goto/16 :goto_2 */
/* .line 311 */
} // :cond_24
final String v2 = "launch_camera_capture"; // const-string v2, "launch_camera_capture"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_25
/* .line 312 */
final String v1 = "CAPTURE"; // const-string v1, "CAPTURE"
v0 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 313 */
} // :cond_25
final String v2 = "launch_camera_video"; // const-string v2, "launch_camera_video"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_26
/* .line 314 */
final String v1 = "VIDEO"; // const-string v1, "VIDEO"
v0 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCamera(Ljava/lang/String;Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 315 */
} // :cond_26
/* const-string/jumbo v2, "vibrate" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_27
/* .line 316 */
v0 = (( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).vibrate ( ); // invoke-virtual {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->vibrate()Z
/* goto/16 :goto_2 */
/* .line 317 */
} // :cond_27
final String v2 = "launch_screen_recorder"; // const-string v2, "launch_screen_recorder"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_28
/* .line 318 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchScreenRecorder()Z */
/* goto/16 :goto_2 */
/* .line 319 */
} // :cond_28
final String v2 = "miui_talkback"; // const-string v2, "miui_talkback"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_29
/* .line 320 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->setAccessibilityTalkBackState(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 321 */
} // :cond_29
/* const-string/jumbo v2, "voice_control" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2a
/* .line 322 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityVoiceControl()Z */
/* goto/16 :goto_2 */
/* .line 323 */
} // :cond_2a
final String v2 = "environment_speech_recognition"; // const-string v2, "environment_speech_recognition"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2b
/* .line 324 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityEnvironmentSpeechRecognition()Z */
/* goto/16 :goto_2 */
/* .line 325 */
} // :cond_2b
final String v2 = "hear_sound"; // const-string v2, "hear_sound"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2c
/* .line 326 */
v0 = /* invoke-direct {p0, p2, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityHearSound(Ljava/lang/String;Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 327 */
} // :cond_2c
final String v2 = "hear_sound_subtitle"; // const-string v2, "hear_sound_subtitle"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2d
/* .line 328 */
v0 = /* invoke-direct {p0, p2, p1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityHearSoundSubtitle(Ljava/lang/String;Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 329 */
} // :cond_2d
final String v2 = "launch_global_power_guide"; // const-string v2, "launch_global_power_guide"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2e
/* .line 330 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchGlobalPowerGuide()Z */
/* goto/16 :goto_2 */
/* .line 331 */
} // :cond_2e
/* const-string/jumbo v2, "split_ltr" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2f
/* .line 332 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).toggleSplitByGesture ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->toggleSplitByGesture(Z)Z
/* goto/16 :goto_2 */
/* .line 333 */
} // :cond_2f
/* const-string/jumbo v2, "split_rtl" */
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_30
/* .line 334 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).toggleSplitByGesture ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->toggleSplitByGesture(Z)Z
/* goto/16 :goto_2 */
/* .line 335 */
} // :cond_30
final String v2 = "launch_home"; // const-string v2, "launch_home"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_31
/* .line 336 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchHome(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 337 */
} // :cond_31
final String v2 = "launch_recents"; // const-string v2, "launch_recents"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_32
/* .line 338 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchRecents(Ljava/lang/String;)Z */
/* goto/16 :goto_2 */
/* .line 339 */
} // :cond_32
final String v2 = "close_app"; // const-string v2, "close_app"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_34
/* .line 340 */
if ( p3 != null) { // if-eqz p3, :cond_33
/* .line 341 */
/* const-string/jumbo v1, "shortcut_type" */
(( android.os.Bundle ) p3 ).getString ( v1 ); // invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 340 */
final String v2 = "phone_shortcut"; // const-string v2, "phone_shortcut"
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_33
/* .line 342 */
v1 = this.mWindowManagerPolicy;
/* check-cast v1, Lcom/android/server/policy/BaseMiuiPhoneWindowManager; */
(( com.android.server.policy.BaseMiuiPhoneWindowManager ) v1 ).triggerCloseApp ( p2 ); // invoke-virtual {v1, p2}, Lcom/android/server/policy/BaseMiuiPhoneWindowManager;->triggerCloseApp(Ljava/lang/String;)Z
/* goto/16 :goto_2 */
/* .line 344 */
} // :cond_33
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
(( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).onMetaKeyCombination ( ); // invoke-virtual {v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->onMetaKeyCombination()V
/* goto/16 :goto_2 */
/* .line 346 */
} // :cond_34
final String v2 = "launch_app_small_window"; // const-string v2, "launch_app_small_window"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_35
/* .line 348 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 349 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).freeFormAndFullScreenToggleByKeyCombination ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->freeFormAndFullScreenToggleByKeyCombination(Z)Z
/* goto/16 :goto_2 */
/* .line 350 */
} // :cond_35
final String v2 = "launch_app_full_window"; // const-string v2, "launch_app_full_window"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_36
/* .line 352 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
/* .line 353 */
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).freeFormAndFullScreenToggleByKeyCombination ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->freeFormAndFullScreenToggleByKeyCombination(Z)Z
/* goto/16 :goto_2 */
/* .line 354 */
} // :cond_36
final String v2 = "launch_split_screen_to_left"; // const-string v2, "launch_split_screen_to_left"
v2 = (( java.lang.String ) v2 ).equals ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_37
/* .line 355 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v2 ).setSplitScreenDirection ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->setSplitScreenDirection(I)Z
/* goto/16 :goto_2 */
/* .line 357 */
} // :cond_37
final String v1 = "launch_split_screen_to_right"; // const-string v1, "launch_split_screen_to_right"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_38
/* .line 358 */
com.android.server.wm.ActivityTaskManagerServiceImpl .getInstance ( );
v0 = (( com.android.server.wm.ActivityTaskManagerServiceImpl ) v1 ).setSplitScreenDirection ( v5 ); // invoke-virtual {v1, v5}, Lcom/android/server/wm/ActivityTaskManagerServiceImpl;->setSplitScreenDirection(I)Z
/* goto/16 :goto_2 */
/* .line 360 */
} // :cond_38
final String v1 = "launch_app"; // const-string v1, "launch_app"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_39
/* .line 361 */
v0 = (( com.miui.server.input.util.ShortCutActionsUtils ) p0 ).launchAppSmallWindow ( p3 ); // invoke-virtual {p0, p3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAppSmallWindow(Landroid/os/Bundle;)Z
/* goto/16 :goto_2 */
/* .line 362 */
} // :cond_39
final String v1 = "launch_camera_and_take_photo"; // const-string v1, "launch_camera_and_take_photo"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3a
/* .line 363 */
/* const v1, 0x10001 */
java.lang.Integer .valueOf ( v1 );
/* filled-new-array {v1}, [Ljava/lang/Object; */
/* const-string/jumbo v2, "sendEvent" */
com.android.server.camera.CameraOpt .callMethod ( v2,v1 );
/* .line 364 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchCameraAndTakePhoto()Z */
/* .line 365 */
} // :cond_3a
final String v1 = "launch_sos"; // const-string v1, "launch_sos"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3b
/* .line 366 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchMiuiSoS()Z */
/* .line 367 */
} // :cond_3b
final String v1 = "launch_xiaoai_guide"; // const-string v1, "launch_xiaoai_guide"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3c
/* .line 368 */
/* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getPowerGuideIntent()Landroid/content/Intent; */
v0 = /* invoke-direct {p0, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchApp(Landroid/content/Intent;)Z */
/* .line 369 */
} // :cond_3c
/* const-string/jumbo v1, "show_menu" */
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3d
/* .line 370 */
v0 = /* invoke-direct {p0, p2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->showMenu(Ljava/lang/String;)Z */
/* .line 371 */
} // :cond_3d
/* const-string/jumbo v1, "split_screen" */
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3e
/* .line 372 */
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->isSupportSpiltScreen()Z */
if ( v1 != null) { // if-eqz v1, :cond_40
v1 = this.mStatusBarManagerInternal;
if ( v1 != null) { // if-eqz v1, :cond_40
/* .line 373 */
/* .line 374 */
int v0 = 1; // const/4 v0, 0x1
/* .line 376 */
} // :cond_3e
final String v1 = "live_subtitles_full_screen"; // const-string v1, "live_subtitles_full_screen"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3f
/* .line 378 */
/* const-string/jumbo v1, "startAiSubtitlesFullscreen" */
v0 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 380 */
} // :cond_3f
final String v1 = "live_subtitles"; // const-string v1, "live_subtitles"
v1 = (( java.lang.String ) v1 ).equals ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_40
/* .line 381 */
/* const-string/jumbo v1, "startAiSubtitlesWindow" */
v0 = /* invoke-direct {p0, p2, v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->launchAccessibilityLiveSubtitle(Ljava/lang/String;Ljava/lang/String;)Z */
/* .line 385 */
} // :cond_40
} // :goto_2
/* move-object v1, p0 */
/* move v2, v0 */
/* move-object v3, p2 */
/* move-object v4, p1 */
/* move v5, p4 */
/* move-object v6, p5 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerHapticFeedback(ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V */
/* .line 386 */
} // .end method
public Boolean vibrate ( ) {
/* .locals 1 */
/* .line 1088 */
v0 = this.mContext;
miui.util.AudioManagerHelper .toggleVibrateSetting ( v0 );
/* .line 1089 */
int v0 = 1; // const/4 v0, 0x1
} // .end method
