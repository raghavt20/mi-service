.class public Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
.super Ljava/lang/Object;
.source "MiuiCustomizeShortCutUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;,
        Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    }
.end annotation


# static fields
.field public static final ALL_SHORTCUT:I = 0x0

.field public static final APP_SHORTCUT:I = 0x2

.field public static final ATTRIBUTE_ALT:Ljava/lang/String; = "alt"

.field public static final ATTRIBUTE_CLASSNAME:Ljava/lang/String; = "className"

.field public static final ATTRIBUTE_CTRL:Ljava/lang/String; = "ctrl"

.field public static final ATTRIBUTE_CUSTOMIZED:Ljava/lang/String; = "customized"

.field public static final ATTRIBUTE_ENABLE:Ljava/lang/String; = "enable"

.field public static final ATTRIBUTE_KEYCODE:Ljava/lang/String; = "keycode"

.field public static final ATTRIBUTE_LEFT_ALT:Ljava/lang/String; = "lalt"

.field public static final ATTRIBUTE_META:Ljava/lang/String; = "meta"

.field public static final ATTRIBUTE_PACKAGENAME:Ljava/lang/String; = "packageName"

.field public static final ATTRIBUTE_RIGHT_ALT:Ljava/lang/String; = "ralt"

.field public static final ATTRIBUTE_SHIFT:Ljava/lang/String; = "shift"

.field public static final ATTRIBUTE_TYPE:Ljava/lang/String; = "type"

.field public static final SYSTEM_SHORTCUT:I = 0x1

.field private static final UPDATE_ADD:I = 0x0

.field private static final UPDATE_DEL:I = 0x2

.field private static final UPDATE_MODIFY:I = 0x1

.field private static final UPDATE_RESET_APP:I = 0x3

.field private static final UPDATE_RESET_SYSTEM:I = 0x4

.field private static volatile mInstance:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final TAG_1:Ljava/lang/String;

.field private final TAG_2:Ljava/lang/String;

.field private final mBackupFile:Ljava/io/File;

.field private mContext:Landroid/content/Context;

.field private mDefaultAppShortcutInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultShortcutInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultSystemShortcutInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mFile:Ljava/io/File;

.field private volatile mInterceptShortcutKey:Z

.field private mKeyboardShortcutInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mNeedLoadInfoForDefaultRes:Z

.field private final mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

.field private final mShortCutFileName:Ljava/lang/String;

.field private mShortcutInfos:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray<",
            "Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static synthetic $r8$lambda$1diIpLZS0vOPaYBfDAtNs8fJ1ho(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/content/IntentFilter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$enableAutoRemoveShortCutWhenAppRemove$3(Landroid/content/IntentFilter;)V

    return-void
.end method

.method public static synthetic $r8$lambda$NYK3NNB8OfMcVHcqqizSmSzdCQ0(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$new$2()V

    return-void
.end method

.method public static synthetic $r8$lambda$TOYPeznr90UgOQk0QdV9JiIfrtY(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$new$1()V

    return-void
.end method

.method public static synthetic $r8$lambda$wvqXSwxxbLhjO8kbuXrrwdsq9QM(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/content/IntentFilter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->lambda$new$0(Landroid/content/IntentFilter;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mdelShortCutByPackage(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->delShortCutByPackage(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "MiuiCustomizeShortCutUtils"

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->TAG:Ljava/lang/String;

    .line 55
    const-string v0, "ShortcutInfos"

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->TAG_1:Ljava/lang/String;

    .line 56
    const-string v0, "ShortcutInfo"

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->TAG_2:Ljava/lang/String;

    .line 81
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultShortcutInfos:Ljava/util/List;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultSystemShortcutInfos:Ljava/util/List;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultAppShortcutInfos:Ljava/util/List;

    .line 87
    const-string v0, "miuishortcuts"

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortCutFileName:Ljava/lang/String;

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mInterceptShortcutKey:Z

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z

    .line 97
    iput-object p1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    .line 99
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "miuishortcuts.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    .line 101
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getDataSystemDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "miuishortcuts-backup.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mBackupFile:Ljava/io/File;

    .line 104
    new-instance v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    invoke-direct {v0, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V

    iput-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    .line 105
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    .local v0, "local_switch":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0, v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/content/IntentFilter;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 115
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 116
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda3;

    invoke-direct {v2, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 118
    return-void
.end method

.method private delShortCutByPackage(Ljava/lang/String;)V
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .line 440
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 441
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/KeyboardShortcutInfo;

    .line 442
    .local v1, "info":Landroid/view/KeyboardShortcutInfo;
    invoke-virtual {v1}, Landroid/view/KeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/KeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 443
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/view/KeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Landroid/util/LongSparseArray;->delete(J)V

    .line 444
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 445
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->writeShortCuts()V

    .line 446
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delShortCutByPackage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiCustomizeShortCutUtils"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    goto :goto_1

    .line 440
    .end local v1    # "info":Landroid/view/KeyboardShortcutInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 450
    .end local v0    # "i":I
    :cond_1
    :goto_1
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 121
    sget-object v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mInstance:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    if-nez v0, :cond_1

    .line 122
    const-class v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    monitor-enter v0

    .line 123
    :try_start_0
    sget-object v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mInstance:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    if-nez v1, :cond_0

    .line 124
    new-instance v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    invoke-direct {v1, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mInstance:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    .line 126
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 128
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mInstance:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;

    return-object v0
.end method

.method private synthetic lambda$enableAutoRemoveShortCutWhenAppRemove$3(Landroid/content/IntentFilter;)V
    .locals 6
    .param p1, "packageFilter"    # Landroid/content/IntentFilter;

    .line 190
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2;

    invoke-direct {v1, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$2;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private synthetic lambda$new$0(Landroid/content/IntentFilter;)V
    .locals 6
    .param p1, "local_switch"    # Landroid/content/IntentFilter;

    .line 108
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$1;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;)V

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private synthetic lambda$new$1()V
    .locals 0

    .line 115
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V

    return-void
.end method

.method private synthetic lambda$new$2()V
    .locals 1

    .line 116
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadFromDefaultRes(Z)V

    return-void
.end method

.method private loadFromDefaultRes(Z)V
    .locals 8
    .param p1, "onlyInitDefault"    # Z

    .line 207
    :try_start_0
    iget-boolean v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z

    if-nez v0, :cond_0

    .line 208
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "miuishortcuts"

    const-string/jumbo v2, "xml"

    const-string v3, "android.miui"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 212
    .local v0, "resId":I
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 213
    .local v1, "parser":Landroid/content/res/XmlResourceParser;
    const-string v2, "ShortcutInfos"

    invoke-static {v1, v2}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 214
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 216
    .local v2, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    :goto_0
    invoke-static {v1}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 217
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 218
    goto :goto_1

    .line 220
    :cond_1
    const-string v3, "ShortcutInfo"

    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 221
    nop

    .line 235
    :goto_1
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultShortcutInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 236
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultAppShortcutInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 237
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultSystemShortcutInfos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 238
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 239
    .local v4, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    invoke-virtual {v5, v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;

    move-result-object v5

    .line 240
    .local v5, "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultShortcutInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v6

    if-nez v6, :cond_2

    .line 242
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultAppShortcutInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    if-nez p1, :cond_3

    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 244
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 247
    :cond_2
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultSystemShortcutInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    if-nez p1, :cond_3

    .line 249
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    .end local v4    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .end local v5    # "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
    :cond_3
    :goto_3
    goto :goto_2

    .line 253
    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z

    .line 256
    .end local v0    # "resId":I
    .end local v1    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v2    # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    goto :goto_5

    .line 223
    .restart local v0    # "resId":I
    .restart local v1    # "parser":Landroid/content/res/XmlResourceParser;
    .restart local v2    # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    :cond_5
    invoke-direct {p0, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->readFromParser(Lorg/xmlpull/v1/XmlPullParser;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    move-result-object v3

    .line 224
    .local v3, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    if-nez p1, :cond_7

    .line 225
    invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v4

    if-nez v4, :cond_6

    .line 226
    iget-object v4, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 227
    iget-object v4, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    goto :goto_4

    .line 230
    :cond_6
    iget-object v4, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 233
    :cond_7
    :goto_4
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    nop

    .end local v3    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    goto/16 :goto_0

    .line 254
    .end local v0    # "resId":I
    .end local v1    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v2    # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiCustomizeShortCutUtils"

    const-string v2, "Got exception parsing bookmarks."

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 257
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_5
    return-void
.end method

.method private readFromParser(Lorg/xmlpull/v1/XmlPullParser;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .locals 23
    .param p1, "in"    # Lorg/xmlpull/v1/XmlPullParser;

    .line 466
    move-object/from16 v0, p1

    const-string/jumbo v1, "true"

    .line 467
    .local v1, "metaStateOn":Ljava/lang/String;
    const-string v2, "meta"

    const/4 v3, 0x0

    invoke-interface {v0, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 468
    .local v2, "meta":Z
    const-string/jumbo v4, "shift"

    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 469
    .local v4, "shift":Z
    const-string v5, "ctrl"

    invoke-interface {v0, v3, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 470
    .local v5, "ctrl":Z
    const-string v6, "alt"

    invoke-interface {v0, v3, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 471
    .local v6, "alt":Z
    const-string v7, "ralt"

    invoke-interface {v0, v3, v7}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 472
    .local v7, "rctrl":Z
    const-string v8, "lalt"

    invoke-interface {v0, v3, v8}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 473
    .local v8, "lctrl":Z
    const-string v9, "keycode"

    invoke-interface {v0, v3, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 475
    .local v9, "keyCode":I
    const-wide/16 v10, 0x0

    .line 476
    .local v10, "shortcutCode":J
    if-eqz v2, :cond_0

    .line 477
    const-wide/high16 v12, 0x1000000000000L

    or-long/2addr v12, v10

    goto :goto_0

    :cond_0
    move-wide v12, v10

    :goto_0
    move-wide v10, v12

    .line 478
    if-eqz v4, :cond_1

    .line 479
    const-wide v12, 0x100000000L

    or-long/2addr v12, v10

    goto :goto_1

    :cond_1
    move-wide v12, v10

    :goto_1
    move-wide v10, v12

    .line 480
    if-eqz v6, :cond_2

    .line 481
    const-wide v12, 0x200000000L

    or-long/2addr v12, v10

    goto :goto_2

    :cond_2
    move-wide v12, v10

    :goto_2
    move-wide v10, v12

    .line 482
    if-eqz v5, :cond_3

    .line 483
    const-wide v12, 0x100000000000L

    or-long/2addr v12, v10

    goto :goto_3

    :cond_3
    move-wide v12, v10

    :goto_3
    move-wide v10, v12

    .line 484
    if-eqz v7, :cond_4

    .line 485
    const-wide v12, 0x2000000000L

    or-long/2addr v12, v10

    goto :goto_4

    :cond_4
    move-wide v12, v10

    :goto_4
    move-wide v10, v12

    .line 486
    if-eqz v8, :cond_5

    .line 487
    const-wide v12, 0x1000000000L

    or-long/2addr v12, v10

    goto :goto_5

    :cond_5
    move-wide v12, v10

    :goto_5
    move-wide v10, v12

    .line 488
    int-to-long v12, v9

    or-long/2addr v10, v12

    .line 490
    const-string/jumbo v12, "type"

    invoke-interface {v0, v3, v12}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 491
    .local v12, "type":I
    const-string v13, "enable"

    invoke-interface {v0, v3, v13}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v13

    .line 492
    .local v13, "enable":Z
    const-string v14, "customized"

    invoke-interface {v0, v3, v14}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v21

    .line 493
    .local v21, "customized":Z
    new-instance v22, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    move-object/from16 v14, v22

    move-object/from16 v15, p0

    move-wide/from16 v16, v10

    move/from16 v18, v13

    move/from16 v19, v12

    move/from16 v20, v21

    invoke-direct/range {v14 .. v20}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;JZIZ)V

    .line 494
    .local v14, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    if-nez v12, :cond_6

    .line 495
    const-string v15, "packageName"

    invoke-interface {v0, v3, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 496
    .local v15, "packageName":Ljava/lang/String;
    move-object/from16 v16, v1

    .end local v1    # "metaStateOn":Ljava/lang/String;
    .local v16, "metaStateOn":Ljava/lang/String;
    const-string v1, "className"

    invoke-interface {v0, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 497
    .local v1, "className":Ljava/lang/String;
    invoke-virtual {v14, v15, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->setAppInfo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 494
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "metaStateOn":Ljava/lang/String;
    .local v1, "metaStateOn":Ljava/lang/String;
    :cond_6
    move-object/from16 v16, v1

    .line 500
    .end local v1    # "metaStateOn":Ljava/lang/String;
    .restart local v16    # "metaStateOn":Ljava/lang/String;
    :goto_6
    return-object v14
.end method


# virtual methods
.method public addShortcut(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Z
    .locals 3
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 426
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 427
    const/4 v0, 0x0

    return v0

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 431
    const/4 v0, 0x1

    return v0
.end method

.method public checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgName"    # Ljava/lang/String;

    .line 260
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 261
    return v1

    .line 264
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    nop

    .line 268
    const/4 v0, 0x1

    return v0

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "x":Ljava/lang/Exception;
    return v1
.end method

.method public delShortCutByShortcutCode(J)V
    .locals 1
    .param p1, "shortcutCode"    # J

    .line 435
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->delete(J)V

    .line 436
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 504
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 505
    const-string v0, "MiuiCustomizeShortCutUtils"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 506
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 507
    const-string v0, "mShortcutInfos ="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 508
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 509
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 508
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 511
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public enableAutoRemoveShortCutWhenAppRemove()V
    .locals 3

    .line 185
    const-string v0, "MiuiCustomizeShortCutUtils"

    const-string v1, "enableAutoRemoveShortCutWhenAppRemove"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 187
    .local v0, "packageFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 188
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 189
    invoke-static {}, Lcom/android/server/MiuiBgThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0, v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;Landroid/content/IntentFilter;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 203
    return-void
.end method

.method public getDefaultKeyboardShortcutInfos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultShortcutInfos:Ljava/util/List;

    return-object v0
.end method

.method public getKeyboardShortcutInfo()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/KeyboardShortcutInfo;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    .line 133
    invoke-static {v0, v1}, Lcom/miui/server/input/custom/InputMiuiDesktopMode;->getMuiDeskModeKeyboardShortcutInfo(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 134
    .local v0, "miuiDeskModeKeyboardShortcutInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/KeyboardShortcutInfo;>;"
    if-eqz v0, :cond_0

    .line 135
    return-object v0

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    return-object v1
.end method

.method public getMiuiKeyboardShortcutInfo()Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray<",
            "Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;",
            ">;"
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method public getShortcut(J)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .locals 1
    .param p1, "shortcutCode"    # J

    .line 453
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    return-object v0
.end method

.method public isShortcutExist(J)Z
    .locals 1
    .param p1, "shortcutCode"    # J

    .line 422
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public loadShortCuts()V
    .locals 9

    .line 272
    const-string v0, "loadShortCuts"

    const-string v1, "MiuiCustomizeShortCutUtils"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const/4 v0, 0x0

    .line 274
    .local v0, "str":Ljava/io/FileInputStream;
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mBackupFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mBackupFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v0, v2

    .line 277
    const-string v2, "Need to read from backup settings file"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cleaning up settings file "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    :cond_0
    goto :goto_0

    .line 285
    :catch_0
    move-exception v2

    .line 290
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->clear()V

    .line 291
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 294
    const/4 v2, 0x1

    if-nez v0, :cond_3

    .line 295
    :try_start_1
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 296
    const-string v3, "No shortcuts xml file found"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iput-boolean v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mNeedLoadInfoForDefaultRes:Z

    .line 298
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadFromDefaultRes(Z)V

    .line 299
    const-string v2, "load shortcuts  from res successfully"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 300
    return-void

    .line 302
    :cond_2
    :try_start_2
    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v0, v3

    .line 305
    :cond_3
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 306
    .local v3, "parser":Lorg/xmlpull/v1/XmlPullParser;
    sget-object v4, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v4}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 308
    const-string v4, "ShortcutInfos"

    invoke-static {v3, v4}, Lcom/android/internal/util/XmlUtils;->beginDocument(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)V

    .line 309
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v4, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    :goto_1
    invoke-static {v3}, Lcom/android/internal/util/XmlUtils;->nextElement(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 312
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v5

    if-ne v5, v2, :cond_4

    .line 313
    goto :goto_2

    .line 316
    :cond_4
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ShortcutInfo"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 317
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "read xml tagName error."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    nop

    .line 324
    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 325
    .local v5, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    invoke-virtual {v6, v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;

    move-result-object v6

    .line 326
    .local v6, "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
    iget-object v7, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    nop

    .end local v5    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .end local v6    # "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
    goto :goto_3

    .line 328
    :cond_5
    const-string v2, "load shortcuts from xml successfully"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    nop

    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    goto :goto_4

    .line 320
    .restart local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v4    # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    :cond_6
    invoke-direct {p0, v3}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->readFromParser(Lorg/xmlpull/v1/XmlPullParser;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    move-result-object v5

    .line 321
    .restart local v5    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8, v5}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 322
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 323
    nop

    .end local v5    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    goto :goto_1

    .line 333
    .end local v3    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v4    # "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    :catchall_0
    move-exception v1

    goto :goto_5

    .line 329
    :catch_1
    move-exception v2

    .line 330
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 331
    const-string v3, "Error reading miui shortcuts settings"

    invoke-static {v1, v3, v2}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 333
    nop

    .end local v2    # "e":Ljava/lang/Exception;
    :goto_4
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 334
    nop

    .line 335
    return-void

    .line 333
    :goto_5
    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 334
    throw v1
.end method

.method public resetShortCut(I)V
    .locals 9
    .param p1, "type"    # I

    .line 392
    const/4 v0, 0x2

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    if-eq p1, v0, :cond_0

    .line 393
    return-void

    .line 395
    :cond_0
    iget-object v2, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->clone()Landroid/util/LongSparseArray;

    move-result-object v2

    .line 396
    .local v2, "shortcutInfos":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 397
    invoke-virtual {v2, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    .line 398
    .local v4, "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    if-ne p1, v1, :cond_1

    invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    if-ne p1, v0, :cond_3

    .line 399
    invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v5

    if-nez v5, :cond_3

    .line 400
    :cond_2
    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Landroid/util/LongSparseArray;->delete(J)V

    .line 401
    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    invoke-virtual {v5, v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->reflectObject(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)Landroid/view/KeyboardShortcutInfo;

    move-result-object v5

    .line 402
    .local v5, "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 396
    .end local v4    # "info":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .end local v5    # "keyboardShortcutInfo":Landroid/view/KeyboardShortcutInfo;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 405
    .end local v3    # "i":I
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 406
    .local v1, "defaultShortcutInfos":Ljava/util/List;, "Ljava/util/List<Landroid/view/KeyboardShortcutInfo;>;"
    if-ne p1, v0, :cond_5

    .line 407
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultAppShortcutInfos:Ljava/util/List;

    goto :goto_1

    .line 409
    :cond_5
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mDefaultSystemShortcutInfos:Ljava/util/List;

    .line 411
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/KeyboardShortcutInfo;

    .line 412
    .local v4, "info":Landroid/view/KeyboardShortcutInfo;
    if-ne p1, v0, :cond_6

    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/view/KeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->checkAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 413
    goto :goto_2

    .line 415
    :cond_6
    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mKeyboardShortcutInfos:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    invoke-virtual {v5, v4}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->invokeObject(Landroid/view/KeyboardShortcutInfo;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    move-result-object v5

    .line 417
    .local v5, "shortcutInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    iget-object v6, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v5}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8, v5}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 418
    .end local v4    # "info":Landroid/view/KeyboardShortcutInfo;
    .end local v5    # "shortcutInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    goto :goto_2

    .line 419
    :cond_7
    return-void
.end method

.method public updateKeyboardShortcut(Landroid/view/KeyboardShortcutInfo;I)V
    .locals 4
    .param p1, "info"    # Landroid/view/KeyboardShortcutInfo;
    .param p2, "type"    # I

    .line 149
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mReflectionUtil:Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$ReflectionUtil;->invokeObject(Landroid/view/KeyboardShortcutInfo;)Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    move-result-object v0

    .line 150
    .local v0, "shortcutInfo":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V

    .line 151
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 174
    :pswitch_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->resetShortCut(I)V

    .line 175
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->resetRule()V

    .line 176
    goto :goto_0

    .line 169
    :pswitch_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->resetShortCut(I)V

    .line 170
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->resetRule()V

    .line 171
    goto :goto_0

    .line 164
    :pswitch_2
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/util/LongSparseArray;->delete(J)V

    .line 165
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->removeRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    .line 166
    goto :goto_0

    .line 159
    :pswitch_3
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getHistoryKeyCode()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 160
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->updateRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    .line 161
    goto :goto_0

    .line 154
    :pswitch_4
    iget-object v1, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 155
    invoke-static {}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->getInstance()Lcom/android/server/input/KeyboardCombinationManagerStubImpl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/input/KeyboardCombinationManagerStubImpl;->addRule(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;)V

    .line 156
    nop

    .line 180
    :goto_0
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->writeShortCuts()V

    .line 181
    invoke-virtual {p0}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->loadShortCuts()V

    .line 182
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public writeShortCuts()V
    .locals 10

    .line 338
    const-string v0, "ShortcutInfos"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    .line 340
    .local v1, "startTime":J
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    const-string v4, "MiuiCustomizeShortCutUtils"

    if-eqz v3, :cond_1

    .line 341
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mBackupFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 342
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mBackupFile:Ljava/io/File;

    invoke-virtual {v3, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 343
    const-string v0, "Unable to backup miui shortcut settings,  current changes will be lost at reboot"

    invoke-static {v4, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    return-void

    .line 349
    :cond_0
    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 350
    const-string v3, "Preserving older settings backup"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :cond_1
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    .local v3, "fstr":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/BufferedOutputStream;

    invoke-direct {v5, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 357
    .local v5, "out":Ljava/io/BufferedOutputStream;
    :try_start_2
    new-instance v6, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct {v6}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 358
    .local v6, "serializer":Lorg/xmlpull/v1/XmlSerializer;
    sget-object v7, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v7}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 359
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v6, v9, v8}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 360
    const-string v8, "http://xmlpull.org/v1/doc/features.html#indent-output"

    invoke-interface {v6, v8, v7}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 362
    invoke-interface {v6, v9, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 364
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v8, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v8}, Landroid/util/LongSparseArray;->size()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 365
    iget-object v8, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mShortcutInfos:Landroid/util/LongSparseArray;

    invoke-virtual {v8, v7}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;

    invoke-virtual {p0, v8, v6}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->writeToParser(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;Lorg/xmlpull/v1/XmlSerializer;)V

    .line 364
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 368
    .end local v7    # "i":I
    :cond_2
    invoke-interface {v6, v9, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 369
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 371
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->flush()V

    .line 372
    invoke-static {v3}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    .line 373
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V

    .line 375
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mBackupFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 376
    const-string v0, "miuishortcuts"

    .line 377
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v1

    .line 376
    invoke-static {v0, v7, v8}, Lcom/android/internal/logging/EventLogTags;->writeCommitSysConfigFile(Ljava/lang/String;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 379
    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 378
    return-void

    .line 354
    .end local v6    # "serializer":Lorg/xmlpull/v1/XmlSerializer;
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v6

    :try_start_6
    invoke-virtual {v0, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "startTime":J
    .end local v3    # "fstr":Ljava/io/FileOutputStream;
    .end local p0    # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    :goto_1
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v5    # "out":Ljava/io/BufferedOutputStream;
    .restart local v1    # "startTime":J
    .restart local v3    # "fstr":Ljava/io/FileOutputStream;
    .restart local p0    # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    :catchall_2
    move-exception v0

    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_2

    :catchall_3
    move-exception v5

    :try_start_8
    invoke-virtual {v0, v5}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "startTime":J
    .end local p0    # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    :goto_2
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 379
    .end local v3    # "fstr":Ljava/io/FileOutputStream;
    .restart local v1    # "startTime":J
    .restart local p0    # "this":Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;
    :catch_0
    move-exception v0

    .line 380
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Unable to write miui shortcut settings, current changes will be lost at reboot"

    invoke-static {v4, v3, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 385
    .end local v0    # "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_3

    .line 386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to clean up mangled file: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils;->mFile:Ljava/io/File;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_3
    return-void
.end method

.method public writeToParser(Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 12
    .param p1, "info"    # Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;
    .param p2, "out"    # Lorg/xmlpull/v1/XmlSerializer;

    .line 515
    const-string v0, "ShortcutInfo"

    const-string v1, ""

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p2, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 517
    const-string v3, "meta"

    .line 519
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v4

    const/16 v6, 0x20

    shr-long/2addr v4, v6

    const-wide/32 v7, 0x10000

    and-long/2addr v4, v7

    cmp-long v4, v4, v7

    const/4 v5, 0x1

    const/4 v7, 0x0

    if-nez v4, :cond_0

    move v4, v5

    goto :goto_0

    :cond_0
    move v4, v7

    .line 518
    :goto_0
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 517
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 521
    const-string/jumbo v3, "shift"

    .line 523
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v8

    shr-long/2addr v8, v6

    const-wide/16 v10, 0x1

    and-long/2addr v8, v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_1

    move v4, v5

    goto :goto_1

    :cond_1
    move v4, v7

    .line 522
    :goto_1
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 521
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 525
    const-string v3, "alt"

    .line 527
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v8

    shr-long/2addr v8, v6

    const-wide/16 v10, 0x2

    and-long/2addr v8, v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_2

    move v4, v5

    goto :goto_2

    :cond_2
    move v4, v7

    .line 526
    :goto_2
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 525
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 529
    const-string v3, "ctrl"

    .line 531
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v8

    shr-long/2addr v8, v6

    const-wide/16 v10, 0x1000

    and-long/2addr v8, v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_3

    move v4, v5

    goto :goto_3

    :cond_3
    move v4, v7

    .line 530
    :goto_3
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 529
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 533
    const-string v3, "ralt"

    .line 535
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v8

    shr-long/2addr v8, v6

    const-wide/16 v10, 0x20

    and-long/2addr v8, v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_4

    move v4, v5

    goto :goto_4

    :cond_4
    move v4, v7

    .line 534
    :goto_4
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 533
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 537
    const-string v3, "lalt"

    .line 539
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v8

    shr-long/2addr v8, v6

    const-wide/16 v10, 0x10

    and-long/2addr v8, v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_5

    goto :goto_5

    :cond_5
    move v5, v7

    .line 538
    :goto_5
    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    .line 537
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 541
    const-string v3, "keycode"

    .line 542
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getShortcutKeyCode()J

    move-result-wide v4

    const-wide/32 v6, 0xffff

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 541
    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 544
    const-string v3, "enable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isEnable()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 545
    const-string/jumbo v3, "type"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 546
    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getType()I

    move-result v3

    if-nez v3, :cond_6

    .line 547
    const-string v3, "packageName"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 548
    const-string v3, "className"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 550
    :cond_6
    const-string v3, "customized"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/input/util/MiuiCustomizeShortCutUtils$MiuiKeyboardShortcutInfo;->isCustomized()Z

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 551
    invoke-interface {p2, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    goto :goto_6

    .line 552
    :catch_0
    move-exception v0

    .line 553
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MiuiCustomizeShortCutUtils"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_6
    return-void
.end method
