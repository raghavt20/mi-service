.class Lcom/miui/server/input/PadManager$H;
.super Landroid/os/Handler;
.source "PadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/PadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "H"
.end annotation


# static fields
.field private static final BRIGHTNESS_STEP:F = 0.059f

.field private static final DATA_DELAY_TIME:Ljava/lang/String; = "delay_adjust"

.field private static final DATA_KEYEVENT:Ljava/lang/String; = "brightness_key"

.field private static final MSG_ADJUST_BRIGHTNESS:I = 0x63


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/PadManager;


# direct methods
.method constructor <init>(Lcom/miui/server/input/PadManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/PadManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 174
    iput-object p1, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    .line 175
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 176
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .line 180
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x63

    if-ne v0, v1, :cond_3

    .line 181
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "brightness_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 182
    .local v0, "event":Landroid/view/KeyEvent;
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 183
    .local v3, "keyCode":I
    const-string v4, "PadManager"

    const/16 v5, 0xdc

    if-eq v3, v5, :cond_0

    const/16 v6, 0xdd

    if-eq v3, v6, :cond_0

    .line 185
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception event for Bright:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return-void

    .line 188
    :cond_0
    iget-object v6, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v6}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;

    move-result-object v6

    .line 189
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 188
    const-string v7, "screen_brightness_mode"

    const/4 v8, 0x0

    const/4 v9, -0x3

    invoke-static {v6, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    .line 193
    .local v6, "auto":I
    if-eqz v6, :cond_1

    .line 194
    iget-object v10, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v10}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-static {v10, v7, v8, v9}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 199
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v7

    const-string v9, "delay_adjust"

    invoke-virtual {v7, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 200
    .local v7, "delayTime":I
    iget-object v10, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v10}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/PadManager;)Landroid/content/Context;

    move-result-object v10

    const-class v11, Landroid/hardware/display/DisplayManager;

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/hardware/display/DisplayManager;

    .line 201
    .local v10, "displayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v10, v8}, Landroid/hardware/display/DisplayManager;->getBrightness(I)F

    move-result v11

    .line 202
    .local v11, "nowBrightness":F
    invoke-static {v11}, Lcom/miui/server/input/PadManager$BrightnessUtils;->convertLinearToGamma(F)F

    move-result v12

    .line 203
    .local v12, "gammaValue":F
    const v13, 0x3d71a9fc    # 0.059f

    if-ne v3, v5, :cond_2

    .line 204
    sub-float v5, v12, v13

    goto :goto_0

    :cond_2
    add-float v5, v12, v13

    .line 205
    .end local v12    # "gammaValue":F
    .local v5, "gammaValue":F
    :goto_0
    invoke-static {v5}, Lcom/miui/server/input/PadManager$BrightnessUtils;->convertGammaToLinear(F)F

    move-result v12

    .line 206
    .local v12, "linearValue":F
    invoke-virtual {v10, v8, v12}, Landroid/hardware/display/DisplayManager;->setBrightness(IF)V

    .line 207
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v13, "set Brightness :"

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v4, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v4}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmRunning(Lcom/miui/server/input/PadManager;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 210
    iget-object v4, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v4}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmHandler(Lcom/miui/server/input/PadManager;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 211
    .local v1, "delayMsg":Landroid/os/Message;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 212
    .local v4, "bundle":Landroid/os/Bundle;
    invoke-virtual {v4, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 213
    const/16 v2, 0x32

    invoke-virtual {v4, v9, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    invoke-virtual {v1, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 215
    iget-object v2, p0, Lcom/miui/server/input/PadManager$H;->this$0:Lcom/miui/server/input/PadManager;

    invoke-static {v2}, Lcom/miui/server/input/PadManager;->-$$Nest$fgetmHandler(Lcom/miui/server/input/PadManager;)Landroid/os/Handler;

    move-result-object v2

    int-to-long v8, v7

    invoke-virtual {v2, v1, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 218
    .end local v0    # "event":Landroid/view/KeyEvent;
    .end local v1    # "delayMsg":Landroid/os/Message;
    .end local v3    # "keyCode":I
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "gammaValue":F
    .end local v6    # "auto":I
    .end local v7    # "delayTime":I
    .end local v10    # "displayManager":Landroid/hardware/display/DisplayManager;
    .end local v11    # "nowBrightness":F
    .end local v12    # "linearValue":F
    :cond_3
    return-void
.end method
