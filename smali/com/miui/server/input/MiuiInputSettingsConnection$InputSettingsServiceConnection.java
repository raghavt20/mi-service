class com.miui.server.input.MiuiInputSettingsConnection$InputSettingsServiceConnection implements android.content.ServiceConnection {
	 /* .source "MiuiInputSettingsConnection.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiInputSettingsConnection; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "InputSettingsServiceConnection" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiInputSettingsConnection this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$gSz5vPtjRpKOhDWMWSJ6gRMSVkQ ( com.miui.server.input.MiuiInputSettingsConnection$InputSettingsServiceConnection p0, android.os.Message p1, com.miui.server.input.MiuiInputSettingsConnection$OnMessageSendFinishCallback p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->lambda$onServiceConnected$0(Landroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V */
return;
} // .end method
private com.miui.server.input.MiuiInputSettingsConnection$InputSettingsServiceConnection ( ) {
/* .locals 0 */
/* .line 281 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.input.MiuiInputSettingsConnection$InputSettingsServiceConnection ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V */
return;
} // .end method
private void lambda$onServiceConnected$0 ( android.os.Message p0, com.miui.server.input.MiuiInputSettingsConnection$OnMessageSendFinishCallback p1 ) { //synthethic
/* .locals 4 */
/* .param p1, "message" # Landroid/os/Message; */
/* .param p2, "callback" # Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback; */
/* .line 291 */
final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
try { // :try_start_0
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v2, "send cached message " */
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v2, p1, Landroid/os/Message;->what:I */
	 /* .line 292 */
	 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$smmessageWhatServerToString ( v2 );
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v2 = " to settings"; // const-string v2, " to settings"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 291 */
	 android.util.Slog .i ( v0,v1 );
	 /* .line 293 */
	 v1 = this.this$0;
	 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$fgetmMessenger ( v1 );
	 (( android.os.Messenger ) v1 ).send ( p1 ); // invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
	 /* .line 294 */
	 v1 = this.this$0;
	 int v2 = 0; // const/4 v2, 0x0
	 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$mexecuteCallback ( v1,p2,v2 );
	 /* :try_end_0 */
	 /* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 299 */
	 /* .line 295 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 296 */
	 /* .local v1, "e":Landroid/os/RemoteException; */
	 v2 = this.this$0;
	 int v3 = 1; // const/4 v3, 0x1
	 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$mexecuteCallback ( v2,p2,v3 );
	 /* .line 298 */
	 (( android.os.RemoteException ) v1 ).getMessage ( ); // invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
	 android.util.Slog .e ( v0,v2,v1 );
	 /* .line 300 */
} // .end local v1 # "e":Landroid/os/RemoteException;
} // :goto_0
return;
} // .end method
/* # virtual methods */
public void onServiceConnected ( android.content.ComponentName p0, android.os.IBinder p1 ) {
/* .locals 3 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .param p2, "service" # Landroid/os/IBinder; */
/* .line 285 */
final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
/* const-string/jumbo v1, "service connected" */
android.util.Slog .w ( v0,v1 );
/* .line 286 */
v0 = this.this$0;
/* monitor-enter v0 */
/* .line 287 */
try { // :try_start_0
v1 = this.this$0;
/* new-instance v2, Landroid/os/Messenger; */
/* invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V */
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$fputmMessenger ( v1,v2 );
/* .line 289 */
v1 = this.this$0;
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$fgetmCachedMessageList ( v1 );
/* new-instance v2, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;)V */
/* .line 301 */
v1 = this.this$0;
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$mresetTimer ( v1 );
/* .line 302 */
v1 = this.this$0;
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$fgetmCachedMessageList ( v1 );
/* .line 303 */
/* monitor-exit v0 */
/* .line 304 */
return;
/* .line 303 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void onServiceDisconnected ( android.content.ComponentName p0 ) {
/* .locals 2 */
/* .param p1, "name" # Landroid/content/ComponentName; */
/* .line 308 */
final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
/* const-string/jumbo v1, "service disconnected" */
android.util.Slog .w ( v0,v1 );
/* .line 309 */
v0 = this.this$0;
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$mresetConnection ( v0 );
/* .line 310 */
return;
} // .end method
