class com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker implements android.view.WindowManagerPolicyConstants$PointerEventListener {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "TouchPositionTracker" */
} // .end annotation
/* # instance fields */
private final Integer TOUCH_NEGATIVE;
private final Integer TOUCH_POSITIVE;
private final Integer TOUCH_UNKNOWN;
private Long mLastObservedTouchTime;
private Object mPointerIndexTriggerBitMask;
public volatile Float mTouchBottom;
public volatile Float mTouchLeft;
public volatile Float mTouchRight;
Integer mTouchStatus;
public volatile Float mTouchTop;
final com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper this$1; //synthetic
/* # direct methods */
static Long -$$Nest$fgetmLastObservedTouchTime ( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J */
/* return-wide v0 */
} // .end method
public com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ( ) {
/* .locals 3 */
/* .line 418 */
this.this$1 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 407 */
int p1 = -1; // const/4 p1, -0x1
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->TOUCH_UNKNOWN:I */
/* .line 408 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->TOUCH_NEGATIVE:I */
/* .line 409 */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->TOUCH_POSITIVE:I */
/* .line 410 */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
/* .line 411 */
int p1 = 0; // const/4 p1, 0x0
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F */
/* .line 412 */
/* const/high16 v1, 0x43fa0000 # 500.0f */
/* iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F */
/* .line 413 */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F */
/* .line 414 */
/* iput v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F */
/* .line 415 */
/* const-wide/16 v1, 0x0 */
/* iput-wide v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J */
/* .line 416 */
/* iput-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* .line 419 */
final String p1 = "BackTapTouchHelper"; // const-string p1, "BackTapTouchHelper"
final String v0 = "TouchPositionTracker new obj!"; // const-string v0, "TouchPositionTracker new obj!"
android.util.Slog .d ( p1,v0 );
/* .line 420 */
return;
} // .end method
/* # virtual methods */
Integer getTouchStatus ( ) {
/* .locals 2 */
/* .line 474 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 475 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v1, "touch status=" */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "BackTapTouchHelper"; // const-string v1, "BackTapTouchHelper"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 477 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 11 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 482 */
v0 = (( android.view.MotionEvent ) p1 ).isTouchEvent ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->isTouchEvent()Z
if ( v0 != null) { // if-eqz v0, :cond_7
	 /* .line 483 */
	 v0 = 	 (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
	 /* .line 484 */
	 /* .local v0, "action":I */
	 v1 = 	 (( android.view.MotionEvent ) p1 ).getPointerCount ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I
	 /* .line 485 */
	 /* .local v1, "mPointerCount":I */
	 /* new-instance v2, Landroid/view/MotionEvent$PointerCoords; */
	 /* invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V */
	 /* .line 487 */
	 /* .local v2, "mPointerCoords":Landroid/view/MotionEvent$PointerCoords; */
	 v3 = this.this$1;
	 v3 = 	 com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v3 );
	 final String v4 = "BackTapTouchHelper"; // const-string v4, "BackTapTouchHelper"
	 if ( v3 != null) { // if-eqz v3, :cond_0
		 /* .line 488 */
		 int v3 = 0; // const/4 v3, 0x0
		 /* .local v3, "i":I */
	 } // :goto_0
	 /* if-ge v3, v1, :cond_0 */
	 /* .line 489 */
	 v5 = 	 (( android.view.MotionEvent ) p1 ).getPointerId ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I
	 /* .line 490 */
	 /* .local v5, "mPointerId":I */
	 /* new-instance v6, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
	 /* const-string/jumbo v7, "touch onPointerEvent: index: " */
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v7 = ", id: "; // const-string v7, ", id: "
	 (( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .d ( v4,v6 );
	 /* .line 488 */
} // .end local v5 # "mPointerId":I
/* add-int/lit8 v3, v3, 0x1 */
/* .line 494 */
} // .end local v3 # "i":I
} // :cond_0
/* and-int/lit16 v3, v0, 0xff */
int v5 = 0; // const/4 v5, 0x0
/* const/16 v6, 0x8 */
/* const-string/jumbo v7, "touch onPointerEvent action: " */
int v8 = 1; // const/4 v8, 0x1
/* packed-switch v3, :pswitch_data_0 */
/* .line 532 */
/* :pswitch_0 */
v3 = this.this$1;
v3 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_7
/* .line 533 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* goto/16 :goto_2 */
/* .line 510 */
/* :pswitch_1 */
/* if-eq v0, v8, :cond_1 */
int v3 = 3; // const/4 v3, 0x3
/* if-ne v0, v3, :cond_4 */
/* .line 512 */
} // :cond_1
v3 = this.this$1;
v3 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v3 );
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 513 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "touch onPointerEvent mTouchStatus = " */
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v9, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
(( java.lang.StringBuilder ) v3 ).append ( v9 ); // invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 514 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v7 ); // invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v3 );
/* .line 516 */
} // :cond_2
/* iget v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
/* if-ne v3, v8, :cond_3 */
/* .line 517 */
android.os.SystemClock .elapsedRealtimeNanos ( );
/* move-result-wide v9 */
/* iput-wide v9, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J */
/* .line 519 */
} // :cond_3
/* iput v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
/* .line 521 */
} // :cond_4
/* const v3, 0xff00 */
/* and-int/2addr v3, v0 */
/* shr-int/2addr v3, v6 */
/* .line 523 */
/* .local v3, "index":I */
v5 = (( android.view.MotionEvent ) p1 ).getPointerId ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I
/* .line 524 */
/* .local v5, "pointerId":I */
/* iget-short v6, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* shl-int v7, v8, v5 */
/* not-int v7, v7 */
/* and-int/2addr v6, v7 */
/* int-to-short v6, v6 */
/* iput-short v6, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* .line 525 */
v6 = this.this$1;
v6 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v6 );
if ( v6 != null) { // if-eqz v6, :cond_7
/* .line 526 */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v7, "touch onPointerEvent ACTION_POINTER_UP: index:" */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3 ); // invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ", pointerId:"; // const-string v7, ", pointerId:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v4,v6 );
/* .line 497 */
} // .end local v3 # "index":I
} // .end local v5 # "pointerId":I
/* :pswitch_2 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_1
/* if-ge v3, v1, :cond_6 */
/* .line 498 */
(( android.view.MotionEvent ) p1 ).getPointerCoords ( v3, v2 ); // invoke-virtual {p1, v3, v2}, Landroid/view/MotionEvent;->getPointerCoords(ILandroid/view/MotionEvent$PointerCoords;)V
/* .line 499 */
v7 = this.this$1;
v7 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v7 );
if ( v7 != null) { // if-eqz v7, :cond_5
/* .line 500 */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v9, "touch getPointerCoords: ORIENTATION = " */
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 501 */
v9 = (( android.view.MotionEvent$PointerCoords ) v2 ).getAxisValue ( v6 ); // invoke-virtual {v2, v6}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", x = "; // const-string v9, ", x = "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 502 */
v9 = (( android.view.MotionEvent$PointerCoords ) v2 ).getAxisValue ( v5 ); // invoke-virtual {v2, v5}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v9 = ", y = "; // const-string v9, ", y = "
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 503 */
v9 = (( android.view.MotionEvent$PointerCoords ) v2 ).getAxisValue ( v8 ); // invoke-virtual {v2, v8}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
(( java.lang.StringBuilder ) v7 ).append ( v9 ); // invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 500 */
android.util.Slog .d ( v4,v7 );
/* .line 504 */
} // :cond_5
v7 = (( android.view.MotionEvent ) p1 ).getPointerId ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I
(( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) p0 ).verifyMotionEvent ( v2, v7 ); // invoke-virtual {p0, v2, v7}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->verifyMotionEvent(Landroid/view/MotionEvent$PointerCoords;I)V
/* .line 497 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 506 */
} // .end local v3 # "i":I
} // :cond_6
/* nop */
/* .line 537 */
} // .end local v0 # "action":I
} // .end local v1 # "mPointerCount":I
} // .end local v2 # "mPointerCoords":Landroid/view/MotionEvent$PointerCoords;
} // :cond_7
} // :goto_2
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
void updateTouchBorder ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 2 */
/* .param p1, "left" # F */
/* .param p2, "right" # F */
/* .param p3, "top" # F */
/* .param p4, "bottom" # F */
/* .line 437 */
/* iput p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F */
/* .line 438 */
/* iput p2, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F */
/* .line 439 */
/* iput p3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F */
/* .line 440 */
/* iput p4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F */
/* .line 441 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 442 */
final String v0 = "BackTapTouchHelper"; // const-string v0, "BackTapTouchHelper"
/* const-string/jumbo v1, "updateTouchBorder!!" */
android.util.Slog .d ( v0,v1 );
/* .line 444 */
} // :cond_0
return;
} // .end method
void updateTouchBorder ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "rotation" # I */
/* .line 423 */
/* packed-switch p1, :pswitch_data_0 */
/* .line 431 */
/* :pswitch_0 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_LEFT ( v0 );
v1 = this.this$1;
v1 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_RIGHT ( v1 );
v2 = this.this$1;
v2 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_TOP ( v2 );
v3 = this.this$1;
v3 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_BOTTOM ( v3 );
(( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) p0 ).updateTouchBorder ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V
/* .line 428 */
/* :pswitch_1 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetSCREEN_HEIGHT ( v0 );
/* int-to-float v0, v0 */
v1 = this.this$1;
v1 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_BOTTOM ( v1 );
/* sub-float/2addr v0, v1 */
v1 = this.this$1;
v1 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetSCREEN_HEIGHT ( v1 );
/* int-to-float v1, v1 */
v2 = this.this$1;
v2 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_TOP ( v2 );
/* sub-float/2addr v1, v2 */
v2 = this.this$1;
v2 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_LEFT ( v2 );
v3 = this.this$1;
v3 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_RIGHT ( v3 );
(( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) p0 ).updateTouchBorder ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V
/* .line 429 */
/* .line 425 */
/* :pswitch_2 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_TOP ( v0 );
v1 = this.this$1;
v1 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_BOTTOM ( v1 );
v2 = this.this$1;
v2 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetSCREEN_WIDTH ( v2 );
/* int-to-float v2, v2 */
v3 = this.this$1;
v3 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_RIGHT ( v3 );
/* sub-float/2addr v2, v3 */
v3 = this.this$1;
v3 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetSCREEN_WIDTH ( v3 );
/* int-to-float v3, v3 */
v4 = this.this$1;
v4 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetTOUCH_LEFT ( v4 );
/* sub-float/2addr v3, v4 */
(( com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker ) p0 ).updateTouchBorder ( v0, v1, v2, v3 ); // invoke-virtual {p0, v0, v1, v2, v3}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->updateTouchBorder(FFFF)V
/* .line 426 */
/* nop */
/* .line 434 */
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
void verifyMotionEvent ( android.view.MotionEvent$PointerCoords p0, Integer p1 ) {
/* .locals 7 */
/* .param p1, "mPointerCoords" # Landroid/view/MotionEvent$PointerCoords; */
/* .param p2, "pointerId" # I */
/* .line 447 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v0 );
/* const-string/jumbo v1, "}" */
final String v2 = "BackTapTouchHelper"; // const-string v2, "BackTapTouchHelper"
int v3 = 0; // const/4 v3, 0x0
int v4 = 1; // const/4 v4, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 448 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "verifyMotionEvent [ID: " */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p2 ); // invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = ", Ori: "; // const-string v5, ", Ori: "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 449 */
/* const/16 v5, 0x8 */
v5 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v5 ); // invoke-virtual {p1, v5}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = "], { "; // const-string v5, "], { "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 450 */
v5 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
final String v5 = ", "; // const-string v5, ", "
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 451 */
v5 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v4 ); // invoke-virtual {p1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 448 */
android.util.Slog .d ( v2,v0 );
/* .line 454 */
} // :cond_0
v0 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
/* iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchLeft:F */
/* cmpl-float v0, v0, v5 */
/* if-lez v0, :cond_1 */
/* .line 455 */
v0 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v3 ); // invoke-virtual {p1, v3}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
/* iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchRight:F */
/* cmpg-float v0, v0, v5 */
/* if-gez v0, :cond_1 */
/* .line 456 */
v0 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v4 ); // invoke-virtual {p1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
/* iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchTop:F */
/* cmpl-float v0, v0, v5 */
/* if-lez v0, :cond_1 */
/* .line 457 */
v0 = (( android.view.MotionEvent$PointerCoords ) p1 ).getAxisValue ( v4 ); // invoke-virtual {p1, v4}, Landroid/view/MotionEvent$PointerCoords;->getAxisValue(I)F
/* iget v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchBottom:F */
/* cmpg-float v0, v0, v5 */
/* if-gez v0, :cond_1 */
/* .line 458 */
/* iget-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* shl-int v5, v4, p2 */
/* or-int/2addr v0, v5 */
/* int-to-short v0, v0 */
/* iput-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* .line 459 */
v0 = this.this$1;
v0 = com.miui.server.input.MiuiBackTapGestureService$BackTapTouchHelper .-$$Nest$fgetDEBUG ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 460 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "touch: { time: " */
(( java.lang.StringBuilder ) v0 ).append ( v5 ); // invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v5, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mLastObservedTouchTime:J */
(( java.lang.StringBuilder ) v0 ).append ( v5, v6 ); // invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 463 */
} // :cond_1
/* iget-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* shl-int v1, v4, p2 */
/* not-int v1, v1 */
/* and-int/2addr v0, v1 */
/* int-to-short v0, v0 */
/* iput-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
/* .line 466 */
} // :cond_2
} // :goto_0
/* iget-short v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mPointerIndexTriggerBitMask:S */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 467 */
/* iput v4, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
/* .line 469 */
} // :cond_3
/* iput v3, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$TouchPositionTracker;->mTouchStatus:I */
/* .line 471 */
} // :goto_1
return;
} // .end method
