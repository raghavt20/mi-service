.class public Lcom/miui/server/input/MiuiCursorPositionListenerService;
.super Ljava/lang/Object;
.source "MiuiCursorPositionListenerService.java"

# interfaces
.implements Landroid/view/WindowManagerPolicyConstants$PointerEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static volatile sInstance:Lcom/miui/server/input/MiuiCursorPositionListenerService;


# instance fields
.field private final mOnCursorPositionChangedListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempOnCursorPositionChangedListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mWindowManagerService:Lcom/android/server/wm/WindowManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    const-class v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    nop

    .line 25
    const-string/jumbo v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/WindowManagerService;

    iput-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mTempOnCursorPositionChangedListeners:Ljava/util/List;

    .line 28
    return-void
.end method

.method private deliverCursorPositionChangedEvent(IFF)V
    .locals 3
    .param p1, "deviceId"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F

    .line 88
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mTempOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 89
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    monitor-enter v0

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mTempOnCursorPositionChangedListeners:Ljava/util/List;

    iget-object v2, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 91
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mTempOnCursorPositionChangedListeners:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/MiuiCursorPositionListenerService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1, p2, p3}, Lcom/miui/server/input/MiuiCursorPositionListenerService$$ExternalSyntheticLambda0;-><init>(IFF)V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 94
    return-void

    .line 91
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static getInstance()Lcom/miui/server/input/MiuiCursorPositionListenerService;
    .locals 2

    .line 31
    sget-object v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->sInstance:Lcom/miui/server/input/MiuiCursorPositionListenerService;

    if-nez v0, :cond_1

    .line 32
    const-class v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;

    monitor-enter v0

    .line 33
    :try_start_0
    sget-object v1, Lcom/miui/server/input/MiuiCursorPositionListenerService;->sInstance:Lcom/miui/server/input/MiuiCursorPositionListenerService;

    if-nez v1, :cond_0

    .line 34
    new-instance v1, Lcom/miui/server/input/MiuiCursorPositionListenerService;

    invoke-direct {v1}, Lcom/miui/server/input/MiuiCursorPositionListenerService;-><init>()V

    sput-object v1, Lcom/miui/server/input/MiuiCursorPositionListenerService;->sInstance:Lcom/miui/server/input/MiuiCursorPositionListenerService;

    .line 36
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 38
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->sInstance:Lcom/miui/server/input/MiuiCursorPositionListenerService;

    return-object v0
.end method

.method static synthetic lambda$deliverCursorPositionChangedEvent$0(IFFLcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V
    .locals 0
    .param p0, "deviceId"    # I
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "listener"    # Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 93
    invoke-interface {p3, p0, p1, p2}, Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;->onCursorPositionChanged(IFF)V

    return-void
.end method

.method private populatePointerListenerLocked()V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 73
    return-void

    .line 75
    :cond_0
    sget-object v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->TAG:Ljava/lang/String;

    const-string v1, "register pointer event listener to wms"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/android/server/wm/WindowManagerService;->registerPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 77
    return-void
.end method

.method private unPopulatePointerListenerLocked()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    return-void

    .line 83
    :cond_0
    sget-object v0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unregister pointer event listener to wms"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mWindowManagerService:Lcom/android/server/wm/WindowManagerService;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/android/server/wm/WindowManagerService;->unregisterPointerEventListener(Landroid/view/WindowManagerPolicyConstants$PointerEventListener;I)V

    .line 85
    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 98
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 100
    return-void

    .line 102
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 103
    .local v0, "action":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v1

    .line 105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 104
    invoke-direct {p0, v1, v2, v3}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->deliverCursorPositionChangedEvent(IFF)V

    .line 107
    :cond_2
    return-void
.end method

.method public registerOnCursorPositionChangedListener(Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 42
    if-eqz p1, :cond_1

    .line 45
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    monitor-enter v0

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    sget-object v1, Lcom/miui/server/input/MiuiCursorPositionListenerService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a cursor position listener added, current size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-direct {p0}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->populatePointerListenerLocked()V

    .line 53
    monitor-exit v0

    .line 54
    return-void

    .line 47
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "The listener already registered"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService;
    .end local p1    # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
    throw v1

    .line 53
    .restart local p0    # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService;
    .restart local p1    # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 43
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The listener can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public unregisterOnCursorPositionChangedListener(Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;

    .line 57
    if-eqz p1, :cond_1

    .line 60
    iget-object v0, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    monitor-enter v0

    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 65
    sget-object v1, Lcom/miui/server/input/MiuiCursorPositionListenerService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a cursor position listener removed, current size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/server/input/MiuiCursorPositionListenerService;->mOnCursorPositionChangedListeners:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-direct {p0}, Lcom/miui/server/input/MiuiCursorPositionListenerService;->unPopulatePointerListenerLocked()V

    .line 68
    monitor-exit v0

    .line 69
    return-void

    .line 62
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "The listener not registered"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .end local p0    # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService;
    .end local p1    # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
    throw v1

    .line 68
    .restart local p0    # "this":Lcom/miui/server/input/MiuiCursorPositionListenerService;
    .restart local p1    # "listener":Lcom/miui/server/input/MiuiCursorPositionListenerService$OnCursorPositionChangedListener;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 58
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The listener can not be null!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
