.class Lcom/miui/server/input/AutoDisableScreenButtonsManager$5;
.super Ljava/lang/Object;
.source "AutoDisableScreenButtonsManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showPromptsIfNeeds()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;


# direct methods
.method constructor <init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    .line 279
    iput-object p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$5;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .line 284
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$5;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "ADSB_NOT_SHOW_PROMPTS"

    invoke-static {v0, v2, v1}, Lcom/miui/server/input/util/AutoDisableScreenButtonsHelper;->setValue(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;)V

    .line 286
    return-void
.end method
