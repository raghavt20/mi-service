class com.miui.server.input.PadManager$H extends android.os.Handler {
	 /* .source "PadManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/PadManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final Float BRIGHTNESS_STEP;
private static final java.lang.String DATA_DELAY_TIME;
private static final java.lang.String DATA_KEYEVENT;
private static final Integer MSG_ADJUST_BRIGHTNESS;
/* # instance fields */
final com.miui.server.input.PadManager this$0; //synthetic
/* # direct methods */
 com.miui.server.input.PadManager$H ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/PadManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 174 */
this.this$0 = p1;
/* .line 175 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 176 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 14 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 180 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* const/16 v1, 0x63 */
/* if-ne v0, v1, :cond_3 */
/* .line 181 */
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v2 = "brightness_key"; // const-string v2, "brightness_key"
(( android.os.Bundle ) v0 ).getParcelable ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
/* check-cast v0, Landroid/view/KeyEvent; */
/* .line 182 */
/* .local v0, "event":Landroid/view/KeyEvent; */
v3 = (( android.view.KeyEvent ) v0 ).getKeyCode ( ); // invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 183 */
/* .local v3, "keyCode":I */
final String v4 = "PadManager"; // const-string v4, "PadManager"
/* const/16 v5, 0xdc */
/* if-eq v3, v5, :cond_0 */
/* const/16 v6, 0xdd */
/* if-eq v3, v6, :cond_0 */
/* .line 185 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Exception event for Bright:"; // const-string v2, "Exception event for Bright:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v1 );
/* .line 186 */
return;
/* .line 188 */
} // :cond_0
v6 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmContext ( v6 );
/* .line 189 */
(( android.content.Context ) v6 ).getContentResolver ( ); // invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 188 */
final String v7 = "screen_brightness_mode"; // const-string v7, "screen_brightness_mode"
int v8 = 0; // const/4 v8, 0x0
int v9 = -3; // const/4 v9, -0x3
v6 = android.provider.Settings$System .getIntForUser ( v6,v7,v8,v9 );
/* .line 193 */
/* .local v6, "auto":I */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 194 */
v10 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmContext ( v10 );
(( android.content.Context ) v10 ).getContentResolver ( ); // invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putIntForUser ( v10,v7,v8,v9 );
/* .line 199 */
} // :cond_1
(( android.os.Message ) p1 ).getData ( ); // invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;
final String v9 = "delay_adjust"; // const-string v9, "delay_adjust"
v7 = (( android.os.Bundle ) v7 ).getInt ( v9 ); // invoke-virtual {v7, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I
/* .line 200 */
/* .local v7, "delayTime":I */
v10 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmContext ( v10 );
/* const-class v11, Landroid/hardware/display/DisplayManager; */
(( android.content.Context ) v10 ).getSystemService ( v11 ); // invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v10, Landroid/hardware/display/DisplayManager; */
/* .line 201 */
/* .local v10, "displayManager":Landroid/hardware/display/DisplayManager; */
v11 = (( android.hardware.display.DisplayManager ) v10 ).getBrightness ( v8 ); // invoke-virtual {v10, v8}, Landroid/hardware/display/DisplayManager;->getBrightness(I)F
/* .line 202 */
/* .local v11, "nowBrightness":F */
v12 = com.miui.server.input.PadManager$BrightnessUtils .convertLinearToGamma ( v11 );
/* .line 203 */
/* .local v12, "gammaValue":F */
/* const v13, 0x3d71a9fc # 0.059f */
/* if-ne v3, v5, :cond_2 */
/* .line 204 */
/* sub-float v5, v12, v13 */
} // :cond_2
/* add-float v5, v12, v13 */
/* .line 205 */
} // .end local v12 # "gammaValue":F
/* .local v5, "gammaValue":F */
} // :goto_0
v12 = com.miui.server.input.PadManager$BrightnessUtils .convertGammaToLinear ( v5 );
/* .line 206 */
/* .local v12, "linearValue":F */
(( android.hardware.display.DisplayManager ) v10 ).setBrightness ( v8, v12 ); // invoke-virtual {v10, v8, v12}, Landroid/hardware/display/DisplayManager;->setBrightness(IF)V
/* .line 207 */
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v13, "set Brightness :" */
(( java.lang.StringBuilder ) v8 ).append ( v13 ); // invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v12 ); // invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).toString ( ); // invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v4,v8 );
/* .line 209 */
v4 = this.this$0;
v4 = com.miui.server.input.PadManager .-$$Nest$fgetmRunning ( v4 );
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 210 */
v4 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmHandler ( v4 );
(( android.os.Handler ) v4 ).obtainMessage ( v1 ); // invoke-virtual {v4, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 211 */
/* .local v1, "delayMsg":Landroid/os/Message; */
/* new-instance v4, Landroid/os/Bundle; */
/* invoke-direct {v4}, Landroid/os/Bundle;-><init>()V */
/* .line 212 */
/* .local v4, "bundle":Landroid/os/Bundle; */
(( android.os.Bundle ) v4 ).putParcelable ( v2, v0 ); // invoke-virtual {v4, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
/* .line 213 */
/* const/16 v2, 0x32 */
(( android.os.Bundle ) v4 ).putInt ( v9, v2 ); // invoke-virtual {v4, v9, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
/* .line 214 */
(( android.os.Message ) v1 ).setData ( v4 ); // invoke-virtual {v1, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
/* .line 215 */
v2 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmHandler ( v2 );
/* int-to-long v8, v7 */
(( android.os.Handler ) v2 ).sendMessageDelayed ( v1, v8, v9 ); // invoke-virtual {v2, v1, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 218 */
} // .end local v0 # "event":Landroid/view/KeyEvent;
} // .end local v1 # "delayMsg":Landroid/os/Message;
} // .end local v3 # "keyCode":I
} // .end local v4 # "bundle":Landroid/os/Bundle;
} // .end local v5 # "gammaValue":F
} // .end local v6 # "auto":I
} // .end local v7 # "delayTime":I
} // .end local v10 # "displayManager":Landroid/hardware/display/DisplayManager;
} // .end local v11 # "nowBrightness":F
} // .end local v12 # "linearValue":F
} // :cond_3
return;
} // .end method
