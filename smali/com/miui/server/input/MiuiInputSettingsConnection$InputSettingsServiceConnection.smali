.class Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;
.super Ljava/lang/Object;
.source "MiuiInputSettingsConnection.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiInputSettingsConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InputSettingsServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;


# direct methods
.method public static synthetic $r8$lambda$gSz5vPtjRpKOhDWMWSJ6gRMSVkQ(Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;Landroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->lambda$onServiceConnected$0(Landroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V

    return-void
.end method

.method private constructor <init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/input/MiuiInputSettingsConnection;Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    return-void
.end method

.method private synthetic lambda$onServiceConnected$0(Landroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V
    .locals 4
    .param p1, "message"    # Landroid/os/Message;
    .param p2, "callback"    # Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;

    .line 291
    const-string v0, "MiuiInputSettingsConnection"

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "send cached message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    .line 292
    invoke-static {v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$smmessageWhatServerToString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to settings"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 291
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$fgetmMessenger(Lcom/miui/server/input/MiuiInputSettingsConnection;)Landroid/os/Messenger;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V

    .line 294
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    const/4 v2, 0x0

    invoke-static {v1, p2, v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$mexecuteCallback(Lcom/miui/server/input/MiuiInputSettingsConnection;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    goto :goto_0

    .line 295
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    const/4 v3, 0x1

    invoke-static {v2, p2, v3}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$mexecuteCallback(Lcom/miui/server/input/MiuiInputSettingsConnection;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V

    .line 298
    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 300
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .line 285
    const-string v0, "MiuiInputSettingsConnection"

    const-string/jumbo v1, "service connected"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    monitor-enter v0

    .line 287
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    new-instance v2, Landroid/os/Messenger;

    invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    invoke-static {v1, v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$fputmMessenger(Lcom/miui/server/input/MiuiInputSettingsConnection;Landroid/os/Messenger;)V

    .line 289
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$fgetmCachedMessageList(Lcom/miui/server/input/MiuiInputSettingsConnection;)Ljava/util/Map;

    move-result-object v1

    new-instance v2, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection$$ExternalSyntheticLambda0;

    invoke-direct {v2, p0}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;)V

    invoke-interface {v1, v2}, Ljava/util/Map;->forEach(Ljava/util/function/BiConsumer;)V

    .line 301
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$mresetTimer(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    .line 302
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$fgetmCachedMessageList(Lcom/miui/server/input/MiuiInputSettingsConnection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 303
    monitor-exit v0

    .line 304
    return-void

    .line 303
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .line 308
    const-string v0, "MiuiInputSettingsConnection"

    const-string/jumbo v1, "service disconnected"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$mresetConnection(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    .line 310
    return-void
.end method
