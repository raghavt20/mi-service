.class Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;
.super Landroid/view/IRotationWatcher$Stub;
.source "MiuiBackTapGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RotationWatcher"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;


# direct methods
.method private constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)V
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-direct {p0}, Landroid/view/IRotationWatcher$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;-><init>(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)V

    return-void
.end method


# virtual methods
.method public onRotationChanged(I)V
    .locals 2
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 398
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rotation changed = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BackTapTouchHelper"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetmRotation(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 400
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0, p1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fputmRotation(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;I)V

    .line 401
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper$RotationWatcher;->this$1:Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$fgetmRotation(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;->-$$Nest$mnotifyRotationChanged(Lcom/miui/server/input/MiuiBackTapGestureService$BackTapTouchHelper;I)V

    .line 403
    :cond_0
    return-void
.end method
