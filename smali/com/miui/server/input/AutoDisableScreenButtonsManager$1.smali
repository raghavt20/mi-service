.class Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;
.super Ljava/lang/Object;
.source "AutoDisableScreenButtonsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onStatusBarVisibilityChange(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/AutoDisableScreenButtonsManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 79
    iput-object p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    iput-boolean p2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->val$visible:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 81
    iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->val$visible:Z

    if-eqz v0, :cond_1

    .line 83
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fgetmScreenButtonsTmpDisabled(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$msaveTmpDisableButtonsStatus(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fgetmFloatView(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Lmiui/view/AutoDisableScreenbuttonsFloatView;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 87
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fgetmFloatView(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Lmiui/view/AutoDisableScreenbuttonsFloatView;

    move-result-object v0

    invoke-virtual {v0}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->dismiss()V

    goto :goto_0

    .line 89
    :cond_1
    invoke-static {}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$smgetRunningTopActivity()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/miui/server/input/util/AutoDisableScreenButtonsHelper;->getAppFlag(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 91
    .local v1, "flag":I
    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v1, v2, :cond_2

    .line 92
    iget-object v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v2, v3}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$msaveTmpDisableButtonsStatus(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V

    goto :goto_0

    .line 93
    :cond_2
    if-ne v1, v3, :cond_3

    .line 94
    iget-object v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    invoke-static {v2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$mshowFloat(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V

    .line 97
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "flag":I
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->this$0:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    iget-boolean v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->val$visible:Z

    invoke-static {v0, v1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->-$$Nest$fputmStatusBarVisibleOld(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V

    .line 98
    return-void
.end method
