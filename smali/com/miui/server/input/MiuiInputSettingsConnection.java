public class com.miui.server.input.MiuiInputSettingsConnection {
	 /* .source "MiuiInputSettingsConnection.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;, */
	 /* Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;, */
	 /* Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;, */
	 /* Lcom/miui/server/input/MiuiInputSettingsConnection$MessageWhatClient;, */
	 /* Lcom/miui/server/input/MiuiInputSettingsConnection$MessageWhatServer; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String INPUT_SETTINGS_COMPONENT;
public static final Integer MSG_CLIENT_TRIGGER_QUICK_NOTE;
public static final Integer MSG_SERVER_ADD_STYLUS_MASK;
public static final Integer MSG_SERVER_REMOVE_STYLUS_MASK;
private static final java.lang.String TAG;
private static final Integer TIMEOUT_TIME;
private static volatile com.miui.server.input.MiuiInputSettingsConnection sInstance;
/* # instance fields */
private final android.content.BroadcastReceiver mBroadcastReceiver;
private final java.util.Map mCachedMessageList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Landroid/os/Message;", */
/* "Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.util.Map mCallbackMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/Integer;", */
/* "Ljava/util/function/Consumer<", */
/* "Landroid/os/Message;", */
/* ">;>;" */
/* } */
} // .end annotation
} // .end field
private final android.os.Messenger mCallbackMessenger;
private final android.content.Context mContext;
private final android.os.Handler mHandler;
private android.os.Messenger mMessenger;
private android.content.ServiceConnection mServiceConnection;
private final java.lang.Runnable mTimerRunnable;
/* # direct methods */
public static void $r8$lambda$HazgjCEfaUwoTCtcu7Fa4YKwfwQ ( com.miui.server.input.MiuiInputSettingsConnection p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetConnection()V */
return;
} // .end method
static java.util.Map -$$Nest$fgetmCachedMessageList ( com.miui.server.input.MiuiInputSettingsConnection p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCachedMessageList;
} // .end method
static java.util.Map -$$Nest$fgetmCallbackMap ( com.miui.server.input.MiuiInputSettingsConnection p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCallbackMap;
} // .end method
static android.os.Messenger -$$Nest$fgetmMessenger ( com.miui.server.input.MiuiInputSettingsConnection p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mMessenger;
} // .end method
static void -$$Nest$fputmMessenger ( com.miui.server.input.MiuiInputSettingsConnection p0, android.os.Messenger p1 ) { //bridge//synthethic
/* .locals 0 */
this.mMessenger = p1;
return;
} // .end method
static void -$$Nest$mexecuteCallback ( com.miui.server.input.MiuiInputSettingsConnection p0, com.miui.server.input.MiuiInputSettingsConnection$OnMessageSendFinishCallback p1, Integer p2 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V */
return;
} // .end method
static void -$$Nest$mresetConnection ( com.miui.server.input.MiuiInputSettingsConnection p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetConnection()V */
return;
} // .end method
static void -$$Nest$mresetTimer ( com.miui.server.input.MiuiInputSettingsConnection p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetTimer()V */
return;
} // .end method
static java.lang.String -$$Nest$smmessageWhatClientToString ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.input.MiuiInputSettingsConnection .messageWhatClientToString ( p0 );
} // .end method
static java.lang.String -$$Nest$smmessageWhatServerToString ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.input.MiuiInputSettingsConnection .messageWhatServerToString ( p0 );
} // .end method
private com.miui.server.input.MiuiInputSettingsConnection ( ) {
/* .locals 5 */
/* .line 103 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 87 */
/* new-instance v0, Lcom/miui/server/input/MiuiInputSettingsConnection$$ExternalSyntheticLambda0; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiInputSettingsConnection$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V */
this.mTimerRunnable = v0;
/* .line 89 */
/* new-instance v0, Lcom/miui/server/input/MiuiInputSettingsConnection$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/input/MiuiInputSettingsConnection$1;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V */
this.mBroadcastReceiver = v0;
/* .line 104 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v1 ).getSystemContext ( ); // invoke-virtual {v1}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
this.mContext = v1;
/* .line 105 */
/* new-instance v2, Landroid/os/Handler; */
com.android.server.input.MiuiInputThread .getThread ( );
(( com.android.server.input.MiuiInputThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 106 */
/* new-instance v2, Landroid/os/Messenger; */
/* new-instance v3, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler; */
/* .line 107 */
com.android.server.input.MiuiInputThread .getThread ( );
(( com.android.server.input.MiuiInputThread ) v4 ).getLooper ( ); // invoke-virtual {v4}, Lcom/android/server/input/MiuiInputThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v3, p0, v4}, Lcom/miui/server/input/MiuiInputSettingsConnection$CallbackHandler;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;Landroid/os/Looper;)V */
/* invoke-direct {v2, v3}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V */
this.mCallbackMessenger = v2;
/* .line 108 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mCachedMessageList = v2;
/* .line 109 */
/* new-instance v2, Ljava/util/HashMap; */
/* invoke-direct {v2}, Ljava/util/HashMap;-><init>()V */
this.mCallbackMap = v2;
/* .line 110 */
/* new-instance v2, Landroid/content/IntentFilter; */
final String v3 = "android.intent.action.USER_SWITCHED"; // const-string v3, "android.intent.action.USER_SWITCHED"
/* invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V */
/* .line 111 */
/* .local v2, "filter":Landroid/content/IntentFilter; */
int v3 = 2; // const/4 v3, 0x2
(( android.content.Context ) v1 ).registerReceiver ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;
/* .line 112 */
return;
} // .end method
private void executeCallback ( com.miui.server.input.MiuiInputSettingsConnection$OnMessageSendFinishCallback p0, Integer p1 ) {
/* .locals 0 */
/* .param p1, "callback" # Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback; */
/* .param p2, "status" # I */
/* .line 315 */
/* if-nez p1, :cond_0 */
/* .line 316 */
return;
/* .line 318 */
} // :cond_0
/* .line 319 */
return;
} // .end method
public static com.miui.server.input.MiuiInputSettingsConnection getInstance ( ) {
/* .locals 2 */
/* .line 118 */
v0 = com.miui.server.input.MiuiInputSettingsConnection.sInstance;
/* if-nez v0, :cond_1 */
/* .line 119 */
/* const-class v0, Lcom/miui/server/input/MiuiInputSettingsConnection; */
/* monitor-enter v0 */
/* .line 120 */
try { // :try_start_0
v1 = com.miui.server.input.MiuiInputSettingsConnection.sInstance;
/* if-nez v1, :cond_0 */
/* .line 121 */
/* new-instance v1, Lcom/miui/server/input/MiuiInputSettingsConnection; */
/* invoke-direct {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;-><init>()V */
/* .line 123 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 125 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.MiuiInputSettingsConnection.sInstance;
} // .end method
private static java.lang.String messageWhatClientToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "messageWhatClient" # I */
/* .line 389 */
int v0 = 1; // const/4 v0, 0x1
/* if-ne p0, v0, :cond_0 */
/* .line 390 */
final String v0 = "MSG_CLIENT_TRIGGER_QUICK_NOTE"; // const-string v0, "MSG_CLIENT_TRIGGER_QUICK_NOTE"
/* .line 392 */
} // :cond_0
final String v0 = "NONE"; // const-string v0, "NONE"
} // .end method
private static java.lang.String messageWhatServerToString ( Integer p0 ) {
/* .locals 1 */
/* .param p0, "messageWhatServer" # I */
/* .line 378 */
/* packed-switch p0, :pswitch_data_0 */
/* .line 384 */
final String v0 = "NONE"; // const-string v0, "NONE"
/* .line 382 */
/* :pswitch_0 */
final String v0 = "MSG_SERVER_REMOVE_STYLUS_MASK"; // const-string v0, "MSG_SERVER_REMOVE_STYLUS_MASK"
/* .line 380 */
/* :pswitch_1 */
final String v0 = "MSG_SERVER_ADD_STYLUS_MASK"; // const-string v0, "MSG_SERVER_ADD_STYLUS_MASK"
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void removeTimer ( ) {
/* .locals 2 */
/* .line 264 */
v0 = this.mHandler;
v1 = this.mTimerRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 265 */
return;
} // .end method
private void resetConnection ( ) {
/* .locals 2 */
/* .line 271 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "request reset connection, connect = "; // const-string v1, "request reset connection, connect = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mServiceConnection;
if ( v1 != null) { // if-eqz v1, :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiInputSettingsConnection"; // const-string v1, "MiuiInputSettingsConnection"
android.util.Slog .i ( v1,v0 );
/* .line 272 */
v0 = this.mServiceConnection;
/* if-nez v0, :cond_1 */
/* .line 273 */
return;
/* .line 275 */
} // :cond_1
v1 = this.mContext;
(( android.content.Context ) v1 ).unbindService ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 276 */
int v0 = 0; // const/4 v0, 0x0
this.mServiceConnection = v0;
/* .line 277 */
this.mMessenger = v0;
/* .line 278 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->removeTimer()V */
/* .line 279 */
return;
} // .end method
private void resetTimer ( ) {
/* .locals 4 */
/* .line 256 */
v0 = this.mHandler;
v1 = this.mTimerRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 257 */
v0 = this.mHandler;
v1 = this.mTimerRunnable;
/* const-wide/16 v2, 0x3a98 */
(( android.os.Handler ) v0 ).postDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 258 */
return;
} // .end method
/* # virtual methods */
public Boolean isBindToInputSettings ( ) {
/* .locals 1 */
/* .line 249 */
v0 = this.mServiceConnection;
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void registerCallbackListener ( Integer p0, java.util.function.Consumer p1 ) {
/* .locals 4 */
/* .param p1, "messageWhatClient" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(I", */
/* "Ljava/util/function/Consumer<", */
/* "Landroid/os/Message;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 218 */
/* .local p2, "consumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Landroid/os/Message;>;" */
v0 = this.mCallbackMap;
/* monitor-enter v0 */
/* .line 219 */
try { // :try_start_0
v1 = this.mCallbackMap;
v1 = java.lang.Integer .valueOf ( p1 );
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 220 */
final String v1 = "MiuiInputSettingsConnection"; // const-string v1, "MiuiInputSettingsConnection"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "callback for what = "; // const-string v3, "callback for what = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 221 */
com.miui.server.input.MiuiInputSettingsConnection .messageWhatClientToString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", already registered"; // const-string v3, ", already registered"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 220 */
android.util.Slog .e ( v1,v2 );
/* .line 222 */
/* monitor-exit v0 */
return;
/* .line 224 */
} // :cond_0
v1 = this.mCallbackMap;
java.lang.Integer .valueOf ( p1 );
/* .line 225 */
/* monitor-exit v0 */
/* .line 226 */
return;
/* .line 225 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void sendMessageToInputSettings ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "messageWhatServer" # I */
/* .line 135 */
android.os.Message .obtain ( );
/* .line 136 */
/* .local v0, "message":Landroid/os/Message; */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.input.MiuiInputSettingsConnection ) p0 ).sendMessageToInputSettings ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(ILandroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V
/* .line 137 */
return;
} // .end method
public void sendMessageToInputSettings ( Integer p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "messageWhatServer" # I */
/* .param p2, "arg1" # I */
/* .line 147 */
android.os.Message .obtain ( );
/* .line 148 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->arg1:I */
/* .line 149 */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.input.MiuiInputSettingsConnection ) p0 ).sendMessageToInputSettings ( p1, v0, v1 ); // invoke-virtual {p0, p1, v0, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(ILandroid/os/Message;Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;)V
/* .line 150 */
return;
} // .end method
public synchronized void sendMessageToInputSettings ( Integer p0, android.os.Message p1, com.miui.server.input.MiuiInputSettingsConnection$OnMessageSendFinishCallback p2 ) {
/* .locals 6 */
/* .param p1, "messageWhatServer" # I */
/* .param p2, "message" # Landroid/os/Message; */
/* .param p3, "callback" # Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback; */
/* monitor-enter p0 */
/* .line 167 */
try { // :try_start_0
/* iput p1, p2, Landroid/os/Message;->what:I */
/* .line 168 */
v0 = this.mCallbackMessenger;
this.replyTo = v0;
/* .line 169 */
v0 = this.mServiceConnection;
/* if-nez v0, :cond_1 */
v0 = this.mMessenger;
/* if-nez v0, :cond_1 */
/* .line 171 */
final String v0 = "com.miui.securitycore/com.miui.miinput.service.MiuiInputSettingsService"; // const-string v0, "com.miui.securitycore/com.miui.miinput.service.MiuiInputSettingsService"
/* .line 172 */
android.content.ComponentName .unflattenFromString ( v0 );
/* .line 173 */
/* .local v0, "serviceComponent":Landroid/content/ComponentName; */
/* new-instance v1, Landroid/content/Intent; */
/* invoke-direct {v1}, Landroid/content/Intent;-><init>()V */
/* .line 174 */
/* .local v1, "serviceIntent":Landroid/content/Intent; */
(( android.content.Intent ) v1 ).setComponent ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
/* .line 175 */
/* new-instance v2, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection; */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection;-><init>(Lcom/miui/server/input/MiuiInputSettingsConnection;Lcom/miui/server/input/MiuiInputSettingsConnection$InputSettingsServiceConnection-IA;)V */
/* .line 176 */
/* .local v2, "connection":Landroid/content/ServiceConnection; */
v3 = this.mContext;
v4 = android.os.UserHandle.CURRENT;
/* const v5, 0x44000001 # 512.00006f */
v3 = (( android.content.Context ) v3 ).bindServiceAsUser ( v1, v2, v5, v4 ); // invoke-virtual {v3, v1, v2, v5, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 179 */
final String v3 = "MiuiInputSettingsConnection"; // const-string v3, "MiuiInputSettingsConnection"
/* const-string/jumbo v4, "service bind success" */
android.util.Slog .i ( v3,v4 );
/* .line 180 */
this.mServiceConnection = v2;
/* .line 182 */
} // .end local p0 # "this":Lcom/miui/server/input/MiuiInputSettingsConnection;
} // :cond_0
final String v3 = "MiuiInputSettingsConnection"; // const-string v3, "MiuiInputSettingsConnection"
/* const-string/jumbo v4, "service bind fail" */
android.util.Slog .e ( v3,v4 );
/* .line 183 */
v3 = this.mContext;
(( android.content.Context ) v3 ).unbindService ( v2 ); // invoke-virtual {v3, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
/* .line 186 */
} // .end local v0 # "serviceComponent":Landroid/content/ComponentName;
} // .end local v1 # "serviceIntent":Landroid/content/Intent;
} // .end local v2 # "connection":Landroid/content/ServiceConnection;
} // :cond_1
} // :goto_0
v0 = this.mServiceConnection;
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_2 */
/* .line 187 */
/* invoke-direct {p0, p3, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 189 */
/* monitor-exit p0 */
return;
/* .line 191 */
} // :cond_2
try { // :try_start_1
v0 = this.mMessenger;
/* if-nez v0, :cond_3 */
/* .line 193 */
final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "cache message "; // const-string v2, "cache message "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p2, Landroid/os/Message;->what:I */
com.miui.server.input.MiuiInputSettingsConnection .messageWhatServerToString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 194 */
v0 = this.mCachedMessageList;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 195 */
/* monitor-exit p0 */
return;
/* .line 198 */
} // :cond_3
try { // :try_start_2
final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "send message " */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p2, Landroid/os/Message;->what:I */
/* .line 199 */
com.miui.server.input.MiuiInputSettingsConnection .messageWhatServerToString ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " to settings"; // const-string v3, " to settings"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 198 */
android.util.Slog .i ( v0,v2 );
/* .line 200 */
v0 = this.mMessenger;
(( android.os.Messenger ) v0 ).send ( p2 ); // invoke-virtual {v0, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
/* .line 201 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p3, v0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V */
/* .line 202 */
/* invoke-direct {p0}, Lcom/miui/server/input/MiuiInputSettingsConnection;->resetTimer()V */
/* :try_end_2 */
/* .catch Landroid/os/RemoteException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 206 */
/* .line 203 */
/* :catch_0 */
/* move-exception v0 */
/* .line 204 */
/* .local v0, "e":Landroid/os/RemoteException; */
try { // :try_start_3
/* invoke-direct {p0, p3, v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->executeCallback(Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;I)V */
/* .line 205 */
final String v1 = "MiuiInputSettingsConnection"; // const-string v1, "MiuiInputSettingsConnection"
(( android.os.RemoteException ) v0 ).getMessage ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;
android.util.Slog .e ( v1,v2,v0 );
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 207 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 166 */
} // .end local p1 # "messageWhatServer":I
} // .end local p2 # "message":Landroid/os/Message;
} // .end local p3 # "callback":Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void unRegisterCallbackListener ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "messageWhatClient" # I */
/* .line 235 */
v0 = this.mCallbackMap;
/* monitor-enter v0 */
/* .line 236 */
try { // :try_start_0
v1 = this.mCallbackMap;
v1 = java.lang.Integer .valueOf ( p1 );
/* if-nez v1, :cond_0 */
/* .line 237 */
final String v1 = "MiuiInputSettingsConnection"; // const-string v1, "MiuiInputSettingsConnection"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "callback for what = "; // const-string v3, "callback for what = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 238 */
com.miui.server.input.MiuiInputSettingsConnection .messageWhatClientToString ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ", not registered"; // const-string v3, ", not registered"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 237 */
android.util.Slog .e ( v1,v2 );
/* .line 239 */
/* monitor-exit v0 */
return;
/* .line 241 */
} // :cond_0
v1 = this.mCallbackMap;
java.lang.Integer .valueOf ( p1 );
/* .line 242 */
/* monitor-exit v0 */
/* .line 243 */
return;
/* .line 242 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
