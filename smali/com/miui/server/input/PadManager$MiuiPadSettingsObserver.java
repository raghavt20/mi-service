class com.miui.server.input.PadManager$MiuiPadSettingsObserver extends android.database.ContentObserver {
	 /* .source "PadManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/PadManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiPadSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.PadManager this$0; //synthetic
/* # direct methods */
 com.miui.server.input.PadManager$MiuiPadSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/PadManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 146 */
this.this$0 = p1;
/* .line 147 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 148 */
return;
} // .end method
/* # virtual methods */
void observer ( ) {
/* .locals 5 */
/* .line 151 */
v0 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 152 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* nop */
/* .line 153 */
/* const-string/jumbo v1, "user_setup_complete" */
android.provider.Settings$Secure .getUriFor ( v1 );
/* .line 152 */
int v3 = 0; // const/4 v3, 0x0
int v4 = -1; // const/4 v4, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v2, v3, p0, v4 ); // invoke-virtual {v0, v2, v3, p0, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 155 */
android.provider.Settings$Secure .getUriFor ( v1 );
(( com.miui.server.input.PadManager$MiuiPadSettingsObserver ) p0 ).onChange ( v3, v1 ); // invoke-virtual {p0, v3, v1}, Lcom/miui/server/input/PadManager$MiuiPadSettingsObserver;->onChange(ZLandroid/net/Uri;)V
/* .line 156 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 5 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 160 */
v0 = this.this$0;
com.miui.server.input.PadManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 161 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
/* const-string/jumbo v1, "user_setup_complete" */
android.provider.Settings$Secure .getUriFor ( v1 );
v2 = (( android.net.Uri ) v2 ).equals ( p2 ); // invoke-virtual {v2, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
	 /* .line 162 */
	 v2 = this.this$0;
	 /* .line 163 */
	 v3 = 	 android.os.UserHandle .myUserId ( );
	 /* .line 162 */
	 int v4 = 0; // const/4 v4, 0x0
	 v1 = 	 android.provider.Settings$Secure .getIntForUser ( v0,v1,v4,v3 );
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 int v4 = 1; // const/4 v4, 0x1
	 } // :cond_0
	 com.miui.server.input.PadManager .-$$Nest$fputmIsUserSetup ( v2,v4 );
	 /* .line 165 */
} // :cond_1
return;
} // .end method
