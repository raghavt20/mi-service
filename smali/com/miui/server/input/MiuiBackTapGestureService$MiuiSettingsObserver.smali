.class Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiBackTapGestureService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiBackTapGestureService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiBackTapGestureService;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/MiuiBackTapGestureService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiBackTapGestureService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 205
    iput-object p1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    .line 206
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 207
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 210
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 211
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "back_double_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 213
    const-string v1, "back_triple_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 215
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 217
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 221
    const-string v0, "back_double_tap"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fputmBackDoubleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :cond_0
    const-string v0, "back_triple_tap"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/provider/MiuiSettings$Key;->getKeyAndGestureShortcutFunction(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fputmBackTripleTapFunction(Lcom/miui/server/input/MiuiBackTapGestureService;Ljava/lang/String;)V

    goto :goto_0

    .line 227
    :cond_1
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 228
    iget-object v1, p0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v1}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiBackTapGestureService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    move v3, v2

    :cond_2
    invoke-static {v1, v3}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$fputmIsGameMode(Lcom/miui/server/input/MiuiBackTapGestureService;Z)V

    .line 230
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiBackTapGestureService$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiBackTapGestureService;

    invoke-static {v0}, Lcom/miui/server/input/MiuiBackTapGestureService;->-$$Nest$mupdateBackTapFeatureState(Lcom/miui/server/input/MiuiBackTapGestureService;)V

    .line 231
    return-void
.end method
