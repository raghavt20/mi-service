class com.miui.server.input.MiuiBackTapGestureService$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiBackTapGestureService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiBackTapGestureService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiBackTapGestureService this$0; //synthetic
/* # direct methods */
public com.miui.server.input.MiuiBackTapGestureService$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiBackTapGestureService; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 205 */
this.this$0 = p1;
/* .line 206 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 207 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 210 */
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 211 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "back_double_tap"; // const-string v1, "back_double_tap"
android.provider.Settings$System .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 213 */
final String v1 = "back_triple_tap"; // const-string v1, "back_triple_tap"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 215 */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 217 */
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 4 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 221 */
final String v0 = "back_double_tap"; // const-string v0, "back_double_tap"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 222 */
	 v1 = this.this$0;
	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v1 );
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v2,v0 );
	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fputmBackDoubleTapFunction ( v1,v0 );
	 /* .line 224 */
} // :cond_0
final String v0 = "back_triple_tap"; // const-string v0, "back_triple_tap"
android.provider.Settings$System .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 225 */
	 v1 = this.this$0;
	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v1 );
	 android.provider.MiuiSettings$Key .getKeyAndGestureShortcutFunction ( v2,v0 );
	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fputmBackTripleTapFunction ( v1,v0 );
	 /* .line 227 */
} // :cond_1
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
v1 = (( android.net.Uri ) v1 ).equals ( p2 ); // invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
	 /* .line 228 */
	 v1 = this.this$0;
	 com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fgetmContext ( v1 );
	 (( android.content.Context ) v2 ).getContentResolver ( ); // invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
	 int v3 = 0; // const/4 v3, 0x0
	 v0 = 	 android.provider.Settings$Secure .getInt ( v2,v0,v3 );
	 int v2 = 1; // const/4 v2, 0x1
	 /* if-ne v0, v2, :cond_2 */
	 /* move v3, v2 */
} // :cond_2
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$fputmIsGameMode ( v1,v3 );
/* .line 230 */
} // :cond_3
} // :goto_0
v0 = this.this$0;
com.miui.server.input.MiuiBackTapGestureService .-$$Nest$mupdateBackTapFeatureState ( v0 );
/* .line 231 */
return;
} // .end method
