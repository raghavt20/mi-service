public abstract class com.miui.server.input.MiuiInputSettingsConnection$OnMessageSendFinishCallback {
	 /* .source "MiuiInputSettingsConnection.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiInputSettingsConnection; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "OnMessageSendFinishCallback" */
} // .end annotation
/* .annotation system Ldalvik/annotation/MemberClasses; */
/* value = { */
/* Lcom/miui/server/input/MiuiInputSettingsConnection$OnMessageSendFinishCallback$MessageSendFinishStatus; */
/* } */
} // .end annotation
/* # static fields */
public static final Integer STATUS_SEND_FAIL_EXCEPTION;
public static final Integer STATUS_SEND_FAIL_NOT_FOUND_SERVICE;
public static final Integer STATUS_SEND_SUCCESS;
/* # virtual methods */
public abstract void onMessageSendFinish ( Integer p0 ) {
} // .end method
