.class public Lcom/miui/server/input/AutoDisableScreenButtonsManager;
.super Ljava/lang/Object;
.source "AutoDisableScreenButtonsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;
    }
.end annotation


# static fields
.field private static final ENABLE_KEY_PRESS_INTERVAL:I = 0x7d0

.field private static final NAVIGATION_BAR_HEIGHT:Ljava/lang/String; = "navigation_bar_height"

.field private static final PREF_ADSB_NOT_SHOW_PROMPTS:Ljava/lang/String; = "ADSB_NOT_SHOW_PROMPTS"

.field private static final SettingsActionComponent:Landroid/content/ComponentName;

.field private static final TAG:Ljava/lang/String; = "AutoDisableScreenButtonsManager"

.field private static final TMP_DISABLE_BUTTON:I = 0x2

.field private static sAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;


# instance fields
.field private mCloudConfig:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mCurUserId:I

.field private mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

.field private mHandler:Landroid/os/Handler;

.field private final mLock:Ljava/lang/Object;

.field private mScreenButtonPressedKeyCode:I

.field private mScreenButtonPressedTime:J

.field private mScreenButtonsDisabled:Z

.field private mScreenButtonsTmpDisabled:Z

.field private mStatusBarVisibleOld:Z

.field private mToastShowTime:J

.field private mTwice:Z

.field private mUserSetting:Ljava/lang/String;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmFloatView(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Lmiui/view/AutoDisableScreenbuttonsFloatView;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmScreenButtonsTmpDisabled(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmStatusBarVisibleOld(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mStatusBarVisibleOld:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$msaveTmpDisableButtonsStatus(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->saveTmpDisableButtonsStatus(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowFloat(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showFloat()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowPromptsIfNeeds(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showPromptsIfNeeds()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mshowSettings(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mshowToastInner(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToastInner(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSettings(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->updateSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$smgetRunningTopActivity()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->getRunningTopActivity()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 60
    nop

    .line 61
    const-string v0, "com.android.settings/.AutoDisableScreenButtonsAppListSettingsActivity"

    invoke-static {v0}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->SettingsActionComponent:Landroid/content/ComponentName;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    .line 50
    iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mStatusBarVisibleOld:Z

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mHandler:Landroid/os/Handler;

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mLock:Ljava/lang/Object;

    .line 70
    iput-object p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    .line 71
    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->resetButtonsStatus()V

    .line 72
    new-instance v0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;

    invoke-direct {v0, p0, p2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Landroid/os/Handler;)V

    .line 73
    .local v0, "observer":Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;
    invoke-virtual {v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->observe()V

    .line 74
    sput-object p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->sAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    .line 75
    return-void
.end method

.method private getAndroidDimen(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .line 255
    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 256
    return v0

    .line 258
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {v1, p2, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 259
    .local v1, "heightResId":I
    if-lez v1, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :cond_1
    return v0
.end method

.method private getNavigationBarHeight(Landroid/content/Context;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 251
    const-string v0, "navigation_bar_height"

    invoke-direct {p0, p1, v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->getAndroidDimen(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static getRunningTopActivity()Ljava/lang/String;
    .locals 2

    .line 330
    invoke-static {}, Lcom/android/server/wm/WindowProcessUtils;->getTopRunningActivityInfo()Ljava/util/HashMap;

    move-result-object v0

    .line 331
    .local v0, "topActivityInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez v0, :cond_0

    .line 332
    const/4 v1, 0x0

    return-object v1

    .line 334
    :cond_0
    const-string v1, "packageName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    return-object v1
.end method

.method public static onStatusBarVisibilityChangeStatic(Z)V
    .locals 1
    .param p0, "visible"    # Z

    .line 104
    sget-object v0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->sAutoDisableScreenButtonsManager:Lcom/miui/server/input/AutoDisableScreenButtonsManager;

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onStatusBarVisibilityChange(Z)V

    .line 107
    :cond_0
    return-void
.end method

.method private resetButtonsStatus()V
    .locals 1

    .line 321
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->saveDisableButtonsStatus(Z)V

    .line 322
    iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z

    .line 323
    return-void
.end method

.method private saveDisableButtonsStatus(Z)V
    .locals 3
    .param p1, "disable"    # Z

    .line 306
    iput-boolean p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z

    .line 307
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 308
    iget v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    .line 307
    const-string v2, "screen_buttons_state"

    invoke-static {v0, v2, p1, v1}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 309
    return-void
.end method

.method private saveTmpDisableButtonsStatus(Z)V
    .locals 4
    .param p1, "tmp"    # Z

    .line 312
    iput-boolean p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z

    .line 313
    iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z

    if-eqz v0, :cond_0

    .line 314
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 317
    if-eqz p1, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    .line 316
    const-string v3, "screen_buttons_state"

    invoke-static {v0, v3, v1, v2}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 318
    return-void
.end method

.method private setLinearLayoutMarginBottom()V
    .locals 4

    .line 241
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    .line 242
    const v1, 0x110a0019

    invoke-virtual {v0, v1}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 243
    .local v0, "linearLayout":Landroid/widget/LinearLayout;
    nop

    .line 244
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 245
    .local v1, "layoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->getNavigationBarHeight(Landroid/content/Context;)I

    move-result v2

    .line 246
    .local v2, "navigationBarHeight":I
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v3, v3, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 247
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    return-void
.end method

.method private showFloat()V
    .locals 2

    .line 214
    const-string v0, "AutoDisableScreenButtonsManager"

    const-string/jumbo v1, "showing auto disable screen buttons float window..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->inflate(Landroid/content/Context;)Lmiui/view/AutoDisableScreenbuttonsFloatView;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    .line 218
    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->setLinearLayoutMarginBottom()V

    .line 220
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    new-instance v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager$3;

    invoke-direct {v1, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$3;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V

    invoke-virtual {v0, v1}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    new-instance v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager$4;

    invoke-direct {v1, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$4;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V

    invoke-virtual {v0, v1}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mFloatView:Lmiui/view/AutoDisableScreenbuttonsFloatView;

    invoke-virtual {v0}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->show()V

    .line 238
    return-void
.end method

.method private showPromptsIfNeeds()Z
    .locals 7

    .line 265
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    const-string v1, "ADSB_NOT_SHOW_PROMPTS"

    invoke-static {v0, v1}, Lcom/miui/server/input/util/AutoDisableScreenButtonsHelper;->getValue(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 266
    .local v0, "obj":Ljava/lang/Object;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move-object v2, v0

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 267
    .local v2, "notShow":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 268
    return v1

    .line 271
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 272
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x110f0060

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 273
    const v4, 0x110f005d

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 277
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v5, Lcom/miui/server/input/AutoDisableScreenButtonsManager$5;

    invoke-direct {v5, p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$5;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;)V

    .line 278
    const v6, 0x110f005f

    invoke-virtual {v3, v6, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 287
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 288
    .local v3, "adlg":Landroid/app/AlertDialog;
    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x7d3

    invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V

    .line 289
    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 290
    return v4
.end method

.method private showSettings()V
    .locals 4

    .line 294
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 295
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->SettingsActionComponent:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 296
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 299
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    goto :goto_0

    .line 300
    :catch_0
    move-exception v1

    .line 301
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start activity exception, component = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->SettingsActionComponent:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "AutoDisableScreenButtonsManager"

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :goto_0
    return-void
.end method

.method private showToast(Ljava/lang/CharSequence;Landroid/os/Handler;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "h"    # Landroid/os/Handler;

    .line 196
    if-eqz p2, :cond_0

    .line 197
    new-instance v0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$2;

    invoke-direct {v0, p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$2;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 203
    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToastInner(Ljava/lang/CharSequence;)V

    .line 205
    :goto_0
    return-void
.end method

.method private showToast(ZLandroid/os/Handler;)V
    .locals 2
    .param p1, "enabled"    # Z
    .param p2, "h"    # Landroid/os/Handler;

    .line 191
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_0

    const v1, 0x110f005b

    goto :goto_0

    .line 192
    :cond_0
    const v1, 0x110f005a

    .line 191
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToast(Ljava/lang/CharSequence;Landroid/os/Handler;)V

    .line 193
    return-void
.end method

.method private showToastInner(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 208
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 209
    .local v0, "toast":Landroid/widget/Toast;
    const/16 v1, 0x7d6

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setType(I)V

    .line 210
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 211
    return-void
.end method

.method private updateSettings()V
    .locals 6

    .line 362
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 363
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 364
    :try_start_0
    const-string v2, "screen_buttons_state"

    iget v3, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    const/4 v4, 0x0

    invoke-static {v0, v2, v4, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    .line 366
    .local v2, "btnState":I
    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 371
    :pswitch_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z

    goto :goto_0

    .line 368
    :pswitch_1
    iput-boolean v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z

    .line 369
    nop

    .line 375
    :goto_0
    const-string v3, "auto_disable_screen_button"

    iget v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    invoke-static {v0, v3, v4}, Landroid/provider/MiuiSettings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 377
    .local v3, "userSetting":Ljava/lang/String;
    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mUserSetting:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 378
    iput-object v3, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mUserSetting:Ljava/lang/String;

    .line 379
    invoke-static {v3}, Lcom/miui/server/input/util/AutoDisableScreenButtonsHelper;->updateUserJson(Ljava/lang/String;)V

    .line 381
    :cond_0
    const-string v4, "auto_disable_screen_button_cloud_setting"

    invoke-static {v0, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 383
    .local v4, "cloudConfig":Ljava/lang/String;
    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCloudConfig:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 384
    iput-object v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCloudConfig:Ljava/lang/String;

    .line 385
    invoke-static {v4}, Lcom/miui/server/input/util/AutoDisableScreenButtonsHelper;->updateCloudJson(Ljava/lang/String;)V

    .line 387
    .end local v2    # "btnState":I
    .end local v3    # "userSetting":Ljava/lang/String;
    .end local v4    # "cloudConfig":Ljava/lang/String;
    :cond_1
    monitor-exit v1

    .line 388
    return-void

    .line 387
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public handleDisableButtons(IZZZLandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "down"    # Z
    .param p3, "disableForSingleKey"    # Z
    .param p4, "disableForLidClose"    # Z
    .param p5, "event"    # Landroid/view/KeyEvent;

    .line 118
    const/4 v0, 0x0

    .line 119
    .local v0, "disable":Z
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {p5}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/InputDevice;->isVirtual()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v3

    .line 120
    .local v1, "isVirtual":Z
    :goto_1
    invoke-virtual {p5}, Landroid/view/KeyEvent;->getFlags()I

    move-result v4

    and-int/lit8 v4, v4, 0x40

    if-eqz v4, :cond_2

    move v4, v3

    goto :goto_2

    :cond_2
    move v4, v2

    .line 122
    .local v4, "isVirtualHardKey":Z
    :goto_2
    iget-object v5, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    iget v6, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    invoke-static {v5, p1, v6}, Lcom/miui/enterprise/RestrictionsHelper;->hasKeyCodeRestriction(Landroid/content/Context;II)Z

    move-result v5

    if-nez v5, :cond_8

    .line 124
    invoke-static {}, Lmiui/enterprise/RestrictionsHelperStub;->getInstance()Lmiui/enterprise/IRestrictionsHelper;

    move-result-object v5

    iget v6, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    invoke-interface {v5, p1, v6}, Lmiui/enterprise/IRestrictionsHelper;->isKeyCodeRestriction(II)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_4

    .line 129
    :cond_3
    const-string v3, "AutoDisableScreenButtonsManager"

    sparse-switch p1, :sswitch_data_0

    .line 151
    return v2

    .line 133
    :sswitch_0
    if-eqz p3, :cond_4

    if-nez v1, :cond_4

    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disableForSingleKey keyCode:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/4 v0, 0x1

    .line 136
    goto :goto_3

    .line 140
    :cond_4
    :sswitch_1
    if-eqz v1, :cond_5

    if-eqz v4, :cond_7

    .line 141
    :cond_5
    if-eqz p4, :cond_6

    invoke-static {}, Lmiui/util/SmartCoverManager;->deviceDisableKeysWhenLidClose()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disableForLidClose keyCode:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v0, 0x1

    goto :goto_3

    .line 144
    :cond_6
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->screenButtonsInterceptKey(IZ)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 145
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "screenButtonsDisabled keyCode:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const/4 v0, 0x1

    .line 153
    :cond_7
    :goto_3
    return v0

    .line 126
    :cond_8
    :goto_4
    return v3

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0x52 -> :sswitch_0
        0x54 -> :sswitch_1
        0xbb -> :sswitch_0
    .end sparse-switch
.end method

.method public isScreenButtonsDisabled()Z
    .locals 1

    .line 187
    iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsDisabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public onStatusBarVisibilityChange(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .line 78
    iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mStatusBarVisibleOld:Z

    if-eq p1, v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;-><init>(Lcom/miui/server/input/AutoDisableScreenButtonsManager;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 101
    :cond_0
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "uid"    # I

    .line 180
    iget v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    if-eq v0, p1, :cond_0

    .line 181
    iput p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mCurUserId:I

    .line 182
    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->updateSettings()V

    .line 184
    :cond_0
    return-void
.end method

.method public resetTmpButtonsStatus()V
    .locals 1

    .line 326
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonsTmpDisabled:Z

    .line 327
    return-void
.end method

.method public screenButtonsInterceptKey(IZ)Z
    .locals 8
    .param p1, "keycode"    # I
    .param p2, "down"    # Z

    .line 157
    invoke-virtual {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->isScreenButtonsDisabled()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 158
    return v1

    .line 160
    :cond_0
    const/4 v0, 0x1

    if-eqz p2, :cond_2

    .line 161
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 162
    .local v2, "time":J
    iget-wide v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedTime:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    iget v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedKeyCode:I

    if-ne v4, p1, :cond_1

    iget-boolean v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z

    if-eqz v4, :cond_1

    .line 164
    iput-boolean v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z

    .line 165
    invoke-direct {p0}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->resetButtonsStatus()V

    .line 166
    return v1

    .line 168
    :cond_1
    iput-wide v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedTime:J

    .line 169
    iput p1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mScreenButtonPressedKeyCode:I

    .line 170
    iput-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mTwice:Z

    .line 171
    iget-wide v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mToastShowTime:J

    sub-long v4, v2, v4

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 172
    iput-wide v2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mToastShowTime:J

    .line 173
    iget-object v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mContext:Landroid/content/Context;

    const v4, 0x110f0061

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->mHandler:Landroid/os/Handler;

    invoke-direct {p0, v1, v4}, Lcom/miui/server/input/AutoDisableScreenButtonsManager;->showToast(Ljava/lang/CharSequence;Landroid/os/Handler;)V

    .line 176
    .end local v2    # "time":J
    :cond_2
    return v0
.end method
