.class Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiFingerPrintTapListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiFingerPrintTapListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiFingerPrintTapListener;


# direct methods
.method constructor <init>(Lcom/miui/server/input/MiuiFingerPrintTapListener;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiFingerPrintTapListener;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 180
    iput-object p1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    .line 181
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 182
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 185
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-static {v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiFingerPrintTapListener;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 186
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "fingerprint_double_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 188
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 192
    const-string v0, "fingerprint_double_tap"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-static {v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->-$$Nest$fgetmContext(Lcom/miui/server/input/MiuiFingerPrintTapListener;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, -0x2

    invoke-static {v2, v0, v3}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->-$$Nest$fputmDoubleTapSideFp(Lcom/miui/server/input/MiuiFingerPrintTapListener;Ljava/lang/String;)V

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fingerprint_drop_down changed, mDoubleTapSideFp = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-static {v1}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->-$$Nest$fgetmDoubleTapSideFp(Lcom/miui/server/input/MiuiFingerPrintTapListener;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiFingerPrintTapListener"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/MiuiFingerPrintTapListener;

    invoke-static {v0}, Lcom/miui/server/input/MiuiFingerPrintTapListener;->-$$Nest$mupdatePrintTapState(Lcom/miui/server/input/MiuiFingerPrintTapListener;)V

    .line 198
    return-void
.end method
