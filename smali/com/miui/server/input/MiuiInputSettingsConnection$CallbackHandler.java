public class com.miui.server.input.MiuiInputSettingsConnection$CallbackHandler extends android.os.Handler {
	 /* .source "MiuiInputSettingsConnection.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiInputSettingsConnection; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x1 */
/* name = "CallbackHandler" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.MiuiInputSettingsConnection this$0; //synthetic
/* # direct methods */
public com.miui.server.input.MiuiInputSettingsConnection$CallbackHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/MiuiInputSettingsConnection; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 322 */
this.this$0 = p1;
/* .line 323 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 324 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 4 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 328 */
final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "receive message from settings "; // const-string v2, "receive message from settings "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$smmessageWhatClientToString ( v2 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 329 */
v0 = this.this$0;
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$mresetTimer ( v0 );
/* .line 331 */
v0 = this.this$0;
com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$fgetmCallbackMap ( v0 );
/* monitor-enter v0 */
/* .line 332 */
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$fgetmCallbackMap ( v1 );
	 /* iget v2, p1, Landroid/os/Message;->what:I */
	 java.lang.Integer .valueOf ( v2 );
	 /* check-cast v1, Ljava/util/function/Consumer; */
	 /* .line 333 */
	 /* .local v1, "messageConsumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Landroid/os/Message;>;" */
	 /* monitor-exit v0 */
	 /* :try_end_0 */
	 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
	 /* .line 334 */
	 /* if-nez v1, :cond_0 */
	 /* .line 335 */
	 final String v0 = "MiuiInputSettingsConnection"; // const-string v0, "MiuiInputSettingsConnection"
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "not found callback for what = "; // const-string v3, "not found callback for what = "
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* iget v3, p1, Landroid/os/Message;->what:I */
	 /* .line 336 */
	 com.miui.server.input.MiuiInputSettingsConnection .-$$Nest$smmessageWhatClientToString ( v3 );
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* .line 335 */
	 android.util.Slog .w ( v0,v2 );
	 /* .line 337 */
	 return;
	 /* .line 339 */
} // :cond_0
/* .line 340 */
return;
/* .line 333 */
} // .end local v1 # "messageConsumer":Ljava/util/function/Consumer;, "Ljava/util/function/Consumer<Landroid/os/Message;>;"
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
