.class Lcom/miui/server/input/MiuiInputSettingsConnection$1;
.super Landroid/content/BroadcastReceiver;
.source "MiuiInputSettingsConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/MiuiInputSettingsConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;


# direct methods
.method constructor <init>(Lcom/miui/server/input/MiuiInputSettingsConnection;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/MiuiInputSettingsConnection;

    .line 89
    iput-object p1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$1;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 92
    iget-object v0, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$1;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    monitor-enter v0

    .line 93
    :try_start_0
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    const-string v1, "MiuiInputSettingsConnection"

    const-string v2, "User switched, request reset connection"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v1, p0, Lcom/miui/server/input/MiuiInputSettingsConnection$1;->this$0:Lcom/miui/server/input/MiuiInputSettingsConnection;

    invoke-static {v1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->-$$Nest$mresetConnection(Lcom/miui/server/input/MiuiInputSettingsConnection;)V

    .line 97
    :cond_0
    monitor-exit v0

    .line 98
    return-void

    .line 97
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
