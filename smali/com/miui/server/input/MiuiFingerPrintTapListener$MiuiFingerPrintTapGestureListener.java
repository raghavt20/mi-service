class com.miui.server.input.MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener implements com.miui.server.input.gesture.MiuiGestureListener {
	 /* .source "MiuiFingerPrintTapListener.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/MiuiFingerPrintTapListener; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x8 */
/* name = "MiuiFingerPrintTapGestureListener" */
} // .end annotation
/* # instance fields */
private Integer mPointerCount;
/* # direct methods */
 com.miui.server.input.MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener ( ) {
/* .locals 0 */
/* .line 146 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public Boolean isFingerPressing ( ) {
/* .locals 1 */
/* .line 163 */
/* iget v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->mPointerCount:I */
/* if-lez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 1 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 151 */
v0 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
/* packed-switch v0, :pswitch_data_0 */
/* .line 157 */
/* :pswitch_0 */
v0 = (( android.view.MotionEvent ) p1 ).getPointerCount ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I
/* iput v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->mPointerCount:I */
/* .line 154 */
/* :pswitch_1 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/MiuiFingerPrintTapListener$MiuiFingerPrintTapGestureListener;->mPointerCount:I */
/* .line 155 */
/* nop */
/* .line 160 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
