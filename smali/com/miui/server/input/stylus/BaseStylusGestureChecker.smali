.class public abstract Lcom/miui/server/input/stylus/BaseStylusGestureChecker;
.super Ljava/lang/Object;
.source "BaseStylusGestureChecker.java"


# static fields
.field protected static final STATE_CHECK_FAIL:I = 0x2

.field protected static final STATE_CHECK_SUCCESS:I = 0x3

.field protected static final STATE_DETECTING:I = 0x1

.field protected static final STATE_NO_DETECT:I


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

.field protected mWindowManager:Landroid/view/WindowManager;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/miui/server/input/stylus/BaseStylusGestureChecker;->mContext:Landroid/content/Context;

    .line 23
    const-class v0, Landroid/view/WindowManager;

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/miui/server/input/stylus/BaseStylusGestureChecker;->mWindowManager:Landroid/view/WindowManager;

    .line 24
    invoke-static {p1}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/BaseStylusGestureChecker;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    .line 25
    return-void
.end method


# virtual methods
.method public abstract onPointerEvent(Landroid/view/MotionEvent;)V
.end method
