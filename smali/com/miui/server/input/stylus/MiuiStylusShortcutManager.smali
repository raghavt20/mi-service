.class public Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;
.super Ljava/lang/Object;
.source "MiuiStylusShortcutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;,
        Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;
    }
.end annotation


# static fields
.field private static final HALL_CLOSE:I = 0x0

.field private static final HALL_FAR:I = 0x1

.field private static final HOME_PACKAGE_NAME:Ljava/lang/String; = "com.miui.home"

.field private static final KEY_GAME_BOOSTER:Ljava/lang/String; = "gb_boosting"

.field private static final KEY_STYLUS_SCREEN_OFF_QUICK_NOTE:Ljava/lang/String; = "stylus_quick_note_screen_off"

.field private static final LONG_PRESS_TIME_OUT:I = 0x17c

.field private static final NEED_LASER_KEY_APP:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final NOTE_PACKAGE_NAME:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final POWER_SUPPLY_PEN_HALL3_EVENT:Ljava/lang/String; = "POWER_SUPPLY_PEN_HALL3"

.field private static final POWER_SUPPLY_PEN_HALL4_EVENT:Ljava/lang/String; = "POWER_SUPPLY_PEN_HALL4"

.field public static final SCENE_APP:Ljava/lang/String; = "app"

.field public static final SCENE_HOME:Ljava/lang/String; = "home"

.field public static final SCENE_KEYGUARD:Ljava/lang/String; = "keyguard"

.field public static final SCENE_OFF_SCREEN:Ljava/lang/String; = "off_screen"

.field private static final STYLUS_HALL_STATUS:Ljava/lang/String; = "stylus_hall_status"

.field private static final STYLUS_PAGE_KEY_BW_CONFIG:Ljava/lang/String; = "STYLUS_PAGE_KEY_BLACK_WHITE_LIST"

.field private static final STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT:Z

.field private static final SWIPE_HEIGHT_DOWN_PERCENTAGE:F = 0.25f

.field private static final SWIPE_HEIGHT_UP_PERCENTAGE:F = 0.75f

.field private static final SWIPE_WIDTH_PERCENTAGE:F = 0.65f

.field private static final SYSTEM_UI_PACKAGE_NAME:Ljava/lang/String; = "com.android.systemui"

.field private static final TAG:Ljava/lang/String; = "MiuiStylusShortcutManager"

.field private static volatile sInstance:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;


# instance fields
.field private final mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

.field private final mContext:Landroid/content/Context;

.field private mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

.field private mHallStatus:I

.field private final mHandler:Landroid/os/Handler;

.field private final mInputSettingsConnection:Lcom/miui/server/input/MiuiInputSettingsConnection;

.field private mIsGameMode:Z

.field private mIsRequestMaskShow:Z

.field private mIsScreenOffQuickNoteOn:Z

.field private mIsScreenOn:Z

.field private mIsUserSetupComplete:Z

.field private mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

.field private mLidOpen:Z

.field private final mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private mMiuiSettingsObserver:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;

.field private final mMiuiStylusDeviceListener:Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;

.field private mNeedDispatchLaserKey:Z

.field private volatile mQuickNoteKeyFunctionTriggered:Z

.field private mScene:Ljava/lang/String;

.field private volatile mScreenShotKeyFunctionTriggered:Z

.field private final mStylusHallObserver:Landroid/os/UEventObserver;

.field private mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;


# direct methods
.method public static synthetic $r8$lambda$2krTiFNYdR_q2Ls00Qp_2nzzjoI(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->previous()V

    return-void
.end method

.method public static synthetic $r8$lambda$RQkSk2eh5hHaC6CqamncJv8B4c8(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->next()V

    return-void
.end method

.method public static synthetic $r8$lambda$jw0tF3ExyitgQREokA951TRxRr0(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->lambda$new$0(Landroid/os/Message;)V

    return-void
.end method

.method public static synthetic $r8$lambda$r2EYqsjNQKcPKPwRP4WRajCH5Sk(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->lambda$initStylusPageKeyConfig$1()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmIsGameMode(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fputmHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;I)V
    .locals 0

    iput p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmQuickNoteKeyFunctionTriggered(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmScreenShotKeyFunctionTriggered(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$maddStylusMaskWindow(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->addStylusMaskWindow()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mputHallSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->putHallSettings(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveStylusMaskWindow(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->removeStylusMaskWindow(Z)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msetHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->setHallStatus()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtriggerNoteWhenScreenOff(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->triggerNoteWhenScreenOff()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mtriggerScreenShotKeyFunction(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->triggerScreenShotKeyFunction()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateCloudConfig(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateCloudConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateGameModeSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateGameModeSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateStylusScreenOffQuickNoteSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateStylusScreenOffQuickNoteSettings()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 52
    nop

    .line 53
    const-string v0, "com.miui.notes"

    const-string v1, "com.miui.pen.demo"

    const-string v2, "com.miui.creation"

    invoke-static {v0, v1, v2}, Ljava/util/Set;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->NOTE_PACKAGE_NAME:Ljava/util/Set;

    .line 62
    const-string v0, "com.android.camera"

    invoke-static {v0}, Ljava/util/Set;->of(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->NEED_LASER_KEY_APP:Ljava/util/Set;

    .line 101
    nop

    .line 102
    const-string v0, "persist.sys.quick.note.enable.default"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT:Z

    .line 101
    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z

    .line 149
    iput v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I

    .line 152
    new-instance v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    iput-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusHallObserver:Landroid/os/UEventObserver;

    .line 172
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    .line 173
    new-instance v3, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    .line 174
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetUp()Z

    move-result v3

    iput-boolean v3, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsUserSetupComplete:Z

    .line 175
    new-instance v3, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;

    invoke-direct {v3, v2}, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mMiuiStylusDeviceListener:Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;

    .line 176
    invoke-static {}, Lcom/miui/server/input/MiuiInputSettingsConnection;->getInstance()Lcom/miui/server/input/MiuiInputSettingsConnection;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mInputSettingsConnection:Lcom/miui/server/input/MiuiInputSettingsConnection;

    .line 177
    new-instance v3, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda0;

    invoke-direct {v3, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 178
    invoke-virtual {v2, v0, v3}, Lcom/miui/server/input/MiuiInputSettingsConnection;->registerCallbackListener(ILjava/util/function/Consumer;)V

    .line 180
    invoke-static {}, Landroid/app/ActivityTaskManager;->getService()Landroid/app/IActivityTaskManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/wm/ActivityTaskManagerService;

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    .line 181
    const-string v0, "POWER_SUPPLY_PEN_HALL3"

    invoke-virtual {v1, v0}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 182
    const-string v0, "POWER_SUPPLY_PEN_HALL4"

    invoke-virtual {v1, v0}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 183
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 184
    return-void
.end method

.method private addStylusMaskWindow()V
    .locals 4

    .line 500
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    const-string v1, "MiuiStylusShortcutManager"

    if-nez v0, :cond_0

    .line 501
    const-string v0, "Can\'t add stylus mask window because screen is not on"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    return-void

    .line 505
    :cond_0
    const-string v0, "app"

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScene:Ljava/lang/String;

    .line 506
    const/4 v0, 0x0

    .line 507
    .local v0, "owningPackage":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v2, :cond_1

    .line 508
    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    .line 510
    :cond_1
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    const-class v3, Landroid/app/KeyguardManager;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 511
    const-string v2, "com.android.systemui"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 512
    const-string v2, "keyguard"

    iput-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScene:Ljava/lang/String;

    goto :goto_0

    .line 513
    :cond_2
    const-string v2, "com.miui.home"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 514
    const-string v2, "home"

    iput-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScene:Ljava/lang/String;

    .line 517
    :cond_3
    :goto_0
    const-string v2, "request add stylus mask"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mInputSettingsConnection:Lcom/miui/server/input/MiuiInputSettingsConnection;

    .line 519
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(I)V

    .line 520
    iput-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z

    .line 521
    return-void
.end method

.method private fadeLaserAndResetPosition()V
    .locals 2

    .line 443
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    if-nez v0, :cond_0

    .line 444
    return-void

    .line 446
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fade(I)V

    .line 447
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPosition()V

    .line 448
    return-void
.end method

.method private getActivityName()Ljava/lang/String;
    .locals 6

    .line 258
    const-string v0, ""

    .line 259
    .local v0, "activityName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-nez v1, :cond_0

    .line 260
    return-object v0

    .line 262
    :cond_0
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    .line 263
    const-string v3, "getAttrs"

    invoke-static {v1, v3, v2}, Lcom/android/server/input/ReflectionUtils;->callPrivateMethod(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager$LayoutParams;

    .line 264
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    if-nez v1, :cond_1

    .line 265
    return-object v0

    .line 267
    :cond_1
    invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 268
    .local v2, "className":Ljava/lang/String;
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 269
    .local v3, "index":I
    if-ltz v3, :cond_2

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 270
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 272
    :cond_2
    return-object v0
.end method

.method public static getInstance()Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;
    .locals 2

    .line 187
    sget-object v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->sInstance:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-nez v0, :cond_1

    .line 188
    const-class v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    monitor-enter v0

    .line 189
    :try_start_0
    sget-object v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->sInstance:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    if-nez v1, :cond_0

    .line 190
    new-instance v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-direct {v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;-><init>()V

    sput-object v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->sInstance:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    .line 192
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 194
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->sInstance:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    return-object v0
.end method

.method private getWindowSwipeInfo()[I
    .locals 5

    .line 370
    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 371
    .local v0, "swipeInfo":[I
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 372
    .local v1, "heightPixels":I
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 373
    .local v2, "widthPixels":I
    int-to-float v3, v2

    const v4, 0x3f266666    # 0.65f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x0

    aput v3, v0, v4

    .line 374
    int-to-float v3, v1

    const/high16 v4, 0x3e800000    # 0.25f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x1

    aput v3, v0, v4

    .line 375
    int-to-float v3, v1

    const/high16 v4, 0x3f400000    # 0.75f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/4 v4, 0x2

    aput v3, v0, v4

    .line 376
    return-object v0
.end method

.method private initStylusPageKeyConfig()V
    .locals 2

    .line 205
    new-instance v0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    .line 206
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 208
    return-void
.end method

.method private interceptKeyByRemoteControl(Landroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .line 316
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    const/16 v1, 0x5c

    const/16 v2, 0x5d

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 317
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 318
    .local v0, "keyCode":I
    const-string v3, ""

    if-ne v0, v2, :cond_1

    .line 319
    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v4

    .line 320
    invoke-static {v0}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v5

    .line 321
    iget-object v6, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v6, :cond_0

    invoke-interface {v6}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    .line 319
    :cond_0
    invoke-virtual {v4, v5, v3}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyPress(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 322
    :cond_1
    if-ne v0, v1, :cond_3

    .line 323
    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v4

    .line 324
    invoke-static {v0}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v5

    .line 325
    iget-object v6, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v6, :cond_2

    invoke-interface {v6}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v3

    .line 323
    :cond_2
    invoke-virtual {v4, v5, v3}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyPress(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    .end local v0    # "keyCode":I
    :cond_3
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 329
    .local v0, "device":Landroid/view/InputDevice;
    const/4 v3, 0x0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v4

    const/4 v5, 0x3

    if-ge v4, v5, :cond_4

    goto :goto_2

    .line 332
    :cond_4
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isSupportStylusRemoteControl()Z

    move-result v4

    if-nez v4, :cond_5

    .line 333
    return v3

    .line 336
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_7

    .line 337
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 338
    .local v3, "keyCode":I
    if-ne v3, v2, :cond_6

    .line 339
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda1;

    invoke-direct {v2, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 340
    :cond_6
    if-ne v3, v1, :cond_7

    .line 341
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda2;

    invoke-direct {v2, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 344
    .end local v3    # "keyCode":I
    :cond_7
    :goto_1
    const/4 v1, 0x1

    return v1

    .line 330
    :cond_8
    :goto_2
    return v3
.end method

.method private interceptLaserKey(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 426
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 427
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->NEED_LASER_KEY_APP:Ljava/util/Set;

    .line 429
    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mNeedDispatchLaserKey:Z

    .line 431
    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mNeedDispatchLaserKey:Z

    if-eqz v0, :cond_2

    .line 432
    return v1

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    if-nez v0, :cond_3

    .line 436
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 437
    invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerInternal;->obtainLaserPointerController()Lcom/miui/server/input/stylus/laser/PointerControllerInterface;

    move-result-object v0

    check-cast v0, Lcom/miui/server/input/stylus/laser/LaserPointerController;

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    .line 439
    :cond_3
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLaserPointerController:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->interceptLaserKey(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method private interceptQuickNoteKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 451
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 452
    .local v0, "isUp":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 454
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 456
    invoke-direct {p0, v2}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->removeMessageIfHas(I)V

    .line 457
    iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z

    return v1

    .line 459
    :cond_1
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z

    .line 460
    iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    if-eqz v1, :cond_2

    .line 461
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptQuickNoteKeyDownWhenScreenOn()Z

    move-result v1

    return v1

    .line 463
    :cond_2
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptQuickNoteKeyDownWhenScreenOff()Z

    move-result v1

    return v1
.end method

.method private interceptQuickNoteKeyDownWhenScreenOff()Z
    .locals 1

    .line 467
    const/4 v0, 0x0

    return v0
.end method

.method private interceptQuickNoteKeyDownWhenScreenOn()Z
    .locals 5

    .line 471
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    const-string v1, "MiuiStylusShortcutManager"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 473
    const-string v0, "focus window is null"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    return v2

    .line 476
    :cond_0
    sget-object v3, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->NOTE_PACKAGE_NAME:Ljava/util/Set;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 478
    const-string v0, "focus window is notes, so long press page down disable"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    return v2

    .line 481
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v3, 0x17c

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 482
    return v2
.end method

.method private interceptScreenShotKey(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 486
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    move v0, v2

    .line 487
    .local v0, "isUp":Z
    const/4 v2, 0x2

    if-eqz v0, :cond_1

    .line 489
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 491
    iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z

    return v1

    .line 493
    :cond_1
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z

    .line 494
    iget-object v3, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x17c

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 496
    return v1
.end method

.method private static isLaserKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p0, "event"    # Landroid/view/KeyEvent;

    .line 650
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 651
    .local v0, "keyCode":I
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    .line 652
    .local v1, "device":Landroid/view/InputDevice;
    const/16 v2, 0xc1

    if-ne v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 653
    invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 652
    :goto_0
    return v2
.end method

.method private static isQuickNoteKey(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p0, "event"    # Landroid/view/KeyEvent;

    .line 636
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 637
    .local v0, "keyCode":I
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    .line 638
    .local v1, "device":Landroid/view/InputDevice;
    const/16 v2, 0x5d

    if-ne v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 639
    invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 638
    :goto_0
    return v2
.end method

.method private static isScreenShotKey(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p0, "event"    # Landroid/view/KeyEvent;

    .line 643
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 644
    .local v0, "keyCode":I
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v1

    .line 645
    .local v1, "device":Landroid/view/InputDevice;
    const/16 v2, 0x5c

    if-ne v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 646
    invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 645
    :goto_0
    return v2
.end method

.method private isSplitOrFreeFormMode()Z
    .locals 6

    .line 380
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z

    move-result v0

    const-string v1, "MiuiStylusShortcutManager"

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "current activity is split screen windowing mode\uff0c not support short video remote control"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    return v2

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mAtms:Lcom/android/server/wm/ActivityTaskManagerService;

    invoke-virtual {v0, v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getTasks(I)Ljava/util/List;

    move-result-object v0

    .line 386
    .local v0, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    invoke-virtual {v4}, Landroid/app/ActivityManager$RunningTaskInfo;->getWindowingMode()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    goto :goto_0

    .line 392
    :cond_1
    return v3

    .line 388
    :cond_2
    :goto_0
    const-string v3, "current activity is free form windowing mode\uff0c not support short video remote control"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    return v2
.end method

.method private isSupportStylusRemoteControl()Z
    .locals 6

    .line 235
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->isEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    return v1

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    invoke-interface {v0}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    .line 240
    .local v0, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getAppWhiteSet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 241
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getActivityBlackSet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    const-string v4, "MiuiStylusShortcutManager"

    if-eqz v2, :cond_1

    .line 242
    const-string v1, "Current activity supports short video remote control"

    invoke-static {v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    return v3

    .line 245
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getActivityName()Ljava/lang/String;

    move-result-object v2

    .line 246
    .local v2, "activityName":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    .line 247
    invoke-virtual {v5}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getActivityBlackSet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 248
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current activity "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " supports short video remote control"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    return v3

    .line 254
    .end local v0    # "name":Ljava/lang/String;
    .end local v2    # "activityName":Ljava/lang/String;
    :cond_2
    return v1
.end method

.method private isUserSetUp()Z
    .locals 4

    .line 592
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string/jumbo v2, "user_setup_complete"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3
.end method

.method private isUserSetupComplete()Z
    .locals 1

    .line 542
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsUserSetupComplete:Z

    if-nez v0, :cond_0

    .line 544
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetUp()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsUserSetupComplete:Z

    .line 545
    return v0

    .line 547
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private synthetic lambda$initStylusPageKeyConfig$1()V
    .locals 1

    .line 207
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->getDefaultStylusPageKeyConfig()Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    return-void
.end method

.method private synthetic lambda$new$0(Landroid/os/Message;)V
    .locals 0
    .param p1, "value"    # Landroid/os/Message;

    .line 179
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->triggerQuickNoteKeyFunction()V

    return-void
.end method

.method private launchNote()V
    .locals 5

    .line 606
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 607
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "scene"

    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScene:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v1

    const-string/jumbo v2, "stylus"

    const/4 v3, 0x1

    const-string v4, "note"

    invoke-virtual {v1, v4, v2, v0, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 610
    return-void
.end method

.method private next()V
    .locals 12

    .line 359
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isSplitOrFreeFormMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    return-void

    .line 362
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getWindowSwipeInfo()[I

    move-result-object v0

    .line 363
    .local v0, "swipeInfo":[I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "swipePositionInfo: downX: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " downY: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x2

    aget v4, v0, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " upX: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v4, v0, v2

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " upY: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x1

    aget v5, v0, v4

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "MiuiStylusShortcutManager"

    invoke-static {v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-static {}, Lcom/miui/server/input/util/MiuiInputShellCommand;->getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;

    move-result-object v6

    aget v7, v0, v2

    aget v8, v0, v3

    aget v9, v0, v2

    aget v10, v0, v4

    const/16 v11, 0x3c

    invoke-virtual/range {v6 .. v11}, Lcom/miui/server/input/util/MiuiInputShellCommand;->swipeGenerator(IIIII)Z

    .line 367
    return-void
.end method

.method private previous()V
    .locals 12

    .line 348
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isSplitOrFreeFormMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    return-void

    .line 351
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getWindowSwipeInfo()[I

    move-result-object v0

    .line 352
    .local v0, "swipeInfo":[I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "swipePositionInfo: downX: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget v3, v0, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " downY: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x1

    aget v4, v0, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " upX: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v4, v0, v2

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " upY: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x2

    aget v5, v0, v4

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "MiuiStylusShortcutManager"

    invoke-static {v5, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    invoke-static {}, Lcom/miui/server/input/util/MiuiInputShellCommand;->getInstance()Lcom/miui/server/input/util/MiuiInputShellCommand;

    move-result-object v6

    aget v7, v0, v2

    aget v8, v0, v3

    aget v9, v0, v2

    aget v10, v0, v4

    const/16 v11, 0x3c

    invoke-virtual/range {v6 .. v11}, Lcom/miui/server/input/util/MiuiInputShellCommand;->swipeGenerator(IIIII)Z

    .line 356
    return-void
.end method

.method private putHallSettings(I)V
    .locals 2
    .param p1, "hall"    # I

    .line 691
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "stylus_hall_status"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 693
    return-void
.end method

.method private removeMessageIfHas(I)V
    .locals 1
    .param p1, "what"    # I

    .line 536
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 539
    :cond_0
    return-void
.end method

.method private removeStylusMaskWindow(Z)V
    .locals 2
    .param p1, "animation"    # Z

    .line 525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request remove stylus mask, mIsRequestMaskShow = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiStylusShortcutManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z

    if-nez v0, :cond_0

    .line 528
    return-void

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mInputSettingsConnection:Lcom/miui/server/input/MiuiInputSettingsConnection;

    .line 531
    nop

    .line 530
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(II)V

    .line 532
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z

    .line 533
    return-void
.end method

.method private setHallStatus()V
    .locals 2

    .line 682
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Stylus hall event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I

    if-nez v1, :cond_0

    const-string v1, "close"

    goto :goto_0

    :cond_0
    const-string v1, "far"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiStylusShortcutManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->removeMessageIfHas(I)V

    .line 684
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 685
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setWhat(I)Landroid/os/Message;

    .line 686
    iget v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I

    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 687
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 688
    return-void
.end method

.method private takePartialScreenshot()V
    .locals 5

    .line 597
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    if-nez v0, :cond_0

    .line 598
    const-string v0, "MiuiStylusShortcutManager"

    const-string v1, "Can\'t take screenshot because screen is not on"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    return-void

    .line 601
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string/jumbo v3, "stylus_partial_screenshot"

    const-string v4, "long_press_page_up_key"

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 603
    return-void
.end method

.method private triggerNoteWhenScreenOff()V
    .locals 1

    .line 630
    const-string v0, "off_screen"

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScene:Ljava/lang/String;

    .line 631
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->launchNote()V

    .line 632
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusShortHandTrigger()V

    .line 633
    return-void
.end method

.method private triggerQuickNoteKeyFunction()V
    .locals 4

    .line 614
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->launchNote()V

    .line 615
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v0

    .line 616
    const/16 v1, 0x5d

    invoke-static {v1}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v1

    .line 617
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 615
    :goto_0
    const-string v3, "note"

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyLongPress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    return-void
.end method

.method private triggerScreenShotKeyFunction()V
    .locals 4

    .line 622
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->takePartialScreenshot()V

    .line 623
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v0

    .line 625
    const/16 v1, 0x5c

    invoke-static {v1}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v1

    .line 626
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 623
    :goto_0
    const-string/jumbo v3, "stylus_partial_screenshot"

    invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyLongPress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    return-void
.end method

.method private updateCloudConfig()V
    .locals 5

    .line 211
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    .line 212
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "STYLUS_PAGE_KEY_BLACK_WHITE_LIST"

    invoke-static {v0, v3, v1, v1, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 214
    .local v0, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    const-string v1, "MiuiStylusShortcutManager"

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 219
    :cond_0
    nop

    .line 220
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->parseJsonToStylusPageKeyConfig(Lorg/json/JSONObject;)Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    move-result-object v2

    .line 221
    .local v2, "cloudConfig":Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I

    move-result v3

    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v4}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 223
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Do not use cloud config, because cloud config version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 224
    invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < current config version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    .line 225
    invoke-virtual {v4}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 223
    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    return-void

    .line 229
    :cond_1
    iput-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    .line 230
    const-string v3, "Cloud config has updated, use cloud config"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 216
    .end local v2    # "cloudConfig":Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    :cond_2
    :goto_0
    const-string v2, "Cloud config is null, use default local config"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :goto_1
    return-void
.end method

.method private updateGameModeSettings()V
    .locals 3

    .line 677
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gb_boosting"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    iput-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z

    .line 679
    return-void
.end method

.method private updateStylusScreenOffQuickNoteSettings()V
    .locals 4

    .line 669
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mContext:Landroid/content/Context;

    .line 670
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT:Z

    .line 669
    const-string/jumbo v2, "stylus_quick_note_screen_off"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/MiuiSettings$System;->getBooleanForUser(Landroid/content/ContentResolver;Ljava/lang/String;ZI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z

    .line 673
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update stylus screen off quick note settings = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiStylusShortcutManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 705
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 706
    const-string v0, "MiuiStylusShortcutManager"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 707
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 708
    const-string v0, "mScene="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 709
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScene:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 710
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 711
    const-string v0, "mLidOpen="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 712
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 713
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 714
    const-string v0, "mStylusPageKeyConfig"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 715
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 716
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 717
    const-string v0, "mEnable = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 718
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->isEnable()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 719
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 720
    const-string v0, "mAppWhiteSet.size = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 721
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getAppWhiteSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 722
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 723
    const-string v0, "mActivityBlackSet.size = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 724
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mStylusPageKeyConfig:Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getActivityBlackSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 725
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 726
    const-string v0, "StylusScreenOffQuickNoteDefault = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 727
    sget-boolean v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 728
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 729
    const-string v0, "IsSupportScreenOffQuickNote = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 730
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportOffScreenQuickNote()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 731
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 732
    const-string v0, "mIsScreenOffQuickNoteOn = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 733
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 734
    return-void
.end method

.method public getDelayTime(Landroid/view/KeyEvent;)J
    .locals 9
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .line 281
    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isScreenShotKey(Landroid/view/KeyEvent;)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isQuickNoteKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    return-wide v1

    .line 285
    :cond_0
    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isQuickNoteKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z

    .line 286
    .local v0, "isTriggered":Z
    :goto_0
    const-wide/16 v3, -0x1

    if-eqz v0, :cond_3

    .line 287
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    .line 289
    .local v1, "repeatCount":I
    if-nez v1, :cond_2

    .line 290
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Shortcut triggered, so intercept "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 291
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    invoke-static {v5}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 290
    const-string v5, "MiuiStylusShortcutManager"

    invoke-static {v5, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_2
    return-wide v3

    .line 295
    .end local v1    # "repeatCount":I
    :cond_3
    iget-object v5, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    .line 296
    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-nez v5, :cond_5

    .line 298
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptKeyByRemoteControl(Landroid/view/KeyEvent;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 299
    return-wide v3

    .line 302
    :cond_4
    return-wide v1

    .line 304
    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 306
    .local v3, "now":J
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v5

    const-wide/16 v7, 0x17c

    add-long/2addr v5, v7

    const-wide/16 v7, 0x14

    add-long/2addr v5, v7

    .line 307
    .local v5, "timeoutTime":J
    cmp-long v7, v3, v5

    if-gez v7, :cond_6

    .line 309
    sub-long v1, v5, v3

    return-wide v1

    .line 311
    :cond_6
    return-wide v1
.end method

.method public needInterceptBeforeDispatching(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 551
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetupComplete()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z

    if-nez v0, :cond_1

    .line 552
    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isScreenShotKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isQuickNoteKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 551
    :goto_0
    return v0
.end method

.method public notifyLidSwitchChanged(Z)V
    .locals 2
    .param p1, "lidOpen"    # Z

    .line 657
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z

    .line 658
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notify lidSwitch change to : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiStylusShortcutManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    return-void
.end method

.method public onDefaultDisplayFocusChangedLw(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
    .locals 0
    .param p1, "newFocus"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 565
    iput-object p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mFocusedWindow:Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 566
    return-void
.end method

.method public onSystemBooted()V
    .locals 2

    .line 198
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->initStylusPageKeyConfig()V

    .line 199
    new-instance v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;

    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mMiuiSettingsObserver:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;

    .line 200
    invoke-virtual {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->observe()V

    .line 201
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 1
    .param p1, "newUserId"    # I

    .line 662
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateGameModeSettings()V

    .line 663
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportOffScreenQuickNote()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateStylusScreenOffQuickNoteSettings()V

    .line 666
    :cond_0
    return-void
.end method

.method public parseInt(Ljava/lang/String;)I
    .locals 3
    .param p1, "argument"    # Ljava/lang/String;

    .line 697
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid integer argument "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiStylusShortcutManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    const/4 v1, -0x1

    return v1
.end method

.method public processMotionEventForQuickNote(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 569
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z

    const-string v1, "MiuiStylusShortcutManager"

    if-nez v0, :cond_0

    .line 570
    const-string v0, "Stylus screen off quick note function is off."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    return-void

    .line 573
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z

    if-nez v0, :cond_1

    goto :goto_1

    .line 578
    :cond_1
    const/16 v0, 0x4002

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->isFromSource(I)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 579
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    nop

    :goto_0
    move v0, v2

    .line 580
    .local v0, "eventFromStylus":Z
    if-nez v0, :cond_3

    .line 581
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Event not from stylus, event = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    return-void

    .line 584
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    if-eqz v1, :cond_4

    .line 585
    return-void

    .line 588
    :cond_4
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 589
    return-void

    .line 574
    .end local v0    # "eventFromStylus":Z
    :cond_5
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not support quick note for offscreen or lid not open state. screenOn = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", lidOpen = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    return-void
.end method

.method public shouldInterceptKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 402
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetupComplete()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 403
    return v1

    .line 405
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z

    if-eqz v0, :cond_1

    .line 406
    return v1

    .line 408
    :cond_1
    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isScreenShotKey(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 409
    .local v0, "isScreenShotKey":Z
    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isQuickNoteKey(Landroid/view/KeyEvent;)Z

    move-result v2

    .line 410
    .local v2, "isQuickNoteKey":Z
    if-nez v0, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_3

    .line 411
    invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->fadeLaserAndResetPosition()V

    .line 413
    :cond_3
    if-eqz v0, :cond_4

    .line 414
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptScreenShotKey(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1

    .line 416
    :cond_4
    if-eqz v2, :cond_5

    .line 417
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptQuickNoteKey(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1

    .line 419
    :cond_5
    invoke-static {p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isLaserKey(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 420
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptLaserKey(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1

    .line 422
    :cond_6
    return v1
.end method

.method public updateScreenState(Z)V
    .locals 2
    .param p1, "screenOn"    # Z

    .line 556
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z

    .line 557
    if-eqz p1, :cond_0

    .line 558
    return-void

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 562
    return-void
.end method
