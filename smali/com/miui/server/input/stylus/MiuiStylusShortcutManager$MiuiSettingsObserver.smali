.class Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiStylusShortcutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;


# direct methods
.method public static synthetic $r8$lambda$w9kMM0A6URLpStvwNQDoduK_zRM(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mupdateCloudConfig(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    return-void
.end method

.method constructor <init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 797
    iput-object p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    .line 798
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 799
    return-void
.end method


# virtual methods
.method observe()V
    .locals 4

    .line 802
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mupdateGameModeSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 803
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fgetmHandler(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    new-instance v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;

    invoke-direct {v2, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 804
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 805
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 807
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 809
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportOffScreenQuickNote()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 810
    nop

    .line 811
    const-string/jumbo v1, "stylus_quick_note_screen_off"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 810
    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 813
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mupdateStylusScreenOffQuickNoteSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 815
    :cond_0
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 819
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 820
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mupdateGameModeSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 821
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "game_booster changed, isGameMode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fgetmIsGameMode(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiStylusShortcutManager"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 822
    :cond_0
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 823
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mupdateCloudConfig(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    goto :goto_0

    .line 824
    :cond_1
    const-string/jumbo v0, "stylus_quick_note_screen_off"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mupdateStylusScreenOffQuickNoteSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 827
    :cond_2
    :goto_0
    return-void
.end method
