.class public Lcom/miui/server/input/stylus/StylusPageKeyConfig;
.super Ljava/lang/Object;
.source "StylusPageKeyConfig.java"


# instance fields
.field private mActivityBlackSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppWhiteSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEnable:Z

.field private mVersion:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z

    .line 18
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mAppWhiteSet:Ljava/util/Set;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mActivityBlackSet:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public getActivityBlackSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mActivityBlackSet:Ljava/util/Set;

    return-object v0
.end method

.method public getAppWhiteSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mAppWhiteSet:Ljava/util/Set;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I

    return v0
.end method

.method public isEnable()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z

    return v0
.end method

.method public setActivityBlackSet(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 53
    .local p1, "activityBlackSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mActivityBlackSet:Ljava/util/Set;

    .line 54
    return-void
.end method

.method public setAppWhiteSet(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 45
    .local p1, "appWhiteSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mAppWhiteSet:Ljava/util/Set;

    .line 46
    return-void
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 37
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z

    .line 38
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .line 29
    iput p1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I

    .line 30
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StylusPageKeyConfig{version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appWhiteSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mAppWhiteSet:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activityBlackSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mActivityBlackSet:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
