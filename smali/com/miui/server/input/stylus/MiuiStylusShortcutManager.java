public class com.miui.server.input.stylus.MiuiStylusShortcutManager {
	 /* .source "MiuiStylusShortcutManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;, */
	 /* Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer HALL_CLOSE;
private static final Integer HALL_FAR;
private static final java.lang.String HOME_PACKAGE_NAME;
private static final java.lang.String KEY_GAME_BOOSTER;
private static final java.lang.String KEY_STYLUS_SCREEN_OFF_QUICK_NOTE;
private static final Integer LONG_PRESS_TIME_OUT;
private static final java.util.Set NEED_LASER_KEY_APP;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.Set NOTE_PACKAGE_NAME;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.lang.String POWER_SUPPLY_PEN_HALL3_EVENT;
private static final java.lang.String POWER_SUPPLY_PEN_HALL4_EVENT;
public static final java.lang.String SCENE_APP;
public static final java.lang.String SCENE_HOME;
public static final java.lang.String SCENE_KEYGUARD;
public static final java.lang.String SCENE_OFF_SCREEN;
private static final java.lang.String STYLUS_HALL_STATUS;
private static final java.lang.String STYLUS_PAGE_KEY_BW_CONFIG;
private static final Boolean STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT;
private static final Float SWIPE_HEIGHT_DOWN_PERCENTAGE;
private static final Float SWIPE_HEIGHT_UP_PERCENTAGE;
private static final Float SWIPE_WIDTH_PERCENTAGE;
private static final java.lang.String SYSTEM_UI_PACKAGE_NAME;
private static final java.lang.String TAG;
private static volatile com.miui.server.input.stylus.MiuiStylusShortcutManager sInstance;
/* # instance fields */
private final com.android.server.wm.ActivityTaskManagerService mAtms;
private final android.content.Context mContext;
private com.android.server.policy.WindowManagerPolicy$WindowState mFocusedWindow;
private Integer mHallStatus;
private final android.os.Handler mHandler;
private final com.miui.server.input.MiuiInputSettingsConnection mInputSettingsConnection;
private Boolean mIsGameMode;
private Boolean mIsRequestMaskShow;
private Boolean mIsScreenOffQuickNoteOn;
private Boolean mIsScreenOn;
private Boolean mIsUserSetupComplete;
private com.miui.server.input.stylus.laser.LaserPointerController mLaserPointerController;
private Boolean mLidOpen;
private final com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private com.miui.server.input.stylus.MiuiStylusShortcutManager$MiuiSettingsObserver mMiuiSettingsObserver;
private final com.miui.server.input.stylus.MiuiStylusDeviceListener mMiuiStylusDeviceListener;
private Boolean mNeedDispatchLaserKey;
private volatile Boolean mQuickNoteKeyFunctionTriggered;
private java.lang.String mScene;
private volatile Boolean mScreenShotKeyFunctionTriggered;
private final android.os.UEventObserver mStylusHallObserver;
private com.miui.server.input.stylus.StylusPageKeyConfig mStylusPageKeyConfig;
/* # direct methods */
public static void $r8$lambda$2krTiFNYdR_q2Ls00Qp_2nzzjoI ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->previous()V */
return;
} // .end method
public static void $r8$lambda$RQkSk2eh5hHaC6CqamncJv8B4c8 ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->next()V */
return;
} // .end method
public static void $r8$lambda$jw0tF3ExyitgQREokA951TRxRr0 ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0, android.os.Message p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->lambda$new$0(Landroid/os/Message;)V */
return;
} // .end method
public static void $r8$lambda$r2EYqsjNQKcPKPwRP4WRajCH5Sk ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->lambda$initStylusPageKeyConfig$1()V */
return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mContext;
} // .end method
static Integer -$$Nest$fgetmHallStatus ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget p0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I */
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static Boolean -$$Nest$fgetmIsGameMode ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* iget-boolean p0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z */
} // .end method
static void -$$Nest$fputmHallStatus ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I */
return;
} // .end method
static void -$$Nest$fputmQuickNoteKeyFunctionTriggered ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z */
return;
} // .end method
static void -$$Nest$fputmScreenShotKeyFunctionTriggered ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z */
return;
} // .end method
static void -$$Nest$maddStylusMaskWindow ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->addStylusMaskWindow()V */
return;
} // .end method
static void -$$Nest$mputHallSettings ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0, Integer p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->putHallSettings(I)V */
return;
} // .end method
static void -$$Nest$mremoveStylusMaskWindow ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->removeStylusMaskWindow(Z)V */
return;
} // .end method
static void -$$Nest$msetHallStatus ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->setHallStatus()V */
return;
} // .end method
static void -$$Nest$mtriggerNoteWhenScreenOff ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->triggerNoteWhenScreenOff()V */
return;
} // .end method
static void -$$Nest$mtriggerScreenShotKeyFunction ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->triggerScreenShotKeyFunction()V */
return;
} // .end method
static void -$$Nest$mupdateCloudConfig ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateCloudConfig()V */
return;
} // .end method
static void -$$Nest$mupdateGameModeSettings ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateGameModeSettings()V */
return;
} // .end method
static void -$$Nest$mupdateStylusScreenOffQuickNoteSettings ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateStylusScreenOffQuickNoteSettings()V */
return;
} // .end method
static com.miui.server.input.stylus.MiuiStylusShortcutManager ( ) {
/* .locals 3 */
/* .line 52 */
/* nop */
/* .line 53 */
final String v0 = "com.miui.notes"; // const-string v0, "com.miui.notes"
final String v1 = "com.miui.pen.demo"; // const-string v1, "com.miui.pen.demo"
final String v2 = "com.miui.creation"; // const-string v2, "com.miui.creation"
java.util.Set .of ( v0,v1,v2 );
/* .line 62 */
final String v0 = "com.android.camera"; // const-string v0, "com.android.camera"
java.util.Set .of ( v0 );
/* .line 101 */
/* nop */
/* .line 102 */
final String v0 = "persist.sys.quick.note.enable.default"; // const-string v0, "persist.sys.quick.note.enable.default"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.input.stylus.MiuiStylusShortcutManager.STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT = (v0!= 0);
/* .line 101 */
return;
} // .end method
private com.miui.server.input.stylus.MiuiStylusShortcutManager ( ) {
/* .locals 5 */
/* .line 171 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 139 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z */
/* .line 149 */
/* iput v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I */
/* .line 152 */
/* new-instance v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V */
this.mStylusHallObserver = v1;
/* .line 172 */
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v2 ).getSystemContext ( ); // invoke-virtual {v2}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
this.mContext = v2;
/* .line 173 */
/* new-instance v3, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H; */
com.android.server.input.MiuiInputThread .getHandler ( );
(( android.os.Handler ) v4 ).getLooper ( ); // invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v3, p0, v4}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Looper;)V */
this.mHandler = v3;
/* .line 174 */
v3 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetUp()Z */
/* iput-boolean v3, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsUserSetupComplete:Z */
/* .line 175 */
/* new-instance v3, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener; */
/* invoke-direct {v3, v2}, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;-><init>(Landroid/content/Context;)V */
this.mMiuiStylusDeviceListener = v3;
/* .line 176 */
com.miui.server.input.MiuiInputSettingsConnection .getInstance ( );
this.mInputSettingsConnection = v2;
/* .line 177 */
/* new-instance v3, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v3, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V */
/* .line 178 */
(( com.miui.server.input.MiuiInputSettingsConnection ) v2 ).registerCallbackListener ( v0, v3 ); // invoke-virtual {v2, v0, v3}, Lcom/miui/server/input/MiuiInputSettingsConnection;->registerCallbackListener(ILjava/util/function/Consumer;)V
/* .line 180 */
android.app.ActivityTaskManager .getService ( );
/* check-cast v0, Lcom/android/server/wm/ActivityTaskManagerService; */
this.mAtms = v0;
/* .line 181 */
final String v0 = "POWER_SUPPLY_PEN_HALL3"; // const-string v0, "POWER_SUPPLY_PEN_HALL3"
(( android.os.UEventObserver ) v1 ).startObserving ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 182 */
final String v0 = "POWER_SUPPLY_PEN_HALL4"; // const-string v0, "POWER_SUPPLY_PEN_HALL4"
(( android.os.UEventObserver ) v1 ).startObserving ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V
/* .line 183 */
/* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
com.android.server.LocalServices .getService ( v0 );
/* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
this.mMiuiInputManagerInternal = v0;
/* .line 184 */
return;
} // .end method
private void addStylusMaskWindow ( ) {
/* .locals 4 */
/* .line 500 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
/* if-nez v0, :cond_0 */
/* .line 501 */
final String v0 = "Can\'t add stylus mask window because screen is not on"; // const-string v0, "Can\'t add stylus mask window because screen is not on"
android.util.Slog .w ( v1,v0 );
/* .line 502 */
return;
/* .line 505 */
} // :cond_0
final String v0 = "app"; // const-string v0, "app"
this.mScene = v0;
/* .line 506 */
int v0 = 0; // const/4 v0, 0x0
/* .line 507 */
/* .local v0, "owningPackage":Ljava/lang/String; */
v2 = this.mFocusedWindow;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 508 */
/* .line 510 */
} // :cond_1
v2 = this.mContext;
/* const-class v3, Landroid/app/KeyguardManager; */
(( android.content.Context ) v2 ).getSystemService ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
/* check-cast v2, Landroid/app/KeyguardManager; */
v2 = (( android.app.KeyguardManager ) v2 ).isKeyguardLocked ( ); // invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 511 */
final String v2 = "com.android.systemui"; // const-string v2, "com.android.systemui"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 512 */
final String v2 = "keyguard"; // const-string v2, "keyguard"
this.mScene = v2;
/* .line 513 */
} // :cond_2
final String v2 = "com.miui.home"; // const-string v2, "com.miui.home"
v2 = (( java.lang.String ) v2 ).equals ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 514 */
final String v2 = "home"; // const-string v2, "home"
this.mScene = v2;
/* .line 517 */
} // :cond_3
} // :goto_0
final String v2 = "request add stylus mask"; // const-string v2, "request add stylus mask"
android.util.Slog .i ( v1,v2 );
/* .line 518 */
v1 = this.mInputSettingsConnection;
/* .line 519 */
int v2 = 1; // const/4 v2, 0x1
(( com.miui.server.input.MiuiInputSettingsConnection ) v1 ).sendMessageToInputSettings ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(I)V
/* .line 520 */
/* iput-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z */
/* .line 521 */
return;
} // .end method
private void fadeLaserAndResetPosition ( ) {
/* .locals 2 */
/* .line 443 */
v0 = this.mLaserPointerController;
/* if-nez v0, :cond_0 */
/* .line 444 */
return;
/* .line 446 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).fade ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fade(I)V
/* .line 447 */
v0 = this.mLaserPointerController;
(( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).resetPosition ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPosition()V
/* .line 448 */
return;
} // .end method
private java.lang.String getActivityName ( ) {
/* .locals 6 */
/* .line 258 */
final String v0 = ""; // const-string v0, ""
/* .line 259 */
/* .local v0, "activityName":Ljava/lang/String; */
v1 = this.mFocusedWindow;
/* if-nez v1, :cond_0 */
/* .line 260 */
/* .line 262 */
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* new-array v2, v2, [Ljava/lang/Object; */
/* .line 263 */
final String v3 = "getAttrs"; // const-string v3, "getAttrs"
com.android.server.input.ReflectionUtils .callPrivateMethod ( v1,v3,v2 );
/* check-cast v1, Landroid/view/WindowManager$LayoutParams; */
/* .line 264 */
/* .local v1, "attrs":Landroid/view/WindowManager$LayoutParams; */
/* if-nez v1, :cond_1 */
/* .line 265 */
/* .line 267 */
} // :cond_1
(( android.view.WindowManager$LayoutParams ) v1 ).getTitle ( ); // invoke-virtual {v1}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;
/* .line 268 */
/* .local v2, "className":Ljava/lang/String; */
/* const/16 v3, 0x2f */
v3 = (( java.lang.String ) v2 ).lastIndexOf ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I
/* .line 269 */
/* .local v3, "index":I */
/* if-ltz v3, :cond_2 */
/* add-int/lit8 v4, v3, 0x1 */
v5 = (( java.lang.String ) v2 ).length ( ); // invoke-virtual {v2}, Ljava/lang/String;->length()I
/* if-ge v4, v5, :cond_2 */
/* .line 270 */
/* add-int/lit8 v4, v3, 0x1 */
(( java.lang.String ) v2 ).substring ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 272 */
} // :cond_2
} // .end method
public static com.miui.server.input.stylus.MiuiStylusShortcutManager getInstance ( ) {
/* .locals 2 */
/* .line 187 */
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 188 */
/* const-class v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
/* monitor-enter v0 */
/* .line 189 */
try { // :try_start_0
v1 = com.miui.server.input.stylus.MiuiStylusShortcutManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 190 */
/* new-instance v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
/* invoke-direct {v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;-><init>()V */
/* .line 192 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 194 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager.sInstance;
} // .end method
private getWindowSwipeInfo ( ) {
/* .locals 5 */
/* .line 370 */
int v0 = 3; // const/4 v0, 0x3
/* new-array v0, v0, [I */
/* .line 371 */
/* .local v0, "swipeInfo":[I */
v1 = this.mContext;
(( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v1 ).getDisplayMetrics ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I */
/* .line 372 */
/* .local v1, "heightPixels":I */
v2 = this.mContext;
(( android.content.Context ) v2 ).getResources ( ); // invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
(( android.content.res.Resources ) v2 ).getDisplayMetrics ( ); // invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
/* iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I */
/* .line 373 */
/* .local v2, "widthPixels":I */
/* int-to-float v3, v2 */
/* const v4, 0x3f266666 # 0.65f */
/* mul-float/2addr v3, v4 */
/* float-to-int v3, v3 */
int v4 = 0; // const/4 v4, 0x0
/* aput v3, v0, v4 */
/* .line 374 */
/* int-to-float v3, v1 */
/* const/high16 v4, 0x3e800000 # 0.25f */
/* mul-float/2addr v3, v4 */
/* float-to-int v3, v3 */
int v4 = 1; // const/4 v4, 0x1
/* aput v3, v0, v4 */
/* .line 375 */
/* int-to-float v3, v1 */
/* const/high16 v4, 0x3f400000 # 0.75f */
/* mul-float/2addr v3, v4 */
/* float-to-int v3, v3 */
int v4 = 2; // const/4 v4, 0x2
/* aput v3, v0, v4 */
/* .line 376 */
} // .end method
private void initStylusPageKeyConfig ( ) {
/* .locals 2 */
/* .line 205 */
/* new-instance v0, Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
/* invoke-direct {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;-><init>()V */
this.mStylusPageKeyConfig = v0;
/* .line 206 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 208 */
return;
} // .end method
private Boolean interceptKeyByRemoteControl ( android.view.KeyEvent p0 ) {
/* .locals 7 */
/* .param p1, "keyEvent" # Landroid/view/KeyEvent; */
/* .line 316 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
/* const/16 v1, 0x5c */
/* const/16 v2, 0x5d */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v0, :cond_3 */
/* .line 317 */
v0 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 318 */
/* .local v0, "keyCode":I */
final String v3 = ""; // const-string v3, ""
/* if-ne v0, v2, :cond_1 */
/* .line 319 */
v4 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v4 );
/* .line 320 */
android.view.KeyEvent .keyCodeToString ( v0 );
/* .line 321 */
v6 = this.mFocusedWindow;
if ( v6 != null) { // if-eqz v6, :cond_0
/* .line 319 */
} // :cond_0
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v4 ).trackStylusKeyPress ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyPress(Ljava/lang/String;Ljava/lang/String;)V
/* .line 322 */
} // :cond_1
/* if-ne v0, v1, :cond_3 */
/* .line 323 */
v4 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v4 );
/* .line 324 */
android.view.KeyEvent .keyCodeToString ( v0 );
/* .line 325 */
v6 = this.mFocusedWindow;
if ( v6 != null) { // if-eqz v6, :cond_2
/* .line 323 */
} // :cond_2
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v4 ).trackStylusKeyPress ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyPress(Ljava/lang/String;Ljava/lang/String;)V
/* .line 328 */
} // .end local v0 # "keyCode":I
} // :cond_3
} // :goto_0
(( android.view.KeyEvent ) p1 ).getDevice ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 329 */
/* .local v0, "device":Landroid/view/InputDevice; */
int v3 = 0; // const/4 v3, 0x0
if ( v0 != null) { // if-eqz v0, :cond_8
v4 = (( android.view.InputDevice ) v0 ).isXiaomiStylus ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->isXiaomiStylus()I
int v5 = 3; // const/4 v5, 0x3
/* if-ge v4, v5, :cond_4 */
/* .line 332 */
} // :cond_4
v4 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isSupportStylusRemoteControl()Z */
/* if-nez v4, :cond_5 */
/* .line 333 */
/* .line 336 */
} // :cond_5
v3 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v3, :cond_7 */
/* .line 337 */
v3 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 338 */
/* .local v3, "keyCode":I */
/* if-ne v3, v2, :cond_6 */
/* .line 339 */
v1 = this.mHandler;
/* new-instance v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 340 */
} // :cond_6
/* if-ne v3, v1, :cond_7 */
/* .line 341 */
v1 = this.mHandler;
/* new-instance v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda2; */
/* invoke-direct {v2, p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V */
(( android.os.Handler ) v1 ).post ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 344 */
} // .end local v3 # "keyCode":I
} // :cond_7
} // :goto_1
int v1 = 1; // const/4 v1, 0x1
/* .line 330 */
} // :cond_8
} // :goto_2
} // .end method
private Boolean interceptLaserKey ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 426 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_1 */
/* .line 427 */
v0 = this.mFocusedWindow;
if ( v0 != null) { // if-eqz v0, :cond_0
v2 = com.miui.server.input.stylus.MiuiStylusShortcutManager.NEED_LASER_KEY_APP;
v0 = /* .line 429 */
if ( v0 != null) { // if-eqz v0, :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
/* move v0, v1 */
} // :goto_0
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mNeedDispatchLaserKey:Z */
/* .line 431 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mNeedDispatchLaserKey:Z */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 432 */
/* .line 434 */
} // :cond_2
v0 = this.mLaserPointerController;
/* if-nez v0, :cond_3 */
/* .line 436 */
v0 = this.mMiuiInputManagerInternal;
/* .line 437 */
(( com.android.server.input.MiuiInputManagerInternal ) v0 ).obtainLaserPointerController ( ); // invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerInternal;->obtainLaserPointerController()Lcom/miui/server/input/stylus/laser/PointerControllerInterface;
/* check-cast v0, Lcom/miui/server/input/stylus/laser/LaserPointerController; */
this.mLaserPointerController = v0;
/* .line 439 */
} // :cond_3
v0 = this.mLaserPointerController;
v0 = (( com.miui.server.input.stylus.laser.LaserPointerController ) v0 ).interceptLaserKey ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->interceptLaserKey(Landroid/view/KeyEvent;)Z
} // .end method
private Boolean interceptQuickNoteKey ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 451 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v0, v2 */
} // :cond_0
/* move v0, v1 */
/* .line 452 */
/* .local v0, "isUp":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 454 */
v1 = this.mHandler;
int v3 = 4; // const/4 v3, 0x4
(( android.os.Handler ) v1 ).sendEmptyMessage ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 456 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->removeMessageIfHas(I)V */
/* .line 457 */
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z */
/* .line 459 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z */
/* .line 460 */
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 461 */
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptQuickNoteKeyDownWhenScreenOn()Z */
/* .line 463 */
} // :cond_2
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptQuickNoteKeyDownWhenScreenOff()Z */
} // .end method
private Boolean interceptQuickNoteKeyDownWhenScreenOff ( ) {
/* .locals 1 */
/* .line 467 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private Boolean interceptQuickNoteKeyDownWhenScreenOn ( ) {
/* .locals 5 */
/* .line 471 */
v0 = this.mFocusedWindow;
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
int v2 = 0; // const/4 v2, 0x0
/* if-nez v0, :cond_0 */
/* .line 473 */
final String v0 = "focus window is null"; // const-string v0, "focus window is null"
android.util.Slog .i ( v1,v0 );
/* .line 474 */
/* .line 476 */
} // :cond_0
v0 = v3 = com.miui.server.input.stylus.MiuiStylusShortcutManager.NOTE_PACKAGE_NAME;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 478 */
final String v0 = "focus window is notes, so long press page down disable"; // const-string v0, "focus window is notes, so long press page down disable"
android.util.Slog .i ( v1,v0 );
/* .line 479 */
/* .line 481 */
} // :cond_1
v0 = this.mHandler;
int v1 = 1; // const/4 v1, 0x1
/* const-wide/16 v3, 0x17c */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 482 */
} // .end method
private Boolean interceptScreenShotKey ( android.view.KeyEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 486 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 0; // const/4 v1, 0x0
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
} // :cond_0
/* move v2, v1 */
} // :goto_0
/* move v0, v2 */
/* .line 487 */
/* .local v0, "isUp":Z */
int v2 = 2; // const/4 v2, 0x2
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 489 */
v1 = this.mHandler;
(( android.os.Handler ) v1 ).removeMessages ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V
/* .line 491 */
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z */
/* .line 493 */
} // :cond_1
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z */
/* .line 494 */
v3 = this.mHandler;
/* const-wide/16 v4, 0x17c */
(( android.os.Handler ) v3 ).sendEmptyMessageDelayed ( v2, v4, v5 ); // invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 496 */
} // .end method
private static Boolean isLaserKey ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p0, "event" # Landroid/view/KeyEvent; */
/* .line 650 */
v0 = (( android.view.KeyEvent ) p0 ).getKeyCode ( ); // invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 651 */
/* .local v0, "keyCode":I */
(( android.view.KeyEvent ) p0 ).getDevice ( ); // invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 652 */
/* .local v1, "device":Landroid/view/InputDevice; */
/* const/16 v2, 0xc1 */
/* if-ne v0, v2, :cond_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 653 */
v2 = (( android.view.InputDevice ) v1 ).isXiaomiStylus ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I
int v3 = 3; // const/4 v3, 0x3
/* if-ne v2, v3, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 652 */
} // :goto_0
} // .end method
private static Boolean isQuickNoteKey ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p0, "event" # Landroid/view/KeyEvent; */
/* .line 636 */
v0 = (( android.view.KeyEvent ) p0 ).getKeyCode ( ); // invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 637 */
/* .local v0, "keyCode":I */
(( android.view.KeyEvent ) p0 ).getDevice ( ); // invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 638 */
/* .local v1, "device":Landroid/view/InputDevice; */
/* const/16 v2, 0x5d */
/* if-ne v0, v2, :cond_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 639 */
v2 = (( android.view.InputDevice ) v1 ).isXiaomiStylus ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I
/* if-lez v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 638 */
} // :goto_0
} // .end method
private static Boolean isScreenShotKey ( android.view.KeyEvent p0 ) {
/* .locals 3 */
/* .param p0, "event" # Landroid/view/KeyEvent; */
/* .line 643 */
v0 = (( android.view.KeyEvent ) p0 ).getKeyCode ( ); // invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I
/* .line 644 */
/* .local v0, "keyCode":I */
(( android.view.KeyEvent ) p0 ).getDevice ( ); // invoke-virtual {p0}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;
/* .line 645 */
/* .local v1, "device":Landroid/view/InputDevice; */
/* const/16 v2, 0x5c */
/* if-ne v0, v2, :cond_0 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 646 */
v2 = (( android.view.InputDevice ) v1 ).isXiaomiStylus ( ); // invoke-virtual {v1}, Landroid/view/InputDevice;->isXiaomiStylus()I
/* if-lez v2, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
int v2 = 0; // const/4 v2, 0x0
/* .line 645 */
} // :goto_0
} // .end method
private Boolean isSplitOrFreeFormMode ( ) {
/* .locals 6 */
/* .line 380 */
v0 = this.mAtms;
v0 = (( com.android.server.wm.ActivityTaskManagerService ) v0 ).isInSplitScreenWindowingMode ( ); // invoke-virtual {v0}, Lcom/android/server/wm/ActivityTaskManagerService;->isInSplitScreenWindowingMode()Z
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
int v2 = 1; // const/4 v2, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 381 */
final String v0 = "current activity is split screen windowing mode\uff0c not support short video remote control"; // const-string v0, "current activity is split screen windowing mode\uff0c not support short video remote control"
android.util.Slog .w ( v1,v0 );
/* .line 383 */
/* .line 385 */
} // :cond_0
v0 = this.mAtms;
(( com.android.server.wm.ActivityTaskManagerService ) v0 ).getTasks ( v2 ); // invoke-virtual {v0, v2}, Lcom/android/server/wm/ActivityTaskManagerService;->getTasks(I)Ljava/util/List;
/* .line 386 */
v3 = /* .local v0, "runningTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;" */
/* if-nez v3, :cond_2 */
int v3 = 0; // const/4 v3, 0x0
/* check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo; */
v4 = (( android.app.ActivityManager$RunningTaskInfo ) v4 ).getWindowingMode ( ); // invoke-virtual {v4}, Landroid/app/ActivityManager$RunningTaskInfo;->getWindowingMode()I
int v5 = 5; // const/4 v5, 0x5
/* if-ne v4, v5, :cond_1 */
/* .line 392 */
} // :cond_1
/* .line 388 */
} // :cond_2
} // :goto_0
final String v3 = "current activity is free form windowing mode\uff0c not support short video remote control"; // const-string v3, "current activity is free form windowing mode\uff0c not support short video remote control"
android.util.Slog .w ( v1,v3 );
/* .line 390 */
} // .end method
private Boolean isSupportStylusRemoteControl ( ) {
/* .locals 6 */
/* .line 235 */
v0 = this.mFocusedWindow;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 236 */
v0 = this.mStylusPageKeyConfig;
v0 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v0 ).isEnable ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->isEnable()Z
/* if-nez v0, :cond_0 */
/* .line 237 */
/* .line 239 */
} // :cond_0
v0 = this.mFocusedWindow;
/* .line 240 */
/* .local v0, "name":Ljava/lang/String; */
v2 = this.mStylusPageKeyConfig;
v2 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v2 ).getAppWhiteSet ( ); // invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getAppWhiteSet()Ljava/util/Set;
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 241 */
v2 = this.mStylusPageKeyConfig;
v2 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v2 ).getActivityBlackSet ( ); // invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getActivityBlackSet()Ljava/util/Set;
int v3 = 1; // const/4 v3, 0x1
final String v4 = "MiuiStylusShortcutManager"; // const-string v4, "MiuiStylusShortcutManager"
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 242 */
final String v1 = "Current activity supports short video remote control"; // const-string v1, "Current activity supports short video remote control"
android.util.Slog .w ( v4,v1 );
/* .line 243 */
/* .line 245 */
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getActivityName()Ljava/lang/String; */
/* .line 246 */
/* .local v2, "activityName":Ljava/lang/String; */
v5 = android.text.TextUtils .isEmpty ( v2 );
/* if-nez v5, :cond_2 */
v5 = this.mStylusPageKeyConfig;
/* .line 247 */
v5 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v5 ).getActivityBlackSet ( ); // invoke-virtual {v5}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getActivityBlackSet()Ljava/util/Set;
/* if-nez v5, :cond_2 */
/* .line 248 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Current activity "; // const-string v5, "Current activity "
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " supports short video remote control"; // const-string v5, " supports short video remote control"
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v4,v1 );
/* .line 250 */
/* .line 254 */
} // .end local v0 # "name":Ljava/lang/String;
} // .end local v2 # "activityName":Ljava/lang/String;
} // :cond_2
} // .end method
private Boolean isUserSetUp ( ) {
/* .locals 4 */
/* .line 592 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
/* const-string/jumbo v2, "user_setup_complete" */
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
if ( v0 != null) { // if-eqz v0, :cond_0
int v3 = 1; // const/4 v3, 0x1
} // :cond_0
} // .end method
private Boolean isUserSetupComplete ( ) {
/* .locals 1 */
/* .line 542 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsUserSetupComplete:Z */
/* if-nez v0, :cond_0 */
/* .line 544 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetUp()Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsUserSetupComplete:Z */
/* .line 545 */
/* .line 547 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // .end method
private void lambda$initStylusPageKeyConfig$1 ( ) { //synthethic
/* .locals 1 */
/* .line 207 */
com.miui.server.input.stylus.MiuiStylusUtils .getDefaultStylusPageKeyConfig ( );
this.mStylusPageKeyConfig = v0;
return;
} // .end method
private void lambda$new$0 ( android.os.Message p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "value" # Landroid/os/Message; */
/* .line 179 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->triggerQuickNoteKeyFunction()V */
return;
} // .end method
private void launchNote ( ) {
/* .locals 5 */
/* .line 606 */
/* new-instance v0, Landroid/os/Bundle; */
/* invoke-direct {v0}, Landroid/os/Bundle;-><init>()V */
/* .line 607 */
/* .local v0, "bundle":Landroid/os/Bundle; */
final String v1 = "scene"; // const-string v1, "scene"
v2 = this.mScene;
(( android.os.Bundle ) v0 ).putString ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
/* .line 608 */
v1 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v1 );
/* const-string/jumbo v2, "stylus" */
int v3 = 1; // const/4 v3, 0x1
final String v4 = "note"; // const-string v4, "note"
(( com.miui.server.input.util.ShortCutActionsUtils ) v1 ).triggerFunction ( v4, v2, v0, v3 ); // invoke-virtual {v1, v4, v2, v0, v3}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 610 */
return;
} // .end method
private void next ( ) {
/* .locals 12 */
/* .line 359 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isSplitOrFreeFormMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 360 */
return;
/* .line 362 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getWindowSwipeInfo()[I */
/* .line 363 */
/* .local v0, "swipeInfo":[I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "swipePositionInfo: downX: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v0, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " downY: "; // const-string v3, " downY: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v3 = 2; // const/4 v3, 0x2
/* aget v4, v0, v3 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " upX: "; // const-string v4, " upX: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v4, v0, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " upY: "; // const-string v4, " upY: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v4 = 1; // const/4 v4, 0x1
/* aget v5, v0, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiStylusShortcutManager"; // const-string v5, "MiuiStylusShortcutManager"
android.util.Slog .w ( v5,v1 );
/* .line 365 */
com.miui.server.input.util.MiuiInputShellCommand .getInstance ( );
/* aget v7, v0, v2 */
/* aget v8, v0, v3 */
/* aget v9, v0, v2 */
/* aget v10, v0, v4 */
/* const/16 v11, 0x3c */
/* invoke-virtual/range {v6 ..v11}, Lcom/miui/server/input/util/MiuiInputShellCommand;->swipeGenerator(IIIII)Z */
/* .line 367 */
return;
} // .end method
private void previous ( ) {
/* .locals 12 */
/* .line 348 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isSplitOrFreeFormMode()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 349 */
return;
/* .line 351 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->getWindowSwipeInfo()[I */
/* .line 352 */
/* .local v0, "swipeInfo":[I */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "swipePositionInfo: downX: " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v2 = 0; // const/4 v2, 0x0
/* aget v3, v0, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " downY: "; // const-string v3, " downY: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v3 = 1; // const/4 v3, 0x1
/* aget v4, v0, v3 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " upX: "; // const-string v4, " upX: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* aget v4, v0, v2 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " upY: "; // const-string v4, " upY: "
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
int v4 = 2; // const/4 v4, 0x2
/* aget v5, v0, v4 */
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v5 = "MiuiStylusShortcutManager"; // const-string v5, "MiuiStylusShortcutManager"
android.util.Slog .w ( v5,v1 );
/* .line 354 */
com.miui.server.input.util.MiuiInputShellCommand .getInstance ( );
/* aget v7, v0, v2 */
/* aget v8, v0, v3 */
/* aget v9, v0, v2 */
/* aget v10, v0, v4 */
/* const/16 v11, 0x3c */
/* invoke-virtual/range {v6 ..v11}, Lcom/miui/server/input/util/MiuiInputShellCommand;->swipeGenerator(IIIII)Z */
/* .line 356 */
return;
} // .end method
private void putHallSettings ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "hall" # I */
/* .line 691 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* const-string/jumbo v1, "stylus_hall_status" */
android.provider.Settings$System .putInt ( v0,v1,p1 );
/* .line 693 */
return;
} // .end method
private void removeMessageIfHas ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "what" # I */
/* .line 536 */
v0 = this.mHandler;
v0 = (( android.os.Handler ) v0 ).hasMessages ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 537 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).removeMessages ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 539 */
} // :cond_0
return;
} // .end method
private void removeStylusMaskWindow ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "animation" # Z */
/* .line 525 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "request remove stylus mask, mIsRequestMaskShow = "; // const-string v1, "request remove stylus mask, mIsRequestMaskShow = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
android.util.Slog .i ( v1,v0 );
/* .line 526 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z */
/* if-nez v0, :cond_0 */
/* .line 528 */
return;
/* .line 530 */
} // :cond_0
v0 = this.mInputSettingsConnection;
/* .line 531 */
/* nop */
/* .line 530 */
int v1 = 2; // const/4 v1, 0x2
(( com.miui.server.input.MiuiInputSettingsConnection ) v0 ).sendMessageToInputSettings ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lcom/miui/server/input/MiuiInputSettingsConnection;->sendMessageToInputSettings(II)V
/* .line 532 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsRequestMaskShow:Z */
/* .line 533 */
return;
} // .end method
private void setHallStatus ( ) {
/* .locals 2 */
/* .line 682 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Stylus hall event "; // const-string v1, "Stylus hall event "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I */
/* if-nez v1, :cond_0 */
final String v1 = "close"; // const-string v1, "close"
} // :cond_0
final String v1 = "far"; // const-string v1, "far"
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
android.util.Slog .d ( v1,v0 );
/* .line 683 */
int v0 = 6; // const/4 v0, 0x6
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->removeMessageIfHas(I)V */
/* .line 684 */
android.os.Message .obtain ( );
/* .line 685 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).setWhat ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Message;->setWhat(I)Landroid/os/Message;
/* .line 686 */
/* iget v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mHallStatus:I */
/* iput v0, v1, Landroid/os/Message;->arg1:I */
/* .line 687 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).sendMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 688 */
return;
} // .end method
private void takePartialScreenshot ( ) {
/* .locals 5 */
/* .line 597 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
/* if-nez v0, :cond_0 */
/* .line 598 */
final String v0 = "MiuiStylusShortcutManager"; // const-string v0, "MiuiStylusShortcutManager"
final String v1 = "Can\'t take screenshot because screen is not on"; // const-string v1, "Can\'t take screenshot because screen is not on"
android.util.Slog .w ( v0,v1 );
/* .line 599 */
return;
/* .line 601 */
} // :cond_0
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
/* const-string/jumbo v3, "stylus_partial_screenshot" */
final String v4 = "long_press_page_up_key"; // const-string v4, "long_press_page_up_key"
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 603 */
return;
} // .end method
private void triggerNoteWhenScreenOff ( ) {
/* .locals 1 */
/* .line 630 */
final String v0 = "off_screen"; // const-string v0, "off_screen"
this.mScene = v0;
/* .line 631 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->launchNote()V */
/* .line 632 */
v0 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v0 );
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v0 ).trackStylusShortHandTrigger ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusShortHandTrigger()V
/* .line 633 */
return;
} // .end method
private void triggerQuickNoteKeyFunction ( ) {
/* .locals 4 */
/* .line 614 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->launchNote()V */
/* .line 615 */
v0 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v0 );
/* .line 616 */
/* const/16 v1, 0x5d */
android.view.KeyEvent .keyCodeToString ( v1 );
/* .line 617 */
v2 = this.mFocusedWindow;
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
final String v2 = ""; // const-string v2, ""
/* .line 615 */
} // :goto_0
final String v3 = "note"; // const-string v3, "note"
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v0 ).trackStylusKeyLongPress ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyLongPress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 618 */
return;
} // .end method
private void triggerScreenShotKeyFunction ( ) {
/* .locals 4 */
/* .line 622 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->takePartialScreenshot()V */
/* .line 623 */
v0 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v0 );
/* .line 625 */
/* const/16 v1, 0x5c */
android.view.KeyEvent .keyCodeToString ( v1 );
/* .line 626 */
v2 = this.mFocusedWindow;
if ( v2 != null) { // if-eqz v2, :cond_0
} // :cond_0
final String v2 = ""; // const-string v2, ""
/* .line 623 */
} // :goto_0
/* const-string/jumbo v3, "stylus_partial_screenshot" */
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v0 ).trackStylusKeyLongPress ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusKeyLongPress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
/* .line 627 */
return;
} // .end method
private void updateCloudConfig ( ) {
/* .locals 5 */
/* .line 211 */
v0 = this.mContext;
/* .line 212 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
final String v3 = "STYLUS_PAGE_KEY_BLACK_WHITE_LIST"; // const-string v3, "STYLUS_PAGE_KEY_BLACK_WHITE_LIST"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v3,v1,v1,v2 );
/* .line 214 */
/* .local v0, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
if ( v0 != null) { // if-eqz v0, :cond_2
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* if-nez v2, :cond_0 */
/* .line 219 */
} // :cond_0
/* nop */
/* .line 220 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
com.miui.server.input.stylus.MiuiStylusUtils .parseJsonToStylusPageKeyConfig ( v2 );
/* .line 221 */
/* .local v2, "cloudConfig":Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
v3 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v2 ).getVersion ( ); // invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I
v4 = this.mStylusPageKeyConfig;
v4 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v4 ).getVersion ( ); // invoke-virtual {v4}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I
/* if-ge v3, v4, :cond_1 */
/* .line 223 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Do not use cloud config, because cloud config version: "; // const-string v4, "Do not use cloud config, because cloud config version: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 224 */
v4 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v2 ).getVersion ( ); // invoke-virtual {v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " < current config version: "; // const-string v4, " < current config version: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mStylusPageKeyConfig;
/* .line 225 */
v4 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v4 ).getVersion ( ); // invoke-virtual {v4}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getVersion()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 223 */
android.util.Slog .w ( v1,v3 );
/* .line 226 */
return;
/* .line 229 */
} // :cond_1
this.mStylusPageKeyConfig = v2;
/* .line 230 */
final String v3 = "Cloud config has updated, use cloud config"; // const-string v3, "Cloud config has updated, use cloud config"
android.util.Slog .w ( v1,v3 );
/* .line 216 */
} // .end local v2 # "cloudConfig":Lcom/miui/server/input/stylus/StylusPageKeyConfig;
} // :cond_2
} // :goto_0
final String v2 = "Cloud config is null, use default local config"; // const-string v2, "Cloud config is null, use default local config"
android.util.Slog .w ( v1,v2 );
/* .line 232 */
} // :goto_1
return;
} // .end method
private void updateGameModeSettings ( ) {
/* .locals 3 */
/* .line 677 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v2, v1 */
} // :cond_0
/* iput-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z */
/* .line 679 */
return;
} // .end method
private void updateStylusScreenOffQuickNoteSettings ( ) {
/* .locals 4 */
/* .line 669 */
v0 = this.mContext;
/* .line 670 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* sget-boolean v1, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT:Z */
/* .line 669 */
/* const-string/jumbo v2, "stylus_quick_note_screen_off" */
int v3 = -2; // const/4 v3, -0x2
v0 = android.provider.MiuiSettings$System .getBooleanForUser ( v0,v2,v1,v3 );
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z */
/* .line 673 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Update stylus screen off quick note settings = "; // const-string v1, "Update stylus screen off quick note settings = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
android.util.Slog .i ( v1,v0 );
/* .line 674 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 705 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 706 */
final String v0 = "MiuiStylusShortcutManager"; // const-string v0, "MiuiStylusShortcutManager"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 707 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 708 */
final String v0 = "mScene="; // const-string v0, "mScene="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 709 */
v0 = this.mScene;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 710 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 711 */
final String v0 = "mLidOpen="; // const-string v0, "mLidOpen="
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 712 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 713 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 714 */
final String v0 = "mStylusPageKeyConfig"; // const-string v0, "mStylusPageKeyConfig"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 715 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 716 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 717 */
final String v0 = "mEnable = "; // const-string v0, "mEnable = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 718 */
v0 = this.mStylusPageKeyConfig;
v0 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v0 ).isEnable ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->isEnable()Z
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 719 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 720 */
final String v0 = "mAppWhiteSet.size = "; // const-string v0, "mAppWhiteSet.size = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 721 */
v0 = this.mStylusPageKeyConfig;
v0 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v0 ).getAppWhiteSet ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getAppWhiteSet()Ljava/util/Set;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 722 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 723 */
final String v0 = "mActivityBlackSet.size = "; // const-string v0, "mActivityBlackSet.size = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 724 */
v0 = this.mStylusPageKeyConfig;
v0 = (( com.miui.server.input.stylus.StylusPageKeyConfig ) v0 ).getActivityBlackSet ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->getActivityBlackSet()Ljava/util/Set;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 725 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 726 */
final String v0 = "StylusScreenOffQuickNoteDefault = "; // const-string v0, "StylusScreenOffQuickNoteDefault = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 727 */
/* sget-boolean v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->STYLUS_SCREEN_OFF_QUICK_NOTE_DEFAULT:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 728 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 729 */
final String v0 = "IsSupportScreenOffQuickNote = "; // const-string v0, "IsSupportScreenOffQuickNote = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 730 */
v0 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportOffScreenQuickNote ( );
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 731 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 732 */
final String v0 = "mIsScreenOffQuickNoteOn = "; // const-string v0, "mIsScreenOffQuickNoteOn = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 733 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 734 */
return;
} // .end method
public Long getDelayTime ( android.view.KeyEvent p0 ) {
/* .locals 9 */
/* .param p1, "keyEvent" # Landroid/view/KeyEvent; */
/* .line 281 */
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isScreenShotKey ( p1 );
/* const-wide/16 v1, 0x0 */
/* if-nez v0, :cond_0 */
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isQuickNoteKey ( p1 );
/* if-nez v0, :cond_0 */
/* .line 283 */
/* return-wide v1 */
/* .line 285 */
} // :cond_0
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isQuickNoteKey ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mQuickNoteKeyFunctionTriggered:Z */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mScreenShotKeyFunctionTriggered:Z */
/* .line 286 */
/* .local v0, "isTriggered":Z */
} // :goto_0
/* const-wide/16 v3, -0x1 */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 287 */
v1 = (( android.view.KeyEvent ) p1 ).getRepeatCount ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I
/* .line 289 */
/* .local v1, "repeatCount":I */
/* if-nez v1, :cond_2 */
/* .line 290 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Shortcut triggered, so intercept "; // const-string v5, "Shortcut triggered, so intercept "
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 291 */
v5 = (( android.view.KeyEvent ) p1 ).getKeyCode ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I
android.view.KeyEvent .keyCodeToString ( v5 );
(( java.lang.StringBuilder ) v2 ).append ( v5 ); // invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 290 */
final String v5 = "MiuiStylusShortcutManager"; // const-string v5, "MiuiStylusShortcutManager"
android.util.Slog .i ( v5,v2 );
/* .line 293 */
} // :cond_2
/* return-wide v3 */
/* .line 295 */
} // .end local v1 # "repeatCount":I
} // :cond_3
v5 = this.mHandler;
int v6 = 1; // const/4 v6, 0x1
v5 = (( android.os.Handler ) v5 ).hasMessages ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v5, :cond_5 */
v5 = this.mHandler;
/* .line 296 */
int v6 = 2; // const/4 v6, 0x2
v5 = (( android.os.Handler ) v5 ).hasMessages ( v6 ); // invoke-virtual {v5, v6}, Landroid/os/Handler;->hasMessages(I)Z
/* if-nez v5, :cond_5 */
/* .line 298 */
v5 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptKeyByRemoteControl(Landroid/view/KeyEvent;)Z */
if ( v5 != null) { // if-eqz v5, :cond_4
/* .line 299 */
/* return-wide v3 */
/* .line 302 */
} // :cond_4
/* return-wide v1 */
/* .line 304 */
} // :cond_5
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v3 */
/* .line 306 */
/* .local v3, "now":J */
(( android.view.KeyEvent ) p1 ).getDownTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J
/* move-result-wide v5 */
/* const-wide/16 v7, 0x17c */
/* add-long/2addr v5, v7 */
/* const-wide/16 v7, 0x14 */
/* add-long/2addr v5, v7 */
/* .line 307 */
/* .local v5, "timeoutTime":J */
/* cmp-long v7, v3, v5 */
/* if-gez v7, :cond_6 */
/* .line 309 */
/* sub-long v1, v5, v3 */
/* return-wide v1 */
/* .line 311 */
} // :cond_6
/* return-wide v1 */
} // .end method
public Boolean needInterceptBeforeDispatching ( android.view.KeyEvent p0 ) {
/* .locals 1 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 551 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetupComplete()Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z */
/* if-nez v0, :cond_1 */
/* .line 552 */
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isScreenShotKey ( p1 );
/* if-nez v0, :cond_0 */
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isQuickNoteKey ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 551 */
} // :goto_0
} // .end method
public void notifyLidSwitchChanged ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "lidOpen" # Z */
/* .line 657 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z */
/* .line 658 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "notify lidSwitch change to : "; // const-string v1, "notify lidSwitch change to : "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
android.util.Slog .d ( v1,v0 );
/* .line 659 */
return;
} // .end method
public void onDefaultDisplayFocusChangedLw ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 0 */
/* .param p1, "newFocus" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 565 */
this.mFocusedWindow = p1;
/* .line 566 */
return;
} // .end method
public void onSystemBooted ( ) {
/* .locals 2 */
/* .line 198 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->initStylusPageKeyConfig()V */
/* .line 199 */
/* new-instance v0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Handler;)V */
this.mMiuiSettingsObserver = v0;
/* .line 200 */
(( com.miui.server.input.stylus.MiuiStylusShortcutManager$MiuiSettingsObserver ) v0 ).observe ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver;->observe()V
/* .line 201 */
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "newUserId" # I */
/* .line 662 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateGameModeSettings()V */
/* .line 663 */
v0 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportOffScreenQuickNote ( );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 664 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->updateStylusScreenOffQuickNoteSettings()V */
/* .line 666 */
} // :cond_0
return;
} // .end method
public Integer parseInt ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "argument" # Ljava/lang/String; */
/* .line 697 */
try { // :try_start_0
v0 = java.lang.Integer .parseInt ( p1 );
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 698 */
/* :catch_0 */
/* move-exception v0 */
/* .line 699 */
/* .local v0, "e":Ljava/lang/NumberFormatException; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid integer argument "; // const-string v2, "Invalid integer argument "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiStylusShortcutManager"; // const-string v2, "MiuiStylusShortcutManager"
android.util.Slog .d ( v2,v1 );
/* .line 700 */
int v1 = -1; // const/4 v1, -0x1
} // .end method
public void processMotionEventForQuickNote ( android.view.MotionEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 569 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOffQuickNoteOn:Z */
final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
/* if-nez v0, :cond_0 */
/* .line 570 */
final String v0 = "Stylus screen off quick note function is off."; // const-string v0, "Stylus screen off quick note function is off."
android.util.Slog .w ( v1,v0 );
/* .line 571 */
return;
/* .line 573 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
/* if-nez v0, :cond_5 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z */
/* if-nez v0, :cond_1 */
/* .line 578 */
} // :cond_1
/* const/16 v0, 0x4002 */
v0 = (( android.view.MotionEvent ) p1 ).isFromSource ( v0 ); // invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->isFromSource(I)Z
int v2 = 0; // const/4 v2, 0x0
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 579 */
v0 = (( android.view.MotionEvent ) p1 ).getToolType ( v2 ); // invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I
int v3 = 2; // const/4 v3, 0x2
/* if-ne v0, v3, :cond_2 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_2
/* nop */
} // :goto_0
/* move v0, v2 */
/* .line 580 */
/* .local v0, "eventFromStylus":Z */
/* if-nez v0, :cond_3 */
/* .line 581 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Event not from stylus, event = "; // const-string v3, "Event not from stylus, event = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 582 */
return;
/* .line 584 */
} // :cond_3
v1 = (( android.view.MotionEvent ) p1 ).getActionMasked ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 585 */
return;
/* .line 588 */
} // :cond_4
v1 = this.mHandler;
int v2 = 5; // const/4 v2, 0x5
(( android.os.Handler ) v1 ).sendEmptyMessage ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 589 */
return;
/* .line 574 */
} // .end local v0 # "eventFromStylus":Z
} // :cond_5
} // :goto_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Not support quick note for offscreen or lid not open state.screenOn = "; // const-string v2, "Not support quick note for offscreen or lid not open state.screenOn = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v2 = ", lidOpen = "; // const-string v2, ", lidOpen = "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mLidOpen:Z */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v0 );
/* .line 576 */
return;
} // .end method
public Boolean shouldInterceptKey ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 402 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->isUserSetupComplete()Z */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 403 */
/* .line 405 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsGameMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 406 */
/* .line 408 */
} // :cond_1
v0 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isScreenShotKey ( p1 );
/* .line 409 */
/* .local v0, "isScreenShotKey":Z */
v2 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isQuickNoteKey ( p1 );
/* .line 410 */
/* .local v2, "isQuickNoteKey":Z */
/* if-nez v0, :cond_2 */
if ( v2 != null) { // if-eqz v2, :cond_3
} // :cond_2
v3 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
/* if-nez v3, :cond_3 */
/* .line 411 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->fadeLaserAndResetPosition()V */
/* .line 413 */
} // :cond_3
if ( v0 != null) { // if-eqz v0, :cond_4
/* .line 414 */
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptScreenShotKey(Landroid/view/KeyEvent;)Z */
/* .line 416 */
} // :cond_4
if ( v2 != null) { // if-eqz v2, :cond_5
/* .line 417 */
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptQuickNoteKey(Landroid/view/KeyEvent;)Z */
/* .line 419 */
} // :cond_5
v3 = com.miui.server.input.stylus.MiuiStylusShortcutManager .isLaserKey ( p1 );
if ( v3 != null) { // if-eqz v3, :cond_6
/* .line 420 */
v1 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->interceptLaserKey(Landroid/view/KeyEvent;)Z */
/* .line 422 */
} // :cond_6
} // .end method
public void updateScreenState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "screenOn" # Z */
/* .line 556 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->mIsScreenOn:Z */
/* .line 557 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 558 */
return;
/* .line 561 */
} // :cond_0
v0 = this.mHandler;
int v1 = 3; // const/4 v1, 0x3
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 562 */
return;
} // .end method
