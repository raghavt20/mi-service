.class public Lcom/miui/server/input/stylus/MiuiStylusUtils;
.super Ljava/lang/Object;
.source "MiuiStylusUtils.java"


# static fields
.field private static final DEFAULT_STYLUS_PAGE_KEY_BW_CONFIG_FILE_PATH:Ljava/lang/String; = "/system_ext/etc/input/stylus_page_key_bw_config.json"

.field private static final SUPPORT_STYLUS_PALM_REJECT:Ljava/lang/String; = "persist.stylus.palm.reject"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    const-class v0, Lcom/miui/server/input/stylus/MiuiStylusUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/stylus/MiuiStylusUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method public static getDefaultStylusPageKeyConfig()Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    .locals 4

    .line 71
    new-instance v0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;-><init>()V

    .line 73
    .local v0, "config":Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    :try_start_0
    const-string v1, "/system_ext/etc/input/stylus_page_key_bw_config.json"

    invoke-static {v1}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->readFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "configString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 76
    return-object v0

    .line 78
    :cond_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->parseJsonToStylusPageKeyConfigInternal(Lcom/miui/server/input/stylus/StylusPageKeyConfig;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .end local v1    # "configString":Ljava/lang/String;
    goto :goto_0

    .line 79
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/miui/server/input/stylus/MiuiStylusUtils;->TAG:Ljava/lang/String;

    const-string v3, "parse config failed!!!"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 82
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method

.method public static isSupportOffScreenQuickNote()Z
    .locals 2

    .line 41
    const-string/jumbo v0, "stylus_quick_note"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isSupportStylusPalmReject()Z
    .locals 2

    .line 48
    const-string v0, "persist.stylus.palm.reject"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static parseJsonToStylusPageKeyConfig(Lorg/json/JSONObject;)Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    .locals 4
    .param p0, "object"    # Lorg/json/JSONObject;

    .line 86
    new-instance v0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;-><init>()V

    .line 88
    .local v0, "config":Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    :try_start_0
    invoke-static {v0, p0}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->parseJsonToStylusPageKeyConfigInternal(Lcom/miui/server/input/stylus/StylusPageKeyConfig;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    goto :goto_0

    .line 89
    :catch_0
    move-exception v1

    .line 90
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/miui/server/input/stylus/MiuiStylusUtils;->TAG:Ljava/lang/String;

    const-string v3, "parse config failed!!!"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 92
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method

.method private static parseJsonToStylusPageKeyConfigInternal(Lcom/miui/server/input/stylus/StylusPageKeyConfig;Lorg/json/JSONObject;)V
    .locals 7
    .param p0, "config"    # Lcom/miui/server/input/stylus/StylusPageKeyConfig;
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 97
    const-string/jumbo v0, "version"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setVersion(I)V

    .line 98
    const-string v0, "enable"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setEnable(Z)V

    .line 99
    const-string v0, "appWhiteList"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 100
    .local v0, "appWhiteList":Lorg/json/JSONArray;
    const-string v1, "activityBlackList"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 101
    .local v1, "activityBlackList":Lorg/json/JSONArray;
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 102
    .local v2, "appWhiteSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 103
    .local v3, "activityBlackSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    .local v5, "size":I
    :goto_0
    if-ge v4, v5, :cond_0

    .line 104
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 106
    .end local v4    # "i":I
    .end local v5    # "size":I
    :cond_0
    const/4 v4, 0x0

    .restart local v4    # "i":I
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    .restart local v5    # "size":I
    :goto_1
    if-ge v4, v5, :cond_1

    .line 107
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 109
    .end local v4    # "i":I
    .end local v5    # "size":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setAppWhiteSet(Ljava/util/Set;)V

    .line 110
    invoke-virtual {p0, v3}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setActivityBlackSet(Ljava/util/Set;)V

    .line 111
    return-void
.end method

.method private static readFileToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_3

    .line 58
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .local v1, "result":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .local v2, "bufferedReader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "line":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 62
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 64
    .end local v4    # "line":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 66
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 59
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "result":Ljava/lang/StringBuilder;
    .end local p0    # "filePath":Ljava/lang/String;
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 64
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "result":Ljava/lang/StringBuilder;
    .restart local p0    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 65
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/miui/server/input/stylus/MiuiStylusUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") load fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 67
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 55
    .end local v1    # "result":Ljava/lang/StringBuilder;
    :cond_2
    :goto_3
    sget-object v1, Lcom/miui/server/input/stylus/MiuiStylusUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "where is my file ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), I can\'t find it :("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string v1, ""

    return-object v1
.end method

.method public static supportStylusGesture()Z
    .locals 2

    .line 34
    const-string/jumbo v0, "support_stylus_gesture"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiui/util/FeatureParser;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
