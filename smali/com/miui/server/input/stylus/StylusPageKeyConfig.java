public class com.miui.server.input.stylus.StylusPageKeyConfig {
	 /* .source "StylusPageKeyConfig.java" */
	 /* # instance fields */
	 private java.util.Set mActivityBlackSet;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.Set mAppWhiteSet;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private Boolean mEnable;
private Integer mVersion;
/* # direct methods */
public com.miui.server.input.stylus.StylusPageKeyConfig ( ) {
/* .locals 1 */
/* .line 6 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 10 */
/* const/high16 v0, -0x80000000 */
/* iput v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I */
/* .line 14 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z */
/* .line 18 */
java.util.Collections .emptySet ( );
this.mAppWhiteSet = v0;
/* .line 22 */
java.util.Collections .emptySet ( );
this.mActivityBlackSet = v0;
return;
} // .end method
/* # virtual methods */
public java.util.Set getActivityBlackSet ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 49 */
v0 = this.mActivityBlackSet;
} // .end method
public java.util.Set getAppWhiteSet ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 41 */
v0 = this.mAppWhiteSet;
} // .end method
public Integer getVersion ( ) {
/* .locals 1 */
/* .line 25 */
/* iget v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I */
} // .end method
public Boolean isEnable ( ) {
/* .locals 1 */
/* .line 33 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z */
} // .end method
public void setActivityBlackSet ( java.util.Set p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 53 */
/* .local p1, "activityBlackSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
this.mActivityBlackSet = p1;
/* .line 54 */
return;
} // .end method
public void setAppWhiteSet ( java.util.Set p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 45 */
/* .local p1, "appWhiteSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
this.mAppWhiteSet = p1;
/* .line 46 */
return;
} // .end method
public void setEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 37 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z */
/* .line 38 */
return;
} // .end method
public void setVersion ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "version" # I */
/* .line 29 */
/* iput p1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I */
/* .line 30 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 58 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "StylusPageKeyConfig{version="; // const-string v1, "StylusPageKeyConfig{version="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mVersion:I */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = ", enable="; // const-string v1, ", enable="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->mEnable:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v1 = ", appWhiteSet="; // const-string v1, ", appWhiteSet="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mAppWhiteSet;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = ", activityBlackSet="; // const-string v1, ", activityBlackSet="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.mActivityBlackSet;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x7d */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
