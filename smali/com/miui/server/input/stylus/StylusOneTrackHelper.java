public class com.miui.server.input.stylus.StylusOneTrackHelper {
	 /* .source "StylusOneTrackHelper.java" */
	 /* # static fields */
	 private static final java.lang.String EVENT_NAME;
	 private static final java.lang.String FUNCTION_TRIGGER_TRACK_TYPE;
	 private static final java.lang.String LASER_TRIGGER_TRACK_TYPE;
	 private static final java.lang.String LONG_PRESS_TRACK_TYPE;
	 private static final java.lang.String PRESS_TRACK_TYPE;
	 private static final java.lang.String SHORTHAND_TRACK_TYPE;
	 private static final java.lang.String STYLUS_TRACK_APP_PACKAGE_NAME;
	 private static final java.lang.String STYLUS_TRACK_FUNCTION_TYPE;
	 private static final java.lang.String STYLUS_TRACK_KEY_TYPE;
	 private static final java.lang.String STYLUS_TRACK_TIP;
	 private static final java.lang.String STYLUS_TRACK_TIP_FUNCTION_TRIGGER_HIGHLIGHT;
	 private static final java.lang.String STYLUS_TRACK_TIP_FUNCTION_TRIGGER_PEN;
	 private static final java.lang.String STYLUS_TRACK_TIP_LASER_TRIGGER;
	 private static final java.lang.String STYLUS_TRACK_TIP_LONG_PRESS;
	 private static final java.lang.String STYLUS_TRACK_TIP_PRESS;
	 private static final java.lang.String STYLUS_TRACK_TIP_SHORTHAND;
	 private static final java.lang.String TAG;
	 private static volatile com.miui.server.input.stylus.StylusOneTrackHelper sInstance;
	 /* # instance fields */
	 private final android.content.Context mContext;
	 /* # direct methods */
	 private com.miui.server.input.stylus.StylusOneTrackHelper ( ) {
		 /* .locals 0 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 37 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 38 */
		 this.mContext = p1;
		 /* .line 39 */
		 return;
	 } // .end method
	 public static com.miui.server.input.stylus.StylusOneTrackHelper getInstance ( android.content.Context p0 ) {
		 /* .locals 2 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .line 42 */
		 v0 = com.miui.server.input.stylus.StylusOneTrackHelper.sInstance;
		 /* if-nez v0, :cond_1 */
		 /* .line 43 */
		 /* const-class v0, Lcom/miui/server/input/stylus/StylusOneTrackHelper; */
		 /* monitor-enter v0 */
		 /* .line 44 */
		 try { // :try_start_0
			 v1 = com.miui.server.input.stylus.StylusOneTrackHelper.sInstance;
			 /* if-nez v1, :cond_0 */
			 /* .line 45 */
			 /* new-instance v1, Lcom/miui/server/input/stylus/StylusOneTrackHelper; */
			 /* invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;-><init>(Landroid/content/Context;)V */
			 /* .line 47 */
		 } // :cond_0
		 /* monitor-exit v0 */
		 /* :catchall_0 */
		 /* move-exception v1 */
		 /* monitor-exit v0 */
		 /* :try_end_0 */
		 /* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
		 /* throw v1 */
		 /* .line 49 */
	 } // :cond_1
} // :goto_0
v0 = com.miui.server.input.stylus.StylusOneTrackHelper.sInstance;
} // .end method
private org.json.JSONObject getTrackJSONObject ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "trackType" # Ljava/lang/String; */
/* .param p2, "tip" # Ljava/lang/String; */
/* .line 133 */
/* new-instance v0, Lorg/json/JSONObject; */
/* invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V */
/* .line 135 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
	 final String v1 = "EVENT_NAME"; // const-string v1, "EVENT_NAME"
	 (( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* .line 136 */
	 /* const-string/jumbo v1, "tip" */
	 (( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
	 /* :try_end_0 */
	 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
	 /* .line 139 */
	 /* .line 137 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 138 */
	 /* .local v1, "e":Lorg/json/JSONException; */
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "Failed to get event tracking data of stylus!"; // const-string v3, "Failed to get event tracking data of stylus!"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v3 = "StylusOneTrackHelper"; // const-string v3, "StylusOneTrackHelper"
	 android.util.Slog .e ( v3,v2 );
	 /* .line 140 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
} // .end method
/* # virtual methods */
public void trackStylusHighLightTrigger ( ) {
/* .locals 3 */
/* .line 120 */
final String v0 = "function_trigger"; // const-string v0, "function_trigger"
final String v1 = "899.2.0.1.28606"; // const-string v1, "899.2.0.1.28606"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 122 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackStylusEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V
/* .line 123 */
return;
} // .end method
public void trackStylusKeyLongPress ( java.lang.String p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 4 */
/* .param p1, "functionType" # Ljava/lang/String; */
/* .param p2, "keyType" # Ljava/lang/String; */
/* .param p3, "packageName" # Ljava/lang/String; */
/* .line 69 */
final String v0 = "long_press"; // const-string v0, "long_press"
final String v1 = "899.2.3.1.27099"; // const-string v1, "899.2.3.1.27099"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 72 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "function_type"; // const-string v1, "function_type"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 73 */
final String v1 = "key_type"; // const-string v1, "key_type"
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 74 */
final String v1 = "app_package_name"; // const-string v1, "app_package_name"
(( org.json.JSONObject ) v0 ).put ( v1, p3 ); // invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 77 */
/* .line 75 */
/* :catch_0 */
/* move-exception v1 */
/* .line 76 */
/* .local v1, "e":Lorg/json/JSONException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Stylus trackEvent data fail!"; // const-string v3, "Stylus trackEvent data fail!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "StylusOneTrackHelper"; // const-string v3, "StylusOneTrackHelper"
android.util.Slog .e ( v3,v2 );
/* .line 78 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackStylusEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V
/* .line 79 */
return;
} // .end method
public void trackStylusKeyPress ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p1, "keyType" # Ljava/lang/String; */
/* .param p2, "packageName" # Ljava/lang/String; */
/* .line 88 */
final String v0 = "press"; // const-string v0, "press"
final String v1 = "899.2.3.1.27100"; // const-string v1, "899.2.3.1.27100"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 90 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
try { // :try_start_0
final String v1 = "key_type"; // const-string v1, "key_type"
(( org.json.JSONObject ) v0 ).put ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* .line 91 */
final String v1 = "app_package_name"; // const-string v1, "app_package_name"
(( org.json.JSONObject ) v0 ).put ( v1, p2 ); // invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 94 */
/* .line 92 */
/* :catch_0 */
/* move-exception v1 */
/* .line 93 */
/* .local v1, "e":Lorg/json/JSONException; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed to get event tracking data of stylus!"; // const-string v3, "Failed to get event tracking data of stylus!"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "StylusOneTrackHelper"; // const-string v3, "StylusOneTrackHelper"
android.util.Slog .e ( v3,v2 );
/* .line 95 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackStylusEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V
/* .line 96 */
return;
} // .end method
public void trackStylusLaserTrigger ( ) {
/* .locals 3 */
/* .line 56 */
final String v0 = "laser_trigger"; // const-string v0, "laser_trigger"
final String v1 = "899.2.2.1.27097"; // const-string v1, "899.2.2.1.27097"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 58 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackStylusEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V
/* .line 59 */
return;
} // .end method
public void trackStylusPenTrigger ( ) {
/* .locals 3 */
/* .line 111 */
final String v0 = "function_trigger"; // const-string v0, "function_trigger"
final String v1 = "899.2.0.1.28605"; // const-string v1, "899.2.0.1.28605"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 113 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackStylusEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V
/* .line 114 */
return;
} // .end method
public void trackStylusShortHandTrigger ( ) {
/* .locals 3 */
/* .line 102 */
/* const-string/jumbo v0, "shorthand" */
final String v1 = "899.2.0.1.29459"; // const-string v1, "899.2.0.1.29459"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject; */
/* .line 104 */
/* .local v0, "jsonObject":Lorg/json/JSONObject; */
v1 = this.mContext;
com.android.server.input.InputOneTrackUtil .getInstance ( v1 );
(( org.json.JSONObject ) v0 ).toString ( ); // invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
(( com.android.server.input.InputOneTrackUtil ) v1 ).trackStylusEvent ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V
/* .line 105 */
return;
} // .end method
