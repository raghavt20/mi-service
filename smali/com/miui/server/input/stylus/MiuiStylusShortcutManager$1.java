class com.miui.server.input.stylus.MiuiStylusShortcutManager$1 extends android.os.UEventObserver {
	 /* .source "MiuiStylusShortcutManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.stylus.MiuiStylusShortcutManager this$0; //synthetic
/* # direct methods */
 com.miui.server.input.stylus.MiuiStylusShortcutManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
/* .line 152 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onUEvent ( android.os.UEventObserver$UEvent p0 ) {
/* .locals 6 */
/* .param p1, "event" # Landroid/os/UEventObserver$UEvent; */
/* .line 155 */
final String v0 = "POWER_SUPPLY_PEN_HALL3"; // const-string v0, "POWER_SUPPLY_PEN_HALL3"
(( android.os.UEventObserver$UEvent ) p1 ).get ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;
/* .line 156 */
/* .local v0, "hall3_str":Ljava/lang/String; */
final String v1 = "POWER_SUPPLY_PEN_HALL4"; // const-string v1, "POWER_SUPPLY_PEN_HALL4"
(( android.os.UEventObserver$UEvent ) p1 ).get ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;
/* .line 157 */
/* .local v1, "hall4_str":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_2
	 if ( v1 != null) { // if-eqz v1, :cond_2
		 /* .line 158 */
		 v2 = this.this$0;
		 v2 = 		 (( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v2 ).parseInt ( v0 ); // invoke-virtual {v2, v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->parseInt(Ljava/lang/String;)I
		 /* .line 159 */
		 /* .local v2, "hall3":I */
		 v3 = this.this$0;
		 v3 = 		 (( com.miui.server.input.stylus.MiuiStylusShortcutManager ) v3 ).parseInt ( v1 ); // invoke-virtual {v3, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->parseInt(Ljava/lang/String;)I
		 /* .line 160 */
		 /* .local v3, "hall4":I */
		 int v4 = 1; // const/4 v4, 0x1
		 if ( v2 != null) { // if-eqz v2, :cond_0
			 /* if-nez v3, :cond_1 */
		 } // :cond_0
		 v5 = this.this$0;
		 v5 = 		 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fgetmHallStatus ( v5 );
		 /* if-ne v5, v4, :cond_1 */
		 /* .line 161 */
		 v4 = this.this$0;
		 int v5 = 0; // const/4 v5, 0x0
		 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fputmHallStatus ( v4,v5 );
		 /* .line 162 */
		 v4 = this.this$0;
		 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$msetHallStatus ( v4 );
		 /* .line 163 */
	 } // :cond_1
	 /* if-ne v2, v4, :cond_2 */
	 /* if-ne v3, v4, :cond_2 */
	 v5 = this.this$0;
	 v5 = 	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fgetmHallStatus ( v5 );
	 /* if-nez v5, :cond_2 */
	 /* .line 164 */
	 v5 = this.this$0;
	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fputmHallStatus ( v5,v4 );
	 /* .line 165 */
	 v4 = this.this$0;
	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$msetHallStatus ( v4 );
	 /* .line 168 */
} // .end local v2 # "hall3":I
} // .end local v3 # "hall4":I
} // :cond_2
} // :goto_0
return;
} // .end method
