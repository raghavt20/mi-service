public class com.miui.server.input.stylus.blocker.StylusBlockerConfig {
	 /* .source "StylusBlockerConfig.java" */
	 /* # instance fields */
	 private java.util.Set mBlockSet;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private Boolean mEnable;
private Integer mStrongModeDelayTime;
private Float mStrongModeMoveThreshold;
private Integer mVersion;
private Integer mWeakModeDelayTime;
private Float mWeakModeMoveThreshold;
/* # direct methods */
public com.miui.server.input.stylus.blocker.StylusBlockerConfig ( ) {
/* .locals 2 */
/* .line 6 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 10 */
/* const/high16 v0, -0x80000000 */
/* iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mVersion:I */
/* .line 14 */
java.util.Collections .emptySet ( );
this.mBlockSet = v0;
/* .line 18 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mEnable:Z */
/* .line 22 */
/* const/16 v0, 0x40 */
/* iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeDelayTime:I */
/* .line 26 */
/* const/high16 v0, 0x41000000 # 8.0f */
/* iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeMoveThreshold:F */
/* .line 30 */
/* const/16 v1, 0x28 */
/* iput v1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeDelayTime:I */
/* .line 34 */
/* iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeMoveThreshold:F */
return;
} // .end method
/* # virtual methods */
public java.util.Set getBlockSet ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 37 */
v0 = this.mBlockSet;
} // .end method
public Integer getStrongModeDelayTime ( ) {
/* .locals 1 */
/* .line 53 */
/* iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeDelayTime:I */
} // .end method
public Float getStrongModeMoveThreshold ( ) {
/* .locals 1 */
/* .line 61 */
/* iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeMoveThreshold:F */
} // .end method
public Integer getVersion ( ) {
/* .locals 1 */
/* .line 85 */
/* iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mVersion:I */
} // .end method
public Integer getWeakModeDelayTime ( ) {
/* .locals 1 */
/* .line 69 */
/* iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeDelayTime:I */
} // .end method
public Float getWeakModeMoveThreshold ( ) {
/* .locals 1 */
/* .line 77 */
/* iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeMoveThreshold:F */
} // .end method
public Boolean isEnable ( ) {
/* .locals 1 */
/* .line 45 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mEnable:Z */
} // .end method
public void setBlockSet ( java.util.Set p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/Set<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 41 */
/* .local p1, "blockSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
this.mBlockSet = p1;
/* .line 42 */
return;
} // .end method
public void setEnable ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "enable" # Z */
/* .line 49 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mEnable:Z */
/* .line 50 */
return;
} // .end method
public void setStrongModeDelayTime ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "strongModeDelayTime" # I */
/* .line 57 */
/* iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeDelayTime:I */
/* .line 58 */
return;
} // .end method
public void setStrongModeMoveThreshold ( Float p0 ) {
/* .locals 0 */
/* .param p1, "strongModeMoveThreshold" # F */
/* .line 65 */
/* iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeMoveThreshold:F */
/* .line 66 */
return;
} // .end method
public void setVersion ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "version" # I */
/* .line 89 */
/* iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mVersion:I */
/* .line 90 */
return;
} // .end method
public void setWeakModeDelayTime ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "weakModeDelayTime" # I */
/* .line 73 */
/* iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeDelayTime:I */
/* .line 74 */
return;
} // .end method
public void setWeakModeMoveThreshold ( Float p0 ) {
/* .locals 0 */
/* .param p1, "weakModeMoveThreshold" # F */
/* .line 81 */
/* iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeMoveThreshold:F */
/* .line 82 */
return;
} // .end method
