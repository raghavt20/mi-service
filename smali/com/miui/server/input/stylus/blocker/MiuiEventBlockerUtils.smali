.class public Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;
.super Ljava/lang/Object;
.source "MiuiEventBlockerUtils.java"


# static fields
.field private static final DEFAULT_STYLUS_BLOCKER_CONFIG_FILE_PATH:Ljava/lang/String; = "/system_ext/etc/input/stylus_palm_reject_config.json"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    const-class v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultStylusBlockerConfig()Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    .locals 4

    .line 40
    new-instance v0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;-><init>()V

    .line 42
    .local v0, "config":Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    :try_start_0
    const-string v1, "/system_ext/etc/input/stylus_palm_reject_config.json"

    invoke-static {v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->readFileToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "configString":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 45
    return-object v0

    .line 47
    :cond_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->parseJsonToStylusBlockerConfigInternal(Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .end local v1    # "configString":Ljava/lang/String;
    goto :goto_0

    .line 48
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->TAG:Ljava/lang/String;

    const-string v3, "parse config failed!!!"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 51
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method

.method public static parseJsonToStylusBlockerConfig(Lorg/json/JSONObject;)Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    .locals 4
    .param p0, "jsonObject"    # Lorg/json/JSONObject;

    .line 55
    new-instance v0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;-><init>()V

    .line 57
    .local v0, "config":Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    :try_start_0
    invoke-static {v0, p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->parseJsonToStylusBlockerConfigInternal(Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    goto :goto_0

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->TAG:Ljava/lang/String;

    const-string v3, "parse config failed!!!"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 61
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method

.method private static parseJsonToStylusBlockerConfigInternal(Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;Lorg/json/JSONObject;)V
    .locals 5
    .param p0, "config"    # Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 67
    const-string/jumbo v0, "version"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setVersion(I)V

    .line 68
    const-string v0, "enable"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setEnable(Z)V

    .line 69
    const-string/jumbo v0, "weakModeDelayTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setWeakModeDelayTime(I)V

    .line 70
    const-string/jumbo v0, "weakModeMoveThreshold"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setWeakModeMoveThreshold(F)V

    .line 71
    const-string/jumbo v0, "strongModeDelayTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setStrongModeDelayTime(I)V

    .line 72
    const-string/jumbo v0, "strongModeMoveThreshold"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setStrongModeMoveThreshold(F)V

    .line 73
    const-string v0, "blockList"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 74
    .local v0, "blockList":Lorg/json/JSONArray;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 75
    .local v1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v3

    .local v3, "size":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 76
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 78
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_0
    invoke-virtual {p0, v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->setBlockSet(Ljava/util/Set;)V

    .line 79
    return-void
.end method

.method private static readFileToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_3

    .line 27
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .local v1, "result":Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .local v2, "bufferedReader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "line":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 31
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 33
    .end local v4    # "line":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 35
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 28
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "result":Ljava/lang/StringBuilder;
    .end local p0    # "filePath":Ljava/lang/String;
    :goto_1
    throw v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 33
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "result":Ljava/lang/StringBuilder;
    .restart local p0    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 34
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "file("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") load fail"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 36
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 24
    .end local v1    # "result":Ljava/lang/StringBuilder;
    :cond_2
    :goto_3
    sget-object v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "where is my file ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), I can\'t find it :("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    const-string v1, ""

    return-object v1
.end method
