.class public Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
.super Ljava/lang/Object;
.source "StylusBlockerConfig.java"


# instance fields
.field private mBlockSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEnable:Z

.field private mStrongModeDelayTime:I

.field private mStrongModeMoveThreshold:F

.field private mVersion:I

.field private mWeakModeDelayTime:I

.field private mWeakModeMoveThreshold:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mVersion:I

    .line 14
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mBlockSet:Ljava/util/Set;

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mEnable:Z

    .line 22
    const/16 v0, 0x40

    iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeDelayTime:I

    .line 26
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeMoveThreshold:F

    .line 30
    const/16 v1, 0x28

    iput v1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeDelayTime:I

    .line 34
    iput v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeMoveThreshold:F

    return-void
.end method


# virtual methods
.method public getBlockSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mBlockSet:Ljava/util/Set;

    return-object v0
.end method

.method public getStrongModeDelayTime()I
    .locals 1

    .line 53
    iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeDelayTime:I

    return v0
.end method

.method public getStrongModeMoveThreshold()F
    .locals 1

    .line 61
    iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeMoveThreshold:F

    return v0
.end method

.method public getVersion()I
    .locals 1

    .line 85
    iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mVersion:I

    return v0
.end method

.method public getWeakModeDelayTime()I
    .locals 1

    .line 69
    iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeDelayTime:I

    return v0
.end method

.method public getWeakModeMoveThreshold()F
    .locals 1

    .line 77
    iget v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeMoveThreshold:F

    return v0
.end method

.method public isEnable()Z
    .locals 1

    .line 45
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mEnable:Z

    return v0
.end method

.method public setBlockSet(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 41
    .local p1, "blockSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mBlockSet:Ljava/util/Set;

    .line 42
    return-void
.end method

.method public setEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .line 49
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mEnable:Z

    .line 50
    return-void
.end method

.method public setStrongModeDelayTime(I)V
    .locals 0
    .param p1, "strongModeDelayTime"    # I

    .line 57
    iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeDelayTime:I

    .line 58
    return-void
.end method

.method public setStrongModeMoveThreshold(F)V
    .locals 0
    .param p1, "strongModeMoveThreshold"    # F

    .line 65
    iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mStrongModeMoveThreshold:F

    .line 66
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .line 89
    iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mVersion:I

    .line 90
    return-void
.end method

.method public setWeakModeDelayTime(I)V
    .locals 0
    .param p1, "weakModeDelayTime"    # I

    .line 73
    iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeDelayTime:I

    .line 74
    return-void
.end method

.method public setWeakModeMoveThreshold(F)V
    .locals 0
    .param p1, "weakModeMoveThreshold"    # F

    .line 81
    iput p1, p0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->mWeakModeMoveThreshold:F

    .line 82
    return-void
.end method
