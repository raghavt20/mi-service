.class public Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;
.super Ljava/lang/Object;
.source "MiuiEventBlockerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;
    }
.end annotation


# static fields
.field private static final IS_IN_GAME_MODE_KEY:Ljava/lang/String; = "gb_boosting"

.field private static final STYLUS_BLOCKER_CONFIG:Ljava/lang/String; = "STYLUS_BLOCKER"

.field private static final SUPPORT_STYLUS_PALM_REJECT:Ljava/lang/String; = "persist.stylus.palm.reject"

.field private static final TAG:Ljava/lang/String;

.field private static volatile sInstance:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentFocusedPackageName:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mHasEditText:Z

.field private mInGameMode:Z

.field private mNativeEnableState:Z

.field private mNativeStrongMode:Z

.field private mSettingsObserver:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;

.field private mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

.field private mStylusConnected:Z


# direct methods
.method public static synthetic $r8$lambda$YZNnOnJltxooROd27xU-OiPuLyc(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateSettingsConfig()V

    return-void
.end method

.method public static synthetic $r8$lambda$ouObh8eMr5ls_fLdRSamM8Ar9n0(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->lambda$onSystemBooted$0()V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mupdateCloudConfig(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateCloudConfig()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateGameModeSettings(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateGameModeSettings()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mupdateSettingsConfig(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateSettingsConfig()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 22
    const-class v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z

    .line 61
    return-void
.end method

.method private getCurrentStylusBlockerState()Z
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->isEnable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 200
    return v1

    .line 202
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z

    if-eqz v0, :cond_1

    .line 204
    return v1

    .line 206
    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusConnected:Z

    if-nez v0, :cond_2

    .line 208
    return v1

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getBlockSet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mCurrentFocusedPackageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;
    .locals 2

    .line 64
    sget-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->sInstance:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    if-nez v0, :cond_1

    .line 65
    const-class v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    monitor-enter v0

    .line 66
    :try_start_0
    sget-object v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->sInstance:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    if-nez v1, :cond_0

    .line 67
    new-instance v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    invoke-direct {v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;-><init>()V

    sput-object v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->sInstance:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    .line 69
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 71
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->sInstance:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    return-object v0
.end method

.method private isNotInit()Z
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private synthetic lambda$onSystemBooted$0()V
    .locals 1

    .line 83
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->getDefaultStylusBlockerConfig()Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    return-void
.end method

.method private updateCloudConfig()V
    .locals 5

    .line 125
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mContext:Landroid/content/Context;

    .line 126
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v3, "STYLUS_BLOCKER"

    invoke-static {v0, v3, v1, v1, v2}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataSingle(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;

    move-result-object v0

    .line 128
    .local v0, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 133
    :cond_0
    nop

    .line 134
    invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerUtils;->parseJsonToStylusBlockerConfig(Lorg/json/JSONObject;)Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    move-result-object v1

    .line 135
    .local v1, "cloudConfig":Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I

    move-result v2

    iget-object v3, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v3}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 137
    sget-object v2, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Do not use cloud config, because "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 138
    invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    .line 139
    invoke-virtual {v4}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 137
    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return-void

    .line 143
    :cond_1
    iput-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    .line 144
    sget-object v2, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    const-string v3, "Use cloud config."

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 130
    .end local v1    # "cloudConfig":Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
    :cond_2
    :goto_0
    sget-object v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    const-string v2, "Not has cloud config, use default"

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :goto_1
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V

    .line 149
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateThresholdToNative(Z)V

    .line 150
    return-void
.end method

.method private updateEnableStateToNative()V
    .locals 4

    .line 215
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getCurrentStylusBlockerState()Z

    move-result v0

    .line 216
    .local v0, "newState":Z
    iget-boolean v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z

    if-ne v1, v0, :cond_0

    .line 217
    return-void

    .line 219
    :cond_0
    iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z

    .line 220
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z

    invoke-virtual {v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setStylusBlockerEnable(Z)V

    .line 221
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 222
    sget-object v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Flush enable state to native, enable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    return-void
.end method

.method private updateGameModeSettings()V
    .locals 4

    .line 153
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, -0x2

    const-string v2, "gb_boosting"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v3, v1

    :cond_0
    iput-boolean v3, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z

    .line 155
    sget-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update game mode settings, current is in game mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V

    .line 157
    return-void
.end method

.method private updateSettingsConfig()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateCloudConfig()V

    .line 121
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateGameModeSettings()V

    .line 122
    return-void
.end method

.method private updateThresholdToNative(Z)V
    .locals 5
    .param p1, "force"    # Z

    .line 236
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z

    iget-boolean v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z

    if-ne v0, v1, :cond_0

    .line 237
    return-void

    .line 239
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z

    iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z

    .line 240
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeDelayTime()I

    move-result v0

    goto :goto_0

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeDelayTime()I

    move-result v0

    :goto_0
    nop

    .line 242
    .local v0, "delayTime":I
    iget-boolean v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeMoveThreshold()F

    move-result v1

    goto :goto_1

    .line 243
    :cond_2
    iget-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeMoveThreshold()F

    move-result v1

    :goto_1
    nop

    .line 244
    .local v1, "moveThreshold":F
    sget-object v2, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Flush delay time and move threshold to native, delayTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", modeThreshold = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/server/input/config/InputCommonConfig;->setStylusBlockerDelayTime(I)V

    .line 247
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/input/config/InputCommonConfig;->setStylusBlockerMoveThreshold(F)V

    .line 248
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 249
    return-void
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .line 259
    const-string v0, "    "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 260
    sget-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 262
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const-string v0, "not support"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 264
    return-void

    .line 266
    :cond_0
    const-string v0, "init = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 268
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 269
    const-string v0, "mCurrentFocusedWindow = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mCurrentFocusedPackageName:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "null"

    :cond_2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 271
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 272
    const-string v0, "mStylusConnected = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 273
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusConnected:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 274
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 275
    const-string v0, "mHasEditText = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 276
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 277
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 278
    const-string v0, "mInGameMode = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 279
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 280
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 281
    const-string v0, "mNativeEnableState = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 282
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 283
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 284
    const-string v0, "mNativeStrongMode = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 285
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 286
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 287
    const-string v0, "mStylusBlockerConfig"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 289
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 290
    const-string v0, "mVersion = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 292
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 293
    const-string v0, "mEnable = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->isEnable()Z

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 295
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 296
    const-string v0, "mWeakModeDelayTime = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeDelayTime()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 298
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 299
    const-string v0, "mWeakModeMoveThreshold = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeMoveThreshold()F

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(F)V

    .line 301
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 302
    const-string v0, "mStrongModeDelayTime = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeDelayTime()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 304
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 305
    const-string v0, "mStrongModeMoveThreshold = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeMoveThreshold()F

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(F)V

    .line 307
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 308
    const-string v0, "mBlockedList.size = "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 309
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getBlockSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 310
    return-void
.end method

.method public onFocusedWindowChanged(Lcom/android/server/policy/WindowManagerPolicy$WindowState;)V
    .locals 2
    .param p1, "newFocus"    # Lcom/android/server/policy/WindowManagerPolicy$WindowState;

    .line 169
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    return-void

    .line 172
    :cond_0
    if-nez p1, :cond_1

    .line 174
    return-void

    .line 176
    :cond_1
    invoke-interface {p1}, Lcom/android/server/policy/WindowManagerPolicy$WindowState;->getOwningPackage()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "newFocusedPackageName":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 179
    return-void

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mCurrentFocusedPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 183
    return-void

    .line 185
    :cond_3
    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mCurrentFocusedPackageName:Ljava/lang/String;

    .line 186
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V

    .line 187
    return-void
.end method

.method public onStylusConnectionStateChanged(Z)V
    .locals 3
    .param p1, "connect"    # Z

    .line 160
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    return-void

    .line 163
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusConnected:Z

    .line 164
    sget-object v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stylus connect state changed, connect = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V

    .line 166
    return-void
.end method

.method public onSystemBooted()V
    .locals 2

    .line 75
    invoke-static {}, Lcom/miui/server/input/stylus/MiuiStylusUtils;->isSupportStylusPalmReject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    return-void

    .line 78
    :cond_0
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mContext:Landroid/content/Context;

    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/input/MiuiInputThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHandler:Landroid/os/Handler;

    .line 81
    new-instance v0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    invoke-direct {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusBlockerConfig:Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;

    .line 82
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    new-instance v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;

    iget-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mSettingsObserver:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;

    .line 85
    invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->observer()V

    .line 86
    return-void
.end method

.method public onUserSwitch(I)V
    .locals 2
    .param p1, "userId"    # I

    .line 89
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 93
    return-void
.end method

.method public setHasEditTextOnScreen(Z)V
    .locals 1
    .param p1, "hasEditText"    # Z

    .line 227
    invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    return-void

    .line 230
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z

    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateThresholdToNative(Z)V

    .line 233
    return-void
.end method
