public class com.miui.server.input.stylus.blocker.MiuiEventBlockerManager {
	 /* .source "MiuiEventBlockerManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String IS_IN_GAME_MODE_KEY;
private static final java.lang.String STYLUS_BLOCKER_CONFIG;
private static final java.lang.String SUPPORT_STYLUS_PALM_REJECT;
private static final java.lang.String TAG;
private static volatile com.miui.server.input.stylus.blocker.MiuiEventBlockerManager sInstance;
/* # instance fields */
private android.content.Context mContext;
private java.lang.String mCurrentFocusedPackageName;
private android.os.Handler mHandler;
private Boolean mHasEditText;
private Boolean mInGameMode;
private Boolean mNativeEnableState;
private Boolean mNativeStrongMode;
private com.miui.server.input.stylus.blocker.MiuiEventBlockerManager$SettingsObserver mSettingsObserver;
private com.miui.server.input.stylus.blocker.StylusBlockerConfig mStylusBlockerConfig;
private Boolean mStylusConnected;
/* # direct methods */
public static void $r8$lambda$YZNnOnJltxooROd27xU-OiPuLyc ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateSettingsConfig()V */
	 return;
} // .end method
public static void $r8$lambda$ouObh8eMr5ls_fLdRSamM8Ar9n0 ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->lambda$onSystemBooted$0()V */
	 return;
} // .end method
static android.content.Context -$$Nest$fgetmContext ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mContext;
} // .end method
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mHandler;
} // .end method
static void -$$Nest$mupdateCloudConfig ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateCloudConfig()V */
	 return;
} // .end method
static void -$$Nest$mupdateGameModeSettings ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateGameModeSettings()V */
	 return;
} // .end method
static void -$$Nest$mupdateSettingsConfig ( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateSettingsConfig()V */
	 return;
} // .end method
static com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ( ) {
	 /* .locals 1 */
	 /* .line 22 */
	 /* const-class v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager; */
	 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
	 return;
} // .end method
private com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ( ) {
	 /* .locals 1 */
	 /* .line 59 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 57 */
	 int v0 = 1; // const/4 v0, 0x1
	 /* iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z */
	 /* .line 61 */
	 return;
} // .end method
private Boolean getCurrentStylusBlockerState ( ) {
	 /* .locals 2 */
	 /* .line 198 */
	 v0 = this.mStylusBlockerConfig;
	 v0 = 	 (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).isEnable ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->isEnable()Z
	 int v1 = 0; // const/4 v1, 0x0
	 /* if-nez v0, :cond_0 */
	 /* .line 200 */
	 /* .line 202 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 204 */
	 /* .line 206 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusConnected:Z */
/* if-nez v0, :cond_2 */
/* .line 208 */
/* .line 211 */
} // :cond_2
v0 = this.mStylusBlockerConfig;
(( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getBlockSet ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getBlockSet()Ljava/util/Set;
v0 = v1 = this.mCurrentFocusedPackageName;
/* xor-int/lit8 v0, v0, 0x1 */
} // .end method
public static com.miui.server.input.stylus.blocker.MiuiEventBlockerManager getInstance ( ) {
/* .locals 2 */
/* .line 64 */
v0 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.sInstance;
/* if-nez v0, :cond_1 */
/* .line 65 */
/* const-class v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager; */
/* monitor-enter v0 */
/* .line 66 */
try { // :try_start_0
v1 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.sInstance;
/* if-nez v1, :cond_0 */
/* .line 67 */
/* new-instance v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager; */
/* invoke-direct {v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;-><init>()V */
/* .line 69 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 71 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.sInstance;
} // .end method
private Boolean isNotInit ( ) {
/* .locals 1 */
/* .line 255 */
v0 = this.mContext;
/* if-nez v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
private void lambda$onSystemBooted$0 ( ) { //synthethic
/* .locals 1 */
/* .line 83 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerUtils .getDefaultStylusBlockerConfig ( );
this.mStylusBlockerConfig = v0;
return;
} // .end method
private void updateCloudConfig ( ) {
/* .locals 5 */
/* .line 125 */
v0 = this.mContext;
/* .line 126 */
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
final String v3 = "STYLUS_BLOCKER"; // const-string v3, "STYLUS_BLOCKER"
android.provider.MiuiSettings$SettingsCloudData .getCloudDataSingle ( v0,v3,v1,v1,v2 );
/* .line 128 */
/* .local v0, "cloudData":Landroid/provider/MiuiSettings$SettingsCloudData$CloudData; */
if ( v0 != null) { // if-eqz v0, :cond_2
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
/* if-nez v1, :cond_0 */
/* .line 133 */
} // :cond_0
/* nop */
/* .line 134 */
(( android.provider.MiuiSettings$SettingsCloudData$CloudData ) v0 ).json ( ); // invoke-virtual {v0}, Landroid/provider/MiuiSettings$SettingsCloudData$CloudData;->json()Lorg/json/JSONObject;
com.miui.server.input.stylus.blocker.MiuiEventBlockerUtils .parseJsonToStylusBlockerConfig ( v1 );
/* .line 135 */
/* .local v1, "cloudConfig":Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig; */
v2 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v1 ).getVersion ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I
v3 = this.mStylusBlockerConfig;
v3 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v3 ).getVersion ( ); // invoke-virtual {v3}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I
/* if-ge v2, v3, :cond_1 */
/* .line 137 */
v2 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Do not use cloud config, because "; // const-string v4, "Do not use cloud config, because "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 138 */
v4 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v1 ).getVersion ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " < "; // const-string v4, " < "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = this.mStylusBlockerConfig;
/* .line 139 */
v4 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v4 ).getVersion ( ); // invoke-virtual {v4}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 137 */
android.util.Slog .w ( v2,v3 );
/* .line 140 */
return;
/* .line 143 */
} // :cond_1
this.mStylusBlockerConfig = v1;
/* .line 144 */
v2 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
final String v3 = "Use cloud config."; // const-string v3, "Use cloud config."
android.util.Slog .w ( v2,v3 );
/* .line 130 */
} // .end local v1 # "cloudConfig":Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;
} // :cond_2
} // :goto_0
v1 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
final String v2 = "Not has cloud config, use default"; // const-string v2, "Not has cloud config, use default"
android.util.Slog .w ( v1,v2 );
/* .line 147 */
} // :goto_1
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V */
/* .line 149 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateThresholdToNative(Z)V */
/* .line 150 */
return;
} // .end method
private void updateEnableStateToNative ( ) {
/* .locals 4 */
/* .line 215 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getCurrentStylusBlockerState()Z */
/* .line 216 */
/* .local v0, "newState":Z */
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z */
/* if-ne v1, v0, :cond_0 */
/* .line 217 */
return;
/* .line 219 */
} // :cond_0
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z */
/* .line 220 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z */
(( com.android.server.input.config.InputCommonConfig ) v1 ).setStylusBlockerEnable ( v2 ); // invoke-virtual {v1, v2}, Lcom/android/server/input/config/InputCommonConfig;->setStylusBlockerEnable(Z)V
/* .line 221 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
(( com.android.server.input.config.InputCommonConfig ) v1 ).flushToNative ( ); // invoke-virtual {v1}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 222 */
v1 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Flush enable state to native, enable = "; // const-string v3, "Flush enable state to native, enable = "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v1,v2 );
/* .line 223 */
return;
} // .end method
private void updateGameModeSettings ( ) {
/* .locals 4 */
/* .line 153 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v1 = -2; // const/4 v1, -0x2
final String v2 = "gb_boosting"; // const-string v2, "gb_boosting"
int v3 = 0; // const/4 v3, 0x0
v0 = android.provider.Settings$Secure .getIntForUser ( v0,v2,v3,v1 );
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_0 */
/* move v3, v1 */
} // :cond_0
/* iput-boolean v3, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z */
/* .line 155 */
v0 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Update game mode settings, current is in game mode "; // const-string v2, "Update game mode settings, current is in game mode "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 156 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V */
/* .line 157 */
return;
} // .end method
private void updateSettingsConfig ( ) {
/* .locals 0 */
/* .line 120 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateCloudConfig()V */
/* .line 121 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateGameModeSettings()V */
/* .line 122 */
return;
} // .end method
private void updateThresholdToNative ( Boolean p0 ) {
/* .locals 5 */
/* .param p1, "force" # Z */
/* .line 236 */
/* if-nez p1, :cond_0 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z */
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z */
/* if-ne v0, v1, :cond_0 */
/* .line 237 */
return;
/* .line 239 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z */
/* .line 240 */
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getStrongModeDelayTime ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeDelayTime()I
/* .line 241 */
} // :cond_1
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getWeakModeDelayTime ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeDelayTime()I
} // :goto_0
/* nop */
/* .line 242 */
/* .local v0, "delayTime":I */
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z */
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = this.mStylusBlockerConfig;
v1 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v1 ).getStrongModeMoveThreshold ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeMoveThreshold()F
/* .line 243 */
} // :cond_2
v1 = this.mStylusBlockerConfig;
v1 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v1 ).getWeakModeMoveThreshold ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeMoveThreshold()F
} // :goto_1
/* nop */
/* .line 244 */
/* .local v1, "moveThreshold":F */
v2 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Flush delay time and move threshold to native, delayTime = "; // const-string v4, "Flush delay time and move threshold to native, delayTime = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = ", modeThreshold = "; // const-string v4, ", modeThreshold = "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v3 );
/* .line 246 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
(( com.android.server.input.config.InputCommonConfig ) v2 ).setStylusBlockerDelayTime ( v0 ); // invoke-virtual {v2, v0}, Lcom/android/server/input/config/InputCommonConfig;->setStylusBlockerDelayTime(I)V
/* .line 247 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
(( com.android.server.input.config.InputCommonConfig ) v2 ).setStylusBlockerMoveThreshold ( v1 ); // invoke-virtual {v2, v1}, Lcom/android/server/input/config/InputCommonConfig;->setStylusBlockerMoveThreshold(F)V
/* .line 248 */
com.android.server.input.config.InputCommonConfig .getInstance ( );
(( com.android.server.input.config.InputCommonConfig ) v2 ).flushToNative ( ); // invoke-virtual {v2}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 249 */
return;
} // .end method
/* # virtual methods */
public void dump ( java.lang.String p0, java.io.PrintWriter p1 ) {
/* .locals 2 */
/* .param p1, "prefix" # Ljava/lang/String; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .line 259 */
final String v0 = " "; // const-string v0, " "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 260 */
v0 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 261 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 262 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 263 */
final String v0 = "not support"; // const-string v0, "not support"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 264 */
return;
/* .line 266 */
} // :cond_0
final String v0 = "init = "; // const-string v0, "init = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 267 */
v0 = this.mContext;
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 1; // const/4 v0, 0x1
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 268 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 269 */
final String v0 = "mCurrentFocusedWindow = "; // const-string v0, "mCurrentFocusedWindow = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 270 */
v0 = this.mCurrentFocusedPackageName;
/* if-nez v0, :cond_2 */
final String v0 = "null"; // const-string v0, "null"
} // :cond_2
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 271 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 272 */
final String v0 = "mStylusConnected = "; // const-string v0, "mStylusConnected = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 273 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusConnected:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 274 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 275 */
final String v0 = "mHasEditText = "; // const-string v0, "mHasEditText = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 276 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 277 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 278 */
final String v0 = "mInGameMode = "; // const-string v0, "mInGameMode = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 279 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mInGameMode:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 280 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 281 */
final String v0 = "mNativeEnableState = "; // const-string v0, "mNativeEnableState = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 282 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeEnableState:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 283 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 284 */
final String v0 = "mNativeStrongMode = "; // const-string v0, "mNativeStrongMode = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 285 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mNativeStrongMode:Z */
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 286 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 287 */
final String v0 = "mStylusBlockerConfig"; // const-string v0, "mStylusBlockerConfig"
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 288 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " "; // const-string v1, " "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 289 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 290 */
final String v0 = "mVersion = "; // const-string v0, "mVersion = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 291 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getVersion ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getVersion()I
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 292 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 293 */
final String v0 = "mEnable = "; // const-string v0, "mEnable = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 294 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).isEnable ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->isEnable()Z
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V
/* .line 295 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 296 */
final String v0 = "mWeakModeDelayTime = "; // const-string v0, "mWeakModeDelayTime = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 297 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getWeakModeDelayTime ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeDelayTime()I
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 298 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 299 */
final String v0 = "mWeakModeMoveThreshold = "; // const-string v0, "mWeakModeMoveThreshold = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 300 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getWeakModeMoveThreshold ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getWeakModeMoveThreshold()F
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(F)V
/* .line 301 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 302 */
final String v0 = "mStrongModeDelayTime = "; // const-string v0, "mStrongModeDelayTime = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 303 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getStrongModeDelayTime ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeDelayTime()I
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 304 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 305 */
final String v0 = "mStrongModeMoveThreshold = "; // const-string v0, "mStrongModeMoveThreshold = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 306 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getStrongModeMoveThreshold ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getStrongModeMoveThreshold()F
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(F)V
/* .line 307 */
(( java.io.PrintWriter ) p2 ).print ( p1 ); // invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 308 */
final String v0 = "mBlockedList.size = "; // const-string v0, "mBlockedList.size = "
(( java.io.PrintWriter ) p2 ).print ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
/* .line 309 */
v0 = this.mStylusBlockerConfig;
v0 = (( com.miui.server.input.stylus.blocker.StylusBlockerConfig ) v0 ).getBlockSet ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;->getBlockSet()Ljava/util/Set;
(( java.io.PrintWriter ) p2 ).println ( v0 ); // invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V
/* .line 310 */
return;
} // .end method
public void onFocusedWindowChanged ( com.android.server.policy.WindowManagerPolicy$WindowState p0 ) {
/* .locals 2 */
/* .param p1, "newFocus" # Lcom/android/server/policy/WindowManagerPolicy$WindowState; */
/* .line 169 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 170 */
return;
/* .line 172 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 174 */
return;
/* .line 176 */
} // :cond_1
/* .line 177 */
/* .local v0, "newFocusedPackageName":Ljava/lang/String; */
/* if-nez v0, :cond_2 */
/* .line 179 */
return;
/* .line 181 */
} // :cond_2
v1 = this.mCurrentFocusedPackageName;
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 183 */
return;
/* .line 185 */
} // :cond_3
this.mCurrentFocusedPackageName = v0;
/* .line 186 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V */
/* .line 187 */
return;
} // .end method
public void onStylusConnectionStateChanged ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "connect" # Z */
/* .line 160 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 161 */
return;
/* .line 163 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mStylusConnected:Z */
/* .line 164 */
v0 = com.miui.server.input.stylus.blocker.MiuiEventBlockerManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Stylus connect state changed, connect = "; // const-string v2, "Stylus connect state changed, connect = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v0,v1 );
/* .line 165 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateEnableStateToNative()V */
/* .line 166 */
return;
} // .end method
public void onSystemBooted ( ) {
/* .locals 2 */
/* .line 75 */
v0 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportStylusPalmReject ( );
/* if-nez v0, :cond_0 */
/* .line 76 */
return;
/* .line 78 */
} // :cond_0
android.app.ActivityThread .currentActivityThread ( );
(( android.app.ActivityThread ) v0 ).getSystemContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
this.mContext = v0;
/* .line 79 */
/* new-instance v0, Landroid/os/Handler; */
com.android.server.input.MiuiInputThread .getHandler ( );
(( android.os.Handler ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 81 */
/* new-instance v0, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig; */
/* invoke-direct {v0}, Lcom/miui/server/input/stylus/blocker/StylusBlockerConfig;-><init>()V */
this.mStylusBlockerConfig = v0;
/* .line 82 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 84 */
/* new-instance v0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver; */
v1 = this.mHandler;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;Landroid/os/Handler;)V */
this.mSettingsObserver = v0;
/* .line 85 */
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager$SettingsObserver ) v0 ).observer ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->observer()V
/* .line 86 */
return;
} // .end method
public void onUserSwitch ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "userId" # I */
/* .line 89 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 90 */
return;
/* .line 92 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 93 */
return;
} // .end method
public void setHasEditTextOnScreen ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "hasEditText" # Z */
/* .line 227 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->isNotInit()Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 228 */
return;
/* .line 230 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->mHasEditText:Z */
/* .line 232 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->updateThresholdToNative(Z)V */
/* .line 233 */
return;
} // .end method
