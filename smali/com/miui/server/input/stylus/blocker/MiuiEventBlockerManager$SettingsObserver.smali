.class final Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;
.super Landroid/database/ContentObserver;
.source "MiuiEventBlockerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;


# direct methods
.method public static synthetic $r8$lambda$YZNnOnJltxooROd27xU-OiPuLyc(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->-$$Nest$mupdateSettingsConfig(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V

    return-void
.end method

.method public constructor <init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 96
    iput-object p1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    .line 97
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 98
    return-void
.end method


# virtual methods
.method public observer()V
    .locals 4

    .line 101
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->-$$Nest$fgetmContext(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 102
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 104
    const-string v1, "gb_boosting"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 106
    iget-object v1, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    invoke-static {v1}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->-$$Nest$fgetmHandler(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    new-instance v3, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver$$ExternalSyntheticLambda0;

    invoke-direct {v3, v2}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 107
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 1
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 111
    invoke-static {}, Landroid/provider/MiuiSettings$SettingsCloudData;->getCloudDataNotifyUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->-$$Nest$mupdateCloudConfig(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V

    goto :goto_0

    .line 113
    :cond_0
    const-string v0, "gb_boosting"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager$SettingsObserver;->this$0:Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->-$$Nest$mupdateGameModeSettings(Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;)V

    .line 116
    :cond_1
    :goto_0
    return-void
.end method
