.class public Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;
.super Ljava/lang/Object;
.source "MiuiStylusDeviceListener.java"

# interfaces
.implements Landroid/hardware/input/InputManager$InputDeviceListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "MiuiStylusDeviceListener"


# instance fields
.field private final mInputManager:Landroid/hardware/input/InputManager;

.field private final mStylusDeviceList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mTouchFeature:Lmiui/util/ITouchFeature;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    .line 22
    const-string v0, "input"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mInputManager:Landroid/hardware/input/InputManager;

    .line 23
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    .line 24
    invoke-static {}, Lmiui/util/ITouchFeature;->getInstance()Lmiui/util/ITouchFeature;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mTouchFeature:Lmiui/util/ITouchFeature;

    .line 26
    const/16 v1, 0x14

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v1, v2}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 28
    return-void
.end method


# virtual methods
.method public onInputDeviceAdded(I)V
    .locals 6
    .param p1, "i"    # I

    .line 39
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mInputManager:Landroid/hardware/input/InputManager;

    invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    .line 40
    .local v0, "inputDevice":Landroid/view/InputDevice;
    if-nez v0, :cond_0

    .line 41
    return-void

    .line 43
    :cond_0
    invoke-virtual {v0}, Landroid/view/InputDevice;->isXiaomiStylus()I

    move-result v1

    .line 44
    .local v1, "isXiaomiStylus":I
    if-lez v1, :cond_2

    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 45
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onStylusConnectionStateChanged(Z)V

    .line 48
    :cond_1
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    or-int/lit8 v2, v1, 0x10

    .line 50
    .local v2, "value":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stylus device add device id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiStylusDeviceListener"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-object v3, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mTouchFeature:Lmiui/util/ITouchFeature;

    const/4 v4, 0x0

    const/16 v5, 0x14

    invoke-virtual {v3, v4, v5, v2}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 55
    .end local v2    # "value":I
    :cond_2
    return-void
.end method

.method public onInputDeviceChanged(I)V
    .locals 0
    .param p1, "i"    # I

    .line 77
    return-void
.end method

.method public onInputDeviceRemoved(I)V
    .locals 5
    .param p1, "i"    # I

    .line 59
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 61
    .local v0, "isXiaomiStylus":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 62
    return-void

    .line 64
    :cond_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 65
    invoke-static {}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->getInstance()Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onStylusConnectionStateChanged(Z)V

    .line 67
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stylus device remove device id : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " version : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", stylus have "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mStylusDeviceList:Ljava/util/Map;

    .line 68
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " connected"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 67
    const-string v3, "MiuiStylusDeviceListener"

    invoke-static {v3, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusDeviceListener;->mTouchFeature:Lmiui/util/ITouchFeature;

    .line 70
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 69
    const/16 v4, 0x14

    invoke-virtual {v1, v2, v4, v3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z

    .line 72
    .end local v0    # "isXiaomiStylus":Ljava/lang/Integer;
    :cond_2
    return-void
.end method
