.class Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;
.super Landroid/os/Handler;
.source "MiuiStylusShortcutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field private static final MSG_HALL:I = 0x6

.field private static final MSG_KEY_DISMISS_WITHOUT_ANIMATION:I = 0x3

.field private static final MSG_KEY_DISMISS_WITH_ANIMATION:I = 0x4

.field private static final MSG_OFF_SCREEN_QUICK_NOTE:I = 0x5

.field private static final MSG_QUICK_NOTE_KEY_LONG_PRESS_ACTION:I = 0x1

.field private static final MSG_SCREEN_SHOT_KEY_LONG_PRESS_ACTION:I = 0x2


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 762
    iput-object p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    .line 763
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 764
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 768
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 787
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 788
    .local v0, "hall":I
    iget-object v1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v1, v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mputHallSettings(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;I)V

    .line 789
    goto :goto_0

    .line 780
    .end local v0    # "hall":I
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mtriggerNoteWhenScreenOff(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 781
    goto :goto_0

    .line 777
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mremoveStylusMaskWindow(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V

    .line 778
    goto :goto_0

    .line 774
    :pswitch_3
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mremoveStylusMaskWindow(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V

    .line 775
    goto :goto_0

    .line 783
    :pswitch_4
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fputmScreenShotKeyFunctionTriggered(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V

    .line 784
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$mtriggerScreenShotKeyFunction(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 785
    goto :goto_0

    .line 770
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fputmQuickNoteKeyFunctionTriggered(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;Z)V

    .line 771
    iget-object v0, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$H;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$maddStylusMaskWindow(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 772
    nop

    .line 793
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
