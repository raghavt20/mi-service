public class com.miui.server.input.stylus.MiuiStylusUtils {
	 /* .source "MiuiStylusUtils.java" */
	 /* # static fields */
	 private static final java.lang.String DEFAULT_STYLUS_PAGE_KEY_BW_CONFIG_FILE_PATH;
	 private static final java.lang.String SUPPORT_STYLUS_PALM_REJECT;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.miui.server.input.stylus.MiuiStylusUtils ( ) {
		 /* .locals 1 */
		 /* .line 20 */
		 /* const-class v0, Lcom/miui/server/input/stylus/MiuiStylusUtils; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 return;
	 } // .end method
	 private com.miui.server.input.stylus.MiuiStylusUtils ( ) {
		 /* .locals 0 */
		 /* .line 25 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 27 */
		 return;
	 } // .end method
	 public static com.miui.server.input.stylus.StylusPageKeyConfig getDefaultStylusPageKeyConfig ( ) {
		 /* .locals 4 */
		 /* .line 71 */
		 /* new-instance v0, Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
		 /* invoke-direct {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;-><init>()V */
		 /* .line 73 */
		 /* .local v0, "config":Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
		 try { // :try_start_0
			 final String v1 = "/system_ext/etc/input/stylus_page_key_bw_config.json"; // const-string v1, "/system_ext/etc/input/stylus_page_key_bw_config.json"
			 com.miui.server.input.stylus.MiuiStylusUtils .readFileToString ( v1 );
			 /* .line 74 */
			 /* .local v1, "configString":Ljava/lang/String; */
			 v2 = 			 (( java.lang.String ) v1 ).length ( ); // invoke-virtual {v1}, Ljava/lang/String;->length()I
			 /* if-nez v2, :cond_0 */
			 /* .line 76 */
			 /* .line 78 */
		 } // :cond_0
		 /* new-instance v2, Lorg/json/JSONObject; */
		 /* invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
		 com.miui.server.input.stylus.MiuiStylusUtils .parseJsonToStylusPageKeyConfigInternal ( v0,v2 );
		 /* :try_end_0 */
		 /* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
		 /* .line 81 */
	 } // .end local v1 # "configString":Ljava/lang/String;
	 /* .line 79 */
	 /* :catch_0 */
	 /* move-exception v1 */
	 /* .line 80 */
	 /* .local v1, "e":Lorg/json/JSONException; */
	 v2 = com.miui.server.input.stylus.MiuiStylusUtils.TAG;
	 final String v3 = "parse config failed!!!"; // const-string v3, "parse config failed!!!"
	 android.util.Slog .e ( v2,v3,v1 );
	 /* .line 82 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
} // .end method
public static Boolean isSupportOffScreenQuickNote ( ) {
/* .locals 2 */
/* .line 41 */
/* const-string/jumbo v0, "stylus_quick_note" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
} // .end method
public static Boolean isSupportStylusPalmReject ( ) {
/* .locals 2 */
/* .line 48 */
final String v0 = "persist.stylus.palm.reject"; // const-string v0, "persist.stylus.palm.reject"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
public static com.miui.server.input.stylus.StylusPageKeyConfig parseJsonToStylusPageKeyConfig ( org.json.JSONObject p0 ) {
/* .locals 4 */
/* .param p0, "object" # Lorg/json/JSONObject; */
/* .line 86 */
/* new-instance v0, Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
/* invoke-direct {v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;-><init>()V */
/* .line 88 */
/* .local v0, "config":Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
try { // :try_start_0
com.miui.server.input.stylus.MiuiStylusUtils .parseJsonToStylusPageKeyConfigInternal ( v0,p0 );
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 91 */
/* .line 89 */
/* :catch_0 */
/* move-exception v1 */
/* .line 90 */
/* .local v1, "e":Lorg/json/JSONException; */
v2 = com.miui.server.input.stylus.MiuiStylusUtils.TAG;
final String v3 = "parse config failed!!!"; // const-string v3, "parse config failed!!!"
android.util.Slog .e ( v2,v3,v1 );
/* .line 92 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :goto_0
} // .end method
private static void parseJsonToStylusPageKeyConfigInternal ( com.miui.server.input.stylus.StylusPageKeyConfig p0, org.json.JSONObject p1 ) {
/* .locals 7 */
/* .param p0, "config" # Lcom/miui/server/input/stylus/StylusPageKeyConfig; */
/* .param p1, "jsonObject" # Lorg/json/JSONObject; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 97 */
/* const-string/jumbo v0, "version" */
v0 = (( org.json.JSONObject ) p1 ).getInt ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
(( com.miui.server.input.stylus.StylusPageKeyConfig ) p0 ).setVersion ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setVersion(I)V
/* .line 98 */
final String v0 = "enable"; // const-string v0, "enable"
v0 = (( org.json.JSONObject ) p1 ).getBoolean ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
(( com.miui.server.input.stylus.StylusPageKeyConfig ) p0 ).setEnable ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setEnable(Z)V
/* .line 99 */
final String v0 = "appWhiteList"; // const-string v0, "appWhiteList"
(( org.json.JSONObject ) p1 ).getJSONArray ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 100 */
/* .local v0, "appWhiteList":Lorg/json/JSONArray; */
final String v1 = "activityBlackList"; // const-string v1, "activityBlackList"
(( org.json.JSONObject ) p1 ).getJSONArray ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 101 */
/* .local v1, "activityBlackList":Lorg/json/JSONArray; */
/* new-instance v2, Ljava/util/HashSet; */
/* invoke-direct {v2}, Ljava/util/HashSet;-><init>()V */
/* .line 102 */
/* .local v2, "appWhiteSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
/* new-instance v3, Ljava/util/HashSet; */
/* invoke-direct {v3}, Ljava/util/HashSet;-><init>()V */
/* .line 103 */
/* .local v3, "activityBlackSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;" */
int v4 = 0; // const/4 v4, 0x0
/* .local v4, "i":I */
v5 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* .local v5, "size":I */
} // :goto_0
/* if-ge v4, v5, :cond_0 */
/* .line 104 */
(( org.json.JSONArray ) v0 ).getString ( v4 ); // invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 103 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 106 */
} // .end local v4 # "i":I
} // .end local v5 # "size":I
} // :cond_0
int v4 = 0; // const/4 v4, 0x0
/* .restart local v4 # "i":I */
v5 = (( org.json.JSONArray ) v1 ).length ( ); // invoke-virtual {v1}, Lorg/json/JSONArray;->length()I
/* .restart local v5 # "size":I */
} // :goto_1
/* if-ge v4, v5, :cond_1 */
/* .line 107 */
(( org.json.JSONArray ) v1 ).getString ( v4 ); // invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 106 */
/* add-int/lit8 v4, v4, 0x1 */
/* .line 109 */
} // .end local v4 # "i":I
} // .end local v5 # "size":I
} // :cond_1
(( com.miui.server.input.stylus.StylusPageKeyConfig ) p0 ).setAppWhiteSet ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setAppWhiteSet(Ljava/util/Set;)V
/* .line 110 */
(( com.miui.server.input.stylus.StylusPageKeyConfig ) p0 ).setActivityBlackSet ( v3 ); // invoke-virtual {p0, v3}, Lcom/miui/server/input/stylus/StylusPageKeyConfig;->setActivityBlackSet(Ljava/util/Set;)V
/* .line 111 */
return;
} // .end method
private static java.lang.String readFileToString ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p0, "filePath" # Ljava/lang/String; */
/* .line 53 */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 54 */
/* .local v0, "file":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_2
v1 = (( java.io.File ) v0 ).isFile ( ); // invoke-virtual {v0}, Ljava/io/File;->isFile()Z
/* if-nez v1, :cond_0 */
/* .line 58 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 59 */
/* .local v1, "result":Ljava/lang/StringBuilder; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 61 */
/* .local v2, "bufferedReader":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 62 */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 64 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_1
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 66 */
} // .end local v2 # "bufferedReader":Ljava/io/BufferedReader;
/* .line 59 */
/* .restart local v2 # "bufferedReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "file":Ljava/io/File;
} // .end local v1 # "result":Ljava/lang/StringBuilder;
} // .end local p0 # "filePath":Ljava/lang/String;
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 64 */
} // .end local v2 # "bufferedReader":Ljava/io/BufferedReader;
/* .restart local v0 # "file":Ljava/io/File; */
/* .restart local v1 # "result":Ljava/lang/StringBuilder; */
/* .restart local p0 # "filePath":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 65 */
/* .local v2, "e":Ljava/lang/Exception; */
v3 = com.miui.server.input.stylus.MiuiStylusUtils.TAG;
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "file("; // const-string v5, "file("
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = ") load fail"; // const-string v5, ") load fail"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4,v2 );
/* .line 67 */
} // .end local v2 # "e":Ljava/lang/Exception;
} // :goto_2
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 55 */
} // .end local v1 # "result":Ljava/lang/StringBuilder;
} // :cond_2
} // :goto_3
v1 = com.miui.server.input.stylus.MiuiStylusUtils.TAG;
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v3, "where is my file (" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "), I can\'t find it :("; // const-string v3, "), I can\'t find it :("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 56 */
final String v1 = ""; // const-string v1, ""
} // .end method
public static Boolean supportStylusGesture ( ) {
/* .locals 2 */
/* .line 34 */
/* const-string/jumbo v0, "support_stylus_gesture" */
int v1 = 0; // const/4 v1, 0x0
v0 = miui.util.FeatureParser .getBoolean ( v0,v1 );
} // .end method
