class com.miui.server.input.stylus.MiuiStylusShortcutManager$MiuiSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiStylusShortcutManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "MiuiSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.stylus.MiuiStylusShortcutManager this$0; //synthetic
/* # direct methods */
public static void $r8$lambda$w9kMM0A6URLpStvwNQDoduK_zRM ( com.miui.server.input.stylus.MiuiStylusShortcutManager p0 ) { //synthethic
/* .locals 0 */
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mupdateCloudConfig ( p0 );
return;
} // .end method
 com.miui.server.input.stylus.MiuiStylusShortcutManager$MiuiSettingsObserver ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 797 */
this.this$0 = p1;
/* .line 798 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 799 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 802 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mupdateGameModeSettings ( v0 );
/* .line 803 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fgetmHandler ( v0 );
v1 = this.this$0;
/* new-instance v2, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver$$ExternalSyntheticLambda0; */
/* invoke-direct {v2, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$MiuiSettingsObserver$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V */
(( android.os.Handler ) v0 ).post ( v2 ); // invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 804 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 805 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "gb_boosting"; // const-string v1, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 807 */
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
int v2 = 1; // const/4 v2, 0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 809 */
v1 = com.miui.server.input.stylus.MiuiStylusUtils .isSupportOffScreenQuickNote ( );
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 810 */
	 /* nop */
	 /* .line 811 */
	 /* const-string/jumbo v1, "stylus_quick_note_screen_off" */
	 android.provider.Settings$System .getUriFor ( v1 );
	 /* .line 810 */
	 (( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
	 /* .line 813 */
	 v1 = this.this$0;
	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mupdateStylusScreenOffQuickNoteSettings ( v1 );
	 /* .line 815 */
} // :cond_0
return;
} // .end method
public void onChange ( Boolean p0, android.net.Uri p1 ) {
/* .locals 2 */
/* .param p1, "selfChange" # Z */
/* .param p2, "uri" # Landroid/net/Uri; */
/* .line 819 */
final String v0 = "gb_boosting"; // const-string v0, "gb_boosting"
android.provider.Settings$Secure .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 820 */
	 v0 = this.this$0;
	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mupdateGameModeSettings ( v0 );
	 /* .line 821 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "game_booster changed, isGameMode = "; // const-string v1, "game_booster changed, isGameMode = "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 v1 = this.this$0;
	 v1 = 	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fgetmIsGameMode ( v1 );
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 final String v1 = "MiuiStylusShortcutManager"; // const-string v1, "MiuiStylusShortcutManager"
	 android.util.Slog .d ( v1,v0 );
	 /* .line 822 */
} // :cond_0
android.provider.MiuiSettings$SettingsCloudData .getCloudDataNotifyUri ( );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 /* .line 823 */
	 v0 = this.this$0;
	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mupdateCloudConfig ( v0 );
	 /* .line 824 */
} // :cond_1
/* const-string/jumbo v0, "stylus_quick_note_screen_off" */
android.provider.Settings$System .getUriFor ( v0 );
v0 = (( android.net.Uri ) v0 ).equals ( p2 ); // invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_2
	 /* .line 825 */
	 v0 = this.this$0;
	 com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mupdateStylusScreenOffQuickNoteSettings ( v0 );
	 /* .line 827 */
} // :cond_2
} // :goto_0
return;
} // .end method
