.class Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;
.super Landroid/os/UEventObserver;
.source "MiuiStylusShortcutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;


# direct methods
.method constructor <init>(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    .line 152
    iput-object p1, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/os/UEventObserver$UEvent;

    .line 155
    const-string v0, "POWER_SUPPLY_PEN_HALL3"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "hall3_str":Ljava/lang/String;
    const-string v1, "POWER_SUPPLY_PEN_HALL4"

    invoke-virtual {p1, v1}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "hall4_str":Ljava/lang/String;
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 158
    iget-object v2, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-virtual {v2, v0}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 159
    .local v2, "hall3":I
    iget-object v3, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-virtual {v3, v1}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 160
    .local v3, "hall4":I
    const/4 v4, 0x1

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v5}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fgetmHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 161
    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fputmHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;I)V

    .line 162
    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v4}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$msetHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    goto :goto_0

    .line 163
    :cond_1
    if-ne v2, v4, :cond_2

    if-ne v3, v4, :cond_2

    iget-object v5, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v5}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fgetmHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)I

    move-result v5

    if-nez v5, :cond_2

    .line 164
    iget-object v5, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v5, v4}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$fputmHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;I)V

    .line 165
    iget-object v4, p0, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager$1;->this$0:Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;

    invoke-static {v4}, Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;->-$$Nest$msetHallStatus(Lcom/miui/server/input/stylus/MiuiStylusShortcutManager;)V

    .line 168
    .end local v2    # "hall3":I
    .end local v3    # "hall4":I
    :cond_2
    :goto_0
    return-void
.end method
