.class public Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;
.super Lcom/miui/server/input/stylus/BaseStylusGestureChecker;
.source "StylusGestureLowerLeft.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "StylusGestureLowerLeft"


# instance fields
.field private final AREA_X:I

.field private final AREA_Y:I

.field private mInitMotionX:F

.field private mInitMotionY:F

.field private mTableGestureState:I

.field private mTableGestureThreshold_X:D

.field private mTableGestureThreshold_Y:D

.field private final mTouchSlop:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 27
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/BaseStylusGestureChecker;-><init>(Landroid/content/Context;)V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_Y:I

    iput v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_X:I

    .line 29
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTouchSlop:F

    .line 30
    return-void
.end method

.method private changeTableGestureState(I)V
    .locals 0
    .param p1, "newState"    # I

    .line 97
    iput p1, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureState:I

    .line 98
    return-void
.end method

.method private checkIsStartPosition(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 62
    iget-object v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 63
    .local v0, "display":Landroid/view/Display;
    iget-object v1, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 64
    .local v1, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 65
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 66
    .local v2, "height":I
    const/high16 v3, 0x41f00000    # 30.0f

    iget v4, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v3

    float-to-double v3, v4

    iput-wide v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_X:D

    .line 67
    int-to-double v5, v2

    sub-double/2addr v5, v3

    iput-wide v5, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_Y:D

    .line 68
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F

    .line 69
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iput v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionY:F

    .line 70
    iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F

    iget v5, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_X:I

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gtz v4, :cond_0

    iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_Y:I

    sub-int v4, v2, v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " InitMotionX : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , InitMotionY : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionY:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " successful "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "StylusGestureLowerLeft"

    invoke-static {v4, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v3, 0x1

    return v3

    .line 75
    :cond_0
    const/4 v3, 0x0

    return v3
.end method

.method private checkIsStylusGesture(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 79
    iget v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 80
    return-void

    .line 82
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 83
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 84
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 85
    .local v1, "y":F
    iget v2, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F

    iget v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionY:F

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->dist(FFFF)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTouchSlop:F

    mul-float/2addr v4, v3

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_1

    .line 86
    const-string v2, "StylusGestureLowerLeft"

    const-string v3, "PilferPointers cancel event because of three stylus gesture detecting"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v2, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mMiuiGestureMonitor:Lcom/miui/server/input/gesture/MiuiGestureMonitor;

    invoke-virtual {v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->pilferPointers()V

    .line 89
    :cond_1
    float-to-double v2, v0

    iget-wide v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_X:D

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_2

    float-to-double v2, v1

    iget-wide v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_Y:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_2

    .line 90
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V

    .line 91
    invoke-direct {p0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->takeScreenshot()V

    .line 94
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_2
    return-void
.end method

.method private checkLowerLeftGesture(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .line 43
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V

    goto :goto_0

    .line 45
    :cond_0
    iget v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureState:I

    if-nez v0, :cond_2

    .line 46
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->checkIsStartPosition(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V

    goto :goto_0

    .line 49
    :cond_1
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V

    .line 52
    :cond_2
    :goto_0
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->checkIsStylusGesture(Landroid/view/MotionEvent;)V

    .line 53
    return-void
.end method

.method private dist(FFFF)F
    .locals 6
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F

    .line 106
    sub-float v0, p3, p1

    .line 107
    .local v0, "x":F
    sub-float v1, p4, p2

    .line 108
    .local v1, "y":F
    float-to-double v2, v0

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method private takeScreenshot()V
    .locals 5

    .line 101
    iget-object v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/server/input/util/ShortCutActionsUtils;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/util/ShortCutActionsUtils;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string/jumbo v3, "screenshot_without_anim"

    const-string/jumbo v4, "stylus_screen_lower_left"

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z

    .line 103
    return-void
.end method


# virtual methods
.method public onPointerEvent(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 34
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->checkLowerLeftGesture(Landroid/view/MotionEvent;)V

    .line 35
    return-void
.end method
