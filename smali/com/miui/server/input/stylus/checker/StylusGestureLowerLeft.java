public class com.miui.server.input.stylus.checker.StylusGestureLowerLeft extends com.miui.server.input.stylus.BaseStylusGestureChecker {
	 /* .source "StylusGestureLowerLeft.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final Integer AREA_X;
	 private final Integer AREA_Y;
	 private Float mInitMotionX;
	 private Float mInitMotionY;
	 private Integer mTableGestureState;
	 private Double mTableGestureThreshold_X;
	 private Double mTableGestureThreshold_Y;
	 private final Float mTouchSlop;
	 /* # direct methods */
	 public com.miui.server.input.stylus.checker.StylusGestureLowerLeft ( ) {
		 /* .locals 2 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 27 */
		 /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/BaseStylusGestureChecker;-><init>(Landroid/content/Context;)V */
		 /* .line 28 */
		 (( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 (( android.content.res.Resources ) v0 ).getDisplayMetrics ( ); // invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
		 /* iget v0, v0, Landroid/util/DisplayMetrics;->density:F */
		 /* const/high16 v1, 0x41200000 # 10.0f */
		 /* mul-float/2addr v0, v1 */
		 /* float-to-int v0, v0 */
		 /* iput v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_Y:I */
		 /* iput v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_X:I */
		 /* .line 29 */
		 android.view.ViewConfiguration .get ( p1 );
		 v0 = 		 (( android.view.ViewConfiguration ) v0 ).getScaledTouchSlop ( ); // invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I
		 /* int-to-float v0, v0 */
		 /* iput v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTouchSlop:F */
		 /* .line 30 */
		 return;
	 } // .end method
	 private void changeTableGestureState ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "newState" # I */
		 /* .line 97 */
		 /* iput p1, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureState:I */
		 /* .line 98 */
		 return;
	 } // .end method
	 private Boolean checkIsStartPosition ( android.view.MotionEvent p0 ) {
		 /* .locals 7 */
		 /* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
		 /* .line 62 */
		 v0 = this.mWindowManager;
		 /* .line 63 */
		 /* .local v0, "display":Landroid/view/Display; */
		 v1 = this.mContext;
		 (( android.content.Context ) v1 ).getResources ( ); // invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
		 (( android.content.res.Resources ) v1 ).getDisplayMetrics ( ); // invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;
		 /* .line 64 */
		 /* .local v1, "dm":Landroid/util/DisplayMetrics; */
		 (( android.view.Display ) v0 ).getRealMetrics ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V
		 /* .line 65 */
		 /* iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I */
		 /* .line 66 */
		 /* .local v2, "height":I */
		 /* const/high16 v3, 0x41f00000 # 30.0f */
		 /* iget v4, v1, Landroid/util/DisplayMetrics;->density:F */
		 /* mul-float/2addr v4, v3 */
		 /* float-to-double v3, v4 */
		 /* iput-wide v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_X:D */
		 /* .line 67 */
		 /* int-to-double v5, v2 */
		 /* sub-double/2addr v5, v3 */
		 /* iput-wide v5, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_Y:D */
		 /* .line 68 */
		 v3 = 		 (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
		 /* iput v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F */
		 /* .line 69 */
		 v3 = 		 (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
		 /* iput v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionY:F */
		 /* .line 70 */
		 /* iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F */
		 /* iget v5, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_X:I */
		 /* int-to-float v5, v5 */
		 /* cmpg-float v4, v4, v5 */
		 /* if-gtz v4, :cond_0 */
		 /* iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->AREA_Y:I */
		 /* sub-int v4, v2, v4 */
		 /* int-to-float v4, v4 */
		 /* cmpl-float v3, v3, v4 */
		 /* if-ltz v3, :cond_0 */
		 /* .line 71 */
		 /* new-instance v3, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v4 = " InitMotionX : "; // const-string v4, " InitMotionX : "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
		 final String v4 = " , InitMotionY : "; // const-string v4, " , InitMotionY : "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionY:F */
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
		 final String v4 = " successful "; // const-string v4, " successful "
		 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v4 = "StylusGestureLowerLeft"; // const-string v4, "StylusGestureLowerLeft"
		 android.util.Slog .w ( v4,v3 );
		 /* .line 73 */
		 int v3 = 1; // const/4 v3, 0x1
		 /* .line 75 */
	 } // :cond_0
	 int v3 = 0; // const/4 v3, 0x0
} // .end method
private void checkIsStylusGesture ( android.view.MotionEvent p0 ) {
	 /* .locals 6 */
	 /* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
	 /* .line 79 */
	 /* iget v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureState:I */
	 int v1 = 1; // const/4 v1, 0x1
	 /* if-eq v0, v1, :cond_0 */
	 /* .line 80 */
	 return;
	 /* .line 82 */
} // :cond_0
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
int v1 = 2; // const/4 v1, 0x2
/* if-ne v0, v1, :cond_2 */
/* .line 83 */
v0 = (( android.view.MotionEvent ) p1 ).getX ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F
/* .line 84 */
/* .local v0, "x":F */
v1 = (( android.view.MotionEvent ) p1 ).getY ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F
/* .line 85 */
/* .local v1, "y":F */
/* iget v2, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionX:F */
/* iget v3, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mInitMotionY:F */
v2 = /* invoke-direct {p0, v0, v1, v2, v3}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->dist(FFFF)F */
/* const/high16 v3, 0x40000000 # 2.0f */
/* iget v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTouchSlop:F */
/* mul-float/2addr v4, v3 */
/* cmpl-float v2, v2, v4 */
/* if-ltz v2, :cond_1 */
/* .line 86 */
final String v2 = "StylusGestureLowerLeft"; // const-string v2, "StylusGestureLowerLeft"
final String v3 = "PilferPointers cancel event because of three stylus gesture detecting"; // const-string v3, "PilferPointers cancel event because of three stylus gesture detecting"
android.util.Slog .w ( v2,v3 );
/* .line 87 */
v2 = this.mMiuiGestureMonitor;
(( com.miui.server.input.gesture.MiuiGestureMonitor ) v2 ).pilferPointers ( ); // invoke-virtual {v2}, Lcom/miui/server/input/gesture/MiuiGestureMonitor;->pilferPointers()V
/* .line 89 */
} // :cond_1
/* float-to-double v2, v0 */
/* iget-wide v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_X:D */
/* cmpl-double v2, v2, v4 */
/* if-ltz v2, :cond_2 */
/* float-to-double v2, v1 */
/* iget-wide v4, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureThreshold_Y:D */
/* cmpg-double v2, v2, v4 */
/* if-gtz v2, :cond_2 */
/* .line 90 */
int v2 = 3; // const/4 v2, 0x3
/* invoke-direct {p0, v2}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V */
/* .line 91 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->takeScreenshot()V */
/* .line 94 */
} // .end local v0 # "x":F
} // .end local v1 # "y":F
} // :cond_2
return;
} // .end method
private void checkLowerLeftGesture ( android.view.MotionEvent p0 ) {
/* .locals 1 */
/* .param p1, "motionEvent" # Landroid/view/MotionEvent; */
/* .line 43 */
v0 = (( android.view.MotionEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
/* if-nez v0, :cond_0 */
/* .line 44 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V */
/* .line 45 */
} // :cond_0
/* iget v0, p0, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->mTableGestureState:I */
/* if-nez v0, :cond_2 */
/* .line 46 */
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->checkIsStartPosition(Landroid/view/MotionEvent;)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 47 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V */
/* .line 49 */
} // :cond_1
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->changeTableGestureState(I)V */
/* .line 52 */
} // :cond_2
} // :goto_0
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->checkIsStylusGesture(Landroid/view/MotionEvent;)V */
/* .line 53 */
return;
} // .end method
private Float dist ( Float p0, Float p1, Float p2, Float p3 ) {
/* .locals 6 */
/* .param p1, "x1" # F */
/* .param p2, "y1" # F */
/* .param p3, "x2" # F */
/* .param p4, "y2" # F */
/* .line 106 */
/* sub-float v0, p3, p1 */
/* .line 107 */
/* .local v0, "x":F */
/* sub-float v1, p4, p2 */
/* .line 108 */
/* .local v1, "y":F */
/* float-to-double v2, v0 */
/* float-to-double v4, v1 */
java.lang.Math .hypot ( v2,v3,v4,v5 );
/* move-result-wide v2 */
/* double-to-float v2, v2 */
} // .end method
private void takeScreenshot ( ) {
/* .locals 5 */
/* .line 101 */
v0 = this.mContext;
com.miui.server.input.util.ShortCutActionsUtils .getInstance ( v0 );
int v1 = 0; // const/4 v1, 0x0
int v2 = 0; // const/4 v2, 0x0
/* const-string/jumbo v3, "screenshot_without_anim" */
/* const-string/jumbo v4, "stylus_screen_lower_left" */
(( com.miui.server.input.util.ShortCutActionsUtils ) v0 ).triggerFunction ( v3, v4, v1, v2 ); // invoke-virtual {v0, v3, v4, v1, v2}, Lcom/miui/server/input/util/ShortCutActionsUtils;->triggerFunction(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)Z
/* .line 103 */
return;
} // .end method
/* # virtual methods */
public void onPointerEvent ( android.view.MotionEvent p0 ) {
/* .locals 0 */
/* .param p1, "event" # Landroid/view/MotionEvent; */
/* .line 34 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/checker/StylusGestureLowerLeft;->checkLowerLeftGesture(Landroid/view/MotionEvent;)V */
/* .line 35 */
return;
} // .end method
