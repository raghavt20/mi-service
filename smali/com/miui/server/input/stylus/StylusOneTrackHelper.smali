.class public Lcom/miui/server/input/stylus/StylusOneTrackHelper;
.super Ljava/lang/Object;
.source "StylusOneTrackHelper.java"


# static fields
.field private static final EVENT_NAME:Ljava/lang/String; = "EVENT_NAME"

.field private static final FUNCTION_TRIGGER_TRACK_TYPE:Ljava/lang/String; = "function_trigger"

.field private static final LASER_TRIGGER_TRACK_TYPE:Ljava/lang/String; = "laser_trigger"

.field private static final LONG_PRESS_TRACK_TYPE:Ljava/lang/String; = "long_press"

.field private static final PRESS_TRACK_TYPE:Ljava/lang/String; = "press"

.field private static final SHORTHAND_TRACK_TYPE:Ljava/lang/String; = "shorthand"

.field private static final STYLUS_TRACK_APP_PACKAGE_NAME:Ljava/lang/String; = "app_package_name"

.field private static final STYLUS_TRACK_FUNCTION_TYPE:Ljava/lang/String; = "function_type"

.field private static final STYLUS_TRACK_KEY_TYPE:Ljava/lang/String; = "key_type"

.field private static final STYLUS_TRACK_TIP:Ljava/lang/String; = "tip"

.field private static final STYLUS_TRACK_TIP_FUNCTION_TRIGGER_HIGHLIGHT:Ljava/lang/String; = "899.2.0.1.28606"

.field private static final STYLUS_TRACK_TIP_FUNCTION_TRIGGER_PEN:Ljava/lang/String; = "899.2.0.1.28605"

.field private static final STYLUS_TRACK_TIP_LASER_TRIGGER:Ljava/lang/String; = "899.2.2.1.27097"

.field private static final STYLUS_TRACK_TIP_LONG_PRESS:Ljava/lang/String; = "899.2.3.1.27099"

.field private static final STYLUS_TRACK_TIP_PRESS:Ljava/lang/String; = "899.2.3.1.27100"

.field private static final STYLUS_TRACK_TIP_SHORTHAND:Ljava/lang/String; = "899.2.0.1.29459"

.field private static final TAG:Ljava/lang/String; = "StylusOneTrackHelper"

.field private static volatile sInstance:Lcom/miui/server/input/stylus/StylusOneTrackHelper;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .line 42
    sget-object v0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->sInstance:Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    if-nez v0, :cond_1

    .line 43
    const-class v0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    monitor-enter v0

    .line 44
    :try_start_0
    sget-object v1, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->sInstance:Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    if-nez v1, :cond_0

    .line 45
    new-instance v1, Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    invoke-direct {v1, p0}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->sInstance:Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    .line 47
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 49
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->sInstance:Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    return-object v0
.end method

.method private getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 4
    .param p1, "trackType"    # Ljava/lang/String;
    .param p2, "tip"    # Ljava/lang/String;

    .line 133
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 135
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "EVENT_NAME"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 136
    const-string/jumbo v1, "tip"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    goto :goto_0

    .line 137
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get event tracking data of stylus!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "StylusOneTrackHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    return-object v0
.end method


# virtual methods
.method public trackStylusHighLightTrigger()V
    .locals 3

    .line 120
    const-string v0, "function_trigger"

    const-string v1, "899.2.0.1.28606"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 122
    .local v0, "jsonObject":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method public trackStylusKeyLongPress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "functionType"    # Ljava/lang/String;
    .param p2, "keyType"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;

    .line 69
    const-string v0, "long_press"

    const-string v1, "899.2.3.1.27099"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 72
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "function_type"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 73
    const-string v1, "key_type"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 74
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    goto :goto_0

    .line 75
    :catch_0
    move-exception v1

    .line 76
    .local v1, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stylus trackEvent data fail!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "StylusOneTrackHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public trackStylusKeyPress(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 88
    const-string v0, "press"

    const-string v1, "899.2.3.1.27100"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 90
    .local v0, "jsonObject":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "key_type"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 91
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    goto :goto_0

    .line 92
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get event tracking data of stylus!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "StylusOneTrackHelper"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    .end local v1    # "e":Lorg/json/JSONException;
    :goto_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public trackStylusLaserTrigger()V
    .locals 3

    .line 56
    const-string v0, "laser_trigger"

    const-string v1, "899.2.2.1.27097"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 58
    .local v0, "jsonObject":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public trackStylusPenTrigger()V
    .locals 3

    .line 111
    const-string v0, "function_trigger"

    const-string v1, "899.2.0.1.28605"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 113
    .local v0, "jsonObject":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public trackStylusShortHandTrigger()V
    .locals 3

    .line 102
    const-string/jumbo v0, "shorthand"

    const-string v1, "899.2.0.1.29459"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getTrackJSONObject(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 104
    .local v0, "jsonObject":Lorg/json/JSONObject;
    iget-object v1, p0, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/input/InputOneTrackUtil;->getInstance(Landroid/content/Context;)Lcom/android/server/input/InputOneTrackUtil;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/server/input/InputOneTrackUtil;->trackStylusEvent(Ljava/lang/String;)V

    .line 105
    return-void
.end method
