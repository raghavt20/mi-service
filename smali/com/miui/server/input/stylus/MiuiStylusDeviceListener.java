public class com.miui.server.input.stylus.MiuiStylusDeviceListener implements android.hardware.input.InputManager$InputDeviceListener {
	 /* .source "MiuiStylusDeviceListener.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private final android.hardware.input.InputManager mInputManager;
	 private final java.util.Map mStylusDeviceList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Map<", */
	 /* "Ljava/lang/Integer;", */
	 /* "Ljava/lang/Integer;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private final miui.util.ITouchFeature mTouchFeature;
/* # direct methods */
public com.miui.server.input.stylus.MiuiStylusDeviceListener ( ) {
/* .locals 4 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 21 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 19 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mStylusDeviceList = v0;
/* .line 22 */
final String v0 = "input"; // const-string v0, "input"
(( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/hardware/input/InputManager; */
this.mInputManager = v0;
/* .line 23 */
int v1 = 0; // const/4 v1, 0x0
(( android.hardware.input.InputManager ) v0 ).registerInputDeviceListener ( p0, v1 ); // invoke-virtual {v0, p0, v1}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V
/* .line 24 */
miui.util.ITouchFeature .getInstance ( );
this.mTouchFeature = v0;
/* .line 26 */
/* const/16 v1, 0x14 */
int v2 = -1; // const/4 v2, -0x1
int v3 = 0; // const/4 v3, 0x0
(( miui.util.ITouchFeature ) v0 ).setTouchMode ( v3, v1, v2 ); // invoke-virtual {v0, v3, v1, v2}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 28 */
return;
} // .end method
/* # virtual methods */
public void onInputDeviceAdded ( Integer p0 ) {
/* .locals 6 */
/* .param p1, "i" # I */
/* .line 39 */
v0 = this.mInputManager;
(( android.hardware.input.InputManager ) v0 ).getInputDevice ( p1 ); // invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;
/* .line 40 */
/* .local v0, "inputDevice":Landroid/view/InputDevice; */
/* if-nez v0, :cond_0 */
/* .line 41 */
return;
/* .line 43 */
} // :cond_0
v1 = (( android.view.InputDevice ) v0 ).isXiaomiStylus ( ); // invoke-virtual {v0}, Landroid/view/InputDevice;->isXiaomiStylus()I
/* .line 44 */
/* .local v1, "isXiaomiStylus":I */
/* if-lez v1, :cond_2 */
v2 = this.mStylusDeviceList;
v2 = java.lang.Integer .valueOf ( p1 );
/* if-nez v2, :cond_2 */
/* .line 45 */
v2 = v2 = this.mStylusDeviceList;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 46 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
int v3 = 1; // const/4 v3, 0x1
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v2 ).onStylusConnectionStateChanged ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onStylusConnectionStateChanged(Z)V
/* .line 48 */
} // :cond_1
v2 = this.mStylusDeviceList;
java.lang.Integer .valueOf ( p1 );
java.lang.Integer .valueOf ( v1 );
/* .line 49 */
/* or-int/lit8 v2, v1, 0x10 */
/* .line 50 */
/* .local v2, "value":I */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "Stylus device add device id : "; // const-string v4, "Stylus device add device id : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " version : "; // const-string v4, " version : "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " value :"; // const-string v4, " value :"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiStylusDeviceListener"; // const-string v4, "MiuiStylusDeviceListener"
android.util.Slog .w ( v4,v3 );
/* .line 52 */
v3 = this.mTouchFeature;
int v4 = 0; // const/4 v4, 0x0
/* const/16 v5, 0x14 */
(( miui.util.ITouchFeature ) v3 ).setTouchMode ( v4, v5, v2 ); // invoke-virtual {v3, v4, v5, v2}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 55 */
} // .end local v2 # "value":I
} // :cond_2
return;
} // .end method
public void onInputDeviceChanged ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "i" # I */
/* .line 77 */
return;
} // .end method
public void onInputDeviceRemoved ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "i" # I */
/* .line 59 */
v0 = this.mStylusDeviceList;
v0 = java.lang.Integer .valueOf ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 60 */
v0 = this.mStylusDeviceList;
java.lang.Integer .valueOf ( p1 );
/* check-cast v0, Ljava/lang/Integer; */
/* .line 61 */
/* .local v0, "isXiaomiStylus":Ljava/lang/Integer; */
/* if-nez v0, :cond_0 */
/* .line 62 */
return;
/* .line 64 */
} // :cond_0
v1 = v1 = this.mStylusDeviceList;
int v2 = 0; // const/4 v2, 0x0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 65 */
com.miui.server.input.stylus.blocker.MiuiEventBlockerManager .getInstance ( );
(( com.miui.server.input.stylus.blocker.MiuiEventBlockerManager ) v1 ).onStylusConnectionStateChanged ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/stylus/blocker/MiuiEventBlockerManager;->onStylusConnectionStateChanged(Z)V
/* .line 67 */
} // :cond_1
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Stylus device remove device id : "; // const-string v3, "Stylus device remove device id : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " version : "; // const-string v3, " version : "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v3 = ", stylus have "; // const-string v3, ", stylus have "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = this.mStylusDeviceList;
v3 = /* .line 68 */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = " connected"; // const-string v3, " connected"
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 67 */
final String v3 = "MiuiStylusDeviceListener"; // const-string v3, "MiuiStylusDeviceListener"
android.util.Slog .w ( v3,v1 );
/* .line 69 */
v1 = this.mTouchFeature;
/* .line 70 */
v3 = (( java.lang.Integer ) v0 ).intValue ( ); // invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
/* .line 69 */
/* const/16 v4, 0x14 */
(( miui.util.ITouchFeature ) v1 ).setTouchMode ( v2, v4, v3 ); // invoke-virtual {v1, v2, v4, v3}, Lmiui/util/ITouchFeature;->setTouchMode(III)Z
/* .line 72 */
} // .end local v0 # "isXiaomiStylus":Ljava/lang/Integer;
} // :cond_2
return;
} // .end method
