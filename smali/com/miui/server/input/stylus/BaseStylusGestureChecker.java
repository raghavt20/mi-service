public abstract class com.miui.server.input.stylus.BaseStylusGestureChecker {
	 /* .source "BaseStylusGestureChecker.java" */
	 /* # static fields */
	 protected static final Integer STATE_CHECK_FAIL;
	 protected static final Integer STATE_CHECK_SUCCESS;
	 protected static final Integer STATE_DETECTING;
	 protected static final Integer STATE_NO_DETECT;
	 /* # instance fields */
	 protected android.content.Context mContext;
	 protected com.miui.server.input.gesture.MiuiGestureMonitor mMiuiGestureMonitor;
	 protected android.view.WindowManager mWindowManager;
	 /* # direct methods */
	 protected com.miui.server.input.stylus.BaseStylusGestureChecker ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 21 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 22 */
		 this.mContext = p1;
		 /* .line 23 */
		 /* const-class v0, Landroid/view/WindowManager; */
		 (( android.content.Context ) p1 ).getSystemService ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
		 /* check-cast v0, Landroid/view/WindowManager; */
		 this.mWindowManager = v0;
		 /* .line 24 */
		 com.miui.server.input.gesture.MiuiGestureMonitor .getInstance ( p1 );
		 this.mMiuiGestureMonitor = v0;
		 /* .line 25 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void onPointerEvent ( android.view.MotionEvent p0 ) {
	 } // .end method
