class com.miui.server.input.stylus.MiuiStylusShortcutManager$H extends android.os.Handler {
	 /* .source "MiuiStylusShortcutManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/stylus/MiuiStylusShortcutManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final Integer MSG_HALL;
private static final Integer MSG_KEY_DISMISS_WITHOUT_ANIMATION;
private static final Integer MSG_KEY_DISMISS_WITH_ANIMATION;
private static final Integer MSG_OFF_SCREEN_QUICK_NOTE;
private static final Integer MSG_QUICK_NOTE_KEY_LONG_PRESS_ACTION;
private static final Integer MSG_SCREEN_SHOT_KEY_LONG_PRESS_ACTION;
/* # instance fields */
final com.miui.server.input.stylus.MiuiStylusShortcutManager this$0; //synthetic
/* # direct methods */
public com.miui.server.input.stylus.MiuiStylusShortcutManager$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 762 */
this.this$0 = p1;
/* .line 763 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 764 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 768 */
/* iget v0, p1, Landroid/os/Message;->what:I */
int v1 = 1; // const/4 v1, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* .line 787 */
/* :pswitch_0 */
/* iget v0, p1, Landroid/os/Message;->arg1:I */
/* .line 788 */
/* .local v0, "hall":I */
v1 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mputHallSettings ( v1,v0 );
/* .line 789 */
/* .line 780 */
} // .end local v0 # "hall":I
/* :pswitch_1 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mtriggerNoteWhenScreenOff ( v0 );
/* .line 781 */
/* .line 777 */
/* :pswitch_2 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mremoveStylusMaskWindow ( v0,v1 );
/* .line 778 */
/* .line 774 */
/* :pswitch_3 */
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mremoveStylusMaskWindow ( v0,v1 );
/* .line 775 */
/* .line 783 */
/* :pswitch_4 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fputmScreenShotKeyFunctionTriggered ( v0,v1 );
/* .line 784 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$mtriggerScreenShotKeyFunction ( v0 );
/* .line 785 */
/* .line 770 */
/* :pswitch_5 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$fputmQuickNoteKeyFunctionTriggered ( v0,v1 );
/* .line 771 */
v0 = this.this$0;
com.miui.server.input.stylus.MiuiStylusShortcutManager .-$$Nest$maddStylusMaskWindow ( v0 );
/* .line 772 */
/* nop */
/* .line 793 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
