.class public Lcom/miui/server/input/stylus/laser/LaserPointerController;
.super Ljava/lang/Object;
.source "LaserPointerController.java"

# interfaces
.implements Lcom/miui/server/input/stylus/laser/PointerControllerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/stylus/laser/LaserPointerController$H;,
        Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;
    }
.end annotation


# static fields
.field private static final DIRTY_KEEP_PATH:I = 0x4

.field private static final DIRTY_MODE:I = 0x2

.field private static final DIRTY_POSITION:I = 0x1

.field private static final DIRTY_PRESENTATION_VISIBLE:I = 0x10

.field private static final DIRTY_VISIBLE:I = 0x8

.field private static final KEY_DOWN_TIMEOUT:J = 0x3a980L

.field private static final LONG_PRESS_TIMEOUT:J = 0xc8L

.field private static final MULTI_PRESS_TIMEOUT:J = 0x12cL

.field private static final POKE_USER_ACTIVITY_TIMEOUT:J = 0x7d0L

.field private static final REMOVE_WINDOW_TIME:J = 0x1d4c0L

.field private static final TAG:Ljava/lang/String; = "LaserPointerController"

.field private static final TIMEOUT_TIME:J = 0x1d4c0L


# instance fields
.field private mCanShowLaser:Z

.field private final mContext:Landroid/content/Context;

.field private mDisplayId:I

.field private mDisplayViewport:Landroid/hardware/display/DisplayViewport;

.field private mHandleByKeyDownTimeout:Z

.field private mHandleByLongPressing:Z

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Landroid/os/HandlerThread;

.field private final mInputCommonConfig:Lcom/android/server/input/config/InputCommonConfig;

.field private mIsDownFromScreenOff:Z

.field private mIsKeyDown:Z

.field private mIsScreenOn:Z

.field private final mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

.field private mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

.field private mLastDownTime:J

.field private mLastPokeUserActivityTime:J

.field private mLastRequestFadeTime:J

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private final mLock:Ljava/lang/Object;

.field private final mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

.field private final mPowerManager:Landroid/os/PowerManager;

.field private mPressCounter:I

.field private final mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

.field private final mTempPosition:Landroid/graphics/PointF;

.field private final mWindowManger:Landroid/view/WindowManager;


# direct methods
.method public static synthetic $r8$lambda$ZlpzQeSHlTWHEx84RzWCWfLgDHY(Lcom/miui/server/input/stylus/laser/LaserPointerController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->lambda$sendModeToBluetooth$0(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mclearPath(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->clearPath()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monKeyDownTimeout(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->onKeyDownTimeout()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monLaserKeyLongPressed(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->onLaserKeyLongPressed()V

    return-void
.end method

.method static bridge synthetic -$$Nest$monLaserKeyPressed(Lcom/miui/server/input/stylus/laser/LaserPointerController;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->onLaserKeyPressed(I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mrefreshLaserState(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->refreshLaserState()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mremoveLaserWindow(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->removeLaserWindow()V

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I

    .line 89
    const-wide/32 v0, -0x1d4c0

    iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastRequestFadeTime:J

    .line 92
    const-wide/16 v0, -0x7d0

    iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastPokeUserActivityTime:J

    .line 95
    invoke-static {}, Landroid/app/ActivityThread;->currentActivityThread()Landroid/app/ActivityThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mContext:Landroid/content/Context;

    .line 96
    const-class v1, Landroid/view/WindowManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mWindowManger:Landroid/view/WindowManager;

    .line 97
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "LaserPointerController"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandlerThread:Landroid/os/HandlerThread;

    .line 98
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 99
    new-instance v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;-><init>(Lcom/miui/server/input/stylus/laser/LaserPointerController;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    .line 100
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    .line 101
    new-instance v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    invoke-direct {v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;-><init>()V

    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    .line 102
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempPosition:Landroid/graphics/PointF;

    .line 103
    new-instance v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    invoke-direct {v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;-><init>()V

    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    .line 104
    const-class v1, Landroid/os/PowerManager;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPowerManager:Landroid/os/PowerManager;

    .line 105
    invoke-static {}, Lcom/android/server/input/config/InputCommonConfig;->getInstance()Lcom/android/server/input/config/InputCommonConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mInputCommonConfig:Lcom/android/server/input/config/InputCommonConfig;

    .line 106
    const-class v0, Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal;

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    .line 107
    return-void
.end method

.method private addLaserWindow()V
    .locals 3

    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request add window, mLaserView is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-nez v1, :cond_0

    const-string v1, "null"

    goto :goto_0

    :cond_0
    const-string v1, "not null"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 309
    const-string v1, "LaserPointerController"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-eqz v0, :cond_1

    .line 313
    return-void

    .line 315
    :cond_1
    new-instance v0, Lcom/miui/server/input/stylus/laser/LaserView;

    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    .line 316
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setForceDarkAllowed(Z)V

    .line 317
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 318
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mWindowManger:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 319
    return-void
.end method

.method private canShowLaserViewLocked()Z
    .locals 2

    .line 448
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    if-ne v0, v1, :cond_2

    :cond_1
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private clearPath()V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-nez v0, :cond_0

    .line 382
    return-void

    .line 384
    :cond_0
    invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserView;->clearPath()V

    .line 385
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetWindowRemoveTimeout()V

    .line 386
    return-void
.end method

.method private fadeLocked(I)V
    .locals 3
    .param p1, "transition"    # I

    .line 164
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    if-nez v0, :cond_0

    .line 165
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    .line 168
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 169
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 171
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setModeLocked(I)V

    .line 173
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V

    .line 176
    :cond_1
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadePresentationLocked(I)V

    .line 177
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z

    .line 178
    return-void
.end method

.method private fadePresentationLocked(I)V
    .locals 2
    .param p1, "transition"    # I

    .line 202
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z

    if-nez v0, :cond_0

    .line 204
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z

    .line 207
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 208
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastRequestFadeTime:J

    .line 209
    return-void
.end method

.method private findDisplayViewportById(Ljava/util/List;I)Landroid/hardware/display/DisplayViewport;
    .locals 3
    .param p2, "displayId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;I)",
            "Landroid/hardware/display/DisplayViewport;"
        }
    .end annotation

    .line 501
    .local p1, "displayViewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayViewport;

    .line 502
    .local v1, "displayViewport":Landroid/hardware/display/DisplayViewport;
    iget v2, v1, Landroid/hardware/display/DisplayViewport;->displayId:I

    if-ne v2, p2, :cond_0

    .line 503
    return-object v1

    .line 505
    .end local v1    # "displayViewport":Landroid/hardware/display/DisplayViewport;
    :cond_0
    goto :goto_0

    .line 506
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private getLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 2

    .line 480
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 481
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const/16 v1, 0x138

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 485
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I

    .line 487
    const/16 v1, 0x7e2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 488
    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 489
    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 490
    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 491
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 492
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 493
    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 494
    invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->setTrustedOverlay()V

    .line 495
    const-string v1, "laser"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 496
    return-object v0
.end method

.method private getLongPressTimeoutMs()J
    .locals 2

    .line 674
    const-wide/16 v0, 0xc8

    return-wide v0
.end method

.method private getMaxPressCount()I
    .locals 1

    .line 682
    const/4 v0, 0x2

    return v0
.end method

.method private getMultiPressTimeout()J
    .locals 2

    .line 678
    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method private hideLaserOrStopDrawingLocked()V
    .locals 3

    .line 652
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 654
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadePresentationLocked(I)V

    goto :goto_0

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 657
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V

    .line 659
    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z

    .line 660
    return-void
.end method

.method private interceptLaserKeyDown(Landroid/view/KeyEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 537
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastDownTime:J

    sub-long/2addr v0, v2

    .line 538
    .local v0, "keyDownInterval":J
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastDownTime:J

    .line 539
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMultiPressTimeout()J

    move-result-wide v2

    cmp-long v2, v0, v2

    const/4 v3, 0x1

    if-ltz v2, :cond_0

    .line 540
    iput v3, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    goto :goto_0

    .line 542
    :cond_0
    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    .line 545
    :goto_0
    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    const/4 v4, 0x5

    const/4 v5, 0x0

    if-ne v2, v3, :cond_1

    .line 547
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 548
    .local v2, "message":Landroid/os/Message;
    invoke-virtual {v2, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 549
    iget-object v4, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getLongPressTimeoutMs()J

    move-result-wide v6

    invoke-virtual {v4, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 551
    iget-object v4, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 552
    .local v6, "downTimeoutMessage":Landroid/os/Message;
    invoke-virtual {v6, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 553
    iget-object v4, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const-wide/32 v7, 0x3a980

    invoke-virtual {v4, v6, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 554
    iget-object v7, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v7

    .line 555
    :try_start_0
    iput-boolean v3, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsKeyDown:Z

    .line 556
    iput-boolean v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByLongPressing:Z

    .line 557
    iput-boolean v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByKeyDownTimeout:Z

    .line 558
    monitor-exit v7

    .line 559
    .end local v2    # "message":Landroid/os/Message;
    .end local v6    # "downTimeoutMessage":Landroid/os/Message;
    goto :goto_1

    .line 558
    .restart local v2    # "message":Landroid/os/Message;
    .restart local v6    # "downTimeoutMessage":Landroid/os/Message;
    :catchall_0
    move-exception v3

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 561
    .end local v2    # "message":Landroid/os/Message;
    .end local v6    # "downTimeoutMessage":Landroid/os/Message;
    :cond_1
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x4

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 562
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 563
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I

    move-result v2

    if-le v2, v3, :cond_2

    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I

    move-result v4

    if-ne v2, v4, :cond_2

    .line 565
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    iget v4, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 566
    .restart local v2    # "message":Landroid/os/Message;
    invoke-virtual {v2, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 567
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 570
    .end local v2    # "message":Landroid/os/Message;
    :cond_2
    :goto_1
    return-void
.end method

.method private interceptLaserKeyUp()V
    .locals 6

    .line 574
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 576
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 577
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 578
    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsKeyDown:Z

    .line 579
    iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByKeyDownTimeout:Z

    if-eqz v2, :cond_0

    .line 581
    const-string v1, "LaserPointerController"

    const-string v2, "Key up handle by key down timeout, not process"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    monitor-exit v0

    return-void

    .line 584
    :cond_0
    iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByLongPressing:Z

    const/4 v3, 0x1

    if-nez v2, :cond_3

    .line 586
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v3, :cond_1

    .line 588
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4, v3, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 589
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 590
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .end local v1    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 591
    :cond_1
    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 593
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    iget v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    invoke-virtual {v2, v4, v5, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 594
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v3}, Landroid/os/Message;->setAsynchronous(Z)V

    .line 595
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMultiPressTimeout()J

    move-result-wide v3

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 596
    nop

    .end local v1    # "message":Landroid/os/Message;
    goto :goto_1

    .line 591
    :cond_2
    :goto_0
    goto :goto_1

    .line 597
    :cond_3
    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I

    if-ne v1, v3, :cond_4

    .line 599
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->hideLaserOrStopDrawingLocked()V

    .line 601
    :cond_4
    :goto_1
    monitor-exit v0

    .line 602
    return-void

    .line 601
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private invalidateLocked(I)V
    .locals 4
    .param p1, "dirty"    # I

    .line 511
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 512
    .local v0, "wasDirty":Z
    :goto_0
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v3, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    or-int/2addr v3, p1

    iput v3, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    .line 513
    if-nez v0, :cond_1

    .line 514
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 516
    :cond_1
    return-void
.end method

.method private synthetic lambda$sendModeToBluetooth$0(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 426
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.input.laser.LASER_STATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 427
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.xiaomi.bluetooth"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    const-string v1, "mode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 429
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 430
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 431
    return-void
.end method

.method private movePointInScreenLocked(Landroid/graphics/PointF;)Z
    .locals 3
    .param p1, "pointF"    # Landroid/graphics/PointF;

    .line 460
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayViewport:Landroid/hardware/display/DisplayViewport;

    if-nez v0, :cond_0

    .line 461
    const/4 v0, 0x0

    return v0

    .line 463
    :cond_0
    iget-object v0, v0, Landroid/hardware/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    .line 464
    .local v0, "logicalFrame":Landroid/graphics/Rect;
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 465
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iput v1, p1, Landroid/graphics/PointF;->x:F

    .line 467
    :cond_1
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 468
    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iput v1, p1, Landroid/graphics/PointF;->x:F

    .line 470
    :cond_2
    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 471
    iget v1, v0, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iput v1, p1, Landroid/graphics/PointF;->y:F

    .line 473
    :cond_3
    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    .line 474
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    iput v1, p1, Landroid/graphics/PointF;->y:F

    .line 476
    :cond_4
    const/4 v1, 0x1

    return v1
.end method

.method private onKeyDownTimeout()V
    .locals 2

    .line 666
    const-string v0, "LaserPointerController"

    const-string v1, "May be the hardware occurred an exception, laser key not up"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 668
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByKeyDownTimeout:Z

    .line 669
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->hideLaserOrStopDrawingLocked()V

    .line 670
    monitor-exit v0

    .line 671
    return-void

    .line 670
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onLaserKeyLongPressed()V
    .locals 3

    .line 606
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 607
    :try_start_0
    iget-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsKeyDown:Z

    if-nez v1, :cond_0

    .line 609
    monitor-exit v0

    return-void

    .line 611
    :cond_0
    const-string v1, "LaserPointerController"

    const-string v2, "On laser key long pressed"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z

    .line 613
    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByLongPressing:Z

    .line 614
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    if-nez v2, :cond_1

    .line 616
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadePresentationLocked(I)V

    .line 617
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusLaserTrigger()V

    goto :goto_0

    .line 618
    :cond_1
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    if-ne v2, v1, :cond_2

    .line 620
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V

    .line 621
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusHighLightTrigger()V

    .line 623
    :cond_2
    :goto_0
    monitor-exit v0

    .line 624
    return-void

    .line 623
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private onLaserKeyPressed(I)V
    .locals 4
    .param p1, "count"    # I

    .line 627
    const-string v0, "LaserPointerController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "On laser key pressed, count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 629
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 631
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 633
    invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setModeLocked(I)V

    .line 635
    invoke-direct {p0, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadePresentationLocked(I)V

    .line 636
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->getInstance(Landroid/content/Context;)Lcom/miui/server/input/stylus/StylusOneTrackHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusPenTrigger()V

    goto :goto_0

    .line 637
    :cond_0
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    if-ne v2, v1, :cond_2

    .line 639
    invoke-direct {p0, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setModeLocked(I)V

    .line 641
    invoke-direct {p0, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadePresentationLocked(I)V

    goto :goto_0

    .line 647
    :catchall_0
    move-exception v1

    goto :goto_1

    .line 643
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 645
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->clearPath()V

    .line 647
    :cond_2
    :goto_0
    monitor-exit v0

    .line 648
    return-void

    .line 647
    :goto_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private pokeUserActivity()V
    .locals 6

    .line 397
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 398
    .local v0, "now":J
    iget-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastPokeUserActivityTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 399
    return-void

    .line 401
    :cond_0
    iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastPokeUserActivityTime:J

    .line 402
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPowerManager:Landroid/os/PowerManager;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/os/PowerManager;->userActivity(JII)V

    .line 404
    return-void
.end method

.method private refreshLaserState()V
    .locals 3

    .line 325
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 326
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    invoke-virtual {v1, v2}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->copyFrom(Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;)V

    .line 327
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    invoke-virtual {v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->resetDirty()V

    .line 328
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    and-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    if-eqz v0, :cond_1

    .line 331
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-nez v0, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->addLaserWindow()V

    goto :goto_0

    .line 334
    :cond_0
    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setVisible(Z)V

    .line 337
    :goto_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mMiuiInputManagerInternal:Lcom/android/server/input/MiuiInputManagerInternal;

    invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerInternal;->hideMouseCursor()V

    .line 339
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->markAllDirty()V

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-nez v0, :cond_2

    .line 343
    return-void

    .line 346
    :cond_2
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    if-nez v0, :cond_3

    .line 347
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/miui/server/input/stylus/laser/LaserView;->setVisible(Z)V

    .line 349
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    invoke-virtual {v0, v2}, Lcom/miui/server/input/stylus/laser/LaserView;->setKeepPath(Z)V

    .line 351
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserView;->clearPath()V

    .line 352
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetWindowRemoveTimeout()V

    .line 355
    :cond_3
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    if-nez v0, :cond_4

    .line 357
    return-void

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_5

    .line 360
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z

    invoke-virtual {v0, v2}, Lcom/miui/server/input/stylus/laser/LaserView;->setPresentationVisible(Z)V

    .line 362
    :cond_5
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_6

    .line 363
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setPosition(Landroid/graphics/PointF;)V

    .line 365
    :cond_6
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_7

    .line 366
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setMode(I)V

    .line 368
    :cond_7
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_8

    .line 369
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z

    invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setKeepPath(Z)V

    .line 372
    :cond_8
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->pokeUserActivity()V

    .line 374
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetWindowRemoveTimeout()V

    .line 375
    return-void

    .line 328
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private removeLaserWindow()V
    .locals 3

    .line 290
    const-string v0, "LaserPointerController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request remove window, mLaserView is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 291
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-nez v2, :cond_0

    const-string v2, "null"

    goto :goto_0

    :cond_0
    const-string v2, "not null"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-nez v0, :cond_1

    .line 294
    return-void

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mWindowManger:Landroid/view/WindowManager;

    invoke-interface {v1, v0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 297
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 298
    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    .line 299
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 301
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    .line 302
    monitor-exit v0

    .line 303
    return-void

    .line 302
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private resetPositionLocked()V
    .locals 3

    .line 279
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayViewport:Landroid/hardware/display/DisplayViewport;

    if-nez v0, :cond_0

    .line 280
    return-void

    .line 282
    :cond_0
    iget-object v0, v0, Landroid/hardware/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayViewport:Landroid/hardware/display/DisplayViewport;

    iget-object v2, v2, Landroid/hardware/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    .line 283
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v1

    .line 282
    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V

    .line 284
    return-void
.end method

.method private resetWindowRemoveTimeout()V
    .locals 4

    .line 389
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 390
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserView:Lcom/miui/server/input/stylus/laser/LaserView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserView;->needExist()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 394
    return-void

    .line 391
    :cond_1
    :goto_0
    return-void
.end method

.method private sendModeToBluetooth(I)V
    .locals 2
    .param p1, "mode"    # I

    .line 424
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sync mode to bluetooth, mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LaserPointerController"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/laser/LaserPointerController;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 432
    return-void
.end method

.method private setKeepPathLocked(Z)V
    .locals 2
    .param p1, "keepPath"    # Z

    .line 436
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z

    if-ne v0, p1, :cond_0

    .line 437
    return-void

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iput-boolean p1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z

    .line 440
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 441
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mInputCommonConfig:Lcom/android/server/input/config/InputCommonConfig;

    invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setLaserIsDrawing(Z)V

    .line 442
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mInputCommonConfig:Lcom/android/server/input/config/InputCommonConfig;

    invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V

    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set keep path, new mode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LaserPointerController"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    return-void
.end method

.method private setModeLocked(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 408
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    if-ne v0, p1, :cond_0

    .line 409
    return-void

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iput p1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I

    .line 412
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 413
    if-nez p1, :cond_1

    .line 415
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V

    .line 417
    :cond_1
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->sendModeToBluetooth(I)V

    .line 418
    return-void
.end method

.method private setPositionLocked(FFZ)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "force"    # Z

    .line 123
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->canShowLaserViewLocked()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_0

    .line 125
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempPosition:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 128
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempPosition:Landroid/graphics/PointF;

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->movePointInScreenLocked(Landroid/graphics/PointF;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempPosition:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    .line 129
    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    return-void

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mTempPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 133
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 134
    return-void
.end method

.method private unfadeLocked(I)V
    .locals 2
    .param p1, "transition"    # I

    .line 189
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->canShowLaserViewLocked()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z

    .line 194
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 195
    return-void

    .line 191
    :cond_1
    :goto_0
    return-void
.end method

.method private unfadePresentationLocked(I)V
    .locals 4
    .param p1, "transition"    # I

    .line 217
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadeLocked(I)V

    .line 218
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z

    if-eqz v0, :cond_0

    .line 220
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z

    .line 223
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V

    .line 224
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastRequestFadeTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 226
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPositionLocked()V

    .line 228
    :cond_1
    return-void
.end method


# virtual methods
.method public canShowPointer()Z
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 273
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->canShowLaserViewLocked()Z

    move-result v1

    monitor-exit v0

    return v1

    .line 274
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public fade(I)V
    .locals 2
    .param p1, "transition"    # I

    .line 157
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 158
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadeLocked(I)V

    .line 159
    monitor-exit v0

    .line 160
    return-void

    .line 159
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getPosition([F)V
    .locals 3
    .param p1, "outPosition"    # [F

    .line 138
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 139
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/4 v2, 0x0

    aput v1, p1, v2

    .line 140
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    const/4 v2, 0x1

    aput v1, p1, v2

    .line 141
    monitor-exit v0

    .line 142
    return-void

    .line 141
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public interceptLaserKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 519
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 520
    .local v0, "isDown":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 521
    iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsScreenOn:Z

    xor-int/2addr v2, v1

    iput-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsDownFromScreenOff:Z

    .line 523
    :cond_1
    iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsDownFromScreenOff:Z

    if-eqz v2, :cond_2

    .line 525
    const-string v2, "LaserPointerController"

    const-string v3, "Screen is off when laser key down, not response."

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    return v1

    .line 528
    :cond_2
    if-eqz v0, :cond_3

    .line 529
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->interceptLaserKeyDown(Landroid/view/KeyEvent;)V

    goto :goto_1

    .line 531
    :cond_3
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->interceptLaserKeyUp()V

    .line 533
    :goto_1
    return v1
.end method

.method public move(FF)V
    .locals 4
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F

    .line 148
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, p1

    .line 150
    .local v1, "newX":F
    iget-object v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLaserState:Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;

    iget-object v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, p2

    .line 151
    .local v2, "newY":F
    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V

    .line 152
    monitor-exit v0

    .line 153
    return-void

    .line 152
    .end local v1    # "newX":F
    .end local v2    # "newY":F
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public resetPosition()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 266
    :try_start_0
    invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPositionLocked()V

    .line 267
    monitor-exit v0

    .line 268
    return-void

    .line 267
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setDisplayId(I)V
    .locals 3
    .param p1, "displayId"    # I

    .line 256
    const-string v0, "LaserPointerController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DisplayId update, new displayId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", old displayId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 259
    :try_start_0
    iput p1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I

    .line 260
    monitor-exit v0

    .line 261
    return-void

    .line 260
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setDisplayViewPort(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;)V"
        }
    .end annotation

    .line 232
    .local p1, "viewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;"
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 233
    :try_start_0
    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I

    invoke-direct {p0, p1, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->findDisplayViewportById(Ljava/util/List;I)Landroid/hardware/display/DisplayViewport;

    move-result-object v1

    .line 234
    .local v1, "displayViewport":Landroid/hardware/display/DisplayViewport;
    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 235
    const-string v3, "LaserPointerController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t find the designated viewport with ID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to update laser input mapper. Fall back to default display"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-direct {p0, p1, v2}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->findDisplayViewportById(Ljava/util/List;I)Landroid/hardware/display/DisplayViewport;

    move-result-object v3

    move-object v1, v3

    .line 239
    :cond_0
    if-nez v1, :cond_1

    .line 240
    const-string v2, "LaserPointerController"

    const-string v3, "Still can\'t find a viable viewport to update cursor input mapper. Skip setting it to LaserPointerController."

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    monitor-exit v0

    return-void

    .line 244
    :cond_1
    const-string v3, "LaserPointerController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Display viewport updated, new display viewport = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    iput-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayViewport:Landroid/hardware/display/DisplayViewport;

    .line 247
    invoke-direct {p0, v2}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadeLocked(I)V

    .line 249
    iget-object v2, v1, Landroid/hardware/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v4, v1, Landroid/hardware/display/DisplayViewport;->logicalFrame:Landroid/graphics/Rect;

    .line 250
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v3

    .line 249
    const/4 v3, 0x1

    invoke-direct {p0, v2, v4, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V

    .line 251
    .end local v1    # "displayViewport":Landroid/hardware/display/DisplayViewport;
    monitor-exit v0

    .line 252
    return-void

    .line 251
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setPosition(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 111
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 112
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V

    .line 113
    monitor-exit v0

    .line 114
    return-void

    .line 113
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setScreenState(Z)V
    .locals 2
    .param p1, "isScreenOn"    # Z

    .line 686
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsScreenOn:Z

    if-ne v0, p1, :cond_0

    .line 687
    return-void

    .line 689
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsScreenOn:Z

    .line 690
    if-eqz p1, :cond_1

    .line 691
    return-void

    .line 693
    :cond_1
    const-string v0, "LaserPointerController"

    const-string v1, "Screen off, fade laser and reset position"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fade(I)V

    .line 696
    invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPosition()V

    .line 697
    return-void
.end method

.method public unfade(I)V
    .locals 2
    .param p1, "transition"    # I

    .line 182
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 183
    :try_start_0
    invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadeLocked(I)V

    .line 184
    monitor-exit v0

    .line 185
    return-void

    .line 184
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
