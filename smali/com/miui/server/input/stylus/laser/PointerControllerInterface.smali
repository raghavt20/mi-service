.class public interface abstract Lcom/miui/server/input/stylus/laser/PointerControllerInterface;
.super Ljava/lang/Object;
.source "PointerControllerInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/stylus/laser/PointerControllerInterface$TransitionType;
    }
.end annotation


# static fields
.field public static final TRANSITION_GRADUAL:I = 0x1

.field public static final TRANSITION_IMMEDIATE:I


# virtual methods
.method public abstract canShowPointer()Z
.end method

.method public abstract fade(I)V
.end method

.method public abstract getPosition([F)V
.end method

.method public abstract move(FF)V
.end method

.method public abstract resetPosition()V
.end method

.method public abstract setDisplayId(I)V
.end method

.method public abstract setDisplayViewPort(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/hardware/display/DisplayViewport;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setPosition(FF)V
.end method

.method public abstract unfade(I)V
.end method
