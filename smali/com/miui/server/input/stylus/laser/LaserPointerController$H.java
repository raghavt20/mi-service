class com.miui.server.input.stylus.laser.LaserPointerController$H extends android.os.Handler {
	 /* .source "LaserPointerController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/stylus/laser/LaserPointerController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "H" */
} // .end annotation
/* # static fields */
private static final Integer MSG_CLEAR_PATH;
private static final Integer MSG_KEY_DOWN_TIMEOUT;
private static final Integer MSG_KEY_LONG_PRESSED;
private static final Integer MSG_KEY_PRESSED;
private static final Integer MSG_REMOVE_LASER_WINDOW;
private static final Integer MSG_UPDATE_LASER_STATE;
/* # instance fields */
final com.miui.server.input.stylus.laser.LaserPointerController this$0; //synthetic
/* # direct methods */
public com.miui.server.input.stylus.laser.LaserPointerController$H ( ) {
/* .locals 0 */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 784 */
this.this$0 = p1;
/* .line 785 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 786 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 2 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 790 */
/* iget v0, p1, Landroid/os/Message;->what:I */
/* packed-switch v0, :pswitch_data_0 */
/* .line 807 */
/* :pswitch_0 */
v0 = this.this$0;
com.miui.server.input.stylus.laser.LaserPointerController .-$$Nest$monKeyDownTimeout ( v0 );
/* .line 808 */
/* .line 804 */
/* :pswitch_1 */
v0 = this.this$0;
com.miui.server.input.stylus.laser.LaserPointerController .-$$Nest$monLaserKeyLongPressed ( v0 );
/* .line 805 */
/* .line 801 */
/* :pswitch_2 */
v0 = this.this$0;
/* iget v1, p1, Landroid/os/Message;->arg1:I */
com.miui.server.input.stylus.laser.LaserPointerController .-$$Nest$monLaserKeyPressed ( v0,v1 );
/* .line 802 */
/* .line 798 */
/* :pswitch_3 */
v0 = this.this$0;
com.miui.server.input.stylus.laser.LaserPointerController .-$$Nest$mclearPath ( v0 );
/* .line 799 */
/* .line 795 */
/* :pswitch_4 */
v0 = this.this$0;
com.miui.server.input.stylus.laser.LaserPointerController .-$$Nest$mremoveLaserWindow ( v0 );
/* .line 796 */
/* .line 792 */
/* :pswitch_5 */
v0 = this.this$0;
com.miui.server.input.stylus.laser.LaserPointerController .-$$Nest$mrefreshLaserState ( v0 );
/* .line 793 */
/* nop */
/* .line 812 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
