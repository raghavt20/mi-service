public abstract class com.miui.server.input.stylus.laser.PointerControllerInterface {
	 /* .source "PointerControllerInterface.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/stylus/laser/PointerControllerInterface$TransitionType; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer TRANSITION_GRADUAL;
public static final Integer TRANSITION_IMMEDIATE;
/* # virtual methods */
public abstract Boolean canShowPointer ( ) {
} // .end method
public abstract void fade ( Integer p0 ) {
} // .end method
public abstract void getPosition ( Float[] p0 ) {
} // .end method
public abstract void move ( Float p0, Float p1 ) {
} // .end method
public abstract void resetPosition ( ) {
} // .end method
public abstract void setDisplayId ( Integer p0 ) {
} // .end method
public abstract void setDisplayViewPort ( java.util.List p0 ) {
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "(", */
	 /* "Ljava/util/List<", */
	 /* "Landroid/hardware/display/DisplayViewport;", */
	 /* ">;)V" */
	 /* } */
} // .end annotation
} // .end method
public abstract void setPosition ( Float p0, Float p1 ) {
} // .end method
public abstract void unfade ( Integer p0 ) {
} // .end method
