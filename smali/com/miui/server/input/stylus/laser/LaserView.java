public class com.miui.server.input.stylus.laser.LaserView extends android.view.View {
	 /* .source "LaserView.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/stylus/laser/LaserView$Mode; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Integer MODE_LASER;
public static final Integer MODE_PEN;
/* # instance fields */
private final android.graphics.Paint mInPaint;
private Boolean mKeepPath;
private final Float mLaserCenterX;
private final Float mLaserCenterY;
private final Float mLaserInRadius;
private final Float mLaserOutBlurRadius;
private final Float mLaserOutFillRadius;
private final Float mLaserPenHeight;
private final android.graphics.drawable.Drawable mLaserPenImage;
private final Float mLaserPenWidth;
private Integer mMode;
private final android.graphics.Paint mOutBlurPaint;
private final android.graphics.Paint mOutFillPaint;
private final java.util.List mPathList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Landroid/graphics/Path;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.graphics.Paint mPathPaint;
private Boolean mPresentationVisible;
private Boolean mVisible;
private Float mX;
private Float mY;
/* # direct methods */
public static void $r8$lambda$8x2GSzqO9Ak1Zm12o_sX4qM3KbQ ( com.miui.server.input.stylus.laser.LaserView p0, android.graphics.Canvas p1, android.graphics.Path p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/input/stylus/laser/LaserView;->lambda$onDraw$0(Landroid/graphics/Canvas;Landroid/graphics/Path;)V */
return;
} // .end method
public com.miui.server.input.stylus.laser.LaserView ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 66 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/input/stylus/laser/LaserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V */
/* .line 67 */
return;
} // .end method
public com.miui.server.input.stylus.laser.LaserView ( ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "attrs" # Landroid/util/AttributeSet; */
/* .line 70 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, p2, v0}, Lcom/miui/server/input/stylus/laser/LaserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
/* .line 71 */
return;
} // .end method
public com.miui.server.input.stylus.laser.LaserView ( ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "attrs" # Landroid/util/AttributeSet; */
/* .param p3, "defStyleAttr" # I */
/* .line 74 */
/* invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V */
/* .line 47 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I */
/* .line 62 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z */
/* .line 63 */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z */
/* .line 77 */
/* new-instance v0, Landroid/graphics/Paint; */
/* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
this.mInPaint = v0;
/* .line 78 */
v2 = android.graphics.Paint$Style.FILL;
(( android.graphics.Paint ) v0 ).setStyle ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
/* .line 79 */
final String v2 = "#CCFF001F"; // const-string v2, "#CCFF001F"
v2 = android.graphics.Color .parseColor ( v2 );
(( android.graphics.Paint ) v0 ).setColor ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V
/* .line 82 */
/* new-instance v0, Landroid/graphics/Paint; */
/* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
this.mOutBlurPaint = v0;
/* .line 83 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 84 */
/* const v3, 0x11070061 */
v2 = (( android.content.res.Resources ) v2 ).getDimension ( v3 ); // invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F
/* .line 83 */
(( android.graphics.Paint ) v0 ).setStrokeWidth ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V
/* .line 85 */
v2 = android.graphics.Paint$Style.STROKE;
(( android.graphics.Paint ) v0 ).setStyle ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
/* .line 86 */
final String v2 = "#FF001F"; // const-string v2, "#FF001F"
v4 = android.graphics.Color .parseColor ( v2 );
(( android.graphics.Paint ) v0 ).setColor ( v4 ); // invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V
/* .line 87 */
/* new-instance v4, Landroid/graphics/BlurMaskFilter; */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 88 */
/* const v6, 0x1107005e */
v5 = (( android.content.res.Resources ) v5 ).getDimension ( v6 ); // invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F
v6 = android.graphics.BlurMaskFilter$Blur.NORMAL;
/* invoke-direct {v4, v5, v6}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V */
/* .line 87 */
(( android.graphics.Paint ) v0 ).setMaskFilter ( v4 ); // invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;
/* .line 91 */
/* new-instance v0, Landroid/graphics/Paint; */
/* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
this.mOutFillPaint = v0;
/* .line 92 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 93 */
v3 = (( android.content.res.Resources ) v4 ).getDimension ( v3 ); // invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimension(I)F
/* .line 92 */
(( android.graphics.Paint ) v0 ).setStrokeWidth ( v3 ); // invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V
/* .line 94 */
v3 = android.graphics.Paint$Style.STROKE;
(( android.graphics.Paint ) v0 ).setStyle ( v3 ); // invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
/* .line 95 */
v2 = android.graphics.Color .parseColor ( v2 );
(( android.graphics.Paint ) v0 ).setColor ( v2 ); // invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V
/* .line 97 */
/* new-instance v0, Ljava/util/LinkedList; */
/* invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V */
this.mPathList = v0;
/* .line 99 */
/* new-instance v0, Landroid/graphics/Paint; */
/* invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V */
this.mPathPaint = v0;
/* .line 100 */
(( android.graphics.Paint ) v0 ).setAntiAlias ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V
/* .line 101 */
(( android.graphics.Paint ) v0 ).setDither ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V
/* .line 102 */
/* const/high16 v1, -0x10000 */
(( android.graphics.Paint ) v0 ).setColor ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V
/* .line 103 */
v1 = android.graphics.Paint$Style.STROKE;
(( android.graphics.Paint ) v0 ).setStyle ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V
/* .line 104 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 105 */
/* const v2, 0x11070065 */
v1 = (( android.content.res.Resources ) v1 ).getDimension ( v2 ); // invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F
/* .line 104 */
(( android.graphics.Paint ) v0 ).setStrokeWidth ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V
/* .line 106 */
v1 = android.graphics.Paint$Join.ROUND;
(( android.graphics.Paint ) v0 ).setStrokeJoin ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V
/* .line 107 */
v1 = android.graphics.Paint$Cap.ROUND;
(( android.graphics.Paint ) v0 ).setStrokeCap ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V
/* .line 109 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 110 */
/* const v1, 0x1107005d */
v0 = (( android.content.res.Resources ) v0 ).getDimension ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserInRadius:F */
/* .line 111 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 112 */
/* const v1, 0x11070060 */
v0 = (( android.content.res.Resources ) v0 ).getDimension ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutFillRadius:F */
/* .line 113 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 114 */
/* const v1, 0x1107005f */
v0 = (( android.content.res.Resources ) v0 ).getDimension ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutBlurRadius:F */
/* .line 115 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 116 */
/* const v1, 0x11070066 */
v0 = (( android.content.res.Resources ) v0 ).getDimension ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenWidth:F */
/* .line 117 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 118 */
/* const v1, 0x11070064 */
v0 = (( android.content.res.Resources ) v0 ).getDimension ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenHeight:F */
/* .line 119 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 120 */
/* const v1, 0x11070062 */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterX:F */
/* .line 121 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 122 */
/* const v1, 0x11070063 */
v0 = (( android.content.res.Resources ) v0 ).getFloat ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterY:F */
/* .line 123 */
/* const v0, 0x1108024f */
(( android.content.Context ) p1 ).getDrawable ( v0 ); // invoke-virtual {p1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;
this.mLaserPenImage = v0;
/* .line 124 */
return;
} // .end method
private void lambda$onDraw$0 ( android.graphics.Canvas p0, android.graphics.Path p1 ) { //synthethic
/* .locals 1 */
/* .param p1, "canvas" # Landroid/graphics/Canvas; */
/* .param p2, "path" # Landroid/graphics/Path; */
/* .line 132 */
v0 = this.mPathPaint;
(( android.graphics.Canvas ) p1 ).drawPath ( p2, v0 ); // invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
return;
} // .end method
/* # virtual methods */
public void clearPath ( ) {
/* .locals 1 */
/* .line 246 */
v0 = this.mPathList;
/* .line 247 */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).invalidate ( ); // invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V
/* .line 248 */
return;
} // .end method
public Integer getMode ( ) {
/* .locals 1 */
/* .line 216 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I */
} // .end method
public void getPosition ( Float[] p0 ) {
/* .locals 2 */
/* .param p1, "outPosition" # [F */
/* .line 196 */
int v0 = 0; // const/4 v0, 0x0
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* aput v1, p1, v0 */
/* .line 197 */
int v0 = 1; // const/4 v0, 0x1
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* aput v1, p1, v0 */
/* .line 198 */
return;
} // .end method
public Boolean isKeepPath ( ) {
/* .locals 1 */
/* .line 224 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z */
} // .end method
public void move ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "deltaX" # F */
/* .param p2, "deltaY" # F */
/* .line 166 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* add-float/2addr v0, p1 */
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* add-float/2addr v1, p2 */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).setPosition ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setPosition(FF)V
/* .line 167 */
return;
} // .end method
public Boolean needExist ( ) {
/* .locals 1 */
/* .line 251 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z */
/* if-nez v0, :cond_0 */
/* .line 253 */
int v0 = 0; // const/4 v0, 0x0
/* .line 255 */
} // :cond_0
v0 = v0 = this.mPathList;
/* if-nez v0, :cond_1 */
/* .line 257 */
int v0 = 1; // const/4 v0, 0x1
/* .line 260 */
} // :cond_1
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z */
} // .end method
protected void onDraw ( android.graphics.Canvas p0 ) {
/* .locals 4 */
/* .param p1, "canvas" # Landroid/graphics/Canvas; */
/* .line 128 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z */
/* if-nez v0, :cond_0 */
/* .line 129 */
return;
/* .line 132 */
} // :cond_0
v0 = this.mPathList;
/* new-instance v1, Lcom/miui/server/input/stylus/laser/LaserView$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/input/stylus/laser/LaserView$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/laser/LaserView;Landroid/graphics/Canvas;)V */
/* .line 134 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z */
/* if-nez v0, :cond_1 */
/* .line 135 */
return;
/* .line 137 */
} // :cond_1
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I */
/* if-nez v0, :cond_2 */
/* .line 139 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutBlurRadius:F */
v3 = this.mOutBlurPaint;
(( android.graphics.Canvas ) p1 ).drawCircle ( v0, v1, v2, v3 ); // invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
/* .line 141 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutFillRadius:F */
v3 = this.mOutFillPaint;
(( android.graphics.Canvas ) p1 ).drawCircle ( v0, v1, v2, v3 ); // invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
/* .line 143 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserInRadius:F */
v3 = this.mInPaint;
(( android.graphics.Canvas ) p1 ).drawCircle ( v0, v1, v2, v3 ); // invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V
/* .line 144 */
} // :cond_2
int v1 = 1; // const/4 v1, 0x1
/* if-ne v0, v1, :cond_3 */
/* .line 145 */
v0 = this.mLaserPenImage;
(( android.graphics.drawable.Drawable ) v0 ).draw ( p1 ); // invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
/* .line 147 */
} // :cond_3
} // :goto_0
return;
} // .end method
public void setKeepPath ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "keepPath" # Z */
/* .line 231 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z */
/* if-ne v0, p1, :cond_0 */
/* .line 232 */
return;
/* .line 234 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z */
/* .line 236 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 237 */
/* new-instance v0, Landroid/graphics/Path; */
/* invoke-direct {v0}, Landroid/graphics/Path;-><init>()V */
/* .line 238 */
/* .local v0, "path":Landroid/graphics/Path; */
v1 = this.mPathList;
int v2 = 0; // const/4 v2, 0x0
/* .line 240 */
} // .end local v0 # "path":Landroid/graphics/Path;
} // :cond_1
return;
} // .end method
public void setMode ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 207 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I */
/* if-ne v0, p1, :cond_0 */
/* .line 208 */
return;
/* .line 210 */
} // :cond_0
/* iput p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I */
/* .line 211 */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).invalidate ( ); // invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V
/* .line 212 */
return;
} // .end method
public void setPosition ( Float p0, Float p1 ) {
/* .locals 6 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .line 177 */
v0 = v0 = this.mPathList;
/* if-nez v0, :cond_1 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 178 */
v0 = this.mPathList;
int v1 = 0; // const/4 v1, 0x0
/* check-cast v0, Landroid/graphics/Path; */
/* .line 179 */
/* .local v0, "path":Landroid/graphics/Path; */
v1 = (( android.graphics.Path ) v0 ).isEmpty ( ); // invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 180 */
(( android.graphics.Path ) v0 ).moveTo ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V
/* .line 182 */
} // :cond_0
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* add-float v3, p1, v1 */
/* const/high16 v4, 0x40000000 # 2.0f */
/* div-float/2addr v3, v4 */
/* add-float v5, p2, v2 */
/* div-float/2addr v5, v4 */
(( android.graphics.Path ) v0 ).quadTo ( v1, v2, v3, v5 ); // invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Path;->quadTo(FFFF)V
/* .line 185 */
} // .end local v0 # "path":Landroid/graphics/Path;
} // :cond_1
} // :goto_0
/* iput p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F */
/* .line 186 */
/* iput p2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F */
/* .line 187 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenWidth:F */
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterX:F */
/* mul-float/2addr v1, v0 */
/* sub-float v1, p1, v1 */
/* float-to-int v1, v1 */
/* .line 188 */
/* .local v1, "left":I */
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenHeight:F */
/* iget v3, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterY:F */
/* mul-float/2addr v3, v2 */
/* sub-float v3, p2, v3 */
/* float-to-int v3, v3 */
/* .line 189 */
/* .local v3, "top":I */
/* int-to-float v4, v1 */
/* add-float/2addr v4, v0 */
/* float-to-int v0, v4 */
/* .line 190 */
/* .local v0, "right":I */
/* int-to-float v4, v3 */
/* add-float/2addr v4, v2 */
/* float-to-int v2, v4 */
/* .line 191 */
/* .local v2, "bottom":I */
v4 = this.mLaserPenImage;
(( android.graphics.drawable.Drawable ) v4 ).setBounds ( v1, v3, v0, v2 ); // invoke-virtual {v4, v1, v3, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V
/* .line 192 */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).invalidate ( ); // invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V
/* .line 193 */
return;
} // .end method
public void setPosition ( android.graphics.PointF p0 ) {
/* .locals 2 */
/* .param p1, "position" # Landroid/graphics/PointF; */
/* .line 170 */
/* iget v0, p1, Landroid/graphics/PointF;->x:F */
/* iget v1, p1, Landroid/graphics/PointF;->y:F */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).setPosition ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setPosition(FF)V
/* .line 171 */
return;
} // .end method
public void setPresentationVisible ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "presentationVisible" # Z */
/* .line 155 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z */
/* .line 156 */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).invalidate ( ); // invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V
/* .line 157 */
return;
} // .end method
public void setVisible ( Boolean p0 ) {
/* .locals 0 */
/* .param p1, "visible" # Z */
/* .line 150 */
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z */
/* .line 151 */
(( com.miui.server.input.stylus.laser.LaserView ) p0 ).invalidate ( ); // invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V
/* .line 152 */
return;
} // .end method
