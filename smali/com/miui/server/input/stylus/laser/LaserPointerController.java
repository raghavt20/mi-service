public class com.miui.server.input.stylus.laser.LaserPointerController implements com.miui.server.input.stylus.laser.PointerControllerInterface {
	 /* .source "LaserPointerController.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/input/stylus/laser/LaserPointerController$H;, */
	 /* Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Integer DIRTY_KEEP_PATH;
private static final Integer DIRTY_MODE;
private static final Integer DIRTY_POSITION;
private static final Integer DIRTY_PRESENTATION_VISIBLE;
private static final Integer DIRTY_VISIBLE;
private static final Long KEY_DOWN_TIMEOUT;
private static final Long LONG_PRESS_TIMEOUT;
private static final Long MULTI_PRESS_TIMEOUT;
private static final Long POKE_USER_ACTIVITY_TIMEOUT;
private static final Long REMOVE_WINDOW_TIME;
private static final java.lang.String TAG;
private static final Long TIMEOUT_TIME;
/* # instance fields */
private Boolean mCanShowLaser;
private final android.content.Context mContext;
private Integer mDisplayId;
private android.hardware.display.DisplayViewport mDisplayViewport;
private Boolean mHandleByKeyDownTimeout;
private Boolean mHandleByLongPressing;
private final android.os.Handler mHandler;
private final android.os.HandlerThread mHandlerThread;
private final com.android.server.input.config.InputCommonConfig mInputCommonConfig;
private Boolean mIsDownFromScreenOff;
private Boolean mIsKeyDown;
private Boolean mIsScreenOn;
private final com.miui.server.input.stylus.laser.LaserPointerController$LaserState mLaserState;
private com.miui.server.input.stylus.laser.LaserView mLaserView;
private Long mLastDownTime;
private Long mLastPokeUserActivityTime;
private Long mLastRequestFadeTime;
private android.view.WindowManager$LayoutParams mLayoutParams;
private final java.lang.Object mLock;
private final com.android.server.input.MiuiInputManagerInternal mMiuiInputManagerInternal;
private final android.os.PowerManager mPowerManager;
private Integer mPressCounter;
private final com.miui.server.input.stylus.laser.LaserPointerController$LaserState mTempLaserState;
private final android.graphics.PointF mTempPosition;
private final android.view.WindowManager mWindowManger;
/* # direct methods */
public static void $r8$lambda$ZlpzQeSHlTWHEx84RzWCWfLgDHY ( com.miui.server.input.stylus.laser.LaserPointerController p0, Integer p1 ) { //synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->lambda$sendModeToBluetooth$0(I)V */
	 return;
} // .end method
static void -$$Nest$mclearPath ( com.miui.server.input.stylus.laser.LaserPointerController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->clearPath()V */
	 return;
} // .end method
static void -$$Nest$monKeyDownTimeout ( com.miui.server.input.stylus.laser.LaserPointerController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->onKeyDownTimeout()V */
	 return;
} // .end method
static void -$$Nest$monLaserKeyLongPressed ( com.miui.server.input.stylus.laser.LaserPointerController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->onLaserKeyLongPressed()V */
	 return;
} // .end method
static void -$$Nest$monLaserKeyPressed ( com.miui.server.input.stylus.laser.LaserPointerController p0, Integer p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->onLaserKeyPressed(I)V */
	 return;
} // .end method
static void -$$Nest$mrefreshLaserState ( com.miui.server.input.stylus.laser.LaserPointerController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->refreshLaserState()V */
	 return;
} // .end method
static void -$$Nest$mremoveLaserWindow ( com.miui.server.input.stylus.laser.LaserPointerController p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->removeLaserWindow()V */
	 return;
} // .end method
public com.miui.server.input.stylus.laser.LaserPointerController ( ) {
	 /* .locals 3 */
	 /* .line 94 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 65 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I */
	 /* .line 89 */
	 /* const-wide/32 v0, -0x1d4c0 */
	 /* iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastRequestFadeTime:J */
	 /* .line 92 */
	 /* const-wide/16 v0, -0x7d0 */
	 /* iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastPokeUserActivityTime:J */
	 /* .line 95 */
	 android.app.ActivityThread .currentActivityThread ( );
	 (( android.app.ActivityThread ) v0 ).getSystemContext ( ); // invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;
	 this.mContext = v0;
	 /* .line 96 */
	 /* const-class v1, Landroid/view/WindowManager; */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v1, Landroid/view/WindowManager; */
	 this.mWindowManger = v1;
	 /* .line 97 */
	 /* new-instance v1, Landroid/os/HandlerThread; */
	 final String v2 = "LaserPointerController"; // const-string v2, "LaserPointerController"
	 /* invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
	 this.mHandlerThread = v1;
	 /* .line 98 */
	 (( android.os.HandlerThread ) v1 ).start ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V
	 /* .line 99 */
	 /* new-instance v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$H; */
	 (( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
	 /* invoke-direct {v2, p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;-><init>(Lcom/miui/server/input/stylus/laser/LaserPointerController;Landroid/os/Looper;)V */
	 this.mHandler = v2;
	 /* .line 100 */
	 /* new-instance v1, Ljava/lang/Object; */
	 /* invoke-direct {v1}, Ljava/lang/Object;-><init>()V */
	 this.mLock = v1;
	 /* .line 101 */
	 /* new-instance v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState; */
	 /* invoke-direct {v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;-><init>()V */
	 this.mLaserState = v1;
	 /* .line 102 */
	 /* new-instance v1, Landroid/graphics/PointF; */
	 /* invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V */
	 this.mTempPosition = v1;
	 /* .line 103 */
	 /* new-instance v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState; */
	 /* invoke-direct {v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;-><init>()V */
	 this.mTempLaserState = v1;
	 /* .line 104 */
	 /* const-class v1, Landroid/os/PowerManager; */
	 (( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;
	 /* check-cast v0, Landroid/os/PowerManager; */
	 this.mPowerManager = v0;
	 /* .line 105 */
	 com.android.server.input.config.InputCommonConfig .getInstance ( );
	 this.mInputCommonConfig = v0;
	 /* .line 106 */
	 /* const-class v0, Lcom/android/server/input/MiuiInputManagerInternal; */
	 com.android.server.LocalServices .getService ( v0 );
	 /* check-cast v0, Lcom/android/server/input/MiuiInputManagerInternal; */
	 this.mMiuiInputManagerInternal = v0;
	 /* .line 107 */
	 return;
} // .end method
private void addLaserWindow ( ) {
	 /* .locals 3 */
	 /* .line 309 */
	 /* new-instance v0, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v1 = "Request add window, mLaserView is "; // const-string v1, "Request add window, mLaserView is "
	 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 310 */
	 v1 = this.mLaserView;
	 /* if-nez v1, :cond_0 */
	 final String v1 = "null"; // const-string v1, "null"
} // :cond_0
final String v1 = "not null"; // const-string v1, "not null"
} // :goto_0
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 309 */
final String v1 = "LaserPointerController"; // const-string v1, "LaserPointerController"
android.util.Slog .w ( v1,v0 );
/* .line 311 */
v0 = this.mLaserView;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 313 */
return;
/* .line 315 */
} // :cond_1
/* new-instance v0, Lcom/miui/server/input/stylus/laser/LaserView; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;-><init>(Landroid/content/Context;)V */
this.mLaserView = v0;
/* .line 316 */
int v1 = 0; // const/4 v1, 0x0
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setForceDarkAllowed ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setForceDarkAllowed(Z)V
/* .line 317 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getLayoutParams()Landroid/view/WindowManager$LayoutParams; */
this.mLayoutParams = v0;
/* .line 318 */
v1 = this.mWindowManger;
v2 = this.mLaserView;
/* .line 319 */
return;
} // .end method
private Boolean canShowLaserViewLocked ( ) {
/* .locals 2 */
/* .line 448 */
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z */
/* if-nez v0, :cond_1 */
} // :cond_0
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* if-ne v0, v1, :cond_2 */
} // :cond_1
} // :cond_2
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
} // .end method
private void clearPath ( ) {
/* .locals 1 */
/* .line 381 */
v0 = this.mLaserView;
/* if-nez v0, :cond_0 */
/* .line 382 */
return;
/* .line 384 */
} // :cond_0
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).clearPath ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserView;->clearPath()V
/* .line 385 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetWindowRemoveTimeout()V */
/* .line 386 */
return;
} // .end method
private void fadeLocked ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "transition" # I */
/* .line 164 */
v0 = this.mLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* if-nez v0, :cond_0 */
/* .line 165 */
return;
/* .line 167 */
} // :cond_0
v0 = this.mLaserState;
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* .line 168 */
/* const/16 v0, 0x8 */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 169 */
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* .line 171 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setModeLocked(I)V */
/* .line 173 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V */
/* .line 176 */
} // :cond_1
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadePresentationLocked(I)V */
/* .line 177 */
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z */
/* .line 178 */
return;
} // .end method
private void fadePresentationLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "transition" # I */
/* .line 202 */
v0 = this.mLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
/* if-nez v0, :cond_0 */
/* .line 204 */
return;
/* .line 206 */
} // :cond_0
v0 = this.mLaserState;
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
/* .line 207 */
/* const/16 v0, 0x10 */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 208 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastRequestFadeTime:J */
/* .line 209 */
return;
} // .end method
private android.hardware.display.DisplayViewport findDisplayViewportById ( java.util.List p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "displayId" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/hardware/display/DisplayViewport;", */
/* ">;I)", */
/* "Landroid/hardware/display/DisplayViewport;" */
/* } */
} // .end annotation
/* .line 501 */
/* .local p1, "displayViewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Landroid/hardware/display/DisplayViewport; */
/* .line 502 */
/* .local v1, "displayViewport":Landroid/hardware/display/DisplayViewport; */
/* iget v2, v1, Landroid/hardware/display/DisplayViewport;->displayId:I */
/* if-ne v2, p2, :cond_0 */
/* .line 503 */
/* .line 505 */
} // .end local v1 # "displayViewport":Landroid/hardware/display/DisplayViewport;
} // :cond_0
/* .line 506 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private android.view.WindowManager$LayoutParams getLayoutParams ( ) {
/* .locals 2 */
/* .line 480 */
/* new-instance v0, Landroid/view/WindowManager$LayoutParams; */
/* invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V */
/* .line 481 */
/* .local v0, "params":Landroid/view/WindowManager$LayoutParams; */
/* const/16 v1, 0x138 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I */
/* .line 485 */
int v1 = 1; // const/4 v1, 0x1
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->layoutInDisplayCutoutMode:I */
/* .line 487 */
/* const/16 v1, 0x7e2 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I */
/* .line 488 */
int v1 = -3; // const/4 v1, -0x3
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I */
/* .line 489 */
/* const v1, 0x800033 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I */
/* .line 490 */
int v1 = 0; // const/4 v1, 0x0
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I */
/* .line 491 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I */
/* .line 492 */
int v1 = -1; // const/4 v1, -0x1
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I */
/* .line 493 */
/* iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I */
/* .line 494 */
(( android.view.WindowManager$LayoutParams ) v0 ).setTrustedOverlay ( ); // invoke-virtual {v0}, Landroid/view/WindowManager$LayoutParams;->setTrustedOverlay()V
/* .line 495 */
final String v1 = "laser"; // const-string v1, "laser"
(( android.view.WindowManager$LayoutParams ) v0 ).setTitle ( v1 ); // invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V
/* .line 496 */
} // .end method
private Long getLongPressTimeoutMs ( ) {
/* .locals 2 */
/* .line 674 */
/* const-wide/16 v0, 0xc8 */
/* return-wide v0 */
} // .end method
private Integer getMaxPressCount ( ) {
/* .locals 1 */
/* .line 682 */
int v0 = 2; // const/4 v0, 0x2
} // .end method
private Long getMultiPressTimeout ( ) {
/* .locals 2 */
/* .line 678 */
/* const-wide/16 v0, 0x12c */
/* return-wide v0 */
} // .end method
private void hideLaserOrStopDrawingLocked ( ) {
/* .locals 3 */
/* .line 652 */
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
int v1 = 0; // const/4 v1, 0x0
/* if-nez v0, :cond_0 */
/* .line 654 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadePresentationLocked(I)V */
/* .line 655 */
} // :cond_0
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_1 */
/* .line 657 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V */
/* .line 659 */
} // :cond_1
} // :goto_0
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z */
/* .line 660 */
return;
} // .end method
private void interceptLaserKeyDown ( android.view.KeyEvent p0 ) {
/* .locals 9 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 537 */
(( android.view.KeyEvent ) p1 ).getDownTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastDownTime:J */
/* sub-long/2addr v0, v2 */
/* .line 538 */
/* .local v0, "keyDownInterval":J */
(( android.view.KeyEvent ) p1 ).getDownTime ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getDownTime()J
/* move-result-wide v2 */
/* iput-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastDownTime:J */
/* .line 539 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMultiPressTimeout()J */
/* move-result-wide v2 */
/* cmp-long v2, v0, v2 */
int v3 = 1; // const/4 v3, 0x1
/* if-ltz v2, :cond_0 */
/* .line 540 */
/* iput v3, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
/* .line 542 */
} // :cond_0
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
/* add-int/2addr v2, v3 */
/* iput v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
/* .line 545 */
} // :goto_0
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
int v4 = 5; // const/4 v4, 0x5
int v5 = 0; // const/4 v5, 0x0
/* if-ne v2, v3, :cond_1 */
/* .line 547 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 548 */
/* .local v2, "message":Landroid/os/Message; */
(( android.os.Message ) v2 ).setAsynchronous ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 549 */
v4 = this.mHandler;
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getLongPressTimeoutMs()J */
/* move-result-wide v6 */
(( android.os.Handler ) v4 ).sendMessageDelayed ( v2, v6, v7 ); // invoke-virtual {v4, v2, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 551 */
v4 = this.mHandler;
int v6 = 6; // const/4 v6, 0x6
(( android.os.Handler ) v4 ).obtainMessage ( v6 ); // invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 552 */
/* .local v6, "downTimeoutMessage":Landroid/os/Message; */
(( android.os.Message ) v6 ).setAsynchronous ( v3 ); // invoke-virtual {v6, v3}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 553 */
v4 = this.mHandler;
/* const-wide/32 v7, 0x3a980 */
(( android.os.Handler ) v4 ).sendMessageDelayed ( v6, v7, v8 ); // invoke-virtual {v4, v6, v7, v8}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 554 */
v7 = this.mLock;
/* monitor-enter v7 */
/* .line 555 */
try { // :try_start_0
/* iput-boolean v3, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsKeyDown:Z */
/* .line 556 */
/* iput-boolean v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByLongPressing:Z */
/* .line 557 */
/* iput-boolean v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByKeyDownTimeout:Z */
/* .line 558 */
/* monitor-exit v7 */
/* .line 559 */
} // .end local v2 # "message":Landroid/os/Message;
} // .end local v6 # "downTimeoutMessage":Landroid/os/Message;
/* .line 558 */
/* .restart local v2 # "message":Landroid/os/Message; */
/* .restart local v6 # "downTimeoutMessage":Landroid/os/Message; */
/* :catchall_0 */
/* move-exception v3 */
/* monitor-exit v7 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v3 */
/* .line 561 */
} // .end local v2 # "message":Landroid/os/Message;
} // .end local v6 # "downTimeoutMessage":Landroid/os/Message;
} // :cond_1
v2 = this.mHandler;
int v6 = 4; // const/4 v6, 0x4
(( android.os.Handler ) v2 ).removeMessages ( v6 ); // invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V
/* .line 562 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).removeMessages ( v4 ); // invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V
/* .line 563 */
v2 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I */
/* if-le v2, v3, :cond_2 */
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
v4 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I */
/* if-ne v2, v4, :cond_2 */
/* .line 565 */
v2 = this.mHandler;
/* iget v4, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
(( android.os.Handler ) v2 ).obtainMessage ( v6, v4, v5 ); // invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 566 */
/* .restart local v2 # "message":Landroid/os/Message; */
(( android.os.Message ) v2 ).setAsynchronous ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 567 */
(( android.os.Message ) v2 ).sendToTarget ( ); // invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
/* .line 570 */
} // .end local v2 # "message":Landroid/os/Message;
} // :cond_2
} // :goto_1
return;
} // .end method
private void interceptLaserKeyUp ( ) {
/* .locals 6 */
/* .line 574 */
v0 = this.mHandler;
int v1 = 5; // const/4 v1, 0x5
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 576 */
v0 = this.mHandler;
int v1 = 6; // const/4 v1, 0x6
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 577 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 578 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsKeyDown:Z */
/* .line 579 */
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByKeyDownTimeout:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 581 */
final String v1 = "LaserPointerController"; // const-string v1, "LaserPointerController"
final String v2 = "Key up handle by key down timeout, not process"; // const-string v2, "Key up handle by key down timeout, not process"
android.util.Slog .i ( v1,v2 );
/* .line 582 */
/* monitor-exit v0 */
return;
/* .line 584 */
} // :cond_0
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByLongPressing:Z */
int v3 = 1; // const/4 v3, 0x1
/* if-nez v2, :cond_3 */
/* .line 586 */
v2 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I */
int v4 = 4; // const/4 v4, 0x4
/* if-ne v2, v3, :cond_1 */
/* .line 588 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).obtainMessage ( v4, v3, v1 ); // invoke-virtual {v2, v4, v3, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 589 */
/* .local v1, "msg":Landroid/os/Message; */
(( android.os.Message ) v1 ).setAsynchronous ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 590 */
(( android.os.Message ) v1 ).sendToTarget ( ); // invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
} // .end local v1 # "msg":Landroid/os/Message;
/* .line 591 */
} // :cond_1
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
v5 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMaxPressCount()I */
/* if-ge v2, v5, :cond_2 */
/* .line 593 */
v2 = this.mHandler;
/* iget v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
(( android.os.Handler ) v2 ).obtainMessage ( v4, v5, v1 ); // invoke-virtual {v2, v4, v5, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;
/* .line 594 */
/* .local v1, "message":Landroid/os/Message; */
(( android.os.Message ) v1 ).setAsynchronous ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Message;->setAsynchronous(Z)V
/* .line 595 */
v2 = this.mHandler;
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->getMultiPressTimeout()J */
/* move-result-wide v3 */
(( android.os.Handler ) v2 ).sendMessageDelayed ( v1, v3, v4 ); // invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 596 */
/* nop */
} // .end local v1 # "message":Landroid/os/Message;
/* .line 591 */
} // :cond_2
} // :goto_0
/* .line 597 */
} // :cond_3
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mPressCounter:I */
/* if-ne v1, v3, :cond_4 */
/* .line 599 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->hideLaserOrStopDrawingLocked()V */
/* .line 601 */
} // :cond_4
} // :goto_1
/* monitor-exit v0 */
/* .line 602 */
return;
/* .line 601 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void invalidateLocked ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "dirty" # I */
/* .line 511 */
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_0
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 512 */
/* .local v0, "wasDirty":Z */
} // :goto_0
v2 = this.mLaserState;
/* iget v3, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* or-int/2addr v3, p1 */
/* iput v3, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 513 */
/* if-nez v0, :cond_1 */
/* .line 514 */
v2 = this.mHandler;
(( android.os.Handler ) v2 ).sendEmptyMessage ( v1 ); // invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 516 */
} // :cond_1
return;
} // .end method
private void lambda$sendModeToBluetooth$0 ( Integer p0 ) { //synthethic
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 426 */
/* new-instance v0, Landroid/content/Intent; */
final String v1 = "com.android.input.laser.LASER_STATE"; // const-string v1, "com.android.input.laser.LASER_STATE"
/* invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V */
/* .line 427 */
/* .local v0, "intent":Landroid/content/Intent; */
final String v1 = "com.xiaomi.bluetooth"; // const-string v1, "com.xiaomi.bluetooth"
(( android.content.Intent ) v0 ).setPackage ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;
/* .line 428 */
final String v1 = "mode"; // const-string v1, "mode"
(( android.content.Intent ) v0 ).putExtra ( v1, p1 ); // invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
/* .line 429 */
/* const/high16 v1, 0x10000000 */
(( android.content.Intent ) v0 ).addFlags ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;
/* .line 430 */
v1 = this.mContext;
(( android.content.Context ) v1 ).sendBroadcast ( v0 ); // invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
/* .line 431 */
return;
} // .end method
private Boolean movePointInScreenLocked ( android.graphics.PointF p0 ) {
/* .locals 3 */
/* .param p1, "pointF" # Landroid/graphics/PointF; */
/* .line 460 */
v0 = this.mDisplayViewport;
/* if-nez v0, :cond_0 */
/* .line 461 */
int v0 = 0; // const/4 v0, 0x0
/* .line 463 */
} // :cond_0
v0 = this.logicalFrame;
/* .line 464 */
/* .local v0, "logicalFrame":Landroid/graphics/Rect; */
/* iget v1, p1, Landroid/graphics/PointF;->x:F */
/* iget v2, v0, Landroid/graphics/Rect;->left:I */
/* int-to-float v2, v2 */
/* cmpg-float v1, v1, v2 */
/* if-gez v1, :cond_1 */
/* .line 465 */
/* iget v1, v0, Landroid/graphics/Rect;->left:I */
/* int-to-float v1, v1 */
/* iput v1, p1, Landroid/graphics/PointF;->x:F */
/* .line 467 */
} // :cond_1
/* iget v1, p1, Landroid/graphics/PointF;->x:F */
/* iget v2, v0, Landroid/graphics/Rect;->right:I */
/* int-to-float v2, v2 */
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_2 */
/* .line 468 */
/* iget v1, v0, Landroid/graphics/Rect;->right:I */
/* int-to-float v1, v1 */
/* iput v1, p1, Landroid/graphics/PointF;->x:F */
/* .line 470 */
} // :cond_2
/* iget v1, p1, Landroid/graphics/PointF;->y:F */
/* iget v2, v0, Landroid/graphics/Rect;->top:I */
/* int-to-float v2, v2 */
/* cmpg-float v1, v1, v2 */
/* if-gez v1, :cond_3 */
/* .line 471 */
/* iget v1, v0, Landroid/graphics/Rect;->top:I */
/* int-to-float v1, v1 */
/* iput v1, p1, Landroid/graphics/PointF;->y:F */
/* .line 473 */
} // :cond_3
/* iget v1, p1, Landroid/graphics/PointF;->y:F */
/* iget v2, v0, Landroid/graphics/Rect;->bottom:I */
/* int-to-float v2, v2 */
/* cmpl-float v1, v1, v2 */
/* if-lez v1, :cond_4 */
/* .line 474 */
/* iget v1, v0, Landroid/graphics/Rect;->bottom:I */
/* int-to-float v1, v1 */
/* iput v1, p1, Landroid/graphics/PointF;->y:F */
/* .line 476 */
} // :cond_4
int v1 = 1; // const/4 v1, 0x1
} // .end method
private void onKeyDownTimeout ( ) {
/* .locals 2 */
/* .line 666 */
final String v0 = "LaserPointerController"; // const-string v0, "LaserPointerController"
final String v1 = "May be the hardware occurred an exception, laser key not up"; // const-string v1, "May be the hardware occurred an exception, laser key not up"
android.util.Slog .w ( v0,v1 );
/* .line 667 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 668 */
int v1 = 1; // const/4 v1, 0x1
try { // :try_start_0
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByKeyDownTimeout:Z */
/* .line 669 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->hideLaserOrStopDrawingLocked()V */
/* .line 670 */
/* monitor-exit v0 */
/* .line 671 */
return;
/* .line 670 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onLaserKeyLongPressed ( ) {
/* .locals 3 */
/* .line 606 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 607 */
try { // :try_start_0
/* iget-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsKeyDown:Z */
/* if-nez v1, :cond_0 */
/* .line 609 */
/* monitor-exit v0 */
return;
/* .line 611 */
} // :cond_0
final String v1 = "LaserPointerController"; // const-string v1, "LaserPointerController"
final String v2 = "On laser key long pressed"; // const-string v2, "On laser key long pressed"
android.util.Slog .i ( v1,v2 );
/* .line 612 */
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mCanShowLaser:Z */
/* .line 613 */
/* iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mHandleByLongPressing:Z */
/* .line 614 */
v2 = this.mLaserState;
/* iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* if-nez v2, :cond_1 */
/* .line 616 */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadePresentationLocked(I)V */
/* .line 617 */
v1 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v1 );
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v1 ).trackStylusLaserTrigger ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusLaserTrigger()V
/* .line 618 */
} // :cond_1
v2 = this.mLaserState;
/* iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* if-ne v2, v1, :cond_2 */
/* .line 620 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V */
/* .line 621 */
v1 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v1 );
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v1 ).trackStylusHighLightTrigger ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusHighLightTrigger()V
/* .line 623 */
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
/* .line 624 */
return;
/* .line 623 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void onLaserKeyPressed ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "count" # I */
/* .line 627 */
final String v0 = "LaserPointerController"; // const-string v0, "LaserPointerController"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "On laser key pressed, count = "; // const-string v2, "On laser key pressed, count = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 628 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 629 */
int v1 = 1; // const/4 v1, 0x1
/* if-ne p1, v1, :cond_1 */
/* .line 631 */
try { // :try_start_0
v2 = this.mLaserState;
/* iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
int v3 = 0; // const/4 v3, 0x0
/* if-nez v2, :cond_0 */
/* .line 633 */
/* invoke-direct {p0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setModeLocked(I)V */
/* .line 635 */
/* invoke-direct {p0, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadePresentationLocked(I)V */
/* .line 636 */
v1 = this.mContext;
com.miui.server.input.stylus.StylusOneTrackHelper .getInstance ( v1 );
(( com.miui.server.input.stylus.StylusOneTrackHelper ) v1 ).trackStylusPenTrigger ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/StylusOneTrackHelper;->trackStylusPenTrigger()V
/* .line 637 */
} // :cond_0
v2 = this.mLaserState;
/* iget v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* if-ne v2, v1, :cond_2 */
/* .line 639 */
/* invoke-direct {p0, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setModeLocked(I)V */
/* .line 641 */
/* invoke-direct {p0, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadePresentationLocked(I)V */
/* .line 647 */
/* :catchall_0 */
/* move-exception v1 */
/* .line 643 */
} // :cond_1
int v1 = 2; // const/4 v1, 0x2
/* if-ne p1, v1, :cond_2 */
/* .line 645 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->clearPath()V */
/* .line 647 */
} // :cond_2
} // :goto_0
/* monitor-exit v0 */
/* .line 648 */
return;
/* .line 647 */
} // :goto_1
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void pokeUserActivity ( ) {
/* .locals 6 */
/* .line 397 */
android.os.SystemClock .uptimeMillis ( );
/* move-result-wide v0 */
/* .line 398 */
/* .local v0, "now":J */
/* iget-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastPokeUserActivityTime:J */
/* sub-long v2, v0, v2 */
/* const-wide/16 v4, 0x7d0 */
/* cmp-long v2, v2, v4 */
/* if-gez v2, :cond_0 */
/* .line 399 */
return;
/* .line 401 */
} // :cond_0
/* iput-wide v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastPokeUserActivityTime:J */
/* .line 402 */
v2 = this.mPowerManager;
int v3 = 2; // const/4 v3, 0x2
int v4 = 0; // const/4 v4, 0x0
(( android.os.PowerManager ) v2 ).userActivity ( v0, v1, v3, v4 ); // invoke-virtual {v2, v0, v1, v3, v4}, Landroid/os/PowerManager;->userActivity(JII)V
/* .line 404 */
return;
} // .end method
private void refreshLaserState ( ) {
/* .locals 3 */
/* .line 325 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 326 */
try { // :try_start_0
v1 = this.mTempLaserState;
v2 = this.mLaserState;
(( com.miui.server.input.stylus.laser.LaserPointerController$LaserState ) v1 ).copyFrom ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->copyFrom(Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;)V
/* .line 327 */
v1 = this.mLaserState;
(( com.miui.server.input.stylus.laser.LaserPointerController$LaserState ) v1 ).resetDirty ( ); // invoke-virtual {v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->resetDirty()V
/* .line 328 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 330 */
v0 = this.mTempLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* and-int/lit8 v0, v0, 0x8 */
int v1 = 1; // const/4 v1, 0x1
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = this.mTempLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 331 */
v0 = this.mLaserView;
/* if-nez v0, :cond_0 */
/* .line 332 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->addLaserWindow()V */
/* .line 334 */
} // :cond_0
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setVisible ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setVisible(Z)V
/* .line 337 */
} // :goto_0
v0 = this.mMiuiInputManagerInternal;
(( com.android.server.input.MiuiInputManagerInternal ) v0 ).hideMouseCursor ( ); // invoke-virtual {v0}, Lcom/android/server/input/MiuiInputManagerInternal;->hideMouseCursor()V
/* .line 339 */
v0 = this.mTempLaserState;
(( com.miui.server.input.stylus.laser.LaserPointerController$LaserState ) v0 ).markAllDirty ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->markAllDirty()V
/* .line 341 */
} // :cond_1
v0 = this.mLaserView;
/* if-nez v0, :cond_2 */
/* .line 343 */
return;
/* .line 346 */
} // :cond_2
v0 = this.mTempLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* and-int/lit8 v0, v0, 0x8 */
if ( v0 != null) { // if-eqz v0, :cond_3
v0 = this.mTempLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* if-nez v0, :cond_3 */
/* .line 347 */
v0 = this.mLaserView;
int v2 = 0; // const/4 v2, 0x0
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setVisible ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/input/stylus/laser/LaserView;->setVisible(Z)V
/* .line 349 */
v0 = this.mLaserView;
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setKeepPath ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/input/stylus/laser/LaserView;->setKeepPath(Z)V
/* .line 351 */
v0 = this.mLaserView;
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).clearPath ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserView;->clearPath()V
/* .line 352 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetWindowRemoveTimeout()V */
/* .line 355 */
} // :cond_3
v0 = this.mTempLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* if-nez v0, :cond_4 */
/* .line 357 */
return;
/* .line 359 */
} // :cond_4
v0 = this.mTempLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* and-int/lit8 v0, v0, 0x10 */
if ( v0 != null) { // if-eqz v0, :cond_5
/* .line 360 */
v0 = this.mLaserView;
v2 = this.mTempLaserState;
/* iget-boolean v2, v2, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setPresentationVisible ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/input/stylus/laser/LaserView;->setPresentationVisible(Z)V
/* .line 362 */
} // :cond_5
v0 = this.mTempLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* and-int/2addr v0, v1 */
if ( v0 != null) { // if-eqz v0, :cond_6
/* .line 363 */
v0 = this.mLaserView;
v1 = this.mTempLaserState;
v1 = this.mPosition;
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setPosition ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setPosition(Landroid/graphics/PointF;)V
/* .line 365 */
} // :cond_6
v0 = this.mTempLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* and-int/lit8 v0, v0, 0x2 */
if ( v0 != null) { // if-eqz v0, :cond_7
/* .line 366 */
v0 = this.mLaserView;
v1 = this.mTempLaserState;
/* iget v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setMode ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setMode(I)V
/* .line 368 */
} // :cond_7
v0 = this.mTempLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* and-int/lit8 v0, v0, 0x4 */
if ( v0 != null) { // if-eqz v0, :cond_8
/* .line 369 */
v0 = this.mLaserView;
v1 = this.mTempLaserState;
/* iget-boolean v1, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z */
(( com.miui.server.input.stylus.laser.LaserView ) v0 ).setKeepPath ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setKeepPath(Z)V
/* .line 372 */
} // :cond_8
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->pokeUserActivity()V */
/* .line 374 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetWindowRemoveTimeout()V */
/* .line 375 */
return;
/* .line 328 */
/* :catchall_0 */
/* move-exception v1 */
try { // :try_start_1
/* monitor-exit v0 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v1 */
} // .end method
private void removeLaserWindow ( ) {
/* .locals 3 */
/* .line 290 */
final String v0 = "LaserPointerController"; // const-string v0, "LaserPointerController"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Request remove window, mLaserView is "; // const-string v2, "Request remove window, mLaserView is "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 291 */
v2 = this.mLaserView;
/* if-nez v2, :cond_0 */
final String v2 = "null"; // const-string v2, "null"
} // :cond_0
final String v2 = "not null"; // const-string v2, "not null"
} // :goto_0
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 290 */
android.util.Slog .w ( v0,v1 );
/* .line 292 */
v0 = this.mLaserView;
/* if-nez v0, :cond_1 */
/* .line 294 */
return;
/* .line 296 */
} // :cond_1
v1 = this.mWindowManger;
/* .line 297 */
int v0 = 0; // const/4 v0, 0x0
this.mLayoutParams = v0;
/* .line 298 */
this.mLaserView = v0;
/* .line 299 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 301 */
try { // :try_start_0
v1 = this.mLaserState;
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* .line 302 */
/* monitor-exit v0 */
/* .line 303 */
return;
/* .line 302 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
private void resetPositionLocked ( ) {
/* .locals 3 */
/* .line 279 */
v0 = this.mDisplayViewport;
/* if-nez v0, :cond_0 */
/* .line 280 */
return;
/* .line 282 */
} // :cond_0
v0 = this.logicalFrame;
v0 = (( android.graphics.Rect ) v0 ).width ( ); // invoke-virtual {v0}, Landroid/graphics/Rect;->width()I
/* int-to-float v0, v0 */
/* const/high16 v1, 0x40000000 # 2.0f */
/* div-float/2addr v0, v1 */
v2 = this.mDisplayViewport;
v2 = this.logicalFrame;
/* .line 283 */
v2 = (( android.graphics.Rect ) v2 ).height ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->height()I
/* int-to-float v2, v2 */
/* div-float/2addr v2, v1 */
/* .line 282 */
int v1 = 1; // const/4 v1, 0x1
/* invoke-direct {p0, v0, v2, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V */
/* .line 284 */
return;
} // .end method
private void resetWindowRemoveTimeout ( ) {
/* .locals 4 */
/* .line 389 */
v0 = this.mHandler;
int v1 = 2; // const/4 v1, 0x2
(( android.os.Handler ) v0 ).removeMessages ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
/* .line 390 */
v0 = this.mLaserView;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = (( com.miui.server.input.stylus.laser.LaserView ) v0 ).needExist ( ); // invoke-virtual {v0}, Lcom/miui/server/input/stylus/laser/LaserView;->needExist()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 393 */
} // :cond_0
v0 = this.mHandler;
/* const-wide/32 v2, 0x1d4c0 */
(( android.os.Handler ) v0 ).sendEmptyMessageDelayed ( v1, v2, v3 ); // invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
/* .line 394 */
return;
/* .line 391 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void sendModeToBluetooth ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "mode" # I */
/* .line 424 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Sync mode to bluetooth, mode = "; // const-string v1, "Sync mode to bluetooth, mode = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LaserPointerController"; // const-string v1, "LaserPointerController"
android.util.Slog .w ( v1,v0 );
/* .line 425 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/input/stylus/laser/LaserPointerController$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/laser/LaserPointerController;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 432 */
return;
} // .end method
private void setKeepPathLocked ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "keepPath" # Z */
/* .line 436 */
v0 = this.mLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z */
/* if-ne v0, p1, :cond_0 */
/* .line 437 */
return;
/* .line 439 */
} // :cond_0
v0 = this.mLaserState;
/* iput-boolean p1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z */
/* .line 440 */
int v0 = 4; // const/4 v0, 0x4
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 441 */
v0 = this.mInputCommonConfig;
(( com.android.server.input.config.InputCommonConfig ) v0 ).setLaserIsDrawing ( p1 ); // invoke-virtual {v0, p1}, Lcom/android/server/input/config/InputCommonConfig;->setLaserIsDrawing(Z)V
/* .line 442 */
v0 = this.mInputCommonConfig;
(( com.android.server.input.config.InputCommonConfig ) v0 ).flushToNative ( ); // invoke-virtual {v0}, Lcom/android/server/input/config/InputCommonConfig;->flushToNative()V
/* .line 443 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v1, "set keep path, new mode = " */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "LaserPointerController"; // const-string v1, "LaserPointerController"
android.util.Slog .w ( v1,v0 );
/* .line 444 */
return;
} // .end method
private void setModeLocked ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "mode" # I */
/* .line 408 */
v0 = this.mLaserState;
/* iget v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* if-ne v0, p1, :cond_0 */
/* .line 409 */
return;
/* .line 411 */
} // :cond_0
v0 = this.mLaserState;
/* iput p1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* .line 412 */
int v0 = 2; // const/4 v0, 0x2
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 413 */
/* if-nez p1, :cond_1 */
/* .line 415 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setKeepPathLocked(Z)V */
/* .line 417 */
} // :cond_1
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->sendModeToBluetooth(I)V */
/* .line 418 */
return;
} // .end method
private void setPositionLocked ( Float p0, Float p1, Boolean p2 ) {
/* .locals 3 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .param p3, "force" # Z */
/* .line 123 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->canShowLaserViewLocked()Z */
/* if-nez v0, :cond_0 */
/* if-nez p3, :cond_0 */
/* .line 125 */
return;
/* .line 127 */
} // :cond_0
v0 = this.mTempPosition;
(( android.graphics.PointF ) v0 ).set ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V
/* .line 128 */
v0 = this.mTempPosition;
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->movePointInScreenLocked(Landroid/graphics/PointF;)Z */
/* if-nez v0, :cond_1 */
v0 = this.mTempPosition;
v1 = this.mLaserState;
v1 = this.mPosition;
/* .line 129 */
v0 = (( android.graphics.PointF ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 130 */
return;
/* .line 132 */
} // :cond_1
v0 = this.mLaserState;
v0 = this.mPosition;
v1 = this.mTempPosition;
/* iget v1, v1, Landroid/graphics/PointF;->x:F */
v2 = this.mTempPosition;
/* iget v2, v2, Landroid/graphics/PointF;->y:F */
(( android.graphics.PointF ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V
/* .line 133 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 134 */
return;
} // .end method
private void unfadeLocked ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "transition" # I */
/* .line 189 */
v0 = this.mLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* if-nez v0, :cond_1 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->canShowLaserViewLocked()Z */
/* if-nez v0, :cond_0 */
/* .line 193 */
} // :cond_0
v0 = this.mLaserState;
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* .line 194 */
/* const/16 v0, 0x8 */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 195 */
return;
/* .line 191 */
} // :cond_1
} // :goto_0
return;
} // .end method
private void unfadePresentationLocked ( Integer p0 ) {
/* .locals 4 */
/* .param p1, "transition" # I */
/* .line 217 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadeLocked(I)V */
/* .line 218 */
v0 = this.mLaserState;
/* iget-boolean v0, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 220 */
return;
/* .line 222 */
} // :cond_0
v0 = this.mLaserState;
int v1 = 1; // const/4 v1, 0x1
/* iput-boolean v1, v0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
/* .line 223 */
/* const/16 v0, 0x10 */
/* invoke-direct {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->invalidateLocked(I)V */
/* .line 224 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v0 */
/* iget-wide v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mLastRequestFadeTime:J */
/* sub-long/2addr v0, v2 */
/* const-wide/32 v2, 0x1d4c0 */
/* cmp-long v0, v0, v2 */
/* if-lez v0, :cond_1 */
/* .line 226 */
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPositionLocked()V */
/* .line 228 */
} // :cond_1
return;
} // .end method
/* # virtual methods */
public Boolean canShowPointer ( ) {
/* .locals 2 */
/* .line 272 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 273 */
try { // :try_start_0
v1 = /* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->canShowLaserViewLocked()Z */
/* monitor-exit v0 */
/* .line 274 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void fade ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "transition" # I */
/* .line 157 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 158 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadeLocked(I)V */
/* .line 159 */
/* monitor-exit v0 */
/* .line 160 */
return;
/* .line 159 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void getPosition ( Float[] p0 ) {
/* .locals 3 */
/* .param p1, "outPosition" # [F */
/* .line 138 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 139 */
try { // :try_start_0
v1 = this.mLaserState;
v1 = this.mPosition;
/* iget v1, v1, Landroid/graphics/PointF;->x:F */
int v2 = 0; // const/4 v2, 0x0
/* aput v1, p1, v2 */
/* .line 140 */
v1 = this.mLaserState;
v1 = this.mPosition;
/* iget v1, v1, Landroid/graphics/PointF;->y:F */
int v2 = 1; // const/4 v2, 0x1
/* aput v1, p1, v2 */
/* .line 141 */
/* monitor-exit v0 */
/* .line 142 */
return;
/* .line 141 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public Boolean interceptLaserKey ( android.view.KeyEvent p0 ) {
/* .locals 4 */
/* .param p1, "event" # Landroid/view/KeyEvent; */
/* .line 519 */
v0 = (( android.view.KeyEvent ) p1 ).getAction ( ); // invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I
int v1 = 1; // const/4 v1, 0x1
/* if-nez v0, :cond_0 */
/* move v0, v1 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* .line 520 */
/* .local v0, "isDown":Z */
} // :goto_0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 521 */
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsScreenOn:Z */
/* xor-int/2addr v2, v1 */
/* iput-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsDownFromScreenOff:Z */
/* .line 523 */
} // :cond_1
/* iget-boolean v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsDownFromScreenOff:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 525 */
final String v2 = "LaserPointerController"; // const-string v2, "LaserPointerController"
final String v3 = "Screen is off when laser key down, not response."; // const-string v3, "Screen is off when laser key down, not response."
android.util.Slog .w ( v2,v3 );
/* .line 526 */
/* .line 528 */
} // :cond_2
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 529 */
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->interceptLaserKeyDown(Landroid/view/KeyEvent;)V */
/* .line 531 */
} // :cond_3
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->interceptLaserKeyUp()V */
/* .line 533 */
} // :goto_1
} // .end method
public void move ( Float p0, Float p1 ) {
/* .locals 4 */
/* .param p1, "deltaX" # F */
/* .param p2, "deltaY" # F */
/* .line 148 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 149 */
try { // :try_start_0
v1 = this.mLaserState;
v1 = this.mPosition;
/* iget v1, v1, Landroid/graphics/PointF;->x:F */
/* add-float/2addr v1, p1 */
/* .line 150 */
/* .local v1, "newX":F */
v2 = this.mLaserState;
v2 = this.mPosition;
/* iget v2, v2, Landroid/graphics/PointF;->y:F */
/* add-float/2addr v2, p2 */
/* .line 151 */
/* .local v2, "newY":F */
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {p0, v1, v2, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V */
/* .line 152 */
/* monitor-exit v0 */
/* .line 153 */
return;
/* .line 152 */
} // .end local v1 # "newX":F
} // .end local v2 # "newY":F
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void resetPosition ( ) {
/* .locals 2 */
/* .line 265 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 266 */
try { // :try_start_0
/* invoke-direct {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPositionLocked()V */
/* .line 267 */
/* monitor-exit v0 */
/* .line 268 */
return;
/* .line 267 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setDisplayId ( Integer p0 ) {
/* .locals 3 */
/* .param p1, "displayId" # I */
/* .line 256 */
final String v0 = "LaserPointerController"; // const-string v0, "LaserPointerController"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "DisplayId update, new displayId = "; // const-string v2, "DisplayId update, new displayId = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = ", old displayId = "; // const-string v2, ", old displayId = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v0,v1 );
/* .line 258 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 259 */
try { // :try_start_0
/* iput p1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I */
/* .line 260 */
/* monitor-exit v0 */
/* .line 261 */
return;
/* .line 260 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setDisplayViewPort ( java.util.List p0 ) {
/* .locals 6 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Landroid/hardware/display/DisplayViewport;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 232 */
/* .local p1, "viewports":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/display/DisplayViewport;>;" */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 233 */
try { // :try_start_0
/* iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I */
/* invoke-direct {p0, p1, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->findDisplayViewportById(Ljava/util/List;I)Landroid/hardware/display/DisplayViewport; */
/* .line 234 */
/* .local v1, "displayViewport":Landroid/hardware/display/DisplayViewport; */
int v2 = 0; // const/4 v2, 0x0
/* if-nez v1, :cond_0 */
/* .line 235 */
final String v3 = "LaserPointerController"; // const-string v3, "LaserPointerController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Can\'t find the designated viewport with ID "; // const-string v5, "Can\'t find the designated viewport with ID "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v5, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mDisplayId:I */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = " to update laser input mapper.Fall back to default display"; // const-string v5, " to update laser input mapper.Fall back to default display"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v3,v4 );
/* .line 237 */
/* invoke-direct {p0, p1, v2}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->findDisplayViewportById(Ljava/util/List;I)Landroid/hardware/display/DisplayViewport; */
/* move-object v1, v3 */
/* .line 239 */
} // :cond_0
/* if-nez v1, :cond_1 */
/* .line 240 */
final String v2 = "LaserPointerController"; // const-string v2, "LaserPointerController"
final String v3 = "Still can\'t find a viable viewport to update cursor input mapper.Skip setting it to LaserPointerController."; // const-string v3, "Still can\'t find a viable viewport to update cursor input mapper.Skip setting it to LaserPointerController."
android.util.Slog .e ( v2,v3 );
/* .line 242 */
/* monitor-exit v0 */
return;
/* .line 244 */
} // :cond_1
final String v3 = "LaserPointerController"; // const-string v3, "LaserPointerController"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Display viewport updated, new display viewport = "; // const-string v5, "Display viewport updated, new display viewport = "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v3,v4 );
/* .line 245 */
this.mDisplayViewport = v1;
/* .line 247 */
/* invoke-direct {p0, v2}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fadeLocked(I)V */
/* .line 249 */
v2 = this.logicalFrame;
v2 = (( android.graphics.Rect ) v2 ).width ( ); // invoke-virtual {v2}, Landroid/graphics/Rect;->width()I
/* int-to-float v2, v2 */
/* const/high16 v3, 0x40000000 # 2.0f */
/* div-float/2addr v2, v3 */
v4 = this.logicalFrame;
/* .line 250 */
v4 = (( android.graphics.Rect ) v4 ).height ( ); // invoke-virtual {v4}, Landroid/graphics/Rect;->height()I
/* int-to-float v4, v4 */
/* div-float/2addr v4, v3 */
/* .line 249 */
int v3 = 1; // const/4 v3, 0x1
/* invoke-direct {p0, v2, v4, v3}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V */
/* .line 251 */
} // .end local v1 # "displayViewport":Landroid/hardware/display/DisplayViewport;
/* monitor-exit v0 */
/* .line 252 */
return;
/* .line 251 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setPosition ( Float p0, Float p1 ) {
/* .locals 2 */
/* .param p1, "x" # F */
/* .param p2, "y" # F */
/* .line 111 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 112 */
int v1 = 0; // const/4 v1, 0x0
try { // :try_start_0
/* invoke-direct {p0, p1, p2, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->setPositionLocked(FFZ)V */
/* .line 113 */
/* monitor-exit v0 */
/* .line 114 */
return;
/* .line 113 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void setScreenState ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "isScreenOn" # Z */
/* .line 686 */
/* iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsScreenOn:Z */
/* if-ne v0, p1, :cond_0 */
/* .line 687 */
return;
/* .line 689 */
} // :cond_0
/* iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController;->mIsScreenOn:Z */
/* .line 690 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* .line 691 */
return;
/* .line 693 */
} // :cond_1
final String v0 = "LaserPointerController"; // const-string v0, "LaserPointerController"
final String v1 = "Screen off, fade laser and reset position"; // const-string v1, "Screen off, fade laser and reset position"
android.util.Slog .w ( v0,v1 );
/* .line 695 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.input.stylus.laser.LaserPointerController ) p0 ).fade ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->fade(I)V
/* .line 696 */
(( com.miui.server.input.stylus.laser.LaserPointerController ) p0 ).resetPosition ( ); // invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->resetPosition()V
/* .line 697 */
return;
} // .end method
public void unfade ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "transition" # I */
/* .line 182 */
v0 = this.mLock;
/* monitor-enter v0 */
/* .line 183 */
try { // :try_start_0
/* invoke-direct {p0, p1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->unfadeLocked(I)V */
/* .line 184 */
/* monitor-exit v0 */
/* .line 185 */
return;
/* .line 184 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
