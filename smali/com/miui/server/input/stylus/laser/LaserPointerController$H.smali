.class Lcom/miui/server/input/stylus/laser/LaserPointerController$H;
.super Landroid/os/Handler;
.source "LaserPointerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/input/stylus/laser/LaserPointerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "H"
.end annotation


# static fields
.field private static final MSG_CLEAR_PATH:I = 0x3

.field private static final MSG_KEY_DOWN_TIMEOUT:I = 0x6

.field private static final MSG_KEY_LONG_PRESSED:I = 0x5

.field private static final MSG_KEY_PRESSED:I = 0x4

.field private static final MSG_REMOVE_LASER_WINDOW:I = 0x2

.field private static final MSG_UPDATE_LASER_STATE:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;


# direct methods
.method public constructor <init>(Lcom/miui/server/input/stylus/laser/LaserPointerController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 784
    iput-object p1, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    .line 785
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 786
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .line 790
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 807
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-static {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->-$$Nest$monKeyDownTimeout(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V

    .line 808
    goto :goto_0

    .line 804
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-static {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->-$$Nest$monLaserKeyLongPressed(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V

    .line 805
    goto :goto_0

    .line 801
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->-$$Nest$monLaserKeyPressed(Lcom/miui/server/input/stylus/laser/LaserPointerController;I)V

    .line 802
    goto :goto_0

    .line 798
    :pswitch_3
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-static {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->-$$Nest$mclearPath(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V

    .line 799
    goto :goto_0

    .line 795
    :pswitch_4
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-static {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->-$$Nest$mremoveLaserWindow(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V

    .line 796
    goto :goto_0

    .line 792
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$H;->this$0:Lcom/miui/server/input/stylus/laser/LaserPointerController;

    invoke-static {v0}, Lcom/miui/server/input/stylus/laser/LaserPointerController;->-$$Nest$mrefreshLaserState(Lcom/miui/server/input/stylus/laser/LaserPointerController;)V

    .line 793
    nop

    .line 812
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
