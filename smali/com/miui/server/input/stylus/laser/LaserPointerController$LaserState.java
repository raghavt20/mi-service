class com.miui.server.input.stylus.laser.LaserPointerController$LaserState {
	 /* .source "LaserPointerController.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/stylus/laser/LaserPointerController; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "LaserState" */
} // .end annotation
/* # instance fields */
Integer mCurrentMode;
Integer mDirty;
Boolean mKeepPath;
final android.graphics.PointF mPosition;
Boolean mPresentationVisible;
Boolean mVisible;
/* # direct methods */
 com.miui.server.input.stylus.laser.LaserPointerController$LaserState ( ) {
/* .locals 2 */
/* .line 726 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 727 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 728 */
/* new-instance v1, Landroid/graphics/PointF; */
/* invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V */
this.mPosition = v1;
/* .line 729 */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* .line 730 */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z */
/* .line 731 */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* .line 732 */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
/* .line 733 */
return;
} // .end method
/* # virtual methods */
void copyFrom ( com.miui.server.input.stylus.laser.LaserPointerController$LaserState p0 ) {
/* .locals 3 */
/* .param p1, "laserState" # Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState; */
/* .line 736 */
/* iget v0, p1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 737 */
v0 = this.mPosition;
v1 = this.mPosition;
/* iget v1, v1, Landroid/graphics/PointF;->x:F */
v2 = this.mPosition;
/* iget v2, v2, Landroid/graphics/PointF;->y:F */
(( android.graphics.PointF ) v0 ).set ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V
/* .line 738 */
/* iget v0, p1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mCurrentMode:I */
/* .line 739 */
/* iget-boolean v0, p1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mKeepPath:Z */
/* .line 740 */
/* iget-boolean v0, p1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mVisible:Z */
/* .line 741 */
/* iget-boolean v0, p1, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
/* iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mPresentationVisible:Z */
/* .line 742 */
return;
} // .end method
void markAllDirty ( ) {
/* .locals 1 */
/* .line 749 */
/* iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* or-int/lit8 v0, v0, 0x1 */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 750 */
/* or-int/lit8 v0, v0, 0x2 */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 751 */
/* or-int/lit8 v0, v0, 0x4 */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 752 */
/* or-int/lit8 v0, v0, 0x8 */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 753 */
/* or-int/lit8 v0, v0, 0x10 */
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 754 */
return;
} // .end method
void resetDirty ( ) {
/* .locals 1 */
/* .line 745 */
int v0 = 0; // const/4 v0, 0x0
/* iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserPointerController$LaserState;->mDirty:I */
/* .line 746 */
return;
} // .end method
