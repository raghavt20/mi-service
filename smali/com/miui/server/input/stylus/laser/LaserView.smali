.class public Lcom/miui/server/input/stylus/laser/LaserView;
.super Landroid/view/View;
.source "LaserView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/input/stylus/laser/LaserView$Mode;
    }
.end annotation


# static fields
.field public static final MODE_LASER:I = 0x0

.field public static final MODE_PEN:I = 0x1


# instance fields
.field private final mInPaint:Landroid/graphics/Paint;

.field private mKeepPath:Z

.field private final mLaserCenterX:F

.field private final mLaserCenterY:F

.field private final mLaserInRadius:F

.field private final mLaserOutBlurRadius:F

.field private final mLaserOutFillRadius:F

.field private final mLaserPenHeight:F

.field private final mLaserPenImage:Landroid/graphics/drawable/Drawable;

.field private final mLaserPenWidth:F

.field private mMode:I

.field private final mOutBlurPaint:Landroid/graphics/Paint;

.field private final mOutFillPaint:Landroid/graphics/Paint;

.field private final mPathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private final mPathPaint:Landroid/graphics/Paint;

.field private mPresentationVisible:Z

.field private mVisible:Z

.field private mX:F

.field private mY:F


# direct methods
.method public static synthetic $r8$lambda$8x2GSzqO9Ak1Zm12o_sX4qM3KbQ(Lcom/miui/server/input/stylus/laser/LaserView;Landroid/graphics/Canvas;Landroid/graphics/Path;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/input/stylus/laser/LaserView;->lambda$onDraw$0(Landroid/graphics/Canvas;Landroid/graphics/Path;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/server/input/stylus/laser/LaserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/server/input/stylus/laser/LaserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I

    .line 62
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z

    .line 63
    iput-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z

    .line 77
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mInPaint:Landroid/graphics/Paint;

    .line 78
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    const-string v2, "#CCFF001F"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mOutBlurPaint:Landroid/graphics/Paint;

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 84
    const v3, 0x11070061

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 83
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    const-string v2, "#FF001F"

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    new-instance v4, Landroid/graphics/BlurMaskFilter;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 88
    const v6, 0x1107005e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    sget-object v6, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v4, v5, v6}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 87
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 91
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mOutFillPaint:Landroid/graphics/Paint;

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 93
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 92
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 94
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 95
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    .line 99
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathPaint:Landroid/graphics/Paint;

    .line 100
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 101
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 102
    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 103
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 105
    const v2, 0x11070065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 104
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 106
    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 107
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 110
    const v1, 0x1107005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserInRadius:F

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    const v1, 0x11070060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutFillRadius:F

    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 114
    const v1, 0x1107005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutBlurRadius:F

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 116
    const v1, 0x11070066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenWidth:F

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 118
    const v1, 0x11070064

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenHeight:F

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 120
    const v1, 0x11070062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterX:F

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 122
    const v1, 0x11070063

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterY:F

    .line 123
    const v0, 0x1108024f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenImage:Landroid/graphics/drawable/Drawable;

    .line 124
    return-void
.end method

.method private synthetic lambda$onDraw$0(Landroid/graphics/Canvas;Landroid/graphics/Path;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "path"    # Landroid/graphics/Path;

    .line 132
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method


# virtual methods
.method public clearPath()V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 247
    invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V

    .line 248
    return-void
.end method

.method public getMode()I
    .locals 1

    .line 216
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I

    return v0
.end method

.method public getPosition([F)V
    .locals 2
    .param p1, "outPosition"    # [F

    .line 196
    const/4 v0, 0x0

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    aput v1, p1, v0

    .line 197
    const/4 v0, 0x1

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    aput v1, p1, v0

    .line 198
    return-void
.end method

.method public isKeepPath()Z
    .locals 1

    .line 224
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z

    return v0
.end method

.method public move(FF)V
    .locals 2
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F

    .line 166
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    add-float/2addr v0, p1

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    add-float/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setPosition(FF)V

    .line 167
    return-void
.end method

.method public needExist()Z
    .locals 1

    .line 251
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z

    if-nez v0, :cond_0

    .line 253
    const/4 v0, 0x0

    return v0

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 257
    const/4 v0, 0x1

    return v0

    .line 260
    :cond_1
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 128
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z

    if-nez v0, :cond_0

    .line 129
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    new-instance v1, Lcom/miui/server/input/stylus/laser/LaserView$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/input/stylus/laser/LaserView$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/input/stylus/laser/LaserView;Landroid/graphics/Canvas;)V

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    .line 134
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z

    if-nez v0, :cond_1

    .line 135
    return-void

    .line 137
    :cond_1
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I

    if-nez v0, :cond_2

    .line 139
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutBlurRadius:F

    iget-object v3, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mOutBlurPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 141
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserOutFillRadius:F

    iget-object v3, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mOutFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 143
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserInRadius:F

    iget-object v3, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mInPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 144
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 145
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 147
    :cond_3
    :goto_0
    return-void
.end method

.method public setKeepPath(Z)V
    .locals 3
    .param p1, "keepPath"    # Z

    .line 231
    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z

    if-ne v0, p1, :cond_0

    .line 232
    return-void

    .line 234
    :cond_0
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z

    .line 236
    if-eqz p1, :cond_1

    .line 237
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 238
    .local v0, "path":Landroid/graphics/Path;
    iget-object v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 240
    .end local v0    # "path":Landroid/graphics/Path;
    :cond_1
    return-void
.end method

.method public setMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .line 207
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I

    if-ne v0, p1, :cond_0

    .line 208
    return-void

    .line 210
    :cond_0
    iput p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mMode:I

    .line 211
    invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V

    .line 212
    return-void
.end method

.method public setPosition(FF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 177
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mKeepPath:Z

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPathList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    .line 179
    .local v0, "path":Landroid/graphics/Path;
    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_0

    .line 182
    :cond_0
    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    add-float v3, p1, v1

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float v5, p2, v2

    div-float/2addr v5, v4

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 185
    .end local v0    # "path":Landroid/graphics/Path;
    :cond_1
    :goto_0
    iput p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mX:F

    .line 186
    iput p2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mY:F

    .line 187
    iget v0, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenWidth:F

    iget v1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterX:F

    mul-float/2addr v1, v0

    sub-float v1, p1, v1

    float-to-int v1, v1

    .line 188
    .local v1, "left":I
    iget v2, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenHeight:F

    iget v3, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserCenterY:F

    mul-float/2addr v3, v2

    sub-float v3, p2, v3

    float-to-int v3, v3

    .line 189
    .local v3, "top":I
    int-to-float v4, v1

    add-float/2addr v4, v0

    float-to-int v0, v4

    .line 190
    .local v0, "right":I
    int-to-float v4, v3

    add-float/2addr v4, v2

    float-to-int v2, v4

    .line 191
    .local v2, "bottom":I
    iget-object v4, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mLaserPenImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v1, v3, v0, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 192
    invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V

    .line 193
    return-void
.end method

.method public setPosition(Landroid/graphics/PointF;)V
    .locals 2
    .param p1, "position"    # Landroid/graphics/PointF;

    .line 170
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/input/stylus/laser/LaserView;->setPosition(FF)V

    .line 171
    return-void
.end method

.method public setPresentationVisible(Z)V
    .locals 0
    .param p1, "presentationVisible"    # Z

    .line 155
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mPresentationVisible:Z

    .line 156
    invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V

    .line 157
    return-void
.end method

.method public setVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .line 150
    iput-boolean p1, p0, Lcom/miui/server/input/stylus/laser/LaserView;->mVisible:Z

    .line 151
    invoke-virtual {p0}, Lcom/miui/server/input/stylus/laser/LaserView;->invalidate()V

    .line 152
    return-void
.end method
