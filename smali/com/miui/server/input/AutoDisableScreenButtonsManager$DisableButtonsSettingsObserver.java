class com.miui.server.input.AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver extends android.database.ContentObserver {
	 /* .source "AutoDisableScreenButtonsManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/input/AutoDisableScreenButtonsManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DisableButtonsSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.input.AutoDisableScreenButtonsManager this$0; //synthetic
/* # direct methods */
public com.miui.server.input.AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver ( ) {
/* .locals 0 */
/* .param p2, "handler" # Landroid/os/Handler; */
/* .line 339 */
this.this$0 = p1;
/* .line 340 */
/* invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 341 */
return;
} // .end method
/* # virtual methods */
void observe ( ) {
/* .locals 4 */
/* .line 349 */
v0 = this.this$0;
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$fgetmContext ( v0 );
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 350 */
/* .local v0, "resolver":Landroid/content/ContentResolver; */
final String v1 = "screen_buttons_state"; // const-string v1, "screen_buttons_state"
android.provider.Settings$Secure .getUriFor ( v1 );
int v2 = 0; // const/4 v2, 0x0
int v3 = -1; // const/4 v3, -0x1
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 352 */
final String v1 = "auto_disable_screen_button"; // const-string v1, "auto_disable_screen_button"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 354 */
final String v1 = "auto_disable_screen_button_cloud_setting"; // const-string v1, "auto_disable_screen_button_cloud_setting"
android.provider.Settings$System .getUriFor ( v1 );
(( android.content.ContentResolver ) v0 ).registerContentObserver ( v1, v2, p0, v3 ); // invoke-virtual {v0, v1, v2, p0, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V
/* .line 357 */
(( com.miui.server.input.AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver ) p0 ).onChange ( v2 ); // invoke-virtual {p0, v2}, Lcom/miui/server/input/AutoDisableScreenButtonsManager$DisableButtonsSettingsObserver;->onChange(Z)V
/* .line 358 */
return;
} // .end method
public void onChange ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "selfChange" # Z */
/* .line 345 */
v0 = this.this$0;
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$mupdateSettings ( v0 );
/* .line 346 */
return;
} // .end method
