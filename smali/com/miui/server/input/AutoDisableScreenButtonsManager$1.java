class com.miui.server.input.AutoDisableScreenButtonsManager$1 implements java.lang.Runnable {
	 /* .source "AutoDisableScreenButtonsManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/input/AutoDisableScreenButtonsManager;->onStatusBarVisibilityChange(Z)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.input.AutoDisableScreenButtonsManager this$0; //synthetic
final Boolean val$visible; //synthetic
/* # direct methods */
 com.miui.server.input.AutoDisableScreenButtonsManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/input/AutoDisableScreenButtonsManager; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()V" */
/* } */
} // .end annotation
/* .line 79 */
this.this$0 = p1;
/* iput-boolean p2, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->val$visible:Z */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 81 */
/* iget-boolean v0, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->val$visible:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 83 */
v0 = this.this$0;
v0 = com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$fgetmScreenButtonsTmpDisabled ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 84 */
	 v0 = this.this$0;
	 int v1 = 0; // const/4 v1, 0x0
	 com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$msaveTmpDisableButtonsStatus ( v0,v1 );
	 /* .line 86 */
} // :cond_0
v0 = this.this$0;
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$fgetmFloatView ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_3
	 /* .line 87 */
	 v0 = this.this$0;
	 com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$fgetmFloatView ( v0 );
	 (( miui.view.AutoDisableScreenbuttonsFloatView ) v0 ).dismiss ( ); // invoke-virtual {v0}, Lmiui/view/AutoDisableScreenbuttonsFloatView;->dismiss()V
	 /* .line 89 */
} // :cond_1
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$smgetRunningTopActivity ( );
/* .line 90 */
/* .local v0, "packageName":Ljava/lang/String; */
v1 = this.this$0;
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$fgetmContext ( v1 );
v1 = com.miui.server.input.util.AutoDisableScreenButtonsHelper .getAppFlag ( v1,v0 );
/* .line 91 */
/* .local v1, "flag":I */
int v2 = 2; // const/4 v2, 0x2
int v3 = 1; // const/4 v3, 0x1
/* if-ne v1, v2, :cond_2 */
/* .line 92 */
v2 = this.this$0;
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$msaveTmpDisableButtonsStatus ( v2,v3 );
/* .line 93 */
} // :cond_2
/* if-ne v1, v3, :cond_3 */
/* .line 94 */
v2 = this.this$0;
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$mshowFloat ( v2 );
/* .line 97 */
} // .end local v0 # "packageName":Ljava/lang/String;
} // .end local v1 # "flag":I
} // :cond_3
} // :goto_0
v0 = this.this$0;
/* iget-boolean v1, p0, Lcom/miui/server/input/AutoDisableScreenButtonsManager$1;->val$visible:Z */
com.miui.server.input.AutoDisableScreenButtonsManager .-$$Nest$fputmStatusBarVisibleOld ( v0,v1 );
/* .line 98 */
return;
} // .end method
