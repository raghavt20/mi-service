.class final Lcom/miui/server/SecurityManagerService$LocalService;
.super Lmiui/security/SecurityManagerInternal;
.source "SecurityManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/SecurityManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# static fields
.field public static final DISPLAY_DEVICE_EVENT_ADDED:I = 0x1

.field public static final DISPLAY_DEVICE_EVENT_CHANGED:I = 0x2

.field public static final DISPLAY_DEVICE_EVENT_REMOVED:I = 0x3


# instance fields
.field final synthetic this$0:Lcom/miui/server/SecurityManagerService;


# direct methods
.method private constructor <init>(Lcom/miui/server/SecurityManagerService;)V
    .locals 0

    .line 1430
    iput-object p1, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-direct {p0}, Lmiui/security/SecurityManagerInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/SecurityManagerService;Lcom/miui/server/SecurityManagerService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/SecurityManagerService$LocalService;-><init>(Lcom/miui/server/SecurityManagerService;)V

    return-void
.end method


# virtual methods
.method public checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 1493
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/server/SecurityManagerService;->checkAccessControlPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method

.method public checkAllowStartActivity(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;II)Z
    .locals 4
    .param p1, "callerPkgName"    # Ljava/lang/String;
    .param p2, "calleePkgName"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "callerUid"    # I
    .param p5, "calleeUid"    # I

    .line 1466
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmUserManager(Lcom/miui/server/SecurityManagerService;)Lcom/android/server/pm/UserManagerService;

    move-result-object v0

    invoke-static {p5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/pm/UserManagerService;->exists(I)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 1467
    return v1

    .line 1469
    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-virtual {v0, v1, p2, p3}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v0

    .line 1470
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v2, v2, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, p1, v3}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v2, v2, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    .line 1471
    invoke-static {v2, p2, v3}, Lmiui/content/pm/PreloadedAppPolicy;->isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 1474
    :cond_1
    if-nez v0, :cond_2

    .line 1475
    invoke-static {}, Lmiui/security/WakePathChecker;->getInstance()Lmiui/security/WakePathChecker;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p4, p5}, Lmiui/security/WakePathChecker;->checkAllowStartActivity(Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v0

    .line 1478
    :cond_2
    return v0

    .line 1472
    :cond_3
    :goto_0
    return v1
.end method

.method public checkGameBoosterAntiMsgPassAsUser(Ljava/lang/String;Landroid/content/Intent;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1483
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/miui/server/AccessController;->filterIntentLocked(ZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public checkGameBoosterPayPassAsUser(Ljava/lang/String;Landroid/content/Intent;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "userId"    # I

    .line 1450
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mGameBoosterImpl:Lcom/miui/server/security/GameBoosterImpl;

    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v1, v1, Lcom/miui/server/SecurityManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/miui/server/security/GameBoosterImpl;->isVtbMode(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p3}, Lcom/miui/server/SecurityManagerService;->getGameMode(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/miui/server/AccessController;->filterIntentLocked(ZZLjava/lang/String;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    return v1
.end method

.method public exemptByRestrictChain(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 1556
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/SecurityManagerService;->exemptByRestrictChain(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getAppDarkModeForUser(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .line 1536
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1537
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p2}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1539
    .local v1, "userState":Lcom/miui/server/security/SecurityUserState;
    :try_start_1
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 1540
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iget-boolean v3, v2, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    return v3

    .line 1541
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    :catch_0
    move-exception v2

    .line 1542
    .local v2, "e":Ljava/lang/Exception;
    monitor-exit v0

    const/4 v0, 0x0

    return v0

    .line 1544
    .end local v1    # "userState":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public getThreshold(I)J
    .locals 2
    .param p1, "behavior"    # I

    .line 1514
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppBehavior(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppBehaviorService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/security/AppBehaviorService;->getThreshold(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public isAccessControlActive(I)Z
    .locals 2
    .param p1, "userId"    # I

    .line 1498
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v0

    .line 1499
    .local v0, "userState":Lcom/miui/server/security/SecurityUserState;
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v1, v1, Lcom/miui/server/SecurityManagerService;->mAccessControlImpl:Lcom/miui/server/security/AccessControlImpl;

    invoke-virtual {v1, v0}, Lcom/miui/server/security/AccessControlImpl;->getAccessControlEnabledLocked(Lcom/miui/server/security/SecurityUserState;)Z

    move-result v1

    return v1
.end method

.method public isApplicationLockActivity(Ljava/lang/String;)Z
    .locals 1
    .param p1, "className"    # Ljava/lang/String;

    .line 1460
    const-string v0, "com.miui.securitycenter/com.miui.applicationlock.ConfirmAccessControl"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public isBlockActivity(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .line 1504
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppRunningControlService(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/AppRunningControlService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/AppRunningControlService;->isBlockActivity(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public isForceLaunchNewTask(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "rawPackageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .line 1455
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mAccessController:Lcom/miui/server/AccessController;

    invoke-virtual {v0}, Lcom/miui/server/AccessController;->getForceLaunchList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isGameBoosterActive(I)Z
    .locals 1
    .param p1, "userId"    # I

    .line 1488
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/SecurityManagerService;->getGameMode(I)Z

    move-result v0

    return v0
.end method

.method public onDisplayDeviceEvent(Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "token"    # Landroid/os/IBinder;
    .param p4, "event"    # I

    .line 1437
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmPrivacyVirtualDisplay(Lcom/miui/server/SecurityManagerService;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-ne v0, p4, :cond_0

    .line 1439
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v1}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmPrivacyDisplayNameList(Lcom/miui/server/SecurityManagerService;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1440
    invoke-static {p3, v0}, Landroid/view/SurfaceControl;->setMiSecurityDisplay(Landroid/os/IBinder;Z)V

    .line 1442
    :cond_0
    const/4 v0, 0x3

    if-ne v0, p4, :cond_1

    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmPrivacyDisplayNameList(Lcom/miui/server/SecurityManagerService;)Ljava/util/List;

    move-result-object v0

    .line 1443
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1444
    const/4 v0, 0x0

    invoke-static {p3, v0}, Landroid/view/SurfaceControl;->setMiSecurityDisplay(Landroid/os/IBinder;Z)V

    .line 1446
    :cond_1
    return-void
.end method

.method public recordAppBehaviorAsync(ILjava/lang/String;JLjava/lang/String;)V
    .locals 2
    .param p1, "behavior"    # I
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "count"    # J
    .param p5, "extra"    # Ljava/lang/String;

    .line 1549
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppBehavior(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppBehaviorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->hasAppBehaviorWatching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1550
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppBehavior(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppBehaviorService;

    move-result-object v0

    invoke-static {p1, p2, p3, p4, p5}, Lmiui/security/AppBehavior;->buildCountEvent(ILjava/lang/String;JLjava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    .line 1552
    :cond_0
    return-void
.end method

.method public recordAppChainAsync(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "behavior"    # I
    .param p2, "upPkg"    # Ljava/lang/String;
    .param p3, "downPkg"    # Ljava/lang/String;
    .param p4, "callData"    # Ljava/lang/String;

    .line 1519
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppBehavior(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppBehaviorService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/security/AppBehaviorService;->hasAppBehaviorWatching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1520
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppBehavior(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppBehaviorService;

    move-result-object v0

    invoke-static {p1, p2, p3, p4}, Lmiui/security/AppBehavior;->buildChainEvent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lmiui/security/AppBehavior;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/server/security/AppBehaviorService;->recordAppBehaviorAsync(Lmiui/security/AppBehavior;)V

    .line 1522
    :cond_0
    return-void
.end method

.method public recordDurationInfo(ILandroid/os/IBinder;ILjava/lang/String;Z)V
    .locals 7
    .param p1, "behavior"    # I
    .param p2, "binder"    # Landroid/os/IBinder;
    .param p3, "uid"    # I
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "start"    # Z

    .line 1509
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-static {v0}, Lcom/miui/server/SecurityManagerService;->-$$Nest$fgetmAppDuration(Lcom/miui/server/SecurityManagerService;)Lcom/miui/server/security/AppDurationService;

    move-result-object v1

    move v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/miui/server/security/AppDurationService;->onDurationEvent(ILandroid/os/IBinder;ILjava/lang/String;Z)V

    .line 1510
    return-void
.end method

.method public removeCalleeRestrictChain(Ljava/lang/String;)V
    .locals 1
    .param p1, "callee"    # Ljava/lang/String;

    .line 1561
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v0, p1}, Lcom/miui/server/SecurityManagerService;->removeCalleeRestrictChain(Ljava/lang/String;)V

    .line 1562
    return-void
.end method

.method public setAppDarkModeForUser(Ljava/lang/String;ZI)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "userId"    # I

    .line 1526
    iget-object v0, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v0, v0, Lcom/miui/server/SecurityManagerService;->mUserStateLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1527
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v1, p3}, Lcom/miui/server/SecurityManagerService;->getUserStateLocked(I)Lcom/miui/server/security/SecurityUserState;

    move-result-object v1

    .line 1528
    .local v1, "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    iget-object v2, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    iget-object v3, v1, Lcom/miui/server/security/SecurityUserState;->mPackages:Ljava/util/HashMap;

    invoke-virtual {v2, v3, p1}, Lcom/miui/server/SecurityManagerService;->getPackageSetting(Ljava/util/HashMap;Ljava/lang/String;)Lcom/miui/server/security/SecurityPackageSettings;

    move-result-object v2

    .line 1529
    .local v2, "ps":Lcom/miui/server/security/SecurityPackageSettings;
    iput-boolean p2, v2, Lcom/miui/server/security/SecurityPackageSettings;->isDarkModeChecked:Z

    .line 1530
    iget-object v3, p0, Lcom/miui/server/SecurityManagerService$LocalService;->this$0:Lcom/miui/server/SecurityManagerService;

    invoke-virtual {v3}, Lcom/miui/server/SecurityManagerService;->scheduleWriteSettings()V

    .line 1531
    .end local v1    # "userStateLocked":Lcom/miui/server/security/SecurityUserState;
    .end local v2    # "ps":Lcom/miui/server/security/SecurityPackageSettings;
    monitor-exit v0

    .line 1532
    return-void

    .line 1531
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
