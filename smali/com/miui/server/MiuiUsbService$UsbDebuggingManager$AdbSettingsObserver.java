class com.miui.server.MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver extends android.database.ContentObserver {
	 /* .source "MiuiUsbService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/MiuiUsbService$UsbDebuggingManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "AdbSettingsObserver" */
} // .end annotation
/* # instance fields */
final com.miui.server.MiuiUsbService$UsbDebuggingManager this$1; //synthetic
/* # direct methods */
public com.miui.server.MiuiUsbService$UsbDebuggingManager$AdbSettingsObserver ( ) {
/* .locals 0 */
/* .line 187 */
this.this$1 = p1;
/* .line 188 */
int p1 = 0; // const/4 p1, 0x0
/* invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V */
/* .line 189 */
return;
} // .end method
/* # virtual methods */
public void onChange ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "selfChange" # Z */
/* .line 193 */
v0 = this.this$1;
com.miui.server.MiuiUsbService$UsbDebuggingManager .-$$Nest$fgetmContentResolver ( v0 );
final String v1 = "adb_enabled"; // const-string v1, "adb_enabled"
int v2 = 0; // const/4 v2, 0x0
v0 = android.provider.Settings$Secure .getInt ( v0,v1,v2 );
/* if-lez v0, :cond_0 */
int v2 = 1; // const/4 v2, 0x1
} // :cond_0
/* move v0, v2 */
/* .line 195 */
/* .local v0, "enable":Z */
v1 = this.this$1;
(( com.miui.server.MiuiUsbService$UsbDebuggingManager ) v1 ).setAdbEnabled ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/MiuiUsbService$UsbDebuggingManager;->setAdbEnabled(Z)V
/* .line 196 */
return;
} // .end method
