.class public final Lcom/miui/server/PerfShielderService;
.super Lcom/android/internal/app/IPerfShielder$Stub;
.source "PerfShielderService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;,
        Lcom/miui/server/PerfShielderService$BindServiceHandler;,
        Lcom/miui/server/PerfShielderService$PidSwapGetter;,
        Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;,
        Lcom/miui/server/PerfShielderService$Lifecycle;
    }
.end annotation


# static fields
.field private static final ACTIVITY_BATCH_MAX_INTERVAL:J = 0xea60L

.field private static final ACTIVITY_BATCH_MAX_SIZE:I = 0xa

.field private static final BIND_FAIL_RETRY_TIME:J = 0xea60L

.field private static final BIND_RETRY_TIME_BASE:J = 0xea60L

.field private static final BIND_RETRY_TIME_MAX:J = 0x36ee80L

.field private static final BIND_SYSOPT_SERVICE_FIRST:J = 0x5dcL

.field private static final DEBUG:Z = true

.field private static final DELAY_TIME:J = 0x493e0L

.field private static final LAUNCH_TYPE_DEFAULT:I = 0x0

.field private static final LAUNCH_TYPE_FROM_HOME:I = 0x1

.field private static final MIUI_SYS_USER_CLASS:Ljava/lang/String; = "com.miui.daemon.performance.SysoptService"

.field private static final MIUI_SYS_USER_PACKAHE:Ljava/lang/String; = "com.miui.daemon"

.field static final MSG_BIND_MIUI_SYS_USER:I = 0x2

.field static final MSG_REBIND:I = 0x1

.field private static final NATIVE_ADJ:I

.field private static final PERFORMANCE_CLASS:Ljava/lang/String; = "com.miui.daemon.performance.MiuiPerfService"

.field private static final PERFORMANCE_PACKAGE:Ljava/lang/String; = "com.miui.daemon"

.field private static final SELF_CAUSE_ANR:I = 0x7

.field private static final SELF_CAUSE_NAMES:[Ljava/lang/String;

.field public static final SERVICE_NAME:Ljava/lang/String; = "perfshielder"

.field private static final SYSTEM_SERVER:Ljava/lang/String; = "system_server"

.field public static final TAG:Ljava/lang/String; = "PerfShielderService"

.field private static WINDOW_NAME_REX:Ljava/util/regex/Pattern;

.field private static WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mLastRetryTime:J


# instance fields
.field private mContext:Landroid/content/Context;

.field mDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

.field private mLaunchTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/internal/app/LaunchTimeRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mMiuiSysUserConnection:Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;

.field mMiuiSysUserDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private final mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Landroid/os/ParcelFileDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private final mPerfEventSocketFdLock:Ljava/lang/Object;

.field protected mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

.field private final mPerformanceConnection:Landroid/content/ServiceConnection;

.field private mReflectGetPssMethod:Ljava/lang/reflect/Method;

.field private mWMServiceConnection:Lcom/miui/server/WMServiceConnection;


# direct methods
.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/PerfShielderService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/PerfShielderService;)Lcom/miui/server/PerfShielderService$BindServiceHandler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmMiuiSysUserConnection(Lcom/miui/server/PerfShielderService;)Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/PerfShielderService;->mMiuiSysUserConnection:Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmPerformanceConnection(Lcom/miui/server/PerfShielderService;)Landroid/content/ServiceConnection;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/PerfShielderService;->mPerformanceConnection:Landroid/content/ServiceConnection;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mbindMiuiSysUser(Lcom/miui/server/PerfShielderService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/PerfShielderService;->bindMiuiSysUser()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mbindService(Lcom/miui/server/PerfShielderService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/PerfShielderService;->bindService()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mgetProcessStatusValues(Lcom/miui/server/PerfShielderService;I)[J
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/PerfShielderService;->getProcessStatusValues(I)[J

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$msendBindMiuiSysUserMsg(Lcom/miui/server/PerfShielderService;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/PerfShielderService;->sendBindMiuiSysUserMsg(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$msendRebindServiceMsg(Lcom/miui/server/PerfShielderService;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/PerfShielderService;->sendRebindServiceMsg(J)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmLastRetryTime()J
    .locals 2

    sget-wide v0, Lcom/miui/server/PerfShielderService;->mLastRetryTime:J

    return-wide v0
.end method

.method static constructor <clinit>()V
    .locals 9

    .line 94
    const-wide/32 v0, 0xea60

    sput-wide v0, Lcom/miui/server/PerfShielderService;->mLastRetryTime:J

    .line 100
    const-string v0, "(\\w+\\.)+(\\w+)\\/\\.?(\\w+\\.)*(\\w+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_REX:Ljava/util/regex/Pattern;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    .line 103
    const-string v1, "Keyguard"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "StatusBar"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "RecentsPanel"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "InputMethod"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "Volume Control"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "GestureStubBottom"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "GestureStub"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "GestureAnywhereView"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    sget-object v0, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    const-string v1, "NavigationBar"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    nop

    .line 118
    const/16 v0, -0x3e8

    sput v0, Lcom/miui/server/PerfShielderService;->NATIVE_ADJ:I

    .line 144
    const-string v1, "Slow main thread"

    const-string v2, "Slow handle input"

    const-string v3, "Slow handle animation"

    const-string v4, "Slow handle traversal"

    const-string v5, "Slow bitmap uploads"

    const-string v6, "Slow issue draw commands"

    const-string v7, "Slow swap buffers"

    const-string v8, "ANR"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/PerfShielderService;->SELF_CAUSE_NAMES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 182
    invoke-direct {p0}, Lcom/android/internal/app/IPerfShielder$Stub;-><init>()V

    .line 123
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFdLock:Ljava/lang/Object;

    .line 124
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    .line 140
    new-instance v0, Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;-><init>(Lcom/miui/server/PerfShielderService;Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection-IA;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mMiuiSysUserConnection:Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;

    .line 599
    new-instance v0, Lcom/miui/server/PerfShielderService$2;

    invoke-direct {v0, p0}, Lcom/miui/server/PerfShielderService$2;-><init>(Lcom/miui/server/PerfShielderService;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerformanceConnection:Landroid/content/ServiceConnection;

    .line 651
    new-instance v0, Lcom/miui/server/PerfShielderService$3;

    invoke-direct {v0, p0}, Lcom/miui/server/PerfShielderService$3;-><init>(Lcom/miui/server/PerfShielderService;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mMiuiSysUserDeathHandler:Landroid/os/IBinder$DeathRecipient;

    .line 683
    new-instance v0, Lcom/miui/server/PerfShielderService$4;

    invoke-direct {v0, p0}, Lcom/miui/server/PerfShielderService$4;-><init>(Lcom/miui/server/PerfShielderService;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    .line 183
    iput-object p1, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    .line 184
    new-instance v0, Lcom/miui/server/PerfShielderService$BindServiceHandler;

    invoke-static {}, Lcom/android/server/MiuiBgThread;->get()Lcom/android/server/MiuiBgThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/MiuiBgThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/PerfShielderService$BindServiceHandler;-><init>(Lcom/miui/server/PerfShielderService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    .line 185
    invoke-direct {p0}, Lcom/miui/server/PerfShielderService;->reflectDebugGetPssMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mReflectGetPssMethod:Ljava/lang/reflect/Method;

    .line 186
    new-instance v0, Lcom/miui/server/WMServiceConnection;

    invoke-direct {v0, p1}, Lcom/miui/server/WMServiceConnection;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/PerfShielderService;->mWMServiceConnection:Lcom/miui/server/WMServiceConnection;

    .line 191
    const-class v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    .line 192
    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/app/SpeedTestModeServiceInternal;

    .line 193
    .local v0, "speedTestModeService":Lcom/miui/app/SpeedTestModeServiceInternal;
    if-eqz v0, :cond_0

    .line 194
    invoke-interface {v0, p1}, Lcom/miui/app/SpeedTestModeServiceInternal;->init(Landroid/content/Context;)V

    .line 196
    :cond_0
    return-void
.end method

.method private bindMiuiSysUser()V
    .locals 9

    .line 667
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mMiuiSysUserConnection:Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;

    invoke-static {v0}, Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;->-$$Nest$fgetisServiceDisconnected(Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 668
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 669
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.daemon"

    const-string v2, "com.miui.daemon.performance.SysoptService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 670
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mMiuiSysUserConnection:Lcom/miui/server/PerfShielderService$MiuiSysUserServiceConnection;

    sget-object v3, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v4, v3}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v1

    const-string v2, "PerfShielderService"

    if-nez v1, :cond_1

    .line 672
    sget-wide v5, Lcom/miui/server/PerfShielderService;->mLastRetryTime:J

    invoke-direct {p0, v5, v6}, Lcom/miui/server/PerfShielderService;->sendBindMiuiSysUserMsg(J)V

    .line 673
    sget-wide v5, Lcom/miui/server/PerfShielderService;->mLastRetryTime:J

    const-wide/32 v7, 0x36ee80

    cmp-long v1, v5, v7

    if-ltz v1, :cond_0

    .line 674
    goto :goto_0

    :cond_0
    shl-long v7, v5, v4

    :goto_0
    sput-wide v7, Lcom/miui/server/PerfShielderService;->mLastRetryTime:J

    .line 675
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MiuiSysUser: can\'t bind to com.miui.daemon.performance.SysoptService, retry time == "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-wide v3, Lcom/miui/server/PerfShielderService;->mLastRetryTime:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 678
    :cond_1
    const-string v1, "MiuiSysUser service started"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    :goto_1
    return-void
.end method

.method private bindService()V
    .locals 5

    .line 586
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-nez v0, :cond_1

    .line 587
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 588
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.miui.daemon"

    const-string v2, "com.miui.daemon.performance.MiuiPerfService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerformanceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    sget-object v4, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    move-result v1

    const-string v2, "PerfShielderService"

    if-nez v1, :cond_0

    .line 591
    const-string v1, "Miui performance: can\'t bind to com.miui.daemon.performance.MiuiPerfService"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    const-wide/32 v1, 0xea60

    invoke-direct {p0, v1, v2}, Lcom/miui/server/PerfShielderService;->sendRebindServiceMsg(J)V

    goto :goto_0

    .line 594
    :cond_0
    const-string v1, "Miui performance service started"

    invoke-static {v2, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void
.end method

.method private dumpFromFile(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;
    .param p2, "path"    # Ljava/lang/String;

    .line 525
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 526
    .local v0, "file":Ljava/io/File;
    const/4 v1, 0x0

    .line 528
    .local v1, "reader":Ljava/io/BufferedReader;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 529
    return-void

    .line 533
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v1, v2

    .line 535
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .local v3, "line":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 536
    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 542
    .end local v3    # "line":Ljava/lang/String;
    :cond_1
    nop

    .line 543
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 547
    :cond_2
    :goto_1
    goto :goto_2

    .line 545
    :catch_0
    move-exception v2

    .line 546
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 548
    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 541
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 538
    :catch_1
    move-exception v2

    .line 539
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2, p1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 542
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_2

    .line 543
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 549
    :goto_2
    return-void

    .line 542
    :goto_3
    if-eqz v1, :cond_3

    .line 543
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    .line 545
    :catch_2
    move-exception v3

    .line 546
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 547
    .end local v3    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    nop

    .line 548
    :goto_5
    throw v2
.end method

.method private getProcessPss(I)J
    .locals 6
    .param p1, "pid"    # I

    .line 345
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mReflectGetPssMethod:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 346
    const-wide/16 v0, 0x0

    return-wide v0

    .line 348
    :cond_0
    const-wide/16 v0, 0x0

    .line 350
    .local v0, "pss":J
    nop

    .line 351
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mReflectGetPssMethod:Ljava/lang/reflect/Method;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v5, v3, v4

    invoke-virtual {v2, v5, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-wide v0, v2

    .line 358
    :goto_0
    goto :goto_1

    .line 357
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 355
    :catch_0
    move-exception v2

    .line 356
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .end local v2    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 359
    :goto_1
    return-wide v0
.end method

.method private getProcessStatusValues(I)[J
    .locals 5
    .param p1, "pid"    # I

    .line 363
    const-string v0, "PPid:"

    const-string v1, "VmRSS:"

    const-string v2, "VmSwap:"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    .line 364
    .local v0, "procStatusLabels":[Ljava/lang/String;
    const/4 v1, 0x3

    new-array v1, v1, [J

    .line 365
    .local v1, "procStatusValues":[J
    const/4 v2, 0x0

    const-wide/16 v3, -0x1

    aput-wide v3, v1, v2

    .line 366
    const/4 v2, 0x1

    aput-wide v3, v1, v2

    .line 367
    const/4 v2, 0x2

    aput-wide v3, v1, v2

    .line 368
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/proc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/status"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/os/Process;->readProcLines(Ljava/lang/String;[Ljava/lang/String;[J)V

    .line 369
    return-object v1
.end method

.method private needToLimit(ILjava/lang/String;)Z
    .locals 6
    .param p1, "pid"    # I
    .param p2, "processName"    # Ljava/lang/String;

    .line 210
    const/4 v0, 0x0

    .line 211
    .local v0, "limit":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/proc/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/cmdline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 212
    .local v1, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 214
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    .line 215
    const/4 v3, 0x0

    .line 216
    .local v3, "line":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_0

    .line 217
    invoke-virtual {v3, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_0

    .line 218
    const/4 v0, 0x1

    .line 225
    .end local v3    # "line":Ljava/lang/String;
    :cond_0
    nop

    .line 226
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 230
    :cond_1
    :goto_0
    goto :goto_1

    .line 228
    :catch_0
    move-exception v3

    .line 229
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 231
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 224
    :catchall_0
    move-exception v3

    goto :goto_2

    .line 221
    :catch_1
    move-exception v3

    .line 222
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 225
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_1

    .line 226
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 232
    :goto_1
    return v0

    .line 225
    :goto_2
    if-eqz v2, :cond_2

    .line 226
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    .line 228
    :catch_2
    move-exception v4

    .line 229
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 230
    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    :goto_3
    nop

    .line 231
    :goto_4
    throw v3
.end method

.method private obtainPerfEventSocketFd()V
    .locals 4

    .line 787
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 788
    .local v0, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 789
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFdLock:Ljava/lang/Object;

    monitor-enter v1

    .line 790
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 792
    :try_start_1
    invoke-interface {v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->getPerfEventSocketFd()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 793
    .local v2, "fd":Landroid/os/ParcelFileDescriptor;
    iget-object v3, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796
    .end local v2    # "fd":Landroid/os/ParcelFileDescriptor;
    goto :goto_0

    .line 794
    :catch_0
    move-exception v2

    .line 795
    .local v2, "ex":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 798
    .end local v2    # "ex":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 800
    :cond_1
    :goto_1
    return-void
.end method

.method private reflectDebugGetPssMethod()Ljava/lang/reflect/Method;
    .locals 7

    .line 330
    const-class v0, [J

    const/4 v1, 0x0

    .line 332
    .local v1, "getPss":Ljava/lang/reflect/Method;
    nop

    .line 333
    :try_start_0
    const-class v2, Landroid/os/Debug;

    const-string v3, "getPss"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v5, 0x2

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v0

    .line 340
    :goto_0
    goto :goto_1

    .line 339
    :catchall_0
    move-exception v0

    goto :goto_1

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .end local v0    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 341
    :goto_1
    return-object v1
.end method

.method private reportActivityLaunchRecords()V
    .locals 8

    .line 450
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 451
    new-instance v0, Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;-><init>(Lcom/miui/server/PerfShielderService;Lcom/miui/server/PerfShielderService$PackageVersionNameGetter-IA;)V

    .line 452
    .local v0, "versionGetter":Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 453
    .local v1, "bundles":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 454
    iget-object v3, p0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/app/LaunchTimeRecord;

    .line 455
    .local v3, "record":Lcom/android/internal/app/LaunchTimeRecord;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 456
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v5, "PackageName"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-string v5, "PackageVersion"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const-string v5, "Activity"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getActivity()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v5, "LaunchStartTime"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getLaunchStartTime()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 460
    const-string v5, "LaunchEndTime"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getLaunchEndTime()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 461
    const-string v5, "Type"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getType()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 462
    const-string v5, "IsColdStart"

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->isColdStart()Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 463
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453
    nop

    .end local v3    # "record":Lcom/android/internal/app/LaunchTimeRecord;
    .end local v4    # "bundle":Landroid/os/Bundle;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 465
    .end local v2    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    invoke-interface {v2, v1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportActivityLaunchRecords(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    .end local v0    # "versionGetter":Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;
    .end local v1    # "bundles":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :cond_1
    goto :goto_1

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 470
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-void
.end method

.method private sendBindMiuiSysUserMsg(J)V
    .locals 2
    .param p1, "delayedTime"    # J

    .line 662
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 663
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    invoke-virtual {v1, v0, p1, p2}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 664
    return-void
.end method

.method private sendRebindServiceMsg(J)V
    .locals 2
    .param p1, "delayedTime"    # J

    .line 580
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->removeMessages(I)V

    .line 581
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    invoke-virtual {v0, v1}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 582
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    invoke-virtual {v1, v0, p1, p2}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 583
    return-void
.end method


# virtual methods
.method public abortMatchingScenario(Landroid/os/statistics/E2EScenario;Ljava/lang/String;IJ)V
    .locals 15
    .param p1, "scenario"    # Landroid/os/statistics/E2EScenario;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "tid"    # I
    .param p4, "uptimeMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 864
    move-object v0, p0

    iget-object v1, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 865
    .local v1, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    if-nez v1, :cond_0

    .line 866
    return-void

    .line 868
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-nez v2, :cond_1

    .line 869
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .end local p4    # "uptimeMillis":J
    .local v2, "uptimeMillis":J
    goto :goto_0

    .line 868
    .end local v2    # "uptimeMillis":J
    .restart local p4    # "uptimeMillis":J
    :cond_1
    move-wide/from16 v2, p4

    .line 871
    .end local p4    # "uptimeMillis":J
    .restart local v2    # "uptimeMillis":J
    :goto_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v13

    .line 872
    .local v13, "pid":I
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    if-ne v13, v4, :cond_2

    .line 873
    const-string/jumbo v4, "system_server"

    move-object v11, v4

    goto :goto_1

    .line 874
    :cond_2
    invoke-static {v13}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v4

    move-object v11, v4

    :goto_1
    nop

    .line 875
    .local v11, "processName":Ljava/lang/String;
    invoke-static {v13}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v14

    .line 876
    .local v14, "packageName":Ljava/lang/String;
    iget-object v4, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-wide v7, v2

    move v9, v13

    move/from16 v10, p3

    move-object v12, v14

    invoke-interface/range {v4 .. v12}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->abortMatchingScenario(Landroid/os/statistics/E2EScenario;Ljava/lang/String;JIILjava/lang/String;Ljava/lang/String;)V

    .line 877
    return-void
.end method

.method public abortSpecificScenario(Landroid/os/Bundle;IJ)V
    .locals 11
    .param p1, "scenarioBundle"    # Landroid/os/Bundle;
    .param p2, "tid"    # I
    .param p3, "uptimeMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 882
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 883
    .local v0, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    if-nez v0, :cond_0

    .line 884
    return-void

    .line 886
    :cond_0
    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_1

    .line 887
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p3

    .line 889
    :cond_1
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v9

    .line 890
    .local v9, "pid":I
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    if-ne v9, v1, :cond_2

    .line 891
    const-string/jumbo v1, "system_server"

    move-object v7, v1

    goto :goto_0

    .line 892
    :cond_2
    invoke-static {v9}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v1

    move-object v7, v1

    :goto_0
    nop

    .line 893
    .local v7, "processName":Ljava/lang/String;
    invoke-static {v9}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v10

    .line 894
    .local v10, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    move-object v2, p1

    move-wide v3, p3

    move v5, v9

    move v6, p2

    move-object v8, v10

    invoke-interface/range {v1 .. v8}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->abortSpecificScenario(Landroid/os/Bundle;JIILjava/lang/String;Ljava/lang/String;)V

    .line 895
    return-void
.end method

.method public addActivityLaunchTime(Ljava/lang/String;Ljava/lang/String;JJZZ)V
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "activityName"    # Ljava/lang/String;
    .param p3, "launchStartTime"    # J
    .param p5, "launchEndTime"    # J
    .param p7, "fromHome"    # Z
    .param p8, "isColdStart"    # Z

    .line 431
    move-object v0, p0

    move/from16 v1, p7

    if-nez p1, :cond_0

    .line 432
    return-void

    .line 434
    :cond_0
    new-instance v10, Lcom/android/internal/app/LaunchTimeRecord;

    move-object v2, v10

    move-object v3, p1

    move-object v4, p2

    move-wide v5, p3

    move-wide/from16 v7, p5

    move/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcom/android/internal/app/LaunchTimeRecord;-><init>(Ljava/lang/String;Ljava/lang/String;JJZ)V

    .line 435
    .local v2, "record":Lcom/android/internal/app/LaunchTimeRecord;
    invoke-virtual {v2, v1}, Lcom/android/internal/app/LaunchTimeRecord;->setType(I)V

    .line 436
    iget-object v3, v0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 437
    iget-object v3, v0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/internal/app/LaunchTimeRecord;

    invoke-virtual {v3}, Lcom/android/internal/app/LaunchTimeRecord;->getLaunchStartTime()J

    move-result-wide v3

    .line 438
    .local v3, "batchStartTime":J
    iget-object v5, v0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/internal/app/LaunchTimeRecord;

    invoke-virtual {v5}, Lcom/android/internal/app/LaunchTimeRecord;->getLaunchEndTime()J

    move-result-wide v5

    .line 439
    .local v5, "batchEndTime":J
    if-nez v1, :cond_1

    iget-object v7, v0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    .line 440
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/16 v8, 0xa

    if-ge v7, v8, :cond_1

    cmp-long v7, v5, v3

    if-ltz v7, :cond_1

    sub-long v7, v5, v3

    const-wide/32 v9, 0xea60

    cmp-long v7, v7, v9

    if-ltz v7, :cond_2

    .line 443
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/PerfShielderService;->reportActivityLaunchRecords()V

    .line 444
    iget-object v7, v0, Lcom/miui/server/PerfShielderService;->mLaunchTimes:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 446
    :cond_2
    return-void
.end method

.method public addCallingPkgHookRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "hostApp"    # Ljava/lang/String;
    .param p2, "originCallingPkg"    # Ljava/lang/String;
    .param p3, "hookCallingPkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 824
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 825
    const-string v0, "CallingPkgHook"

    const-string v1, "Check permission failed when addCallingPkgHookRule."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    const/4 v0, 0x0

    return v0

    .line 829
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/CallingPkgHook;->getInstance()Lcom/miui/hybrid/hook/CallingPkgHook;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/miui/hybrid/hook/CallingPkgHook;->add(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public beginScenario(Landroid/os/statistics/E2EScenario;Landroid/os/statistics/E2EScenarioSettings;Ljava/lang/String;Landroid/os/statistics/E2EScenarioPayload;IJZ)Landroid/os/Bundle;
    .locals 18
    .param p1, "scenario"    # Landroid/os/statistics/E2EScenario;
    .param p2, "settings"    # Landroid/os/statistics/E2EScenarioSettings;
    .param p3, "tag"    # Ljava/lang/String;
    .param p4, "payload"    # Landroid/os/statistics/E2EScenarioPayload;
    .param p5, "tid"    # I
    .param p6, "uptimeMillis"    # J
    .param p8, "needResultBundle"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 846
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 847
    .local v1, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    if-nez v1, :cond_0

    .line 848
    const/4 v2, 0x0

    return-object v2

    .line 850
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-nez v2, :cond_1

    .line 851
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .end local p6    # "uptimeMillis":J
    .local v2, "uptimeMillis":J
    goto :goto_0

    .line 850
    .end local v2    # "uptimeMillis":J
    .restart local p6    # "uptimeMillis":J
    :cond_1
    move-wide/from16 v2, p6

    .line 853
    .end local p6    # "uptimeMillis":J
    .restart local v2    # "uptimeMillis":J
    :goto_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v15

    .line 854
    .local v15, "pid":I
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    if-ne v15, v4, :cond_2

    .line 855
    const-string/jumbo v4, "system_server"

    move-object v13, v4

    goto :goto_1

    .line 856
    :cond_2
    invoke-static {v15}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v4

    move-object v13, v4

    :goto_1
    nop

    .line 857
    .local v13, "processName":Ljava/lang/String;
    invoke-static {v15}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v16

    .line 858
    .local v16, "packageName":Ljava/lang/String;
    iget-object v4, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-wide v9, v2

    move v11, v15

    move/from16 v12, p5

    move-object/from16 v14, v16

    move/from16 v17, v15

    .end local v15    # "pid":I
    .local v17, "pid":I
    move/from16 v15, p8

    invoke-interface/range {v4 .. v15}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->beginScenario(Landroid/os/statistics/E2EScenario;Landroid/os/statistics/E2EScenarioSettings;Ljava/lang/String;Landroid/os/statistics/E2EScenarioPayload;JIILjava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v4

    return-object v4
.end method

.method public deleteFilterInfo(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .line 961
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 962
    const-string v0, "CallingPkgHook"

    const-string v1, "Check permission failed when deleteFilterInfo."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    const/4 v0, 0x0

    return v0

    .line 966
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/FilterInfoInjector;->getInstance()Lcom/miui/hybrid/hook/FilterInfoInjector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/hybrid/hook/FilterInfoInjector;->deleteFilterInfo(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deletePackageInfo(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 702
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 703
    const-string v0, "PkgInfoHook"

    const-string v2, "Check permission failed when delete PackageInfo."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    return v1

    .line 706
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/PkgInfoHook;->getInstance()Lcom/miui/hybrid/hook/PkgInfoHook;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/hybrid/hook/PkgInfoHook;->delete(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public deleteRedirectRule(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "destPkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 723
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 724
    const-string v0, "IntentHook"

    const-string v2, "Check permission failed when delete RedirectRule."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    return v1

    .line 728
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/IntentHook;->getInstance()Lcom/miui/hybrid/hook/IntentHook;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/hybrid/hook/IntentHook;->delete(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 552
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.DUMP"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 555
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Permission Denial: can\'t dump perfshielder from pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 556
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 557
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " due to missing android.permission.DUMP permission"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 559
    .local v0, "errMsg":Ljava/lang/String;
    const-string v1, "PerfShielderService"

    invoke-static {v1, v0}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 562
    .end local v0    # "errMsg":Ljava/lang/String;
    return-void

    .line 565
    :cond_0
    const-string v0, "---- ION Memory Usage ----"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 566
    const-string v0, "/d/ion/heaps/system"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/PerfShielderService;->dumpFromFile(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 567
    const-string v0, "/d/ion/ion_mm_heap"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/PerfShielderService;->dumpFromFile(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 568
    const-string v0, "---- End of ION Memory Usage ----\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 570
    const-string v0, "---- minfree & adj ----"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 571
    const-string v0, "minfree: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 572
    const-string v0, "/sys/module/lowmemorykiller/parameters/minfree"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/PerfShielderService;->dumpFromFile(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 573
    const-string v0, "    adj: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 574
    const-string v0, "/sys/module/lowmemorykiller/parameters/adj"

    invoke-direct {p0, p2, v0}, Lcom/miui/server/PerfShielderService;->dumpFromFile(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 575
    const-string v0, "---- End of minfree & adj ----\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 576
    return-void
.end method

.method public finishMatchingScenario(Landroid/os/statistics/E2EScenario;Ljava/lang/String;Landroid/os/statistics/E2EScenarioPayload;IJ)V
    .locals 16
    .param p1, "scenario"    # Landroid/os/statistics/E2EScenario;
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "payload"    # Landroid/os/statistics/E2EScenarioPayload;
    .param p4, "tid"    # I
    .param p5, "uptimeMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 900
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 901
    .local v1, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    if-nez v1, :cond_0

    .line 902
    return-void

    .line 904
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-nez v2, :cond_1

    .line 905
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .end local p5    # "uptimeMillis":J
    .local v2, "uptimeMillis":J
    goto :goto_0

    .line 904
    .end local v2    # "uptimeMillis":J
    .restart local p5    # "uptimeMillis":J
    :cond_1
    move-wide/from16 v2, p5

    .line 907
    .end local p5    # "uptimeMillis":J
    .restart local v2    # "uptimeMillis":J
    :goto_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v14

    .line 908
    .local v14, "pid":I
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    if-ne v14, v4, :cond_2

    .line 909
    const-string/jumbo v4, "system_server"

    move-object v12, v4

    goto :goto_1

    .line 910
    :cond_2
    invoke-static {v14}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v4

    move-object v12, v4

    :goto_1
    nop

    .line 911
    .local v12, "processName":Ljava/lang/String;
    invoke-static {v14}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v15

    .line 912
    .local v15, "packageName":Ljava/lang/String;
    iget-object v4, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-wide v8, v2

    move v10, v14

    move/from16 v11, p4

    move-object v13, v15

    invoke-interface/range {v4 .. v13}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->finishMatchingScenario(Landroid/os/statistics/E2EScenario;Ljava/lang/String;Landroid/os/statistics/E2EScenarioPayload;JIILjava/lang/String;Ljava/lang/String;)V

    .line 913
    return-void
.end method

.method public finishSpecificScenario(Landroid/os/Bundle;Landroid/os/statistics/E2EScenarioPayload;IJ)V
    .locals 15
    .param p1, "scenarioBundle"    # Landroid/os/Bundle;
    .param p2, "payload"    # Landroid/os/statistics/E2EScenarioPayload;
    .param p3, "tid"    # I
    .param p4, "uptimeMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 918
    move-object v0, p0

    iget-object v1, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 919
    .local v1, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    if-nez v1, :cond_0

    .line 920
    return-void

    .line 922
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-nez v2, :cond_1

    .line 923
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .end local p4    # "uptimeMillis":J
    .local v2, "uptimeMillis":J
    goto :goto_0

    .line 922
    .end local v2    # "uptimeMillis":J
    .restart local p4    # "uptimeMillis":J
    :cond_1
    move-wide/from16 v2, p4

    .line 925
    .end local p4    # "uptimeMillis":J
    .restart local v2    # "uptimeMillis":J
    :goto_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v13

    .line 926
    .local v13, "pid":I
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    if-ne v13, v4, :cond_2

    .line 927
    const-string/jumbo v4, "system_server"

    move-object v11, v4

    goto :goto_1

    .line 928
    :cond_2
    invoke-static {v13}, Lcom/android/server/am/ProcessUtils;->getProcessNameByPid(I)Ljava/lang/String;

    move-result-object v4

    move-object v11, v4

    :goto_1
    nop

    .line 929
    .local v11, "processName":Ljava/lang/String;
    invoke-static {v13}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v14

    .line 930
    .local v14, "packageName":Ljava/lang/String;
    iget-object v4, v0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-wide v7, v2

    move v9, v13

    move/from16 v10, p3

    move-object v12, v14

    invoke-interface/range {v4 .. v12}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->finishSpecificScenario(Landroid/os/Bundle;Landroid/os/statistics/E2EScenarioPayload;JIILjava/lang/String;Ljava/lang/String;)V

    .line 931
    return-void
.end method

.method public getFreeMemory()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 733
    invoke-static {}, Lcom/android/server/am/MiuiMemoryInfoStub;->getInstance()Lcom/android/server/am/MiuiMemoryInfoStub;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/server/am/MiuiMemoryInfoStub;->getFreeMemory()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMemoryTrimLevel()I
    .locals 1

    .line 273
    invoke-static {}, Lcom/android/server/am/ProcessUtils;->getMemoryTrimLevel()I

    move-result v0

    return v0
.end method

.method public getPerfEventSocketFd()Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 742
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 743
    .local v0, "fd":Landroid/os/ParcelFileDescriptor;
    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 744
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 745
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->valid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 746
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 747
    :cond_0
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 749
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/PerfShielderService;->obtainPerfEventSocketFd()V

    .line 752
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 753
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->valid()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_2

    .line 759
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->dup()Landroid/os/ParcelFileDescriptor;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 760
    .local v1, "result":Landroid/os/ParcelFileDescriptor;
    return-object v1

    .line 761
    .end local v1    # "result":Landroid/os/ParcelFileDescriptor;
    :catch_0
    move-exception v2

    .line 766
    :try_start_1
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 768
    goto :goto_0

    .line 767
    :catch_1
    move-exception v2

    .line 769
    :goto_0
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 770
    invoke-direct {p0}, Lcom/miui/server/PerfShielderService;->obtainPerfEventSocketFd()V

    .line 774
    iget-object v2, p0, Lcom/miui/server/PerfShielderService;->mPerfEventSocketFd:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 775
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->valid()Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_1

    .line 779
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->dup()Landroid/os/ParcelFileDescriptor;

    move-result-object v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 780
    .restart local v1    # "result":Landroid/os/ParcelFileDescriptor;
    return-object v1

    .line 781
    .end local v1    # "result":Landroid/os/ParcelFileDescriptor;
    :catch_2
    move-exception v2

    .line 782
    .local v2, "ex":Ljava/io/IOException;
    return-object v1

    .line 776
    .end local v2    # "ex":Ljava/io/IOException;
    :cond_4
    :goto_1
    return-object v1

    .line 754
    :cond_5
    :goto_2
    return-object v1
.end method

.method public insertFilterInfo(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "defaultLabel"    # Ljava/lang/String;
    .param p3, "iconUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;)Z"
        }
    .end annotation

    .line 950
    .local p4, "filterInfos":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 951
    const-string v0, "CallingPkgHook"

    const-string v1, "Check permission failed when insertFilterInfo."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    const/4 v0, 0x0

    return v0

    .line 955
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/FilterInfoInjector;->getInstance()Lcom/miui/hybrid/hook/FilterInfoInjector;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/hybrid/hook/FilterInfoInjector;->insertFilterInfo(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public insertPackageInfo(Landroid/content/pm/PackageInfo;)Z
    .locals 2
    .param p1, "pInfo"    # Landroid/content/pm/PackageInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 693
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 694
    const-string v0, "PkgInfoHook"

    const-string v1, "Check permission failed when insert PackageInfo."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    const/4 v0, 0x0

    return v0

    .line 697
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/PkgInfoHook;->getInstance()Lcom/miui/hybrid/hook/PkgInfoHook;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/hybrid/hook/PkgInfoHook;->insert(Landroid/content/pm/PackageInfo;)Z

    move-result v0

    return v0
.end method

.method public insertRedirectRule(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "destPkg"    # Ljava/lang/String;
    .param p3, "redirectPkgname"    # Ljava/lang/String;
    .param p4, "clsNameMap"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 712
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 713
    const-string v0, "IntentHook"

    const-string v1, "Check permission failed when insert RedirectRule."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    const/4 v0, 0x0

    return v0

    .line 717
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/IntentHook;->getInstance()Lcom/miui/hybrid/hook/IntentHook;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/hybrid/hook/IntentHook;->insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public markPerceptibleJank(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 422
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    .line 423
    invoke-interface {v0, p1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->markPerceptibleJank(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    :cond_0
    goto :goto_0

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 428
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public removeCallingPkgHookRule(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "hostApp"    # Ljava/lang/String;
    .param p2, "originCallingPkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 835
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 836
    const-string v0, "CallingPkgHook"

    const-string v1, "Check permission failed when removeCallingPkgHookRule."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    const/4 v0, 0x0

    return v0

    .line 840
    :cond_0
    invoke-static {}, Lcom/miui/hybrid/hook/CallingPkgHook;->getInstance()Lcom/miui/hybrid/hook/CallingPkgHook;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/miui/hybrid/hook/CallingPkgHook;->remove(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public reportAnr(ILjava/lang/String;JJLjava/lang/String;)V
    .locals 14
    .param p1, "callingPid"    # I
    .param p2, "windowName"    # Ljava/lang/String;
    .param p3, "totalDuration"    # J
    .param p5, "endTs"    # J
    .param p7, "cpuInfo"    # Ljava/lang/String;

    .line 380
    const/4 v2, -0x1

    const/4 v10, 0x7

    const-wide/16 v11, 0x0

    move-object v0, p0

    move v1, p1

    move-object/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v13, p7

    invoke-virtual/range {v0 .. v13}, Lcom/miui/server/PerfShielderService;->reportPerceptibleJank(IILjava/lang/String;JJJIJLjava/lang/String;)V

    .line 382
    return-void
.end method

.method public reportApplicationStart(Ljava/lang/String;II[Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "startFlag"    # I
    .param p3, "residentPkg"    # I
    .param p4, "residentPkgNameList"    # [Ljava/lang/String;

    .line 1016
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1017
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "packageName"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    const-string/jumbo v1, "startFlag"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1019
    const-string v1, "residentPkg"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1020
    const-string v1, "residentPkgNameList"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1022
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v1, :cond_0

    .line 1023
    invoke-interface {v1, v0}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportApplicationStart(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1027
    :cond_0
    goto :goto_0

    .line 1025
    :catch_0
    move-exception v1

    .line 1026
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1028
    .end local v1    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public reportExcessiveCpuUsageRecords(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .line 474
    .local p1, "records":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 475
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    invoke-interface {v0, p1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportExcessiveCpuUsageRecords(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :cond_0
    goto :goto_0

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 480
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public reportKillMessage(Ljava/lang/String;IIJ)V
    .locals 6
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "uid"    # I
    .param p4, "pss"    # J

    .line 996
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    .line 997
    move-object v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportKillMessage(Ljava/lang/String;IIJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1001
    :cond_0
    goto :goto_0

    .line 999
    :catch_0
    move-exception v0

    .line 1000
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1002
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public reportKillProcessEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJJZZ)V
    .locals 16
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "adj"    # I
    .param p5, "pss"    # J
    .param p7, "rss"    # J
    .param p9, "memAvail"    # J
    .param p11, "residentDuration"    # J
    .param p13, "hasActivities"    # Z
    .param p14, "hasForegroundServices"    # Z

    .line 1033
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v1, v0

    .line 1034
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v0, "packageName"

    move-object/from16 v2, p2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    const-string v0, "reason"

    move-object/from16 v3, p3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    const-string v0, "adj"

    move/from16 v4, p4

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1037
    const-string v0, "pss"

    move-wide/from16 v5, p5

    invoke-virtual {v1, v0, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1038
    const-string v0, "rss"

    move-wide/from16 v7, p7

    invoke-virtual {v1, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1039
    const-string v0, "memAvail"

    move-wide/from16 v9, p9

    invoke-virtual {v1, v0, v9, v10}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1040
    const-string v0, "residentDuration"

    move-wide/from16 v11, p11

    invoke-virtual {v1, v0, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1041
    const-string v0, "hasActivities"

    move/from16 v13, p13

    invoke-virtual {v1, v0, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1042
    const-string v0, "hasForegroundServices"

    move/from16 v14, p14

    invoke-virtual {v1, v0, v14}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1044
    move-object/from16 v15, p0

    :try_start_0
    iget-object v0, v15, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    .line 1045
    invoke-interface {v0, v1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportKillProcessEvent(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1049
    :cond_0
    goto :goto_0

    .line 1047
    :catch_0
    move-exception v0

    .line 1048
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1050
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public reportMiSpeedRecord(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "data"    # Landroid/os/Bundle;

    .line 1006
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    .line 1007
    invoke-interface {v0, p1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportMiSpeedRecord(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011
    :cond_0
    goto :goto_0

    .line 1009
    :catch_0
    move-exception v0

    .line 1010
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1012
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public reportNotificationClick(Ljava/lang/String;Landroid/content/Intent;J)V
    .locals 3
    .param p1, "postPackage"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "uptimeMillis"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 936
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    .line 937
    .local v0, "perfService":Lcom/miui/daemon/performance/server/IMiuiPerfService;
    if-nez v0, :cond_0

    .line 938
    return-void

    .line 940
    :cond_0
    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_1

    .line 941
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p3

    .line 944
    :cond_1
    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportNotificationClick(Ljava/lang/String;Landroid/content/Intent;J)V

    .line 945
    return-void
.end method

.method public reportPerceptibleJank(IILjava/lang/String;JJJIJ)V
    .locals 14
    .param p1, "callingPid"    # I
    .param p2, "renderThreadTid"    # I
    .param p3, "windowName"    # Ljava/lang/String;
    .param p4, "totalDuration"    # J
    .param p6, "maxFrameDuration"    # J
    .param p8, "endTs"    # J
    .param p10, "appCause"    # I
    .param p11, "numFrames"    # J

    .line 374
    const-string v13, ""

    move-object v0, p0

    move v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move/from16 v10, p10

    move-wide/from16 v11, p11

    invoke-virtual/range {v0 .. v13}, Lcom/miui/server/PerfShielderService;->reportPerceptibleJank(IILjava/lang/String;JJJIJLjava/lang/String;)V

    .line 376
    return-void
.end method

.method public reportPerceptibleJank(IILjava/lang/String;JJJIJLjava/lang/String;)V
    .locals 18
    .param p1, "callingPid"    # I
    .param p2, "renderThreadTid"    # I
    .param p3, "windowName"    # Ljava/lang/String;
    .param p4, "totalDuration"    # J
    .param p6, "maxFrameDuration"    # J
    .param p8, "endTs"    # J
    .param p10, "appCause"    # I
    .param p11, "numFrames"    # J
    .param p13, "cpuInfo"    # Ljava/lang/String;

    .line 386
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-wide/from16 v2, p4

    move-wide/from16 v4, p6

    move-wide/from16 v6, p8

    move/from16 v8, p10

    move-wide/from16 v9, p11

    invoke-static/range {p1 .. p1}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v11

    .line 388
    .local v11, "callingPkg":Ljava/lang/String;
    if-nez v11, :cond_0

    .line 389
    return-void

    .line 393
    :cond_0
    if-eqz v1, :cond_2

    sget-object v12, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_WHITE_LIST:Ljava/util/ArrayList;

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    sget-object v12, Lcom/miui/server/PerfShielderService;->WINDOW_NAME_REX:Ljava/util/regex/Pattern;

    invoke-virtual {v12, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/regex/Matcher;->matches()Z

    move-result v12

    if-nez v12, :cond_2

    .line 394
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x3

    if-lt v13, v14, :cond_1

    const/4 v13, 0x0

    invoke-virtual {v1, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    goto :goto_0

    :cond_1
    move-object v13, v1

    :goto_0
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->hashCode()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 395
    .local v12, "windowIdentityCode":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "-"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 398
    .end local v12    # "windowIdentityCode":Ljava/lang/String;
    .end local p3    # "windowName":Ljava/lang/String;
    .local v1, "windowName":Ljava/lang/String;
    :cond_2
    new-instance v12, Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;

    const/4 v13, 0x0

    invoke-direct {v12, v0, v13}, Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;-><init>(Lcom/miui/server/PerfShielderService;Lcom/miui/server/PerfShielderService$PackageVersionNameGetter-IA;)V

    invoke-virtual {v12, v11}, Lcom/miui/server/PerfShielderService$PackageVersionNameGetter;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 399
    .local v12, "packageVersion":Ljava/lang/String;
    if-ltz v8, :cond_3

    sget-object v13, Lcom/miui/server/PerfShielderService;->SELF_CAUSE_NAMES:[Ljava/lang/String;

    array-length v14, v13

    if-ge v8, v14, :cond_3

    aget-object v13, v13, v8

    goto :goto_1

    :cond_3
    const-string v13, "Unknown"

    .line 401
    .local v13, "strAppCause":Ljava/lang/String;
    :goto_1
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "|"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-wide/32 v16, 0xf4240

    move-object/from16 p3, v1

    .end local v1    # "windowName":Ljava/lang/String;
    .restart local p3    # "windowName":Ljava/lang/String;
    div-long v0, v2, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object v1, v13

    .end local v13    # "strAppCause":Ljava/lang/String;
    .local v1, "strAppCause":Ljava/lang/String;
    div-long v13, v4, v16

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v13, "PerfShielderService"

    invoke-static {v13, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 406
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v13, "pid"

    move/from16 v14, p1

    invoke-virtual {v0, v13, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 407
    const-string/jumbo v13, "tid"

    move/from16 v15, p2

    invoke-virtual {v0, v13, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 408
    const-string v13, "pkg"

    invoke-virtual {v0, v13, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string v13, "pkgVersion"

    invoke-virtual {v0, v13, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string/jumbo v13, "window"

    move-object/from16 v8, p3

    .end local p3    # "windowName":Ljava/lang/String;
    .local v8, "windowName":Ljava/lang/String;
    invoke-virtual {v0, v13, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string/jumbo v13, "totalDuration"

    invoke-virtual {v0, v13, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 412
    const-string v13, "maxFrameDuration"

    invoke-virtual {v0, v13, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 413
    const-string v13, "endTs"

    invoke-virtual {v0, v13, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 414
    const-string v13, "appCause"

    invoke-virtual {v0, v13, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v13, "cpuInfo"

    move-object/from16 v16, v1

    move-object/from16 v1, p13

    .end local v1    # "strAppCause":Ljava/lang/String;
    .local v16, "strAppCause":Ljava/lang/String;
    invoke-virtual {v0, v13, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    const-string v13, "numFrames"

    invoke-virtual {v0, v13, v9, v10}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 417
    move-object/from16 v13, p0

    invoke-virtual {v13, v0}, Lcom/miui/server/PerfShielderService;->markPerceptibleJank(Landroid/os/Bundle;)V

    .line 418
    return-void
.end method

.method public reportProcessCleanEvent(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .line 484
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    .line 485
    invoke-interface {v0, p1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportProcessCleanEvent(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :cond_0
    goto :goto_0

    .line 487
    :catch_0
    move-exception v0

    .line 488
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 490
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public reportPssRecord(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V
    .locals 7
    .param p1, "processName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "pss"    # J
    .param p5, "versionName"    # Ljava/lang/String;
    .param p6, "versionCode"    # I

    .line 986
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_0

    .line 987
    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->reportPssRecord(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 991
    :cond_0
    goto :goto_0

    .line 989
    :catch_0
    move-exception v0

    .line 990
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 992
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public resolveQuickAppInfos(Landroid/content/Intent;)Ljava/util/List;
    .locals 2
    .param p1, "targetIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List<",
            "Lcom/android/internal/app/QuickAppResolveInfo;",
            ">;"
        }
    .end annotation

    .line 971
    invoke-static {}, Lcom/miui/hybrid/hook/FilterInfoInjector;->getInstance()Lcom/miui/hybrid/hook/FilterInfoInjector;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/miui/hybrid/hook/FilterInfoInjector;->resolveAppInfos(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setForkedProcessGroup(IIILjava/lang/String;)V
    .locals 8
    .param p1, "puid"    # I
    .param p2, "ppid"    # I
    .param p3, "group"    # I
    .param p4, "processName"    # Ljava/lang/String;

    .line 236
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicyMask()I

    move-result v0

    .line 242
    .local v0, "threadPolicyMask":I
    invoke-static {p1, p2}, Lmiui/process/ProcessManagerNative;->getCgroupFilePath(II)Ljava/lang/String;

    move-result-object v1

    .line 243
    .local v1, "fileName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 245
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    .line 246
    const/4 v3, 0x0

    .line 247
    .local v3, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_2

    .line 248
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 249
    .local v4, "subPid":I
    if-ne v4, p2, :cond_0

    .line 250
    goto :goto_0

    .line 252
    :cond_0
    if-eqz p4, :cond_1

    invoke-direct {p0, v4, p4}, Lcom/miui/server/PerfShielderService;->needToLimit(ILjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 253
    goto :goto_0

    .line 255
    :cond_1
    invoke-static {v4, p3}, Landroid/os/Process;->setProcessGroup(II)V

    .line 256
    const-string v5, "PerfShielderService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sFPG ppid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " grp:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " forked:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " pid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    nop

    .end local v4    # "subPid":I
    goto :goto_0

    .line 263
    .end local v3    # "line":Ljava/lang/String;
    :cond_2
    nop

    .line 264
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 268
    :cond_3
    :goto_1
    goto :goto_2

    .line 266
    :catch_0
    move-exception v3

    .line 267
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 269
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 262
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 259
    :catch_1
    move-exception v3

    .line 260
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 263
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_3

    .line 264
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 270
    :goto_2
    return-void

    .line 263
    :goto_3
    if-eqz v2, :cond_4

    .line 264
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    .line 266
    :catch_2
    move-exception v4

    .line 267
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 268
    .end local v4    # "e":Ljava/io/IOException;
    :cond_4
    :goto_4
    nop

    .line 269
    :goto_5
    throw v3
.end method

.method public setHapLinks(Ljava/util/Map;Landroid/content/pm/ActivityInfo;)V
    .locals 2
    .param p1, "data"    # Ljava/util/Map;
    .param p2, "activityInfo"    # Landroid/content/pm/ActivityInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 975
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/hybrid/hook/PermissionChecker;->check(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 976
    const-string v0, "CallingPkgHook"

    const-string v1, "Check permission failed when setHapLinks."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    return-void

    .line 980
    :cond_0
    invoke-static {p1, p2}, Lcom/miui/hybrid/hook/HapLinksInjector;->setData(Ljava/util/Map;Landroid/content/pm/ActivityInfo;)V

    .line 981
    return-void
.end method

.method public setSchedFgPid(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 511
    if-gtz p1, :cond_0

    .line 512
    return-void

    .line 516
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mPerfService:Lcom/miui/daemon/performance/server/IMiuiPerfService;

    if-eqz v0, :cond_1

    .line 517
    invoke-interface {v0, p1}, Lcom/miui/daemon/performance/server/IMiuiPerfService;->setSchedFgPid(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    :cond_1
    goto :goto_0

    .line 519
    :catch_0
    move-exception v0

    .line 520
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 522
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_0
    return-void
.end method

.method public systemReady()V
    .locals 4

    .line 199
    iget-object v0, p0, Lcom/miui/server/PerfShielderService;->mHandler:Lcom/miui/server/PerfShielderService$BindServiceHandler;

    new-instance v1, Lcom/miui/server/PerfShielderService$1;

    invoke-direct {v1, p0}, Lcom/miui/server/PerfShielderService$1;-><init>(Lcom/miui/server/PerfShielderService;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/server/PerfShielderService$BindServiceHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 206
    const-wide/16 v0, 0x5dc

    invoke-direct {p0, v0, v1}, Lcom/miui/server/PerfShielderService;->sendBindMiuiSysUserMsg(J)V

    .line 207
    return-void
.end method

.method public updateProcessFullMemInfoByPids([I)Ljava/util/List;
    .locals 10
    .param p1, "pids"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .line 277
    array-length v0, p1

    .line 278
    .local v0, "pidSize":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 279
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    new-instance v2, Lcom/miui/server/PerfShielderService$PidSwapGetter;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/miui/server/PerfShielderService$PidSwapGetter;-><init>(Lcom/miui/server/PerfShielderService;Lcom/miui/server/PerfShielderService$PidSwapGetter-IA;)V

    .line 280
    .local v2, "swapgetter":Lcom/miui/server/PerfShielderService$PidSwapGetter;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 281
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 282
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v5, "pid"

    aget v6, p1, v3

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 283
    const-string v5, "lastPssTime"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 284
    aget v5, p1, v3

    invoke-direct {p0, v5}, Lcom/miui/server/PerfShielderService;->getProcessPss(I)J

    move-result-wide v5

    const-string v7, "lastPss"

    invoke-virtual {v4, v7, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 285
    const-string v5, "lastRssTime"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 286
    aget v5, p1, v3

    invoke-direct {p0, v5}, Lcom/miui/server/PerfShielderService;->getProcessStatusValues(I)[J

    move-result-object v5

    .line 287
    .local v5, "pidStatus":[J
    const/4 v6, 0x0

    aget-wide v6, v5, v6

    const-string/jumbo v8, "swap"

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 288
    const/4 v6, 0x1

    aget-wide v6, v5, v6

    long-to-int v6, v6

    .line 289
    .local v6, "ppid":I
    const-string v7, "ppid"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 290
    const-string v7, "pswap"

    invoke-virtual {v2, v6}, Lcom/miui/server/PerfShielderService$PidSwapGetter;->get(I)J

    move-result-wide v8

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 291
    const/4 v7, 0x2

    aget-wide v7, v5, v7

    const-string v9, "rss"

    invoke-virtual {v4, v9, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 292
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "pidStatus":[J
    .end local v6    # "ppid":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 294
    .end local v3    # "i":I
    :cond_0
    return-object v1
.end method

.method public updateProcessPartialMemInfoByPids([I)Ljava/util/List;
    .locals 10
    .param p1, "pids"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .line 298
    array-length v0, p1

    .line 299
    .local v0, "pidSize":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 300
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    new-instance v2, Lcom/miui/server/PerfShielderService$PidSwapGetter;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/miui/server/PerfShielderService$PidSwapGetter;-><init>(Lcom/miui/server/PerfShielderService;Lcom/miui/server/PerfShielderService$PidSwapGetter-IA;)V

    .line 301
    .local v2, "swapgetter":Lcom/miui/server/PerfShielderService$PidSwapGetter;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 302
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 303
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v5, "pid"

    aget v6, p1, v3

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 304
    const-string v5, "lastRssTime"

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 305
    aget v5, p1, v3

    invoke-direct {p0, v5}, Lcom/miui/server/PerfShielderService;->getProcessStatusValues(I)[J

    move-result-object v5

    .line 306
    .local v5, "pidStatus":[J
    const/4 v6, 0x0

    aget-wide v6, v5, v6

    const-string/jumbo v8, "swap"

    invoke-virtual {v4, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 307
    const/4 v6, 0x1

    aget-wide v6, v5, v6

    long-to-int v6, v6

    .line 308
    .local v6, "ppid":I
    const-string v7, "ppid"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 309
    const-string v7, "pswap"

    invoke-virtual {v2, v6}, Lcom/miui/server/PerfShielderService$PidSwapGetter;->get(I)J

    move-result-wide v8

    invoke-virtual {v4, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 310
    const/4 v7, 0x2

    aget-wide v7, v5, v7

    const-string v9, "rss"

    invoke-virtual {v4, v9, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 311
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    .end local v4    # "bundle":Landroid/os/Bundle;
    .end local v5    # "pidStatus":[J
    .end local v6    # "ppid":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 313
    .end local v3    # "i":I
    :cond_0
    return-object v1
.end method
