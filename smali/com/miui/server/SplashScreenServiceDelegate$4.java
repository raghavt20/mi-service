class com.miui.server.SplashScreenServiceDelegate$4 extends com.miui.server.ISplashPackageCheckListener$Stub {
	 /* .source "SplashScreenServiceDelegate.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/SplashScreenServiceDelegate; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.SplashScreenServiceDelegate this$0; //synthetic
/* # direct methods */
 com.miui.server.SplashScreenServiceDelegate$4 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/SplashScreenServiceDelegate; */
/* .line 261 */
this.this$0 = p1;
/* invoke-direct {p0}, Lcom/miui/server/ISplashPackageCheckListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void updateSplashPackageCheckInfo ( com.miui.server.SplashPackageCheckInfo p0 ) {
/* .locals 3 */
/* .param p1, "splashPackageCheckInfo" # Lcom/miui/server/SplashPackageCheckInfo; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 283 */
try { // :try_start_0
v0 = this.this$0;
v0 = com.miui.server.SplashScreenServiceDelegate .-$$Nest$mcheckSplashPackageCheckInfo ( v0,p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 284 */
	 v0 = this.this$0;
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "Valid "; // const-string v2, "Valid "
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v0,v1 );
	 /* .line 285 */
	 v0 = this.this$0;
	 com.miui.server.SplashScreenServiceDelegate .-$$Nest$mkeepSplashPackageCheckInfo ( v0,p1 );
	 /* .line 287 */
} // :cond_0
v0 = this.this$0;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Invalid "; // const-string v2, "Invalid "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v0,v1 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 291 */
} // :goto_0
/* .line 289 */
/* :catch_0 */
/* move-exception v0 */
/* .line 290 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.this$0;
/* const-string/jumbo v2, "updateSplashPackageCheckInfo exception" */
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogE ( v1,v2,v0 );
/* .line 292 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
public void updateSplashPackageCheckInfoList ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/SplashPackageCheckInfo;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Landroid/os/RemoteException; */
/* } */
} // .end annotation
/* .line 266 */
/* .local p1, "splashPackageCheckInfos":Ljava/util/List;, "Ljava/util/List<Lcom/miui/server/SplashPackageCheckInfo;>;" */
try { // :try_start_0
v0 = this.this$0;
/* const-string/jumbo v1, "updateSplashPackageCheckInfoList" */
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogI ( v0,v1 );
/* .line 267 */
v0 = this.this$0;
com.miui.server.SplashScreenServiceDelegate .-$$Nest$fgetmSplashPackageCheckInfoMap ( v0 );
/* .line 268 */
v0 = if ( p1 != null) { // if-eqz p1, :cond_2
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 272 */
} // :cond_0
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Lcom/miui/server/SplashPackageCheckInfo; */
/* .line 273 */
/* .local v1, "info":Lcom/miui/server/SplashPackageCheckInfo; */
(( com.miui.server.SplashScreenServiceDelegate$4 ) p0 ).updateSplashPackageCheckInfo ( v1 ); // invoke-virtual {p0, v1}, Lcom/miui/server/SplashScreenServiceDelegate$4;->updateSplashPackageCheckInfo(Lcom/miui/server/SplashPackageCheckInfo;)V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 274 */
} // .end local v1 # "info":Lcom/miui/server/SplashPackageCheckInfo;
/* .line 277 */
} // :cond_1
/* .line 269 */
} // :cond_2
} // :goto_1
return;
/* .line 275 */
/* :catch_0 */
/* move-exception v0 */
/* .line 276 */
/* .local v0, "e":Ljava/lang/Exception; */
v1 = this.this$0;
/* const-string/jumbo v2, "updateSplashPackageCheckInfoList exception" */
com.miui.server.SplashScreenServiceDelegate .-$$Nest$mlogE ( v1,v2,v0 );
/* .line 278 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_2
return;
} // .end method
