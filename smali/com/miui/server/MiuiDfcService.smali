.class public Lcom/miui/server/MiuiDfcService;
.super Lmiui/dfc/IDfcService$Stub;
.source "MiuiDfcService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;,
        Lcom/miui/server/MiuiDfcService$Lifecycle;
    }
.end annotation


# static fields
.field public static final ACTION_TEST_USB_STATE:Ljava/lang/String; = "android.hardware.usb.action.USB_STATE"

.field public static final BUILD_TYPE:Ljava/lang/String;

.field public static final DFC_DEBUG:Z

.field private static final DFC_NATIVE_SERVICE:Ljava/lang/String; = "DfcNativeService"

.field public static final IS_FRONT_SCENE:Ljava/lang/String; = "sys.dfcservice.is_front_scene"

.field public static final IS_USERDEBUG:Z

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.dfc.service"

.field public static final START_DFC_PROP:Ljava/lang/String; = "sys.dfcservice.ctrl"

.field public static final STOP_COMPRESS_PROP:Ljava/lang/String; = "sys.dfcservice.stop_compress"

.field public static final STOP_SCAN_PROP:Ljava/lang/String; = "sys.dfcservice.stop_scan"

.field private static final TAG:Ljava/lang/String; = "MiuiDfcService"

.field private static volatile sInstance:Lcom/miui/server/MiuiDfcService;


# instance fields
.field private mContext:Landroid/content/Context;

.field mDeathHandler:Landroid/os/IBinder$DeathRecipient;

.field private volatile mService:Lmiui/dfc/IDfc;

.field private mUsbConnect:Z


# direct methods
.method static bridge synthetic -$$Nest$fputmService(Lcom/miui/server/MiuiDfcService;Lmiui/dfc/IDfc;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmUsbConnect(Lcom/miui/server/MiuiDfcService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 29
    nop

    .line 30
    const-string v0, "persist.sys.miui_dfc_debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/MiuiDfcService;->DFC_DEBUG:Z

    .line 37
    const-string v0, "ro.build.type"

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/MiuiDfcService;->BUILD_TYPE:Ljava/lang/String;

    .line 38
    const-string/jumbo v1, "userdebug"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/MiuiDfcService;->IS_USERDEBUG:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 122
    invoke-direct {p0}, Lmiui/dfc/IDfcService$Stub;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z

    .line 114
    new-instance v0, Lcom/miui/server/MiuiDfcService$1;

    invoke-direct {v0, p0}, Lcom/miui/server/MiuiDfcService$1;-><init>(Lcom/miui/server/MiuiDfcService;)V

    iput-object v0, p0, Lcom/miui/server/MiuiDfcService;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    .line 123
    const-string v0, "MiuiDfcService"

    const-string v1, "Create MiuiDfcService"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    return-void
.end method

.method private checkIsSystemUid()Z
    .locals 2

    .line 315
    const/16 v0, 0x3e8

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static getInstance()Lcom/miui/server/MiuiDfcService;
    .locals 2

    .line 70
    sget-object v0, Lcom/miui/server/MiuiDfcService;->sInstance:Lcom/miui/server/MiuiDfcService;

    if-nez v0, :cond_1

    .line 71
    const-class v0, Lcom/miui/server/MiuiDfcService;

    monitor-enter v0

    .line 72
    :try_start_0
    sget-object v1, Lcom/miui/server/MiuiDfcService;->sInstance:Lcom/miui/server/MiuiDfcService;

    if-nez v1, :cond_0

    .line 73
    new-instance v1, Lcom/miui/server/MiuiDfcService;

    invoke-direct {v1}, Lcom/miui/server/MiuiDfcService;-><init>()V

    sput-object v1, Lcom/miui/server/MiuiDfcService;->sInstance:Lcom/miui/server/MiuiDfcService;

    .line 75
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 77
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/MiuiDfcService;->sInstance:Lcom/miui/server/MiuiDfcService;

    return-object v0
.end method


# virtual methods
.method public compressDuplicateFiles(Ljava/lang/String;Lmiui/dfc/IDupCompressCallback;)V
    .locals 8
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "callback"    # Lmiui/dfc/IDupCompressCallback;

    .line 206
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    if-nez v0, :cond_0

    .line 207
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    return-void

    .line 210
    :cond_0
    const-string v0, "compressDuplicateFiles!"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const-string v0, ""

    .line 213
    .local v0, "result":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;

    move-result-object v2

    .line 214
    .local v2, "service":Lmiui/dfc/IDfc;
    if-eqz v2, :cond_3

    .line 215
    const-string/jumbo v3, "sys.dfcservice.stop_compress"

    const-string v4, "false"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-interface {v2, p1}, Lmiui/dfc/IDfc;->Dfc_compressDuplicateFiles(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 217
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dfc_compressDuplicateFiles, result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_1
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "countSizeArray":[Ljava/lang/String;
    if-eqz p2, :cond_2

    if-eqz v3, :cond_2

    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 221
    const/4 v4, 0x0

    aget-object v4, v3, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v6, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-interface {p2, v4, v5, v6, v7}, Lmiui/dfc/IDupCompressCallback;->onFinish(JJ)V

    .line 222
    return-void

    .line 224
    .end local v3    # "countSizeArray":[Ljava/lang/String;
    :cond_2
    goto :goto_0

    .line 225
    :cond_3
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "compressDuplicateFiles getDfcNativeService is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    .end local v2    # "service":Lmiui/dfc/IDfc;
    :cond_4
    :goto_0
    goto :goto_1

    .line 227
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dfc_compressDuplicateFiles error:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    if-eqz p2, :cond_5

    .line 233
    const-wide/16 v2, 0x0

    :try_start_1
    invoke-interface {p2, v2, v3, v2, v3}, Lmiui/dfc/IDupCompressCallback;->onFinish(JJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 235
    :catch_1
    move-exception v2

    .line 236
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFinish error:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 237
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_2
    nop

    .line 238
    :goto_3
    return-void
.end method

.method public forDfcInitialization(Landroid/content/Context;)Lcom/miui/server/MiuiDfcService;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 61
    iput-object p1, p0, Lcom/miui/server/MiuiDfcService;->mContext:Landroid/content/Context;

    .line 62
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "use MiuiDfcService(Context context) mContext"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/MiuiDfcService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiuiDfcService"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 64
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lcom/miui/server/MiuiDfcService;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;

    invoke-direct {v2, p0}, Lcom/miui/server/MiuiDfcService$UsbStatusReceiver;-><init>(Lcom/miui/server/MiuiDfcService;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 66
    invoke-static {}, Lcom/miui/server/MiuiDfcService;->getInstance()Lcom/miui/server/MiuiDfcService;

    move-result-object v1

    return-object v1
.end method

.method public getDFCVersion()I
    .locals 2

    .line 128
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    if-nez v0, :cond_0

    .line 129
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/4 v0, -0x1

    return v0

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "getDFCVersion!"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method protected getDfcNativeService()Lmiui/dfc/IDfc;
    .locals 5

    .line 81
    const-string/jumbo v0, "sys.dfcservice.is_front_scene"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 82
    .local v0, "isScene":I
    iget-boolean v2, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 83
    const-string v1, "MiuiDfcService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Usb connect, no need to get DfcNativeService, isScene:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const-string/jumbo v1, "sys.dfcservice.ctrl"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const/4 v1, 0x0

    return-object v1

    .line 87
    :cond_0
    iget-object v2, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;

    if-nez v2, :cond_4

    .line 88
    const-class v2, Lcom/miui/server/MiuiDfcService;

    monitor-enter v2

    .line 89
    :try_start_0
    iget-object v3, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;

    if-nez v3, :cond_3

    .line 90
    const-string/jumbo v3, "sys.dfcservice.ctrl"

    invoke-static {v3, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 91
    const-string/jumbo v3, "sys.dfcservice.ctrl"

    const-string/jumbo v4, "true"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    const-wide/16 v3, 0x1f4

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 96
    goto :goto_0

    .line 94
    :catch_0
    move-exception v3

    .line 95
    .local v3, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 98
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :goto_0
    const-string v3, "DfcNativeService"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lmiui/dfc/IDfc$Stub;->asInterface(Landroid/os/IBinder;)Lmiui/dfc/IDfc;

    move-result-object v3

    iput-object v3, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;

    .line 99
    iget-object v3, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_2

    .line 101
    :try_start_3
    iget-object v3, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;

    invoke-interface {v3}, Lmiui/dfc/IDfc;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/MiuiDfcService;->mDeathHandler:Landroid/os/IBinder$DeathRecipient;

    invoke-interface {v3, v4, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 102
    :catch_1
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 104
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    goto :goto_2

    .line 106
    :cond_2
    const-string v1, "MiuiDfcService"

    const-string v3, "failed to get DfcNativeService."

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_3
    :goto_2
    monitor-exit v2

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 111
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/miui/server/MiuiDfcService;->mService:Lmiui/dfc/IDfc;

    return-object v1
.end method

.method public getDuplicateFiles(Ljava/lang/String;Lmiui/dfc/IGetDuplicateFilesCallback;)V
    .locals 5
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "callback"    # Lmiui/dfc/IGetDuplicateFilesCallback;

    .line 178
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    if-nez v0, :cond_0

    .line 179
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return-void

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDuplicateFiles! data: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_1
    const-string v0, ""

    .line 185
    .local v0, "result":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;

    move-result-object v2

    .line 186
    .local v2, "service":Lmiui/dfc/IDfc;
    if-eqz v2, :cond_2

    .line 187
    invoke-interface {v2, p1}, Lmiui/dfc/IDfc;->Dfc_getDuplicateFiles(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 188
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dfc_getDuplicateFiles, result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 190
    :cond_2
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "getDuplicateFiles getDfcNativeService is null"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .end local v2    # "service":Lmiui/dfc/IDfc;
    :cond_3
    :goto_0
    goto :goto_1

    .line 192
    :catch_0
    move-exception v2

    .line 193
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Dfc_getDuplicateFiles error:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1
    if-eqz p2, :cond_4

    .line 197
    :try_start_1
    invoke-interface {p2, v0}, Lmiui/dfc/IGetDuplicateFilesCallback;->onFinish(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 199
    :catch_1
    move-exception v2

    .line 200
    .restart local v2    # "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IGetDuplicateFilesCallback callback error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 201
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_2
    nop

    .line 202
    :goto_3
    return-void
.end method

.method public isDfcDebug()Z
    .locals 1

    .line 319
    sget-boolean v0, Lcom/miui/server/MiuiDfcService;->IS_USERDEBUG:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/server/MiuiDfcService;->DFC_DEBUG:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isSupportDFC()Z
    .locals 5

    .line 138
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 139
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return v2

    .line 142
    :cond_0
    const-string/jumbo v0, "sys.dfcservice.is_front_scene"

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 143
    .local v0, "isScene":I
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isSupportDFC! isScene:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_1
    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget-boolean v3, p0, Lcom/miui/server/MiuiDfcService;->mUsbConnect:Z

    if-nez v3, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    return v2
.end method

.method public restoreDuplicateFiles(Ljava/lang/String;)J
    .locals 6
    .param p1, "data"    # Ljava/lang/String;

    .line 261
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    if-nez v0, :cond_0

    .line 262
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-wide/16 v0, 0x0

    return-wide v0

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreDuplicateFiles!, data:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_1
    const-wide/16 v2, 0x0

    .line 268
    .local v2, "result":J
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;

    move-result-object v0

    .line 269
    .local v0, "service":Lmiui/dfc/IDfc;
    if-eqz v0, :cond_2

    .line 270
    invoke-interface {v0, p1}, Lmiui/dfc/IDfc;->Dfc_restoreDuplicateFiles(Ljava/lang/String;)J

    move-result-wide v4

    move-wide v2, v4

    .line 271
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dfc_restoreDuplicateFiles, result:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    :cond_2
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "restoreDuplicateFiles getDfcNativeService is null"

    invoke-static {v1, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    .end local v0    # "service":Lmiui/dfc/IDfc;
    :cond_3
    :goto_0
    goto :goto_1

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dfc_restoreDuplicateFiles error:  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-wide v2
.end method

.method public scanDuplicateFiles(Ljava/lang/String;Lmiui/dfc/IDuplicateFileScanCallback;I)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "callback"    # Lmiui/dfc/IDuplicateFileScanCallback;
    .param p3, "flags"    # I

    .line 149
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    if-nez v0, :cond_0

    .line 150
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    return-void

    .line 153
    :cond_0
    const-string v0, "scanDuplicateFiles!"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;

    move-result-object v0

    .line 156
    .local v0, "service":Lmiui/dfc/IDfc;
    if-eqz v0, :cond_1

    .line 157
    const-string/jumbo v2, "sys.dfcservice.stop_scan"

    const-string v3, "false"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-interface {v0, p1}, Lmiui/dfc/IDfc;->Dfc_scanDuplicateFiles(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Dfc_scanDuplicateFiles done"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "scanDuplicateFiles getDfcNativeService is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    .end local v0    # "service":Lmiui/dfc/IDfc;
    :cond_2
    :goto_0
    goto :goto_1

    .line 163
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "scanDuplicateFiles error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    if-eqz p2, :cond_3

    .line 169
    :try_start_1
    invoke-interface {p2}, Lmiui/dfc/IDuplicateFileScanCallback;->onDumplicateFileScanfinished()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 171
    :catch_1
    move-exception v0

    .line 172
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDumplicateFileScanfinished error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 173
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    nop

    .line 174
    :goto_3
    return-void
.end method

.method public setDFCExceptRules(Ljava/lang/String;)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;

    .line 242
    invoke-direct {p0}, Lcom/miui/server/MiuiDfcService;->checkIsSystemUid()Z

    move-result v0

    const-string v1, "MiuiDfcService"

    if-nez v0, :cond_0

    .line 243
    const-string v0, "calling permission denied"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    return-void

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDFCExceptRules!, data:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->getDfcNativeService()Lmiui/dfc/IDfc;

    move-result-object v0

    .line 249
    .local v0, "service":Lmiui/dfc/IDfc;
    if-eqz v0, :cond_2

    .line 250
    invoke-interface {v0, p1}, Lmiui/dfc/IDfc;->Dfc_setDFCExceptRules(Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :cond_2
    invoke-virtual {p0}, Lcom/miui/server/MiuiDfcService;->isDfcDebug()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "setDFCExceptRules getDfcNativeService is null"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    .end local v0    # "service":Lmiui/dfc/IDfc;
    :cond_3
    :goto_0
    goto :goto_1

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dfc_setDFCExceptRules error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
