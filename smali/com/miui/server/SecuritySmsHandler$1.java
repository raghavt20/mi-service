class com.miui.server.SecuritySmsHandler$1 extends android.content.BroadcastReceiver {
	 /* .source "SecuritySmsHandler.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/SecuritySmsHandler; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.SecuritySmsHandler this$0; //synthetic
/* # direct methods */
 com.miui.server.SecuritySmsHandler$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/SecuritySmsHandler; */
/* .line 276 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 7 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 279 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 280 */
/* .local v0, "action":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "mNormalMsgResultReceiver sms dispatched, action:"; // const-string v2, "mNormalMsgResultReceiver sms dispatched, action:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "SecuritySmsHandler"; // const-string v2, "SecuritySmsHandler"
android.util.Log .i ( v2,v1 );
/* .line 282 */
final String v1 = "android.provider.Telephony.SMS_DELIVER"; // const-string v1, "android.provider.Telephony.SMS_DELIVER"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* const/16 v3, 0x10 */
final String v4 = "android.permission.RECEIVE_SMS"; // const-string v4, "android.permission.RECEIVE_SMS"
/* const/high16 v5, 0x8000000 */
int v6 = 0; // const/4 v6, 0x0
if ( v1 != null) { // if-eqz v1, :cond_0
	 /* .line 283 */
	 (( android.content.Intent ) p2 ).setComponent ( v6 ); // invoke-virtual {p2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
	 /* .line 284 */
	 (( android.content.Intent ) p2 ).setFlags ( v5 ); // invoke-virtual {p2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
	 /* .line 285 */
	 final String v1 = "android.provider.Telephony.SMS_RECEIVED"; // const-string v1, "android.provider.Telephony.SMS_RECEIVED"
	 (( android.content.Intent ) p2 ).setAction ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 286 */
	 v1 = this.this$0;
	 com.miui.server.SecuritySmsHandler .-$$Nest$mdispatchIntent ( v1,p2,v4,v3,v6 );
	 /* .line 287 */
	 final String v1 = "mNormalMsgResultReceiver dispatch SMS_RECEIVED_ACTION"; // const-string v1, "mNormalMsgResultReceiver dispatch SMS_RECEIVED_ACTION"
	 android.util.Log .i ( v2,v1 );
	 /* .line 289 */
} // :cond_0
final String v1 = "android.provider.Telephony.WAP_PUSH_DELIVER"; // const-string v1, "android.provider.Telephony.WAP_PUSH_DELIVER"
v1 = (( java.lang.String ) v1 ).equals ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
	 /* .line 290 */
	 (( android.content.Intent ) p2 ).setComponent ( v6 ); // invoke-virtual {p2, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
	 /* .line 291 */
	 (( android.content.Intent ) p2 ).setFlags ( v5 ); // invoke-virtual {p2, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;
	 /* .line 292 */
	 final String v1 = "android.provider.Telephony.WAP_PUSH_RECEIVED"; // const-string v1, "android.provider.Telephony.WAP_PUSH_RECEIVED"
	 (( android.content.Intent ) p2 ).setAction ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;
	 /* .line 293 */
	 v1 = this.this$0;
	 com.miui.server.SecuritySmsHandler .-$$Nest$mdispatchIntent ( v1,p2,v4,v3,v6 );
	 /* .line 294 */
	 final String v1 = "mNormalMsgResultReceiver dispatch WAP_PUSH_RECEIVED_ACTION"; // const-string v1, "mNormalMsgResultReceiver dispatch WAP_PUSH_RECEIVED_ACTION"
	 android.util.Log .i ( v2,v1 );
	 /* .line 296 */
} // :cond_1
} // :goto_0
return;
} // .end method
