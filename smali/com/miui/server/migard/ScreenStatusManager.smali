.class public Lcom/miui/server/migard/ScreenStatusManager;
.super Landroid/content/BroadcastReceiver;
.source "ScreenStatusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/miui/server/migard/ScreenStatusManager;


# instance fields
.field private mCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mFilter:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    const-class v0, Lcom/miui/server/migard/ScreenStatusManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/ScreenStatusManager;->TAG:Ljava/lang/String;

    .line 14
    new-instance v0, Lcom/miui/server/migard/ScreenStatusManager;

    invoke-direct {v0}, Lcom/miui/server/migard/ScreenStatusManager;-><init>()V

    sput-object v0, Lcom/miui/server/migard/ScreenStatusManager;->sInstance:Lcom/miui/server/migard/ScreenStatusManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    .line 35
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mFilter:Landroid/content/IntentFilter;

    .line 36
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public static getInstance()Lcom/miui/server/migard/ScreenStatusManager;
    .locals 1

    .line 27
    sget-object v0, Lcom/miui/server/migard/ScreenStatusManager;->sInstance:Lcom/miui/server/migard/ScreenStatusManager;

    return-object v0
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mFilter:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 41
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 43
    return-void

    .line 45
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_1
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :goto_0
    const/4 v1, -0x1

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_4

    .line 53
    :pswitch_0
    sget-object v1, Lcom/miui/server/migard/ScreenStatusManager;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "user present"

    invoke-static {v1, v2}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v1, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;

    .line 55
    .local v2, "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
    invoke-interface {v2}, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;->onUserPresent()V

    .line 56
    .end local v2    # "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
    goto :goto_2

    .line 57
    :cond_2
    goto :goto_4

    .line 47
    :pswitch_1
    sget-object v1, Lcom/miui/server/migard/ScreenStatusManager;->TAG:Ljava/lang/String;

    const-string v2, "screen off"

    invoke-static {v1, v2}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iget-object v1, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;

    .line 49
    .restart local v2    # "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
    invoke-interface {v2}, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;->onScreenOff()V

    .line 50
    .end local v2    # "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
    goto :goto_3

    .line 51
    :cond_3
    nop

    .line 61
    :goto_4
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7ed8ea7f -> :sswitch_1
        0x311a1d6c -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public registerCallback(Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;

    .line 64
    sget-object v0, Lcom/miui/server/migard/ScreenStatusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Register callback, name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;->getCallbackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_0
    return-void
.end method

.method public unregisterCallback(Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;

    .line 70
    sget-object v0, Lcom/miui/server/migard/ScreenStatusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unregister callback, name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;->getCallbackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/miui/server/migard/ScreenStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 73
    :cond_0
    return-void
.end method
