.class public abstract Lcom/miui/server/migard/MiGardInternal;
.super Ljava/lang/Object;
.source "MiGardInternal.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract addGameProcessCompactor(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
.end method

.method public abstract addGameProcessKiller(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
.end method

.method public abstract notifyGameBackground()V
.end method

.method public abstract notifyGameForeground(Ljava/lang/String;)V
.end method

.method public abstract notifyProcessDied(I)V
.end method

.method public abstract onProcessKilled(IILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onProcessStart(IILjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onVpnConnected(Ljava/lang/String;Z)V
.end method

.method public abstract reclaimBackgroundForGame(J)V
.end method
