public class com.miui.server.migard.ScreenStatusManager extends android.content.BroadcastReceiver {
	 /* .source "ScreenStatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.miui.server.migard.ScreenStatusManager sInstance;
/* # instance fields */
private java.util.List mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final android.content.IntentFilter mFilter;
/* # direct methods */
static com.miui.server.migard.ScreenStatusManager ( ) {
/* .locals 1 */
/* .line 13 */
/* const-class v0, Lcom/miui/server/migard/ScreenStatusManager; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 14 */
/* new-instance v0, Lcom/miui/server/migard/ScreenStatusManager; */
/* invoke-direct {v0}, Lcom/miui/server/migard/ScreenStatusManager;-><init>()V */
return;
} // .end method
private com.miui.server.migard.ScreenStatusManager ( ) {
/* .locals 2 */
/* .line 34 */
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
/* .line 24 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCallbacks = v0;
/* .line 35 */
/* new-instance v0, Landroid/content/IntentFilter; */
/* invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V */
this.mFilter = v0;
/* .line 36 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 37 */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
(( android.content.IntentFilter ) v0 ).addAction ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V
/* .line 38 */
return;
} // .end method
public static com.miui.server.migard.ScreenStatusManager getInstance ( ) {
/* .locals 1 */
/* .line 27 */
v0 = com.miui.server.migard.ScreenStatusManager.sInstance;
} // .end method
/* # virtual methods */
public android.content.IntentFilter getFilter ( ) {
/* .locals 1 */
/* .line 31 */
v0 = this.mFilter;
} // .end method
public final void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 41 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* .line 42 */
/* .local v0, "action":Ljava/lang/String; */
/* if-nez v0, :cond_0 */
/* .line 43 */
return;
/* .line 45 */
} // :cond_0
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "android.intent.action.USER_PRESENT"; // const-string v1, "android.intent.action.USER_PRESENT"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 1; // const/4 v1, 0x1
/* :sswitch_1 */
final String v1 = "android.intent.action.SCREEN_OFF"; // const-string v1, "android.intent.action.SCREEN_OFF"
v1 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
int v1 = 0; // const/4 v1, 0x0
} // :goto_0
int v1 = -1; // const/4 v1, -0x1
} // :goto_1
/* packed-switch v1, :pswitch_data_0 */
/* .line 53 */
/* :pswitch_0 */
v1 = com.miui.server.migard.ScreenStatusManager.TAG;
/* const-string/jumbo v2, "user present" */
com.miui.server.migard.utils.LogUtils .d ( v1,v2 );
/* .line 54 */
v1 = this.mCallbacks;
v2 = } // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_2
/* check-cast v2, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
/* .line 55 */
/* .local v2, "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
/* .line 56 */
} // .end local v2 # "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
/* .line 57 */
} // :cond_2
/* .line 47 */
/* :pswitch_1 */
v1 = com.miui.server.migard.ScreenStatusManager.TAG;
final String v2 = "screen off"; // const-string v2, "screen off"
com.miui.server.migard.utils.LogUtils .d ( v1,v2 );
/* .line 48 */
v1 = this.mCallbacks;
v2 = } // :goto_3
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
/* .line 49 */
/* .restart local v2 # "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
/* .line 50 */
} // .end local v2 # "cb":Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
/* .line 51 */
} // :cond_3
/* nop */
/* .line 61 */
} // :goto_4
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7ed8ea7f -> :sswitch_1 */
/* 0x311a1d6c -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void registerCallback ( com.miui.server.migard.ScreenStatusManager$IScreenChangedCallback p0 ) {
/* .locals 3 */
/* .param p1, "cb" # Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
/* .line 64 */
v0 = com.miui.server.migard.ScreenStatusManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Register callback, name:"; // const-string v2, "Register callback, name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 65 */
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_0 */
/* .line 66 */
v0 = this.mCallbacks;
/* .line 67 */
} // :cond_0
return;
} // .end method
public void unregisterCallback ( com.miui.server.migard.ScreenStatusManager$IScreenChangedCallback p0 ) {
/* .locals 3 */
/* .param p1, "cb" # Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback; */
/* .line 70 */
v0 = com.miui.server.migard.ScreenStatusManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unregister callback, name:"; // const-string v2, "Unregister callback, name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 71 */
v0 = v0 = this.mCallbacks;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 72 */
v0 = this.mCallbacks;
/* .line 73 */
} // :cond_0
return;
} // .end method
