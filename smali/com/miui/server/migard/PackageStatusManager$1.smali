.class Lcom/miui/server/migard/PackageStatusManager$1;
.super Lmiui/process/IForegroundInfoListener$Stub;
.source "PackageStatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/PackageStatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/migard/PackageStatusManager;


# direct methods
.method constructor <init>(Lcom/miui/server/migard/PackageStatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/migard/PackageStatusManager;

    .line 27
    iput-object p1, p0, Lcom/miui/server/migard/PackageStatusManager$1;->this$0:Lcom/miui/server/migard/PackageStatusManager;

    invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onForegroundInfoChanged(Lmiui/process/ForegroundInfo;)V
    .locals 5
    .param p1, "foregroundInfo"    # Lmiui/process/ForegroundInfo;

    .line 30
    invoke-static {}, Lcom/miui/server/migard/PackageStatusManager;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "on foreground info changed, info:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager$1;->this$0:Lcom/miui/server/migard/PackageStatusManager;

    invoke-static {v0}, Lcom/miui/server/migard/PackageStatusManager;->-$$Nest$fgetmCallbacks(Lcom/miui/server/migard/PackageStatusManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;

    .line 32
    .local v1, "cb":Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;
    iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundPid:I

    iget v3, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I

    iget-object v4, p1, Lmiui/process/ForegroundInfo;->mForegroundPackageName:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;->onForegroundChanged(IILjava/lang/String;)V

    .line 34
    .end local v1    # "cb":Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;
    goto :goto_0

    .line 35
    :cond_0
    return-void
.end method
