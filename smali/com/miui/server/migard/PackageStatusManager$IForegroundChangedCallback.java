public abstract class com.miui.server.migard.PackageStatusManager$IForegroundChangedCallback {
	 /* .source "PackageStatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/PackageStatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IForegroundChangedCallback" */
} // .end annotation
/* # virtual methods */
public abstract java.lang.String getCallbackName ( ) {
} // .end method
public abstract void onForegroundChanged ( Integer p0, Integer p1, java.lang.String p2 ) {
} // .end method
