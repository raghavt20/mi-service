.class public final Lcom/miui/server/migard/MiGardService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiGardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/MiGardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/migard/MiGardService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 85
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 86
    new-instance v0, Lcom/miui/server/migard/MiGardService;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/miui/server/migard/MiGardService;-><init>(Landroid/content/Context;Lcom/miui/server/migard/MiGardService-IA;)V

    iput-object v0, p0, Lcom/miui/server/migard/MiGardService$Lifecycle;->mService:Lcom/miui/server/migard/MiGardService;

    .line 87
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 91
    const-string v0, "migard"

    iget-object v1, p0, Lcom/miui/server/migard/MiGardService$Lifecycle;->mService:Lcom/miui/server/migard/MiGardService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/migard/MiGardService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 92
    return-void
.end method
