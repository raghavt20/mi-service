.class Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
.super Ljava/lang/Object;
.source "GameMemoryCleanerConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GameConfig"
.end annotation


# instance fields
.field mActionConfigs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;",
            ">;"
        }
    .end annotation
.end field

.field mCleanFirstDelay:I

.field mCleanPeriod:I

.field mReclaimMemoryPercent:I

.field final synthetic this$0:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;


# direct methods
.method constructor <init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V
    .locals 0

    .line 333
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->this$0:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 334
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mActionConfigs:Ljava/util/List;

    .line 335
    const/16 p1, 0x64

    iput p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I

    .line 336
    const/4 p1, 0x0

    iput p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I

    .line 337
    const/4 p1, -0x1

    iput p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I

    .line 338
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 343
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "clean memory percent: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mActionConfigs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;

    .line 345
    .local v3, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 346
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    .end local v3    # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    goto :goto_0

    .line 348
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    const-string v1, "first clean delay: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    const-string v1, "clean periodic: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
