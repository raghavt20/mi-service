.class Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;
.super Ljava/lang/Object;
.source "GameMemoryCleaner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/memory/GameMemoryCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DelayedCleanRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/migard/memory/GameMemoryCleaner;


# direct methods
.method private constructor <init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;->this$0:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 93
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->-$$Nest$sfgetTAG()Ljava/lang/String;

    move-result-object v0

    const-string v1, "reclaim memory for game(periodic)."

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;->this$0:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-static {v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->-$$Nest$fgetmCurrentPackage(Lcom/miui/server/migard/memory/GameMemoryCleaner;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->-$$Nest$mreclaimMemoryForGameIfNeed(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCleanPeriod()I

    move-result v0

    if-lez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;->this$0:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-static {v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->-$$Nest$mperiodicClean(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V

    .line 98
    :cond_0
    return-void
.end method
