.class Lcom/miui/server/migard/memory/GameMemInfo$1;
.super Ljava/lang/Object;
.source "GameMemInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/memory/GameMemInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/miui/server/migard/memory/GameMemInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/miui/server/migard/memory/GameMemInfo;
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .line 39
    new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>()V

    .line 40
    .local v0, "meminfo":Lcom/miui/server/migard/memory/GameMemInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I

    .line 43
    iget-object v1, v0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readLongArray([J)V

    .line 44
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 35
    invoke-virtual {p0, p1}, Lcom/miui/server/migard/memory/GameMemInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/miui/server/migard/memory/GameMemInfo;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/miui/server/migard/memory/GameMemInfo;
    .locals 1
    .param p1, "size"    # I

    .line 50
    new-array v0, p1, [Lcom/miui/server/migard/memory/GameMemInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 35
    invoke-virtual {p0, p1}, Lcom/miui/server/migard/memory/GameMemInfo$1;->newArray(I)[Lcom/miui/server/migard/memory/GameMemInfo;

    move-result-object p1

    return-object p1
.end method
