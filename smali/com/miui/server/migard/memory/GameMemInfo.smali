.class Lcom/miui/server/migard/memory/GameMemInfo;
.super Ljava/lang/Object;
.source "GameMemInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/miui/server/migard/memory/GameMemInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final INVALID_SIZE:J = -0x1L

.field private static final MAX_CNT:I = 0x14

.field public static final POLICY_AVG:I = 0x1

.field public static final POLICY_MAX:I = 0x2

.field public static final POLICY_PRE:I


# instance fields
.field hasUpdated:Z

.field lastIndex:I

.field pkgName:Ljava/lang/String;

.field prevSize:J

.field sizeHistory:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo$1;

    invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemInfo$1;-><init>()V

    sput-object v0, Lcom/miui/server/migard/memory/GameMemInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/16 v0, 0x14

    new-array v1, v0, [J

    iput-object v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    .line 26
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    .line 27
    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J

    .line 28
    const/4 v3, 0x0

    iput v3, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I

    .line 29
    iput-boolean v3, p0, Lcom/miui/server/migard/memory/GameMemInfo;->hasUpdated:Z

    .line 30
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 31
    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    aput-wide v1, v4, v3

    .line 30
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 33
    .end local v3    # "i":I
    :cond_0
    return-void
.end method

.method public static marshall(Lcom/miui/server/migard/memory/GameMemInfo;)[B
    .locals 2
    .param p0, "parceable"    # Lcom/miui/server/migard/memory/GameMemInfo;

    .line 128
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 129
    .local v0, "parcel":Landroid/os/Parcel;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 130
    invoke-virtual {p0, v0, v1}, Lcom/miui/server/migard/memory/GameMemInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 131
    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    move-result-object v1

    .line 132
    .local v1, "bytes":[B
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 133
    return-object v1
.end method

.method public static unmarshall([B)Lcom/miui/server/migard/memory/GameMemInfo;
    .locals 4
    .param p0, "bytes"    # [B

    .line 137
    array-length v0, p0

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    return-object v0

    .line 139
    :cond_0
    new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>()V

    .line 140
    .local v0, "meminfo":Lcom/miui/server/migard/memory/GameMemInfo;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 141
    .local v1, "parcel":Landroid/os/Parcel;
    array-length v2, p0

    const/4 v3, 0x0

    invoke-virtual {v1, p0, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 142
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 143
    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 144
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 145
    return-object v0
.end method


# virtual methods
.method public addHistoryItem(J)V
    .locals 4
    .param p1, "size"    # J

    .line 92
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    iget v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I

    aget-wide v2, v0, v1

    cmp-long v2, p1, v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 95
    :cond_0
    const/4 v2, 0x1

    add-int/2addr v1, v2

    rem-int/lit8 v1, v1, 0x14

    .line 96
    .local v1, "idx":I
    iput-wide p1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J

    .line 97
    aput-wide p1, v0, v1

    .line 98
    iput v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I

    .line 99
    iput-boolean v2, p0, Lcom/miui/server/migard/memory/GameMemInfo;->hasUpdated:Z

    .line 100
    return-void

    .line 93
    .end local v1    # "idx":I
    :cond_1
    :goto_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public getPredSize(I)J
    .locals 14
    .param p1, "policy"    # I

    .line 105
    const-wide/16 v0, -0x1

    if-ltz p1, :cond_6

    const/4 v2, 0x2

    if-le p1, v2, :cond_0

    goto :goto_1

    .line 107
    :cond_0
    if-nez p1, :cond_1

    .line 108
    iget-wide v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J

    return-wide v0

    .line 109
    :cond_1
    const-wide/16 v3, 0x0

    .line 110
    .local v3, "max":J
    const-wide/16 v5, 0x0

    .line 111
    .local v5, "sum":J
    const/4 v7, 0x0

    .line 112
    .local v7, "cnt":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/16 v9, 0x14

    if-ge v8, v9, :cond_3

    .line 113
    iget-object v9, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    aget-wide v10, v9, v8

    const-wide/16 v12, 0x0

    cmp-long v12, v10, v12

    if-lez v12, :cond_2

    .line 114
    add-int/lit8 v7, v7, 0x1

    .line 115
    add-long/2addr v5, v10

    .line 116
    cmp-long v10, v10, v3

    if-lez v10, :cond_2

    .line 117
    aget-wide v3, v9, v8

    .line 112
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 120
    .end local v8    # "i":I
    :cond_3
    if-nez v7, :cond_4

    .line 121
    return-wide v0

    .line 122
    :cond_4
    if-ne p1, v2, :cond_5

    .line 123
    return-wide v3

    .line 124
    :cond_5
    int-to-long v0, v7

    div-long v0, v5, v0

    return-wide v0

    .line 106
    .end local v3    # "max":J
    .end local v5    # "sum":J
    .end local v7    # "cnt":I
    :cond_6
    :goto_1
    return-wide v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I

    .line 72
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readLongArray([J)V

    .line 73
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string/jumbo v1, "{package:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    const-string v1, " history_size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0x14

    if-ge v1, v2, :cond_1

    .line 82
    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    aget-wide v2, v2, v1

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 83
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 84
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "i":I
    :cond_1
    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 62
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-wide v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 64
    iget v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->sizeHistory:[J

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 66
    return-void
.end method
