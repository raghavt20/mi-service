.class public Lcom/miui/server/migard/memory/GameMemoryCleaner;
.super Ljava/lang/Object;
.source "GameMemoryCleaner.java"

# interfaces
.implements Lcom/miui/server/migard/IMiGardFeature;
.implements Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;
.implements Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;
.implements Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final DEFAULT_MEMCG_PATH_R:Ljava/lang/String; = "/sys/fs/cgroup/memory"

.field private static final DEFAULT_MEMCG_PATH_S:Ljava/lang/String; = "/dev/memcg"

.field private static final GAME_MEMCG_PATH_R:Ljava/lang/String; = "/sys/fs/cgroup/memory/game"

.field private static final GAME_MEMCG_PATH_S:Ljava/lang/String; = "/dev/memcg/game"

.field private static final GAME_MEMORY_PREDICT_POLICY:I

.field private static final GAME_MEM_SAVE_PATH:Ljava/lang/String; = "/data/system/migard/game_mem/"

.field private static final IS_MEMORY_CLEAN_ENABLED:Z

.field private static final IS_MIUI_LITE:Z

.field private static final MEMCG_CURR_MEMUSAGE_NODE:Ljava/lang/String; = "memory.memsw.usage_in_bytes"

.field private static final MEMCG_MAX_MEMUSAGE_NODE:Ljava/lang/String; = "memory.memsw.max_usage_in_bytes"

.field private static final MEMCG_PROCS_NODE:Ljava/lang/String; = "cgroup.procs"

.field private static final TAG:Ljava/lang/String;

.field private static final UID_PREFIX:Ljava/lang/String; = "uid_"


# instance fields
.field private hasConfiged:Z

.field private mContext:Landroid/content/Context;

.field private mCurrentAppUid:I

.field private mCurrentGame:Ljava/lang/String;

.field private mCurrentPackage:Ljava/lang/String;

.field private mDelayedCleanRunnable:Ljava/lang/Runnable;

.field private mFeatureUsed:Z

.field private mGameMemCgroupPath:Ljava/lang/String;

.field private mGameMemInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/migard/memory/GameMemInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

.field private mHandler:Landroid/os/Handler;

.field private mMemCgroupPath:Ljava/lang/String;

.field private mServiceThread:Lcom/android/server/ServiceThread;

.field private mVpnPackage:Ljava/lang/String;


# direct methods
.method public static synthetic $r8$lambda$9bxCXNEUmR3ic9q-W3veVZveV6Q(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onScreenOff$4()V

    return-void
.end method

.method public static synthetic $r8$lambda$E_eNQL8jbo4Y7H5O2WQgcBUnbaM(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$reclaimBackgroundMemory$0()V

    return-void
.end method

.method public static synthetic $r8$lambda$NVZCgRjE4_9WvtG64_8PtqTg8bc(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$addGameList$1(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic $r8$lambda$QnCRf_vicmPzXKGzwgYVGZdp5pY(Lcom/miui/server/migard/memory/GameMemoryCleaner;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onUidGone$5(I)V

    return-void
.end method

.method public static synthetic $r8$lambda$Ue-nPkQsE7-Uy0CkF_dqMWA8c68(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onUserPresent$3()V

    return-void
.end method

.method public static synthetic $r8$lambda$hyq6jmvb0iar4_j200VXJm62P0E(Lcom/miui/server/migard/memory/GameMemoryCleaner;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onProcessStart$6(IILjava/lang/String;)V

    return-void
.end method

.method public static synthetic $r8$lambda$noEGEni3kXBXQxqg8LV4Omnxna4(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onForegroundChanged$2(Ljava/lang/String;I)V

    return-void
.end method

.method static bridge synthetic -$$Nest$fgetmCurrentPackage(Lcom/miui/server/migard/memory/GameMemoryCleaner;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mperiodicClean(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->periodicClean()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mreclaimMemoryForGameIfNeed(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->reclaimMemoryForGameIfNeed(Ljava/lang/String;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 53
    const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    .line 57
    sget-boolean v0, Lmiui/os/Build;->IS_MIUI_LITE_VERSION:Z

    sput-boolean v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MIUI_LITE:Z

    .line 58
    nop

    .line 59
    const-string v0, "persist.sys.migard.mem_clean_enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MEMORY_CLEAN_ENABLED:Z

    .line 60
    nop

    .line 61
    const-string v0, "persist.sys.migard.game_predict_policy"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->GAME_MEMORY_PREDICT_POLICY:I

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/am/GameMemoryReclaimer;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "gameMemoryReclaimer"    # Lcom/android/server/am/GameMemoryReclaimer;

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mContext:Landroid/content/Context;

    .line 74
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mFeatureUsed:Z

    .line 76
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mVpnPackage:Ljava/lang/String;

    .line 77
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    .line 78
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentGame:Ljava/lang/String;

    .line 79
    iput v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentAppUid:I

    .line 83
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mMemCgroupPath:Ljava/lang/String;

    .line 84
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    .line 85
    iput-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z

    .line 87
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mDelayedCleanRunnable:Ljava/lang/Runnable;

    .line 102
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mContext:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    .line 104
    new-instance v0, Lcom/android/server/ServiceThread;

    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    invoke-direct {v0, v2, v1, v1}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mServiceThread:Lcom/android/server/ServiceThread;

    .line 105
    invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V

    .line 106
    new-instance v0, Landroid/os/Handler;

    iget-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mServiceThread:Lcom/android/server/ServiceThread;

    invoke-virtual {v3}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    .line 108
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->initMemCgroupInfo()Z

    move-result v0

    .line 109
    .local v0, "isCgroupSupportd":Z
    if-nez v0, :cond_0

    .line 110
    const-string v3, "mem cgroup is not supported"

    invoke-static {v2, v3}, Lcom/miui/server/migard/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    if-eqz v0, :cond_2

    sget-boolean v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MIUI_LITE:Z

    if-nez v2, :cond_1

    sget-boolean v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MEMORY_CLEAN_ENABLED:Z

    if-eqz v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    iput-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mFeatureUsed:Z

    .line 113
    if-eqz v1, :cond_3

    .line 114
    invoke-static {}, Lcom/miui/server/migard/PackageStatusManager;->getInstance()Lcom/miui/server/migard/PackageStatusManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/miui/server/migard/PackageStatusManager;->registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V

    .line 115
    invoke-static {}, Lcom/miui/server/migard/ScreenStatusManager;->getInstance()Lcom/miui/server/migard/ScreenStatusManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/miui/server/migard/ScreenStatusManager;->registerCallback(Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;)V

    .line 116
    invoke-static {}, Lcom/miui/server/migard/UidStateManager;->getInstance()Lcom/miui/server/migard/UidStateManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/miui/server/migard/UidStateManager;->registerCallback(Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;)V

    .line 119
    :cond_3
    new-instance v1, Ljava/util/HashMap;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    .line 121
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->configFromFile()V

    .line 122
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getGameList()Ljava/util/List;

    move-result-object v1

    .line 123
    .local v1, "gameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 124
    .local v3, "g":Ljava/lang/String;
    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 125
    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-direct {p0, v3}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->generateGameMemInfo(Ljava/lang/String;)Lcom/miui/server/migard/memory/GameMemInfo;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    .end local v3    # "g":Ljava/lang/String;
    :cond_4
    goto :goto_0

    .line 127
    :cond_5
    return-void
.end method

.method private checkAndApplyConfig(Z)Z
    .locals 7
    .param p1, "onForeground"    # Z

    .line 239
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 240
    iput-boolean v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z

    .line 242
    :cond_0
    iget-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z

    if-eqz v1, :cond_1

    .line 243
    return v1

    .line 246
    :cond_1
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getActionConfigs()Ljava/util/List;

    move-result-object v1

    .line 247
    .local v1, "configs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;>;"
    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    goto :goto_3

    .line 251
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v0, v2, :cond_6

    .line 252
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;

    .line 253
    .local v2, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    instance-of v4, v2, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    if-eqz v4, :cond_4

    .line 254
    move-object v4, v2

    check-cast v4, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    .line 255
    .local v4, "kcfg":Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getUserProtectList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V

    .line 256
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getKillerCommonWhilteList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V

    .line 257
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getPowerWhiteList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V

    .line 258
    iget-object v5, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mVpnPackage:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 259
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 260
    .local v5, "vpn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mVpnPackage:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V

    .line 263
    .end local v5    # "vpn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    iget-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-virtual {v3, v4}, Lcom/android/server/am/GameMemoryReclaimer;->addGameProcessKiller(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V

    .end local v4    # "kcfg":Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;
    goto :goto_1

    .line 264
    :cond_4
    instance-of v4, v2, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    if-eqz v4, :cond_5

    .line 265
    move-object v4, v2

    check-cast v4, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    .line 266
    .local v4, "ccfg":Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCompactorCommonWhiteList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->addWhiteList(Ljava/util/List;Z)V

    .line 267
    iget-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-virtual {v3, v4}, Lcom/android/server/am/GameMemoryReclaimer;->addGameProcessCompactor(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V

    goto :goto_2

    .line 264
    .end local v4    # "ccfg":Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;
    :cond_5
    :goto_1
    nop

    .line 251
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    .end local v0    # "i":I
    .end local v2    # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    :cond_6
    iput-boolean v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z

    .line 271
    return v3

    .line 248
    :cond_7
    :goto_3
    iput-boolean v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z

    .line 249
    return v0
.end method

.method private checkGameCgroup(I)Z
    .locals 5
    .param p1, "uid"    # I

    .line 338
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "uid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, "uidPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 340
    .local v1, "gameCgDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    .line 341
    return v3

    .line 342
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 343
    invoke-virtual {v1, v3, v4}, Ljava/io/File;->setReadable(ZZ)Z

    .line 344
    invoke-virtual {v1, v3, v4}, Ljava/io/File;->setWritable(ZZ)Z

    .line 345
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    const-string v4, "create game cgroup succeed"

    invoke-static {v2, v4}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    return v3

    .line 348
    :cond_1
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    const-string v3, "create game cgroup failed"

    invoke-static {v2, v3}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    return v4
.end method

.method private currentGameMemSize()J
    .locals 8

    .line 222
    const-wide/16 v0, -0x1

    .line 224
    .local v0, "memSize":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v4, "uid_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentAppUid:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 225
    .local v2, "uidPath":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "memory.memsw.usage_in_bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/miui/server/migard/utils/FileUtils;->readFromSys(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 226
    .local v3, "content":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 228
    :try_start_0
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v0, v4

    .line 232
    goto :goto_0

    .line 229
    :catch_0
    move-exception v4

    .line 230
    .local v4, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v0, -0x1

    .line 231
    sget-object v5, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read game size failed, err:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    :goto_0
    return-wide v0
.end method

.method private delayClean(Z)V
    .locals 4
    .param p1, "periodic"    # Z

    .line 301
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->disableDelayedClean()V

    .line 302
    new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable-IA;)V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mDelayedCleanRunnable:Ljava/lang/Runnable;

    .line 303
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    .line 304
    if-eqz p1, :cond_0

    .line 305
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCleanPeriod()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    goto :goto_0

    .line 306
    :cond_0
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCleanFirstDelay()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    .line 303
    :goto_0
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 307
    return-void
.end method

.method private disableDelayedClean()V
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mDelayedCleanRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mDelayedCleanRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mDelayedCleanRunnable:Ljava/lang/Runnable;

    .line 323
    :cond_0
    return-void
.end method

.method private generateGameMemInfo(Ljava/lang/String;)Lcom/miui/server/migard/memory/GameMemInfo;
    .locals 8
    .param p1, "pkg"    # Ljava/lang/String;

    .line 148
    const-string v0, "close file failed, "

    const/4 v1, 0x0

    .line 149
    .local v1, "info":Lcom/miui/server/migard/memory/GameMemInfo;
    const/4 v2, 0x0

    .line 150
    .local v2, "in":Ljava/io/FileInputStream;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/data/system/migard/game_mem/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 151
    .local v3, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 152
    .local v4, "f":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 153
    new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-direct {v0, p1}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 155
    :cond_0
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v2, v5

    .line 156
    invoke-static {v2}, Lcom/miui/server/migard/utils/FileUtils;->readFileByBytes(Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/miui/server/migard/memory/GameMemInfo;->unmarshall([B)Lcom/miui/server/migard/memory/GameMemInfo;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v5

    .line 161
    nop

    .line 163
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 166
    :goto_0
    goto :goto_1

    .line 164
    :catch_0
    move-exception v5

    .line 165
    .local v5, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    invoke-static {v6, v0, v5}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v5    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 161
    :catchall_0
    move-exception v5

    goto :goto_2

    .line 157
    :catch_1
    move-exception v5

    .line 158
    .restart local v5    # "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 159
    :try_start_2
    sget-object v6, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    const-string v7, "read game info failed, "

    invoke-static {v6, v7, v5}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 161
    .end local v5    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_1

    .line 163
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 169
    :cond_1
    :goto_1
    if-nez v1, :cond_2

    .line 170
    new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-direct {v0, p1}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 171
    :cond_2
    return-object v1

    .line 161
    :goto_2
    if-eqz v2, :cond_3

    .line 163
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 166
    goto :goto_3

    .line 164
    :catch_2
    move-exception v6

    .line 165
    .local v6, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    invoke-static {v7, v0, v6}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 168
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_3
    throw v5
.end method

.method private initMemCgroupInfo()Z
    .locals 4

    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "ret":Z
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/fs/cgroup/memory/uid_1000"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/io/File;

    const-string v2, "/dev/memcg/uid_1000"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 135
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/fs/cgroup/memory"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/File;

    const-string v3, "/sys/fs/cgroup/memory/game"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    iput-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mMemCgroupPath:Ljava/lang/String;

    .line 137
    iput-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    .line 138
    const/4 v0, 0x1

    goto :goto_0

    .line 139
    :cond_1
    new-instance v1, Ljava/io/File;

    const-string v2, "/dev/memcg"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/io/File;

    const-string v3, "/dev/memcg/game"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 140
    iput-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mMemCgroupPath:Ljava/lang/String;

    .line 141
    iput-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    .line 142
    const/4 v0, 0x1

    .line 144
    :cond_2
    :goto_0
    return v0

    .line 133
    :cond_3
    :goto_1
    return v0
.end method

.method private synthetic lambda$addGameList$1(Ljava/util/List;)V
    .locals 5
    .param p1, "gameList"    # Ljava/util/List;

    .line 374
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addGameList(Ljava/util/List;Z)V

    .line 375
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 376
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 377
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 378
    .local v1, "pkg":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 379
    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-direct {p0, v2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->saveGameMemInfo(Lcom/miui/server/migard/memory/GameMemInfo;)V

    .line 380
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 382
    .end local v1    # "pkg":Ljava/lang/String;
    :cond_0
    goto :goto_0

    .line 383
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 384
    .local v2, "g":Ljava/lang/String;
    iget-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 385
    iget-object v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-direct {p0, v2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->generateGameMemInfo(Ljava/lang/String;)Lcom/miui/server/migard/memory/GameMemInfo;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    .end local v2    # "g":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 387
    :cond_3
    return-void
.end method

.method private synthetic lambda$onForegroundChanged$2(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "uid"    # I

    .line 399
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 402
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->disableDelayedClean()V

    .line 403
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentGame:Ljava/lang/String;

    .line 405
    :cond_1
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    .line 406
    iput p2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentAppUid:I

    .line 407
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 408
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentGame:Ljava/lang/String;

    .line 409
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentGame:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->applyGameConfig(Ljava/lang/String;)V

    .line 410
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onGameForeground()V

    goto :goto_0

    .line 412
    :cond_2
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentGame:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->applyGameConfig(Ljava/lang/String;)V

    .line 415
    :cond_3
    :goto_0
    return-void
.end method

.method private synthetic lambda$onProcessStart$6(IILjava/lang/String;)V
    .locals 0
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "pkg"    # Ljava/lang/String;

    .line 449
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onGameProcessStart(IILjava/lang/String;)V

    .line 450
    return-void
.end method

.method private synthetic lambda$onScreenOff$4()V
    .locals 2

    .line 434
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->saveGameMemInfo()V

    .line 435
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->disableDelayedClean()V

    .line 437
    :cond_0
    return-void
.end method

.method private synthetic lambda$onUidGone$5(I)V
    .locals 0
    .param p1, "uid"    # I

    .line 443
    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->recordGameMemInfo(I)V

    .line 444
    return-void
.end method

.method private synthetic lambda$onUserPresent$3()V
    .locals 2

    .line 426
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onGameForeground()V

    .line 428
    :cond_0
    return-void
.end method

.method private synthetic lambda$reclaimBackgroundMemory$0()V
    .locals 2

    .line 276
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->checkAndApplyConfig(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    return-void

    .line 279
    :cond_0
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    const-string v1, "reclaim memory for game(scene)."

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->reclaimMemoryForGameIfNeed(Ljava/lang/String;)V

    .line 282
    :cond_1
    return-void
.end method

.method private onGameForeground()V
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/server/am/GameMemoryReclaimer;->notifyGameForeground(Ljava/lang/String;)V

    .line 287
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->checkAndApplyConfig(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 288
    return-void

    .line 289
    :cond_0
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->hasForegroundClean()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 290
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->delayClean(Z)V

    .line 291
    :cond_1
    return-void
.end method

.method private onGameProcessStart(IILjava/lang/String;)V
    .locals 3
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "pkg"    # Ljava/lang/String;

    .line 330
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->checkGameCgroup(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 331
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "monitor game pid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "uid_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, "uidPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "cgroup.procs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/miui/server/migard/utils/FileUtils;->writeToSys(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    .end local v0    # "uidPath":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private periodicClean()V
    .locals 3

    .line 310
    iget-boolean v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mFeatureUsed:Z

    if-nez v0, :cond_0

    .line 311
    return-void

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentPackage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", plan periodic clean..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->delayClean(Z)V

    .line 316
    :cond_1
    return-void
.end method

.method private predictGameMemory(Ljava/lang/String;)J
    .locals 6
    .param p1, "game"    # Ljava/lang/String;

    .line 190
    const-wide/16 v0, -0x1

    .line 191
    .local v0, "pred":J
    const-string v2, "predictGameMemory"

    const-wide/32 v3, 0x80000

    invoke-static {v3, v4, v2}, Landroid/os/Trace;->traceBegin(JLjava/lang/String;)V

    .line 192
    sget v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->GAME_MEMORY_PREDICT_POLICY:I

    if-gez v2, :cond_0

    .line 193
    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    nop

    .line 194
    .local v2, "policy":I
    :goto_0
    const/4 v5, 0x2

    if-le v2, v5, :cond_1

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    move v2, v5

    .line 195
    iget-object v5, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 196
    iget-object v5, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-virtual {v5, v2}, Lcom/miui/server/migard/memory/GameMemInfo;->getPredSize(I)J

    move-result-wide v0

    .line 197
    :cond_2
    invoke-static {v3, v4}, Landroid/os/Trace;->traceEnd(J)V

    .line 198
    return-wide v0
.end method

.method private reclaimMemoryForGameIfNeed(Ljava/lang/String;)V
    .locals 11
    .param p1, "game"    # Ljava/lang/String;

    .line 202
    invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->predictGameMemory(Ljava/lang/String;)J

    move-result-wide v0

    .line 203
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getReclaimMemoryPercent()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    .line 204
    .local v0, "pred":J
    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 205
    return-void

    .line 206
    :cond_0
    invoke-static {}, Landroid/os/Process;->getFreeMemory()J

    move-result-wide v4

    .line 207
    .local v4, "free":J
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->currentGameMemSize()J

    move-result-wide v6

    .line 208
    .local v6, "used":J
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_1

    .line 209
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    const-string v3, "read current memory usage failed, abort reclaim."

    invoke-static {v2, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    return-void

    .line 212
    :cond_1
    sget-object v8, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "device free mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", pred game mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", game used mem: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    sub-long v9, v0, v6

    sub-long/2addr v9, v4

    .line 214
    .local v9, "need":J
    cmp-long v2, v9, v2

    if-gtz v2, :cond_2

    .line 215
    const-string v2, "enough mem for game, dont need kill background apps"

    invoke-static {v8, v2}, Lcom/miui/server/migard/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    return-void

    .line 218
    :cond_2
    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-virtual {v2, v9, v10}, Lcom/android/server/am/GameMemoryReclaimer;->reclaimBackground(J)V

    .line 219
    return-void
.end method

.method private recordGameMemInfo(I)V
    .locals 9
    .param p1, "uid"    # I

    .line 354
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/miui/server/migard/utils/PackageUtils;->getPackageNameByUid(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 355
    .local v0, "game":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    return-void

    .line 357
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemCgroupPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "uid_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 358
    .local v1, "uidPath":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "memory.memsw.max_usage_in_bytes"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/server/migard/utils/FileUtils;->readFromSys(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 359
    .local v2, "content":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 361
    :try_start_0
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    .local v3, "memSize":J
    goto :goto_0

    .line 362
    .end local v3    # "memSize":J
    :catch_0
    move-exception v3

    .line 363
    .local v3, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v4, -0x1

    .line 364
    .local v4, "memSize":J
    sget-object v6, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "read game size failed, err:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v3, v4

    .line 366
    .end local v4    # "memSize":J
    .local v3, "memSize":J
    :goto_0
    sget-object v5, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "game mem size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_1

    .line 368
    iget-object v5, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/server/migard/memory/GameMemInfo;

    invoke-virtual {v5, v3, v4}, Lcom/miui/server/migard/memory/GameMemInfo;->addHistoryItem(J)V

    .line 370
    .end local v3    # "memSize":J
    :cond_1
    return-void
.end method

.method private saveGameMemInfo()V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/migard/memory/GameMemInfo;

    .line 176
    .local v1, "info":Lcom/miui/server/migard/memory/GameMemInfo;
    invoke-direct {p0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->saveGameMemInfo(Lcom/miui/server/migard/memory/GameMemInfo;)V

    .line 177
    .end local v1    # "info":Lcom/miui/server/migard/memory/GameMemInfo;
    goto :goto_0

    .line 178
    :cond_0
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    const-string v1, "save game memory info succeed"

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method private saveGameMemInfo(Lcom/miui/server/migard/memory/GameMemInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/miui/server/migard/memory/GameMemInfo;

    .line 182
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/miui/server/migard/memory/GameMemInfo;->hasUpdated:Z

    if-eqz v0, :cond_0

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/system/migard/game_mem/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 184
    invoke-static {p1}, Lcom/miui/server/migard/memory/GameMemInfo;->marshall(Lcom/miui/server/migard/memory/GameMemInfo;)[B

    move-result-object v1

    .line 183
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/miui/server/migard/utils/FileUtils;->writeFileByBytes(Ljava/lang/String;[BZ)V

    .line 185
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "save game memory info succeed, pkg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/miui/server/migard/memory/GameMemInfo;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    return-void
.end method


# virtual methods
.method public addGameList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 373
    .local p1, "gameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda1;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 388
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 466
    const-string v0, "game memory cleaner info: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "is miui lite: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MIUI_LITE:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 468
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "is enabled from sys prop: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MEMORY_CLEAN_ENABLED:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "game memory predict policy: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/miui/server/migard/memory/GameMemoryCleaner;->GAME_MEMORY_PREDICT_POLICY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 470
    const-string v0, "game memory records: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 471
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mGameMemInfos:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/server/migard/memory/GameMemInfo;

    .line 472
    .local v1, "info":Lcom/miui/server/migard/memory/GameMemInfo;
    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 473
    .end local v1    # "info":Lcom/miui/server/migard/memory/GameMemInfo;
    goto :goto_0

    .line 474
    :cond_0
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->dump(Ljava/io/PrintWriter;)V

    .line 475
    return-void
.end method

.method public getCallbackName()Ljava/lang/String;
    .locals 1

    .line 420
    const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onForegroundChanged(IILjava/lang/String;)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 396
    if-eqz p3, :cond_1

    if-gtz p2, :cond_0

    goto :goto_0

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda4;

    invoke-direct {v1, p0, p3, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda4;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 416
    return-void

    .line 397
    :cond_1
    :goto_0
    return-void
.end method

.method public onProcessKilled(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .line 327
    return-void
.end method

.method public onProcessStart(IILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "pkg"    # Ljava/lang/String;
    .param p4, "caller"    # Ljava/lang/String;

    .line 448
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;IILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 451
    return-void
.end method

.method public onScreenOff()V
    .locals 2

    .line 433
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda5;

    invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda5;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 438
    return-void
.end method

.method public onShellCommand(Ljava/lang/String;Ljava/lang/String;Ljava/io/PrintWriter;)Z
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "pw"    # Ljava/io/PrintWriter;

    .line 461
    const/4 v0, 0x0

    return v0
.end method

.method public onShellHelp(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 455
    const-string v0, "GameMemoryCleaner commands:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 456
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 457
    return-void
.end method

.method public onUidGone(I)V
    .locals 2
    .param p1, "uid"    # I

    .line 442
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda2;

    invoke-direct {v1, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 445
    return-void
.end method

.method public onUserPresent()V
    .locals 2

    .line 425
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda6;

    invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda6;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 429
    return-void
.end method

.method public onVpnConnected(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "connected"    # Z

    .line 294
    if-eqz p2, :cond_0

    .line 295
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mVpnPackage:Ljava/lang/String;

    goto :goto_0

    .line 297
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mVpnPackage:Ljava/lang/String;

    .line 298
    :goto_0
    return-void
.end method

.method public reclaimBackgroundMemory()V
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda0;

    invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 283
    return-void
.end method

.method public runOnThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .line 391
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 392
    return-void
.end method
