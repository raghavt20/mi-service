public class com.miui.server.migard.memory.GameMemoryCleanerConfig {
	 /* .source "GameMemoryCleanerConfig.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String CLEAN_FIRST_DELAY;
private static final java.lang.String CLEAN_PERIOD;
private static final java.lang.String COMPACTOR_CONFIGS;
private static final java.lang.String CONFIG_PATH;
private static final Integer DEFAULT_CLEAN_FIRST_DELAY;
private static final Integer DEFAULT_CLEAN_PERIOD;
private static final Integer DEFAULT_RECLAIM_MEMORY_PCT;
private static final java.lang.String GAME_LIST;
private static final java.lang.String KILLER_CONFIGS;
private static final java.lang.String RECLAIM_MEMORY_PCT;
private static final java.lang.String TAG;
private static com.miui.server.migard.memory.GameMemoryCleanerConfig sInstance;
/* # instance fields */
private com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig mActiveConfig;
private com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig mCommonConfig;
private java.util.List mCompactorCommonWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.Map mConfigMap;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mGameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mKillerCommonWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mPowerWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.List mUserProtectList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.migard.memory.GameMemoryCleanerConfig ( ) {
/* .locals 1 */
/* .line 30 */
/* const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 55 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig; */
/* invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;-><init>()V */
return;
} // .end method
private com.miui.server.migard.memory.GameMemoryCleanerConfig ( ) {
/* .locals 2 */
/* .line 61 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 51 */
int v0 = 0; // const/4 v0, 0x0
this.mCommonConfig = v0;
/* .line 52 */
/* new-instance v1, Ljava/util/HashMap; */
/* invoke-direct {v1}, Ljava/util/HashMap;-><init>()V */
this.mConfigMap = v1;
/* .line 53 */
this.mActiveConfig = v0;
/* .line 62 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mGameList = v0;
/* .line 63 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mUserProtectList = v0;
/* .line 64 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mKillerCommonWhiteList = v0;
/* .line 65 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCompactorCommonWhiteList = v0;
/* .line 66 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mPowerWhiteList = v0;
/* .line 67 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->initMMSWhiteList()V */
/* .line 68 */
return;
} // .end method
public static com.miui.server.migard.memory.GameMemoryCleanerConfig getInstance ( ) {
/* .locals 1 */
/* .line 58 */
v0 = com.miui.server.migard.memory.GameMemoryCleanerConfig.sInstance;
} // .end method
private void initMMSWhiteList ( ) {
/* .locals 2 */
/* .line 71 */
v0 = this.mPowerWhiteList;
final String v1 = "com.miui.home"; // const-string v1, "com.miui.home"
/* .line 72 */
v0 = this.mPowerWhiteList;
final String v1 = "com.android.systemui"; // const-string v1, "com.android.systemui"
/* .line 73 */
v0 = this.mPowerWhiteList;
final String v1 = "com.xiaomi.xmsf"; // const-string v1, "com.xiaomi.xmsf"
/* .line 74 */
v0 = this.mPowerWhiteList;
final String v1 = "com.miui.securitycore"; // const-string v1, "com.miui.securitycore"
/* .line 76 */
v0 = this.mPowerWhiteList;
final String v1 = "com.miui.hybrid"; // const-string v1, "com.miui.hybrid"
/* .line 77 */
v0 = this.mPowerWhiteList;
final String v1 = "com.android.providers.media.module"; // const-string v1, "com.android.providers.media.module"
/* .line 78 */
v0 = this.mPowerWhiteList;
final String v1 = "com.google.android.providers.media.module"; // const-string v1, "com.google.android.providers.media.module"
/* .line 79 */
v0 = this.mPowerWhiteList;
final String v1 = "com.miui.mishare.connectivity"; // const-string v1, "com.miui.mishare.connectivity"
/* .line 80 */
v0 = this.mPowerWhiteList;
final String v1 = "com.lbe.security.miui"; // const-string v1, "com.lbe.security.miui"
/* .line 81 */
v0 = this.mPowerWhiteList;
final String v1 = "com.xiaomi.metoknlp"; // const-string v1, "com.xiaomi.metoknlp"
/* .line 82 */
v0 = this.mPowerWhiteList;
final String v1 = "com.miui.cloudbackup"; // const-string v1, "com.miui.cloudbackup"
/* .line 83 */
return;
} // .end method
private void parseCompactorConfigs ( org.json.JSONArray p0, com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig p1 ) {
/* .locals 5 */
/* .param p1, "array" # Lorg/json/JSONArray; */
/* .param p2, "config" # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 150 */
/* if-nez p1, :cond_0 */
/* .line 151 */
return;
/* .line 152 */
} // :cond_0
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* .line 153 */
/* .local v0, "N":I */
/* if-nez v0, :cond_1 */
/* .line 154 */
return;
/* .line 156 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_3 */
/* .line 157 */
try { // :try_start_0
(( org.json.JSONArray ) p1 ).optJSONObject ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* .line 158 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
/* if-nez v2, :cond_2 */
/* .line 159 */
/* .line 160 */
} // :cond_2
/* new-instance v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig; */
/* invoke-direct {v3}, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;-><init>()V */
/* .line 161 */
/* .local v3, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* .line 162 */
v4 = this.mActionConfigs;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 156 */
} // .end local v3 # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 164 */
} // .end local v1 # "i":I
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
/* :catch_0 */
/* move-exception v1 */
/* .line 165 */
/* .local v1, "e":Lorg/json/JSONException; */
/* throw v1 */
/* .line 166 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :cond_3
/* nop */
/* .line 167 */
return;
} // .end method
private Boolean parseConfig ( java.lang.String p0, com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig p1, Boolean p2 ) {
/* .locals 6 */
/* .param p1, "jsonString" # Ljava/lang/String; */
/* .param p2, "config" # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
/* .param p3, "cloud" # Z */
/* .line 86 */
final String v0 = "game-list"; // const-string v0, "game-list"
int v1 = 1; // const/4 v1, 0x1
/* .line 91 */
/* .local v1, "ret":Z */
try { // :try_start_0
/* new-instance v2, Lorg/json/JSONObject; */
/* invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V */
/* .line 92 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
/* if-nez p3, :cond_1 */
v3 = (( org.json.JSONObject ) v2 ).has ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 93 */
(( org.json.JSONObject ) v2 ).optJSONArray ( v0 ); // invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 94 */
/* .local v0, "tmpArray":Lorg/json/JSONArray; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 95 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v0 ).length ( ); // invoke-virtual {v0}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_1 */
/* .line 96 */
v4 = (( org.json.JSONArray ) v0 ).isNull ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONArray;->isNull(I)Z
/* if-nez v4, :cond_0 */
v4 = this.mGameList;
/* .line 97 */
v4 = (( org.json.JSONArray ) v0 ).getString ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* if-nez v4, :cond_0 */
/* .line 98 */
v4 = this.mGameList;
(( org.json.JSONArray ) v0 ).getString ( v3 ); // invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
/* .line 95 */
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 102 */
} // .end local v0 # "tmpArray":Lorg/json/JSONArray;
} // .end local v3 # "i":I
} // :cond_1
/* invoke-direct {p0, v2, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseGameConfig(Lorg/json/JSONObject;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V */
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 106 */
/* .line 103 */
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
/* :catch_0 */
/* move-exception v0 */
/* .line 104 */
/* .local v0, "e":Lorg/json/JSONException; */
v2 = com.miui.server.migard.memory.GameMemoryCleanerConfig.TAG;
final String v3 = "parse config failed"; // const-string v3, "parse config failed"
com.miui.server.migard.utils.LogUtils .e ( v2,v3,v0 );
/* .line 105 */
int v1 = 0; // const/4 v1, 0x0
/* .line 107 */
} // .end local v0 # "e":Lorg/json/JSONException;
} // :goto_1
} // .end method
private void parseGameConfig ( org.json.JSONObject p0, com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig p1 ) {
/* .locals 2 */
/* .param p1, "json" # Lorg/json/JSONObject; */
/* .param p2, "config" # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 111 */
final String v0 = "mem-clean-period"; // const-string v0, "mem-clean-period"
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 112 */
int v1 = 0; // const/4 v1, 0x0
v0 = (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v0, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I */
/* .line 113 */
} // :cond_0
final String v0 = "clean-first-delay"; // const-string v0, "clean-first-delay"
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 114 */
int v1 = -1; // const/4 v1, -0x1
v0 = (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v0, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I */
/* .line 115 */
} // :cond_1
final String v0 = "killer-configs"; // const-string v0, "killer-configs"
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 116 */
(( org.json.JSONObject ) p1 ).optJSONArray ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* invoke-direct {p0, v0, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseKillerConfigs(Lorg/json/JSONArray;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V */
/* .line 117 */
} // :cond_2
final String v0 = "compactor-configs"; // const-string v0, "compactor-configs"
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 118 */
(( org.json.JSONObject ) p1 ).optJSONArray ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* invoke-direct {p0, v0, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseCompactorConfigs(Lorg/json/JSONArray;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V */
/* .line 119 */
} // :cond_3
final String v0 = "reclaim_memory_percent"; // const-string v0, "reclaim_memory_percent"
v1 = (( org.json.JSONObject ) p1 ).has ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
if ( v1 != null) { // if-eqz v1, :cond_4
/* .line 120 */
/* const/16 v1, 0x64 */
v0 = (( org.json.JSONObject ) p1 ).optInt ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I
/* iput v0, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I */
/* .line 121 */
} // :cond_4
return;
} // .end method
private void parseKillerConfigs ( org.json.JSONArray p0, com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig p1 ) {
/* .locals 5 */
/* .param p1, "array" # Lorg/json/JSONArray; */
/* .param p2, "config" # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Lorg/json/JSONException; */
/* } */
} // .end annotation
/* .line 127 */
/* if-nez p1, :cond_0 */
/* .line 128 */
return;
/* .line 129 */
} // :cond_0
v0 = (( org.json.JSONArray ) p1 ).length ( ); // invoke-virtual {p1}, Lorg/json/JSONArray;->length()I
/* .line 130 */
/* .local v0, "N":I */
/* if-nez v0, :cond_1 */
/* .line 131 */
return;
/* .line 133 */
} // :cond_1
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* if-ge v1, v0, :cond_3 */
/* .line 134 */
try { // :try_start_0
(( org.json.JSONArray ) p1 ).optJSONObject ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;
/* .line 135 */
/* .local v2, "jsonObject":Lorg/json/JSONObject; */
/* if-nez v2, :cond_2 */
/* .line 136 */
/* .line 137 */
} // :cond_2
/* new-instance v3, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig; */
/* invoke-direct {v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;-><init>()V */
/* .line 138 */
/* .local v3, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* .line 139 */
v4 = this.mActionConfigs;
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 133 */
} // .end local v3 # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
} // :goto_1
/* add-int/lit8 v1, v1, 0x1 */
/* .line 141 */
} // .end local v1 # "i":I
} // .end local v2 # "jsonObject":Lorg/json/JSONObject;
/* :catch_0 */
/* move-exception v1 */
/* .line 142 */
/* .local v1, "e":Lorg/json/JSONException; */
/* throw v1 */
/* .line 143 */
} // .end local v1 # "e":Lorg/json/JSONException;
} // :cond_3
/* nop */
/* .line 144 */
return;
} // .end method
/* # virtual methods */
public void addCompactorCommonWhiteList ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 290 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mCompactorCommonWhiteList;
/* .line 291 */
v0 = this.mCompactorCommonWhiteList;
/* .line 292 */
return;
} // .end method
void addGameList ( java.util.List p0, Boolean p1 ) {
/* .locals 1 */
/* .param p2, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 201 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 202 */
v0 = this.mGameList;
/* .line 204 */
} // :cond_0
v0 = this.mGameList;
/* .line 205 */
} // :goto_0
v0 = this.mGameList;
/* .line 206 */
return;
} // .end method
public void addKillerCommonWhilteList ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 285 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mKillerCommonWhiteList;
/* .line 286 */
v0 = this.mKillerCommonWhiteList;
/* .line 287 */
return;
} // .end method
public void addUserProtectList ( java.util.List p0, Boolean p1 ) {
/* .locals 3 */
/* .param p2, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 266 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = com.miui.server.migard.memory.GameMemoryCleanerConfig.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "add user protect list: "; // const-string v2, "add user protect list: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 267 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 268 */
v0 = this.mUserProtectList;
/* .line 270 */
} // :cond_0
v0 = this.mUserProtectList;
/* .line 272 */
} // :goto_0
v0 = this.mUserProtectList;
/* .line 273 */
return;
} // .end method
void applyGameConfig ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "game" # Ljava/lang/String; */
/* .line 221 */
if ( p1 != null) { // if-eqz p1, :cond_2
v0 = android.text.TextUtils .isEmpty ( p1 );
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 225 */
} // :cond_0
v0 = v0 = this.mConfigMap;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 226 */
v0 = this.mConfigMap;
/* check-cast v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
this.mActiveConfig = v0;
/* .line 228 */
} // :cond_1
v0 = this.mCommonConfig;
this.mActiveConfig = v0;
/* .line 229 */
} // :goto_0
return;
/* .line 222 */
} // :cond_2
} // :goto_1
int v0 = 0; // const/4 v0, 0x0
this.mActiveConfig = v0;
/* .line 223 */
return;
} // .end method
public void configFromCloudControl ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "game" # Ljava/lang/String; */
/* .param p2, "jsonString" # Ljava/lang/String; */
/* .line 188 */
if ( p1 != null) { // if-eqz p1, :cond_0
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 189 */
v0 = android.text.TextUtils .isEmpty ( p2 );
/* if-nez v0, :cond_0 */
/* .line 190 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
/* invoke-direct {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V */
/* .line 191 */
/* .local v0, "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
int v1 = 1; // const/4 v1, 0x1
v1 = /* invoke-direct {p0, p2, v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseConfig(Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;Z)Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 192 */
v1 = this.mConfigMap;
/* .line 194 */
} // .end local v0 # "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
} // :cond_0
return;
} // .end method
void configFromFile ( ) {
/* .locals 3 */
/* .line 176 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/vendor/etc/game_memory_cleaner.cfg"; // const-string v1, "/vendor/etc/game_memory_cleaner.cfg"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v0 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 177 */
com.miui.server.migard.utils.FileUtils .readFromSys ( v1 );
/* .line 178 */
/* .local v0, "jsonString":Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = (( java.lang.String ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/String;->length()I
/* if-lez v1, :cond_0 */
/* .line 179 */
v1 = this.mGameList;
/* .line 180 */
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
/* invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V */
/* .line 181 */
/* .local v1, "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig; */
int v2 = 0; // const/4 v2, 0x0
v2 = /* invoke-direct {p0, v0, v1, v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseConfig(Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;Z)Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 182 */
this.mCommonConfig = v1;
/* .line 185 */
} // .end local v0 # "jsonString":Ljava/lang/String;
} // .end local v1 # "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
} // :cond_0
return;
} // .end method
void dump ( java.io.PrintWriter p0 ) {
/* .locals 4 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 303 */
final String v0 = "game memory cleaner config: "; // const-string v0, "game memory cleaner config: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 304 */
v0 = this.mCommonConfig;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 305 */
final String v0 = "common config: "; // const-string v0, "common config: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 306 */
v0 = this.mCommonConfig;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 308 */
} // :cond_0
v0 = this.mConfigMap;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 309 */
/* .local v1, "c":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "game: "; // const-string v3, "game: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 310 */
(( java.io.PrintWriter ) p1 ).println ( v2 ); // invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 311 */
} // .end local v1 # "c":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;>;"
/* .line 312 */
} // :cond_1
final String v0 = ""; // const-string v0, ""
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 313 */
/* const-string/jumbo v1, "user protect list:" */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 314 */
v1 = this.mUserProtectList;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 315 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 316 */
/* const-string/jumbo v1, "smartpower white list:" */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 317 */
v1 = this.mPowerWhiteList;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 318 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 319 */
final String v1 = "common kill white list:"; // const-string v1, "common kill white list:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 320 */
v1 = this.mKillerCommonWhiteList;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 321 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 322 */
final String v1 = "common compact white list:"; // const-string v1, "common compact white list:"
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 323 */
v1 = this.mCompactorCommonWhiteList;
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 324 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 325 */
return;
} // .end method
java.util.List getActionConfigs ( ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 233 */
v0 = this.mActiveConfig;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 234 */
v0 = this.mActionConfigs;
/* .line 235 */
/* .local v0, "actionConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;>;" */
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$1;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V */
java.util.Collections .sort ( v0,v1 );
/* .line 241 */
/* .line 243 */
} // .end local v0 # "actionConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;>;"
} // :cond_0
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
} // .end method
Integer getCleanFirstDelay ( ) {
/* .locals 1 */
/* .line 215 */
v0 = this.mActiveConfig;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 216 */
/* iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I */
/* .line 217 */
} // :cond_0
int v0 = -1; // const/4 v0, -0x1
} // .end method
Integer getCleanPeriod ( ) {
/* .locals 1 */
/* .line 209 */
v0 = this.mActiveConfig;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 210 */
/* iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I */
/* .line 211 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
java.util.List getCompactorCommonWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 299 */
v0 = this.mCompactorCommonWhiteList;
} // .end method
java.util.List getGameList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 197 */
v0 = this.mGameList;
} // .end method
java.util.List getKillerCommonWhilteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 295 */
v0 = this.mKillerCommonWhiteList;
} // .end method
java.util.List getPowerWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 262 */
v0 = this.mPowerWhiteList;
} // .end method
Integer getReclaimMemoryPercent ( ) {
/* .locals 1 */
/* .line 247 */
v0 = this.mActiveConfig;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 248 */
/* iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I */
/* .line 249 */
} // :cond_0
/* const/16 v0, 0x64 */
} // .end method
java.util.List getUserProtectList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 276 */
v0 = this.mUserProtectList;
} // .end method
Boolean hasForegroundClean ( ) {
/* .locals 2 */
/* .line 170 */
v0 = this.mActiveConfig;
int v1 = 0; // const/4 v1, 0x0
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 171 */
/* iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I */
/* if-ltz v0, :cond_0 */
int v1 = 1; // const/4 v1, 0x1
} // :cond_0
/* .line 172 */
} // :cond_1
} // .end method
public void removeUserProtectList ( java.util.List p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 280 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = com.miui.server.migard.memory.GameMemoryCleanerConfig.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "remove user protect list: "; // const-string v2, "remove user protect list: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 281 */
v0 = this.mUserProtectList;
/* .line 282 */
return;
} // .end method
public void updatePowerWhiteList ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "pkgList" # Ljava/lang/String; */
/* .line 253 */
v0 = android.text.TextUtils .isEmpty ( p1 );
/* if-nez v0, :cond_0 */
/* .line 254 */
final String v0 = ","; // const-string v0, ","
(( java.lang.String ) p1 ).split ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 255 */
/* .local v0, "pkgArray":[Ljava/lang/String; */
/* array-length v1, v0 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, v0, v2 */
/* .line 256 */
/* .local v3, "packageName":Ljava/lang/String; */
v4 = this.mPowerWhiteList;
/* .line 255 */
} // .end local v3 # "packageName":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 259 */
} // .end local v0 # "pkgArray":[Ljava/lang/String;
} // :cond_0
return;
} // .end method
