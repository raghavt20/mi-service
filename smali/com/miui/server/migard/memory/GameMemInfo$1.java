class com.miui.server.migard.memory.GameMemInfo$1 implements android.os.Parcelable$Creator {
	 /* .source "GameMemInfo.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/memory/GameMemInfo; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/lang/Object;", */
/* "Landroid/os/Parcelable$Creator<", */
/* "Lcom/miui/server/migard/memory/GameMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* # direct methods */
 com.miui.server.migard.memory.GameMemInfo$1 ( ) {
/* .locals 0 */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
/* # virtual methods */
public com.miui.server.migard.memory.GameMemInfo createFromParcel ( android.os.Parcel p0 ) {
/* .locals 3 */
/* .param p1, "source" # Landroid/os/Parcel; */
/* .line 39 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo; */
/* invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>()V */
/* .line 40 */
/* .local v0, "meminfo":Lcom/miui/server/migard/memory/GameMemInfo; */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.pkgName = v1;
/* .line 41 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v1 */
/* iput-wide v1, v0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J */
/* .line 42 */
v1 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* iput v1, v0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I */
/* .line 43 */
v1 = this.sizeHistory;
(( android.os.Parcel ) p1 ).readLongArray ( v1 ); // invoke-virtual {p1, v1}, Landroid/os/Parcel;->readLongArray([J)V
/* .line 44 */
} // .end method
public java.lang.Object createFromParcel ( android.os.Parcel p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 35 */
(( com.miui.server.migard.memory.GameMemInfo$1 ) p0 ).createFromParcel ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/migard/memory/GameMemInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/miui/server/migard/memory/GameMemInfo;
} // .end method
public com.miui.server.migard.memory.GameMemInfo newArray ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "size" # I */
/* .line 50 */
/* new-array v0, p1, [Lcom/miui/server/migard/memory/GameMemInfo; */
} // .end method
public java.lang.Object newArray ( Integer p0 ) { //bridge//synthethic
/* .locals 0 */
/* .line 35 */
(( com.miui.server.migard.memory.GameMemInfo$1 ) p0 ).newArray ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/migard/memory/GameMemInfo$1;->newArray(I)[Lcom/miui/server/migard/memory/GameMemInfo;
} // .end method
