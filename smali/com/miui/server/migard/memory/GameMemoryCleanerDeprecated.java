public class com.miui.server.migard.memory.GameMemoryCleanerDeprecated implements com.miui.server.migard.PackageStatusManager$IForegroundChangedCallback {
	 /* .source "GameMemoryCleanerDeprecated.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # instance fields */
	 private android.content.Context mContext;
	 private java.lang.String mCurrentGame;
	 private java.util.List mGameList;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/List<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private java.util.List mWhiteList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.migard.memory.GameMemoryCleanerDeprecated ( ) {
/* .locals 1 */
/* .line 15 */
/* const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
return;
} // .end method
public com.miui.server.migard.memory.GameMemoryCleanerDeprecated ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 20 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 16 */
int v0 = 0; // const/4 v0, 0x0
this.mCurrentGame = v0;
/* .line 17 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mGameList = v1;
/* .line 18 */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
this.mWhiteList = v1;
/* .line 19 */
this.mContext = v0;
/* .line 21 */
this.mContext = p1;
/* .line 22 */
v0 = this.mGameList;
final String v1 = "com.tencent.ig"; // const-string v1, "com.tencent.ig"
/* .line 23 */
v0 = this.mGameList;
final String v1 = "com.garena.game.kgtw"; // const-string v1, "com.garena.game.kgtw"
/* .line 24 */
v0 = this.mGameList;
final String v1 = "com.miHoYo.GenshinImpact"; // const-string v1, "com.miHoYo.GenshinImpact"
/* .line 25 */
v0 = this.mGameList;
final String v1 = "com.mobile.legends"; // const-string v1, "com.mobile.legends"
/* .line 26 */
v0 = this.mGameList;
final String v1 = "com.tencent.tmgp.pubgmhd"; // const-string v1, "com.tencent.tmgp.pubgmhd"
/* .line 27 */
v0 = this.mGameList;
final String v1 = "com.tencent.tmgp.sgame"; // const-string v1, "com.tencent.tmgp.sgame"
/* .line 28 */
return;
} // .end method
private void killBackgroundApps ( ) {
/* .locals 5 */
/* .line 38 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 39 */
/* .local v0, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v1 = this.mWhiteList;
/* monitor-enter v1 */
/* .line 40 */
try { // :try_start_0
v2 = this.mWhiteList;
/* .line 41 */
v2 = this.mCurrentGame;
/* .line 42 */
/* monitor-exit v1 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 43 */
/* new-instance v1, Lmiui/process/ProcessConfig; */
int v2 = 4; // const/4 v2, 0x4
/* invoke-direct {v1, v2}, Lmiui/process/ProcessConfig;-><init>(I)V */
/* .line 44 */
v2 = /* .local v1, "config":Lmiui/process/ProcessConfig; */
/* if-lez v2, :cond_0 */
/* .line 45 */
(( miui.process.ProcessConfig ) v1 ).setWhiteList ( v0 ); // invoke-virtual {v1, v0}, Lmiui/process/ProcessConfig;->setWhiteList(Ljava/util/List;)V
/* .line 46 */
v2 = com.miui.server.migard.memory.GameMemoryCleanerDeprecated.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v4, "skip white list: " */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v2,v3 );
/* .line 48 */
} // :cond_0
miui.process.ProcessManager .kill ( v1 );
/* .line 49 */
return;
/* .line 42 */
} // .end local v1 # "config":Lmiui/process/ProcessConfig;
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_1
/* monitor-exit v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* throw v2 */
} // .end method
/* # virtual methods */
public void addGameCleanUserProtectList ( java.util.List p0, Boolean p1 ) {
/* .locals 2 */
/* .param p2, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 76 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mWhiteList;
/* monitor-enter v0 */
/* .line 77 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 78 */
try { // :try_start_0
v1 = this.mWhiteList;
/* .line 80 */
} // :cond_0
v1 = this.mWhiteList;
/* .line 82 */
} // :goto_0
v1 = this.mWhiteList;
/* .line 83 */
/* monitor-exit v0 */
/* .line 84 */
return;
/* .line 83 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 5 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 91 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "physical mem size (GB): "; // const-string v1, "physical mem size (GB): "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 92 */
miui.util.HardwareInfo .getTotalPhysicalMemory ( );
/* move-result-wide v1 */
/* const-wide/32 v3, 0x40000000 */
/* div-long/2addr v1, v3 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 91 */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 93 */
/* const-string/jumbo v0, "white list packages: " */
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 94 */
v0 = this.mWhiteList;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 95 */
return;
} // .end method
public java.lang.String getCallbackName ( ) {
/* .locals 1 */
/* .line 73 */
/* const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
} // .end method
public Boolean isLowMemDevice ( ) {
/* .locals 5 */
/* .line 30 */
miui.util.HardwareInfo .getTotalPhysicalMemory ( );
/* move-result-wide v0 */
/* const-wide/32 v2, 0x40000000 */
/* div-long/2addr v0, v2 */
/* .line 31 */
/* .local v0, "memSize":J */
/* sget-boolean v2, Lcom/miui/server/migard/MiGardService;->DEBUG_VERSION:Z */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 32 */
v2 = com.miui.server.migard.memory.GameMemoryCleanerDeprecated.TAG;
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "check physical mem size, size: "; // const-string v4, "check physical mem size, size: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0, v1 ); // invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v4 = " GB"; // const-string v4, " GB"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .d ( v2,v3 );
/* .line 33 */
} // :cond_0
/* const-wide/16 v2, 0x4 */
/* cmp-long v2, v0, v2 */
/* if-gtz v2, :cond_1 */
/* .line 34 */
int v2 = 1; // const/4 v2, 0x1
/* .line 35 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
} // .end method
public void onForegroundChanged ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 6 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .line 52 */
v0 = v0 = this.mGameList;
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 53 */
v0 = this.mContext;
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) v0 ).getSystemService ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v0, Landroid/app/ActivityManager; */
/* .line 54 */
/* .local v0, "am":Landroid/app/ActivityManager; */
/* if-nez v0, :cond_0 */
/* .line 55 */
return;
/* .line 57 */
} // :cond_0
/* nop */
/* .line 58 */
(( android.app.ActivityManager ) v0 ).getRunningAppProcesses ( ); // invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
/* .line 59 */
/* .local v1, "appProcessList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;" */
/* if-nez v1, :cond_1 */
/* .line 60 */
return;
/* .line 62 */
} // :cond_1
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 63 */
/* .local v3, "ai":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v4 = this.mWhiteList;
v5 = this.processName;
/* .line 64 */
} // .end local v3 # "ai":Landroid/app/ActivityManager$RunningAppProcessInfo;
/* .line 65 */
} // :cond_2
this.mCurrentGame = p3;
/* .line 66 */
v2 = com.miui.server.migard.memory.GameMemoryCleanerDeprecated.TAG;
/* const-string/jumbo v3, "start kill background apps..." */
android.util.Slog .i ( v2,v3 );
/* .line 67 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->killBackgroundApps()V */
/* .line 68 */
final String v3 = "finish killing"; // const-string v3, "finish killing"
android.util.Slog .i ( v2,v3 );
/* .line 70 */
} // .end local v0 # "am":Landroid/app/ActivityManager;
} // .end local v1 # "appProcessList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
} // :cond_3
return;
} // .end method
public void removeGameCleanUserProtectList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 86 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mWhiteList;
/* monitor-enter v0 */
/* .line 87 */
try { // :try_start_0
v1 = this.mWhiteList;
/* .line 88 */
/* monitor-exit v0 */
/* .line 89 */
return;
/* .line 88 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
} // .end method
