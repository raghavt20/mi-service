class com.miui.server.migard.memory.GameMemoryCleaner$DelayedCleanRunnable implements java.lang.Runnable {
	 /* .source "GameMemoryCleaner.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/memory/GameMemoryCleaner; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "DelayedCleanRunnable" */
} // .end annotation
/* # instance fields */
final com.miui.server.migard.memory.GameMemoryCleaner this$0; //synthetic
/* # direct methods */
private com.miui.server.migard.memory.GameMemoryCleaner$DelayedCleanRunnable ( ) {
/* .locals 0 */
/* .line 90 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
return;
} // .end method
 com.miui.server.migard.memory.GameMemoryCleaner$DelayedCleanRunnable ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 2 */
/* .line 93 */
com.miui.server.migard.memory.GameMemoryCleaner .-$$Nest$sfgetTAG ( );
final String v1 = "reclaim memory for game(periodic)."; // const-string v1, "reclaim memory for game(periodic)."
com.miui.server.migard.utils.LogUtils .i ( v0,v1 );
/* .line 94 */
v0 = this.this$0;
com.miui.server.migard.memory.GameMemoryCleaner .-$$Nest$fgetmCurrentPackage ( v0 );
com.miui.server.migard.memory.GameMemoryCleaner .-$$Nest$mreclaimMemoryForGameIfNeed ( v0,v1 );
/* .line 95 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v0 = (( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).getCleanPeriod ( ); // invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCleanPeriod()I
/* if-lez v0, :cond_0 */
/* .line 96 */
v0 = this.this$0;
com.miui.server.migard.memory.GameMemoryCleaner .-$$Nest$mperiodicClean ( v0 );
/* .line 98 */
} // :cond_0
return;
} // .end method
