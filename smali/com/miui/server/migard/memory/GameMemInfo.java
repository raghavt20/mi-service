class com.miui.server.migard.memory.GameMemInfo implements android.os.Parcelable {
	 /* .source "GameMemInfo.java" */
	 /* # interfaces */
	 /* # static fields */
	 public static final android.os.Parcelable$Creator CREATOR;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Landroid/os/Parcelable$Creator<", */
	 /* "Lcom/miui/server/migard/memory/GameMemInfo;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
public static final Long INVALID_SIZE;
private static final Integer MAX_CNT;
public static final Integer POLICY_AVG;
public static final Integer POLICY_MAX;
public static final Integer POLICY_PRE;
/* # instance fields */
Boolean hasUpdated;
Integer lastIndex;
java.lang.String pkgName;
Long prevSize;
 sizeHistory;
/* # direct methods */
static com.miui.server.migard.memory.GameMemInfo ( ) {
/* .locals 1 */
/* .line 35 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo$1; */
/* invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemInfo$1;-><init>()V */
return;
} // .end method
 com.miui.server.migard.memory.GameMemInfo ( ) {
/* .locals 1 */
/* .line 22 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>(Ljava/lang/String;)V */
/* .line 23 */
return;
} // .end method
 com.miui.server.migard.memory.GameMemInfo ( ) {
/* .locals 5 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 25 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 18 */
/* const/16 v0, 0x14 */
/* new-array v1, v0, [J */
this.sizeHistory = v1;
/* .line 26 */
this.pkgName = p1;
/* .line 27 */
/* const-wide/16 v1, -0x1 */
/* iput-wide v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J */
/* .line 28 */
int v3 = 0; // const/4 v3, 0x0
/* iput v3, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I */
/* .line 29 */
/* iput-boolean v3, p0, Lcom/miui/server/migard/memory/GameMemInfo;->hasUpdated:Z */
/* .line 30 */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
/* if-ge v3, v0, :cond_0 */
/* .line 31 */
v4 = this.sizeHistory;
/* aput-wide v1, v4, v3 */
/* .line 30 */
/* add-int/lit8 v3, v3, 0x1 */
/* .line 33 */
} // .end local v3 # "i":I
} // :cond_0
return;
} // .end method
public static marshall ( com.miui.server.migard.memory.GameMemInfo p0 ) {
/* .locals 2 */
/* .param p0, "parceable" # Lcom/miui/server/migard/memory/GameMemInfo; */
/* .line 128 */
android.os.Parcel .obtain ( );
/* .line 129 */
/* .local v0, "parcel":Landroid/os/Parcel; */
int v1 = 0; // const/4 v1, 0x0
(( android.os.Parcel ) v0 ).setDataPosition ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 130 */
(( com.miui.server.migard.memory.GameMemInfo ) p0 ).writeToParcel ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/migard/memory/GameMemInfo;->writeToParcel(Landroid/os/Parcel;I)V
/* .line 131 */
(( android.os.Parcel ) v0 ).marshall ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
/* .line 132 */
/* .local v1, "bytes":[B */
(( android.os.Parcel ) v0 ).recycle ( ); // invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
/* .line 133 */
} // .end method
public static com.miui.server.migard.memory.GameMemInfo unmarshall ( Object[] p0 ) {
/* .locals 4 */
/* .param p0, "bytes" # [B */
/* .line 137 */
/* array-length v0, p0 */
/* if-nez v0, :cond_0 */
/* .line 138 */
int v0 = 0; // const/4 v0, 0x0
/* .line 139 */
} // :cond_0
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo; */
/* invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>()V */
/* .line 140 */
/* .local v0, "meminfo":Lcom/miui/server/migard/memory/GameMemInfo; */
android.os.Parcel .obtain ( );
/* .line 141 */
/* .local v1, "parcel":Landroid/os/Parcel; */
/* array-length v2, p0 */
int v3 = 0; // const/4 v3, 0x0
(( android.os.Parcel ) v1 ).unmarshall ( p0, v3, v2 ); // invoke-virtual {v1, p0, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V
/* .line 142 */
(( android.os.Parcel ) v1 ).setDataPosition ( v3 ); // invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V
/* .line 143 */
(( com.miui.server.migard.memory.GameMemInfo ) v0 ).readFromParcel ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemInfo;->readFromParcel(Landroid/os/Parcel;)V
/* .line 144 */
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 145 */
} // .end method
/* # virtual methods */
public void addHistoryItem ( Long p0 ) {
/* .locals 4 */
/* .param p1, "size" # J */
/* .line 92 */
/* const-wide/16 v0, 0x0 */
/* cmp-long v0, p1, v0 */
/* if-lez v0, :cond_1 */
v0 = this.sizeHistory;
/* iget v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I */
/* aget-wide v2, v0, v1 */
/* cmp-long v2, p1, v2 */
/* if-nez v2, :cond_0 */
/* .line 95 */
} // :cond_0
int v2 = 1; // const/4 v2, 0x1
/* add-int/2addr v1, v2 */
/* rem-int/lit8 v1, v1, 0x14 */
/* .line 96 */
/* .local v1, "idx":I */
/* iput-wide p1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J */
/* .line 97 */
/* aput-wide p1, v0, v1 */
/* .line 98 */
/* iput v1, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I */
/* .line 99 */
/* iput-boolean v2, p0, Lcom/miui/server/migard/memory/GameMemInfo;->hasUpdated:Z */
/* .line 100 */
return;
/* .line 93 */
} // .end local v1 # "idx":I
} // :cond_1
} // :goto_0
return;
} // .end method
public Integer describeContents ( ) {
/* .locals 1 */
/* .line 57 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Long getPredSize ( Integer p0 ) {
/* .locals 14 */
/* .param p1, "policy" # I */
/* .line 105 */
/* const-wide/16 v0, -0x1 */
/* if-ltz p1, :cond_6 */
int v2 = 2; // const/4 v2, 0x2
/* if-le p1, v2, :cond_0 */
/* .line 107 */
} // :cond_0
/* if-nez p1, :cond_1 */
/* .line 108 */
/* iget-wide v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J */
/* return-wide v0 */
/* .line 109 */
} // :cond_1
/* const-wide/16 v3, 0x0 */
/* .line 110 */
/* .local v3, "max":J */
/* const-wide/16 v5, 0x0 */
/* .line 111 */
/* .local v5, "sum":J */
int v7 = 0; // const/4 v7, 0x0
/* .line 112 */
/* .local v7, "cnt":I */
int v8 = 0; // const/4 v8, 0x0
/* .local v8, "i":I */
} // :goto_0
/* const/16 v9, 0x14 */
/* if-ge v8, v9, :cond_3 */
/* .line 113 */
v9 = this.sizeHistory;
/* aget-wide v10, v9, v8 */
/* const-wide/16 v12, 0x0 */
/* cmp-long v12, v10, v12 */
/* if-lez v12, :cond_2 */
/* .line 114 */
/* add-int/lit8 v7, v7, 0x1 */
/* .line 115 */
/* add-long/2addr v5, v10 */
/* .line 116 */
/* cmp-long v10, v10, v3 */
/* if-lez v10, :cond_2 */
/* .line 117 */
/* aget-wide v3, v9, v8 */
/* .line 112 */
} // :cond_2
/* add-int/lit8 v8, v8, 0x1 */
/* .line 120 */
} // .end local v8 # "i":I
} // :cond_3
/* if-nez v7, :cond_4 */
/* .line 121 */
/* return-wide v0 */
/* .line 122 */
} // :cond_4
/* if-ne p1, v2, :cond_5 */
/* .line 123 */
/* return-wide v3 */
/* .line 124 */
} // :cond_5
/* int-to-long v0, v7 */
/* div-long v0, v5, v0 */
/* return-wide v0 */
/* .line 106 */
} // .end local v3 # "max":J
} // .end local v5 # "sum":J
} // .end local v7 # "cnt":I
} // :cond_6
} // :goto_1
/* return-wide v0 */
} // .end method
public void readFromParcel ( android.os.Parcel p0 ) {
/* .locals 2 */
/* .param p1, "in" # Landroid/os/Parcel; */
/* .line 69 */
(( android.os.Parcel ) p1 ).readString ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;
this.pkgName = v0;
/* .line 70 */
(( android.os.Parcel ) p1 ).readLong ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J */
/* .line 71 */
v0 = (( android.os.Parcel ) p1 ).readInt ( ); // invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I
/* iput v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I */
/* .line 72 */
v0 = this.sizeHistory;
(( android.os.Parcel ) p1 ).readLongArray ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->readLongArray([J)V
/* .line 73 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 6 */
/* .line 77 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 78 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* const-string/jumbo v1, "{package:" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 79 */
v1 = this.pkgName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 80 */
final String v1 = " history_size:"; // const-string v1, " history_size:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 81 */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
} // :goto_0
/* const/16 v2, 0x14 */
/* if-ge v1, v2, :cond_1 */
/* .line 82 */
v2 = this.sizeHistory;
/* aget-wide v2, v2, v1 */
/* const-wide/16 v4, 0x0 */
/* cmp-long v4, v2, v4 */
/* if-lez v4, :cond_0 */
/* .line 83 */
(( java.lang.StringBuilder ) v0 ).append ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
/* .line 84 */
final String v2 = ","; // const-string v2, ","
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 81 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 87 */
} // .end local v1 # "i":I
} // :cond_1
/* const-string/jumbo v1, "}" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 88 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public void writeToParcel ( android.os.Parcel p0, Integer p1 ) {
/* .locals 2 */
/* .param p1, "dest" # Landroid/os/Parcel; */
/* .param p2, "flags" # I */
/* .line 62 */
v0 = this.pkgName;
(( android.os.Parcel ) p1 ).writeString ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V
/* .line 63 */
/* iget-wide v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->prevSize:J */
(( android.os.Parcel ) p1 ).writeLong ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V
/* .line 64 */
/* iget v0, p0, Lcom/miui/server/migard/memory/GameMemInfo;->lastIndex:I */
(( android.os.Parcel ) p1 ).writeInt ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V
/* .line 65 */
v0 = this.sizeHistory;
(( android.os.Parcel ) p1 ).writeLongArray ( v0 ); // invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V
/* .line 66 */
return;
} // .end method
