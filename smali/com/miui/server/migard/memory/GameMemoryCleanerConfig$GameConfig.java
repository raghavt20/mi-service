class com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig {
	 /* .source "GameMemoryCleanerConfig.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/memory/GameMemoryCleanerConfig; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "GameConfig" */
} // .end annotation
/* # instance fields */
java.util.List mActionConfigs;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Integer mCleanFirstDelay;
Integer mCleanPeriod;
Integer mReclaimMemoryPercent;
final com.miui.server.migard.memory.GameMemoryCleanerConfig this$0; //synthetic
/* # direct methods */
 com.miui.server.migard.memory.GameMemoryCleanerConfig$GameConfig ( ) {
/* .locals 0 */
/* .line 333 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 334 */
/* new-instance p1, Ljava/util/ArrayList; */
/* invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V */
this.mActionConfigs = p1;
/* .line 335 */
/* const/16 p1, 0x64 */
/* iput p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I */
/* .line 336 */
int p1 = 0; // const/4 p1, 0x0
/* iput p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I */
/* .line 337 */
int p1 = -1; // const/4 p1, -0x1
/* iput p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I */
/* .line 338 */
return;
} // .end method
/* # virtual methods */
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 342 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 343 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "clean memory percent: "; // const-string v1, "clean memory percent: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 344 */
v1 = this.mActionConfigs;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_0
/* check-cast v3, Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* .line 345 */
/* .local v3, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
/* .line 346 */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 347 */
} // .end local v3 # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
/* .line 348 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 349 */
final String v1 = "first clean delay: "; // const-string v1, "first clean delay: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 350 */
final String v1 = "clean periodic: "; // const-string v1, "clean periodic: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 351 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
