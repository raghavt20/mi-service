public class com.miui.server.migard.memory.GameMemoryCleaner implements com.miui.server.migard.IMiGardFeature implements com.miui.server.migard.PackageStatusManager$IForegroundChangedCallback implements com.miui.server.migard.ScreenStatusManager$IScreenChangedCallback implements com.miui.server.migard.UidStateManager$IUidStateChangedCallback {
	 /* .source "GameMemoryCleaner.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable; */
	 /* } */
} // .end annotation
/* # static fields */
private static final Boolean DEBUG;
private static final java.lang.String DEFAULT_MEMCG_PATH_R;
private static final java.lang.String DEFAULT_MEMCG_PATH_S;
private static final java.lang.String GAME_MEMCG_PATH_R;
private static final java.lang.String GAME_MEMCG_PATH_S;
private static final Integer GAME_MEMORY_PREDICT_POLICY;
private static final java.lang.String GAME_MEM_SAVE_PATH;
private static final Boolean IS_MEMORY_CLEAN_ENABLED;
private static final Boolean IS_MIUI_LITE;
private static final java.lang.String MEMCG_CURR_MEMUSAGE_NODE;
private static final java.lang.String MEMCG_MAX_MEMUSAGE_NODE;
private static final java.lang.String MEMCG_PROCS_NODE;
private static final java.lang.String TAG;
private static final java.lang.String UID_PREFIX;
/* # instance fields */
private Boolean hasConfiged;
private android.content.Context mContext;
private Integer mCurrentAppUid;
private java.lang.String mCurrentGame;
private java.lang.String mCurrentPackage;
private java.lang.Runnable mDelayedCleanRunnable;
private Boolean mFeatureUsed;
private java.lang.String mGameMemCgroupPath;
private java.util.Map mGameMemInfos;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/Map<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/migard/memory/GameMemInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private com.android.server.am.GameMemoryReclaimer mGameMemoryReclaimer;
private android.os.Handler mHandler;
private java.lang.String mMemCgroupPath;
private com.android.server.ServiceThread mServiceThread;
private java.lang.String mVpnPackage;
/* # direct methods */
public static void $r8$lambda$9bxCXNEUmR3ic9q-W3veVZveV6Q ( com.miui.server.migard.memory.GameMemoryCleaner p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onScreenOff$4()V */
return;
} // .end method
public static void $r8$lambda$E_eNQL8jbo4Y7H5O2WQgcBUnbaM ( com.miui.server.migard.memory.GameMemoryCleaner p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$reclaimBackgroundMemory$0()V */
return;
} // .end method
public static void $r8$lambda$NVZCgRjE4_9WvtG64_8PtqTg8bc ( com.miui.server.migard.memory.GameMemoryCleaner p0, java.util.List p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$addGameList$1(Ljava/util/List;)V */
return;
} // .end method
public static void $r8$lambda$QnCRf_vicmPzXKGzwgYVGZdp5pY ( com.miui.server.migard.memory.GameMemoryCleaner p0, Integer p1 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onUidGone$5(I)V */
return;
} // .end method
public static void $r8$lambda$Ue-nPkQsE7-Uy0CkF_dqMWA8c68 ( com.miui.server.migard.memory.GameMemoryCleaner p0 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onUserPresent$3()V */
return;
} // .end method
public static void $r8$lambda$hyq6jmvb0iar4_j200VXJm62P0E ( com.miui.server.migard.memory.GameMemoryCleaner p0, Integer p1, Integer p2, java.lang.String p3 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onProcessStart$6(IILjava/lang/String;)V */
return;
} // .end method
public static void $r8$lambda$noEGEni3kXBXQxqg8LV4Omnxna4 ( com.miui.server.migard.memory.GameMemoryCleaner p0, java.lang.String p1, Integer p2 ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->lambda$onForegroundChanged$2(Ljava/lang/String;I)V */
return;
} // .end method
static java.lang.String -$$Nest$fgetmCurrentPackage ( com.miui.server.migard.memory.GameMemoryCleaner p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCurrentPackage;
} // .end method
static void -$$Nest$mperiodicClean ( com.miui.server.migard.memory.GameMemoryCleaner p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->periodicClean()V */
return;
} // .end method
static void -$$Nest$mreclaimMemoryForGameIfNeed ( com.miui.server.migard.memory.GameMemoryCleaner p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->reclaimMemoryForGameIfNeed(Ljava/lang/String;)V */
return;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
} // .end method
static com.miui.server.migard.memory.GameMemoryCleaner ( ) {
/* .locals 2 */
/* .line 53 */
/* const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleaner; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 57 */
/* sget-boolean v0, Lmiui/os/Build;->IS_MIUI_LITE_VERSION:Z */
com.miui.server.migard.memory.GameMemoryCleaner.IS_MIUI_LITE = (v0!= 0);
/* .line 58 */
/* nop */
/* .line 59 */
final String v0 = "persist.sys.migard.mem_clean_enabled"; // const-string v0, "persist.sys.migard.mem_clean_enabled"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.migard.memory.GameMemoryCleaner.IS_MEMORY_CLEAN_ENABLED = (v0!= 0);
/* .line 60 */
/* nop */
/* .line 61 */
final String v0 = "persist.sys.migard.game_predict_policy"; // const-string v0, "persist.sys.migard.game_predict_policy"
int v1 = 2; // const/4 v1, 0x2
v0 = android.os.SystemProperties .getInt ( v0,v1 );
/* .line 60 */
return;
} // .end method
public com.miui.server.migard.memory.GameMemoryCleaner ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "gameMemoryReclaimer" # Lcom/android/server/am/GameMemoryReclaimer; */
/* .line 101 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 71 */
int v0 = 0; // const/4 v0, 0x0
this.mContext = v0;
/* .line 74 */
int v1 = 0; // const/4 v1, 0x0
/* iput-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mFeatureUsed:Z */
/* .line 76 */
this.mVpnPackage = v0;
/* .line 77 */
this.mCurrentPackage = v0;
/* .line 78 */
this.mCurrentGame = v0;
/* .line 79 */
/* iput v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentAppUid:I */
/* .line 83 */
this.mMemCgroupPath = v0;
/* .line 84 */
this.mGameMemCgroupPath = v0;
/* .line 85 */
/* iput-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z */
/* .line 87 */
this.mDelayedCleanRunnable = v0;
/* .line 102 */
this.mContext = p1;
/* .line 103 */
this.mGameMemoryReclaimer = p2;
/* .line 104 */
/* new-instance v0, Lcom/android/server/ServiceThread; */
v2 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* invoke-direct {v0, v2, v1, v1}, Lcom/android/server/ServiceThread;-><init>(Ljava/lang/String;IZ)V */
this.mServiceThread = v0;
/* .line 105 */
(( com.android.server.ServiceThread ) v0 ).start ( ); // invoke-virtual {v0}, Lcom/android/server/ServiceThread;->start()V
/* .line 106 */
/* new-instance v0, Landroid/os/Handler; */
v3 = this.mServiceThread;
(( com.android.server.ServiceThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Lcom/android/server/ServiceThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
this.mHandler = v0;
/* .line 108 */
v0 = /* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->initMemCgroupInfo()Z */
/* .line 109 */
/* .local v0, "isCgroupSupportd":Z */
/* if-nez v0, :cond_0 */
/* .line 110 */
final String v3 = "mem cgroup is not supported"; // const-string v3, "mem cgroup is not supported"
com.miui.server.migard.utils.LogUtils .i ( v2,v3 );
/* .line 112 */
} // :cond_0
if ( v0 != null) { // if-eqz v0, :cond_2
/* sget-boolean v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MIUI_LITE:Z */
/* if-nez v2, :cond_1 */
/* sget-boolean v2, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MEMORY_CLEAN_ENABLED:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
} // :cond_1
int v1 = 1; // const/4 v1, 0x1
} // :cond_2
/* iput-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mFeatureUsed:Z */
/* .line 113 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 114 */
com.miui.server.migard.PackageStatusManager .getInstance ( );
(( com.miui.server.migard.PackageStatusManager ) v1 ).registerCallback ( p0 ); // invoke-virtual {v1, p0}, Lcom/miui/server/migard/PackageStatusManager;->registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V
/* .line 115 */
com.miui.server.migard.ScreenStatusManager .getInstance ( );
(( com.miui.server.migard.ScreenStatusManager ) v1 ).registerCallback ( p0 ); // invoke-virtual {v1, p0}, Lcom/miui/server/migard/ScreenStatusManager;->registerCallback(Lcom/miui/server/migard/ScreenStatusManager$IScreenChangedCallback;)V
/* .line 116 */
com.miui.server.migard.UidStateManager .getInstance ( );
(( com.miui.server.migard.UidStateManager ) v1 ).registerCallback ( p0 ); // invoke-virtual {v1, p0}, Lcom/miui/server/migard/UidStateManager;->registerCallback(Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;)V
/* .line 119 */
} // :cond_3
/* new-instance v1, Ljava/util/HashMap; */
/* const/16 v2, 0xa */
/* invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V */
this.mGameMemInfos = v1;
/* .line 121 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v1 ).configFromFile ( ); // invoke-virtual {v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->configFromFile()V
/* .line 122 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v1 ).getGameList ( ); // invoke-virtual {v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getGameList()Ljava/util/List;
/* .line 123 */
/* .local v1, "gameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_5
/* check-cast v3, Ljava/lang/String; */
/* .line 124 */
/* .local v3, "g":Ljava/lang/String; */
v4 = v4 = this.mGameMemInfos;
/* if-nez v4, :cond_4 */
/* .line 125 */
v4 = this.mGameMemInfos;
/* invoke-direct {p0, v3}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->generateGameMemInfo(Ljava/lang/String;)Lcom/miui/server/migard/memory/GameMemInfo; */
/* .line 126 */
} // .end local v3 # "g":Ljava/lang/String;
} // :cond_4
/* .line 127 */
} // :cond_5
return;
} // .end method
private Boolean checkAndApplyConfig ( Boolean p0 ) {
/* .locals 7 */
/* .param p1, "onForeground" # Z */
/* .line 239 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 240 */
/* iput-boolean v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z */
/* .line 242 */
} // :cond_0
/* iget-boolean v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z */
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 243 */
/* .line 246 */
} // :cond_1
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v1 ).getActionConfigs ( ); // invoke-virtual {v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getActionConfigs()Ljava/util/List;
/* .line 247 */
/* .local v1, "configs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;>;" */
v2 = if ( v1 != null) { // if-eqz v1, :cond_7
/* if-nez v2, :cond_2 */
/* .line 251 */
} // :cond_2
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
v2 = } // :goto_0
int v3 = 1; // const/4 v3, 0x1
/* if-ge v0, v2, :cond_6 */
/* .line 252 */
/* check-cast v2, Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* .line 253 */
/* .local v2, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig; */
/* instance-of v4, v2, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig; */
if ( v4 != null) { // if-eqz v4, :cond_4
/* .line 254 */
/* move-object v4, v2 */
/* check-cast v4, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig; */
/* .line 255 */
/* .local v4, "kcfg":Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig; */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v5 ).getUserProtectList ( ); // invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getUserProtectList()Ljava/util/List;
(( com.android.server.am.GameProcessKiller$GameProcessKillerConfig ) v4 ).addWhiteList ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V
/* .line 256 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v5 ).getKillerCommonWhilteList ( ); // invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getKillerCommonWhilteList()Ljava/util/List;
(( com.android.server.am.GameProcessKiller$GameProcessKillerConfig ) v4 ).addWhiteList ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V
/* .line 257 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v5 ).getPowerWhiteList ( ); // invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getPowerWhiteList()Ljava/util/List;
(( com.android.server.am.GameProcessKiller$GameProcessKillerConfig ) v4 ).addWhiteList ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V
/* .line 258 */
v5 = this.mVpnPackage;
if ( v5 != null) { // if-eqz v5, :cond_3
/* .line 259 */
/* new-instance v5, Ljava/util/ArrayList; */
/* invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V */
/* .line 260 */
/* .local v5, "vpn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v6 = this.mVpnPackage;
/* .line 261 */
(( com.android.server.am.GameProcessKiller$GameProcessKillerConfig ) v4 ).addWhiteList ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;->addWhiteList(Ljava/util/List;Z)V
/* .line 263 */
} // .end local v5 # "vpn":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
} // :cond_3
v3 = this.mGameMemoryReclaimer;
(( com.android.server.am.GameMemoryReclaimer ) v3 ).addGameProcessKiller ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/GameMemoryReclaimer;->addGameProcessKiller(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
} // .end local v4 # "kcfg":Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;
/* .line 264 */
} // :cond_4
/* instance-of v4, v2, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig; */
if ( v4 != null) { // if-eqz v4, :cond_5
/* .line 265 */
/* move-object v4, v2 */
/* check-cast v4, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig; */
/* .line 266 */
/* .local v4, "ccfg":Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig; */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v5 ).getCompactorCommonWhiteList ( ); // invoke-virtual {v5}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCompactorCommonWhiteList()Ljava/util/List;
(( com.android.server.am.GameProcessCompactor$GameProcessCompactorConfig ) v4 ).addWhiteList ( v5, v3 ); // invoke-virtual {v4, v5, v3}, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;->addWhiteList(Ljava/util/List;Z)V
/* .line 267 */
v3 = this.mGameMemoryReclaimer;
(( com.android.server.am.GameMemoryReclaimer ) v3 ).addGameProcessCompactor ( v4 ); // invoke-virtual {v3, v4}, Lcom/android/server/am/GameMemoryReclaimer;->addGameProcessCompactor(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
/* .line 264 */
} // .end local v4 # "ccfg":Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;
} // :cond_5
} // :goto_1
/* nop */
/* .line 251 */
} // :goto_2
/* add-int/lit8 v0, v0, 0x1 */
/* .line 270 */
} // .end local v0 # "i":I
} // .end local v2 # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
} // :cond_6
/* iput-boolean v3, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z */
/* .line 271 */
/* .line 248 */
} // :cond_7
} // :goto_3
/* iput-boolean v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->hasConfiged:Z */
/* .line 249 */
} // .end method
private Boolean checkGameCgroup ( Integer p0 ) {
/* .locals 5 */
/* .param p1, "uid" # I */
/* .line 338 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mGameMemCgroupPath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "uid_" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 339 */
/* .local v0, "uidPath":Ljava/lang/String; */
/* new-instance v1, Ljava/io/File; */
/* invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 340 */
/* .local v1, "gameCgDir":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
int v3 = 1; // const/4 v3, 0x1
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 341 */
/* .line 342 */
} // :cond_0
v2 = (( java.io.File ) v1 ).mkdir ( ); // invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
int v4 = 0; // const/4 v4, 0x0
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 343 */
(( java.io.File ) v1 ).setReadable ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/io/File;->setReadable(ZZ)Z
/* .line 344 */
(( java.io.File ) v1 ).setWritable ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Ljava/io/File;->setWritable(ZZ)Z
/* .line 345 */
v2 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
final String v4 = "create game cgroup succeed"; // const-string v4, "create game cgroup succeed"
com.miui.server.migard.utils.LogUtils .d ( v2,v4 );
/* .line 346 */
/* .line 348 */
} // :cond_1
v2 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
final String v3 = "create game cgroup failed"; // const-string v3, "create game cgroup failed"
com.miui.server.migard.utils.LogUtils .d ( v2,v3 );
/* .line 349 */
} // .end method
private Long currentGameMemSize ( ) {
/* .locals 8 */
/* .line 222 */
/* const-wide/16 v0, -0x1 */
/* .line 224 */
/* .local v0, "memSize":J */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mGameMemCgroupPath;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/"; // const-string v3, "/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v4, "uid_" */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentAppUid:I */
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 225 */
/* .local v2, "uidPath":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "memory.memsw.usage_in_bytes"; // const-string v4, "memory.memsw.usage_in_bytes"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.FileUtils .readFromSys ( v3 );
/* .line 226 */
/* .local v3, "content":Ljava/lang/String; */
v4 = (( java.lang.String ) v3 ).isEmpty ( ); // invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z
/* if-nez v4, :cond_0 */
/* .line 228 */
try { // :try_start_0
java.lang.Long .parseLong ( v3 );
/* move-result-wide v4 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* move-wide v0, v4 */
/* .line 232 */
/* .line 229 */
/* :catch_0 */
/* move-exception v4 */
/* .line 230 */
/* .local v4, "e":Ljava/lang/NumberFormatException; */
/* const-wide/16 v0, -0x1 */
/* .line 231 */
v5 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "read game size failed, err:"; // const-string v7, "read game size failed, err:"
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .e ( v5,v6 );
/* .line 234 */
} // .end local v4 # "e":Ljava/lang/NumberFormatException;
} // :cond_0
} // :goto_0
/* return-wide v0 */
} // .end method
private void delayClean ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "periodic" # Z */
/* .line 301 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->disableDelayedClean()V */
/* .line 302 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable; */
int v1 = 0; // const/4 v1, 0x0
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Lcom/miui/server/migard/memory/GameMemoryCleaner$DelayedCleanRunnable-IA;)V */
this.mDelayedCleanRunnable = v0;
/* .line 303 */
v1 = this.mHandler;
/* .line 304 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 305 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v2 = (( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v2 ).getCleanPeriod ( ); // invoke-virtual {v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCleanPeriod()I
/* mul-int/lit16 v2, v2, 0x3e8 */
/* int-to-long v2, v2 */
/* .line 306 */
} // :cond_0
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v2 = (( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v2 ).getCleanFirstDelay ( ); // invoke-virtual {v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getCleanFirstDelay()I
/* mul-int/lit16 v2, v2, 0x3e8 */
/* int-to-long v2, v2 */
/* .line 303 */
} // :goto_0
(( android.os.Handler ) v1 ).postDelayed ( v0, v2, v3 ); // invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
/* .line 307 */
return;
} // .end method
private void disableDelayedClean ( ) {
/* .locals 2 */
/* .line 319 */
v0 = this.mDelayedCleanRunnable;
if ( v0 != null) { // if-eqz v0, :cond_0
v1 = this.mHandler;
v0 = (( android.os.Handler ) v1 ).hasCallbacks ( v0 ); // invoke-virtual {v1, v0}, Landroid/os/Handler;->hasCallbacks(Ljava/lang/Runnable;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 320 */
v0 = this.mHandler;
v1 = this.mDelayedCleanRunnable;
(( android.os.Handler ) v0 ).removeCallbacks ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
/* .line 321 */
int v0 = 0; // const/4 v0, 0x0
this.mDelayedCleanRunnable = v0;
/* .line 323 */
} // :cond_0
return;
} // .end method
private com.miui.server.migard.memory.GameMemInfo generateGameMemInfo ( java.lang.String p0 ) {
/* .locals 8 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .line 148 */
final String v0 = "close file failed, "; // const-string v0, "close file failed, "
int v1 = 0; // const/4 v1, 0x0
/* .line 149 */
/* .local v1, "info":Lcom/miui/server/migard/memory/GameMemInfo; */
int v2 = 0; // const/4 v2, 0x0
/* .line 150 */
/* .local v2, "in":Ljava/io/FileInputStream; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "/data/system/migard/game_mem/"; // const-string v4, "/data/system/migard/game_mem/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 151 */
/* .local v3, "path":Ljava/lang/String; */
/* new-instance v4, Ljava/io/File; */
/* invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 152 */
/* .local v4, "f":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).exists ( ); // invoke-virtual {v4}, Ljava/io/File;->exists()Z
/* if-nez v5, :cond_0 */
/* .line 153 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo; */
/* invoke-direct {v0, p1}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>(Ljava/lang/String;)V */
/* .line 155 */
} // :cond_0
try { // :try_start_0
/* new-instance v5, Ljava/io/FileInputStream; */
/* invoke-direct {v5, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v2, v5 */
/* .line 156 */
com.miui.server.migard.utils.FileUtils .readFileByBytes ( v2 );
com.miui.server.migard.memory.GameMemInfo .unmarshall ( v5 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* move-object v1, v5 */
/* .line 161 */
/* nop */
/* .line 163 */
try { // :try_start_1
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 166 */
} // :goto_0
/* .line 164 */
/* :catch_0 */
/* move-exception v5 */
/* .line 165 */
/* .local v5, "e":Ljava/lang/Exception; */
v6 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
com.miui.server.migard.utils.LogUtils .e ( v6,v0,v5 );
} // .end local v5 # "e":Ljava/lang/Exception;
/* .line 161 */
/* :catchall_0 */
/* move-exception v5 */
/* .line 157 */
/* :catch_1 */
/* move-exception v5 */
/* .line 158 */
/* .restart local v5 # "e":Ljava/lang/Exception; */
int v1 = 0; // const/4 v1, 0x0
/* .line 159 */
try { // :try_start_2
v6 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
final String v7 = "read game info failed, "; // const-string v7, "read game info failed, "
com.miui.server.migard.utils.LogUtils .e ( v6,v7,v5 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 161 */
} // .end local v5 # "e":Ljava/lang/Exception;
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 163 */
try { // :try_start_3
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_0 */
/* .line 169 */
} // :cond_1
} // :goto_1
/* if-nez v1, :cond_2 */
/* .line 170 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemInfo; */
/* invoke-direct {v0, p1}, Lcom/miui/server/migard/memory/GameMemInfo;-><init>(Ljava/lang/String;)V */
/* move-object v1, v0 */
/* .line 171 */
} // :cond_2
/* .line 161 */
} // :goto_2
if ( v2 != null) { // if-eqz v2, :cond_3
/* .line 163 */
try { // :try_start_4
(( java.io.FileInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_2 */
/* .line 166 */
/* .line 164 */
/* :catch_2 */
/* move-exception v6 */
/* .line 165 */
/* .local v6, "e":Ljava/lang/Exception; */
v7 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
com.miui.server.migard.utils.LogUtils .e ( v7,v0,v6 );
/* .line 168 */
} // .end local v6 # "e":Ljava/lang/Exception;
} // :cond_3
} // :goto_3
/* throw v5 */
} // .end method
private Boolean initMemCgroupInfo ( ) {
/* .locals 4 */
/* .line 130 */
int v0 = 0; // const/4 v0, 0x0
/* .line 131 */
/* .local v0, "ret":Z */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/fs/cgroup/memory/uid_1000"; // const-string v2, "/sys/fs/cgroup/memory/uid_1000"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_3 */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/dev/memcg/uid_1000"; // const-string v2, "/dev/memcg/uid_1000"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 132 */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 135 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/fs/cgroup/memory"; // const-string v2, "/sys/fs/cgroup/memory"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* new-instance v1, Ljava/io/File; */
final String v3 = "/sys/fs/cgroup/memory/game"; // const-string v3, "/sys/fs/cgroup/memory/game"
/* invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 136 */
this.mMemCgroupPath = v2;
/* .line 137 */
this.mGameMemCgroupPath = v3;
/* .line 138 */
int v0 = 1; // const/4 v0, 0x1
/* .line 139 */
} // :cond_1
/* new-instance v1, Ljava/io/File; */
final String v2 = "/dev/memcg"; // const-string v2, "/dev/memcg"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* new-instance v1, Ljava/io/File; */
final String v3 = "/dev/memcg/game"; // const-string v3, "/dev/memcg/game"
/* invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
v1 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 140 */
this.mMemCgroupPath = v2;
/* .line 141 */
this.mGameMemCgroupPath = v3;
/* .line 142 */
int v0 = 1; // const/4 v0, 0x1
/* .line 144 */
} // :cond_2
} // :goto_0
/* .line 133 */
} // :cond_3
} // :goto_1
} // .end method
private void lambda$addGameList$1 ( java.util.List p0 ) { //synthethic
/* .locals 5 */
/* .param p1, "gameList" # Ljava/util/List; */
/* .line 374 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
int v1 = 1; // const/4 v1, 0x1
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).addGameList ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addGameList(Ljava/util/List;Z)V
/* .line 375 */
v0 = this.mGameMemInfos;
/* .line 376 */
/* .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;" */
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 377 */
/* check-cast v1, Ljava/lang/String; */
/* .line 378 */
v2 = /* .local v1, "pkg":Ljava/lang/String; */
/* if-nez v2, :cond_0 */
/* .line 379 */
v2 = this.mGameMemInfos;
/* check-cast v2, Lcom/miui/server/migard/memory/GameMemInfo; */
/* invoke-direct {p0, v2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->saveGameMemInfo(Lcom/miui/server/migard/memory/GameMemInfo;)V */
/* .line 380 */
/* .line 382 */
} // .end local v1 # "pkg":Ljava/lang/String;
} // :cond_0
/* .line 383 */
} // :cond_1
v2 = } // :goto_1
if ( v2 != null) { // if-eqz v2, :cond_3
/* check-cast v2, Ljava/lang/String; */
/* .line 384 */
/* .local v2, "g":Ljava/lang/String; */
v3 = v3 = this.mGameMemInfos;
/* if-nez v3, :cond_2 */
/* .line 385 */
v3 = this.mGameMemInfos;
/* invoke-direct {p0, v2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->generateGameMemInfo(Ljava/lang/String;)Lcom/miui/server/migard/memory/GameMemInfo; */
/* .line 386 */
} // .end local v2 # "g":Ljava/lang/String;
} // :cond_2
/* .line 387 */
} // :cond_3
return;
} // .end method
private void lambda$onForegroundChanged$2 ( java.lang.String p0, Integer p1 ) { //synthethic
/* .locals 2 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "uid" # I */
/* .line 399 */
v0 = this.mCurrentPackage;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = (( java.lang.String ) v0 ).equals ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_3 */
/* .line 400 */
} // :cond_0
v0 = this.mGameMemInfos;
v0 = v1 = this.mCurrentPackage;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 402 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->disableDelayedClean()V */
/* .line 403 */
int v0 = 0; // const/4 v0, 0x0
this.mCurrentGame = v0;
/* .line 405 */
} // :cond_1
this.mCurrentPackage = p1;
/* .line 406 */
/* iput p2, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mCurrentAppUid:I */
/* .line 407 */
v0 = v0 = this.mGameMemInfos;
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 408 */
v0 = this.mCurrentPackage;
this.mCurrentGame = v0;
/* .line 409 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v1 = this.mCurrentGame;
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).applyGameConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->applyGameConfig(Ljava/lang/String;)V
/* .line 410 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onGameForeground()V */
/* .line 412 */
} // :cond_2
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v1 = this.mCurrentGame;
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).applyGameConfig ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->applyGameConfig(Ljava/lang/String;)V
/* .line 415 */
} // :cond_3
} // :goto_0
return;
} // .end method
private void lambda$onProcessStart$6 ( Integer p0, Integer p1, java.lang.String p2 ) { //synthethic
/* .locals 0 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .line 449 */
/* invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onGameProcessStart(IILjava/lang/String;)V */
/* .line 450 */
return;
} // .end method
private void lambda$onScreenOff$4 ( ) { //synthethic
/* .locals 2 */
/* .line 434 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->saveGameMemInfo()V */
/* .line 435 */
v0 = this.mCurrentPackage;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = v1 = this.mGameMemInfos;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 436 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->disableDelayedClean()V */
/* .line 437 */
} // :cond_0
return;
} // .end method
private void lambda$onUidGone$5 ( Integer p0 ) { //synthethic
/* .locals 0 */
/* .param p1, "uid" # I */
/* .line 443 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->recordGameMemInfo(I)V */
/* .line 444 */
return;
} // .end method
private void lambda$onUserPresent$3 ( ) { //synthethic
/* .locals 2 */
/* .line 426 */
v0 = this.mCurrentPackage;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = v1 = this.mGameMemInfos;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 427 */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onGameForeground()V */
/* .line 428 */
} // :cond_0
return;
} // .end method
private void lambda$reclaimBackgroundMemory$0 ( ) { //synthethic
/* .locals 2 */
/* .line 276 */
v0 = this.mCurrentPackage;
if ( v0 != null) { // if-eqz v0, :cond_1
v0 = v1 = this.mGameMemInfos;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 277 */
int v0 = 0; // const/4 v0, 0x0
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->checkAndApplyConfig(Z)Z */
/* if-nez v0, :cond_0 */
/* .line 278 */
return;
/* .line 279 */
} // :cond_0
v0 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
final String v1 = "reclaim memory for game(scene)."; // const-string v1, "reclaim memory for game(scene)."
com.miui.server.migard.utils.LogUtils .i ( v0,v1 );
/* .line 280 */
v0 = this.mCurrentPackage;
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->reclaimMemoryForGameIfNeed(Ljava/lang/String;)V */
/* .line 282 */
} // :cond_1
return;
} // .end method
private void onGameForeground ( ) {
/* .locals 2 */
/* .line 286 */
v0 = this.mGameMemoryReclaimer;
v1 = this.mCurrentPackage;
(( com.android.server.am.GameMemoryReclaimer ) v0 ).notifyGameForeground ( v1 ); // invoke-virtual {v0, v1}, Lcom/android/server/am/GameMemoryReclaimer;->notifyGameForeground(Ljava/lang/String;)V
/* .line 287 */
int v0 = 1; // const/4 v0, 0x1
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->checkAndApplyConfig(Z)Z */
/* if-nez v0, :cond_0 */
/* .line 288 */
return;
/* .line 289 */
} // :cond_0
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v0 = (( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).hasForegroundClean ( ); // invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->hasForegroundClean()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 290 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->delayClean(Z)V */
/* .line 291 */
} // :cond_1
return;
} // .end method
private void onGameProcessStart ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 3 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .line 330 */
v0 = v0 = this.mGameMemInfos;
if ( v0 != null) { // if-eqz v0, :cond_0
v0 = /* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->checkGameCgroup(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 331 */
v0 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "monitor game pid: "; // const-string v2, "monitor game pid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p2 ); // invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " uid: "; // const-string v2, " uid: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 332 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mGameMemCgroupPath;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/"; // const-string v1, "/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "uid_" */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 333 */
/* .local v0, "uidPath":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "cgroup.procs"; // const-string v2, "cgroup.procs"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
java.lang.Integer .toString ( p2 );
com.miui.server.migard.utils.FileUtils .writeToSys ( v1,v2 );
/* .line 335 */
} // .end local v0 # "uidPath":Ljava/lang/String;
} // :cond_0
return;
} // .end method
private void periodicClean ( ) {
/* .locals 3 */
/* .line 310 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleaner;->mFeatureUsed:Z */
/* if-nez v0, :cond_0 */
/* .line 311 */
return;
/* .line 312 */
} // :cond_0
v0 = this.mGameMemInfos;
v0 = v1 = this.mCurrentPackage;
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 313 */
v0 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mCurrentPackage;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = ", plan periodic clean..."; // const-string v2, ", plan periodic clean..."
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 314 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->delayClean(Z)V */
/* .line 316 */
} // :cond_1
return;
} // .end method
private Long predictGameMemory ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "game" # Ljava/lang/String; */
/* .line 190 */
/* const-wide/16 v0, -0x1 */
/* .line 191 */
/* .local v0, "pred":J */
final String v2 = "predictGameMemory"; // const-string v2, "predictGameMemory"
/* const-wide/32 v3, 0x80000 */
android.os.Trace .traceBegin ( v3,v4,v2 );
/* .line 192 */
/* if-gez v2, :cond_0 */
/* .line 193 */
int v2 = 0; // const/4 v2, 0x0
} // :cond_0
/* nop */
/* .line 194 */
/* .local v2, "policy":I */
} // :goto_0
int v5 = 2; // const/4 v5, 0x2
/* if-le v2, v5, :cond_1 */
} // :cond_1
/* move v5, v2 */
} // :goto_1
/* move v2, v5 */
/* .line 195 */
v5 = v5 = this.mGameMemInfos;
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 196 */
v5 = this.mGameMemInfos;
/* check-cast v5, Lcom/miui/server/migard/memory/GameMemInfo; */
(( com.miui.server.migard.memory.GameMemInfo ) v5 ).getPredSize ( v2 ); // invoke-virtual {v5, v2}, Lcom/miui/server/migard/memory/GameMemInfo;->getPredSize(I)J
/* move-result-wide v0 */
/* .line 197 */
} // :cond_2
android.os.Trace .traceEnd ( v3,v4 );
/* .line 198 */
/* return-wide v0 */
} // .end method
private void reclaimMemoryForGameIfNeed ( java.lang.String p0 ) {
/* .locals 11 */
/* .param p1, "game" # Ljava/lang/String; */
/* .line 202 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->predictGameMemory(Ljava/lang/String;)J */
/* move-result-wide v0 */
/* .line 203 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
v2 = (( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v2 ).getReclaimMemoryPercent ( ); // invoke-virtual {v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getReclaimMemoryPercent()I
/* int-to-long v2, v2 */
/* mul-long/2addr v0, v2 */
/* const-wide/16 v2, 0x64 */
/* div-long/2addr v0, v2 */
/* .line 204 */
/* .local v0, "pred":J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v4, v0, v2 */
/* if-gtz v4, :cond_0 */
/* .line 205 */
return;
/* .line 206 */
} // :cond_0
android.os.Process .getFreeMemory ( );
/* move-result-wide v4 */
/* .line 207 */
/* .local v4, "free":J */
/* invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->currentGameMemSize()J */
/* move-result-wide v6 */
/* .line 208 */
/* .local v6, "used":J */
/* const-wide/16 v8, -0x1 */
/* cmp-long v8, v6, v8 */
/* if-nez v8, :cond_1 */
/* .line 209 */
v2 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
final String v3 = "read current memory usage failed, abort reclaim."; // const-string v3, "read current memory usage failed, abort reclaim."
com.miui.server.migard.utils.LogUtils .e ( v2,v3 );
/* .line 210 */
return;
/* .line 212 */
} // :cond_1
v8 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
final String v10 = "device free mem: "; // const-string v10, "device free mem: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v4, v5 ); // invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = ", pred game mem: "; // const-string v10, ", pred game mem: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v0, v1 ); // invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v10 = ", game used mem: "; // const-string v10, ", game used mem: "
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).append ( v6, v7 ); // invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v8,v9 );
/* .line 213 */
/* sub-long v9, v0, v6 */
/* sub-long/2addr v9, v4 */
/* .line 214 */
/* .local v9, "need":J */
/* cmp-long v2, v9, v2 */
/* if-gtz v2, :cond_2 */
/* .line 215 */
final String v2 = "enough mem for game, dont need kill background apps"; // const-string v2, "enough mem for game, dont need kill background apps"
com.miui.server.migard.utils.LogUtils .i ( v8,v2 );
/* .line 216 */
return;
/* .line 218 */
} // :cond_2
v2 = this.mGameMemoryReclaimer;
(( com.android.server.am.GameMemoryReclaimer ) v2 ).reclaimBackground ( v9, v10 ); // invoke-virtual {v2, v9, v10}, Lcom/android/server/am/GameMemoryReclaimer;->reclaimBackground(J)V
/* .line 219 */
return;
} // .end method
private void recordGameMemInfo ( Integer p0 ) {
/* .locals 9 */
/* .param p1, "uid" # I */
/* .line 354 */
v0 = this.mContext;
com.miui.server.migard.utils.PackageUtils .getPackageNameByUid ( v0,p1 );
/* .line 355 */
/* .local v0, "game":Ljava/lang/String; */
v1 = v1 = this.mGameMemInfos;
/* if-nez v1, :cond_0 */
/* .line 356 */
return;
/* .line 357 */
} // :cond_0
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mGameMemCgroupPath;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "/"; // const-string v2, "/"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "uid_" */
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 358 */
/* .local v1, "uidPath":Ljava/lang/String; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "memory.memsw.max_usage_in_bytes"; // const-string v3, "memory.memsw.max_usage_in_bytes"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.FileUtils .readFromSys ( v2 );
/* .line 359 */
/* .local v2, "content":Ljava/lang/String; */
v3 = (( java.lang.String ) v2 ).isEmpty ( ); // invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z
/* if-nez v3, :cond_1 */
/* .line 361 */
try { // :try_start_0
java.lang.Long .parseLong ( v2 );
/* move-result-wide v3 */
/* :try_end_0 */
/* .catch Ljava/lang/NumberFormatException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 365 */
/* .local v3, "memSize":J */
/* .line 362 */
} // .end local v3 # "memSize":J
/* :catch_0 */
/* move-exception v3 */
/* .line 363 */
/* .local v3, "e":Ljava/lang/NumberFormatException; */
/* const-wide/16 v4, -0x1 */
/* .line 364 */
/* .local v4, "memSize":J */
v6 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
final String v8 = "read game size failed, err:"; // const-string v8, "read game size failed, err:"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v3 ); // invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .e ( v6,v7 );
/* move-wide v3, v4 */
/* .line 366 */
} // .end local v4 # "memSize":J
/* .local v3, "memSize":J */
} // :goto_0
v5 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "game mem size: "; // const-string v7, "game mem size: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v3, v4 ); // invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v5,v6 );
/* .line 367 */
/* const-wide/16 v5, 0x0 */
/* cmp-long v5, v3, v5 */
/* if-lez v5, :cond_1 */
/* .line 368 */
v5 = this.mGameMemInfos;
/* check-cast v5, Lcom/miui/server/migard/memory/GameMemInfo; */
(( com.miui.server.migard.memory.GameMemInfo ) v5 ).addHistoryItem ( v3, v4 ); // invoke-virtual {v5, v3, v4}, Lcom/miui/server/migard/memory/GameMemInfo;->addHistoryItem(J)V
/* .line 370 */
} // .end local v3 # "memSize":J
} // :cond_1
return;
} // .end method
private void saveGameMemInfo ( ) {
/* .locals 2 */
/* .line 175 */
v0 = this.mGameMemInfos;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/miui/server/migard/memory/GameMemInfo; */
/* .line 176 */
/* .local v1, "info":Lcom/miui/server/migard/memory/GameMemInfo; */
/* invoke-direct {p0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->saveGameMemInfo(Lcom/miui/server/migard/memory/GameMemInfo;)V */
/* .line 177 */
} // .end local v1 # "info":Lcom/miui/server/migard/memory/GameMemInfo;
/* .line 178 */
} // :cond_0
v0 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
final String v1 = "save game memory info succeed"; // const-string v1, "save game memory info succeed"
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 179 */
return;
} // .end method
private void saveGameMemInfo ( com.miui.server.migard.memory.GameMemInfo p0 ) {
/* .locals 3 */
/* .param p1, "info" # Lcom/miui/server/migard/memory/GameMemInfo; */
/* .line 182 */
if ( p1 != null) { // if-eqz p1, :cond_0
/* iget-boolean v0, p1, Lcom/miui/server/migard/memory/GameMemInfo;->hasUpdated:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 183 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/data/system/migard/game_mem/"; // const-string v1, "/data/system/migard/game_mem/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.pkgName;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 184 */
com.miui.server.migard.memory.GameMemInfo .marshall ( p1 );
/* .line 183 */
int v2 = 0; // const/4 v2, 0x0
com.miui.server.migard.utils.FileUtils .writeFileByBytes ( v0,v1,v2 );
/* .line 185 */
v0 = com.miui.server.migard.memory.GameMemoryCleaner.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "save game memory info succeed, pkg: "; // const-string v2, "save game memory info succeed, pkg: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.pkgName;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 187 */
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void addGameList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 373 */
/* .local p1, "gameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda1;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/util/List;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 388 */
return;
} // .end method
public void dump ( java.io.PrintWriter p0 ) {
/* .locals 2 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 466 */
final String v0 = "game memory cleaner info: "; // const-string v0, "game memory cleaner info: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 467 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "is miui lite: "; // const-string v1, "is miui lite: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MIUI_LITE:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 468 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "is enabled from sys prop: "; // const-string v1, "is enabled from sys prop: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* sget-boolean v1, Lcom/miui/server/migard/memory/GameMemoryCleaner;->IS_MEMORY_CLEAN_ENABLED:Z */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 469 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "game memory predict policy: "; // const-string v1, "game memory predict policy: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 470 */
final String v0 = "game memory records: "; // const-string v0, "game memory records: "
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 471 */
v0 = this.mGameMemInfos;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/miui/server/migard/memory/GameMemInfo; */
/* .line 472 */
/* .local v1, "info":Lcom/miui/server/migard/memory/GameMemInfo; */
(( java.io.PrintWriter ) p1 ).println ( v1 ); // invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
/* .line 473 */
} // .end local v1 # "info":Lcom/miui/server/migard/memory/GameMemInfo;
/* .line 474 */
} // :cond_0
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->dump(Ljava/io/PrintWriter;)V
/* .line 475 */
return;
} // .end method
public java.lang.String getCallbackName ( ) {
/* .locals 1 */
/* .line 420 */
/* const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleaner; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
} // .end method
public void onForegroundChanged ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .line 396 */
if ( p3 != null) { // if-eqz p3, :cond_1
/* if-gtz p2, :cond_0 */
/* .line 398 */
} // :cond_0
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p0, p3, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda4;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;Ljava/lang/String;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 416 */
return;
/* .line 397 */
} // :cond_1
} // :goto_0
return;
} // .end method
public void onProcessKilled ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 0 */
/* .param p1, "pkg" # Ljava/lang/String; */
/* .param p2, "reason" # Ljava/lang/String; */
/* .line 327 */
return;
} // .end method
public void onProcessStart ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .param p2, "pid" # I */
/* .param p3, "pkg" # Ljava/lang/String; */
/* .param p4, "caller" # Ljava/lang/String; */
/* .line 448 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p0, p1, p2, p3}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda3;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;IILjava/lang/String;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 451 */
return;
} // .end method
public void onScreenOff ( ) {
/* .locals 2 */
/* .line 433 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda5;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 438 */
return;
} // .end method
public Boolean onShellCommand ( java.lang.String p0, java.lang.String p1, java.io.PrintWriter p2 ) {
/* .locals 1 */
/* .param p1, "cmd" # Ljava/lang/String; */
/* .param p2, "value" # Ljava/lang/String; */
/* .param p3, "pw" # Ljava/io/PrintWriter; */
/* .line 461 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
public void onShellHelp ( java.io.PrintWriter p0 ) {
/* .locals 1 */
/* .param p1, "pw" # Ljava/io/PrintWriter; */
/* .line 455 */
final String v0 = "GameMemoryCleaner commands:"; // const-string v0, "GameMemoryCleaner commands:"
(( java.io.PrintWriter ) p1 ).println ( v0 ); // invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 456 */
(( java.io.PrintWriter ) p1 ).println ( ); // invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
/* .line 457 */
return;
} // .end method
public void onUidGone ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "uid" # I */
/* .line 442 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda2;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;I)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 445 */
return;
} // .end method
public void onUserPresent ( ) {
/* .locals 2 */
/* .line 425 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda6; */
/* invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda6;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 429 */
return;
} // .end method
public void onVpnConnected ( java.lang.String p0, Boolean p1 ) {
/* .locals 1 */
/* .param p1, "user" # Ljava/lang/String; */
/* .param p2, "connected" # Z */
/* .line 294 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 295 */
this.mVpnPackage = p1;
/* .line 297 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
this.mVpnPackage = v0;
/* .line 298 */
} // :goto_0
return;
} // .end method
public void reclaimBackgroundMemory ( ) {
/* .locals 2 */
/* .line 275 */
v0 = this.mHandler;
/* new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleaner$$ExternalSyntheticLambda0;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleaner;)V */
(( android.os.Handler ) v0 ).post ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 283 */
return;
} // .end method
public void runOnThread ( java.lang.Runnable p0 ) {
/* .locals 1 */
/* .param p1, "r" # Ljava/lang/Runnable; */
/* .line 391 */
v0 = this.mHandler;
(( android.os.Handler ) v0 ).post ( p1 ); // invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
/* .line 392 */
return;
} // .end method
