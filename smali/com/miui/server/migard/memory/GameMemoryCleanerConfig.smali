.class public Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;
.super Ljava/lang/Object;
.source "GameMemoryCleanerConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    }
.end annotation


# static fields
.field private static final CLEAN_FIRST_DELAY:Ljava/lang/String; = "clean-first-delay"

.field private static final CLEAN_PERIOD:Ljava/lang/String; = "mem-clean-period"

.field private static final COMPACTOR_CONFIGS:Ljava/lang/String; = "compactor-configs"

.field private static final CONFIG_PATH:Ljava/lang/String; = "/vendor/etc/game_memory_cleaner.cfg"

.field private static final DEFAULT_CLEAN_FIRST_DELAY:I = -0x1

.field private static final DEFAULT_CLEAN_PERIOD:I = 0x0

.field private static final DEFAULT_RECLAIM_MEMORY_PCT:I = 0x64

.field private static final GAME_LIST:Ljava/lang/String; = "game-list"

.field private static final KILLER_CONFIGS:Ljava/lang/String; = "killer-configs"

.field private static final RECLAIM_MEMORY_PCT:Ljava/lang/String; = "reclaim_memory_percent"

.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;


# instance fields
.field private mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

.field private mCommonConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

.field private mCompactorCommonWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mConfigMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;",
            ">;"
        }
    .end annotation
.end field

.field private mGameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mKillerCommonWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPowerWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserProtectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->TAG:Ljava/lang/String;

    .line 55
    new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    invoke-direct {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;-><init>()V

    sput-object v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->sInstance:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCommonConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    .line 52
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mConfigMap:Ljava/util/Map;

    .line 53
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mKillerCommonWhiteList:Ljava/util/List;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCompactorCommonWhiteList:Ljava/util/List;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    .line 67
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->initMMSWhiteList()V

    .line 68
    return-void
.end method

.method public static getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;
    .locals 1

    .line 58
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->sInstance:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    return-object v0
.end method

.method private initMMSWhiteList()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.miui.home"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.android.systemui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.xiaomi.xmsf"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.miui.securitycore"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.miui.hybrid"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.android.providers.media.module"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.google.android.providers.media.module"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.miui.mishare.connectivity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.lbe.security.miui"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.xiaomi.metoknlp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    const-string v1, "com.miui.cloudbackup"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method private parseCompactorConfigs(Lorg/json/JSONArray;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V
    .locals 5
    .param p1, "array"    # Lorg/json/JSONArray;
    .param p2, "config"    # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 150
    if-nez p1, :cond_0

    .line 151
    return-void

    .line 152
    :cond_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 153
    .local v0, "N":I
    if-nez v0, :cond_1

    .line 154
    return-void

    .line 156
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 157
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 158
    .local v2, "jsonObject":Lorg/json/JSONObject;
    if-nez v2, :cond_2

    .line 159
    goto :goto_1

    .line 160
    :cond_2
    new-instance v3, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;

    invoke-direct {v3}, Lcom/android/server/am/GameProcessCompactor$GameProcessCompactorConfig;-><init>()V

    .line 161
    .local v3, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    invoke-interface {v3, v2}, Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;->initFromJSON(Lorg/json/JSONObject;)V

    .line 162
    iget-object v4, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mActionConfigs:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v3    # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 164
    .end local v1    # "i":I
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Lorg/json/JSONException;
    throw v1

    .line 166
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_3
    nop

    .line 167
    return-void
.end method

.method private parseConfig(Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;Z)Z
    .locals 6
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "config"    # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    .param p3, "cloud"    # Z

    .line 86
    const-string v0, "game-list"

    const/4 v1, 0x1

    .line 91
    .local v1, "ret":Z
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 92
    .local v2, "jsonObject":Lorg/json/JSONObject;
    if-nez p3, :cond_1

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 94
    .local v0, "tmpArray":Lorg/json/JSONArray;
    if-eqz v0, :cond_1

    .line 95
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 96
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    .line 97
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 98
    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 102
    .end local v0    # "tmpArray":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :cond_1
    invoke-direct {p0, v2, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseGameConfig(Lorg/json/JSONObject;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    goto :goto_1

    .line 103
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->TAG:Ljava/lang/String;

    const-string v3, "parse config failed"

    invoke-static {v2, v3, v0}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    const/4 v1, 0x0

    .line 107
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return v1
.end method

.method private parseGameConfig(Lorg/json/JSONObject;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V
    .locals 2
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "config"    # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 111
    const-string v0, "mem-clean-period"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I

    .line 113
    :cond_0
    const-string v0, "clean-first-delay"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I

    .line 115
    :cond_1
    const-string v0, "killer-configs"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 116
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseKillerConfigs(Lorg/json/JSONArray;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V

    .line 117
    :cond_2
    const-string v0, "compactor-configs"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 118
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseCompactorConfigs(Lorg/json/JSONArray;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V

    .line 119
    :cond_3
    const-string v0, "reclaim_memory_percent"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 120
    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I

    .line 121
    :cond_4
    return-void
.end method

.method private parseKillerConfigs(Lorg/json/JSONArray;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;)V
    .locals 5
    .param p1, "array"    # Lorg/json/JSONArray;
    .param p2, "config"    # Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 127
    if-nez p1, :cond_0

    .line 128
    return-void

    .line 129
    :cond_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 130
    .local v0, "N":I
    if-nez v0, :cond_1

    .line 131
    return-void

    .line 133
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 134
    :try_start_0
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 135
    .local v2, "jsonObject":Lorg/json/JSONObject;
    if-nez v2, :cond_2

    .line 136
    goto :goto_1

    .line 137
    :cond_2
    new-instance v3, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;

    invoke-direct {v3}, Lcom/android/server/am/GameProcessKiller$GameProcessKillerConfig;-><init>()V

    .line 138
    .local v3, "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    invoke-interface {v3, v2}, Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;->initFromJSON(Lorg/json/JSONObject;)V

    .line 139
    iget-object v4, p2, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mActionConfigs:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .end local v3    # "cfg":Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    .end local v1    # "i":I
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Lorg/json/JSONException;
    throw v1

    .line 143
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_3
    nop

    .line 144
    return-void
.end method


# virtual methods
.method public addCompactorCommonWhiteList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 290
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCompactorCommonWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 291
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCompactorCommonWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 292
    return-void
.end method

.method addGameList(Ljava/util/List;Z)V
    .locals 1
    .param p2, "append"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 201
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p2, :cond_0

    .line 202
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 205
    :goto_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 206
    return-void
.end method

.method public addKillerCommonWhilteList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 285
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mKillerCommonWhiteList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 286
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mKillerCommonWhiteList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 287
    return-void
.end method

.method public addUserProtectList(Ljava/util/List;Z)V
    .locals 3
    .param p2, "append"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 266
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "add user protect list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    if-eqz p2, :cond_0

    .line 268
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 272
    :goto_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 273
    return-void
.end method

.method applyGameConfig(Ljava/lang/String;)V
    .locals 1
    .param p1, "game"    # Ljava/lang/String;

    .line 221
    if-eqz p1, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mConfigMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mConfigMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    goto :goto_0

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCommonConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    .line 229
    :goto_0
    return-void

    .line 222
    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    .line 223
    return-void
.end method

.method public configFromCloudControl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "game"    # Ljava/lang/String;
    .param p2, "jsonString"    # Ljava/lang/String;

    .line 188
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 189
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    invoke-direct {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V

    .line 191
    .local v0, "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseConfig(Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mConfigMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    .end local v0    # "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    :cond_0
    return-void
.end method

.method configFromFile()V
    .locals 3

    .line 176
    new-instance v0, Ljava/io/File;

    const-string v1, "/vendor/etc/game_memory_cleaner.cfg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    invoke-static {v1}, Lcom/miui/server/migard/utils/FileUtils;->readFromSys(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "jsonString":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 180
    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V

    .line 181
    .local v1, "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->parseConfig(Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 182
    iput-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCommonConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    .line 185
    .end local v0    # "jsonString":Ljava/lang/String;
    .end local v1    # "config":Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;
    :cond_0
    return-void
.end method

.method dump(Ljava/io/PrintWriter;)V
    .locals 4
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 303
    const-string v0, "game memory cleaner config: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCommonConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    if-eqz v0, :cond_0

    .line 305
    const-string v0, "common config: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCommonConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mConfigMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 309
    .local v1, "c":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "game: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 310
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 311
    .end local v1    # "c":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;>;"
    goto :goto_0

    .line 312
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 313
    const-string/jumbo v1, "user protect list:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 314
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 315
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 316
    const-string/jumbo v1, "smartpower white list:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 317
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 318
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 319
    const-string v1, "common kill white list:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 320
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mKillerCommonWhiteList:Ljava/util/List;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 321
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 322
    const-string v1, "common compact white list:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCompactorCommonWhiteList:Ljava/util/List;

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 324
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method getActionConfigs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;",
            ">;"
        }
    .end annotation

    .line 233
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mActionConfigs:Ljava/util/List;

    .line 235
    .local v0, "actionConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;>;"
    new-instance v1, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$1;

    invoke-direct {v1, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$1;-><init>(Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 241
    return-object v0

    .line 243
    .end local v0    # "actionConfigs":Ljava/util/List;, "Ljava/util/List<Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;>;"
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method getCleanFirstDelay()I
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    if-eqz v0, :cond_0

    .line 216
    iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I

    return v0

    .line 217
    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method getCleanPeriod()I
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    if-eqz v0, :cond_0

    .line 210
    iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanPeriod:I

    return v0

    .line 211
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method getCompactorCommonWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 299
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mCompactorCommonWhiteList:Ljava/util/List;

    return-object v0
.end method

.method getGameList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mGameList:Ljava/util/List;

    return-object v0
.end method

.method getKillerCommonWhilteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mKillerCommonWhiteList:Ljava/util/List;

    return-object v0
.end method

.method getPowerWhiteList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    return-object v0
.end method

.method getReclaimMemoryPercent()I
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    if-eqz v0, :cond_0

    .line 248
    iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mReclaimMemoryPercent:I

    return v0

    .line 249
    :cond_0
    const/16 v0, 0x64

    return v0
.end method

.method getUserProtectList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 276
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    return-object v0
.end method

.method hasForegroundClean()Z
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mActiveConfig:Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 171
    iget v0, v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig$GameConfig;->mCleanFirstDelay:I

    if-ltz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 172
    :cond_1
    return v1
.end method

.method public removeUserProtectList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 280
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remove user protect list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mUserProtectList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 282
    return-void
.end method

.method public updatePowerWhiteList(Ljava/lang/String;)V
    .locals 5
    .param p1, "pkgList"    # Ljava/lang/String;

    .line 253
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 254
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "pkgArray":[Ljava/lang/String;
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 256
    .local v3, "packageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->mPowerWhiteList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    .end local v3    # "packageName":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 259
    .end local v0    # "pkgArray":[Ljava/lang/String;
    :cond_0
    return-void
.end method
