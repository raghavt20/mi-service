.class public Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;
.super Ljava/lang/Object;
.source "GameMemoryCleanerDeprecated.java"

# interfaces
.implements Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentGame:Ljava/lang/String;

.field private mGameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWhiteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mCurrentGame:Ljava/lang/String;

    .line 17
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    .line 18
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    .line 19
    iput-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mContext:Landroid/content/Context;

    .line 21
    iput-object p1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mContext:Landroid/content/Context;

    .line 22
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    const-string v1, "com.tencent.ig"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    const-string v1, "com.garena.game.kgtw"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    const-string v1, "com.miHoYo.GenshinImpact"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    const-string v1, "com.mobile.legends"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    const-string v1, "com.tencent.tmgp.pubgmhd"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    const-string v1, "com.tencent.tmgp.sgame"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method private killBackgroundApps()V
    .locals 5

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    monitor-enter v1

    .line 40
    :try_start_0
    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 41
    iget-object v2, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mCurrentGame:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    new-instance v1, Lmiui/process/ProcessConfig;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lmiui/process/ProcessConfig;-><init>(I)V

    .line 44
    .local v1, "config":Lmiui/process/ProcessConfig;
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 45
    invoke-virtual {v1, v0}, Lmiui/process/ProcessConfig;->setWhiteList(Ljava/util/List;)V

    .line 46
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "skip white list: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_0
    invoke-static {v1}, Lmiui/process/ProcessManager;->kill(Lmiui/process/ProcessConfig;)Z

    .line 49
    return-void

    .line 42
    .end local v1    # "config":Lmiui/process/ProcessConfig;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method


# virtual methods
.method public addGameCleanUserProtectList(Ljava/util/List;Z)V
    .locals 2
    .param p2, "append"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 76
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    monitor-enter v0

    .line 77
    if-eqz p2, :cond_0

    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 82
    :goto_0
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 83
    monitor-exit v0

    .line 84
    return-void

    .line 83
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "physical mem size (GB): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalPhysicalMemory()J

    move-result-wide v1

    const-wide/32 v3, 0x40000000

    div-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 93
    const-string/jumbo v0, "white list packages: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 95
    return-void
.end method

.method public getCallbackName()Ljava/lang/String;
    .locals 1

    .line 73
    const-class v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isLowMemDevice()Z
    .locals 5

    .line 30
    invoke-static {}, Lmiui/util/HardwareInfo;->getTotalPhysicalMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x40000000

    div-long/2addr v0, v2

    .line 31
    .local v0, "memSize":J
    sget-boolean v2, Lcom/miui/server/migard/MiGardService;->DEBUG_VERSION:Z

    if-eqz v2, :cond_0

    .line 32
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "check physical mem size, size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " GB"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :cond_0
    const-wide/16 v2, 0x4

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    .line 34
    const/4 v2, 0x1

    return v2

    .line 35
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public onForegroundChanged(IILjava/lang/String;)V
    .locals 6
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mGameList:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 53
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 54
    .local v0, "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_0

    .line 55
    return-void

    .line 57
    :cond_0
    nop

    .line 58
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    .line 59
    .local v1, "appProcessList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-nez v1, :cond_1

    .line 60
    return-void

    .line 62
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 63
    .local v3, "ai":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v4, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    iget-object v5, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    .end local v3    # "ai":Landroid/app/ActivityManager$RunningAppProcessInfo;
    goto :goto_0

    .line 65
    :cond_2
    iput-object p3, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mCurrentGame:Ljava/lang/String;

    .line 66
    sget-object v2, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "start kill background apps..."

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-direct {p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->killBackgroundApps()V

    .line 68
    const-string v3, "finish killing"

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "appProcessList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :cond_3
    return-void
.end method

.method public removeGameCleanUserProtectList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 86
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    monitor-enter v0

    .line 87
    :try_start_0
    iget-object v1, p0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->mWhiteList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 88
    monitor-exit v0

    .line 89
    return-void

    .line 88
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
