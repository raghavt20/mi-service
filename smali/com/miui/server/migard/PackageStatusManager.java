public class com.miui.server.migard.PackageStatusManager {
	 /* .source "PackageStatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.miui.server.migard.PackageStatusManager sInstance;
/* # instance fields */
private java.util.List mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private miui.process.IForegroundInfoListener$Stub mForegroundInfoChangeListener;
/* # direct methods */
static java.util.List -$$Nest$fgetmCallbacks ( com.miui.server.migard.PackageStatusManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCallbacks;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.migard.PackageStatusManager.TAG;
} // .end method
static com.miui.server.migard.PackageStatusManager ( ) {
/* .locals 1 */
/* .line 13 */
/* const-class v0, Lcom/miui/server/migard/PackageStatusManager; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 14 */
/* new-instance v0, Lcom/miui/server/migard/PackageStatusManager; */
/* invoke-direct {v0}, Lcom/miui/server/migard/PackageStatusManager;-><init>()V */
return;
} // .end method
private com.miui.server.migard.PackageStatusManager ( ) {
/* .locals 1 */
/* .line 38 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 21 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCallbacks = v0;
/* .line 27 */
/* new-instance v0, Lcom/miui/server/migard/PackageStatusManager$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/migard/PackageStatusManager$1;-><init>(Lcom/miui/server/migard/PackageStatusManager;)V */
this.mForegroundInfoChangeListener = v0;
/* .line 39 */
return;
} // .end method
public static com.miui.server.migard.PackageStatusManager getInstance ( ) {
/* .locals 1 */
/* .line 24 */
v0 = com.miui.server.migard.PackageStatusManager.sInstance;
} // .end method
/* # virtual methods */
public void registerCallback ( com.miui.server.migard.PackageStatusManager$IForegroundChangedCallback p0 ) {
/* .locals 3 */
/* .param p1, "cb" # Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback; */
/* .line 42 */
v0 = com.miui.server.migard.PackageStatusManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Register callback, name:"; // const-string v2, "Register callback, name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 43 */
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_0 */
/* .line 44 */
v0 = this.mForegroundInfoChangeListener;
miui.process.ProcessManager .registerForegroundInfoListener ( v0 );
/* .line 45 */
} // :cond_0
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_1 */
/* .line 46 */
v0 = this.mCallbacks;
/* .line 47 */
} // :cond_1
return;
} // .end method
public void unregisterCallback ( com.miui.server.migard.PackageStatusManager$IForegroundChangedCallback p0 ) {
/* .locals 3 */
/* .param p1, "cb" # Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback; */
/* .line 50 */
v0 = com.miui.server.migard.PackageStatusManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unregister callback, name:"; // const-string v2, "Unregister callback, name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 51 */
v0 = v0 = this.mCallbacks;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 52 */
v0 = this.mCallbacks;
/* .line 53 */
} // :cond_0
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_1 */
/* .line 54 */
v0 = this.mForegroundInfoChangeListener;
miui.process.ProcessManager .unregisterForegroundInfoListener ( v0 );
/* .line 55 */
} // :cond_1
return;
} // .end method
