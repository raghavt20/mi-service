public class inal {
	 /* .source "PackageUtils.java" */
	 /* # static fields */
	 private static final java.lang.String ANDROID_IDS_INFO;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static inal ( ) {
		 /* .locals 63 */
		 /* .line 11 */
		 /* const-class v0, Lcom/miui/server/migard/utils/PackageUtils; */
		 (( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
		 /* .line 16 */
		 /* const-string/jumbo v1, "system" */
		 final String v2 = "radio"; // const-string v2, "radio"
		 final String v3 = "bluetooth"; // const-string v3, "bluetooth"
		 final String v4 = "graphics"; // const-string v4, "graphics"
		 final String v5 = "input"; // const-string v5, "input"
		 final String v6 = "audio"; // const-string v6, "audio"
		 final String v7 = "camera"; // const-string v7, "camera"
		 final String v8 = "log"; // const-string v8, "log"
		 final String v9 = "compass"; // const-string v9, "compass"
		 final String v10 = "mount"; // const-string v10, "mount"
		 /* const-string/jumbo v11, "wifi" */
		 final String v12 = "adb"; // const-string v12, "adb"
		 final String v13 = "install"; // const-string v13, "install"
		 final String v14 = "media"; // const-string v14, "media"
		 final String v15 = "dhcp"; // const-string v15, "dhcp"
		 /* const-string/jumbo v16, "sdcard_rw" */
		 /* const-string/jumbo v17, "vpn" */
		 final String v18 = "keystore"; // const-string v18, "keystore"
		 /* const-string/jumbo v19, "usb" */
		 final String v20 = "drm"; // const-string v20, "drm"
		 final String v21 = "mdnsr"; // const-string v21, "mdnsr"
		 final String v22 = "gps"; // const-string v22, "gps"
		 /* const/16 v23, 0x0 */
		 final String v24 = "media_rw"; // const-string v24, "media_rw"
		 final String v25 = "mtp"; // const-string v25, "mtp"
		 /* const/16 v26, 0x0 */
		 final String v27 = "drmrpc"; // const-string v27, "drmrpc"
		 final String v28 = "nfc"; // const-string v28, "nfc"
		 /* const-string/jumbo v29, "sdcard_r" */
		 final String v30 = "clat"; // const-string v30, "clat"
		 final String v31 = "loop_radio"; // const-string v31, "loop_radio"
		 final String v32 = "mediadrm"; // const-string v32, "mediadrm"
		 final String v33 = "package_info"; // const-string v33, "package_info"
		 /* const-string/jumbo v34, "sdcard_pics" */
		 /* const-string/jumbo v35, "sdcard_av" */
		 /* const-string/jumbo v36, "sdcard_all" */
		 final String v37 = "logd"; // const-string v37, "logd"
		 /* const-string/jumbo v38, "shared_relro" */
		 final String v39 = "dbus"; // const-string v39, "dbus"
		 /* const-string/jumbo v40, "tlsdate" */
		 final String v41 = "mediaextractor"; // const-string v41, "mediaextractor"
		 final String v42 = "audioserver"; // const-string v42, "audioserver"
		 final String v43 = "metrics_collector"; // const-string v43, "metrics_collector"
		 final String v44 = "metricsd"; // const-string v44, "metricsd"
		 /* const-string/jumbo v45, "webservd" */
		 final String v46 = "debuggerd"; // const-string v46, "debuggerd"
		 final String v47 = "mediacodec"; // const-string v47, "mediacodec"
		 final String v48 = "cameraserver"; // const-string v48, "cameraserver"
		 final String v49 = "firewalld"; // const-string v49, "firewalld"
		 /* const-string/jumbo v50, "trunksd" */
		 final String v51 = "nvram"; // const-string v51, "nvram"
		 final String v52 = "dns"; // const-string v52, "dns"
		 final String v53 = "dns_tether"; // const-string v53, "dns_tether"
		 /* const-string/jumbo v54, "webview_zygote" */
		 /* const-string/jumbo v55, "vehicle_network" */
		 final String v56 = "media_audio"; // const-string v56, "media_audio"
		 final String v57 = "media_vidio"; // const-string v57, "media_vidio"
		 final String v58 = "media_image"; // const-string v58, "media_image"
		 /* const-string/jumbo v59, "tombstoned" */
		 final String v60 = "media_obb"; // const-string v60, "media_obb"
		 final String v61 = "ese"; // const-string v61, "ese"
		 final String v62 = "ota_update"; // const-string v62, "ota_update"
		 /* filled-new-array/range {v1 ..v62}, [Ljava/lang/String; */
		 return;
	 } // .end method
	 private inal ( ) {
		 /* .locals 0 */
		 /* .line 13 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 14 */
		 return;
	 } // .end method
	 public static java.lang.String getPackageNameByUid ( android.content.Context p0, Integer p1 ) {
		 /* .locals 3 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "uid" # I */
		 /* .line 110 */
		 /* if-nez p1, :cond_0 */
		 /* .line 111 */
		 final String v0 = "root"; // const-string v0, "root"
		 /* .line 112 */
	 } // :cond_0
	 /* const/16 v0, 0x2710 */
	 /* if-ge p1, v0, :cond_2 */
	 /* .line 113 */
	 /* add-int/lit16 v0, p1, -0x3e8 */
	 /* .line 114 */
	 /* .local v0, "index":I */
	 /* if-ltz v0, :cond_1 */
	 v1 = com.miui.server.migard.utils.PackageUtils.ANDROID_IDS_INFO;
	 /* array-length v2, v1 */
	 /* if-ge v0, v2, :cond_1 */
	 /* .line 115 */
	 /* aget-object v1, v1, v0 */
	 /* .line 117 */
} // .end local v0 # "index":I
} // :cond_1
/* .line 118 */
} // :cond_2
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
(( android.content.pm.PackageManager ) v0 ).getPackagesForUid ( p1 ); // invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;
/* .line 119 */
/* .local v0, "pkgs":[Ljava/lang/String; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* array-length v1, v0 */
/* if-lez v1, :cond_3 */
/* .line 120 */
int v1 = 0; // const/4 v1, 0x0
/* aget-object v1, v0, v1 */
/* .line 123 */
} // .end local v0 # "pkgs":[Ljava/lang/String;
} // :cond_3
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Integer getPidByPackage ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 5 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 94 */
int v0 = -1; // const/4 v0, -0x1
/* .line 96 */
/* .local v0, "pid":I */
try { // :try_start_0
final String v1 = "activity"; // const-string v1, "activity"
(( android.content.Context ) p0 ).getSystemService ( v1 ); // invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
/* check-cast v1, Landroid/app/ActivityManager; */
/* .line 97 */
/* .local v1, "am":Landroid/app/ActivityManager; */
(( android.app.ActivityManager ) v1 ).getRunningAppProcesses ( ); // invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_1
/* check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo; */
/* .line 98 */
/* .local v3, "appProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo; */
v4 = this.processName;
v4 = (( java.lang.String ) v4 ).equals ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 99 */
/* iget v2, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v2 */
/* .line 100 */
/* .line 102 */
} // .end local v3 # "appProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
} // :cond_0
/* .line 105 */
} // .end local v1 # "am":Landroid/app/ActivityManager;
} // :cond_1
} // :goto_1
/* .line 103 */
/* :catch_0 */
/* move-exception v1 */
/* .line 104 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.migard.utils.PackageUtils.TAG;
final String v3 = ""; // const-string v3, ""
com.miui.server.migard.utils.LogUtils .e ( v2,v3,v1 );
/* .line 106 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_2
} // .end method
public static Integer getUidByPackage ( android.content.Context p0, java.lang.String p1 ) {
/* .locals 4 */
/* .param p0, "context" # Landroid/content/Context; */
/* .param p1, "packageName" # Ljava/lang/String; */
/* .line 82 */
int v0 = -1; // const/4 v0, -0x1
/* .line 84 */
/* .local v0, "uid":I */
try { // :try_start_0
(( android.content.Context ) p0 ).getPackageManager ( ); // invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
/* .line 85 */
/* .local v1, "pm":Landroid/content/pm/PackageManager; */
int v2 = 1; // const/4 v2, 0x1
(( android.content.pm.PackageManager ) v1 ).getApplicationInfo ( p1, v2 ); // invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
/* .line 86 */
/* .local v2, "applicationInfo":Landroid/content/pm/ApplicationInfo; */
/* iget v3, v2, Landroid/content/pm/ApplicationInfo;->uid:I */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* move v0, v3 */
/* .line 89 */
} // .end local v1 # "pm":Landroid/content/pm/PackageManager;
} // .end local v2 # "applicationInfo":Landroid/content/pm/ApplicationInfo;
/* .line 87 */
/* :catch_0 */
/* move-exception v1 */
/* .line 88 */
/* .local v1, "e":Ljava/lang/Exception; */
v2 = com.miui.server.migard.utils.PackageUtils.TAG;
final String v3 = ""; // const-string v3, ""
com.miui.server.migard.utils.LogUtils .e ( v2,v3,v1 );
/* .line 90 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_0
} // .end method
