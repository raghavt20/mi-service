.class public final Lcom/miui/server/migard/utils/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x1000

.field private static final MAGIC_CODE:Ljava/lang/String; = "MI^G^arD"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    const-class v0, Lcom/miui/server/migard/utils/FileUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static delOldZipFile(Ljava/io/File;)V
    .locals 7
    .param p0, "path"    # Ljava/io/File;

    .line 360
    const-string v0, "GameTrace"

    const-string/jumbo v1, "start  delOldZipFile"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 364
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 365
    .local v0, "tmpList":[Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 366
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    .line 367
    .local v3, "aTempList":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 368
    .local v4, "tmpFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ".zip"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 369
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 370
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 371
    invoke-static {v4}, Lcom/miui/server/migard/utils/FileUtils;->delOldZipFile(Ljava/io/File;)V

    .line 366
    .end local v3    # "aTempList":Ljava/lang/String;
    .end local v4    # "tmpFile":Ljava/io/File;
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 375
    :cond_3
    return-void

    .line 362
    .end local v0    # "tmpList":[Ljava/lang/String;
    :cond_4
    :goto_2
    return-void
.end method

.method public static delTmpTraceFile(Ljava/io/File;)V
    .locals 7
    .param p0, "path"    # Ljava/io/File;

    .line 378
    const-string v0, "GameTrace"

    const-string/jumbo v1, "start  delTraceFile"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 382
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, "tmpList":[Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 384
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    .line 385
    .local v3, "aTempList":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 386
    .local v4, "tmpFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "_trace"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 387
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 388
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 389
    invoke-static {v4}, Lcom/miui/server/migard/utils/FileUtils;->delTmpTraceFile(Ljava/io/File;)V

    .line 384
    .end local v3    # "aTempList":Ljava/lang/String;
    .end local v4    # "tmpFile":Ljava/io/File;
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 393
    :cond_3
    return-void

    .line 380
    .end local v0    # "tmpList":[Ljava/lang/String;
    :cond_4
    :goto_2
    return-void
.end method

.method public static deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .line 396
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 397
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 398
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 400
    :cond_0
    return-void
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .line 36
    if-nez p0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "trimPath":Ljava/lang/String;
    :goto_0
    const-string v1, ""

    .line 38
    .local v1, "fileName":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 39
    move-object v2, v0

    .line 40
    .local v2, "tmpPath":Ljava/lang/String;
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 41
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 43
    :cond_1
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 45
    .end local v2    # "tmpPath":Ljava/lang/String;
    :cond_2
    return-object v1
.end method

.method public static readAllSystraceToZip(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "sourceFilePath"    # Ljava/lang/String;
    .param p1, "zipFilePath"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;

    .line 289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "readAllSystraceToZip zipPath: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameTrace"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "sourceFile":Ljava/io/File;
    const/4 v1, 0x0

    .line 292
    .local v1, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 293
    .local v2, "zos":Ljava/util/zip/ZipOutputStream;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 294
    sget-object v3, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "sourceFilePath: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "not found"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    return-void

    .line 298
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 299
    .local v3, "zipFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 300
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    const-string v5, "The file already exists"

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    if-eqz v2, :cond_1

    :try_start_1
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V

    goto :goto_0

    .line 325
    :catch_0
    move-exception v4

    goto :goto_1

    .line 324
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 326
    .local v4, "e":Ljava/io/IOException;
    :goto_1
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 327
    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    :goto_2
    nop

    .line 301
    :goto_3
    return-void

    .line 303
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 304
    .local v4, "sourceFiles":[Ljava/io/File;
    const/4 v5, 0x1

    if-eqz v4, :cond_6

    array-length v6, v4

    if-ge v6, v5, :cond_4

    goto :goto_5

    .line 307
    :cond_4
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v1, v6

    .line 308
    new-instance v6, Ljava/util/zip/ZipOutputStream;

    new-instance v7, Ljava/io/BufferedOutputStream;

    invoke-direct {v7, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v6, v7}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v2, v6

    .line 309
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_4
    array-length v7, v4

    if-ge v6, v7, :cond_7

    .line 310
    aget-object v7, v4, v6

    invoke-static {v7, v2}, Lcom/miui/server/migard/utils/FileUtils;->readOneTraceToZip(Ljava/io/File;Ljava/util/zip/ZipOutputStream;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 311
    sget-object v7, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    const-string v8, "There is exception to be handled"

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    goto :goto_6

    .line 309
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 305
    .end local v6    # "i":I
    :cond_6
    :goto_5
    sget-object v6, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    const-string v7, "The directory has no files and does not need to be compressed"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_7
    :goto_6
    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 323
    .end local v3    # "zipFile":Ljava/io/File;
    .end local v4    # "sourceFiles":[Ljava/io/File;
    if-eqz v2, :cond_8

    :try_start_3
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V

    goto :goto_7

    .line 325
    :catch_1
    move-exception v3

    goto :goto_8

    .line 324
    :cond_8
    :goto_7
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_9

    .line 326
    .local v3, "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 328
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_a

    .line 327
    :cond_9
    :goto_9
    goto :goto_a

    .line 322
    :catchall_0
    move-exception v3

    goto :goto_b

    .line 319
    :catch_2
    move-exception v3

    .line 320
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 323
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_a

    :try_start_5
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 324
    :cond_a
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_9

    .line 317
    :catch_3
    move-exception v3

    .line 318
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 323
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    if-eqz v2, :cond_b

    :try_start_7
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 324
    :cond_b
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_9

    .line 329
    :goto_a
    return-void

    .line 323
    :goto_b
    if-eqz v2, :cond_c

    :try_start_8
    invoke-virtual {v2}, Ljava/util/zip/ZipOutputStream;->close()V

    goto :goto_c

    .line 325
    :catch_4
    move-exception v4

    goto :goto_d

    .line 324
    :cond_c
    :goto_c
    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_e

    .line 326
    .local v4, "e":Ljava/io/IOException;
    :goto_d
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 327
    .end local v4    # "e":Ljava/io/IOException;
    :cond_d
    :goto_e
    nop

    .line 328
    :goto_f
    throw v3
.end method

.method public static readFileByBytes(Ljava/io/InputStream;)[B
    .locals 7
    .param p0, "in"    # Ljava/io/InputStream;

    .line 118
    const-string v0, ""

    const/4 v1, 0x0

    .line 119
    .local v1, "len":I
    const/4 v2, 0x0

    .line 121
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    :try_start_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object v2, v4

    .line 122
    const/16 v4, 0x1000

    new-array v4, v4, [B

    .line 123
    .local v4, "buffer":[B
    :goto_0
    invoke-virtual {p0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    move v1, v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 124
    invoke-virtual {v2, v4, v3, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 130
    .end local v4    # "buffer":[B
    :cond_0
    nop

    .line 132
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 135
    :goto_1
    goto :goto_2

    .line 133
    :catch_0
    move-exception v3

    .line 134
    .local v3, "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 138
    :goto_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 130
    :catchall_0
    move-exception v3

    goto :goto_4

    .line 126
    :catch_1
    move-exception v4

    .line 127
    .local v4, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v0, v4}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    new-array v3, v3, [B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 130
    if-eqz v2, :cond_1

    .line 132
    :try_start_3
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 135
    goto :goto_3

    .line 133
    :catch_2
    move-exception v5

    .line 134
    .local v5, "e":Ljava/io/IOException;
    sget-object v6, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v6, v0, v5}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    return-object v3

    .line 130
    .end local v4    # "e":Ljava/io/IOException;
    :goto_4
    if-eqz v2, :cond_2

    .line 132
    :try_start_4
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 135
    goto :goto_5

    .line 133
    :catch_3
    move-exception v4

    .line 134
    .restart local v4    # "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v0, v4}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 137
    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    :goto_5
    throw v3
.end method

.method public static readFromSys(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/miui/server/migard/utils/FileUtils;->readFromSys(Ljava/lang/String;Lcom/miui/server/migard/utils/FileReadCallback;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readFromSys(Ljava/lang/String;Lcom/miui/server/migard/utils/FileReadCallback;)Ljava/lang/String;
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/miui/server/migard/utils/FileReadCallback;

    .line 74
    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .local v1, "info":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 78
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object v2, v3

    .line 79
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .local v4, "temp":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 80
    if-eqz p1, :cond_0

    .line 81
    invoke-interface {p1, v4}, Lcom/miui/server/migard/utils/FileReadCallback;->onRead(Ljava/lang/String;)V

    .line 82
    :cond_0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :cond_1
    nop

    .line 89
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 92
    goto :goto_1

    .line 90
    :catch_0
    move-exception v3

    .line 91
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 87
    .end local v4    # "temp":Ljava/lang/String;
    :catchall_0
    move-exception v3

    goto :goto_2

    .line 84
    :catch_1
    move-exception v3

    .line 85
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 87
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v2, :cond_2

    .line 89
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 92
    goto :goto_1

    .line 90
    :catch_2
    move-exception v3

    .line 91
    .local v3, "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    .end local v3    # "e":Ljava/io/IOException;
    nop

    .line 95
    :cond_2
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 87
    :goto_2
    if-eqz v2, :cond_3

    .line 89
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 92
    goto :goto_3

    .line 90
    :catch_3
    move-exception v4

    .line 91
    .local v4, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v0, v4}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    .end local v4    # "e":Ljava/io/IOException;
    :cond_3
    :goto_3
    throw v3
.end method

.method public static readOneTraceToZip(Ljava/io/File;Ljava/util/zip/ZipOutputStream;)Z
    .locals 9
    .param p0, "tracefile"    # Ljava/io/File;
    .param p1, "zipstream"    # Ljava/util/zip/ZipOutputStream;

    .line 332
    const/4 v0, 0x0

    .line 333
    .local v0, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 334
    .local v1, "bis":Ljava/io/BufferedInputStream;
    const/16 v2, 0x2800

    new-array v3, v2, [B

    .line 336
    .local v3, "bufs":[B
    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Ljava/util/zip/ZipEntry;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 337
    .local v5, "zipEntry":Ljava/util/zip/ZipEntry;
    invoke-virtual {p1, v5}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 338
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v0, v6

    .line 339
    new-instance v6, Ljava/io/BufferedInputStream;

    invoke-direct {v6, v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object v1, v6

    .line 340
    const/4 v6, 0x0

    .line 341
    .local v6, "read":I
    :goto_0
    invoke-virtual {v1, v3, v4, v2}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    move v6, v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 342
    invoke-virtual {p1, v3, v4, v6}, Ljava/util/zip/ZipOutputStream;->write([BII)V

    goto :goto_0

    .line 344
    :cond_0
    invoke-virtual {p1}, Ljava/util/zip/ZipOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    .end local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v6    # "read":I
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 351
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 354
    goto :goto_1

    .line 352
    :catch_0
    move-exception v2

    .line 353
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 355
    .end local v2    # "e":Ljava/io/IOException;
    nop

    .line 356
    :goto_1
    const/4 v2, 0x1

    return v2

    .line 349
    :catchall_0
    move-exception v2

    goto :goto_6

    .line 345
    :catch_1
    move-exception v2

    .line 346
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 347
    nop

    .line 350
    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_2

    .line 352
    :catch_2
    move-exception v5

    goto :goto_3

    .line 351
    :cond_1
    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_4

    .line 353
    .local v5, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 354
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :goto_4
    nop

    .line 347
    :goto_5
    return v4

    .line 350
    .end local v2    # "e":Ljava/io/IOException;
    :goto_6
    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    goto :goto_7

    .line 352
    :catch_3
    move-exception v4

    goto :goto_8

    .line 351
    :cond_3
    :goto_7
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_9

    .line 353
    .local v4, "e":Ljava/io/IOException;
    :goto_8
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 354
    .end local v4    # "e":Ljava/io/IOException;
    :cond_4
    :goto_9
    nop

    .line 355
    :goto_a
    throw v2
.end method

.method public static readSysToCryptoZip(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "dir"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "aes"    # Z

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "zipPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 243
    .local v1, "out":Ljava/util/zip/ZipOutputStream;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "readSysToCryptoZip zipPath: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GameTrace"

    invoke-static {v3, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 246
    .local v2, "destFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    .line 248
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 249
    const/4 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    goto :goto_0

    .line 250
    :catch_0
    move-exception v4

    .line 251
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 254
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 256
    :try_start_1
    new-instance v4, Ljava/util/zip/ZipOutputStream;

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v6}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v1, v4

    .line 257
    new-instance v4, Ljava/util/zip/ZipEntry;

    invoke-direct {v4, p2}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 258
    const-string v4, "MI^G^arD"

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/zip/ZipOutputStream;->write([B)V

    .line 260
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    new-array v4, v4, [B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 261
    .local v4, "buf":[B
    const-string v6, "readSysToCryptoZip aes:"

    if-eqz p4, :cond_2

    .line 262
    :goto_1
    :try_start_2
    invoke-virtual {p0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v7

    move v8, v7

    .local v8, "len":I
    if-ltz v7, :cond_1

    .line 263
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-static {v4, v7}, Lcom/miui/server/migard/utils/SecretUtils;->encryptWithAES([B[B)[B

    move-result-object v7

    invoke-virtual {v1, v7, v5, v8}, Ljava/util/zip/ZipOutputStream;->write([BII)V

    goto :goto_1

    .line 265
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "encryptWithAES"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 267
    .end local v8    # "len":I
    :cond_2
    :goto_2
    invoke-virtual {p0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v7

    move v8, v7

    .restart local v8    # "len":I
    if-ltz v7, :cond_3

    .line 268
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-static {v4, v7}, Lcom/miui/server/migard/utils/SecretUtils;->encryptWithXOR([B[B)[B

    move-result-object v7

    invoke-virtual {v1, v7, v5, v8}, Ljava/util/zip/ZipOutputStream;->write([BII)V

    goto :goto_2

    .line 270
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "encryptWithXOR"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :goto_3
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->flush()V

    .line 273
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->closeEntry()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 278
    .end local v4    # "buf":[B
    .end local v8    # "len":I
    nop

    .line 279
    :try_start_3
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 282
    :cond_4
    :goto_4
    goto :goto_8

    .line 280
    :catch_1
    move-exception v3

    .line 281
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 283
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_8

    .line 277
    :catchall_0
    move-exception v3

    goto :goto_5

    .line 274
    :catch_2
    move-exception v3

    .line 275
    .local v3, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 278
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_4

    .line 279
    :try_start_5
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_4

    .line 278
    :goto_5
    if-eqz v1, :cond_5

    .line 279
    :try_start_6
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_6

    .line 280
    :catch_3
    move-exception v4

    .line 281
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 282
    .end local v4    # "e":Ljava/io/IOException;
    :cond_5
    :goto_6
    nop

    .line 283
    :goto_7
    throw v3

    .line 285
    :cond_6
    :goto_8
    return-void
.end method

.method public static readSysToFile(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "dir"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "filePath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "readSysToFile filePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GameTrace"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v1, 0x0

    .line 172
    .local v1, "out":Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 174
    .local v2, "destFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    const-string v4, ""

    if-nez v3, :cond_0

    .line 176
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    goto :goto_0

    .line 177
    :catch_0
    move-exception v3

    .line 178
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v4, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 183
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object v1, v3

    .line 185
    const/16 v3, 0x1000

    new-array v3, v3, [B

    .line 186
    .local v3, "buf":[B
    :goto_1
    invoke-virtual {p0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v5

    move v6, v5

    .local v6, "len":I
    if-ltz v5, :cond_1

    .line 187
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v6}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1

    .line 189
    :cond_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    .end local v3    # "buf":[B
    .end local v6    # "len":I
    nop

    .line 195
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 198
    :cond_2
    :goto_2
    goto :goto_6

    .line 196
    :catch_1
    move-exception v3

    .line 197
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v4, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 199
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_6

    .line 193
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 190
    :catch_2
    move-exception v3

    .line 191
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v4, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 194
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_2

    .line 195
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 194
    :goto_3
    if-eqz v1, :cond_3

    .line 195
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_4

    .line 196
    :catch_3
    move-exception v5

    .line 197
    .local v5, "e":Ljava/io/IOException;
    sget-object v6, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v6, v4, v5}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 198
    .end local v5    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    nop

    .line 199
    :goto_5
    throw v3

    .line 201
    :cond_4
    :goto_6
    return-void
.end method

.method public static readSysToZip(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "dir"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "zipPath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "readSysToZip zipPath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GameTrace"

    invoke-static {v2, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    const/4 v1, 0x0

    .line 207
    .local v1, "out":Ljava/util/zip/ZipOutputStream;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    .local v2, "destFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    const-string v4, ""

    if-nez v3, :cond_0

    .line 211
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    goto :goto_0

    .line 212
    :catch_0
    move-exception v3

    .line 213
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v4, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 216
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 218
    :try_start_1
    new-instance v3, Ljava/util/zip/ZipOutputStream;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v5}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v1, v3

    .line 219
    new-instance v3, Ljava/util/zip/ZipEntry;

    invoke-direct {v3, p2}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 221
    const/16 v3, 0x1000

    new-array v3, v3, [B

    .line 222
    .local v3, "buf":[B
    :goto_1
    invoke-virtual {p0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v5

    move v6, v5

    .local v6, "len":I
    if-ltz v5, :cond_1

    .line 223
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v6}, Ljava/util/zip/ZipOutputStream;->write([BII)V

    goto :goto_1

    .line 225
    :cond_1
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->flush()V

    .line 226
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->closeEntry()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    .end local v3    # "buf":[B
    .end local v6    # "len":I
    nop

    .line 232
    :try_start_2
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 235
    :cond_2
    :goto_2
    goto :goto_6

    .line 233
    :catch_1
    move-exception v3

    .line 234
    .local v3, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v4, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 236
    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_6

    .line 230
    :catchall_0
    move-exception v3

    goto :goto_3

    .line 227
    :catch_2
    move-exception v3

    .line 228
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v4, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 231
    .end local v3    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_2

    .line 232
    :try_start_4
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 231
    :goto_3
    if-eqz v1, :cond_3

    .line 232
    :try_start_5
    invoke-virtual {v1}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_4

    .line 233
    :catch_3
    move-exception v5

    .line 234
    .local v5, "e":Ljava/io/IOException;
    sget-object v6, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v6, v4, v5}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 235
    .end local v5    # "e":Ljava/io/IOException;
    :cond_3
    :goto_4
    nop

    .line 236
    :goto_5
    throw v3

    .line 238
    :cond_4
    :goto_6
    return-void
.end method

.method public static truncateSysFile(Ljava/lang/String;)V
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .line 403
    const-string v0, ""

    const/4 v1, 0x0

    .line 405
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v2

    .line 409
    nop

    .line 411
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 414
    :goto_0
    goto :goto_1

    .line 412
    :catch_0
    move-exception v2

    .line 413
    .local v2, "e":Ljava/io/IOException;
    sget-object v3, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v3, v0, v2}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 409
    :catchall_0
    move-exception v2

    goto :goto_2

    .line 406
    :catch_1
    move-exception v2

    .line 407
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v3, v0, v2}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 409
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_0

    .line 411
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 417
    :cond_0
    :goto_1
    return-void

    .line 409
    :goto_2
    if-eqz v1, :cond_1

    .line 411
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 414
    goto :goto_3

    .line 412
    :catch_2
    move-exception v3

    .line 413
    .local v3, "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 416
    .end local v3    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    throw v2
.end method

.method public static writeFileByBytes(Ljava/lang/String;[BZ)V
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "bytes"    # [B
    .param p2, "append"    # Z

    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "out":Ljava/io/OutputStream;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    .local v1, "destFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, ""

    if-nez v2, :cond_0

    .line 147
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    goto :goto_0

    .line 148
    :catch_0
    move-exception v2

    .line 149
    .local v2, "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v3, v2}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    :try_start_1
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v2, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v0, v2

    .line 154
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    nop

    .line 160
    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 163
    :goto_1
    goto :goto_2

    .line 161
    :catch_1
    move-exception v2

    .line 162
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v3, v2}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v2    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 158
    :catchall_0
    move-exception v2

    goto :goto_3

    .line 155
    :catch_2
    move-exception v2

    .line 156
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_3
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v3, v2}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 158
    .end local v2    # "e":Ljava/io/IOException;
    if-eqz v0, :cond_1

    .line 160
    :try_start_4
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 166
    :cond_1
    :goto_2
    return-void

    .line 158
    :goto_3
    if-eqz v0, :cond_2

    .line 160
    :try_start_5
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 163
    goto :goto_4

    .line 161
    :catch_3
    move-exception v4

    .line 162
    .local v4, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v3, v4}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 165
    .end local v4    # "e":Ljava/io/IOException;
    :cond_2
    :goto_4
    throw v2
.end method

.method public static writeToFile(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "strContent"    # Ljava/lang/String;
    .param p2, "isAppend"    # Z

    .line 99
    const-string v0, ""

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    .local v1, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 102
    .local v2, "writer":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object v2, v3

    .line 103
    invoke-virtual {v2, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    nop

    .line 109
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 112
    :goto_0
    goto :goto_1

    .line 110
    :catch_0
    move-exception v3

    .line 111
    .local v3, "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 107
    :catchall_0
    move-exception v3

    goto :goto_2

    .line 104
    :catch_1
    move-exception v3

    .line 105
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_2
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_0

    .line 109
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 115
    :cond_0
    :goto_1
    return-void

    .line 107
    :goto_2
    if-eqz v2, :cond_1

    .line 109
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 112
    goto :goto_3

    .line 110
    :catch_2
    move-exception v4

    .line 111
    .local v4, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v0, v4}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    .end local v4    # "e":Ljava/io/IOException;
    :cond_1
    :goto_3
    throw v3
.end method

.method public static writeToSys(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "content"    # Ljava/lang/String;

    .line 49
    const-string v0, ""

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 51
    .local v2, "writer":Ljava/io/FileWriter;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 53
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    .line 54
    invoke-virtual {v2, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    nop

    .line 60
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 63
    :goto_0
    goto :goto_3

    .line 61
    :catch_0
    move-exception v3

    .line 62
    .local v3, "e":Ljava/io/IOException;
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v3    # "e":Ljava/io/IOException;
    goto :goto_0

    .line 58
    :catchall_0
    move-exception v3

    goto :goto_1

    .line 55
    :catch_1
    move-exception v3

    .line 56
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_2
    sget-object v4, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v4, v0, v3}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 58
    .end local v3    # "e":Ljava/io/IOException;
    if-eqz v2, :cond_1

    .line 60
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 58
    :goto_1
    if-eqz v2, :cond_0

    .line 60
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 63
    goto :goto_2

    .line 61
    :catch_2
    move-exception v4

    .line 62
    .local v4, "e":Ljava/io/IOException;
    sget-object v5, Lcom/miui/server/migard/utils/FileUtils;->TAG:Ljava/lang/String;

    invoke-static {v5, v0, v4}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .end local v4    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    throw v3

    .line 67
    :cond_1
    :goto_3
    return-void
.end method
