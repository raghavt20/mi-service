.class public final Lcom/miui/server/migard/utils/PackageUtils;
.super Ljava/lang/Object;
.source "PackageUtils.java"


# static fields
.field private static final ANDROID_IDS_INFO:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 63

    .line 11
    const-class v0, Lcom/miui/server/migard/utils/PackageUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/utils/PackageUtils;->TAG:Ljava/lang/String;

    .line 16
    const-string/jumbo v1, "system"

    const-string v2, "radio"

    const-string v3, "bluetooth"

    const-string v4, "graphics"

    const-string v5, "input"

    const-string v6, "audio"

    const-string v7, "camera"

    const-string v8, "log"

    const-string v9, "compass"

    const-string v10, "mount"

    const-string/jumbo v11, "wifi"

    const-string v12, "adb"

    const-string v13, "install"

    const-string v14, "media"

    const-string v15, "dhcp"

    const-string/jumbo v16, "sdcard_rw"

    const-string/jumbo v17, "vpn"

    const-string v18, "keystore"

    const-string/jumbo v19, "usb"

    const-string v20, "drm"

    const-string v21, "mdnsr"

    const-string v22, "gps"

    const/16 v23, 0x0

    const-string v24, "media_rw"

    const-string v25, "mtp"

    const/16 v26, 0x0

    const-string v27, "drmrpc"

    const-string v28, "nfc"

    const-string/jumbo v29, "sdcard_r"

    const-string v30, "clat"

    const-string v31, "loop_radio"

    const-string v32, "mediadrm"

    const-string v33, "package_info"

    const-string/jumbo v34, "sdcard_pics"

    const-string/jumbo v35, "sdcard_av"

    const-string/jumbo v36, "sdcard_all"

    const-string v37, "logd"

    const-string/jumbo v38, "shared_relro"

    const-string v39, "dbus"

    const-string/jumbo v40, "tlsdate"

    const-string v41, "mediaextractor"

    const-string v42, "audioserver"

    const-string v43, "metrics_collector"

    const-string v44, "metricsd"

    const-string/jumbo v45, "webservd"

    const-string v46, "debuggerd"

    const-string v47, "mediacodec"

    const-string v48, "cameraserver"

    const-string v49, "firewalld"

    const-string/jumbo v50, "trunksd"

    const-string v51, "nvram"

    const-string v52, "dns"

    const-string v53, "dns_tether"

    const-string/jumbo v54, "webview_zygote"

    const-string/jumbo v55, "vehicle_network"

    const-string v56, "media_audio"

    const-string v57, "media_vidio"

    const-string v58, "media_image"

    const-string/jumbo v59, "tombstoned"

    const-string v60, "media_obb"

    const-string v61, "ese"

    const-string v62, "ota_update"

    filled-new-array/range {v1 .. v62}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/utils/PackageUtils;->ANDROID_IDS_INFO:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static getPackageNameByUid(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uid"    # I

    .line 110
    if-nez p1, :cond_0

    .line 111
    const-string v0, "root"

    return-object v0

    .line 112
    :cond_0
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_2

    .line 113
    add-int/lit16 v0, p1, -0x3e8

    .line 114
    .local v0, "index":I
    if-ltz v0, :cond_1

    sget-object v1, Lcom/miui/server/migard/utils/PackageUtils;->ANDROID_IDS_INFO:[Ljava/lang/String;

    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 115
    aget-object v1, v1, v0

    return-object v1

    .line 117
    .end local v0    # "index":I
    :cond_1
    goto :goto_0

    .line 118
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "pkgs":[Ljava/lang/String;
    if-eqz v0, :cond_3

    array-length v1, v0

    if-lez v1, :cond_3

    .line 120
    const/4 v1, 0x0

    aget-object v1, v0, v1

    return-object v1

    .line 123
    .end local v0    # "pkgs":[Ljava/lang/String;
    :cond_3
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getPidByPackage(Landroid/content/Context;Ljava/lang/String;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .line 94
    const/4 v0, -0x1

    .line 96
    .local v0, "pid":I
    :try_start_0
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 97
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 98
    .local v3, "appProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v4, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    iget v2, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 100
    goto :goto_1

    .line 102
    .end local v3    # "appProcessInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_0
    goto :goto_0

    .line 105
    .end local v1    # "am":Landroid/app/ActivityManager;
    :cond_1
    :goto_1
    goto :goto_2

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/migard/utils/PackageUtils;->TAG:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v2, v3, v1}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    return v0
.end method

.method public static getUidByPackage(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .line 82
    const/4 v0, -0x1

    .line 84
    .local v0, "uid":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 85
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 86
    .local v2, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    iget v3, v2, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v3

    .line 89
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    .end local v2    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/miui/server/migard/utils/PackageUtils;->TAG:Ljava/lang/String;

    const-string v3, ""

    invoke-static {v2, v3, v1}, Lcom/miui/server/migard/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 90
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_0
    return v0
.end method
