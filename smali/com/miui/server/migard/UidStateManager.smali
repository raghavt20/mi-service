.class public Lcom/miui/server/migard/UidStateManager;
.super Ljava/lang/Object;
.source "UidStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/miui/server/migard/UidStateManager;


# instance fields
.field private mCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mUidObserver:Landroid/app/IUidObserver;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCallbacks(Lcom/miui/server/migard/UidStateManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/migard/UidStateManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 15
    const-class v0, Lcom/miui/server/migard/UidStateManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/UidStateManager;->TAG:Ljava/lang/String;

    .line 16
    new-instance v0, Lcom/miui/server/migard/UidStateManager;

    invoke-direct {v0}, Lcom/miui/server/migard/UidStateManager;-><init>()V

    sput-object v0, Lcom/miui/server/migard/UidStateManager;->sInstance:Lcom/miui/server/migard/UidStateManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    .line 29
    new-instance v0, Lcom/miui/server/migard/UidStateManager$1;

    invoke-direct {v0, p0}, Lcom/miui/server/migard/UidStateManager$1;-><init>(Lcom/miui/server/migard/UidStateManager;)V

    iput-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mUidObserver:Landroid/app/IUidObserver;

    return-void
.end method

.method public static getInstance()Lcom/miui/server/migard/UidStateManager;
    .locals 1

    .line 26
    sget-object v0, Lcom/miui/server/migard/UidStateManager;->sInstance:Lcom/miui/server/migard/UidStateManager;

    return-object v0
.end method


# virtual methods
.method public registerCallback(Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;)V
    .locals 5
    .param p1, "cb"    # Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;

    .line 70
    sget-object v0, Lcom/miui/server/migard/UidStateManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unregister callback, name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;->getCallbackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 73
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 74
    .local v0, "activityManager":Landroid/app/IActivityManager;
    iget-object v1, p0, Lcom/miui/server/migard/UidStateManager;->mUidObserver:Landroid/app/IUidObserver;

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-interface {v0, v1, v4, v2, v3}, Landroid/app/IActivityManager;->registerUidObserver(Landroid/app/IUidObserver;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    .end local v0    # "activityManager":Landroid/app/IActivityManager;
    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 81
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_1
    return-void
.end method

.method public unregisterCallback(Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;

    .line 86
    sget-object v0, Lcom/miui/server/migard/UidStateManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unregister callback, name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;->getCallbackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/UidStateManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 91
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 92
    .local v0, "activityManager":Landroid/app/IActivityManager;
    iget-object v1, p0, Lcom/miui/server/migard/UidStateManager;->mUidObserver:Landroid/app/IUidObserver;

    invoke-interface {v0, v1}, Landroid/app/IActivityManager;->unregisterUidObserver(Landroid/app/IUidObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    .end local v0    # "activityManager":Landroid/app/IActivityManager;
    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 97
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_0
    return-void
.end method
