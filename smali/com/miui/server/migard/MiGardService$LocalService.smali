.class final Lcom/miui/server/migard/MiGardService$LocalService;
.super Lcom/miui/server/migard/MiGardInternal;
.source "MiGardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/MiGardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LocalService"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/migard/MiGardService;


# direct methods
.method private constructor <init>(Lcom/miui/server/migard/MiGardService;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-direct {p0}, Lcom/miui/server/migard/MiGardInternal;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/migard/MiGardService;Lcom/miui/server/migard/MiGardService$LocalService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/MiGardService$LocalService;-><init>(Lcom/miui/server/migard/MiGardService;)V

    return-void
.end method


# virtual methods
.method public addGameProcessCompactor(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
    .locals 1
    .param p1, "compactorConfig"    # Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;

    .line 269
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-static {v0}, Lcom/miui/server/migard/MiGardService;->-$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/GameMemoryReclaimer;->addGameProcessCompactor(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V

    .line 270
    return-void
.end method

.method public addGameProcessKiller(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V
    .locals 1
    .param p1, "killerConfig"    # Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;

    .line 265
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-static {v0}, Lcom/miui/server/migard/MiGardService;->-$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/GameMemoryReclaimer;->addGameProcessKiller(Lcom/android/server/am/IGameProcessAction$IGameProcessActionConfig;)V

    .line 266
    return-void
.end method

.method public notifyGameBackground()V
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-static {v0}, Lcom/miui/server/migard/MiGardService;->-$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/am/GameMemoryReclaimer;->notifyGameBackground()V

    .line 258
    return-void
.end method

.method public notifyGameForeground(Ljava/lang/String;)V
    .locals 1
    .param p1, "game"    # Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-static {v0}, Lcom/miui/server/migard/MiGardService;->-$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/GameMemoryReclaimer;->notifyGameForeground(Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public notifyProcessDied(I)V
    .locals 1
    .param p1, "pid"    # I

    .line 273
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-static {v0}, Lcom/miui/server/migard/MiGardService;->-$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/am/GameMemoryReclaimer;->notifyProcessDied(I)V

    .line 274
    return-void
.end method

.method public onProcessKilled(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "pkg"    # Ljava/lang/String;
    .param p4, "reason"    # Ljava/lang/String;

    .line 240
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    iget-object v0, v0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0, p3, p4}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onProcessKilled(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public onProcessStart(IILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uid"    # I
    .param p2, "pid"    # I
    .param p3, "pkg"    # Ljava/lang/String;
    .param p4, "caller"    # Ljava/lang/String;

    .line 236
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    iget-object v0, v0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onProcessStart(IILjava/lang/String;Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public onVpnConnected(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "connected"    # Z

    .line 244
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "on vpn established, user="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MiGardService"

    invoke-static {v1, v0}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    iget-object v0, v0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onVpnConnected(Ljava/lang/String;Z)V

    .line 248
    :cond_0
    return-void
.end method

.method public reclaimBackgroundForGame(J)V
    .locals 1
    .param p1, "need"    # J

    .line 261
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService$LocalService;->this$0:Lcom/miui/server/migard/MiGardService;

    invoke-static {v0}, Lcom/miui/server/migard/MiGardService;->-$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/GameMemoryReclaimer;->reclaimBackground(J)V

    .line 262
    return-void
.end method
