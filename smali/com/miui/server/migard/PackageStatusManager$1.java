class com.miui.server.migard.PackageStatusManager$1 extends miui.process.IForegroundInfoListener$Stub {
	 /* .source "PackageStatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/PackageStatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.migard.PackageStatusManager this$0; //synthetic
/* # direct methods */
 com.miui.server.migard.PackageStatusManager$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/migard/PackageStatusManager; */
/* .line 27 */
this.this$0 = p1;
/* invoke-direct {p0}, Lmiui/process/IForegroundInfoListener$Stub;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onForegroundInfoChanged ( miui.process.ForegroundInfo p0 ) {
/* .locals 5 */
/* .param p1, "foregroundInfo" # Lmiui/process/ForegroundInfo; */
/* .line 30 */
com.miui.server.migard.PackageStatusManager .-$$Nest$sfgetTAG ( );
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "on foreground info changed, info:"; // const-string v2, "on foreground info changed, info:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1 ); // invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 31 */
v0 = this.this$0;
com.miui.server.migard.PackageStatusManager .-$$Nest$fgetmCallbacks ( v0 );
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_0
/* check-cast v1, Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback; */
/* .line 32 */
/* .local v1, "cb":Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback; */
/* iget v2, p1, Lmiui/process/ForegroundInfo;->mForegroundPid:I */
/* iget v3, p1, Lmiui/process/ForegroundInfo;->mForegroundUid:I */
v4 = this.mForegroundPackageName;
/* .line 34 */
} // .end local v1 # "cb":Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;
/* .line 35 */
} // :cond_0
return;
} // .end method
