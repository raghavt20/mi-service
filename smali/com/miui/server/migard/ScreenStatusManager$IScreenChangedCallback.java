public abstract class com.miui.server.migard.ScreenStatusManager$IScreenChangedCallback {
	 /* .source "ScreenStatusManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/ScreenStatusManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x609 */
/* name = "IScreenChangedCallback" */
} // .end annotation
/* # virtual methods */
public abstract java.lang.String getCallbackName ( ) {
} // .end method
public abstract void onScreenOff ( ) {
} // .end method
public abstract void onUserPresent ( ) {
} // .end method
