public class com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup implements com.miui.server.migard.PackageStatusManager$IForegroundChangedCallback {
	 /* .source "SurfaceFlingerSetCgroup.java" */
	 /* # interfaces */
	 /* # static fields */
	 private static final Integer GAME_SURFACEFLINGER_CANCEL_TO_LITTLE_CORE_CODE;
	 private static final Integer GAME_SURFACEFLINGER_TO_LITTLE_CORE_CODE;
	 private static final java.lang.String SURFACE_FLINGER;
	 private static final java.lang.String TAG;
	 private static java.util.Set mSupportProductName;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/Set<", */
	 /* "Ljava/lang/String;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
/* # instance fields */
private android.content.Context mContext;
private java.util.List mGameList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup ( ) {
/* .locals 2 */
/* .line 28 */
/* const-class v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 42 */
/* new-instance v0, Ljava/util/HashSet; */
/* invoke-direct {v0}, Ljava/util/HashSet;-><init>()V */
/* .line 44 */
final String v1 = "corot"; // const-string v1, "corot"
/* .line 45 */
v0 = com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup.mSupportProductName;
final String v1 = "miproduct_corot_cn"; // const-string v1, "miproduct_corot_cn"
/* .line 46 */
return;
} // .end method
public com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup ( ) {
/* .locals 2 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 35 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 32 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mGameList = v0;
/* .line 33 */
int v1 = 0; // const/4 v1, 0x0
this.mContext = v1;
/* .line 36 */
this.mContext = p1;
/* .line 37 */
final String v1 = "com.tencent.tmgp.pubgmhd"; // const-string v1, "com.tencent.tmgp.pubgmhd"
/* .line 38 */
v0 = this.mGameList;
final String v1 = "com.tencent.tmgp.sgame"; // const-string v1, "com.tencent.tmgp.sgame"
/* .line 39 */
return;
} // .end method
private void cancelSurfaceFlingerToLittleCore ( ) {
/* .locals 5 */
/* .line 73 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 74 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 75 */
android.os.Parcel .obtain ( );
/* .line 76 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 77 */
int v2 = 1; // const/4 v2, 0x1
(( android.os.Parcel ) v1 ).writeInt ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 79 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x798c */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 84 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 85 */
/* .line 84 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 81 */
/* :catch_0 */
/* move-exception v2 */
/* .line 82 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
v3 = com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup.TAG;
final String v4 = "Failed to set SurfaceFlinger"; // const-string v4, "Failed to set SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 84 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 85 */
/* throw v2 */
/* .line 88 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
private void setSurfaceFlingerToLittleCore ( ) {
/* .locals 5 */
/* .line 56 */
final String v0 = "SurfaceFlinger"; // const-string v0, "SurfaceFlinger"
android.os.ServiceManager .getService ( v0 );
/* .line 57 */
/* .local v0, "flinger":Landroid/os/IBinder; */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 58 */
android.os.Parcel .obtain ( );
/* .line 59 */
/* .local v1, "data":Landroid/os/Parcel; */
final String v2 = "android.ui.ISurfaceComposer"; // const-string v2, "android.ui.ISurfaceComposer"
(( android.os.Parcel ) v1 ).writeInterfaceToken ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V
/* .line 60 */
int v2 = 1; // const/4 v2, 0x1
(( android.os.Parcel ) v1 ).writeInt ( v2 ); // invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V
/* .line 62 */
int v2 = 0; // const/4 v2, 0x0
int v3 = 0; // const/4 v3, 0x0
/* const/16 v4, 0x798b */
try { // :try_start_0
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catch Ljava/lang/SecurityException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 67 */
/* nop */
} // :goto_0
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 68 */
/* .line 67 */
/* :catchall_0 */
/* move-exception v2 */
/* .line 64 */
/* :catch_0 */
/* move-exception v2 */
/* .line 65 */
/* .local v2, "ex":Ljava/lang/Exception; */
try { // :try_start_1
v3 = com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup.TAG;
final String v4 = "Failed to set sdr2hdr to SurfaceFlinger"; // const-string v4, "Failed to set sdr2hdr to SurfaceFlinger"
android.util.Slog .e ( v3,v4,v2 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 67 */
/* nop */
} // .end local v2 # "ex":Ljava/lang/Exception;
} // :goto_1
(( android.os.Parcel ) v1 ).recycle ( ); // invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V
/* .line 68 */
/* throw v2 */
/* .line 70 */
} // .end local v1 # "data":Landroid/os/Parcel;
} // :cond_0
} // :goto_2
return;
} // .end method
/* # virtual methods */
public Boolean SupportProduct ( ) {
/* .locals 2 */
/* .line 49 */
final String v0 = "ro.product.product.name"; // const-string v0, "ro.product.product.name"
android.os.SystemProperties .get ( v0 );
/* .line 50 */
/* .local v0, "product":Ljava/lang/String; */
v1 = v1 = com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup.mSupportProductName;
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 51 */
int v1 = 1; // const/4 v1, 0x1
/* .line 52 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
} // .end method
public java.lang.String getCallbackName ( ) {
/* .locals 1 */
/* .line 102 */
/* const-class v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
} // .end method
public void onForegroundChanged ( Integer p0, Integer p1, java.lang.String p2 ) {
/* .locals 2 */
/* .param p1, "pid" # I */
/* .param p2, "uid" # I */
/* .param p3, "name" # Ljava/lang/String; */
/* .line 92 */
v0 = v0 = this.mGameList;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 93 */
v0 = com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup.TAG;
/* const-string/jumbo v1, "start set SurfaceFlinger To Little Core..." */
android.util.Slog .i ( v0,v1 );
/* .line 94 */
/* invoke-direct {p0}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->setSurfaceFlingerToLittleCore()V */
/* .line 96 */
} // :cond_0
v0 = com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup.TAG;
/* const-string/jumbo v1, "start set SurfaceFlinger cancel To Little Core..." */
android.util.Slog .i ( v0,v1 );
/* .line 97 */
/* invoke-direct {p0}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->cancelSurfaceFlingerToLittleCore()V */
/* .line 99 */
} // :goto_0
return;
} // .end method
