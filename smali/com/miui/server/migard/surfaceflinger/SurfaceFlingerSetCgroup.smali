.class public Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;
.super Ljava/lang/Object;
.source "SurfaceFlingerSetCgroup.java"

# interfaces
.implements Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;


# static fields
.field private static final GAME_SURFACEFLINGER_CANCEL_TO_LITTLE_CORE_CODE:I = 0x798c

.field private static final GAME_SURFACEFLINGER_TO_LITTLE_CORE_CODE:I = 0x798b

.field private static final SURFACE_FLINGER:Ljava/lang/String; = "SurfaceFlinger"

.field private static final TAG:Ljava/lang/String;

.field private static mSupportProductName:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mGameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    const-class v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->TAG:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mSupportProductName:Ljava/util/Set;

    .line 44
    const-string v1, "corot"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mSupportProductName:Ljava/util/Set;

    const-string v1, "miproduct_corot_cn"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mGameList:Ljava/util/List;

    .line 33
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mContext:Landroid/content/Context;

    .line 36
    iput-object p1, p0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mContext:Landroid/content/Context;

    .line 37
    const-string v1, "com.tencent.tmgp.pubgmhd"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v0, p0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mGameList:Ljava/util/List;

    const-string v1, "com.tencent.tmgp.sgame"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method private cancelSurfaceFlingerToLittleCore()V
    .locals 5

    .line 73
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 74
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 75
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 76
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 77
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x798c

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 85
    goto :goto_2

    .line 84
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 81
    :catch_0
    move-exception v2

    .line 82
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->TAG:Ljava/lang/String;

    const-string v4, "Failed to set SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 85
    throw v2

    .line 88
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method

.method private setSurfaceFlingerToLittleCore()V
    .locals 5

    .line 56
    const-string v0, "SurfaceFlinger"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 57
    .local v0, "flinger":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 58
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 59
    .local v1, "data":Landroid/os/Parcel;
    const-string v2, "android.ui.ISurfaceComposer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 60
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x798b

    :try_start_0
    invoke-interface {v0, v4, v1, v2, v3}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    nop

    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 68
    goto :goto_2

    .line 67
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 64
    :catch_0
    move-exception v2

    .line 65
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->TAG:Ljava/lang/String;

    const-string v4, "Failed to set sdr2hdr to SurfaceFlinger"

    invoke-static {v3, v4, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    nop

    .end local v2    # "ex":Ljava/lang/Exception;
    goto :goto_0

    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 68
    throw v2

    .line 70
    .end local v1    # "data":Landroid/os/Parcel;
    :cond_0
    :goto_2
    return-void
.end method


# virtual methods
.method public SupportProduct()Z
    .locals 2

    .line 49
    const-string v0, "ro.product.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "product":Ljava/lang/String;
    sget-object v1, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mSupportProductName:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    const/4 v1, 0x1

    return v1

    .line 52
    :cond_0
    const/4 v1, 0x0

    return v1
.end method

.method public getCallbackName()Ljava/lang/String;
    .locals 1

    .line 102
    const-class v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onForegroundChanged(IILjava/lang/String;)V
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "name"    # Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->mGameList:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "start set SurfaceFlinger To Little Core..."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-direct {p0}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->setSurfaceFlingerToLittleCore()V

    goto :goto_0

    .line 96
    :cond_0
    sget-object v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "start set SurfaceFlinger cancel To Little Core..."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-direct {p0}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->cancelSurfaceFlingerToLittleCore()V

    .line 99
    :goto_0
    return-void
.end method
