.class public Lcom/miui/server/migard/PackageStatusManager;
.super Ljava/lang/Object;
.source "PackageStatusManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static sInstance:Lcom/miui/server/migard/PackageStatusManager;


# instance fields
.field private mCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;


# direct methods
.method static bridge synthetic -$$Nest$fgetmCallbacks(Lcom/miui/server/migard/PackageStatusManager;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetTAG()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/server/migard/PackageStatusManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 13
    const-class v0, Lcom/miui/server/migard/PackageStatusManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/miui/server/migard/PackageStatusManager;->TAG:Ljava/lang/String;

    .line 14
    new-instance v0, Lcom/miui/server/migard/PackageStatusManager;

    invoke-direct {v0}, Lcom/miui/server/migard/PackageStatusManager;-><init>()V

    sput-object v0, Lcom/miui/server/migard/PackageStatusManager;->sInstance:Lcom/miui/server/migard/PackageStatusManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    .line 27
    new-instance v0, Lcom/miui/server/migard/PackageStatusManager$1;

    invoke-direct {v0, p0}, Lcom/miui/server/migard/PackageStatusManager$1;-><init>(Lcom/miui/server/migard/PackageStatusManager;)V

    iput-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

    .line 39
    return-void
.end method

.method public static getInstance()Lcom/miui/server/migard/PackageStatusManager;
    .locals 1

    .line 24
    sget-object v0, Lcom/miui/server/migard/PackageStatusManager;->sInstance:Lcom/miui/server/migard/PackageStatusManager;

    return-object v0
.end method


# virtual methods
.method public registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;

    .line 42
    sget-object v0, Lcom/miui/server/migard/PackageStatusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Register callback, name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;->getCallbackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->registerForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_1
    return-void
.end method

.method public unregisterCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;

    .line 50
    sget-object v0, Lcom/miui/server/migard/PackageStatusManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unregister callback, name:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;->getCallbackName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/miui/server/migard/PackageStatusManager;->mForegroundInfoChangeListener:Lmiui/process/IForegroundInfoListener$Stub;

    invoke-static {v0}, Lmiui/process/ProcessManager;->unregisterForegroundInfoListener(Lmiui/process/IForegroundInfoListener;)V

    .line 55
    :cond_1
    return-void
.end method
