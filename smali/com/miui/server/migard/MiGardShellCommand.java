class com.miui.server.migard.MiGardShellCommand extends android.os.ShellCommand {
	 /* .source "MiGardShellCommand.java" */
	 /* # instance fields */
	 com.miui.server.migard.MiGardService mService;
	 /* # direct methods */
	 com.miui.server.migard.MiGardShellCommand ( ) {
		 /* .locals 0 */
		 /* .param p1, "service" # Lcom/miui/server/migard/MiGardService; */
		 /* .line 9 */
		 /* invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V */
		 /* .line 10 */
		 this.mService = p1;
		 /* .line 11 */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer onCommand ( java.lang.String p0 ) {
		 /* .locals 6 */
		 /* .param p1, "cmd" # Ljava/lang/String; */
		 /* .line 15 */
		 (( com.miui.server.migard.MiGardShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
		 /* .line 16 */
		 /* .local v0, "pw":Ljava/io/PrintWriter; */
		 /* if-nez p1, :cond_0 */
		 /* .line 17 */
		 v1 = 		 (( com.miui.server.migard.MiGardShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/migard/MiGardShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
		 /* .line 19 */
	 } // :cond_0
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 20 */
	 /* .local v1, "async":Z */
	 int v2 = 0; // const/4 v2, 0x0
	 /* .line 22 */
	 /* .local v2, "compressed":Z */
	 int v3 = 0; // const/4 v3, 0x0
	 try { // :try_start_0
		 v4 = 		 (( java.lang.String ) p1 ).hashCode ( ); // invoke-virtual {p1}, Ljava/lang/String;->hashCode()I
		 /* sparse-switch v4, :sswitch_data_0 */
	 } // :cond_1
	 /* :sswitch_0 */
	 /* const-string/jumbo v4, "stop-trace" */
	 v4 = 	 (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_1
		 int v4 = 1; // const/4 v4, 0x1
		 /* :sswitch_1 */
		 /* const-string/jumbo v4, "start-trace" */
		 v4 = 		 (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v4 != null) { // if-eqz v4, :cond_1
			 /* move v4, v3 */
			 /* :sswitch_2 */
			 final String v4 = "dump-trace"; // const-string v4, "dump-trace"
			 v4 = 			 (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v4 != null) { // if-eqz v4, :cond_1
				 int v4 = 2; // const/4 v4, 0x2
				 /* :sswitch_3 */
				 /* const-string/jumbo v4, "trace-buffer-size" */
				 v4 = 				 (( java.lang.String ) p1 ).equals ( v4 ); // invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v4 != null) { // if-eqz v4, :cond_1
					 int v4 = 3; // const/4 v4, 0x3
				 } // :goto_0
				 int v4 = -1; // const/4 v4, -0x1
			 } // :goto_1
			 /* packed-switch v4, :pswitch_data_0 */
			 /* .line 43 */
			 /* .line 36 */
			 /* :pswitch_0 */
			 v4 = this.mService;
			 (( com.miui.server.migard.MiGardShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;
			 v5 = 			 java.lang.Integer .parseInt ( v5 );
			 (( com.miui.server.migard.MiGardService ) v4 ).setTraceBufferSize ( v5 ); // invoke-virtual {v4, v5}, Lcom/miui/server/migard/MiGardService;->setTraceBufferSize(I)V
			 /* .line 37 */
			 /* .line 32 */
			 /* :pswitch_1 */
			 (( com.miui.server.migard.MiGardShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;
			 v4 = 			 java.lang.Boolean .parseBoolean ( v4 );
			 /* move v2, v4 */
			 /* .line 33 */
			 v4 = this.mService;
			 (( com.miui.server.migard.MiGardService ) v4 ).dumpTrace ( v2 ); // invoke-virtual {v4, v2}, Lcom/miui/server/migard/MiGardService;->dumpTrace(Z)V
			 /* .line 34 */
			 /* .line 28 */
			 /* :pswitch_2 */
			 (( com.miui.server.migard.MiGardShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;
			 v4 = 			 java.lang.Boolean .parseBoolean ( v4 );
			 /* move v2, v4 */
			 /* .line 29 */
			 v4 = this.mService;
			 (( com.miui.server.migard.MiGardService ) v4 ).stopTrace ( v2 ); // invoke-virtual {v4, v2}, Lcom/miui/server/migard/MiGardService;->stopTrace(Z)V
			 /* .line 30 */
			 /* .line 24 */
			 /* :pswitch_3 */
			 (( com.miui.server.migard.MiGardShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;
			 v4 = 			 java.lang.Boolean .parseBoolean ( v4 );
			 /* move v1, v4 */
			 /* .line 25 */
			 v4 = this.mService;
			 (( com.miui.server.migard.MiGardService ) v4 ).startDefaultTrace ( v1 ); // invoke-virtual {v4, v1}, Lcom/miui/server/migard/MiGardService;->startDefaultTrace(Z)V
			 /* :try_end_0 */
			 /* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
			 /* .line 26 */
			 /* .line 41 */
			 /* :catch_0 */
			 /* move-exception v4 */
			 /* .line 42 */
			 /* .local v4, "e":Ljava/lang/Exception; */
			 (( java.io.PrintWriter ) v0 ).println ( v4 ); // invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
			 /* .line 46 */
		 } // .end local v4 # "e":Ljava/lang/Exception;
	 } // :goto_2
	 v4 = this.mService;
	 v4 = this.mMemCleaner;
	 (( com.miui.server.migard.MiGardShellCommand ) p0 ).getNextArgRequired ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;
	 v4 = 	 (( com.miui.server.migard.memory.GameMemoryCleaner ) v4 ).onShellCommand ( p1, v5, v0 ); // invoke-virtual {v4, p1, v5, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onShellCommand(Ljava/lang/String;Ljava/lang/String;Ljava/io/PrintWriter;)Z
	 if ( v4 != null) { // if-eqz v4, :cond_2
		 /* .line 47 */
		 /* .line 49 */
	 } // :cond_2
	 v3 = 	 (( com.miui.server.migard.MiGardShellCommand ) p0 ).handleDefaultCommands ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/migard/MiGardShellCommand;->handleDefaultCommands(Ljava/lang/String;)I
	 /* nop */
	 /* :sswitch_data_0 */
	 /* .sparse-switch */
	 /* -0x6717a71a -> :sswitch_3 */
	 /* -0x1d0d07d4 -> :sswitch_2 */
	 /* 0x4fec781a -> :sswitch_1 */
	 /* 0x6ebe83ba -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void onHelp ( ) {
/* .locals 2 */
/* .line 54 */
(( com.miui.server.migard.MiGardShellCommand ) p0 ).getOutPrintWriter ( ); // invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;
/* .line 55 */
/* .local v0, "pw":Ljava/io/PrintWriter; */
final String v1 = "MiGardService commands:"; // const-string v1, "MiGardService commands:"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 56 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 57 */
/* const-string/jumbo v1, "start-trace [async=true|false]" */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 58 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 59 */
/* const-string/jumbo v1, "stop-trace [compressed=true|false]" */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 60 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 61 */
final String v1 = "dump-trace [compressed=true|false]"; // const-string v1, "dump-trace [compressed=true|false]"
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 62 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 63 */
/* const-string/jumbo v1, "trace-buffer-size [size KB]" */
(( java.io.PrintWriter ) v0 ).println ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
/* .line 64 */
(( java.io.PrintWriter ) v0 ).println ( ); // invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V
/* .line 65 */
v1 = this.mService;
v1 = this.mMemCleaner;
(( com.miui.server.migard.memory.GameMemoryCleaner ) v1 ).onShellHelp ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onShellHelp(Ljava/io/PrintWriter;)V
/* .line 66 */
return;
} // .end method
