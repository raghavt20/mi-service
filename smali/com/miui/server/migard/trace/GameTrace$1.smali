.class Lcom/miui/server/migard/trace/GameTrace$1;
.super Ljava/lang/Object;
.source "GameTrace.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/migard/trace/GameTrace;->getFileSort(Ljava/lang/String;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/migard/trace/GameTrace;


# direct methods
.method constructor <init>(Lcom/miui/server/migard/trace/GameTrace;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/migard/trace/GameTrace;

    .line 168
    iput-object p1, p0, Lcom/miui/server/migard/trace/GameTrace$1;->this$0:Lcom/miui/server/migard/trace/GameTrace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/io/File;Ljava/io/File;)I
    .locals 4
    .param p1, "file"    # Ljava/io/File;
    .param p2, "newFile"    # Ljava/io/File;

    .line 170
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 171
    const/4 v0, 0x1

    return v0

    .line 172
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 173
    const/4 v0, 0x0

    return v0

    .line 175
    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 168
    check-cast p1, Ljava/io/File;

    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Lcom/miui/server/migard/trace/GameTrace$1;->compare(Ljava/io/File;Ljava/io/File;)I

    move-result p1

    return p1
.end method
