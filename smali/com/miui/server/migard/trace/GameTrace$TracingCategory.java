class com.miui.server.migard.trace.GameTrace$TracingCategory {
	 /* .source "GameTrace.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/migard/trace/GameTrace; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0xa */
/* name = "TracingCategory" */
} // .end annotation
/* # instance fields */
java.lang.String name;
java.util.HashMap pathConfig;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
Integer tags;
/* # direct methods */
 com.miui.server.migard.trace.GameTrace$TracingCategory ( ) {
/* .locals 1 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "tags" # I */
/* .line 676 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 677 */
this.name = p1;
/* .line 678 */
/* iput p2, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->tags:I */
/* .line 679 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.pathConfig = v0;
/* .line 680 */
return;
} // .end method
 com.miui.server.migard.trace.GameTrace$TracingCategory ( ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .param p2, "tags" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "I", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 681 */
/* .local p3, "pathConfig":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 682 */
this.name = p1;
/* .line 683 */
/* iput p2, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->tags:I */
/* .line 684 */
this.pathConfig = p3;
/* .line 685 */
return;
} // .end method
/* # virtual methods */
void addPath ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 687 */
int v0 = 0; // const/4 v0, 0x0
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) p0 ).addPath ( p1, v0 ); // invoke-virtual {p0, p1, v0}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 688 */
return;
} // .end method
void addPath ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "on" # Z */
/* .line 690 */
v0 = this.pathConfig;
java.lang.Boolean .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 691 */
return;
} // .end method
java.util.ArrayList getEnabledTracePathList ( ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 709 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 710 */
/* .local v0, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v1 = this.pathConfig;
(( java.util.HashMap ) v1 ).entrySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_1
/* check-cast v2, Ljava/util/Map$Entry; */
/* .line 711 */
/* .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* check-cast v3, Ljava/lang/Boolean; */
v3 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 712 */
/* check-cast v3, Ljava/lang/String; */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 713 */
} // .end local v2 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
} // :cond_0
/* .line 714 */
} // :cond_1
} // .end method
Integer getTags ( ) {
/* .locals 1 */
/* .line 703 */
/* iget v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->tags:I */
} // .end method
java.util.ArrayList getTracePathList ( ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 717 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 718 */
/* .local v0, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v1 = this.pathConfig;
(( java.util.HashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/String; */
/* .line 719 */
/* .local v2, "p":Ljava/lang/String; */
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 720 */
} // .end local v2 # "p":Ljava/lang/String;
/* .line 721 */
} // :cond_0
} // .end method
java.util.HashMap getTracePathMap ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 706 */
v0 = this.pathConfig;
} // .end method
void removePath ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 700 */
v0 = this.pathConfig;
(( java.util.HashMap ) v0 ).remove ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 701 */
return;
} // .end method
Boolean setPathEnabled ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "on" # Z */
/* .line 693 */
v0 = this.pathConfig;
v0 = (( java.util.HashMap ) v0 ).containsKey ( p1 ); // invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 694 */
v0 = this.pathConfig;
java.lang.Boolean .valueOf ( p2 );
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 695 */
int v0 = 1; // const/4 v0, 0x1
/* .line 697 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
