public class com.miui.server.migard.trace.GameTrace {
	 /* .source "GameTrace.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String ADD_KERNEL_CATEGORY;
private static final java.lang.String ADD_KERNEL_CATEGORY_PATH;
private static final java.lang.String AES_ALGO;
private static final Integer ATRACE_TAG_ACTIVITY_MANAGER;
private static final Integer ATRACE_TAG_ADB;
private static final Integer ATRACE_TAG_AIDL;
private static final Integer ATRACE_TAG_AUDIO;
private static final Integer ATRACE_TAG_BIONIC;
private static final Integer ATRACE_TAG_CAMERA;
private static final Integer ATRACE_TAG_DALVIK;
private static final Integer ATRACE_TAG_DATABASE;
private static final Integer ATRACE_TAG_GRAPHICS;
private static final Integer ATRACE_TAG_HAL;
private static final Integer ATRACE_TAG_INPUT;
private static final Integer ATRACE_TAG_NETWORK;
private static final Integer ATRACE_TAG_NEVER;
private static final Integer ATRACE_TAG_NNAPI;
private static final Integer ATRACE_TAG_PACKAGE_MANAGER;
private static final Integer ATRACE_TAG_POWER;
private static final Integer ATRACE_TAG_RESOURCES;
private static final Integer ATRACE_TAG_RRO;
private static final Integer ATRACE_TAG_RS;
private static final Integer ATRACE_TAG_SYNC_MANAGER;
private static final Integer ATRACE_TAG_SYSTEM_SERVER;
private static final Integer ATRACE_TAG_VIBRATOR;
private static final Integer ATRACE_TAG_VIDEO;
private static final Integer ATRACE_TAG_VIEW;
private static final Integer ATRACE_TAG_WEBVIEW;
private static final Integer ATRACE_TAG_WINDOW_MANAGER;
private static final java.lang.String CATETORY_KEY;
private static final java.lang.String COMMANDS_AS_ROOT_FILE_PATH;
private static final java.lang.String CURRENT_TRACER_PATH;
private static final java.lang.String DEBUG_FS_PATH;
private static final java.lang.String DETAILS_KEY;
private static final java.lang.String ENABLED_KEY;
private static final java.lang.String ENABLE_KERNEL_CATEGORY_PATH;
private static final java.lang.String FTRACE_FILTER_PATH;
private static final Integer MAX_TRACE_SIZE;
private static final java.lang.String NON_ALGO;
private static final java.lang.String PATH_KEY;
private static final java.lang.String REMOVE_KERNEL_CATEGORY_PATH;
private static final java.lang.String SET_BUFFER_SIZE;
private static final java.lang.String SET_CRYPTO_ALGO;
private static final java.lang.String SET_TRACE_DIR;
private static final java.lang.String TAG;
private static final Integer TRACE_BUFFER_SIZE;
private static final java.lang.String TRACE_BUFFER_SIZE_PATH;
private static final java.lang.String TRACE_CLOCK_PATH;
private static final java.lang.String TRACE_FILE_SAVE_PATH;
private static final java.lang.String TRACE_FS_PATH;
private static final java.lang.String TRACE_MARKER_PATH;
private static final java.lang.String TRACE_OVERWRITE_PATH;
private static final java.lang.String TRACE_PATH;
private static final java.lang.String TRACE_PRINT_TGID_PATH;
private static final java.lang.String TRACE_RECORD_TGID_PATH;
private static final java.lang.String TRACE_TAGS_PROPERTY;
private static final java.lang.String TRACING_ON_PATH;
private static final java.lang.String USER_INITIATED_PROPERTY;
private static final java.lang.String XOR_ALGO;
private static com.miui.server.migard.trace.GameTrace sInstance;
/* # instance fields */
private Boolean isAsync;
private volatile Boolean isGameTraceRunning;
private Boolean isTraceSupported;
private final java.util.HashMap mCategories;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/migard/trace/GameTrace$TracingCategory;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private final java.lang.String mModuleName;
private Integer mTraceBufferSizeKB;
private java.lang.String mTraceCryptoAlgo;
private java.lang.String mTraceDir;
private java.lang.String mTraceFolder;
/* # direct methods */
static com.miui.server.migard.trace.GameTrace ( ) {
/* .locals 1 */
/* .line 101 */
/* new-instance v0, Lcom/miui/server/migard/trace/GameTrace; */
/* invoke-direct {v0}, Lcom/miui/server/migard/trace/GameTrace;-><init>()V */
return;
} // .end method
private com.miui.server.migard.trace.GameTrace ( ) {
/* .locals 3 */
/* .line 103 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 92 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
this.mCategories = v0;
/* .line 95 */
int v0 = 0; // const/4 v0, 0x0
/* iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z */
/* .line 98 */
int v1 = 0; // const/4 v1, 0x0
this.mTraceDir = v1;
/* .line 99 */
this.mTraceCryptoAlgo = v1;
/* .line 100 */
final String v1 = "ro.product.model"; // const-string v1, "ro.product.model"
final String v2 = ""; // const-string v2, ""
android.os.SystemProperties .get ( v1,v2 );
this.mModuleName = v1;
/* .line 104 */
/* iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
/* .line 105 */
/* const/16 v0, 0x800 */
(( com.miui.server.migard.trace.GameTrace ) p0 ).setBufferSize ( v0 ); // invoke-virtual {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setBufferSize(I)V
/* .line 106 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->initTracingCategories()V */
/* .line 107 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->initTraceFolder()V */
/* .line 108 */
return;
} // .end method
private void addKernelCategory ( org.json.JSONObject p0 ) {
/* .locals 8 */
/* .param p1, "content" # Lorg/json/JSONObject; */
/* .line 293 */
try { // :try_start_0
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 297 */
/* .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
final String v1 = "category"; // const-string v1, "category"
(( org.json.JSONObject ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 298 */
/* .local v1, "category":Ljava/lang/String; */
final String v2 = "details"; // const-string v2, "details"
(( org.json.JSONObject ) p1 ).getJSONArray ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
/* .line 299 */
/* .local v2, "details":Lorg/json/JSONArray; */
int v3 = 0; // const/4 v3, 0x0
/* .local v3, "i":I */
} // :goto_0
v4 = (( org.json.JSONArray ) v2 ).length ( ); // invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
/* if-ge v3, v4, :cond_1 */
/* .line 300 */
(( org.json.JSONArray ) v2 ).getJSONObject ( v3 ); // invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
/* .line 301 */
/* .local v4, "temp":Lorg/json/JSONObject; */
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 302 */
final String v5 = "path"; // const-string v5, "path"
(( org.json.JSONObject ) v4 ).getString ( v5 ); // invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 303 */
/* .local v5, "path":Ljava/lang/String; */
final String v6 = "enabled"; // const-string v6, "enabled"
v6 = (( org.json.JSONObject ) v4 ).getBoolean ( v6 ); // invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
/* .line 304 */
/* .local v6, "enabled":Z */
java.lang.Boolean .valueOf ( v6 );
(( java.util.HashMap ) v0 ).put ( v5, v7 ); // invoke-virtual {v0, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 299 */
} // .end local v5 # "path":Ljava/lang/String;
} // .end local v6 # "enabled":Z
} // :cond_0
/* add-int/lit8 v3, v3, 0x1 */
/* .line 307 */
} // .end local v3 # "i":I
} // .end local v4 # "temp":Lorg/json/JSONObject;
} // :cond_1
(( com.miui.server.migard.trace.GameTrace ) p0 ).addKernelCategory ( v1, v0 ); // invoke-virtual {p0, v1, v0}, Lcom/miui/server/migard/trace/GameTrace;->addKernelCategory(Ljava/lang/String;Ljava/util/HashMap;)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 310 */
} // .end local v0 # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
} // .end local v1 # "category":Ljava/lang/String;
} // .end local v2 # "details":Lorg/json/JSONArray;
/* .line 308 */
/* :catch_0 */
/* move-exception v0 */
/* .line 309 */
/* .local v0, "ex":Lorg/json/JSONException; */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
final String v2 = "parse config failed, add Kernel Category failed."; // const-string v2, "parse config failed, add Kernel Category failed."
android.util.Slog .e ( v1,v2 );
/* .line 311 */
} // .end local v0 # "ex":Lorg/json/JSONException;
} // :goto_1
return;
} // .end method
private void addKernelCategoryPath ( org.json.JSONObject p0 ) {
/* .locals 3 */
/* .param p1, "content" # Lorg/json/JSONObject; */
/* .line 315 */
try { // :try_start_0
final String v0 = "category"; // const-string v0, "category"
(( org.json.JSONObject ) p1 ).getString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 316 */
/* .local v0, "category":Ljava/lang/String; */
final String v1 = "path"; // const-string v1, "path"
(( org.json.JSONObject ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 317 */
/* .local v1, "path":Ljava/lang/String; */
final String v2 = "enabled"; // const-string v2, "enabled"
v2 = (( org.json.JSONObject ) p1 ).getBoolean ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
/* .line 318 */
/* .local v2, "enabled":Z */
(( com.miui.server.migard.trace.GameTrace ) p0 ).addKernelCategoryPath ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace;->addKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;Z)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 321 */
} // .end local v0 # "category":Ljava/lang/String;
} // .end local v1 # "path":Ljava/lang/String;
} // .end local v2 # "enabled":Z
/* .line 319 */
/* :catch_0 */
/* move-exception v0 */
/* .line 320 */
/* .local v0, "ex":Lorg/json/JSONException; */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
final String v2 = "parse config failed, add kernel category path failed."; // const-string v2, "parse config failed, add kernel category path failed."
android.util.Slog .e ( v1,v2 );
/* .line 322 */
} // .end local v0 # "ex":Lorg/json/JSONException;
} // :goto_0
return;
} // .end method
private void applyTraceBufferSize ( ) {
/* .locals 2 */
/* .line 477 */
/* iget v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceBufferSizeKB:I */
java.lang.Integer .toString ( v0 );
final String v1 = "buffer_size_kb"; // const-string v1, "buffer_size_kb"
/* invoke-direct {p0, v1, v0}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 478 */
return;
} // .end method
private java.lang.String checkCryptoAlgo ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "algo" # Ljava/lang/String; */
/* .line 217 */
/* const-string/jumbo v0, "xor" */
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
/* if-nez v0, :cond_1 */
final String v0 = "aes"; // const-string v0, "aes"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 221 */
} // :cond_0
final String v0 = "non"; // const-string v0, "non"
/* .line 218 */
} // :cond_1
} // :goto_0
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "checkCryptoAlgo algo: "; // const-string v1, "checkCryptoAlgo algo: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
android.util.Slog .i ( v1,v0 );
/* .line 219 */
} // .end method
private java.lang.String checkTraceDir ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 202 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "checkTraceDir path:"; // const-string v1, "checkTraceDir path:"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
android.util.Slog .i ( v1,v0 );
/* .line 203 */
int v0 = 0; // const/4 v0, 0x0
if ( p1 != null) { // if-eqz p1, :cond_0
/* .line 204 */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 205 */
/* .local v2, "dir":Ljava/io/File; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "dir.exists: "; // const-string v4, "dir.exists: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
final String v4 = " dir: "; // const-string v4, " dir: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 206 */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* if-nez v3, :cond_0 */
/* .line 207 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "path: "; // const-string v4, "path: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v3 );
/* .line 208 */
(( java.io.File ) v2 ).mkdirs ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
/* .line 210 */
/* .line 213 */
} // .end local v2 # "dir":Ljava/io/File;
} // :cond_0
} // .end method
private void cleanUpKernelCategories ( ) {
/* .locals 1 */
/* .line 532 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->disableAllKernelCategories()V */
/* .line 533 */
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setTraceOverwriteEnable(Z)V */
/* .line 534 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->resetBufferSize()V */
/* .line 535 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setPrintTgidEnableIfPresent(Z)V */
/* .line 536 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearKernelTraceFuncs()V */
/* .line 537 */
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setUserInitiatedTraceProperty(Z)V */
/* .line 538 */
return;
} // .end method
private void cleanUpUserspaceCategories ( ) {
/* .locals 1 */
/* .line 511 */
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setupUserspaceCategories(I)V */
/* .line 512 */
return;
} // .end method
private void clearKernelTraceFuncs ( ) {
/* .locals 5 */
/* .line 435 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mTraceFolder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "current_tracer"; // const-string v2, "current_tracer"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 436 */
/* .local v0, "cfile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 437 */
final String v1 = "nop"; // const-string v1, "nop"
/* invoke-direct {p0, v2, v1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 439 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mTraceFolder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v3, "set_ftrace_filter" */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 440 */
/* .local v1, "ffile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 441 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v4 = this.mTraceFolder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.FileUtils .truncateSysFile ( v2 );
/* .line 443 */
} // :cond_1
return;
} // .end method
private void clearOldTrace ( ) {
/* .locals 3 */
/* .line 156 */
final String v0 = "GameTrace"; // const-string v0, "GameTrace"
/* const-string/jumbo v1, "start clearOldTrace." */
android.util.Slog .i ( v0,v1 );
/* .line 157 */
final String v0 = "/data/system/migard/game_trace"; // const-string v0, "/data/system/migard/game_trace"
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->getFileSort(Ljava/lang/String;)Ljava/util/List; */
/* .line 158 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .local v1, "i":I */
v2 = } // :goto_0
/* if-ge v1, v2, :cond_1 */
/* .line 159 */
int v2 = 5; // const/4 v2, 0x5
/* if-lt v1, v2, :cond_0 */
/* .line 160 */
/* check-cast v2, Ljava/io/File; */
(( java.io.File ) v2 ).delete ( ); // invoke-virtual {v2}, Ljava/io/File;->delete()Z
/* .line 158 */
} // :cond_0
/* add-int/lit8 v1, v1, 0x1 */
/* .line 163 */
} // .end local v1 # "i":I
} // :cond_1
return;
} // .end method
private void clearTrace ( ) {
/* .locals 2 */
/* .line 541 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mTraceFolder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "trace" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.FileUtils .truncateSysFile ( v0 );
/* .line 542 */
return;
} // .end method
private void disableAllKernelCategories ( ) {
/* .locals 4 */
/* .line 522 */
v0 = this.mCategories;
(( java.util.HashMap ) v0 ).entrySet ( ); // invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_2
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 523 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/trace/GameTrace$TracingCategory;>;" */
/* check-cast v2, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* .line 524 */
/* .local v2, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* if-nez v2, :cond_0 */
/* .line 525 */
} // :cond_0
v3 = (( com.miui.server.migard.trace.GameTrace$TracingCategory ) v2 ).getTags ( ); // invoke-virtual {v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTags()I
/* if-nez v3, :cond_1 */
/* .line 526 */
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v2 ).getTracePathList ( ); // invoke-virtual {v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTracePathList()Ljava/util/ArrayList;
/* invoke-direct {p0, v3}, Lcom/miui/server/migard/trace/GameTrace;->disableKernelCategories(Ljava/util/ArrayList;)V */
/* .line 528 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/trace/GameTrace$TracingCategory;>;"
} // :cond_1
/* .line 529 */
} // .end local v2 # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
} // :cond_2
return;
} // .end method
private void disableKernelCategories ( java.util.ArrayList p0 ) {
/* .locals 3 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 515 */
/* .local p1, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
int v0 = 0; // const/4 v0, 0x0
/* .local v0, "i":I */
} // :goto_0
v1 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* if-ge v0, v1, :cond_0 */
/* .line 516 */
(( java.util.ArrayList ) p1 ).get ( v0 ); // invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/String; */
final String v2 = "0"; // const-string v2, "0"
com.miui.server.migard.utils.FileUtils .writeToSys ( v1,v2 );
/* .line 515 */
/* add-int/lit8 v0, v0, 0x1 */
/* .line 518 */
} // .end local v0 # "i":I
} // :cond_0
return;
} // .end method
private void enableKernelCategoryPath ( org.json.JSONObject p0 ) {
/* .locals 3 */
/* .param p1, "content" # Lorg/json/JSONObject; */
/* .line 326 */
try { // :try_start_0
final String v0 = "category"; // const-string v0, "category"
(( org.json.JSONObject ) p1 ).getString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 327 */
/* .local v0, "category":Ljava/lang/String; */
final String v1 = "path"; // const-string v1, "path"
(( org.json.JSONObject ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 328 */
/* .local v1, "path":Ljava/lang/String; */
final String v2 = "enabled"; // const-string v2, "enabled"
v2 = (( org.json.JSONObject ) p1 ).getBoolean ( v2 ); // invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z
/* .line 329 */
/* .local v2, "enabled":Z */
(( com.miui.server.migard.trace.GameTrace ) p0 ).enableKernelCategoryPath ( v0, v1, v2 ); // invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace;->enableKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;Z)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 332 */
} // .end local v0 # "category":Ljava/lang/String;
} // .end local v1 # "path":Ljava/lang/String;
} // .end local v2 # "enabled":Z
/* .line 330 */
/* :catch_0 */
/* move-exception v0 */
/* .line 331 */
/* .local v0, "ex":Lorg/json/JSONException; */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
final String v2 = "parse config failed, enable kernel category path failed."; // const-string v2, "parse config failed, enable kernel category path failed."
android.util.Slog .e ( v1,v2 );
/* .line 333 */
} // .end local v0 # "ex":Lorg/json/JSONException;
} // :goto_0
return;
} // .end method
private java.lang.String generateTraceName ( ) {
/* .locals 4 */
/* .line 481 */
/* new-instance v0, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v1, "yyyyMMddHHmmssSSS" */
/* invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 482 */
/* .local v0, "formatter":Ljava/text/SimpleDateFormat; */
/* new-instance v1, Ljava/util/Date; */
/* invoke-direct {v1}, Ljava/util/Date;-><init>()V */
(( java.text.SimpleDateFormat ) v0 ).format ( v1 ); // invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
/* .line 483 */
/* .local v1, "dateString":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "_trace"; // const-string v3, "_trace"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private java.util.List getFileSort ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "path" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* ")", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 166 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* invoke-direct {p0, p1, v0}, Lcom/miui/server/migard/trace/GameTrace;->getFiles(Ljava/lang/String;Ljava/util/List;)Ljava/util/List; */
/* .line 167 */
/* .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
v1 = if ( v0 != null) { // if-eqz v0, :cond_0
/* if-lez v1, :cond_0 */
/* .line 168 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$1; */
/* invoke-direct {v1, p0}, Lcom/miui/server/migard/trace/GameTrace$1;-><init>(Lcom/miui/server/migard/trace/GameTrace;)V */
java.util.Collections .sort ( v0,v1 );
/* .line 183 */
} // :cond_0
} // .end method
private java.util.List getFiles ( java.lang.String p0, java.util.List p1 ) {
/* .locals 6 */
/* .param p1, "realpath" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;)", */
/* "Ljava/util/List<", */
/* "Ljava/io/File;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 187 */
/* .local p2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;" */
/* new-instance v0, Ljava/io/File; */
/* invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 188 */
/* .local v0, "realFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).isDirectory ( ); // invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 189 */
(( java.io.File ) v0 ).listFiles ( ); // invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 190 */
/* .local v1, "subfiles":[Ljava/io/File; */
/* array-length v2, v1 */
int v3 = 0; // const/4 v3, 0x0
} // :goto_0
/* if-ge v3, v2, :cond_1 */
/* aget-object v4, v1, v3 */
/* .line 191 */
/* .local v4, "file":Ljava/io/File; */
v5 = (( java.io.File ) v4 ).isDirectory ( ); // invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z
if ( v5 != null) { // if-eqz v5, :cond_0
/* .line 192 */
(( java.io.File ) v4 ).getAbsolutePath ( ); // invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* invoke-direct {p0, v5, p2}, Lcom/miui/server/migard/trace/GameTrace;->getFiles(Ljava/lang/String;Ljava/util/List;)Ljava/util/List; */
/* .line 194 */
} // :cond_0
/* .line 190 */
} // .end local v4 # "file":Ljava/io/File;
} // :goto_1
/* add-int/lit8 v3, v3, 0x1 */
/* .line 198 */
} // .end local v1 # "subfiles":[Ljava/io/File;
} // :cond_1
} // .end method
public static com.miui.server.migard.trace.GameTrace getInstance ( ) {
/* .locals 1 */
/* .line 111 */
v0 = com.miui.server.migard.trace.GameTrace.sInstance;
} // .end method
private void initTraceFolder ( ) {
/* .locals 3 */
/* .line 660 */
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isTraceSupported:Z */
/* .line 661 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/sys/kernel/tracing"; // const-string v1, "/sys/kernel/tracing"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 662 */
/* .local v0, "traceFile":Ljava/io/File; */
/* new-instance v1, Ljava/io/File; */
final String v2 = "/sys/kernel/debug/tracing"; // const-string v2, "/sys/kernel/debug/tracing"
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 663 */
/* .local v1, "debugFile":Ljava/io/File; */
v2 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 664 */
final String v2 = "/sys/kernel/tracing/"; // const-string v2, "/sys/kernel/tracing/"
this.mTraceFolder = v2;
/* .line 665 */
} // :cond_0
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 666 */
final String v2 = "/sys/kernel/debug/tracing/"; // const-string v2, "/sys/kernel/debug/tracing/"
this.mTraceFolder = v2;
/* .line 668 */
} // :cond_1
int v2 = 0; // const/4 v2, 0x0
/* iput-boolean v2, p0, Lcom/miui/server/migard/trace/GameTrace;->isTraceSupported:Z */
/* .line 670 */
} // :goto_0
return;
} // .end method
private void initTracingCategories ( ) {
/* .locals 5 */
/* .line 546 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
int v2 = 2; // const/4 v2, 0x2
final String v3 = "gfx"; // const-string v3, "gfx"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 547 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
int v2 = 4; // const/4 v2, 0x4
final String v3 = "input"; // const-string v3, "input"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 548 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x8 */
/* const-string/jumbo v3, "view" */
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 549 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x10 */
/* const-string/jumbo v3, "webview" */
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 550 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x20 */
/* const-string/jumbo v3, "wm" */
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 551 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x40 */
final String v3 = "am"; // const-string v3, "am"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 552 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x80 */
/* const-string/jumbo v3, "sm" */
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 553 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x100 */
final String v3 = "audio"; // const-string v3, "audio"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 554 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x200 */
/* const-string/jumbo v3, "video" */
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 555 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x400 */
final String v3 = "camera"; // const-string v3, "camera"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 556 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x800 */
final String v3 = "hal"; // const-string v3, "hal"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 557 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x2000 */
final String v3 = "res"; // const-string v3, "res"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 558 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/16 v2, 0x4000 */
final String v3 = "dalvik"; // const-string v3, "dalvik"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 559 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const v2, 0x8000 */
final String v3 = "rs"; // const-string v3, "rs"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 560 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/high16 v2, 0x10000 */
final String v3 = "bionic"; // const-string v3, "bionic"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 561 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/high16 v2, 0x20000 */
final String v3 = "power"; // const-string v3, "power"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 562 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/high16 v2, 0x40000 */
final String v3 = "pm"; // const-string v3, "pm"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 563 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/high16 v2, 0x80000 */
/* const-string/jumbo v3, "ss" */
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 564 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/high16 v2, 0x100000 */
final String v3 = "database"; // const-string v3, "database"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 565 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const/high16 v2, 0x200000 */
final String v3 = "network"; // const-string v3, "network"
/* invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
(( java.util.HashMap ) v0 ).put ( v3, v1 ); // invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 566 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "adb"; // const-string v2, "adb"
/* const/high16 v3, 0x400000 */
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
final String v2 = "adb"; // const-string v2, "adb"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 567 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const-string/jumbo v2, "vibrator" */
/* const/high16 v3, 0x800000 */
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* const-string/jumbo v2, "vibrator" */
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 568 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "aidl"; // const-string v2, "aidl"
/* const/high16 v3, 0x1000000 */
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
final String v2 = "aidl"; // const-string v2, "aidl"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 569 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "nnapi"; // const-string v2, "nnapi"
/* const/high16 v3, 0x2000000 */
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
final String v2 = "nnapi"; // const-string v2, "nnapi"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 570 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "rro"; // const-string v2, "rro"
/* const/high16 v3, 0x4000000 */
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
final String v2 = "rro"; // const-string v2, "rro"
(( java.util.HashMap ) v0 ).put ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 573 */
int v0 = 0; // const/4 v0, 0x0
/* .line 574 */
/* .local v0, "temp":Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "sched"; // const-string v2, "sched"
int v3 = 0; // const/4 v3, 0x0
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 575 */
final String v1 = "events/sched/sched_switch/enable"; // const-string v1, "events/sched/sched_switch/enable"
int v2 = 1; // const/4 v2, 0x1
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 576 */
final String v1 = "events/sched/sched_wakeup/enable"; // const-string v1, "events/sched/sched_wakeup/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 577 */
final String v1 = "events/sched/sched_waking/enable"; // const-string v1, "events/sched/sched_waking/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 578 */
final String v1 = "events/sched/sched_blocked_reason/enable"; // const-string v1, "events/sched/sched_blocked_reason/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 579 */
final String v1 = "events/sched/sched_pi_setprio/enable"; // const-string v1, "events/sched/sched_pi_setprio/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 580 */
final String v1 = "events/sched/sched_process_exit/enable"; // const-string v1, "events/sched/sched_process_exit/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 581 */
final String v1 = "events/cgroup/enable"; // const-string v1, "events/cgroup/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 582 */
final String v1 = "events/oom/oom_score_adj_update/enable"; // const-string v1, "events/oom/oom_score_adj_update/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 583 */
final String v1 = "events/task/task_rename/enable"; // const-string v1, "events/task/task_rename/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 584 */
final String v1 = "events/task/task_newtask/enable"; // const-string v1, "events/task/task_newtask/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 585 */
v1 = this.mCategories;
final String v4 = "sched"; // const-string v4, "sched"
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 587 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v4 = "irq"; // const-string v4, "irq"
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 588 */
final String v1 = "events/irq/enable"; // const-string v1, "events/irq/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 589 */
final String v1 = "events/ipi/enable"; // const-string v1, "events/ipi/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 590 */
v1 = this.mCategories;
final String v4 = "irq"; // const-string v4, "irq"
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 593 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v4 = "freq"; // const-string v4, "freq"
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 594 */
final String v1 = "events/power/cpu_frequency/enable"; // const-string v1, "events/power/cpu_frequency/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 595 */
final String v1 = "events/power/clock_set_rate/enable"; // const-string v1, "events/power/clock_set_rate/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 596 */
final String v1 = "events/power/clock_disable/enable"; // const-string v1, "events/power/clock_disable/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 597 */
final String v1 = "events/power/clock_enable/enable"; // const-string v1, "events/power/clock_enable/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 598 */
final String v1 = "events/clk/clk_set_rate/enable"; // const-string v1, "events/clk/clk_set_rate/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 599 */
final String v1 = "events/clk/clk_disable/enable"; // const-string v1, "events/clk/clk_disable/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 600 */
final String v1 = "events/clk/clk_enable/enable"; // const-string v1, "events/clk/clk_enable/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 601 */
final String v1 = "events/power/cpu_frequency_limits/enable"; // const-string v1, "events/power/cpu_frequency_limits/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 602 */
final String v1 = "events/power/suspend_resume/enable"; // const-string v1, "events/power/suspend_resume/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 603 */
final String v1 = "events/cpuhp/cpuhp_enter/enable"; // const-string v1, "events/cpuhp/cpuhp_enter/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 604 */
final String v1 = "events/cpuhp/cpuhp_exit/enable"; // const-string v1, "events/cpuhp/cpuhp_exit/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 605 */
v1 = this.mCategories;
final String v4 = "freq"; // const-string v4, "freq"
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 607 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v4 = "idle"; // const-string v4, "idle"
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 608 */
final String v1 = "events/power/cpu_idle/enable"; // const-string v1, "events/power/cpu_idle/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 609 */
v1 = this.mCategories;
final String v4 = "idle"; // const-string v4, "idle"
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 611 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v4 = "disk"; // const-string v4, "disk"
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 612 */
final String v1 = "events/block/block_rq_issue/enable"; // const-string v1, "events/block/block_rq_issue/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 613 */
final String v1 = "events/block/block_rq_complete/enable"; // const-string v1, "events/block/block_rq_complete/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 614 */
final String v1 = "events/f2fs/f2fs_sync_file_enter/enable"; // const-string v1, "events/f2fs/f2fs_sync_file_enter/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 615 */
final String v1 = "events/f2fs/f2fs_sync_file_exit/enable"; // const-string v1, "events/f2fs/f2fs_sync_file_exit/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 616 */
final String v1 = "events/f2fs/f2fs_write_begin/enable"; // const-string v1, "events/f2fs/f2fs_write_begin/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 617 */
final String v1 = "events/f2fs/f2fs_write_end/enable"; // const-string v1, "events/f2fs/f2fs_write_end/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 618 */
final String v1 = "events/ext4/ext4_da_write_begin/enable"; // const-string v1, "events/ext4/ext4_da_write_begin/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 619 */
final String v1 = "events/ext4/ext4_da_write_end/enable"; // const-string v1, "events/ext4/ext4_da_write_end/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 620 */
final String v1 = "events/ext4/ext4_sync_file_enter/enable"; // const-string v1, "events/ext4/ext4_sync_file_enter/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 621 */
final String v1 = "events/ext4/ext4_sync_file_exit/enable"; // const-string v1, "events/ext4/ext4_sync_file_exit/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 622 */
v1 = this.mCategories;
final String v4 = "disk"; // const-string v4, "disk"
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 624 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const-string/jumbo v4, "sync" */
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 625 */
final String v1 = "events/dma_fence/enable"; // const-string v1, "events/dma_fence/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 626 */
v1 = this.mCategories;
/* const-string/jumbo v4, "sync" */
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 629 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v4 = "memreclaim"; // const-string v4, "memreclaim"
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 630 */
final String v1 = "events/vmscan/mm_vmscan_direct_reclaim_begin/enable"; // const-string v1, "events/vmscan/mm_vmscan_direct_reclaim_begin/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 631 */
final String v1 = "events/vmscan/mm_vmscan_direct_reclaim_end/enable"; // const-string v1, "events/vmscan/mm_vmscan_direct_reclaim_end/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 632 */
final String v1 = "events/vmscan/mm_vmscan_kswapd_wake/enable"; // const-string v1, "events/vmscan/mm_vmscan_kswapd_wake/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 633 */
final String v1 = "events/vmscan/mm_vmscan_kswapd_sleep/enable"; // const-string v1, "events/vmscan/mm_vmscan_kswapd_sleep/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 634 */
v1 = this.mCategories;
final String v4 = "memreclaim"; // const-string v4, "memreclaim"
(( java.util.HashMap ) v1 ).put ( v4, v0 ); // invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 636 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v4 = "binder_driver"; // const-string v4, "binder_driver"
/* invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 637 */
final String v1 = "events/binder/binder_transaction/enable"; // const-string v1, "events/binder/binder_transaction/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 638 */
final String v1 = "events/binder/binder_transaction_received/enable"; // const-string v1, "events/binder/binder_transaction_received/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 639 */
final String v1 = "events/binder/binder_transaction_alloc_buf/enable"; // const-string v1, "events/binder/binder_transaction_alloc_buf/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 640 */
final String v1 = "events/binder/binder_set_priority/enable"; // const-string v1, "events/binder/binder_set_priority/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 641 */
v1 = this.mCategories;
final String v2 = "binder_driver"; // const-string v2, "binder_driver"
(( java.util.HashMap ) v1 ).put ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 643 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "binder_lock"; // const-string v2, "binder_lock"
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 644 */
final String v1 = "events/binder/binder_lock/enable"; // const-string v1, "events/binder/binder_lock/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 645 */
final String v1 = "events/binder/binder_locked/enable"; // const-string v1, "events/binder/binder_locked/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 646 */
final String v1 = "events/binder/binder_unlock/enable"; // const-string v1, "events/binder/binder_unlock/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 647 */
v1 = this.mCategories;
final String v2 = "binder_lock"; // const-string v2, "binder_lock"
(( java.util.HashMap ) v1 ).put ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 649 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
final String v2 = "memory"; // const-string v2, "memory"
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 650 */
final String v1 = "events/kmem/rss_stat/enable"; // const-string v1, "events/kmem/rss_stat/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 651 */
v1 = this.mCategories;
final String v2 = "memory"; // const-string v2, "memory"
(( java.util.HashMap ) v1 ).put ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 653 */
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* const-string/jumbo v2, "thermal" */
/* invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V */
/* move-object v0, v1 */
/* .line 654 */
final String v1 = "events/thermal/thermal_temperature/enable"; // const-string v1, "events/thermal/thermal_temperature/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 655 */
final String v1 = "events/thermal/cdev_update/enable"; // const-string v1, "events/thermal/cdev_update/enable"
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).addPath ( v1, v3 ); // invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 656 */
v1 = this.mCategories;
/* const-string/jumbo v2, "thermal" */
(( java.util.HashMap ) v1 ).put ( v2, v0 ); // invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 657 */
return;
} // .end method
private void removeKernelCategoryPath ( org.json.JSONObject p0 ) {
/* .locals 3 */
/* .param p1, "content" # Lorg/json/JSONObject; */
/* .line 337 */
try { // :try_start_0
final String v0 = "category"; // const-string v0, "category"
(( org.json.JSONObject ) p1 ).getString ( v0 ); // invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 338 */
/* .local v0, "category":Ljava/lang/String; */
final String v1 = "path"; // const-string v1, "path"
(( org.json.JSONObject ) p1 ).getString ( v1 ); // invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
/* .line 339 */
/* .local v1, "path":Ljava/lang/String; */
(( com.miui.server.migard.trace.GameTrace ) p0 ).removeKernelCategoryPath ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/migard/trace/GameTrace;->removeKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Lorg/json/JSONException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 342 */
} // .end local v0 # "category":Ljava/lang/String;
} // .end local v1 # "path":Ljava/lang/String;
/* .line 340 */
/* :catch_0 */
/* move-exception v0 */
/* .line 341 */
/* .local v0, "ex":Lorg/json/JSONException; */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
final String v2 = "parse config failed, remove kernel category path failed."; // const-string v2, "parse config failed, remove kernel category path failed."
android.util.Slog .e ( v1,v2 );
/* .line 343 */
} // .end local v0 # "ex":Lorg/json/JSONException;
} // :goto_0
return;
} // .end method
private void resetBufferSize ( ) {
/* .locals 2 */
/* .line 418 */
/* const/16 v0, 0x800 */
/* iput v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceBufferSizeKB:I */
/* .line 419 */
final String v0 = "buffer_size_kb"; // const-string v0, "buffer_size_kb"
final String v1 = "1"; // const-string v1, "1"
/* invoke-direct {p0, v0, v1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 420 */
return;
} // .end method
private void setCategoryPathEnabled ( java.lang.String p0, java.lang.String p1, Boolean p2, Boolean p3 ) {
/* .locals 3 */
/* .param p1, "category" # Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "enabled" # Z */
/* .param p4, "modify" # Z */
/* .line 399 */
final String v0 = "GameTrace"; // const-string v0, "GameTrace"
if ( p1 != null) { // if-eqz p1, :cond_4
/* if-nez p2, :cond_0 */
/* .line 403 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 404 */
/* .local v1, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
v2 = this.mCategories;
(( java.util.HashMap ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v1, v2 */
/* check-cast v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* .line 405 */
/* if-nez v1, :cond_1 */
/* .line 406 */
final String v2 = "remove kernel category path failed, unknown category."; // const-string v2, "remove kernel category path failed, unknown category."
android.util.Slog .e ( v0,v2 );
/* .line 407 */
return;
/* .line 409 */
} // :cond_1
if ( p4 != null) { // if-eqz p4, :cond_2
/* .line 410 */
v2 = (( com.miui.server.migard.trace.GameTrace$TracingCategory ) v1 ).setPathEnabled ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->setPathEnabled(Ljava/lang/String;Z)Z
/* if-nez v2, :cond_3 */
/* .line 411 */
final String v2 = "enable kernel category path failed, unknown path."; // const-string v2, "enable kernel category path failed, unknown path."
android.util.Slog .e ( v0,v2 );
/* .line 414 */
} // :cond_2
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v1 ).addPath ( p2, p3 ); // invoke-virtual {v1, p2, p3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V
/* .line 415 */
} // :cond_3
} // :goto_0
return;
/* .line 400 */
} // .end local v1 # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
} // :cond_4
} // :goto_1
final String v1 = "remove kernel category path failed, check your parameters."; // const-string v1, "remove kernel category path failed, check your parameters."
android.util.Slog .e ( v0,v1 );
/* .line 401 */
return;
} // .end method
private void setClock ( ) {
/* .locals 4 */
/* .line 457 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mTraceFolder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v1, "trace_clock" */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.FileUtils .readFromSys ( v0 );
/* .line 459 */
/* .local v0, "str":Ljava/lang/String; */
final String v2 = "boot"; // const-string v2, "boot"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 460 */
final String v2 = "boot"; // const-string v2, "boot"
/* .local v2, "newClock":Ljava/lang/String; */
/* .line 461 */
} // .end local v2 # "newClock":Ljava/lang/String;
} // :cond_0
final String v2 = "mono"; // const-string v2, "mono"
v2 = (( java.lang.String ) v0 ).contains ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 462 */
final String v2 = "mono"; // const-string v2, "mono"
/* .restart local v2 # "newClock":Ljava/lang/String; */
/* .line 464 */
} // .end local v2 # "newClock":Ljava/lang/String;
} // :cond_1
final String v2 = "global"; // const-string v2, "global"
/* .line 465 */
/* .restart local v2 # "newClock":Ljava/lang/String; */
} // :goto_0
v3 = (( java.lang.String ) v0 ).equals ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 466 */
return;
/* .line 467 */
} // :cond_2
/* invoke-direct {p0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 468 */
return;
} // .end method
private void setKernelContent ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "content" # Ljava/lang/String; */
/* .line 507 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mTraceFolder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.FileUtils .writeToSys ( v0,p2 );
/* .line 508 */
return;
} // .end method
private void setKernelOptionEnable ( java.lang.String p0, Boolean p1 ) {
/* .locals 2 */
/* .param p1, "path" # Ljava/lang/String; */
/* .param p2, "enabled" # Z */
/* .line 503 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
v1 = this.mTraceFolder;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
if ( p2 != null) { // if-eqz p2, :cond_0
final String v1 = "1"; // const-string v1, "1"
} // :cond_0
final String v1 = "0"; // const-string v1, "0"
} // :goto_0
com.miui.server.migard.utils.FileUtils .writeToSys ( v0,v1 );
/* .line 504 */
return;
} // .end method
private void setPrintTgidEnableIfPresent ( Boolean p0 ) {
/* .locals 4 */
/* .param p1, "enable" # Z */
/* .line 446 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mTraceFolder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "options/print-tgid"; // const-string v2, "options/print-tgid"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 447 */
/* .local v0, "pfile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 448 */
/* invoke-direct {p0, v2, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V */
/* .line 450 */
} // :cond_0
/* new-instance v1, Ljava/io/File; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mTraceFolder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "options/record-tgid"; // const-string v3, "options/record-tgid"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 451 */
/* .local v1, "rfile":Ljava/io/File; */
v2 = (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 452 */
/* invoke-direct {p0, v3, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V */
/* .line 454 */
} // :cond_1
return;
} // .end method
private void setTraceEnabled ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .line 495 */
/* const-string/jumbo v0, "tracing_on" */
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V */
/* .line 496 */
return;
} // .end method
private void setTraceOverwriteEnable ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "enabled" # Z */
/* .line 499 */
final String v0 = "options/overwrite"; // const-string v0, "options/overwrite"
/* invoke-direct {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V */
/* .line 500 */
return;
} // .end method
private void setUserInitiatedTraceProperty ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 492 */
if ( p1 != null) { // if-eqz p1, :cond_0
final String v0 = "1"; // const-string v0, "1"
} // :cond_0
final String v0 = ""; // const-string v0, ""
} // :goto_0
final String v1 = "debug.atrace.user_initiated"; // const-string v1, "debug.atrace.user_initiated"
android.os.SystemProperties .set ( v1,v0 );
/* .line 493 */
return;
} // .end method
private void setupKernelCategories ( java.util.HashMap p0 ) {
/* .locals 4 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 471 */
/* .local p1, "categories":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
(( java.util.HashMap ) p1 ).entrySet ( ); // invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;
v1 = } // :goto_0
if ( v1 != null) { // if-eqz v1, :cond_1
/* check-cast v1, Ljava/util/Map$Entry; */
/* .line 472 */
/* .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
v3 = this.mTraceFolder;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* check-cast v3, Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* check-cast v3, Ljava/lang/Boolean; */
v3 = (( java.lang.Boolean ) v3 ).booleanValue ( ); // invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
if ( v3 != null) { // if-eqz v3, :cond_0
final String v3 = "1"; // const-string v3, "1"
} // :cond_0
final String v3 = "0"; // const-string v3, "0"
} // :goto_1
com.miui.server.migard.utils.FileUtils .writeToSys ( v2,v3 );
/* .line 473 */
} // .end local v1 # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
/* .line 474 */
} // :cond_1
return;
} // .end method
private void setupUserspaceCategories ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "tags" # I */
/* .line 487 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "0x"; // const-string v1, "0x"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.Integer .toHexString ( p1 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 488 */
/* .local v0, "value":Ljava/lang/String; */
final String v1 = "debug.atrace.tags.enableflags"; // const-string v1, "debug.atrace.tags.enableflags"
android.os.SystemProperties .set ( v1,v0 );
/* .line 489 */
return;
} // .end method
private void writeClockSyncMarker ( ) {
/* .locals 6 */
/* .line 423 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mTraceFolder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "trace_marker" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 424 */
/* .local v0, "markerFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( java.io.File ) v0 ).canWrite ( ); // invoke-virtual {v0}, Ljava/io/File;->canWrite()Z
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 425 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
/* long-to-float v1, v3 */
/* const/high16 v3, 0x447a0000 # 1000.0f */
/* div-float/2addr v1, v3 */
/* .line 426 */
/* .local v1, "realTime":F */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "trace_event_clock_sync: realtime_ts=" */
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v1 ); // invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2, v4}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 428 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v4 */
/* long-to-float v4, v4 */
/* div-float/2addr v4, v3 */
/* .line 429 */
/* .local v4, "monoTime":F */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v5, "trace_event_clock_sync: parent_ts=" */
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v2, v3}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V */
/* .line 432 */
} // .end local v1 # "realTime":F
} // .end local v4 # "monoTime":F
} // :cond_0
return;
} // .end method
/* # virtual methods */
public void addKernelCategory ( java.lang.String p0, java.util.HashMap p1 ) {
/* .locals 3 */
/* .param p1, "category" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Boolean;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 354 */
/* .local p2, "details":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;" */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 355 */
final String v0 = "cannot add kernel category, while game trace running."; // const-string v0, "cannot add kernel category, while game trace running."
android.util.Slog .w ( v1,v0 );
/* .line 356 */
return;
/* .line 358 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_1
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-lez v0, :cond_1 */
if ( p2 != null) { // if-eqz p2, :cond_1
/* .line 359 */
v0 = this.mCategories;
/* new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p1, v2, p2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;ILjava/util/HashMap;)V */
(( java.util.HashMap ) v0 ).put ( p1, v1 ); // invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 361 */
} // :cond_1
final String v0 = "add kernel category failed, check your parameters."; // const-string v0, "add kernel category failed, check your parameters."
android.util.Slog .e ( v1,v0 );
/* .line 362 */
} // :goto_0
return;
} // .end method
public void addKernelCategoryPath ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "category" # Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "enabled" # Z */
/* .line 365 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 366 */
final String v0 = "GameTrace"; // const-string v0, "GameTrace"
final String v1 = "cannot add kernel category path, while game trace running."; // const-string v1, "cannot add kernel category path, while game trace running."
android.util.Slog .w ( v0,v1 );
/* .line 367 */
return;
/* .line 369 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/migard/trace/GameTrace;->setCategoryPathEnabled(Ljava/lang/String;Ljava/lang/String;ZZ)V */
/* .line 370 */
return;
} // .end method
public void configTrace ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "json" # Ljava/lang/String; */
/* .line 288 */
return;
} // .end method
public void dump ( java.lang.String p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "dirPath" # Ljava/lang/String; */
/* .param p2, "zip" # Z */
/* .line 263 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "dump cameinv dirPath: "; // const-string v1, "dump cameinv dirPath: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
android.util.Slog .w ( v1,v0 );
/* .line 265 */
/* new-instance v0, Ljava/io/File; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
v2 = this.mTraceFolder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "trace" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 266 */
/* .local v0, "traceFile":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* if-nez v1, :cond_0 */
/* .line 267 */
return;
/* .line 268 */
} // :cond_0
int v1 = 0; // const/4 v1, 0x0
/* .line 270 */
/* .local v1, "in":Ljava/io/FileInputStream; */
try { // :try_start_0
/* new-instance v2, Ljava/io/FileInputStream; */
/* invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V */
/* move-object v1, v2 */
/* .line 271 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->generateTraceName()Ljava/lang/String; */
/* .line 272 */
/* .local v2, "name":Ljava/lang/String; */
com.miui.server.migard.utils.FileUtils .readSysToFile ( v1,p1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 276 */
/* nop */
/* .line 278 */
try { // :try_start_1
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_0 */
/* .line 281 */
/* .line 279 */
/* :catch_0 */
/* move-exception v3 */
/* .line 280 */
/* .local v3, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 281 */
} // .end local v3 # "e":Ljava/lang/Exception;
/* .line 276 */
} // .end local v2 # "name":Ljava/lang/String;
/* :catchall_0 */
/* move-exception v2 */
/* .line 273 */
/* :catch_1 */
/* move-exception v2 */
/* .line 274 */
/* .local v2, "e":Ljava/lang/Exception; */
try { // :try_start_2
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 276 */
} // .end local v2 # "e":Ljava/lang/Exception;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 278 */
try { // :try_start_3
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_3 */
/* .catch Ljava/lang/Exception; {:try_start_3 ..:try_end_3} :catch_2 */
/* .line 281 */
/* .line 279 */
/* :catch_2 */
/* move-exception v2 */
/* .line 280 */
/* .restart local v2 # "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 281 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* nop */
/* .line 284 */
} // :cond_1
} // :goto_0
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearTrace()V */
/* .line 285 */
return;
/* .line 276 */
} // :goto_1
if ( v1 != null) { // if-eqz v1, :cond_2
/* .line 278 */
try { // :try_start_4
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_3 */
/* .line 281 */
/* .line 279 */
/* :catch_3 */
/* move-exception v3 */
/* .line 280 */
/* .restart local v3 # "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v3 ).printStackTrace ( ); // invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
/* .line 283 */
} // .end local v3 # "e":Ljava/lang/Exception;
} // :cond_2
} // :goto_2
/* throw v2 */
} // .end method
public void dump ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "zip" # Z */
/* .line 240 */
v0 = this.mTraceDir;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 241 */
(( com.miui.server.migard.trace.GameTrace ) p0 ).dump ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->dump(Ljava/lang/String;Z)V
/* .line 243 */
} // :cond_0
final String v0 = "/data/system/migard/game_trace"; // const-string v0, "/data/system/migard/game_trace"
(( com.miui.server.migard.trace.GameTrace ) p0 ).dump ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->dump(Ljava/lang/String;Z)V
/* .line 244 */
} // :goto_0
return;
} // .end method
public void enableKernelCategoryPath ( java.lang.String p0, java.lang.String p1, Boolean p2 ) {
/* .locals 2 */
/* .param p1, "category" # Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .param p3, "enabled" # Z */
/* .line 373 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 374 */
final String v0 = "GameTrace"; // const-string v0, "GameTrace"
final String v1 = "cannot enable kernel category path, while game trace running."; // const-string v1, "cannot enable kernel category path, while game trace running."
android.util.Slog .w ( v0,v1 );
/* .line 375 */
return;
/* .line 377 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/migard/trace/GameTrace;->setCategoryPathEnabled(Ljava/lang/String;Ljava/lang/String;ZZ)V */
/* .line 378 */
return;
} // .end method
public void removeKernelCategoryPath ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 3 */
/* .param p1, "category" # Ljava/lang/String; */
/* .param p2, "path" # Ljava/lang/String; */
/* .line 381 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 382 */
final String v0 = "cannot remove kernel category path, while game trace running."; // const-string v0, "cannot remove kernel category path, while game trace running."
android.util.Slog .w ( v1,v0 );
/* .line 383 */
return;
/* .line 385 */
} // :cond_0
if ( p1 != null) { // if-eqz p1, :cond_3
/* if-nez p2, :cond_1 */
/* .line 389 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
/* .line 390 */
/* .local v0, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
v2 = this.mCategories;
(( java.util.HashMap ) v2 ).get ( p1 ); // invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* move-object v0, v2 */
/* check-cast v0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* .line 391 */
/* if-nez v0, :cond_2 */
/* .line 392 */
final String v2 = "remove kernel category path failed, unknown category."; // const-string v2, "remove kernel category path failed, unknown category."
android.util.Slog .e ( v1,v2 );
/* .line 393 */
return;
/* .line 395 */
} // :cond_2
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v0 ).removePath ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->removePath(Ljava/lang/String;)V
/* .line 396 */
return;
/* .line 386 */
} // .end local v0 # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
} // :cond_3
} // :goto_0
final String v0 = "remove kernel category path failed, check your parameters."; // const-string v0, "remove kernel category path failed, check your parameters."
android.util.Slog .e ( v1,v0 );
/* .line 387 */
return;
} // .end method
public void setBufferSize ( Integer p0 ) {
/* .locals 2 */
/* .param p1, "size" # I */
/* .line 346 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 347 */
final String v0 = "GameTrace"; // const-string v0, "GameTrace"
final String v1 = "cannot set buffer size, while game trace running."; // const-string v1, "cannot set buffer size, while game trace running."
android.util.Slog .w ( v0,v1 );
/* .line 348 */
return;
/* .line 350 */
} // :cond_0
/* iput p1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceBufferSizeKB:I */
/* .line 351 */
return;
} // .end method
public void start ( java.util.ArrayList p0, Boolean p1 ) {
/* .locals 5 */
/* .param p2, "async" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 124 */
/* .local p1, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* iput-boolean p2, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z */
/* .line 125 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isTraceSupported:Z */
if ( v0 != null) { // if-eqz v0, :cond_5
if ( p1 != null) { // if-eqz p1, :cond_5
v0 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* if-nez v0, :cond_0 */
/* .line 127 */
} // :cond_0
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 128 */
final String v0 = "GameTrace"; // const-string v0, "GameTrace"
/* const-string/jumbo v1, "start game trace failed, game trace is already running." */
android.util.Slog .w ( v0,v1 );
/* .line 129 */
return;
/* .line 131 */
} // :cond_1
int v0 = 1; // const/4 v0, 0x1
/* iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
/* .line 132 */
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setUserInitiatedTraceProperty(Z)V */
/* .line 133 */
int v1 = 0; // const/4 v1, 0x0
/* .line 135 */
/* .local v1, "tags":I */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( java.util.ArrayList ) p1 ).size ( ); // invoke-virtual {p1}, Ljava/util/ArrayList;->size()I
/* if-ge v2, v3, :cond_4 */
/* .line 136 */
v3 = this.mCategories;
(( java.util.ArrayList ) p1 ).get ( v2 ); // invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
(( java.util.HashMap ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* .line 137 */
/* .local v3, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory; */
/* if-nez v3, :cond_2 */
/* .line 138 */
} // :cond_2
v4 = (( com.miui.server.migard.trace.GameTrace$TracingCategory ) v3 ).getTags ( ); // invoke-virtual {v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTags()I
if ( v4 != null) { // if-eqz v4, :cond_3
/* .line 139 */
v4 = (( com.miui.server.migard.trace.GameTrace$TracingCategory ) v3 ).getTags ( ); // invoke-virtual {v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTags()I
/* or-int/2addr v1, v4 */
/* .line 141 */
} // :cond_3
(( com.miui.server.migard.trace.GameTrace$TracingCategory ) v3 ).getTracePathMap ( ); // invoke-virtual {v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTracePathMap()Ljava/util/HashMap;
/* invoke-direct {p0, v4}, Lcom/miui/server/migard/trace/GameTrace;->setupKernelCategories(Ljava/util/HashMap;)V */
/* .line 135 */
} // :goto_1
/* add-int/lit8 v2, v2, 0x1 */
/* .line 144 */
} // .end local v2 # "i":I
} // .end local v3 # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
} // :cond_4
/* invoke-direct {p0, p2}, Lcom/miui/server/migard/trace/GameTrace;->setTraceOverwriteEnable(Z)V */
/* .line 145 */
/* invoke-direct {p0, v1}, Lcom/miui/server/migard/trace/GameTrace;->setupUserspaceCategories(I)V */
/* .line 146 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->applyTraceBufferSize()V */
/* .line 147 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->setClock()V */
/* .line 148 */
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setPrintTgidEnableIfPresent(Z)V */
/* .line 149 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearKernelTraceFuncs()V */
/* .line 150 */
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setTraceEnabled(Z)V */
/* .line 151 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearTrace()V */
/* .line 152 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->writeClockSyncMarker()V */
/* .line 153 */
return;
/* .line 126 */
} // .end local v1 # "tags":I
} // :cond_5
} // :goto_2
return;
} // .end method
public void start ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "async" # Z */
/* .line 115 */
/* iput-boolean p1, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z */
/* .line 116 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 117 */
/* .local v0, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
v1 = this.mCategories;
(( java.util.HashMap ) v1 ).keySet ( ); // invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* check-cast v2, Ljava/lang/String; */
/* .line 118 */
/* .local v2, "category":Ljava/lang/String; */
(( java.util.ArrayList ) v0 ).add ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 119 */
} // .end local v2 # "category":Ljava/lang/String;
/* .line 120 */
} // :cond_0
(( com.miui.server.migard.trace.GameTrace ) p0 ).start ( v0, p1 ); // invoke-virtual {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->start(Ljava/util/ArrayList;Z)V
/* .line 121 */
return;
} // .end method
public void stop ( java.lang.String p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "dirPath" # Ljava/lang/String; */
/* .param p2, "zip" # Z */
/* .line 247 */
/* iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
final String v1 = "GameTrace"; // const-string v1, "GameTrace"
/* if-nez v0, :cond_0 */
/* .line 248 */
/* const-string/jumbo v0, "stop game trace failed, game trace is not running." */
android.util.Slog .w ( v1,v0 );
/* .line 249 */
return;
/* .line 251 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
/* invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setTraceEnabled(Z)V */
/* .line 252 */
if ( p1 != null) { // if-eqz p1, :cond_1
final String v2 = ""; // const-string v2, ""
/* if-eq p1, v2, :cond_1 */
/* iget-boolean v2, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z */
/* if-nez v2, :cond_1 */
/* .line 253 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "isAsync: "; // const-string v3, "isAsync: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-boolean v3, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z */
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .i ( v1,v2 );
/* .line 254 */
(( com.miui.server.migard.trace.GameTrace ) p0 ).dump ( p1, p2 ); // invoke-virtual {p0, p1, p2}, Lcom/miui/server/migard/trace/GameTrace;->dump(Ljava/lang/String;Z)V
/* .line 256 */
} // :cond_1
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->cleanUpUserspaceCategories()V */
/* .line 257 */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->cleanUpKernelCategories()V */
/* .line 258 */
/* iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z */
/* .line 259 */
int v0 = 0; // const/4 v0, 0x0
this.mTraceDir = v0;
/* .line 260 */
return;
} // .end method
public void stop ( Boolean p0 ) {
/* .locals 3 */
/* .param p1, "zip" # Z */
/* .line 226 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/system/migard/game_trace"; // const-string v1, "/data/system/migard/game_trace"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 227 */
/* .local v0, "mTracePath":Ljava/io/File; */
v2 = this.mTraceDir;
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 228 */
(( com.miui.server.migard.trace.GameTrace ) p0 ).stop ( v2, p1 ); // invoke-virtual {p0, v2, p1}, Lcom/miui/server/migard/trace/GameTrace;->stop(Ljava/lang/String;Z)V
/* .line 230 */
} // :cond_0
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->generateTraceName()Ljava/lang/String; */
/* .line 231 */
/* .local v2, "name":Ljava/lang/String; */
/* invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearOldTrace()V */
/* .line 232 */
(( com.miui.server.migard.trace.GameTrace ) p0 ).stop ( v1, p1 ); // invoke-virtual {p0, v1, p1}, Lcom/miui/server/migard/trace/GameTrace;->stop(Ljava/lang/String;Z)V
/* .line 233 */
com.miui.server.migard.utils.FileUtils .delOldZipFile ( v0 );
/* .line 234 */
com.miui.server.migard.utils.FileUtils .readAllSystraceToZip ( v1,v1,v2 );
/* .line 235 */
com.miui.server.migard.utils.FileUtils .delTmpTraceFile ( v0 );
/* .line 237 */
} // .end local v2 # "name":Ljava/lang/String;
} // :goto_0
return;
} // .end method
