.class Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
.super Ljava/lang/Object;
.source "GameTrace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/migard/trace/GameTrace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TracingCategory"
.end annotation


# instance fields
.field name:Ljava/lang/String;

.field pathConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field tags:I


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tags"    # I

    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677
    iput-object p1, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->name:Ljava/lang/String;

    .line 678
    iput p2, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->tags:I

    .line 679
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    .line 680
    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/util/HashMap;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "tags"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 681
    .local p3, "pathConfig":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 682
    iput-object p1, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->name:Ljava/lang/String;

    .line 683
    iput p2, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->tags:I

    .line 684
    iput-object p3, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    .line 685
    return-void
.end method


# virtual methods
.method addPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 687
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 688
    return-void
.end method

.method addPath(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "on"    # Z

    .line 690
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 691
    return-void
.end method

.method getEnabledTracePathList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 709
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 710
    .local v0, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 711
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 712
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 713
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :cond_0
    goto :goto_0

    .line 714
    :cond_1
    return-object v0
.end method

.method getTags()I
    .locals 1

    .line 703
    iget v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->tags:I

    return v0
.end method

.method getTracePathList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 717
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 718
    .local v0, "pathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 719
    .local v2, "p":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 720
    .end local v2    # "p":Ljava/lang/String;
    goto :goto_0

    .line 721
    :cond_0
    return-object v0
.end method

.method getTracePathMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 706
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    return-object v0
.end method

.method removePath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .line 700
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 701
    return-void
.end method

.method setPathEnabled(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "on"    # Z

    .line 693
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->pathConfig:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    const/4 v0, 0x1

    return v0

    .line 697
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
