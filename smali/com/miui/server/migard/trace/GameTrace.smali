.class public Lcom/miui/server/migard/trace/GameTrace;
.super Ljava/lang/Object;
.source "GameTrace.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    }
.end annotation


# static fields
.field private static final ADD_KERNEL_CATEGORY:Ljava/lang/String; = "addKernelCategory"

.field private static final ADD_KERNEL_CATEGORY_PATH:Ljava/lang/String; = "addKernelCategoryPath"

.field private static final AES_ALGO:Ljava/lang/String; = "aes"

.field private static final ATRACE_TAG_ACTIVITY_MANAGER:I = 0x40

.field private static final ATRACE_TAG_ADB:I = 0x400000

.field private static final ATRACE_TAG_AIDL:I = 0x1000000

.field private static final ATRACE_TAG_AUDIO:I = 0x100

.field private static final ATRACE_TAG_BIONIC:I = 0x10000

.field private static final ATRACE_TAG_CAMERA:I = 0x400

.field private static final ATRACE_TAG_DALVIK:I = 0x4000

.field private static final ATRACE_TAG_DATABASE:I = 0x100000

.field private static final ATRACE_TAG_GRAPHICS:I = 0x2

.field private static final ATRACE_TAG_HAL:I = 0x800

.field private static final ATRACE_TAG_INPUT:I = 0x4

.field private static final ATRACE_TAG_NETWORK:I = 0x200000

.field private static final ATRACE_TAG_NEVER:I = 0x0

.field private static final ATRACE_TAG_NNAPI:I = 0x2000000

.field private static final ATRACE_TAG_PACKAGE_MANAGER:I = 0x40000

.field private static final ATRACE_TAG_POWER:I = 0x20000

.field private static final ATRACE_TAG_RESOURCES:I = 0x2000

.field private static final ATRACE_TAG_RRO:I = 0x4000000

.field private static final ATRACE_TAG_RS:I = 0x8000

.field private static final ATRACE_TAG_SYNC_MANAGER:I = 0x80

.field private static final ATRACE_TAG_SYSTEM_SERVER:I = 0x80000

.field private static final ATRACE_TAG_VIBRATOR:I = 0x800000

.field private static final ATRACE_TAG_VIDEO:I = 0x200

.field private static final ATRACE_TAG_VIEW:I = 0x8

.field private static final ATRACE_TAG_WEBVIEW:I = 0x10

.field private static final ATRACE_TAG_WINDOW_MANAGER:I = 0x20

.field private static final CATETORY_KEY:Ljava/lang/String; = "category"

.field private static final COMMANDS_AS_ROOT_FILE_PATH:Ljava/lang/String; = "/data/system/migard/game_trace/root_commands"

.field private static final CURRENT_TRACER_PATH:Ljava/lang/String; = "current_tracer"

.field private static final DEBUG_FS_PATH:Ljava/lang/String; = "/sys/kernel/debug/tracing/"

.field private static final DETAILS_KEY:Ljava/lang/String; = "details"

.field private static final ENABLED_KEY:Ljava/lang/String; = "enabled"

.field private static final ENABLE_KERNEL_CATEGORY_PATH:Ljava/lang/String; = "enableKernelCategoryPath"

.field private static final FTRACE_FILTER_PATH:Ljava/lang/String; = "set_ftrace_filter"

.field private static final MAX_TRACE_SIZE:I = 0x5

.field private static final NON_ALGO:Ljava/lang/String; = "non"

.field private static final PATH_KEY:Ljava/lang/String; = "path"

.field private static final REMOVE_KERNEL_CATEGORY_PATH:Ljava/lang/String; = "removeKernelCategoryPath"

.field private static final SET_BUFFER_SIZE:Ljava/lang/String; = "setBufferSize"

.field private static final SET_CRYPTO_ALGO:Ljava/lang/String; = "setCryptoAlgo"

.field private static final SET_TRACE_DIR:Ljava/lang/String; = "setTraceDir"

.field private static final TAG:Ljava/lang/String; = "GameTrace"

.field private static final TRACE_BUFFER_SIZE:I = 0x800

.field private static final TRACE_BUFFER_SIZE_PATH:Ljava/lang/String; = "buffer_size_kb"

.field private static final TRACE_CLOCK_PATH:Ljava/lang/String; = "trace_clock"

.field private static final TRACE_FILE_SAVE_PATH:Ljava/lang/String; = "/data/system/migard/game_trace"

.field private static final TRACE_FS_PATH:Ljava/lang/String; = "/sys/kernel/tracing/"

.field private static final TRACE_MARKER_PATH:Ljava/lang/String; = "trace_marker"

.field private static final TRACE_OVERWRITE_PATH:Ljava/lang/String; = "options/overwrite"

.field private static final TRACE_PATH:Ljava/lang/String; = "trace"

.field private static final TRACE_PRINT_TGID_PATH:Ljava/lang/String; = "options/print-tgid"

.field private static final TRACE_RECORD_TGID_PATH:Ljava/lang/String; = "options/record-tgid"

.field private static final TRACE_TAGS_PROPERTY:Ljava/lang/String; = "debug.atrace.tags.enableflags"

.field private static final TRACING_ON_PATH:Ljava/lang/String; = "tracing_on"

.field private static final USER_INITIATED_PROPERTY:Ljava/lang/String; = "debug.atrace.user_initiated"

.field private static final XOR_ALGO:Ljava/lang/String; = "xor"

.field private static sInstance:Lcom/miui/server/migard/trace/GameTrace;


# instance fields
.field private isAsync:Z

.field private volatile isGameTraceRunning:Z

.field private isTraceSupported:Z

.field private final mCategories:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/migard/trace/GameTrace$TracingCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final mModuleName:Ljava/lang/String;

.field private mTraceBufferSizeKB:I

.field private mTraceCryptoAlgo:Ljava/lang/String;

.field private mTraceDir:Ljava/lang/String;

.field private mTraceFolder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 101
    new-instance v0, Lcom/miui/server/migard/trace/GameTrace;

    invoke-direct {v0}, Lcom/miui/server/migard/trace/GameTrace;-><init>()V

    sput-object v0, Lcom/miui/server/migard/trace/GameTrace;->sInstance:Lcom/miui/server/migard/trace/GameTrace;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z

    .line 98
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceDir:Ljava/lang/String;

    .line 99
    iput-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceCryptoAlgo:Ljava/lang/String;

    .line 100
    const-string v1, "ro.product.model"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mModuleName:Ljava/lang/String;

    .line 104
    iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    .line 105
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setBufferSize(I)V

    .line 106
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->initTracingCategories()V

    .line 107
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->initTraceFolder()V

    .line 108
    return-void
.end method

.method private addKernelCategory(Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "content"    # Lorg/json/JSONObject;

    .line 293
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 297
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const-string v1, "category"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "category":Ljava/lang/String;
    const-string v2, "details"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 299
    .local v2, "details":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 300
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 301
    .local v4, "temp":Lorg/json/JSONObject;
    if-eqz v4, :cond_0

    .line 302
    const-string v5, "path"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 303
    .local v5, "path":Ljava/lang/String;
    const-string v6, "enabled"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 304
    .local v6, "enabled":Z
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    .end local v5    # "path":Ljava/lang/String;
    .end local v6    # "enabled":Z
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 307
    .end local v3    # "i":I
    .end local v4    # "temp":Lorg/json/JSONObject;
    :cond_1
    invoke-virtual {p0, v1, v0}, Lcom/miui/server/migard/trace/GameTrace;->addKernelCategory(Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v1    # "category":Ljava/lang/String;
    .end local v2    # "details":Lorg/json/JSONArray;
    goto :goto_1

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "ex":Lorg/json/JSONException;
    const-string v1, "GameTrace"

    const-string v2, "parse config failed, add Kernel Category failed."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v0    # "ex":Lorg/json/JSONException;
    :goto_1
    return-void
.end method

.method private addKernelCategoryPath(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "content"    # Lorg/json/JSONObject;

    .line 315
    :try_start_0
    const-string v0, "category"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    .local v0, "category":Ljava/lang/String;
    const-string v1, "path"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "path":Ljava/lang/String;
    const-string v2, "enabled"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 318
    .local v2, "enabled":Z
    invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace;->addKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "enabled":Z
    goto :goto_0

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "ex":Lorg/json/JSONException;
    const-string v1, "GameTrace"

    const-string v2, "parse config failed, add kernel category path failed."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    .end local v0    # "ex":Lorg/json/JSONException;
    :goto_0
    return-void
.end method

.method private applyTraceBufferSize()V
    .locals 2

    .line 477
    iget v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceBufferSizeKB:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "buffer_size_kb"

    invoke-direct {p0, v1, v0}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    return-void
.end method

.method private checkCryptoAlgo(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "algo"    # Ljava/lang/String;

    .line 217
    const-string/jumbo v0, "xor"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "aes"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 221
    :cond_0
    const-string v0, "non"

    return-object v0

    .line 218
    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkCryptoAlgo algo: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameTrace"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return-object p1
.end method

.method private checkTraceDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkTraceDir path:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameTrace"

    invoke-static {v1, v0}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 204
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 205
    .local v2, "dir":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dir.exists: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " dir: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 207
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 210
    return-object v0

    .line 213
    .end local v2    # "dir":Ljava/io/File;
    :cond_0
    return-object v0
.end method

.method private cleanUpKernelCategories()V
    .locals 1

    .line 532
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->disableAllKernelCategories()V

    .line 533
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setTraceOverwriteEnable(Z)V

    .line 534
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->resetBufferSize()V

    .line 535
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setPrintTgidEnableIfPresent(Z)V

    .line 536
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearKernelTraceFuncs()V

    .line 537
    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setUserInitiatedTraceProperty(Z)V

    .line 538
    return-void
.end method

.method private cleanUpUserspaceCategories()V
    .locals 1

    .line 511
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setupUserspaceCategories(I)V

    .line 512
    return-void
.end method

.method private clearKernelTraceFuncs()V
    .locals 5

    .line 435
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "current_tracer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 436
    .local v0, "cfile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 437
    const-string v1, "nop"

    invoke-direct {p0, v2, v1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "set_ftrace_filter"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 440
    .local v1, "ffile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 441
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/server/migard/utils/FileUtils;->truncateSysFile(Ljava/lang/String;)V

    .line 443
    :cond_1
    return-void
.end method

.method private clearOldTrace()V
    .locals 3

    .line 156
    const-string v0, "GameTrace"

    const-string/jumbo v1, "start clearOldTrace."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const-string v0, "/data/system/migard/game_trace"

    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->getFileSort(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 158
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 159
    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    .line 160
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 158
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private clearTrace()V
    .locals 2

    .line 541
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "trace"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/migard/utils/FileUtils;->truncateSysFile(Ljava/lang/String;)V

    .line 542
    return-void
.end method

.method private disableAllKernelCategories()V
    .locals 4

    .line 522
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 523
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/trace/GameTrace$TracingCategory;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    .line 524
    .local v2, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    if-nez v2, :cond_0

    goto :goto_0

    .line 525
    :cond_0
    invoke-virtual {v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTags()I

    move-result v3

    if-nez v3, :cond_1

    .line 526
    invoke-virtual {v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTracePathList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/miui/server/migard/trace/GameTrace;->disableKernelCategories(Ljava/util/ArrayList;)V

    .line 528
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/miui/server/migard/trace/GameTrace$TracingCategory;>;"
    :cond_1
    goto :goto_0

    .line 529
    .end local v2    # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    :cond_2
    return-void
.end method

.method private disableKernelCategories(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 515
    .local p1, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 516
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/miui/server/migard/utils/FileUtils;->writeToSys(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 518
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private enableKernelCategoryPath(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "content"    # Lorg/json/JSONObject;

    .line 326
    :try_start_0
    const-string v0, "category"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "category":Ljava/lang/String;
    const-string v1, "path"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 328
    .local v1, "path":Ljava/lang/String;
    const-string v2, "enabled"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 329
    .local v2, "enabled":Z
    invoke-virtual {p0, v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace;->enableKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "enabled":Z
    goto :goto_0

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "ex":Lorg/json/JSONException;
    const-string v1, "GameTrace"

    const-string v2, "parse config failed, enable kernel category path failed."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    .end local v0    # "ex":Lorg/json/JSONException;
    :goto_0
    return-void
.end method

.method private generateTraceName()Ljava/lang/String;
    .locals 4

    .line 481
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyyMMddHHmmssSSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 482
    .local v0, "formatter":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "dateString":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_trace"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getFileSort(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/miui/server/migard/trace/GameTrace;->getFiles(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 167
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 168
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$1;

    invoke-direct {v1, p0}, Lcom/miui/server/migard/trace/GameTrace$1;-><init>(Lcom/miui/server/migard/trace/GameTrace;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 183
    :cond_0
    return-object v0
.end method

.method private getFiles(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .param p1, "realpath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 187
    .local p2, "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "realFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 189
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 190
    .local v1, "subfiles":[Ljava/io/File;
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 191
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 192
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, p2}, Lcom/miui/server/migard/trace/GameTrace;->getFiles(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    goto :goto_1

    .line 194
    :cond_0
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    .end local v4    # "file":Ljava/io/File;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "subfiles":[Ljava/io/File;
    :cond_1
    return-object p2
.end method

.method public static getInstance()Lcom/miui/server/migard/trace/GameTrace;
    .locals 1

    .line 111
    sget-object v0, Lcom/miui/server/migard/trace/GameTrace;->sInstance:Lcom/miui/server/migard/trace/GameTrace;

    return-object v0
.end method

.method private initTraceFolder()V
    .locals 3

    .line 660
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isTraceSupported:Z

    .line 661
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/kernel/tracing"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 662
    .local v0, "traceFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/kernel/debug/tracing"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 663
    .local v1, "debugFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 664
    const-string v2, "/sys/kernel/tracing/"

    iput-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    goto :goto_0

    .line 665
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 666
    const-string v2, "/sys/kernel/debug/tracing/"

    iput-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    goto :goto_0

    .line 668
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/miui/server/migard/trace/GameTrace;->isTraceSupported:Z

    .line 670
    :goto_0
    return-void
.end method

.method private initTracingCategories()V
    .locals 5

    .line 546
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/4 v2, 0x2

    const-string v3, "gfx"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/4 v2, 0x4

    const-string v3, "input"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x8

    const-string/jumbo v3, "view"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x10

    const-string/jumbo v3, "webview"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x20

    const-string/jumbo v3, "wm"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x40

    const-string v3, "am"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x80

    const-string/jumbo v3, "sm"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x100

    const-string v3, "audio"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x200

    const-string/jumbo v3, "video"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x400

    const-string v3, "camera"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x800

    const-string v3, "hal"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x2000

    const-string v3, "res"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/16 v2, 0x4000

    const-string v3, "dalvik"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const v2, 0x8000

    const-string v3, "rs"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/high16 v2, 0x10000

    const-string v3, "bionic"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/high16 v2, 0x20000

    const-string v3, "power"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/high16 v2, 0x40000

    const-string v3, "pm"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/high16 v2, 0x80000

    const-string/jumbo v3, "ss"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/high16 v2, 0x100000

    const-string v3, "database"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/high16 v2, 0x200000

    const-string v3, "network"

    invoke-direct {v1, v3, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "adb"

    const/high16 v3, 0x400000

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    const-string v2, "adb"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string/jumbo v2, "vibrator"

    const/high16 v3, 0x800000

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    const-string/jumbo v2, "vibrator"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "aidl"

    const/high16 v3, 0x1000000

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    const-string v2, "aidl"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "nnapi"

    const/high16 v3, 0x2000000

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    const-string v2, "nnapi"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "rro"

    const/high16 v3, 0x4000000

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    const-string v2, "rro"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    const/4 v0, 0x0

    .line 574
    .local v0, "temp":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "sched"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 575
    const-string v1, "events/sched/sched_switch/enable"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 576
    const-string v1, "events/sched/sched_wakeup/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 577
    const-string v1, "events/sched/sched_waking/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 578
    const-string v1, "events/sched/sched_blocked_reason/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 579
    const-string v1, "events/sched/sched_pi_setprio/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 580
    const-string v1, "events/sched/sched_process_exit/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 581
    const-string v1, "events/cgroup/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 582
    const-string v1, "events/oom/oom_score_adj_update/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 583
    const-string v1, "events/task/task_rename/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 584
    const-string v1, "events/task/task_newtask/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 585
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v4, "sched"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 587
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v4, "irq"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 588
    const-string v1, "events/irq/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 589
    const-string v1, "events/ipi/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 590
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v4, "irq"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v4, "freq"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 594
    const-string v1, "events/power/cpu_frequency/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 595
    const-string v1, "events/power/clock_set_rate/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 596
    const-string v1, "events/power/clock_disable/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 597
    const-string v1, "events/power/clock_enable/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 598
    const-string v1, "events/clk/clk_set_rate/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 599
    const-string v1, "events/clk/clk_disable/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 600
    const-string v1, "events/clk/clk_enable/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 601
    const-string v1, "events/power/cpu_frequency_limits/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 602
    const-string v1, "events/power/suspend_resume/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 603
    const-string v1, "events/cpuhp/cpuhp_enter/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 604
    const-string v1, "events/cpuhp/cpuhp_exit/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 605
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v4, "freq"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 607
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v4, "idle"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 608
    const-string v1, "events/power/cpu_idle/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 609
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v4, "idle"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 611
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v4, "disk"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 612
    const-string v1, "events/block/block_rq_issue/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 613
    const-string v1, "events/block/block_rq_complete/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 614
    const-string v1, "events/f2fs/f2fs_sync_file_enter/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 615
    const-string v1, "events/f2fs/f2fs_sync_file_exit/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 616
    const-string v1, "events/f2fs/f2fs_write_begin/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 617
    const-string v1, "events/f2fs/f2fs_write_end/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 618
    const-string v1, "events/ext4/ext4_da_write_begin/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 619
    const-string v1, "events/ext4/ext4_da_write_end/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 620
    const-string v1, "events/ext4/ext4_sync_file_enter/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 621
    const-string v1, "events/ext4/ext4_sync_file_exit/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 622
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v4, "disk"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 624
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string/jumbo v4, "sync"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 625
    const-string v1, "events/dma_fence/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 626
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string/jumbo v4, "sync"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v4, "memreclaim"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 630
    const-string v1, "events/vmscan/mm_vmscan_direct_reclaim_begin/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 631
    const-string v1, "events/vmscan/mm_vmscan_direct_reclaim_end/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 632
    const-string v1, "events/vmscan/mm_vmscan_kswapd_wake/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 633
    const-string v1, "events/vmscan/mm_vmscan_kswapd_sleep/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 634
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v4, "memreclaim"

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 636
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v4, "binder_driver"

    invoke-direct {v1, v4, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 637
    const-string v1, "events/binder/binder_transaction/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 638
    const-string v1, "events/binder/binder_transaction_received/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 639
    const-string v1, "events/binder/binder_transaction_alloc_buf/enable"

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 640
    const-string v1, "events/binder/binder_set_priority/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 641
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v2, "binder_driver"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "binder_lock"

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 644
    const-string v1, "events/binder/binder_lock/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 645
    const-string v1, "events/binder/binder_locked/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 646
    const-string v1, "events/binder/binder_unlock/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 647
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v2, "binder_lock"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 649
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string v2, "memory"

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 650
    const-string v1, "events/kmem/rss_stat/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 651
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string v2, "memory"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 653
    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const-string/jumbo v2, "thermal"

    invoke-direct {v1, v2, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;I)V

    move-object v0, v1

    .line 654
    const-string v1, "events/thermal/thermal_temperature/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 655
    const-string v1, "events/thermal/cdev_update/enable"

    invoke-virtual {v0, v1, v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 656
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    const-string/jumbo v2, "thermal"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 657
    return-void
.end method

.method private removeKernelCategoryPath(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "content"    # Lorg/json/JSONObject;

    .line 337
    :try_start_0
    const-string v0, "category"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 338
    .local v0, "category":Ljava/lang/String;
    const-string v1, "path"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/miui/server/migard/trace/GameTrace;->removeKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    .end local v0    # "category":Ljava/lang/String;
    .end local v1    # "path":Ljava/lang/String;
    goto :goto_0

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "ex":Lorg/json/JSONException;
    const-string v1, "GameTrace"

    const-string v2, "parse config failed, remove kernel category path failed."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    .end local v0    # "ex":Lorg/json/JSONException;
    :goto_0
    return-void
.end method

.method private resetBufferSize()V
    .locals 2

    .line 418
    const/16 v0, 0x800

    iput v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceBufferSizeKB:I

    .line 419
    const-string v0, "buffer_size_kb"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    return-void
.end method

.method private setCategoryPathEnabled(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "enabled"    # Z
    .param p4, "modify"    # Z

    .line 399
    const-string v0, "GameTrace"

    if-eqz p1, :cond_4

    if-nez p2, :cond_0

    goto :goto_1

    .line 403
    :cond_0
    const/4 v1, 0x0

    .line 404
    .local v1, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    check-cast v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    .line 405
    if-nez v1, :cond_1

    .line 406
    const-string v2, "remove kernel category path failed, unknown category."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    return-void

    .line 409
    :cond_1
    if-eqz p4, :cond_2

    .line 410
    invoke-virtual {v1, p2, p3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->setPathEnabled(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_3

    .line 411
    const-string v2, "enable kernel category path failed, unknown path."

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 414
    :cond_2
    invoke-virtual {v1, p2, p3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->addPath(Ljava/lang/String;Z)V

    .line 415
    :cond_3
    :goto_0
    return-void

    .line 400
    .end local v1    # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    :cond_4
    :goto_1
    const-string v1, "remove kernel category path failed, check your parameters."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    return-void
.end method

.method private setClock()V
    .locals 4

    .line 457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "trace_clock"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/migard/utils/FileUtils;->readFromSys(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 459
    .local v0, "str":Ljava/lang/String;
    const-string v2, "boot"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 460
    const-string v2, "boot"

    .local v2, "newClock":Ljava/lang/String;
    goto :goto_0

    .line 461
    .end local v2    # "newClock":Ljava/lang/String;
    :cond_0
    const-string v2, "mono"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 462
    const-string v2, "mono"

    .restart local v2    # "newClock":Ljava/lang/String;
    goto :goto_0

    .line 464
    .end local v2    # "newClock":Ljava/lang/String;
    :cond_1
    const-string v2, "global"

    .line 465
    .restart local v2    # "newClock":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 466
    return-void

    .line 467
    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    return-void
.end method

.method private setKernelContent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .line 507
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/miui/server/migard/utils/FileUtils;->writeToSys(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    return-void
.end method

.method private setKernelOptionEnable(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    const-string v1, "1"

    goto :goto_0

    :cond_0
    const-string v1, "0"

    :goto_0
    invoke-static {v0, v1}, Lcom/miui/server/migard/utils/FileUtils;->writeToSys(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    return-void
.end method

.method private setPrintTgidEnableIfPresent(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .line 446
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "options/print-tgid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 447
    .local v0, "pfile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 448
    invoke-direct {p0, v2, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V

    .line 450
    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "options/record-tgid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 451
    .local v1, "rfile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 452
    invoke-direct {p0, v3, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V

    .line 454
    :cond_1
    return-void
.end method

.method private setTraceEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .line 495
    const-string/jumbo v0, "tracing_on"

    invoke-direct {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V

    .line 496
    return-void
.end method

.method private setTraceOverwriteEnable(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .line 499
    const-string v0, "options/overwrite"

    invoke-direct {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->setKernelOptionEnable(Ljava/lang/String;Z)V

    .line 500
    return-void
.end method

.method private setUserInitiatedTraceProperty(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 492
    if-eqz p1, :cond_0

    const-string v0, "1"

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    const-string v1, "debug.atrace.user_initiated"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    return-void
.end method

.method private setupKernelCategories(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 471
    .local p1, "categories":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 472
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "1"

    goto :goto_1

    :cond_0
    const-string v3, "0"

    :goto_1
    invoke-static {v2, v3}, Lcom/miui/server/migard/utils/FileUtils;->writeToSys(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    goto :goto_0

    .line 474
    :cond_1
    return-void
.end method

.method private setupUserspaceCategories(I)V
    .locals 2
    .param p1, "tags"    # I

    .line 487
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 488
    .local v0, "value":Ljava/lang/String;
    const-string v1, "debug.atrace.tags.enableflags"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    return-void
.end method

.method private writeClockSyncMarker()V
    .locals 6

    .line 423
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "trace_marker"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 424
    .local v0, "markerFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 425
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    long-to-float v1, v3

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v1, v3

    .line 426
    .local v1, "realTime":F
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "trace_event_clock_sync: realtime_ts="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    long-to-float v4, v4

    div-float/2addr v4, v3

    .line 429
    .local v4, "monoTime":F
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "trace_event_clock_sync: parent_ts="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/miui/server/migard/trace/GameTrace;->setKernelContent(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    .end local v1    # "realTime":F
    .end local v4    # "monoTime":F
    :cond_0
    return-void
.end method


# virtual methods
.method public addKernelCategory(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 354
    .local p2, "details":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    const-string v1, "GameTrace"

    if-eqz v0, :cond_0

    .line 355
    const-string v0, "cannot add kernel category, while game trace running."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    return-void

    .line 358
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz p2, :cond_1

    .line 359
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    new-instance v1, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, p2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;-><init>(Ljava/lang/String;ILjava/util/HashMap;)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 361
    :cond_1
    const-string v0, "add kernel category failed, check your parameters."

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    :goto_0
    return-void
.end method

.method public addKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "enabled"    # Z

    .line 365
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    if-eqz v0, :cond_0

    .line 366
    const-string v0, "GameTrace"

    const-string v1, "cannot add kernel category path, while game trace running."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    return-void

    .line 369
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/migard/trace/GameTrace;->setCategoryPathEnabled(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 370
    return-void
.end method

.method public configTrace(Ljava/lang/String;)V
    .locals 0
    .param p1, "json"    # Ljava/lang/String;

    .line 288
    return-void
.end method

.method public dump(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "dirPath"    # Ljava/lang/String;
    .param p2, "zip"    # Z

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "dump cameinv dirPath: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameTrace"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceFolder:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "trace"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 266
    .local v0, "traceFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 267
    return-void

    .line 268
    :cond_0
    const/4 v1, 0x0

    .line 270
    .local v1, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v1, v2

    .line 271
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->generateTraceName()Ljava/lang/String;

    move-result-object v2

    .line 272
    .local v2, "name":Ljava/lang/String;
    invoke-static {v1, p1, v2}, Lcom/miui/server/migard/utils/FileUtils;->readSysToFile(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    nop

    .line 278
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 281
    goto :goto_0

    .line 279
    :catch_0
    move-exception v3

    .line 280
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 281
    .end local v3    # "e":Ljava/lang/Exception;
    goto :goto_0

    .line 276
    .end local v2    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v2

    goto :goto_1

    .line 273
    :catch_1
    move-exception v2

    .line 274
    .local v2, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    .end local v2    # "e":Ljava/lang/Exception;
    if-eqz v1, :cond_1

    .line 278
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 281
    goto :goto_0

    .line 279
    :catch_2
    move-exception v2

    .line 280
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 281
    .end local v2    # "e":Ljava/lang/Exception;
    nop

    .line 284
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearTrace()V

    .line 285
    return-void

    .line 276
    :goto_1
    if-eqz v1, :cond_2

    .line 278
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 281
    goto :goto_2

    .line 279
    :catch_3
    move-exception v3

    .line 280
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 283
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_2
    throw v2
.end method

.method public dump(Z)V
    .locals 1
    .param p1, "zip"    # Z

    .line 240
    iget-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceDir:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->dump(Ljava/lang/String;Z)V

    goto :goto_0

    .line 243
    :cond_0
    const-string v0, "/data/system/migard/game_trace"

    invoke-virtual {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->dump(Ljava/lang/String;Z)V

    .line 244
    :goto_0
    return-void
.end method

.method public enableKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "enabled"    # Z

    .line 373
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    if-eqz v0, :cond_0

    .line 374
    const-string v0, "GameTrace"

    const-string v1, "cannot enable kernel category path, while game trace running."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    return-void

    .line 377
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/server/migard/trace/GameTrace;->setCategoryPathEnabled(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 378
    return-void
.end method

.method public removeKernelCategoryPath(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "category"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .line 381
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    const-string v1, "GameTrace"

    if-eqz v0, :cond_0

    .line 382
    const-string v0, "cannot remove kernel category path, while game trace running."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    return-void

    .line 385
    :cond_0
    if-eqz p1, :cond_3

    if-nez p2, :cond_1

    goto :goto_0

    .line 389
    :cond_1
    const/4 v0, 0x0

    .line 390
    .local v0, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    .line 391
    if-nez v0, :cond_2

    .line 392
    const-string v2, "remove kernel category path failed, unknown category."

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    return-void

    .line 395
    :cond_2
    invoke-virtual {v0, p2}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->removePath(Ljava/lang/String;)V

    .line 396
    return-void

    .line 386
    .end local v0    # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    :cond_3
    :goto_0
    const-string v0, "remove kernel category path failed, check your parameters."

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    return-void
.end method

.method public setBufferSize(I)V
    .locals 2
    .param p1, "size"    # I

    .line 346
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    if-eqz v0, :cond_0

    .line 347
    const-string v0, "GameTrace"

    const-string v1, "cannot set buffer size, while game trace running."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    return-void

    .line 350
    :cond_0
    iput p1, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceBufferSizeKB:I

    .line 351
    return-void
.end method

.method public start(Ljava/util/ArrayList;Z)V
    .locals 5
    .param p2, "async"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 124
    .local p1, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-boolean p2, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z

    .line 125
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isTraceSupported:Z

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 127
    :cond_0
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    if-eqz v0, :cond_1

    .line 128
    const-string v0, "GameTrace"

    const-string/jumbo v1, "start game trace failed, game trace is already running."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    return-void

    .line 131
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    .line 132
    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setUserInitiatedTraceProperty(Z)V

    .line 133
    const/4 v1, 0x0

    .line 135
    .local v1, "tags":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 136
    iget-object v3, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;

    .line 137
    .local v3, "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    if-nez v3, :cond_2

    goto :goto_1

    .line 138
    :cond_2
    invoke-virtual {v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTags()I

    move-result v4

    if-eqz v4, :cond_3

    .line 139
    invoke-virtual {v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTags()I

    move-result v4

    or-int/2addr v1, v4

    goto :goto_1

    .line 141
    :cond_3
    invoke-virtual {v3}, Lcom/miui/server/migard/trace/GameTrace$TracingCategory;->getTracePathMap()Ljava/util/HashMap;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/miui/server/migard/trace/GameTrace;->setupKernelCategories(Ljava/util/HashMap;)V

    .line 135
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 144
    .end local v2    # "i":I
    .end local v3    # "tempCategory":Lcom/miui/server/migard/trace/GameTrace$TracingCategory;
    :cond_4
    invoke-direct {p0, p2}, Lcom/miui/server/migard/trace/GameTrace;->setTraceOverwriteEnable(Z)V

    .line 145
    invoke-direct {p0, v1}, Lcom/miui/server/migard/trace/GameTrace;->setupUserspaceCategories(I)V

    .line 146
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->applyTraceBufferSize()V

    .line 147
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->setClock()V

    .line 148
    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setPrintTgidEnableIfPresent(Z)V

    .line 149
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearKernelTraceFuncs()V

    .line 150
    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setTraceEnabled(Z)V

    .line 151
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearTrace()V

    .line 152
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->writeClockSyncMarker()V

    .line 153
    return-void

    .line 126
    .end local v1    # "tags":I
    :cond_5
    :goto_2
    return-void
.end method

.method public start(Z)V
    .locals 3
    .param p1, "async"    # Z

    .line 115
    iput-boolean p1, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v0, "categories":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/miui/server/migard/trace/GameTrace;->mCategories:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 118
    .local v2, "category":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    .end local v2    # "category":Ljava/lang/String;
    goto :goto_0

    .line 120
    :cond_0
    invoke-virtual {p0, v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->start(Ljava/util/ArrayList;Z)V

    .line 121
    return-void
.end method

.method public stop(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "dirPath"    # Ljava/lang/String;
    .param p2, "zip"    # Z

    .line 247
    iget-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    const-string v1, "GameTrace"

    if-nez v0, :cond_0

    .line 248
    const-string/jumbo v0, "stop game trace failed, game trace is not running."

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    return-void

    .line 251
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/trace/GameTrace;->setTraceEnabled(Z)V

    .line 252
    if-eqz p1, :cond_1

    const-string v2, ""

    if-eq p1, v2, :cond_1

    iget-boolean v2, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z

    if-nez v2, :cond_1

    .line 253
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isAsync: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/miui/server/migard/trace/GameTrace;->isAsync:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual {p0, p1, p2}, Lcom/miui/server/migard/trace/GameTrace;->dump(Ljava/lang/String;Z)V

    .line 256
    :cond_1
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->cleanUpUserspaceCategories()V

    .line 257
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->cleanUpKernelCategories()V

    .line 258
    iput-boolean v0, p0, Lcom/miui/server/migard/trace/GameTrace;->isGameTraceRunning:Z

    .line 259
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceDir:Ljava/lang/String;

    .line 260
    return-void
.end method

.method public stop(Z)V
    .locals 3
    .param p1, "zip"    # Z

    .line 226
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/migard/game_trace"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v0, "mTracePath":Ljava/io/File;
    iget-object v2, p0, Lcom/miui/server/migard/trace/GameTrace;->mTraceDir:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 228
    invoke-virtual {p0, v2, p1}, Lcom/miui/server/migard/trace/GameTrace;->stop(Ljava/lang/String;Z)V

    goto :goto_0

    .line 230
    :cond_0
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->generateTraceName()Ljava/lang/String;

    move-result-object v2

    .line 231
    .local v2, "name":Ljava/lang/String;
    invoke-direct {p0}, Lcom/miui/server/migard/trace/GameTrace;->clearOldTrace()V

    .line 232
    invoke-virtual {p0, v1, p1}, Lcom/miui/server/migard/trace/GameTrace;->stop(Ljava/lang/String;Z)V

    .line 233
    invoke-static {v0}, Lcom/miui/server/migard/utils/FileUtils;->delOldZipFile(Ljava/io/File;)V

    .line 234
    invoke-static {v1, v1, v2}, Lcom/miui/server/migard/utils/FileUtils;->readAllSystraceToZip(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-static {v0}, Lcom/miui/server/migard/utils/FileUtils;->delTmpTraceFile(Ljava/io/File;)V

    .line 237
    .end local v2    # "name":Ljava/lang/String;
    :goto_0
    return-void
.end method
