.class public Lcom/miui/server/migard/MiGardService;
.super Lmiui/migard/IMiGard$Stub;
.source "MiGardService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/migard/MiGardService$LocalService;,
        Lcom/miui/server/migard/MiGardService$Lifecycle;
    }
.end annotation


# static fields
.field public static DEBUG_VERSION:Z = false

.field private static final IMiGardSingleton:Landroid/util/Singleton;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Singleton<",
            "Lmiui/migard/IMiGard;",
            ">;"
        }
    .end annotation
.end field

.field public static final JOYOSE_NAME:Ljava/lang/String; = "xiaomi.joyose"

.field public static final MIGARD_DATA_PATH:Ljava/lang/String; = "/data/system/migard"

.field public static final SERVICE_NAME:Ljava/lang/String; = "migard"

.field public static final TAG:Ljava/lang/String; = "MiGardService"


# instance fields
.field private mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private final mContext:Landroid/content/Context;

.field private mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

.field mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

.field mMemCleanerDeprecated:Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

.field private mSurfaceFlingerSetCgroup:Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;


# direct methods
.method static bridge synthetic -$$Nest$fgetmGameMemoryReclaimer(Lcom/miui/server/migard/MiGardService;)Lcom/android/server/am/GameMemoryReclaimer;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/migard/MiGardService;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    return-object p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 43
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    sput-boolean v0, Lcom/miui/server/migard/MiGardService;->DEBUG_VERSION:Z

    .line 68
    new-instance v0, Lcom/miui/server/migard/MiGardService$1;

    invoke-direct {v0}, Lcom/miui/server/migard/MiGardService$1;-><init>()V

    sput-object v0, Lcom/miui/server/migard/MiGardService;->IMiGardSingleton:Landroid/util/Singleton;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .line 52
    invoke-direct {p0}, Lmiui/migard/IMiGard$Stub;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/miui/server/migard/MiGardService;->mContext:Landroid/content/Context;

    .line 54
    const-class v0, Lcom/miui/server/migard/MiGardInternal;

    new-instance v1, Lcom/miui/server/migard/MiGardService$LocalService;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/miui/server/migard/MiGardService$LocalService;-><init>(Lcom/miui/server/migard/MiGardService;Lcom/miui/server/migard/MiGardService$LocalService-IA;)V

    invoke-static {v0, v1}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 55
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/ActivityManagerService;

    iput-object v0, p0, Lcom/miui/server/migard/MiGardService;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 56
    new-instance v1, Lcom/android/server/am/GameMemoryReclaimer;

    invoke-direct {v1, p1, v0}, Lcom/android/server/am/GameMemoryReclaimer;-><init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V

    iput-object v1, p0, Lcom/miui/server/migard/MiGardService;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    .line 57
    new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleaner;

    iget-object v1, p0, Lcom/miui/server/migard/MiGardService;->mGameMemoryReclaimer:Lcom/android/server/am/GameMemoryReclaimer;

    invoke-direct {v0, p1, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;-><init>(Landroid/content/Context;Lcom/android/server/am/GameMemoryReclaimer;)V

    iput-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    .line 58
    new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-direct {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleanerDeprecated:Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    .line 59
    invoke-static {}, Lcom/miui/server/migard/ScreenStatusManager;->getInstance()Lcom/miui/server/migard/ScreenStatusManager;

    move-result-object v0

    invoke-static {}, Lcom/miui/server/migard/ScreenStatusManager;->getInstance()Lcom/miui/server/migard/ScreenStatusManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/server/migard/ScreenStatusManager;->getFilter()Landroid/content/IntentFilter;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 60
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleanerDeprecated:Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->isLowMemDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {}, Lcom/miui/server/migard/PackageStatusManager;->getInstance()Lcom/miui/server/migard/PackageStatusManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/migard/MiGardService;->mMemCleanerDeprecated:Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/PackageStatusManager;->registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V

    .line 62
    :cond_0
    new-instance v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;

    invoke-direct {v0, p1}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/migard/MiGardService;->mSurfaceFlingerSetCgroup:Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;

    .line 63
    invoke-virtual {v0}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->SupportProduct()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    invoke-static {}, Lcom/miui/server/migard/PackageStatusManager;->getInstance()Lcom/miui/server/migard/PackageStatusManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/migard/MiGardService;->mSurfaceFlingerSetCgroup:Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/PackageStatusManager;->registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V

    .line 66
    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/miui/server/migard/MiGardService-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/migard/MiGardService;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private checkPermission(I)Z
    .locals 1
    .param p1, "uid"    # I

    .line 216
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static getService()Lcom/miui/server/migard/MiGardService;
    .locals 1

    .line 78
    sget-object v0, Lcom/miui/server/migard/MiGardService;->IMiGardSingleton:Landroid/util/Singleton;

    invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/server/migard/MiGardService;

    return-object v0
.end method

.method static synthetic lambda$addGameCleanUserProtectList$0(Ljava/util/List;Z)V
    .locals 1
    .param p0, "list"    # Ljava/util/List;
    .param p1, "append"    # Z

    .line 145
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addUserProtectList(Ljava/util/List;Z)V

    .line 146
    return-void
.end method

.method static synthetic lambda$configCompactorWhiteList$4(Ljava/util/List;)V
    .locals 1
    .param p0, "list"    # Ljava/util/List;

    .line 201
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addCompactorCommonWhiteList(Ljava/util/List;)V

    .line 202
    return-void
.end method

.method static synthetic lambda$configGameMemoryCleaner$2(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "game"    # Ljava/lang/String;
    .param p1, "json"    # Ljava/lang/String;

    .line 167
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->configFromCloudControl(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method static synthetic lambda$configKillerWhiteList$3(Ljava/util/List;)V
    .locals 1
    .param p0, "list"    # Ljava/util/List;

    .line 191
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addKillerCommonWhilteList(Ljava/util/List;)V

    .line 192
    return-void
.end method

.method static synthetic lambda$configPowerWhiteList$5(Ljava/lang/String;)V
    .locals 1
    .param p0, "pkgList"    # Ljava/lang/String;

    .line 210
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->updatePowerWhiteList(Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method static synthetic lambda$removeGameCleanUserProtectList$1(Ljava/util/List;)V
    .locals 1
    .param p0, "list"    # Ljava/util/List;

    .line 155
    invoke-static {}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->getInstance()Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->removeUserProtectList(Ljava/util/List;)V

    .line 156
    return-void
.end method


# virtual methods
.method public addGameCleanUserProtectList(Ljava/util/List;Z)V
    .locals 2
    .param p2, "append"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 143
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda4;

    invoke-direct {v1, p1, p2}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda4;-><init>(Ljava/util/List;Z)V

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleanerDeprecated:Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-virtual {v0, p1, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->addGameCleanUserProtectList(Ljava/util/List;Z)V

    .line 149
    return-void
.end method

.method public configCompactorWhiteList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 199
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda1;

    invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda1;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V

    .line 204
    :cond_0
    return-void
.end method

.method public configGameList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 181
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->addGameList(Ljava/util/List;)V

    .line 184
    :cond_0
    return-void
.end method

.method public configGameMemoryCleaner(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "game"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;

    .line 165
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda3;

    invoke-direct {v1, p1, p2}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V

    .line 170
    :cond_0
    return-void
.end method

.method public configKillerWhiteList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 189
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda0;

    invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda0;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V

    .line 194
    :cond_0
    return-void
.end method

.method public configPowerWhiteList(Ljava/lang/String;)V
    .locals 2
    .param p1, "pkgList"    # Ljava/lang/String;

    .line 208
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda2;

    invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda2;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V

    .line 213
    :cond_0
    return-void
.end method

.method public configTrace(Ljava/lang/String;)V
    .locals 1
    .param p1, "json"    # Ljava/lang/String;

    .line 129
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    invoke-static {}, Lcom/miui/server/migard/trace/GameTrace;->getInstance()Lcom/miui/server/migard/trace/GameTrace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->configTrace(Ljava/lang/String;)V

    .line 131
    :cond_0
    return-void
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mContext:Landroid/content/Context;

    const-string v1, "MiGardService"

    invoke-static {v0, v1, p2}, Lcom/android/internal/util/DumpUtils;->checkDumpPermission(Landroid/content/Context;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->dump(Ljava/io/PrintWriter;)V

    .line 224
    return-void
.end method

.method public dumpTrace(Z)V
    .locals 1
    .param p1, "zip"    # Z

    .line 123
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-static {}, Lcom/miui/server/migard/trace/GameTrace;->getInstance()Lcom/miui/server/migard/trace/GameTrace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->dump(Z)V

    .line 125
    :cond_0
    return-void
.end method

.method public onShellCommand(Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)V
    .locals 8
    .param p1, "in"    # Ljava/io/FileDescriptor;
    .param p2, "out"    # Ljava/io/FileDescriptor;
    .param p3, "err"    # Ljava/io/FileDescriptor;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "callback"    # Landroid/os/ShellCallback;
    .param p6, "resultReceiver"    # Landroid/os/ResultReceiver;

    .line 230
    new-instance v0, Lcom/miui/server/migard/MiGardShellCommand;

    invoke-direct {v0, p0}, Lcom/miui/server/migard/MiGardShellCommand;-><init>(Lcom/miui/server/migard/MiGardService;)V

    .line 231
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/miui/server/migard/MiGardShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I

    .line 232
    return-void
.end method

.method public reclaimBackgroundMemory()V
    .locals 1

    .line 174
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->reclaimBackgroundMemory()V

    .line 177
    :cond_0
    return-void
.end method

.method public removeGameCleanUserProtectList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 153
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda5;

    invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda5;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/miui/server/migard/MiGardService;->mMemCleanerDeprecated:Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->removeGameCleanUserProtectList(Ljava/util/List;)V

    .line 159
    return-void
.end method

.method protected setTraceBufferSize(I)V
    .locals 1
    .param p1, "size"    # I

    .line 134
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-static {}, Lcom/miui/server/migard/trace/GameTrace;->getInstance()Lcom/miui/server/migard/trace/GameTrace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->setBufferSize(I)V

    .line 136
    :cond_0
    return-void
.end method

.method public startDefaultTrace(Z)V
    .locals 1
    .param p1, "async"    # Z

    .line 100
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {}, Lcom/miui/server/migard/trace/GameTrace;->getInstance()Lcom/miui/server/migard/trace/GameTrace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->start(Z)V

    .line 102
    :cond_0
    return-void
.end method

.method public startTrace([Ljava/lang/String;Z)V
    .locals 4
    .param p1, "categories"    # [Ljava/lang/String;
    .param p2, "async"    # Z

    .line 106
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v0, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 109
    .local v3, "c":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    .end local v3    # "c":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    :cond_0
    invoke-static {}, Lcom/miui/server/migard/trace/GameTrace;->getInstance()Lcom/miui/server/migard/trace/GameTrace;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/miui/server/migard/trace/GameTrace;->start(Ljava/util/ArrayList;Z)V

    .line 113
    .end local v0    # "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method

.method public stopTrace(Z)V
    .locals 1
    .param p1, "zip"    # Z

    .line 117
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-static {}, Lcom/miui/server/migard/trace/GameTrace;->getInstance()Lcom/miui/server/migard/trace/GameTrace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->stop(Z)V

    .line 119
    :cond_0
    return-void
.end method
