public abstract class com.miui.server.migard.MiGardInternal {
	 /* .source "MiGardInternal.java" */
	 /* # direct methods */
	 public com.miui.server.migard.MiGardInternal ( ) {
		 /* .locals 0 */
		 /* .line 9 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public abstract void addGameProcessCompactor ( com.android.server.am.IGameProcessAction$IGameProcessActionConfig p0 ) {
	 } // .end method
	 public abstract void addGameProcessKiller ( com.android.server.am.IGameProcessAction$IGameProcessActionConfig p0 ) {
	 } // .end method
	 public abstract void notifyGameBackground ( ) {
	 } // .end method
	 public abstract void notifyGameForeground ( java.lang.String p0 ) {
	 } // .end method
	 public abstract void notifyProcessDied ( Integer p0 ) {
	 } // .end method
	 public abstract void onProcessKilled ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
	 } // .end method
	 public abstract void onProcessStart ( Integer p0, Integer p1, java.lang.String p2, java.lang.String p3 ) {
	 } // .end method
	 public abstract void onVpnConnected ( java.lang.String p0, Boolean p1 ) {
	 } // .end method
	 public abstract void reclaimBackgroundForGame ( Long p0 ) {
	 } // .end method
