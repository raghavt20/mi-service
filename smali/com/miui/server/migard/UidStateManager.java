public class com.miui.server.migard.UidStateManager {
	 /* .source "UidStateManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.lang.String TAG;
private static com.miui.server.migard.UidStateManager sInstance;
/* # instance fields */
private java.util.List mCallbacks;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/List<", */
/* "Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.app.IUidObserver mUidObserver;
/* # direct methods */
static java.util.List -$$Nest$fgetmCallbacks ( com.miui.server.migard.UidStateManager p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mCallbacks;
} // .end method
static java.lang.String -$$Nest$sfgetTAG ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.migard.UidStateManager.TAG;
} // .end method
static com.miui.server.migard.UidStateManager ( ) {
/* .locals 1 */
/* .line 15 */
/* const-class v0, Lcom/miui/server/migard/UidStateManager; */
(( java.lang.Class ) v0 ).getSimpleName ( ); // invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;
/* .line 16 */
/* new-instance v0, Lcom/miui/server/migard/UidStateManager; */
/* invoke-direct {v0}, Lcom/miui/server/migard/UidStateManager;-><init>()V */
return;
} // .end method
public com.miui.server.migard.UidStateManager ( ) {
/* .locals 1 */
/* .line 14 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 23 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
this.mCallbacks = v0;
/* .line 29 */
/* new-instance v0, Lcom/miui/server/migard/UidStateManager$1; */
/* invoke-direct {v0, p0}, Lcom/miui/server/migard/UidStateManager$1;-><init>(Lcom/miui/server/migard/UidStateManager;)V */
this.mUidObserver = v0;
return;
} // .end method
public static com.miui.server.migard.UidStateManager getInstance ( ) {
/* .locals 1 */
/* .line 26 */
v0 = com.miui.server.migard.UidStateManager.sInstance;
} // .end method
/* # virtual methods */
public void registerCallback ( com.miui.server.migard.UidStateManager$IUidStateChangedCallback p0 ) {
/* .locals 5 */
/* .param p1, "cb" # Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback; */
/* .line 70 */
v0 = com.miui.server.migard.UidStateManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unregister callback, name:"; // const-string v2, "Unregister callback, name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 71 */
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_0 */
/* .line 73 */
try { // :try_start_0
android.app.ActivityManagerNative .getDefault ( );
/* .line 74 */
/* .local v0, "activityManager":Landroid/app/IActivityManager; */
v1 = this.mUidObserver;
int v2 = -1; // const/4 v2, -0x1
int v3 = 0; // const/4 v3, 0x0
int v4 = 3; // const/4 v4, 0x3
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 79 */
} // .end local v0 # "activityManager":Landroid/app/IActivityManager;
/* .line 77 */
/* :catch_0 */
/* move-exception v0 */
/* .line 78 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 81 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_0
} // :goto_0
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_1 */
/* .line 82 */
v0 = this.mCallbacks;
/* .line 83 */
} // :cond_1
return;
} // .end method
public void unregisterCallback ( com.miui.server.migard.UidStateManager$IUidStateChangedCallback p0 ) {
/* .locals 3 */
/* .param p1, "cb" # Lcom/miui/server/migard/UidStateManager$IUidStateChangedCallback; */
/* .line 86 */
v0 = com.miui.server.migard.UidStateManager.TAG;
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Unregister callback, name:"; // const-string v2, "Unregister callback, name:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.migard.utils.LogUtils .d ( v0,v1 );
/* .line 87 */
v0 = v0 = this.mCallbacks;
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 88 */
v0 = this.mCallbacks;
/* .line 89 */
} // :cond_0
v0 = v0 = this.mCallbacks;
/* if-nez v0, :cond_1 */
/* .line 91 */
try { // :try_start_0
android.app.ActivityManagerNative .getDefault ( );
/* .line 92 */
/* .local v0, "activityManager":Landroid/app/IActivityManager; */
v1 = this.mUidObserver;
/* :try_end_0 */
/* .catch Landroid/os/RemoteException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 95 */
} // .end local v0 # "activityManager":Landroid/app/IActivityManager;
/* .line 93 */
/* :catch_0 */
/* move-exception v0 */
/* .line 94 */
/* .local v0, "e":Landroid/os/RemoteException; */
(( android.os.RemoteException ) v0 ).printStackTrace ( ); // invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
/* .line 97 */
} // .end local v0 # "e":Landroid/os/RemoteException;
} // :cond_1
} // :goto_0
return;
} // .end method
