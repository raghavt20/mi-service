.class Lcom/miui/server/migard/MiGardShellCommand;
.super Landroid/os/ShellCommand;
.source "MiGardShellCommand.java"


# instance fields
.field mService:Lcom/miui/server/migard/MiGardService;


# direct methods
.method constructor <init>(Lcom/miui/server/migard/MiGardService;)V
    .locals 0
    .param p1, "service"    # Lcom/miui/server/migard/MiGardService;

    .line 9
    invoke-direct {p0}, Landroid/os/ShellCommand;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    .line 11
    return-void
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;)I
    .locals 6
    .param p1, "cmd"    # Ljava/lang/String;

    .line 15
    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 16
    .local v0, "pw":Ljava/io/PrintWriter;
    if-nez p1, :cond_0

    .line 17
    invoke-virtual {p0, p1}, Lcom/miui/server/migard/MiGardShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 19
    :cond_0
    const/4 v1, 0x0

    .line 20
    .local v1, "async":Z
    const/4 v2, 0x0

    .line 22
    .local v2, "compressed":Z
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_1
    goto :goto_0

    :sswitch_0
    const-string/jumbo v4, "stop-trace"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :sswitch_1
    const-string/jumbo v4, "start-trace"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v3

    goto :goto_1

    :sswitch_2
    const-string v4, "dump-trace"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    goto :goto_1

    :sswitch_3
    const-string/jumbo v4, "trace-buffer-size"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x3

    goto :goto_1

    :goto_0
    const/4 v4, -0x1

    :goto_1
    packed-switch v4, :pswitch_data_0

    .line 43
    goto :goto_2

    .line 36
    :pswitch_0
    iget-object v4, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/miui/server/migard/MiGardService;->setTraceBufferSize(I)V

    .line 37
    return v3

    .line 32
    :pswitch_1
    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    move v2, v4

    .line 33
    iget-object v4, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    invoke-virtual {v4, v2}, Lcom/miui/server/migard/MiGardService;->dumpTrace(Z)V

    .line 34
    return v3

    .line 28
    :pswitch_2
    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    move v2, v4

    .line 29
    iget-object v4, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    invoke-virtual {v4, v2}, Lcom/miui/server/migard/MiGardService;->stopTrace(Z)V

    .line 30
    return v3

    .line 24
    :pswitch_3
    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    move v1, v4

    .line 25
    iget-object v4, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    invoke-virtual {v4, v1}, Lcom/miui/server/migard/MiGardService;->startDefaultTrace(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    return v3

    .line 41
    :catch_0
    move-exception v4

    .line 42
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 46
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    iget-object v4, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    iget-object v4, v4, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getNextArgRequired()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p1, v5, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onShellCommand(Ljava/lang/String;Ljava/lang/String;Ljava/io/PrintWriter;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 47
    return v3

    .line 49
    :cond_2
    invoke-virtual {p0, p1}, Lcom/miui/server/migard/MiGardShellCommand;->handleDefaultCommands(Ljava/lang/String;)I

    move-result v3

    return v3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6717a71a -> :sswitch_3
        -0x1d0d07d4 -> :sswitch_2
        0x4fec781a -> :sswitch_1
        0x6ebe83ba -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHelp()V
    .locals 2

    .line 54
    invoke-virtual {p0}, Lcom/miui/server/migard/MiGardShellCommand;->getOutPrintWriter()Ljava/io/PrintWriter;

    move-result-object v0

    .line 55
    .local v0, "pw":Ljava/io/PrintWriter;
    const-string v1, "MiGardService commands:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 56
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 57
    const-string/jumbo v1, "start-trace [async=true|false]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 58
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 59
    const-string/jumbo v1, "stop-trace [compressed=true|false]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 61
    const-string v1, "dump-trace [compressed=true|false]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 62
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 63
    const-string/jumbo v1, "trace-buffer-size [size KB]"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 64
    invoke-virtual {v0}, Ljava/io/PrintWriter;->println()V

    .line 65
    iget-object v1, p0, Lcom/miui/server/migard/MiGardShellCommand;->mService:Lcom/miui/server/migard/MiGardService;

    iget-object v1, v1, Lcom/miui/server/migard/MiGardService;->mMemCleaner:Lcom/miui/server/migard/memory/GameMemoryCleaner;

    invoke-virtual {v1, v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->onShellHelp(Ljava/io/PrintWriter;)V

    .line 66
    return-void
.end method
