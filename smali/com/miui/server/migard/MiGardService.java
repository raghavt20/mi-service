public class com.miui.server.migard.MiGardService extends miui.migard.IMiGard$Stub {
	 /* .source "MiGardService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/migard/MiGardService$LocalService;, */
	 /* Lcom/miui/server/migard/MiGardService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
public static Boolean DEBUG_VERSION;
private static final android.util.Singleton IMiGardSingleton;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Landroid/util/Singleton<", */
/* "Lmiui/migard/IMiGard;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final java.lang.String JOYOSE_NAME;
public static final java.lang.String MIGARD_DATA_PATH;
public static final java.lang.String SERVICE_NAME;
public static final java.lang.String TAG;
/* # instance fields */
private com.android.server.am.ActivityManagerService mActivityManagerService;
private final android.content.Context mContext;
private com.android.server.am.GameMemoryReclaimer mGameMemoryReclaimer;
com.miui.server.migard.memory.GameMemoryCleaner mMemCleaner;
com.miui.server.migard.memory.GameMemoryCleanerDeprecated mMemCleanerDeprecated;
private com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup mSurfaceFlingerSetCgroup;
/* # direct methods */
static com.android.server.am.GameMemoryReclaimer -$$Nest$fgetmGameMemoryReclaimer ( com.miui.server.migard.MiGardService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mGameMemoryReclaimer;
} // .end method
static com.miui.server.migard.MiGardService ( ) {
/* .locals 1 */
/* .line 43 */
/* sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z */
com.miui.server.migard.MiGardService.DEBUG_VERSION = (v0!= 0);
/* .line 68 */
/* new-instance v0, Lcom/miui/server/migard/MiGardService$1; */
/* invoke-direct {v0}, Lcom/miui/server/migard/MiGardService$1;-><init>()V */
return;
} // .end method
private com.miui.server.migard.MiGardService ( ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 52 */
/* invoke-direct {p0}, Lmiui/migard/IMiGard$Stub;-><init>()V */
/* .line 53 */
this.mContext = p1;
/* .line 54 */
/* const-class v0, Lcom/miui/server/migard/MiGardInternal; */
/* new-instance v1, Lcom/miui/server/migard/MiGardService$LocalService; */
int v2 = 0; // const/4 v2, 0x0
/* invoke-direct {v1, p0, v2}, Lcom/miui/server/migard/MiGardService$LocalService;-><init>(Lcom/miui/server/migard/MiGardService;Lcom/miui/server/migard/MiGardService$LocalService-IA;)V */
com.android.server.LocalServices .addService ( v0,v1 );
/* .line 55 */
android.app.ActivityManagerNative .getDefault ( );
/* check-cast v0, Lcom/android/server/am/ActivityManagerService; */
this.mActivityManagerService = v0;
/* .line 56 */
/* new-instance v1, Lcom/android/server/am/GameMemoryReclaimer; */
/* invoke-direct {v1, p1, v0}, Lcom/android/server/am/GameMemoryReclaimer;-><init>(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V */
this.mGameMemoryReclaimer = v1;
/* .line 57 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleaner; */
v1 = this.mGameMemoryReclaimer;
/* invoke-direct {v0, p1, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;-><init>(Landroid/content/Context;Lcom/android/server/am/GameMemoryReclaimer;)V */
this.mMemCleaner = v0;
/* .line 58 */
/* new-instance v0, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated; */
/* invoke-direct {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;-><init>(Landroid/content/Context;)V */
this.mMemCleanerDeprecated = v0;
/* .line 59 */
com.miui.server.migard.ScreenStatusManager .getInstance ( );
com.miui.server.migard.ScreenStatusManager .getInstance ( );
(( com.miui.server.migard.ScreenStatusManager ) v1 ).getFilter ( ); // invoke-virtual {v1}, Lcom/miui/server/migard/ScreenStatusManager;->getFilter()Landroid/content/IntentFilter;
(( android.content.Context ) p1 ).registerReceiver ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
/* .line 60 */
v0 = this.mMemCleanerDeprecated;
v0 = (( com.miui.server.migard.memory.GameMemoryCleanerDeprecated ) v0 ).isLowMemDevice ( ); // invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->isLowMemDevice()Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 61 */
com.miui.server.migard.PackageStatusManager .getInstance ( );
v1 = this.mMemCleanerDeprecated;
(( com.miui.server.migard.PackageStatusManager ) v0 ).registerCallback ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/PackageStatusManager;->registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V
/* .line 62 */
} // :cond_0
/* new-instance v0, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup; */
/* invoke-direct {v0, p1}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;-><init>(Landroid/content/Context;)V */
this.mSurfaceFlingerSetCgroup = v0;
/* .line 63 */
v0 = (( com.miui.server.migard.surfaceflinger.SurfaceFlingerSetCgroup ) v0 ).SupportProduct ( ); // invoke-virtual {v0}, Lcom/miui/server/migard/surfaceflinger/SurfaceFlingerSetCgroup;->SupportProduct()Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 64 */
com.miui.server.migard.PackageStatusManager .getInstance ( );
v1 = this.mSurfaceFlingerSetCgroup;
(( com.miui.server.migard.PackageStatusManager ) v0 ).registerCallback ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/PackageStatusManager;->registerCallback(Lcom/miui/server/migard/PackageStatusManager$IForegroundChangedCallback;)V
/* .line 66 */
} // :cond_1
return;
} // .end method
 com.miui.server.migard.MiGardService ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/migard/MiGardService;-><init>(Landroid/content/Context;)V */
return;
} // .end method
private Boolean checkPermission ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "uid" # I */
/* .line 216 */
/* const/16 v0, 0x3e8 */
/* if-ne p1, v0, :cond_0 */
int v0 = 1; // const/4 v0, 0x1
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :goto_0
} // .end method
public static com.miui.server.migard.MiGardService getService ( ) {
/* .locals 1 */
/* .line 78 */
v0 = com.miui.server.migard.MiGardService.IMiGardSingleton;
(( android.util.Singleton ) v0 ).get ( ); // invoke-virtual {v0}, Landroid/util/Singleton;->get()Ljava/lang/Object;
/* check-cast v0, Lcom/miui/server/migard/MiGardService; */
} // .end method
static void lambda$addGameCleanUserProtectList$0 ( java.util.List p0, Boolean p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "list" # Ljava/util/List; */
/* .param p1, "append" # Z */
/* .line 145 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).addUserProtectList ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addUserProtectList(Ljava/util/List;Z)V
/* .line 146 */
return;
} // .end method
static void lambda$configCompactorWhiteList$4 ( java.util.List p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "list" # Ljava/util/List; */
/* .line 201 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).addCompactorCommonWhiteList ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addCompactorCommonWhiteList(Ljava/util/List;)V
/* .line 202 */
return;
} // .end method
static void lambda$configGameMemoryCleaner$2 ( java.lang.String p0, java.lang.String p1 ) { //synthethic
/* .locals 1 */
/* .param p0, "game" # Ljava/lang/String; */
/* .param p1, "json" # Ljava/lang/String; */
/* .line 167 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).configFromCloudControl ( p0, p1 ); // invoke-virtual {v0, p0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->configFromCloudControl(Ljava/lang/String;Ljava/lang/String;)V
/* .line 168 */
return;
} // .end method
static void lambda$configKillerWhiteList$3 ( java.util.List p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "list" # Ljava/util/List; */
/* .line 191 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).addKillerCommonWhilteList ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->addKillerCommonWhilteList(Ljava/util/List;)V
/* .line 192 */
return;
} // .end method
static void lambda$configPowerWhiteList$5 ( java.lang.String p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "pkgList" # Ljava/lang/String; */
/* .line 210 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).updatePowerWhiteList ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->updatePowerWhiteList(Ljava/lang/String;)V
/* .line 211 */
return;
} // .end method
static void lambda$removeGameCleanUserProtectList$1 ( java.util.List p0 ) { //synthethic
/* .locals 1 */
/* .param p0, "list" # Ljava/util/List; */
/* .line 155 */
com.miui.server.migard.memory.GameMemoryCleanerConfig .getInstance ( );
(( com.miui.server.migard.memory.GameMemoryCleanerConfig ) v0 ).removeUserProtectList ( p0 ); // invoke-virtual {v0, p0}, Lcom/miui/server/migard/memory/GameMemoryCleanerConfig;->removeUserProtectList(Ljava/util/List;)V
/* .line 156 */
return;
} // .end method
/* # virtual methods */
public void addGameCleanUserProtectList ( java.util.List p0, Boolean p1 ) {
/* .locals 2 */
/* .param p2, "append" # Z */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;Z)V" */
/* } */
} // .end annotation
/* .line 143 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 144 */
v0 = this.mMemCleaner;
/* new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda4; */
/* invoke-direct {v1, p1, p2}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda4;-><init>(Ljava/util/List;Z)V */
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).runOnThread ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V
/* .line 148 */
} // :cond_0
v0 = this.mMemCleanerDeprecated;
(( com.miui.server.migard.memory.GameMemoryCleanerDeprecated ) v0 ).addGameCleanUserProtectList ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->addGameCleanUserProtectList(Ljava/util/List;Z)V
/* .line 149 */
return;
} // .end method
public void configCompactorWhiteList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 199 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 200 */
v0 = this.mMemCleaner;
/* new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda1; */
/* invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda1;-><init>(Ljava/util/List;)V */
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).runOnThread ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V
/* .line 204 */
} // :cond_0
return;
} // .end method
public void configGameList ( java.util.List p0 ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 181 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 182 */
v0 = this.mMemCleaner;
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).addGameList ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->addGameList(Ljava/util/List;)V
/* .line 184 */
} // :cond_0
return;
} // .end method
public void configGameMemoryCleaner ( java.lang.String p0, java.lang.String p1 ) {
/* .locals 2 */
/* .param p1, "game" # Ljava/lang/String; */
/* .param p2, "json" # Ljava/lang/String; */
/* .line 165 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 166 */
v0 = this.mMemCleaner;
/* new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda3; */
/* invoke-direct {v1, p1, p2}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda3;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).runOnThread ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V
/* .line 170 */
} // :cond_0
return;
} // .end method
public void configKillerWhiteList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 189 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 190 */
v0 = this.mMemCleaner;
/* new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda0; */
/* invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda0;-><init>(Ljava/util/List;)V */
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).runOnThread ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V
/* .line 194 */
} // :cond_0
return;
} // .end method
public void configPowerWhiteList ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "pkgList" # Ljava/lang/String; */
/* .line 208 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 209 */
v0 = this.mMemCleaner;
/* new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda2; */
/* invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda2;-><init>(Ljava/lang/String;)V */
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).runOnThread ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V
/* .line 213 */
} // :cond_0
return;
} // .end method
public void configTrace ( java.lang.String p0 ) {
/* .locals 1 */
/* .param p1, "json" # Ljava/lang/String; */
/* .line 129 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 130 */
com.miui.server.migard.trace.GameTrace .getInstance ( );
(( com.miui.server.migard.trace.GameTrace ) v0 ).configTrace ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->configTrace(Ljava/lang/String;)V
/* .line 131 */
} // :cond_0
return;
} // .end method
public void dump ( java.io.FileDescriptor p0, java.io.PrintWriter p1, java.lang.String[] p2 ) {
/* .locals 2 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .param p2, "pw" # Ljava/io/PrintWriter; */
/* .param p3, "args" # [Ljava/lang/String; */
/* .line 221 */
v0 = this.mContext;
final String v1 = "MiGardService"; // const-string v1, "MiGardService"
v0 = com.android.internal.util.DumpUtils .checkDumpPermission ( v0,v1,p2 );
/* if-nez v0, :cond_0 */
/* .line 222 */
return;
/* .line 223 */
} // :cond_0
v0 = this.mMemCleaner;
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).dump ( p2 ); // invoke-virtual {v0, p2}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->dump(Ljava/io/PrintWriter;)V
/* .line 224 */
return;
} // .end method
public void dumpTrace ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "zip" # Z */
/* .line 123 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 124 */
com.miui.server.migard.trace.GameTrace .getInstance ( );
(( com.miui.server.migard.trace.GameTrace ) v0 ).dump ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->dump(Z)V
/* .line 125 */
} // :cond_0
return;
} // .end method
public void onShellCommand ( java.io.FileDescriptor p0, java.io.FileDescriptor p1, java.io.FileDescriptor p2, java.lang.String[] p3, android.os.ShellCallback p4, android.os.ResultReceiver p5 ) {
/* .locals 8 */
/* .param p1, "in" # Ljava/io/FileDescriptor; */
/* .param p2, "out" # Ljava/io/FileDescriptor; */
/* .param p3, "err" # Ljava/io/FileDescriptor; */
/* .param p4, "args" # [Ljava/lang/String; */
/* .param p5, "callback" # Landroid/os/ShellCallback; */
/* .param p6, "resultReceiver" # Landroid/os/ResultReceiver; */
/* .line 230 */
/* new-instance v0, Lcom/miui/server/migard/MiGardShellCommand; */
/* invoke-direct {v0, p0}, Lcom/miui/server/migard/MiGardShellCommand;-><init>(Lcom/miui/server/migard/MiGardService;)V */
/* .line 231 */
/* move-object v1, p0 */
/* move-object v2, p1 */
/* move-object v3, p2 */
/* move-object v4, p3 */
/* move-object v5, p4 */
/* move-object v6, p5 */
/* move-object v7, p6 */
/* invoke-virtual/range {v0 ..v7}, Lcom/miui/server/migard/MiGardShellCommand;->exec(Landroid/os/Binder;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;Ljava/io/FileDescriptor;[Ljava/lang/String;Landroid/os/ShellCallback;Landroid/os/ResultReceiver;)I */
/* .line 232 */
return;
} // .end method
public void reclaimBackgroundMemory ( ) {
/* .locals 1 */
/* .line 174 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 175 */
v0 = this.mMemCleaner;
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).reclaimBackgroundMemory ( ); // invoke-virtual {v0}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->reclaimBackgroundMemory()V
/* .line 177 */
} // :cond_0
return;
} // .end method
public void removeGameCleanUserProtectList ( java.util.List p0 ) {
/* .locals 2 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/List<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 153 */
/* .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 154 */
v0 = this.mMemCleaner;
/* new-instance v1, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda5; */
/* invoke-direct {v1, p1}, Lcom/miui/server/migard/MiGardService$$ExternalSyntheticLambda5;-><init>(Ljava/util/List;)V */
(( com.miui.server.migard.memory.GameMemoryCleaner ) v0 ).runOnThread ( v1 ); // invoke-virtual {v0, v1}, Lcom/miui/server/migard/memory/GameMemoryCleaner;->runOnThread(Ljava/lang/Runnable;)V
/* .line 158 */
} // :cond_0
v0 = this.mMemCleanerDeprecated;
(( com.miui.server.migard.memory.GameMemoryCleanerDeprecated ) v0 ).removeGameCleanUserProtectList ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/memory/GameMemoryCleanerDeprecated;->removeGameCleanUserProtectList(Ljava/util/List;)V
/* .line 159 */
return;
} // .end method
protected void setTraceBufferSize ( Integer p0 ) {
/* .locals 1 */
/* .param p1, "size" # I */
/* .line 134 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 135 */
com.miui.server.migard.trace.GameTrace .getInstance ( );
(( com.miui.server.migard.trace.GameTrace ) v0 ).setBufferSize ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->setBufferSize(I)V
/* .line 136 */
} // :cond_0
return;
} // .end method
public void startDefaultTrace ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "async" # Z */
/* .line 100 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 101 */
com.miui.server.migard.trace.GameTrace .getInstance ( );
(( com.miui.server.migard.trace.GameTrace ) v0 ).start ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->start(Z)V
/* .line 102 */
} // :cond_0
return;
} // .end method
public void startTrace ( java.lang.String[] p0, Boolean p1 ) {
/* .locals 4 */
/* .param p1, "categories" # [Ljava/lang/String; */
/* .param p2, "async" # Z */
/* .line 106 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 107 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 108 */
/* .local v0, "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
/* array-length v1, p1 */
int v2 = 0; // const/4 v2, 0x0
} // :goto_0
/* if-ge v2, v1, :cond_0 */
/* aget-object v3, p1, v2 */
/* .line 109 */
/* .local v3, "c":Ljava/lang/String; */
(( java.util.ArrayList ) v0 ).add ( v3 ); // invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 108 */
} // .end local v3 # "c":Ljava/lang/String;
/* add-int/lit8 v2, v2, 0x1 */
/* .line 111 */
} // :cond_0
com.miui.server.migard.trace.GameTrace .getInstance ( );
(( com.miui.server.migard.trace.GameTrace ) v1 ).start ( v0, p2 ); // invoke-virtual {v1, v0, p2}, Lcom/miui/server/migard/trace/GameTrace;->start(Ljava/util/ArrayList;Z)V
/* .line 113 */
} // .end local v0 # "categoryList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
} // :cond_1
return;
} // .end method
public void stopTrace ( Boolean p0 ) {
/* .locals 1 */
/* .param p1, "zip" # Z */
/* .line 117 */
v0 = android.os.Binder .getCallingUid ( );
v0 = /* invoke-direct {p0, v0}, Lcom/miui/server/migard/MiGardService;->checkPermission(I)Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 118 */
com.miui.server.migard.trace.GameTrace .getInstance ( );
(( com.miui.server.migard.trace.GameTrace ) v0 ).stop ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/migard/trace/GameTrace;->stop(Z)V
/* .line 119 */
} // :cond_0
return;
} // .end method
