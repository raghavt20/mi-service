.class public Lcom/miui/server/MiuiWebViewManagerService;
.super Lmiui/webview/IMiuiWebViewManager$Stub;
.source "MiuiWebViewManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/MiuiWebViewManagerService$Lifecycle;
    }
.end annotation


# static fields
.field private static final EXEMPT_APPS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SEND_TIME_KEY:Ljava/lang/String; = "sendTime"

.field private static final TAG:Ljava/lang/String; = "MiuiWebViewManagerService"


# instance fields
.field private MSG_RESTART_WEBVIEW:I

.field private final mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method static bridge synthetic -$$Nest$fgetMSG_RESTART_WEBVIEW(Lcom/miui/server/MiuiWebViewManagerService;)I
    .locals 0

    iget p0, p0, Lcom/miui/server/MiuiWebViewManagerService;->MSG_RESTART_WEBVIEW:I

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmContext(Lcom/miui/server/MiuiWebViewManagerService;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/MiuiWebViewManagerService;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mcollectWebViewProcesses(Lcom/miui/server/MiuiWebViewManagerService;Lcom/android/server/am/ActivityManagerService;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/MiuiWebViewManagerService;->collectWebViewProcesses(Lcom/android/server/am/ActivityManagerService;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$sfgetEXEMPT_APPS()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/miui/server/MiuiWebViewManagerService;->EXEMPT_APPS:Ljava/util/List;

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/miui/server/MiuiWebViewManagerService$1;

    invoke-direct {v0}, Lcom/miui/server/MiuiWebViewManagerService$1;-><init>()V

    sput-object v0, Lcom/miui/server/MiuiWebViewManagerService;->EXEMPT_APPS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .line 40
    invoke-direct {p0}, Lmiui/webview/IMiuiWebViewManager$Stub;-><init>()V

    .line 37
    const/16 v0, 0x7f

    iput v0, p0, Lcom/miui/server/MiuiWebViewManagerService;->MSG_RESTART_WEBVIEW:I

    .line 41
    iput-object p1, p0, Lcom/miui/server/MiuiWebViewManagerService;->mContext:Landroid/content/Context;

    .line 42
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MiuiWebViewWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/MiuiWebViewManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 43
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 44
    new-instance v0, Lcom/miui/server/MiuiWebViewManagerService$2;

    iget-object v1, p0, Lcom/miui/server/MiuiWebViewManagerService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/MiuiWebViewManagerService$2;-><init>(Lcom/miui/server/MiuiWebViewManagerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/MiuiWebViewManagerService;->mHandler:Landroid/os/Handler;

    .line 76
    return-void
.end method

.method private collectWebViewProcesses(Lcom/android/server/am/ActivityManagerService;)Ljava/util/List;
    .locals 14
    .param p1, "am"    # Lcom/android/server/am/ActivityManagerService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/server/am/ActivityManagerService;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 79
    const-string v0, "com.google.android.webview"

    .line 80
    .local v0, "webViewComponentName":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v1, "pkgsToKill":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    :try_start_0
    sget-object v3, Lcom/miui/app/am/ActivityManagerServiceProxy;->collectProcesses:Lcom/xiaomi/reflect/RefMethod;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    .line 83
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x1

    aput-object v6, v4, v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v8, 0x2

    aput-object v6, v4, v8

    new-array v6, v5, [Ljava/lang/String;

    const/4 v8, 0x3

    aput-object v6, v4, v8

    invoke-virtual {v3, p1, v4}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 84
    .local v3, "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    if-nez v3, :cond_0

    .line 85
    return-object v2

    .line 87
    :cond_0
    sget-object v4, Lcom/miui/app/am/ActivityManagerServiceProxy;->mProcLock:Lcom/xiaomi/reflect/RefObject;

    invoke-virtual {v4, p1}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    sub-int/2addr v6, v7

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_4

    .line 89
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/ProcessRecord;

    .line 90
    .local v7, "pr":Lcom/android/server/am/ProcessRecord;
    sget-object v8, Lcom/miui/app/am/ProcessRecordProxy;->getPkgDeps:Lcom/xiaomi/reflect/RefMethod;

    new-array v9, v5, [Ljava/lang/Object;

    invoke-virtual {v8, v7, v9}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/util/ArraySet;

    .line 91
    .local v8, "deps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    sget-object v9, Lcom/miui/app/am/ProcessRecordProxy;->info:Lcom/xiaomi/reflect/RefObject;

    invoke-virtual {v9, v7}, Lcom/xiaomi/reflect/RefObject;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ApplicationInfo;

    .line 92
    .local v9, "info":Landroid/content/pm/ApplicationInfo;
    if-eqz v8, :cond_3

    if-eqz v9, :cond_3

    iget-object v10, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-nez v10, :cond_1

    .line 93
    goto :goto_1

    .line 95
    :cond_1
    invoke-virtual {v8, v0}, Landroid/util/ArraySet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    iget-object v10, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v11, "com.android.browser"

    .line 96
    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 97
    :cond_2
    iget-object v10, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 98
    .local v10, "pkgName":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "#"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/miui/app/am/ProcessRecordProxy;->getPid:Lcom/xiaomi/reflect/RefMethod;

    new-array v13, v5, [Ljava/lang/Object;

    invoke-virtual {v12, v7, v13}, Lcom/xiaomi/reflect/RefMethod;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    .end local v7    # "pr":Lcom/android/server/am/ProcessRecord;
    .end local v8    # "deps":Landroid/util/ArraySet;, "Landroid/util/ArraySet<Ljava/lang/String;>;"
    .end local v9    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v10    # "pkgName":Ljava/lang/String;
    :cond_3
    :goto_1
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 101
    .end local v6    # "i":I
    :cond_4
    monitor-exit v4

    .line 105
    .end local v3    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    nop

    .line 106
    return-object v1

    .line 101
    .restart local v3    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "webViewComponentName":Ljava/lang/String;
    .end local v1    # "pkgsToKill":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local p0    # "this":Lcom/miui/server/MiuiWebViewManagerService;
    .end local p1    # "am":Lcom/android/server/am/ActivityManagerService;
    :try_start_2
    throw v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 102
    .end local v3    # "procs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/server/am/ProcessRecord;>;"
    .restart local v0    # "webViewComponentName":Ljava/lang/String;
    .restart local v1    # "pkgsToKill":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local p0    # "this":Lcom/miui/server/MiuiWebViewManagerService;
    .restart local p1    # "am":Lcom/android/server/am/ActivityManagerService;
    :catch_0
    move-exception v3

    .line 103
    .local v3, "e":Ljava/lang/Exception;
    const-string v4, "MiuiWebViewManagerService"

    const-string v5, "MiuiWebViewManagerService.collectWebViewProcesses failed"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 104
    return-object v2
.end method


# virtual methods
.method public restartWebViewProcesses()V
    .locals 5

    .line 111
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 112
    .local v0, "msg":Landroid/os/Message;
    iget v1, p0, Lcom/miui/server/MiuiWebViewManagerService;->MSG_RESTART_WEBVIEW:I

    iput v1, v0, Landroid/os/Message;->what:I

    .line 113
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 114
    .local v1, "data":Landroid/os/Bundle;
    const-string/jumbo v2, "sendTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 115
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 116
    iget-object v2, p0, Lcom/miui/server/MiuiWebViewManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    const-string v2, "MiuiWebViewManagerService"

    const-string v3, "restartWebViewProcesses called"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return-void
.end method
