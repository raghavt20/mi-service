.class Lcom/miui/server/SecuritySmsHandler;
.super Ljava/lang/Object;
.source "SecuritySmsHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SecuritySmsHandler"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInterceptSmsCallerPkgName:Ljava/lang/String;

.field private mInterceptSmsCallerUid:I

.field private mInterceptSmsCount:I

.field private final mInterceptSmsLock:Ljava/lang/Object;

.field private mInterceptSmsSenderNum:Ljava/lang/String;

.field private mInterceptedSmsResultReceiver:Landroid/content/BroadcastReceiver;

.field private mNormalMsgResultReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static bridge synthetic -$$Nest$mcheckByAntiSpam(Lcom/miui/server/SecuritySmsHandler;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/server/SecuritySmsHandler;->checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mdispatchIntent(Lcom/miui/server/SecuritySmsHandler;Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdispatchNormalSms(Lcom/miui/server/SecuritySmsHandler;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchNormalSms(Landroid/content/Intent;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$mdispatchSmsToAntiSpam(Lcom/miui/server/SecuritySmsHandler;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchSmsToAntiSpam(Landroid/content/Intent;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    .line 35
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerPkgName:Ljava/lang/String;

    .line 36
    iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    .line 37
    iput-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsSenderNum:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsLock:Ljava/lang/Object;

    .line 276
    new-instance v0, Lcom/miui/server/SecuritySmsHandler$1;

    invoke-direct {v0, p0}, Lcom/miui/server/SecuritySmsHandler$1;-><init>(Lcom/miui/server/SecuritySmsHandler;)V

    iput-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mNormalMsgResultReceiver:Landroid/content/BroadcastReceiver;

    .line 299
    new-instance v0, Lcom/miui/server/SecuritySmsHandler$2;

    invoke-direct {v0, p0}, Lcom/miui/server/SecuritySmsHandler$2;-><init>(Lcom/miui/server/SecuritySmsHandler;)V

    iput-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptedSmsResultReceiver:Landroid/content/BroadcastReceiver;

    .line 41
    iput-object p2, p0, Lcom/miui/server/SecuritySmsHandler;->mHandler:Landroid/os/Handler;

    .line 42
    iput-object p1, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 5
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "slotId"    # I

    .line 177
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 178
    .local v0, "token":J
    iget-object v2, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, p2, p3}, Lmiui/provider/ExtraTelephony;->getSmsBlockType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 179
    .local v2, "blockType":I
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 180
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkByAntiSpam : blockType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "SecuritySmsHandler"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    return v2
.end method

.method private checkWithInterceptedSender(Ljava/lang/String;)Z
    .locals 8
    .param p1, "sender"    # Ljava/lang/String;

    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "result":Z
    iget-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 153
    :try_start_0
    const-string v2, "SecuritySmsHandler"

    const-string v3, "checkWithInterceptedSender: callerUid:%d, senderNum:%s, count:%d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    .line 154
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsSenderNum:Ljava/lang/String;

    const/4 v6, 0x1

    aput-object v5, v4, v6

    iget v5, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v7, 0x2

    aput-object v5, v4, v7

    .line 153
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsSenderNum:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 156
    iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    if-lez v2, :cond_0

    .line 157
    sub-int/2addr v2, v6

    iput v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    .line 158
    const/4 v0, 0x1

    .line 160
    :cond_0
    iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    if-nez v2, :cond_1

    .line 161
    invoke-direct {p0}, Lcom/miui/server/SecuritySmsHandler;->releaseSmsIntercept()V

    .line 164
    :cond_1
    monitor-exit v1

    .line 165
    return v0

    .line 164
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "permission"    # Ljava/lang/String;
    .param p3, "appOp"    # I
    .param p4, "resultReceiver"    # Landroid/content/BroadcastReceiver;

    .line 272
    iget-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/miui/server/SecuritySmsHandler;->mHandler:Landroid/os/Handler;

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 274
    return-void
.end method

.method private dispatchMmsToAntiSpam(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 222
    const-string v0, "SecuritySmsHandler"

    const-string v1, "dispatchMmsToAntiSpam"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 224
    const-string v1, "com.android.mms"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    const-string v1, "android.provider.Telephony.WAP_PUSH_DELIVER"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const-string v1, "android.permission.RECEIVE_SMS"

    const/16 v2, 0x10

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V

    .line 227
    return-void
.end method

.method private dispatchNormalMms(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .line 255
    const-string v0, "dispatchNormalMms"

    const-string v1, "SecuritySmsHandler"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    iget-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/internal/telephony/SmsApplication;->getDefaultMmsApplication(Landroid/content/Context;Z)Landroid/content/ComponentName;

    move-result-object v0

    .line 260
    .local v0, "componentName":Landroid/content/ComponentName;
    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 263
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Delivering MMS to: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    const/high16 v1, 0x8000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 266
    const-string v1, "android.provider.Telephony.WAP_PUSH_DELIVER"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/miui/server/SecuritySmsHandler;->mNormalMsgResultReceiver:Landroid/content/BroadcastReceiver;

    const-string v3, "android.permission.RECEIVE_SMS"

    invoke-direct {p0, p1, v3, v1, v2}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V

    .line 269
    return-void
.end method

.method private dispatchNormalSms(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .line 234
    const-string v0, "dispatchNormalSms"

    const-string v1, "SecuritySmsHandler"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    iget-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/android/internal/telephony/SmsApplication;->getDefaultSmsApplication(Landroid/content/Context;Z)Landroid/content/ComponentName;

    move-result-object v0

    .line 239
    .local v0, "componentName":Landroid/content/ComponentName;
    if-eqz v0, :cond_0

    .line 241
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 242
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Delivering SMS to: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_0
    const/high16 v1, 0x8000000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 245
    const-string v1, "android.provider.Telephony.SMS_DELIVER"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/miui/server/SecuritySmsHandler;->mNormalMsgResultReceiver:Landroid/content/BroadcastReceiver;

    const-string v3, "android.permission.RECEIVE_SMS"

    invoke-direct {p0, p1, v3, v1, v2}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V

    .line 248
    return-void
.end method

.method private dispatchSmsToAntiSpam(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 210
    const-string v0, "SecuritySmsHandler"

    const-string v1, "dispatchSmsToAntiSpam"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 212
    const-string v1, "com.android.mms"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string v1, "android.provider.Telephony.SMS_DELIVER"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const-string v1, "android.permission.RECEIVE_SMS"

    const/16 v2, 0x10

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V

    .line 215
    return-void
.end method

.method private dispatchToInterceptApp(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .line 196
    const-string v0, "SecuritySmsHandler"

    const-string v1, "dispatchToInterceptApp"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 198
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 199
    iget-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerPkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string v0, "android.provider.Telephony.SMS_RECEIVED"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptedSmsResultReceiver:Landroid/content/BroadcastReceiver;

    const-string v2, "android.permission.RECEIVE_SMS"

    invoke-direct {p0, p1, v2, v0, v1}, Lcom/miui/server/SecuritySmsHandler;->dispatchIntent(Landroid/content/Intent;Ljava/lang/String;ILandroid/content/BroadcastReceiver;)V

    .line 203
    return-void
.end method

.method public static getSlotIdFromIntent(Landroid/content/Intent;)I
    .locals 3
    .param p0, "intent"    # Landroid/content/Intent;

    .line 341
    const/4 v0, 0x0

    .line 342
    .local v0, "slotId":I
    invoke-static {}, Lmiui/telephony/TelephonyManager;->getDefault()Lmiui/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1}, Lmiui/telephony/TelephonyManager;->getPhoneCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 343
    sget-object v1, Lmiui/telephony/SubscriptionManager;->SLOT_KEY:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 344
    if-gez v0, :cond_0

    .line 345
    const-string v1, "SecuritySmsHandler"

    const-string v2, "getSlotIdFromIntent slotId < 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    :cond_0
    return v0
.end method

.method private releaseSmsIntercept()V
    .locals 2

    .line 185
    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    .line 186
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerPkgName:Ljava/lang/String;

    .line 187
    iput-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsSenderNum:Ljava/lang/String;

    .line 188
    iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    .line 189
    return-void
.end method


# virtual methods
.method checkSmsBlocked(Landroid/content/Intent;)Z
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;

    .line 46
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-class v0, [B

    const-string v3, "enter checkSmsBlocked"

    const-string v4, "SecuritySmsHandler"

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/4 v3, 0x0

    .line 48
    .local v3, "blocked":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 49
    .local v5, "action":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/miui/server/SecuritySmsHandler;->getSlotIdFromIntent(Landroid/content/Intent;)I

    move-result v6

    .line 51
    .local v6, "slotId":I
    const-string v7, "android.provider.Telephony.SMS_DELIVER"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    const/4 v8, 0x3

    const-string v9, "blockType"

    const/4 v10, 0x0

    if-eqz v7, :cond_4

    .line 52
    invoke-static/range {p1 .. p1}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    move-result-object v0

    .line 53
    .local v0, "msg":[Landroid/telephony/SmsMessage;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v7, "sb":Ljava/lang/StringBuilder;
    if-nez v0, :cond_0

    .line 55
    return v10

    .line 57
    :cond_0
    array-length v11, v0

    move v12, v10

    :goto_0
    if-ge v12, v11, :cond_1

    aget-object v13, v0, v12

    .line 58
    .local v13, "smsMessage":Landroid/telephony/SmsMessage;
    invoke-virtual {v13}, Landroid/telephony/SmsMessage;->getDisplayMessageBody()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .end local v13    # "smsMessage":Landroid/telephony/SmsMessage;
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 61
    :cond_1
    aget-object v10, v0, v10

    invoke-virtual {v10}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v10

    .line 62
    .local v10, "address":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 64
    .local v11, "body":Ljava/lang/String;
    invoke-direct {v1, v10}, Lcom/miui/server/SecuritySmsHandler;->checkWithInterceptedSender(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 65
    const-string v12, "Intercepted by sender address"

    invoke-static {v4, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchToInterceptApp(Landroid/content/Intent;)V

    .line 67
    const/4 v3, 0x1

    .line 70
    :cond_2
    if-nez v3, :cond_3

    .line 71
    invoke-direct {v1, v10, v11, v6}, Lcom/miui/server/SecuritySmsHandler;->checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v12

    .line 72
    .local v12, "blockType":I
    if-eqz v12, :cond_3

    .line 73
    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 74
    invoke-static {v12}, Lmiui/provider/ExtraTelephony;->getRealBlockType(I)I

    move-result v9

    if-lt v9, v8, :cond_3

    .line 75
    const-string v8, "This sms is intercepted by AntiSpam"

    invoke-static {v4, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchSmsToAntiSpam(Landroid/content/Intent;)V

    .line 77
    const/4 v3, 0x1

    .line 81
    .end local v0    # "msg":[Landroid/telephony/SmsMessage;
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v10    # "address":Ljava/lang/String;
    .end local v11    # "body":Ljava/lang/String;
    .end local v12    # "blockType":I
    :cond_3
    move-object/from16 v19, v5

    goto/16 :goto_2

    :cond_4
    const-string v7, "android.provider.Telephony.WAP_PUSH_DELIVER"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 83
    const-string v7, "data"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    .line 85
    .local v7, "pushData":[B
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x11101aa

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v11

    .line 89
    .local v11, "contentDisposition":Z
    :try_start_0
    const-string v12, "com.google.android.mms.pdu.PduParser"

    invoke-static {v12}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v12

    .line 90
    .local v12, "pduParserClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v13, "com.google.android.mms.pdu.GenericPdu"

    invoke-static {v13}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    .line 91
    .local v13, "pduClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v14, "com.google.android.mms.pdu.EncodedStringValue"

    invoke-static {v14}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    .line 92
    .local v14, "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v15, "com.google.android.mms.pdu.PduPersister"

    invoke-static {v15}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v15

    .line 93
    .local v15, "persisterClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v8, 0x2

    new-array v10, v8, [Ljava/lang/Class;

    const/16 v16, 0x0

    aput-object v0, v10, v16

    sget-object v17, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v8, 0x1

    aput-object v17, v10, v8

    invoke-virtual {v12, v10}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v10

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v7, v8, v16

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    const/16 v17, 0x1

    aput-object v18, v8, v17

    invoke-virtual {v10, v8}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 94
    .local v8, "parser":Ljava/lang/Object;
    const-string v10, "parse"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v18, v3

    move-object/from16 v19, v5

    const/4 v3, 0x0

    .end local v3    # "blocked":Z
    .end local v5    # "action":Ljava/lang/String;
    .local v18, "blocked":Z
    .local v19, "action":Ljava/lang/String;
    :try_start_1
    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v12, v10, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v5, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 95
    .local v5, "pdu":Ljava/lang/Object;
    const-string v10, "getFrom"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v20, v7

    .end local v7    # "pushData":[B
    .local v20, "pushData":[B
    :try_start_2
    new-array v7, v3, [Ljava/lang/Class;

    invoke-virtual {v13, v10, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v7, v5, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 96
    .local v7, "encodedStringValue":Ljava/lang/Object;
    const-string v10, "getTextString"

    move-object/from16 v21, v5

    .end local v5    # "pdu":Ljava/lang/Object;
    .local v21, "pdu":Ljava/lang/Object;
    new-array v5, v3, [Ljava/lang/Class;

    invoke-virtual {v14, v10, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v5, v7, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    .line 97
    .local v3, "addressByte":[B
    const-string/jumbo v5, "toIsoString"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/16 v16, 0x0

    aput-object v0, v10, v16

    invoke-virtual {v15, v5, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v5

    const/4 v10, 0x0

    invoke-virtual {v0, v10, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 101
    .end local v3    # "addressByte":[B
    .end local v7    # "encodedStringValue":Ljava/lang/Object;
    .end local v8    # "parser":Ljava/lang/Object;
    .end local v12    # "pduParserClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v13    # "pduClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v14    # "valueClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v15    # "persisterClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v21    # "pdu":Ljava/lang/Object;
    .local v0, "address":Ljava/lang/String;
    nop

    .line 102
    invoke-direct {v1, v0, v10, v6}, Lcom/miui/server/SecuritySmsHandler;->checkByAntiSpam(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 103
    .local v3, "blockType":I
    if-eqz v3, :cond_6

    .line 104
    invoke-virtual {v2, v9, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 105
    invoke-static {v3}, Lmiui/provider/ExtraTelephony;->getRealBlockType(I)I

    move-result v5

    const/4 v7, 0x3

    if-lt v5, v7, :cond_6

    .line 106
    const-string v5, "This mms is intercepted by AntiSpam"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-direct/range {p0 .. p1}, Lcom/miui/server/SecuritySmsHandler;->dispatchMmsToAntiSpam(Landroid/content/Intent;)V

    .line 108
    const/4 v5, 0x1

    move v3, v5

    .end local v18    # "blocked":Z
    .local v5, "blocked":Z
    goto :goto_2

    .line 98
    .end local v0    # "address":Ljava/lang/String;
    .end local v3    # "blockType":I
    .end local v5    # "blocked":Z
    .restart local v18    # "blocked":Z
    :catch_0
    move-exception v0

    goto :goto_1

    .end local v20    # "pushData":[B
    .local v7, "pushData":[B
    :catch_1
    move-exception v0

    move-object/from16 v20, v7

    .end local v7    # "pushData":[B
    .restart local v20    # "pushData":[B
    goto :goto_1

    .end local v18    # "blocked":Z
    .end local v19    # "action":Ljava/lang/String;
    .end local v20    # "pushData":[B
    .local v3, "blocked":Z
    .local v5, "action":Ljava/lang/String;
    .restart local v7    # "pushData":[B
    :catch_2
    move-exception v0

    move/from16 v18, v3

    move-object/from16 v19, v5

    move-object/from16 v20, v7

    .line 99
    .end local v3    # "blocked":Z
    .end local v5    # "action":Ljava/lang/String;
    .end local v7    # "pushData":[B
    .local v0, "e":Ljava/lang/Exception;
    .restart local v18    # "blocked":Z
    .restart local v19    # "action":Ljava/lang/String;
    .restart local v20    # "pushData":[B
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 100
    const/4 v3, 0x0

    return v3

    .line 81
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v11    # "contentDisposition":Z
    .end local v18    # "blocked":Z
    .end local v19    # "action":Ljava/lang/String;
    .end local v20    # "pushData":[B
    .restart local v3    # "blocked":Z
    .restart local v5    # "action":Ljava/lang/String;
    :cond_5
    move/from16 v18, v3

    move-object/from16 v19, v5

    .line 112
    .end local v3    # "blocked":Z
    .end local v5    # "action":Ljava/lang/String;
    .restart local v18    # "blocked":Z
    .restart local v19    # "action":Ljava/lang/String;
    :cond_6
    move/from16 v3, v18

    .end local v18    # "blocked":Z
    .restart local v3    # "blocked":Z
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "leave checkSmsBlocked, blocked:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return v3
.end method

.method startInterceptSmsBySender(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "sender"    # Ljava/lang/String;
    .param p3, "count"    # I

    .line 117
    iget-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    const-string v1, "com.miui.permission.MANAGE_SMS_INTERCEPT"

    const-string v2, "SecuritySmsHandler"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 119
    .local v0, "callerUid":I
    iget-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :try_start_0
    iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    if-nez v2, :cond_0

    .line 121
    iput v0, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    .line 122
    iput-object p1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerPkgName:Ljava/lang/String;

    .line 123
    iput-object p2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsSenderNum:Ljava/lang/String;

    .line 124
    iput p3, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCount:I

    .line 125
    monitor-exit v1

    const/4 v1, 0x1

    return v1

    .line 127
    :cond_0
    monitor-exit v1

    .line 128
    const/4 v1, 0x0

    return v1

    .line 127
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method stopInterceptSmsBySender()Z
    .locals 4

    .line 132
    nop

    .line 136
    iget-object v0, p0, Lcom/miui/server/SecuritySmsHandler;->mContext:Landroid/content/Context;

    const-string v1, "com.miui.permission.MANAGE_SMS_INTERCEPT"

    const-string v2, "SecuritySmsHandler"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 138
    .local v0, "callerUid":I
    iget-object v1, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    iget v2, p0, Lcom/miui/server/SecuritySmsHandler;->mInterceptSmsCallerUid:I

    const/4 v3, 0x1

    if-nez v2, :cond_0

    .line 140
    monitor-exit v1

    return v3

    .line 142
    :cond_0
    if-ne v2, v0, :cond_1

    .line 143
    invoke-direct {p0}, Lcom/miui/server/SecuritySmsHandler;->releaseSmsIntercept()V

    .line 144
    monitor-exit v1

    return v3

    .line 146
    :cond_1
    monitor-exit v1

    .line 147
    const/4 v1, 0x0

    return v1

    .line 146
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
