public class com.miui.server.sentinel.FdUsageInfo extends com.miui.server.sentinel.ProcUsageInfo {
	 /* .source "FdUsageInfo.java" */
	 /* # instance fields */
	 private Long fd_amount;
	 private java.lang.String usageInfo;
	 /* # direct methods */
	 public com.miui.server.sentinel.FdUsageInfo ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Lcom/miui/server/sentinel/ProcUsageInfo;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Long getFd_amount ( ) {
		 /* .locals 2 */
		 /* .line 8 */
		 /* iget-wide v0, p0, Lcom/miui/server/sentinel/FdUsageInfo;->fd_amount:J */
		 /* return-wide v0 */
	 } // .end method
	 public java.lang.String getUsageInfo ( ) {
		 /* .locals 1 */
		 /* .line 16 */
		 v0 = this.usageInfo;
	 } // .end method
	 public void setFd_amount ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "fd_amount" # J */
		 /* .line 12 */
		 /* iput-wide p1, p0, Lcom/miui/server/sentinel/FdUsageInfo;->fd_amount:J */
		 /* .line 13 */
		 return;
	 } // .end method
	 public void setUsageInfo ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "usageInfo" # Ljava/lang/String; */
		 /* .line 20 */
		 this.usageInfo = p1;
		 /* .line 21 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 4 */
		 /* .line 24 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 25 */
		 /* .local v0, "sb":Ljava/lang/StringBuilder; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Proc info Name: "; // const-string v2, "Proc info Name: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( com.miui.server.sentinel.FdUsageInfo ) p0 ).getName ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/FdUsageInfo;->getName()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = "Pid = "; // const-string v2, "Pid = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = 		 (( com.miui.server.sentinel.FdUsageInfo ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/FdUsageInfo;->getPid()I
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = "Thread Amount size = "; // const-string v2, "Thread Amount size = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/sentinel/FdUsageInfo;->fd_amount:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 27 */
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
