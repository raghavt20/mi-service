.class Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;
.super Ljava/lang/Thread;
.source "MiuiSentinelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/sentinel/MiuiSentinelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MiuiSentinelServiceThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/sentinel/MiuiSentinelService;


# direct methods
.method private constructor <init>(Lcom/miui/server/sentinel/MiuiSentinelService;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;->this$0:Lcom/miui/server/sentinel/MiuiSentinelService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/server/sentinel/MiuiSentinelService;Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread-IA;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;-><init>(Lcom/miui/server/sentinel/MiuiSentinelService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 80
    const-string v0, "MiuiSentinelService"

    :try_start_0
    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;->this$0:Lcom/miui/server/sentinel/MiuiSentinelService;

    invoke-static {v1}, Lcom/miui/server/sentinel/MiuiSentinelService;->-$$Nest$mcreateSystemServerSocketForProchunter(Lcom/miui/server/sentinel/MiuiSentinelService;)Landroid/net/LocalSocket;

    move-result-object v1

    .line 81
    .local v1, "localSocket":Landroid/net/LocalSocket;
    if-eqz v1, :cond_0

    .line 82
    .end local v1    # "localSocket":Landroid/net/LocalSocket;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "local socekt fd is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :goto_0
    iget-object v2, p0, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;->this$0:Lcom/miui/server/sentinel/MiuiSentinelService;

    invoke-virtual {v1}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/miui/server/sentinel/MiuiSentinelService;->-$$Nest$mrecvMessage(Lcom/miui/server/sentinel/MiuiSentinelService;Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 90
    :cond_0
    goto :goto_1

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "prochunter_native_sockets connection catch Exception"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 91
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method
