.class public Lcom/miui/server/sentinel/NativeHeapUsageInfo;
.super Lcom/miui/server/sentinel/ProcUsageInfo;
.source "NativeHeapUsageInfo.java"


# instance fields
.field private nativeHeapSize:J

.field private stackTrace:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/miui/server/sentinel/ProcUsageInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getNativeHeapSize()J
    .locals 2

    .line 8
    iget-wide v0, p0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->nativeHeapSize:J

    return-wide v0
.end method

.method public getStackTrace()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->stackTrace:Ljava/lang/String;

    return-object v0
.end method

.method public setNativeHeapSize(J)V
    .locals 0
    .param p1, "nativeHeapSize"    # J

    .line 12
    iput-wide p1, p0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->nativeHeapSize:J

    .line 13
    return-void
.end method

.method public setStackTrace(Ljava/lang/String;)V
    .locals 0
    .param p1, "stackTrace"    # Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->stackTrace:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Proc info Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Pid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "NativeHeap Size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->nativeHeapSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Track stackTrace"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->stackTrace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
