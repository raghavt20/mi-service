.class public Lcom/miui/server/sentinel/ProcUsageInfo;
.super Ljava/lang/Object;
.source "ProcUsageInfo.java"


# instance fields
.field private adj:I

.field private name:Ljava/lang/String;

.field private pid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdj()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/miui/server/sentinel/ProcUsageInfo;->adj:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/miui/server/sentinel/ProcUsageInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPid()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/miui/server/sentinel/ProcUsageInfo;->pid:I

    return v0
.end method

.method public setAdj(I)V
    .locals 0
    .param p1, "adj"    # I

    .line 29
    iput p1, p0, Lcom/miui/server/sentinel/ProcUsageInfo;->adj:I

    .line 30
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 13
    iput-object p1, p0, Lcom/miui/server/sentinel/ProcUsageInfo;->name:Ljava/lang/String;

    .line 14
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 21
    iput p1, p0, Lcom/miui/server/sentinel/ProcUsageInfo;->pid:I

    .line 22
    return-void
.end method
