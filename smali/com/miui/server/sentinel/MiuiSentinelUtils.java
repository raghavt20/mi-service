public class com.miui.server.sentinel.MiuiSentinelUtils {
	 /* .source "MiuiSentinelUtils.java" */
	 /* # static fields */
	 private static final Integer MAX_FD_AMOUNT;
	 private static final Integer MAX_THREAD_AMOUNT;
	 private static final java.lang.String MIUI_SENTINEL_DIR;
	 private static Boolean MTBF_MIUI_TEST;
	 private static final java.lang.String PROPERTIES_BUILD_FINGERPRINT;
	 private static final java.lang.String PROPERTIES_MTBF_COREDUMP;
	 private static final java.lang.String PROPERTIES_MTBF_MIUI_TEST;
	 private static final java.lang.String PROPERTIES_MTBF_TEST;
	 private static final java.lang.String PROPERTIES_OMNI_TEST;
	 private static Boolean REBOOT_COREDUMP;
	 private static final java.lang.String SYSPROP_ENABLE_TRACK_MALLOC;
	 private static final Integer SYSTEM_SERVER_MAX_JAVAHEAP;
	 private static final Integer SYSTEM_SERVER_MAX_NATIVEHEAP;
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 static com.miui.server.sentinel.MiuiSentinelUtils ( ) {
		 /* .locals 3 */
		 /* .line 36 */
		 final String v0 = "persist.reboot.coredump"; // const-string v0, "persist.reboot.coredump"
		 int v1 = 0; // const/4 v1, 0x0
		 v0 = 		 android.os.SystemProperties .getBoolean ( v0,v1 );
		 com.miui.server.sentinel.MiuiSentinelUtils.REBOOT_COREDUMP = (v0!= 0);
		 /* .line 37 */
		 final String v0 = "ro.miui.mtbftest"; // const-string v0, "ro.miui.mtbftest"
		 v0 = 		 android.os.SystemProperties .getInt ( v0,v1 );
		 int v2 = 1; // const/4 v2, 0x1
		 /* if-ne v0, v2, :cond_0 */
		 /* move v1, v2 */
	 } // :cond_0
	 com.miui.server.sentinel.MiuiSentinelUtils.MTBF_MIUI_TEST = (v1!= 0);
	 return;
} // .end method
public com.miui.server.sentinel.MiuiSentinelUtils ( ) {
	 /* .locals 0 */
	 /* .line 24 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
public static void dumpFdInfo ( Integer p0, java.lang.String p1, java.io.File p2 ) {
	 /* .locals 8 */
	 /* .param p0, "pid" # I */
	 /* .param p1, "cmdline" # Ljava/lang/String; */
	 /* .param p2, "file" # Ljava/io/File; */
	 /* .line 164 */
	 /* new-instance v0, Ljava/io/File; */
	 /* new-instance v1, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v2 = "/proc/"; // const-string v2, "/proc/"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 java.lang.String .valueOf ( p0 );
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v2 = "/fd"; // const-string v2, "/fd"
	 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 /* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
	 /* .line 165 */
	 /* .local v0, "fdPath":Ljava/io/File; */
	 (( java.io.File ) v0 ).listFiles ( ); // invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;
	 /* .line 166 */
	 /* .local v1, "fdList":[Ljava/io/File; */
	 v2 = 	 com.android.internal.util.ArrayUtils .isEmpty ( v1 );
	 if ( v2 != null) { // if-eqz v2, :cond_0
		 /* .line 167 */
		 /* new-instance v2, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v3 = "failed to read fdList path=/proc/"; // const-string v3, "failed to read fdList path=/proc/"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 java.lang.String .valueOf ( p0 );
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v3 = "/fdinfo"; // const-string v3, "/fdinfo"
		 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 final String v3 = "MiuiSentinelUtils"; // const-string v3, "MiuiSentinelUtils"
		 android.util.Slog .w ( v3,v2 );
		 /* .line 168 */
		 return;
		 /* .line 170 */
	 } // :cond_0
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 /* .line 171 */
	 /* .local v2, "sb":Ljava/lang/StringBuilder; */
	 final String v3 = "Sentinel Monitor Reason: FdLeak\n"; // const-string v3, "Sentinel Monitor Reason: FdLeak\n"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 172 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "Build fingerprint:"; // const-string v4, "Build fingerprint:"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v4 = "ro.build.fingerprint"; // const-string v4, "ro.build.fingerprint"
	 /* const-string/jumbo v5, "unknown" */
	 android.os.SystemProperties .get ( v4,v5 );
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 final String v4 = "\n"; // const-string v4, "\n"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 173 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "TimeStamp: "; // const-string v5, "TimeStamp: "
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 java.lang.System .currentTimeMillis ( );
	 /* move-result-wide v5 */
	 com.miui.server.sentinel.MiuiSentinelUtils .getFormatDateTime ( v5,v6 );
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 174 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "Cmdline\uff1a "; // const-string v5, "Cmdline\uff1a "
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 175 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v5 = "pid: "; // const-string v5, "pid: "
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 java.lang.String .valueOf ( p0 );
	 (( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 176 */
	 final String v3 = "Fd Info:\n"; // const-string v3, "Fd Info:\n"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 177 */
	 /* new-instance v3, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v4 = "---Dump Fd info pid: "; // const-string v4, "---Dump Fd info pid: "
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
	 final String v4 = " ---\n"; // const-string v4, " ---\n"
	 (( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 /* .line 178 */
	 /* array-length v3, v1 */
	 int v4 = 0; // const/4 v4, 0x0
} // :goto_0
/* if-ge v4, v3, :cond_1 */
/* aget-object v5, v1, v4 */
/* .line 179 */
/* .local v5, "fd":Ljava/io/File; */
(( java.io.File ) v5 ).getAbsolutePath ( ); // invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
/* .line 181 */
/* .local v6, "path":Ljava/lang/String; */
com.miui.server.sentinel.MiuiSentinelUtils .getFdInfo ( p0,v5 );
/* .line 182 */
/* .local v7, "fdinfo":Ljava/lang/String; */
(( java.lang.StringBuilder ) v2 ).append ( v7 ); // invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 178 */
} // .end local v5 # "fd":Ljava/io/File;
} // .end local v6 # "path":Ljava/lang/String;
} // .end local v7 # "fdinfo":Ljava/lang/String;
/* add-int/lit8 v4, v4, 0x1 */
/* .line 184 */
} // :cond_1
final String v3 = "---Dump Fd info END---\n"; // const-string v3, "---Dump Fd info END---\n"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 185 */
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v3,p2 );
/* .line 186 */
return;
} // .end method
public static java.lang.String dumpProcSmaps ( Integer p0 ) {
/* .locals 6 */
/* .param p0, "pid" # I */
/* .line 189 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/proc/"; // const-string v1, "/proc/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/smaps"; // const-string v1, "/smaps"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 190 */
/* .local v0, "path":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 191 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 193 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
} // :goto_0
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v4, v3 */
/* .local v4, "line":Ljava/lang/String; */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 194 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.StringBuilder ) v3 ).append ( v5 ); // invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 196 */
} // .end local v4 # "line":Ljava/lang/String;
} // :cond_0
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 199 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .line 191 */
/* .restart local v2 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "path":Ljava/lang/String;
} // .end local v1 # "sb":Ljava/lang/StringBuilder;
} // .end local p0 # "pid":I
} // :goto_1
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 196 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "path":Ljava/lang/String; */
/* .restart local v1 # "sb":Ljava/lang/StringBuilder; */
/* .restart local p0 # "pid":I */
/* :catch_0 */
/* move-exception v2 */
/* .line 197 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 198 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "read failed: "; // const-string v4, "read failed: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v2 ); // invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiSentinelUtils"; // const-string v4, "MiuiSentinelUtils"
android.util.Slog .d ( v4,v3 );
/* .line 200 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
public static void dumpRssInfo ( Integer p0, Long p1, java.lang.String p2, java.io.File p3 ) {
/* .locals 5 */
/* .param p0, "pid" # I */
/* .param p1, "rssszie" # J */
/* .param p3, "cmdline" # Ljava/lang/String; */
/* .param p4, "file" # Ljava/io/File; */
/* .line 203 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 204 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
final String v1 = "Sentinel Monitor Reason: RssLeak\n"; // const-string v1, "Sentinel Monitor Reason: RssLeak\n"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 205 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Build fingerprint:"; // const-string v2, "Build fingerprint:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "ro.build.fingerprint"; // const-string v2, "ro.build.fingerprint"
/* const-string/jumbo v3, "unknown" */
android.os.SystemProperties .get ( v2,v3 );
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "\n"; // const-string v2, "\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 206 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "TimeStamp: "; // const-string v3, "TimeStamp: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v3 */
com.miui.server.sentinel.MiuiSentinelUtils .getFormatDateTime ( v3,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 207 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Cmdline\uff1a "; // const-string v3, "Cmdline\uff1a "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p3 ); // invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 208 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "pid: "; // const-string v3, "pid: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 209 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "RSS: "; // const-string v3, "RSS: "
(( java.lang.StringBuilder ) v1 ).append ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p1, p2 ); // invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 210 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "---Dump smaps info pid"; // const-string v2, "---Dump smaps info pid"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( p0 ); // invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = " ---\n"; // const-string v2, " ---\n"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 211 */
com.miui.server.sentinel.MiuiSentinelUtils .dumpProcSmaps ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 212 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v1,p4 );
/* .line 213 */
return;
} // .end method
public static void dumpThreadInfo ( Integer p0, java.lang.String p1, java.io.File p2 ) {
/* .locals 13 */
/* .param p0, "pid" # I */
/* .param p1, "cmdline" # Ljava/lang/String; */
/* .param p2, "file" # Ljava/io/File; */
/* .line 108 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/proc/"; // const-string v1, "/proc/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p0 ); // invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = "/task"; // const-string v1, "/task"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 109 */
/* .local v0, "path":Ljava/lang/String; */
/* new-instance v2, Ljava/io/File; */
/* invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 110 */
/* .local v2, "taskDir":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
if ( v3 != null) { // if-eqz v3, :cond_4
v3 = (( java.io.File ) v2 ).isDirectory ( ); // invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z
/* if-nez v3, :cond_0 */
/* goto/16 :goto_4 */
/* .line 114 */
} // :cond_0
(( java.io.File ) v2 ).listFiles ( ); // invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;
/* .line 115 */
/* .local v1, "taskFiles":[Ljava/io/File; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 116 */
/* .local v3, "sb":Ljava/lang/StringBuilder; */
final String v4 = "Sentinel Monitor Reason: ThreadLeak\n"; // const-string v4, "Sentinel Monitor Reason: ThreadLeak\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 117 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Build fingerprint:"; // const-string v5, "Build fingerprint:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "ro.build.fingerprint"; // const-string v5, "ro.build.fingerprint"
/* const-string/jumbo v6, "unknown" */
android.os.SystemProperties .get ( v5,v6 );
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "\n"; // const-string v5, "\n"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 118 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "TimeStamp: "; // const-string v6, "TimeStamp: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
com.miui.server.sentinel.MiuiSentinelUtils .getFormatDateTime ( v6,v7 );
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 119 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "Cmdline\uff1a "; // const-string v6, "Cmdline\uff1a "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 120 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "pid: "; // const-string v6, "pid: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 121 */
final String v4 = "Thread Info:\n"; // const-string v4, "Thread Info:\n"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 122 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "---Dump thread info pid: "; // const-string v6, "---Dump thread info pid: "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( p0 ); // invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " ---\n"; // const-string v6, " ---\n"
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 123 */
if ( v1 != null) { // if-eqz v1, :cond_3
/* .line 124 */
/* array-length v4, v1 */
int v6 = 0; // const/4 v6, 0x0
} // :goto_0
/* if-ge v6, v4, :cond_3 */
/* aget-object v7, v1, v6 */
/* .line 125 */
/* .local v7, "taskFile":Ljava/io/File; */
v8 = (( java.io.File ) v7 ).isDirectory ( ); // invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z
if ( v8 != null) { // if-eqz v8, :cond_2
/* .line 127 */
(( java.io.File ) v7 ).getName ( ); // invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;
/* .line 128 */
/* .local v8, "tid":Ljava/lang/String; */
try { // :try_start_0
/* new-instance v9, Ljava/io/BufferedReader; */
/* new-instance v10, Ljava/io/FileReader; */
/* new-instance v11, Ljava/io/File; */
final String v12 = "comm"; // const-string v12, "comm"
/* invoke-direct {v11, v7, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* invoke-direct {v10, v11}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V */
/* invoke-direct {v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 129 */
/* .local v9, "reader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v9 ).readLine ( ); // invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* .line 130 */
/* .local v10, "taskName":Ljava/lang/String; */
if ( v10 != null) { // if-eqz v10, :cond_1
v11 = (( java.lang.String ) v10 ).isEmpty ( ); // invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z
/* if-nez v11, :cond_1 */
/* .line 131 */
final String v11 = ""; // const-string v11, ""
(( java.lang.String ) v10 ).replaceAll ( v5, v11 ); // invoke-virtual {v10, v5, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
/* move-object v10, v11 */
/* .line 133 */
} // :cond_1
final String v11 = "<UNNAMED THREAD>"; // const-string v11, "<UNNAMED THREAD>"
/* move-object v10, v11 */
/* .line 135 */
} // :goto_1
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v12, "sysTid = " */
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v12 = " "; // const-string v12, " "
(( java.lang.StringBuilder ) v11 ).append ( v12 ); // invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v10 ); // invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v5 ); // invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v11 ); // invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 136 */
try { // :try_start_2
(( java.io.BufferedReader ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 139 */
} // .end local v9 # "reader":Ljava/io/BufferedReader;
/* .line 128 */
} // .end local v10 # "taskName":Ljava/lang/String;
/* .restart local v9 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v10 */
try { // :try_start_3
(( java.io.BufferedReader ) v9 ).close ( ); // invoke-virtual {v9}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v11 */
try { // :try_start_4
(( java.lang.Throwable ) v10 ).addSuppressed ( v11 ); // invoke-virtual {v10, v11}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "path":Ljava/lang/String;
} // .end local v1 # "taskFiles":[Ljava/io/File;
} // .end local v2 # "taskDir":Ljava/io/File;
} // .end local v3 # "sb":Ljava/lang/StringBuilder;
} // .end local v7 # "taskFile":Ljava/io/File;
} // .end local v8 # "tid":Ljava/lang/String;
} // .end local p0 # "pid":I
} // .end local p1 # "cmdline":Ljava/lang/String;
} // .end local p2 # "file":Ljava/io/File;
} // :goto_2
/* throw v10 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 136 */
} // .end local v9 # "reader":Ljava/io/BufferedReader;
/* .restart local v0 # "path":Ljava/lang/String; */
/* .restart local v1 # "taskFiles":[Ljava/io/File; */
/* .restart local v2 # "taskDir":Ljava/io/File; */
/* .restart local v3 # "sb":Ljava/lang/StringBuilder; */
/* .restart local v7 # "taskFile":Ljava/io/File; */
/* .restart local v8 # "tid":Ljava/lang/String; */
/* .restart local p0 # "pid":I */
/* .restart local p1 # "cmdline":Ljava/lang/String; */
/* .restart local p2 # "file":Ljava/io/File; */
/* :catch_0 */
/* move-exception v9 */
/* .line 137 */
/* .local v9, "e":Ljava/io/IOException; */
(( java.io.IOException ) v9 ).printStackTrace ( ); // invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
/* .line 138 */
/* nop */
/* .line 124 */
} // .end local v7 # "taskFile":Ljava/io/File;
} // .end local v8 # "tid":Ljava/lang/String;
} // .end local v9 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_3
/* add-int/lit8 v6, v6, 0x1 */
/* .line 143 */
} // :cond_3
final String v4 = "---Dump thread info End---"; // const-string v4, "---Dump thread info End---"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 144 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
com.miui.server.stability.ScoutMemoryUtils .dumpInfoToFile ( v4,p2 );
/* .line 145 */
return;
/* .line 111 */
} // .end local v1 # "taskFiles":[Ljava/io/File;
} // .end local v3 # "sb":Ljava/lang/StringBuilder;
} // :cond_4
} // :goto_4
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "failed to open proc/"; // const-string v4, "failed to open proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiSentinelUtils"; // const-string v3, "MiuiSentinelUtils"
android.util.Slog .d ( v3,v1 );
/* .line 112 */
return;
} // .end method
public static java.io.File getExceptionPath ( Integer p0, java.lang.String p1, java.lang.String p2 ) {
/* .locals 11 */
/* .param p0, "pid" # I */
/* .param p1, "cmdline" # Ljava/lang/String; */
/* .param p2, "type" # Ljava/lang/String; */
/* .line 217 */
final String v0 = "_"; // const-string v0, "_"
final String v1 = "MiuiSentinelUtils"; // const-string v1, "MiuiSentinelUtils"
try { // :try_start_0
/* new-instance v2, Ljava/io/File; */
final String v3 = "/data/miuilog/stability/memleak/sentinel"; // const-string v3, "/data/miuilog/stability/memleak/sentinel"
/* invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 218 */
/* .local v2, "sentinelDir":Ljava/io/File; */
v3 = (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
/* const/16 v4, 0x1fc */
int v5 = -1; // const/4 v5, -0x1
/* if-nez v3, :cond_1 */
/* .line 219 */
v3 = (( java.io.File ) v2 ).mkdirs ( ); // invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
/* if-nez v3, :cond_0 */
/* .line 220 */
final String v3 = "cannot create sentinel Dir"; // const-string v3, "cannot create sentinel Dir"
/* new-instance v6, Ljava/lang/Throwable; */
/* invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V */
android.util.Slog .e ( v1,v3,v6 );
/* .line 222 */
} // :cond_0
android.os.FileUtils .setPermissions ( v2,v4,v5,v5 );
/* .line 225 */
} // :cond_1
java.lang.System .currentTimeMillis ( );
/* move-result-wide v6 */
com.miui.server.sentinel.MiuiSentinelUtils .getFormatDateTime ( v6,v7 );
/* .line 226 */
/* .local v3, "dirSuffix":Ljava/lang/String; */
/* move-object v6, p2 */
/* .line 227 */
/* .local v6, "dirname":Ljava/lang/String; */
/* new-instance v7, Ljava/io/File; */
/* invoke-direct {v7, v2, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 228 */
/* .local v7, "exceptionDir":Ljava/io/File; */
v8 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
/* if-nez v8, :cond_3 */
/* .line 229 */
v8 = (( java.io.File ) v7 ).mkdirs ( ); // invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z
/* if-nez v8, :cond_2 */
/* .line 230 */
final String v8 = "cannot create exceptionDir"; // const-string v8, "cannot create exceptionDir"
/* new-instance v9, Ljava/lang/Throwable; */
/* invoke-direct {v9}, Ljava/lang/Throwable;-><init>()V */
android.util.Slog .e ( v1,v8,v9 );
/* .line 232 */
} // :cond_2
android.os.FileUtils .setPermissions ( v7,v4,v5,v5 );
/* .line 234 */
} // :cond_3
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v8 ).append ( p2 ); // invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( p1 ); // invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 236 */
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v0 ); // invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = ".txt"; // const-string v8, ".txt"
(( java.lang.StringBuilder ) v0 ).append ( v8 ); // invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 238 */
/* .local v0, "filename":Ljava/lang/String; */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v7, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 239 */
/* .local v8, "leakfile":Ljava/io/File; */
v9 = (( java.io.File ) v8 ).exists ( ); // invoke-virtual {v8}, Ljava/io/File;->exists()Z
/* if-nez v9, :cond_5 */
/* .line 240 */
v9 = (( java.io.File ) v8 ).createNewFile ( ); // invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z
/* if-nez v9, :cond_4 */
/* .line 241 */
final String v9 = "cannot create leakfile"; // const-string v9, "cannot create leakfile"
/* new-instance v10, Ljava/lang/Throwable; */
/* invoke-direct {v10}, Ljava/lang/Throwable;-><init>()V */
android.util.Slog .e ( v1,v9,v10 );
/* .line 243 */
} // :cond_4
(( java.io.File ) v8 ).getAbsolutePath ( ); // invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.os.FileUtils .setPermissions ( v9,v4,v5,v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 246 */
} // :cond_5
/* .line 247 */
} // .end local v0 # "filename":Ljava/lang/String;
} // .end local v2 # "sentinelDir":Ljava/io/File;
} // .end local v3 # "dirSuffix":Ljava/lang/String;
} // .end local v6 # "dirname":Ljava/lang/String;
} // .end local v7 # "exceptionDir":Ljava/io/File;
} // .end local v8 # "leakfile":Ljava/io/File;
/* :catch_0 */
/* move-exception v0 */
/* .line 248 */
/* .local v0, "e":Ljava/io/IOException; */
final String v2 = "Unable get leakfile : "; // const-string v2, "Unable get leakfile : "
android.util.Slog .w ( v1,v2,v0 );
/* .line 249 */
int v1 = 0; // const/4 v1, 0x0
} // .end method
public static java.lang.String getFdInfo ( Integer p0, java.io.File p1 ) {
/* .locals 5 */
/* .param p0, "pid" # I */
/* .param p1, "fdPath" # Ljava/io/File; */
/* .line 148 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "/proc/"; // const-string v1, "/proc/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p0 );
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "/fd/"; // const-string v1, "/fd/"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.io.File ) p1 ).getName ( ); // invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 149 */
/* .local v0, "path":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 150 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
final String v2 = "fdname --> "; // const-string v2, "fdname --> "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 152 */
int v2 = 0; // const/4 v2, 0x0
try { // :try_start_0
/* new-array v2, v2, [Ljava/lang/String; */
java.nio.file.Paths .get ( v0,v2 );
/* .line 153 */
/* .local v2, "paths":Ljava/nio/file/Path; */
java.nio.file.Files .readSymbolicLink ( v2 );
/* .line 154 */
/* .local v3, "targetPaths":Ljava/nio/file/Path; */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 155 */
final String v4 = " <--"; // const-string v4, " <--"
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 156 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 157 */
} // .end local v2 # "paths":Ljava/nio/file/Path;
} // .end local v3 # "targetPaths":Ljava/nio/file/Path;
/* :catch_0 */
/* move-exception v2 */
/* .line 158 */
/* .local v2, "e":Ljava/lang/Exception; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "getFdInfo: "; // const-string v4, "getFdInfo: "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p1 ); // invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiSentinelUtils"; // const-string v4, "MiuiSentinelUtils"
android.util.Slog .w ( v4,v3,v2 );
/* .line 160 */
} // .end local v2 # "e":Ljava/lang/Exception;
final String v2 = " <--(unknow fd)"; // const-string v2, " <--(unknow fd)"
} // .end method
public static java.lang.String getFormatDateTime ( Long p0 ) {
/* .locals 3 */
/* .param p0, "timeMillis" # J */
/* .line 68 */
/* new-instance v0, Ljava/util/Date; */
/* invoke-direct {v0, p0, p1}, Ljava/util/Date;-><init>(J)V */
/* .line 69 */
/* .local v0, "date":Ljava/util/Date; */
/* new-instance v1, Ljava/text/SimpleDateFormat; */
/* const-string/jumbo v2, "yyyy-MM-dd-HH-mm-ss" */
/* invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V */
/* .line 70 */
/* .local v1, "dateFormat":Ljava/text/SimpleDateFormat; */
(( java.text.SimpleDateFormat ) v1 ).format ( v0 ); // invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
} // .end method
public static java.lang.String getProcessCmdline ( Integer p0 ) {
/* .locals 5 */
/* .param p0, "pid" # I */
/* .line 74 */
/* const-string/jumbo v0, "unknown" */
/* .line 75 */
/* .local v0, "cmdline":Ljava/lang/String; */
/* if-nez p0, :cond_0 */
/* .line 76 */
} // :cond_0
try { // :try_start_0
/* new-instance v1, Ljava/io/BufferedReader; */
/* new-instance v2, Ljava/io/FileReader; */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "proc/"; // const-string v4, "proc/"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( p0 ); // invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = "/cmdline"; // const-string v4, "/cmdline"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v2, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 77 */
/* .local v1, "cmdlineReader":Ljava/io/BufferedReader; */
try { // :try_start_1
(( java.io.BufferedReader ) v1 ).readLine ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
(( java.lang.String ) v2 ).trim ( ); // invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* move-object v0, v2 */
/* .line 78 */
try { // :try_start_2
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 80 */
} // .end local v1 # "cmdlineReader":Ljava/io/BufferedReader;
/* .line 76 */
/* .restart local v1 # "cmdlineReader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v2 */
try { // :try_start_3
(( java.io.BufferedReader ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v3 */
try { // :try_start_4
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "cmdline":Ljava/lang/String;
} // .end local p0 # "pid":I
} // :goto_0
/* throw v2 */
/* :try_end_4 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 78 */
} // .end local v1 # "cmdlineReader":Ljava/io/BufferedReader;
/* .restart local v0 # "cmdline":Ljava/lang/String; */
/* .restart local p0 # "pid":I */
/* :catch_0 */
/* move-exception v1 */
/* .line 79 */
/* .local v1, "e":Ljava/lang/Exception; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Read process("; // const-string v3, "Read process("
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v3 = ") cmdline Error"; // const-string v3, ") cmdline Error"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v3 = "MiuiSentinelUtils"; // const-string v3, "MiuiSentinelUtils"
android.util.Slog .w ( v3,v2 );
/* .line 81 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
} // .end method
public static Long getProcessThreshold ( java.lang.String p0, Integer p1 ) {
/* .locals 4 */
/* .param p0, "processname" # Ljava/lang/String; */
/* .param p1, "type" # I */
/* .line 84 */
/* const-wide/16 v0, 0x0 */
/* .line 85 */
/* .local v0, "threshold":J */
/* const-string/jumbo v2, "system_server" */
/* packed-switch p1, :pswitch_data_0 */
/* .line 101 */
final String v2 = "MiuiSentinelUtils"; // const-string v2, "MiuiSentinelUtils"
final String v3 = "The process threshold cannot be obtained and may not be in the monitoring whitelist"; // const-string v3, "The process threshold cannot be obtained and may not be in the monitoring whitelist"
android.util.Slog .d ( v2,v3 );
/* .line 102 */
/* const-wide/16 v0, -0x1 */
/* .line 87 */
/* :pswitch_0 */
com.miui.server.sentinel.MiuiSentinelService .getAppNativeheapWhiteList ( );
(( java.util.HashMap ) v3 ).get ( p0 ); // invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 88 */
com.miui.server.sentinel.MiuiSentinelService .getAppNativeheapWhiteList ( );
(( java.util.HashMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* int-to-long v0, v2 */
/* .line 89 */
} // :cond_0
v2 = (( java.lang.String ) v2 ).equals ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 90 */
/* const-wide/32 v0, 0x57800 */
/* .line 94 */
/* :pswitch_1 */
com.miui.server.sentinel.MiuiSentinelService .getAppJavaheapWhiteList ( );
(( java.util.HashMap ) v3 ).get ( p0 ); // invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 95 */
com.miui.server.sentinel.MiuiSentinelService .getAppJavaheapWhiteList ( );
(( java.util.HashMap ) v2 ).get ( p0 ); // invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Ljava/lang/Integer; */
v2 = (( java.lang.Integer ) v2 ).intValue ( ); // invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
/* int-to-long v0, v2 */
/* .line 96 */
} // :cond_1
v2 = (( java.lang.String ) v2 ).equals ( p0 ); // invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 97 */
/* const-wide/32 v0, 0x64000 */
/* .line 104 */
} // :cond_2
} // :goto_0
/* return-wide v0 */
/* :pswitch_data_0 */
/* .packed-switch 0x11 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public static Long getTotalRss ( ) {
/* .locals 3 */
/* .line 62 */
/* new-instance v0, Landroid/os/Debug$MemoryInfo; */
/* invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V */
/* .line 63 */
/* .local v0, "mi":Landroid/os/Debug$MemoryInfo; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v2, "total RSS :" */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = (( android.os.Debug$MemoryInfo ) v0 ).getTotalRss ( ); // invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalRss()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSentinelUtils"; // const-string v2, "MiuiSentinelUtils"
android.util.Slog .d ( v2,v1 );
/* .line 64 */
v1 = (( android.os.Debug$MemoryInfo ) v0 ).getTotalRss ( ); // invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalRss()I
/* int-to-long v1, v1 */
/* return-wide v1 */
} // .end method
public static Boolean isEnaleTrack ( ) {
/* .locals 2 */
/* .line 55 */
final String v0 = "persist.track.malloc.enable"; // const-string v0, "persist.track.malloc.enable"
final String v1 = ""; // const-string v1, ""
android.os.SystemProperties .get ( v0,v1 );
/* if-eq v0, v1, :cond_0 */
/* .line 56 */
int v0 = 1; // const/4 v0, 0x1
/* .line 58 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public static Boolean isLaboratoryTest ( ) {
/* .locals 1 */
/* .line 51 */
v0 = com.miui.server.sentinel.MiuiSentinelUtils .isMtbfTest ( );
/* if-nez v0, :cond_1 */
v0 = com.miui.server.sentinel.MiuiSentinelUtils .isOmniTest ( );
if ( v0 != null) { // if-eqz v0, :cond_0
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // :cond_1
} // :goto_0
int v0 = 1; // const/4 v0, 0x1
} // :goto_1
} // .end method
private static Boolean isMtbfTest ( ) {
/* .locals 2 */
/* .line 46 */
final String v0 = "persist.mtbf.test"; // const-string v0, "persist.mtbf.test"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelUtils;->REBOOT_COREDUMP:Z */
/* if-nez v0, :cond_0 */
/* sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelUtils;->MTBF_MIUI_TEST:Z */
if ( v0 != null) { // if-eqz v0, :cond_1
} // :cond_0
int v1 = 1; // const/4 v1, 0x1
} // :cond_1
} // .end method
private static Boolean isOmniTest ( ) {
/* .locals 3 */
/* .line 42 */
final String v0 = "persist.omni.test"; // const-string v0, "persist.omni.test"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getInt ( v0,v1 );
int v2 = 1; // const/4 v2, 0x1
/* if-ne v0, v2, :cond_0 */
/* move v1, v2 */
} // :cond_0
} // .end method
