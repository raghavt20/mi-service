.class public Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
.super Ljava/lang/Object;
.source "MiuiSentinelMemoryManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;,
        Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;
    }
.end annotation


# static fields
.field public static final DEBUG:Z

.field private static final DETAILS_LIMIT:I = 0x7d0

.field private static final DIALOG_APP_LIST:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ENABLE_MQS_REPORT:Z

.field public static final ENABLE_SENTINEL_MEMORY_MONITOR:Z

.field private static final END_TRACK:I = 0x4

.field private static final END_TRACK_SIGNAL:I = 0x33

.field private static final HANDLER_NAME:Ljava/lang/String; = "sentineMemoryWork"

.field private static final MAX_FD_AMOUNT:I = 0x3e8

.field private static final MAX_THREAD_AMOUNT:I = 0x1f4

.field private static final MEMLEAK_DIR:Ljava/lang/String; = "/data/miuilog/stability/memleak/heapleak"

.field private static final REPORT_FD_AMOUNT_LEAKTOMQS:I = 0x9

.field private static final REPORT_JAVAHEAP_LEAKTOMQS:I = 0x7

.field private static final REPORT_NATIVEHEAP_LEAKTOMQS:I = 0x6

.field private static final REPORT_THREAD_AMOUNT_LEAKTOMQS:I = 0x8

.field private static final RESUME_LEAK:I = 0x5

.field private static final START_FD_TRACK:I = 0x2

.field private static final START_THREAD_TRACK:I = 0x3

.field private static final START_TRACK:I = 0x1

.field private static final START_TRACK_SIGNAL:I = 0x32

.field private static final SYSPROP_ENABLE_MQS_REPORT:Ljava/lang/String; = "persist.sys.debug.enable_mqs_report"

.field private static final SYSPROP_ENABLE_RESUME_STRATEGY:Ljava/lang/String; = "persist.sentinel.resume.enable"

.field private static final SYSPROP_ENABLE_SENTINEL_MEMORY_MONITOR:Ljava/lang/String; = "persist.sys.debug.enable_sentinel_memory_monitor"

.field private static final SYSPROP_ENABLE_TRACK_MALLOC:Ljava/lang/String; = "persist.track.malloc.enable"

.field private static final SYSTEM_SERVER:Ljava/lang/String; = "system_server"

.field private static final SYSTEM_SERVER_MAX_JAVAHEAP:I = 0x64000

.field private static final SYSTEM_SERVER_MAX_NATIVEHEAP:I = 0x57800

.field private static final TAG:Ljava/lang/String; = "MiuiSentinelMemoryManager"

.field private static final TOTAL_RSS_LIMIT:I = 0x300000

.field private static miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;


# instance fields
.field private eventList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private highCapacityRssList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mMiuiSentineThread:Landroid/os/HandlerThread;

.field private volatile mSentineHandler:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

.field private mService:Lcom/android/server/am/ActivityManagerService;

.field private trackEventList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private trackList:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/sentinel/NativeHeapUsageInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static bridge synthetic -$$Nest$mresumeMemLeak(Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(Ljava/lang/Object;)V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 85
    const-string v0, "debug.sys.mss"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z

    .line 86
    nop

    .line 87
    const-string v0, "persist.sys.debug.enable_sentinel_memory_monitor"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->ENABLE_SENTINEL_MEMORY_MONITOR:Z

    .line 88
    const-string v0, "persist.sys.debug.enable_mqs_report"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->ENABLE_MQS_REPORT:Z

    .line 99
    new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$1;

    invoke-direct {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$1;-><init>()V

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DIALOG_APP_LIST:Ljava/util/HashSet;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 90
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 91
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackEventList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 92
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->highCapacityRssList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 168
    new-instance v0, Landroid/os/HandlerThread;

    const-string/jumbo v1, "sentineMemoryWork"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mMiuiSentineThread:Landroid/os/HandlerThread;

    .line 169
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 170
    new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mMiuiSentineThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;-><init>(Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mSentineHandler:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

    .line 171
    const-string v0, "MiuiSentinelMemoryManager"

    const-string v1, "MiuiSentinelMemoryManager init"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-void
.end method

.method private getDfxDetails(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "details"    # Ljava/lang/String;

    .line 333
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    return-object v0
.end method

.method public static getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
    .locals 1

    .line 155
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    invoke-direct {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;-><init>()V

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    .line 158
    :cond_0
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    return-object v0
.end method

.method private isLibraryExist(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .line 539
    const-string v0, "MiuiSentinelMemoryManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 540
    .local v1, "libraryName":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "lib"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "persist.track.malloc.enable"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".so"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 541
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    .local v2, "reader":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 543
    .local v3, "mapsInfo":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    if-eqz v4, :cond_1

    .line 544
    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 545
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " found track\'s library"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 546
    nop

    .line 549
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 546
    const/4 v0, 0x1

    return v0

    .line 549
    .end local v3    # "mapsInfo":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 551
    .end local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 541
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v1    # "libraryName":Ljava/lang/StringBuilder;
    .end local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
    .end local p1    # "path":Ljava/lang/String;
    :goto_0
    throw v3
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 549
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "libraryName":Ljava/lang/StringBuilder;
    .restart local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
    .restart local p1    # "path":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 550
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 552
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found track\'s library: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const/4 v0, 0x0

    return v0
.end method

.method private reportHeapLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "stacktrace"    # Ljava/lang/String;
    .param p4, "pid"    # I
    .param p5, "procname"    # Ljava/lang/String;
    .param p6, "growsize"    # J

    .line 290
    new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 291
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v1, "extraFile":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "/data/miuilog/stability/memleak/heapleak"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setExtraFiles(Ljava/util/List;)V

    .line 294
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 295
    invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V

    .line 296
    invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 297
    invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setProcessName(Ljava/lang/String;)V

    .line 298
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 299
    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 300
    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 301
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v2

    invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 302
    invoke-direct {p0, v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportMemLeakMisight(Lmiui/mqsas/sdk/event/ExceptionEvent;)V

    .line 303
    return-void
.end method

.method private reportLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "stacktrace"    # Ljava/lang/String;
    .param p4, "pid"    # I
    .param p5, "procname"    # Ljava/lang/String;

    .line 337
    new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 338
    .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 339
    invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V

    .line 340
    invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 341
    invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setProcessName(Ljava/lang/String;)V

    .line 342
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 343
    invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 345
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 346
    return-void
.end method

.method private reportMemLeakMisight(Lmiui/mqsas/sdk/event/ExceptionEvent;)V
    .locals 5
    .param p1, "event"    # Lmiui/mqsas/sdk/event/ExceptionEvent;

    .line 306
    if-nez p1, :cond_0

    .line 307
    return-void

    .line 310
    :cond_0
    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getType()I

    move-result v0

    const-string v1, "Stacktrace"

    packed-switch v0, :pswitch_data_0

    .line 324
    return-void

    .line 320
    :pswitch_0
    new-instance v0, Lcom/miui/misight/MiEvent;

    const v2, 0x35b43bad

    invoke-direct {v0, v2}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 321
    .local v0, "miEvent":Lcom/miui/misight/MiEvent;
    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getDfxDetails(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 322
    goto :goto_0

    .line 316
    .end local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    :pswitch_1
    new-instance v0, Lcom/miui/misight/MiEvent;

    const v2, 0x35b43bac

    invoke-direct {v0, v2}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 317
    .restart local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getDfxDetails(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 318
    goto :goto_0

    .line 312
    .end local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    :pswitch_2
    new-instance v0, Lcom/miui/misight/MiEvent;

    const v1, 0x35b43bab

    invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V

    .line 313
    .restart local v0    # "miEvent":Lcom/miui/misight/MiEvent;
    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getDfxDetails(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "RssInfo"

    invoke-virtual {v0, v2, v1}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    .line 314
    nop

    .line 326
    :goto_0
    const-string v1, "Summary"

    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getSummary()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    move-result-object v1

    .line 327
    invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PackageName"

    invoke-virtual {v1, v3, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;

    move-result-object v1

    .line 328
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string v4, "CurrentTime"

    invoke-virtual {v1, v4, v2, v3}, Lcom/miui/misight/MiEvent;->addLong(Ljava/lang/String;J)Lcom/miui/misight/MiEvent;

    .line 329
    invoke-static {v0}, Lcom/miui/misight/MiSight;->sendEvent(Lcom/miui/misight/MiEvent;)V

    .line 330
    return-void

    :pswitch_data_0
    .packed-switch 0x1a0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private reportRssLeakEvent(ILjava/lang/String;Lcom/miui/server/sentinel/RssUsageInfo;)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "subject"    # Ljava/lang/String;
    .param p3, "rssUsageInfo"    # Lcom/miui/server/sentinel/RssUsageInfo;

    .line 264
    invoke-virtual {p3}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I

    move-result v0

    .line 265
    .local v0, "pid":I
    invoke-static {v0}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "packageName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 267
    invoke-static {v0}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v1

    .line 269
    :cond_0
    const-string/jumbo v2, "unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 271
    const-string v2, "MiuiSentinelMemoryManager"

    const-string v3, "The current process may exit and ignore report mqs"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    return-void

    .line 274
    :cond_1
    const-string v2, "RSS_LEAK"

    invoke-static {v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getExceptionPath(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 275
    .local v2, "filename":Ljava/io/File;
    invoke-virtual {p3}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J

    move-result-wide v3

    invoke-static {v0, v3, v4, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->dumpRssInfo(IJLjava/lang/String;Ljava/io/File;)V

    .line 276
    new-instance v3, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;

    invoke-direct {v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V

    .line 277
    .local v3, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent;
    invoke-virtual {v3, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V

    .line 278
    invoke-virtual {v3, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V

    .line 279
    invoke-virtual {v3, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V

    .line 280
    invoke-virtual {v3, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setProcessName(Ljava/lang/String;)V

    .line 281
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V

    .line 282
    invoke-virtual {v3, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V

    .line 283
    invoke-virtual {v3, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V

    .line 284
    invoke-direct {p0, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportMemLeakMisight(Lmiui/mqsas/sdk/event/ExceptionEvent;)V

    .line 285
    invoke-static {}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->getInstance()Lmiui/mqsas/sdk/MQSEventManagerDelegate;

    move-result-object v4

    invoke-virtual {v4, v3}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z

    .line 286
    return-void
.end method

.method private resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "pid"    # I
    .param p2, "adj"    # I
    .param p3, "size"    # J
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "reason"    # Ljava/lang/String;
    .param p7, "type"    # Ljava/lang/String;

    .line 434
    move/from16 v0, p1

    move/from16 v1, p2

    move-wide/from16 v2, p3

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->isLaboratoryTest()Z

    move-result v6

    const-string v7, "kB"

    const-string v8, ") used"

    const-string v9, "("

    if-eqz v6, :cond_6

    .line 435
    const-string v6, "MiuiSentinelMemoryManager"

    if-lez v0, :cond_5

    invoke-static/range {p1 .. p1}, Landroid/os/Process;->getThreadGroupLeader(I)I

    move-result v10

    if-ne v10, v0, :cond_5

    .line 436
    const-string v10, "kB "

    const/16 v11, -0x384

    if-ne v1, v11, :cond_1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v12

    if-eq v0, v12, :cond_0

    move-object/from16 v12, p7

    goto :goto_0

    .line 437
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "system_server("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") use "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v12, p7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "kb too many occurring"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "system_server ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") used "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 436
    :cond_1
    move-object/from16 v12, p7

    .line 440
    :goto_0
    move-object/from16 v13, p0

    iget-object v14, v13, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mService:Lcom/android/server/am/ActivityManagerService;

    if-eqz v14, :cond_4

    if-le v1, v11, :cond_4

    const/16 v11, 0x3e9

    if-ge v1, v11, :cond_4

    .line 441
    invoke-static/range {p1 .. p1}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v10

    .line 442
    .local v10, "app":Lcom/android/server/am/ProcessRecord;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 443
    .local v11, "appReason":Ljava/lang/String;
    const/4 v14, 0x0

    .line 444
    .local v14, "killAction":Z
    invoke-static {}, Lcom/android/server/am/ScoutMemoryError;->getInstance()Lcom/android/server/am/ScoutMemoryError;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 445
    const/4 v14, 0x1

    .line 447
    :cond_2
    if-nez v14, :cond_3

    .line 448
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Kill"

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 449
    invoke-static/range {p1 .. p1}, Landroid/os/Process;->killProcess(I)V

    .line 451
    :cond_3
    invoke-static {v6, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    .end local v10    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v11    # "appReason":Ljava/lang/String;
    .end local v14    # "killAction":Z
    goto/16 :goto_1

    .line 453
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Kill "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "), Used "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-static/range {p1 .. p1}, Landroid/os/Process;->killProcess(I)V

    goto :goto_1

    .line 435
    :cond_5
    move-object/from16 v13, p0

    move-object/from16 v12, p7

    .line 458
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ") is invalid"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 461
    :cond_6
    move-object/from16 v13, p0

    move-object/from16 v12, p7

    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DIALOG_APP_LIST:Ljava/util/HashSet;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 462
    invoke-static/range {p1 .. p1}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord;

    move-result-object v1

    .line 463
    .local v1, "app":Lcom/android/server/am/ProcessRecord;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 464
    .local v6, "appReason":Ljava/lang/String;
    invoke-static {}, Lcom/android/server/am/ScoutMemoryError;->getInstance()Lcom/android/server/am/ScoutMemoryError;

    move-result-object v7

    invoke-virtual {v7, v1, v6}, Lcom/android/server/am/ScoutMemoryError;->showAppMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z

    .line 467
    .end local v1    # "app":Lcom/android/server/am/ProcessRecord;
    .end local v6    # "appReason":Ljava/lang/String;
    :cond_7
    :goto_1
    return-void
.end method

.method private resumeMemLeak(Ljava/lang/Object;)V
    .locals 11
    .param p1, "object"    # Ljava/lang/Object;

    .line 404
    const-string v0, "persist.sentinel.resume.enable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    const-string v0, "MiuiSentinelMemoryManager"

    const-string v1, "no enable resume tactics \uff01"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    return-void

    .line 409
    :cond_0
    instance-of v0, p1, Lcom/miui/server/sentinel/RssUsageInfo;

    if-eqz v0, :cond_1

    .line 410
    move-object v0, p1

    check-cast v0, Lcom/miui/server/sentinel/RssUsageInfo;

    .line 411
    .local v0, "rssUsageInfo":Lcom/miui/server/sentinel/RssUsageInfo;
    const-string v9, "RSS is too large, leak occurred"

    .line 412
    .local v9, "reason":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I

    move-result v1

    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->getCurAdjByPid(I)I

    move-result v10

    .line 413
    .local v10, "adj":I
    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I

    move-result v2

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J

    move-result-wide v4

    .line 414
    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getMaxIncrease()Ljava/lang/String;

    move-result-object v8

    .line 413
    move-object v1, p0

    move v3, v10

    move-object v7, v9

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    .end local v0    # "rssUsageInfo":Lcom/miui/server/sentinel/RssUsageInfo;
    .end local v9    # "reason":Ljava/lang/String;
    .end local v10    # "adj":I
    goto :goto_1

    :cond_1
    instance-of v0, p1, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    if-eqz v0, :cond_2

    .line 416
    move-object v0, p1

    check-cast v0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    .line 417
    .local v0, "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    const-string v9, "Native Heap is too large, leak occurred"

    .line 418
    .restart local v9    # "reason":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v1

    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->getCurAdjByPid(I)I

    move-result v10

    .line 419
    .restart local v10    # "adj":I
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v2

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J

    move-result-wide v4

    .line 420
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v8, "NativeHeap"

    .line 419
    move-object v1, p0

    move v3, v10

    move-object v7, v9

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    .end local v9    # "reason":Ljava/lang/String;
    .end local v10    # "adj":I
    goto :goto_0

    .line 421
    :cond_2
    instance-of v0, p1, Lcom/miui/server/sentinel/JavaHeapUsageInfo;

    if-eqz v0, :cond_3

    .line 422
    move-object v0, p1

    check-cast v0, Lcom/miui/server/sentinel/JavaHeapUsageInfo;

    .line 423
    .local v0, "javaHeapUsageInfo":Lcom/miui/server/sentinel/JavaHeapUsageInfo;
    const-string v9, "Java Heap is too large, leak occurred"

    .line 424
    .restart local v9    # "reason":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getPid()I

    move-result v1

    invoke-static {v1}, Lcom/android/server/am/ProcessUtils;->getCurAdjByPid(I)I

    move-result v10

    .line 425
    .restart local v10    # "adj":I
    invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getPid()I

    move-result v2

    invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getJavaHeapSize()J

    move-result-wide v4

    .line 426
    invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v8, "JavaHeap"

    .line 425
    move-object v1, p0

    move v3, v10

    move-object v7, v9

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 421
    .end local v0    # "javaHeapUsageInfo":Lcom/miui/server/sentinel/JavaHeapUsageInfo;
    .end local v9    # "reason":Ljava/lang/String;
    .end local v10    # "adj":I
    :cond_3
    :goto_0
    nop

    .line 428
    :goto_1
    return-void
.end method


# virtual methods
.method public enableNativeHeapTrack(Lcom/miui/server/sentinel/NativeHeapUsageInfo;Z)V
    .locals 3
    .param p1, "nativeHeapUsageInfo"    # Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    .param p2, "isEnable"    # Z

    .line 648
    if-eqz p2, :cond_0

    .line 649
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v0

    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->START_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z

    .line 650
    const-string v0, "MiuiSentinelMemoryManager"

    const-string v1, "begin stack track "

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    const/4 v0, 0x4

    const-wide/32 v1, 0x493e0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;IJ)V

    .line 652
    return-void

    .line 654
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->removeTrackEvent(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)V

    .line 655
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v0

    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->REPORT_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z

    .line 656
    return-void
.end method

.method public filterMessages(Lcom/miui/server/sentinel/SocketPacket;)Z
    .locals 9
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 176
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "procname":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "type":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    sget-boolean v3, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "filtermessages item:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MiuiSentinelMemoryManager"

    invoke-static {v4, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_0
    iget-object v3, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez v3, :cond_1

    .line 184
    iget-object v3, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    return v4

    .line 186
    :cond_1
    iget-object v3, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v6, 0x3

    if-ge v3, v6, :cond_2

    .line 187
    iget-object v3, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    return v4

    .line 190
    :cond_2
    return v5
.end method

.method public getFdUsageInfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/FdUsageInfo;
    .locals 4
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 394
    new-instance v0, Lcom/miui/server/sentinel/FdUsageInfo;

    invoke-direct {v0}, Lcom/miui/server/sentinel/FdUsageInfo;-><init>()V

    .line 395
    .local v0, "info":Lcom/miui/server/sentinel/FdUsageInfo;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 396
    .local v1, "split":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/FdUsageInfo;->setName(Ljava/lang/String;)V

    .line 397
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/FdUsageInfo;->setPid(I)V

    .line 398
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/FdUsageInfo;->setFd_amount(J)V

    .line 399
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/FdUsageInfo;->setUsageInfo(Ljava/lang/String;)V

    .line 400
    return-object v0
.end method

.method public getJavaHeapinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/JavaHeapUsageInfo;
    .locals 4
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 374
    new-instance v0, Lcom/miui/server/sentinel/JavaHeapUsageInfo;

    invoke-direct {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;-><init>()V

    .line 375
    .local v0, "info":Lcom/miui/server/sentinel/JavaHeapUsageInfo;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 376
    .local v1, "split":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->setName(Ljava/lang/String;)V

    .line 377
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->setPid(I)V

    .line 378
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->setJavaHeapSize(J)V

    .line 379
    return-object v0
.end method

.method public getNativeHeapinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    .locals 4
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 364
    new-instance v0, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    invoke-direct {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;-><init>()V

    .line 365
    .local v0, "info":Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "split":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setName(Ljava/lang/String;)V

    .line 367
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setPid(I)V

    .line 368
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setNativeHeapSize(J)V

    .line 369
    return-object v0
.end method

.method public getRssinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/RssUsageInfo;
    .locals 7
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 350
    new-instance v0, Lcom/miui/server/sentinel/RssUsageInfo;

    invoke-direct {v0}, Lcom/miui/server/sentinel/RssUsageInfo;-><init>()V

    .line 351
    .local v0, "info":Lcom/miui/server/sentinel/RssUsageInfo;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRSSinfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSentinelMemoryManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v1

    const-string v3, "#"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 353
    .local v1, "split":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Lcom/miui/server/sentinel/RssUsageInfo;->setName(Ljava/lang/String;)V

    .line 354
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/miui/server/sentinel/RssUsageInfo;->setPid(I)V

    .line 355
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lcom/miui/server/sentinel/RssUsageInfo;->setRssSize(J)V

    .line 356
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;

    move-result-object v3

    .line 357
    .local v3, "data":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cmdline: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "RSSsize:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 358
    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 357
    invoke-static {v2, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    return-object v0
.end method

.method public getThreadUsageInfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/ThreadUsageInfo;
    .locals 4
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 383
    new-instance v0, Lcom/miui/server/sentinel/ThreadUsageInfo;

    invoke-direct {v0}, Lcom/miui/server/sentinel/ThreadUsageInfo;-><init>()V

    .line 384
    .local v0, "info":Lcom/miui/server/sentinel/ThreadUsageInfo;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getThreadinfo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSentinelMemoryManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 386
    .local v1, "split":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setName(Ljava/lang/String;)V

    .line 387
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setPid(I)V

    .line 388
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setThreadAmount(J)V

    .line 389
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setUsageInfo(Ljava/lang/String;)V

    .line 390
    return-object v0
.end method

.method public getTrackList()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/miui/server/sentinel/NativeHeapUsageInfo;",
            ">;"
        }
    .end annotation

    .line 674
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackList:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z
    .locals 5
    .param p1, "pid"    # I
    .param p2, "action"    # Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    .line 511
    const/4 v0, 0x0

    const-string v1, "MiuiSentinelMemoryManager"

    if-gtz p1, :cond_0

    .line 512
    const-string v2, "Failed to enable Track! pid is invalid"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    return v0

    .line 515
    :cond_0
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->isEnaleTrack()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 516
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/proc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/maps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 517
    .local v2, "mapsPath":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->isLibraryExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 518
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$2;->$SwitchMap$com$miui$server$sentinel$MiuiSentinelMemoryManager$Action:[I

    invoke-virtual {p2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->ordinal()I

    move-result v3

    aget v0, v0, v3

    const-string v3, ") Track"

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 524
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "report pid("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    const/16 v0, 0x33

    invoke-static {p1, v0}, Landroid/os/Process;->sendSignal(II)V

    .line 526
    goto :goto_0

    .line 520
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "begin pid("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    const/16 v0, 0x32

    invoke-static {p1, v0}, Landroid/os/Process;->sendSignal(II)V

    .line 522
    nop

    .line 530
    :goto_0
    const-string v0, "enable track malloc sucess!"

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    const/4 v0, 0x1

    return v0

    .line 534
    .end local v2    # "mapsPath":Ljava/lang/String;
    :cond_1
    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
    .locals 1
    .param p1, "mService"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "mContext"    # Landroid/content/Context;

    .line 162
    iput-object p1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 163
    iput-object p2, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mContext:Landroid/content/Context;

    .line 164
    invoke-static {}, Lcom/android/server/am/ScoutMemoryError;->getInstance()Lcom/android/server/am/ScoutMemoryError;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ScoutMemoryError;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V

    .line 165
    return-void
.end method

.method public isEnableMqsReport()Z
    .locals 1

    .line 496
    sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->ENABLE_MQS_REPORT:Z

    if-eqz v0, :cond_0

    .line 497
    const/4 v0, 0x1

    return v0

    .line 498
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isEnableSentinelMemoryMonitor()Z
    .locals 1

    .line 490
    sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->ENABLE_SENTINEL_MEMORY_MONITOR:Z

    if-eqz v0, :cond_0

    .line 491
    const/4 v0, 0x1

    return v0

    .line 492
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public isProcessTracked(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)Z
    .locals 3
    .param p1, "nativeHeapUsageInfo"    # Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    .line 660
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 661
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackEventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    .line 662
    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackEventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    const/4 v1, 0x0

    return v1

    .line 665
    :cond_0
    return v2
.end method

.method public judgmentFdAmountLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 5
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 257
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getFdUsageInfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/FdUsageInfo;

    move-result-object v0

    .line 258
    .local v0, "fdUsageInfo":Lcom/miui/server/sentinel/FdUsageInfo;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/FdUsageInfo;->getFd_amount()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 259
    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    .line 261
    :cond_0
    return-void
.end method

.method public judgmentJavaHeapLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 7
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 233
    invoke-virtual {p0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->isEnableMqsReport()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    return-void

    .line 236
    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getJavaHeapinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/JavaHeapUsageInfo;

    move-result-object v0

    .line 237
    .local v0, "javaHeapUsageInfo":Lcom/miui/server/sentinel/JavaHeapUsageInfo;
    const-string/jumbo v1, "system_server"

    invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x7

    if-eqz v1, :cond_1

    .line 238
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v3

    const-wide/32 v5, 0x64000

    cmp-long v1, v3, v5

    if-lez v1, :cond_2

    .line 239
    invoke-virtual {p0, v0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    goto :goto_0

    .line 242
    :cond_1
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->getAppJavaheapWhiteList()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v3, v1

    .line 243
    .local v3, "limit":J
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v5

    cmp-long v1, v5, v3

    if-lez v1, :cond_2

    .line 244
    invoke-virtual {p0, v0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    .line 247
    .end local v3    # "limit":J
    :cond_2
    :goto_0
    return-void
.end method

.method public judgmentNativeHeapLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 10
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 207
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getNativeHeapinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    move-result-object v0

    .line 208
    .local v0, "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "system_server"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v3, 0x1

    const-string v4, "#"

    const/4 v5, 0x0

    .line 210
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 208
    if-eqz v1, :cond_0

    .line 209
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J

    move-result-wide v6

    const-wide/32 v8, 0x57800

    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->highCapacityRssList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 210
    invoke-virtual {v1, v2, v5}, Ljava/util/concurrent/ConcurrentHashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 211
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 213
    iget-object v2, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    invoke-virtual {p0, v0, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    .line 215
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    goto/16 :goto_0

    .line 217
    :cond_0
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->getAppNativeheapWhiteList()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 218
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->getAppNativeheapWhiteList()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    .line 219
    .local v1, "limit":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "app limit: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "MiuiSentinelMemoryManager"

    invoke-static {v7, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v6

    cmp-long v6, v6, v1

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->highCapacityRssList:Ljava/util/concurrent/ConcurrentHashMap;

    .line 221
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 222
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v6

    if-ne v5, v6, :cond_1

    .line 223
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 224
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 225
    iget-object v4, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    invoke-virtual {p0, v0, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    .line 230
    .end local v1    # "limit":J
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    :goto_0
    return-void
.end method

.method public judgmentRssLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 9
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 194
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getRssinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/RssUsageInfo;

    move-result-object v0

    .line 195
    .local v0, "rssUsageInfo":Lcom/miui/server/sentinel/RssUsageInfo;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J

    move-result-wide v1

    .line 196
    .local v1, "rssSize":J
    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    long-to-double v5, v1

    mul-double/2addr v5, v3

    const-wide/high16 v3, 0x4148000000000000L    # 3145728.0

    div-double/2addr v5, v3

    .line 197
    .local v5, "percentage":D
    const-wide v3, 0x3fc999999999999aL    # 0.2

    cmpl-double v3, v5, v3

    if-lez v3, :cond_0

    .line 198
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    .local v3, "sb":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RSS Leak in pid ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RSS size:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->removeEventList(Lcom/miui/server/sentinel/SocketPacket;)V

    .line 202
    const/16 v4, 0x1a0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v4, v7, v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportRssLeakEvent(ILjava/lang/String;Lcom/miui/server/sentinel/RssUsageInfo;)V

    .line 204
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method

.method public judgmentThreadAmountLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 5
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 250
    invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getThreadUsageInfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/ThreadUsageInfo;

    move-result-object v0

    .line 251
    .local v0, "threadUsageInfo":Lcom/miui/server/sentinel/ThreadUsageInfo;
    invoke-virtual {v0}, Lcom/miui/server/sentinel/ThreadUsageInfo;->getThreadAmount()J

    move-result-wide v1

    const-wide/16 v3, 0x1f4

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 252
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    .line 254
    :cond_0
    return-void
.end method

.method public outPutTrackLog(Lcom/miui/server/sentinel/TrackPacket;)V
    .locals 12
    .param p1, "trackPacket"    # Lcom/miui/server/sentinel/TrackPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 557
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/miuilog/stability/memleak/heapleak"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 558
    .local v0, "trackDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/16 v2, 0x1fc

    const-string v3, "MiuiSentinelMemoryManager"

    const/4 v4, -0x1

    if-nez v1, :cond_1

    .line 559
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 560
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    const-string v5, "cannot create memleak Dir"

    invoke-static {v3, v5, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 562
    :cond_0
    invoke-static {v0, v2, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 565
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getFormatDateTime(J)Ljava/lang/String;

    move-result-object v1

    .line 566
    .local v1, "dirSuffix":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 567
    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 569
    .local v5, "dirname":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 570
    .local v7, "exceptionDir":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_3

    .line 571
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    move-result v8

    if-nez v8, :cond_2

    .line 572
    new-instance v8, Ljava/lang/Throwable;

    invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V

    const-string v9, "cannot create exceptionDir"

    invoke-static {v3, v9, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 574
    :cond_2
    invoke-static {v7, v2, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    .line 576
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 577
    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "_heapleak_info.txt"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 579
    .local v6, "filename":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v7, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 580
    .local v8, "trackfile":Ljava/io/File;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 581
    .local v9, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getData()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 582
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_5

    .line 583
    invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z

    move-result v10

    if-nez v10, :cond_4

    .line 584
    new-instance v10, Ljava/lang/Throwable;

    invoke-direct {v10}, Ljava/lang/Throwable;-><init>()V

    const-string v11, "cannot create leakfile"

    invoke-static {v3, v11, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 586
    :cond_4
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v2, v4, v4}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 589
    :cond_5
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    const/4 v4, 0x1

    invoke-direct {v2, v8, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 590
    .local v2, "writers":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 591
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 593
    .end local v2    # "writers":Ljava/io/FileWriter;
    goto :goto_1

    .line 589
    .restart local v2    # "writers":Ljava/io/FileWriter;
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v10

    :try_start_4
    invoke-virtual {v4, v10}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "trackDir":Ljava/io/File;
    .end local v1    # "dirSuffix":Ljava/lang/String;
    .end local v5    # "dirname":Ljava/lang/String;
    .end local v6    # "filename":Ljava/lang/String;
    .end local v7    # "exceptionDir":Ljava/io/File;
    .end local v8    # "trackfile":Ljava/io/File;
    .end local v9    # "sb":Ljava/lang/StringBuilder;
    .end local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
    .end local p1    # "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
    :goto_0
    throw v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 591
    .end local v2    # "writers":Ljava/io/FileWriter;
    .restart local v0    # "trackDir":Ljava/io/File;
    .restart local v1    # "dirSuffix":Ljava/lang/String;
    .restart local v5    # "dirname":Ljava/lang/String;
    .restart local v6    # "filename":Ljava/lang/String;
    .restart local v7    # "exceptionDir":Ljava/io/File;
    .restart local v8    # "trackfile":Ljava/io/File;
    .restart local v9    # "sb":Ljava/lang/StringBuilder;
    .restart local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
    .restart local p1    # "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
    :catch_0
    move-exception v2

    .line 592
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    const-string v10, "Unable to write Track Stack to file"

    invoke-static {v3, v10, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 594
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    return-void
.end method

.method public removeEventList(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 5
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 502
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 503
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v1

    .line 504
    .local v1, "procname":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;

    move-result-object v2

    .line 505
    .local v2, "type":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "#"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    iget-object v3, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->eventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    return-void
.end method

.method public removeTrackEvent(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)V
    .locals 2
    .param p1, "nativeHeapUsageInfo"    # Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    .line 669
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 670
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->trackEventList:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 671
    return-void
.end method

.method public reportFdAmountLeak(Lcom/miui/server/sentinel/FdUsageInfo;)V
    .locals 7
    .param p1, "fdUsageInfo"    # Lcom/miui/server/sentinel/FdUsageInfo;

    .line 640
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Fd Amount Leak "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (Threshold = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 642
    .local v0, "fdleakmsg":Ljava/lang/String;
    const/16 v2, 0x1b0

    .line 643
    invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->getUsageInfo()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->getPid()I

    move-result v5

    .line 644
    invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    .line 642
    move-object v1, p0

    move-object v3, v0

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 645
    return-void
.end method

.method public reportJavaHeapLeak(Lcom/miui/server/sentinel/JavaHeapUsageInfo;)V
    .locals 9
    .param p1, "javaHeapUsageInfo"    # Lcom/miui/server/sentinel/JavaHeapUsageInfo;

    .line 611
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JavaHeap Leak "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (Threshold = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 612
    invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getProcessThreshold(Ljava/lang/String;I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KB)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 615
    .local v0, "javaleakmsg":Ljava/lang/String;
    const/16 v2, 0x1a2

    const-string v4, ""

    .line 616
    invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getPid()I

    move-result v5

    .line 617
    invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getJavaHeapSize()J

    move-result-wide v7

    .line 615
    move-object v1, p0

    move-object v3, v0

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportHeapLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V

    .line 618
    const/4 v1, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;IJ)V

    .line 619
    return-void
.end method

.method public reportNativeHeapLeak(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)V
    .locals 9
    .param p1, "nativeHeapUsageInfo"    # Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    .line 597
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NativeHeap Leak Proc info Name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 598
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Pid = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NativeHeap Size = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 599
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KB (Threshold = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 600
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x12

    invoke-static {v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getProcessThreshold(Ljava/lang/String;I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KB) "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 603
    .local v0, "nativeleakmsg":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "debug mqs info:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSentinelMemoryManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    const/16 v2, 0x1a1

    .line 605
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getStackTrace()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I

    move-result v5

    .line 606
    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J

    move-result-wide v7

    .line 604
    move-object v1, p0

    move-object v3, v0

    invoke-direct/range {v1 .. v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportHeapLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V

    .line 607
    const/4 v1, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;IJ)V

    .line 608
    return-void
.end method

.method public reportThreadAmountLeak(Lcom/miui/server/sentinel/ThreadUsageInfo;)V
    .locals 10
    .param p1, "threadUsageInfo"    # Lcom/miui/server/sentinel/ThreadUsageInfo;

    .line 622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thread Amount Leak "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/server/sentinel/ThreadUsageInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (Threshold = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 624
    .local v0, "thleakmsg":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/ThreadUsageInfo;->getPid()I

    move-result v7

    .line 625
    .local v7, "pid":I
    invoke-static {v7}, Lcom/android/server/am/ProcessUtils;->getPackageNameByPid(I)Ljava/lang/String;

    move-result-object v1

    .line 626
    .local v1, "packageName":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 627
    invoke-static {v7}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getProcessCmdline(I)Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    goto :goto_0

    .line 626
    :cond_0
    move-object v8, v1

    .line 629
    .end local v1    # "packageName":Ljava/lang/String;
    .local v8, "packageName":Ljava/lang/String;
    :goto_0
    const-string/jumbo v1, "unknown"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 630
    const-string v1, "MiuiSentinelMemoryManager"

    const-string v2, "The current process may exit and ignore report mqs"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    return-void

    .line 633
    :cond_1
    const-string v1, "Thread_LEAK"

    invoke-static {v7, v8, v1}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->getExceptionPath(ILjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    .line 634
    .local v9, "filename":Ljava/io/File;
    invoke-static {v7, v8, v9}, Lcom/miui/server/sentinel/MiuiSentinelUtils;->dumpThreadInfo(ILjava/lang/String;Ljava/io/File;)V

    .line 635
    const/16 v2, 0x1b8

    .line 636
    invoke-virtual {p1}, Lcom/miui/server/sentinel/ThreadUsageInfo;->getUsageInfo()Ljava/lang/String;

    move-result-object v4

    .line 635
    move-object v1, p0

    move-object v3, v0

    move v5, v7

    move-object v6, v8

    invoke-direct/range {v1 .. v6}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 637
    return-void
.end method

.method public sendMessage(Ljava/lang/Object;I)V
    .locals 3
    .param p2, "number"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;I)V"
        }
    .end annotation

    .line 470
    .local p1, "obj":Ljava/lang/Object;, "TT;"
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v0, v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mSentineHandler:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

    invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 471
    .local v0, "message":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    .line 472
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 473
    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mSentineHandler:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

    invoke-virtual {v1, v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->sendMessage(Landroid/os/Message;)Z

    .line 474
    sget-boolean v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 475
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msg.what = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "msg.obj = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSentinelMemoryManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    return-void
.end method

.method public sendMessage(Ljava/lang/Object;IJ)V
    .locals 4
    .param p2, "number"    # I
    .param p3, "timeout"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;IJ)V"
        }
    .end annotation

    .line 480
    .local p1, "obj":Ljava/lang/Object;, "TT;"
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v0, v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mSentineHandler:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

    invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 481
    .local v0, "message":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->what:I

    .line 482
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 483
    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->miuiSentinelMemoryManager:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->mSentineHandler:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;

    invoke-virtual {v1, v0, p3, p4}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 484
    sget-boolean v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 485
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msg.what = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "send message time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MiuiSentinelMemoryManager"

    invoke-static {v2, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    :cond_0
    return-void
.end method
