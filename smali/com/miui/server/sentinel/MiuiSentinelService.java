public class com.miui.server.sentinel.MiuiSentinelService extends android.os.Binder {
	 /* .source "MiuiSentinelService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;, */
	 /* Lcom/miui/server/sentinel/MiuiSentinelService$Lifecycle; */
	 /* } */
} // .end annotation
/* # static fields */
private static final java.util.HashMap APP_JAVAHEAP_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private static final java.util.HashMap APP_NATIVEHEAP_WHITE_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Boolean DEBUG;
private static final Integer DEFAULT_BUFFER_SIZE;
public static final java.lang.String HOST_NAME;
public static final Integer MAX_BUFF_SIZE;
public static final Integer PROC_HUNTER_MSG;
public static final Integer REPORT_NATIVEHEAP_LEAKTOMQS;
public static final java.lang.String SERVICE_NAME;
private static final java.lang.String TAG;
public static final Integer TRACK_HEAP_REPORT_MSG;
private static com.miui.server.sentinel.MiuiSentinelService$MiuiSentinelServiceThread miuiSentinelServiceThread;
private static volatile com.miui.server.sentinel.MiuiSentinelService sInstance;
/* # instance fields */
private android.content.Context mContext;
/* # direct methods */
static android.net.LocalSocket -$$Nest$mcreateSystemServerSocketForProchunter ( com.miui.server.sentinel.MiuiSentinelService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/sentinel/MiuiSentinelService;->createSystemServerSocketForProchunter()Landroid/net/LocalSocket; */
} // .end method
static com.miui.server.sentinel.MiuiSentinelService -$$Nest$minitContext ( com.miui.server.sentinel.MiuiSentinelService p0, android.content.Context p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->initContext(Landroid/content/Context;)Lcom/miui/server/sentinel/MiuiSentinelService; */
} // .end method
static void -$$Nest$mrecvMessage ( com.miui.server.sentinel.MiuiSentinelService p0, java.io.FileDescriptor p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->recvMessage(Ljava/io/FileDescriptor;)V */
return;
} // .end method
static com.miui.server.sentinel.MiuiSentinelService$MiuiSentinelServiceThread -$$Nest$sfgetmiuiSentinelServiceThread ( ) { //bridge//synthethic
/* .locals 1 */
v0 = com.miui.server.sentinel.MiuiSentinelService.miuiSentinelServiceThread;
} // .end method
static void -$$Nest$smnRegisteredBpfEvent ( ) { //bridge//synthethic
/* .locals 0 */
com.miui.server.sentinel.MiuiSentinelService .nRegisteredBpfEvent ( );
return;
} // .end method
static com.miui.server.sentinel.MiuiSentinelService ( ) {
/* .locals 2 */
/* .line 39 */
int v0 = 0; // const/4 v0, 0x0
/* .line 47 */
final String v0 = "debug.sys.mss"; // const-string v0, "debug.sys.mss"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.sentinel.MiuiSentinelService.DEBUG = (v0!= 0);
/* .line 50 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
/* .line 51 */
/* new-instance v0, Ljava/util/HashMap; */
/* invoke-direct {v0}, Ljava/util/HashMap;-><init>()V */
return;
} // .end method
private com.miui.server.sentinel.MiuiSentinelService ( ) {
/* .locals 2 */
/* .line 55 */
/* invoke-direct {p0}, Landroid/os/Binder;-><init>()V */
/* .line 53 */
int v0 = 0; // const/4 v0, 0x0
this.mContext = v0;
/* .line 56 */
/* new-instance v1, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread; */
/* invoke-direct {v1, p0, v0}, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;-><init>(Lcom/miui/server/sentinel/MiuiSentinelService;Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread-IA;)V */
/* .line 57 */
return;
} // .end method
private android.net.LocalSocket createSystemServerSocketForProchunter ( ) {
/* .locals 4 */
/* .line 193 */
int v0 = 0; // const/4 v0, 0x0
/* .line 195 */
/* .local v0, "serverSocket":Landroid/net/LocalSocket; */
try { // :try_start_0
/* new-instance v1, Landroid/net/LocalSocket; */
int v2 = 1; // const/4 v2, 0x1
/* invoke-direct {v1, v2}, Landroid/net/LocalSocket;-><init>(I)V */
/* move-object v0, v1 */
/* .line 196 */
/* new-instance v1, Landroid/net/LocalSocketAddress; */
final String v2 = "data/localsocket/prochunter_native_socket"; // const-string v2, "data/localsocket/prochunter_native_socket"
v3 = android.net.LocalSocketAddress$Namespace.ABSTRACT;
/* invoke-direct {v1, v2, v3}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V */
(( android.net.LocalSocket ) v0 ).bind ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->bind(Landroid/net/LocalSocketAddress;)V
/* .line 197 */
/* const/16 v1, 0x2710 */
(( android.net.LocalSocket ) v0 ).setSendBufferSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->setSendBufferSize(I)V
/* .line 198 */
(( android.net.LocalSocket ) v0 ).setReceiveBufferSize ( v1 ); // invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V
/* .line 199 */
final String v1 = "MiuiSentinelService"; // const-string v1, "MiuiSentinelService"
final String v2 = "prochunter socket create success"; // const-string v2, "prochunter socket create success"
android.util.Slog .e ( v1,v2 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 209 */
/* .line 200 */
/* :catch_0 */
/* move-exception v1 */
/* .line 201 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 202 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 204 */
try { // :try_start_1
(( android.net.LocalSocket ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_1 */
/* .line 206 */
/* .line 205 */
/* :catch_1 */
/* move-exception v2 */
/* .line 207 */
} // :goto_0
int v0 = 0; // const/4 v0, 0x0
/* .line 210 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :cond_0
} // :goto_1
} // .end method
public static java.util.HashMap getAppJavaheapWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 189 */
v0 = com.miui.server.sentinel.MiuiSentinelService.APP_JAVAHEAP_WHITE_LIST;
} // .end method
public static java.util.HashMap getAppNativeheapWhiteList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/HashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 185 */
v0 = com.miui.server.sentinel.MiuiSentinelService.APP_NATIVEHEAP_WHITE_LIST;
} // .end method
public static com.miui.server.sentinel.MiuiSentinelService getInstance ( ) {
/* .locals 2 */
/* .line 60 */
v0 = com.miui.server.sentinel.MiuiSentinelService.sInstance;
/* if-nez v0, :cond_1 */
/* .line 61 */
/* const-class v0, Lcom/miui/server/sentinel/MiuiSentinelService; */
/* monitor-enter v0 */
/* .line 62 */
try { // :try_start_0
v1 = com.miui.server.sentinel.MiuiSentinelService.sInstance;
/* if-nez v1, :cond_0 */
/* .line 63 */
/* new-instance v1, Lcom/miui/server/sentinel/MiuiSentinelService; */
/* invoke-direct {v1}, Lcom/miui/server/sentinel/MiuiSentinelService;-><init>()V */
/* .line 65 */
} // :cond_0
/* monitor-exit v0 */
/* :catchall_0 */
/* move-exception v1 */
/* monitor-exit v0 */
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* throw v1 */
/* .line 67 */
} // :cond_1
} // :goto_0
v0 = com.miui.server.sentinel.MiuiSentinelService.sInstance;
} // .end method
private void handlerTrackMessage ( com.miui.server.sentinel.TrackPacket p0 ) {
/* .locals 5 */
/* .param p1, "trackPacket" # Lcom/miui/server/sentinel/TrackPacket; */
/* .line 125 */
try { // :try_start_0
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v0 ).outPutTrackLog ( p1 ); // invoke-virtual {v0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->outPutTrackLog(Lcom/miui/server/sentinel/TrackPacket;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 128 */
/* .line 126 */
/* :catch_0 */
/* move-exception v0 */
/* .line 127 */
/* .local v0, "e":Ljava/io/IOException; */
/* new-instance v1, Ljava/lang/Throwable; */
/* invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V */
final String v2 = "MiuiSentinelService"; // const-string v2, "MiuiSentinelService"
final String v3 = "Track stack output to file failed"; // const-string v3, "Track stack output to file failed"
android.util.Slog .e ( v2,v3,v1 );
/* .line 131 */
} // .end local v0 # "e":Ljava/io/IOException;
} // :goto_0
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v0 ).getTrackList ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getTrackList()Ljava/util/concurrent/ConcurrentHashMap;
/* .line 132 */
/* .local v0, "tracklist":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/miui/server/sentinel/NativeHeapUsageInfo;>;" */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 133 */
/* .local v1, "key":Ljava/lang/StringBuilder; */
(( com.miui.server.sentinel.TrackPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "#"; // const-string v3, "#"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v3 = (( com.miui.server.sentinel.TrackPacket ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 134 */
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v0 ).get ( v2 ); // invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v2, Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* .line 135 */
/* .local v2, "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
if ( v2 != null) { // if-eqz v2, :cond_0
/* .line 136 */
(( com.miui.server.sentinel.TrackPacket ) p1 ).getData ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getData()Ljava/lang/String;
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v2 ).setStackTrace ( v3 ); // invoke-virtual {v2, v3}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setStackTrace(Ljava/lang/String;)V
/* .line 137 */
(( java.util.concurrent.ConcurrentHashMap ) v0 ).remove ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 138 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
int v4 = 6; // const/4 v4, 0x6
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v3 ).sendMessage ( v2, v4 ); // invoke-virtual {v3, v2, v4}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 140 */
} // :cond_0
return;
} // .end method
private void handlerTrigger ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 3 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 95 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getEvent_type ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;
v0 = com.miui.server.sentinel.MiuiSentinelEvent .getEventType ( v0 );
/* .line 96 */
/* .local v0, "type":I */
final String v1 = "MiuiSentinelService"; // const-string v1, "MiuiSentinelService"
/* packed-switch v0, :pswitch_data_0 */
/* .line 118 */
/* :pswitch_0 */
final String v2 = "receive invalid event"; // const-string v2, "receive invalid event"
android.util.Slog .e ( v1,v2 );
/* .line 114 */
/* :pswitch_1 */
final String v2 = "begin judgment FdAmountLeakException"; // const-string v2, "begin judgment FdAmountLeakException"
android.util.Slog .d ( v1,v2 );
/* .line 115 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v1 ).judgmentFdAmountLeakException ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentFdAmountLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
/* .line 116 */
/* .line 110 */
/* :pswitch_2 */
final String v2 = "begin judgmentRssLeakException"; // const-string v2, "begin judgmentRssLeakException"
android.util.Slog .d ( v1,v2 );
/* .line 111 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v1 ).judgmentRssLeakException ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentRssLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
/* .line 112 */
/* .line 106 */
/* :pswitch_3 */
final String v2 = "begin judgment ThreadAmountLeakException"; // const-string v2, "begin judgment ThreadAmountLeakException"
android.util.Slog .d ( v1,v2 );
/* .line 107 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v1 ).judgmentThreadAmountLeakException ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentThreadAmountLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
/* .line 108 */
/* .line 102 */
/* :pswitch_4 */
final String v2 = "begin judgmentJavaHeapLeakException"; // const-string v2, "begin judgmentJavaHeapLeakException"
android.util.Slog .d ( v1,v2 );
/* .line 103 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v1 ).judgmentJavaHeapLeakException ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentJavaHeapLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
/* .line 104 */
/* .line 98 */
/* :pswitch_5 */
final String v2 = "begin judgmentNativeHeapLeakException"; // const-string v2, "begin judgmentNativeHeapLeakException"
android.util.Slog .d ( v1,v2 );
/* .line 99 */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v1 ).judgmentNativeHeapLeakException ( p1 ); // invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentNativeHeapLeakException(Lcom/miui/server/sentinel/SocketPacket;)V
/* .line 100 */
/* nop */
/* .line 121 */
} // :goto_0
return;
/* nop */
/* :pswitch_data_0 */
/* .packed-switch 0x2 */
/* :pswitch_5 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_0 */
/* :pswitch_0 */
/* :pswitch_2 */
/* :pswitch_0 */
/* :pswitch_1 */
} // .end packed-switch
} // .end method
private com.miui.server.sentinel.MiuiSentinelService initContext ( android.content.Context p0 ) {
/* .locals 1 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 71 */
this.mContext = p1;
/* .line 72 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->initWhilteList(Landroid/content/Context;)V */
/* .line 73 */
com.miui.server.sentinel.MiuiSentinelService .getInstance ( );
} // .end method
private void initSystemServerTrack ( ) {
/* .locals 3 */
/* .line 315 */
v0 = android.os.Process .myPid ( );
/* .line 316 */
/* .local v0, "systemPid":I */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
v2 = com.miui.server.sentinel.MiuiSentinelMemoryManager$Action.START_TRACK;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v1 ).handlerTriggerTrack ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z
/* .line 317 */
return;
} // .end method
private void initWhilteList ( android.content.Context p0 ) {
/* .locals 13 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 143 */
(( android.content.Context ) p1 ).getResources ( ); // invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
/* .line 144 */
/* .local v0, "r":Landroid/content/res/Resources; */
/* const v1, 0x11030015 */
(( android.content.res.Resources ) v0 ).getStringArray ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 145 */
/* .local v1, "javaheaps":[Ljava/lang/String; */
/* const v2, 0x11030016 */
(( android.content.res.Resources ) v0 ).getStringArray ( v2 ); // invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
/* .line 146 */
/* .local v2, "nativeheaps":[Ljava/lang/String; */
if ( v1 != null) { // if-eqz v1, :cond_0
/* array-length v3, v1 */
if ( v3 != null) { // if-eqz v3, :cond_0
if ( v2 != null) { // if-eqz v2, :cond_0
/* array-length v3, v2 */
/* if-nez v3, :cond_1 */
/* .line 147 */
} // :cond_0
final String v3 = "MiuiSentinelService"; // const-string v3, "MiuiSentinelService"
final String v4 = "initwhileList is failed"; // const-string v4, "initwhileList is failed"
android.util.Slog .e ( v3,v4 );
/* .line 151 */
} // :cond_1
/* array-length v3, v1 */
int v4 = 0; // const/4 v4, 0x0
/* move v5, v4 */
} // :goto_0
final String v6 = ","; // const-string v6, ","
int v7 = 1; // const/4 v7, 0x1
/* if-ge v5, v3, :cond_2 */
/* aget-object v8, v1, v5 */
/* .line 152 */
/* .local v8, "javaheap":Ljava/lang/String; */
(( java.lang.String ) v8 ).split ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 153 */
/* .local v6, "split":[Ljava/lang/String; */
/* aget-object v7, v6, v7 */
v7 = java.lang.Integer .parseInt ( v7 );
java.lang.Integer .valueOf ( v7 );
/* .line 154 */
/* .local v7, "threshold":Ljava/lang/Integer; */
v9 = com.miui.server.sentinel.MiuiSentinelService.APP_JAVAHEAP_WHITE_LIST;
/* aget-object v10, v6, v4 */
(( java.util.HashMap ) v9 ).put ( v10, v7 ); // invoke-virtual {v9, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 151 */
} // .end local v6 # "split":[Ljava/lang/String;
} // .end local v7 # "threshold":Ljava/lang/Integer;
} // .end local v8 # "javaheap":Ljava/lang/String;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 158 */
} // :cond_2
/* array-length v3, v2 */
/* move v5, v4 */
} // :goto_1
/* if-ge v5, v3, :cond_3 */
/* aget-object v8, v2, v5 */
/* .line 159 */
/* .local v8, "nativeheap":Ljava/lang/String; */
(( java.lang.String ) v8 ).split ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 160 */
/* .local v9, "split":[Ljava/lang/String; */
/* aget-object v10, v9, v7 */
v10 = java.lang.Integer .parseInt ( v10 );
java.lang.Integer .valueOf ( v10 );
/* .line 161 */
/* .local v10, "threshold":Ljava/lang/Integer; */
v11 = com.miui.server.sentinel.MiuiSentinelService.APP_NATIVEHEAP_WHITE_LIST;
/* aget-object v12, v9, v4 */
(( java.util.HashMap ) v11 ).put ( v12, v10 ); // invoke-virtual {v11, v12, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 158 */
} // .end local v8 # "nativeheap":Ljava/lang/String;
} // .end local v9 # "split":[Ljava/lang/String;
} // .end local v10 # "threshold":Ljava/lang/Integer;
/* add-int/lit8 v5, v5, 0x1 */
/* .line 163 */
} // :cond_3
return;
} // .end method
private static native void nRegisteredBpfEvent ( ) {
} // .end method
private com.miui.server.sentinel.SocketPacket parseSocketPacket ( java.io.BufferedInputStream p0 ) {
/* .locals 10 */
/* .param p1, "bufferedInputStream" # Ljava/io/BufferedInputStream; */
/* .line 239 */
/* new-instance v0, Lcom/miui/server/sentinel/SocketPacket; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/SocketPacket;-><init>()V */
/* .line 240 */
/* .local v0, "socketPacket":Lcom/miui/server/sentinel/SocketPacket; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 241 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
/* const/16 v2, 0x2328 */
/* new-array v3, v2, [B */
/* .line 242 */
/* .local v3, "databuff":[B */
/* const/16 v4, 0x80 */
/* new-array v5, v4, [B */
/* .line 244 */
/* .local v5, "splitbuffer":[B */
int v6 = 4; // const/4 v6, 0x4
int v7 = 0; // const/4 v7, 0x0
try { // :try_start_0
(( java.io.BufferedInputStream ) p1 ).read ( v5, v7, v6 ); // invoke-virtual {p1, v5, v7, v6}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 245 */
v6 = com.miui.server.sentinel.SocketPacket .readInt ( v5 );
(( com.miui.server.sentinel.SocketPacket ) v0 ).setPid ( v6 ); // invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/SocketPacket;->setPid(I)V
/* .line 246 */
/* const/16 v6, 0x8 */
(( java.io.BufferedInputStream ) p1 ).read ( v5, v7, v6 ); // invoke-virtual {p1, v5, v7, v6}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 247 */
com.miui.server.sentinel.SocketPacket .readLong ( v5 );
/* move-result-wide v8 */
(( com.miui.server.sentinel.SocketPacket ) v0 ).setGrowsize ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Lcom/miui/server/sentinel/SocketPacket;->setGrowsize(J)V
/* .line 248 */
/* const/16 v6, 0x1e */
(( java.io.BufferedInputStream ) p1 ).read ( v5, v7, v6 ); // invoke-virtual {p1, v5, v7, v6}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 249 */
com.miui.server.sentinel.SocketPacket .readString ( v5,v7,v6 );
(( com.miui.server.sentinel.SocketPacket ) v0 ).setEvent_type ( v6 ); // invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/SocketPacket;->setEvent_type(Ljava/lang/String;)V
/* .line 250 */
(( java.io.BufferedInputStream ) p1 ).read ( v5, v7, v4 ); // invoke-virtual {p1, v5, v7, v4}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 251 */
com.miui.server.sentinel.SocketPacket .readString ( v5,v7,v4 );
(( com.miui.server.sentinel.SocketPacket ) v0 ).setProcess_name ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/sentinel/SocketPacket;->setProcess_name(Ljava/lang/String;)V
/* .line 252 */
} // :goto_0
v4 = (( java.io.BufferedInputStream ) p1 ).available ( ); // invoke-virtual {p1}, Ljava/io/BufferedInputStream;->available()I
/* if-lez v4, :cond_1 */
/* .line 253 */
v4 = (( java.io.BufferedInputStream ) p1 ).read ( v3 ); // invoke-virtual {p1, v3}, Ljava/io/BufferedInputStream;->read([B)I
/* .line 254 */
/* .local v4, "readsize":I */
/* if-ne v4, v2, :cond_0 */
/* .line 255 */
com.miui.server.sentinel.SocketPacket .readString ( v3,v7,v2 );
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 257 */
} // :cond_0
com.miui.server.sentinel.SocketPacket .readString ( v3,v7,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v6 ); // invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 259 */
} // .end local v4 # "readsize":I
} // :goto_1
/* .line 260 */
} // :cond_1
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.sentinel.SocketPacket ) v0 ).setData ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/SocketPacket;->setData(Ljava/lang/String;)V
/* .line 261 */
/* sget-boolean v2, Lcom/miui/server/sentinel/MiuiSentinelService;->DEBUG:Z */
if ( v2 != null) { // if-eqz v2, :cond_2
/* .line 262 */
final String v2 = "MiuiSentinelService"; // const-string v2, "MiuiSentinelService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "SocketPacket data = "; // const-string v6, "SocketPacket data = "
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.SocketPacket ) v0 ).toString ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/SocketPacket;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 266 */
/* :catch_0 */
/* move-exception v2 */
/* .line 267 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 264 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 265 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 268 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :cond_2
} // :goto_2
/* nop */
/* .line 269 */
} // :goto_3
} // .end method
private com.miui.server.sentinel.TrackPacket parseTrackPacket ( java.io.BufferedInputStream p0 ) {
/* .locals 11 */
/* .param p1, "bufferedInputStream" # Ljava/io/BufferedInputStream; */
/* .line 273 */
/* new-instance v0, Lcom/miui/server/sentinel/TrackPacket; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/TrackPacket;-><init>()V */
/* .line 274 */
/* .local v0, "trackPacket":Lcom/miui/server/sentinel/TrackPacket; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 275 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
/* const/16 v2, 0x40 */
/* new-array v3, v2, [B */
/* .line 277 */
/* .local v3, "splitbuffer":[B */
int v4 = 4; // const/4 v4, 0x4
int v5 = 0; // const/4 v5, 0x0
try { // :try_start_0
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v4 ); // invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 278 */
v6 = com.miui.server.sentinel.SocketPacket .readInt ( v3 );
(( com.miui.server.sentinel.TrackPacket ) v0 ).setReport_id ( v6 ); // invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/TrackPacket;->setReport_id(I)V
/* .line 279 */
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v4 ); // invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 280 */
v6 = com.miui.server.sentinel.SocketPacket .readInt ( v3 );
(( com.miui.server.sentinel.TrackPacket ) v0 ).setReport_argsz ( v6 ); // invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/TrackPacket;->setReport_argsz(I)V
/* .line 281 */
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v2 ); // invoke-virtual {p1, v3, v5, v2}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 282 */
com.miui.server.sentinel.SocketPacket .readString ( v3,v5,v2 );
(( com.miui.server.sentinel.TrackPacket ) v0 ).setProcess_name ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setProcess_name(Ljava/lang/String;)V
/* .line 283 */
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v4 ); // invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 284 */
v2 = com.miui.server.sentinel.SocketPacket .readInt ( v3 );
(( com.miui.server.sentinel.TrackPacket ) v0 ).setPid ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setPid(I)V
/* .line 285 */
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v4 ); // invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 286 */
v2 = com.miui.server.sentinel.SocketPacket .readInt ( v3 );
(( com.miui.server.sentinel.TrackPacket ) v0 ).setTid ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setTid(I)V
/* .line 287 */
/* const/16 v2, 0xc */
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v2 ); // invoke-virtual {p1, v3, v5, v2}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 288 */
v2 = com.miui.server.sentinel.SocketPacket .readInt ( v3 );
/* int-to-long v6, v2 */
(( com.miui.server.sentinel.TrackPacket ) v0 ).setTimestamp ( v6, v7 ); // invoke-virtual {v0, v6, v7}, Lcom/miui/server/sentinel/TrackPacket;->setTimestamp(J)V
/* .line 289 */
/* const/16 v2, 0x8 */
(( java.io.BufferedInputStream ) p1 ).read ( v3, v5, v2 ); // invoke-virtual {p1, v3, v5, v2}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 290 */
v2 = com.miui.server.sentinel.SocketPacket .readInt ( v3 );
(( com.miui.server.sentinel.TrackPacket ) v0 ).setReport_sz ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setReport_sz(I)V
/* .line 291 */
final String v2 = "MiuiSentinelService"; // const-string v2, "MiuiSentinelService"
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v6, "track-heap report_sz:" */
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( com.miui.server.sentinel.TrackPacket ) v0 ).getReport_sz ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/TrackPacket;->getReport_sz()I
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v2,v4 );
/* .line 292 */
v2 = (( com.miui.server.sentinel.TrackPacket ) v0 ).getReport_sz ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/TrackPacket;->getReport_sz()I
/* .line 293 */
/* .local v2, "chunkSize":I */
/* const/16 v4, 0x2328 */
/* new-array v6, v4, [B */
/* .line 294 */
/* .local v6, "databuff":[B */
int v7 = 0; // const/4 v7, 0x0
/* .line 296 */
/* .local v7, "totalRead":I */
} // :goto_0
/* if-ge v7, v2, :cond_1 */
/* sub-int v8, v2, v7 */
/* array-length v9, v6 */
/* .line 297 */
v8 = java.lang.Math .min ( v8,v9 );
/* .line 296 */
v8 = (( java.io.BufferedInputStream ) p1 ).read ( v6, v5, v8 ); // invoke-virtual {p1, v6, v5, v8}, Ljava/io/BufferedInputStream;->read([BII)I
/* move v9, v8 */
/* .local v9, "readsize":I */
int v10 = -1; // const/4 v10, -0x1
/* if-eq v8, v10, :cond_1 */
/* .line 298 */
/* if-ne v9, v4, :cond_0 */
/* .line 299 */
com.miui.server.sentinel.SocketPacket .readString ( v6,v5,v4 );
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 301 */
} // :cond_0
com.miui.server.sentinel.SocketPacket .readString ( v6,v5,v9 );
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 303 */
} // :goto_1
/* add-int/2addr v7, v9 */
/* .line 305 */
} // .end local v9 # "readsize":I
} // :cond_1
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( com.miui.server.sentinel.TrackPacket ) v0 ).setData ( v4 ); // invoke-virtual {v0, v4}, Lcom/miui/server/sentinel/TrackPacket;->setData(Ljava/lang/String;)V
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
} // .end local v2 # "chunkSize":I
} // .end local v6 # "databuff":[B
} // .end local v7 # "totalRead":I
/* .line 308 */
/* :catch_0 */
/* move-exception v2 */
/* .line 309 */
/* .local v2, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
/* .line 306 */
} // .end local v2 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v2 */
/* .line 307 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 310 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_2
/* nop */
/* .line 311 */
} // :goto_3
} // .end method
private void recvMessage ( java.io.FileDescriptor p0 ) {
/* .locals 6 */
/* .param p1, "fd" # Ljava/io/FileDescriptor; */
/* .line 214 */
/* sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelService;->DEBUG:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 215 */
final String v0 = "MiuiSentinelService"; // const-string v0, "MiuiSentinelService"
final String v1 = "begin recv message"; // const-string v1, "begin recv message"
android.util.Slog .e ( v0,v1 );
/* .line 217 */
} // :cond_0
/* const/16 v0, 0x8 */
/* new-array v0, v0, [B */
/* .line 218 */
/* .local v0, "splitbuffer":[B */
try { // :try_start_0
/* new-instance v1, Ljava/io/FileInputStream; */
/* invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 219 */
/* .local v1, "fi":Ljava/io/FileInputStream; */
try { // :try_start_1
/* new-instance v2, Ljava/io/BufferedInputStream; */
/* const v3, 0x3e8000 */
/* invoke-direct {v2, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_2 */
/* .line 220 */
/* .local v2, "bufferedInputStream":Ljava/io/BufferedInputStream; */
int v3 = 0; // const/4 v3, 0x0
int v4 = 4; // const/4 v4, 0x4
try { // :try_start_2
(( java.io.BufferedInputStream ) v2 ).read ( v0, v3, v4 ); // invoke-virtual {v2, v0, v3, v4}, Ljava/io/BufferedInputStream;->read([BII)I
/* .line 221 */
v3 = com.miui.server.sentinel.SocketPacket .readInt ( v0 );
/* .line 222 */
/* .local v3, "type":I */
/* const/16 v4, 0xa */
/* if-le v3, v4, :cond_1 */
/* .line 223 */
/* invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelService;->parseSocketPacket(Ljava/io/BufferedInputStream;)Lcom/miui/server/sentinel/SocketPacket; */
/* .line 224 */
/* .local v4, "socketPacket":Lcom/miui/server/sentinel/SocketPacket; */
com.miui.server.sentinel.MiuiSentinelMemoryManager .getInstance ( );
v5 = (( com.miui.server.sentinel.MiuiSentinelMemoryManager ) v5 ).filterMessages ( v4 ); // invoke-virtual {v5, v4}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->filterMessages(Lcom/miui/server/sentinel/SocketPacket;)Z
if ( v5 != null) { // if-eqz v5, :cond_2
/* .line 225 */
/* invoke-direct {p0, v4}, Lcom/miui/server/sentinel/MiuiSentinelService;->handlerTrigger(Lcom/miui/server/sentinel/SocketPacket;)V */
/* .line 227 */
} // .end local v4 # "socketPacket":Lcom/miui/server/sentinel/SocketPacket;
} // :cond_1
int v4 = 6; // const/4 v4, 0x6
/* if-ne v3, v4, :cond_2 */
/* .line 228 */
/* invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelService;->parseTrackPacket(Ljava/io/BufferedInputStream;)Lcom/miui/server/sentinel/TrackPacket; */
/* .line 229 */
/* .local v4, "trackPacket":Lcom/miui/server/sentinel/TrackPacket; */
/* invoke-direct {p0, v4}, Lcom/miui/server/sentinel/MiuiSentinelService;->handlerTrackMessage(Lcom/miui/server/sentinel/TrackPacket;)V */
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 227 */
} // .end local v4 # "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
} // :cond_2
} // :goto_0
/* nop */
/* .line 231 */
} // .end local v3 # "type":I
} // :goto_1
try { // :try_start_3
(( java.io.BufferedInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_2 */
} // .end local v2 # "bufferedInputStream":Ljava/io/BufferedInputStream;
try { // :try_start_4
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 218 */
/* .restart local v2 # "bufferedInputStream":Ljava/io/BufferedInputStream; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_5
(( java.io.BufferedInputStream ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
/* :try_end_5 */
/* .catchall {:try_start_5 ..:try_end_5} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_6
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "splitbuffer":[B
} // .end local v1 # "fi":Ljava/io/FileInputStream;
} // .end local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelService;
} // .end local p1 # "fd":Ljava/io/FileDescriptor;
} // :goto_2
/* throw v3 */
/* :try_end_6 */
/* .catchall {:try_start_6 ..:try_end_6} :catchall_2 */
} // .end local v2 # "bufferedInputStream":Ljava/io/BufferedInputStream;
/* .restart local v0 # "splitbuffer":[B */
/* .restart local v1 # "fi":Ljava/io/FileInputStream; */
/* .restart local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelService; */
/* .restart local p1 # "fd":Ljava/io/FileDescriptor; */
/* :catchall_2 */
/* move-exception v2 */
try { // :try_start_7
(( java.io.FileInputStream ) v1 ).close ( ); // invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
/* :try_end_7 */
/* .catchall {:try_start_7 ..:try_end_7} :catchall_3 */
/* :catchall_3 */
/* move-exception v3 */
try { // :try_start_8
(( java.lang.Throwable ) v2 ).addSuppressed ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "splitbuffer":[B
} // .end local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelService;
} // .end local p1 # "fd":Ljava/io/FileDescriptor;
} // :goto_3
/* throw v2 */
/* :try_end_8 */
/* .catch Ljava/io/IOException; {:try_start_8 ..:try_end_8} :catch_1 */
/* .catch Ljava/lang/Exception; {:try_start_8 ..:try_end_8} :catch_0 */
/* .line 233 */
} // .end local v1 # "fi":Ljava/io/FileInputStream;
/* .restart local v0 # "splitbuffer":[B */
/* .restart local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelService; */
/* .restart local p1 # "fd":Ljava/io/FileDescriptor; */
/* :catch_0 */
/* move-exception v1 */
/* .line 234 */
/* .local v1, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 231 */
} // .end local v1 # "e":Ljava/lang/Exception;
/* :catch_1 */
/* move-exception v1 */
/* .line 232 */
/* .local v1, "e":Ljava/io/IOException; */
(( java.io.IOException ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
/* .line 235 */
} // .end local v1 # "e":Ljava/io/IOException;
} // :goto_4
/* nop */
/* .line 236 */
} // :goto_5
return;
} // .end method
