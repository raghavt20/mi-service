.class public Lcom/miui/server/sentinel/RssUsageInfo;
.super Lcom/miui/server/sentinel/ProcUsageInfo;
.source "RssUsageInfo.java"


# instance fields
.field private maxIncrease:Ljava/lang/String;

.field private rssSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/miui/server/sentinel/ProcUsageInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxIncrease()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/miui/server/sentinel/RssUsageInfo;->maxIncrease:Ljava/lang/String;

    return-object v0
.end method

.method public getRssSize()J
    .locals 2

    .line 11
    iget-wide v0, p0, Lcom/miui/server/sentinel/RssUsageInfo;->rssSize:J

    return-wide v0
.end method

.method public setMaxIncrease(Ljava/lang/String;)V
    .locals 0
    .param p1, "maxIncrease"    # Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/miui/server/sentinel/RssUsageInfo;->maxIncrease:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setRssSize(J)V
    .locals 0
    .param p1, "rssSize"    # J

    .line 15
    iput-wide p1, p0, Lcom/miui/server/sentinel/RssUsageInfo;->rssSize:J

    .line 16
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Proc info Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Pid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Rss Size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/miui/server/sentinel/RssUsageInfo;->rssSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " THE MAX INREASE is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/server/sentinel/RssUsageInfo;->maxIncrease:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
