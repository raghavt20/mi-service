public class com.miui.server.sentinel.TrackPacket extends com.miui.server.sentinel.SocketPacket {
	 /* .source "TrackPacket.java" */
	 /* # instance fields */
	 Integer report_argsz;
	 Integer report_id;
	 Integer report_sz;
	 Long timestamp;
	 /* # direct methods */
	 public com.miui.server.sentinel.TrackPacket ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Lcom/miui/server/sentinel/SocketPacket;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public Integer getReport_argsz ( ) {
		 /* .locals 1 */
		 /* .line 13 */
		 /* iget v0, p0, Lcom/miui/server/sentinel/TrackPacket;->report_argsz:I */
	 } // .end method
	 public Integer getReport_id ( ) {
		 /* .locals 1 */
		 /* .line 9 */
		 /* iget v0, p0, Lcom/miui/server/sentinel/TrackPacket;->report_id:I */
	 } // .end method
	 public Integer getReport_sz ( ) {
		 /* .locals 1 */
		 /* .line 21 */
		 /* iget v0, p0, Lcom/miui/server/sentinel/TrackPacket;->report_sz:I */
	 } // .end method
	 public Long getTimestamp ( ) {
		 /* .locals 2 */
		 /* .line 17 */
		 /* iget-wide v0, p0, Lcom/miui/server/sentinel/TrackPacket;->timestamp:J */
		 /* return-wide v0 */
	 } // .end method
	 public void setReport_argsz ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "report_argsz" # I */
		 /* .line 15 */
		 /* iput p1, p0, Lcom/miui/server/sentinel/TrackPacket;->report_argsz:I */
		 return;
	 } // .end method
	 public void setReport_id ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "report_id" # I */
		 /* .line 11 */
		 /* iput p1, p0, Lcom/miui/server/sentinel/TrackPacket;->report_id:I */
		 return;
	 } // .end method
	 public void setReport_sz ( Integer p0 ) {
		 /* .locals 0 */
		 /* .param p1, "report_sz" # I */
		 /* .line 23 */
		 /* iput p1, p0, Lcom/miui/server/sentinel/TrackPacket;->report_sz:I */
		 return;
	 } // .end method
	 public void setTimestamp ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "timestamp" # J */
		 /* .line 19 */
		 /* iput-wide p1, p0, Lcom/miui/server/sentinel/TrackPacket;->timestamp:J */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 3 */
		 /* .line 27 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 28 */
		 /* .local v0, "sb":Ljava/lang/StringBuilder; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Track info Proc info Name: "; // const-string v2, "Track info Proc info Name: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( com.miui.server.sentinel.TrackPacket ) p0 ).getProcess_name ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = "Pid = "; // const-string v2, "Pid = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 29 */
		 v2 = 		 (( com.miui.server.sentinel.TrackPacket ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = "data = "; // const-string v2, "data = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 30 */
		 (( com.miui.server.sentinel.TrackPacket ) p0 ).getData ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/TrackPacket;->getData()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 /* .line 28 */
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 31 */
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
