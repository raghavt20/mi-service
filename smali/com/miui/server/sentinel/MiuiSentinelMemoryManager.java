public class com.miui.server.sentinel.MiuiSentinelMemoryManager {
	 /* .source "MiuiSentinelMemoryManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;, */
	 /* Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action; */
	 /* } */
} // .end annotation
/* # static fields */
public static final Boolean DEBUG;
private static final Integer DETAILS_LIMIT;
private static final java.util.HashSet DIALOG_APP_LIST;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/HashSet<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
public static final Boolean ENABLE_MQS_REPORT;
public static final Boolean ENABLE_SENTINEL_MEMORY_MONITOR;
private static final Integer END_TRACK;
private static final Integer END_TRACK_SIGNAL;
private static final java.lang.String HANDLER_NAME;
private static final Integer MAX_FD_AMOUNT;
private static final Integer MAX_THREAD_AMOUNT;
private static final java.lang.String MEMLEAK_DIR;
private static final Integer REPORT_FD_AMOUNT_LEAKTOMQS;
private static final Integer REPORT_JAVAHEAP_LEAKTOMQS;
private static final Integer REPORT_NATIVEHEAP_LEAKTOMQS;
private static final Integer REPORT_THREAD_AMOUNT_LEAKTOMQS;
private static final Integer RESUME_LEAK;
private static final Integer START_FD_TRACK;
private static final Integer START_THREAD_TRACK;
private static final Integer START_TRACK;
private static final Integer START_TRACK_SIGNAL;
private static final java.lang.String SYSPROP_ENABLE_MQS_REPORT;
private static final java.lang.String SYSPROP_ENABLE_RESUME_STRATEGY;
private static final java.lang.String SYSPROP_ENABLE_SENTINEL_MEMORY_MONITOR;
private static final java.lang.String SYSPROP_ENABLE_TRACK_MALLOC;
private static final java.lang.String SYSTEM_SERVER;
private static final Integer SYSTEM_SERVER_MAX_JAVAHEAP;
private static final Integer SYSTEM_SERVER_MAX_NATIVEHEAP;
private static final java.lang.String TAG;
private static final Integer TOTAL_RSS_LIMIT;
private static com.miui.server.sentinel.MiuiSentinelMemoryManager miuiSentinelMemoryManager;
/* # instance fields */
private java.util.concurrent.ConcurrentHashMap eventList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ConcurrentHashMap highCapacityRssList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private android.content.Context mContext;
private android.os.HandlerThread mMiuiSentineThread;
private volatile com.miui.server.sentinel.MiuiSentinelMemoryManager$MiuiSentineHandler mSentineHandler;
private com.android.server.am.ActivityManagerService mService;
private java.util.concurrent.ConcurrentHashMap trackEventList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Ljava/lang/Integer;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
private java.util.concurrent.ConcurrentHashMap trackList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/sentinel/NativeHeapUsageInfo;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
/* # direct methods */
static void -$$Nest$mresumeMemLeak ( com.miui.server.sentinel.MiuiSentinelMemoryManager p0, java.lang.Object p1 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(Ljava/lang/Object;)V */
return;
} // .end method
static com.miui.server.sentinel.MiuiSentinelMemoryManager ( ) {
/* .locals 2 */
/* .line 85 */
final String v0 = "debug.sys.mss"; // const-string v0, "debug.sys.mss"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.sentinel.MiuiSentinelMemoryManager.DEBUG = (v0!= 0);
/* .line 86 */
/* nop */
/* .line 87 */
final String v0 = "persist.sys.debug.enable_sentinel_memory_monitor"; // const-string v0, "persist.sys.debug.enable_sentinel_memory_monitor"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.sentinel.MiuiSentinelMemoryManager.ENABLE_SENTINEL_MEMORY_MONITOR = (v0!= 0);
/* .line 88 */
final String v0 = "persist.sys.debug.enable_mqs_report"; // const-string v0, "persist.sys.debug.enable_mqs_report"
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
com.miui.server.sentinel.MiuiSentinelMemoryManager.ENABLE_MQS_REPORT = (v0!= 0);
/* .line 99 */
/* new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$1; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$1;-><init>()V */
return;
} // .end method
private com.miui.server.sentinel.MiuiSentinelMemoryManager ( ) {
/* .locals 2 */
/* .line 167 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 89 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.eventList = v0;
/* .line 90 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.trackList = v0;
/* .line 91 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.trackEventList = v0;
/* .line 92 */
/* new-instance v0, Ljava/util/concurrent/ConcurrentHashMap; */
/* invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V */
this.highCapacityRssList = v0;
/* .line 168 */
/* new-instance v0, Landroid/os/HandlerThread; */
/* const-string/jumbo v1, "sentineMemoryWork" */
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mMiuiSentineThread = v0;
/* .line 169 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 170 */
/* new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler; */
v1 = this.mMiuiSentineThread;
(( android.os.HandlerThread ) v1 ).getLooper ( ); // invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v0, p0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;-><init>(Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;Landroid/os/Looper;)V */
this.mSentineHandler = v0;
/* .line 171 */
final String v0 = "MiuiSentinelMemoryManager"; // const-string v0, "MiuiSentinelMemoryManager"
final String v1 = "MiuiSentinelMemoryManager init"; // const-string v1, "MiuiSentinelMemoryManager init"
android.util.Slog .d ( v0,v1 );
/* .line 172 */
return;
} // .end method
private java.lang.String getDfxDetails ( java.lang.String p0 ) {
/* .locals 2 */
/* .param p1, "details" # Ljava/lang/String; */
/* .line 333 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* const/16 v1, 0x7d0 */
/* if-le v0, v1, :cond_0 */
int v0 = 0; // const/4 v0, 0x0
(( java.lang.String ) p1 ).substring ( v0, v1 ); // invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
} // :cond_0
/* move-object v0, p1 */
} // :goto_0
} // .end method
public static com.miui.server.sentinel.MiuiSentinelMemoryManager getInstance ( ) {
/* .locals 1 */
/* .line 155 */
v0 = com.miui.server.sentinel.MiuiSentinelMemoryManager.miuiSentinelMemoryManager;
/* if-nez v0, :cond_0 */
/* .line 156 */
/* new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;-><init>()V */
/* .line 158 */
} // :cond_0
v0 = com.miui.server.sentinel.MiuiSentinelMemoryManager.miuiSentinelMemoryManager;
} // .end method
private Boolean isLibraryExist ( java.lang.String p0 ) {
/* .locals 6 */
/* .param p1, "path" # Ljava/lang/String; */
/* .line 539 */
final String v0 = "MiuiSentinelMemoryManager"; // const-string v0, "MiuiSentinelMemoryManager"
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 540 */
/* .local v1, "libraryName":Ljava/lang/StringBuilder; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "lib"; // const-string v3, "lib"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "persist.track.malloc.enable"; // const-string v3, "persist.track.malloc.enable"
android.os.SystemProperties .get ( v3 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = ".so"; // const-string v3, ".so"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 541 */
try { // :try_start_0
/* new-instance v2, Ljava/io/BufferedReader; */
/* new-instance v3, Ljava/io/FileReader; */
/* invoke-direct {v3, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V */
/* invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 542 */
/* .local v2, "reader":Ljava/io/BufferedReader; */
int v3 = 0; // const/4 v3, 0x0
/* .line 543 */
/* .local v3, "mapsInfo":Ljava/lang/String; */
} // :cond_0
try { // :try_start_1
(( java.io.BufferedReader ) v2 ).readLine ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
/* move-object v3, v4 */
if ( v4 != null) { // if-eqz v4, :cond_1
/* .line 544 */
v4 = (( java.lang.String ) v3 ).contains ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
if ( v4 != null) { // if-eqz v4, :cond_0
/* .line 545 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v4 ).append ( p1 ); // invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = " found track\'s library"; // const-string v5, " found track\'s library"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v4 );
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 546 */
/* nop */
/* .line 549 */
try { // :try_start_2
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* .line 546 */
int v0 = 1; // const/4 v0, 0x1
/* .line 549 */
} // .end local v3 # "mapsInfo":Ljava/lang/String;
} // :cond_1
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 551 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .line 541 */
/* .restart local v2 # "reader":Ljava/io/BufferedReader; */
/* :catchall_0 */
/* move-exception v3 */
try { // :try_start_3
(( java.io.BufferedReader ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v4 */
try { // :try_start_4
(( java.lang.Throwable ) v3 ).addSuppressed ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v1 # "libraryName":Ljava/lang/StringBuilder;
} // .end local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
} // .end local p1 # "path":Ljava/lang/String;
} // :goto_0
/* throw v3 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 549 */
} // .end local v2 # "reader":Ljava/io/BufferedReader;
/* .restart local v1 # "libraryName":Ljava/lang/StringBuilder; */
/* .restart local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager; */
/* .restart local p1 # "path":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 550 */
/* .local v2, "e":Ljava/io/IOException; */
(( java.io.IOException ) v2 ).printStackTrace ( ); // invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
/* .line 552 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_1
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v2 ).append ( p1 ); // invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = " not found track\'s library: "; // const-string v3, " not found track\'s library: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v2 );
/* .line 553 */
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void reportHeapLeakEvent ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.String p4, Long p5 ) {
/* .locals 4 */
/* .param p1, "type" # I */
/* .param p2, "subject" # Ljava/lang/String; */
/* .param p3, "stacktrace" # Ljava/lang/String; */
/* .param p4, "pid" # I */
/* .param p5, "procname" # Ljava/lang/String; */
/* .param p6, "growsize" # J */
/* .line 290 */
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 291 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* new-instance v1, Ljava/util/ArrayList; */
/* invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V */
/* .line 292 */
/* .local v1, "extraFile":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;" */
final String v2 = "/data/miuilog/stability/memleak/heapleak"; // const-string v2, "/data/miuilog/stability/memleak/heapleak"
/* .line 293 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setExtraFiles ( v1 ); // invoke-virtual {v0, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setExtraFiles(Ljava/util/List;)V
/* .line 294 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 295 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPid ( p4 ); // invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V
/* .line 296 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPackageName ( p5 ); // invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 297 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setProcessName ( p5 ); // invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setProcessName(Ljava/lang/String;)V
/* .line 298 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setTimeStamp ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 299 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSummary ( p2 ); // invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 300 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setDetails ( p3 ); // invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 301 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v2 ).reportGeneralException ( v0 ); // invoke-virtual {v2, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 302 */
/* invoke-direct {p0, v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportMemLeakMisight(Lmiui/mqsas/sdk/event/ExceptionEvent;)V */
/* .line 303 */
return;
} // .end method
private void reportLeakEvent ( Integer p0, java.lang.String p1, java.lang.String p2, Integer p3, java.lang.String p4 ) {
/* .locals 3 */
/* .param p1, "type" # I */
/* .param p2, "subject" # Ljava/lang/String; */
/* .param p3, "stacktrace" # Ljava/lang/String; */
/* .param p4, "pid" # I */
/* .param p5, "procname" # Ljava/lang/String; */
/* .line 337 */
/* new-instance v0, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 338 */
/* .local v0, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setType ( p1 ); // invoke-virtual {v0, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 339 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPid ( p4 ); // invoke-virtual {v0, p4}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V
/* .line 340 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setPackageName ( p5 ); // invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 341 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setProcessName ( p5 ); // invoke-virtual {v0, p5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setProcessName(Ljava/lang/String;)V
/* .line 342 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v1 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setTimeStamp ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 343 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setSummary ( p2 ); // invoke-virtual {v0, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 344 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v0 ).setDetails ( p3 ); // invoke-virtual {v0, p3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 345 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v1 ).reportGeneralException ( v0 ); // invoke-virtual {v1, v0}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 346 */
return;
} // .end method
private void reportMemLeakMisight ( miui.mqsas.sdk.event.ExceptionEvent p0 ) {
/* .locals 5 */
/* .param p1, "event" # Lmiui/mqsas/sdk/event/ExceptionEvent; */
/* .line 306 */
/* if-nez p1, :cond_0 */
/* .line 307 */
return;
/* .line 310 */
} // :cond_0
v0 = (( miui.mqsas.sdk.event.ExceptionEvent ) p1 ).getType ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getType()I
final String v1 = "Stacktrace"; // const-string v1, "Stacktrace"
/* packed-switch v0, :pswitch_data_0 */
/* .line 324 */
return;
/* .line 320 */
/* :pswitch_0 */
/* new-instance v0, Lcom/miui/misight/MiEvent; */
/* const v2, 0x35b43bad */
/* invoke-direct {v0, v2}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 321 */
/* .local v0, "miEvent":Lcom/miui/misight/MiEvent; */
(( miui.mqsas.sdk.event.ExceptionEvent ) p1 ).getDetails ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getDfxDetails(Ljava/lang/String;)Ljava/lang/String; */
(( com.miui.misight.MiEvent ) v0 ).addStr ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 322 */
/* .line 316 */
} // .end local v0 # "miEvent":Lcom/miui/misight/MiEvent;
/* :pswitch_1 */
/* new-instance v0, Lcom/miui/misight/MiEvent; */
/* const v2, 0x35b43bac */
/* invoke-direct {v0, v2}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 317 */
/* .restart local v0 # "miEvent":Lcom/miui/misight/MiEvent; */
(( miui.mqsas.sdk.event.ExceptionEvent ) p1 ).getDetails ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;
/* invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getDfxDetails(Ljava/lang/String;)Ljava/lang/String; */
(( com.miui.misight.MiEvent ) v0 ).addStr ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 318 */
/* .line 312 */
} // .end local v0 # "miEvent":Lcom/miui/misight/MiEvent;
/* :pswitch_2 */
/* new-instance v0, Lcom/miui/misight/MiEvent; */
/* const v1, 0x35b43bab */
/* invoke-direct {v0, v1}, Lcom/miui/misight/MiEvent;-><init>(I)V */
/* .line 313 */
/* .restart local v0 # "miEvent":Lcom/miui/misight/MiEvent; */
(( miui.mqsas.sdk.event.ExceptionEvent ) p1 ).getDetails ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getDetails()Ljava/lang/String;
/* invoke-direct {p0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getDfxDetails(Ljava/lang/String;)Ljava/lang/String; */
final String v2 = "RssInfo"; // const-string v2, "RssInfo"
(( com.miui.misight.MiEvent ) v0 ).addStr ( v2, v1 ); // invoke-virtual {v0, v2, v1}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 314 */
/* nop */
/* .line 326 */
} // :goto_0
final String v1 = "Summary"; // const-string v1, "Summary"
(( miui.mqsas.sdk.event.ExceptionEvent ) p1 ).getSummary ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getSummary()Ljava/lang/String;
(( com.miui.misight.MiEvent ) v0 ).addStr ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 327 */
(( miui.mqsas.sdk.event.ExceptionEvent ) p1 ).getPackageName ( ); // invoke-virtual {p1}, Lmiui/mqsas/sdk/event/ExceptionEvent;->getPackageName()Ljava/lang/String;
final String v3 = "PackageName"; // const-string v3, "PackageName"
(( com.miui.misight.MiEvent ) v1 ).addStr ( v3, v2 ); // invoke-virtual {v1, v3, v2}, Lcom/miui/misight/MiEvent;->addStr(Ljava/lang/String;Ljava/lang/String;)Lcom/miui/misight/MiEvent;
/* .line 328 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
final String v4 = "CurrentTime"; // const-string v4, "CurrentTime"
(( com.miui.misight.MiEvent ) v1 ).addLong ( v4, v2, v3 ); // invoke-virtual {v1, v4, v2, v3}, Lcom/miui/misight/MiEvent;->addLong(Ljava/lang/String;J)Lcom/miui/misight/MiEvent;
/* .line 329 */
com.miui.misight.MiSight .sendEvent ( v0 );
/* .line 330 */
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1a0 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
private void reportRssLeakEvent ( Integer p0, java.lang.String p1, com.miui.server.sentinel.RssUsageInfo p2 ) {
/* .locals 6 */
/* .param p1, "type" # I */
/* .param p2, "subject" # Ljava/lang/String; */
/* .param p3, "rssUsageInfo" # Lcom/miui/server/sentinel/RssUsageInfo; */
/* .line 264 */
v0 = (( com.miui.server.sentinel.RssUsageInfo ) p3 ).getPid ( ); // invoke-virtual {p3}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I
/* .line 265 */
/* .local v0, "pid":I */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v0 );
/* .line 266 */
/* .local v1, "packageName":Ljava/lang/String; */
/* if-nez v1, :cond_0 */
/* .line 267 */
com.miui.server.sentinel.MiuiSentinelUtils .getProcessCmdline ( v0 );
/* .line 269 */
} // :cond_0
/* const-string/jumbo v2, "unknown" */
v2 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 271 */
final String v2 = "MiuiSentinelMemoryManager"; // const-string v2, "MiuiSentinelMemoryManager"
final String v3 = "The current process may exit and ignore report mqs"; // const-string v3, "The current process may exit and ignore report mqs"
android.util.Slog .d ( v2,v3 );
/* .line 272 */
return;
/* .line 274 */
} // :cond_1
final String v2 = "RSS_LEAK"; // const-string v2, "RSS_LEAK"
com.miui.server.sentinel.MiuiSentinelUtils .getExceptionPath ( v0,v1,v2 );
/* .line 275 */
/* .local v2, "filename":Ljava/io/File; */
(( com.miui.server.sentinel.RssUsageInfo ) p3 ).getRssSize ( ); // invoke-virtual {p3}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J
/* move-result-wide v3 */
com.miui.server.sentinel.MiuiSentinelUtils .dumpRssInfo ( v0,v3,v4,v1,v2 );
/* .line 276 */
/* new-instance v3, Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
/* invoke-direct {v3}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;-><init>()V */
/* .line 277 */
/* .local v3, "event":Lmiui/mqsas/sdk/event/GeneralExceptionEvent; */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setType ( p1 ); // invoke-virtual {v3, p1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setType(I)V
/* .line 278 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setPid ( v0 ); // invoke-virtual {v3, v0}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPid(I)V
/* .line 279 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setPackageName ( v1 ); // invoke-virtual {v3, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setPackageName(Ljava/lang/String;)V
/* .line 280 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setProcessName ( v1 ); // invoke-virtual {v3, v1}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setProcessName(Ljava/lang/String;)V
/* .line 281 */
java.lang.System .currentTimeMillis ( );
/* move-result-wide v4 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setTimeStamp ( v4, v5 ); // invoke-virtual {v3, v4, v5}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setTimeStamp(J)V
/* .line 282 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setSummary ( p2 ); // invoke-virtual {v3, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setSummary(Ljava/lang/String;)V
/* .line 283 */
(( miui.mqsas.sdk.event.GeneralExceptionEvent ) v3 ).setDetails ( p2 ); // invoke-virtual {v3, p2}, Lmiui/mqsas/sdk/event/GeneralExceptionEvent;->setDetails(Ljava/lang/String;)V
/* .line 284 */
/* invoke-direct {p0, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportMemLeakMisight(Lmiui/mqsas/sdk/event/ExceptionEvent;)V */
/* .line 285 */
miui.mqsas.sdk.MQSEventManagerDelegate .getInstance ( );
(( miui.mqsas.sdk.MQSEventManagerDelegate ) v4 ).reportGeneralException ( v3 ); // invoke-virtual {v4, v3}, Lmiui/mqsas/sdk/MQSEventManagerDelegate;->reportGeneralException(Lmiui/mqsas/sdk/event/GeneralExceptionEvent;)Z
/* .line 286 */
return;
} // .end method
private void resumeMemLeak ( Integer p0, Integer p1, Long p2, java.lang.String p3, java.lang.String p4, java.lang.String p5 ) {
/* .locals 16 */
/* .param p1, "pid" # I */
/* .param p2, "adj" # I */
/* .param p3, "size" # J */
/* .param p5, "name" # Ljava/lang/String; */
/* .param p6, "reason" # Ljava/lang/String; */
/* .param p7, "type" # Ljava/lang/String; */
/* .line 434 */
/* move/from16 v0, p1 */
/* move/from16 v1, p2 */
/* move-wide/from16 v2, p3 */
/* move-object/from16 v4, p5 */
/* move-object/from16 v5, p6 */
v6 = com.miui.server.sentinel.MiuiSentinelUtils .isLaboratoryTest ( );
final String v7 = "kB"; // const-string v7, "kB"
final String v8 = ") used"; // const-string v8, ") used"
final String v9 = "("; // const-string v9, "("
if ( v6 != null) { // if-eqz v6, :cond_6
/* .line 435 */
final String v6 = "MiuiSentinelMemoryManager"; // const-string v6, "MiuiSentinelMemoryManager"
/* if-lez v0, :cond_5 */
v10 = /* invoke-static/range {p1 ..p1}, Landroid/os/Process;->getThreadGroupLeader(I)I */
/* if-ne v10, v0, :cond_5 */
/* .line 436 */
final String v10 = "kB "; // const-string v10, "kB "
/* const/16 v11, -0x384 */
/* if-ne v1, v11, :cond_1 */
v12 = android.os.Process .myPid ( );
/* if-eq v0, v12, :cond_0 */
/* move-object/from16 v12, p7 */
/* .line 437 */
} // :cond_0
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "system_server(" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ") use "; // const-string v8, ") use "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* move-object/from16 v12, p7 */
(( java.lang.StringBuilder ) v7 ).append ( v12 ); // invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2, v3 ); // invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v8 = "kb too many occurring"; // const-string v8, "kb too many occurring"
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v7 );
/* .line 439 */
/* new-instance v6, Ljava/lang/RuntimeException; */
/* new-instance v7, Ljava/lang/StringBuilder; */
/* invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V */
/* const-string/jumbo v8, "system_server (" */
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v0 ); // invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v8 = ") used "; // const-string v8, ") used "
(( java.lang.StringBuilder ) v7 ).append ( v8 ); // invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v2, v3 ); // invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v10 ); // invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).append ( v5 ); // invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v7 ).toString ( ); // invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V */
/* throw v6 */
/* .line 436 */
} // :cond_1
/* move-object/from16 v12, p7 */
/* .line 440 */
} // :goto_0
/* move-object/from16 v13, p0 */
v14 = this.mService;
if ( v14 != null) { // if-eqz v14, :cond_4
/* if-le v1, v11, :cond_4 */
/* const/16 v11, 0x3e9 */
/* if-ge v1, v11, :cond_4 */
/* .line 441 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord; */
/* .line 442 */
/* .local v10, "app":Lcom/android/server/am/ProcessRecord; */
/* new-instance v11, Ljava/lang/StringBuilder; */
/* invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v11 ).append ( v4 ); // invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v9 ); // invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v0 ); // invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v8 ); // invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v2, v3 ); // invoke-virtual {v11, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v7 ); // invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).append ( v5 ); // invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v11 ).toString ( ); // invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 443 */
/* .local v11, "appReason":Ljava/lang/String; */
int v14 = 0; // const/4 v14, 0x0
/* .line 444 */
/* .local v14, "killAction":Z */
com.android.server.am.ScoutMemoryError .getInstance ( );
v15 = (( com.android.server.am.ScoutMemoryError ) v15 ).scheduleCrashApp ( v10, v11 ); // invoke-virtual {v15, v10, v11}, Lcom/android/server/am/ScoutMemoryError;->scheduleCrashApp(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
if ( v15 != null) { // if-eqz v15, :cond_2
/* .line 445 */
int v14 = 1; // const/4 v14, 0x1
/* .line 447 */
} // :cond_2
/* if-nez v14, :cond_3 */
/* .line 448 */
/* new-instance v15, Ljava/lang/StringBuilder; */
/* invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Kill"; // const-string v1, "Kill"
(( java.lang.StringBuilder ) v15 ).append ( v1 ); // invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 449 */
/* invoke-static/range {p1 ..p1}, Landroid/os/Process;->killProcess(I)V */
/* .line 451 */
} // :cond_3
android.util.Slog .e ( v6,v11 );
/* .line 452 */
} // .end local v10 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v11 # "appReason":Ljava/lang/String;
} // .end local v14 # "killAction":Z
/* goto/16 :goto_1 */
/* .line 453 */
} // :cond_4
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Kill "; // const-string v7, "Kill "
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = "), Used "; // const-string v7, "), Used "
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v10 ); // invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v5 ); // invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v1 );
/* .line 455 */
/* invoke-static/range {p1 ..p1}, Landroid/os/Process;->killProcess(I)V */
/* .line 435 */
} // :cond_5
/* move-object/from16 v13, p0 */
/* move-object/from16 v12, p7 */
/* .line 458 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v1 ).append ( v4 ); // invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v9 ); // invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).append ( v0 ); // invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ") is invalid"; // const-string v7, ") is invalid"
(( java.lang.StringBuilder ) v1 ).append ( v7 ); // invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v6,v1 );
/* .line 461 */
} // :cond_6
/* move-object/from16 v13, p0 */
/* move-object/from16 v12, p7 */
v1 = com.miui.server.sentinel.MiuiSentinelMemoryManager.DIALOG_APP_LIST;
v1 = (( java.util.HashSet ) v1 ).contains ( v4 ); // invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_7
/* .line 462 */
/* invoke-static/range {p1 ..p1}, Lcom/android/server/am/ProcessUtils;->getProcessRecordByPid(I)Lcom/android/server/am/ProcessRecord; */
/* .line 463 */
/* .local v1, "app":Lcom/android/server/am/ProcessRecord; */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v9 ); // invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v0 ); // invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v2, v3 ); // invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v5 ); // invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 464 */
/* .local v6, "appReason":Ljava/lang/String; */
com.android.server.am.ScoutMemoryError .getInstance ( );
(( com.android.server.am.ScoutMemoryError ) v7 ).showAppMemoryErrorDialog ( v1, v6 ); // invoke-virtual {v7, v1, v6}, Lcom/android/server/am/ScoutMemoryError;->showAppMemoryErrorDialog(Lcom/android/server/am/ProcessRecord;Ljava/lang/String;)Z
/* .line 467 */
} // .end local v1 # "app":Lcom/android/server/am/ProcessRecord;
} // .end local v6 # "appReason":Ljava/lang/String;
} // :cond_7
} // :goto_1
return;
} // .end method
private void resumeMemLeak ( java.lang.Object p0 ) {
/* .locals 11 */
/* .param p1, "object" # Ljava/lang/Object; */
/* .line 404 */
final String v0 = "persist.sentinel.resume.enable"; // const-string v0, "persist.sentinel.resume.enable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
/* if-nez v0, :cond_0 */
/* .line 405 */
final String v0 = "MiuiSentinelMemoryManager"; // const-string v0, "MiuiSentinelMemoryManager"
final String v1 = "no enable resume tactics \uff01"; // const-string v1, "no enable resume tactics \uff01"
android.util.Slog .e ( v0,v1 );
/* .line 406 */
return;
/* .line 409 */
} // :cond_0
/* instance-of v0, p1, Lcom/miui/server/sentinel/RssUsageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 410 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/miui/server/sentinel/RssUsageInfo; */
/* .line 411 */
/* .local v0, "rssUsageInfo":Lcom/miui/server/sentinel/RssUsageInfo; */
final String v9 = "RSS is too large, leak occurred"; // const-string v9, "RSS is too large, leak occurred"
/* .line 412 */
/* .local v9, "reason":Ljava/lang/String; */
v1 = (( com.miui.server.sentinel.RssUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I
v10 = com.android.server.am.ProcessUtils .getCurAdjByPid ( v1 );
/* .line 413 */
/* .local v10, "adj":I */
v2 = (( com.miui.server.sentinel.RssUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getRssSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J
/* move-result-wide v4 */
/* .line 414 */
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getMaxIncrease ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getMaxIncrease()Ljava/lang/String;
/* .line 413 */
/* move-object v1, p0 */
/* move v3, v10 */
/* move-object v7, v9 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 415 */
} // .end local v0 # "rssUsageInfo":Lcom/miui/server/sentinel/RssUsageInfo;
} // .end local v9 # "reason":Ljava/lang/String;
} // .end local v10 # "adj":I
} // :cond_1
/* instance-of v0, p1, Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 416 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* .line 417 */
/* .local v0, "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
final String v9 = "Native Heap is too large, leak occurred"; // const-string v9, "Native Heap is too large, leak occurred"
/* .line 418 */
/* .restart local v9 # "reason":Ljava/lang/String; */
v1 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
v10 = com.android.server.am.ProcessUtils .getCurAdjByPid ( v1 );
/* .line 419 */
/* .restart local v10 # "adj":I */
v2 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getNativeHeapSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J
/* move-result-wide v4 */
/* .line 420 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
final String v8 = "NativeHeap"; // const-string v8, "NativeHeap"
/* .line 419 */
/* move-object v1, p0 */
/* move v3, v10 */
/* move-object v7, v9 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
} // .end local v0 # "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo;
} // .end local v9 # "reason":Ljava/lang/String;
} // .end local v10 # "adj":I
/* .line 421 */
} // :cond_2
/* instance-of v0, p1, Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
if ( v0 != null) { // if-eqz v0, :cond_3
/* .line 422 */
/* move-object v0, p1 */
/* check-cast v0, Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
/* .line 423 */
/* .local v0, "javaHeapUsageInfo":Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
final String v9 = "Java Heap is too large, leak occurred"; // const-string v9, "Java Heap is too large, leak occurred"
/* .line 424 */
/* .restart local v9 # "reason":Ljava/lang/String; */
v1 = (( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getPid()I
v10 = com.android.server.am.ProcessUtils .getCurAdjByPid ( v1 );
/* .line 425 */
/* .restart local v10 # "adj":I */
v2 = (( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getPid()I
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).getJavaHeapSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getJavaHeapSize()J
/* move-result-wide v4 */
/* .line 426 */
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;
final String v8 = "JavaHeap"; // const-string v8, "JavaHeap"
/* .line 425 */
/* move-object v1, p0 */
/* move v3, v10 */
/* move-object v7, v9 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->resumeMemLeak(IIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V */
/* .line 421 */
} // .end local v0 # "javaHeapUsageInfo":Lcom/miui/server/sentinel/JavaHeapUsageInfo;
} // .end local v9 # "reason":Ljava/lang/String;
} // .end local v10 # "adj":I
} // :cond_3
} // :goto_0
/* nop */
/* .line 428 */
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void enableNativeHeapTrack ( com.miui.server.sentinel.NativeHeapUsageInfo p0, Boolean p1 ) {
/* .locals 3 */
/* .param p1, "nativeHeapUsageInfo" # Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* .param p2, "isEnable" # Z */
/* .line 648 */
if ( p2 != null) { // if-eqz p2, :cond_0
/* .line 649 */
v0 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
v1 = com.miui.server.sentinel.MiuiSentinelMemoryManager$Action.START_TRACK;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).handlerTriggerTrack ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z
/* .line 650 */
final String v0 = "MiuiSentinelMemoryManager"; // const-string v0, "MiuiSentinelMemoryManager"
final String v1 = "begin stack track "; // const-string v1, "begin stack track "
android.util.Slog .d ( v0,v1 );
/* .line 651 */
int v0 = 4; // const/4 v0, 0x4
/* const-wide/32 v1, 0x493e0 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( p1, v0, v1, v2 ); // invoke-virtual {p0, p1, v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;IJ)V
/* .line 652 */
return;
/* .line 654 */
} // :cond_0
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).removeTrackEvent ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->removeTrackEvent(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)V
/* .line 655 */
v0 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
v1 = com.miui.server.sentinel.MiuiSentinelMemoryManager$Action.REPORT_TRACK;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).handlerTriggerTrack ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z
/* .line 656 */
return;
} // .end method
public Boolean filterMessages ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 9 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 176 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
/* .line 177 */
/* .local v0, "procname":Ljava/lang/String; */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getEvent_type ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;
/* .line 178 */
/* .local v1, "type":Ljava/lang/String; */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 179 */
/* .local v2, "sb":Ljava/lang/StringBuilder; */
(( java.lang.StringBuilder ) v2 ).append ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "#"; // const-string v4, "#"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v0 ); // invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 180 */
/* sget-boolean v3, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z */
if ( v3 != null) { // if-eqz v3, :cond_0
/* .line 181 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "filtermessages item:"; // const-string v4, "filtermessages item:"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v4 = "MiuiSentinelMemoryManager"; // const-string v4, "MiuiSentinelMemoryManager"
android.util.Slog .e ( v4,v3 );
/* .line 183 */
} // :cond_0
v3 = this.eventList;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).get ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
int v4 = 0; // const/4 v4, 0x0
int v5 = 1; // const/4 v5, 0x1
/* if-nez v3, :cond_1 */
/* .line 184 */
v3 = this.eventList;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
java.lang.Integer .valueOf ( v5 );
(( java.util.concurrent.ConcurrentHashMap ) v3 ).put ( v6, v5 ); // invoke-virtual {v3, v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 185 */
/* .line 186 */
} // :cond_1
v3 = this.eventList;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).get ( v6 ); // invoke-virtual {v3, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v3, Ljava/lang/Integer; */
v3 = (( java.lang.Integer ) v3 ).intValue ( ); // invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
int v6 = 3; // const/4 v6, 0x3
/* if-ge v3, v6, :cond_2 */
/* .line 187 */
v3 = this.eventList;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
v7 = this.eventList;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v7 ).get ( v8 ); // invoke-virtual {v7, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v7, Ljava/lang/Integer; */
v7 = (( java.lang.Integer ) v7 ).intValue ( ); // invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
/* add-int/2addr v7, v5 */
java.lang.Integer .valueOf ( v7 );
(( java.util.concurrent.ConcurrentHashMap ) v3 ).put ( v6, v5 ); // invoke-virtual {v3, v6, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 188 */
/* .line 190 */
} // :cond_2
} // .end method
public com.miui.server.sentinel.FdUsageInfo getFdUsageInfo ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 4 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 394 */
/* new-instance v0, Lcom/miui/server/sentinel/FdUsageInfo; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/FdUsageInfo;-><init>()V */
/* .line 395 */
/* .local v0, "info":Lcom/miui/server/sentinel/FdUsageInfo; */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
final String v2 = "#"; // const-string v2, "#"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 396 */
/* .local v1, "split":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
(( com.miui.server.sentinel.FdUsageInfo ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/FdUsageInfo;->setName(Ljava/lang/String;)V
/* .line 397 */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
(( com.miui.server.sentinel.FdUsageInfo ) v0 ).setPid ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/FdUsageInfo;->setPid(I)V
/* .line 398 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v2 */
(( com.miui.server.sentinel.FdUsageInfo ) v0 ).setFd_amount ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/FdUsageInfo;->setFd_amount(J)V
/* .line 399 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getData ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;
(( com.miui.server.sentinel.FdUsageInfo ) v0 ).setUsageInfo ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/FdUsageInfo;->setUsageInfo(Ljava/lang/String;)V
/* .line 400 */
} // .end method
public com.miui.server.sentinel.JavaHeapUsageInfo getJavaHeapinfo ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 4 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 374 */
/* new-instance v0, Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;-><init>()V */
/* .line 375 */
/* .local v0, "info":Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
final String v2 = "#"; // const-string v2, "#"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 376 */
/* .local v1, "split":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->setName(Ljava/lang/String;)V
/* .line 377 */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).setPid ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->setPid(I)V
/* .line 378 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v2 */
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).setJavaHeapSize ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->setJavaHeapSize(J)V
/* .line 379 */
} // .end method
public com.miui.server.sentinel.NativeHeapUsageInfo getNativeHeapinfo ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 4 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 364 */
/* new-instance v0, Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;-><init>()V */
/* .line 365 */
/* .local v0, "info":Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
final String v2 = "#"; // const-string v2, "#"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 366 */
/* .local v1, "split":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setName(Ljava/lang/String;)V
/* .line 367 */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).setPid ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setPid(I)V
/* .line 368 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v2 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).setNativeHeapSize ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setNativeHeapSize(J)V
/* .line 369 */
} // .end method
public com.miui.server.sentinel.RssUsageInfo getRssinfo ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 7 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 350 */
/* new-instance v0, Lcom/miui/server/sentinel/RssUsageInfo; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/RssUsageInfo;-><init>()V */
/* .line 351 */
/* .local v0, "info":Lcom/miui/server/sentinel/RssUsageInfo; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getRSSinfo: "; // const-string v2, "getRSSinfo: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSentinelMemoryManager"; // const-string v2, "MiuiSentinelMemoryManager"
android.util.Slog .e ( v2,v1 );
/* .line 352 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
final String v3 = "#"; // const-string v3, "#"
(( java.lang.String ) v1 ).split ( v3 ); // invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 353 */
/* .local v1, "split":[Ljava/lang/String; */
int v3 = 0; // const/4 v3, 0x0
/* aget-object v3, v1, v3 */
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).setName ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/sentinel/RssUsageInfo;->setName(Ljava/lang/String;)V
/* .line 354 */
int v3 = 1; // const/4 v3, 0x1
/* aget-object v3, v1, v3 */
v3 = java.lang.Integer .parseInt ( v3 );
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).setPid ( v3 ); // invoke-virtual {v0, v3}, Lcom/miui/server/sentinel/RssUsageInfo;->setPid(I)V
/* .line 355 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v3 */
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).setRssSize ( v3, v4 ); // invoke-virtual {v0, v3, v4}, Lcom/miui/server/sentinel/RssUsageInfo;->setRssSize(J)V
/* .line 356 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getData ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;
/* .line 357 */
/* .local v3, "data":Ljava/lang/String; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "cmdline: "; // const-string v5, "cmdline: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v5 = "pid: "; // const-string v5, "pid: "
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v5 = (( com.miui.server.sentinel.RssUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v5 = "RSSsize:"; // const-string v5, "RSSsize:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 358 */
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getRssSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J
/* move-result-wide v5 */
(( java.lang.StringBuilder ) v4 ).append ( v5, v6 ); // invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 357 */
android.util.Slog .d ( v2,v4 );
/* .line 359 */
} // .end method
public com.miui.server.sentinel.ThreadUsageInfo getThreadUsageInfo ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 4 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 383 */
/* new-instance v0, Lcom/miui/server/sentinel/ThreadUsageInfo; */
/* invoke-direct {v0}, Lcom/miui/server/sentinel/ThreadUsageInfo;-><init>()V */
/* .line 384 */
/* .local v0, "info":Lcom/miui/server/sentinel/ThreadUsageInfo; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "getThreadinfo: "; // const-string v2, "getThreadinfo: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSentinelMemoryManager"; // const-string v2, "MiuiSentinelMemoryManager"
android.util.Slog .e ( v2,v1 );
/* .line 385 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
final String v2 = "#"; // const-string v2, "#"
(( java.lang.String ) v1 ).split ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
/* .line 386 */
/* .local v1, "split":[Ljava/lang/String; */
int v2 = 0; // const/4 v2, 0x0
/* aget-object v2, v1, v2 */
(( com.miui.server.sentinel.ThreadUsageInfo ) v0 ).setName ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setName(Ljava/lang/String;)V
/* .line 387 */
int v2 = 1; // const/4 v2, 0x1
/* aget-object v2, v1, v2 */
v2 = java.lang.Integer .parseInt ( v2 );
(( com.miui.server.sentinel.ThreadUsageInfo ) v0 ).setPid ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setPid(I)V
/* .line 388 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v2 */
(( com.miui.server.sentinel.ThreadUsageInfo ) v0 ).setThreadAmount ( v2, v3 ); // invoke-virtual {v0, v2, v3}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setThreadAmount(J)V
/* .line 389 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getData ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;
(( com.miui.server.sentinel.ThreadUsageInfo ) v0 ).setUsageInfo ( v2 ); // invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/ThreadUsageInfo;->setUsageInfo(Ljava/lang/String;)V
/* .line 390 */
} // .end method
public java.util.concurrent.ConcurrentHashMap getTrackList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/concurrent/ConcurrentHashMap<", */
/* "Ljava/lang/String;", */
/* "Lcom/miui/server/sentinel/NativeHeapUsageInfo;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 674 */
v0 = this.trackList;
} // .end method
public Boolean handlerTriggerTrack ( Integer p0, com.miui.server.sentinel.MiuiSentinelMemoryManager$Action p1 ) {
/* .locals 5 */
/* .param p1, "pid" # I */
/* .param p2, "action" # Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action; */
/* .line 511 */
int v0 = 0; // const/4 v0, 0x0
final String v1 = "MiuiSentinelMemoryManager"; // const-string v1, "MiuiSentinelMemoryManager"
/* if-gtz p1, :cond_0 */
/* .line 512 */
final String v2 = "Failed to enable Track! pid is invalid"; // const-string v2, "Failed to enable Track! pid is invalid"
android.util.Slog .e ( v1,v2 );
/* .line 513 */
/* .line 515 */
} // :cond_0
v2 = com.miui.server.sentinel.MiuiSentinelUtils .isEnaleTrack ( );
if ( v2 != null) { // if-eqz v2, :cond_1
/* .line 516 */
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "/proc/"; // const-string v3, "/proc/"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.String .valueOf ( p1 );
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v3 = "/maps"; // const-string v3, "/maps"
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 517 */
/* .local v2, "mapsPath":Ljava/lang/String; */
v3 = /* invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->isLibraryExist(Ljava/lang/String;)Z */
if ( v3 != null) { // if-eqz v3, :cond_1
/* .line 518 */
v0 = com.miui.server.sentinel.MiuiSentinelMemoryManager$2.$SwitchMap$com$miui$server$sentinel$MiuiSentinelMemoryManager$Action;
v3 = (( com.miui.server.sentinel.MiuiSentinelMemoryManager$Action ) p2 ).ordinal ( ); // invoke-virtual {p2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->ordinal()I
/* aget v0, v0, v3 */
final String v3 = ") Track"; // const-string v3, ") Track"
/* packed-switch v0, :pswitch_data_0 */
/* .line 524 */
/* :pswitch_0 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "report pid("; // const-string v4, "report pid("
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 525 */
/* const/16 v0, 0x33 */
android.os.Process .sendSignal ( p1,v0 );
/* .line 526 */
/* .line 520 */
/* :pswitch_1 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "begin pid("; // const-string v4, "begin pid("
(( java.lang.StringBuilder ) v0 ).append ( v4 ); // invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( p1 ); // invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 521 */
/* const/16 v0, 0x32 */
android.os.Process .sendSignal ( p1,v0 );
/* .line 522 */
/* nop */
/* .line 530 */
} // :goto_0
final String v0 = "enable track malloc sucess!"; // const-string v0, "enable track malloc sucess!"
android.util.Slog .e ( v1,v0 );
/* .line 531 */
int v0 = 1; // const/4 v0, 0x1
/* .line 534 */
} // .end local v2 # "mapsPath":Ljava/lang/String;
} // :cond_1
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
public void init ( com.android.server.am.ActivityManagerService p0, android.content.Context p1 ) {
/* .locals 1 */
/* .param p1, "mService" # Lcom/android/server/am/ActivityManagerService; */
/* .param p2, "mContext" # Landroid/content/Context; */
/* .line 162 */
this.mService = p1;
/* .line 163 */
this.mContext = p2;
/* .line 164 */
com.android.server.am.ScoutMemoryError .getInstance ( );
(( com.android.server.am.ScoutMemoryError ) v0 ).init ( p1, p2 ); // invoke-virtual {v0, p1, p2}, Lcom/android/server/am/ScoutMemoryError;->init(Lcom/android/server/am/ActivityManagerService;Landroid/content/Context;)V
/* .line 165 */
return;
} // .end method
public Boolean isEnableMqsReport ( ) {
/* .locals 1 */
/* .line 496 */
/* sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->ENABLE_MQS_REPORT:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 497 */
int v0 = 1; // const/4 v0, 0x1
/* .line 498 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isEnableSentinelMemoryMonitor ( ) {
/* .locals 1 */
/* .line 490 */
/* sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->ENABLE_SENTINEL_MEMORY_MONITOR:Z */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 491 */
int v0 = 1; // const/4 v0, 0x1
/* .line 492 */
} // :cond_0
int v0 = 0; // const/4 v0, 0x0
} // .end method
public Boolean isProcessTracked ( com.miui.server.sentinel.NativeHeapUsageInfo p0 ) {
/* .locals 3 */
/* .param p1, "nativeHeapUsageInfo" # Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* .line 660 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "#"; // const-string v1, "#"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 661 */
/* .local v0, "key":Ljava/lang/String; */
v1 = this.trackEventList;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).get ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
int v2 = 1; // const/4 v2, 0x1
/* if-nez v1, :cond_0 */
/* .line 662 */
v1 = this.trackEventList;
java.lang.Integer .valueOf ( v2 );
(( java.util.concurrent.ConcurrentHashMap ) v1 ).put ( v0, v2 ); // invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 663 */
int v1 = 0; // const/4 v1, 0x0
/* .line 665 */
} // :cond_0
} // .end method
public void judgmentFdAmountLeakException ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 5 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 257 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).getFdUsageInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getFdUsageInfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/FdUsageInfo;
/* .line 258 */
/* .local v0, "fdUsageInfo":Lcom/miui/server/sentinel/FdUsageInfo; */
(( com.miui.server.sentinel.FdUsageInfo ) v0 ).getFd_amount ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/FdUsageInfo;->getFd_amount()J
/* move-result-wide v1 */
/* const-wide/16 v3, 0x3e8 */
/* cmp-long v1, v1, v3 */
/* if-lez v1, :cond_0 */
/* .line 259 */
/* const/16 v1, 0x9 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 261 */
} // :cond_0
return;
} // .end method
public void judgmentJavaHeapLeakException ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 7 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 233 */
v0 = (( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).isEnableMqsReport ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->isEnableMqsReport()Z
/* if-nez v0, :cond_0 */
/* .line 234 */
return;
/* .line 236 */
} // :cond_0
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).getJavaHeapinfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getJavaHeapinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/JavaHeapUsageInfo;
/* .line 237 */
/* .local v0, "javaHeapUsageInfo":Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
/* const-string/jumbo v1, "system_server" */
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;
v1 = (( java.lang.String ) v1 ).equals ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v2 = 7; // const/4 v2, 0x7
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 238 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v3 */
/* const-wide/32 v5, 0x64000 */
/* cmp-long v1, v3, v5 */
/* if-lez v1, :cond_2 */
/* .line 239 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 242 */
} // :cond_1
com.miui.server.sentinel.MiuiSentinelService .getAppJavaheapWhiteList ( );
(( com.miui.server.sentinel.JavaHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;
(( java.util.HashMap ) v1 ).get ( v3 ); // invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* int-to-long v3, v1 */
/* .line 243 */
/* .local v3, "limit":J */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v5 */
/* cmp-long v1, v5, v3 */
/* if-lez v1, :cond_2 */
/* .line 244 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( v0, v2 ); // invoke-virtual {p0, v0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 247 */
} // .end local v3 # "limit":J
} // :cond_2
} // :goto_0
return;
} // .end method
public void judgmentNativeHeapLeakException ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 10 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 207 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).getNativeHeapinfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getNativeHeapinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/NativeHeapUsageInfo;
/* .line 208 */
/* .local v0, "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
/* const-string/jumbo v2, "system_server" */
v1 = (( java.lang.String ) v2 ).equals ( v1 ); // invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
int v3 = 1; // const/4 v3, 0x1
final String v4 = "#"; // const-string v4, "#"
int v5 = 0; // const/4 v5, 0x0
/* .line 210 */
java.lang.Integer .valueOf ( v5 );
/* .line 208 */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 209 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getNativeHeapSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J
/* move-result-wide v6 */
/* const-wide/32 v8, 0x57800 */
/* cmp-long v1, v6, v8 */
/* if-lez v1, :cond_1 */
v1 = this.highCapacityRssList;
/* .line 210 */
(( java.util.concurrent.ConcurrentHashMap ) v1 ).getOrDefault ( v2, v5 ); // invoke-virtual {v1, v2, v5}, Ljava/util/concurrent/ConcurrentHashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
v2 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
/* if-ne v1, v2, :cond_1 */
/* .line 211 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 212 */
/* .local v1, "sb":Ljava/lang/StringBuilder; */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v4 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v2 ).append ( v4 ); // invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 213 */
v2 = this.trackList;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v2 ).put ( v4, v0 ); // invoke-virtual {v2, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 214 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 215 */
} // .end local v1 # "sb":Ljava/lang/StringBuilder;
/* goto/16 :goto_0 */
/* .line 217 */
} // :cond_0
com.miui.server.sentinel.MiuiSentinelService .getAppNativeheapWhiteList ( );
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 218 */
com.miui.server.sentinel.MiuiSentinelService .getAppNativeheapWhiteList ( );
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.util.HashMap ) v1 ).get ( v2 ); // invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v1, Ljava/lang/Integer; */
v1 = (( java.lang.Integer ) v1 ).intValue ( ); // invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
/* int-to-long v1, v1 */
/* .line 219 */
/* .local v1, "limit":J */
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "app limit: "; // const-string v7, "app limit: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1, v2 ); // invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v7 = "MiuiSentinelMemoryManager"; // const-string v7, "MiuiSentinelMemoryManager"
android.util.Slog .e ( v7,v6 );
/* .line 220 */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getGrowsize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v6 */
/* cmp-long v6, v6, v1 */
/* if-lez v6, :cond_1 */
v6 = this.highCapacityRssList;
/* .line 221 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v6 ).getOrDefault ( v7, v5 ); // invoke-virtual {v6, v7, v5}, Ljava/util/concurrent/ConcurrentHashMap;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* check-cast v5, Ljava/lang/Integer; */
/* .line 222 */
v5 = (( java.lang.Integer ) v5 ).intValue ( ); // invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
v6 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
/* if-ne v5, v6, :cond_1 */
/* .line 223 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 224 */
/* .local v5, "sb":Ljava/lang/StringBuilder; */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v4 ); // invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v6 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v4 ).append ( v6 ); // invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* .line 225 */
v4 = this.trackList;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v4 ).put ( v6, v0 ); // invoke-virtual {v4, v6, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
/* .line 226 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( v0, v3 ); // invoke-virtual {p0, v0, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 230 */
} // .end local v1 # "limit":J
} // .end local v5 # "sb":Ljava/lang/StringBuilder;
} // :cond_1
} // :goto_0
return;
} // .end method
public void judgmentRssLeakException ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 9 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 194 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).getRssinfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getRssinfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/RssUsageInfo;
/* .line 195 */
/* .local v0, "rssUsageInfo":Lcom/miui/server/sentinel/RssUsageInfo; */
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getRssSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J
/* move-result-wide v1 */
/* .line 196 */
/* .local v1, "rssSize":J */
/* const-wide/high16 v3, 0x3ff0000000000000L # 1.0 */
/* long-to-double v5, v1 */
/* mul-double/2addr v5, v3 */
/* const-wide/high16 v3, 0x4148000000000000L # 3145728.0 */
/* div-double/2addr v5, v3 */
/* .line 197 */
/* .local v5, "percentage":D */
/* const-wide v3, 0x3fc999999999999aL # 0.2 */
/* cmpl-double v3, v5, v3 */
/* if-lez v3, :cond_0 */
/* .line 198 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 199 */
/* .local v3, "sb":Ljava/lang/StringBuilder; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "RSS Leak in pid ("; // const-string v7, "RSS Leak in pid ("
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v7 = (( com.miui.server.sentinel.RssUsageInfo ) v0 ).getPid ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v7 = ")"; // const-string v7, ")"
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getName ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 200 */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "RSS size:"; // const-string v7, "RSS size:"
(( java.lang.StringBuilder ) v4 ).append ( v7 ); // invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.RssUsageInfo ) v0 ).getRssSize ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/RssUsageInfo;->getRssSize()J
/* move-result-wide v7 */
(( java.lang.StringBuilder ) v4 ).append ( v7, v8 ); // invoke-virtual {v4, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 201 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).removeEventList ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->removeEventList(Lcom/miui/server/sentinel/SocketPacket;)V
/* .line 202 */
/* const/16 v4, 0x1a0 */
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* invoke-direct {p0, v4, v7, v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportRssLeakEvent(ILjava/lang/String;Lcom/miui/server/sentinel/RssUsageInfo;)V */
/* .line 204 */
} // .end local v3 # "sb":Ljava/lang/StringBuilder;
} // :cond_0
return;
} // .end method
public void judgmentThreadAmountLeakException ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 5 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 250 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).getThreadUsageInfo ( p1 ); // invoke-virtual {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getThreadUsageInfo(Lcom/miui/server/sentinel/SocketPacket;)Lcom/miui/server/sentinel/ThreadUsageInfo;
/* .line 251 */
/* .local v0, "threadUsageInfo":Lcom/miui/server/sentinel/ThreadUsageInfo; */
(( com.miui.server.sentinel.ThreadUsageInfo ) v0 ).getThreadAmount ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/ThreadUsageInfo;->getThreadAmount()J
/* move-result-wide v1 */
/* const-wide/16 v3, 0x1f4 */
/* cmp-long v1, v1, v3 */
/* if-lez v1, :cond_0 */
/* .line 252 */
/* const/16 v1, 0x8 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( v0, v1 ); // invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V
/* .line 254 */
} // :cond_0
return;
} // .end method
public void outPutTrackLog ( com.miui.server.sentinel.TrackPacket p0 ) {
/* .locals 12 */
/* .param p1, "trackPacket" # Lcom/miui/server/sentinel/TrackPacket; */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 557 */
/* new-instance v0, Ljava/io/File; */
final String v1 = "/data/miuilog/stability/memleak/heapleak"; // const-string v1, "/data/miuilog/stability/memleak/heapleak"
/* invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V */
/* .line 558 */
/* .local v0, "trackDir":Ljava/io/File; */
v1 = (( java.io.File ) v0 ).exists ( ); // invoke-virtual {v0}, Ljava/io/File;->exists()Z
/* const/16 v2, 0x1fc */
final String v3 = "MiuiSentinelMemoryManager"; // const-string v3, "MiuiSentinelMemoryManager"
int v4 = -1; // const/4 v4, -0x1
/* if-nez v1, :cond_1 */
/* .line 559 */
v1 = (( java.io.File ) v0 ).mkdirs ( ); // invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z
/* if-nez v1, :cond_0 */
/* .line 560 */
/* new-instance v1, Ljava/lang/Throwable; */
/* invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V */
final String v5 = "cannot create memleak Dir"; // const-string v5, "cannot create memleak Dir"
android.util.Slog .e ( v3,v5,v1 );
/* .line 562 */
} // :cond_0
android.os.FileUtils .setPermissions ( v0,v2,v4,v4 );
/* .line 565 */
} // :cond_1
java.lang.System .currentTimeMillis ( );
/* move-result-wide v5 */
com.miui.server.sentinel.MiuiSentinelUtils .getFormatDateTime ( v5,v6 );
/* .line 566 */
/* .local v1, "dirSuffix":Ljava/lang/String; */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
v6 = (( com.miui.server.sentinel.TrackPacket ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = "_"; // const-string v6, "_"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 567 */
(( com.miui.server.sentinel.TrackPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;
(( java.lang.StringBuilder ) v5 ).append ( v7 ); // invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v1 ); // invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 569 */
/* .local v5, "dirname":Ljava/lang/String; */
/* new-instance v7, Ljava/io/File; */
/* invoke-direct {v7, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 570 */
/* .local v7, "exceptionDir":Ljava/io/File; */
v8 = (( java.io.File ) v7 ).exists ( ); // invoke-virtual {v7}, Ljava/io/File;->exists()Z
/* if-nez v8, :cond_3 */
/* .line 571 */
v8 = (( java.io.File ) v7 ).mkdirs ( ); // invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z
/* if-nez v8, :cond_2 */
/* .line 572 */
/* new-instance v8, Ljava/lang/Throwable; */
/* invoke-direct {v8}, Ljava/lang/Throwable;-><init>()V */
final String v9 = "cannot create exceptionDir"; // const-string v9, "cannot create exceptionDir"
android.util.Slog .e ( v3,v9,v8 );
/* .line 574 */
} // :cond_2
android.os.FileUtils .setPermissions ( v7,v2,v4,v4 );
/* .line 576 */
} // :cond_3
/* new-instance v8, Ljava/lang/StringBuilder; */
/* invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V */
v9 = (( com.miui.server.sentinel.TrackPacket ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 577 */
(( com.miui.server.sentinel.TrackPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;
(( java.lang.StringBuilder ) v8 ).append ( v9 ); // invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v8 ).append ( v6 ); // invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).append ( v1 ); // invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v8 = "_heapleak_info.txt"; // const-string v8, "_heapleak_info.txt"
(( java.lang.StringBuilder ) v6 ).append ( v8 ); // invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v6 ).toString ( ); // invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 579 */
/* .local v6, "filename":Ljava/lang/String; */
/* new-instance v8, Ljava/io/File; */
/* invoke-direct {v8, v7, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
/* .line 580 */
/* .local v8, "trackfile":Ljava/io/File; */
/* new-instance v9, Ljava/lang/StringBuilder; */
/* invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 581 */
/* .local v9, "sb":Ljava/lang/StringBuilder; */
(( com.miui.server.sentinel.TrackPacket ) p1 ).getData ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getData()Ljava/lang/String;
(( java.lang.StringBuilder ) v9 ).append ( v10 ); // invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 582 */
v10 = (( java.io.File ) v8 ).exists ( ); // invoke-virtual {v8}, Ljava/io/File;->exists()Z
/* if-nez v10, :cond_5 */
/* .line 583 */
v10 = (( java.io.File ) v8 ).createNewFile ( ); // invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z
/* if-nez v10, :cond_4 */
/* .line 584 */
/* new-instance v10, Ljava/lang/Throwable; */
/* invoke-direct {v10}, Ljava/lang/Throwable;-><init>()V */
final String v11 = "cannot create leakfile"; // const-string v11, "cannot create leakfile"
android.util.Slog .e ( v3,v11,v10 );
/* .line 586 */
} // :cond_4
(( java.io.File ) v8 ).getAbsolutePath ( ); // invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
android.os.FileUtils .setPermissions ( v10,v2,v4,v4 );
/* .line 589 */
} // :cond_5
try { // :try_start_0
/* new-instance v2, Ljava/io/FileWriter; */
int v4 = 1; // const/4 v4, 0x1
/* invoke-direct {v2, v8, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 590 */
/* .local v2, "writers":Ljava/io/FileWriter; */
try { // :try_start_1
(( java.lang.StringBuilder ) v9 ).toString ( ); // invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.io.FileWriter ) v2 ).write ( v4 ); // invoke-virtual {v2, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 591 */
try { // :try_start_2
(( java.io.FileWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
/* :try_end_2 */
/* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 593 */
} // .end local v2 # "writers":Ljava/io/FileWriter;
/* .line 589 */
/* .restart local v2 # "writers":Ljava/io/FileWriter; */
/* :catchall_0 */
/* move-exception v4 */
try { // :try_start_3
(( java.io.FileWriter ) v2 ).close ( ); // invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
/* :try_end_3 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
/* :catchall_1 */
/* move-exception v10 */
try { // :try_start_4
(( java.lang.Throwable ) v4 ).addSuppressed ( v10 ); // invoke-virtual {v4, v10}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
} // .end local v0 # "trackDir":Ljava/io/File;
} // .end local v1 # "dirSuffix":Ljava/lang/String;
} // .end local v5 # "dirname":Ljava/lang/String;
} // .end local v6 # "filename":Ljava/lang/String;
} // .end local v7 # "exceptionDir":Ljava/io/File;
} // .end local v8 # "trackfile":Ljava/io/File;
} // .end local v9 # "sb":Ljava/lang/StringBuilder;
} // .end local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
} // .end local p1 # "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
} // :goto_0
/* throw v4 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 591 */
} // .end local v2 # "writers":Ljava/io/FileWriter;
/* .restart local v0 # "trackDir":Ljava/io/File; */
/* .restart local v1 # "dirSuffix":Ljava/lang/String; */
/* .restart local v5 # "dirname":Ljava/lang/String; */
/* .restart local v6 # "filename":Ljava/lang/String; */
/* .restart local v7 # "exceptionDir":Ljava/io/File; */
/* .restart local v8 # "trackfile":Ljava/io/File; */
/* .restart local v9 # "sb":Ljava/lang/StringBuilder; */
/* .restart local p0 # "this":Lcom/miui/server/sentinel/MiuiSentinelMemoryManager; */
/* .restart local p1 # "trackPacket":Lcom/miui/server/sentinel/TrackPacket; */
/* :catch_0 */
/* move-exception v2 */
/* .line 592 */
/* .local v2, "e":Ljava/io/IOException; */
/* new-instance v4, Ljava/lang/Throwable; */
/* invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V */
final String v10 = "Unable to write Track Stack to file"; // const-string v10, "Unable to write Track Stack to file"
android.util.Slog .w ( v3,v10,v4 );
/* .line 594 */
} // .end local v2 # "e":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
public void removeEventList ( com.miui.server.sentinel.SocketPacket p0 ) {
/* .locals 5 */
/* .param p1, "socketPacket" # Lcom/miui/server/sentinel/SocketPacket; */
/* .line 502 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 503 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getProcess_name ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
/* .line 504 */
/* .local v1, "procname":Ljava/lang/String; */
(( com.miui.server.sentinel.SocketPacket ) p1 ).getEvent_type ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;
/* .line 505 */
/* .local v2, "type":Ljava/lang/String; */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v4 = "#"; // const-string v4, "#"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).append ( v1 ); // invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 506 */
v3 = this.eventList;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
(( java.util.concurrent.ConcurrentHashMap ) v3 ).remove ( v4 ); // invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 507 */
return;
} // .end method
public void removeTrackEvent ( com.miui.server.sentinel.NativeHeapUsageInfo p0 ) {
/* .locals 2 */
/* .param p1, "nativeHeapUsageInfo" # Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* .line 669 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = "#"; // const-string v1, "#"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 670 */
/* .local v0, "key":Ljava/lang/String; */
v1 = this.trackEventList;
(( java.util.concurrent.ConcurrentHashMap ) v1 ).remove ( v0 ); // invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
/* .line 671 */
return;
} // .end method
public void reportFdAmountLeak ( com.miui.server.sentinel.FdUsageInfo p0 ) {
/* .locals 7 */
/* .param p1, "fdUsageInfo" # Lcom/miui/server/sentinel/FdUsageInfo; */
/* .line 640 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Fd Amount Leak "; // const-string v1, "Fd Amount Leak "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.FdUsageInfo ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " (Threshold = "; // const-string v1, " (Threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x3e8 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " )"; // const-string v1, " )"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 642 */
/* .local v0, "fdleakmsg":Ljava/lang/String; */
/* const/16 v2, 0x1b0 */
/* .line 643 */
(( com.miui.server.sentinel.FdUsageInfo ) p1 ).getUsageInfo ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->getUsageInfo()Ljava/lang/String;
v5 = (( com.miui.server.sentinel.FdUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->getPid()I
/* .line 644 */
(( com.miui.server.sentinel.FdUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/FdUsageInfo;->getName()Ljava/lang/String;
/* .line 642 */
/* move-object v1, p0 */
/* move-object v3, v0 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V */
/* .line 645 */
return;
} // .end method
public void reportJavaHeapLeak ( com.miui.server.sentinel.JavaHeapUsageInfo p0 ) {
/* .locals 9 */
/* .param p1, "javaHeapUsageInfo" # Lcom/miui/server/sentinel/JavaHeapUsageInfo; */
/* .line 611 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "JavaHeap Leak "; // const-string v1, "JavaHeap Leak "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.JavaHeapUsageInfo ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " (Threshold = "; // const-string v1, " (Threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 612 */
(( com.miui.server.sentinel.JavaHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;
/* const/16 v2, 0x11 */
com.miui.server.sentinel.MiuiSentinelUtils .getProcessThreshold ( v1,v2 );
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "KB)"; // const-string v1, "KB)"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 615 */
/* .local v0, "javaleakmsg":Ljava/lang/String; */
/* const/16 v2, 0x1a2 */
final String v4 = ""; // const-string v4, ""
/* .line 616 */
v5 = (( com.miui.server.sentinel.JavaHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getPid()I
/* .line 617 */
(( com.miui.server.sentinel.JavaHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getName()Ljava/lang/String;
(( com.miui.server.sentinel.JavaHeapUsageInfo ) p1 ).getJavaHeapSize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/JavaHeapUsageInfo;->getJavaHeapSize()J
/* move-result-wide v7 */
/* .line 615 */
/* move-object v1, p0 */
/* move-object v3, v0 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportHeapLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V */
/* .line 618 */
int v1 = 5; // const/4 v1, 0x5
/* const-wide/16 v2, 0x0 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( p1, v1, v2, v3 ); // invoke-virtual {p0, p1, v1, v2, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;IJ)V
/* .line 619 */
return;
} // .end method
public void reportNativeHeapLeak ( com.miui.server.sentinel.NativeHeapUsageInfo p0 ) {
/* .locals 9 */
/* .param p1, "nativeHeapUsageInfo" # Lcom/miui/server/sentinel/NativeHeapUsageInfo; */
/* .line 597 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "NativeHeap Leak Proc info Name: "; // const-string v1, "NativeHeap Leak Proc info Name: "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 598 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " Pid = "; // const-string v1, " Pid = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " NativeHeap Size = "; // const-string v1, " NativeHeap Size = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 599 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getNativeHeapSize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = " KB (Threshold = "; // const-string v1, " KB (Threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 600 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
/* const/16 v2, 0x12 */
com.miui.server.sentinel.MiuiSentinelUtils .getProcessThreshold ( v1,v2 );
/* move-result-wide v1 */
(( java.lang.StringBuilder ) v0 ).append ( v1, v2 ); // invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v1 = "KB) "; // const-string v1, "KB) "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 603 */
/* .local v0, "nativeleakmsg":Ljava/lang/String; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "debug mqs info:"; // const-string v2, "debug mqs info:"
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSentinelMemoryManager"; // const-string v2, "MiuiSentinelMemoryManager"
android.util.Slog .d ( v2,v1 );
/* .line 604 */
/* const/16 v2, 0x1a1 */
/* .line 605 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getStackTrace ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getStackTrace()Ljava/lang/String;
v5 = (( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getPid()I
/* .line 606 */
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getName ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getName()Ljava/lang/String;
(( com.miui.server.sentinel.NativeHeapUsageInfo ) p1 ).getNativeHeapSize ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->getNativeHeapSize()J
/* move-result-wide v7 */
/* .line 604 */
/* move-object v1, p0 */
/* move-object v3, v0 */
/* invoke-direct/range {v1 ..v8}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportHeapLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;J)V */
/* .line 607 */
int v1 = 5; // const/4 v1, 0x5
/* const-wide/16 v2, 0x0 */
(( com.miui.server.sentinel.MiuiSentinelMemoryManager ) p0 ).sendMessage ( p1, v1, v2, v3 ); // invoke-virtual {p0, p1, v1, v2, v3}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;IJ)V
/* .line 608 */
return;
} // .end method
public void reportThreadAmountLeak ( com.miui.server.sentinel.ThreadUsageInfo p0 ) {
/* .locals 10 */
/* .param p1, "threadUsageInfo" # Lcom/miui/server/sentinel/ThreadUsageInfo; */
/* .line 622 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "Thread Amount Leak "; // const-string v1, "Thread Amount Leak "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.ThreadUsageInfo ) p1 ).toString ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/ThreadUsageInfo;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = " (Threshold = "; // const-string v1, " (Threshold = "
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* const/16 v1, 0x1f4 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v1 = " )"; // const-string v1, " )"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 624 */
/* .local v0, "thleakmsg":Ljava/lang/String; */
v7 = (( com.miui.server.sentinel.ThreadUsageInfo ) p1 ).getPid ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/ThreadUsageInfo;->getPid()I
/* .line 625 */
/* .local v7, "pid":I */
com.android.server.am.ProcessUtils .getPackageNameByPid ( v7 );
/* .line 626 */
/* .local v1, "packageName":Ljava/lang/String; */
/* if-nez v1, :cond_0 */
/* .line 627 */
com.miui.server.sentinel.MiuiSentinelUtils .getProcessCmdline ( v7 );
/* move-object v8, v1 */
/* .line 626 */
} // :cond_0
/* move-object v8, v1 */
/* .line 629 */
} // .end local v1 # "packageName":Ljava/lang/String;
/* .local v8, "packageName":Ljava/lang/String; */
} // :goto_0
/* const-string/jumbo v1, "unknown" */
v1 = (( java.lang.String ) v1 ).equals ( v8 ); // invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v1 != null) { // if-eqz v1, :cond_1
/* .line 630 */
final String v1 = "MiuiSentinelMemoryManager"; // const-string v1, "MiuiSentinelMemoryManager"
final String v2 = "The current process may exit and ignore report mqs"; // const-string v2, "The current process may exit and ignore report mqs"
android.util.Slog .d ( v1,v2 );
/* .line 631 */
return;
/* .line 633 */
} // :cond_1
final String v1 = "Thread_LEAK"; // const-string v1, "Thread_LEAK"
com.miui.server.sentinel.MiuiSentinelUtils .getExceptionPath ( v7,v8,v1 );
/* .line 634 */
/* .local v9, "filename":Ljava/io/File; */
com.miui.server.sentinel.MiuiSentinelUtils .dumpThreadInfo ( v7,v8,v9 );
/* .line 635 */
/* const/16 v2, 0x1b8 */
/* .line 636 */
(( com.miui.server.sentinel.ThreadUsageInfo ) p1 ).getUsageInfo ( ); // invoke-virtual {p1}, Lcom/miui/server/sentinel/ThreadUsageInfo;->getUsageInfo()Ljava/lang/String;
/* .line 635 */
/* move-object v1, p0 */
/* move-object v3, v0 */
/* move v5, v7 */
/* move-object v6, v8 */
/* invoke-direct/range {v1 ..v6}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportLeakEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;)V */
/* .line 637 */
return;
} // .end method
public void sendMessage ( java.lang.Object p0, Integer p1 ) {
/* .locals 3 */
/* .param p2, "number" # I */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(TT;I)V" */
/* } */
} // .end annotation
/* .line 470 */
/* .local p1, "obj":Ljava/lang/Object;, "TT;" */
v0 = com.miui.server.sentinel.MiuiSentinelMemoryManager.miuiSentinelMemoryManager;
v0 = this.mSentineHandler;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager$MiuiSentineHandler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->obtainMessage()Landroid/os/Message;
/* .line 471 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->what:I */
/* .line 472 */
this.obj = p1;
/* .line 473 */
v1 = com.miui.server.sentinel.MiuiSentinelMemoryManager.miuiSentinelMemoryManager;
v1 = this.mSentineHandler;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager$MiuiSentineHandler ) v1 ).sendMessage ( v0 ); // invoke-virtual {v1, v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->sendMessage(Landroid/os/Message;)Z
/* .line 474 */
/* sget-boolean v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 475 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "msg.what = "; // const-string v2, "msg.what = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "msg.obj = "; // const-string v2, "msg.obj = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v2 = this.obj;
(( java.lang.Object ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSentinelMemoryManager"; // const-string v2, "MiuiSentinelMemoryManager"
android.util.Slog .d ( v2,v1 );
/* .line 477 */
} // :cond_0
return;
} // .end method
public void sendMessage ( java.lang.Object p0, Integer p1, Long p2 ) {
/* .locals 4 */
/* .param p2, "number" # I */
/* .param p3, "timeout" # J */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "<T:", */
/* "Ljava/lang/Object;", */
/* ">(TT;IJ)V" */
/* } */
} // .end annotation
/* .line 480 */
/* .local p1, "obj":Ljava/lang/Object;, "TT;" */
v0 = com.miui.server.sentinel.MiuiSentinelMemoryManager.miuiSentinelMemoryManager;
v0 = this.mSentineHandler;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager$MiuiSentineHandler ) v0 ).obtainMessage ( ); // invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->obtainMessage()Landroid/os/Message;
/* .line 481 */
/* .local v0, "message":Landroid/os/Message; */
/* iput p2, v0, Landroid/os/Message;->what:I */
/* .line 482 */
this.obj = p1;
/* .line 483 */
v1 = com.miui.server.sentinel.MiuiSentinelMemoryManager.miuiSentinelMemoryManager;
v1 = this.mSentineHandler;
(( com.miui.server.sentinel.MiuiSentinelMemoryManager$MiuiSentineHandler ) v1 ).sendMessageDelayed ( v0, p3, p4 ); // invoke-virtual {v1, v0, p3, p4}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->sendMessageDelayed(Landroid/os/Message;J)Z
/* .line 484 */
/* sget-boolean v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z */
if ( v1 != null) { // if-eqz v1, :cond_0
/* .line 485 */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "msg.what = "; // const-string v2, "msg.what = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, v0, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
/* const-string/jumbo v2, "send message time = " */
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
java.lang.System .currentTimeMillis ( );
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
final String v2 = "MiuiSentinelMemoryManager"; // const-string v2, "MiuiSentinelMemoryManager"
android.util.Slog .d ( v2,v1 );
/* .line 487 */
} // :cond_0
return;
} // .end method
