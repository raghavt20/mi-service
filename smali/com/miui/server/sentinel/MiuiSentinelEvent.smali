.class public Lcom/miui/server/sentinel/MiuiSentinelEvent;
.super Ljava/lang/Object;
.source "MiuiSentinelEvent.java"


# static fields
.field public static final EVENT_DMABUFF_LEAK:I = 0x10

.field public static final EVENT_FD_LEAK:I = 0x13

.field public static final EVENT_HEAP_TRACK:I = 0x16

.field public static final EVENT_JAVAHEAP_LEAK:I = 0x11

.field public static final EVENT_KGSL_LEAK:I = 0xf

.field public static final EVENT_NATIVEHEAP_LEAK:I = 0x12

.field public static final EVENT_OPENFILE_AMOUNT:I = 0x9

.field public static final EVENT_OPENFILE_FDSIZE:I = 0x8

.field public static final EVENT_PROCKTHREAD_AMOUNT:I = 0xc

.field public static final EVENT_PROCNATIVE_AMOUNT:I = 0xb

.field public static final EVENT_PROCRUNTIME_AMOUNT:I = 0xa

.field public static final EVENT_PROCTOTAL_AMOUNT:I = 0xd

.field public static final EVENT_PROC_LEAK:I = 0x15

.field public static final EVENT_RESIDENTSIZE:I = 0x7

.field public static final EVENT_RSS_LEAK:I = 0xe

.field public static final EVENT_THREAD_AMOUNT:I = 0x4

.field public static final EVENT_THREAD_LEAK:I = 0x14

.field public static final EVENT_VSSASGMEM:I = 0x5

.field public static final EVENT_VSSDALVIK:I = 0x3

.field public static final EVENT_VSSDMABUFF:I = 0x6

.field public static final EVENT_VSSHEAP:I = 0x2

.field public static final EVENT_VSSKGSL:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEventType(Ljava/lang/String;)I
    .locals 14
    .param p0, "type"    # Ljava/lang/String;

    .line 32
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/16 v1, 0xc

    const/16 v2, 0xb

    const/16 v3, 0xa

    const/16 v4, 0x9

    const/16 v5, 0x8

    const/4 v6, 0x7

    const/4 v7, 0x6

    const/4 v8, 0x5

    const/4 v9, 0x4

    const/4 v10, 0x3

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/4 v13, -0x1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    goto/16 :goto_0

    :sswitch_0
    const-string v0, "Event_ThreadAmount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v10

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "Event_ProcKthreadAmount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto/16 :goto_1

    :sswitch_2
    const-string v0, "Event_ProcTotalAmount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto/16 :goto_1

    :sswitch_3
    const-string v0, "Event_ProcRuntimeAmoun"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    goto/16 :goto_1

    :sswitch_4
    const-string v0, "Event_ProcNativeAmount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    goto :goto_1

    :sswitch_5
    const-string v0, "Event_OpenFileFDSize"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v6

    goto :goto_1

    :sswitch_6
    const-string v0, "Event_VssKgsl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_7
    const-string v0, "Event_VssHeap"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v12

    goto :goto_1

    :sswitch_8
    const-string v0, "Event_OpenFileAmount"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v5

    goto :goto_1

    :sswitch_9
    const-string v0, "Event_ResidentSize"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v7

    goto :goto_1

    :sswitch_a
    const-string v0, "Event_VssDmaBuf"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v8

    goto :goto_1

    :sswitch_b
    const-string v0, "Event_VssDalvik"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v11

    goto :goto_1

    :sswitch_c
    const-string v0, "Event_VssAshmem"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v9

    goto :goto_1

    :goto_0
    move v0, v13

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 60
    return v13

    .line 58
    :pswitch_0
    const/16 v0, 0xd

    return v0

    .line 56
    :pswitch_1
    return v1

    .line 54
    :pswitch_2
    return v2

    .line 52
    :pswitch_3
    return v3

    .line 50
    :pswitch_4
    return v4

    .line 48
    :pswitch_5
    return v5

    .line 46
    :pswitch_6
    return v6

    .line 44
    :pswitch_7
    return v7

    .line 42
    :pswitch_8
    return v8

    .line 40
    :pswitch_9
    return v9

    .line 38
    :pswitch_a
    return v10

    .line 36
    :pswitch_b
    return v11

    .line 34
    :pswitch_c
    return v12

    nop

    :sswitch_data_0
    .sparse-switch
        -0x72b74710 -> :sswitch_c
        -0x6e947086 -> :sswitch_b
        -0x6df11854 -> :sswitch_a
        -0x6933f4ca -> :sswitch_9
        -0x2c8e789d -> :sswitch_8
        -0x2965e883 -> :sswitch_7
        -0x296481ba -> :sswitch_6
        -0x2654e736 -> :sswitch_5
        -0x53f52f6 -> :sswitch_4
        -0x39ea7a1 -> :sswitch_3
        0x2d23a3a1 -> :sswitch_2
        0x380dbc92 -> :sswitch_1
        0x65d8acc7 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
