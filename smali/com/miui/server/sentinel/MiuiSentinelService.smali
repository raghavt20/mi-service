.class public Lcom/miui/server/sentinel/MiuiSentinelService;
.super Landroid/os/Binder;
.source "MiuiSentinelService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;,
        Lcom/miui/server/sentinel/MiuiSentinelService$Lifecycle;
    }
.end annotation


# static fields
.field private static final APP_JAVAHEAP_WHITE_LIST:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final APP_NATIVEHEAP_WHITE_LIST:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEBUG:Z

.field private static final DEFAULT_BUFFER_SIZE:I = 0x3e8000

.field public static final HOST_NAME:Ljava/lang/String; = "data/localsocket/prochunter_native_socket"

.field public static final MAX_BUFF_SIZE:I = 0x2328

.field public static final PROC_HUNTER_MSG:I = 0xa

.field public static final REPORT_NATIVEHEAP_LEAKTOMQS:I = 0x6

.field public static final SERVICE_NAME:Ljava/lang/String; = "miui.sentinel.service"

.field private static final TAG:Ljava/lang/String; = "MiuiSentinelService"

.field public static final TRACK_HEAP_REPORT_MSG:I = 0x6

.field private static miuiSentinelServiceThread:Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;

.field private static volatile sInstance:Lcom/miui/server/sentinel/MiuiSentinelService;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static bridge synthetic -$$Nest$mcreateSystemServerSocketForProchunter(Lcom/miui/server/sentinel/MiuiSentinelService;)Landroid/net/LocalSocket;
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/sentinel/MiuiSentinelService;->createSystemServerSocketForProchunter()Landroid/net/LocalSocket;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$minitContext(Lcom/miui/server/sentinel/MiuiSentinelService;Landroid/content/Context;)Lcom/miui/server/sentinel/MiuiSentinelService;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->initContext(Landroid/content/Context;)Lcom/miui/server/sentinel/MiuiSentinelService;

    move-result-object p0

    return-object p0
.end method

.method static bridge synthetic -$$Nest$mrecvMessage(Lcom/miui/server/sentinel/MiuiSentinelService;Ljava/io/FileDescriptor;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->recvMessage(Ljava/io/FileDescriptor;)V

    return-void
.end method

.method static bridge synthetic -$$Nest$sfgetmiuiSentinelServiceThread()Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;
    .locals 1

    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->miuiSentinelServiceThread:Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;

    return-object v0
.end method

.method static bridge synthetic -$$Nest$smnRegisteredBpfEvent()V
    .locals 0

    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->nRegisteredBpfEvent()V

    return-void
.end method

.method static constructor <clinit>()V
    .locals 2

    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->sInstance:Lcom/miui/server/sentinel/MiuiSentinelService;

    .line 47
    const-string v0, "debug.sys.mss"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelService;->DEBUG:Z

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->APP_JAVAHEAP_WHITE_LIST:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->APP_NATIVEHEAP_WHITE_LIST:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 55
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelService;->mContext:Landroid/content/Context;

    .line 56
    new-instance v1, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;

    invoke-direct {v1, p0, v0}, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;-><init>(Lcom/miui/server/sentinel/MiuiSentinelService;Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread-IA;)V

    sput-object v1, Lcom/miui/server/sentinel/MiuiSentinelService;->miuiSentinelServiceThread:Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;

    .line 57
    return-void
.end method

.method private createSystemServerSocketForProchunter()Landroid/net/LocalSocket;
    .locals 4

    .line 193
    const/4 v0, 0x0

    .line 195
    .local v0, "serverSocket":Landroid/net/LocalSocket;
    :try_start_0
    new-instance v1, Landroid/net/LocalSocket;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/net/LocalSocket;-><init>(I)V

    move-object v0, v1

    .line 196
    new-instance v1, Landroid/net/LocalSocketAddress;

    const-string v2, "data/localsocket/prochunter_native_socket"

    sget-object v3, Landroid/net/LocalSocketAddress$Namespace;->ABSTRACT:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v1, v2, v3}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->bind(Landroid/net/LocalSocketAddress;)V

    .line 197
    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->setSendBufferSize(I)V

    .line 198
    invoke-virtual {v0, v1}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V

    .line 199
    const-string v1, "MiuiSentinelService"

    const-string v2, "prochunter socket create success"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    goto :goto_1

    .line 200
    :catch_0
    move-exception v1

    .line 201
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 202
    if-eqz v0, :cond_0

    .line 204
    :try_start_1
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 206
    goto :goto_0

    .line 205
    :catch_1
    move-exception v2

    .line 207
    :goto_0
    const/4 v0, 0x0

    .line 210
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return-object v0
.end method

.method public static getAppJavaheapWhiteList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 189
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->APP_JAVAHEAP_WHITE_LIST:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getAppNativeheapWhiteList()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 185
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->APP_NATIVEHEAP_WHITE_LIST:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getInstance()Lcom/miui/server/sentinel/MiuiSentinelService;
    .locals 2

    .line 60
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->sInstance:Lcom/miui/server/sentinel/MiuiSentinelService;

    if-nez v0, :cond_1

    .line 61
    const-class v0, Lcom/miui/server/sentinel/MiuiSentinelService;

    monitor-enter v0

    .line 62
    :try_start_0
    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelService;->sInstance:Lcom/miui/server/sentinel/MiuiSentinelService;

    if-nez v1, :cond_0

    .line 63
    new-instance v1, Lcom/miui/server/sentinel/MiuiSentinelService;

    invoke-direct {v1}, Lcom/miui/server/sentinel/MiuiSentinelService;-><init>()V

    sput-object v1, Lcom/miui/server/sentinel/MiuiSentinelService;->sInstance:Lcom/miui/server/sentinel/MiuiSentinelService;

    .line 65
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 67
    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelService;->sInstance:Lcom/miui/server/sentinel/MiuiSentinelService;

    return-object v0
.end method

.method private handlerTrackMessage(Lcom/miui/server/sentinel/TrackPacket;)V
    .locals 5
    .param p1, "trackPacket"    # Lcom/miui/server/sentinel/TrackPacket;

    .line 125
    :try_start_0
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->outPutTrackLog(Lcom/miui/server/sentinel/TrackPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    goto :goto_0

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    const-string v2, "MiuiSentinelService"

    const-string v3, "Track stack output to file failed"

    invoke-static {v2, v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 131
    .end local v0    # "e":Ljava/io/IOException;
    :goto_0
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getTrackList()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    .line 132
    .local v0, "tracklist":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/miui/server/sentinel/NativeHeapUsageInfo;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    .local v1, "key":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    .line 135
    .local v2, "nativeHeapUsageInfo":Lcom/miui/server/sentinel/NativeHeapUsageInfo;
    if-eqz v2, :cond_0

    .line 136
    invoke-virtual {p1}, Lcom/miui/server/sentinel/TrackPacket;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/miui/server/sentinel/NativeHeapUsageInfo;->setStackTrace(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v2, v4}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->sendMessage(Ljava/lang/Object;I)V

    .line 140
    :cond_0
    return-void
.end method

.method private handlerTrigger(Lcom/miui/server/sentinel/SocketPacket;)V
    .locals 3
    .param p1, "socketPacket"    # Lcom/miui/server/sentinel/SocketPacket;

    .line 95
    invoke-virtual {p1}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/server/sentinel/MiuiSentinelEvent;->getEventType(Ljava/lang/String;)I

    move-result v0

    .line 96
    .local v0, "type":I
    const-string v1, "MiuiSentinelService"

    packed-switch v0, :pswitch_data_0

    .line 118
    :pswitch_0
    const-string v2, "receive invalid event"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    :pswitch_1
    const-string v2, "begin judgment FdAmountLeakException"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentFdAmountLeakException(Lcom/miui/server/sentinel/SocketPacket;)V

    .line 116
    goto :goto_0

    .line 110
    :pswitch_2
    const-string v2, "begin judgmentRssLeakException"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentRssLeakException(Lcom/miui/server/sentinel/SocketPacket;)V

    .line 112
    goto :goto_0

    .line 106
    :pswitch_3
    const-string v2, "begin judgment ThreadAmountLeakException"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentThreadAmountLeakException(Lcom/miui/server/sentinel/SocketPacket;)V

    .line 108
    goto :goto_0

    .line 102
    :pswitch_4
    const-string v2, "begin judgmentJavaHeapLeakException"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentJavaHeapLeakException(Lcom/miui/server/sentinel/SocketPacket;)V

    .line 104
    goto :goto_0

    .line 98
    :pswitch_5
    const-string v2, "begin judgmentNativeHeapLeakException"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->judgmentNativeHeapLeakException(Lcom/miui/server/sentinel/SocketPacket;)V

    .line 100
    nop

    .line 121
    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initContext(Landroid/content/Context;)Lcom/miui/server/sentinel/MiuiSentinelService;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 71
    iput-object p1, p0, Lcom/miui/server/sentinel/MiuiSentinelService;->mContext:Landroid/content/Context;

    .line 72
    invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->initWhilteList(Landroid/content/Context;)V

    .line 73
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelService;

    move-result-object v0

    return-object v0
.end method

.method private initSystemServerTrack()V
    .locals 3

    .line 315
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    .line 316
    .local v0, "systemPid":I
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v1

    sget-object v2, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->START_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    invoke-virtual {v1, v0, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->handlerTriggerTrack(ILcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;)Z

    .line 317
    return-void
.end method

.method private initWhilteList(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .line 143
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 144
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x11030015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, "javaheaps":[Ljava/lang/String;
    const v2, 0x11030016

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, "nativeheaps":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v3, v1

    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    array-length v3, v2

    if-nez v3, :cond_1

    .line 147
    :cond_0
    const-string v3, "MiuiSentinelService"

    const-string v4, "initwhileList is failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    :cond_1
    array-length v3, v1

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    const-string v6, ","

    const/4 v7, 0x1

    if-ge v5, v3, :cond_2

    aget-object v8, v1, v5

    .line 152
    .local v8, "javaheap":Ljava/lang/String;
    invoke-virtual {v8, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 153
    .local v6, "split":[Ljava/lang/String;
    aget-object v7, v6, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 154
    .local v7, "threshold":Ljava/lang/Integer;
    sget-object v9, Lcom/miui/server/sentinel/MiuiSentinelService;->APP_JAVAHEAP_WHITE_LIST:Ljava/util/HashMap;

    aget-object v10, v6, v4

    invoke-virtual {v9, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    .end local v6    # "split":[Ljava/lang/String;
    .end local v7    # "threshold":Ljava/lang/Integer;
    .end local v8    # "javaheap":Ljava/lang/String;
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 158
    :cond_2
    array-length v3, v2

    move v5, v4

    :goto_1
    if-ge v5, v3, :cond_3

    aget-object v8, v2, v5

    .line 159
    .local v8, "nativeheap":Ljava/lang/String;
    invoke-virtual {v8, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 160
    .local v9, "split":[Ljava/lang/String;
    aget-object v10, v9, v7

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    .line 161
    .local v10, "threshold":Ljava/lang/Integer;
    sget-object v11, Lcom/miui/server/sentinel/MiuiSentinelService;->APP_NATIVEHEAP_WHITE_LIST:Ljava/util/HashMap;

    aget-object v12, v9, v4

    invoke-virtual {v11, v12, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    .end local v8    # "nativeheap":Ljava/lang/String;
    .end local v9    # "split":[Ljava/lang/String;
    .end local v10    # "threshold":Ljava/lang/Integer;
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 163
    :cond_3
    return-void
.end method

.method private static native nRegisteredBpfEvent()V
.end method

.method private parseSocketPacket(Ljava/io/BufferedInputStream;)Lcom/miui/server/sentinel/SocketPacket;
    .locals 10
    .param p1, "bufferedInputStream"    # Ljava/io/BufferedInputStream;

    .line 239
    new-instance v0, Lcom/miui/server/sentinel/SocketPacket;

    invoke-direct {v0}, Lcom/miui/server/sentinel/SocketPacket;-><init>()V

    .line 240
    .local v0, "socketPacket":Lcom/miui/server/sentinel/SocketPacket;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/16 v2, 0x2328

    new-array v3, v2, [B

    .line 242
    .local v3, "databuff":[B
    const/16 v4, 0x80

    new-array v5, v4, [B

    .line 244
    .local v5, "splitbuffer":[B
    const/4 v6, 0x4

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {p1, v5, v7, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 245
    invoke-static {v5}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/SocketPacket;->setPid(I)V

    .line 246
    const/16 v6, 0x8

    invoke-virtual {p1, v5, v7, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 247
    invoke-static {v5}, Lcom/miui/server/sentinel/SocketPacket;->readLong([B)J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Lcom/miui/server/sentinel/SocketPacket;->setGrowsize(J)V

    .line 248
    const/16 v6, 0x1e

    invoke-virtual {p1, v5, v7, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 249
    invoke-static {v5, v7, v6}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/SocketPacket;->setEvent_type(Ljava/lang/String;)V

    .line 250
    invoke-virtual {p1, v5, v7, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 251
    invoke-static {v5, v7, v4}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/miui/server/sentinel/SocketPacket;->setProcess_name(Ljava/lang/String;)V

    .line 252
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedInputStream;->available()I

    move-result v4

    if-lez v4, :cond_1

    .line 253
    invoke-virtual {p1, v3}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4

    .line 254
    .local v4, "readsize":I
    if-ne v4, v2, :cond_0

    .line 255
    invoke-static {v3, v7, v2}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 257
    :cond_0
    invoke-static {v3, v7, v4}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .end local v4    # "readsize":I
    :goto_1
    goto :goto_0

    .line 260
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/SocketPacket;->setData(Ljava/lang/String;)V

    .line 261
    sget-boolean v2, Lcom/miui/server/sentinel/MiuiSentinelService;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 262
    const-string v2, "MiuiSentinelService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SocketPacket data = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/SocketPacket;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 266
    :catch_0
    move-exception v2

    .line 267
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 264
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 265
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 268
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    :goto_2
    nop

    .line 269
    :goto_3
    return-object v0
.end method

.method private parseTrackPacket(Ljava/io/BufferedInputStream;)Lcom/miui/server/sentinel/TrackPacket;
    .locals 11
    .param p1, "bufferedInputStream"    # Ljava/io/BufferedInputStream;

    .line 273
    new-instance v0, Lcom/miui/server/sentinel/TrackPacket;

    invoke-direct {v0}, Lcom/miui/server/sentinel/TrackPacket;-><init>()V

    .line 274
    .local v0, "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/16 v2, 0x40

    new-array v3, v2, [B

    .line 277
    .local v3, "splitbuffer":[B
    const/4 v4, 0x4

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 278
    invoke-static {v3}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/TrackPacket;->setReport_id(I)V

    .line 279
    invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 280
    invoke-static {v3}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/miui/server/sentinel/TrackPacket;->setReport_argsz(I)V

    .line 281
    invoke-virtual {p1, v3, v5, v2}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 282
    invoke-static {v3, v5, v2}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setProcess_name(Ljava/lang/String;)V

    .line 283
    invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 284
    invoke-static {v3}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setPid(I)V

    .line 285
    invoke-virtual {p1, v3, v5, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 286
    invoke-static {v3}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setTid(I)V

    .line 287
    const/16 v2, 0xc

    invoke-virtual {p1, v3, v5, v2}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 288
    invoke-static {v3}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v0, v6, v7}, Lcom/miui/server/sentinel/TrackPacket;->setTimestamp(J)V

    .line 289
    const/16 v2, 0x8

    invoke-virtual {p1, v3, v5, v2}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 290
    invoke-static {v3}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/server/sentinel/TrackPacket;->setReport_sz(I)V

    .line 291
    const-string v2, "MiuiSentinelService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "track-heap report_sz:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/miui/server/sentinel/TrackPacket;->getReport_sz()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-virtual {v0}, Lcom/miui/server/sentinel/TrackPacket;->getReport_sz()I

    move-result v2

    .line 293
    .local v2, "chunkSize":I
    const/16 v4, 0x2328

    new-array v6, v4, [B

    .line 294
    .local v6, "databuff":[B
    const/4 v7, 0x0

    .line 296
    .local v7, "totalRead":I
    :goto_0
    if-ge v7, v2, :cond_1

    sub-int v8, v2, v7

    array-length v9, v6

    .line 297
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 296
    invoke-virtual {p1, v6, v5, v8}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v8

    move v9, v8

    .local v9, "readsize":I
    const/4 v10, -0x1

    if-eq v8, v10, :cond_1

    .line 298
    if-ne v9, v4, :cond_0

    .line 299
    invoke-static {v6, v5, v4}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 301
    :cond_0
    invoke-static {v6, v5, v9}, Lcom/miui/server/sentinel/SocketPacket;->readString([BII)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    :goto_1
    add-int/2addr v7, v9

    goto :goto_0

    .line 305
    .end local v9    # "readsize":I
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/miui/server/sentinel/TrackPacket;->setData(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "chunkSize":I
    .end local v6    # "databuff":[B
    .end local v7    # "totalRead":I
    goto :goto_2

    .line 308
    :catch_0
    move-exception v2

    .line 309
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 306
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 307
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 310
    .end local v2    # "e":Ljava/io/IOException;
    :goto_2
    nop

    .line 311
    :goto_3
    return-object v0
.end method

.method private recvMessage(Ljava/io/FileDescriptor;)V
    .locals 6
    .param p1, "fd"    # Ljava/io/FileDescriptor;

    .line 214
    sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelService;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 215
    const-string v0, "MiuiSentinelService"

    const-string v1, "begin recv message"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [B

    .line 218
    .local v0, "splitbuffer":[B
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    .local v1, "fi":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/io/BufferedInputStream;

    const v3, 0x3e8000

    invoke-direct {v2, v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 220
    .local v2, "bufferedInputStream":Ljava/io/BufferedInputStream;
    const/4 v3, 0x0

    const/4 v4, 0x4

    :try_start_2
    invoke-virtual {v2, v0, v3, v4}, Ljava/io/BufferedInputStream;->read([BII)I

    .line 221
    invoke-static {v0}, Lcom/miui/server/sentinel/SocketPacket;->readInt([B)I

    move-result v3

    .line 222
    .local v3, "type":I
    const/16 v4, 0xa

    if-le v3, v4, :cond_1

    .line 223
    invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelService;->parseSocketPacket(Ljava/io/BufferedInputStream;)Lcom/miui/server/sentinel/SocketPacket;

    move-result-object v4

    .line 224
    .local v4, "socketPacket":Lcom/miui/server/sentinel/SocketPacket;
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->filterMessages(Lcom/miui/server/sentinel/SocketPacket;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 225
    invoke-direct {p0, v4}, Lcom/miui/server/sentinel/MiuiSentinelService;->handlerTrigger(Lcom/miui/server/sentinel/SocketPacket;)V

    goto :goto_0

    .line 227
    .end local v4    # "socketPacket":Lcom/miui/server/sentinel/SocketPacket;
    :cond_1
    const/4 v4, 0x6

    if-ne v3, v4, :cond_2

    .line 228
    invoke-direct {p0, v2}, Lcom/miui/server/sentinel/MiuiSentinelService;->parseTrackPacket(Ljava/io/BufferedInputStream;)Lcom/miui/server/sentinel/TrackPacket;

    move-result-object v4

    .line 229
    .local v4, "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
    invoke-direct {p0, v4}, Lcom/miui/server/sentinel/MiuiSentinelService;->handlerTrackMessage(Lcom/miui/server/sentinel/TrackPacket;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 227
    .end local v4    # "trackPacket":Lcom/miui/server/sentinel/TrackPacket;
    :cond_2
    :goto_0
    nop

    .line 231
    .end local v3    # "type":I
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_4

    .line 218
    .restart local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    :catchall_0
    move-exception v3

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v4

    :try_start_6
    invoke-virtual {v3, v4}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "splitbuffer":[B
    .end local v1    # "fi":Ljava/io/FileInputStream;
    .end local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelService;
    .end local p1    # "fd":Ljava/io/FileDescriptor;
    :goto_2
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .end local v2    # "bufferedInputStream":Ljava/io/BufferedInputStream;
    .restart local v0    # "splitbuffer":[B
    .restart local v1    # "fi":Ljava/io/FileInputStream;
    .restart local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelService;
    .restart local p1    # "fd":Ljava/io/FileDescriptor;
    :catchall_2
    move-exception v2

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_3

    :catchall_3
    move-exception v3

    :try_start_8
    invoke-virtual {v2, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "splitbuffer":[B
    .end local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelService;
    .end local p1    # "fd":Ljava/io/FileDescriptor;
    :goto_3
    throw v2
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 233
    .end local v1    # "fi":Ljava/io/FileInputStream;
    .restart local v0    # "splitbuffer":[B
    .restart local p0    # "this":Lcom/miui/server/sentinel/MiuiSentinelService;
    .restart local p1    # "fd":Ljava/io/FileDescriptor;
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 231
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 232
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 235
    .end local v1    # "e":Ljava/io/IOException;
    :goto_4
    nop

    .line 236
    :goto_5
    return-void
.end method
