.class final Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;
.super Landroid/os/Handler;
.source "MiuiSentinelMemoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MiuiSentineHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .line 110
    iput-object p1, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    .line 111
    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 112
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .line 116
    sget-boolean v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->DEBUG:Z

    const-string v1, "MiuiSentinelMemoryManager"

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "information has been received and message.what = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "msg.when = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 118
    invoke-virtual {p1}, Landroid/os/Message;->getWhen()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "System.currentTimeMillis = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 148
    :pswitch_0
    const-string v0, "Unknown message"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 145
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/FdUsageInfo;

    invoke-virtual {v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportFdAmountLeak(Lcom/miui/server/sentinel/FdUsageInfo;)V

    .line 146
    goto :goto_0

    .line 142
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/ThreadUsageInfo;

    invoke-virtual {v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportThreadAmountLeak(Lcom/miui/server/sentinel/ThreadUsageInfo;)V

    .line 143
    goto :goto_0

    .line 139
    :pswitch_3
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/JavaHeapUsageInfo;

    invoke-virtual {v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportJavaHeapLeak(Lcom/miui/server/sentinel/JavaHeapUsageInfo;)V

    .line 140
    goto :goto_0

    .line 136
    :pswitch_4
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    invoke-virtual {v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->reportNativeHeapLeak(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)V

    .line 137
    goto :goto_0

    .line 133
    :pswitch_5
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->-$$Nest$mresumeMemLeak(Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;Ljava/lang/Object;)V

    .line 134
    goto :goto_0

    .line 129
    :pswitch_6
    const-string v0, "end stack track, ready report event"

    invoke-static {v1, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->enableNativeHeapTrack(Lcom/miui/server/sentinel/NativeHeapUsageInfo;Z)V

    .line 131
    goto :goto_0

    .line 124
    :pswitch_7
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    invoke-virtual {v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->isProcessTracked(Lcom/miui/server/sentinel/NativeHeapUsageInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$MiuiSentineHandler;->this$0:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/miui/server/sentinel/NativeHeapUsageInfo;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;->enableNativeHeapTrack(Lcom/miui/server/sentinel/NativeHeapUsageInfo;Z)V

    .line 151
    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
