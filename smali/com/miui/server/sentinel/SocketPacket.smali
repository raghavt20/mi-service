.class public Lcom/miui/server/sentinel/SocketPacket;
.super Ljava/lang/Object;
.source "SocketPacket.java"


# instance fields
.field private data:Ljava/lang/String;

.field private event_type:Ljava/lang/String;

.field private growsize:J

.field private pid:I

.field private process_name:Ljava/lang/String;

.field private tid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readInt([B)I
    .locals 8
    .param p0, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "ret":I
    const/4 v1, 0x0

    .line 77
    .local v1, "i":I
    :goto_0
    const/4 v2, 0x3

    if-gt v1, v2, :cond_0

    .line 78
    rsub-int/lit8 v2, v1, 0x3

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    .line 79
    .local v2, "tmp":I
    int-to-long v3, v0

    int-to-long v5, v2

    mul-int/lit8 v7, v1, 0x8

    rsub-int/lit8 v7, v7, 0x18

    shl-long/2addr v5, v7

    add-long/2addr v3, v5

    long-to-int v0, v3

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    .end local v2    # "tmp":I
    :cond_0
    return v0
.end method

.method public static readLong([B)J
    .locals 7
    .param p0, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 63
    const-wide/16 v0, 0x0

    .line 64
    .local v0, "ret":J
    const/4 v2, 0x0

    .line 65
    .local v2, "i":I
    :goto_0
    const/4 v3, 0x7

    if-gt v2, v3, :cond_0

    .line 66
    rsub-int/lit8 v3, v2, 0x7

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    .line 67
    .local v3, "tmp":I
    int-to-long v4, v3

    mul-int/lit8 v6, v2, 0x8

    rsub-int/lit8 v6, v6, 0x38

    shl-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v3    # "tmp":I
    :cond_0
    return-wide v0
.end method

.method public static readString([BII)Ljava/lang/String;
    .locals 3
    .param p0, "buffer"    # [B
    .param p1, "start"    # I
    .param p2, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .local v0, "sb":Ljava/lang/StringBuilder;
    move v1, p1

    .local v1, "i":I
    :goto_0
    add-int v2, p1, p2

    if-ge v1, v2, :cond_1

    .line 88
    aget-byte v2, p0, v1

    int-to-char v2, v2

    .line 89
    .local v2, "c":C
    if-nez v2, :cond_0

    .line 90
    goto :goto_1

    .line 92
    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 87
    .end local v2    # "c":C
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .end local v1    # "i":I
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getData()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/miui/server/sentinel/SocketPacket;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getEvent_type()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/miui/server/sentinel/SocketPacket;->event_type:Ljava/lang/String;

    return-object v0
.end method

.method public getGrowsize()J
    .locals 2

    .line 22
    iget-wide v0, p0, Lcom/miui/server/sentinel/SocketPacket;->growsize:J

    return-wide v0
.end method

.method public getPid()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/miui/server/sentinel/SocketPacket;->pid:I

    return v0
.end method

.method public getProcess_name()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/miui/server/sentinel/SocketPacket;->process_name:Ljava/lang/String;

    return-object v0
.end method

.method public getTid()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/miui/server/sentinel/SocketPacket;->tid:I

    return v0
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/miui/server/sentinel/SocketPacket;->data:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setEvent_type(Ljava/lang/String;)V
    .locals 0
    .param p1, "event_type"    # Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/miui/server/sentinel/SocketPacket;->event_type:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public setGrowsize(J)V
    .locals 0
    .param p1, "growsize"    # J

    .line 26
    iput-wide p1, p0, Lcom/miui/server/sentinel/SocketPacket;->growsize:J

    .line 27
    return-void
.end method

.method public setPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .line 18
    iput p1, p0, Lcom/miui/server/sentinel/SocketPacket;->pid:I

    .line 19
    return-void
.end method

.method public setProcess_name(Ljava/lang/String;)V
    .locals 0
    .param p1, "process_name"    # Ljava/lang/String;

    .line 42
    iput-object p1, p0, Lcom/miui/server/sentinel/SocketPacket;->process_name:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setTid(I)V
    .locals 0
    .param p1, "tid"    # I

    .line 58
    iput p1, p0, Lcom/miui/server/sentinel/SocketPacket;->tid:I

    .line 59
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SocketPacket info Proc Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Pid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 100
    invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "grow Size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 101
    invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "event_type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 102
    invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "data = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 103
    invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
