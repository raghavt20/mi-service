public class com.miui.server.sentinel.RssUsageInfo extends com.miui.server.sentinel.ProcUsageInfo {
	 /* .source "RssUsageInfo.java" */
	 /* # instance fields */
	 private java.lang.String maxIncrease;
	 private Long rssSize;
	 /* # direct methods */
	 public com.miui.server.sentinel.RssUsageInfo ( ) {
		 /* .locals 0 */
		 /* .line 6 */
		 /* invoke-direct {p0}, Lcom/miui/server/sentinel/ProcUsageInfo;-><init>()V */
		 return;
	 } // .end method
	 /* # virtual methods */
	 public java.lang.String getMaxIncrease ( ) {
		 /* .locals 1 */
		 /* .line 19 */
		 v0 = this.maxIncrease;
	 } // .end method
	 public Long getRssSize ( ) {
		 /* .locals 2 */
		 /* .line 11 */
		 /* iget-wide v0, p0, Lcom/miui/server/sentinel/RssUsageInfo;->rssSize:J */
		 /* return-wide v0 */
	 } // .end method
	 public void setMaxIncrease ( java.lang.String p0 ) {
		 /* .locals 0 */
		 /* .param p1, "maxIncrease" # Ljava/lang/String; */
		 /* .line 23 */
		 this.maxIncrease = p1;
		 /* .line 24 */
		 return;
	 } // .end method
	 public void setRssSize ( Long p0 ) {
		 /* .locals 0 */
		 /* .param p1, "rssSize" # J */
		 /* .line 15 */
		 /* iput-wide p1, p0, Lcom/miui/server/sentinel/RssUsageInfo;->rssSize:J */
		 /* .line 16 */
		 return;
	 } // .end method
	 public java.lang.String toString ( ) {
		 /* .locals 4 */
		 /* .line 27 */
		 /* new-instance v0, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
		 /* .line 28 */
		 /* .local v0, "sb":Ljava/lang/StringBuilder; */
		 /* new-instance v1, Ljava/lang/StringBuilder; */
		 /* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
		 final String v2 = "Proc info Name: "; // const-string v2, "Proc info Name: "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( com.miui.server.sentinel.RssUsageInfo ) p0 ).getName ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/RssUsageInfo;->getName()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 final String v2 = "Pid = "; // const-string v2, "Pid = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = 		 (( com.miui.server.sentinel.RssUsageInfo ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/RssUsageInfo;->getPid()I
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
		 final String v2 = "Rss Size = "; // const-string v2, "Rss Size = "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* iget-wide v2, p0, Lcom/miui/server/sentinel/RssUsageInfo;->rssSize:J */
		 (( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
		 final String v2 = " THE MAX INREASE is "; // const-string v2, " THE MAX INREASE is "
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 v2 = this.maxIncrease;
		 (( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 (( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
		 (( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
		 /* .line 30 */
		 (( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 } // .end method
