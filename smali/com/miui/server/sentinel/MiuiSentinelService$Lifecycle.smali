.class public final Lcom/miui/server/sentinel/MiuiSentinelService$Lifecycle;
.super Lcom/android/server/SystemService;
.source "MiuiSentinelService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/sentinel/MiuiSentinelService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Lifecycle"
.end annotation


# instance fields
.field private final mService:Lcom/miui/server/sentinel/MiuiSentinelService;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 169
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 170
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->getInstance()Lcom/miui/server/sentinel/MiuiSentinelService;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService;->-$$Nest$minitContext(Lcom/miui/server/sentinel/MiuiSentinelService;Landroid/content/Context;)Lcom/miui/server/sentinel/MiuiSentinelService;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/sentinel/MiuiSentinelService$Lifecycle;->mService:Lcom/miui/server/sentinel/MiuiSentinelService;

    .line 171
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .line 175
    sget-boolean v0, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "persist.sys.debug.enable_sentinel_memory_monitor"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->-$$Nest$sfgetmiuiSentinelServiceThread()Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;->start()V

    .line 178
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelService;->-$$Nest$smnRegisteredBpfEvent()V

    .line 179
    const-string v0, "miui.sentinel.service"

    iget-object v1, p0, Lcom/miui/server/sentinel/MiuiSentinelService$Lifecycle;->mService:Lcom/miui/server/sentinel/MiuiSentinelService;

    invoke-virtual {p0, v0, v1}, Lcom/miui/server/sentinel/MiuiSentinelService$Lifecycle;->publishBinderService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 181
    :cond_0
    return-void
.end method
