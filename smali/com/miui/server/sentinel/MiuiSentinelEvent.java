public class com.miui.server.sentinel.MiuiSentinelEvent {
	 /* .source "MiuiSentinelEvent.java" */
	 /* # static fields */
	 public static final Integer EVENT_DMABUFF_LEAK;
	 public static final Integer EVENT_FD_LEAK;
	 public static final Integer EVENT_HEAP_TRACK;
	 public static final Integer EVENT_JAVAHEAP_LEAK;
	 public static final Integer EVENT_KGSL_LEAK;
	 public static final Integer EVENT_NATIVEHEAP_LEAK;
	 public static final Integer EVENT_OPENFILE_AMOUNT;
	 public static final Integer EVENT_OPENFILE_FDSIZE;
	 public static final Integer EVENT_PROCKTHREAD_AMOUNT;
	 public static final Integer EVENT_PROCNATIVE_AMOUNT;
	 public static final Integer EVENT_PROCRUNTIME_AMOUNT;
	 public static final Integer EVENT_PROCTOTAL_AMOUNT;
	 public static final Integer EVENT_PROC_LEAK;
	 public static final Integer EVENT_RESIDENTSIZE;
	 public static final Integer EVENT_RSS_LEAK;
	 public static final Integer EVENT_THREAD_AMOUNT;
	 public static final Integer EVENT_THREAD_LEAK;
	 public static final Integer EVENT_VSSASGMEM;
	 public static final Integer EVENT_VSSDALVIK;
	 public static final Integer EVENT_VSSDMABUFF;
	 public static final Integer EVENT_VSSHEAP;
	 public static final Integer EVENT_VSSKGSL;
	 /* # direct methods */
	 public com.miui.server.sentinel.MiuiSentinelEvent ( ) {
		 /* .locals 0 */
		 /* .line 3 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Integer getEventType ( java.lang.String p0 ) {
		 /* .locals 14 */
		 /* .param p0, "type" # Ljava/lang/String; */
		 /* .line 32 */
		 v0 = 		 (( java.lang.String ) p0 ).hashCode ( ); // invoke-virtual {p0}, Ljava/lang/String;->hashCode()I
		 /* const/16 v1, 0xc */
		 /* const/16 v2, 0xb */
		 /* const/16 v3, 0xa */
		 /* const/16 v4, 0x9 */
		 /* const/16 v5, 0x8 */
		 int v6 = 7; // const/4 v6, 0x7
		 int v7 = 6; // const/4 v7, 0x6
		 int v8 = 5; // const/4 v8, 0x5
		 int v9 = 4; // const/4 v9, 0x4
		 int v10 = 3; // const/4 v10, 0x3
		 int v11 = 2; // const/4 v11, 0x2
		 int v12 = 1; // const/4 v12, 0x1
		 int v13 = -1; // const/4 v13, -0x1
		 /* sparse-switch v0, :sswitch_data_0 */
	 } // :cond_0
	 /* goto/16 :goto_0 */
	 /* :sswitch_0 */
	 final String v0 = "Event_ThreadAmount"; // const-string v0, "Event_ThreadAmount"
	 v0 = 	 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_0
		 /* move v0, v10 */
		 /* goto/16 :goto_1 */
		 /* :sswitch_1 */
		 final String v0 = "Event_ProcKthreadAmount"; // const-string v0, "Event_ProcKthreadAmount"
		 v0 = 		 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* move v0, v2 */
			 /* goto/16 :goto_1 */
			 /* :sswitch_2 */
			 final String v0 = "Event_ProcTotalAmount"; // const-string v0, "Event_ProcTotalAmount"
			 v0 = 			 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
			 if ( v0 != null) { // if-eqz v0, :cond_0
				 /* move v0, v1 */
				 /* goto/16 :goto_1 */
				 /* :sswitch_3 */
				 final String v0 = "Event_ProcRuntimeAmoun"; // const-string v0, "Event_ProcRuntimeAmoun"
				 v0 = 				 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
				 if ( v0 != null) { // if-eqz v0, :cond_0
					 /* move v0, v4 */
					 /* goto/16 :goto_1 */
					 /* :sswitch_4 */
					 final String v0 = "Event_ProcNativeAmount"; // const-string v0, "Event_ProcNativeAmount"
					 v0 = 					 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
					 if ( v0 != null) { // if-eqz v0, :cond_0
						 /* move v0, v3 */
						 /* :sswitch_5 */
						 final String v0 = "Event_OpenFileFDSize"; // const-string v0, "Event_OpenFileFDSize"
						 v0 = 						 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
						 if ( v0 != null) { // if-eqz v0, :cond_0
							 /* move v0, v6 */
							 /* :sswitch_6 */
							 final String v0 = "Event_VssKgsl"; // const-string v0, "Event_VssKgsl"
							 v0 = 							 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
							 if ( v0 != null) { // if-eqz v0, :cond_0
								 int v0 = 0; // const/4 v0, 0x0
								 /* :sswitch_7 */
								 final String v0 = "Event_VssHeap"; // const-string v0, "Event_VssHeap"
								 v0 = 								 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
								 if ( v0 != null) { // if-eqz v0, :cond_0
									 /* move v0, v12 */
									 /* :sswitch_8 */
									 final String v0 = "Event_OpenFileAmount"; // const-string v0, "Event_OpenFileAmount"
									 v0 = 									 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
									 if ( v0 != null) { // if-eqz v0, :cond_0
										 /* move v0, v5 */
										 /* :sswitch_9 */
										 final String v0 = "Event_ResidentSize"; // const-string v0, "Event_ResidentSize"
										 v0 = 										 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
										 if ( v0 != null) { // if-eqz v0, :cond_0
											 /* move v0, v7 */
											 /* :sswitch_a */
											 final String v0 = "Event_VssDmaBuf"; // const-string v0, "Event_VssDmaBuf"
											 v0 = 											 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
											 if ( v0 != null) { // if-eqz v0, :cond_0
												 /* move v0, v8 */
												 /* :sswitch_b */
												 final String v0 = "Event_VssDalvik"; // const-string v0, "Event_VssDalvik"
												 v0 = 												 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
												 if ( v0 != null) { // if-eqz v0, :cond_0
													 /* move v0, v11 */
													 /* :sswitch_c */
													 final String v0 = "Event_VssAshmem"; // const-string v0, "Event_VssAshmem"
													 v0 = 													 (( java.lang.String ) p0 ).equals ( v0 ); // invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
													 if ( v0 != null) { // if-eqz v0, :cond_0
														 /* move v0, v9 */
													 } // :goto_0
													 /* move v0, v13 */
												 } // :goto_1
												 /* packed-switch v0, :pswitch_data_0 */
												 /* .line 60 */
												 /* .line 58 */
												 /* :pswitch_0 */
												 /* const/16 v0, 0xd */
												 /* .line 56 */
												 /* :pswitch_1 */
												 /* .line 54 */
												 /* :pswitch_2 */
												 /* .line 52 */
												 /* :pswitch_3 */
												 /* .line 50 */
												 /* :pswitch_4 */
												 /* .line 48 */
												 /* :pswitch_5 */
												 /* .line 46 */
												 /* :pswitch_6 */
												 /* .line 44 */
												 /* :pswitch_7 */
												 /* .line 42 */
												 /* :pswitch_8 */
												 /* .line 40 */
												 /* :pswitch_9 */
												 /* .line 38 */
												 /* :pswitch_a */
												 /* .line 36 */
												 /* :pswitch_b */
												 /* .line 34 */
												 /* :pswitch_c */
												 /* nop */
												 /* :sswitch_data_0 */
												 /* .sparse-switch */
												 /* -0x72b74710 -> :sswitch_c */
												 /* -0x6e947086 -> :sswitch_b */
												 /* -0x6df11854 -> :sswitch_a */
												 /* -0x6933f4ca -> :sswitch_9 */
												 /* -0x2c8e789d -> :sswitch_8 */
												 /* -0x2965e883 -> :sswitch_7 */
												 /* -0x296481ba -> :sswitch_6 */
												 /* -0x2654e736 -> :sswitch_5 */
												 /* -0x53f52f6 -> :sswitch_4 */
												 /* -0x39ea7a1 -> :sswitch_3 */
												 /* 0x2d23a3a1 -> :sswitch_2 */
												 /* 0x380dbc92 -> :sswitch_1 */
												 /* 0x65d8acc7 -> :sswitch_0 */
											 } // .end sparse-switch
											 /* :pswitch_data_0 */
											 /* .packed-switch 0x0 */
											 /* :pswitch_c */
											 /* :pswitch_b */
											 /* :pswitch_a */
											 /* :pswitch_9 */
											 /* :pswitch_8 */
											 /* :pswitch_7 */
											 /* :pswitch_6 */
											 /* :pswitch_5 */
											 /* :pswitch_4 */
											 /* :pswitch_3 */
											 /* :pswitch_2 */
											 /* :pswitch_1 */
											 /* :pswitch_0 */
										 } // .end packed-switch
									 } // .end method
