.class public Lcom/miui/server/sentinel/TrackPacket;
.super Lcom/miui/server/sentinel/SocketPacket;
.source "TrackPacket.java"


# instance fields
.field report_argsz:I

.field report_id:I

.field report_sz:I

.field timestamp:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/miui/server/sentinel/SocketPacket;-><init>()V

    return-void
.end method


# virtual methods
.method public getReport_argsz()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/miui/server/sentinel/TrackPacket;->report_argsz:I

    return v0
.end method

.method public getReport_id()I
    .locals 1

    .line 9
    iget v0, p0, Lcom/miui/server/sentinel/TrackPacket;->report_id:I

    return v0
.end method

.method public getReport_sz()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/miui/server/sentinel/TrackPacket;->report_sz:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    .line 17
    iget-wide v0, p0, Lcom/miui/server/sentinel/TrackPacket;->timestamp:J

    return-wide v0
.end method

.method public setReport_argsz(I)V
    .locals 0
    .param p1, "report_argsz"    # I

    .line 15
    iput p1, p0, Lcom/miui/server/sentinel/TrackPacket;->report_argsz:I

    return-void
.end method

.method public setReport_id(I)V
    .locals 0
    .param p1, "report_id"    # I

    .line 11
    iput p1, p0, Lcom/miui/server/sentinel/TrackPacket;->report_id:I

    return-void
.end method

.method public setReport_sz(I)V
    .locals 0
    .param p1, "report_sz"    # I

    .line 23
    iput p1, p0, Lcom/miui/server/sentinel/TrackPacket;->report_sz:I

    return-void
.end method

.method public setTimestamp(J)V
    .locals 0
    .param p1, "timestamp"    # J

    .line 19
    iput-wide p1, p0, Lcom/miui/server/sentinel/TrackPacket;->timestamp:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .local v0, "sb":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Track info Proc info Name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/server/sentinel/TrackPacket;->getProcess_name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Pid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 29
    invoke-virtual {p0}, Lcom/miui/server/sentinel/TrackPacket;->getPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "data = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 30
    invoke-virtual {p0}, Lcom/miui/server/sentinel/TrackPacket;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
