class com.miui.server.sentinel.MiuiSentinelService$MiuiSentinelServiceThread extends java.lang.Thread {
	 /* .source "MiuiSentinelService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/sentinel/MiuiSentinelService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x2 */
/* name = "MiuiSentinelServiceThread" */
} // .end annotation
/* # instance fields */
final com.miui.server.sentinel.MiuiSentinelService this$0; //synthetic
/* # direct methods */
private com.miui.server.sentinel.MiuiSentinelService$MiuiSentinelServiceThread ( ) {
/* .locals 0 */
/* .line 76 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Thread;-><init>()V */
return;
} // .end method
 com.miui.server.sentinel.MiuiSentinelService$MiuiSentinelServiceThread ( ) { //synthethic
/* .locals 0 */
/* invoke-direct {p0, p1}, Lcom/miui/server/sentinel/MiuiSentinelService$MiuiSentinelServiceThread;-><init>(Lcom/miui/server/sentinel/MiuiSentinelService;)V */
return;
} // .end method
/* # virtual methods */
public void run ( ) {
/* .locals 4 */
/* .line 80 */
final String v0 = "MiuiSentinelService"; // const-string v0, "MiuiSentinelService"
try { // :try_start_0
	 v1 = this.this$0;
	 com.miui.server.sentinel.MiuiSentinelService .-$$Nest$mcreateSystemServerSocketForProchunter ( v1 );
	 /* .line 81 */
	 /* .local v1, "localSocket":Landroid/net/LocalSocket; */
	 if ( v1 != null) { // if-eqz v1, :cond_0
		 /* .line 82 */
	 } // .end local v1 # "localSocket":Landroid/net/LocalSocket;
	 /* new-instance v2, Ljava/lang/StringBuilder; */
	 /* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
	 final String v3 = "local socekt fd is:"; // const-string v3, "local socekt fd is:"
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
	 (( android.net.LocalSocket ) v1 ).getFileDescriptor ( ); // invoke-virtual {v1}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;
	 (( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
	 (( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
	 android.util.Slog .e ( v0,v2 );
	 /* .line 84 */
} // :goto_0
v2 = this.this$0;
(( android.net.LocalSocket ) v1 ).getFileDescriptor ( ); // invoke-virtual {v1}, Landroid/net/LocalSocket;->getFileDescriptor()Ljava/io/FileDescriptor;
com.miui.server.sentinel.MiuiSentinelService .-$$Nest$mrecvMessage ( v2,v3 );
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 90 */
} // :cond_0
/* .line 87 */
/* :catch_0 */
/* move-exception v1 */
/* .line 88 */
/* .local v1, "e":Ljava/lang/Exception; */
final String v2 = "prochunter_native_sockets connection catch Exception"; // const-string v2, "prochunter_native_sockets connection catch Exception"
android.util.Slog .e ( v0,v2 );
/* .line 89 */
(( java.lang.Exception ) v1 ).printStackTrace ( ); // invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
/* .line 91 */
} // .end local v1 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
