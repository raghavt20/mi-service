.class final enum Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;
.super Ljava/lang/Enum;
.source "MiuiSentinelMemoryManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/sentinel/MiuiSentinelMemoryManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

.field public static final enum REPORT_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

.field public static final enum START_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;


# direct methods
.method private static synthetic $values()[Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;
    .locals 2

    .line 94
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->START_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    sget-object v1, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->REPORT_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    filled-new-array {v0, v1}, [Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    move-result-object v0

    return-object v0
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 95
    new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    const-string v1, "START_TRACK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->START_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    .line 96
    new-instance v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    const-string v1, "REPORT_TRACK"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->REPORT_TRACK:Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    .line 94
    invoke-static {}, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->$values()[Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    move-result-object v0

    sput-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->$VALUES:[Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 94
    const-class v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    return-object v0
.end method

.method public static values()[Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;
    .locals 1

    .line 94
    sget-object v0, Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->$VALUES:[Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    invoke-virtual {v0}, [Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/server/sentinel/MiuiSentinelMemoryManager$Action;

    return-object v0
.end method
