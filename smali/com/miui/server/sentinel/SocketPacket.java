public class com.miui.server.sentinel.SocketPacket {
	 /* .source "SocketPacket.java" */
	 /* # instance fields */
	 private java.lang.String data;
	 private java.lang.String event_type;
	 private Long growsize;
	 private Integer pid;
	 private java.lang.String process_name;
	 private Integer tid;
	 /* # direct methods */
	 public com.miui.server.sentinel.SocketPacket ( ) {
		 /* .locals 0 */
		 /* .line 5 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 public static Integer readInt ( Object[] p0 ) {
		 /* .locals 8 */
		 /* .param p0, "buffer" # [B */
		 /* .annotation system Ldalvik/annotation/Throws; */
		 /* value = { */
		 /* Ljava/io/IOException; */
		 /* } */
	 } // .end annotation
	 /* .line 75 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* .line 76 */
	 /* .local v0, "ret":I */
	 int v1 = 0; // const/4 v1, 0x0
	 /* .line 77 */
	 /* .local v1, "i":I */
} // :goto_0
int v2 = 3; // const/4 v2, 0x3
/* if-gt v1, v2, :cond_0 */
/* .line 78 */
/* rsub-int/lit8 v2, v1, 0x3 */
/* aget-byte v2, p0, v2 */
/* and-int/lit16 v2, v2, 0xff */
/* .line 79 */
/* .local v2, "tmp":I */
/* int-to-long v3, v0 */
/* int-to-long v5, v2 */
/* mul-int/lit8 v7, v1, 0x8 */
/* rsub-int/lit8 v7, v7, 0x18 */
/* shl-long/2addr v5, v7 */
/* add-long/2addr v3, v5 */
/* long-to-int v0, v3 */
/* .line 80 */
/* add-int/lit8 v1, v1, 0x1 */
/* .line 82 */
} // .end local v2 # "tmp":I
} // :cond_0
} // .end method
public static Long readLong ( Object[] p0 ) {
/* .locals 7 */
/* .param p0, "buffer" # [B */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 63 */
/* const-wide/16 v0, 0x0 */
/* .line 64 */
/* .local v0, "ret":J */
int v2 = 0; // const/4 v2, 0x0
/* .line 65 */
/* .local v2, "i":I */
} // :goto_0
int v3 = 7; // const/4 v3, 0x7
/* if-gt v2, v3, :cond_0 */
/* .line 66 */
/* rsub-int/lit8 v3, v2, 0x7 */
/* aget-byte v3, p0, v3 */
/* and-int/lit16 v3, v3, 0xff */
/* .line 67 */
/* .local v3, "tmp":I */
/* int-to-long v4, v3 */
/* mul-int/lit8 v6, v2, 0x8 */
/* rsub-int/lit8 v6, v6, 0x38 */
/* shl-long/2addr v4, v6 */
/* add-long/2addr v0, v4 */
/* .line 68 */
/* add-int/lit8 v2, v2, 0x1 */
/* .line 70 */
} // .end local v3 # "tmp":I
} // :cond_0
/* return-wide v0 */
} // .end method
public static java.lang.String readString ( Object[] p0, Integer p1, Integer p2 ) {
/* .locals 3 */
/* .param p0, "buffer" # [B */
/* .param p1, "start" # I */
/* .param p2, "len" # I */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 86 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 87 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* move v1, p1 */
/* .local v1, "i":I */
} // :goto_0
/* add-int v2, p1, p2 */
/* if-ge v1, v2, :cond_1 */
/* .line 88 */
/* aget-byte v2, p0, v1 */
/* int-to-char v2, v2 */
/* .line 89 */
/* .local v2, "c":C */
/* if-nez v2, :cond_0 */
/* .line 90 */
/* .line 92 */
} // :cond_0
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 87 */
} // .end local v2 # "c":C
/* add-int/lit8 v1, v1, 0x1 */
/* .line 94 */
} // .end local v1 # "i":I
} // :cond_1
} // :goto_1
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
/* # virtual methods */
public java.lang.String getData ( ) {
/* .locals 1 */
/* .line 46 */
v0 = this.data;
} // .end method
public java.lang.String getEvent_type ( ) {
/* .locals 1 */
/* .line 30 */
v0 = this.event_type;
} // .end method
public Long getGrowsize ( ) {
/* .locals 2 */
/* .line 22 */
/* iget-wide v0, p0, Lcom/miui/server/sentinel/SocketPacket;->growsize:J */
/* return-wide v0 */
} // .end method
public Integer getPid ( ) {
/* .locals 1 */
/* .line 14 */
/* iget v0, p0, Lcom/miui/server/sentinel/SocketPacket;->pid:I */
} // .end method
public java.lang.String getProcess_name ( ) {
/* .locals 1 */
/* .line 38 */
v0 = this.process_name;
} // .end method
public Integer getTid ( ) {
/* .locals 1 */
/* .line 54 */
/* iget v0, p0, Lcom/miui/server/sentinel/SocketPacket;->tid:I */
} // .end method
public void setData ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "data" # Ljava/lang/String; */
/* .line 50 */
this.data = p1;
/* .line 51 */
return;
} // .end method
public void setEvent_type ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "event_type" # Ljava/lang/String; */
/* .line 34 */
this.event_type = p1;
/* .line 35 */
return;
} // .end method
public void setGrowsize ( Long p0 ) {
/* .locals 0 */
/* .param p1, "growsize" # J */
/* .line 26 */
/* iput-wide p1, p0, Lcom/miui/server/sentinel/SocketPacket;->growsize:J */
/* .line 27 */
return;
} // .end method
public void setPid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "pid" # I */
/* .line 18 */
/* iput p1, p0, Lcom/miui/server/sentinel/SocketPacket;->pid:I */
/* .line 19 */
return;
} // .end method
public void setProcess_name ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "process_name" # Ljava/lang/String; */
/* .line 42 */
this.process_name = p1;
/* .line 43 */
return;
} // .end method
public void setTid ( Integer p0 ) {
/* .locals 0 */
/* .param p1, "tid" # I */
/* .line 58 */
/* iput p1, p0, Lcom/miui/server/sentinel/SocketPacket;->tid:I */
/* .line 59 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 4 */
/* .line 98 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 99 */
/* .local v0, "sb":Ljava/lang/StringBuilder; */
/* new-instance v1, Ljava/lang/StringBuilder; */
/* invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "SocketPacket info Proc Name: "; // const-string v2, "SocketPacket info Proc Name: "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( com.miui.server.sentinel.SocketPacket ) p0 ).getProcess_name ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getProcess_name()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "Pid = "; // const-string v2, "Pid = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 100 */
v2 = (( com.miui.server.sentinel.SocketPacket ) p0 ).getPid ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getPid()I
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v2 = "grow Size = "; // const-string v2, "grow Size = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 101 */
(( com.miui.server.sentinel.SocketPacket ) p0 ).getGrowsize ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getGrowsize()J
/* move-result-wide v2 */
(( java.lang.StringBuilder ) v1 ).append ( v2, v3 ); // invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
final String v2 = "event_type = "; // const-string v2, "event_type = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 102 */
(( com.miui.server.sentinel.SocketPacket ) p0 ).getEvent_type ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getEvent_type()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v2 = "data = "; // const-string v2, "data = "
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 103 */
(( com.miui.server.sentinel.SocketPacket ) p0 ).getData ( ); // invoke-virtual {p0}, Lcom/miui/server/sentinel/SocketPacket;->getData()Ljava/lang/String;
(( java.lang.StringBuilder ) v1 ).append ( v2 ); // invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v1 ).toString ( ); // invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 99 */
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 104 */
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
