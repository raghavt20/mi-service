class com.miui.server.rescue.ContactsHelper$ContactsBean {
	 /* .source "ContactsHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/rescue/ContactsHelper; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "ContactsBean" */
} // .end annotation
/* # instance fields */
private java.lang.String name;
private java.util.ArrayList numList;
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
} // .end field
final com.miui.server.rescue.ContactsHelper this$0; //synthetic
/* # direct methods */
public com.miui.server.rescue.ContactsHelper$ContactsBean ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/rescue/ContactsHelper; */
/* .line 32 */
this.this$0 = p1;
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 33 */
com.google.android.collect.Lists .newArrayList ( );
this.numList = v0;
/* .line 34 */
return;
} // .end method
public com.miui.server.rescue.ContactsHelper$ContactsBean ( ) {
/* .locals 1 */
/* .param p1, "this$0" # Lcom/miui/server/rescue/ContactsHelper; */
/* .param p2, "displayName" # Ljava/lang/String; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/lang/String;", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 35 */
/* .local p3, "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
this.this$0 = p1;
/* .line 36 */
/* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
/* .line 37 */
this.name = p2;
/* .line 38 */
/* if-nez p3, :cond_0 */
/* .line 39 */
com.google.android.collect.Lists .newArrayList ( );
this.numList = v0;
/* .line 41 */
} // :cond_0
this.numList = p3;
/* .line 43 */
} // :goto_0
return;
} // .end method
/* # virtual methods */
public java.lang.String getName ( ) {
/* .locals 1 */
/* .line 21 */
v0 = this.name;
} // .end method
public java.util.ArrayList getNumList ( ) {
/* .locals 1 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "()", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 27 */
v0 = this.numList;
} // .end method
public void setName ( java.lang.String p0 ) {
/* .locals 0 */
/* .param p1, "name" # Ljava/lang/String; */
/* .line 24 */
this.name = p1;
/* .line 25 */
return;
} // .end method
public void setNumList ( java.util.ArrayList p0 ) {
/* .locals 0 */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Ljava/util/ArrayList<", */
/* "Ljava/lang/String;", */
/* ">;)V" */
/* } */
} // .end annotation
/* .line 30 */
/* .local p1, "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;" */
this.numList = p1;
/* .line 31 */
return;
} // .end method
public java.lang.String toString ( ) {
/* .locals 2 */
/* .line 46 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v1 = "ContactsBean [name="; // const-string v1, "ContactsBean [name="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.name;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
final String v1 = ", numList="; // const-string v1, ", numList="
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
v1 = this.numList;
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
final String v1 = "]"; // const-string v1, "]"
(( java.lang.StringBuilder ) v0 ).append ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
