.class Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;
.super Landroid/os/Handler;
.source "AutoUsbDebuggingManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rescue/AutoUsbDebuggingManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UsbDebuggingHandler"
.end annotation


# static fields
.field private static final MESSAGE_ADB_ALLOW:I = 0x3

.field private static final MESSAGE_ADB_DISABLED:I = 0x2

.field private static final MESSAGE_ADB_ENABLED:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;


# direct methods
.method public constructor <init>(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 41
    iput-object p1, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    .line 42
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 43
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .line 46
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "BrokenScreenRescueService"

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UsbDebuggingHandler msg "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 48
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 50
    .local v0, "key":Ljava/lang/String;
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v2, :cond_0

    .line 51
    iget-object v1, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-static {v1, v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$mwriteKey(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->sendResponse(Ljava/lang/String;)V

    .line 54
    goto :goto_1

    .line 66
    .end local v0    # "key":Ljava/lang/String;
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fgetmAdbEnabled(Lcom/miui/server/rescue/AutoUsbDebuggingManager;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    goto :goto_1

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fputmAdbEnabled(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Z)V

    .line 70
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-virtual {v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->closeSocket()V

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fgetmThread(Lcom/miui/server/rescue/AutoUsbDebuggingManager;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "UsbDebuggingHandler MESSAGE_ADB_DISABLED join failed"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_0
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fputmThread(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Ljava/lang/Thread;)V

    .line 77
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    iput-object v1, v0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    .line 78
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    iput-object v1, v0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    .line 79
    goto :goto_1

    .line 57
    :pswitch_2
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fgetmAdbEnabled(Lcom/miui/server/rescue/AutoUsbDebuggingManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    goto :goto_1

    .line 59
    :cond_2
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-static {v0, v2}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fputmAdbEnabled(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Z)V

    .line 60
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {v0, v1}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fputmThread(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Ljava/lang/Thread;)V

    .line 61
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;->this$0:Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    invoke-static {v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->-$$Nest$fgetmThread(Lcom/miui/server/rescue/AutoUsbDebuggingManager;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 62
    nop

    .line 85
    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
