class com.miui.server.rescue.AutoUsbDebuggingManager$UsbDebuggingHandler extends android.os.Handler {
	 /* .source "AutoUsbDebuggingManager.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/rescue/AutoUsbDebuggingManager; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = "UsbDebuggingHandler" */
} // .end annotation
/* # static fields */
private static final Integer MESSAGE_ADB_ALLOW;
private static final Integer MESSAGE_ADB_DISABLED;
private static final Integer MESSAGE_ADB_ENABLED;
/* # instance fields */
final com.miui.server.rescue.AutoUsbDebuggingManager this$0; //synthetic
/* # direct methods */
public com.miui.server.rescue.AutoUsbDebuggingManager$UsbDebuggingHandler ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/rescue/AutoUsbDebuggingManager; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 41 */
this.this$0 = p1;
/* .line 42 */
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
/* .line 43 */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 46 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
int v2 = 1; // const/4 v2, 0x1
/* packed-switch v0, :pswitch_data_0 */
/* .line 83 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "UsbDebuggingHandler msg "; // const-string v2, "UsbDebuggingHandler msg "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 48 */
/* :pswitch_0 */
v0 = this.obj;
/* check-cast v0, Ljava/lang/String; */
/* .line 50 */
/* .local v0, "key":Ljava/lang/String; */
/* iget v1, p1, Landroid/os/Message;->arg1:I */
/* if-ne v1, v2, :cond_0 */
/* .line 51 */
v1 = this.this$0;
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$mwriteKey ( v1,v0 );
/* .line 53 */
} // :cond_0
v1 = this.this$0;
final String v2 = "OK"; // const-string v2, "OK"
(( com.miui.server.rescue.AutoUsbDebuggingManager ) v1 ).sendResponse ( v2 ); // invoke-virtual {v1, v2}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->sendResponse(Ljava/lang/String;)V
/* .line 54 */
/* .line 66 */
} // .end local v0 # "key":Ljava/lang/String;
/* :pswitch_1 */
v0 = this.this$0;
v0 = com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fgetmAdbEnabled ( v0 );
/* if-nez v0, :cond_1 */
/* .line 67 */
/* .line 69 */
} // :cond_1
v0 = this.this$0;
int v2 = 0; // const/4 v2, 0x0
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fputmAdbEnabled ( v0,v2 );
/* .line 70 */
v0 = this.this$0;
(( com.miui.server.rescue.AutoUsbDebuggingManager ) v0 ).closeSocket ( ); // invoke-virtual {v0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->closeSocket()V
/* .line 72 */
try { // :try_start_0
v0 = this.this$0;
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fgetmThread ( v0 );
(( java.lang.Thread ) v0 ).join ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->join()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 75 */
/* .line 73 */
/* :catch_0 */
/* move-exception v0 */
/* .line 74 */
/* .local v0, "ex":Ljava/lang/Exception; */
final String v2 = "UsbDebuggingHandler MESSAGE_ADB_DISABLED join failed"; // const-string v2, "UsbDebuggingHandler MESSAGE_ADB_DISABLED join failed"
android.util.Slog .e ( v1,v2 );
/* .line 76 */
} // .end local v0 # "ex":Ljava/lang/Exception;
} // :goto_0
v0 = this.this$0;
int v1 = 0; // const/4 v1, 0x0
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fputmThread ( v0,v1 );
/* .line 77 */
v0 = this.this$0;
this.mOutputStream = v1;
/* .line 78 */
v0 = this.this$0;
this.mSocket = v1;
/* .line 79 */
/* .line 57 */
/* :pswitch_2 */
v0 = this.this$0;
v0 = com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fgetmAdbEnabled ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_2
/* .line 58 */
/* .line 59 */
} // :cond_2
v0 = this.this$0;
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fputmAdbEnabled ( v0,v2 );
/* .line 60 */
v0 = this.this$0;
/* new-instance v1, Ljava/lang/Thread; */
v2 = this.this$0;
/* invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V */
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fputmThread ( v0,v1 );
/* .line 61 */
v0 = this.this$0;
com.miui.server.rescue.AutoUsbDebuggingManager .-$$Nest$fgetmThread ( v0 );
(( java.lang.Thread ) v0 ).start ( ); // invoke-virtual {v0}, Ljava/lang/Thread;->start()V
/* .line 62 */
/* nop */
/* .line 85 */
} // :goto_1
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
