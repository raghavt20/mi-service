public class com.miui.server.rescue.LockServicesCompat {
	 /* .source "LockServicesCompat.java" */
	 /* # instance fields */
	 private com.android.internal.widget.LockPatternUtils mLockPatternUtils;
	 private com.android.internal.widget.ILockSettings mLockSettingsService;
	 /* # direct methods */
	 public com.miui.server.rescue.LockServicesCompat ( ) {
		 /* .locals 1 */
		 /* .param p1, "context" # Landroid/content/Context; */
		 /* .line 20 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 /* .line 21 */
		 /* new-instance v0, Lcom/android/internal/widget/LockPatternUtils; */
		 /* invoke-direct {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;-><init>(Landroid/content/Context;)V */
		 this.mLockPatternUtils = v0;
		 /* .line 22 */
		 return;
	 } // .end method
	 private com.android.internal.widget.LockscreenCredential getCredential ( java.lang.String p0, Integer p1 ) {
		 /* .locals 2 */
		 /* .param p1, "password" # Ljava/lang/String; */
		 /* .param p2, "currentUserId" # I */
		 /* .line 34 */
		 v0 = 		 android.text.TextUtils .isEmpty ( p1 );
		 if ( v0 != null) { // if-eqz v0, :cond_0
			 /* .line 35 */
			 com.android.internal.widget.LockscreenCredential .createNone ( );
			 /* .line 37 */
		 } // :cond_0
		 v0 = this.mLockPatternUtils;
		 v0 = 		 (( com.android.internal.widget.LockPatternUtils ) v0 ).isLockPasswordEnabled ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled(I)Z
		 if ( v0 != null) { // if-eqz v0, :cond_2
			 /* .line 38 */
			 v0 = this.mLockPatternUtils;
			 v0 = 			 (( com.android.internal.widget.LockPatternUtils ) v0 ).getKeyguardStoredPasswordQuality ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality(I)I
			 /* .line 39 */
			 /* .local v0, "quality":I */
			 v1 = 			 com.android.internal.widget.LockPatternUtils .isQualityAlphabeticPassword ( v0 );
			 if ( v1 != null) { // if-eqz v1, :cond_1
				 /* .line 40 */
				 com.android.internal.widget.LockscreenCredential .createPassword ( p1 );
				 /* .line 42 */
			 } // :cond_1
			 com.android.internal.widget.LockscreenCredential .createPin ( p1 );
			 /* .line 45 */
		 } // .end local v0 # "quality":I
	 } // :cond_2
	 v0 = this.mLockPatternUtils;
	 v0 = 	 (( com.android.internal.widget.LockPatternUtils ) v0 ).isLockPatternEnabled ( p2 ); // invoke-virtual {v0, p2}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled(I)Z
	 if ( v0 != null) { // if-eqz v0, :cond_3
		 /* .line 46 */
		 /* nop */
		 /* .line 47 */
		 (( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
		 /* .line 46 */
		 com.android.internal.widget.LockPatternUtils .byteArrayToPattern ( v0 );
		 com.android.internal.widget.LockscreenCredential .createPattern ( v0 );
		 /* .line 51 */
	 } // :cond_3
	 com.android.internal.widget.LockscreenCredential .createPassword ( p1 );
} // .end method
private com.android.internal.widget.ILockSettings getLockSettings ( ) {
	 /* .locals 1 */
	 /* .line 25 */
	 v0 = this.mLockSettingsService;
	 /* if-nez v0, :cond_0 */
	 /* .line 26 */
	 /* nop */
	 /* .line 27 */
	 final String v0 = "lock_settings"; // const-string v0, "lock_settings"
	 android.os.ServiceManager .getService ( v0 );
	 /* .line 26 */
	 com.android.internal.widget.ILockSettings$Stub .asInterface ( v0 );
	 /* .line 28 */
	 /* .local v0, "service":Lcom/android/internal/widget/ILockSettings; */
	 this.mLockSettingsService = v0;
	 /* .line 30 */
} // .end local v0 # "service":Lcom/android/internal/widget/ILockSettings;
} // :cond_0
v0 = this.mLockSettingsService;
} // .end method
private Integer verifyCredential ( com.android.internal.widget.LockscreenCredential p0, Integer p1, Integer p2 ) {
/* .locals 5 */
/* .param p1, "credential" # Lcom/android/internal/widget/LockscreenCredential; */
/* .param p2, "challenge" # I */
/* .param p3, "userId" # I */
/* .line 66 */
final String v0 = "BrokenScreenRescueService"; // const-string v0, "BrokenScreenRescueService"
int v1 = -1; // const/4 v1, -0x1
try { // :try_start_0
/* invoke-direct {p0}, Lcom/miui/server/rescue/LockServicesCompat;->getLockSettings()Lcom/android/internal/widget/ILockSettings; */
/* .line 68 */
/* .local v2, "response":Lcom/android/internal/widget/VerifyCredentialResponse; */
v3 = (( com.android.internal.widget.VerifyCredentialResponse ) v2 ).getResponseCode ( ); // invoke-virtual {v2}, Lcom/android/internal/widget/VerifyCredentialResponse;->getResponseCode()I
/* if-nez v3, :cond_0 */
/* .line 69 */
int v0 = 0; // const/4 v0, 0x0
/* .line 70 */
} // :cond_0
v3 = (( com.android.internal.widget.VerifyCredentialResponse ) v2 ).getResponseCode ( ); // invoke-virtual {v2}, Lcom/android/internal/widget/VerifyCredentialResponse;->getResponseCode()I
int v4 = 1; // const/4 v4, 0x1
/* if-ne v3, v4, :cond_1 */
/* .line 76 */
/* new-instance v3, Ljava/lang/StringBuilder; */
/* invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V */
final String v4 = "LockscreenService lock the service because of too many passsword errors.\nPlease wait "; // const-string v4, "LockscreenService lock the service because of too many passsword errors.\nPlease wait "
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* .line 77 */
v4 = (( com.android.internal.widget.VerifyCredentialResponse ) v2 ).getTimeout ( ); // invoke-virtual {v2}, Lcom/android/internal/widget/VerifyCredentialResponse;->getTimeout()I
/* div-int/lit16 v4, v4, 0x3e8 */
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v4 = " seconds and try again!"; // const-string v4, " seconds and try again!"
(( java.lang.StringBuilder ) v3 ).append ( v4 ); // invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v3 ).toString ( ); // invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 76 */
android.util.Slog .e ( v0,v3 );
/* .line 78 */
v0 = (( com.android.internal.widget.VerifyCredentialResponse ) v2 ).getTimeout ( ); // invoke-virtual {v2}, Lcom/android/internal/widget/VerifyCredentialResponse;->getTimeout()I
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 80 */
} // :cond_1
/* .line 82 */
} // .end local v2 # "response":Lcom/android/internal/widget/VerifyCredentialResponse;
/* :catch_0 */
/* move-exception v2 */
/* .line 83 */
/* .local v2, "re":Ljava/lang/Exception; */
final String v3 = "failed to verify credential"; // const-string v3, "failed to verify credential"
android.util.Slog .e ( v0,v3,v2 );
/* .line 84 */
} // .end method
/* # virtual methods */
public Integer verifyCredentials ( java.lang.String p0, Integer p1, Integer p2 ) {
/* .locals 2 */
/* .param p1, "pin" # Ljava/lang/String; */
/* .param p2, "challenge" # I */
/* .param p3, "userId" # I */
/* .line 56 */
v0 = this.mLockPatternUtils;
v0 = (( com.android.internal.widget.LockPatternUtils ) v0 ).isSecure ( p3 ); // invoke-virtual {v0, p3}, Lcom/android/internal/widget/LockPatternUtils;->isSecure(I)Z
/* if-nez v0, :cond_0 */
final String v0 = "nopasswd"; // const-string v0, "nopasswd"
v0 = (( java.lang.String ) p1 ).equals ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 57 */
int v0 = 0; // const/4 v0, 0x0
/* .line 59 */
} // :cond_0
/* invoke-direct {p0, p1, p3}, Lcom/miui/server/rescue/LockServicesCompat;->getCredential(Ljava/lang/String;I)Lcom/android/internal/widget/LockscreenCredential; */
/* .line 60 */
/* .local v0, "lkCredential":Lcom/android/internal/widget/LockscreenCredential; */
v1 = /* invoke-direct {p0, v0, p2, p3}, Lcom/miui/server/rescue/LockServicesCompat;->verifyCredential(Lcom/android/internal/widget/LockscreenCredential;II)I */
} // .end method
