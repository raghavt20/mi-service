public class com.miui.server.rescue.BrokenScreenRescueService extends com.android.server.SystemService {
	 /* .source "BrokenScreenRescueService.java" */
	 /* # static fields */
	 private static final java.lang.String BROLEN_SCREEN_RESCUE_SERVICE_RUNTIME;
	 public static final Boolean DEBUG;
	 public static final Integer MSG_RECEIVE_SMS;
	 public static final Integer MSG_UPDATE_CONTACTS;
	 private static final java.lang.String PIN_ERROR_COUNT;
	 private static final java.lang.String PIN_REGEX;
	 public static final java.lang.String SERVICE;
	 private static final java.lang.String SERVICE_ENABLE_PROP;
	 public static final java.lang.String TAG;
	 private static final java.util.concurrent.atomic.AtomicInteger sUsbOperationCount;
	 /* # instance fields */
	 private java.util.ArrayList mContacts;
	 /* .annotation system Ldalvik/annotation/Signature; */
	 /* value = { */
	 /* "Ljava/util/ArrayList<", */
	 /* "Lcom/miui/server/rescue/ContactsHelper$ContactsBean;", */
	 /* ">;" */
	 /* } */
} // .end annotation
} // .end field
private android.database.ContentObserver mContactsContentObserver;
private com.miui.server.rescue.ContactsHelper mContactsHelper;
private Boolean mContentEvent;
private android.content.Context mContext;
private android.os.Handler mHandler;
private android.os.HandlerThread mHandlerThread;
private java.lang.String mLastMsmUri;
private com.miui.server.rescue.LockServicesCompat mLockServicesCompat;
private Long mLockStartTime;
private Long mLockTime;
private android.database.ContentObserver mMsgContentObserver;
private java.lang.String mMsgbody;
private java.util.regex.Pattern mPinPattern;
private android.content.ContentResolver mResolver;
private android.hardware.usb.IUsbManager mUsbManager;
private Long mlastMsmEventTime;
/* # direct methods */
static android.os.Handler -$$Nest$fgetmHandler ( com.miui.server.rescue.BrokenScreenRescueService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mHandler;
} // .end method
static java.lang.String -$$Nest$fgetmLastMsmUri ( com.miui.server.rescue.BrokenScreenRescueService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = this.mLastMsmUri;
} // .end method
static Long -$$Nest$fgetmlastMsmEventTime ( com.miui.server.rescue.BrokenScreenRescueService p0 ) { //bridge//synthethic
/* .locals 2 */
/* iget-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mlastMsmEventTime:J */
/* return-wide v0 */
} // .end method
static void -$$Nest$fputmContentEvent ( com.miui.server.rescue.BrokenScreenRescueService p0, Boolean p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-boolean p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContentEvent:Z */
return;
} // .end method
static void -$$Nest$fputmLastMsmUri ( com.miui.server.rescue.BrokenScreenRescueService p0, java.lang.String p1 ) { //bridge//synthethic
/* .locals 0 */
this.mLastMsmUri = p1;
return;
} // .end method
static void -$$Nest$fputmlastMsmEventTime ( com.miui.server.rescue.BrokenScreenRescueService p0, Long p1 ) { //bridge//synthethic
/* .locals 0 */
/* iput-wide p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mlastMsmEventTime:J */
return;
} // .end method
static Boolean -$$Nest$mcheckDisableBrokenScreenService ( com.miui.server.rescue.BrokenScreenRescueService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkDisableBrokenScreenService()Z */
} // .end method
static void -$$Nest$mcheckSendSecurityPasswd ( com.miui.server.rescue.BrokenScreenRescueService p0 ) { //bridge//synthethic
/* .locals 0 */
/* invoke-direct {p0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkSendSecurityPasswd()V */
return;
} // .end method
static Boolean -$$Nest$mmatchContacts ( com.miui.server.rescue.BrokenScreenRescueService p0 ) { //bridge//synthethic
/* .locals 0 */
p0 = /* invoke-direct {p0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->matchContacts()Z */
} // .end method
static com.miui.server.rescue.BrokenScreenRescueService ( ) {
/* .locals 1 */
/* .line 40 */
/* new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger; */
/* invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V */
return;
} // .end method
public com.miui.server.rescue.BrokenScreenRescueService ( ) {
/* .locals 6 */
/* .param p1, "context" # Landroid/content/Context; */
/* .line 221 */
/* invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V */
/* .line 43 */
int v0 = 0; // const/4 v0, 0x0
this.mUsbManager = v0;
/* .line 44 */
this.mHandlerThread = v0;
/* .line 222 */
/* new-instance v0, Lcom/miui/server/rescue/ContactsHelper; */
/* invoke-direct {v0}, Lcom/miui/server/rescue/ContactsHelper;-><init>()V */
this.mContactsHelper = v0;
/* .line 223 */
/* new-instance v0, Landroid/os/HandlerThread; */
final String v1 = "BrokenScreenHandler"; // const-string v1, "BrokenScreenHandler"
/* invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V */
this.mHandlerThread = v0;
/* .line 224 */
(( android.os.HandlerThread ) v0 ).start ( ); // invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V
/* .line 225 */
this.mContext = p1;
/* .line 226 */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
this.mResolver = v0;
/* .line 227 */
/* const-string/jumbo v0, "usb" */
android.os.ServiceManager .getService ( v0 );
android.hardware.usb.IUsbManager$Stub .asInterface ( v0 );
this.mUsbManager = v0;
/* .line 228 */
/* new-instance v0, Lcom/miui/server/rescue/LockServicesCompat; */
v1 = this.mContext;
/* invoke-direct {v0, v1}, Lcom/miui/server/rescue/LockServicesCompat;-><init>(Landroid/content/Context;)V */
this.mLockServicesCompat = v0;
/* .line 229 */
final String v0 = "^\\*#\\*#(([0-9a-zA-Z\\S\\s]{4,16}))#\\*#\\*$"; // const-string v0, "^\\*#\\*#(([0-9a-zA-Z\\S\\s]{4,16}))#\\*#\\*$"
java.util.regex.Pattern .compile ( v0 );
this.mPinPattern = v0;
/* .line 230 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* iput-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mlastMsmEventTime:J */
/* .line 232 */
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 234 */
/* .local v0, "now":J */
v2 = this.mResolver;
final String v3 = "brokenscreenrescue_start_time"; // const-string v3, "brokenscreenrescue_start_time"
/* const-wide/16 v4, 0x0 */
android.provider.Settings$System .getLong ( v2,v3,v4,v5 );
/* move-result-wide v2 */
/* cmp-long v2, v2, v0 */
/* if-lez v2, :cond_0 */
/* .line 235 */
v2 = this.mResolver;
final String v3 = "brokenscreenrescue_pin_error_count"; // const-string v3, "brokenscreenrescue_pin_error_count"
int v4 = 0; // const/4 v4, 0x0
android.provider.Settings$System .putInt ( v2,v3,v4 );
/* .line 238 */
} // :cond_0
/* new-instance v2, Lcom/miui/server/rescue/BrokenScreenRescueService$1; */
v3 = this.mHandlerThread;
(( android.os.HandlerThread ) v3 ).getLooper ( ); // invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService$1;-><init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Looper;)V */
this.mHandler = v2;
/* .line 267 */
int v3 = 2; // const/4 v3, 0x2
(( android.os.Handler ) v2 ).sendEmptyMessage ( v3 ); // invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 268 */
/* new-instance v2, Lcom/miui/server/rescue/BrokenScreenRescueService$2; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService$2;-><init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Handler;)V */
this.mMsgContentObserver = v2;
/* .line 280 */
/* new-instance v2, Lcom/miui/server/rescue/BrokenScreenRescueService$3; */
v3 = this.mHandler;
/* invoke-direct {v2, p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService$3;-><init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Handler;)V */
this.mContactsContentObserver = v2;
/* .line 287 */
v2 = this.mResolver;
final String v3 = "content://sms/"; // const-string v3, "content://sms/"
android.net.Uri .parse ( v3 );
v4 = this.mMsgContentObserver;
int v5 = 1; // const/4 v5, 0x1
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v5, v4 ); // invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 288 */
v2 = this.mResolver;
v3 = android.provider.ContactsContract$Contacts.CONTENT_URI;
v4 = this.mContactsContentObserver;
(( android.content.ContentResolver ) v2 ).registerContentObserver ( v3, v5, v4 ); // invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
/* .line 289 */
return;
} // .end method
private Boolean checkDisableBrokenScreenService ( ) {
/* .locals 2 */
/* .line 61 */
final String v0 = "persist.sys.brokenscreenservice.disable"; // const-string v0, "persist.sys.brokenscreenservice.disable"
int v1 = 0; // const/4 v1, 0x0
v0 = android.os.SystemProperties .getBoolean ( v0,v1 );
} // .end method
private Boolean checkPinErrorCountAndLock ( Integer p0, Long p1 ) {
/* .locals 6 */
/* .param p1, "errorCount" # I */
/* .param p2, "now" # J */
/* .line 132 */
if ( p1 != null) { // if-eqz p1, :cond_1
/* rem-int/lit8 v0, p1, 0x5 */
/* if-nez v0, :cond_1 */
/* iget-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* const-wide/16 v2, 0x0 */
/* cmp-long v0, v0, v2 */
/* if-nez v0, :cond_1 */
/* .line 133 */
/* iput-wide p2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockStartTime:J */
/* .line 134 */
/* div-int/lit8 v0, p1, 0x5 */
/* int-to-double v0, v0 */
/* const-wide/high16 v4, 0x4000000000000000L # 2.0 */
java.lang.Math .pow ( v4,v5,v0,v1 );
/* move-result-wide v0 */
/* double-to-long v0, v0 */
/* const-wide/16 v4, 0x3c */
/* mul-long/2addr v0, v4 */
/* const-wide/16 v4, 0x3e8 */
/* mul-long/2addr v0, v4 */
/* iput-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* .line 135 */
/* cmp-long v0, v0, v2 */
/* if-gez v0, :cond_0 */
/* .line 136 */
/* const-wide v0, 0x7fffffffffffffffL */
/* iput-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* .line 138 */
} // :cond_0
int v0 = 1; // const/4 v0, 0x1
/* .line 140 */
} // :cond_1
int v0 = 0; // const/4 v0, 0x0
} // .end method
private void checkSendSecurityPasswd ( ) {
/* .locals 3 */
/* .line 144 */
v0 = this.mPinPattern;
v1 = this.mMsgbody;
(( java.util.regex.Pattern ) v0 ).matcher ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 146 */
/* .local v0, "matcher":Ljava/util/regex/Matcher; */
v1 = (( java.util.regex.Matcher ) v0 ).find ( ); // invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z
if ( v1 != null) { // if-eqz v1, :cond_0
v1 = (( java.util.regex.Matcher ) v0 ).groupCount ( ); // invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I
int v2 = 2; // const/4 v2, 0x2
/* if-ne v1, v2, :cond_0 */
/* .line 147 */
int v1 = 1; // const/4 v1, 0x1
(( java.util.regex.Matcher ) v0 ).group ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
/* .line 149 */
/* .local v1, "pin":Ljava/lang/String; */
/* invoke-direct {p0, v1}, Lcom/miui/server/rescue/BrokenScreenRescueService;->unlockAndSetUsb(Ljava/lang/String;)V */
/* .line 151 */
} // .end local v1 # "pin":Ljava/lang/String;
} // :cond_0
return;
} // .end method
private Boolean matchContacts ( ) {
/* .locals 11 */
/* .line 155 */
final String v0 = "content://sms/inbox"; // const-string v0, "content://sms/inbox"
android.net.Uri .parse ( v0 );
/* .line 156 */
/* .local v0, "inboxUri":Landroid/net/Uri; */
v1 = this.mContext;
(( android.content.Context ) v1 ).getContentResolver ( ); // invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v3 = 0; // const/4 v3, 0x0
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
final String v6 = "date desc"; // const-string v6, "date desc"
/* move-object v2, v0 */
/* invoke-virtual/range {v1 ..v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 157 */
/* .local v1, "c":Landroid/database/Cursor; */
int v2 = 0; // const/4 v2, 0x0
v3 = if ( v1 != null) { // if-eqz v1, :cond_5
if ( v3 != null) { // if-eqz v3, :cond_5
/* .line 158 */
v3 = final String v3 = "address"; // const-string v3, "address"
/* .line 159 */
/* .local v3, "address":Ljava/lang/String; */
v4 = final String v4 = "body"; // const-string v4, "body"
/* .line 160 */
/* .local v4, "body":Ljava/lang/String; */
/* invoke-direct {p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService;->only11Number(Ljava/lang/String;)Ljava/lang/String; */
/* .line 163 */
v5 = this.mPinPattern;
(( java.util.regex.Pattern ) v5 ).matcher ( v4 ); // invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
/* .line 165 */
/* .local v5, "matcher":Ljava/util/regex/Matcher; */
v6 = (( java.util.regex.Matcher ) v5 ).find ( ); // invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z
if ( v6 != null) { // if-eqz v6, :cond_4
v6 = (( java.util.regex.Matcher ) v5 ).groupCount ( ); // invoke-virtual {v5}, Ljava/util/regex/Matcher;->groupCount()I
int v7 = 2; // const/4 v7, 0x2
/* if-eq v6, v7, :cond_0 */
/* .line 171 */
} // :cond_0
/* iget-boolean v6, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContentEvent:Z */
if ( v6 != null) { // if-eqz v6, :cond_1
/* .line 172 */
v6 = this.mContactsHelper;
v7 = this.mContext;
(( com.miui.server.rescue.ContactsHelper ) v6 ).getContactsList ( v7 ); // invoke-virtual {v6, v7}, Lcom/miui/server/rescue/ContactsHelper;->getContactsList(Landroid/content/Context;)Ljava/util/ArrayList;
this.mContacts = v6;
/* .line 173 */
/* iput-boolean v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContentEvent:Z */
/* .line 176 */
} // :cond_1
v6 = this.mContacts;
(( java.util.ArrayList ) v6 ).iterator ( ); // invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v7 = } // :goto_0
if ( v7 != null) { // if-eqz v7, :cond_5
/* check-cast v7, Lcom/miui/server/rescue/ContactsHelper$ContactsBean; */
/* .line 177 */
/* .local v7, "contact":Lcom/miui/server/rescue/ContactsHelper$ContactsBean; */
(( com.miui.server.rescue.ContactsHelper$ContactsBean ) v7 ).getNumList ( ); // invoke-virtual {v7}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->getNumList()Ljava/util/ArrayList;
(( java.util.ArrayList ) v8 ).iterator ( ); // invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
v9 = } // :goto_1
if ( v9 != null) { // if-eqz v9, :cond_3
/* check-cast v9, Ljava/lang/String; */
/* .line 178 */
/* .local v9, "number":Ljava/lang/String; */
/* invoke-direct {p0, v9}, Lcom/miui/server/rescue/BrokenScreenRescueService;->only11Number(Ljava/lang/String;)Ljava/lang/String; */
/* .line 179 */
v10 = (( java.lang.String ) v9 ).equals ( v3 ); // invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v10 != null) { // if-eqz v10, :cond_2
/* .line 180 */
this.mMsgbody = v4;
/* .line 181 */
/* .line 182 */
int v2 = 1; // const/4 v2, 0x1
/* .line 184 */
} // .end local v9 # "number":Ljava/lang/String;
} // :cond_2
/* .line 185 */
} // .end local v7 # "contact":Lcom/miui/server/rescue/ContactsHelper$ContactsBean;
} // :cond_3
/* .line 166 */
} // :cond_4
} // :goto_2
/* .line 167 */
/* .line 187 */
} // .end local v3 # "address":Ljava/lang/String;
} // .end local v4 # "body":Ljava/lang/String;
} // .end local v5 # "matcher":Ljava/util/regex/Matcher;
} // :cond_5
if ( v1 != null) { // if-eqz v1, :cond_6
/* .line 188 */
/* .line 190 */
} // :cond_6
} // .end method
private java.lang.String only11Number ( java.lang.String p0 ) {
/* .locals 5 */
/* .param p1, "number" # Ljava/lang/String; */
/* .line 199 */
v0 = android.text.TextUtils .isDigitsOnly ( p1 );
/* const/16 v1, 0xb */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 200 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-le v0, v1, :cond_0 */
/* .line 201 */
v0 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* sub-int/2addr v0, v1 */
(( java.lang.String ) p1 ).substring ( v0 ); // invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;
/* .line 203 */
} // :cond_0
/* .line 206 */
} // :cond_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
/* .line 207 */
/* .local v0, "str":Ljava/lang/StringBuilder; */
int v2 = 0; // const/4 v2, 0x0
/* .local v2, "i":I */
} // :goto_0
v3 = (( java.lang.String ) p1 ).length ( ); // invoke-virtual {p1}, Ljava/lang/String;->length()I
/* if-ge v2, v3, :cond_3 */
/* .line 208 */
v3 = (( java.lang.String ) p1 ).charAt ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C
/* const/16 v4, 0x30 */
/* if-lt v3, v4, :cond_2 */
v3 = (( java.lang.String ) p1 ).charAt ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C
/* const/16 v4, 0x39 */
/* if-gt v3, v4, :cond_2 */
/* .line 209 */
v3 = (( java.lang.String ) p1 ).charAt ( v2 ); // invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C
(( java.lang.StringBuilder ) v0 ).append ( v3 ); // invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
/* .line 207 */
} // :cond_2
/* add-int/lit8 v2, v2, 0x1 */
/* .line 212 */
} // .end local v2 # "i":I
} // :cond_3
v2 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* if-le v2, v1, :cond_4 */
/* .line 213 */
v2 = (( java.lang.StringBuilder ) v0 ).length ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I
/* sub-int/2addr v2, v1 */
(( java.lang.StringBuilder ) v0 ).substring ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;
/* .line 215 */
} // :cond_4
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
} // .end method
private void unlockAndSetUsb ( java.lang.String p0 ) {
/* .locals 17 */
/* .param p1, "passwd" # Ljava/lang/String; */
/* .line 67 */
/* move-object/from16 v1, p0 */
final String v0 = "brokenscreenrescue_pin_error_count"; // const-string v0, "brokenscreenrescue_pin_error_count"
final String v2 = "BrokenScreenRescueService"; // const-string v2, "BrokenScreenRescueService"
try { // :try_start_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v3 */
/* .line 68 */
/* .local v3, "now":J */
/* iget-wide v5, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockStartTime:J */
/* sub-long v5, v3, v5 */
/* .line 69 */
/* .local v5, "elapsedTime":J */
v7 = this.mContext;
(( android.content.Context ) v7 ).getContentResolver ( ); // invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
int v8 = 0; // const/4 v8, 0x0
v7 = android.provider.Settings$System .getInt ( v7,v0,v8 );
/* .line 70 */
/* .local v7, "errorCount":I */
v9 = this.mContext;
(( android.content.Context ) v9 ).getContentResolver ( ); // invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v10 = "brokenscreenrescue_start_time"; // const-string v10, "brokenscreenrescue_start_time"
android.provider.Settings$System .putLong ( v9,v10,v3,v4 );
/* .line 72 */
/* const-wide/16 v9, 0x0 */
/* cmp-long v11, v5, v9 */
/* if-gez v11, :cond_0 */
/* .line 73 */
/* iput-wide v3, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockStartTime:J */
/* .line 77 */
} // :cond_0
/* const/16 v11, 0x14 */
/* if-le v7, v11, :cond_1 */
/* .line 78 */
final String v0 = "Broken Screen Rescue Service is locked!\nIf you want restart the service, you need to reboot."; // const-string v0, "Broken Screen Rescue Service is locked!\nIf you want restart the service, you need to reboot."
android.util.Slog .w ( v2,v0 );
/* .line 80 */
return;
/* .line 84 */
} // :cond_1
v11 = /* invoke-direct {v1, v7, v3, v4}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkPinErrorCountAndLock(IJ)Z */
if ( v11 != null) { // if-eqz v11, :cond_2
/* .line 85 */
/* const-wide/16 v5, 0x0 */
/* .line 89 */
} // :cond_2
/* iget-wide v11, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_1 */
/* cmp-long v11, v5, v11 */
final String v12 = " seconds unlock."; // const-string v12, " seconds unlock."
/* const-wide/16 v13, 0x3e8 */
final String v15 = "Service is locked, left "; // const-string v15, "Service is locked, left "
/* if-gez v11, :cond_3 */
/* .line 90 */
try { // :try_start_1
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v8, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* sub-long/2addr v8, v5 */
/* div-long/2addr v8, v13 */
(( java.lang.StringBuilder ) v0 ).append ( v8, v9 ); // invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 91 */
return;
/* .line 93 */
} // :cond_3
/* iput-wide v9, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* .line 95 */
v11 = this.mLockServicesCompat;
/* :try_end_1 */
/* .catch Ljava/lang/Exception; {:try_start_1 ..:try_end_1} :catch_1 */
/* move-object/from16 v9, p1 */
try { // :try_start_2
v10 = (( com.miui.server.rescue.LockServicesCompat ) v11 ).verifyCredentials ( v9, v8, v8 ); // invoke-virtual {v11, v9, v8, v8}, Lcom/miui/server/rescue/LockServicesCompat;->verifyCredentials(Ljava/lang/String;II)I
/* .line 97 */
/* .local v10, "ret":I */
int v11 = 1; // const/4 v11, 0x1
if ( v10 != null) { // if-eqz v10, :cond_6
/* .line 101 */
/* add-int/2addr v7, v11 */
/* .line 102 */
v8 = this.mContext;
(( android.content.Context ) v8 ).getContentResolver ( ); // invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putInt ( v8,v0,v7 );
/* .line 104 */
v8 = /* invoke-direct {v1, v7, v3, v4}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkPinErrorCountAndLock(IJ)Z */
if ( v8 != null) { // if-eqz v8, :cond_5
/* .line 105 */
/* if-lez v10, :cond_4 */
/* rem-int/lit8 v8, v7, 0x5 */
if ( v8 != null) { // if-eqz v8, :cond_4
/* .line 106 */
/* int-to-long v13, v10 */
/* iput-wide v13, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* .line 107 */
v8 = this.mContext;
(( android.content.Context ) v8 ).getContentResolver ( ); // invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* add-int/lit8 v11, v7, -0x1 */
android.provider.Settings$System .putInt ( v8,v0,v11 );
/* .line 109 */
} // :cond_4
/* const-wide/16 v5, 0x0 */
/* .line 111 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
(( java.lang.StringBuilder ) v0 ).append ( v15 ); // invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget-wide v13, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* sub-long/2addr v13, v5 */
/* const-wide/16 v15, 0x3e8 */
/* div-long/2addr v13, v15 */
(( java.lang.StringBuilder ) v0 ).append ( v13, v14 ); // invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).append ( v12 ); // invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .w ( v2,v0 );
/* .line 113 */
} // :cond_5
return;
/* .line 117 */
} // :cond_6
v12 = this.mContext;
(( android.content.Context ) v12 ).getContentResolver ( ); // invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
android.provider.Settings$System .putInt ( v12,v0,v8 );
/* .line 118 */
/* const-wide/16 v12, 0x0 */
/* iput-wide v12, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J */
/* .line 120 */
v0 = this.mContext;
(( android.content.Context ) v0 ).getContentResolver ( ); // invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
final String v8 = "adb_enabled"; // const-string v8, "adb_enabled"
android.provider.Settings$Global .putInt ( v0,v8,v11 );
/* .line 121 */
v0 = com.miui.server.rescue.BrokenScreenRescueService.sUsbOperationCount;
v0 = (( java.util.concurrent.atomic.AtomicInteger ) v0 ).incrementAndGet ( ); // invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
/* .line 122 */
/* .local v0, "operationId":I */
v8 = this.mUsbManager;
/* const-wide/16 v12, 0x4 */
/* .line 123 */
/* new-instance v8, Lcom/miui/server/rescue/AutoUsbDebuggingManager; */
v12 = this.mHandlerThread;
(( android.os.HandlerThread ) v12 ).getLooper ( ); // invoke-virtual {v12}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;
/* invoke-direct {v8, v12}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;-><init>(Landroid/os/Looper;)V */
/* .line 124 */
/* .local v8, "usbDebuggingManager":Lcom/miui/server/rescue/AutoUsbDebuggingManager; */
(( com.miui.server.rescue.AutoUsbDebuggingManager ) v8 ).setAdbEnabled ( v11 ); // invoke-virtual {v8, v11}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->setAdbEnabled(Z)V
/* :try_end_2 */
/* .catch Ljava/lang/Exception; {:try_start_2 ..:try_end_2} :catch_0 */
/* .line 128 */
} // .end local v0 # "operationId":I
} // .end local v3 # "now":J
} // .end local v5 # "elapsedTime":J
} // .end local v7 # "errorCount":I
} // .end local v8 # "usbDebuggingManager":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
} // .end local v10 # "ret":I
/* .line 125 */
/* :catch_0 */
/* move-exception v0 */
/* :catch_1 */
/* move-exception v0 */
/* move-object/from16 v9, p1 */
/* .line 126 */
/* .local v0, "e":Ljava/lang/Exception; */
} // :goto_0
(( java.lang.Exception ) v0 ).fillInStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;
/* .line 127 */
/* const-string/jumbo v3, "stackTrace" */
android.util.Slog .e ( v2,v3,v0 );
/* .line 129 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_1
return;
} // .end method
/* # virtual methods */
public void onStart ( ) {
/* .locals 0 */
/* .line 294 */
return;
} // .end method
