.class public Lcom/miui/server/rescue/BrokenScreenRescueService;
.super Lcom/android/server/SystemService;
.source "BrokenScreenRescueService.java"


# static fields
.field private static final BROLEN_SCREEN_RESCUE_SERVICE_RUNTIME:Ljava/lang/String; = "brokenscreenrescue_start_time"

.field public static final DEBUG:Z = false

.field public static final MSG_RECEIVE_SMS:I = 0x1

.field public static final MSG_UPDATE_CONTACTS:I = 0x2

.field private static final PIN_ERROR_COUNT:Ljava/lang/String; = "brokenscreenrescue_pin_error_count"

.field private static final PIN_REGEX:Ljava/lang/String; = "^\\*#\\*#(([0-9a-zA-Z\\S\\s]{4,16}))#\\*#\\*$"

.field public static final SERVICE:Ljava/lang/String; = "BrokenScreenRescueService"

.field private static final SERVICE_ENABLE_PROP:Ljava/lang/String; = "persist.sys.brokenscreenservice.disable"

.field public static final TAG:Ljava/lang/String; = "BrokenScreenRescueService"

.field private static final sUsbOperationCount:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private mContacts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/rescue/ContactsHelper$ContactsBean;",
            ">;"
        }
    .end annotation
.end field

.field private mContactsContentObserver:Landroid/database/ContentObserver;

.field private mContactsHelper:Lcom/miui/server/rescue/ContactsHelper;

.field private mContentEvent:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLastMsmUri:Ljava/lang/String;

.field private mLockServicesCompat:Lcom/miui/server/rescue/LockServicesCompat;

.field private mLockStartTime:J

.field private mLockTime:J

.field private mMsgContentObserver:Landroid/database/ContentObserver;

.field private mMsgbody:Ljava/lang/String;

.field private mPinPattern:Ljava/util/regex/Pattern;

.field private mResolver:Landroid/content/ContentResolver;

.field private mUsbManager:Landroid/hardware/usb/IUsbManager;

.field private mlastMsmEventTime:J


# direct methods
.method static bridge synthetic -$$Nest$fgetmHandler(Lcom/miui/server/rescue/BrokenScreenRescueService;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmLastMsmUri(Lcom/miui/server/rescue/BrokenScreenRescueService;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLastMsmUri:Ljava/lang/String;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fgetmlastMsmEventTime(Lcom/miui/server/rescue/BrokenScreenRescueService;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mlastMsmEventTime:J

    return-wide v0
.end method

.method static bridge synthetic -$$Nest$fputmContentEvent(Lcom/miui/server/rescue/BrokenScreenRescueService;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContentEvent:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmLastMsmUri(Lcom/miui/server/rescue/BrokenScreenRescueService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLastMsmUri:Ljava/lang/String;

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmlastMsmEventTime(Lcom/miui/server/rescue/BrokenScreenRescueService;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mlastMsmEventTime:J

    return-void
.end method

.method static bridge synthetic -$$Nest$mcheckDisableBrokenScreenService(Lcom/miui/server/rescue/BrokenScreenRescueService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkDisableBrokenScreenService()Z

    move-result p0

    return p0
.end method

.method static bridge synthetic -$$Nest$mcheckSendSecurityPasswd(Lcom/miui/server/rescue/BrokenScreenRescueService;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkSendSecurityPasswd()V

    return-void
.end method

.method static bridge synthetic -$$Nest$mmatchContacts(Lcom/miui/server/rescue/BrokenScreenRescueService;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->matchContacts()Z

    move-result p0

    return p0
.end method

.method static constructor <clinit>()V
    .locals 1

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/miui/server/rescue/BrokenScreenRescueService;->sUsbOperationCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .line 221
    invoke-direct {p0, p1}, Lcom/android/server/SystemService;-><init>(Landroid/content/Context;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mUsbManager:Landroid/hardware/usb/IUsbManager;

    .line 44
    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 222
    new-instance v0, Lcom/miui/server/rescue/ContactsHelper;

    invoke-direct {v0}, Lcom/miui/server/rescue/ContactsHelper;-><init>()V

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContactsHelper:Lcom/miui/server/rescue/ContactsHelper;

    .line 223
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BrokenScreenHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 224
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 225
    iput-object p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    .line 226
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mResolver:Landroid/content/ContentResolver;

    .line 227
    const-string/jumbo v0, "usb"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/usb/IUsbManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/usb/IUsbManager;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mUsbManager:Landroid/hardware/usb/IUsbManager;

    .line 228
    new-instance v0, Lcom/miui/server/rescue/LockServicesCompat;

    iget-object v1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/miui/server/rescue/LockServicesCompat;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockServicesCompat:Lcom/miui/server/rescue/LockServicesCompat;

    .line 229
    const-string v0, "^\\*#\\*#(([0-9a-zA-Z\\S\\s]{4,16}))#\\*#\\*$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mPinPattern:Ljava/util/regex/Pattern;

    .line 230
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mlastMsmEventTime:J

    .line 232
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 234
    .local v0, "now":J
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "brokenscreenrescue_start_time"

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    .line 235
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "brokenscreenrescue_pin_error_count"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 238
    :cond_0
    new-instance v2, Lcom/miui/server/rescue/BrokenScreenRescueService$1;

    iget-object v3, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService$1;-><init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandler:Landroid/os/Handler;

    .line 267
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 268
    new-instance v2, Lcom/miui/server/rescue/BrokenScreenRescueService$2;

    iget-object v3, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService$2;-><init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mMsgContentObserver:Landroid/database/ContentObserver;

    .line 280
    new-instance v2, Lcom/miui/server/rescue/BrokenScreenRescueService$3;

    iget-object v3, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService$3;-><init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContactsContentObserver:Landroid/database/ContentObserver;

    .line 287
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "content://sms/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mMsgContentObserver:Landroid/database/ContentObserver;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 288
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mResolver:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContactsContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 289
    return-void
.end method

.method private checkDisableBrokenScreenService()Z
    .locals 2

    .line 61
    const-string v0, "persist.sys.brokenscreenservice.disable"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private checkPinErrorCountAndLock(IJ)Z
    .locals 6
    .param p1, "errorCount"    # I
    .param p2, "now"    # J

    .line 132
    if-eqz p1, :cond_1

    rem-int/lit8 v0, p1, 0x5

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 133
    iput-wide p2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockStartTime:J

    .line 134
    div-int/lit8 v0, p1, 0x5

    int-to-double v0, v0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/16 v4, 0x3c

    mul-long/2addr v0, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    .line 135
    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 136
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    .line 138
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 140
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method private checkSendSecurityPasswd()V
    .locals 3

    .line 144
    iget-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mPinPattern:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mMsgbody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 146
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 147
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "pin":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/miui/server/rescue/BrokenScreenRescueService;->unlockAndSetUsb(Ljava/lang/String;)V

    .line 151
    .end local v1    # "pin":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private matchContacts()Z
    .locals 11

    .line 155
    const-string v0, "content://sms/inbox"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 156
    .local v0, "inboxUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "date desc"

    move-object v2, v0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 157
    .local v1, "c":Landroid/database/Cursor;
    const/4 v2, 0x0

    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 158
    const-string v3, "address"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 159
    .local v3, "address":Ljava/lang/String;
    const-string v4, "body"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, "body":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/miui/server/rescue/BrokenScreenRescueService;->only11Number(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 163
    iget-object v5, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mPinPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 165
    .local v5, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v6

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    goto :goto_2

    .line 171
    :cond_0
    iget-boolean v6, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContentEvent:Z

    if-eqz v6, :cond_1

    .line 172
    iget-object v6, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContactsHelper:Lcom/miui/server/rescue/ContactsHelper;

    iget-object v7, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/miui/server/rescue/ContactsHelper;->getContactsList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContacts:Ljava/util/ArrayList;

    .line 173
    iput-boolean v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContentEvent:Z

    .line 176
    :cond_1
    iget-object v6, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContacts:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;

    .line 177
    .local v7, "contact":Lcom/miui/server/rescue/ContactsHelper$ContactsBean;
    invoke-virtual {v7}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->getNumList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 178
    .local v9, "number":Ljava/lang/String;
    invoke-direct {p0, v9}, Lcom/miui/server/rescue/BrokenScreenRescueService;->only11Number(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 179
    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 180
    iput-object v4, p0, Lcom/miui/server/rescue/BrokenScreenRescueService;->mMsgbody:Ljava/lang/String;

    .line 181
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 182
    const/4 v2, 0x1

    return v2

    .line 184
    .end local v9    # "number":Ljava/lang/String;
    :cond_2
    goto :goto_1

    .line 185
    .end local v7    # "contact":Lcom/miui/server/rescue/ContactsHelper$ContactsBean;
    :cond_3
    goto :goto_0

    .line 166
    :cond_4
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 167
    return v2

    .line 187
    .end local v3    # "address":Ljava/lang/String;
    .end local v4    # "body":Ljava/lang/String;
    .end local v5    # "matcher":Ljava/util/regex/Matcher;
    :cond_5
    if-eqz v1, :cond_6

    .line 188
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 190
    :cond_6
    return v2
.end method

.method private only11Number(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "number"    # Ljava/lang/String;

    .line 199
    invoke-static {p1}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v0

    const/16 v1, 0xb

    if-eqz v0, :cond_1

    .line 200
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 201
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 203
    :cond_0
    return-object p1

    .line 206
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .local v0, "str":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 208
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_2

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_2

    .line 209
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 207
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 212
    .end local v2    # "i":I
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-le v2, v1, :cond_4

    .line 213
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 215
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private unlockAndSetUsb(Ljava/lang/String;)V
    .locals 17
    .param p1, "passwd"    # Ljava/lang/String;

    .line 67
    move-object/from16 v1, p0

    const-string v0, "brokenscreenrescue_pin_error_count"

    const-string v2, "BrokenScreenRescueService"

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 68
    .local v3, "now":J
    iget-wide v5, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockStartTime:J

    sub-long v5, v3, v5

    .line 69
    .local v5, "elapsedTime":J
    iget-object v7, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v0, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    .line 70
    .local v7, "errorCount":I
    iget-object v9, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "brokenscreenrescue_start_time"

    invoke-static {v9, v10, v3, v4}, Landroid/provider/Settings$System;->putLong(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    .line 72
    const-wide/16 v9, 0x0

    cmp-long v11, v5, v9

    if-gez v11, :cond_0

    .line 73
    iput-wide v3, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockStartTime:J

    .line 77
    :cond_0
    const/16 v11, 0x14

    if-le v7, v11, :cond_1

    .line 78
    const-string v0, "Broken Screen Rescue Service is locked!\nIf you want restart the service, you need to reboot."

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    return-void

    .line 84
    :cond_1
    invoke-direct {v1, v7, v3, v4}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkPinErrorCountAndLock(IJ)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 85
    const-wide/16 v5, 0x0

    .line 89
    :cond_2
    iget-wide v11, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    cmp-long v11, v5, v11

    const-string v12, " seconds unlock."

    const-wide/16 v13, 0x3e8

    const-string v15, "Service is locked, left "

    if-gez v11, :cond_3

    .line 90
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v8, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    sub-long/2addr v8, v5

    div-long/2addr v8, v13

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void

    .line 93
    :cond_3
    iput-wide v9, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    .line 95
    iget-object v11, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockServicesCompat:Lcom/miui/server/rescue/LockServicesCompat;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object/from16 v9, p1

    :try_start_2
    invoke-virtual {v11, v9, v8, v8}, Lcom/miui/server/rescue/LockServicesCompat;->verifyCredentials(Ljava/lang/String;II)I

    move-result v10

    .line 97
    .local v10, "ret":I
    const/4 v11, 0x1

    if-eqz v10, :cond_6

    .line 101
    add-int/2addr v7, v11

    .line 102
    iget-object v8, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, v0, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 104
    invoke-direct {v1, v7, v3, v4}, Lcom/miui/server/rescue/BrokenScreenRescueService;->checkPinErrorCountAndLock(IJ)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 105
    if-lez v10, :cond_4

    rem-int/lit8 v8, v7, 0x5

    if-eqz v8, :cond_4

    .line 106
    int-to-long v13, v10

    iput-wide v13, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    .line 107
    iget-object v8, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    add-int/lit8 v11, v7, -0x1

    invoke-static {v8, v0, v11}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 109
    :cond_4
    const-wide/16 v5, 0x0

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v13, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    sub-long/2addr v13, v5

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_5
    return-void

    .line 117
    :cond_6
    iget-object v12, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-static {v12, v0, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 118
    const-wide/16 v12, 0x0

    iput-wide v12, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mLockTime:J

    .line 120
    iget-object v0, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v8, "adb_enabled"

    invoke-static {v0, v8, v11}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 121
    sget-object v0, Lcom/miui/server/rescue/BrokenScreenRescueService;->sUsbOperationCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 122
    .local v0, "operationId":I
    iget-object v8, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mUsbManager:Landroid/hardware/usb/IUsbManager;

    const-wide/16 v12, 0x4

    invoke-interface {v8, v12, v13, v0}, Landroid/hardware/usb/IUsbManager;->setCurrentFunctions(JI)V

    .line 123
    new-instance v8, Lcom/miui/server/rescue/AutoUsbDebuggingManager;

    iget-object v12, v1, Lcom/miui/server/rescue/BrokenScreenRescueService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v12}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v12

    invoke-direct {v8, v12}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;-><init>(Landroid/os/Looper;)V

    .line 124
    .local v8, "usbDebuggingManager":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    invoke-virtual {v8, v11}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->setAdbEnabled(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 128
    .end local v0    # "operationId":I
    .end local v3    # "now":J
    .end local v5    # "elapsedTime":J
    .end local v7    # "errorCount":I
    .end local v8    # "usbDebuggingManager":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    .end local v10    # "ret":I
    goto :goto_1

    .line 125
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v9, p1

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 127
    const-string/jumbo v3, "stackTrace"

    invoke-static {v2, v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 129
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 0

    .line 294
    return-void
.end method
