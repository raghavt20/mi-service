.class Lcom/miui/server/rescue/ContactsHelper$ContactsBean;
.super Ljava/lang/Object;
.source "ContactsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/server/rescue/ContactsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ContactsBean"
.end annotation


# instance fields
.field private name:Ljava/lang/String;

.field private numList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/server/rescue/ContactsHelper;


# direct methods
.method public constructor <init>(Lcom/miui/server/rescue/ContactsHelper;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/rescue/ContactsHelper;

    .line 32
    iput-object p1, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->this$0:Lcom/miui/server/rescue/ContactsHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->numList:Ljava/util/ArrayList;

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/miui/server/rescue/ContactsHelper;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "this$0"    # Lcom/miui/server/rescue/ContactsHelper;
    .param p2, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 35
    .local p3, "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->this$0:Lcom/miui/server/rescue/ContactsHelper;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->name:Ljava/lang/String;

    .line 38
    if-nez p3, :cond_0

    .line 39
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->numList:Ljava/util/ArrayList;

    goto :goto_0

    .line 41
    :cond_0
    iput-object p3, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->numList:Ljava/util/ArrayList;

    .line 43
    :goto_0
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNumList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->numList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .line 24
    iput-object p1, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->name:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setNumList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 30
    .local p1, "numList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->numList:Ljava/util/ArrayList;

    .line 31
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContactsBean [name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->numList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
