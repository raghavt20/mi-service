class com.miui.server.rescue.BrokenScreenRescueService$1 extends android.os.Handler {
	 /* .source "BrokenScreenRescueService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingMethod; */
	 /* value = Lcom/miui/server/rescue/BrokenScreenRescueService;-><init>(Landroid/content/Context;)V */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.rescue.BrokenScreenRescueService this$0; //synthetic
/* # direct methods */
 com.miui.server.rescue.BrokenScreenRescueService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/rescue/BrokenScreenRescueService; */
/* .param p2, "looper" # Landroid/os/Looper; */
/* .line 238 */
this.this$0 = p1;
/* invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V */
return;
} // .end method
/* # virtual methods */
public void handleMessage ( android.os.Message p0 ) {
/* .locals 6 */
/* .param p1, "msg" # Landroid/os/Message; */
/* .line 241 */
/* iget v0, p1, Landroid/os/Message;->what:I */
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
/* packed-switch v0, :pswitch_data_0 */
/* .line 263 */
/* new-instance v0, Ljava/lang/StringBuilder; */
/* invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V */
final String v2 = "Handler default msg "; // const-string v2, "Handler default msg "
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* iget v2, p1, Landroid/os/Message;->what:I */
(( java.lang.StringBuilder ) v0 ).append ( v2 ); // invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v0 ).toString ( ); // invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v0 );
/* .line 259 */
/* :pswitch_0 */
v0 = this.this$0;
int v1 = 1; // const/4 v1, 0x1
com.miui.server.rescue.BrokenScreenRescueService .-$$Nest$fputmContentEvent ( v0,v1 );
/* .line 260 */
/* .line 243 */
/* :pswitch_1 */
v0 = this.this$0;
v0 = com.miui.server.rescue.BrokenScreenRescueService .-$$Nest$mcheckDisableBrokenScreenService ( v0 );
if ( v0 != null) { // if-eqz v0, :cond_0
	 /* .line 244 */
	 final String v0 = "Broken Screen Rescue Service has been disable!"; // const-string v0, "Broken Screen Rescue Service has been disable!"
	 android.util.Slog .w ( v1,v0 );
	 /* .line 245 */
	 return;
	 /* .line 247 */
} // :cond_0
android.os.SystemClock .elapsedRealtime ( );
/* move-result-wide v0 */
/* .line 248 */
/* .local v0, "now":J */
v2 = this.this$0;
com.miui.server.rescue.BrokenScreenRescueService .-$$Nest$fgetmlastMsmEventTime ( v2 );
/* move-result-wide v2 */
/* const-wide/16 v4, 0x3e8 */
/* add-long/2addr v2, v4 */
/* cmp-long v2, v2, v0 */
/* if-lez v2, :cond_1 */
/* .line 250 */
return;
/* .line 252 */
} // :cond_1
v2 = this.this$0;
com.miui.server.rescue.BrokenScreenRescueService .-$$Nest$fputmlastMsmEventTime ( v2,v0,v1 );
/* .line 254 */
v2 = this.this$0;
v2 = com.miui.server.rescue.BrokenScreenRescueService .-$$Nest$mmatchContacts ( v2 );
if ( v2 != null) { // if-eqz v2, :cond_2
v2 = this.this$0;
com.miui.server.rescue.BrokenScreenRescueService .-$$Nest$mcheckSendSecurityPasswd ( v2 );
/* .line 265 */
} // .end local v0 # "now":J
} // :cond_2
} // :goto_0
return;
/* :pswitch_data_0 */
/* .packed-switch 0x1 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
