public class com.miui.server.rescue.ContactsHelper {
	 /* .source "ContactsHelper.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/rescue/ContactsHelper$ContactsBean; */
	 /* } */
} // .end annotation
/* # direct methods */
public com.miui.server.rescue.ContactsHelper ( ) {
	 /* .locals 0 */
	 /* .line 16 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 return;
} // .end method
private void wirteNumbers ( android.content.ContentResolver p0, java.lang.String p1, com.miui.server.rescue.ContactsHelper$ContactsBean p2 ) {
	 /* .locals 8 */
	 /* .param p1, "contentResolver" # Landroid/content/ContentResolver; */
	 /* .param p2, "name" # Ljava/lang/String; */
	 /* .param p3, "bean" # Lcom/miui/server/rescue/ContactsHelper$ContactsBean; */
	 /* .line 72 */
	 final String v0 = "data1"; // const-string v0, "data1"
	 if ( p2 != null) { // if-eqz p2, :cond_6
		 v1 = 		 android.text.TextUtils .isEmpty ( p2 );
		 if ( v1 != null) { // if-eqz v1, :cond_0
			 /* .line 75 */
		 } // :cond_0
		 try { // :try_start_0
			 v3 = android.provider.ContactsContract$Data.CONTENT_URI;
			 int v1 = 1; // const/4 v1, 0x1
			 /* new-array v4, v1, [Ljava/lang/String; */
			 int v1 = 0; // const/4 v1, 0x0
			 /* aput-object v0, v4, v1 */
			 final String v5 = "display_name= ? "; // const-string v5, "display_name= ? "
			 /* filled-new-array {p2}, [Ljava/lang/String; */
			 int v7 = 0; // const/4 v7, 0x0
			 /* move-object v2, p1 */
			 /* invoke-virtual/range {v2 ..v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
			 /* .line 79 */
			 /* .local v1, "dataCursor":Landroid/database/Cursor; */
			 /* if-nez v1, :cond_1 */
			 /* .line 81 */
			 return;
			 /* .line 83 */
		 v2 = 		 } // :cond_1
		 /* if-lez v2, :cond_5 */
		 /* .line 84 */
		 (( com.miui.server.rescue.ContactsHelper$ContactsBean ) p3 ).setName ( p2 ); // invoke-virtual {p3, p2}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->setName(Ljava/lang/String;)V
		 /* .line 86 */
	 } // :cond_2
v2 = } // :goto_0
if ( v2 != null) { // if-eqz v2, :cond_4
	 v2 = 	 /* .line 87 */
	 /* .line 88 */
	 /* .local v2, "number":Ljava/lang/String; */
	 if ( v2 != null) { // if-eqz v2, :cond_2
		 v3 = 		 android.text.TextUtils .isEmpty ( v2 );
		 if ( v3 != null) { // if-eqz v3, :cond_3
			 /* .line 89 */
		 } // :cond_3
		 (( com.miui.server.rescue.ContactsHelper$ContactsBean ) p3 ).getNumList ( ); // invoke-virtual {p3}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->getNumList()Ljava/util/ArrayList;
		 (( java.util.ArrayList ) v3 ).add ( v2 ); // invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
		 /* .line 91 */
		 /* nop */
	 } // .end local v2 # "number":Ljava/lang/String;
	 /* .line 92 */
} // :cond_4
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 96 */
} // :cond_5
return;
/* .line 97 */
} // .end local v1 # "dataCursor":Landroid/database/Cursor;
/* :catch_0 */
/* move-exception v0 */
/* .line 98 */
/* .local v0, "e":Ljava/lang/Exception; */
(( java.lang.Exception ) v0 ).fillInStackTrace ( ); // invoke-virtual {v0}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;
/* .line 99 */
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
/* const-string/jumbo v2, "stackTrace: " */
android.util.Slog .e ( v1,v2,v0 );
/* .line 102 */
} // .end local v0 # "e":Ljava/lang/Exception;
return;
/* .line 72 */
} // :cond_6
} // :goto_1
return;
} // .end method
/* # virtual methods */
public java.util.ArrayList getContactsList ( android.content.Context p0 ) {
/* .locals 9 */
/* .param p1, "context" # Landroid/content/Context; */
/* .annotation system Ldalvik/annotation/Signature; */
/* value = { */
/* "(", */
/* "Landroid/content/Context;", */
/* ")", */
/* "Ljava/util/ArrayList<", */
/* "Lcom/miui/server/rescue/ContactsHelper$ContactsBean;", */
/* ">;" */
/* } */
} // .end annotation
/* .line 52 */
/* new-instance v0, Ljava/util/ArrayList; */
/* invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V */
/* .line 53 */
/* .local v0, "contactsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rescue/ContactsHelper$ContactsBean;>;" */
int v1 = 0; // const/4 v1, 0x0
/* .line 54 */
/* .local v1, "bean":Lcom/miui/server/rescue/ContactsHelper$ContactsBean; */
(( android.content.Context ) p1 ).getContentResolver ( ); // invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;
/* .line 55 */
/* .local v8, "resolver":Landroid/content/ContentResolver; */
v3 = android.provider.ContactsContract$Contacts.CONTENT_URI;
int v4 = 0; // const/4 v4, 0x0
int v5 = 0; // const/4 v5, 0x0
int v6 = 0; // const/4 v6, 0x0
int v7 = 0; // const/4 v7, 0x0
/* move-object v2, v8 */
/* invoke-virtual/range {v2 ..v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor; */
/* .line 56 */
/* .local v2, "cursor":Landroid/database/Cursor; */
v3 = if ( v2 != null) { // if-eqz v2, :cond_3
/* if-gtz v3, :cond_0 */
/* .line 60 */
} // :cond_0
v3 = } // :goto_0
if ( v3 != null) { // if-eqz v3, :cond_2
/* .line 61 */
/* new-instance v3, Lcom/miui/server/rescue/ContactsHelper$ContactsBean; */
/* invoke-direct {v3, p0}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;-><init>(Lcom/miui/server/rescue/ContactsHelper;)V */
/* move-object v1, v3 */
/* .line 62 */
v3 = final String v3 = "display_name"; // const-string v3, "display_name"
/* .line 63 */
/* .local v3, "name":Ljava/lang/String; */
/* if-nez v3, :cond_1 */
/* .line 64 */
} // :cond_1
/* invoke-direct {p0, v8, v3, v1}, Lcom/miui/server/rescue/ContactsHelper;->wirteNumbers(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/miui/server/rescue/ContactsHelper$ContactsBean;)V */
/* .line 65 */
(( java.util.ArrayList ) v0 ).add ( v1 ); // invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
/* .line 66 */
} // .end local v3 # "name":Ljava/lang/String;
/* .line 67 */
} // :cond_2
/* .line 68 */
/* .line 57 */
} // :cond_3
} // :goto_1
} // .end method
