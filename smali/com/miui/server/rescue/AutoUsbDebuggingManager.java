public class com.miui.server.rescue.AutoUsbDebuggingManager implements java.lang.Runnable {
	 /* .source "AutoUsbDebuggingManager.java" */
	 /* # interfaces */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/MemberClasses; */
	 /* value = { */
	 /* Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler; */
	 /* } */
} // .end annotation
/* # static fields */
static final Integer BUFFER_SIZE;
private static final java.lang.String TAG;
/* # instance fields */
private final java.lang.String ADBD_SOCKET;
private final java.lang.String ADB_DIRECTORY;
private final java.lang.String ADB_KEYS_FILE;
private Boolean mAdbEnabled;
private final android.os.Handler mHandler;
java.io.OutputStream mOutputStream;
android.net.LocalSocket mSocket;
private java.lang.Thread mThread;
/* # direct methods */
static Boolean -$$Nest$fgetmAdbEnabled ( com.miui.server.rescue.AutoUsbDebuggingManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iget-boolean p0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mAdbEnabled:Z */
} // .end method
static java.lang.Thread -$$Nest$fgetmThread ( com.miui.server.rescue.AutoUsbDebuggingManager p0 ) { //bridge//synthethic
	 /* .locals 0 */
	 p0 = this.mThread;
} // .end method
static void -$$Nest$fputmAdbEnabled ( com.miui.server.rescue.AutoUsbDebuggingManager p0, Boolean p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* iput-boolean p1, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mAdbEnabled:Z */
	 return;
} // .end method
static void -$$Nest$fputmThread ( com.miui.server.rescue.AutoUsbDebuggingManager p0, java.lang.Thread p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 this.mThread = p1;
	 return;
} // .end method
static void -$$Nest$mwriteKey ( com.miui.server.rescue.AutoUsbDebuggingManager p0, java.lang.String p1 ) { //bridge//synthethic
	 /* .locals 0 */
	 /* invoke-direct {p0, p1}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->writeKey(Ljava/lang/String;)V */
	 return;
} // .end method
public com.miui.server.rescue.AutoUsbDebuggingManager ( ) {
	 /* .locals 1 */
	 /* .param p1, "looper" # Landroid/os/Looper; */
	 /* .line 88 */
	 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
	 /* .line 26 */
	 int v0 = 0; // const/4 v0, 0x0
	 /* iput-boolean v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mAdbEnabled:Z */
	 /* .line 27 */
	 final String v0 = "adbd"; // const-string v0, "adbd"
	 this.ADBD_SOCKET = v0;
	 /* .line 28 */
	 final String v0 = "misc/adb"; // const-string v0, "misc/adb"
	 this.ADB_DIRECTORY = v0;
	 /* .line 29 */
	 final String v0 = "adb_keys"; // const-string v0, "adb_keys"
	 this.ADB_KEYS_FILE = v0;
	 /* .line 89 */
	 /* new-instance v0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler; */
	 /* invoke-direct {v0, p0, p1}, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;-><init>(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Landroid/os/Looper;)V */
	 this.mHandler = v0;
	 /* .line 90 */
	 return;
} // .end method
private void writeKey ( java.lang.String p0 ) {
	 /* .locals 7 */
	 /* .param p1, "key" # Ljava/lang/String; */
	 /* .line 140 */
	 android.os.Environment .getDataDirectory ( );
	 /* .line 141 */
	 /* .local v0, "dataDir":Ljava/io/File; */
	 /* new-instance v1, Ljava/io/File; */
	 final String v2 = "misc/adb"; // const-string v2, "misc/adb"
	 /* invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
	 /* .line 143 */
	 /* .local v1, "adbDir":Ljava/io/File; */
	 v2 = 	 (( java.io.File ) v1 ).exists ( ); // invoke-virtual {v1}, Ljava/io/File;->exists()Z
	 final String v3 = "BrokenScreenRescueService"; // const-string v3, "BrokenScreenRescueService"
	 /* if-nez v2, :cond_0 */
	 /* .line 144 */
	 final String v2 = "ADB data directory does not exist"; // const-string v2, "ADB data directory does not exist"
	 android.util.Slog .e ( v3,v2 );
	 /* .line 145 */
	 return;
	 /* .line 149 */
} // :cond_0
try { // :try_start_0
	 /* new-instance v2, Ljava/io/File; */
	 final String v4 = "adb_keys"; // const-string v4, "adb_keys"
	 /* invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V */
	 /* .line 151 */
	 /* .local v2, "keyFile":Ljava/io/File; */
	 v4 = 	 (( java.io.File ) v2 ).exists ( ); // invoke-virtual {v2}, Ljava/io/File;->exists()Z
	 /* if-nez v4, :cond_1 */
	 /* .line 152 */
	 (( java.io.File ) v2 ).createNewFile ( ); // invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
	 /* .line 153 */
	 (( java.io.File ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;
	 /* const/16 v5, 0x1a0 */
	 int v6 = -1; // const/4 v6, -0x1
	 android.os.FileUtils .setPermissions ( v4,v5,v6,v6 );
	 /* .line 158 */
} // :cond_1
/* new-instance v4, Ljava/io/FileOutputStream; */
int v5 = 1; // const/4 v5, 0x1
/* invoke-direct {v4, v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V */
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 159 */
/* .local v4, "fo":Ljava/io/FileOutputStream; */
try { // :try_start_1
	 (( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
	 (( java.io.FileOutputStream ) v4 ).write ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V
	 /* .line 160 */
	 /* const/16 v5, 0xa */
	 (( java.io.FileOutputStream ) v4 ).write ( v5 ); // invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write(I)V
	 /* :try_end_1 */
	 /* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
	 /* .line 161 */
	 try { // :try_start_2
		 (( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
		 /* :try_end_2 */
		 /* .catch Ljava/io/IOException; {:try_start_2 ..:try_end_2} :catch_0 */
		 /* .line 164 */
	 } // .end local v2 # "keyFile":Ljava/io/File;
} // .end local v4 # "fo":Ljava/io/FileOutputStream;
/* .line 158 */
/* .restart local v2 # "keyFile":Ljava/io/File; */
/* .restart local v4 # "fo":Ljava/io/FileOutputStream; */
/* :catchall_0 */
/* move-exception v5 */
try { // :try_start_3
	 (( java.io.FileOutputStream ) v4 ).close ( ); // invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
	 /* :try_end_3 */
	 /* .catchall {:try_start_3 ..:try_end_3} :catchall_1 */
	 /* :catchall_1 */
	 /* move-exception v6 */
	 try { // :try_start_4
		 (( java.lang.Throwable ) v5 ).addSuppressed ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V
	 } // .end local v0 # "dataDir":Ljava/io/File;
} // .end local v1 # "adbDir":Ljava/io/File;
} // .end local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
} // .end local p1 # "key":Ljava/lang/String;
} // :goto_0
/* throw v5 */
/* :try_end_4 */
/* .catch Ljava/io/IOException; {:try_start_4 ..:try_end_4} :catch_0 */
/* .line 162 */
} // .end local v2 # "keyFile":Ljava/io/File;
} // .end local v4 # "fo":Ljava/io/FileOutputStream;
/* .restart local v0 # "dataDir":Ljava/io/File; */
/* .restart local v1 # "adbDir":Ljava/io/File; */
/* .restart local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager; */
/* .restart local p1 # "key":Ljava/lang/String; */
/* :catch_0 */
/* move-exception v2 */
/* .line 163 */
/* .local v2, "ex":Ljava/io/IOException; */
/* new-instance v4, Ljava/lang/StringBuilder; */
/* invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V */
final String v5 = "Error writing key:"; // const-string v5, "Error writing key:"
(( java.lang.StringBuilder ) v4 ).append ( v5 ); // invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).append ( v2 ); // invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v4 ).toString ( ); // invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v3,v4 );
/* .line 165 */
} // .end local v2 # "ex":Ljava/io/IOException;
} // :goto_1
return;
} // .end method
/* # virtual methods */
synchronized void closeSocket ( ) {
/* .locals 4 */
/* monitor-enter p0 */
/* .line 98 */
try { // :try_start_0
v0 = this.mOutputStream;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 100 */
try { // :try_start_1
(( java.io.OutputStream ) v0 ).close ( ); // invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 103 */
/* .line 101 */
} // .end local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
/* :catch_0 */
/* move-exception v0 */
/* .line 102 */
/* .local v0, "ex":Ljava/io/IOException; */
try { // :try_start_2
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed closing output stream: "; // const-string v3, "Failed closing output stream: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* .line 106 */
} // .end local v0 # "ex":Ljava/io/IOException;
} // :cond_0
} // :goto_0
v0 = this.mSocket;
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_1
/* .line 108 */
try { // :try_start_3
(( android.net.LocalSocket ) v0 ).close ( ); // invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
/* :try_end_3 */
/* .catch Ljava/io/IOException; {:try_start_3 ..:try_end_3} :catch_1 */
/* .catchall {:try_start_3 ..:try_end_3} :catchall_0 */
/* .line 111 */
/* .line 109 */
/* :catch_1 */
/* move-exception v0 */
/* .line 110 */
/* .restart local v0 # "ex":Ljava/io/IOException; */
try { // :try_start_4
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
/* new-instance v2, Ljava/lang/StringBuilder; */
/* invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V */
final String v3 = "Failed closing socket: "; // const-string v3, "Failed closing socket: "
(( java.lang.StringBuilder ) v2 ).append ( v3 ); // invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).append ( v0 ); // invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v2 ).toString ( ); // invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v1,v2 );
/* :try_end_4 */
/* .catchall {:try_start_4 ..:try_end_4} :catchall_0 */
/* .line 113 */
} // .end local v0 # "ex":Ljava/io/IOException;
} // :cond_1
} // :goto_1
/* monitor-exit p0 */
return;
/* .line 97 */
/* :catchall_0 */
/* move-exception v0 */
/* monitor-exit p0 */
/* throw v0 */
} // .end method
void listenToSocket ( ) {
/* .locals 9 */
/* .annotation system Ldalvik/annotation/Throws; */
/* value = { */
/* Ljava/io/IOException; */
/* } */
} // .end annotation
/* .line 169 */
final String v0 = "BrokenScreenRescueService"; // const-string v0, "BrokenScreenRescueService"
/* const/16 v1, 0x1000 */
try { // :try_start_0
/* new-array v1, v1, [B */
/* .line 170 */
/* .local v1, "buffer":[B */
/* new-instance v2, Landroid/net/LocalSocketAddress; */
final String v3 = "adbd"; // const-string v3, "adbd"
v4 = android.net.LocalSocketAddress$Namespace.RESERVED;
/* invoke-direct {v2, v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V */
/* .line 172 */
/* .local v2, "address":Landroid/net/LocalSocketAddress; */
int v3 = 0; // const/4 v3, 0x0
/* .line 174 */
/* .local v3, "inputStream":Ljava/io/InputStream; */
/* new-instance v4, Landroid/net/LocalSocket; */
/* invoke-direct {v4}, Landroid/net/LocalSocket;-><init>()V */
this.mSocket = v4;
/* .line 175 */
(( android.net.LocalSocket ) v4 ).connect ( v2 ); // invoke-virtual {v4, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
/* .line 176 */
final String v4 = "connected to adbd"; // const-string v4, "connected to adbd"
android.util.Slog .i ( v0,v4 );
/* .line 178 */
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).getOutputStream ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;
this.mOutputStream = v4;
/* .line 179 */
v4 = this.mSocket;
(( android.net.LocalSocket ) v4 ).getInputStream ( ); // invoke-virtual {v4}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;
/* move-object v3, v4 */
/* .line 182 */
} // :goto_0
v4 = (( java.io.InputStream ) v3 ).read ( v1 ); // invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I
/* .line 183 */
/* .local v4, "count":I */
/* if-gez v4, :cond_0 */
/* .line 184 */
/* new-instance v5, Ljava/lang/StringBuilder; */
/* invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V */
final String v6 = "got "; // const-string v6, "got "
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).append ( v4 ); // invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
final String v6 = " reading"; // const-string v6, " reading"
(( java.lang.StringBuilder ) v5 ).append ( v6 ); // invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
android.util.Slog .e ( v0,v5 );
/* .line 185 */
/* .line 188 */
} // :cond_0
int v5 = 0; // const/4 v5, 0x0
/* aget-byte v6, v1, v5 */
/* const/16 v7, 0x50 */
int v8 = 2; // const/4 v8, 0x2
/* if-ne v6, v7, :cond_1 */
int v6 = 1; // const/4 v6, 0x1
/* aget-byte v6, v1, v6 */
/* const/16 v7, 0x4b */
/* if-ne v6, v7, :cond_1 */
/* .line 189 */
/* new-instance v5, Ljava/lang/String; */
java.util.Arrays .copyOfRange ( v1,v8,v4 );
/* invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V */
/* .line 191 */
/* .local v5, "key":Ljava/lang/String; */
v6 = this.mHandler;
/* .line 192 */
int v7 = 3; // const/4 v7, 0x3
(( android.os.Handler ) v6 ).obtainMessage ( v7 ); // invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;
/* .line 193 */
/* .local v6, "msg":Landroid/os/Message; */
this.obj = v5;
/* .line 194 */
v7 = this.mHandler;
(( android.os.Handler ) v7 ).sendMessage ( v6 ); // invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
/* .line 195 */
/* nop */
/* .line 200 */
} // .end local v4 # "count":I
} // .end local v5 # "key":Ljava/lang/String;
} // .end local v6 # "msg":Landroid/os/Message;
/* .line 196 */
/* .restart local v4 # "count":I */
} // :cond_1
/* new-instance v6, Ljava/lang/StringBuilder; */
/* invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V */
final String v7 = "Wrong message: "; // const-string v7, "Wrong message: "
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
/* new-instance v7, Ljava/lang/String; */
/* .line 197 */
java.util.Arrays .copyOfRange ( v1,v5,v8 );
/* invoke-direct {v7, v5}, Ljava/lang/String;-><init>([B)V */
(( java.lang.StringBuilder ) v6 ).append ( v7 ); // invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
(( java.lang.StringBuilder ) v5 ).toString ( ); // invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
/* .line 196 */
android.util.Slog .e ( v0,v5 );
/* :try_end_0 */
/* .catch Ljava/io/IOException; {:try_start_0 ..:try_end_0} :catch_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
/* .line 198 */
/* nop */
/* .line 205 */
} // .end local v1 # "buffer":[B
} // .end local v2 # "address":Landroid/net/LocalSocketAddress;
} // .end local v3 # "inputStream":Ljava/io/InputStream;
} // .end local v4 # "count":I
} // :goto_1
(( com.miui.server.rescue.AutoUsbDebuggingManager ) p0 ).closeSocket ( ); // invoke-virtual {p0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->closeSocket()V
/* .line 206 */
/* nop */
/* .line 207 */
return;
/* .line 205 */
/* :catchall_0 */
/* move-exception v0 */
/* .line 201 */
/* :catch_0 */
/* move-exception v1 */
/* .line 202 */
/* .local v1, "ex":Ljava/io/IOException; */
try { // :try_start_1
final String v2 = "Communication error: "; // const-string v2, "Communication error: "
android.util.Slog .e ( v0,v2,v1 );
/* .line 203 */
/* nop */
} // .end local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
/* throw v1 */
/* :try_end_1 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 205 */
} // .end local v1 # "ex":Ljava/io/IOException;
/* .restart local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager; */
} // :goto_2
(( com.miui.server.rescue.AutoUsbDebuggingManager ) p0 ).closeSocket ( ); // invoke-virtual {p0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->closeSocket()V
/* .line 206 */
/* throw v0 */
} // .end method
public void run ( ) {
/* .locals 3 */
/* .line 212 */
try { // :try_start_0
(( com.miui.server.rescue.AutoUsbDebuggingManager ) p0 ).listenToSocket ( ); // invoke-virtual {p0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->listenToSocket()V
/* :try_end_0 */
/* .catch Ljava/lang/Exception; {:try_start_0 ..:try_end_0} :catch_0 */
/* .line 216 */
/* .line 213 */
/* :catch_0 */
/* move-exception v0 */
/* .line 215 */
/* .local v0, "e":Ljava/lang/Exception; */
/* const-wide/16 v1, 0x3e8 */
android.os.SystemClock .sleep ( v1,v2 );
/* .line 217 */
} // .end local v0 # "e":Ljava/lang/Exception;
} // :goto_0
return;
} // .end method
synchronized void sendResponse ( Integer p0, Integer p1, Object[] p2 ) {
/* .locals 6 */
/* .param p1, "msgType" # I */
/* .param p2, "msgId" # I */
/* .param p3, "byteMsg" # [B */
/* monitor-enter p0 */
/* .line 126 */
try { // :try_start_0
v0 = this.mOutputStream;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
if ( p3 != null) { // if-eqz p3, :cond_0
/* .line 128 */
try { // :try_start_1
final String v1 = "%04x"; // const-string v1, "%04x"
int v2 = 1; // const/4 v2, 0x1
/* new-array v3, v2, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p2 );
int v5 = 0; // const/4 v5, 0x0
/* aput-object v4, v3, v5 */
java.lang.String .format ( v1,v3 );
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
/* .line 129 */
v0 = this.mOutputStream;
final String v1 = "%04x"; // const-string v1, "%04x"
/* new-array v3, v2, [Ljava/lang/Object; */
java.lang.Integer .valueOf ( p1 );
/* aput-object v4, v3, v5 */
java.lang.String .format ( v1,v3 );
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
/* .line 130 */
v0 = this.mOutputStream;
final String v1 = "%08x"; // const-string v1, "%08x"
/* new-array v2, v2, [Ljava/lang/Object; */
/* array-length v3, p3 */
java.lang.Integer .valueOf ( v3 );
/* aput-object v3, v2, v5 */
java.lang.String .format ( v1,v2 );
(( java.lang.String ) v1 ).getBytes ( ); // invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
/* .line 131 */
v0 = this.mOutputStream;
(( java.io.OutputStream ) v0 ).write ( p3 ); // invoke-virtual {v0, p3}, Ljava/io/OutputStream;->write([B)V
/* .line 132 */
v0 = this.mOutputStream;
(( java.io.OutputStream ) v0 ).flush ( ); // invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 135 */
/* .line 133 */
} // .end local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
/* :catch_0 */
/* move-exception v0 */
/* .line 134 */
/* .local v0, "ex":Ljava/io/IOException; */
try { // :try_start_2
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
final String v2 = "Failed to write response:"; // const-string v2, "Failed to write response:"
android.util.Slog .e ( v1,v2,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 137 */
} // .end local v0 # "ex":Ljava/io/IOException;
} // :cond_0
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 125 */
} // .end local p1 # "msgType":I
} // .end local p2 # "msgId":I
} // .end local p3 # "byteMsg":[B
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
synchronized void sendResponse ( java.lang.String p0 ) {
/* .locals 3 */
/* .param p1, "msg" # Ljava/lang/String; */
/* monitor-enter p0 */
/* .line 116 */
try { // :try_start_0
v0 = this.mOutputStream;
/* :try_end_0 */
/* .catchall {:try_start_0 ..:try_end_0} :catchall_0 */
if ( v0 != null) { // if-eqz v0, :cond_0
/* .line 118 */
try { // :try_start_1
(( java.lang.String ) p1 ).getBytes ( ); // invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B
(( java.io.OutputStream ) v0 ).write ( v1 ); // invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
/* :try_end_1 */
/* .catch Ljava/io/IOException; {:try_start_1 ..:try_end_1} :catch_0 */
/* .catchall {:try_start_1 ..:try_end_1} :catchall_0 */
/* .line 121 */
/* .line 119 */
} // .end local p0 # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
/* :catch_0 */
/* move-exception v0 */
/* .line 120 */
/* .local v0, "ex":Ljava/io/IOException; */
try { // :try_start_2
final String v1 = "BrokenScreenRescueService"; // const-string v1, "BrokenScreenRescueService"
final String v2 = "Failed to write response:"; // const-string v2, "Failed to write response:"
android.util.Slog .e ( v1,v2,v0 );
/* :try_end_2 */
/* .catchall {:try_start_2 ..:try_end_2} :catchall_0 */
/* .line 123 */
} // .end local v0 # "ex":Ljava/io/IOException;
} // :cond_0
} // :goto_0
/* monitor-exit p0 */
return;
/* .line 115 */
} // .end local p1 # "msg":Ljava/lang/String;
/* :catchall_0 */
/* move-exception p1 */
/* monitor-exit p0 */
/* throw p1 */
} // .end method
public void setAdbEnabled ( Boolean p0 ) {
/* .locals 2 */
/* .param p1, "enabled" # Z */
/* .line 93 */
v0 = this.mHandler;
if ( p1 != null) { // if-eqz p1, :cond_0
int v1 = 1; // const/4 v1, 0x1
/* .line 94 */
} // :cond_0
int v1 = 2; // const/4 v1, 0x2
/* .line 93 */
} // :goto_0
(( android.os.Handler ) v0 ).sendEmptyMessage ( v1 ); // invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
/* .line 95 */
return;
} // .end method
