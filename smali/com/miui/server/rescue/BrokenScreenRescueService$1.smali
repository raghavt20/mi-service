.class Lcom/miui/server/rescue/BrokenScreenRescueService$1;
.super Landroid/os/Handler;
.source "BrokenScreenRescueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/rescue/BrokenScreenRescueService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;


# direct methods
.method constructor <init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Looper;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/rescue/BrokenScreenRescueService;
    .param p2, "looper"    # Landroid/os/Looper;

    .line 238
    iput-object p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .line 241
    iget v0, p1, Landroid/os/Message;->what:I

    const-string v1, "BrokenScreenRescueService"

    packed-switch v0, :pswitch_data_0

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handler default msg "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 259
    :pswitch_0
    iget-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$fputmContentEvent(Lcom/miui/server/rescue/BrokenScreenRescueService;Z)V

    .line 260
    goto :goto_0

    .line 243
    :pswitch_1
    iget-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$mcheckDisableBrokenScreenService(Lcom/miui/server/rescue/BrokenScreenRescueService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const-string v0, "Broken Screen Rescue Service has been disable!"

    invoke-static {v1, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    return-void

    .line 247
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 248
    .local v0, "now":J
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v2}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$fgetmlastMsmEventTime(Lcom/miui/server/rescue/BrokenScreenRescueService;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 250
    return-void

    .line 252
    :cond_1
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v2, v0, v1}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$fputmlastMsmEventTime(Lcom/miui/server/rescue/BrokenScreenRescueService;J)V

    .line 254
    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v2}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$mmatchContacts(Lcom/miui/server/rescue/BrokenScreenRescueService;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$1;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v2}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$mcheckSendSecurityPasswd(Lcom/miui/server/rescue/BrokenScreenRescueService;)V

    .line 265
    .end local v0    # "now":J
    :cond_2
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
