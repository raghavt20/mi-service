.class Lcom/miui/server/rescue/BrokenScreenRescueService$2;
.super Landroid/database/ContentObserver;
.source "BrokenScreenRescueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/server/rescue/BrokenScreenRescueService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;


# direct methods
.method constructor <init>(Lcom/miui/server/rescue/BrokenScreenRescueService;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lcom/miui/server/rescue/BrokenScreenRescueService;
    .param p2, "handler"    # Landroid/os/Handler;

    .line 268
    iput-object p1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$2;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 2
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 272
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://sms/raw"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$2;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v1}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$fgetmLastMsmUri(Lcom/miui/server/rescue/BrokenScreenRescueService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$2;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$fputmLastMsmUri(Lcom/miui/server/rescue/BrokenScreenRescueService;Ljava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/miui/server/rescue/BrokenScreenRescueService$2;->this$0:Lcom/miui/server/rescue/BrokenScreenRescueService;

    invoke-static {v0}, Lcom/miui/server/rescue/BrokenScreenRescueService;->-$$Nest$fgetmHandler(Lcom/miui/server/rescue/BrokenScreenRescueService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 278
    return-void

    .line 273
    :cond_1
    :goto_0
    return-void
.end method
