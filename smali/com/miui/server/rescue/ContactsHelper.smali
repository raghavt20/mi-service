.class public Lcom/miui/server/rescue/ContactsHelper;
.super Ljava/lang/Object;
.source "ContactsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/rescue/ContactsHelper$ContactsBean;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private wirteNumbers(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/miui/server/rescue/ContactsHelper$ContactsBean;)V
    .locals 8
    .param p1, "contentResolver"    # Landroid/content/ContentResolver;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "bean"    # Lcom/miui/server/rescue/ContactsHelper$ContactsBean;

    .line 72
    const-string v0, "data1"

    if-eqz p2, :cond_6

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 75
    :cond_0
    :try_start_0
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v0, v4, v1

    const-string v5, "display_name= ? "

    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 79
    .local v1, "dataCursor":Landroid/database/Cursor;
    if-nez v1, :cond_1

    .line 81
    return-void

    .line 83
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    .line 84
    invoke-virtual {p3, p2}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->setName(Ljava/lang/String;)V

    .line 86
    :cond_2
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 87
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "number":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 89
    :cond_3
    invoke-virtual {p3}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;->getNumList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    nop

    .end local v2    # "number":Ljava/lang/String;
    goto :goto_0

    .line 92
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :cond_5
    return-void

    .line 97
    .end local v1    # "dataCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->fillInStackTrace()Ljava/lang/Throwable;

    .line 99
    const-string v1, "BrokenScreenRescueService"

    const-string/jumbo v2, "stackTrace: "

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    return-void

    .line 72
    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public getContactsList(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/miui/server/rescue/ContactsHelper$ContactsBean;",
            ">;"
        }
    .end annotation

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v0, "contactsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/miui/server/rescue/ContactsHelper$ContactsBean;>;"
    const/4 v1, 0x0

    .line 54
    .local v1, "bean":Lcom/miui/server/rescue/ContactsHelper$ContactsBean;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 55
    .local v8, "resolver":Landroid/content/ContentResolver;
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v8

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 56
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-gtz v3, :cond_0

    goto :goto_1

    .line 60
    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 61
    new-instance v3, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;

    invoke-direct {v3, p0}, Lcom/miui/server/rescue/ContactsHelper$ContactsBean;-><init>(Lcom/miui/server/rescue/ContactsHelper;)V

    move-object v1, v3

    .line 62
    const-string v3, "display_name"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "name":Ljava/lang/String;
    if-nez v3, :cond_1

    goto :goto_0

    .line 64
    :cond_1
    invoke-direct {p0, v8, v3, v1}, Lcom/miui/server/rescue/ContactsHelper;->wirteNumbers(Landroid/content/ContentResolver;Ljava/lang/String;Lcom/miui/server/rescue/ContactsHelper$ContactsBean;)V

    .line 65
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    .end local v3    # "name":Ljava/lang/String;
    goto :goto_0

    .line 67
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 68
    return-object v0

    .line 57
    :cond_3
    :goto_1
    return-object v0
.end method
