.class public Lcom/miui/server/rescue/AutoUsbDebuggingManager;
.super Ljava/lang/Object;
.source "AutoUsbDebuggingManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;
    }
.end annotation


# static fields
.field static final BUFFER_SIZE:I = 0x1000

.field private static final TAG:Ljava/lang/String; = "BrokenScreenRescueService"


# instance fields
.field private final ADBD_SOCKET:Ljava/lang/String;

.field private final ADB_DIRECTORY:Ljava/lang/String;

.field private final ADB_KEYS_FILE:Ljava/lang/String;

.field private mAdbEnabled:Z

.field private final mHandler:Landroid/os/Handler;

.field mOutputStream:Ljava/io/OutputStream;

.field mSocket:Landroid/net/LocalSocket;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method static bridge synthetic -$$Nest$fgetmAdbEnabled(Lcom/miui/server/rescue/AutoUsbDebuggingManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mAdbEnabled:Z

    return p0
.end method

.method static bridge synthetic -$$Nest$fgetmThread(Lcom/miui/server/rescue/AutoUsbDebuggingManager;)Ljava/lang/Thread;
    .locals 0

    iget-object p0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mThread:Ljava/lang/Thread;

    return-object p0
.end method

.method static bridge synthetic -$$Nest$fputmAdbEnabled(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mAdbEnabled:Z

    return-void
.end method

.method static bridge synthetic -$$Nest$fputmThread(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Ljava/lang/Thread;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mThread:Ljava/lang/Thread;

    return-void
.end method

.method static bridge synthetic -$$Nest$mwriteKey(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->writeKey(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .param p1, "looper"    # Landroid/os/Looper;

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mAdbEnabled:Z

    .line 27
    const-string v0, "adbd"

    iput-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->ADBD_SOCKET:Ljava/lang/String;

    .line 28
    const-string v0, "misc/adb"

    iput-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->ADB_DIRECTORY:Ljava/lang/String;

    .line 29
    const-string v0, "adb_keys"

    iput-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->ADB_KEYS_FILE:Ljava/lang/String;

    .line 89
    new-instance v0, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;

    invoke-direct {v0, p0, p1}, Lcom/miui/server/rescue/AutoUsbDebuggingManager$UsbDebuggingHandler;-><init>(Lcom/miui/server/rescue/AutoUsbDebuggingManager;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mHandler:Landroid/os/Handler;

    .line 90
    return-void
.end method

.method private writeKey(Ljava/lang/String;)V
    .locals 7
    .param p1, "key"    # Ljava/lang/String;

    .line 140
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    .line 141
    .local v0, "dataDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    const-string v2, "misc/adb"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 143
    .local v1, "adbDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "BrokenScreenRescueService"

    if-nez v2, :cond_0

    .line 144
    const-string v2, "ADB data directory does not exist"

    invoke-static {v3, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    return-void

    .line 149
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v4, "adb_keys"

    invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 151
    .local v2, "keyFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 152
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 153
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x1a0

    const/4 v6, -0x1

    invoke-static {v4, v5, v6, v6}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 158
    :cond_1
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    .local v4, "fo":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 160
    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 164
    .end local v2    # "keyFile":Ljava/io/File;
    .end local v4    # "fo":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 158
    .restart local v2    # "keyFile":Ljava/io/File;
    .restart local v4    # "fo":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v5

    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v6

    :try_start_4
    invoke-virtual {v5, v6}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    .end local v0    # "dataDir":Ljava/io/File;
    .end local v1    # "adbDir":Ljava/io/File;
    .end local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    .end local p1    # "key":Ljava/lang/String;
    :goto_0
    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 162
    .end local v2    # "keyFile":Ljava/io/File;
    .end local v4    # "fo":Ljava/io/FileOutputStream;
    .restart local v0    # "dataDir":Ljava/io/File;
    .restart local v1    # "adbDir":Ljava/io/File;
    .restart local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    .restart local p1    # "key":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 163
    .local v2, "ex":Ljava/io/IOException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error writing key:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    .end local v2    # "ex":Ljava/io/IOException;
    :goto_1
    return-void
.end method


# virtual methods
.method declared-synchronized closeSocket()V
    .locals 4

    monitor-enter p0

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 100
    :try_start_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 103
    goto :goto_0

    .line 101
    .end local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    :catch_0
    move-exception v0

    .line 102
    .local v0, "ex":Ljava/io/IOException;
    :try_start_2
    const-string v1, "BrokenScreenRescueService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed closing output stream: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    .line 108
    :try_start_3
    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 111
    goto :goto_1

    .line 109
    :catch_1
    move-exception v0

    .line 110
    .restart local v0    # "ex":Ljava/io/IOException;
    :try_start_4
    const-string v1, "BrokenScreenRescueService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed closing socket: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 113
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method listenToSocket()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    const-string v0, "BrokenScreenRescueService"

    const/16 v1, 0x1000

    :try_start_0
    new-array v1, v1, [B

    .line 170
    .local v1, "buffer":[B
    new-instance v2, Landroid/net/LocalSocketAddress;

    const-string v3, "adbd"

    sget-object v4, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v2, v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    .line 172
    .local v2, "address":Landroid/net/LocalSocketAddress;
    const/4 v3, 0x0

    .line 174
    .local v3, "inputStream":Ljava/io/InputStream;
    new-instance v4, Landroid/net/LocalSocket;

    invoke-direct {v4}, Landroid/net/LocalSocket;-><init>()V

    iput-object v4, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    .line 175
    invoke-virtual {v4, v2}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 176
    const-string v4, "connected to adbd"

    invoke-static {v0, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v4, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    iput-object v4, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    .line 179
    iget-object v4, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    move-object v3, v4

    .line 182
    :goto_0
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 183
    .local v4, "count":I
    if-gez v4, :cond_0

    .line 184
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "got "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " reading"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    goto :goto_1

    .line 188
    :cond_0
    const/4 v5, 0x0

    aget-byte v6, v1, v5

    const/16 v7, 0x50

    const/4 v8, 0x2

    if-ne v6, v7, :cond_1

    const/4 v6, 0x1

    aget-byte v6, v1, v6

    const/16 v7, 0x4b

    if-ne v6, v7, :cond_1

    .line 189
    new-instance v5, Ljava/lang/String;

    invoke-static {v1, v8, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    .line 191
    .local v5, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mHandler:Landroid/os/Handler;

    .line 192
    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 193
    .local v6, "msg":Landroid/os/Message;
    iput-object v5, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 194
    iget-object v7, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 195
    nop

    .line 200
    .end local v4    # "count":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "msg":Landroid/os/Message;
    goto :goto_0

    .line 196
    .restart local v4    # "count":I
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Wrong message: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/String;

    .line 197
    invoke-static {v1, v5, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v5

    invoke-direct {v7, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 196
    invoke-static {v0, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    nop

    .line 205
    .end local v1    # "buffer":[B
    .end local v2    # "address":Landroid/net/LocalSocketAddress;
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .end local v4    # "count":I
    :goto_1
    invoke-virtual {p0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->closeSocket()V

    .line 206
    nop

    .line 207
    return-void

    .line 205
    :catchall_0
    move-exception v0

    goto :goto_2

    .line 201
    :catch_0
    move-exception v1

    .line 202
    .local v1, "ex":Ljava/io/IOException;
    :try_start_1
    const-string v2, "Communication error: "

    invoke-static {v0, v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 203
    nop

    .end local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    :goto_2
    invoke-virtual {p0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->closeSocket()V

    .line 206
    throw v0
.end method

.method public run()V
    .locals 3

    .line 212
    :try_start_0
    invoke-virtual {p0}, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->listenToSocket()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    .line 217
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method declared-synchronized sendResponse(II[B)V
    .locals 6
    .param p1, "msgType"    # I
    .param p2, "msgId"    # I
    .param p3, "byteMsg"    # [B

    monitor-enter p0

    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 128
    :try_start_1
    const-string v1, "%04x"

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 129
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    const-string v1, "%04x"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 130
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    const-string v1, "%08x"

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, p3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 131
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p3}, Ljava/io/OutputStream;->write([B)V

    .line 132
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    goto :goto_0

    .line 133
    .end local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    :catch_0
    move-exception v0

    .line 134
    .local v0, "ex":Ljava/io/IOException;
    :try_start_2
    const-string v1, "BrokenScreenRescueService"

    const-string v2, "Failed to write response:"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 137
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 125
    .end local p1    # "msgType":I
    .end local p2    # "msgId":I
    .end local p3    # "byteMsg":[B
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized sendResponse(Ljava/lang/String;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    monitor-enter p0

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mOutputStream:Ljava/io/OutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 118
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    goto :goto_0

    .line 119
    .end local p0    # "this":Lcom/miui/server/rescue/AutoUsbDebuggingManager;
    :catch_0
    move-exception v0

    .line 120
    .local v0, "ex":Ljava/io/IOException;
    :try_start_2
    const-string v1, "BrokenScreenRescueService"

    const-string v2, "Failed to write response:"

    invoke-static {v1, v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 115
    .end local p1    # "msg":Ljava/lang/String;
    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setAdbEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .line 93
    iget-object v0, p0, Lcom/miui/server/rescue/AutoUsbDebuggingManager;->mHandler:Landroid/os/Handler;

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 94
    :cond_0
    const/4 v1, 0x2

    .line 93
    :goto_0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 95
    return-void
.end method
