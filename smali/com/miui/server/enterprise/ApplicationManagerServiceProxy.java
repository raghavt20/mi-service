public class com.miui.server.enterprise.ApplicationManagerServiceProxy {
	 /* .source "ApplicationManagerServiceProxy.java" */
	 /* # static fields */
	 private static final java.lang.String TAG;
	 /* # direct methods */
	 public com.miui.server.enterprise.ApplicationManagerServiceProxy ( ) {
		 /* .locals 0 */
		 /* .line 16 */
		 /* invoke-direct {p0}, Ljava/lang/Object;-><init>()V */
		 return;
	 } // .end method
	 static void installPackageAsUser ( android.content.Context p0, com.android.server.pm.PackageManagerService$IPackageManagerImpl p1, java.lang.String p2, com.miui.enterprise.sdk.IEpInstallPackageObserver p3, Integer p4, java.lang.String p5, Integer p6 ) {
		 /* .locals 5 */
		 /* .param p0, "context" # Landroid/content/Context; */
		 /* .param p1, "pms" # Lcom/android/server/pm/PackageManagerService$IPackageManagerImpl; */
		 /* .param p2, "path" # Ljava/lang/String; */
		 /* .param p3, "observer" # Lcom/miui/enterprise/sdk/IEpInstallPackageObserver; */
		 /* .param p4, "flag" # I */
		 /* .param p5, "installerPkg" # Ljava/lang/String; */
		 /* .param p6, "userId" # I */
		 /* .line 22 */
		 /* new-instance v0, Landroid/content/Intent; */
		 /* invoke-direct {v0}, Landroid/content/Intent;-><init>()V */
		 /* .line 23 */
		 /* .local v0, "intent":Landroid/content/Intent; */
		 /* new-instance v1, Landroid/content/ComponentName; */
		 final String v2 = "com.miui.securitycore"; // const-string v2, "com.miui.securitycore"
		 final String v3 = "com.miui.enterprise.service.EntInstallService"; // const-string v3, "com.miui.enterprise.service.EntInstallService"
		 /* invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V */
		 (( android.content.Intent ) v0 ).setComponent ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
		 /* .line 24 */
		 /* new-instance v1, Landroid/os/Bundle; */
		 /* invoke-direct {v1}, Landroid/os/Bundle;-><init>()V */
		 /* .line 25 */
		 /* .local v1, "bundle":Landroid/os/Bundle; */
		 if ( p3 != null) { // if-eqz p3, :cond_0
			 /* .line 26 */
			 /* new-instance v2, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy$1; */
			 /* invoke-direct {v2, p3}, Lcom/miui/server/enterprise/ApplicationManagerServiceProxy$1;-><init>(Lcom/miui/enterprise/sdk/IEpInstallPackageObserver;)V */
			 /* .line 42 */
			 /* .local v2, "iMessenger":Landroid/os/IMessenger; */
			 final String v3 = "callback"; // const-string v3, "callback"
			 (( android.os.Bundle ) v1 ).putIBinder ( v3, v4 ); // invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putIBinder(Ljava/lang/String;Landroid/os/IBinder;)V
			 /* .line 44 */
		 } // .end local v2 # "iMessenger":Landroid/os/IMessenger;
	 } // :cond_0
	 final String v2 = "apkPath"; // const-string v2, "apkPath"
	 (( android.os.Bundle ) v1 ).putString ( v2, p2 ); // invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 45 */
	 final String v2 = "flag"; // const-string v2, "flag"
	 (( android.os.Bundle ) v1 ).putInt ( v2, p4 ); // invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
	 /* .line 46 */
	 final String v2 = "installerPkg"; // const-string v2, "installerPkg"
	 (( android.os.Bundle ) v1 ).putString ( v2, p5 ); // invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
	 /* .line 47 */
	 /* const-string/jumbo v2, "userId" */
	 (( android.os.Bundle ) v1 ).putInt ( v2, p6 ); // invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
	 /* .line 48 */
	 (( android.content.Intent ) v0 ).putExtras ( v1 ); // invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
	 /* .line 49 */
	 (( android.content.Context ) p0 ).startService ( v0 ); // invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
	 /* .line 50 */
	 return;
} // .end method
