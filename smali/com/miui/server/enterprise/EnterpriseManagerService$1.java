class com.miui.server.enterprise.EnterpriseManagerService$1 extends android.content.BroadcastReceiver {
	 /* .source "EnterpriseManagerService.java" */
	 /* # annotations */
	 /* .annotation system Ldalvik/annotation/EnclosingClass; */
	 /* value = Lcom/miui/server/enterprise/EnterpriseManagerService; */
} // .end annotation
/* .annotation system Ldalvik/annotation/InnerClass; */
/* accessFlags = 0x0 */
/* name = null */
} // .end annotation
/* # instance fields */
final com.miui.server.enterprise.EnterpriseManagerService this$0; //synthetic
/* # direct methods */
 com.miui.server.enterprise.EnterpriseManagerService$1 ( ) {
/* .locals 0 */
/* .param p1, "this$0" # Lcom/miui/server/enterprise/EnterpriseManagerService; */
/* .line 55 */
this.this$0 = p1;
/* invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V */
return;
} // .end method
/* # virtual methods */
public void onReceive ( android.content.Context p0, android.content.Intent p1 ) {
/* .locals 3 */
/* .param p1, "context" # Landroid/content/Context; */
/* .param p2, "intent" # Landroid/content/Intent; */
/* .line 58 */
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
/* if-nez v0, :cond_0 */
/* .line 59 */
return;
/* .line 61 */
} // :cond_0
(( android.content.Intent ) p2 ).getAction ( ); // invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
v1 = (( java.lang.String ) v0 ).hashCode ( ); // invoke-virtual {v0}, Ljava/lang/String;->hashCode()I
int v2 = 0; // const/4 v2, 0x0
/* sparse-switch v1, :sswitch_data_0 */
} // :cond_1
/* :sswitch_0 */
final String v1 = "android.intent.action.USER_SWITCHED"; // const-string v1, "android.intent.action.USER_SWITCHED"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
int v0 = 2; // const/4 v0, 0x2
/* :sswitch_1 */
final String v1 = "android.intent.action.BOOT_COMPLETED"; // const-string v1, "android.intent.action.BOOT_COMPLETED"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
/* move v0, v2 */
/* :sswitch_2 */
final String v1 = "com.miui.enterprise.ACTION_CERT_UPDATE"; // const-string v1, "com.miui.enterprise.ACTION_CERT_UPDATE"
v0 = (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
if ( v0 != null) { // if-eqz v0, :cond_1
	 int v0 = 4; // const/4 v0, 0x4
	 /* :sswitch_3 */
	 final String v1 = "android.intent.action.USER_STARTED"; // const-string v1, "android.intent.action.USER_STARTED"
	 v0 = 	 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
	 if ( v0 != null) { // if-eqz v0, :cond_1
		 int v0 = 1; // const/4 v0, 0x1
		 /* :sswitch_4 */
		 final String v1 = "android.intent.action.USER_REMOVED"; // const-string v1, "android.intent.action.USER_REMOVED"
		 v0 = 		 (( java.lang.String ) v0 ).equals ( v1 ); // invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
		 if ( v0 != null) { // if-eqz v0, :cond_1
			 int v0 = 3; // const/4 v0, 0x3
		 } // :goto_0
		 int v0 = -1; // const/4 v0, -0x1
	 } // :goto_1
	 final String v1 = "android.intent.extra.user_handle"; // const-string v1, "android.intent.extra.user_handle"
	 /* packed-switch v0, :pswitch_data_0 */
	 /* .line 78 */
	 /* :pswitch_0 */
	 v0 = this.this$0;
	 final String v1 = "extra_ent_cert"; // const-string v1, "extra_ent_cert"
	 (( android.content.Intent ) p2 ).getParcelableExtra ( v1 ); // invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
	 /* check-cast v1, Lcom/miui/enterprise/signature/EnterpriseCer; */
	 com.miui.server.enterprise.EnterpriseManagerService .-$$Nest$fputmCert ( v0,v1 );
	 /* .line 79 */
	 /* .line 74 */
	 /* :pswitch_1 */
	 v0 = 	 (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
	 /* .line 75 */
	 /* .local v0, "userId":I */
	 v1 = this.this$0;
	 com.miui.server.enterprise.EnterpriseManagerService .-$$Nest$monUserRemoved ( v1,v0 );
	 /* .line 76 */
	 /* .line 70 */
} // .end local v0 # "userId":I
/* :pswitch_2 */
v0 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 71 */
/* .restart local v0 # "userId":I */
v1 = this.this$0;
com.miui.server.enterprise.EnterpriseManagerService .-$$Nest$monUserSwitched ( v1,v0 );
/* .line 72 */
/* .line 66 */
} // .end local v0 # "userId":I
/* :pswitch_3 */
v0 = (( android.content.Intent ) p2 ).getIntExtra ( v1, v2 ); // invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I
/* .line 67 */
/* .restart local v0 # "userId":I */
v1 = this.this$0;
com.miui.server.enterprise.EnterpriseManagerService .-$$Nest$monUserStarted ( v1,v0 );
/* .line 68 */
/* .line 63 */
} // .end local v0 # "userId":I
/* :pswitch_4 */
v0 = this.this$0;
com.miui.server.enterprise.EnterpriseManagerService .-$$Nest$mbootComplete ( v0 );
/* .line 64 */
/* nop */
/* .line 82 */
} // :goto_2
return;
/* :sswitch_data_0 */
/* .sparse-switch */
/* -0x7ad942ef -> :sswitch_4 */
/* -0x2d021ace -> :sswitch_3 */
/* 0x3f221b7 -> :sswitch_2 */
/* 0x2f94f923 -> :sswitch_1 */
/* 0x392cb822 -> :sswitch_0 */
} // .end sparse-switch
/* :pswitch_data_0 */
/* .packed-switch 0x0 */
/* :pswitch_4 */
/* :pswitch_3 */
/* :pswitch_2 */
/* :pswitch_1 */
/* :pswitch_0 */
} // .end packed-switch
} // .end method
